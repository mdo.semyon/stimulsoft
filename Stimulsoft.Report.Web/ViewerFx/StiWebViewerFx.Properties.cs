#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Caching;
using Stimulsoft.Base;
using System.Globalization;

namespace Stimulsoft.Report.Web
{
    public partial class StiWebViewerFx :
        WebControl,
        INamingContainer
    {
        #region Appearance

        private StiViewerFxTheme theme = StiViewerFxTheme.Office2013;
        /// <summary>
        /// Gets or sets the current visual theme which is used for drawing visual elements of the viewer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiViewerFxTheme.Office2013)]
        [Description("Gets or sets the current visual theme which is used for drawing visual elements of the viewer.")]
        public StiViewerFxTheme Theme
        {
            get
            {
                return theme;
            }
            set
            {
                theme = value;
            }
        }

        private string localization = string.Empty;
        /// <summary>
        /// Gets or sets a path to the localization file for the viewer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets a path to the localization file for the viewer.")]
        public string Localization
        {
            get
            {
                return localization;
            }
            set
            {
                localization = value;
            }
        }


        private string aboutDialogTextLine1 = string.Empty;
        /// <summary>
        /// Gets or sets the text to display in the About dialog of the viewer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets the text to display in the About dialog of the viewer.")]
        public string AboutDialogTextLine1
        {
            get
            {
                return aboutDialogTextLine1;
            }
            set
            {
                aboutDialogTextLine1 = value;
            }
        }

        private string aboutDialogTextLine2 = string.Empty;
        /// <summary>
        /// Gets or sets the text to display in the About dialog of the viewer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets the text to display in the About dialog of the viewer.")]
        public string AboutDialogTextLine2
        {
            get
            {
                return aboutDialogTextLine2;
            }
            set
            {
                aboutDialogTextLine2 = value;
            }
        }

        private string aboutDialogUrl = string.Empty;
        /// <summary>
        /// Gets or sets the URL to display in the About dialog of the viewer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets the URL to display in the About dialog of the viewer.")]
        public string AboutDialogUrl
        {
            get
            {
                return aboutDialogUrl;
            }
            set
            {
                aboutDialogUrl = value;
            }
        }

        private string aboutDialogUrlText = string.Empty;
        /// <summary>
        /// Gets or sets the URL text to display in the About dialog of the viewer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets the URL text to display in the About dialog of the viewer.")]
        public string AboutDialogUrlText
        {
            get
            {
                return aboutDialogUrlText;
            }
            set
            {
                aboutDialogUrlText = value;
            }
        }

        private bool showCancelButton = false;
        /// <summary>
        /// Gets or sets the value that displays or hides the Cancel button in the window of getting the data from the server.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that displays or hides the Cancel button in the window of getting the data from the server.")]
        public bool ShowCancelButton
        {
            get
            {
                return showCancelButton;
            }
            set
            {
                showCancelButton = value;
            }
        }

        /// <summary>
        /// Gets or sets the background color of the viewer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(typeof(Color), "White")]
        [Description("Gets or sets the background color of the viewer.")]
        [TypeConverter(typeof(WebColorConverter))]
        public Color BackgroundColor
        {
            get
            {
                return this.BackColor;
            }
            set
            {
                this.BackColor = value;
            }
        }

        private StiWMode flashWMode = StiWMode.Window;
        /// <summary>
        /// Gets or sets the Window Mode property of the SWF file for transparency, layering, positioning, and rendering in the browser.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiWMode.Window)]
        [Description("Gets or sets the Window Mode property of the SWF file for transparency, layering, positioning, and rendering in the browser.")]
        public StiWMode FlashWMode
        {
            get
            {
                return flashWMode;
            }
            set
            {
                flashWMode = value;
            }
        }

        private bool autoHideScrollbars = false;
        /// <summary>
        /// Gets or sets the value that allows automatically hide report scrollbars if not required.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that allows automatically hide report scrollbars if not required.")]
        public bool AutoHideScrollbars
        {
            get
            {
                return autoHideScrollbars;
            }
            set
            {
                autoHideScrollbars = value;
            }
        }

        private Color currentPageBorderColor = Color.Gold;
        /// <summary>
        /// Gets or sets a color of the border of the current report page.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(typeof(Color), "Gold")]
        [Description("Gets or sets a color of the border of the current report page.")]
        [TypeConverter(typeof(WebColorConverter))]
        public Color CurrentPageBorderColor
        {
            get
            {
                return currentPageBorderColor;
            }
            set
            {
                currentPageBorderColor = value;
            }
        }

        private int parametersPanelColumnsCount = 2;
        /// <summary>
        /// Gets or sets a count columns in parameters panel.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(2)]
        [Description("Gets or sets a count columns in parameters panel.")]
        public int ParametersPanelColumnsCount
        {
            get
            {
                return parametersPanelColumnsCount;
            }
            set
            {
                parametersPanelColumnsCount = value;
            }
        }

        private int parametersPanelEditorWidth = 200;
        /// <summary>
        /// Gets or sets a editor width in parameters panel.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(200)]
        [Description("Gets or sets a editor width in parameters panel.")]
        public int ParametersPanelEditorWidth
        {
            get
            {
                return parametersPanelEditorWidth;
            }
            set
            {
                parametersPanelEditorWidth = value;
            }
        }

        private string parametersPanelDateFormat = StiDateFormatMode.FromClient;
        /// <summary>
        /// Gets or sets a date format for datetime parameters in parameters panel. To use a server date format please set the StiDateFormatMode.FromServer or "FromServer" string value.
        /// The default is the client system date format.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets a date format for datetime parameters in parameters panel. To use a server date format please set the StiDateFormatMode.FromServer or \"FromServer\" string value. The default is the client system date format.")]
        public string ParametersPanelDateFormat
        {
            get
            {
                if (parametersPanelDateFormat == StiDateFormatMode.FromServer) return CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern + " " + CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern;
                return parametersPanelDateFormat;
            }
            set
            {
                parametersPanelDateFormat = value;
            }
        }

        private string openLinksWindow = StiTargetWindow.Blank;
        /// <summary>
        /// Gets or sets a browser window to open links from the report.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiTargetWindow.Blank)]
        [Description("Gets or sets a browser window to open links from the report.")]
        public string OpenLinksWindow
        {
            get
            {
                return openLinksWindow;
            }
            set
            {
                openLinksWindow = value;
            }
        }

        private string openExportedReportWindow = StiTargetWindow.Blank;
        /// <summary>
        /// Gets or sets a browser window to open the exported report.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiTargetWindow.Blank)]
        [Description("Gets or sets a browser window to open the exported report.")]
        public string OpenExportedReportWindow
        {
            get
            {
                return openExportedReportWindow;
            }
            set
            {
                openExportedReportWindow = value;
            }
        }

        private string designWindow = StiTargetWindow.Self;
        /// <summary>
        /// Gets or sets a browser window to open page at design event.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiTargetWindow.Self)]
        [Description("Gets or sets a browser window to open page at design event.")]
        public string DesignWindow
        {
            get
            {
                return designWindow;
            }
            set
            {
                designWindow = value;
            }
        }

        /// <summary>
        /// Gets or sets an images quality for the report components such as Rich Text, Rotated Text, Charts, Bar Codes.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiImagesQuality.Normal)]
        [Description("Gets or sets an images quality for the report components such as Rich Text, Rotated Text, Charts, Bar Codes.")]
        public StiImagesQuality ImagesQuality
        {
            get
            {
                return StiReportHelperFx.ImagesQuality;
            }
            set
            {
                StiReportHelperFx.ImagesQuality = value;
            }
        }

        private bool showTooltips = true;
        /// <summary>
        /// Gets or sets a value which indicates that show or hide tooltips.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that show or hide tooltips.")]
        public bool ShowTooltips
        {
            get
            {
                return showTooltips;
            }
            set
            {
                showTooltips = value;
            }
        }

        private bool showTooltipsHelp = true;
        /// <summary>
        /// Gets or sets a value which indicates that show or hide the help link in tooltips.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that show or hide the help link in tooltips.")]
        public bool ShowTooltipsHelp
        {
            get
            {
                return showTooltipsHelp;
            }
            set
            {
                showTooltipsHelp = value;
            }
        }

        private bool showFormsHelp = true;
        /// <summary>
        /// Gets or sets a value which indicates that show or hide the help button in dialogs.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that show or hide the help button in dialogs.")]
        public bool ShowFormsHelp
        {
            get
            {
                return showFormsHelp;
            }
            set
            {
                showFormsHelp = value;
            }
        }

        private bool showFormsHints = true;
        /// <summary>
        /// Gets or sets a value which indicates that show or hide help tips in dialogs.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that show or hide help tips in dialogs.")]
        public bool ShowFormsHints
        {
            get
            {
                return showFormsHints;
            }
            set
            {
                showFormsHints = value;
            }
        }

        private string browserTitle = "Default";
        /// Gets or sets a value which change the title of the window that contains the viewer.
        /// If empty, it will use the alias or the name of the report. If "Default", it will use the title of the HTML page.
        [Category("Appearance")]
        [Description("Gets or sets a value which change the title of the window that contains the viewer. If empty, it will use the alias or the name of the report. If \"Default\", it will use the title of the HTML page.")]
        public string BrowserTitle
        {
            get
            {
                return browserTitle;
            }
            set
            {
                browserTitle = value;
            }
        }

        #endregion

        #region Email

        private bool showEmailDialog = true;
        /// <summary>
        /// Gets or sets a value which allows to display the Email dialog, or send Email with the default settings.
        /// </summary>
        [Category("Email")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows to display the Email dialog, or send Email with the default settings.")]
        public bool ShowEmailDialog
        {
            get
            {
                return showEmailDialog;
            }
            set
            {
                showEmailDialog = value;
            }
        }

        private bool showEmailExportDialog = true;
        /// <summary>
        /// Gets or sets a value which allows to display the export dialog for Email, or export report for Email with the default settings.
        /// </summary>
        [Category("Email")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows to display the export dialog for Email, or export report for Email with the default settings.")]
        public bool ShowEmailExportDialog
        {
            get
            {
                return showEmailExportDialog;
            }
            set
            {
                showEmailExportDialog = value;
            }
        }

        private string defaultEmailAddress = string.Empty;
        /// <summary>
        /// Gets or sets the default email address of the message created in the viewer.
        /// </summary>
        [Category("Email")]
        [DefaultValue("")]
        [Description("Gets or sets the default email address of the message created in the viewer.")]
        public string DefaultEmailAddress
        {
            get
            {
                return defaultEmailAddress;
            }
            set
            {
                defaultEmailAddress = value;
            }
        }

        #endregion

        #region Exports

        private bool showExportDialog = true;
        /// <summary>
        /// Gets or sets a value which allows to display the export dialog, or to export with the default settings.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows to display the export dialog, or to export with the default settings.")]
        public bool ShowExportDialog
        {
            get
            {
                return showExportDialog;
            }
            set
            {
                showExportDialog = value;
            }
        }

        private bool showExportToDocument = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the report document file.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the report document file.")]
        public bool ShowExportToDocument
        {
            get
            {
                return showExportToDocument;
            }
            set
            {
                showExportToDocument = value;
            }
        }

        private bool showExportToPdf = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the PDF format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the PDF format.")]
        public bool ShowExportToPdf
        {
            get
            {
                return showExportToPdf;
            }
            set
            {
                showExportToPdf = value;
            }
        }

        private bool showExportToXps = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the XPS format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the XPS format.")]
        public bool ShowExportToXps
        {
            get
            {
                return showExportToXps;
            }
            set
            {
                showExportToXps = value;
            }
        }

        private bool showExportToPowerPoint = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the Power Point 2007-2010 format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the Power Point 2007-2010 format.")]
        public bool ShowExportToPowerPoint
        {
            get
            {
                return showExportToPowerPoint;
            }
            set
            {
                showExportToPowerPoint = value;
            }
        }

        private bool showExportToHtml = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the HTML format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the HTML format.")]
        public bool ShowExportToHtml
        {
            get
            {
                return showExportToHtml;
            }
            set
            {
                showExportToHtml = value;
            }
        }

        private bool showExportToHtml5 = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the HTML5 format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the HTML5 format.")]
        public bool ShowExportToHtml5
        {
            get
            {
                return showExportToHtml5;
            }
            set
            {
                showExportToHtml5 = value;
            }
        }

        private bool showExportToMht = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the MHT (Web Archive) format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the MHT (Web Archive) format.")]
        public bool ShowExportToMht
        {
            get
            {
                return showExportToMht;
            }
            set
            {
                showExportToMht = value;
            }
        }

        private bool showExportToText = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the TEXT format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the TEXT format.")]
        public bool ShowExportToText
        {
            get
            {
                return showExportToText;
            }
            set
            {
                showExportToText = value;
            }
        }

        private bool showExportToRtf = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the Rich Text format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the Rich Text format.")]
        public bool ShowExportToRtf
        {
            get
            {
                return showExportToRtf;
            }
            set
            {
                showExportToRtf = value;
            }
        }

        private bool showExportToWord2007 = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the Word 2007-2010 format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the Word 2007-2010 format.")]
        public bool ShowExportToWord2007
        {
            get
            {
                return showExportToWord2007;
            }
            set
            {
                showExportToWord2007 = value;
            }
        }

        private bool showExportToOpenDocumentWriter = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the Open Document Text format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the Open Document Text format.")]
        public bool ShowExportToOpenDocumentWriter
        {
            get
            {
                return showExportToOpenDocumentWriter;
            }
            set
            {
                showExportToOpenDocumentWriter = value;
            }
        }

        private bool showExportToExcel = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the Excel BIFF format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the Excel BIFF format.")]
        public bool ShowExportToExcel
        {
            get
            {
                return showExportToExcel;
            }
            set
            {
                showExportToExcel = value;
            }
        }

        private bool showExportToExcelXml = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the Excel XML format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the Excel XML format.")]
        public bool ShowExportToExcelXml
        {
            get
            {
                return showExportToExcelXml;
            }
            set
            {
                showExportToExcelXml = value;
            }
        }

        private bool showExportToExcel2007 = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the Excel 2007-2010 format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the Excel 2007-2010 format.")]
        public bool ShowExportToExcel2007
        {
            get
            {
                return showExportToExcel2007;
            }
            set
            {
                showExportToExcel2007 = value;
            }
        }

        private bool showExportToOpenDocumentCalc = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the Open Document Calc format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the Open Document Calc format.")]
        public bool ShowExportToOpenDocumentCalc
        {
            get
            {
                return showExportToOpenDocumentCalc;
            }
            set
            {
                showExportToOpenDocumentCalc = value;
            }
        }

        private bool showExportToCsv = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the CSV format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the CSV format.")]
        public bool ShowExportToCsv
        {
            get
            {
                return showExportToCsv;
            }
            set
            {
                showExportToCsv = value;
            }
        }

        private bool showExportToDbf = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the DBF format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the DBF format.")]
        public bool ShowExportToDbf
        {
            get
            {
                return showExportToDbf;
            }
            set
            {
                showExportToDbf = value;
            }
        }

        private bool showExportToXml = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the XML format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the XML format.")]
        public bool ShowExportToXml
        {
            get
            {
                return showExportToXml;
            }
            set
            {
                showExportToXml = value;
            }
        }

        private bool showExportToDif = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the DIF format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the DIF format.")]
        public bool ShowExportToDif
        {
            get
            {
                return showExportToDif;
            }
            set
            {
                showExportToDif = value;
            }
        }

        private bool showExportToSylk = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the Sylk format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the Sylk format.")]
        public bool ShowExportToSylk
        {
            get
            {
                return showExportToSylk;
            }
            set
            {
                showExportToSylk = value;
            }
        }

        private bool showExportToImageBmp = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the BMP image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the BMP image format.")]
        public bool ShowExportToImageBmp
        {
            get
            {
                return showExportToImageBmp;
            }
            set
            {
                showExportToImageBmp = value;
            }
        }

        private bool showExportToImageGif = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the GIF image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the GIF image format.")]
        public bool ShowExportToImageGif
        {
            get
            {
                return showExportToImageGif;
            }
            set
            {
                showExportToImageGif = value;
            }
        }

        private bool showExportToImageJpeg = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the JPEG image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the JPEG image format.")]
        public bool ShowExportToImageJpeg
        {
            get
            {
                return showExportToImageJpeg;
            }
            set
            {
                showExportToImageJpeg = value;
            }
        }

        private bool showExportToImagePcx = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the PCX image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the PCX image format.")]
        public bool ShowExportToImagePcx
        {
            get
            {
                return showExportToImagePcx;
            }
            set
            {
                showExportToImagePcx = value;
            }
        }

        private bool showExportToImagePng = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the PNG image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the PNG image format.")]
        public bool ShowExportToImagePng
        {
            get
            {
                return showExportToImagePng;
            }
            set
            {
                showExportToImagePng = value;
            }
        }

        private bool showExportToImageTiff = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the TIFF image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the TIFF image format.")]
        public bool ShowExportToImageTiff
        {
            get
            {
                return showExportToImageTiff;
            }
            set
            {
                showExportToImageTiff = value;
            }
        }

        private bool showExportToImageMetafile = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the Metafile image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the Metafile image format.")]
        public bool ShowExportToImageMetafile
        {
            get
            {
                return showExportToImageMetafile;
            }
            set
            {
                showExportToImageMetafile = value;
            }
        }

        private bool showExportToImageSvg = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the SVG image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the SVG image format.")]
        public bool ShowExportToImageSvg
        {
            get
            {
                return showExportToImageSvg;
            }
            set
            {
                showExportToImageSvg = value;
            }
        }

        private bool showExportToImageSvgz = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the viewer to the SVGZ image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the viewer to the SVGZ image format.")]
        public bool ShowExportToImageSvgz
        {
            get
            {
                return showExportToImageSvgz;
            }
            set
            {
                showExportToImageSvgz = value;
            }
        }

        #endregion
        
        #region Internal

        private static bool IsDesignMode
        {
            get
            {
                return HttpContext.Current == null;
            }
        }

        private StiRequestParams requestParams = null;
        private StiRequestParams RequestParams
        {
            get
            {
                if (requestParams == null) requestParams = GetRequestParams();
                return requestParams;
            }
        }

        private string clientGuid = null;
        private string ClientGuid
        {
            get
            {
                if (clientGuid == null) clientGuid = StiGuidUtils.NewGuid();
                return clientGuid;
            }
            set
            {
                clientGuid = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public override ClientIDMode ClientIDMode
        {
            get
            {
                return base.ClientIDMode;
            }
            set
            {
                base.ClientIDMode = value;
            }
        }

        private string userScript = null;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        public string UserScript
        {
            get
            {
                return userScript;
            }
            set
            {
                userScript = value;
            }
        }

        #endregion

        #region Print

        private bool showPrintDialog = true;
        /// <summary>
        /// Gets or sets a value which allows to display the Print dialog, or print report with the default settings.
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows to display the Print dialog, or print report with the default settings.")]
        public bool ShowPrintDialog
        {
            get
            {
                return showPrintDialog;
            }
            set
            {
                showPrintDialog = value;
            }
        }

        private bool autoPageOrientation = true;
        /// <summary>
        /// Gets or sets a value which allows the automatic page orientation when printing the report.
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows the automatic page orientation when printing the report.")]
        public bool AutoPageOrientation
        {
            get
            {
                return autoPageOrientation;
            }
            set
            {
                autoPageOrientation = value;
            }
        }

        private bool autoPageScale = true;
        /// <summary>
        /// Gets or sets a value which allows the automatic page scale when printing the report.
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows the automatic page scale when printing the report.")]
        public bool AutoPageScale
        {
            get
            {
                return autoPageScale;
            }
            set
            {
                autoPageScale = value;
            }
        }

        private bool allowDefaultPrint = true;
        /// <summary>
        /// Gets or sets a value which allows Default printing (by Flash engine).
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows Default printing (by Flash engine).")]
        public bool AllowDefaultPrint
        {
            get
            {
                return allowDefaultPrint;
            }
            set
            {
                allowDefaultPrint = value;
            }
        }

        private bool allowPrintToPdf = true;
        /// <summary>
        /// Gets or sets a value which allows printing in PDF format.
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows printing in PDF format.")]
        public bool AllowPrintToPdf
        {
            get
            {
                return allowPrintToPdf;
            }
            set
            {
                allowPrintToPdf = value;
            }
        }

        private bool allowPrintToHtml = true;
        /// <summary>
        /// Gets or sets a value which allows printing in HTML format.
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows printing in HTML format.")]
        public bool AllowPrintToHtml
        {
            get
            {
                return allowPrintToHtml;
            }
            set
            {
                allowPrintToHtml = value;
            }
        }

        private bool printAsBitmap = true;
        /// <summary>
        /// Gets or sets a value which allows send the report to print as Image (only for Default format).
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows send the report to print as Image (only for Default format).")]
        public bool PrintAsBitmap
        {
            get
            {
                return printAsBitmap;
            }
            set
            {
                printAsBitmap = value;
            }
        }

        #endregion

        #region Report

        private StiReport report = null;
        /// <summary>
        /// Gets or sets a report object which is shown in the viewer.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets a report object which is shown in the viewer.")]
        public StiReport Report
        {
            get
            {
                if (report == null) report = GetReportObject(this.RequestParams);
                return report;
            }
            set
            {
                report = value;
                if (this.RequestParams.Component != StiComponentType.ViewerFx) this.requestParams = this.CreateRequestParams();
                InvokeGetReportData();
                this.RequestParams.Cache.Helper.SaveReportInternal(this.RequestParams, report);
            }
        }

        #endregion

        #region Server

        private static StiCacheHelper cacheHelper = null;
        /// <summary>
        /// Gets or sets an instance of the StiCacheHelper class that will be used for report caching on the server side.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets an instance of the StiCacheHelper class that will be used for report caching on the server side.")]
        public static StiCacheHelper CacheHelper
        {
            get
            {
                if (cacheHelper == null) cacheHelper = new StiCacheHelper();
                return cacheHelper;
            }
            set
            {
                cacheHelper = value;
            }
        }

        private int requestTimeout = 30;
        /// <summary>
        /// Gets or sets time which indicates how many seconds the client side will wait for the response from the server side. The default value is 30 seconds.
        /// </summary>
        [Category("Server")]
        [DefaultValue(30)]
        [Description("Gets or sets time which indicates how many seconds the client side will wait for the response from the server side. The default value is 30 seconds.")]
        public int RequestTimeout
        {
            get
            {
                return requestTimeout;
            }
            set
            {
                // Min 1 sec. Max 6 hours.
                requestTimeout = Math.Max(1, Math.Min(21600, value));
            }
        }

        private int cacheTimeout = 10;
        /// <summary>
        /// Gets or sets time which indicates how many minutes the result of the report rendering will be stored in the server cache or session. The default value is 10 minutes.
        /// </summary>
        [Category("Server")]
        [DefaultValue(10)]
        [Description("Gets or sets time which indicates how many minutes the result of the report rendering will be stored in the server cache or session. The default value is 10 minutes.")]
        public int CacheTimeout
        {
            get
            {
                return cacheTimeout;
            }
            set
            {
                cacheTimeout = value;
            }
        }

        private StiServerCacheMode cacheMode = StiServerCacheMode.ObjectCache;
        /// <summary>
        /// Gets or sets the mode of the report caching.
        /// </summary>
        [Category("Server")]
        [DefaultValue(StiServerCacheMode.ObjectCache)]
        [Description("Gets or sets the mode of the report caching.")]
        public StiServerCacheMode CacheMode
        {
            get
            {
                return cacheMode;
            }
            set
            {
                cacheMode = value;
            }
        }

        private CacheItemPriority cacheItemPriority = CacheItemPriority.Default;
        /// <summary>
        /// Specifies the relative priority of report, stored in the system cache.
        /// </summary>
        [Category("Server")]
        [DefaultValue(CacheItemPriority.Default)]
        [Description("Specifies the relative priority of report, stored in the system cache.")]
        public CacheItemPriority CacheItemPriority
        {
            get
            {
                return cacheItemPriority;
            }
            set
            {
                cacheItemPriority = value;
            }
        }

        private int repeatCount = 1;
        /// <summary>
        /// Gets or sets the the number of repeats of requests of the server side to the client side, when getting errors of obtaining data.
        /// </summary>
        [Category("Server")]
        [DefaultValue(1)]
        [Description("Gets or sets the the number of repeats of requests of the server side to the client side, when getting errors of obtaining data.")]
        public int RepeatCount
        {
            get
            {
                return repeatCount;
            }
            set
            {
                repeatCount = value;
            }
        }

        private bool enableDataLogger = false;
        /// <summary>
        /// Gets or sets the value that enables or disables the logging of all requests/responses.
        /// </summary>
        [Category("Server")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that enables or disables the logging of all requests/responses.")]
        public bool EnableDataLogger
        {
            get
            {
                return enableDataLogger;
            }
            set
            {
                enableDataLogger = value;
            }
        }

        private bool useRelativeUrls = true;
        /// <summary>
        /// Gets or sets a value which indicates that the viewer will use relative or absolute URLs.
        /// </summary>
        [Category("Server")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the viewer will use relative or absolute URLs.")]
        public bool UseRelativeUrls
        {
            get
            {
                return useRelativeUrls;
            }
            set
            {
                useRelativeUrls = value;
            }
        }

        private bool passQueryParametersForResources = true;
        /// <summary>
        /// Gets or sets a value which enables or disables the transfer of URL parameters when requesting the scripts and styles of the viewer.
        /// </summary>
        [Category("Server")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which enables or disables the transfer of URL parameters when requesting the scripts and styles of the viewer.")]
        public bool PassQueryParametersForResources
        {
            get
            {
                return passQueryParametersForResources;
            }
            set
            {
                passQueryParametersForResources = value;
            }
        }

        #endregion

        #region Toolbar

        private bool showToolbar = true;
        /// <summary>
        /// Gets or sets a value which indicates that toolbar will be shown in the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that toolbar will be shown in the viewer.")]
        public bool ShowToolbar
        {
            get
            {
                return showToolbar;
            }
            set
            {
                showToolbar = value;
            }
        }

        private bool showNavigatePanel = true;
        /// <summary>
        /// Gets or sets a value which indicates that navigate panel will be shown in the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that navigate panel will be shown in the viewer.")]
        public bool ShowNavigatePanel
        {
            get
            {
                return showNavigatePanel;
            }
            set
            {
                showNavigatePanel = value;
            }
        }

        private bool showViewModePanel = true;
        /// <summary>
        /// Gets or sets a value which indicates that view mode panel will be shown in the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that view mode panel will be shown in the viewer.")]
        public bool ShowViewModePanel
        {
            get
            {
                return showViewModePanel;
            }
            set
            {
                showViewModePanel = value;
            }
        }

        private bool showPrintButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Print button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Print button in the toolbar of the viewer.")]
        public bool ShowPrintButton
        {
            get
            {
                return showPrintButton;
            }
            set
            {
                showPrintButton = value;
            }
        }

        private bool showOpenButton = false;
        /// <summary>
        /// Gets or sets a visibility of the Open button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(false)]
        [Description("Gets or sets a visibility of the Open button in the toolbar of the viewer.")]
        public bool ShowOpenButton
        {
            get
            {
                return showOpenButton;
            }
            set
            {
                showOpenButton = value;
            }
        }

        private bool showSaveButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Save button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Save button in the toolbar of the viewer.")]
        public bool ShowSaveButton
        {
            get
            {
                return showSaveButton;
            }
            set
            {
                showSaveButton = value;
            }
        }

        private bool showSendEmailButton = false;
        /// <summary>
        /// Gets or sets a visibility of the Send Email button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(false)]
        [Description("Gets or sets a visibility of the Send Email button in the toolbar of the viewer.")]
        public bool ShowSendEmailButton
        {
            get
            {
                return showSendEmailButton;
            }
            set
            {
                showSendEmailButton = value;
            }
        }

        private bool showBookmarksButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Bookmarks button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Bookmarks button in the toolbar of the viewer.")]
        public bool ShowBookmarksButton
        {
            get
            {
                return showBookmarksButton;
            }
            set
            {
                showBookmarksButton = value;
            }
        }

        private bool showParametersButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Parameters button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Parameters button in the toolbar of the viewer.")]
        public bool ShowParametersButton
        {
            get
            {
                return showParametersButton;
            }
            set
            {
                showParametersButton = value;
            }
        }

        private bool showThumbnailsButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Thumbnails button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Thumbnails button in the toolbar of the viewer.")]
        public bool ShowThumbnailsButton
        {
            get
            {
                return showThumbnailsButton;
            }
            set
            {
                showThumbnailsButton = value;
            }
        }

        private bool showFindButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Find button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Find button in the toolbar of the viewer.")]
        public bool ShowFindButton
        {
            get
            {
                return showFindButton;
            }
            set
            {
                showFindButton = value;
            }
        }

        private bool showFullScreenButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Full Screen button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Full Screen button in the toolbar of the viewer.")]
        public bool ShowFullScreenButton
        {
            get
            {
                return showFullScreenButton;
            }
            set
            {
                showFullScreenButton = value;
            }
        }

        private bool showExitButton = false;
        /// <summary>
        /// Gets or sets a visibility of the Exit button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Exit button in the toolbar of the viewer.")]
        public bool ShowExitButton
        {
            get
            {
                return showExitButton;
            }
            set
            {
                showExitButton = value;
            }
        }

        private bool showAboutButton = true;
        /// <summary>
        /// Gets or sets a visibility of the About button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the About button in the toolbar of the viewer.")]
        public bool ShowAboutButton
        {
            get
            {
                return showAboutButton;
            }
            set
            {
                showAboutButton = value;
            }
        }

        private bool showDesignButton = false;
        /// <summary>
        /// Gets or sets a visibility of the Design button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Design button in the toolbar of the viewer.")]
        public bool ShowDesignButton
        {
            get
            {
                return showDesignButton;
            }
            set
            {
                showDesignButton = value;
            }
        }

        private bool showFirstPageButton = true;
        /// <summary>
        /// Gets or sets a visibility of the First Page button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the First Page button in the toolbar of the viewer.")]
        public bool ShowFirstPageButton
        {
            get
            {
                return showFirstPageButton;
            }
            set
            {
                showFirstPageButton = value;
            }
        }

        private bool showPreviousPageButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Prev Page button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Prev Page button in the toolbar of the viewer.")]
        public bool ShowPreviousPageButton
        {
            get
            {
                return showPreviousPageButton;
            }
            set
            {
                showPreviousPageButton = value;
            }
        }

        private bool showGoToPageButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Go To Page button in the navigate panel of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Go To Page button in the navigate panel of the viewer.")]
        public bool ShowGoToPageButton
        {
            get
            {
                return showGoToPageButton;
            }
            set
            {
                showGoToPageButton = value;
            }
        }

        private bool showNextPageButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Next Page button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Next Page button in the toolbar of the viewer.")]
        public bool ShowNextPageButton
        {
            get
            {
                return showNextPageButton;
            }
            set
            {
                showNextPageButton = value;
            }
        }

        private bool showLastPageButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Last Page button in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Last Page button in the toolbar of the viewer.")]
        public bool ShowLastPageButton
        {
            get
            {
                return showLastPageButton;
            }
            set
            {
                showLastPageButton = value;
            }
        }

        private bool showSinglePageViewModeButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Single Page button in the view mode panel of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Single Page button in the view mode panel of the viewer.")]
        public bool ShowSinglePageViewModeButton
        {
            get
            {
                return showSinglePageViewModeButton;
            }
            set
            {
                showSinglePageViewModeButton = value;
            }
        }

        private bool showContinuousPageViewModeButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Continuous Page button in the view mode panel of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Continuous Page button in the view mode panel of the viewer.")]
        public bool ShowContinuousPageViewModeButton
        {
            get
            {
                return showContinuousPageViewModeButton;
            }
            set
            {
                showContinuousPageViewModeButton = value;
            }
        }

        private bool showMultiplePageViewModeButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Multiple Page button in the view mode panel of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Multiple Page button in the view mode panel of the viewer.")]
        public bool ShowMultiplePageViewModeButton
        {
            get
            {
                return showMultiplePageViewModeButton;
            }
            set
            {
                showMultiplePageViewModeButton = value;
            }
        }

        private bool showZoomButtons = true;
        /// <summary>
        /// Gets or sets a visibility of Zoom buttons in the toolbar of the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of Zoom buttons in the toolbar of the viewer.")]
        public bool ShowZoomButtons
        {
            get
            {
                return showZoomButtons;
            }
            set
            {
                showZoomButtons = value;
            }
        }

        private int zoom = StiZoomModeFx.Default;
        /// <summary>
        /// Gets or sets the report showing zoom.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(100)]
        [Description("Gets or sets the report showing zoom.")]
        public int Zoom
        {
            get
            {
                return zoom;
            }
            set
            {
                if (value > 500) zoom = 500;
                if (value < 0 || (value < 10 && value > 4)) zoom = 10;
                else zoom = value;
            }
        }

        private string exitUrl = string.Empty;
        /// <summary>
        /// The URL address, by which transfer will be done when clicking the Exit button of the viewer toolbar.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue("")]
        [Description("The URL address, by which transfer will be done when clicking the Exit button of the viewer toolbar.")]
        public string ExitUrl
        {
            get
            {
                return exitUrl;
            }
            set
            {
                exitUrl = value;
            }
        }

        #endregion

        
        #region Obsolete

        private bool globalReportCache = false;
        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the OnGetReport and OnSetReport events instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool GlobalReportCache
        {
            get
            {
                return globalReportCache;
            }
            set
            {
                globalReportCache = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowExportToPowerPoint property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowExportToPpt
        {
            get
            {
                return this.ShowExportToPowerPoint;
            }
            set
            {
                this.ShowExportToPowerPoint = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowExportToImageBmp property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowExportToBmp
        {
            get
            {
                return this.ShowExportToImageBmp;
            }
            set
            {
                this.ShowExportToImageBmp = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowExportToImageGif property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowExportToGif
        {
            get
            {
                return this.ShowExportToImageGif;
            }
            set
            {
                this.ShowExportToImageGif = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowExportToImageJpeg property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowExportToJpeg
        {
            get
            {
                return this.ShowExportToImageJpeg;
            }
            set
            {
                this.ShowExportToImageJpeg = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowExportToImagePcx property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowExportToPcx
        {
            get
            {
                return this.ShowExportToImagePcx;
            }
            set
            {
                this.ShowExportToImagePcx = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowExportToImagePng property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowExportToPng
        {
            get
            {
                return this.ShowExportToImagePng;
            }
            set
            {
                this.ShowExportToImagePng = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowExportToImageTiff property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowExportToTiff
        {
            get
            {
                return this.ShowExportToImageTiff;
            }
            set
            {
                this.ShowExportToImageTiff = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowExportToImageMetafile property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowExportToMetafile
        {
            get
            {
                return this.ShowExportToImageMetafile;
            }
            set
            {
                this.ShowExportToImageMetafile = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowExportToImageSvg property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowExportToSvg
        {
            get
            {
                return this.ShowExportToImageSvg;
            }
            set
            {
                this.ShowExportToImageSvg = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowExportToImageSvgz property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowExportToSvgz
        {
            get
            {
                return this.ShowExportToImageSvgz;
            }
            set
            {
                this.ShowExportToImageSvgz = value;
            }
        }

        [Obsolete("This property is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool DataEncryption
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        [Obsolete("This property is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool DataCompression
        {
            get
            {
                return true;
            }
            set
            {
            }
        }

        [Obsolete("This property is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public string LocalizationDirectory
        {
            get
            {
                return "Localization";
            }
            set
            {
            }
        }

        [Obsolete("This property is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public string AppCacheDirectory
        {
            get
            {
                return string.Empty;
            }
            set
            {
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the RequestTimeout property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public TimeSpan ServerTimeOut
        {
            get
            {
                return new TimeSpan(0, 0, this.RequestTimeout);
            }
            set
            {
                this.RequestTimeout = value.Seconds;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the BackgroundColor property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public Color Background
        {
            get
            {
                return this.BackgroundColor;
            }
            set
            {
                this.BackgroundColor = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the FlashWMode property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public StiWMode WMode
        {
            get
            {
                return this.FlashWMode;
            }
            set
            {
                this.FlashWMode = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the Theme property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public StiTheme ThemeName
        {
            get
            {
                switch (this.Theme)
                {
                    case StiViewerFxTheme.Office2007Black:
                        return StiTheme.Office2007Black;

                    case StiViewerFxTheme.Office2007Blue:
                        return StiTheme.Office2007Blue;

                    case StiViewerFxTheme.Office2007Silver:
                        return StiTheme.Office2007Silver;
                }

                return StiTheme.Office2013;
            }
            set
            {
                switch (value)
                {
                    case StiTheme.Office2007Black:
                        this.Theme = StiViewerFxTheme.Office2007Black;
                        break;

                    case StiTheme.Office2007Blue:
                        this.Theme = StiViewerFxTheme.Office2007Blue;
                        break;

                    case StiTheme.Office2007Silver:
                        this.Theme = StiViewerFxTheme.Office2007Silver;
                        break;

                    default:
                        this.Theme = StiViewerFxTheme.Office2013;
                        break;
                }
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the OpenLinksWindow property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public string OpenLinksTarget
        {
            get
            {
                return this.OpenLinksWindow;
            }
            set
            {
                this.OpenLinksWindow = value;
            }
        }

        #endregion
    }
}
