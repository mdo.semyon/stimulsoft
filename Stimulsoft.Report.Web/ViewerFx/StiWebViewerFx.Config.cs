#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Web
{
    public partial class StiWebViewerFx :
        WebControl,
        INamingContainer
    {
        private string GetLinksTarget(string value)
        {
            if (value == "Blank") return StiTargetWindow.Blank;
            if (value == "Self") return StiTargetWindow.Self;
            if (value == "Top") return StiTargetWindow.Top;
            return value;
        }

        private string RenderConfig()
        {
            // Create the Xml document
            XmlDocument doc = new XmlDocument();
            XmlDeclaration declaration = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(declaration);

            XmlElement category = null;
            XmlElement subcategory = null;
            XmlElement element = null;

            // Create the main node
            XmlElement config = doc.CreateElement("StiSerializer");
            config.SetAttribute("type", "Net");
            config.SetAttribute("version", "1.02");
            config.SetAttribute("application", "StiOptions");
            doc.AppendChild(config);

            #region Connection

            category = doc.CreateElement("Connection");
            config.AppendChild(category);

            element = doc.CreateElement("ClientRepeatCount");
            element.InnerText = this.RepeatCount.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ClientRequestTimeout");
            element.InnerText = this.RequestTimeout.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("EnableDataLogger");
            element.InnerText = this.EnableDataLogger.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowCancelButton");
            element.InnerText = this.ShowCancelButton.ToString();
            category.AppendChild(element);

            #endregion

            #region Appearance

            category = doc.CreateElement("Appearance");
            config.AppendChild(category);

            element = doc.CreateElement("ShowTooltips");
            element.InnerText = this.ShowTooltips.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowTooltipsHelp");
            element.InnerText = this.ShowTooltipsHelp.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowFormsHelp");
            element.InnerText = this.ShowFormsHelp.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowFormsHints");
            element.InnerText = this.ShowFormsHints.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("DateFormat");
            element.InnerText = this.ParametersPanelDateFormat;
            category.AppendChild(element);

            #endregion

            #region AboutDialog

            category = doc.CreateElement("AboutDialog");
            config.AppendChild(category);

            element = doc.CreateElement("TextLine1");
            element.InnerText = this.AboutDialogTextLine1;
            category.AppendChild(element);

            element = doc.CreateElement("TextLine2");
            element.InnerText = this.AboutDialogTextLine2;
            category.AppendChild(element);

            element = doc.CreateElement("Url");
            element.InnerText = this.AboutDialogUrl;
            category.AppendChild(element);

            element = doc.CreateElement("UrlText");
            element.InnerText = this.AboutDialogUrlText;
            category.AppendChild(element);

            element = doc.CreateElement("FrameworkType");
            element.InnerText = "ASP.NET";
            category.AppendChild(element);

            #endregion

            #region Localization

            subcategory = doc.CreateElement("Localizations");
            config.AppendChild(subcategory);

            element = doc.CreateElement("Localization");
            element.InnerText = string.IsNullOrEmpty(this.Localization) ? "Default" : this.Localization;
            subcategory.AppendChild(element);

            #endregion

            #region Print

            category = doc.CreateElement("Print");
            config.AppendChild(category);

            element = doc.CreateElement("ShowPrintDialog");
            element.InnerText = this.ShowPrintDialog.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AutoPageOrientation");
            element.InnerText = this.AutoPageOrientation.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AutoPageScale");
            element.InnerText = this.AutoPageScale.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowDefaultPrint");
            element.InnerText = this.AllowDefaultPrint.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowPrintToPdf");
            element.InnerText = this.AllowPrintToPdf.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowPrintToHtml");
            element.InnerText = this.AllowPrintToHtml.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("PrintAsBitmap");
            element.InnerText = this.PrintAsBitmap.ToString();
            category.AppendChild(element);

            #endregion

            #region Theme

            element = doc.CreateElement("Theme");
            element.InnerText = this.Theme.ToString().Replace("Office2007", string.Empty);
            config.AppendChild(element);

            #endregion

            #region Viewer

            category = doc.CreateElement("Viewer");
            config.AppendChild(category);

            element = doc.CreateElement("ExitOnServer");
            element.InnerText = (Exit != null).ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ExitUrl");
            element.InnerText = ExitUrl;
            category.AppendChild(element);

            element = doc.CreateElement("DefaultEmail");
            element.InnerText = this.DefaultEmailAddress.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("Title");
            element.InnerText = BrowserTitle;
            category.AppendChild(element);

            #region Appearance

            subcategory = doc.CreateElement("Appearance");
            category.AppendChild(subcategory);

            element = doc.CreateElement("AutoHideScrollbars");
            element.InnerText = this.AutoHideScrollbars.ToString();
            subcategory.AppendChild(element);

            Color color = this.CurrentPageBorderColor;
            element = doc.CreateElement("CurrentPageBorderColor");
            element.InnerText = string.Format("{0}, {1}, {2}, {3}", color.A, color.R, color.G, color.B);
            subcategory.AppendChild(element);

            element = doc.CreateElement("OpenLinksTarget");
            element.InnerText = GetLinksTarget(this.OpenLinksWindow);
            subcategory.AppendChild(element);

            element = doc.CreateElement("DesignTarget");
            element.InnerText = GetLinksTarget(this.DesignWindow);
            subcategory.AppendChild(element);

            element = doc.CreateElement("VariablesPanelColumns");
            element.InnerText = this.ParametersPanelColumnsCount.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("VariablesPanelEditorWidth");
            element.InnerText = this.ParametersPanelEditorWidth.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowEmailDialog");
            element.InnerText = this.ShowEmailDialog.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowEmailExportDialog");
            element.InnerText = this.ShowEmailExportDialog.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Toolbar

            subcategory = doc.CreateElement("Toolbar");
            category.AppendChild(subcategory);

            element = doc.CreateElement("ShowMainToolbar");
            element.InnerText = this.ShowToolbar.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNavigateToolbar");
            element.InnerText = this.ShowNavigatePanel.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowViewModeToolbar");
            element.InnerText = this.ShowViewModePanel.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Buttons

            element = doc.CreateElement("ShowAboutButton");
            element.InnerText = this.ShowAboutButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowBookmarksButton");
            element.InnerText = this.ShowBookmarksButton.ToString();
            subcategory.AppendChild(element);
            
            element = doc.CreateElement("ShowFindButton");
            element.InnerText = this.ShowFindButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowFirstPageButton");
            element.InnerText = this.ShowFirstPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowFullScreenButton");
            element.InnerText = this.ShowFullScreenButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowGoToPageButton");
            element.InnerText = this.ShowGoToPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowLastPageButton");
            element.InnerText = this.ShowLastPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNextPageButton");
            element.InnerText = this.ShowNextPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOpenButton");
            element.InnerText = this.ShowOpenButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeContinuousButton");
            element.InnerText = this.ShowContinuousPageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeMultipleButton");
            element.InnerText = this.ShowMultiplePageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeSingleButton");
            element.InnerText = this.ShowSinglePageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowParametersButton");
            element.InnerText = this.ShowParametersButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowParametersPanel");
            element.InnerText = this.ShowParametersButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviousPageButton");
            element.InnerText = this.ShowPreviousPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPrintButton");
            element.InnerText = this.ShowPrintButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSaveButton");
            element.InnerText = this.ShowSaveButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSendEMailButton");
            element.InnerText = this.ShowSendEmailButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowThumbnailsButton");
            element.InnerText = this.ShowThumbnailsButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowDesignButton");
            element.InnerText = this.ShowDesignButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExitButton");
            element.InnerText = this.ShowExitButton.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Zoom

            element = doc.CreateElement("ShowZoom");
            element.InnerText = this.ShowZoomButtons.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("Zoom");
            element.InnerText = this.Zoom.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Exports

            subcategory = doc.CreateElement("Exports");
            category.AppendChild(subcategory);

            element = doc.CreateElement("OpenExportedReportTarget");
            element.InnerText = GetLinksTarget(this.OpenExportedReportWindow);
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportDialog");
            element.InnerText = this.ShowExportDialog.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDocument");
            element.InnerText = this.ShowExportToDocument.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPdf");
            element.InnerText = this.ShowExportToPdf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToXps");
            element.InnerText = this.ShowExportToXps.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPpt");
            element.InnerText = this.ShowExportToPowerPoint.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToHtml");
            element.InnerText = this.ShowExportToHtml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToHtml5");
            element.InnerText = this.ShowExportToHtml5.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToMht");
            element.InnerText = this.ShowExportToMht.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToText");
            element.InnerText = this.ShowExportToText.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToRtf");
            element.InnerText = this.ShowExportToRtf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToWord2007");
            element.InnerText = this.ShowExportToWord2007.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToOpenDocumentWriter");
            element.InnerText = this.ShowExportToOpenDocumentWriter.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcel");
            element.InnerText = this.ShowExportToExcel.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcelXml");
            element.InnerText = this.ShowExportToExcelXml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcel2007");
            element.InnerText = this.ShowExportToExcel2007.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToOpenDocumentCalc");
            element.InnerText = this.ShowExportToOpenDocumentCalc.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToCsv");
            element.InnerText = this.ShowExportToCsv.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDbf");
            element.InnerText = this.ShowExportToDbf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToXml");
            element.InnerText = this.ShowExportToXml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDif");
            element.InnerText = this.ShowExportToDif.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSylk");
            element.InnerText = this.ShowExportToSylk.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToBmp");
            element.InnerText = this.ShowExportToImageBmp.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToGif");
            element.InnerText = this.ShowExportToImageGif.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToJpeg");
            element.InnerText = this.ShowExportToImageJpeg.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPcx");
            element.InnerText = this.ShowExportToImagePcx.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPng");
            element.InnerText = this.ShowExportToImagePng.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToTiff");
            element.InnerText = this.ShowExportToImageTiff.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToMetafile");
            element.InnerText = this.ShowExportToImageMetafile.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSvg");
            element.InnerText = this.ShowExportToImageSvg.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSvgz");
            element.InnerText = this.ShowExportToImageSvgz.ToString();
            subcategory.AppendChild(element);

            #endregion

            #endregion

            return StiGZipHelper.Pack(doc.OuterXml);
        }
    }
}
