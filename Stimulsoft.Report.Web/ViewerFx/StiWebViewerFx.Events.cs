#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Export;
using System.Data;

namespace Stimulsoft.Report.Web
{
    public partial class StiWebViewerFx :
        WebControl,
        INamingContainer
    {
        #region GetReport

        /// <summary>
        /// The event occurs when the report is requested after running the viewer.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs when the report is requested after running the viewer.")]
        public event StiReportDataEventHandler GetReport;
        
        private void InvokeGetReport()
        {
            StiRequestParams requestParams = this.RequestParams;
            StiReport report = this.Report;

            if (requestParams.Action == StiAction.GetReport || report == null)
            {
                StiReportDataEventArgs e = new StiReportDataEventArgs(requestParams, report);
                OnGetReport(e);
                this.report = e.Report;
                InvokeGetReportData();
                if (e.Report != null)
                {
                    StiAction action = requestParams.Action;
                    requestParams.Action = StiAction.GetReport;
                    requestParams.Cache.Helper.SaveReportInternal(requestParams, e.Report);
                    requestParams.Action = action;
                }
            }
            else if (report != null && report.IsRendered == false)
            {
                InvokeGetReportData();
            }
        }

        protected virtual void OnGetReport(StiReportDataEventArgs e)
        {
            if (GetReport != null) GetReport(this, e);
        }

        #endregion

        #region GetReportData

        /// <summary>
        /// The event occurs each time before rendering the report.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs each time before rendering the report.")]
        public event StiReportDataEventHandler GetReportData;
        
        private void InvokeGetReportData()
        {
            if (this.Report != null && this.Report.IsDocument == false)
            {
                var e = new StiReportDataEventArgs(this.RequestParams, this.Report);
                OnGetReportData(e);
            }
        }
        
        protected virtual void OnGetReportData(StiReportDataEventArgs e)
        {
            if (GetReportData != null) GetReportData(this, e);
            else if (GetDataSet != null)
            {
                var e2 = new StiGetDataSetEventArgs(e.Report);
                GetDataSet(this, e2);
                if (e2.DataSet != null) Report.RegData(e2.DataSet);
            }
        }

        #endregion

        #region PrintReport

        /// <summary>
        /// The event occurs before the printing of the report from the viewer menu.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs before the printing of the report from the viewer menu.")]
        public event StiPrintReportEventHandler PrintReport;

        private void InvokePrintReport(StiExportSettings settings)
        {
            InvokeGetReport();
            var e = new StiPrintReportEventArgs(this.RequestParams, this.Report, settings);
            OnPrintReport(e);
        }

        protected virtual void OnPrintReport(StiPrintReportEventArgs e)
        {
            if (PrintReport != null) PrintReport(this, e);
        }

        #endregion

        #region ExportReport

        /// <summary>
        /// The event occurs before the exporting of the report from the viewer menu.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs before the exporting of the report from the viewer menu.")]
        public event StiExportReportEventHandler ExportReport;

        private void InvokeExportReport(StiExportSettings settings)
        {
            InvokeGetReport();
            var e = new StiExportReportEventArgs(this.RequestParams, this.Report, settings);
            OnExportReport(e);
        }

        protected virtual void OnExportReport(StiExportReportEventArgs e)
        {
            if (ExportReport != null) ExportReport(this, e);
            else if (ReportExport != null) ReportExport(this, e);
        }

        /// <summary>
        /// The event occurs after the report is exported.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs after the report is exported.")]
        public event StiExportReportResponseEventHandler ExportReportResponse;

        private void InvokeExportReportResponse(StiExportSettings settings, Stream stream, string fileName, string contentType)
        {
            var e = new StiExportReportResponseEventArgs(this.RequestParams, this.Report, settings, stream, fileName, contentType);
            OnExportReportResponse(e);
        }

        protected virtual void OnExportReportResponse(StiExportReportResponseEventArgs e)
        {
            if (ExportReportResponse != null) ExportReportResponse(this, e);
        }

        #endregion

        #region EmailReport

        /// <summary>
        /// The event occurs when sending a report via Email. In this event it is necessary to set settings for sending Email.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs when sending a report via Email. In this event it is necessary to set settings for sending Email.")]
        public event StiEmailReportEventHandler EmailReport;

        private void InvokeEmailReport(StiExportSettings settings, StiEmailOptions options)
        {
            InvokeGetReport();
            var e = new StiEmailReportEventArgs(this.RequestParams, this.Report, settings, options);
            OnEmailReport(e);
        }

        protected virtual void OnEmailReport(StiEmailReportEventArgs e)
        {
            if (EmailReport != null) EmailReport(this, e);
        }

        #endregion

        #region Interaction

        /// <summary>
        /// The event occurs before the interactive action in the viewer, such as sorting, drill-down, request from user variables.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs before the interactive action in the viewer, such as sorting, drill-down, request from user variables.")]
        public event StiReportDataEventHandler Interaction;

        private void InvokeInteraction()
        {
            InvokeGetReport();
            var e = new StiReportDataEventArgs(this.RequestParams, this.Report);
            OnInteraction(e);
        }

        protected virtual void OnInteraction(StiReportDataEventArgs e)
        {
            if (Interaction != null) Interaction(this, e);
        }

        #endregion

        #region DesignReport

        /// <summary>
        /// The event occurs when clicking on the Design button in the viewer toolbar.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs when clicking on the Design button in the viewer toolbar.")]
        public event StiReportDataEventHandler DesignReport;

        private void InvokeDesignReport()
        {
            InvokeGetReport();
            var e = new StiReportDataEventArgs(this.RequestParams, this.Report);
            OnDesignReport(e);
        }

        protected virtual void OnDesignReport(StiReportDataEventArgs e)
        {
            if (DesignReport != null) DesignReport(this, e);
            else if (ReportDesign != null)
            {
                var e2 = new StiReportDesignEventArgs(e.Report);
                ReportDesign(this, e2);
            }
        }

        #endregion

        #region Exit

        /// <summary>
        /// The event occurs when clicking on the Exit button in the viewer toolbar.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs when clicking on the Exit button in the viewer toolbar.")]
        public event StiReportDataEventHandler Exit;

        private void InvokeExit()
        {
            InvokeGetReport();
            var e = new StiReportDataEventArgs(this.RequestParams, this.Report);
            OnExit(e);
        }

        protected virtual void OnExit(StiReportDataEventArgs e)
        {
            if (Exit != null) Exit(this, e);
        }

        #endregion


        #region Obsolete

        #region StiGetReportEventArgs

        [Obsolete("This delegate is obsolete. It will be removed in next versions. Please use the StiReportDataEventHandler delegate instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiGetReportEventHandler(object sender, StiGetReportEventArgs e);

        [Obsolete("This class is obsolete. It will be removed in next versions. Please use the StiReportDataEventArgs class instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiGetReportEventArgs : EventArgs
        {
            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
                set
                {
                    report = value;
                }
            }

            public StiGetReportEventArgs(StiReport report)
            {
                this.report = report;
            }
        }

        #endregion

        #region ReportDesign

        [Obsolete("This delegate is obsolete. It will be removed in next versions. Please use the StiReportDataEventHandler delegate instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiReportDesignEventHandler(object sender, StiReportDesignEventArgs e);

        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiReportDesignEventArgs : EventArgs
        {
            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
            }

            public StiReportDesignEventArgs(StiReport report)
            {
                this.report = report;
            }
        }

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the DesignReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiReportDesignEventHandler ReportDesign;

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the DesignReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        protected virtual void OnReportDesign(StiReportDesignEventArgs e)
        {
        }

        #endregion

        #region ReportExport

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the ExportReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiExportReportEventHandler ReportExport;

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the ExportReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        protected virtual void OnReportExport(StiExportReportEventArgs e)
        {
        }

        #endregion

        #region GetDataSet

        [Obsolete("This delegate is obsolete. It will be removed in next versions. Please use the StiReportDataEventHandler delegate instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiGetDataSetEventHandler(object sender, StiGetDataSetEventArgs e);

        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiGetDataSetEventArgs : EventArgs
        {
            private DataSet dataSet = null;
            public DataSet DataSet
            {
                get
                {
                    return dataSet;
                }
                set
                {
                    dataSet = value;
                }
            }

            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
            }

            public StiGetDataSetEventArgs(StiReport report)
            {
                this.report = report;
            }
        }

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the GetReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiGetDataSetEventHandler GetDataSet = null;

        #endregion

        #region PreInit

        [Obsolete("This delegate is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiPreInitEventHandler(object sender, StiPreInitEventArgs e);

        [Obsolete("This class is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiPreInitEventArgs : EventArgs
        {
            private StiWebViewerFx viewer = null;
            public StiWebViewerFx Viewer
            {
                get
                {
                    return viewer;
                }
            }

            public StiPreInitEventArgs(StiWebViewerFx viewer)
            {
                this.viewer = viewer;
            }
        }

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the ASP.NET Page_Load or ASP.NET Page_PreInit event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiPreInitEventHandler PreInit;

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the ASP.NET Page_Load or ASP.NET Page_PreInit event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        protected virtual void OnPreInit(StiPreInitEventArgs e)
        {
            if (PreInit != null) PreInit(this, e);
        }

        #endregion
        
        #endregion
    }
}
