#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Security.Cryptography;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Base;
using Stimulsoft.Base.Licenses;
using Stimulsoft.Base.Cloud;

namespace Stimulsoft.Report.Web
{
    public partial class StiWebViewerFx :
        WebControl,
        INamingContainer
    {
        #region Render scripts

        private StiJavaScript RenderMainScript()
        {
            string resourceUrl = GetResourceUrl(this.UseRelativeUrls, this.PassQueryParametersForResources);
            var script = new StiJavaScript();
            script.ScriptUrl = string.Format(resourceUrl, "scripts");

            return script;
        }

        private StiJavaScript RenderUserScript()
        {
            if (!string.IsNullOrEmpty(this.UserScript))
            {
                var script = new StiJavaScript();
                if (!UserScript.Trim().StartsWith("<")) script.Text = this.UserScript;
                else script.Text = this.UserScript.Substring(this.UserScript.IndexOf(">") + 1, this.UserScript.LastIndexOf("</") - this.UserScript.IndexOf(">") - 1);
                return script;
            }

            return null;
        }

        private StiJavaScript RenderFlashScript()
        {
            var productVersion = StiVersionHelper.ProductVersion.Trim();

            #region LicenseKey
#if CLOUD
            var isTrial = StiCloudPlan.IsTrialPlan(this.Report != null ? this.Report.ReportGuid : null);
#else
            var key = StiLicenseKeyValidator.GetLicenseKey();
            var isTrial = !StiLicenseKeyValidator.IsValid(StiProductIdent.Web, key);
            if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key)) isTrial = true;

            #region IsValidLicenseKey
            if (!isTrial)
            {
                try
                {
                    using (var rsa = new RSACryptoServiceProvider(512))
                    using (var sha = new SHA1CryptoServiceProvider())
                    {
                        rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                        isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
                    }
                }
                catch (Exception)
                {
                    isTrial = true;
                }
            }
            #endregion
#endif
            if (!isTrial) productVersion += " ";
            #endregion

            string resourceUrl = GetResourceUrl(this.UseRelativeUrls, this.PassQueryParametersForResources);
            string config = RenderConfig();
            string requestUrl = Convert.ToBase64String(Encoding.UTF8.GetBytes(GetRequestUrl(this.UseRelativeUrls, true)));
            string version = Convert.ToBase64String(Encoding.UTF8.GetBytes(productVersion));

            var script = new StiJavaScript();
            script.Text = string.Format(
                "swfobject.embedSWF('{0}','flashContent','100%','100%','11.1.0','{1}'," +
                "{{config:'{2}',url:'{3}',version:'{4}',theme:'{5}',id:'{6}',cachetimeout:'{7}',cachemode:'{8}',cacheitempriority:'{9}',clientguid:'{10}'}}," +
                "{{quality:'high',bgcolor:'{11}',allowscriptaccess:'always',allowfullscreen:true,wmode:'{12}'}}," +
                "{{id:'{6}_swf',name:'{6}_swf',align:'middle'}});",
                string.Format(resourceUrl, "ViewerFx_Web.swf"),
                string.Format(resourceUrl, "Install.swf"),
                HttpUtility.UrlEncode(config),
                HttpUtility.UrlEncode(requestUrl),
                HttpUtility.UrlEncode(version),
                this.Theme.ToString().Replace("Office2007", string.Empty),
                this.ID,
                this.CacheTimeout,
                this.CacheMode,
                this.CacheItemPriority,
                this.ClientGuid,
                StiReportHelper.GetHtmlColor(this.BackgroundColor),
                this.BackgroundColor == Color.Transparent ? "transparent" : this.FlashWMode.ToString().ToLower());
            return script;
        }

        private Panel RenderFlashWrapper()
        {
            var text = new LiteralControl();
            text.Text = "<div id='flashContent'><p>To view this page ensure that Adobe Flash Player 11.1 or greater is installed.</p>" +
                "<a href=\"http://www.adobe.com/go/getflashplayer\"><img src='data:image/gif;base64," +
                "R0lGODlhngAnALMAAAcHB////35+fkNDQ84gJ9vV1aCgoMw+Q8MnLYiIiLy7u2ZmZtx6feWcniIiIgAAACH5BAkAAA8ALAAAAACeACcAAAT+kMhJq7046827" +
                "/2AojldgnmiqrmzrvnAsxyQ533iu73Qt8sCg0CUoGo0t32/IbPKO0KQS5KxaY9CjdDo5HDLXsBiVRbK4h0bB1AC3EnDFzSA3FeAJwxplgO8DfXkneAl/YWVF" +
                "WzUMKW0YLAYDCQoJCyyFKgMDJwoOcAsAAieaCQKhJgMLCZomAHiGV4iiZzUHsAGOJSqRLIYDsAYCDnsKmycOBgEDsyYOcgN1AK1jKbKKIre4bikOLJqeygAD" +
                "yaMFAgkmxXwLBdIolcpyq9PUJ9a0I3UquRa7lgGUMP2aVsDYiQLdEKYzCBAaw4bhACBrpelhLETXPjBq5EWDCjj+6RI4M+AJjjQD/wZB67RG3YlILl9ughag" +
                "oBwACnLWu7fCRgoGHT4yCyCtUk4Fa0CicFBxGcRRyQAYUhXPBEh3VmRp1RJgxMYTQIOmaPen6EOaBw22e1rQ2Ko686oivCmm1FaMJkaM/bDCgDhSqCqaEEYu" +
                "wDkU4xQAWCyJj4PFKQcsdtVqMjond+5m+SPiwE8vXza0uJWtHjVzmo0YEtGgFwLRpmPvUJBaQOG8IDy3eO1Rtm8cwe7exv2h9W7Yv5PHCC5rOHEPpU3w3qa8" +
                "eout+Drodo3cunehWS73/AALNGgOu/DIW4HpIJxkBW7rQRGw/fwUdAbxia8e4CsdmR3+0d542v20BGKqTEKUCp2I59c5m8RUlUql4DQhYgaNY8dMCcojiSnO" +
                "xYCaai6Ql0JoVKSAFj0oqNINKrdJuGIASvEyIyDCEPOihjPWaJEMtBWhT3YaGHcCP3ypOCRWxyizhwApPYXKkEqpc+Mvh8HoUo+XocRDHyGmsMEBDNyCYooY" +
                "arIGk4BY4uVglAH0lyYWDoJOQcnMqJBCdjjgTGBq0vjhQDxEh4IGpZ2J5iiTRKPiJH6h0FZDRxVDpWVTvrPSMCcsEFmjVkmiYT0ZbNdIDZksKemcEyGWE0Nc" +
                "KrlUU8wodSGNl3FKTakrIBlCqigwWYpMgKxBloxUipfphgdhYWVrrID8WAWvkoaFqqwnTOYKodMksNhEyL6jbETiZAmjVeJJxhiujO6KwXYFWOvDd/QGocF5" +
                "XBBQ77465OsBvwDP4K9YARec0cD9GKywCgh3t/DCDff28MMRV2zxxQhHAAA7' alt='Get Adobe Flash Player' border='0'/></a></div>";

            var mainPanel = new Panel();
            mainPanel.ID = this.ID + "_FxViewerMainPanel";
            mainPanel.Style.Add("display", "inline-block");
            mainPanel.Style.Add("width", this.Width.ToString());
            mainPanel.Style.Add("height", this.Height.ToString());
            mainPanel.Controls.Add(text);

            return mainPanel;
        }

        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            if (this.Width.IsEmpty) this.Width = Unit.Percentage(100);
            if (this.Height.IsEmpty) this.Height = Unit.Pixel(650);

            base.OnPreRender(e);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            #region Design Mode

            if (IsDesignMode)
            {
                StiRequestParams requestParams = this.CreateRequestParams();

                Panel panel = new Panel();
                panel.Width = this.Width == Unit.Empty ? Unit.Percentage(100) : this.Width;
                panel.Height = this.Height == Unit.Empty ? Unit.Percentage(100) : this.Height;
                panel.Style.Add("overflow", "hidden");

                Table mainTable = new Table();
                mainTable.CellPadding = 0;
                mainTable.CellSpacing = 0;
                mainTable.Width = Unit.Percentage(100);
                mainTable.Height = Unit.Percentage(100);
                mainTable.BorderColor = this.BorderColor.IsEmpty ? Color.DarkGray : this.BorderColor;
                mainTable.BorderWidth = this.BorderWidth.IsEmpty ? 2 : this.BorderWidth;
                mainTable.BorderStyle = this.BorderStyle == BorderStyle.NotSet ? BorderStyle.Solid : this.BorderStyle;
                panel.Controls.Add(mainTable);

                TableRow rowToolbar = new TableRow();
                rowToolbar.VerticalAlign = VerticalAlign.Top;
                mainTable.Rows.Add(rowToolbar);

                TableCell cellToolbarLeft = new TableCell();
                cellToolbarLeft.Style.Add("width", "513px");
                cellToolbarLeft.Style.Add("height", "30px");
                cellToolbarLeft.Style.Add("background", string.Format("url('{0}')", StiWebViewer.GetImageUrl(requestParams, "DesignToolbarLeftHalf.png")));
                rowToolbar.Cells.Add(cellToolbarLeft);

                TableCell cellToolbarMiddle = new TableCell();
                cellToolbarMiddle.Style.Add("height", "30px");
                cellToolbarMiddle.Style.Add("background", string.Format("url('{0}')", StiWebViewer.GetImageUrl(requestParams, "DesignToolbarMiddleHalf.png")));
                rowToolbar.Cells.Add(cellToolbarMiddle);

                TableCell cellToolbarRight = new TableCell();
                cellToolbarRight.Style.Add("width", "32px");
                cellToolbarRight.Style.Add("height", "30px");
                rowToolbar.Cells.Add(cellToolbarRight);

                Panel rightPanel = new Panel();
                rightPanel.Style.Add("width", "32px");
                rightPanel.Style.Add("height", "30px");
                rightPanel.Style.Add("background", string.Format("url('{0}')", StiWebViewer.GetImageUrl(requestParams, "DesignToolbarRightHalf.png")));
                cellToolbarRight.Controls.Add(rightPanel);

                TableRow rowCaption = new TableRow();
                mainTable.Rows.Add(rowCaption);

                TableCell cellCaption = new TableCell();
                cellCaption.ColumnSpan = 3;
                cellCaption.Height = Unit.Percentage(100);
                cellCaption.Font.Name = "Arial";
                cellCaption.Text = "<strong>Flash Web Viewer</strong><br />" + this.ID;
                cellCaption.HorizontalAlign = HorizontalAlign.Center;
                rowCaption.Cells.Add(cellCaption);

                panel.RenderControl(writer);
            }

            #endregion

            #region Runtime Mode

            else
            {
                StiJavaScript mainScript = RenderMainScript();
                this.Controls.Add(mainScript);

                StiJavaScript userScript = RenderUserScript();
                if (userScript != null) this.Controls.Add(userScript);

                StiJavaScript flashScript = RenderFlashScript();
                this.Controls.Add(flashScript);

                Panel flashWrapper = RenderFlashWrapper();
                this.Controls.Add(flashWrapper);
            }

            #endregion

            base.RenderContents(writer);
        }

        private string RenderControlToHtml(Control control)
        {
            if (control == null) return string.Empty;

            var sb = new StringBuilder();
            var tw = new StringWriter(sb);
            var w = new HtmlTextWriter(tw);
            control.RenderControl(w);
            return tw.ToString();
        }

        /// <summary>
        /// Show the viewer in full screen mode with the specified report.
        /// </summary>
        public void View(StiReport report)
        {
            if (report != null) this.Report = report;
            this.Width = Unit.Percentage(100);
            this.Height = Unit.Percentage(100);

            StiJavaScript mainScript = RenderMainScript();
            StiJavaScript userScript = RenderUserScript();
            StiJavaScript flashScript = RenderFlashScript();
            Panel flashWrapper = RenderFlashWrapper();

            string html = string.Format(
                "<!doctype html>" +
                "<head><title>{0}</title><style>html,body{{margin:0;padding:0;overflow:hidden;background-color:#e8e8e8;width:100%;height:100%}}</style></head>" +
                "<body>{1}{2}{3}{4}</body></html>",
                string.IsNullOrEmpty(this.BrowserTitle) ? "Report Viewer" : this.BrowserTitle,
                RenderControlToHtml(mainScript),
                RenderControlToHtml(userScript),
                RenderControlToHtml(flashScript),
                RenderControlToHtml(flashWrapper)
            );

            StiReportResponse.ResponseString(html, "text/html", false);
        }

        /// <summary>
        /// Show the viewer in full screen mode with the previously specified report. The report should be specified in the OnGetReport event.
        /// </summary>
        public void View()
        {
            View(null);
        }
    }
}
