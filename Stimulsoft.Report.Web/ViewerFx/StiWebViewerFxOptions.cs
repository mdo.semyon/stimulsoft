#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;


namespace Stimulsoft.Report.WebFx
{
    [Obsolete("This class with all static options is obsolete. It will be removed in next versions. Please use the StiWebViewerFx component properties instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class StiWebViewerFxOptions
    {
        #region Class: StiZoomMode
        [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiZoomModeFx enum instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public sealed class StiZoomMode
        {
            public const int Default = 0;
            public const int OnePage = 1;
            public const int TwoPages = 2;
            public const int PageWidth = 3;
            public const int PageHeight = 4;
        }
        #endregion

        #region Enum: StiImageQuality
        [Obsolete("This class is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiImageQuality component properties instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public enum StiImageQuality
        {
            Low,
            Normal,
            High
        }
        #endregion


        #region AboutDialog
        public sealed class AboutDialog
        {
            private static string textLine1 = string.Empty;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.AboutDialogTextLine1 property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string TextLine1
            {
                get
                {
                    return textLine1;
                }
                set
                {
                    textLine1 = value;
                }
            }

            private static string textLine2 = string.Empty;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.AboutDialogTextLine2 property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string TextLine2
            {
                get
                {
                    return textLine2;
                }
                set
                {
                    textLine2 = value;
                }
            }

            private static string url = string.Empty;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.AboutDialogUrl property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string Url
            {
                get
                {
                    return url;
                }
                set
                {
                    url = value;
                }
            }

            private static string urlText = string.Empty;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.AboutDialogUrlText property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string UrlText
            {
                get
                {
                    return urlText;
                }
                set
                {
                    urlText = value;
                }
            }
        }
        #endregion

        #region Connection
        public sealed class Connection
        {
            private static int clientRequestTimeout = 20;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.RequestTimeout property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static int ClientRequestTimeout
            {
                get
                {
                    return clientRequestTimeout;
                }
                set
                {
                    clientRequestTimeout = value;
                }
            }

            private static int clientRepeatCount = 1;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.RepeatCount property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static int ClientRepeatCount
            {
                get
                {
                    return clientRepeatCount;
                }
                set
                {
                    clientRepeatCount = value;
                }
            }

            private static bool relativeUrls = false;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.UseRelativeUrls property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool RelativeUrls
            {
                get
                {
                    return relativeUrls;
                }
                set
                {
                    relativeUrls = value;
                }
            }

            private static bool enableDataLogger = false;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.EnableDataLogger property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool EnableDataLogger
            {
                get
                {
                    return enableDataLogger;
                }
                set
                {
                    enableDataLogger = value;
                }
            }

            private static bool showCancelButton = false;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowCancelButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowCancelButton
            {
                get
                {
                    return showCancelButton;
                }
                set
                {
                    showCancelButton = value;
                }
            }
        }
        #endregion

        #region Print
        public sealed class Print
        {
            private static bool autoPageOrientation = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.AutoPageOrientation property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AutoPageOrientation
            {
                get
                {
                    return autoPageOrientation;
                }
                set
                {
                    autoPageOrientation = value;
                }
            }

            private static bool autoPageScale = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.AutoPageScale property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AutoPageScale
            {
                get
                {
                    return autoPageScale;
                }
                set
                {
                    autoPageScale = value;
                }
            }

            private static bool allowDefaultPrint = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.AllowDefaultPrint property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AllowDefaultPrint
            {
                get
                {
                    return allowDefaultPrint;
                }
                set
                {
                    allowDefaultPrint = value;
                }
            }

            private static bool allowPrintToPdf = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.AllowPrintToPdf property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AllowPrintToPdf
            {
                get
                {
                    return allowPrintToPdf;
                }
                set
                {
                    allowPrintToPdf = value;
                }
            }

            private static bool allowPrintToHtml = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.AllowPrintToHtml property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AllowPrintToHtml
            {
                get
                {
                    return allowPrintToHtml;
                }
                set
                {
                    allowPrintToHtml = value;
                }
            }

            private static bool showPrintDialog = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowPrintDialog property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPrintDialog
            {
                get
                {
                    return showPrintDialog;
                }
                set
                {
                    showPrintDialog = value;
                }
            }

            private static StiImageQuality imageQuality = StiImageQuality.Normal;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiReportHelperFx.ImageQuality property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static StiImageQuality ImageQuality
            {
                get
                {
                    return imageQuality;
                }
                set
                {
                    imageQuality = value;
                }
            }
        }

        #endregion

        #region Toolbar
        public sealed class Toolbar
        {
            private static bool showMainToolbar = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowToolbar property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowMainToolbar
            {
                get
                {
                    return showMainToolbar;
                }
                set
                {
                    showMainToolbar = value;
                }
            }

            private static bool showPrintButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowPrintButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPrintButton
            {
                get
                {
                    return showPrintButton;
                }
                set
                {
                    showPrintButton = value;
                }
            }

            private static bool showOpenButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowOpenButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowOpenButton
            {
                get
                {
                    return showOpenButton;
                }
                set
                {
                    showOpenButton = value;
                }
            }

            private static bool showSaveButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowSaveButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowSaveButton
            {
                get
                {
                    return showSaveButton;
                }
                set
                {
                    showSaveButton = value;
                }
            }

            private static bool showSendEMailButton = false;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowSendEmailButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowSendEMailButton
            {
                get
                {
                    return showSendEMailButton;
                }
                set
                {
                    showSendEMailButton = value;
                }
            }

            private static bool showPageNewButton = false;
            [Obsolete("This property is obsolete. It is no longer used and will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPageNewButton
            {
                get
                {
                    return showPageNewButton;
                }
                set
                {
                    showPageNewButton = value;
                }
            }

            private static bool showPageDeleteButton = false;
            [Obsolete("This property is obsolete. It is no longer used and will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPageDeleteButton
            {
                get
                {
                    return showPageDeleteButton;
                }
                set
                {
                    showPageDeleteButton = value;
                }
            }

            private static bool showPageSizeButton = false;
            [Obsolete("This property is obsolete. It is no longer used and will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPageSizeButton
            {
                get
                {
                    return showPageSizeButton;
                }
                set
                {
                    showPageSizeButton = value;
                }
            }

            private static bool showBookmarksButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowBookmarksButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowBookmarksButton
            {
                get
                {
                    return showBookmarksButton;
                }
                set
                {
                    showBookmarksButton = value;
                }
            }

            private static bool showParametersButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowParametersButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowParametersButton
            {
                get
                {
                    return showParametersButton;
                }
                set
                {
                    showParametersButton = value;
                }
            }

            private static bool showParametersPanel = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowParametersButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowParametersPanel
            {
                get
                {
                    return showParametersPanel;
                }
                set
                {
                    showParametersPanel = value;
                }
            }

            private static bool showThumbnailsButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowThumbnailsButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowThumbnailsButton
            {
                get
                {
                    return showThumbnailsButton;
                }
                set
                {
                    showThumbnailsButton = value;
                }
            }

            private static bool showFindButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowFindButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowFindButton
            {
                get
                {
                    return showFindButton;
                }
                set
                {
                    showFindButton = value;
                }
            }

            private static bool showFullScreenButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowFullScreenButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowFullScreenButton
            {
                get
                {
                    return showFullScreenButton;
                }
                set
                {
                    showFullScreenButton = value;
                }
            }

            private static bool showEditButton = false;
            [Obsolete("This property is obsolete. It is no longer used and will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowEditButton
            {
                get
                {
                    return showEditButton;
                }
                set
                {
                    showEditButton = value;
                }
            }

            private static bool showExitButton = false;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowExitButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowExitButton
            {
                get
                {
                    return showExitButton;
                }
                set
                {
                    showExitButton = value;
                }
            }

            private static bool showDesignButton = false;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowDesignButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowDesignButton
            {
                get
                {
                    return showDesignButton;
                }
                set
                {
                    showDesignButton = value;
                }
            }

            private static bool showAboutButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowAboutButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowAboutButton
            {
                get
                {
                    return showAboutButton;
                }
                set
                {
                    showAboutButton = value;
                }
            }

            private static bool showChangeThemeButton = false;
            [Obsolete("This property is obsolete. It is no longer used and will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowChangeThemeButton
            {
                get
                {
                    return showChangeThemeButton;
                }
                set
                {
                    showChangeThemeButton = value;
                }
            }

            private static bool showNavigateToolbar = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowNavigatePanel property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowNavigateToolbar
            {
                get
                {
                    return showNavigateToolbar;
                }
                set
                {
                    showNavigateToolbar = value;
                }
            }

            private static bool showFirstPageButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowFirstPageButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowFirstPageButton
            {
                get
                {
                    return showFirstPageButton;
                }
                set
                {
                    showFirstPageButton = value;
                }
            }

            private static bool showPreviousPageButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowPreviousPageButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPreviousPageButton
            {
                get
                {
                    return showPreviousPageButton;
                }
                set
                {
                    showPreviousPageButton = value;
                }
            }

            private static bool showGoToPageButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowGoToPageButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowGoToPageButton
            {
                get
                {
                    return showGoToPageButton;
                }
                set
                {
                    showGoToPageButton = value;
                }
            }

            private static bool showNextPageButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowNextPageButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowNextPageButton
            {
                get
                {
                    return showNextPageButton;
                }
                set
                {
                    showNextPageButton = value;
                }
            }

            private static bool showLastPageButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowLastPageButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowLastPageButton
            {
                get
                {
                    return showLastPageButton;
                }
                set
                {
                    showLastPageButton = value;
                }
            }

            private static bool showViewModeToolbar = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowViewModePanel property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowViewModeToolbar
            {
                get
                {
                    return showViewModeToolbar;
                }
                set
                {
                    showViewModeToolbar = value;
                }
            }

            private static bool showPageViewModeSingleButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowSinglePageViewModeButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPageViewModeSingleButton
            {
                get
                {
                    return showPageViewModeSingleButton;
                }
                set
                {
                    showPageViewModeSingleButton = value;
                }
            }

            private static bool showPageViewModeContinuousButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowContinuousPageViewModeButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPageViewModeContinuousButton
            {
                get
                {
                    return showPageViewModeContinuousButton;
                }
                set
                {
                    showPageViewModeContinuousButton = value;
                }
            }

            private static bool showPageViewModeMultipleButton = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowMultiplePageViewModeButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPageViewModeMultipleButton
            {
                get
                {
                    return showPageViewModeMultipleButton;
                }
                set
                {
                    showPageViewModeMultipleButton = value;
                }
            }

            private static bool showZoom = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowZoomButtons property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowZoom
            {
                get
                {
                    return showZoom;
                }
                set
                {
                    showZoom = value;
                }
            }

            private static int zoom = StiZoomMode.Default;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.Zoom property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static int Zoom
            {
                get
                {
                    return zoom;
                }
                set
                {
                    zoom = value;
                }
            }
        }
        #endregion

        #region Mail

        public sealed class Mail
        {
            private static string addressFrom = string.Empty;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string AddressFrom
            {
                get
                {
                    return addressFrom;
                }
                set
                {
                    addressFrom = value;
                }
            }

            private static string addressTo = string.Empty;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string AddressTo
            {
                get
                {
                    return addressTo;
                }
                set
                {
                    addressTo = value;
                }
            }

            private static string subject = string.Empty;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string Subject
            {
                get
                {
                    return subject;
                }
                set
                {
                    subject = value;
                }
            }

            private static string body = string.Empty;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string Body
            {
                get
                {
                    return body;
                }
                set
                {
                    body = value;
                }
            }

            private static string host = "localhost";
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string Host
            {
                get
                {
                    return host;
                }
                set
                {
                    host = value;
                }
            }

            private static int port = 25;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static int Port
            {
                get
                {
                    return port;
                }
                set
                {
                    port = value;
                }
            }

            private static string userName = string.Empty;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string UserName
            {
                get
                {
                    return userName;
                }
                set
                {
                    userName = value;
                }
            }

            private static string password = string.Empty;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string Password
            {
                get
                {
                    return password;
                }
                set
                {
                    password = value;
                }
            }
        }

        #endregion

        #region Appearance

        public sealed class Appearance
        {
            private static bool autoHideScrollbars = false;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.AutoHideScrollbars property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AutoHideScrollbars
            {
                get
                {
                    return autoHideScrollbars;
                }
                set
                {
                    autoHideScrollbars = value;
                }
            }

            private static Color currentPageBorderColor = Color.Gold;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.CurrentPageBorderColor property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static Color CurrentPageBorderColor
            {
                get
                {
                    return currentPageBorderColor;
                }
                set
                {
                    currentPageBorderColor = value;
                }
            }

            private static string openLinksTarget = "_blank";
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.OpenLinksWindow property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string OpenLinksTarget
            {
                get
                {
                    return openLinksTarget;
                }
                set
                {
                    openLinksTarget = value;
                }
            }

            private static int variablesPanelColumns = 2;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ParametersPanelColumnsCount property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static int VariablesPanelColumns
            {
                get
                {
                    return variablesPanelColumns;
                }
                set
                {
                    variablesPanelColumns = value;
                }
            }

            private static int variablesPanelEditorWidth = 200;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ParametersPanelEditorWidth property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static int VariablesPanelEditorWidth
            {
                get
                {
                    return variablesPanelEditorWidth;
                }
                set
                {
                    variablesPanelEditorWidth = value;
                }
            }

            private static bool showTooltipsHelp = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowTooltipsHelp property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowTooltipsHelp
            {
                get
                {
                    return showTooltipsHelp;
                }
                set
                {
                    showTooltipsHelp = value;
                }
            }

            private static bool showFormsHelp = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowFormsHelp property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowFormsHelp
            {
                get
                {
                    return showFormsHelp;
                }
                set
                {
                    showFormsHelp = value;
                }
            }

            private static bool showFormsHints = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowFormsHints property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowFormsHints
            {
                get
                {
                    return showFormsHints;
                }
                set
                {
                    showFormsHints = value;
                }
            }

            private static string dateFormat = "dd.MM.yyyy";
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ParametersPanelDateFormat property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string DateFormat
            {
                get
                {
                    return dateFormat;
                }
                set
                {
                    dateFormat = value;
                }
            }

            private static bool showEmailDialog = true;
            [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiWebViewerFx.ShowEmailDialog property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowEmailDialog
            {
                get
                {
                    return showEmailDialog;
                }
                set
                {
                    showEmailDialog = value;
                }
            }
        }

        #endregion 

        private static bool allowGCCollect = false;
        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the StiOptions.Web.AllowGCCollect property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool AllowGCCollect
        {
            get
            {
                return allowGCCollect;
            }
            set
            {
                allowGCCollect = value;
            }
        }

    }
}
