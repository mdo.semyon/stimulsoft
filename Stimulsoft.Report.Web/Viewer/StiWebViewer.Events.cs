#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Export;

namespace Stimulsoft.Report.Web
{
    public partial class StiWebViewer :
        WebControl,
        INamingContainer
    {
        #region ViewerEvent

        /// <summary>
        /// The event occurs for any action of the viewer before it is processed.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs for any action of the viewer before it is processed.")]
        public event StiViewerEventHandler ViewerEvent;

        private void InvokeViewerEvent()
        {
            var e = new StiViewerEventArgs(this.RequestParams);
            OnViewerEvent(e);
        }

        protected virtual void OnViewerEvent(StiViewerEventArgs e)
        {
            ViewerEvent?.Invoke(this, e);
        }

        #endregion

        #region GetReport

        /// <summary>
        /// The event occurs when the report is requested after running the viewer.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs when the report is requested after running the viewer.")]
        public event StiReportDataEventHandler GetReport;
        
        private void InvokeGetReport()
        {
            var requestParams = this.RequestParams;
            var report = this.Report;

            if (requestParams.Action == StiAction.GetReport || report == null)
            {
                var e = new StiReportDataEventArgs(requestParams, report);
                OnGetReport(e);
                this.report = e.Report;
                InvokeGetReportData();
                if (e.Report != null)
                {
                    StiReportHelper.ApplyQueryParameters(requestParams, e.Report);
                    var action = requestParams.Action;
                    requestParams.Action = StiAction.GetReport;
                    requestParams.Cache.Helper.SaveReportInternal(requestParams, e.Report);
                    requestParams.Action = action;
                }
            }
            else if (report != null && report.IsRendered == false)
            {
                InvokeGetReportData();
            }
        }

        protected virtual void OnGetReport(StiReportDataEventArgs e)
        {
            GetReport?.Invoke(this, e);
        }

        #endregion

        #region GetReportData

        /// <summary>
        /// The event occurs each time before rendering the report.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs each time before rendering the report.")]
        public event StiReportDataEventHandler GetReportData;

        private void InvokeGetReportData()
        {
            if (this.Report != null && this.Report.IsDocument == false)
            {
                var e = new StiReportDataEventArgs(this.RequestParams, this.Report);
                OnGetReportData(e);
            }
        }

        protected virtual void OnGetReportData(StiReportDataEventArgs e)
        {
            GetReportData?.Invoke(this, e);
        }

        #endregion

        #region PrintReport

        /// <summary>
        /// The event occurs before the printing of the report from the viewer menu.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs before the printing of the report from the viewer menu.")]
        public event StiPrintReportEventHandler PrintReport;

        private void InvokePrintReport(StiExportSettings settings)
        {
            InvokeGetReport();
            var e = new StiPrintReportEventArgs(this.RequestParams, this.Report, settings);
            OnPrintReport(e);
        }

        protected virtual void OnPrintReport(StiPrintReportEventArgs e)
        {
            PrintReport?.Invoke(this, e);
        }

        #endregion

        #region ExportReport

        /// <summary>
        /// The event occurs before the exporting of the report from the viewer menu.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs before the exporting of the report from the viewer menu.")]
        public event StiExportReportEventHandler ExportReport;

        private void InvokeExportReport(StiExportSettings settings)
        {
            InvokeGetReport();
            var e = new StiExportReportEventArgs(this.RequestParams, this.Report, settings);
            OnExportReport(e);
        }

        protected virtual void OnExportReport(StiExportReportEventArgs e)
        {
            if (ExportReport != null) ExportReport(this, e);
            else if (ReportExport != null)
            {
                var e2 = new StiExportDataEventArgs(e.Report, e.Settings);
                ReportExport(this, e2);
            }
        }

        /// <summary>
        /// The event occurs after the report is exported.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs after the report is exported.")]
        public event StiExportReportResponseEventHandler ExportReportResponse;

        private void InvokeExportReportResponse(StiExportSettings settings, Stream stream, string fileName, string contentType)
        {
            var e = new StiExportReportResponseEventArgs(this.RequestParams, this.Report, settings, stream, fileName, contentType);
            OnExportReportResponse(e);
        }

        protected virtual void OnExportReportResponse(StiExportReportResponseEventArgs e)
        {
            ExportReportResponse?.Invoke(this, e);
        }

        #endregion

        #region EmailReport

        /// <summary>
        /// The event occurs when sending a report via Email. In this event it is necessary to set settings for sending Email.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs when sending a report via Email. In this event it is necessary to set settings for sending Email.")]
        public event StiEmailReportEventHandler EmailReport;

        private void InvokeEmailReport(StiExportSettings settings, StiEmailOptions options)
        {
            InvokeGetReport();
            var e = new StiEmailReportEventArgs(this.RequestParams, this.Report, settings, options);
            OnEmailReport(e);
        }

        protected virtual void OnEmailReport(StiEmailReportEventArgs e)
        {
            EmailReport?.Invoke(this, e);
        }

        #endregion

        #region Interaction

        /// <summary>
        /// The event occurs before the interactive action in the viewer, such as sorting, collapsing, drill-down, request from user variables.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs before the interactive action in the viewer, such as sorting, collapsing, drill-down, request from user variables.")]
        public event StiReportDataEventHandler Interaction;

        private void InvokeInteraction()
        {
            InvokeGetReport();
            var e = new StiReportDataEventArgs(this.RequestParams, this.Report);
            OnInteraction(e);
        }

        protected virtual void OnInteraction(StiReportDataEventArgs e)
        {
            Interaction?.Invoke(this, e);
        }

        #endregion

        #region DesignReport

        /// <summary>
        /// The event occurs when clicking on the Design button in the viewer toolbar.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs when clicking on the Design button in the viewer toolbar.")]
        public event StiReportDataEventHandler DesignReport;

        private void InvokeDesignReport()
        {
            InvokeGetReport();
            var e = new StiReportDataEventArgs(this.RequestParams, this.Report);
            OnDesignReport(e);
        }

        protected virtual void OnDesignReport(StiReportDataEventArgs e)
        {
            if (DesignReport != null) DesignReport(this, e);
            else if (ReportDesign != null)
            {
                var e2 = new StiReportDesignEventArgs(e.Report);
                ReportDesign(this, e2);
            }
        }

        #endregion
        

        #region Obsolete
        
        #region ReportConnect

        [Obsolete("This event is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiReportDataEventHandler ReportConnect;

        [Obsolete("This event is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected virtual void OnReportConnect(StiReportDataEventArgs e)
        {
            if (ReportConnect != null) ReportConnect(this, e);
        }

        #endregion

        #region ReportDisconnect

        [Obsolete("This event is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiReportDataEventHandler ReportDisconnect;

        [Obsolete("This event is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected virtual void OnReportDisconnect(StiReportDataEventArgs e)
        {
            if (ReportDisconnect != null) ReportDisconnect(this, e);
        }

        #endregion

        #region ReportDialogResult

        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiReportDialogResultEventHandler(object sender, StiReportDialogResultEventArgs e);

        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiReportDialogResultEventArgs : EventArgs
        {
            private System.Windows.Forms.DialogResult result = System.Windows.Forms.DialogResult.None;
            public System.Windows.Forms.DialogResult Result
            {
                get
                {
                    return result;
                }
            }

            public StiReportDialogResultEventArgs(System.Windows.Forms.DialogResult result)
            {
                this.result = result;
            }
        }

        [Obsolete("This event is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiReportDialogResultEventHandler ReportDialogResult;

        [Obsolete("This event is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected virtual void OnReportDialogResult(StiReportDialogResultEventArgs e)
        {
            if (ReportDialogResult != null) ReportDialogResult(this, e);
        }

        #endregion

        #region ReportAfterDialogResult

        [Obsolete("This event is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiReportDialogResultEventHandler ReportAfterDialogResult;

        [Obsolete("This event is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected virtual void OnReportAfterDialogResult(StiReportDialogResultEventArgs e)
        {
            if (ReportAfterDialogResult != null) ReportAfterDialogResult(this, e);
        }

        #endregion

        #region GetViewerLocalization

        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiLocalizationEventHandler(object sender, StiLocalizationEventArgs e);

        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiLocalizationEventArgs : EventArgs
        {
            private Stream localization = null;
            public Stream Localization
            {
                get
                {
                    return localization;
                }
                set
                {
                    localization = value;
                }
            }


            public StiLocalizationEventArgs()
            {
            }
        }

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the Localization property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiLocalizationEventHandler GetViewerLocalization;

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the Localization property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected virtual Stream OnGetViewerLocalization(StiLocalizationEventArgs e)
        {
            if (GetViewerLocalization != null) GetViewerLocalization(this, e);
            return null;
        }

        #endregion

        #region ReportExport

        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiExportDataEventHandler(object sender, StiExportDataEventArgs e);

        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiExportDataEventArgs : EventArgs
        {
            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
            }

            private StiExportSettings settings = null;
            public StiExportSettings Settings
            {
                get
                {
                    return settings;
                }
            }

            public StiExportDataEventArgs(StiReport report, StiExportSettings settings)
            {
                this.report = report;
                this.settings = settings;
            }
        }

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the ExportReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiExportDataEventHandler ReportExport;

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the ExportReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected virtual void OnReportExport(StiExportDataEventArgs e)
        {
        }

        #endregion

        #region ReportDesign

        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiReportDesignEventHandler(object sender, StiReportDesignEventArgs e);

        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiReportDesignEventArgs : EventArgs
        {
            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
            }


            public StiReportDesignEventArgs(StiReport report)
            {
                this.report = report;
            }
        }

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the DesignReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiReportDesignEventHandler ReportDesign;

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the DesignReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected virtual void OnReportDesign(StiReportDesignEventArgs e)
        {
        }

        #endregion

        #endregion
    }
}
