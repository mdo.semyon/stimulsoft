function StiJsViewer(parameters) {
    this.defaultParameters = {};
    this.options = parameters.options;

    // Compatibility of obsolete options. Need to remove this code in 2018.1.1 release.
    if (typeof this.options.appearance.openLinksWindow == "undefined") this.options.appearance.openLinksWindow = this.options.appearance.openLinksTarget;
    if (typeof this.options.appearance.openExportedReportWindow == "undefined") this.options.appearance.openExportedReportWindow = this.options.appearance.openExportedReportTarget;
    if (typeof this.options.appearance.designWindow == "undefined") this.options.appearance.designWindow = this.options.appearance.designTarget;
    if (this.options.toolbar.viewMode == "OnePage") this.options.toolbar.viewMode = "SinglePage";
    if (this.options.toolbar.viewMode == "WholeReport") this.options.toolbar.viewMode = "MultiplePages";
    // ---

    // Options
    this.options.isTouchDevice = this.options.appearance.interfaceType == "Auto"
        ? this.IsTouchDevice()
        : this.options.appearance.interfaceType == "Touch";
    this.options.isMobileDevice = this.options.appearance.interfaceType == "Auto" && this.options.reportDesignerMode == false
        ? this.IsMobileDevice()
        : this.options.appearance.interfaceType == "Mobile";

    this.options.menuAnimDuration = 150;
    this.options.formAnimDuration = 200;
    this.options.scrollDuration = 350;
    this.options.menuHideDelay = 250;
    this.options.server.timeoutAutoUpdateCache = 180000;
    this.options.toolbar.backgroundColor = this.getHTMLColor(this.options.toolbar.backgroundColor);
    this.options.toolbar.borderColor = this.getHTMLColor(this.options.toolbar.borderColor);
    this.options.toolbar.fontColor = this.getHTMLColor(this.options.toolbar.fontColor);
    this.options.appearance.pageBorderColor = this.getHTMLColor(this.options.appearance.pageBorderColor);
    this.options.exports.defaultSettings = parameters.defaultExportSettings;
    this.options.parametersValues = {};
    this.options.parameterRowHeight = this.options.isTouchDevice ? 35 : 30;
    this.options.minParametersCountForMultiColumns = 5;

    // Collections
    this.collections = {};
    if (parameters.loc && this.collections.loc) this.collections.loc = parameters.loc;

    // Controls
    this.controls = {};
    this.controls.forms = {};
    this.controls.head = document.getElementsByTagName("head")[0];
    this.controls.viewer = document.getElementById(this.options.viewerId);
    this.controls.mainPanel = document.getElementById(this.options.viewerId + "_JsViewerMainPanel");
    this.controls.findHelper = { findLabels: [] };

    // Parameters of the current report
    this.reportParams = {
        drillDownGuid: null,
        pageNumber: 0,
        pagesCount: 0,
        pagesWidth: 0,
        pagesHeight: 0,
        zoom: this.options.toolbar.zoom,
        viewMode: this.options.toolbar.viewMode,
        reportFileName: null,
        pagesArray: [],
        interactionCollapsingStates: null,
        bookmarksContent: null,
        editableParameters: null,
        drillDownParameters: []
    };

    // Service objects and states
    this.service = {};
    this.service.refreshReportTimer = null;
    this.service.isRequestInProcess = false;

    // Actions
    if (!this.options.actions.getReport) this.options.actions.getReport = this.options.actions.viewerEvent;
    if (!this.options.actions.printReport) this.options.actions.printReport = this.options.actions.viewerEvent;
    if (!this.options.actions.openReport) this.options.actions.openReport = this.options.actions.viewerEvent;
    if (!this.options.actions.exportReport) this.options.actions.exportReport = this.options.actions.viewerEvent;
    if (!this.options.actions.interaction) this.options.actions.interaction = this.options.actions.viewerEvent;

    // Render JsViewer styles into HEAD
    if (this.options.requestStylesUrl) {
        var stylesUrl = this.options.appearance.customStylesUrl;
        if (!stylesUrl) {
            stylesUrl = this.options.requestStylesUrl.replace("{action}", this.options.actions.viewerEvent);
            stylesUrl += stylesUrl.indexOf("?") > 0 ? "&" : "?";
            stylesUrl += "stiweb_component=Viewer&stiweb_action=Resource&stiweb_data=styles&stiweb_theme=" + this.options.theme;
            stylesUrl += "&stiweb_cachemode=" + (this.options.server.useCacheForResources
                ? this.options.server.cacheMode == "ObjectSession" || this.options.server.cacheMode == "StringSession"
                    ? "session"
                    : "cache"
                : "none");
            stylesUrl += "&stiweb_version=" + this.options.shortProductVersion;
        }

        var viewerStyles = document.createElement("link");
        viewerStyles.setAttribute("type", "text/css");
        viewerStyles.setAttribute("rel", "stylesheet");
        viewerStyles.setAttribute("href", stylesUrl);
        this.controls.head.appendChild(viewerStyles);
    }

    if (this.options.isMobileDevice) this.InitializeMobile();
    else this.options.toolbar.showPinToolbarButton = false;
    
    if (!(window.File && window.FileReader && window.FileList && window.Blob)) this.options.toolbar.showOpenButton = false;
    this.InitializeJsViewer();
    this.InitializeToolBar();
    if (this.options.toolbar.showFindButton) this.InitializeFindPanel();
    this.InitializeDrillDownPanel();
    if (this.options.toolbar.showResourcesButton) this.InitializeResourcesPanel();
    this.InitializeDisabledPanels();
    this.InitializeAboutPanel();
    this.InitializeReportPanel();
    this.InitializeProcessImage();
    this.InitializeDatePicker();
    this.InitializeToolTip();
    if (this.options.toolbar.displayMode == "Separated" && this.options.toolbar.visible) this.InitializeNavigatePanel();
    if (this.options.toolbar.showSaveButton && this.options.toolbar.visible) this.InitializeSaveMenu();
    if (this.options.toolbar.showSendEmailButton && this.options.toolbar.visible) this.InitializeSendEmailMenu();
    if (this.options.toolbar.showPrintButton && this.options.toolbar.visible) this.InitializePrintMenu();
    if (this.options.toolbar.showZoomButton && (this.options.toolbar.visible || this.options.toolbar.displayMode == "Separated")) this.InitializeZoomMenu();
    if (this.options.toolbar.showViewModeButton && this.options.toolbar.visible) this.InitializeViewModeMenu();
    if (this.options.exports.showExportDialog || this.options.email.showExportDialog) this.InitializeExportForm();
    if (this.options.toolbar.showSendEmailButton && this.options.email.showEmailDialog && this.options.toolbar.visible) this.InitializeSendEmailForm();
    this.addHoverEventsToMenus();
        
    var jsObject = this;

    this.addEvent(document, 'mouseup', function (event) {
        jsObject.DocumentMouseUp(event)
    });

    this.addEvent(document, 'mousemove', function (event) {
        jsObject.DocumentMouseMove(event)
    });

    if (document.all && !document.querySelector) {
        alert("Your web browser is not supported by our application. Please upgrade your browser!");
    }

    this.controls.viewer.style.top = 0;
    this.controls.viewer.style.right = 0;
    this.controls.viewer.style.bottom = 0;
    this.controls.viewer.style.left = 0;

    this.options.appearance.userScrollbarsMode = this.options.appearance.scrollbarsMode;
    this.changeFullScreenMode(this.options.appearance.fullScreenMode);
}

StiJsViewer.prototype.InitializeMobile = function () {
    var isViewPortExist = false;
    var metas = this.controls.head.getElementsByTagName("meta");
    for (var i = 0; i < metas.length; i++) {
        if (metas[i].name && metas[i].name.toLowerCase() == "viewport") {
            isViewPortExist = true;
            break;
        }
    }

    if (!isViewPortExist) {
        var viewPortTag = document.createElement("meta");
        viewPortTag.id = "viewport";
        viewPortTag.name = "viewport";
        viewPortTag.content = "initial-scale=1.0,width=device-width,user-scalable=0";
        this.controls.head.appendChild(viewPortTag);
    }

    this.options.appearance.fullScreenMode = true;
    this.options.appearance.scrollbarsMode = true;
    this.options.appearance.parametersPanelPosition = "Left";
    this.options.appearance.parametersPanelColumnsCount = 1;
    this.options.toolbar.displayMode = "Separated";
    this.options.toolbar.viewMode = "SinglePage";
    this.options.toolbar.showZoomButton = false;
    this.options.toolbar.zoom = this.reportParams.zoom = -1; // PageWidth
    this.options.toolbar.showButtonCaptions = false;
    this.options.toolbar.showOpenButton = false;
    this.options.toolbar.showSendEmailButton = false;
    this.options.toolbar.showFindButton = false;
    this.options.toolbar.showEditorButton = false;
    this.options.toolbar.showFullScreenButton = false;
    this.options.toolbar.showAboutButton = false;
    this.options.toolbar.showViewModeButton = false;
    
    this.InitializeCenterText();
    
    // Append font for pictorial chart
    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode(
        "@font-face{font-family:'Stimulsoft';" +
        "src:url(data:font/ttf;base64,AAEAAAALAIAAAwAwT1MvMg8SB94AAAC8AAAAYGNtYXB84HyCAAABHAAAAKxnYXNwAAAAEAAAAcgAAAAIZ2x5ZnsqufkAAAHQAAAcDGhlYWQPpKc5AAAd3AAAADZoaGVhB8UD/AAAHhQAAAAkaG10eOjAB1QAAB44" +
        "AAAA9GxvY2G5XMAMAAAfLAAAAHxtYXhwAEoAgQAAH6gAAAAgbmFtZSWAbToAAB/IAAABqnBvc3QAAwAAAAAhdAAAACAAAwPyAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADqwgPA/8AAQ" +
        "APAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAkAAAACAAIAAEAAAAAQAg6SrpOulC6Vjpdumi6bDpyune6r7qwOrC//3//wAAAAAAIOkA6TrpQulY6Xbpoumv6crp3Oq+6sDqwv/9//8AAf/jFwQW9RbuFt" +
        "kWvBaRFoUWbBZbFXwVexV6AAMAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAf//AA8AAQAAAAAAAAAAAAIAADc5AQAAAAABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAA/8AEAAPAAAYAAAERIQkBIRE" +
        "BIP7gAgACAP7gA8D+AP4AAgACAAAAAAABAAD/wAQAA8AABgAAEyERCQERIQACAAIA/gD+AAKgASD+AP4AASAAAQAA/8AEAAPAAAcAABMBByERBwkBAAGk7ANI7f5d/pACUP5d7QNI7AGk/pAAAAAAAQAA/8AEAAPAAAcAABMBJyERJwkBAAGk7ANI7f5d" +
        "/pABMAGk7Py47P5cAXAAAAAAAQAA/8AEAAPAAAYAABMhESERIQEAASABwAEg/gABwP4AAgACAAAAAAEABwACBAIDcwAeAAA3JzcXHgEfAQE2Nz4BNzY3NhYfAQEGBw4BBwYxIiYnvrdXJA9UK2oBEzkyM0wWFwEDER0s/rxDOztZGhoBbUy/vlwlD1Yub" +
        "QFcSEBAYB0dAQUNHi7+cVJISWwgH29OAAEAAP/ABAADwAAbAAABFAcOAQcGIyInLgEnJjU0Nz4BNzYzMhceARcWBAAoKIteXWpqXV6LKCgoKIteXWpqXV6LKCgBwGpdXosoKCgoi15dampdXosoKCgoi15dAAAAAAIAAP/ABAADwAAdACMAAAEiBw4BBw" +
        "YVFBceARcWMzI3PgE3NjUxNCcuAScmIwMnNxcBFwIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qPudGoQFCRQPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/NvlRaABhEQAAAIAAP/ABAADwAAdACkAAAEiBw4BBwYVFBceARcWMzI3PgE3NjUxNCcuASc" +
        "mIwEHJwcnNyc3FzcXBwIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qASVK29tK3NxK29tK3APAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/SVK3NxK29tK3NxK2wADAAD/wAQAA8AAHQArAEEAAAEiBw4BBwYVFBceARcWMzI3PgE3NjUxNCcuAScmIxEi" +
        "JjU0NjMyFhUxFAYjNxQGIyImNTE0Jy4BJyYxNDYzMhYVMQIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qHisrHh4rKx4lFg8PFgUGDgUGKx4eKwPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/LcrHh4rKx4eK9sPFRUPDjs7gjQ0HisrHgAAAQAA/8AEA" +
        "APAAAwAABM3CQEXCQEHCQEnCQEAkgFuAW6S/pIBbpL+kv6SkgFu/pIDLpL+kgFukv6S/pKSAW7+kpIBbgFuAAAAAgGO/8ACcgPAAAsAIgAAJRQGIyImNTQ2MzIWJyImNTE0Jy4BJyYxNDYzMhYVMQMUBiMCckMvL0NDLy9DchAWDAwcDAxDLy9DTBYQMi" +
        "9DQy8vQ0OOFw8PWlvOVFMvQ0Mv/ccPFwAAAAABAFP/wAOtA8AAIAAAARMHAT4BNzMyFx4BFxYXFhceARcWOwEwBw4BBwYjIiYjATrnY/6VLXZDATEqK0geHxoaHyBKKysyIwUFPkNDfGNjYwIT/c4hA50rNAQODisZGBYVGxswEBAfH0sfHkIAAAAAAQE" +
        "U/8AC7APAACMAAAE1IRUwBw4BBwYVETAXHgEXFjMVITUwNz4BNzY1ETAnLgEnJgEUAdgYGTsZGAIDHSAgO/4oGBk7GRgCAx0gIANxT08CAx0gIDv92BgZOxkYT08CAx0gIDsCKBgZOxkYAAAAAAIAdv/AA4oDwAAjAEcAAAE1IRUwBw4BBwYVETAXHgEX" +
        "FjMVITUwNz4BNzY1ETAnLgEnJiE1IRUwBw4BBwYVETAXHgEXFjMVITUwNz4BNzY1ETAnLgEnJgGxAdkZGDsZGQMCHiAgO/4nGRg8GBkDAh4gIP6KAdkZGDwYGQMCHiAgO/4nGRg7GRkDAh4gIANxT08CAx0gIDv92BgZOxkYT08CAx0gIDsCKBgZOxkYT" +
        "08CAx0gIDv92BgZOxkYT08CAx0gIDsCKBgZOxkYAAMAAP/lBAADmwATACgAPAAAATUhFTAGFREwFjMVITUwNjURMCYhNSEVMAYVETAWMxUhNTA2NREwJiMhNSEVMAYVETAWMxUhNTA2NREwJgJJAbeSJG7+SZIk/m4BtpIlbf5KkiVt/tsBt5Ikbv5Jki" +
        "QDUklJJG7+AJJJSSRuAgCSSUkkbv4AkklJJG4CAJJJSSRu/gCSSUkkbgIAkgAAAAADAAAAIAQAA2AAEwAuADIAABM1IRUwBhURMBYzFSE1MDY1ETAmITUhFQ4BBxUDIwM1MxUwBhcWFx4BFxYxEzA0JRUzNQABgIAgYP6AgCACYAFAOlgOoIDA4DgYDBA" +
        "RHgoLgP4ggAMgQEAgYP5AgEBAIGABwIBAQAFINgH9gAMAQEAoeDxXWKI5OgIggEBAQAABAAABLgQAAlIABAAAExEhESEABAD8AAJS/twBJAAAAAEAAP/ABAADwAAbAAABFAcOAQcGIyInLgEnJjU0Nz4BNzYzMhceARcWBAAoKIteXWpqXV6LKCgoKIte" +
        "XWpqXV6LKCgBwGpdXosoKCgoi15dampdXosoKCgoi15dAAAAAAMAAP/ABAADwAAdADsASwAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTE0Jy4BJyYjESInLgEnJjU0Nz4BNzYzMhceARcWFTEUBw4BBwYjGQEyNz4BNzY1NCcuAScmIwIAal1eiygoKCiLX" +
        "l1qal1eiygoKCiLXl1qY1hXgyUmJiWDV1hjY1hXgyUmJiWDV1hjal1eiygoKCiLXl1qA8AoKIteXWpqXV6LKCgoKIteXWpqXV6LKCj8ICYlg1dYY2NYV4MlJiYlg1dYY2NYV4MlJgPg/AAoKIteXWpqXV6LKCgAAgAA/8AEAAPAAB0AOwAAASIHDgEHBh" +
        "UUFx4BFxYzMjc+ATc2NTE0Jy4BJyYjESInLgEnJjU0Nz4BNzYzMhceARcWFTEUBw4BBwYjAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXWpjWFeDJSYmJYNXWGNjWFeDJSYmJYNXWGMDwCgoi15dampdXosoKCgoi15dampdXosoKPwgJiWDV1hjY1hXgyU" +
        "mJiWDV1hjY1hXgyUmAAAAAwAA/8AEAAPAAB0AOwBFAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1MTQnLgEnJiMRIicuAScmNTQ3PgE3NjMyFx4BFxYVMRQHDgEHBiMZASE0Jy4BJyYjAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXWpjWFeDJSYmJYNXWGNj" +
        "WFeDJSYmJYNXWGMCACgoi15dagPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/CAmJYNXWGNjWFeDJSYmJYNXWGNjWFeDJSYD4P4Aal1eiygoAAMAAP/ABAADwAAdADsAUwAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTE0Jy4BJyYjESInLgEnJjU0Nz4BN" +
        "zYzMhceARcWFTEUBw4BBwYjGQEhFBceARcWMzI3PgE3NjU0Jy4BJyYjAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXWpjWFeDJSYmJYNXWGNjWFeDJSYmJYNXWGP+ICYlg1dYY2NYV4MlJiYlg1dYYwPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/CAmJY" +
        "NXWGNjWFeDJSYmJYNXWGNjWFeDJSYDwP4gY1hXgyUmJiWDV1hjY1hXgyUmAAAACgAA/8ADwAPAAAMABwALAA8AEQAVABkAGwAfACMAAAERMxEDIxEzBREzEQMjETMFMTcjETMnETMRATE3IxEzJxEzEQMAwCCAgP5gwCCAgP5wsMDAoID+cLDAwKCAA8D" +
        "8AAQA/CADwKD8wANA/OADALAQ/YAgAkD9wAGQEP5AIAGA/oAACgAA/8ADwAPAAAMABwALAA8AEwAXABsAHQAhACUAABMzESMTESMRNyMRMwERMxEDIxEzBREzEQMjETMFMTcjETMnETMREKCgkICgwMACQMAggID+YMAggID+cLDAwKCAAXD+YAGQ/oAB" +
        "gCD+QAQA/AAEAPwgA8Cg/MADQPzgAwCwEP2AIAJA/cAAAAAKAAD/wAPAA8AAAwAHAAsADwATABcAGwAfACMAJwAAEzMRIxMRIxE3IxEzEzMRIxMRIxE3IxEzAREzEQMjETMFETMRAyMRMxCgoJCAoMDAUKCgkICgwMABQMAggID+YMAggIABcP5gAZD+g" +
        "AGAIP5AAnD9oAJQ/cACQCD9gAQA/AAEAPwgA8Cg/MADQPzgAwAAAAAACgAA/8ADwAPAAAMABwALAA8AEwAXABsAHwAjACcAABMzESMTESMRNyMRMxMzESMTESMRNyMRMxMzESMTMxEjExEzEQMjETMQoKCQgKDAwFCgoJCAoMDAQMDAIICA4MAggIABcP" +
        "5gAZD+gAGAIP5AAnD9oAJQ/cACQCD9gANA/MADIP0AA+D8AAQA/CADwAAACQAA/8ADwAPAAAMABwALAA8AEwAXABsAHwAjAAATMxEjExEjETcjETMTMxEjExEjETcjETMTMxEjATMRIwMzESMQoKCQgKDAwFCgoJCAoMDAQMDAAQDAwOCAgAFw/mABkP6" +
        "AAYAg/kACcP2gAlD9wAJAIP2AA0D8wAQA/AADIP0AAAAAAQAA/8AEAAPAAAMAAAkDAgD+AAIAAgADwP4A/gACAAAMAAD/wAQAA8AAAwAHAAsADwATABcAGwAfACMAJwArAC8AABMhESEBESERJSERIRMhESEBESERJSERIQUhESEBESERJSERIRMhESEB" +
        "ESERJSERIRABwP5AAbD+YAHA/iAB4FABwP5AAbD+YAHA/iAB4PwQAcD+QAGw/mABwP4gAeBQAcD+QAGw/mABwP4gAeADsP5AAbD+YAGgIP4gAdD+QAGw/mABoCD+IFD+QAGw/mABoCD+IAHQ/kABsP5gAaAg/iAABQAA/8AEAAPAAAMABwALAA8AEwAAA" +
        "REhEQMhESEBIREhASERIQEhESECIAHgIP5gAaD8IAHg/iACIAHg/iD94AHg/iADwP4gAeD+QAGg/gD+IAHg/iAEAP4gAAAGAAD/wAQAA8AAAwAHAAsADwATABcAABMRIREDIREhNxEhEQMhESEBIREhASERIQAB4CD+YAGgYAHgIP5gAaD8IAHg/iACIA" +
        "Hg/iADwP4gAeD+QAGgIP4gAeD+QAGg/gD+IAHg/iAAAAcAAP/ABAADwAADAAcACwAPABMAFwAbAAATESERAyERIRMRIREDIREhAREhEQMhESEBIREhAAHgIP5gAaBgAeAg/mABoP5AAeAg/mABoPwgAeD+IAPA/iAB4P5AAaD+AP4gAeD+QAGgAkD+IAH" +
        "g/kABoP4A/iAAAAgAAP/ABAADwAADAAcACwAPABMAFwAbAB8AABMRIREDIREhExEhEQMhESElESERAyERIRMRIREDIREhAAHgIP5gAaBgAeAg/mABoPwgAeAg/mABoGAB4CD+YAGgA8D+IAHg/kABoP4A/iAB4P5AAaAg/iAB4P5AAaACQP4gAeD+QAGg" +
        "AAEAAP/QBAADsAAJAAABGwEFARMlBRMBAWCgoAFg/wBA/sD+wED/AAKQASD+4ED/AP6AwMABgAEAAAAAAAMAAP/QBAADsAAJABMAGQAAASULAQUBAyUFAxMlBRMnJRsBBQcBBQEDJREEAP6goKD+oAEAQAFAAUBALP7U/tU88QFLlZYBSvD+cP6gAQBAA" +
        "UACUEABIP7gQP8A/oDAwAGA/qC0tAFo7zwBDf7zPO8BOED/AP6AwAMgAAACAAD/0AQAA7AACQATAAABJQsBBQEDJQUDEyUFEyclGwEFBwQA/qCgoP6gAQBAAUABQEAs/tT+1TzxAUuVlgFK8AJQQAEg/uBA/wD+gMDAAYD+oLS0AWjvPAEN/vM87wADAA" +
        "D/0AQAA7AACQATABgAAAElCwEFAQMlBQMTJQUTJyUbAQUHAREFAQMEAP6goKD+oAEAQAFAAUBALP7U/tU88QFLlZYBSvD+cP6gAQBAAlBAASD+4ED/AP6AwMABgP6gtLQBaO88AQ3+8zzv/tgCYED/AP6AAAADAAD/0AQAA7AACQATABsAAAElCwEFAQM" +
        "lBQMTJQUTJyUbAQUHCwIFAQMlFwQA/qCgoP6gAQBAAUABQEAs/tT+1TzxAUuVlgFK8FCgoP6gAQBAAUCgAlBAASD+4ED/AP6AwMABgP6gtLQBaO88AQ3+8zzvATgBIP7gQP8A/oDAYAAAAAABAAAAIAQAA2AAAgAANyEBAAQA/gAgA0AAAAAAAQAAAJsE" +
        "AALlAAIAABMhAQAEAP4AAuX9tgAAAAEAAACbBAAC5QACAAA3IQEABAD+AJsCSgAAAAABAAAAAAQAA2AAEAAAAScRIxUnARUzESE1MxUhETMEAMCAwP4AgAFAgAFAgAFgwAEgoMD+ACD+wMDAAUAAAAAAAwAA/8AEAAOAAAsAFwAwAAAlFAYjIiY1NDYzM" +
        "hYFFAYjIiY1NDYzMhYZASE0JisBFTMTDgEVFBYzITUhIiY1OAE1AYA4KCg4OCgoOAKAOCgoODgoKDj9ACUbwIAwFhpLNQMA/QAbJSAoODgoKDg4KCg4OCgoODgBeAGAGyVA/mQSNB41S0AlGwEAAAABAAD/wAPAA4AANQAAAQ4BIyImJy4BNTQ2NzYnLg" +
        "EnJiMiBw4BBwYxFBceARcWFxYXHgEXFjMwNz4BNzY1NCcuAScmAsAwIDAwYDAwUFAwGBISSCoqGBghITwVFRYXSS0uLy9EQ5FFRDAeHkgeHh8fVCsrAUAwUFAwMGAwMCAwGCsrVB8fHh5IHh4wREWRQ0QvLy4tSRcWFRU8ISEYGCoqSBISAAQAwP/AA0A" +
        "DwAAPABMAHwAjAAABISIGFREUFjMhMjY1ETQmBSEVIRMiJjU0NjMyFhUUBjchESEC4P5AKDg4KAHAKDg4/ngBAP8AgBslJRsbJSXl/gACAAPAOCj8wCg4OCgDQCg4MCD8kCUbGyUlGxslwAKAAAAAAAIAgAAAA4ADwAALABwAAAE0NjMyFhUUBiMiJgUj" +
        "AxMnBxMDIyIGFREhETQmAUBwUFBwcFBQcAHAI8dKYGBKxyNgIAMAIAMAUHBwUFBwcLD+bAF0YGD+jAGUcFD+wAFAUHAAAAADAAD/wAQAA4AAJwA/AEMAAAEjNTQnLgEnJiMiBw4BBwYVERQXHgEXFjMyNz4BNzY9ATMyNjURNCYlLgEnPgE3PgEzMhYXH" +
        "gEXDgEHDgEjIiYBIzUzA8DAHh5pRkVQUEVGaR4eHh5pRkVQUEVGaR4ewBslJfzVHCIJCSIcLGs5OWssHCIJCSIcLGs5OWsCpICAAoBgIR0dLAwNDQwsHR0h/YAhHR0sDA0NDCwdHSFgJRsBQBslPgkSBwcSCQ8PDw8JEgcHEgkPDw/+kcAAAAAAAQAA/8" +
        "AEAAPAABYAAAEnAScFJy4BBwYWHwEDFwEXETM/ATUhAwC3AbeA/dusJlobGg8mrNuAAUm3gEDA/wABQLcBSYDbrCYPGhtaJqz924ABt7f/AMBAgAAAAAACAAAAAAQAA0AAJwArAAABAyM1NCYjISIGFREXMw4BFRQWMzI2NTQmJyEOARUUFjMyNjU0Jic" +
        "zJTUzFwQAgMAmGv3AGiZAUQgJSzU1SwkIAWIICUs1NUsJCFH+wIVgAYABAIAaJiYa/gBADiERNUtLNREhDg4hETVLSzURIQ7AwMAAAAACAAD/wAQAA8AAGwBzAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1NCcuAScmAyImJxM+AT0BNCYjIicuAScmNS4B" +
        "KwEiBh0BFBYfARUmJy4BJyY1NDY3MzI2PwE+AT0BPgEzMhYXDgEHDgEVFBYXHgEzOgEzFhceAQcGBxQGFQYHDgEHBgIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qL1kp6QQEEw0qKipCFBUFDAaADRMKCG4sIyMzDQ4WFXUGDAWABAUeQSE1YywDBgMbH" +
        "R0bHEYmAgUCBgYFBAYFEgEeJSRRLC0DwCgoi15dampdXosoKCgoi15dampdXosoKPxAExEBBwQLBmANExITLBMSAQQFEw3ACRAEN7wfKCdfNTU5NGAsBQSABQwGTQkKFxYDBQMbRyYmRxsbHRMjIl88O0YBAwEgGBkjCgkAAAIBQP/AAoADwAALAB0AAA" +
        "EUBiMiJjU0NjMyFhUjIgYVETMRMxEzETMRMxE0JgJAOCgoODgoKDjAGyVAUCBQQCUDYCg4OCgoODjIJRv+wP6AAYD+gAGAAUAbJQAAAgDA/8ADAAPAAAsAJgAAARQGIyImNTQ2MzIWEzcnLgEjISIGDwEXNxcHMxMzETMRMxMzJzcXAkA4KCg4OCgoOI8" +
        "xhQUOCP8ACA4FhTFvJoZ7FUAgQBV7hiZvA2AoODgoKDg4/jgjzwYICAbOJJBa9v7AAUD+wAFA9lqQAAQAAP/ABAADwAALABcAKQBEAAABFAYjIiY1NDYzMhYFFAYjIiY1NDYzMhYFIyIGFREzETMRMxEzETMRNCYBNycuASMhIgYPARc3FwczEzMRMxEz" +
        "EzMnNxcBADgoKDg4KCg4AkA4KCg4OCgoOP3AwBslQFAgUEAlArQxhQUOCP8ACA4FhTFvJoZ7FUAgQBV7hiZvA2AoODgoKDg4KCg4OCgoODjIJRv+wP6AAYD+gAGAAUAbJf8AI88GCAgGziSQWvb+wAFA/sABQPZakAAAAAACAGL/wAOhA8AALAA5AAABN" +
        "DY3LgEnJgYjIiYHDgEHBgcGFhcWFx4BNz4BMzIWNz4BNz4BNyInLgEnJicDPgEnDgEHDgEXFjY3AxdrBC11GTxqHh9ZMUFxIiIGBxkbGyEgTzIxPDs7OzM1SCAlIQEBFRUzFhUBgBogBSdUHBkjBitSGwGgYWACQiIBBjUuAQFFOjtGRo1AQS8vVQICKC" +
        "oBAU4vNlkDCwoxJyg6AXwhVi0CKyEcViwDKyAAAAYAQP/AA8ADvQANABsANgBZAGsAfgAAASIGFREUFjMyNjURNCYhIgYVERQWMzI2NRE0JhMUFjMxFRQWMzI2PQEzFRQWMzI2PQEyNjURISUuASc3NiYnJgYPAScuASMiBg8BJy4BBw4BHwEOAQcVITU" +
        "jJSImNTQ2MzgBMTgBMTIWFRQGMyImNTQ2MzgBMTgBMTIWFRQGIwOAGiYmGhomJvzmGiYmGhomJkY4KCYaGiaAJhoaJig4/cACPgdFNSAGCQwMGQYgCBYtGBgtFgggBhkMDAkGIDVFBwI+Av6CDRMTDQ0TE7MNExMNDRMTDQJAJhr/ABomJhoBABomJhr/" +
        "ABomJhoBABom/qAoOIAaJiYagIAaJiYagDgoAWBAQm0jQAwZBgYJDEADBwgIBwNADAkGBhkMQCNtQiAgQBMNDRMTDQ0TEw0NExMNDRMABAAA/8ADwAOAAAMABwALAA8AABMRJRETJREhBRElEQMlESEAAYBAAgD+AAIA/gBA/oABgAHAATg0/pQBdkr+Q" +
        "ED+QEgBeP6QNQE7AAEAAAABAABxUvu9Xw889QALBAAAAAAA1kGxXgAAAADWQbFeAAD/wAQCA8AAAAAIAAIAAAAAAAAAAQAAA8D/wAAABAAAAP/+BAIAAQAAAAAAAAAAAAAAAAAAAD0EAAAAAAAAAAAAAAACAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAA" +
        "AHBAAAAAQAAAAEAAAABAAAAAQAAAAEAAGOBAAAUwQAARQEAAB2BAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAADwAAAA8AAAAPAAAADwAAAA8AAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAA" +
        "EAAAABAAAAAQAAAAEAAAABAAAwAQAAIAEAAAABAAAAAQAAAAEAAAABAABQAQAAMAEAAAABAAAYgQAAEAEAAAAAAAAAAAKABQAHgA0AEgAYAB4AIwAwgDyAS4BcgHOAfICJgJeApYC/ANMA5QDpAPUBEQEngUGBYIFxAYKBlQGnAbeBu4HUAd+B7IH7ggw" +
        "CE4IjAi8CPgJOglICVYJZAmECcoKHApYCooK8gsgC2IMCAw2DHQM3A06DeAOBgABAAAAPQB/AAwAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAADgCuAAEAAAAAAAEACgAAAAEAAAAAAAIABwB7AAEAAAAAAAMACgA/AAEAAAAAAAQACgCQAAEAAAAAAAUAC" +
        "wAeAAEAAAAAAAYACgBdAAEAAAAAAAoAGgCuAAMAAQQJAAEAFAAKAAMAAQQJAAIADgCCAAMAAQQJAAMAFABJAAMAAQQJAAQAFACaAAMAAQQJAAUAFgApAAMAAQQJAAYAFABnAAMAAQQJAAoANADIU3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0Vm" +
        "Vyc2lvbiAxLjAAVgBlAHIAcwBpAG8AbgAgADEALgAwU3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0U3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0UmVndWxhcgBSAGUAZwB1AGwAYQByU3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB" +
        "0Rm9udCBnZW5lcmF0ZWQgYnkgSWNvTW9vbi4ARgBvAG4AdAAgAGcAZQBuAGUAcgBhAHQAZQBkACAAYgB5ACAASQBjAG8ATQBvAG8AbgAuAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==) format('truetype');" +
        "font-weight:normal;}"));
    
    document.head.appendChild(newStyle);
}

StiJsViewer.prototype.mergeOptions = function (fromObject, toObject) {
    for (var value in fromObject) {
        if (toObject[value] === undefined || typeof toObject[value] !== "object") toObject[value] = fromObject[value];
        else this.mergeOptions(fromObject[value], toObject[value]);
    }
}

StiJsViewer.prototype.clearViewerState = function (clearParameters) {
    // Clear report state
    this.reportParams.drillDownGuid = null;
    this.reportParams.interactionCollapsingStates = null;
    this.reportParams.bookmarksContent = null;
    this.reportParams.editableParameters = null;
    this.reportParams.resources = null;
    this.reportParams.pageNumber = 0;
    this.reportParams.drillDownParameters = [];
    this.options.paramsVariables = null;

    // Hide panels
    this.InitializeBookmarksPanel();
    this.InitializeParametersPanel();
    if (this.controls.drillDownPanel) this.controls.drillDownPanel.reset();
    if (this.controls.findPanel) this.controls.findPanel.changeVisibleState(false);

    // Clear parameters state
    if (clearParameters) {
        this.options.isParametersReceived = false;
        this.options.isReportRecieved = false;
    }
}
