﻿
StiJsViewer.prototype.BaseMenu = function (name, parentButton, animationDirection, menuStyleName, rightToLeft) {
    var parentMenu = document.createElement("div");
    parentMenu.className = "stiJsViewerParentMenu";
    parentMenu.jsObject = this;
    parentMenu.id = this.generateKey();
    parentMenu.name = name;
    parentMenu.items = {};
    parentMenu.parentButton = parentButton;
    parentMenu.type = null;
    if (parentButton) parentButton.haveMenu = true;
    parentMenu.animationDirection = animationDirection;
    parentMenu.rightToLeft = rightToLeft || this.options.appearance.rightToLeft;
    parentMenu.visible = false;
    parentMenu.style.display = "none";
    if (name) {
        if (!this.controls.menus) this.controls.menus = {};
        if (this.controls.menus[name] != null) {
            this.controls.menus[name].changeVisibleState(false);
            this.controls.mainPanel.removeChild(this.controls.menus[name]);
        }
        this.controls.menus[name] = parentMenu;
    }
    this.controls.mainPanel.appendChild(parentMenu);

    var menu = document.createElement("div");
    menu.style.overflowY = "auto";
    menu.style.overflowX = "hidden";
    menu.style.fontFamily = this.options.toolbar.fontFamily;
    if (this.options.toolbar.fontColor != "") menu.style.color = this.options.toolbar.fontColor;
    parentMenu.appendChild(menu);
    parentMenu.innerContent = menu;
    menu.className = menuStyleName || "stiJsViewerMenu";
    menu.style.maxHeight = "420px";
    
    parentMenu.changeVisibleState = function (state, parentButton, rightAlign, leftOffset) {
        var mainClassName = "stiJsViewerMainPanel";
        if (parentButton) {
            this.parentButton = parentButton;
            parentButton.haveMenu = true;
        }

        if (state) {
            this.style.display = "";
            this.visible = true;
            this.onshow();
            this.parentButton.setSelected(true);
            this.jsObject.options[this.type == null ? "currentMenu" : "current" + this.type] = this;
            this.style.width = this.innerContent.offsetWidth + "px";
            
            if (this.jsObject.options.isMobileDevice) {
                this.jsObject.controls.reportPanel.hideToolbar();
                this.style.marginLeft = "-" + this.style.width;
                setTimeout(function () {
                    parentMenu.style.transitionDuration = "200ms";
                    parentMenu.style.marginLeft = "0";
                });
                setTimeout(function () {
                    parentMenu.style.transitionDuration = "";
                }, 200);
            }
            else {
                this.style.height = this.innerContent.offsetHeight + "px";
                this.style.overflow = "hidden";
                var animDirect = this.animationDirection;
                var browserHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

                this.style.left = this.rightToLeft || rightAlign
                    ? (this.jsObject.FindPosX(this.parentButton, mainClassName) - this.innerContent.offsetWidth + this.parentButton.offsetWidth) - (leftOffset || 0) + "px"
                    : this.jsObject.FindPosX(this.parentButton, mainClassName) - (leftOffset || 0) + "px";

                if (this.parentButton && animDirect == "Down" &&
                    this.jsObject.FindPosY(this.parentButton) + this.parentButton.offsetHeight + this.innerContent.offsetHeight > browserHeight &&
                    this.jsObject.FindPosY(this.parentButton) - this.innerContent.offsetHeight > 0) {
                    animDirect = "Up";
                }

                this.style.top = (animDirect == "Down")
                    ? (this.jsObject.FindPosY(this.parentButton, mainClassName) + this.parentButton.offsetHeight + 2) + "px"
                    : (this.jsObject.FindPosY(this.parentButton, mainClassName) - this.offsetHeight) + "px";
                this.innerContent.style.top = ((animDirect == "Down" ? -1 : 1) * this.innerContent.offsetHeight) + "px";

                d = new Date();
                var endTime = d.getTime();
                if (this.jsObject.options.toolbar.menuAnimation) endTime += this.jsObject.options.menuAnimDuration;
                this.jsObject.ShowAnimationVerticalMenu(this, (animDirect == "Down" ? 0 : -1), endTime);
            }
        }
        else {
            this.onHide();
            clearTimeout(this.innerContent.animationTimer);
            this.visible = false;
            this.parentButton.setSelected(false);
            if (this.jsObject.options.isMobileDevice) {
                this.style.transitionDuration = "200ms";
                this.style.marginLeft = "-" + this.style.width;
                setTimeout(function () {
                    parentMenu.style.transitionDuration = "";
                    parentMenu.style.display = "none";
                }, 200);
            }
            else {
                this.style.display = "none";
            }
            if (this.jsObject.options[this.type == null ? "currentMenu" : "current" + this.type] == this)
                this.jsObject.options[this.type == null ? "currentMenu" : "current" + this.type] = null;
        }
    }

    parentMenu.action = function (menuItem) {
        return menuItem;
    }

    parentMenu.onmousedown = function () {
        if (!this.isTouchStartFlag) this.ontouchstart(true);
    }

    parentMenu.ontouchstart = function (e) {
        if (this.jsObject.options.isMobileDevice && typeof(e) != "boolean") {
            this.touchStartX = parseInt(e.changedTouches[0].clientX);
            this.lastTouches = [{ x: 0, y: 0, time: 0 }, { x: 0, y: 0, time: 0 }];
        }

        var this_ = this;
        this.isTouchStartFlag = e ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.menuPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    parentMenu.ontouchmove = function (e) {
        if (this.jsObject.options.isMobileDevice) {
            this.lastTouches.shift();
            this.lastTouches.push({
                x: e.changedTouches[0].clientX,
                y: e.changedTouches[0].clientY,
                time: new Date().getTime()
            });
        }
    }

    parentMenu.ontouchend = function (e) {
        if (this.jsObject.options.isMobileDevice) {
            var dX = this.lastTouches[1].x - this.lastTouches[0].x;
            var dT = new Date().getTime() - this.lastTouches[1].time;
            if (dX <= -5 && dT <= 14) this.changeVisibleState(false);
        }
    }

    parentMenu.onshow = function () { };
    parentMenu.onHide = function () { };

    return parentMenu;
}