﻿
StiJsViewer.prototype.InitializePrintMenu = function () {
    var items = [];
    items.push(this.Item("PrintPdf", this.collections.loc["PrintPdf"], "PrintPdf.png", "PrintPdf"));
    items.push(this.Item("PrintWithPreview", this.collections.loc["PrintWithPreview"], "PrintWithPreview.png", "PrintWithPreview"));
    items.push(this.Item("PrintWithoutPreview", this.collections.loc["PrintWithoutPreview"], "PrintWithoutPreview.png", "PrintWithoutPreview"));

    var printMenu = this.VerticalMenu("printMenu", this.controls.toolbar.controls["Print"], "Down", items);

    printMenu.action = function (menuItem) {
        printMenu.changeVisibleState(false);
        printMenu.jsObject.postPrint(menuItem.key);
    }
}