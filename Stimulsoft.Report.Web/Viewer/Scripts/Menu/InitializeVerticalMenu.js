﻿
StiJsViewer.prototype.VerticalMenu = function (name, parentButton, animDirection, items, itemStyleName, menuStyleName, rightToLeft) {
    var menu = this.BaseMenu(name, parentButton, animDirection, menuStyleName, rightToLeft);
    menu.itemStyleName = itemStyleName;

    if (this.options.isMobileDevice) {
        menu.style.left = "0";
        menu.style.top = "0";
        menu.style.bottom = "0";
        menu.style.height = "100%";

        menu.innerContent.style.maxHeight = "100%";
        menu.innerContent.style.height = "100%";
        menu.innerContent.style.borderLeftWidth = "0";
        menu.innerContent.style.borderTopWidth = "0";
        menu.innerContent.style.borderBottomWidth = "0";
    }

    menu.addItems = function (items) {
        while (this.innerContent.childNodes[0]) {
            this.innerContent.removeChild(this.innerContent.childNodes[0]);
        }
        for (var index in items) {
            if (typeof (items[index]) != "string")
                this.innerContent.appendChild(this.jsObject.VerticalMenuItem(this, items[index].name, items[index].caption, items[index].imageName, items[index].key, this.itemStyleName));
            else
                this.innerContent.appendChild(this.jsObject.VerticalMenuSeparator(this, items[index]));
        }
    }

    menu.addItems(items);
    
    return menu; 
}

StiJsViewer.prototype.VerticalMenuItem = function (menu, itemName, caption, imageName, key, styleName) {
    var menuItem = document.createElement("div");
    menuItem.jsObject = this;
    menuItem.menu = menu;
    menuItem.name = itemName;
    menuItem.key = key;
    menuItem.caption_ = caption;
    menuItem.imageName = imageName;
    menuItem.styleName = styleName || "stiJsViewerMenuStandartItem";
    menuItem.id = this.generateKey();
    menuItem.className = menuItem.styleName;
    menu.items[itemName] = menuItem;
    menuItem.isEnabled = true;
    menuItem.isSelected = false;
    menuItem.style.height = this.options.isMobileDevice ? "0.4in" : (this.options.isTouchDevice ? "30px" : "24px");

    var innerTable = this.CreateHTMLTable();
    menuItem.appendChild(innerTable);
    innerTable.style.height = "100%";
    innerTable.style.width = "100%";

    if (imageName != null) {
        menuItem.cellImage = innerTable.addCell();
        menuItem.cellImage.style.width = "22px";
        menuItem.cellImage.style.minWidth = "22px";
        menuItem.cellImage.style.padding = "0";
        menuItem.cellImage.style.textAlign = "center";
        var img = document.createElement("img");
        menuItem.image = img;
        menuItem.cellImage.style.lineHeight = "0";
        menuItem.cellImage.appendChild(img);
        if (this.collections.images[imageName]) img.src = this.collections.images[imageName];
    }

    if (caption != null) {
        var captionCell = innerTable.addCell();
        menuItem.caption = captionCell;
        captionCell.style.padding = "0 20px 0 7px";
        captionCell.style.textAlign = "left";
        captionCell.style.whiteSpace = "nowrap";
        if (this.options.isMobileDevice) captionCell.style.fontSize = "0.16in";
        captionCell.innerHTML = caption;
    }

    menuItem.onmouseover = function () {
        if (this.isTouchProcessFlag || !this.isEnabled) return;
        this.className = this.styleName + " " + this.styleName + "Over";
    }

    menuItem.onmouseout = function () {
        if (this.isTouchProcessFlag || !this.isEnabled) return;
        this.className = this.styleName;
        if (this.isSelected) this.className += " " + this.styleName + "Selected";
    }

    menuItem.onclick = function () {
        if (this.isTouchProcessFlag || !this.isEnabled) return;
        this.action();
    }

    menuItem.ontouchstart = function () {
        this.jsObject.options.fingerIsMoved = false;
    }
    
    menuItem.ontouchend = function () {
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        this.isTouchProcessFlag = true;
        this.className = this.styleName + " " + this.styleName + "Over";
        var this_ = this;
        setTimeout(function () {
            this_.className = this_.styleName;
            this_.action();
        }, 150);
        setTimeout(function () {
            this_.isTouchProcessFlag = false;
        }, 1000);
    }

    menuItem.action = function () {
        this.menu.action(this);
    }

    menuItem.setEnabled = function (state) {
        this.isEnabled = state;
        this.className = this.styleName + " " + (state ? "" : (this.styleName + "Disabled"));
    }

    menuItem.setSelected = function (state) {
        if (!state) {
            this.isSelected = false;
            this.className = this.styleName;
            return;
        }
        if (this.menu.selectedItem != null) {
            this.menu.selectedItem.className = this.styleName;
            this.menu.selectedItem.isSelected = false;
        }
        this.className = this.styleName + " " + this.styleName + "Selected";
        this.menu.selectedItem = this;
        this.isSelected = true;
    }

    return menuItem;
}

StiJsViewer.prototype.VerticalMenuSeparator = function (menu, name) {
    var menuSeparator = document.createElement("div");
    menuSeparator.className = "stiJsViewerVerticalMenuSeparator";
    menu.items[name] = menuSeparator;

    return menuSeparator;
}