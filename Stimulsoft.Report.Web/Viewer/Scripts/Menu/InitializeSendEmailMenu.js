﻿
StiJsViewer.prototype.InitializeSendEmailMenu = function () {
    var sendEmailMenu = this.InitializeBaseSaveMenu("sendEmailMenu", this.controls.toolbar.controls["SendEmail"]);

    sendEmailMenu.action = function (menuItem) {
        this.changeVisibleState(false);
        if (this.jsObject.options.email.showExportDialog)
            this.jsObject.controls.forms.exportForm.show(menuItem.key, this.jsObject.options.actions.emailReport);
        else if (this.jsObject.options.email.showEmailDialog) {
            this.jsObject.controls.forms.sendEmailForm.show(menuItem.key, this.jsObject.getDefaultExportSettings(menuItem.key));
        }
        else {
            var exportSettings = this.jsObject.getDefaultExportSettings(menuItem.key);
            exportSettingsObject["Email"] = this.jsObject.options.email.defaultEmailAddress;
            exportSettingsObject["Message"] = this.jsObject.options.email.defaultEmailMessage;
            exportSettingsObject["Subject"] = this.jsObject.options.email.defaultEmailSubject;
            this.jsObject.postEmail(menuItem.key, defaultSettings, this.jsObject.options.actions.emailReport);
        }
    }
}