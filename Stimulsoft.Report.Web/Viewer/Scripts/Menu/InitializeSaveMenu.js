﻿
StiJsViewer.prototype.InitializeSaveMenu = function (menuName, parentButton) {
    var saveMenu = this.InitializeBaseSaveMenu("saveMenu", this.controls.toolbar.controls["Save"]);

    saveMenu.action = function (menuItem) {
        saveMenu.changeVisibleState(false);
        if (saveMenu.jsObject.options.exports.showExportDialog)
            saveMenu.jsObject.controls.forms.exportForm.show(menuItem.key, saveMenu.jsObject.options.actions.exportReport);
        else
            saveMenu.jsObject.postExport(menuItem.key, saveMenu.jsObject.getDefaultExportSettings(menuItem.key));
    }
}


StiJsViewer.prototype.InitializeBaseSaveMenu = function (menuName, parentButton) {
    var isFirst = true;
    var items = [];
    if (this.options.exports.showExportToDocument && menuName == "saveMenu") {
        items.push(this.Item("Document", this.collections.loc["SaveDocument"], "SaveDocument.png", "Document"));
        isFirst = false;
    }
    if (menuName == "saveMenu" && this.options.exports.showExportToPdf || this.options.exports.showExportToXps || this.options.exports.showExportToPowerPoint) {
        if (!isFirst) items.push("separator1");
        isFirst = false;
    }
    if (this.options.exports.showExportToPdf) items.push(this.Item("Pdf", this.collections.loc["SavePdf"], "SavePdf.png", "Pdf"));
    if (this.options.exports.showExportToXps) items.push(this.Item("Xps", this.collections.loc["SaveXps"], "SaveXps.png", "Xps"));
    if (this.options.exports.showExportToPowerPoint) items.push(this.Item("Ppt2007", this.collections.loc["SavePpt2007"], "SavePpt2007.png", "Ppt2007"));

    if (this.options.exports.showExportToHtml || this.options.exports.showExportToHtml5 || this.options.exports.showExportToMht) {
        if (!isFirst) items.push("separator2");
        isFirst = false;
        var htmlType = this.options.exports.defaultSettings["StiHtmlExportSettings"].HtmlType;
        if (!this.options.exports["showExportTo" + htmlType]) {
            if (this.options.exports.showExportToHtml) htmlType = "Html";
            else if (this.options.exports.showExportToHtml5) htmlType = "Html5";
            else if (this.options.exports.showExportToMht) htmlType = "Mht";
        }
        items.push(this.Item(htmlType, this.collections.loc["SaveHtml"], "SaveHtml.png", htmlType));
    }
    if (this.options.exports.showExportToText || this.options.exports.showExportToRtf || this.options.exports.showExportToWord2007 || this.options.exports.showExportToOdt) {
        if (!isFirst) items.push("separator3");
        isFirst = false;
    }
    if (this.options.exports.showExportToText) items.push(this.Item("Text", this.collections.loc["SaveText"], "SaveText.png", "Text"));
    if (this.options.exports.showExportToRtf) items.push(this.Item("Rtf", this.collections.loc["SaveRtf"], "SaveRtf.png", "Rtf"));
    if (this.options.exports.showExportToWord2007) items.push(this.Item("Word2007", this.collections.loc["SaveWord2007"], "SaveWord2007.png", "Word2007"));
    if (this.options.exports.showExportToOpenDocumentWriter) items.push(this.Item("Odt", this.collections.loc["SaveOdt"], "SaveOdt.png", "Odt"));
    if (this.options.exports.showExportToExcel || this.options.exports.showExportToExcel2007 || this.options.exports.showExportToExcelXml || this.options.exports.showExportToOpenDocumentWriter) {
        if (!isFirst) items.push("separator4");
        isFirst = false;
    }
    if (this.options.exports.showExportToExcel || this.options.exports.showExportToExcelXml || this.options.exports.showExportToExcel2007) {
        var excelType = this.options.exports.defaultSettings["StiExcelExportSettings"].ExcelType;
        if (excelType == "ExcelBinary") excelType = "Excel";
        if (!this.options.exports["showExportTo" + excelType]) {
            if (this.options.exports.showExportToExcel) excelType = "Excel";
            else if (this.options.exports.showExportToExcel2007) excelType = "Excel2007";
            else if (this.options.exports.showExportToExcelXml) excelType = "ExcelXml";
        }
        items.push(this.Item(excelType, this.collections.loc["SaveExcel"], "SaveExcel.png", excelType));
    }
    if (this.options.exports.showExportToOpenDocumentCalc) {
        items.push(this.Item("Ods", this.collections.loc["SaveOds"], "SaveOds.png", "Ods"));
    }
    if (this.options.exports.showExportToCsv || this.options.exports.showExportToDbf || this.options.exports.showExportToXml || this.options.exports.showExportToDif || this.options.exports.showExportToSylk) {
        if (!isFirst) items.push("separator5");
        isFirst = false;
        var dataType = this.options.exports.defaultSettings["StiDataExportSettings"].DataType;
        if (!this.options.exports["showExportTo" + dataType]) {
            if (this.options.exports.showExportToCsv) dataType = "Csv";
            else if (this.options.exports.showExportToDbf) dataType = "Dbf";
            else if (this.options.exports.showExportToXml) dataType = "Xml";
            else if (this.options.exports.showExportToDif) dataType = "Dif";
            else if (this.options.exports.showExportToSylk) dataType = "Sylk";
        }
        items.push(this.Item(dataType, this.collections.loc["SaveData"], "SaveData.png", dataType));
    }
    if (this.options.exports.showExportToImageBmp || this.options.exports.showExportToImageGif || this.options.exports.showExportToImageJpeg || this.options.exports.showExportToImagePcx ||
        this.options.exports.showExportToImagePng || this.options.exports.showExportToImageTiff || this.options.exports.showExportToImageMetafile || this.options.exports.showExportToImageSvg || this.options.exports.showExportToImageSvgz) {
        if (!isFirst) items.push("separator6");
        isFirst = false;
        var imageType = this.options.exports.defaultSettings["StiImageExportSettings"].ImageType;
        var imageType_ = imageType == "Emf" ? "Metafile" : imageType;
        if (!this.options.exports["showExportToImage" + imageType_]) {
            if (this.options.exports.showExportToImageBmp) imageType = "Bmp";
            else if (this.options.exports.showExportToImageGif) imageType = "Gif";
            else if (this.options.exports.showExportToImageJpeg) imageType = "Jpeg";
            else if (this.options.exports.showExportToImagePcx) imageType = "Pcx";
            else if (this.options.exports.showExportToImagePng) imageType = "Png";
            else if (this.options.exports.showExportToImageTiff) imageType = "Tiff";
            else if (this.options.exports.showExportToImageMetafile) imageType = "Emf";
            else if (this.options.exports.showExportToImageSvg) imageType = "Svg";
            else if (this.options.exports.showExportToImageSvgz) imageType = "Svgz";
        }
        items.push(this.Item("Image" + imageType, this.collections.loc["SaveImage"], "SaveImage.png", "Image" + imageType));
    }

    var baseSaveMenu = this.VerticalMenu(menuName, parentButton, "Down", items);
    baseSaveMenu.menuName = menuName;

    return baseSaveMenu;
}