
//Document MouseUp
StiJsViewer.prototype.DocumentMouseUp = function (event) {
    this.options.formInDrag = false;
}

//Document Mouse Move
StiJsViewer.prototype.DocumentMouseMove = function (event) {
    if (this.options.formInDrag) this.options.formInDrag[4].move(event);
}