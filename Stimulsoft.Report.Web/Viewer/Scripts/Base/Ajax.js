﻿
StiJsViewer.prototype.createConnection = function () {
    if (typeof XMLHttpRequest != "undefined") return new XMLHttpRequest();

    // old IE
    if (window.ActiveXObject) {
        var allVersions = [
            "MSXML2.XMLHttp.5.0",
            "MSXML2.XMLHttp.4.0",
            "MSXML2.XMLHttp.3.0",
            "MSXML2.XMLHttp",
            "Microsoft.XMLHttp"
        ];
        for (var i = 0; i < allVersions.length; i++) {
            try {
                return new ActiveXObject(allVersions[i]);
            }
            catch (oError) {
            }
        }
    }

    throw new Error("Unable to create XMLHttp object.");
}

StiJsViewer.prototype.openConnection = function (http, url, responseType) {
    http.open("POST", url);
    http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    http.responseType = responseType ? responseType : "text";
}

StiJsViewer.prototype.createPostParameters = function (data, asObject) {
    if (this.reportParams.zoom == -1 || this.reportParams.zoom == -2) this.reportParams.autoZoom = this.reportParams.zoom;

    var params = {
        "viewerId": this.options.viewerId,
        "routes": this.options.routes,
        "formValues": this.options.formValues,
        "clientGuid": this.options.clientGuid,
        "drillDownGuid": this.reportParams.drillDownGuid,
        "cacheMode": this.options.server.cacheMode,
        "cacheTimeout": this.options.server.cacheTimeout,
        "cacheItemPriority": this.options.server.cacheItemPriority,
        "pageNumber": this.reportParams.pageNumber,
        "zoom": (this.reportParams.zoom == -1 || this.reportParams.zoom == -2) ? 100 : this.reportParams.zoom,
        "viewMode": this.reportParams.viewMode,
        "multiPageWidthCount": this.reportParams.multiPageWidthCount,
        "multiPageHeightCount": this.reportParams.multiPageHeightCount,
        "multiPageContainerWidth": this.reportParams.multiPageContainerWidth,
        "multiPageContainerHeight": this.reportParams.multiPageContainerHeight,
        "multiPageMargins": this.reportParams.multiPageMargins,
        "showBookmarks": this.options.toolbar.showBookmarksButton,
        "openLinksWindow": this.options.appearance.openLinksWindow,
        "chartRenderType": this.options.appearance.chartRenderType,
        "reportDisplayMode": this.options.appearance.reportDisplayMode,
        "drillDownParameters": this.reportParams.drillDownParameters,
        "editableParameters": this.reportParams.editableParameters,
        "useRelativeUrls": this.options.server.useRelativeUrls,
        "passQueryParametersForResources": this.options.server.passQueryParametersForResources,
        "passQueryParametersToReport": this.options.server.passQueryParametersToReport,
        "version": this.options.shortProductVersion,
        "reportDesignerMode": this.options.reportDesignerMode
    };

    if (data) for (var key in data) params[key] = data[key];

    // Object params
    var postParams = {
        stiweb_component: "Viewer"
    };
    if (params.action) {
        postParams["stiweb_action"] = params.action;
        delete params.action;
    }
    if (params.base64Data) {
        postParams["stiweb_data"] = params.base64Data;
        delete params.base64Data;
    }

    // Params
    var jsonParams = JSON.stringify(params);
    if (this.options.server.useCompression) postParams["stiweb_packed_parameters"] = StiGZipHelper.pack(jsonParams);
    else postParams["stiweb_parameters"] = Base64.encode(jsonParams);
    if (asObject) return postParams;

    // URL params
    var urlParams = "stiweb_component=" + postParams["stiweb_component"] + "&";
    if (postParams["stiweb_action"]) urlParams += "stiweb_action=" + postParams["stiweb_action"] + "&";
    if (postParams["stiweb_data"]) urlParams += "stiweb_data=" + encodeURIComponent(postParams["stiweb_data"]) + "&";
    if (postParams["stiweb_parameters"]) urlParams += "stiweb_parameters=" + encodeURIComponent(postParams["stiweb_parameters"]);
    else urlParams += "stiweb_packed_parameters=" + encodeURIComponent(postParams["stiweb_packed_parameters"]);
    return urlParams;
}

StiJsViewer.prototype.postAjax = function (url, data, callback) {
    if (data && data.action == "GetReport") {
        this.options.paramsVariablesStartValues = null

        if (this.controls.toolbar) {
            this.controls.toolbar.setEnabled(false);
            if (this.controls.navigatePanel) this.controls.navigatePanel.setEnabled(false);
        }
    }

    var jsObject = this;
    var xmlHttp = this.createConnection();
    this.openConnection(xmlHttp, url, data ? data.responseType : "text");

    if (jsObject.options.server.requestTimeout != 0) {
        setTimeout(function () {
            if (xmlHttp.readyState < 4) xmlHttp.abort();
        }, jsObject.options.server.requestTimeout * 1000);
    }
    
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4) {
            jsObject.service.isRequestInProcess = false;
            var status = 0;
            try {
                status = xmlHttp.status;
            }
            catch (e) {
            }
            
            if (status == 0) {
                callback("ServerError:Timeout response from the server.", jsObject);
            } else if (status == 200) {
                callback(xmlHttp.response ? xmlHttp.response : xmlHttp.responseText, jsObject);
            } else {
                if (jsObject.options.server.showServerErrorPage && xmlHttp.response) jsObject.controls.reportPanel.innerHTML = xmlHttp.response;
                callback("ServerError:" + status + " - " + xmlHttp.statusText, jsObject);
            }
        }
    };

    this.service.isRequestInProcess = true;
    var params = this.createPostParameters(data, false);
    xmlHttp.send(params);
}

StiJsViewer.prototype.postForm = function (url, data, doc) {
    if (!doc) doc = document;
    var form = doc.createElement("FORM");
    form.setAttribute("method", "POST");
    form.setAttribute("action", url);

    var params = this.createPostParameters(data, true);
    for (var key in params) {
        var paramsField = doc.createElement("INPUT");
        paramsField.setAttribute("type", "hidden");
        paramsField.setAttribute("name", key);
        paramsField.setAttribute("value", params[key]);
        form.appendChild(paramsField);
    }

    doc.body.appendChild(form);
    form.submit();
    doc.body.removeChild(form);
}

StiJsViewer.prototype.showError = function (message) {
    var messageType = "Error";
    var messageText = null;
    
    // Check for error in "ServerError:" string format
    if (message != null && typeof(message) == "string" && message.substr(0, 12) == "ServerError:") {
        if (message.length <= 13) messageText = "An unknown error occurred (the server returned an empty value).";
        else messageText = message.substr(12);
    }

    // Check for error in JSON format
    if (message != null && message.success === false && message.type && message.text) {
        messageType = message.type;
        messageText = message.text;
    }

    if (messageText != null) {
        var errorForm = this.controls.forms.errorMessageForm || this.InitializeErrorMessageForm();
        errorForm.show(messageText.replace("\n", "<br>"), messageType);
        return true;
    }

    return false;
}
