﻿StiJsViewer.prototype.postAction = function (action, bookmarkPage, bookmarkAnchor, componentGuid) {
    switch (action) {
        case "Refresh":
            break;

        case "Print":
            switch (this.options.toolbar.printDestination) {
                case "Pdf": this.postPrint("PrintPdf"); break;
                case "Direct": this.postPrint("PrintWithoutPreview"); break;
                case "WithPreview": this.postPrint("PrintWithPreview"); break;
                default: this.controls.menus.printMenu.changeVisibleState(!this.controls.menus.printMenu.visible); break;
            }
            return;

        case "Open":
            var openReportDialog = this.InitializeOpenDialog("openReportDialog", function (fileName, filePath, content) {
                openReportDialog.jsObject.postOpen(fileName, content);
            }, ".mdc,.mdz,.mdx");
            openReportDialog.action();
            return;

        case "Save":
            this.controls.menus.saveMenu.changeVisibleState(!this.controls.menus.saveMenu.visible);
            return;

        case "SendEmail":
            this.controls.menus.sendEmailMenu.changeVisibleState(!this.controls.menus.sendEmailMenu.visible);
            return;

        case "Zoom":
            this.controls.menus.zoomMenu.changeVisibleState(!this.controls.menus.zoomMenu.visible);
            return;

        case "ViewMode":
            this.controls.menus.viewModeMenu.changeVisibleState(!this.controls.menus.viewModeMenu.visible);
            return;

        case "FirstPage":
        case "PrevPage":
        case "NextPage":
        case "LastPage":
            if (action == "FirstPage") this.reportParams.pageNumber = 0;
            if (action == "PrevPage" && this.reportParams.pageNumber > 0) this.reportParams.pageNumber--;
            if (action == "NextPage" && this.reportParams.pageNumber < this.reportParams.pagesCount - 1) this.reportParams.pageNumber++;
            if (action == "LastPage") this.reportParams.pageNumber = this.reportParams.pagesCount - 1;
            if (this.controls.reportPanel.pagesNavigationIsActive()) {
                this.scrollToPage(this.reportParams.pageNumber);
                if (this.controls.toolbar) this.controls.toolbar.changeToolBarState();
                return;
            }
            break;

        case "FullScreen":
            this.changeFullScreenMode(!this.options.appearance.fullScreenMode);
            return;

        case "Zoom25": this.reportParams.zoom = 25; break;
        case "Zoom50": this.reportParams.zoom = 50; break;
        case "Zoom75": this.reportParams.zoom = 75; break;
        case "Zoom100": this.reportParams.zoom = 100; break;
        case "Zoom150": this.reportParams.zoom = 150; break;
        case "Zoom200": this.reportParams.zoom = 200; break;

        case "ZoomOnePage":
        case "ZoomPageWidth":
            {
                if (this.options.toolbar.displayMode == "Separated") {
                    this.controls.toolbar.controls.ZoomOnePage.setSelected(action == "ZoomOnePage");
                    this.controls.toolbar.controls.ZoomPageWidth.setSelected(action == "ZoomPageWidth");
                }
                this.reportParams.zoom = action == "ZoomPageWidth" ? parseInt(this.controls.reportPanel.getZoomByPageWidth()) : parseInt(this.controls.reportPanel.getZoomByPageHeight());
                break;
            }

        case "ViewModeSinglePage":
            this.reportParams.viewMode = "SinglePage";
            break;

        case "ViewModeContinuous":
            this.reportParams.viewMode = "Continuous";
            break;

        case "ViewModeMultiplePages":
            this.reportParams.viewMode = "MultiplePages";
            break;

        case "ViewModeMultiPage":
            this.reportParams.viewMode = "MultiPage";
            this.reportParams.multiPageContainerWidth = this.controls.reportPanel.offsetWidth;
            this.reportParams.multiPageContainerHeight = this.controls.reportPanel.offsetHeight;
            this.reportParams.multiPageMargins = 10;
            break;

        case "GoToPage":
            this.reportParams.pageNumber = this.controls.toolbar.controls["PageControl"].textBox.getCorrectValue() - 1;
            if (this.controls.reportPanel.pagesNavigationIsActive()) {
                this.scrollToPage(this.reportParams.pageNumber);
                if (this.controls.toolbar) this.controls.toolbar.changeToolBarState();
                return;
            }
            break;

        case "BookmarkAction":
            if (this.reportParams.pageNumber == bookmarkPage || this.reportParams.viewMode != "SinglePage") {
                this.scrollToAnchor(bookmarkAnchor, componentGuid);
                return;
            }
            else {
                this.reportParams.pageNumber = bookmarkPage;
                this.options.bookmarkAnchor = bookmarkAnchor;
                this.options.componentGuid = componentGuid;
            }
            break;

        case "Bookmarks":
            this.controls.bookmarksPanel.changeVisibleState(!this.controls.buttons["Bookmarks"].isSelected);
            return;

        case "Parameters":
            this.controls.parametersPanel.changeVisibleState(!this.controls.buttons["Parameters"].isSelected);
            return;

        case "Resources":
            this.controls.resourcesPanel.changeVisibleState(!this.controls.buttons["Resources"].isSelected);
            return;

        case "Find":
            this.controls.findPanel.changeVisibleState(!this.controls.toolbar.controls.Find.isSelected);
            return;

        case "About":
            this.controls.aboutPanel.changeVisibleState(!this.controls.toolbar.controls.About.isSelected);
            return;

        case "Design":
            this.postDesign();
            return;

        case "Pin":
            if (this.controls.toolbar) this.controls.toolbar.changePinState(!this.options.toolbar.autoHide);
            return;

        case "Submit":
            this.reportParams.editableParameters = null;
            this.reportParams.pageNumber = 0;
            if (this.options.isMobileDevice) this.controls.parametersPanel.changeVisibleState(false);
            this.postInteraction({ action: "Variables", variables: this.controls.parametersPanel.getParametersValues() });
            return;

        case "Reset":
            this.options.parameters = {};
            if (this.options.paramsVariablesStartValues) {
                this.options.paramsVariables = this.options.paramsVariablesStartValues;
            }
            this.controls.parametersPanel.clearParameters();
            this.controls.parametersPanel.addParameters();
            return;

        case "Editor":
            this.SetEditableMode(!this.options.editableMode);
            return;
    }
    
    var viewerAction = action == "Refresh" ? "RefreshReport" : "GetPages";
    if (!action || action == "GetReport") {
        this.clearViewerState(true);
        viewerAction = "GetReport";
    }
    var requestUrl = this.options.requestUrl.replace("{action}",
        (viewerAction == "GetReport" || this.options.server.cacheMode == "None")
            ? this.options.actions.getReport
            : this.options.actions.viewerEvent);
    
    this.controls.processImage.show();
    this.postAjax(requestUrl, { action: viewerAction }, this.showReportPage);
}

StiJsViewer.prototype.postOpen = function (fileName, content) {
    if (typeof content != "string" || content == "") return;
    if (content.indexOf("<?xml") == 0 || content.indexOf("{") == 0) content = Base64.encode(content);

    var data = {
        "action": "OpenReport",
        "openingFileName": fileName || "Report.mdc",
        "base64Data": content.indexOf("base64,") > 0 ? content.substr(content.indexOf("base64,") + 7) : content
    };
    
    this.clearViewerState();
    this.reportParams.reportFileName = fileName;

    var jsObject = this;
    if (fileName && fileName.toLowerCase().indexOf(".mdx") >= 0) {
        var passwordForm = this.InitializePasswordForm();
        passwordForm.show(function (password) {
            data.openingFilePassword = password;
            jsObject.controls.processImage.show();
            jsObject.postAjax(jsObject.options.requestUrl.replace("{action}", jsObject.options.actions.openReport), data, jsObject.showReportPage);
        }, this.collections.loc["PasswordEnter"] + ":");
    }
    else {
        this.controls.processImage.show();
        this.postAjax(this.options.requestUrl.replace("{action}", this.options.actions.openReport), data, this.showReportPage);
    }
}

StiJsViewer.prototype.postPrint = function (printAction) {
    var data = {
        "action": "PrintReport",
        "printAction": printAction,
        "bookmarksPrint": this.options.appearance.bookmarksPrint
    };

    var url = this.options.requestUrl.replace("{action}", this.options.actions.printReport);
    switch (printAction) {
        case "PrintPdf":
            // Edge and Safari not able to print the hidden PDF.
            if (this.getNavigatorName() == "Edge" || this.getNavigatorName() == "Safari") this.printAsPdfPopup(data);
            // IE can not work with Blob data.
            else if (this.getNavigatorName() == "MSIE") this.printAsPdfIE(url, data);
            // Print via Blob only if the browser has the desired functionality.
            else if (window.URL || window.webkitURL) this.printAsPdf(url, data);
            // Default print via a query from the HTML form.
            else this.printAsPdfIE(url, data);
            break;

        case "PrintWithPreview":
            this.printAsPopup(data);
            break;

        case "PrintWithoutPreview":
            this.postAjax(url, data, this.printAsHtml);
            break;
    }
}

StiJsViewer.prototype.printAsPdfPopup = function (data) {
    var url = this.options.requestAbsoluteUrl.replace("{action}", this.options.actions.printReport);
    var win = this.openNewWindow("about:blank", "_blank");
    if (win != null) this.postForm(url, data, win.document);
}

StiJsViewer.prototype.printAsPdfIE = function (url, data) {
    var printFrame = document.getElementById("pdfPrintFrame");
    if (printFrame) document.body.removeChild(printFrame);

    printFrame = document.createElement("iframe");
    printFrame.id = "pdfPrintFrame";
    printFrame.name = "pdfPrintFrame";
    printFrame.width = "0";
    printFrame.height = "0";
    printFrame.style.position = "absolute";
    printFrame.style.border = "none";
    document.body.appendChild(printFrame, document.body.firstChild);

    var form = document.createElement("FORM");
    form.setAttribute("id", "printForm");
    form.setAttribute("method", "POST");
    form.setAttribute("action", url);

    var params = this.createPostParameters(data, true);
    for (var key in params) {
        var paramsField = document.createElement("INPUT");
        paramsField.setAttribute("type", "hidden");
        paramsField.setAttribute("name", key);
        paramsField.setAttribute("value", params[key]);
        form.appendChild(paramsField);
    }

    var html = "<html><body>" + form.outerHTML + "<script>setTimeout(function () { document.getElementById('printForm').submit(); });</script></body></html>";
    printFrame.contentWindow.document.open("application/pdf");
    printFrame.contentWindow.document.write(html);
    printFrame.contentWindow.document.close();
}

StiJsViewer.prototype.printAsPdf = function (url, data) {
    data.responseType = "blob";
    this.postAjax(url, data, function (data, jsObject) {
        var printFrame = document.getElementById("pdfPrintFrame");
        if (printFrame) document.body.removeChild(printFrame);

        printFrame = document.createElement("iframe");
        printFrame.id = "pdfPrintFrame";
        printFrame.name = "pdfPrintFrame";
        printFrame.width = "0";
        printFrame.height = "0";
        printFrame.style.position = "absolute";
        printFrame.style.border = "none";
        document.body.appendChild(printFrame, document.body.firstChild);
        printFrame.src = (window.URL ? URL : webkitURL).createObjectURL(data);
    });
}

StiJsViewer.prototype.printAsPopup = function (data) {
    var url = this.options.requestAbsoluteUrl.replace("{action}", this.options.actions.printReport);
    var win = this.openNewWindow("about:blank", "PrintReport", "height=900, width=790, toolbar=no, menubar=yes, scrollbars=yes, resizable=yes, location=no, directories=no, status=no");
    if (win != null) this.postForm(url, data, win.document);
}

StiJsViewer.prototype.printAsHtml = function (text, jsObject) {
    if (jsObject.showError(text)) return;

    // Delete pdfPrintFrame, this is fix IE strange error
    var printFrame = document.getElementById("pdfPrintFrame");
    if (printFrame) document.body.removeChild(printFrame);
    
    printFrame = document.getElementById("htmlPrintFrame");
    if (printFrame) document.body.removeChild(printFrame);
    
    printFrame = document.createElement("iframe");
    printFrame.id = "htmlPrintFrame";
    printFrame.name = "htmlPrintFrame";
    printFrame.width = "0px";
    printFrame.height = "0px";
    printFrame.style.position = "absolute";
    printFrame.style.border = "none";
    document.body.appendChild(printFrame, document.body.firstChild);

    printFrame.contentWindow.document.open();
    printFrame.contentWindow.document.write(text);
    printFrame.contentWindow.document.close();
    setTimeout(function () {
        printFrame.contentWindow.focus();
        printFrame.contentWindow.print();
    });
}

StiJsViewer.prototype.postExport = function (format, settings) {
    var data = {
        action: "ExportReport",
        exportFormat: format,
        exportSettings: settings
    };

    var doc = settings.OpenAfterExport && this.options.appearance.openExportedReportWindow == "_blank" ? this.openNewWindow("about:blank", "_blank").document : null;
    var url = doc ? this.options.requestAbsoluteUrl : this.options.requestUrl;
    this.postForm(url.replace("{action}", this.options.actions.exportReport), data, doc);
}

StiJsViewer.prototype.postEmail = function (format, settings) {
    var data = {
        action: "EmailReport",
        exportFormat: format,
        exportSettings: settings
    };

    this.controls.processImage.show();
    this.postAjax(this.options.requestUrl.replace("{action}", this.options.actions.emailReport), data, this.emailResult);
}

StiJsViewer.prototype.postDesign = function () {
    var doc = this.options.appearance.designWindow == "_blank" ? this.openNewWindow("about:blank", "_blank").document : null;
    var url = doc ? this.options.requestAbsoluteUrl : this.options.requestUrl;
    this.postForm(url.replace("{action}", this.options.actions.designReport), { action: "DesignReport" }, doc);
}

StiJsViewer.prototype.postInteraction = function (params) {
    if (!this.options.actions.interaction) {
        if (this.controls.buttons["Parameters"]) this.controls.buttons["Parameters"].setEnabled(false);
        return;
    }

    // Add new drill-down parameters to drill-down queue and calc guid
    if (params.action != "InitVars" && params.action == "DrillDown") {
        params.drillDownParameters = this.reportParams.drillDownParameters.concat(params.drillDownParameters);
        params.drillDownGuid = hex_md5(JSON.stringify(params.drillDownParameters));
    }

    this.controls.processImage.show();
    this.postAjax(
        this.options.requestUrl.replace("{action}", this.options.actions.interaction),
        params,
        params.action == "InitVars" ? this.showParametersPanel : this.showReportPage
    );
}

StiJsViewer.prototype.postReportResource = function (resourceName, viewType) {
    var data = {
        action: "ReportResource",
        reportResourceParams: {
            resourceName: resourceName,
            viewType: viewType
        }
    };

    var doc = viewType == "View" ? this.openNewWindow("about:blank", "_blank").document : null;
    var url = doc ? this.options.requestAbsoluteUrl : this.options.requestUrl;
    this.postForm(url.replace("{action}", this.options.actions.viewerEvent), data, doc);
}

StiJsViewer.prototype.initAutoUpdateCache = function (jsText, jsObject) {
    if (jsObject.options.server.allowAutoUpdateCache) {
        if (jsObject.controls.timerAutoUpdateCache) clearTimeout(jsObject.controls.timerAutoUpdateCache);
        jsObject.controls.timerAutoUpdateCache = setTimeout(function () {
            jsObject.postAjax(jsObject.options.requestUrl.replace("{action}", jsObject.options.actions.viewerEvent), { action: "UpdateCache" }, jsObject.initAutoUpdateCache);
        }, jsObject.options.server.timeoutAutoUpdateCache);
    }
}

StiJsViewer.prototype.emailResult = function (jsText, jsObject) {
    jsObject.controls.processImage.hide();
    if (jsText == "0")
        alert(jsObject.collections.loc["EmailSuccessfullySent"]);
    else {
        if (jsText.indexOf("<?xml") == 0) {
            alert(jsObject.GetXmlValue(jsText, "ErrorCode"));
            alert(jsObject.GetXmlValue(jsText, "ErrorDescription"));
        }
        else
            alert(jsText);
    }
}

StiJsViewer.prototype.parseParameters = function (parameters) {
    // Add first report
    var drillDownPanel = this.controls.drillDownPanel;
    if (drillDownPanel.buttonsRow.children.length == 0) drillDownPanel.addButton(parameters.reportFileName, this.reportParams);

    // Add or show drill-down report
    if (parameters.action == "DrillDown") {
        drillDownPanel.changeVisibleState(true);
        var buttonExist = false;
        for (var name in drillDownPanel.buttons) {
            var button = drillDownPanel.buttons[name];
            if (button.reportParams.drillDownGuid == parameters.drillDownGuid) {
                buttonExist = true;
                button.style.display = "inline-block";
                button.select();
                break;
            }
        }
        if (!buttonExist) {
            this.controls.drillDownPanel.addButton(parameters.reportFileName);
            this.reportParams.drillDownParameters = parameters.drillDownParameters;
            this.reportParams.pageNumber = 0;
            this.reportParams.pagesWidth = 0;
            this.reportParams.pagesHeight = 0;
        }

        this.controls.reportPanel.scrollTop = 0;
    }

    // Apply report parameters
    this.reportParams.pagesArray = parameters.pagesArray;

    if (parameters.action != "GetReport" || parameters.action != "OpenReport") {
        this.addCustomFontStyles(parameters.customFonts)
    }

    if (parameters.action != "GetPages") {
        this.reportParams.drillDownGuid = parameters.drillDownGuid;
        this.reportParams.pagesCount = parameters.pagesCount;
        if (parameters.pageNumber != null) this.reportParams.pageNumber = parameters.pageNumber;
        this.reportParams.zoom = parameters.zoom;
        this.reportParams.viewMode = parameters.viewMode;
        this.reportParams.reportFileName = parameters.reportFileName;
        this.reportParams.interactionCollapsingStates = parameters.interactionCollapsingStates;
        if (parameters.bookmarksContent) this.reportParams.bookmarksContent = parameters.bookmarksContent;
        if (parameters.resources) this.reportParams.resources = parameters.resources;
        if (parameters.cloudTrialMode) this.reportParams.cloudTrialMode = parameters.cloudTrialMode;
        if (this.controls.buttons.Editor) this.controls.buttons.Editor.style.display = parameters.isEditableReport ? "" : "none";

        this.stopRefreshReportTimer();
        if (parameters.reportRefreshTime && parseInt(parameters.reportRefreshTime) > 0) this.startRefreshReportTimer(parameters.reportRefreshTime);
    }

    return parameters;
}

StiJsViewer.prototype.showParametersPanel = function (jsText, jsObject) {
    if (jsObject.showError(jsText)) jsText = null;

    jsObject.options.isParametersReceived = true;
    var paramsProps = typeof jsText == "string" ? JSON.parse(jsText) : jsText;
    
    jsObject.options.paramsVariables = paramsProps;
    jsObject.InitializeParametersPanel();
    jsObject.controls.processImage.hide();
}

StiJsViewer.prototype.parseCloudParameters = function (parameters) {
    if (parameters.maxReportPages) this.showError(
        {
            success: false,
            type: "Warning",
            text: this.collections.loc["QuotaMaximumReportPagesCountExceeded"] + "\n" + this.collections.loc["Maximum"] + " " + this.numberWithSpaces(parameters.maxReportPages) + "."
        });

    if (parameters.maxDataRows) this.showError(
        {
            success: false,
            type: "Warning",
            text: this.collections.loc["QuotaMaximumDataRowsCountExceeded"] + "\n" + this.collections.loc["Maximum"] + " " + this.numberWithSpaces(parameters.maxDataRows) + "."
        });
}

StiJsViewer.prototype.showReportPage = function (data, jsObject) {
    // If report not found, try to get the report again
    if (data == "ServerError:The report is not specified." && jsObject.options.isReportRecieved) {
        jsObject.options.isReportRecieved = false;
        jsObject.postAction("GetReport");
        return;
    }

    jsObject.controls.processImage.hide();
    jsObject.options.isReportRecieved = true;
    
    if (jsObject.showError(data)) return; // Check for error

    if (jsObject.options.server.useCompression) {
        data = StiGZipHelper.unpack(data);
        if (jsObject.showError(data)) return; // Check for error unpacked data
    }
    
    // Get JSON data object and check for error in JSON format
    var json = (typeof (data) == "string" && data.substr(0, 1) == "{") ? JSON.parse(data) : data;
    if (jsObject.showError(json)) return;

    // Try to parse parameters
    var parameters = jsObject.parseParameters(json);
    if (parameters == null) return;

    // Parse cloud parameters
    jsObject.parseCloudParameters(json);

    // Init viewer panels
    if (parameters.bookmarksContent) jsObject.InitializeBookmarksPanel();
    if (jsObject.controls.resourcesPanel) jsObject.controls.resourcesPanel.update();
    if (parameters.pagesArray) jsObject.controls.reportPanel.addPages();

    if (jsObject.controls.toolbar) {
        jsObject.controls.toolbar.changeToolBarState();
        jsObject.controls.toolbar.setEnabled(true);
        if (jsObject.controls.navigatePanel) jsObject.controls.navigatePanel.setEnabled(true);
    }

    // Check for auto zoom by page width or page height
    if (jsObject.reportParams.autoZoom != null) {
        jsObject.postAction(jsObject.reportParams.autoZoom == -1 ? "ZoomPageWidth" : "ZoomOnePage");
        delete jsObject.reportParams.autoZoom;
    }

    // Go to the bookmark, if it present
    if (jsObject.options.bookmarkAnchor != null) {
        jsObject.scrollToAnchor(jsObject.options.bookmarkAnchor, jsObject.options.componentGuid);
        jsObject.options.bookmarkAnchor = null;
        jsObject.options.componentGuid = null;
    }

    // Find text in the report
    if (jsObject.options.findMode && jsObject.controls.findPanel) {
        jsObject.showFindLabels(jsObject.controls.findPanel.controls.findTextBox.value);
    }

    // Get the request from user variables when the viewer is launched for the first time
    if (!jsObject.options.isParametersReceived && jsObject.options.toolbar.showParametersButton) jsObject.postInteraction({ action: "InitVars" });

    // Re-init auto-update report cache
    jsObject.initAutoUpdateCache(null, jsObject);
}

StiJsViewer.prototype.startRefreshReportTimer = function (timeout) {
    if (this.service.refreshReportTimer != null)
        clearInterval(this.service.refreshReportTimer);

    var jsObject = this;
    this.service.refreshReportTimer = setInterval(function () {
        if (!jsObject.service.isRequestInProcess)
            jsObject.postAction("Refresh");
    }, timeout * 1000);
}

StiJsViewer.prototype.stopRefreshReportTimer = function () {
    if (this.service.refreshReportTimer != null) {
        clearInterval(this.service.refreshReportTimer);
        this.service.refreshReportTimer = null;
    }
}
