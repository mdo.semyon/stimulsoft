
StiJsViewer.prototype.scrollToAnchor = function (anchorName, componentGuid) {
    var identicalAnchors = [];
    
    if (componentGuid) {
        for (var i = 0; i < document.anchors.length; i++) {
            if (document.anchors[i].getAttribute("guid") == componentGuid)
                identicalAnchors.push(document.anchors[i]);
        }
    }

    if (identicalAnchors.length == 0) {
        for (var i = 0; i < document.anchors.length; i++) {
            if (document.anchors[i].name == anchorName)
                identicalAnchors.push(document.anchors[i]);
        }
    }

    if (identicalAnchors.length > 0) {
        var jsObject = this;

        var anchor = identicalAnchors[0];
        var anchorParent = anchor.parentElement || anchor;
        var anchorHeight = anchorParent.offsetHeight;
        var anchorOffsetTop = anchorParent.offsetTop;
        for (var i = 0; i < identicalAnchors.length; i++) {
            var nextAnchorParent = identicalAnchors[i].parentElement || identicalAnchors[i];
            if (nextAnchorParent.offsetTop > anchorOffsetTop)
                anchorHeight = Math.max(anchorHeight, nextAnchorParent.offsetTop - anchorOffsetTop + nextAnchorParent.offsetHeight);
        }

        var date = new Date();
        var endTime = date.getTime() + this.options.scrollDuration;
        var targetTop = this.FindPosY(anchor, this.options.appearance.scrollbarsMode ? "stiJsViewerReportPanel" : null, true) - anchorParent.offsetHeight * 2;

        this.ShowAnimationForScroll(this.controls.reportPanel, targetTop, endTime, function () {
            var page = jsObject.getPageFromAnchorElement(anchor);
            var anchorParentTopPos = jsObject.FindPosY(anchorParent, "stiJsViewerReportPanel", true);
            var pageTopPos = page ? jsObject.FindPosY(page, "stiJsViewerReportPanel", true) : anchorParentTopPos;

            jsObject.removeBookmarksLabel();

            var label = document.createElement("div");
            jsObject.controls.bookmarksLabel = label;
            label.className = "stiJsViewerBookmarksLabel";

            var labelMargin = 20 * (jsObject.reportParams.zoom / 100);
            var labelWidth = page ? page.offsetWidth - labelMargin - 6 : anchorParent.offsetWidth;
            var labelHeight = anchorHeight - 2;
            label.style.width = labelWidth + "px";
            label.style.height = labelHeight + "px";

            var pageLeftMargin = page.margins ? jsObject.StrToInt(page.margins[3]) : 0;
            var pageTopMargin = page.margins ? jsObject.StrToInt(page.margins[0]) : 0;
            label.style.marginLeft = (labelMargin / 2 - pageLeftMargin) + "px";
            label.style.marginTop = (anchorParentTopPos - pageTopPos - pageTopMargin - (jsObject.reportParams.zoom / 100) - 1) + "px";

            page.insertBefore(label, page.childNodes[0]);
        });
    }
}

StiJsViewer.prototype.isWholeWord = function (str, word) {
    var symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    var index = str.indexOf(word);
    var preSymbol = str.substring(index - 1, index);
    var nextSymbol = str.substring(index + word.length, index + word.length + 1);

    return ((preSymbol == "" || symbols.indexOf(preSymbol) == -1) && (nextSymbol == "" || symbols.indexOf(nextSymbol) == -1));
}

StiJsViewer.prototype.goToFindedElement = function (findLabel) {
    if (findLabel && findLabel.ownerElement) {
        var targetTop = this.FindPosY(findLabel.ownerElement, this.options.appearance.scrollbarsMode ? "stiJsViewerReportPanel" : null, true) - findLabel.ownerElement.offsetHeight - 50;
        var d = new Date();
        var endTime = d.getTime() + this.options.scrollDuration;
        var this_ = this;
        this.ShowAnimationForScroll(this.controls.reportPanel, targetTop, endTime, function () { });
    }
}

StiJsViewer.prototype.hideFindLabels = function () {
    for (var i = 0; i < this.controls.findHelper.findLabels.length; i++) {
        this.controls.findHelper.findLabels[i].parentElement.removeChild(this.controls.findHelper.findLabels[i]);
    }
    this.controls.findHelper.findLabels = [];
    this.options.findMode = false;
}

StiJsViewer.prototype.showFindLabels = function (text) {
    this.hideFindLabels();
    this.options.findMode = true;
    this.options.changeFind = false;
    this.controls.findHelper.lastFindText = text;
    var matchCase = this.controls.findPanel && this.controls.findPanel.controls.matchCase.isSelected;
    var matchWholeWord = this.controls.findPanel && this.controls.findPanel.controls.matchWholeWord.isSelected;
    var pages = this.controls.reportPanel.pages;

    for (var i = 0; i < pages.length; i++) {
        var page = pages[i];
        var pageElements = page.getElementsByTagName('*');
        for (k = 0; k < pageElements.length; k++) {
            var innerText = pageElements[k].innerHTML;
            if (innerText && pageElements[k].childNodes.length == 1 && pageElements[k].childNodes[0].nodeName == "#text") {
                if (!matchCase) {
                    innerText = innerText.toLowerCase();
                    text = text.toLowerCase();
                }
                if (innerText.indexOf(text) >= 0) {
                    if (matchWholeWord && !this.isWholeWord(innerText, text)) {
                        continue;
                    }
                    var label = document.createElement("div");
                    label.ownerElement = pageElements[k];
                    label.className = "stiJsViewerFindLabel";
                    label.style.width = (pageElements[k].offsetWidth - 4) + "px";
                    var labelHeight = pageElements[k].offsetHeight - 4;
                    label.style.height = labelHeight + "px";
                    label.style.marginTop = (-4 * (this.reportParams.zoom / 100)) + "px";
                    pageElements[k].insertBefore(label, pageElements[k].childNodes[0]);

                    label.setSelected = function (state) {
                        this.isSelected = state;
                        this.style.border = "2px solid " + (state ? "red" : "#8a8a8a");
                    }

                    if (this.controls.findHelper.findLabels.length == 0) label.setSelected(true);
                    this.controls.findHelper.findLabels.push(label);
                }
            }
        }
    }

    if (this.controls.findHelper.findLabels.length > 0) this.goToFindedElement(this.controls.findHelper.findLabels[0]);
}

StiJsViewer.prototype.selectFindLabel = function (direction) {
    var labels = this.controls.findHelper.findLabels;
    if (labels.length == 0) return;
    var selectedIndex = 0;
    for (var i = 0; i < labels.length; i++) {
        if (labels[i].isSelected) {
            labels[i].setSelected(false);
            selectedIndex = i;
            break;
        }
    }
    if (direction == "Next") {
        selectedIndex++;
        if (selectedIndex > labels.length - 1) selectedIndex = 0;
    }
    else {
        selectedIndex--;
        if (selectedIndex < 0) selectedIndex = labels.length - 1;
    }
    labels[selectedIndex].setSelected(true);
    this.goToFindedElement(labels[selectedIndex]);
}

StiJsViewer.prototype.scrollToPage = function (pageNumber) {
    var commonPagesHeight = 0;
    for (i = 0; i < pageNumber; i++) {
        commonPagesHeight += this.controls.reportPanel.pages[i].offsetHeight + 20;
    }
    if (!this.options.appearance.scrollbarsMode) commonPagesHeight += this.FindPosY(this.controls.reportPanel, null, true);

    var d = new Date();
    var endTime = d.getTime() + this.options.scrollDuration;
    this.ShowAnimationForScroll(this.controls.reportPanel, commonPagesHeight, endTime);
}

StiJsViewer.prototype.removeBookmarksLabel = function () {
    if (this.controls.bookmarksLabel) {
        this.controls.bookmarksLabel.parentElement.removeChild(this.controls.bookmarksLabel);
        this.controls.bookmarksLabel = null;
    }
}

StiJsViewer.prototype.getPageFromAnchorElement = function (anchorElement) {
    var obj = anchorElement;
    while (obj.parentElement) {
        if (obj.className && obj.className.indexOf("stiJsViewerPage") == 0) {
            return obj;
        }
        obj = obj.parentElement;
    }
    return obj;
}

StiJsViewer.prototype.isContainted = function (array, item) {
    for (var index in array)
        if (item == array[index]) return true;

    return false;
}

StiJsViewer.prototype.IsTouchDevice = function () {
    return ('ontouchstart' in document.documentElement);
}

StiJsViewer.prototype.IsMobileDevice = function () {
    return /iPhone|iPad|iPod|Android|BlackBerry|IEMobile/i.test(navigator.userAgent);
}

StiJsViewer.prototype.SetZoom = function (zoomIn) {
    zoomValues = ["25", "50", "75", "100", "150", "200"];

    for (var i = 0; i < zoomValues.length; i++)
        if (zoomValues[i] == this.reportParams.zoom) break;

    if (zoomIn && i < zoomValues.length - 1) this.postAction("Zoom" + zoomValues[i + 1]);
    if (!zoomIn && i > 0) this.postAction("Zoom" + zoomValues[i - 1]);
}

StiJsViewer.prototype.getCssParameter = function (css) {
    if (css.indexOf(".gif]") > 0 || css.indexOf(".png]") > 0) return css.substr(css.indexOf("["), css.indexOf("]") - css.indexOf("[") + 1);
    return null;
}

StiJsViewer.prototype.newGuid = (function () {
    var CHARS = '0123456789abcdefghijklmnopqrstuvwxyz'.split('');
    return function (len, radix) {
        var chars = CHARS, uuid = [], rnd = Math.random;
        radix = radix || chars.length;

        if (len) {
            for (var i = 0; i < len; i++) uuid[i] = chars[0 | rnd() * radix];
        } else {
            var r;
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4';

            for (var i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    r = 0 | rnd() * 16;
                    uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
                }
            }
        }

        return uuid.join('');
    };
})();

StiJsViewer.prototype.generateKey = function () {
    return this.newGuid().replace(/-/g, '');
}

StiJsViewer.prototype.Item = function (name, caption, imageName, key) {
    var item = {
        "name": name,
        "caption": caption,
        "imageName": imageName,
        "key": key
    }

    return item;
}

StiJsViewer.prototype.StrToInt = function (value) {   
    var result = parseInt(value);
    if (result) return result;
    return 0;
}

StiJsViewer.prototype.formatDate = function (formatDate, formatString) {
    var yyyy = formatDate.getFullYear();
    var yy = yyyy.toString().substring(2);
    var m = formatDate.getMonth() + 1;
    var mm = m < 10 ? "0" + m : m;
    var d = formatDate.getDate();
    var dd = d < 10 ? "0" + d : d;

    var h = formatDate.getHours();
    var hh = h < 10 ? "0" + h : h;
    var h12 = h > 12 ? h - 12 : (h > 0 ? h : 12);
    var hh12 = h12 < 10 ? "0" + h12 : h12;
    var n = formatDate.getMinutes();
    var nn = n < 10 ? "0" + n : n;
    var s = formatDate.getSeconds();
    var ss = s < 10 ? "0" + s : s;
    var tt = h < 12 ? "AM" : "PM";

    formatString = formatString.replace(/yyyy/gi, yyyy);
    formatString = formatString.replace(/yy/gi, yy);
    formatString = formatString.replace(/Y/, yyyy);
    formatString = formatString.replace(/MM/g, mm);
    formatString = formatString.replace(/M/g, m);
    formatString = formatString.replace(/dd/g, dd);
    formatString = formatString.replace(/d/g, d);
    formatString = formatString.replace(/hh/g, hh12);
    formatString = formatString.replace(/h/g, h12);
    formatString = formatString.replace(/HH/g, hh);
    formatString = formatString.replace(/H/g, h);
    formatString = formatString.replace(/mm/g, nn);
    formatString = formatString.replace(/m/g, n);
    formatString = formatString.replace(/ss/g, ss);
    formatString = formatString.replace(/s/g, s);
    formatString = formatString.replace(/tt/gi, tt);
    formatString = formatString.replace(/t/gi, tt.substr(0, 1));

    return formatString;
}

StiJsViewer.prototype.stringToTime = function (timeStr) {
    var timeArray = timeStr.split(":");
    var time = { hours: 0, minutes: 0, seconds: 0 };

    time.hours = this.StrToInt(timeArray[0]);
    if (timeArray.length > 1) time.minutes = this.StrToInt(timeArray[1]);
    if (timeArray.length > 2) time.seconds = this.StrToInt(timeArray[2]);

    if (time.hours < 0) time.hours = 0;
    if (time.minutes < 0) time.minutes = 0;
    if (time.seconds < 0) time.seconds = 0;

    if (time.hours > 23) time.hours = 23;
    if (time.minutes > 59) time.minutes = 59;
    if (time.seconds > 59) time.seconds = 59;

    return time;
}

StiJsViewer.prototype.dateTimeObjectToString = function (dateTimeObject, typeDateTimeObject) {    
    var date = new Date(dateTimeObject.year, dateTimeObject.month - 1, dateTimeObject.day, dateTimeObject.hours, dateTimeObject.minutes, dateTimeObject.seconds);
    if (this.options.appearance.parametersPanelDateFormat != "")
        return this.formatDate(date, this.options.appearance.parametersPanelDateFormat);

    return this.DateToLocaleString(date, typeDateTimeObject);
}

StiJsViewer.prototype.getStringKey = function (key, parameter) {
    var stringKey = (parameter.params.type == "DateTime")
        ? this.dateTimeObjectToString(key, parameter.params.dateTimeType)
        : key;

    return stringKey;
}

StiJsViewer.prototype.getCountObjects = function (objectArray) {
    var count = 0;
    if (objectArray)
        for (var singleObject in objectArray) { count++ };
    return count;
}

StiJsViewer.prototype.getDateTimeObject = function (date) {
    if (!date) date = new Date();
    dateTimeObject = {};
    dateTimeObject.year = date.getFullYear();
    dateTimeObject.month = date.getMonth() + 1;
    dateTimeObject.day = date.getDate();
    dateTimeObject.hours = date.getHours();
    dateTimeObject.minutes = date.getMinutes();
    dateTimeObject.seconds = date.getSeconds();

    return dateTimeObject;
}

StiJsViewer.prototype.getNowTimeSpanObject = function () {
    date = new Date();
    timeSpanObject = {};
    timeSpanObject.hours = date.getHours();
    timeSpanObject.minutes = date.getMinutes();
    timeSpanObject.seconds = date.getSeconds();

    return timeSpanObject;
}

StiJsViewer.prototype.copyObject = function (o) {
    if (!o || "object" !== typeof o) {
        return o;
    }
    var c = "function" === typeof o.pop ? [] : {};
    var p, v;
    for (p in o) {
        if (o.hasOwnProperty(p)) {
            v = o[p];
            if (v && "object" === typeof v) {
                c[p] = this.copyObject(v);
            }
            else c[p] = v;
        }
    }
    return c;
}

StiJsViewer.prototype.getNavigatorName = function () {
    if (!navigator) return "Unknown";
    var userAgent = navigator.userAgent;

    if (userAgent.indexOf("Edge") >= 0) return "Edge";
    if (userAgent.indexOf('MSIE') >= 0 || userAgent.indexOf('Trident') >= 0) return "MSIE";
    if (userAgent.indexOf('Gecko') >= 0) {
        if (userAgent.indexOf('Chrome') >= 0) return "Chrome";
        if (userAgent.indexOf('Safari') >= 0) return "Safari";
        return "Mozilla";
    }
    if (userAgent.indexOf('Opera') >= 0) return "Opera";
    return "Unknown";
}

StiJsViewer.prototype.showHelpWindow = function (url) {
    var helpLanguage;
    switch (this.options.cultureName) {
        case "ru": helpLanguage = "ru";
        //case "de": helpLanguage = "de";
        default: helpLanguage = "en";
    }
    this.openNewWindow("http://www.stimulsoft.com/" + helpLanguage + "/documentation/online/" + url);
}

StiJsViewer.prototype.setObjectToCenter = function (object, defaultTop) {
    var leftPos = (this.controls.viewer.offsetWidth / 2 - object.offsetWidth / 2);
    var topPos = this.options.appearance.fullScreenMode ? (this.controls.viewer.offsetHeight / 2 - object.offsetHeight / 2) : (defaultTop ? defaultTop : 250);
    object.style.left = leftPos > 0 ? leftPos + "px" : 0;
    object.style.top = topPos > 0 ? topPos + "px" : 0;
}

StiJsViewer.prototype.strToInt = function (value) {
    var result = parseInt(value);
    if (result) return result;
    return 0;
}

StiJsViewer.prototype.strToCorrectPositiveInt = function (value) {
    var result = this.strToInt(value);
    if (result >= 0) return result;
    return 0;
}

StiJsViewer.prototype.helpLinks = {
    "Print": "user-manual/index.html?viewing_reports_basic_toolbar_of_report_viewer.htm",
    "Save": "user-manual/index.html?viewing_reports_basic_toolbar_of_report_viewer.htm",
    "SendEmail": "user-manual/index.html?viewing_reports_sending_report_via_e-mail.htm",
    "Bookmarks": "user-manual/index.html?viewing_reports_basic_toolbar_of_report_viewer.htm",
    "Parameters": "user-manual/index.html?viewing_reports_basic_toolbar_of_report_viewer.htm",
    "FirstPage": "user-manual/index.html?viewing_reports_page_navigation.htm",
    "PrevPage": "user-manual/index.html?viewing_reports_page_navigation.htm",
    "NextPage": "user-manual/index.html?viewing_reports_page_navigation.htm",
    "LastPage": "user-manual/index.html?viewing_reports_page_navigation.htm",
    "FullScreen": "user-manual/index.html?viewing_reports_basic_toolbar_of_report_viewer.htm",
    "Zoom": "user-manual/index.html?viewing_reports_basic_toolbar_of_report_viewer.htm",
    "ViewMode": "user-manual/index.html?viewing_reports_displaying_mode.htm",
    "Editor": "user-manual/index.html?viewing_reports_basic_toolbar_of_report_viewer.htm",
    "Find": "user-manual/index.html?viewing_reports_search_panel.htm"
}

StiJsViewer.prototype.getHTMLColor = function (color) {
    if (color.indexOf(",") > 0) return "rgb(" + color + ")";
    return color;
}

StiJsViewer.prototype.clearStyles = function (object) {
    object.className = "stiJsViewerClearAllStyles";
}

StiJsViewer.prototype.getDefaultExportSettings = function (exportFormat) {
    var exportSettings = null;
    switch (exportFormat) {
        case "Document": { exportSettings = {}; break; }
        case "Pdf": { exportSettings = this.options.exports.defaultSettings["StiPdfExportSettings"]; break; }
        case "Xps": { exportSettings = this.options.exports.defaultSettings["StiXpsExportSettings"]; break; }
        case "Ppt2007": { exportSettings = this.options.exports.defaultSettings["StiPpt2007ExportSettings"]; break; }
        case "Html":
        case "Html5":
        case "Mht": { exportSettings = this.options.exports.defaultSettings["StiHtmlExportSettings"]; break; }
        case "Text": { exportSettings = this.options.exports.defaultSettings["StiTxtExportSettings"]; break; }
        case "Rtf": { exportSettings = this.options.exports.defaultSettings["StiRtfExportSettings"]; break; }
        case "Word2007": { exportSettings = this.options.exports.defaultSettings["StiWord2007ExportSettings"]; break; }
        case "Odt": { exportSettings = this.options.exports.defaultSettings["StiOdtExportSettings"]; break; }
        case "Excel":
        case "ExcelXml":
        case "Excel2007": { exportSettings = this.options.exports.defaultSettings["StiExcelExportSettings"]; break; }
        case "Ods": { exportSettings = this.options.exports.defaultSettings["StiOdsExportSettings"]; break; }
        case "ImageBmp":
        case "ImageGif":
        case "ImageJpeg":
        case "ImagePcx":
        case "ImagePng":
        case "ImageTiff":
        case "ImageSvg":
        case "ImageSvgz":
        case "ImageEmf": { exportSettings = this.options.exports.defaultSettings["StiImageExportSettings"]; break; }
        case "Xml":
        case "Csv":
        case "Dbf":
        case "Dif":
        case "Sylk": { exportSettings = this.options.exports.defaultSettings["StiDataExportSettings"]; break; }
    }

    return exportSettings;
}

StiJsViewer.prototype.changeFullScreenMode = function (fullScreenMode) {
    this.options.appearance.scrollbarsMode = fullScreenMode || this.options.appearance.userScrollbarsMode;
    this.options.appearance.fullScreenMode = fullScreenMode;
    if (this.options.toolbar.visible && this.options.toolbar.showFullScreenButton) this.controls.toolbar.controls.FullScreen.setSelected(fullScreenMode);
    this.controls.reportPanel.correctMargins();

    var toolbarHeight = this.options.toolbar.visible ? this.controls.toolbar.offsetHeight : 0;

    if (fullScreenMode) {
        this.controls.viewer.style.zIndex = "1000000";
        this.controls.viewer.style.position = "absolute";

        if (this.controls.viewer.style.width) {
            this.controls.viewer.style.userWidth = this.controls.viewer.style.width;
            this.controls.viewer.style.width = null;
        }

        if (this.controls.viewer.style.height) {
            this.controls.viewer.style.userHeight = this.controls.viewer.style.height;
            this.controls.viewer.style.height = null;
        }

        this.controls.reportPanel.style.position = "absolute";
        this.controls.reportPanel.style.top = toolbarHeight + "px";
        this.controls.reportPanel.style.height = "auto";

        document.body.prevOverflow = document.body.style.overflow;
        document.body.style.overflow = "hidden";
    }
    else {
        this.controls.viewer.style.zIndex = "auto";
        this.controls.viewer.style.position = "";
        if (this.controls.viewer.style.userWidth) this.controls.viewer.style.width = this.controls.viewer.style.userWidth;
        if (this.controls.viewer.style.userHeight) this.controls.viewer.style.height = this.controls.viewer.style.userHeight;
        this.controls.reportPanel.style.position = this.options.viewerHeightType != "Percentage" || this.options.appearance.scrollbarsMode ? "absolute" : "relative";                
        this.controls.reportPanel.style.top = this.options.toolbar.visible ? (this.options.viewerHeightType != "Percentage" || this.options.appearance.scrollbarsMode ? toolbarHeight + "px" : 0) : 0;
        if (this.options.viewerHeightType == "Percentage" && !this.options.appearance.scrollbarsMode) this.controls.reportPanel.style.height = "calc(100% - 35px)";

        if (typeof document.body.prevOverflow != "undefined") {
            document.body.style.overflow = document.body.prevOverflow;
            delete document.body.prevOverflow;
        }
    }
        
    //Correct Bookmarks & Parameters Panels 
    this.controls.reportPanel.style.marginTop =
        (this.controls.parametersPanel && this.options.appearance.parametersPanelPosition == "Top" ? this.controls.parametersPanel.offsetHeight : 0) +
        (this.controls.reportPanel.style.position == "absolute" && this.controls.drillDownPanel ? this.controls.drillDownPanel.offsetHeight : 0) +
        (this.controls.reportPanel.style.position == "absolute" && this.controls.findPanel ? this.controls.findPanel.offsetHeight : 0) +
        (this.controls.reportPanel.style.position == "absolute" && this.controls.resourcesPanel ? this.controls.resourcesPanel.offsetHeight : 0) + "px";

    var parametersPanelTopPos = toolbarHeight +
        (this.controls.drillDownPanel ? this.controls.drillDownPanel.offsetHeight : 0) +
        (this.controls.resourcesPanel ? this.controls.resourcesPanel.offsetHeight : 0) +
        (this.controls.findPanel ? this.controls.findPanel.offsetHeight : 0);

    if (this.controls.parametersPanel)
        this.controls.parametersPanel.style.top = parametersPanelTopPos + "px";

    if (this.controls.bookmarksPanel) {
        this.controls.bookmarksPanel.style.top =
            (this.controls.parametersPanel && this.options.appearance.parametersPanelPosition == "Top" ? this.controls.parametersPanel.offsetHeight : 0) +
            + parametersPanelTopPos + "px";
    }

    this.controls.reportPanel.style.overflow = this.options.appearance.scrollbarsMode ? "auto" : "hidden";
}

StiJsViewer.prototype.addEvent = function (element, eventName, fn) {
    if (element.addEventListener) element.addEventListener(eventName, fn, false);
    else if (element.attachEvent) element.attachEvent('on' + eventName, fn);
}

StiJsViewer.prototype.lowerFirstChar = function (text) {
    return text.charAt(0).toLowerCase() + text.substr(1);
}

StiJsViewer.prototype.addHoverEventsToMenus = function () {
    if (this.options.toolbar.showMenuMode == "Hover") {
        var buttonsWithMenu = ["Print", "Save", "SendEmail", "Zoom", "ViewMode"];
        for (var i = 0; i < buttonsWithMenu.length; i++) {
            var button = this.controls.toolbar.controls[buttonsWithMenu[i]];
            if (button) {
                var menu = this.controls.menus[this.lowerFirstChar(button.name) + "Menu"];
                if (menu) {
                    menu.buttonName = button.name;

                    menu.onmouseover = function () {
                        clearTimeout(this.jsObject.options.toolbar["hideTimer" + this.buttonName + "Menu"]);
                    }

                    menu.onmouseout = function () {
                        var thisMenu = this;
                        this.jsObject.options.toolbar["hideTimer" + this.buttonName + "Menu"] = setTimeout(function () {
                            thisMenu.changeVisibleState(false);
                        }, this.jsObject.options.menuHideDelay);
                    }
                }
            }
        }
    }
}

StiJsViewer.prototype.GetXmlValue = function (xml, key) {
    var string = xml.substr(0, xml.indexOf("</" + key + ">"));
    return string.substr(xml.indexOf("<" + key + ">") + key.length + 2);
}

StiJsViewer.prototype.DateToLocaleString = function (date, dateTimeType) {
    var timeString = date.toLocaleTimeString();
    var isAmericanFormat = timeString.toLowerCase().indexOf("am") >= 0 || timeString.toLowerCase().indexOf("pm") >= 0;
    var formatDate = isAmericanFormat ? "MM/dd/yyyy" : "dd.MM.yyyy";

    var yyyy = date.getFullYear();
    var yy = yyyy.toString().substring(2);
    var M = date.getMonth() + 1;
    var MM = M < 10 ? "0" + M : M;
    var d = date.getDate();
    var dd = d < 10 ? "0" + d : d;

    formatDate = formatDate.replace(/yyyy/i, yyyy);
    formatDate = formatDate.replace(/yy/i, yy);
    formatDate = formatDate.replace(/MM/i, MM);
    formatDate = formatDate.replace(/M/i, M);
    formatDate = formatDate.replace(/dd/i, dd);
    formatDate = formatDate.replace(/d/i, d);

    var h = date.getHours();
    var tt = "";

    if (isAmericanFormat) {
        tt = h < 12 ? " AM" : " PM";
        h = h > 12 ? h - 12 : h;
        if (h == 0) h = 12;
    }
    else
        h < 10 ? "0" + h : h;

    var m = date.getMinutes();
    m = m < 10 ? "0" + m : m;
    var s = date.getSeconds();
    s = s < 10 ? "0" + s : s;

    var formatTime = h + ":" + m + ":" + s + tt;

    if (dateTimeType == "Time") return formatTime;
    if (dateTimeType == "Date") return formatDate;
    return formatDate + " " + formatTime;
}

StiJsViewer.prototype.UpdateAllHyperLinks = function () {
    if (this.reportParams.viewMode != "SinglePage") return;
    var aHyperlinks = this.controls.reportPanel.getElementsByTagName("a");

    if (this.controls.bookmarksPanel) {
        var aBookmarks = this.controls.bookmarksPanel.getElementsByTagName("a");

        for (var i = 0; i < aHyperlinks.length; i++) {
            if (aHyperlinks[i].getAttribute("href")) {
                aHyperlinks[i].anchorName = aHyperlinks[i].getAttribute("href").replace("#", "");

                aHyperlinks[i].onclick = function () {
                    for (var k = 0; k < aBookmarks.length; k++) {
                        var clickFunc = aBookmarks[k].getAttribute("onclick");
                        if (clickFunc && clickFunc.indexOf("'" + decodeURI(this.anchorName) + "'") > 0) {
                            try {
                                eval(clickFunc);
                                return false;
                            }
                            catch (e) { }
                        }
                    }
                }
            }
        }
    }
}

StiJsViewer.prototype.openNewWindow = function (url, name, specs) {
    var win = window.open(url, name, specs);
    return win;
}

StiJsViewer.prototype.SetCookie = function (name, value, path, domain, secure) {
    var pathName = location.pathname;
    var expDate = new Date();
    expDate.setTime(expDate.getTime() + (365 * 24 * 3600 * 1000));//TODO Setup period
    document.cookie = name + "=" + escape(value) +
        "; expires=" + expDate.toGMTString() +
        ((path) ? "; path=" + path : "; path=/") +
        ((domain) ? "; domain=" + pathName.substring(0, pathName.lastIndexOf('/')) + '/' : "") +
        ((secure) ? "; secure" : "");
}

StiJsViewer.prototype.GetCookie = function (name) {
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset)
            if (end == -1) {
                end = cookie.length;
            }
            setStr = unescape(cookie.substring(offset, end));
        }
    }
    return (setStr);
}

StiJsViewer.prototype.numberWithSpaces = function (x) {
    if (x == null) return "";
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

StiJsViewer.prototype.GetHumanFileSize = function (value, decimals) {
    var i = Math.floor(Math.log(value) / Math.log(1024));
    return ((value / Math.pow(1024, i)).toFixed(decimals) * 1) + ' ' + ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'][i];
}

StiJsViewer.prototype.addCustomFontStyles = function (customFonts) {
    if (!customFonts) return;

    for (var i = 0; i < customFonts.length; i++) {
        if (this.controls.head && customFonts[i].contentForCss && customFonts[i].originalFontFamily) {
            var style = document.createElement("style");
            var cssText = "@font-face {\r\n" +
                "font-family: '" + customFonts[i].originalFontFamily + "';\r\n" +
                "src: url(" + customFonts[i].contentForCss + ");\r\n }";

            style.innerHTML = cssText;
            this.controls.head.appendChild(style);
        }
    }
}