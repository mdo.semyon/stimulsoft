﻿
StiJsViewer.prototype.InitializeParametersPanel = function () {
    var createAndShow = !this.options.isMobileDevice;
    if (this.controls.parametersPanel) {
        createAndShow = this.controls.parametersPanel.visible;
        this.controls.parametersPanel.isUpdatePanel = true;
        this.controls.parametersPanel.changeVisibleState(false);
        this.controls.mainPanel.removeChild(this.controls.parametersPanel);
        delete this.controls.parametersPanel;
    }
    if (this.options.toolbar.visible && this.options.toolbar.showParametersButton) {
        this.controls.toolbar.controls.Parameters.setEnabled(this.options.paramsVariables != null);
    }
    if (this.options.paramsVariables == null) return;

    var parametersPanel = document.createElement("div");
    parametersPanel.menus = {};
    this.controls.parametersPanel = parametersPanel;
    this.controls.mainPanel.appendChild(parametersPanel);

    parametersPanel.className = "stiJsViewerParametersPanel";

    if (this.options.appearance.parametersPanelPosition == "Top") {
        parametersPanel.className += " stiJsViewerParametersPanelTop";

        if (this.options.toolbar.displayMode == "Separated") {
            parametersPanel.className += " stiJsViewerParametersPanelSeparatedTop";
        }
    }

    parametersPanel.id = this.controls.viewer.id + "_ParametersPanel";
    parametersPanel.style.display = "none";
    parametersPanel.visible = false;
    parametersPanel.style.fontFamily = this.options.toolbar.fontFamily;
    if (this.options.toolbar.fontColor != "") parametersPanel.style.color = this.options.toolbar.fontColor;
    parametersPanel.jsObject = this;
    parametersPanel.currentOpeningParameter = null;
    parametersPanel.dropDownButtonWasClicked = false;
    parametersPanel.dateTimeButtonWasClicked = false;

    var styleTop = this.options.toolbar.visible ? this.controls.toolbar.offsetHeight : 0;
    if (this.options.isMobileDevice && this.options.toolbar.autoHide) styleTop = 0;
    styleTop += this.controls.drillDownPanel ? this.controls.drillDownPanel.offsetHeight : 0;
    styleTop += this.controls.findPanel ? this.controls.findPanel.offsetHeight : 0;
    styleTop += this.controls.resourcesPanel ? this.controls.resourcesPanel.offsetHeight : 0;
    parametersPanel.style.top = styleTop + "px";

    if (this.options.appearance.parametersPanelPosition == "Left") {
        if (this.options.isMobileDevice) parametersPanel.style.bottom = this.options.toolbar.autoHide ? "0" : "0.5in";
        else parametersPanel.style.bottom = this.options.toolbar.displayMode == "Separated" && this.options.toolbar.visible ? "35px" : "0";
    }

    if (this.options.isMobileDevice) parametersPanel.style.transition = "opacity 300ms ease";
    
    var innerPanel = document.createElement("div");
    parametersPanel.appendChild(innerPanel);

    if (this.options.toolbar.displayMode == "Simple") {
        innerPanel.style.marginTop = "2px";
        innerPanel.className = "stiJsViewerInnerParametersPanelSimple";
    }

    if (this.options.appearance.parametersPanelPosition == "Left") {
        innerPanel.className += " stiJsViewerInnerParametersPanelLeft";
        if (this.options.toolbar.displayMode == "Separated") {
            innerPanel.className += " stiJsViewerInnerParametersPanelSeparatedLeft";
        }
    }
       
    //Container
    parametersPanel.container = document.createElement("div");
    parametersPanel.container.id = parametersPanel.id + "Container";
    parametersPanel.container.className = "stiJsViewerInnerContainerParametersPanel";
    parametersPanel.container.jsObject = this;
    innerPanel.appendChild(parametersPanel.container);

    if (this.options.toolbar.backgroundColor != "") {
        parametersPanel.container.style.background = this.options.toolbar.backgroundColor;
    }

    if (this.options.toolbar.borderColor != "") {
        parametersPanel.container.style.border = "1px solid " + this.options.toolbar.borderColor;
    }
    
    if (this.options.appearance.parametersPanelPosition == "Top") {
        parametersPanel.container.style.maxHeight = this.options.appearance.parametersPanelMaxHeight + "px";
    }

    //Buttons
    var mainButtons = this.CreateHTMLTable();
    parametersPanel.mainButtons = mainButtons;
    mainButtons.setAttribute("align", "right");
    mainButtons.style.margin = "5px 0 10px 0";
    mainButtons.ID = parametersPanel.id + "MainButtons";

    parametersPanel.mainButtons.reset = this.FormButton("Reset", this.collections.loc["Reset"], null, 80);
    parametersPanel.mainButtons.submit = this.FormButton("Submit", this.collections.loc["Submit"], null, 80);
    mainButtons.addCell(parametersPanel.mainButtons.reset);
    mainButtons.addCell(parametersPanel.mainButtons.submit).style.paddingLeft = "10px";

    if (!this.options.isTouchDevice) {
        parametersPanel.container.onscroll = function () { parametersPanel.hideAllMenus(); }
    }

    parametersPanel.changeVisibleState = function (state) {
        var options = this.jsObject.options;
        var controls = this.jsObject.controls;
        parametersPanel.style.display = state ? "" : "none";
        if (!state) parametersPanel.hideAllMenus();
        parametersPanel.visible = state;
        if (options.toolbar.visible && options.toolbar.showParametersButton) controls.toolbar.controls.Parameters.setSelected(state);

        var parametersPanelHeight = this.jsObject.options.appearance.parametersPanelPosition == "Top" ? parametersPanel.offsetHeight : 0;
        var findPanelHeight = controls.findPanel ? controls.findPanel.offsetHeight : 0;
        var resourcesPanelHeight = controls.resourcesPanel ? controls.resourcesPanel.offsetHeight : 0;
        var drillDownPanelHeight = controls.drillDownPanel ? controls.drillDownPanel.offsetHeight : 0;
        var toolbarHeight = options.toolbar.visible ? controls.toolbar.offsetHeight : 0;
        if (options.isMobileDevice && options.toolbar.autoHide) toolbarHeight = 0;
        
        controls.reportPanel.style.marginTop = (controls.reportPanel.style.position == "relative"
            ? parametersPanelHeight
            : drillDownPanelHeight + findPanelHeight + resourcesPanelHeight + parametersPanelHeight) + "px";
        
        if (controls.bookmarksPanel != null) {
            controls.bookmarksPanel.style.top = (toolbarHeight + drillDownPanelHeight + findPanelHeight + resourcesPanelHeight + parametersPanelHeight) + "px";
        }

        if (options.appearance.parametersPanelPosition == "Left") {
            var bookmarksWidth = controls.bookmarksPanel && controls.bookmarksPanel.visible ? options.appearance.bookmarksTreeWidth : 0;
            parametersPanel.style.left = bookmarksWidth + "px";
            controls.reportPanel.style.marginLeft = (state ? bookmarksWidth + parametersPanel.firstChild.offsetWidth : bookmarksWidth) + "px";
        }

        if (options.isMobileDevice) {
            if (state && controls.bookmarksPanel) controls.bookmarksPanel.changeVisibleState(false);
            setTimeout(function () {
                parametersPanel.style.opacity = state ? "1" : "0";
                if (state) controls.reportPanel.hideToolbar();
                else if (!this.isUpdatePanel) controls.reportPanel.showToolbar();
            });
        }
    }

    parametersPanel.addParameters = function () {
        var paramsVariables = this.jsObject.copyObject(parametersPanel.jsObject.options.paramsVariables);
        var countParameters = this.jsObject.getCountObjects(paramsVariables);
        var countColumns = (countParameters <= this.jsObject.options.minParametersCountForMultiColumns)
            ? 1 : this.jsObject.options.appearance.parametersPanelColumnsCount;

        var countInColumn = parseInt(countParameters / countColumns);
        if (countInColumn * countColumns < countParameters) countInColumn++;

        var table = document.createElement("table");
        table.cellPadding = 0;
        table.cellSpacing = 0;
        table.style.border = 0;
        var tbody = document.createElement("tbody");
        table.appendChild(tbody);
        this.container.appendChild(table);

        var cellsVar = {};
        for (var indexRow = 0; indexRow < countInColumn + 1; indexRow++) {
            var row = document.createElement("tr");
            tbody.appendChild(row);

            for (indexColumn = 0; indexColumn < countColumns; indexColumn++) {
                var cellForName = document.createElement("td");
                cellForName.style.padding = "0 10px 0 " + ((indexColumn > 0) ? "30px" : 0);
                row.appendChild(cellForName);

                var cellForControls = document.createElement("td");
                cellForControls.style.padding = 0;
                row.appendChild(cellForControls);

                cellsVar[indexRow + ";" + indexColumn + "name"] = cellForName;
                cellsVar[indexRow + ";" + indexColumn + "controls"] = cellForControls;
            }
        }

        var indexColumn = 0;
        var indexRow = 0;

        for (var index = 0; index < countParameters; index++) {
            var nameCell = cellsVar[indexRow + ";" + indexColumn + "name"];
            nameCell.style.whiteSpace = "nowrap";
            nameCell.innerHTML = paramsVariables[index].alias;
            if (paramsVariables[index].basicType == "Range" && parametersPanel.jsObject.options.appearance.parametersPanelPosition == "Left") {
                nameCell.style.verticalAlign = "top";
                nameCell.style.paddingTop = parametersPanel.jsObject.options.isTouchDevice ? "11px" : "9px";
            }
            cellsVar[indexRow + ";" + indexColumn + "controls"].appendChild(parametersPanel.jsObject.CreateParameter(paramsVariables[index]));
            indexRow++;
            if (index == countParameters - 1) cellsVar[indexRow + ";" + indexColumn + "controls"].appendChild(parametersPanel.mainButtons);
            if (indexRow == countInColumn) { indexRow = 0; indexColumn++; }
        }
    }

    parametersPanel.clearParameters = function () {
        while (parametersPanel.container.childNodes[0]) {
            parametersPanel.container.removeChild(parametersPanel.container.childNodes[0]);
        }
    }

    parametersPanel.getParametersValues = function () {
        parametersValues = {};

        for (var name in parametersPanel.jsObject.options.parameters) {
            var parameter = parametersPanel.jsObject.options.parameters[name];
            parametersValues[name] = parameter.getValue();
        }

        return parametersValues;
    }

    parametersPanel.hideAllMenus = function () {
        if (parametersPanel.jsObject.options.currentMenu) parametersPanel.jsObject.options.currentMenu.changeVisibleState(false);
        if (parametersPanel.jsObject.options.currentDatePicker) parametersPanel.jsObject.options.currentDatePicker.changeVisibleState(false);
    }
    
    this.options.parameters = {};
    parametersPanel.addParameters();
    parametersPanel.changeVisibleState(createAndShow);
}

//Button
StiJsViewer.prototype.ParameterButton = function (buttonType, parameter) {
    var button = this.SmallButton(null, null, buttonType + ".png", null, null, "stiJsViewerFormButton");
    button.style.height = this.options.isTouchDevice ? "26px" : "21px";
    button.style.height = this.options.isTouchDevice ? "26px" : "21px";
    button.innerTable.style.width = "100%";
    button.imageCell.style.textAlign = "center";
    button.parameter = parameter;
    button.buttonType = buttonType;

    return button;
}

//TextBox
StiJsViewer.prototype.ParameterTextBox = function (parameter) {
    var textBox = this.TextBox(null, null, null, true);
    textBox.parameter = parameter;
    if (parameter.params.type == "Char") textBox.maxLength = 1;

    var width = "210px";
    if (parameter.params.basicType == "Range") {
        width = "140px";
        if (parameter.params.type == "Guid" || parameter.params.type == "String") width = "190px";
        if (parameter.params.type == "DateTime") width = "235px";
        if (parameter.params.type == "Char") width = "60px";
    }
    else {
        if (parameter.params.type == "Guid") width = "265px"; else width = "210px";
    }
    textBox.style.width = width;

    if (parameter.params.type == "DateTime") {
        textBox.action = function () {
            if (this.oldValue == this.value) return;
            try {
                var timeString = new Date().toLocaleTimeString();
                var isAmericanFormat = timeString.toLowerCase().indexOf("am") >= 0 || timeString.toLowerCase().indexOf("pm") >= 0;
                var formatDate = isAmericanFormat ? "MM/dd/yyyy" : "dd.MM.yyyy";
                var format = formatDate + (isAmericanFormat ? " h:mm:ss tt" : " hh:mm:ss");
                if (textBox.parameter.params.dateTimeType == "Date") format = formatDate;
                if (textBox.parameter.params.dateTimeType == "Time") format = "hh:mm:ss";
                var date = textBox.jsObject.GetDateTimeFromString(this.value, format);
                var dateTimeObject = textBox.jsObject.getDateTimeObject(date);
                textBox.parameter.params[textBox.parameter.controls.secondTextBox == textBox ? "keyTo" : "key"] = dateTimeObject;
                textBox.value = textBox.jsObject.dateTimeObjectToString(dateTimeObject, textBox.parameter.params.dateTimeType);
            }
            catch (e) {
                alert(e);
            }
        }
    }

    return textBox;
}

//CheckBox
StiJsViewer.prototype.ParameterCheckBox = function (parameter, caption) {
    var checkBox = this.CheckBox(null, caption);
    checkBox.parameter = parameter;

    return checkBox;
}

//Menu
StiJsViewer.prototype.ParameterMenu = function (parameter) {
    var menu = this.BaseMenu(null, parameter.controls.dropDownButton, "Down", "stiJsViewerDropdownMenu");
    menu.parameter = parameter;

    menu.changeVisibleState = function (state, parentButton) {
        var mainClassName = "stiJsViewerMainPanel";
        if (parentButton) {
            this.parentButton = parentButton;
            parentButton.haveMenu = true;
        }
        if (state) {            
            this.style.display = "";
            this.onshow();
            this.visible = true;
            this.style.overflow = "hidden";
            this.parentButton.setSelected(true);
            this.jsObject.options.currentMenu = this;
            this.style.width = this.innerContent.offsetWidth + "px";
            this.style.height = this.innerContent.offsetHeight + "px";
            this.style.left = (this.jsObject.FindPosX(parameter, mainClassName)) + "px";
            var animDirect = this.animationDirection;
            var browserHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            if (this.parentButton && animDirect == "Down" &&
                this.jsObject.FindPosY(this.parentButton) + this.parentButton.offsetHeight + this.innerContent.offsetHeight > browserHeight &&
                this.jsObject.FindPosY(this.parentButton) - this.innerContent.offsetHeight > 0) {
                animDirect = "Up";
            }
            this.style.top = (animDirect == "Down")
                ? (this.jsObject.FindPosY(this.parentButton, mainClassName) + this.parentButton.offsetHeight + 2) + "px"
                : (this.jsObject.FindPosY(this.parentButton, mainClassName) - this.offsetHeight) + "px";
            this.innerContent.style.top = ((animDirect == "Down" ? -1 : 1) * this.innerContent.offsetHeight) + "px";

            parameter.menu = this;

            d = new Date();
            var endTime = d.getTime();
            if (this.jsObject.options.toolbar.menuAnimation) endTime += this.jsObject.options.menuAnimDuration;
            this.jsObject.ShowAnimationVerticalMenu(this, (animDirect == "Down" ? 0 : -1), endTime); 
        }
        else {
            this.onHide();
            clearTimeout(this.innerContent.animationTimer);
            this.visible = false;
            this.parentButton.setSelected(false);
            this.style.display = "none";
            this.jsObject.controls.mainPanel.removeChild(this);
            parameter.menu = null;
            if (this.jsObject.options.currentMenu == this) this.jsObject.options.currentMenu = null;
        }
    }

    var table = this.CreateHTMLTable();
    table.style.fontFamily = this.options.toolbar.fontFamily;
    if (this.options.toolbar.fontColor != "") table.style.color = this.options.toolbar.fontColor;
    table.style.fontSize = "12px";
    table.style.width = (parameter.offsetWidth - 5) + "px";
    table.className = "stiJsViewerClearAllStyles stiJsViewerParametersMenuInnerTable";
    menu.innerContent.appendChild(table);
    menu.innerTable = table;

    return menu;
}

//MenuItem
StiJsViewer.prototype.parameterMenuItem = function (parameter) {
    var menuItem = document.createElement("div");
    menuItem.jsObject = this;
    menuItem.parameter = parameter;
    menuItem.isOver = false;
    menuItem.className = "stiJsViewerParametersMenuItem";
    menuItem.style.height = this.options.isTouchDevice ? "30px" : "24px";

    var table = this.CreateHTMLTable();
    table.className = "stiJsViewerClearAllStyles stiJsViewerParametersMenuItemInnerTable";
    menuItem.appendChild(table);

    menuItem.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    menuItem.onmouseout = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseleave();
    }

    menuItem.onmouseenter = function () {
        this.className = "stiJsViewerParametersMenuItemOver";
        this.isOver = true;
        if (this.parameter && this.parameter.menu && this.parameter.menu.currentFindedIndex != null) {
            var menu = this.parameter.menu;
            menu.findedItems[menu.currentFindedIndex].setSelected(false);
            for (var i = 0; i < menu.findedItems.length; i++) {
                if (menu.findedItems[i] == this) {
                    this.setSelected(true);
                    menu.currentFindedIndex = i;
                    break;
                }
            }
        };
    }
    menuItem.onmouseleave = function () {
        this.className = this.isSelected ? "stiJsViewerParametersMenuItemOver" : "stiJsViewerParametersMenuItem";
        this.isOver = false;
    }

    menuItem.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.className = "stiJsViewerParametersMenuItemPressed";
    }

    menuItem.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        this.parameter.jsObject.options.fingerIsMoved = false;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    menuItem.onmouseup = function () {
        if (this.isTouchEndFlag) return;
        this.parameter.jsObject.TouchEndMenuItem(this.id, false);
    }

    menuItem.ontouchend = function () {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        this.parameter.jsObject.TouchEndMenuItem(this.id, true);
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    menuItem.setSelected = function (state) {
        this.isSelected = state;
        this.className = state ? "stiJsViewerParametersMenuItemOver" : "stiJsViewerParametersMenuItem";
    }

    menuItem.innerContainer = table.addCell();
    menuItem.innerContainer.style.padding = "0 5px 0 5px";

    return menuItem;
}

StiJsViewer.prototype.addFindControlToParameterMenu = function (parameterMenu, parameter, notShowFindTextControl) {
    var findControl = this.CreateHTMLTable();
    
    findControl.addCell(this.TextBlock(this.collections.loc.FindWhat)).style.paddingLeft = "8px";

    var findTextbox = this.TextBox(null, 80);
    findTextbox.style.margin = "4px";
    findControl.addCell(findTextbox);
    findControl.findTextbox = findTextbox;
    
    var scrollContainer = document.createElement("div");
    scrollContainer.style.maxHeight = "400px";
    scrollContainer.style.overflowX = "hidden";
    scrollContainer.style.overflowY = "auto";
    scrollContainer.appendChild(parameterMenu.innerTable);

    parameterMenu.innerContent.style.overflowX = parameterMenu.innerContent.style.overflowY = "visible";
    parameterMenu.innerContent.style.maxHeight = "";
    if (!notShowFindTextControl) {
        parameterMenu.innerContent.appendChild(findControl);
        parameterMenu.innerContent.appendChild(this.parameterMenuSeparator());
    }
    parameterMenu.innerContent.appendChild(scrollContainer);

    parameterMenu.findItems = function (findText) {
        if (parameterMenu.currentFindedIndex != null) {
            parameterMenu.findedItems[parameterMenu.currentFindedIndex].setSelected(false);
            scrollContainer.scrollTop = 0;
        }

        parameterMenu.findedItems = [];
        parameterMenu.currentFindedIndex = null;
                
        for (var i = 0; i < parameterMenu.paramsItems.length; i++) {
            var itemText = parameterMenu.paramsItems[i].checkBox
                ? parameterMenu.paramsItems[i].checkBox.captionCell.innerHTML
                : parameterMenu.paramsItems[i].innerContainer.innerHTML;

            if (parameterMenu.paramsItems[i].isOver) {
                parameterMenu.paramsItems[i].onmouseleave();
            }

            if (itemText.toLowerCase().indexOf(findText.toLowerCase()) >= 0) {                
                parameterMenu.paramsItems[i].style.display = "";
                parameterMenu.findedItems.push(parameterMenu.paramsItems[i]);
            }
            else {
                parameterMenu.paramsItems[i].style.display = "none";
            }
        }
        
        if (parameterMenu.findedItems.length > 0) {            
            parameterMenu.showFindedItem(0, findText == "");
        }
    }

    parameterMenu.showFindedItem = function (index, notVisualSelect) {
        if (parameterMenu.currentFindedIndex != null) {
            parameterMenu.findedItems[parameterMenu.currentFindedIndex].setSelected(false);
        }

        parameterMenu.currentFindedIndex = index;
        if (index >= parameterMenu.findedItems.length) {
            parameterMenu.currentFindedIndex = 0;
        }
        else if (index < 0) {
            parameterMenu.currentFindedIndex = parameterMenu.findedItems.length - 1;
        }
        
        if (!notVisualSelect) {
            parameterMenu.findedItems[parameterMenu.currentFindedIndex].setSelected(true);
            var yPos = this.jsObject.FindPosY(parameterMenu.findedItems[parameterMenu.currentFindedIndex], "stiJsViewerDropdownMenu", true);
            scrollContainer.scrollTop = yPos - scrollContainer.offsetHeight;
        }
    }

    findTextbox.onchange = function () {
        parameterMenu.findItems(this.value);
    }

    return findControl;
}

StiJsViewer.prototype.TouchEndMenuItem = function (menuItemId, flag) {
    var menuItem = document.getElementById(menuItemId);
    if (!menuItem || menuItem.parameter.jsObject.options.fingerIsMoved) return;

    if (flag) {
        menuItem.className = "stiJsViewerParametersMenuItemPressed";
        if (typeof event !== "undefined" && ('preventDefault' in event)) event.preventDefault();
        setTimeout(function () {
            menuItem.parameter.jsObject.TouchEndMenuItem(menuItem.id, false);
        }, 200);
        return;
    }

    menuItem.className = menuItem.isOver ? "stiJsViewerParametersMenuItemOver" : "stiJsViewerParametersMenuItem";
    if (menuItem.action != null) menuItem.action();
}

//MenuSeparator
StiJsViewer.prototype.parameterMenuSeparator = function () {
    var separator = document.createElement("Div");
    separator.className = "stiJsViewerParametersMenuSeparator";

    return separator;
}

//Menu For Value
StiJsViewer.prototype.parameterMenuForValue = function (parameter) {
    var menuParent = this.ParameterMenu(parameter);
    menuParent.paramsItems = [];

    var findControl = this.addFindControlToParameterMenu(menuParent, parameter, this.getCountObjects(parameter.params.items) < 10);

    menuParent.onshow = function () {        
        menuParent.findItems("");
        setTimeout(function () { findControl.findTextbox.focus(); }, 200);
    }

    for (var i = 0; i < parameter.params.items.length; i++) {
        var cell = menuParent.innerTable.addCellInNextRow();
        var menuItem = this.parameterMenuItem(parameter);
        cell.appendChild(menuItem);
        menuParent.paramsItems.push(menuItem);

        menuItem.id = parameter.jsObject.controls.viewer.id + parameter.params.name + "Item" + i;
        menuItem.parameter = parameter;
        menuItem.key = parameter.params.items[i].key;
        menuItem.value = parameter.params.items[i].value;
        menuItem.innerContainer.innerHTML =
            (menuItem.value != "" && parameter.params.type != "DateTime" && parameter.params.type != "TimeSpan" && parameter.params.type != "Bool")
                ? menuItem.value
                : this.getStringKey(menuItem.key, menuItem.parameter);

        menuItem.action = function () {
            this.parameter.params.key = this.key;
            if (this.parameter.params.type != "Bool")
                this.parameter.controls.firstTextBox.value = (this.parameter.params.type == "DateTime" || this.parameter.params.type == "TimeSpan")
                    ? this.parameter.jsObject.getStringKey(this.key, this.parameter)
                    : (this.parameter.params.allowUserValues ? this.key : (this.value != "" ? this.value : this.key));
            else
                this.parameter.controls.boolCheckBox.setChecked(this.key == "True");
            this.parameter.changeVisibleStateMenu(false);

            if (this.parameter.params.binding) {
                if (!this.jsObject.options.paramsVariablesStartValues) {
                    this.jsObject.options.paramsVariablesStartValues = this.jsObject.copyObject(this.jsObject.options.paramsVariables);
                }
                var params = { action: "InitVars", variables: this.jsObject.controls.parametersPanel.getParametersValues() };
                this.jsObject.postInteraction(params);
            }
        }
    }
        
    return menuParent;
}

//Menu For Range
StiJsViewer.prototype.parameterMenuForRange = function (parameter) {
    var menuParent = this.ParameterMenu(parameter);
    menuParent.paramsItems = [];

    var findControl = this.addFindControlToParameterMenu(menuParent, parameter, this.getCountObjects(parameter.params.items) < 10);

    menuParent.onshow = function () {
        menuParent.findItems("");
        setTimeout(function () { findControl.findTextbox.focus(); }, 200);
    }

    for (var index in parameter.params.items) {
        var cell = menuParent.innerTable.addCellInNextRow();
        var menuItem = this.parameterMenuItem(parameter);
        cell.appendChild(menuItem);
        menuParent.paramsItems.push(menuItem);

        menuItem.id = parameter.jsObject.controls.viewer.id + parameter.params.name + "Item" + index;
        menuItem.parameter = parameter;
        menuItem.value = parameter.params.items[index].value;
        menuItem.key = parameter.params.items[index].key;
        menuItem.keyTo = parameter.params.items[index].keyTo;
        menuItem.innerContainer.innerHTML = menuItem.value + " [" + this.getStringKey(menuItem.key, menuItem.parameter) +
            " - " + this.getStringKey(menuItem.keyTo, menuItem.parameter) + "]";

        menuItem.action = function () {
            this.parameter.params.key = this.key;
            this.parameter.params.keyTo = this.keyTo;
            this.parameter.controls.firstTextBox.value = this.parameter.jsObject.getStringKey(this.key, this.parameter);
            this.parameter.controls.secondTextBox.value = this.parameter.jsObject.getStringKey(this.keyTo, this.parameter);
            this.parameter.changeVisibleStateMenu(false);
        }
    }

    return menuParent;
}

//Menu For ListNotEdit
StiJsViewer.prototype.parameterMenuForNotEditList = function (parameter) {
    var menuParent = this.ParameterMenu(parameter);
    menuParent.menuItems = {};
    var selectedAll = true;
    menuParent.paramsItems = [];

    var findControl = this.addFindControlToParameterMenu(menuParent, parameter, this.getCountObjects(parameter.params.items) < 10);
        
    var checkBoxSelectAll = this.CheckBox(null, this.collections.loc["SelectAll"]);
    menuParent.checkBoxSelectAll = checkBoxSelectAll;
    checkBoxSelectAll.style.margin = "8px 7px 8px 7px";
    menuParent.innerTable.addCellInNextRow(checkBoxSelectAll);
    menuParent.innerTable.addCellInNextRow(this.parameterMenuSeparator());
    checkBoxSelectAll.setChecked(selectedAll);
    checkBoxSelectAll.action = function () {
        var selectAll = this.isChecked;
        for (var index in parameter.params.items) {
            menuParent.menuItems[index].checkBox.setChecked(selectAll, true);
        }
        menuParent.updateItems();
    }

    menuParent.onshow = function () {
        menuParent.findItems("");
        menuParent.updateItems();
        setTimeout(function () { findControl.findTextbox.focus(); }, 200);
    }

    menuParent.updateItems = function () {
        parameter.params.items = {};
        parameter.controls.firstTextBox.value = "";
        var selectAll = true;

        for (var index in this.menuItems) {
            parameter.params.items[index] = {};
            parameter.params.items[index].key = this.menuItems[index].key;
            parameter.params.items[index].value = this.menuItems[index].value;
            parameter.params.items[index].isChecked = this.menuItems[index].checkBox.isChecked;

            if (selectAll && !this.menuItems[index].checkBox.isChecked) {
                selectAll = false;
            }

            if (parameter.params.items[index].isChecked) {
                if (parameter.controls.firstTextBox.value != "") parameter.controls.firstTextBox.value += ";";
                parameter.controls.firstTextBox.value += this.menuItems[index].value != ""
                    ? this.menuItems[index].value : parameter.jsObject.getStringKey(this.menuItems[index].key, parameter);
            }
        }

        this.checkBoxSelectAll.setChecked(selectAll);
    }

    for (var index in parameter.params.items) {
        var cell = menuParent.innerTable.addCellInNextRow();
        menuItem = this.parameterMenuItem(parameter);
        cell.appendChild(menuItem);
        menuParent.paramsItems.push(menuItem);
        
        menuItem.id = parameter.jsObject.controls.viewer.id + parameter.params.name + "Item" + index;
        menuItem.parameter = parameter;
        menuItem.value = parameter.params.items[index].value;
        menuItem.key = parameter.params.items[index].key;
        menuParent.menuItems[index] = menuItem;

        var innerTable = this.CreateHTMLTable();
        innerTable.style.width = "100%";
        menuItem.innerContainer.appendChild(innerTable);
        var cellCheck = innerTable.addCell();
        
        var checkBox = this.ParameterCheckBox(parameter, menuItem.value != "" ? menuItem.value : this.getStringKey(menuItem.key, menuItem.parameter));
        checkBox.style.marginRight = "5px";
        checkBox.style.width = "100%";
        checkBox.imageBlock.parentElement.style.width = "1px";
        cellCheck.appendChild(checkBox);
        checkBox.menuParent = menuParent;
        checkBox.setChecked(parameter.params.items[index].isChecked);
        menuItem.checkBox = checkBox;
        if (!checkBox.isChecked) selectedAll = false;
                
        checkBox.onChecked = function () {
            menuParent.updateItems();
        }
    }

    var closeButton = this.parameterMenuItem(parameter);
    closeButton.id = parameter.jsObject.controls.viewer.id + parameter.params.name + "ItemClose";
    closeButton.innerContainer.innerHTML = this.collections.loc["Close"];
    closeButton.innerContainer.style.paddingLeft = "13px";
    closeButton.action = function () { this.parameter.changeVisibleStateMenu(false); }
    menuParent.innerTable.addCellInNextRow(this.parameterMenuSeparator());
    menuParent.innerTable.addCellInNextRow(closeButton);
        
    return menuParent;
}

//Menu For ListEdit
StiJsViewer.prototype.parameterMenuForEditList = function (parameter) {
    var menuParent = this.ParameterMenu(parameter);

    //New Item Method
    menuParent.newItem = function (item, parameter) {
        var menuItem = parameter.jsObject.parameterMenuItem(parameter);
        //cell.appendChild(menuItem);
        menuItem.id = parameter.jsObject.controls.viewer.id + parameter.params.name + "Item" + parameter.jsObject.newGuid().replace(/-/g, '');
        menuItem.onmouseover = null;
        menuItem.onmousedown = null;
        menuItem.ontouchend = null;
        menuItem.action = null;
        menuItem.parameter = parameter;
        menuItem.value = item.value;
        menuItem.key = item.key;

        var innerTable = menuItem.jsObject.CreateHTMLTable();
        menuItem.innerContainer.appendChild(innerTable);

        //Text Box        
        var textBox = parameter.jsObject.ParameterTextBox(parameter);
        menuItem.textBox = textBox;
        //textBox.setReadOnly(parameter.params.type == "DateTime");
        textBox.value = parameter.jsObject.getStringKey(menuItem.key, menuItem.parameter);
        textBox.thisMenu = menuParent;
        innerTable.addCell(textBox).style.padding = "0 1px 0 0";

        //DateTime Button
        if (parameter.params.type == "DateTime") {
            var dateTimeButton = parameter.jsObject.ParameterButton("DateTimeButton", parameter);
            dateTimeButton.id = menuItem.id + "DateTimeButton";
            dateTimeButton.parameter = parameter;
            dateTimeButton.thisItem = menuItem;
            innerTable.addCell(dateTimeButton).style.padding = "0 1px 0 1px";

            dateTimeButton.action = function () {
                var datePicker = dateTimeButton.jsObject.controls.datePicker;
                datePicker.ownerValue = this.thisItem.key;
                datePicker.parentDataControl = this.thisItem.textBox;
                datePicker.parentButton = this;                
                datePicker.changeVisibleState(!datePicker.visible);
            }
        }

        //Guid Button
        if (parameter.params.type == "Guid") {
            var guidButton = parameter.jsObject.ParameterButton("GuidButton", parameter);
            guidButton.id = menuItem.id + "GuidButton";
            guidButton.thisItem = menuItem;
            guidButton.thisMenu = menuParent;
            innerTable.addCell(guidButton).style.padding = "0 1px 0 1px";

            guidButton.action = function () {
                this.thisItem.textBox.value = this.parameter.jsObject.newGuid();
                this.thisMenu.updateItems();
            }
        }

        //Remove Button                        
        var removeButton = parameter.jsObject.ParameterButton("RemoveItemButton", parameter);
        removeButton.id = menuItem.id + "RemoveButton";
        removeButton.itemsContainer = this.itemsContainer;
        removeButton.thisItem = menuItem;
        removeButton.thisMenu = menuParent;
        innerTable.addCell(removeButton).style.padding = "0 1px 0 1px";
        removeButton.action = function () {
            this.itemsContainer.removeChild(this.thisItem);
            this.thisMenu.updateItems();
        }

        return menuItem;
    }

    //Update Items
    menuParent.updateItems = function () {
        this.parameter.params.items = {};
        this.parameter.controls.firstTextBox.value = "";
        for (index = 0; index < this.itemsContainer.childNodes.length; index++) {
            itemMenu = this.itemsContainer.childNodes[index];
            this.parameter.params.items[index] = {};
            this.parameter.params.items[index].key =
                (this.parameter.params.type == "DateTime")
                ? itemMenu.key
                : itemMenu.textBox.value;
            this.parameter.params.items[index].value = itemMenu.value;
            if (this.parameter.controls.firstTextBox.value != "") this.parameter.controls.firstTextBox.value += ";";
            this.parameter.controls.firstTextBox.value += this.parameter.jsObject.getStringKey(this.parameter.params.items[index].key, this.parameter);
        }

        if (this.parameter.menu.innerTable.offsetHeight > 400) this.parameter.menu.style.height = "350px;"
        else this.parameter.menu.style.height = this.parameter.menu.innerTable.offsetHeight + "px";
    }

    //New Item Button
    var newItemButton = this.parameterMenuItem(parameter);
    menuParent.innerTable.addCell(newItemButton);
    newItemButton.id = parameter.jsObject.controls.viewer.id + parameter.params.name + "ItemNew";
    newItemButton.innerContainer.innerHTML = this.collections.loc["NewItem"];
    newItemButton.thisMenu = menuParent;
    newItemButton.action = function () {
        var item_ = {};
        if (this.parameter.params.type == "DateTime") {
            item_.key = this.parameter.jsObject.getDateTimeObject();
            item_.value = this.parameter.jsObject.dateTimeObjectToString(item_.key, this.parameter);
        }
        else if (this.parameter.params.type == "TimeSpan") {
                item_.key = "00:00:00";
                item_.value = "00:00:00";
            }
            else if (this.parameter.params.type == "Bool") {
                    item_.key = "False";
                    item_.value = "False";
                }
                else {
                    item_.key = "";
                    item_.value = "";
                }        
        var newItem = this.thisMenu.newItem(item_, this.parameter);
        this.thisMenu.itemsContainer.appendChild(newItem);
        if ("textBox" in newItem) newItem.textBox.focus();
        this.thisMenu.updateItems();
    }

    //Add Items
    var cellItems = menuParent.innerTable.addCellInNextRow();
    menuParent.itemsContainer = cellItems;

    for (var index in parameter.params.items) {
        cellItems.appendChild(menuParent.newItem(parameter.params.items[index], parameter));
    }

    var cellDown = menuParent.innerTable.addCellInNextRow();

    //Remove All Button
    var removeAllButton = this.parameterMenuItem(parameter);
    cellDown.appendChild(removeAllButton);
    removeAllButton.id = parameter.jsObject.controls.viewer.id + parameter.params.name + "ItemRemoveAll";
    removeAllButton.innerContainer.innerHTML = this.collections.loc["RemoveAll"];
    removeAllButton.thisMenu = menuParent;
    removeAllButton.action = function () {
        while (this.thisMenu.itemsContainer.childNodes[0]) {
            this.thisMenu.itemsContainer.removeChild(this.thisMenu.itemsContainer.childNodes[0]);
        }
        this.thisMenu.updateItems();
    }

    //Close Button
    cellDown.appendChild(this.parameterMenuSeparator());
    var closeButton = this.parameterMenuItem(parameter);
    cellDown.appendChild(closeButton);
    closeButton.id = parameter.jsObject.controls.viewer.id + parameter.params.name + "ItemClose";
    closeButton.innerContainer.innerHTML = this.collections.loc["Close"];
    closeButton.action = function () { this.parameter.changeVisibleStateMenu(false); }

    menuParent.onHide = function () {
        this.updateItems();
    }

    return menuParent;
}

StiJsViewer.prototype.ReplaceMonths = function (value) {
    for (var i = 1; i <= 12; i++) {
        var enName = "";
        var locName = "";
        switch (i) {
            case 1:
                enName = "January";
                locName = this.collections.loc.MonthJanuary;
                break;

            case 2:
                enName = "February";
                locName = this.collections.loc.MonthFebruary;
                break;

            case 3:
                enName = "March";
                locName = this.collections.loc.MonthMarch;
                break;

            case 4:
                enName = "April";
                locName = this.collections.loc.MonthApril;
                break;

            case 5:
                enName = "May";
                locName = this.collections.loc.MonthMay;
                break;

            case 6:
                enName = "June";
                locName = this.collections.loc.MonthJune;
                break;

            case 7:
                enName = "July";
                locName = this.collections.loc.MonthJuly;
                break;

            case 8:
                enName = "August";
                locName = this.collections.loc.MonthAugust;
                break;

            case 9:
                enName = "September";
                locName = this.collections.loc.MonthSeptember;
                break;

            case 10:
                enName = "October";
                locName = this.collections.loc.MonthOctober;
                break;

            case 11:
                enName = "November";
                locName = this.collections.loc.MonthNovember;
                break;

            case 12:
                enName = "December";
                locName = this.collections.loc.MonthDecember;
                break;
        }

        var enShortName = enName.substring(0, 3);
        var locShortName = locName.substring(0, 3);
        value = value.replace(enName, i).replace(enName.toLowerCase(), i).replace(enShortName, i).replace(enShortName.toLowerCase(), i);
        value = value.replace(locName, i).replace(locName.toLowerCase(), i).replace(locShortName, i).replace(locShortName.toLowerCase(), i);

    }

    return value;
}

StiJsViewer.prototype.GetDateTimeFromString = function (value, format) {
    var charIsDigit = function (char) {
        return ("0123456789".indexOf(char) >= 0);
    }

    if (!value) return new Date();
    value = this.ReplaceMonths(value);

    var dateTime = new Date();

    // If the date format is not specified, then deserializator for getting date and time is applied
    if (format == null) format = "dd.MM.yyyy hh:mm:ss";
    // Otherwise the format is parsed. Now only numeric date and time formats are supported

    var year = 1970;
    var month = 1;
    var day = 1;
    var hour = 0;
    var minute = 0;
    var second = 0;
    var millisecond = 0;

    var char = "";
    var pos = 0;
    var values = [];

    // Parse date and time into separate numeric values
    while (pos < value.length) {
        char = value.charAt(pos);
        if (charIsDigit(char)) {
            values.push(char);
            pos++;

            while (pos < value.length && charIsDigit(value.charAt(pos))) {
                values[values.length - 1] += value.charAt(pos);
                pos++;
            }

            values[values.length - 1] = this.StrToInt(values[values.length - 1]);
        }

        pos++;
    }

    pos = 0;
    var charCount = 0;
    var index = -1;
    var is12hour = false;

    // Parsing format and replacement of appropriate values of date and time
    while (pos < format.length) {
        char = format.charAt(pos);
        charCount = 0;

        if (char == "Y" || char == "y" || char == "M" || char == "d" || char == "h" || char == "H" ||
						char == "m" || char == "s" || char == "f" || char == "F" || char == "t" || char == "z") {
            index++;

            while (pos < format.length && format.charAt(pos) == char) {
                pos++;
                charCount++;
            }
        }

        switch (char) {
            case "Y": // full year
                year = values[index];
                break;

            case "y": // year
                if (values[index] < 1000) year = 2000 + values[index];
                else year = values[index];
                break;

            case "M": // month
                month = values[index];
                break;

            case "d": // day
                day = values[index];
                break;

            case "h": // (hour 12)
                is12hour = true;

            case "H": // (hour 24)
                hour = values[index];
                break;

            case "m": // minute
                minute = values[index];
                break;

            case "s": // second
                second = values[index];
                break;

            case "f": // second fraction
            case "F": // second fraction, trailing zeroes are trimmed
                millisecond = values[index];
                break;

            case "t": // PM or AM
                if (value.toLowerCase().indexOf("am") >= 0 && hour == 12) hour = 0;
                if (value.toLowerCase().indexOf("pm") >= 0 && hour < 12) hour += 12;
                break;

            default:
                pos++;
                break;
        }
    }

    dateTime = new Date(year, month - 1, day, hour, minute, second, millisecond);

    return dateTime;
}