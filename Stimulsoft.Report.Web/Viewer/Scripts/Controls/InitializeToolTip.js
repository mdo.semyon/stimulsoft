﻿
StiJsViewer.prototype.InitializeToolTip = function () {
    var toolTip = document.createElement("div");
    toolTip.id = this.controls.viewer.id + "ToolTip";
    toolTip.jsObject = this;
    this.controls.toolTip = toolTip;
    this.controls.mainPanel.appendChild(toolTip);
    toolTip.className = "stiJsViewerToolTip";
    toolTip.style.display = "none";
    toolTip.showTimer = null;
    toolTip.hideTimer = null;
    toolTip.visible = false;

    toolTip.innerTable = this.CreateHTMLTable();
    toolTip.appendChild(toolTip.innerTable);

    toolTip.textCell = toolTip.innerTable.addCell();
    toolTip.textCell.className = "stiJsViewerToolTipTextCell";

    if (this.options.appearance.showTooltipsHelp) {
        toolTip.helpButton = this.SmallButton(null, this.collections.loc["TellMeMore"], "HelpIcon.png", null, null, "stiJsViewerHyperlinkButton");
        toolTip.innerTable.addCellInNextRow(toolTip.helpButton);
        toolTip.helpButton.style.margin = "4px 8px 4px 8px";
    }
    else
        toolTip.textCell.style.border = 0;

    toolTip.show = function (text, helpUrl, leftPos, topPos, controlWidthForRightToLeft) {
        if ((this.visible && text == this.textCell.innerHTML) || this.jsObject.options.isTouchDevice) return;
        this.hide();

        if (this.jsObject.options.appearance.showTooltipsHelp) {
            this.helpButton.helpUrl = helpUrl;
            this.helpButton.action = function () {
                this.jsObject.showHelpWindow(this.helpUrl);
            }
        }

        this.textCell.innerHTML = text;
        var d = new Date();
        var endTime = d.getTime() + 300;
        this.style.opacity = 1 / 100;
        this.style.display = "";

        this.style.left = (controlWidthForRightToLeft != null ?
            leftPos - this.offsetWidth + controlWidthForRightToLeft : leftPos) + "px";
        this.style.top = (topPos == "isNavigatePanelTooltip"
            ? (this.jsObject.controls.reportPanel.offsetHeight +
              (this.jsObject.options.toolbar.visible ? this.jsObject.controls.toolbar.offsetHeight : 0) +
              (this.jsObject.controls.findPanel ? this.jsObject.controls.findPanel.offsetHeight : 0) +
              (this.jsObject.controls.drillDownPanel ? this.jsObject.controls.drillDownPanel.offsetHeight : 0) +
              (this.jsObject.controls.parametersPanel && this.jsObject.options.appearance.parametersPanelPosition == "Top" ? this.jsObject.controls.parametersPanel.offsetHeight : 0) -
              this.offsetHeight - 2)
            : topPos) + "px";
        
        this.visible = true;
        this.jsObject.ShowAnimationForm(this, endTime);
    }

    toolTip.showWithDelay = function (text, helpUrl, leftPos, topPos, controlWidthForRightToLeft) {
        clearTimeout(this.showTimer);
        clearTimeout(this.hideTimer);
        var this_ = this;
        this.showTimer = setTimeout(function () {
            this_.show(text, helpUrl, leftPos, topPos, controlWidthForRightToLeft);
        }, 300);
    }

    toolTip.hide = function () {
        this.visible = false;
        clearTimeout(this.showTimer);
        this.style.display = "none";
    }

    toolTip.hideWithDelay = function () {
        clearTimeout(this.showTimer);
        clearTimeout(this.hideTimer);
        var this_ = this;
        this.hideTimer = setTimeout(function () {
            this_.hide();
        }, 500);
    }

    toolTip.onmouseover = function () {
        clearTimeout(this.showTimer);
        clearTimeout(this.hideTimer);
    }

    toolTip.onmouseout = function () {
        this.hideWithDelay();
    }
}