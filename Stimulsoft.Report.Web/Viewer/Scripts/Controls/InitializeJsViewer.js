
StiJsViewer.prototype.InitializeJsViewer = function () {
    this.controls.viewer.jsObject = this;

    this.controls.viewer.pressedDown = function () {
        var options = this.jsObject.options;

        this.jsObject.removeBookmarksLabel();

        //Close Current Menu
        if (options.currentMenu != null)
            if (options.menuPressed != options.currentMenu && options.currentMenu.parentButton != options.buttonPressed && !options.datePickerPressed && !options.dropDownListMenuPressed)
                options.currentMenu.changeVisibleState(false);

        //Close Current DropDownList
        if (options.currentDropDownListMenu != null)
            if (options.dropDownListMenuPressed != options.currentDropDownListMenu && options.currentDropDownListMenu.parentButton != options.buttonPressed)
                options.currentDropDownListMenu.changeVisibleState(false);

        //Close Current DatePicker
        if (options.currentDatePicker != null)
            if (options.datePickerPressed != options.currentDatePicker && options.currentDatePicker.parentButton != options.buttonPressed)
                options.currentDatePicker.changeVisibleState(false);

        options.buttonPressed = false;
        options.menuPressed = false;
        options.formPressed = false;
        options.dropDownListMenuPressed = false;
        options.disabledPanelPressed = false;
        options.datePickerPressed = false;
        options.fingerIsMoved = false;
    }

    this.controls.viewer.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.jsObject.options.isTouchClick = false;
        this.pressedDown();
    }

    this.controls.viewer.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        if (this.jsObject.options.buttonsTimer) {
            clearTimeout(this.jsObject.options.buttonsTimer[2]);
            this.jsObject.options.buttonsTimer[0].className = this.jsObject.options.buttonsTimer[1];
            this.jsObject.options.buttonsTimer = null;
        }
        this.jsObject.options.isTouchClick = true;
        this.pressedDown();
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    this.controls.viewer.onmouseup = function () {
        if (this.isTouchEndFlag) return;
        this.ontouchend();
    }

    this.controls.viewer.ontouchend = function () {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        this.jsObject.options.fingerIsMoved = false;
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    this.controls.viewer.ontouchmove = function () {
        this.jsObject.options.fingerIsMoved = true;
    }

    var jsObject = this;

    this.addEvent(window, 'keypress', function (e) {
        if (e) {
            if (jsObject.options.currentMenu && jsObject.options.currentMenu.currentFindedIndex != null) {
                if (e.keyCode == 13) {
                    if (jsObject.options.currentMenu.findedItems[jsObject.options.currentMenu.currentFindedIndex].action != null) {
                        jsObject.options.currentMenu.findedItems[jsObject.options.currentMenu.currentFindedIndex].action();
                        e.stopPropagation();
                        e.cancelBubble = true;
                    }
                }
            }
        }
    });

    this.addEvent(window, 'keyup', function (e) {
        if (e) {
            if (jsObject.options.currentMenu && jsObject.options.currentMenu.currentFindedIndex != null) {
                var currentMenu = jsObject.options.currentMenu;
                if (e.keyCode == 40 || e.keyCode == 38) {
                    var selectIndex = (currentMenu.currentFindedIndex == 0 &&
                        currentMenu.findedItems.length > 0 &&
                        !currentMenu.findedItems[currentMenu.currentFindedIndex].isSelected)
                            ? 0 : jsObject.options.currentMenu.currentFindedIndex + (e.keyCode == 40 ? 1 : -1);
                    jsObject.options.currentMenu.showFindedItem(selectIndex);
                }
            }
        }
    });
}