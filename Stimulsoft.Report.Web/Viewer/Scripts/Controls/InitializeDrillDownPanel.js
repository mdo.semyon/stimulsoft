
StiJsViewer.prototype.InitializeDrillDownPanel = function () {
    var drillDownPanel = document.createElement("div");
    this.controls.drillDownPanel = drillDownPanel;
    this.controls.mainPanel.appendChild(drillDownPanel);
    drillDownPanel.jsObject = this;
    drillDownPanel.className = "stiJsViewerToolBar";
    if (this.options.toolbar.displayMode == "Separated") drillDownPanel.className += " stiJsViewerToolBarSeparated";
    drillDownPanel.style.display = "none";

    var drillDownInnerContent = document.createElement("div");
    drillDownPanel.appendChild(drillDownInnerContent);
    if (this.options.toolbar.displayMode == "Simple") drillDownInnerContent.style.paddingTop = "2px";

    var drillDownInnerTable = this.CreateHTMLTable();
    drillDownInnerTable.className = "stiJsViewerToolBarTable";
    if (this.options.toolbar.displayMode == "Separated") drillDownInnerTable.style.border = "0px";
    drillDownInnerContent.appendChild(drillDownInnerTable);
    drillDownInnerTable.style.margin = "0";
    if (this.options.toolbar.fontColor != "") drillDownInnerTable.style.color = this.options.toolbar.fontColor;
    drillDownInnerTable.style.fontFamily = this.options.toolbar.fontFamily;
    drillDownInnerTable.style.boxSizing = "border-box";

    var buttonsTable = this.CreateHTMLTable();
    drillDownInnerTable.addCell(buttonsTable);

    drillDownPanel.buttonsRow = buttonsTable.rows[0];
    drillDownPanel.buttons = {};
    drillDownPanel.selectedButton = null;

    drillDownPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        var drillDownPanelHeight = this.offsetHeight;
        var parametersPanelHeight = this.jsObject.controls.parametersPanel && this.jsObject.options.appearance.parametersPanelPosition == "Top"
            ? this.jsObject.controls.parametersPanel.offsetHeight : 0;
        var toolBarHeight = this.jsObject.options.toolbar.visible ? this.jsObject.controls.toolbar.offsetHeight : 0;
        if (this.jsObject.options.isMobileDevice && this.jsObject.options.toolbar.autoHide) toolBarHeight = 0;
        var findPanelHeight = this.jsObject.controls.findPanel ? this.jsObject.controls.findPanel.offsetHeight : 0;
        var resourcesPanelHeight = this.jsObject.controls.resourcesPanel ? this.jsObject.controls.resourcesPanel.offsetHeight : 0;

        if (this.jsObject.controls.parametersPanel) {
            this.jsObject.controls.parametersPanel.style.top = (toolBarHeight + findPanelHeight + drillDownPanelHeight) + "px";
        }
        if (this.jsObject.controls.bookmarksPanel) {
            this.jsObject.controls.bookmarksPanel.style.top = (toolBarHeight + findPanelHeight + parametersPanelHeight + drillDownPanelHeight) + "px";
        }
        this.jsObject.controls.reportPanel.style.marginTop = (this.jsObject.controls.reportPanel.style.position == "relative"
            ? (parametersPanelHeight + findPanelHeight)
            : (drillDownPanelHeight + parametersPanelHeight + findPanelHeight + resourcesPanelHeight)) + "px";
    }

    drillDownPanel.addButton = function (caption, reportParams) {
        var name = "button" + (drillDownPanel.buttonsRow.children.length + 1);
        var button = drillDownPanel.jsObject.SmallButton(name, caption);
        button.style.display = "inline-block";
        button.reportParams = reportParams ? reportParams : this.reportParams = {};
        drillDownPanel.buttons[name] = button;
        button.style.margin = "2px 1px 2px 2px";

        var cell = buttonsTable.addCell(button);
        cell.style.padding = "0";
        cell.style.border = "0";
        cell.style.lineHeight = "0";

        button.select = function () {
            if (drillDownPanel.selectedButton) drillDownPanel.selectedButton.setSelected(false);
            this.setSelected(true);
            drillDownPanel.selectedButton = this;
            drillDownPanel.jsObject.reportParams = this.reportParams;
            drillDownPanel.jsObject.controls.reportPanel.scrollTop = 0;
        }

        button.action = function () {
            if (this.style.display != "none") {
                this.select();
                drillDownPanel.jsObject.postAction("GetPages");
            }
        };

        button.select();

        if (name != "button1") {
            var closeButton = drillDownPanel.jsObject.SmallButton(null, null, "CloseForm.png");
            closeButton.style.display = "inline-block";
            closeButton.style.margin = "0 2px 0 0";
            closeButton.image.style.margin = "1px 0 0 -1px";
            closeButton.imageCell.style.padding = 0;
            closeButton.style.width = drillDownPanel.jsObject.options.isTouchDevice ? "22px" : "17px";
            closeButton.style.height = closeButton.style.width;
            closeButton.reportButton = button;
            button.innerTable.addCell(closeButton);

            closeButton.action = function () {
                this.reportButton.style.display = "none";
                if (this.reportButton.isSelected) drillDownPanel.buttons["button1"].action();
            };

            closeButton.onmouseenter = function (event) {
                this.reportButton.onmouseoutAction();
                this.onmouseoverAction();
                if (event) event.stopPropagation();
            }
        }
    }

    drillDownPanel.reset = function () {
        if (buttonsTable.tr[0].childNodes.length > 0) {
            drillDownPanel.buttons = {};
            while (buttonsTable.tr[0].childNodes.length > 0) {
                buttonsTable.tr[0].removeChild(buttonsTable.tr[0].childNodes[buttonsTable.tr[0].childNodes.length - 1]);
            }
        }
        drillDownPanel.changeVisibleState(false);
    }

    return drillDownPanel;
}