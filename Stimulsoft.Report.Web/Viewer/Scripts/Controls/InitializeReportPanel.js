﻿
StiJsViewer.prototype.InitializeReportPanel = function () {
    var reportPanel = document.createElement("div");
    reportPanel.id = this.controls.viewer.id + "ReportPanel";
    reportPanel.jsObject = this;
    this.controls.reportPanel = reportPanel;
    this.controls.mainPanel.appendChild(reportPanel);
    reportPanel.style.textAlign = this.options.appearance.pageAlignment == "default" ? "center" : this.options.appearance.pageAlignment;
    reportPanel.className = "stiJsViewerReportPanel";
    reportPanel.style.bottom = "0";
    reportPanel.pages = [];
    reportPanel.touchesLength = 0;
    if (this.options.isMobileDevice) reportPanel.style.transition = "margin 200ms ease";
    
    reportPanel.correctMargins = function () {
        if (this.jsObject.options.toolbar.displayMode == "Separated" && this.jsObject.options.toolbar.visible) {
            if (this.jsObject.options.isMobileDevice) {
                this.style.marginBottom =
                    this.jsObject.options.toolbar.autoHide ? "0" : "0.5in";
            }
            else {
                this.style.marginBottom =
                    this.jsObject.options.viewerHeightType == "Percentage" &&
                    !this.jsObject.options.appearance.fullScreenMode &&
                    !this.jsObject.options.appearance.scrollbarsMode ? "0" : "35px";
            }
        }
    }
    
    reportPanel.addPage = function (pageAttributes) {
        if (!pageAttributes) return null;

        var page = document.createElement("div");
        page.jsObject = this.jsObject;
        reportPanel.appendChild(page);
        reportPanel.pages.push(page);

        page.loadContent = function (pageContent) {
            page.style.display = "inline-block";
            var pageAttributes = pageContent[0];
            page.style.background = pageAttributes["background"] == "Transparent" ? "White" : pageAttributes["background"];
            page.innerHTML = pageAttributes["content"];
        }

        page.className = this.jsObject.options.appearance.showPageShadow ? "stiJsViewerPageShadow" : "stiJsViewerPage";

        var pageSizes = pageAttributes["sizes"].split(";");
        var marginsPx = pageAttributes["margins"].split(" ");
        var margins = [];
        for (var i = 0; i < marginsPx.length; i++) {
            margins.push(parseInt(marginsPx[i].replace("px", "")));
        }

        page.margins = margins;
        page.pageWidth = parseInt(pageSizes[0]);
        page.pageHeight = parseInt(pageSizes[1]);
        page.style.overflow = "hidden";
        page.style.margin = this.jsObject.reportParams.viewMode == "Continuous" ? "10px auto 10px auto" : "10px";
        page.style.display = this.jsObject.reportParams.viewMode == "Continuous" ? "table" : "inline-block";
        page.style.textAlign = "left";
        page.style.verticalAlign = "top";
        page.style.padding = pageAttributes["margins"];
        page.style.border = "1px solid " + this.jsObject.options.appearance.pageBorderColor;
        page.style.color = "#000000";
        page.style.background = pageAttributes["background"] == "Transparent" ? "White" : pageAttributes["background"];
        page.style.boxSizing = "content-box";
        page.innerHTML = pageAttributes["content"];

        //fixed bug with long time execute
        if (reportPanel.jsObject.options.appearance.reportDisplayMode != "Table" && reportPanel.jsObject.reportParams.viewMode != "SinglePage") {
            setTimeout(function () {
                page.jsObject.reportParams.pagesWidth = page.offsetWidth || page.pageWidth;
                page.jsObject.reportParams.pagesHeight = page.offsetHeight || page.pageHeight;
            });
        }
        else {
            page.jsObject.reportParams.pagesWidth = page.offsetWidth || page.pageWidth;
            page.jsObject.reportParams.pagesHeight = page.offsetHeight || page.pageHeight;
        }

        if (!pageAttributes["content"]) {
            page.style.width = page.pageWidth + "px";
            page.style.height = page.pageHeight + "px";
        }

        //Correct Watermark
        for (var i = 0; i < page.childNodes.length; i++) {
            if (page.childNodes[i].style && page.childNodes[i].style.backgroundImage) {
                page.style.backgroundImage = page.childNodes[i].style.backgroundImage;
                page.style.backgroundRepeat = "no-repeat";
                page.style.backgroundSize = "contain";

                page.childNodes[i].style.backgroundImage = "";
                page.childNodes[i].style.backgroundColor = "";
                break;
            }
        }

        if (reportPanel.jsObject.options.appearance.reportDisplayMode == "Div" || reportPanel.jsObject.options.appearance.reportDisplayMode == "Span") {
            var childs = page.getElementsByClassName("StiPageContainer");
            if (childs && childs.length > 0) {
                var pageContainer = childs[0];
                pageContainer.style.position = "relative";
                if (reportPanel.jsObject.options.appearance.reportDisplayMode == "Span") pageContainer.style.margin = "0 1px"; // fix Chrome bug with SPAN position
                page.style.width = (page.pageWidth - page.margins[1] - page.margins[3]) + "px";
                page.style.height = (page.pageHeight - page.margins[0] - page.margins[2]) + "px";
            }
        }

        //fixed bug with long time execute
        if (reportPanel.jsObject.options.appearance.reportDisplayMode != "Table" && reportPanel.jsObject.reportParams.viewMode != "SinglePage") {
            setTimeout(function () {
                var currentPageHeight = page.offsetHeight - margins[0] - margins[2];
                if (reportPanel.maxHeights[pageSizes[1]] == null || currentPageHeight > reportPanel.maxHeights[pageSizes[1]])
                    reportPanel.maxHeights[pageSizes[1]] = currentPageHeight;
            });
        }
        else {
            var currentPageHeight = page.offsetHeight - margins[0] - margins[2];
            if (reportPanel.maxHeights[pageSizes[1]] == null || currentPageHeight > reportPanel.maxHeights[pageSizes[1]])
                reportPanel.maxHeights[pageSizes[1]] = currentPageHeight;
        }

        reportPanel.jsObject.InitializeInteractions(page);

        // Touch events
        page.touchesLength = 0;
        page.lastTouches = [{ x: 0, y: 0, time: 0 }, { x: 0, y: 0, time: 0 }];

        page.translateX = function (value) {
            var _this = this;
            this.style.transitionDuration = "300ms";
            this.style.transform = value == 0 ? "" : "translateX(" + value + "px)";
            setTimeout(function () {
                _this.style.transitionDuration = "";
            }, 300);
        }

        page.eventTouchStart = function (e) {
            this.touchAllowPageAction = this.touchesLength == 0 && Math.abs(reportPanel.offsetWidth - reportPanel.scrollWidth) <= 10;
            this.touchesLength++;

            if (this.touchAllowPageAction) {
                this.touchStartX = parseInt(e.changedTouches[0].clientX);
                this.touchStartScrollY = reportPanel.scrollTop;
            }
        }

        page.eventTouchMove = function (e) {
            if (this.touchAllowPageAction) {
                this.lastTouches.shift();
                this.lastTouches.push({
                    x: e.changedTouches[0].clientX,
                    y: e.changedTouches[0].clientY,
                    time: new Date().getTime()
                });

                if (reportPanel.offsetWidth == reportPanel.scrollWidth && this.touchStartScrollY == reportPanel.scrollTop) {
                    this.touchPosX = parseInt(this.lastTouches[1].x - this.touchStartX);
                    if (scrollX == 0) this.style.transform = "translateX(" + this.touchPosX + "px)";
                }
            }
        }

        page.eventTouchEnd = function (e) {
            if (this.touchesLength > 0) this.touchesLength--;
            if (this.touchAllowPageAction && this.touchesLength == 0) {
                var dX = this.lastTouches[1].x - this.lastTouches[0].x;
                var dT = new Date().getTime() - this.lastTouches[1].time;

                if (this.touchStartScrollY != reportPanel.scrollTop ||
                    (dX <= 0 && this.jsObject.reportParams.pageNumber >= this.jsObject.reportParams.pagesCount - 1) ||
                    (dX >= 0 && this.jsObject.reportParams.pageNumber <= 0)) {
                    this.translateX(0);
                }
                else if ((dX < -5 && dT <= 14 && this.lastTouches[1].x < this.touchStartX) ||
                         (dX < 0 && this.touchPosX < -this.pageWidth / 3)) {
                    this.jsObject.postAction("NextPage");
                    this.translateX(-this.pageWidth);
                }
                else if ((dX > 5 && dT <= 14 && this.lastTouches[1].x > this.touchStartX) ||
                         (dX > 0 && this.touchPosX > this.pageWidth / 3)) {
                    this.jsObject.postAction("PrevPage");
                    this.translateX(this.pageWidth);
                }
                else {
                    this.translateX(0);
                }
            }
        }
        
        if (this.jsObject.options.isMobileDevice) {
            page.addEventListener("touchstart", page.eventTouchStart);
            page.addEventListener("touchmove", page.eventTouchMove);
            page.addEventListener("touchend", page.eventTouchEnd);
        }
        
        return page;
    }

    reportPanel.eventTouchStart = function (e) {
        reportPanel.touchesLength++;
        reportPanel.touchStartX = parseInt(e.changedTouches[0].clientX);

        if (reportPanel.jsObject.options.appearance.allowTouchZoom && reportPanel.touchesLength == 1) {
            reportPanel.touchZoomFirstDistance = 0;
            reportPanel.touchZoomSecondDistance = 0;
            reportPanel.touchZoomValue = 0;
        }
    }

    reportPanel.eventTouchMove = function (e) {
        if (reportPanel.jsObject.options.appearance.allowTouchZoom && e.touches.length > 1) {
            if ("preventDefault" in e) e.preventDefault();

            reportPanel.touchZoomSecondDistance = Math.sqrt(Math.pow(e.touches[0].pageX - e.touches[1].pageX, 2) + Math.pow(e.touches[0].pageY - e.touches[1].pageY, 2));
            if (reportPanel.touchZoomFirstDistance == 0)
                reportPanel.touchZoomFirstDistance = Math.sqrt(Math.pow(e.touches[0].pageX - e.touches[1].pageX, 2) + Math.pow(e.touches[0].pageY - e.touches[1].pageY, 2));

            var touchZoomOffset = parseInt((reportPanel.touchZoomSecondDistance - reportPanel.touchZoomFirstDistance) / 2.5);
            if (Math.abs(touchZoomOffset) >= 5) {
                reportPanel.touchZoomValue = parseInt((reportPanel.jsObject.reportParams.zoom + touchZoomOffset) / 5) * 5;
                reportPanel.touchZoomValue = Math.min(Math.max(reportPanel.touchZoomValue, 20), 200);
                reportPanel.jsObject.controls.centerText.setText(reportPanel.touchZoomValue);
            }
        }
    }

    reportPanel.eventTouchEnd = function (e) {
        if (reportPanel.touchesLength > 0) reportPanel.touchesLength--;

        if (reportPanel.jsObject.options.isMobileDevice && reportPanel.jsObject.options.toolbar.autoHide) {
            if (parseInt(reportPanel.touchStartX - e.changedTouches[0].clientX) != 0) {
                reportPanel.keepToolbar();
            }
            else {
                if (reportPanel.isToolbarHidden) reportPanel.showToolbar();
                else reportPanel.hideToolbar();
            }
        }

        if (reportPanel.jsObject.options.appearance.allowTouchZoom && reportPanel.touchZoomValue != 0 && reportPanel.touchesLength == 0) {
            reportPanel.jsObject.controls.centerText.hide();
            reportPanel.jsObject.reportParams.zoom = reportPanel.touchZoomValue;
            reportPanel.jsObject.postAction("GetPages");
            if (reportPanel.jsObject.options.toolbar.displayMode == "Separated") {
                reportPanel.jsObject.controls.toolbar.controls.ZoomOnePage.setSelected(false);
                reportPanel.jsObject.controls.toolbar.controls.ZoomPageWidth.setSelected(false);
            }
        }
    }
    
    reportPanel.showToolbar = function () {
        if (!this.jsObject.options.isMobileDevice || !this.jsObject.options.toolbar.autoHide) return;
        if (this.toolbarHideTimer) clearTimeout(this.toolbarHideTimer);
        this.jsObject.controls.toolbar.style.opacity = this.jsObject.controls.navigatePanel.style.opacity = 0.9;
        this.jsObject.controls.toolbar.style.marginTop = this.jsObject.controls.navigatePanel.style.marginBottom = "0";
        setTimeout(function () {
            reportPanel.isToolbarHidden = false;
            reportPanel.keepToolbar();
        }, 300);
    }

    reportPanel.hideToolbar = function () {
        if (!this.jsObject.options.isMobileDevice || !this.jsObject.options.toolbar.autoHide) return;
        if (this.toolbarHideTimer) clearTimeout(this.toolbarHideTimer);
        this.toolbarHideTimer = null;
        this.jsObject.controls.toolbar.style.opacity = this.jsObject.controls.navigatePanel.style.opacity = 0;
        this.jsObject.controls.toolbar.style.marginTop = this.jsObject.controls.navigatePanel.style.marginBottom = "-0.55in";
        setTimeout(function () {
            reportPanel.isToolbarHidden = true;
        }, 300);
    }

    reportPanel.keepToolbar = function () {
        if (!this.jsObject.options.isMobileDevice || !this.jsObject.options.toolbar.autoHide || this.isToolbarHidden) return;
        if (this.toolbarHideTimer) clearTimeout(this.toolbarHideTimer);
        clearTimeout(this.toolbarHideTimer);
        this.toolbarHideTimer = setTimeout(function () {
            reportPanel.hideToolbar();
        }, 4000);
    }
    
    reportPanel.getZoomByPageWidth = function () {
        if (this.jsObject.reportParams.pagesWidth == 0) return 100;
        var newZoom = ((this.offsetWidth - 35) * this.jsObject.reportParams.zoom) / this.jsObject.reportParams.pagesWidth;
        return newZoom;
    }

    reportPanel.getZoomByPageHeight = function () {
        if (this.jsObject.reportParams.pagesHeight == 0) return 100;
        var newPagesHeight = this.jsObject.options.appearance.scrollbarsMode ? Math.min(this.jsObject.controls.viewer.offsetHeight, window.innerHeight) : window.innerHeight;
        if (this.jsObject.controls.toolbar &&
            (!this.jsObject.options.isMobileDevice || !this.jsObject.options.toolbar.autoHide)) newPagesHeight -= this.jsObject.controls.toolbar.offsetHeight;
        if (this.jsObject.controls.findPanel) newPagesHeight -= this.jsObject.controls.findPanel.offsetHeight;
        if (this.jsObject.controls.drillDownPanel) newPagesHeight -= this.jsObject.controls.drillDownPanel.offsetHeight;
        if (this.jsObject.controls.parametersPanel && this.jsObject.options.appearance.parametersPanelPosition == "Top")
            newPagesHeight -= this.jsObject.controls.parametersPanel.offsetHeight;
        if (this.jsObject.controls.navigatePanel &&
            (!this.jsObject.options.isMobileDevice || !this.jsObject.options.toolbar.autoHide)) newPagesHeight -= this.jsObject.controls.navigatePanel.offsetHeight;
        var newZoom = ((newPagesHeight - 25) * this.jsObject.reportParams.zoom) / (this.jsObject.reportParams.pagesHeight);
        return newZoom;
    }

    reportPanel.addPages = function () {
        if (this.jsObject.reportParams.pagesArray == null) return;
        reportPanel.style.top = (this.jsObject.options.isMobileDevice && this.jsObject.options.toolbar.autoHide)
            ? "0"
            : this.jsObject.options.toolbar.visible
                ? (this.jsObject.options.viewerHeightType != "Percentage" || this.jsObject.options.appearance.scrollbarsMode ? this.jsObject.controls.toolbar.offsetHeight + "px" : "0")
                : "0";
        this.clear();
        this.maxHeights = {};
        var count = this.jsObject.reportParams.pagesArray.length;

        //add pages styles
        if (!this.jsObject.controls.css) this.jsObject.controls.css = document.getElementById(this.jsObject.options.viewerId + "Styles");
        if (!this.jsObject.controls.css) {
            this.jsObject.controls.css = document.createElement("STYLE");
            this.jsObject.controls.css.id = this.jsObject.options.viewerId + "Styles";
            this.jsObject.controls.css.setAttribute('type', 'text/css');
            this.jsObject.controls.head.appendChild(this.jsObject.controls.css);
        }
        if (this.jsObject.controls.css.styleSheet) this.jsObject.controls.css.styleSheet.cssText = this.jsObject.reportParams.pagesArray[count - 2];
        else this.jsObject.controls.css.innerHTML = this.jsObject.reportParams.pagesArray[count - 2];

        //add chart scripts
        var currChartScripts = document.getElementById(this.jsObject.options.viewerId + "chartScriptJsViewer");
        if (currChartScripts) this.jsObject.controls.head.removeChild(currChartScripts);

        if (this.jsObject.reportParams.pagesArray[count - 1]) {
            var chartScripts = document.createElement("Script");
            chartScripts.setAttribute('type', 'text/javascript');
            chartScripts.id = this.jsObject.options.viewerId + "chartScriptJsViewer";
            chartScripts.textContent = this.jsObject.reportParams.pagesArray[count - 1];
            this.jsObject.controls.head.appendChild(chartScripts);
        }        
        for (num = 0; num <= count - 3; num++) {
            var page = this.addPage(this.jsObject.reportParams.pagesArray[num]);
        }
        reportPanel.correctHeights();

        if (typeof stiEvalCharts === "function") stiEvalCharts();

        if (this.jsObject.options.editableMode) this.jsObject.ShowAllEditableFields();
        this.jsObject.UpdateAllHyperLinks();
    }

    reportPanel.clear = function () {
        while (this.childNodes[0]) {
            this.removeChild(this.childNodes[0]);
        }
        reportPanel.pages = [];
    }

    reportPanel.correctHeights = function () {
        for (var i in this.childNodes) {
            if (this.childNodes[i].pageHeight != null) {
                var height = reportPanel.maxHeights[this.childNodes[i].pageHeight.toString()];
                if (height) this.childNodes[i].style.height = height + "px";
            }
        }
    }
    
    reportPanel.pagesNavigationIsActive = function () {
        return (this.jsObject.options.appearance.fullScreenMode || this.jsObject.options.appearance.scrollbarsMode) && this.jsObject.reportParams.viewMode == "Continuous";
    }

    reportPanel.updateToolbarStateByPagePosition = function () {
        var reportParams = this.jsObject.reportParams;
        var commonPagesHeight = 0;
        var index = 0;

        for (index = 0; index < reportPanel.pages.length; index++) {
            commonPagesHeight += reportPanel.pages[index].offsetHeight + 20;
            if (commonPagesHeight > reportPanel.scrollTop) break;
        }

        if (index < reportParams.pagesCount && index >= 0 && index != reportParams.pageNumber) {
            this.jsObject.reportParams.pageNumber = index;
            if (this.jsObject.controls.toolbar) this.jsObject.controls.toolbar.changeToolBarState();
        }
    }

    reportPanel.onscroll = function () {
        if (reportPanel.pagesNavigationIsActive()) {
            clearTimeout(reportPanel.scrollTimer);

            var this_ = this;
            reportPanel.scrollTimer = setTimeout(function () {
                reportPanel.updateToolbarStateByPagePosition();
            }, 300);
        }
    }

    reportPanel.addEventListener("touchstart", reportPanel.eventTouchStart);
    reportPanel.addEventListener("touchmove", reportPanel.eventTouchMove);
    reportPanel.addEventListener("touchend", reportPanel.eventTouchEnd);
}