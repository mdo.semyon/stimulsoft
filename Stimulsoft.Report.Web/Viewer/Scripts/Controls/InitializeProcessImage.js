﻿
StiJsViewer.prototype.InitializeProcessImage = function () {
    var processImage = this.Progress();
    processImage.jsObject = this;
    processImage.style.display = "none";
    this.controls.processImage = processImage;
    this.controls.mainPanel.appendChild(processImage);
    processImage.style.left = "calc(50% - 50px)";

    if (this.options.appearance.fullScreenMode) {
        processImage.style.top = "calc(50% - 125px)";
    }
    else {
        processImage.style.top = "250px";
    }

    processImage.show = function () {
        this.style.display = "";
        //this.jsObject.setObjectToCenter(this);
    }

    processImage.hide = function () {
        this.style.display = "none";
    }

    return processImage;
}

StiJsViewer.prototype.Progress = function () {
    var progressContainer = document.createElement("div");
    progressContainer.style.position = "absolute";
    progressContainer.style.zIndex = "1000";

    var progress = document.createElement("div");
    progressContainer.appendChild(progress);
    progress.className = "js_viewer_loader";

    return progressContainer;
}

StiJsViewer.prototype.InitializeCenterText = function () {
    var centerTextContainer = document.createElement("div");
    centerTextContainer.style.position = "absolute";
    centerTextContainer.style.zIndex = "1000";
    centerTextContainer.style.display = "none";
    centerTextContainer.style.opacity = 0;
    centerTextContainer.style.transitionProperty = "opacity";
    centerTextContainer.style.transitionDuration = "300ms";
    centerTextContainer.style.fontFamily = this.options.toolbarFontFamily;
    centerTextContainer.style.color = this.options.toolbarFontColor;
    centerTextContainer.style.textShadow = "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000";
    centerTextContainer.style.fontSize = "100px";

    var centerText = document.createElement("div");
    centerTextContainer.jsObject = this;
    centerTextContainer.text = centerText;
    centerTextContainer.appendChild(centerText);

    this.controls.centerText = centerTextContainer;
    this.controls.mainPanel.appendChild(centerTextContainer);

    centerTextContainer.show = function () {
        this.isAnimationProcess = true;
        this.toolbarHideTimer = null;
        this.style.display = "";
        this.jsObject.setObjectToCenter(this);

        setTimeout(function () {
            centerTextContainer.style.opacity = 1;
        });
        if (this.hideTimer) clearTimeout(this.hideTimer);
        this.hideTimer = setTimeout(function () {
            centerTextContainer.hide();
        }, 2000);
    }

    centerTextContainer.hide = function () {
        this.style.opacity = 0;
        if (this.hideTimer) clearTimeout(this.hideTimer);
        this.hideTimer = setTimeout(function () {
            centerTextContainer.style.display = "none";
        }, 300);
    }

    centerTextContainer.setText = function (text) {
        centerTextContainer.text.innerHTML = text;
        centerTextContainer.show();
    }

    return centerTextContainer;
}