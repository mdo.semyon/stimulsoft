﻿
StiJsViewer.prototype.SmallButton = function (name, captionText, imageName, toolTip, arrow, styleName) {
    var button = document.createElement("div");
    button.style.fontFamily = this.options.toolbar.fontFamily;
    button.jsObject = this;
    button.name = name;
    button.styleName = styleName || "stiJsViewerStandartSmallButton";
    button.isEnabled = true;
    button.isSelected = false;
    button.isOver = false;
    button.className = button.styleName + " " + button.styleName + "Default";
    button.toolTip = toolTip;
    button.style.height = this.options.isTouchDevice ? "28px" : "23px";
    button.style.boxSizing = "content-box";
    if (name) {
        if (!this.controls.buttons) this.controls.buttons = {};
        this.controls.buttons[name] = button;
    }

    var innerTable = this.CreateHTMLTable();
    button.innerTable = innerTable;
    innerTable.style.height = "100%";
    button.appendChild(innerTable);

    if (imageName != null) {
        button.image = document.createElement("img");
        if (this.collections.images[imageName]) button.image.src = this.collections.images[imageName];
        button.imageCell = innerTable.addCell(button.image);
        button.imageCell.style.lineHeight = "0";
        button.imageCell.style.padding = (this.options.isTouchDevice && captionText == null) ? "0 7px" : "0 3px";
    }

    if (captionText != null) {
        button.caption = innerTable.addCell();
        button.caption.style.padding = (arrow ? "1px 0 " : "1px 5px ") + (imageName ? "0 0" : "0 5px");
        button.caption.style.whiteSpace = "nowrap";
        button.caption.style.textAlign = "left";
        button.caption.innerHTML = captionText;
    }

    if (arrow != null) {
        button.arrow = document.createElement("img");
        button.arrow.src = this.collections.images["ButtonArrow" + arrow + ".png"];
        innerTable.addCell(button.arrow).style.padding = captionText ? "0 5px 0 5px" : (this.options.isTouchDevice ? "0 7px 0 0" : "0 5px 0 2px");
        button.arrow.style.marginTop = "1px";
        button.arrow.style.verticalAlign = "baseline";
    }

    if (toolTip && typeof (toolTip) != "object") {
        button.setAttribute("title", toolTip);
    }

    button.onmouseoverAction = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick || (this["haveMenu"] && this.isSelected)) return;
        this.className = this.styleName + " " + this.styleName + "Over";
        this.isOver = true;
        if (!this.jsObject.options.isTouchDevice && this.jsObject.options.appearance.showTooltips && this.toolTip && typeof (this.toolTip) == "object") {
            this.jsObject.controls.toolTip.showWithDelay(
                this.toolTip[0],
                this.toolTip[1],
                this.toolTip.length == 3 && this.toolTip[2].left ? this.toolTip[2].left : this.jsObject.FindPosX(this, "stiJsViewerMainPanel"),
                this.toolTip.length == 3 && this.toolTip[2].top ? this.toolTip[2].top : this.jsObject.controls.toolbar.offsetHeight,
                this.toolTip.length == 3 && this.toolTip[2].rightToLeft ? this.offsetWidth : null
            );
        }
    }

    button.onmouseoutAction = function () {
        this.isOver = false;
        if (!this.isEnabled) return;
        this.className = this.styleName + " " + this.styleName + (this.isSelected ? "Selected" : "Default");
        if (this.jsObject.options.appearance.showTooltips && this.toolTip && typeof (this.toolTip) == "object") this.jsObject.controls.toolTip.hideWithDelay();
    }
        
    button.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    button.onmouseout = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseleave();
    }

    button.onmouseenter = function () {
        this.onmouseoverAction();
    }

    button.onmouseleave = function () {
        this.onmouseoutAction();
    }

    button.onmousedown = function () {
        if (this.isTouchStartFlag || !this.isEnabled) return;
        this.jsObject.options.buttonPressed = this;
    }

    button.onclick = function () {
        if (this.isTouchEndFlag || !this.isEnabled || this.jsObject.options.isTouchClick) return;
        if (this.jsObject.options.appearance.showTooltips && this.toolTip && typeof (this.toolTip) == "object") this.jsObject.controls.toolTip.hide();
        this.action();
    }

    button.ontouchend = function () {
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        var timer = setTimeout(function (buttonId) {
            this_.jsObject.options.buttonsTimer = null;
            this_.className = this_.styleName + " " + this_.styleName + "Default";
            this_.action();
        }, 150);
        this.jsObject.options.buttonsTimer = [this, this.className, timer];
        this.className = this.styleName + " " + this.styleName + "Over";
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    button.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.fingerIsMoved = false;
        this.jsObject.options.buttonPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    button.setEnabled = function (state) {
        if (this.image) this.image.style.opacity = state ? "1" : "0.5";
        if (this.arrow) this.arrow.style.opacity = state ? "1" : "0.5";
        this.isEnabled = state;
        if (!state && !this.isOver) this.isOver = false;
        this.className = this.styleName + " " + (state ? (this.styleName + (this.isOver ? "Over" : (this.isSelected ? "Selected" : "Default"))) : this.styleName + "Disabled");
    }

    button.setSelected = function (state) {
        this.isSelected = state;
        this.className = this.styleName + " " + this.styleName +
            (state ? "Selected" : (this.isEnabled ? (this.isOver ? "Over" : "Default") : "Disabled"));
    }

    button.action = function () { this.jsObject.postAction(this.name); }

    return button;
}