﻿
StiJsViewer.prototype.CreateHTMLTable = function (rowsCount, cellsCount) {
    var table = document.createElement("table");
    table.jsObject = this;
    this.clearStyles(table);
    table.cellPadding = 0;
    table.cellSpacing = 0;
    table.tbody = document.createElement("tbody");
    table.appendChild(table.tbody);
    table.tr = [];
    table.tr[0] = document.createElement("tr");
    this.clearStyles(table.tr[0]);
    table.tbody.appendChild(table.tr[0]);

    table.addCell = function (control) {
        var cell = document.createElement("td");
        this.jsObject.clearStyles(cell);
        this.tr[0].appendChild(cell);
        if (control) cell.appendChild(control);

        return cell;
    }

    table.addCellInNextRow = function (control) {
        var rowCount = this.tr.length;
        this.tr[rowCount] = document.createElement("tr");
        this.jsObject.clearStyles(this.tr[rowCount]);
        this.tbody.appendChild(this.tr[rowCount]);
        var cell = document.createElement("td");
        this.jsObject.clearStyles(cell);
        this.tr[rowCount].appendChild(cell);
        if (control) cell.appendChild(control);

        return cell;
    }

    table.addCellInLastRow = function (control) {
        var rowCount = this.tr.length;
        var cell = document.createElement("td");
        this.jsObject.clearStyles(cell);
        this.tr[rowCount - 1].appendChild(cell);
        if (control) cell.appendChild(control);

        return cell;
    }

    table.addTextCellInLastRow = function (text) {
        var rowCount = this.tr.length;
        var cell = document.createElement("td");
        this.jsObject.clearStyles(cell);
        this.tr[rowCount - 1].appendChild(cell);
        cell.innerHTML = text;

        return cell;
    }

    table.addCellInRow = function (rowNumber, control) {
        var cell = document.createElement("td");
        this.jsObject.clearStyles(cell);
        this.tr[rowNumber].appendChild(cell);
        if (control) cell.appendChild(control);

        return cell;
    }

    table.addTextCell = function (text) {
        var cell = document.createElement("td");
        this.jsObject.clearStyles(cell);
        this.tr[0].appendChild(cell);
        cell.innerHTML = text;

        return cell;
    }

    table.addRow = function () {
        var rowCount = this.tr.length;
        this.tr[rowCount] = document.createElement("tr");
        this.jsObject.clearStyles(this.tr[rowCount]);
        this.tbody.appendChild(this.tr[rowCount]);

        return this.tr[rowCount];
    }

    return table;
}

StiJsViewer.prototype.TextBlock = function (text) {
    var textBlock = document.createElement("div");
    textBlock.style.fontFamily = this.options.toolbar.fontFamily
    textBlock.style.fontSize = "12px";
    textBlock.style.paddingTop = "2px";
    textBlock.innerHTML = text;

    return textBlock;
}