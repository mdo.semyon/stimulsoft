﻿
StiJsViewer.prototype.InitializeAboutPanel = function () {
    var aboutPanel = document.createElement("div");
    this.controls.aboutPanel = aboutPanel;
    this.controls.mainPanel.appendChild(aboutPanel);
    aboutPanel.jsObject = this;
    aboutPanel.className = "stiJsViewerAboutPanel";
    aboutPanel.style.background = "url(" + this.collections.images["AboutInfo.png"] + ")";
    aboutPanel.style.display = "none";

    var header = document.createElement("div");
    header.innerHTML = "Stimulsoft Reports";
    header.className = "stiJsViewerAboutPanelHeader";
    aboutPanel.appendChild(header);

    var copyRight = document.createElement("div");
    copyRight.innerHTML = "Copyright 2003-" + new Date().getFullYear() + " Stimulsoft";
    copyRight.className = "stiJsViewerAboutPanelCopyright";
    aboutPanel.appendChild(copyRight);

    var version = document.createElement("div");
    version.innerHTML = "Version " + this.options.productVersion.trim();
    if (!this.options.jsMode) version.innerHTML += ", " + this.options.frameworkType;
    version.innerHTML += ", JS";
    version.className = "stiJsViewerAboutPanelVersion";
    aboutPanel.appendChild(version);

    var allRight = document.createElement("div");
    allRight.innerHTML = "All rights reserved";
    allRight.className = "stiJsViewerAboutPanelVersion";
    aboutPanel.appendChild(allRight);

    var stiLink = document.createElement("div");
    stiLink.innerHTML = "www.stimulsoft.com";
    stiLink.className = "stiJsViewerAboutPanelStiLink";
    aboutPanel.appendChild(stiLink);

    stiLink.onclick = function (event) {
        if (event) {
            event.stopPropagation();
            event.preventDefault();
        }
        aboutPanel.jsObject.openNewWindow("https://www.stimulsoft.com");
    };

    aboutPanel.ontouchend = function () { this.changeVisibleState(false); }
    aboutPanel.onclick = function () { this.changeVisibleState(false); }

    aboutPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        this.jsObject.setObjectToCenter(this);
        this.jsObject.controls.disabledPanels[2].changeVisibleState(state);
    }

    return aboutPanel;
}