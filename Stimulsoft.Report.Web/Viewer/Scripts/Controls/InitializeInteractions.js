﻿
StiJsViewer.prototype.InitializeInteractions = function (page) {

    page.getComponentOffset = function (component) {
        var offsetX = 0;
        var offsetY = 0;
        var startComponent = component;
        while (component && !isNaN(component.offsetLeft) && !isNaN(component.offsetTop)
                && (component == startComponent || component.style.position == "" || component.style.position == "static")) {
            offsetX += component.offsetLeft - component.scrollLeft;
            offsetY += component.offsetTop - component.scrollTop;
            component = component.offsetParent;
        }
        return { top: offsetY, left: offsetX };
    }

    page.paintSortingArrow = function (component, sort) {
        var arrowImg = document.createElement("img");
        arrowImg.src = sort == "asc" ? this.jsObject.collections.images["ArrowDown.png"] : this.jsObject.collections.images["ArrowUp.png"];
        var arrowWidth = (this.jsObject.reportParams.zoom / 100) * 9;
        var arrowHeight = (this.jsObject.reportParams.zoom / 100) * 5;        
        arrowImg.style.position = "absolute";
        arrowImg.style.width = arrowWidth + "px";
        arrowImg.style.height = arrowHeight + "px";
        component.appendChild(arrowImg);

        var oldPosition = component.style.position;
        var oldClassName = component.className;
        component.style.position = "relative";
        if (!oldClassName) component.className = "stiSortingParentElement";

        var arrowLeftPos = this.jsObject.FindPosX(arrowImg, component.className);
        var arrowTopPos = this.jsObject.FindPosY(arrowImg, component.className);
        
        arrowImg.style.marginLeft = (component.offsetWidth - arrowLeftPos - arrowWidth - ((this.jsObject.reportParams.zoom / 100) * 3)) + "px";
        arrowImg.style.marginTop = (component.offsetHeight / 2 - arrowHeight / 2 - arrowTopPos) + "px";
        component.style.position = oldPosition;
        component.className = oldClassName;
    }

    page.paintCollapsingIcon = function (component, collapsed) {
        var collapsImg = document.createElement("img");
        collapsImg.src = collapsed ? this.jsObject.collections.images["CollapsingPlus.png"] : this.jsObject.collections.images["CollapsingMinus.png"];
        collapsImg.style.position = "absolute";
        var collapsWidth = (this.jsObject.reportParams.zoom / 100) * 10;
        var collapsHeight = (this.jsObject.reportParams.zoom / 100) * 10;
        collapsImg.style.width = collapsWidth + "px";
        collapsImg.style.height = collapsHeight + "px";
        component.appendChild(collapsImg);

        var componentOffset = page.getComponentOffset(component);
        var collapsOffset = page.getComponentOffset(collapsImg);
        collapsImg.style.marginLeft = (componentOffset.left - collapsOffset.left + collapsWidth / 3) + "px";
        collapsImg.style.marginTop = (componentOffset.top - collapsOffset.top + collapsWidth / 3) + "px";
    }

    page.postInteractionSorting = function (component, isCtrl) {
        var params = {
            "action": "Sorting",
            "sortingParameters": {
                "ComponentName": component.getAttribute("interaction") + ";" + isCtrl.toString(),
                "DataBand": component.getAttribute("databandsort")
            }
        };

        if (this.jsObject.controls.parametersPanel) {
            params.variables = this.jsObject.controls.parametersPanel.getParametersValues();
        }

        this.jsObject.postInteraction(params);
    }

    page.postInteractionDrillDown = function (component) {
        var params = {
            "action": "DrillDown",
            "drillDownParameters": {
                "ComponentIndex": component.getAttribute("compindex"),
                "ElementIndex": component.getAttribute("elementindex"),
                "PageIndex": component.getAttribute("pageindex"),
                "PageGuid": component.getAttribute("pageguid"),
                "ReportFile": component.getAttribute("reportfile")
            }
        };

        this.jsObject.postInteraction(params);
    }

    page.postInteractionCollapsing = function (component) {
        var componentName = component.getAttribute("interaction");
        var collapsingIndex = component.getAttribute("compindex");
        var collapsed = component.getAttribute("collapsed") == "true" ? false : true;

        if (!this.jsObject.reportParams.interactionCollapsingStates) this.jsObject.reportParams.interactionCollapsingStates = {};
        if (!this.jsObject.reportParams.interactionCollapsingStates[componentName]) this.jsObject.reportParams.interactionCollapsingStates[componentName] = {};
        this.jsObject.reportParams.interactionCollapsingStates[componentName][collapsingIndex] = collapsed;

        var params = {
            "action": "Collapsing",
            "collapsingParameters": {
                "ComponentName": componentName,
                "InteractionCollapsingStates": this.jsObject.reportParams.interactionCollapsingStates
            }
        };

        if (this.jsObject.controls.parametersPanel) {
            params.variables = this.jsObject.controls.parametersPanel.getParametersValues();
        }

        this.jsObject.postInteraction(params);
    }

    var elems = page.querySelectorAll ? page.querySelectorAll("td,div,span,rect,path,ellipse") : page.getElementsByTagName("td");
    var collapsedHash = [];
    for (var i = 0; i < elems.length; i++) {
        if (elems[i].getAttribute("interaction") && (
                elems[i].getAttribute("pageguid") ||
                elems[i].getAttribute("reportfile") ||
                elems[i].getAttribute("collapsed") ||
                elems[i].getAttribute("databandsort"))) {

            elems[i].style.cursor = "pointer";
            elems[i].jsObject = this;

            var sort = elems[i].getAttribute("sort");
            if (sort) {
                page.paintSortingArrow(elems[i], sort);
            }

            var collapsed = elems[i].getAttribute("collapsed");
            if (collapsed) {
                var compId = elems[i].getAttribute("compindex") + "|" + elems[i].getAttribute("interaction");
                if (collapsedHash.indexOf(compId) < 0) {
                    page.paintCollapsingIcon(elems[i], collapsed == "true");
                    collapsedHash.push(compId);
                }
            }

            elems[i].onclick = function (e) {
                if (this.getAttribute("pageguid") || this.getAttribute("reportfile")) page.postInteractionDrillDown(this);
                else if (this.getAttribute("collapsed")) page.postInteractionCollapsing(this);
                else page.postInteractionSorting(this, e.ctrlKey);
            }

            if (elems[i].getAttribute("pageguid") || elems[i].getAttribute("reportfile")) {
                elems[i].onmouseover = function (e) { this.style.opacity = 0.75; }
                elems[i].onmouseout = function (e) { this.style.opacity = 1; }
            }
        }
    }
}