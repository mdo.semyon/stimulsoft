#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Export;

namespace Stimulsoft.Report.Web
{
    [ToolboxBitmap(typeof(StiWebViewer), "Viewer.Images.StiWebViewer.bmp")]
    public partial class StiWebViewer :
        WebControl,
        INamingContainer
    {
        internal void ProcessRequest(StiRequestParams requestParams)
        {
            if (requestParams.Action != StiAction.Undefined &&
                requestParams.Component == StiComponentType.Viewer &&
                (requestParams.Id == ID || requestParams.Action == StiAction.Resource))
            {
                this.requestParams = requestParams;
                this.ClientGuid = requestParams.Cache.ClientGuid;
                InvokeViewerEvent();

                StiExportSettings settings = null;
                StiWebActionResult result = null;

                switch (requestParams.Action)
                {
                    case StiAction.Resource:
                        result = StiViewerResourcesHelper.Get(requestParams);
                        break;

                    case StiAction.GetReport:
                        InvokeGetReport();
                        result = StiReportHelper.ViewerResult(requestParams, Report);
                        break;

                    case StiAction.OpenReport:
                        result = StiReportHelper.OpenReportResult(requestParams);
                        break;

                    case StiAction.GetPages:
                        result = StiReportHelper.ViewerResult(requestParams, Report);
                        break;

                    case StiAction.RefreshReport:
                        InvokeGetReportData();
                        result = StiReportHelper.InteractionResult(requestParams, Report);
                        break;

                    case StiAction.PrintReport:
                        settings = GetExportSettings(requestParams);
                        InvokePrintReport(settings);
                        result = StiExportsHelper.PrintReportResult(requestParams, Report, settings);
                        break;

                    case StiAction.ExportReport:
                        settings = GetExportSettings(requestParams);
                        InvokeExportReport(settings);
                        result = StiExportsHelper.ExportReportResult(requestParams, Report, settings);
                        if (ExportReportResponse != null)
                        {
                            var stream = new MemoryStream(result.Data);
                            InvokeExportReportResponse(settings, stream, result.FileName, result.ContentType);
                            result = new StiWebActionResult(stream, result.ContentType, result.FileName);
                        }
                        break;

                    case StiAction.EmailReport:
                        settings = GetExportSettings(requestParams);
                        StiEmailOptions options = GetEmailOptions(requestParams);
                        InvokeEmailReport(settings, options);
                        result = StiExportsHelper.EmailReportResult(requestParams, Report, settings, options);
                        break;

                    case StiAction.ReportResource:
                        var resourceName = requestParams.ReportResourceParams["resourceName"] as string;
                        if (Report != null)
                        {
                            var resource = Report.Dictionary.Resources[resourceName];
                            if (resource != null && resource.Content != null)
                            {
                                var stream = new MemoryStream(resource.Content);
                                var viewType = requestParams.ReportResourceParams["viewType"] as string;
                                var fileName = viewType == "SaveFile" ? resource.Name + StiReportResourceHelper.GetResourceFileExt(resource) : null;
                                result = new StiWebActionResult(stream, StiReportResourceHelper.GetResourceContentType(resource), fileName);
                            }
                        }
                        break;

                    case StiAction.InitVars:
                        var currentReport = Report;
                        if (currentReport != null && !currentReport.IsDocument && currentReport.DataStore.Count == 0 && currentReport.Dictionary.Variables.Count > 0) InvokeGetReportData();
                        result = StiReportHelper.InteractionResult(requestParams, Report);
                        break;

                    case StiAction.Variables:
                    case StiAction.Sorting:
                    case StiAction.DrillDown:
                    case StiAction.Collapsing:
                        InvokeInteraction();
                        result = StiReportHelper.InteractionResult(requestParams, Report);
                        break;

                    case StiAction.DesignReport:
                        InvokeDesignReport();
                        break;

                    case StiAction.UpdateCache:
                        requestParams.Cache.Helper.UpdateObjectCacheInternal(requestParams, null);
                        break;
                }

                if (result != null)
                {
                    var useBrowserCache = requestParams.Action == StiAction.Resource;
                    StiReportResponse.ResponseBuffer(result.Data, result.ContentType, useBrowserCache, result.FileName);
                }
            }
        }
        
        protected override void OnInit(EventArgs e)
        {
            StiRequestParams requestParams = this.RequestParams;
            this.ProcessRequest(requestParams);
            
            base.OnInit(e);
        }

        #region Internal

        /// <summary>
        /// Get the URL for viewer requests
        /// </summary>
        private static string GetRequestUrl(bool useRelativeUrls, bool passQueryParameters)
        {
            if (HttpContext.Current == null) return null;

            Uri url = HttpContext.Current.Request.Url;

            // Get port number
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Headers["Host"]))
            {
                string[] values = HttpContext.Current.Request.Headers["Host"].Split(':');
                if (values.Length > 1)
                {
                    UriBuilder builder = new UriBuilder(url);
                    int port;
                    if (int.TryParse(values[1], out port)) builder.Port = port;
                    url = builder.Uri;
                }
            }
            
            if (useRelativeUrls) return HttpContext.Current.Response.ApplyAppPathModifier(passQueryParameters ? url.PathAndQuery : url.AbsolutePath);
            return url.AbsoluteUri;
        }
        
        /// <summary>
        /// Create default viewer RequestParams to save the report into cache
        /// </summary>
        private StiRequestParams CreateRequestParams()
        {
            StiRequestParams requestParams = new StiRequestParams();
            requestParams.Action = StiAction.GetReport;
            requestParams.Component = StiComponentType.Viewer;
            requestParams.Id = this.ID;
            requestParams.CloudMode = this.CloudMode;
            requestParams.Cache.Mode = this.CacheMode;
            requestParams.Cache.Timeout = new TimeSpan(0, this.CacheTimeout, 0);
            requestParams.Cache.Priority = this.CacheItemPriority;
            requestParams.Cache.ClientGuid = this.ClientGuid;
            requestParams.Cache.Helper = CacheHelper;
            requestParams.Viewer.PageNumber = 0;
            requestParams.Viewer.Zoom = 1;
            requestParams.Viewer.ViewMode = this.ViewMode;
            requestParams.Viewer.OpenLinksWindow = this.OpenLinksWindow;
            requestParams.Viewer.ChartRenderType = this.ChartRenderType;
            requestParams.Viewer.BookmarksPrint = this.BookmarksPrint;
            requestParams.Server.UseRelativeUrls = this.UseRelativeUrls;
            requestParams.Server.UseCompression = this.UseCompression;
            requestParams.Server.PassQueryParametersForResources = this.PassQueryParametersForResources;
            requestParams.Server.PassQueryParametersToReport = this.PassQueryParametersToReport;

            return requestParams;
        }

        private static void WriteToFile(string file, string text)
        {
            var path = Path.GetDirectoryName(file);
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);

            var stream = File.Create(file);
            var buffer = Encoding.UTF8.GetBytes(text);
            stream.Write(buffer, 0, buffer.Length);
            stream.Flush();
            stream.Close();
        }

        /// <summary>
        /// Get the URL to load the images of the web component.
        /// </summary>
        internal static string GetImageUrl(StiRequestParams requestParams, string imageName)
        {
            string imageUrl = imageName;
            if (IsDesignMode)
            {
                imageUrl = Path.Combine(Environment.GetEnvironmentVariable("Temp"), "StiWeb" + requestParams.Component.ToString());
                if (!Directory.Exists(imageUrl)) Directory.CreateDirectory(imageUrl);
                if (!string.IsNullOrEmpty(imageName))
                {
                    imageUrl = Path.Combine(imageUrl, imageName);
                    try
                    {
                        if (!File.Exists(imageUrl))
                        {
                            Bitmap bmp = StiViewerResourcesHelper.GetBitmap(requestParams, imageName);
                            bmp.Save(imageUrl);
                            bmp.Dispose();
                        }
                    }
                    catch
                    {
                    }
                }
            }

            return imageUrl.Replace("'", "\\'").Replace("\"", "&quot;");
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void SaveResourcesForJS(string path)
        {
            StiRequestParams requestParams = this.CreateRequestParams();

            #region Write styles
            
            string[] themes = Enum.GetNames(typeof(StiViewerTheme));
            for (int i = 0; i < themes.Length; i++)
            {
                string fileName = themes[i];
                if (fileName.StartsWith("Simple")) fileName = fileName.Insert("Simple".Length, ".");
                else if (fileName.StartsWith("Office") && fileName != "Office2003") fileName = fileName.Insert("Office20xx".Length, ".");

                requestParams.Theme = themes[i];
                string styles = Encoding.UTF8.GetString(StiViewerResourcesHelper.GetStyles(requestParams));
                WriteToFile(Path.Combine(path, "Css", "stimulsoft.viewer." + fileName.ToLower() + ".css"), styles);
            }

            #endregion

            #region Write scripts

            requestParams.Theme = "Office2013";
            string jsParameters = RenderJsonParameters();
            string scripts = Encoding.UTF8.GetString(StiViewerResourcesHelper.GetScripts(requestParams));
            scripts = scripts.Replace("this.defaultParameters = {};",
                "this.defaultParameters = " + jsParameters + ";" +
                "this.mergeOptions(parameters, this.defaultParameters); " +
                "parameters = this.defaultParameters;");
            WriteToFile(Path.Combine(path, "Scripts", "source.viewer.js"), scripts);

            #endregion
        }

        #endregion
        
        public StiWebViewer()
        {
            StiWebHelper.InitWeb();

            this.ClientIDMode = System.Web.UI.ClientIDMode.Static;
            if (this.BackColor.IsEmpty) this.BackColor = Color.White;

            if (IsDesignMode)
            {
                this.Width = Unit.Percentage(100);
                this.Height = Unit.Pixel(650);
            }
        }
        
        
        #region Obsolete

        [Obsolete("This method is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetReport()
        {
        }

        [Obsolete("This method is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetCurrentPage()
        {
        }

        [Obsolete("This method is obsolete. It is no longer used and will be removed in next versions. You can use the StiReportResponse.PrintAsPdf() method or the JavaScript jsStiWebViewer.postPrint('PrintPdf') method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void PrintToPdf()
        {
        }

        [Obsolete("This method is obsolete. It is no longer used and will be removed in next versions. You can use the StiReportResponse.PrintAsHtml() method or the JavaScript jsStiWebViewer.postPrint('PrintWithoutPreview') method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void PrintToDirect()
        {
        }

        [Obsolete("This method is obsolete. It is no longer used and will be removed in next versions. You can use the StiReportResponse.ResponseAsHtml() method in new browser window or the JavaScript jsStiWebViewer.postPrint('PrintWithPreview') method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void PrintToPopupWindow()
        {
        }

        #endregion
    }
}
