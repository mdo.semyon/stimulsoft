﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Text;
using System.Reflection;
using System.IO;
using System.Collections;
using Stimulsoft.Base.Json;
using Stimulsoft.Base.Json.Converters;
using System.Drawing;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Web
{
    internal class StiViewerResourcesHelper
    {
        #region Methods

        public static StiWebActionResult Get(StiRequestParams requestParams)
        {
            if (requestParams.Resource == "scripts")
            {
                byte[] bytes = GetScripts(requestParams);
                return new StiWebActionResult(bytes, "text/javascript");
            }

            if (requestParams.Resource == "styles")
            {
                byte[] bytes = GetStyles(requestParams);
                return new StiWebActionResult(bytes, "text/css");
            }

            if (requestParams.Resource != null && requestParams.Resource.EndsWith(".swf"))
            {
                byte[] bytes = GetFlashResources(requestParams);
                return new StiWebActionResult(bytes, "application/x-shockwave-flash");
            }

            return new StiWebActionResult();
        }

        #endregion

        #region Images

        /// <summary>
        /// Get Bitmap image for Visual Studio design mode
        /// </summary>
        public static Bitmap GetBitmap(StiRequestParams requestParams, string imageName)
        {
            Assembly assembly = typeof(StiViewerResourcesHelper).Assembly;
            string resourcePath = string.Format("{0}.{1}.Images.{2}", typeof(StiViewerResourcesHelper).Namespace, requestParams.Component, imageName);
            Stream stream = assembly.GetManifestResourceStream(resourcePath);
            if (stream == null) return null;
            return new Bitmap(stream);
        }

        private static Hashtable GetImagesArray(StiRequestParams requestParams)
        {
            Hashtable images = new Hashtable();
            Assembly assembly = typeof(StiViewerResourcesHelper).Assembly;
            string[] names = assembly.GetManifestResourceNames();
            string imagesPath = string.Format("{0}.{1}.Images.", typeof(StiViewerResourcesHelper).Namespace, requestParams.Component.ToString());
            string imagesThemePath = string.Format("{0}{1}.", imagesPath, requestParams.Theme);
            string imageName;

            foreach (var name in names)
            {
                if (name.EndsWith(".png") || name.EndsWith(".gif"))
                {
                    if (name.StartsWith(imagesThemePath))
                    {
                        imageName = name.Replace(imagesThemePath, "");
                        images[imageName] = GetImageData(assembly, name);
                    }
                    else if (name.StartsWith(imagesPath))
                    {
                        imageName = name.Replace(imagesPath, "");
                        if (!images.Contains(imageName)) images[imageName] = GetImageData(assembly, name);
                    }
                }
            }

            return images;
        }

        private static string GetImageData(Assembly assembly, string name)
        {
            Stream stream = assembly.GetManifestResourceStream(name);
            if (stream == null) return null;

            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            byte[] buffer = ms.ToArray();
            ms.Dispose();
            stream.Dispose();

            return string.Format("data:image/{0};base64,{1}", name.Substring(name.Length - 3), Convert.ToBase64String(buffer));
        }

        #endregion

        #region Scripts

        private static string GetCollections(StiRequestParams requestParams)
        {
            Hashtable collections = new Hashtable();
            collections["images"] = GetImagesArray(requestParams);
            collections["loc"] = StiCollectionsHelper.GetLocalizationItems(requestParams);
            collections["encodingData"] = StiCollectionsHelper.GetEncodingDataItems();
            collections["dateRanges"] = StiCollectionsHelper.GetDateRangesItems();
            collections["months"] = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            collections["dayOfWeek"] = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

            return JsonConvert.SerializeObject(collections, Stimulsoft.Base.Json.Formatting.None, new StringEnumConverter());
        }

        public static byte[] GetScripts(StiRequestParams requestParams)
        {
            string jsCacheGuid = "ViewerScripts_" + StiMD5Helper.ComputeHash(requestParams.Theme.ToString() + requestParams.Localization + requestParams.Version);
            if (requestParams.Component == StiComponentType.Viewer)
            {
                string jsCacheResult = requestParams.Cache.Helper.GetResourceInternal(requestParams, jsCacheGuid);
                if (!string.IsNullOrEmpty(jsCacheResult)) return Encoding.UTF8.GetBytes(jsCacheResult);
            }

            Assembly assembly = typeof(StiViewerResourcesHelper).Assembly;
            string[] resourceNames = assembly.GetManifestResourceNames();
            string jsPath = string.Format("{0}.{1}.Scripts.", typeof(StiViewerResourcesHelper).Namespace, requestParams.Component);
            string jsResult = string.Empty;

            foreach (var name in resourceNames)
            {
                if (name.IndexOf(jsPath) == 0 && name.EndsWith(".js"))
                {
                    Stream stream = assembly.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string script = reader.ReadToEnd();
                        if (name.EndsWith(".Main.js") && requestParams.Component == StiComponentType.Viewer)
                        {
                            var collections = string.Format("this.collections = {0}", GetCollections(requestParams));
                            script = script.Replace("this.collections = {};", collections);
                        }
                        jsResult = string.Format("{0}{1}\r\n", jsResult, script);
                    }
                    stream.Dispose();
                }
            }

            if (requestParams.Component == StiComponentType.Viewer)
                requestParams.Cache.Helper.SaveResourceInternal(requestParams, jsResult, jsCacheGuid);

            return Encoding.UTF8.GetBytes(jsResult);
        }

        #endregion

        #region Styles

        private static Hashtable GetStylesConstants(string css)
        {
            Hashtable constants = new Hashtable();
            string[] constantsArray = css.Split(';');
            for (var i = 0; i < constantsArray.Length; i++)
            {
                var index = constantsArray[i].IndexOf('@');
                string[] values = constantsArray[i].Substring(index >= 0 ? index : 0).Split('=');
                if (values.Length == 2) constants[values[0].Trim()] = values[1].Trim();
            }

            return constants;
        }

        public static byte[] GetStyles(StiRequestParams requestParams)
        {
            string stylesCacheGuid = "ViewerStyles_" + StiMD5Helper.ComputeHash(requestParams.Theme.ToString() + requestParams.Version);
            string stylesCacheResult = requestParams.Cache.Helper.GetResourceInternal(requestParams, stylesCacheGuid);
            if (!string.IsNullOrEmpty(stylesCacheResult)) return Encoding.UTF8.GetBytes(stylesCacheResult);

            Assembly assembly = typeof(StiViewerResourcesHelper).Assembly;
            string[] resourceNames = assembly.GetManifestResourceNames();
            string stylesResult = string.Empty;

            string stylesFolder = requestParams.Theme;
            if (requestParams.Theme.StartsWith("Simple")) stylesFolder = "Simple";
            else if (requestParams.Theme.StartsWith("Office2007")) stylesFolder = "Office2007";
            else if (requestParams.Theme.StartsWith("Office2010")) stylesFolder = "Office2010";
            else if (requestParams.Theme.StartsWith("Office2013")) stylesFolder = "Office2013";

            string stylesPath = string.Format("{0}.{1}.Styles.{2}.", typeof(StiViewerResourcesHelper).Namespace, requestParams.Component, stylesFolder);
            Hashtable constants = null;

            foreach (var name in resourceNames)
            {
                if (name.IndexOf(stylesPath) == 0 && name.EndsWith(".css"))
                {
                    Stream stream = assembly.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string styleText = reader.ReadToEnd();
                        if (name.EndsWith(requestParams.Theme + ".Constants.css")) constants = GetStylesConstants(styleText);
                        else if (!name.EndsWith(".Constants.css")) stylesResult += styleText + "\r\n";
                    }
                }
            }

            if (constants != null)
            {
                foreach (DictionaryEntry constant in constants)
                {
                    stylesResult = stylesResult.Replace((string)constant.Key, (string)constant.Value);
                }
            }

            requestParams.Cache.Helper.SaveResourceInternal(requestParams, stylesResult, stylesCacheGuid);

            return Encoding.UTF8.GetBytes(stylesResult);
        }

        #endregion

        #region Flash

        public static byte[] GetFlashResources(StiRequestParams requestParams)
        {
            Assembly assembly = typeof(StiViewerResourcesHelper).Assembly;
            string resourcePath = string.Format("{0}.{1}.Resources.{2}", typeof(StiViewerResourcesHelper).Namespace, requestParams.Component, requestParams.Resource);
            Stream stream = assembly.GetManifestResourceStream(resourcePath);
            if (stream == null) return null;

            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            byte[] buffer = ms.ToArray();
            ms.Dispose();
            stream.Dispose();

            return buffer;
        }

        #endregion
    }
}
