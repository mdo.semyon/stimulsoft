﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Components;
using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;

#if NETCORE
using Stimulsoft.System.Web;
using Stimulsoft.System.Web.Caching;
#else
using System.Web.Caching;
#endif

namespace Stimulsoft.Report.Web
{
    public class StiCacheHelper
    {
        #region Constants: Cache guids
        public const string GUID_ReportSnapshot = "";
        public const string GUID_ReportTemplate = "template";
        public const string GUID_Clipboard = "clipboard";
        public const string GUID_ComponentClone = "clone";
        public const string GUID_ReportCheckers = "checkers";
        public const string GUID_UndoArray = "undo";
        #endregion

        #region Fields
        private string currentCacheGuid;
        #endregion

        #region Properties
        public HttpContext HttpContext { get; set; }

        public StiServerCacheMode CacheMode { get; set; }

        public TimeSpan Timeout { get; set; }

        public CacheItemPriority Priority { get; set; }

        public StiCacheObjectType CacheObjectType
        {
            get
            {
                if (string.IsNullOrEmpty(currentCacheGuid)) return StiCacheObjectType.Undefined;
                if (currentCacheGuid.EndsWith(GUID_ReportTemplate)) return StiCacheObjectType.ReportTemplate;
                if (currentCacheGuid.EndsWith(GUID_Clipboard)) return StiCacheObjectType.Clipboard;
                if (currentCacheGuid.EndsWith(GUID_ComponentClone)) return StiCacheObjectType.ComponentClone;
                if (currentCacheGuid.EndsWith(GUID_ReportCheckers)) return StiCacheObjectType.ReportCheckers;
                if (currentCacheGuid.EndsWith(GUID_UndoArray)) return StiCacheObjectType.UndoArray;
                return StiCacheObjectType.ReportSnapshot;
            }
        }
        #endregion

        #region Methods: Public virtual
        /// <summary>
        /// Get the object container for designer from cache.
        /// </summary>
        public virtual object GetObject(string guid)
        {
            object cacheData = null;

            // Update server cache
            if (CacheMode == StiServerCacheMode.ObjectCache || CacheMode == StiServerCacheMode.StringCache)
            {
                cacheData = HttpContext.Cache[guid];
                if (cacheData != null)
                {
                    HttpContext.Cache.Remove(guid);
                    HttpContext.Cache.Add(guid, cacheData, null, Cache.NoAbsoluteExpiration, Timeout, Priority, null);
                }
            }
            else if (this.CacheMode != StiServerCacheMode.None)
            {
                cacheData = HttpContext.Session[guid];
                if (cacheData != null)
                {
                    HttpContext.Session.Remove(guid);
                    HttpContext.Session.Add(guid, cacheData);
                }
            }

            // Try to get the report as StiReport object
            if (cacheData is StiReport) return cacheData as StiReport;

            // Try to parse the string cache data
            if (cacheData is string && (CacheMode == StiServerCacheMode.StringCache || CacheMode == StiServerCacheMode.StringSession))
            {
                if (CacheObjectType == StiCacheObjectType.UndoArray || CacheObjectType == StiCacheObjectType.ReportCheckers ||
                    CacheObjectType == StiCacheObjectType.Clipboard || CacheObjectType == StiCacheObjectType.ComponentClone)
                {
                    var buffer = StiGZipHelper.Unpack(Convert.FromBase64String((string)cacheData));
                    return GetObjectFromCacheData(buffer);
                }

                var report = new StiReport();
                if (CacheObjectType == StiCacheObjectType.ReportTemplate) report.LoadPackedReportFromString(cacheData as string);
                else report.LoadPackedDocumentFromString(cacheData as string);
                report.Info.Zoom = 1;

                return report;
            }

            return cacheData;
        }

        /// <summary>
        /// Save the object container for designer to cache.
        /// </summary>
        public virtual void SaveObject(object obj, string guid)
        {
            string packedData = null;

            // Remove report object from server cache if exist
            if (CacheMode == StiServerCacheMode.ObjectCache || CacheMode == StiServerCacheMode.StringCache) HttpContext.Cache.Remove(guid);
            else HttpContext.Session.Remove(guid);
            
            // Save the report to server cache or session
            if (CacheMode == StiServerCacheMode.ObjectCache) HttpContext.Cache.Add(guid, obj, null, Cache.NoAbsoluteExpiration, Timeout, Priority, null);
            else if (CacheMode == StiServerCacheMode.ObjectSession) HttpContext.Session.Add(guid, obj);
            else if (CacheMode == StiServerCacheMode.StringCache || CacheMode == StiServerCacheMode.StringSession)
            {
                if (obj is StiReport)
                    packedData = CacheObjectType == StiCacheObjectType.ReportTemplate 
                        ? ((StiReport)obj).SavePackedReportToString()
                        : ((StiReport)obj).SavePackedDocumentToString();
                else
                    packedData = Convert.ToBase64String(StiGZipHelper.Pack(GetCacheDataFromObject(obj)));

                if (CacheMode == StiServerCacheMode.StringCache) HttpContext.Cache.Add(guid, packedData, null, Cache.NoAbsoluteExpiration, Timeout, Priority, null);
                else HttpContext.Session.Add(guid, packedData);
            }
        }

        /// <summary>
        /// Remove the object container for designer from cache.
        /// </summary>
        public virtual void RemoveObject(string guid)
        {
            if (CacheMode == StiServerCacheMode.ObjectCache || CacheMode == StiServerCacheMode.StringCache) HttpContext.Cache.Remove(guid);
            else if (CacheMode != StiServerCacheMode.None) HttpContext.Session.Remove(guid);
        }

        /// <summary>
        /// Get the report object from cache.
        /// </summary>
        public virtual StiReport GetReport(string guid)
        {
            return GetObject(guid, CacheMode, Timeout, Priority) as StiReport;
        }
        
        /// <summary>
        /// Save the report object to cache.
        /// </summary>
        public virtual void SaveReport(StiReport report, string guid)
        {
            SaveObject(report, guid, CacheMode, Timeout, Priority);
        }
        
        /// <summary>
        /// Remove the report object from cache
        /// </summary>
        public virtual void RemoveReport(string guid)
        {
            RemoveObject(guid, CacheMode);
            if (guid.IndexOf(GUID_ReportTemplate) < 0) RemoveObject($"{guid}_{GUID_ReportTemplate}", CacheMode);
        }

        /// <summary>
        /// Get the scripts or styles of the Web component from cache.
        /// </summary>
        public virtual string GetResource(string guid)
        {
            if (CacheMode == StiServerCacheMode.StringCache) CacheMode = StiServerCacheMode.ObjectCache;
            else if (CacheMode == StiServerCacheMode.StringSession) CacheMode = StiServerCacheMode.ObjectSession;

            var cacheData = GetObject(guid);
            if (cacheData is string) return (string)cacheData;

            return null;
        }

        /// <summary>
        /// Save the scripts or styles of the Web component to cache.
        /// </summary>
        public virtual void SaveResource(string text, string guid)
        {
            if (CacheMode == StiServerCacheMode.StringCache) CacheMode = StiServerCacheMode.ObjectCache;
            else if (CacheMode == StiServerCacheMode.StringSession) CacheMode = StiServerCacheMode.ObjectSession;

            SaveObject(text, guid);
        }

        /// <summary>
        /// Remove the scripts or styles of the Web component from cache
        /// </summary>
        public virtual void RemoveResource(string guid)
        {
            RemoveObject(guid);
        }
        #endregion

        #region Methods: Internal

        #region Object
        internal object GetObjectInternal(StiRequestParams requestParams, string objectGuid)
        {
            ApplyRequestParameters(requestParams);
            currentCacheGuid = string.IsNullOrEmpty(objectGuid)
                ? $"{requestParams.Id}_{requestParams.Cache.ClientGuid}"
                : $"{requestParams.Id}_{requestParams.Cache.ClientGuid}_{objectGuid}";

            return GetObject(currentCacheGuid, CacheMode, Timeout, Priority);
        }

        internal void SaveObjectInternal(object obj, StiRequestParams requestParams, string objectGuid)
        {
            ApplyRequestParameters(requestParams);
            currentCacheGuid = string.IsNullOrEmpty(objectGuid)
                ? $"{requestParams.Id}_{requestParams.Cache.ClientGuid}"
                : $"{requestParams.Id}_{requestParams.Cache.ClientGuid}_{objectGuid}";

            SaveObject(obj, currentCacheGuid, CacheMode, Timeout, Priority);
        }

        internal void UpdateObjectCacheInternal(StiRequestParams requestParams, string objectGuid)
        {
            GetObjectInternal(requestParams, objectGuid);
            if (string.IsNullOrEmpty(objectGuid))
            {
                GetObjectInternal(requestParams, GUID_ReportTemplate);
                if (!string.IsNullOrEmpty(requestParams.Cache.DrillDownGuid)) GetObjectInternal(requestParams, requestParams.Cache.DrillDownGuid);
            }
        }
        #endregion

        #region StiReport
        internal StiReport GetReportInternal(StiRequestParams requestParams)
        {
            return GetReportInternal(requestParams, true);
        }

        internal StiReport GetReportInternal(StiRequestParams requestParams, bool useDrillDownReportSnapshot)
        {
            var report = requestParams.Report;
            if (report == null && requestParams.Cache.Mode != StiServerCacheMode.None && !string.IsNullOrEmpty(requestParams.Cache.ClientGuid) &&
                requestParams.Action != StiAction.Undefined && requestParams.Action != StiAction.Resource)
            {
                ApplyRequestParameters(requestParams);

                // Check if require only a report template
                var isReportTemplateRequire = (
                    (requestParams.Component == StiComponentType.Designer || requestParams.Component == StiComponentType.DesignerFx) &&
                    requestParams.Action != StiAction.PrintReport &&
                    requestParams.Action != StiAction.ExportReport &&
                    requestParams.Action != StiAction.EmailReport
                ) || useDrillDownReportSnapshot && (
                        requestParams.Action == StiAction.InitVars ||
                        requestParams.Action == StiAction.Variables ||
                        requestParams.Action == StiAction.Sorting ||
                        requestParams.Action == StiAction.Collapsing ||
                        requestParams.Action == StiAction.DrillDown ||
                        requestParams.Action == StiAction.RefreshReport);

                // Get the report object from server cache
                if (isReportTemplateRequire)
                {
                    // Try to get the report template
                    currentCacheGuid = $"{requestParams.Id}_{requestParams.Cache.ClientGuid}_{GUID_ReportTemplate}";
                    report = GetReport(currentCacheGuid, CacheMode, Timeout, Priority);
                    if (report != null && requestParams.Action != StiAction.InitVars) report.IsRendered = false;
                }
                else
                {
                    // Try to get the report snapshot
                    currentCacheGuid = $"{requestParams.Id}_{requestParams.Cache.ClientGuid}";
                    if (useDrillDownReportSnapshot && !string.IsNullOrEmpty(requestParams.Cache.DrillDownGuid)) currentCacheGuid += $"_{requestParams.Cache.DrillDownGuid}";
                    report = GetReport(currentCacheGuid, CacheMode, Timeout, Priority);
                    
                    // If there is no report snapshot, try to take the report template
                    if (report == null)
                    {
                        currentCacheGuid = $"{requestParams.Id}_{requestParams.Cache.ClientGuid}_{GUID_ReportTemplate}";
                        report = GetReport(currentCacheGuid, CacheMode, Timeout, Priority);
                    }
                }
            }

            return report;
        }

        internal void SaveReportInternal(StiRequestParams requestParams, StiReport report)
        {
            if (report != null && requestParams.Cache.Mode != StiServerCacheMode.None && !string.IsNullOrEmpty(requestParams.Cache.ClientGuid) &&
                requestParams.Action != StiAction.Undefined && requestParams.Action != StiAction.Resource)
            {
                ApplyRequestParameters(requestParams);

                // Save only rendered report, if one of the specified actions
                var isReportSnapshot =
                    requestParams.Action == StiAction.Variables ||
                    requestParams.Action == StiAction.Sorting ||
                    requestParams.Action == StiAction.Collapsing ||
                    requestParams.Action == StiAction.DrillDown ||
                    requestParams.Action == StiAction.PreviewReport ||
                    requestParams.Action == StiAction.RefreshReport;

                // Save the report template to cache if it needed in the future
                if (!isReportSnapshot && !report.IsDocument &&
                    (requestParams.Component == StiComponentType.Designer || requestParams.Component == StiComponentType.DesignerFx ||
                     report.Dictionary.IsRequestFromUserVariablesPresent || StiReportHelper.IsReportHasInteractions(report) || report.RefreshTime > 0))
                {
                    currentCacheGuid = $"{requestParams.Id}_{requestParams.Cache.ClientGuid}_{GUID_ReportTemplate}";
                    SaveReport(report, currentCacheGuid, CacheMode, Timeout, Priority);

                    // Skip render process if report template for Designer
                    if (requestParams.Component == StiComponentType.Designer || requestParams.Component == StiComponentType.DesignerFx) return;
                }
                // Remove old report from cache if GetReport or OpenReport actions received
                else if (!requestParams.Viewer.ReportDesignerMode && (requestParams.Action == StiAction.GetReport || requestParams.Action == StiAction.OpenReport))
                {
                    currentCacheGuid = $"{requestParams.Id}_{requestParams.Cache.ClientGuid}";
                    RemoveReport(currentCacheGuid);
                }

                // Render report if not rendered and not document
                if (!report.IsRendered && !report.IsDocument)
                {
                    try
                    {
                        report.Render(false);
                    }
                    catch
                    {
                    }
                }

                // Save the report snapshot to cache
                currentCacheGuid = $"{requestParams.Id}_{requestParams.Cache.ClientGuid}";
                report.SaveInteractionParametersToDocument = true;
                if (!string.IsNullOrEmpty(requestParams.Cache.DrillDownGuid)) currentCacheGuid = $"{currentCacheGuid}_{requestParams.Cache.DrillDownGuid}";
                SaveReport(report, currentCacheGuid, CacheMode, Timeout, Priority);
                report.SaveInteractionParametersToDocument = false;
            }
        }

        internal void SavePreviewReportInternal(StiRequestParams requestParams, StiReport report, string guid)
        {
            ApplyRequestParameters(requestParams);
            RemoveReport(guid);

            // Save report template
            currentCacheGuid = $"{guid}_{GUID_ReportTemplate}";
            SaveReport(report, currentCacheGuid);

            // Save rendered report
            if (report.IsRendered)
            {
                currentCacheGuid = guid;
                report.SaveInteractionParametersToDocument = true;
                SaveReport(report, currentCacheGuid);
                report.SaveInteractionParametersToDocument = false;
            }
        }

        public virtual void RemoveReportInternal(StiRequestParams requestParams)
        {
            ApplyRequestParameters(requestParams);
            currentCacheGuid = $"{requestParams.Id}_{requestParams.Cache.ClientGuid}";
            RemoveReport(currentCacheGuid, CacheMode);
        }
        #endregion

        #region Resources
        internal string GetResourceInternal(StiRequestParams requestParams, string guid)
        {
            ApplyRequestParameters(requestParams);
            return GetResource(guid);
        }

        internal void SaveResourceInternal(StiRequestParams requestParams, string text, string guid)
        {
            ApplyRequestParameters(requestParams);
            SaveResource(text, guid);
        }
        #endregion

        internal void ApplyRequestParameters(StiRequestParams requestParams)
        {
            HttpContext = requestParams.HttpContext;
            CacheMode = requestParams.Cache.Mode;
            Timeout = requestParams.Cache.Timeout;
            Priority = requestParams.Cache.Priority;
        }
        #endregion

        #region Methods: Object serializer
        private static class DataSerializer
        {
            public static byte[] Serialize(object obj)
            {
                if (obj == null) return null;

                var binaryFormatter = new BinaryFormatter();
                using (var memoryStream = new MemoryStream())
                {
                    binaryFormatter.Serialize(memoryStream, obj);
                    var objectDataAsStream = memoryStream.ToArray();
                    return objectDataAsStream;
                }
            }

            public static T Deserialize<T>(byte[] bytes)
            {
                if (bytes == null) return default(T);

                var binaryFormatter = new BinaryFormatter();
                using (var memoryStream = new MemoryStream(bytes))
                {
                    var result = (T)binaryFormatter.Deserialize(memoryStream);
                    return result;
                }
            }
        }

        public static object GetObjectFromCacheData(byte[] fileData)
        {
            var data = DataSerializer.Deserialize<object>(fileData);
            try
            {
                if (data is byte[])
                {
                    var report = new StiReport();
                    report.Load((byte[])data);
                    report.Info.Zoom = 1;

                    return report;
                }

                if (data is string)
                    return (string)data;

                if (data is ArrayList)
                {
                    var array = (ArrayList)data;
                    for (var i = 0; i < array.Count; i++)
                    {
                        if (array[i] != null)
                        {
                            if (array[i] is byte[])
                            {
                                StiReport report = new StiReport();
                                report.Load((byte[])array[i]);
                                report.Info.Zoom = 1;
                                array[i] = report;
                            }
                            else if (array[i] is string)
                            {
                                var str = (string)array[i];
                                if (!str.StartsWith("<?xml"))
                                {
                                    var typeComponent = str.Substring(0, str.IndexOf(";"));
                                    var strComponent = str.Substring(str.IndexOf(";") + 1);

                                    var assembly = typeof(StiReport).Assembly;
                                    var component = assembly.CreateInstance("Stimulsoft.Report.Components." + typeComponent) as StiComponent;
                                    if (component == null) component = assembly.CreateInstance("Stimulsoft.Report.Chart." + typeComponent) as StiComponent;
                                    else if (component == null) component = assembly.CreateInstance("Stimulsoft.Report.CrossTab." + typeComponent) as StiComponent;

                                    if (component != null)
                                    {
                                        using (var stringReader = new StringReader(strComponent))
                                        {
                                            var sr = new StiSerializing(new StiReportObjectStringConverter());
                                            sr.Deserialize(component, stringReader, "StiComponent");
                                            stringReader.Close();
                                        }
                                    }
                                    array[i] = component;
                                }
                            }
                            else if (array[i] is Hashtable)
                            {
                                var report = new StiReport();
                                report.Load(((Hashtable)array[i])["report"] as byte[]);
                                report.Info.Zoom = 1;

                                var command = (StiDesignerCommand)((Hashtable)array[i])["command"];
                                var reportContainer = new StiReportContainer(report, (bool)((Hashtable)array[i])["resourcesIncluded"], command);

                                array[i] = reportContainer;
                            }
                        }
                    }

                    return array;
                }
            }
            finally
            {
            }

            return null;
        }

        public static byte[] GetCacheDataFromObject(object obj)
        {
            byte[] data = null;

            if (obj is StiReport)
                data = DataSerializer.Serialize(((StiReport)obj).SaveToByteArray());

            else if (obj is string)
                data = DataSerializer.Serialize((string)obj);

            else if (obj is ArrayList)
            {
                var array = (ArrayList)obj;
                for (var i = 0; i < array.Count; i++)
                {
                    if (array[i] != null)
                    {
                        if (array[i] is StiReport)
                        {
                            array[i] = ((StiReport)array[i]).SaveToByteArray();
                        }
                        else if (array[i] is StiComponent)
                        {
                            var sr = new StiSerializing(new StiReportObjectStringConverter());
                            var sb = new StringBuilder();
                            using (var stringWriter = new StringWriter(sb))
                            {
                                sr.Serialize(array[i], stringWriter, "StiComponent", StiSerializeTypes.SerializeToAll);
                                stringWriter.Close();
                                array[i] = array[i].GetType().Name + ";" + sb.ToString();
                            }
                        }
                        else if (array[i] is StiReportContainer)
                        {
                            var reportContainer = new Hashtable();
                            reportContainer["command"] = (array[i] as StiReportContainer).command;
                            reportContainer["resourcesIncluded"] = (array[i] as StiReportContainer).resourcesIncluded;
                            reportContainer["report"] = (array[i] as StiReportContainer).report.SaveToByteArray();
                            array[i] = reportContainer;
                        }
                    }
                }

                data = DataSerializer.Serialize(array);
            }

            return data;
        }
        #endregion


        #region Obsolete

        #region Hidden / Delete this methods in the 2019.1 release

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public virtual object GetObject(string guid, StiServerCacheMode cacheMode, TimeSpan timeout, CacheItemPriority priority)
        {
            return GetObject(guid);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public virtual void SaveObject(object obj, string guid, StiServerCacheMode cacheMode, TimeSpan timeout, CacheItemPriority priority)
        {
            SaveObject(obj, guid);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public virtual void RemoveObject(string guid, StiServerCacheMode cacheMode)
        {
            RemoveObject(guid);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public virtual StiReport GetReport(string guid, StiServerCacheMode cacheMode, TimeSpan timeout, CacheItemPriority priority)
        {
            return GetReport(guid);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public virtual void SaveReport(StiReport report, string guid, StiServerCacheMode cacheMode, TimeSpan timeout, CacheItemPriority priority)
        {
            SaveReport(report, guid);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public virtual void RemoveReport(string guid, StiServerCacheMode cacheMode)
        {
            RemoveReport(guid);
        }

        #endregion

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetObject() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public virtual object GetObjectFromCache(string guid)
        {
            return null;
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the SaveObject() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public virtual void SaveObjectToCache(object obj, string guid)
        {
        }

        #endregion
    }
}
