﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stimulsoft.Report.Export;
using System.IO;
using System.Collections;
using Stimulsoft.Report.Components;
using System.Drawing.Imaging;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.CrossTab;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.Chart;
using Stimulsoft.Base;
using Stimulsoft.Report.Dictionary;
using System.Reflection;
using System.CodeDom.Compiler;
using System.Drawing;
using System.Web;
using System.Net;
using Stimulsoft.Base.Json;
using Stimulsoft.Base.Json.Converters;
using Stimulsoft.Report.Painters;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Cloud;

#if NETCORE
using Stimulsoft.System.Web;
#endif

namespace Stimulsoft.Report.Web
{
    internal class StiReportHelper
    {
        #region Methods
        /// <summary>
        /// Get the color in HEX format.
        /// </summary>
        public static string GetHtmlColor(Color color)
        {
            return "#" + color.R.ToString("x2") + color.G.ToString("x2") + color.B.ToString("x2");
        }
        
        public static StiReport GetCompiledReport(StiReport report)
        {
            if (report.NeedsCompiling && !report.IsCompiled && report.CalculationMode == StiCalculationMode.Compilation && !StiOptions.Engine.ForceInterpretationMode)
            {
                try
                {
                    report.Compile();
                }
                catch
                {
                }
            }
            
            return report.CompiledReport != null ? report.CompiledReport : report;
        }

        public static StiReport CreateReportCopy(StiReport report)
        {
            StiReport reportCopy;
            if (!StiOptions.Engine.FullTrust || report.CalculationMode == StiCalculationMode.Interpretation || report.NeedsCompiling)
            {
                reportCopy = new StiReport();
                var stream = new MemoryStream();
                report.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);
                reportCopy.Load(stream);
                stream.Close();
            }
            else
            {
                reportCopy = StiActivator.CreateObject(report.GetType()) as StiReport;
            }

            // Copy report variables
            foreach (StiVariable variable in report.Dictionary.Variables)
            {
                reportCopy.Dictionary.Variables[variable.Name].DialogInfo.Keys = variable.DialogInfo.Keys.Clone() as string[];
                reportCopy.Dictionary.Variables[variable.Name].DialogInfo.Values = variable.DialogInfo.Values.Clone() as string[];
                FieldInfo field = report.GetType().GetField(variable.Name);
                if (field != null) reportCopy[variable.Name] = field.GetValue(report);
            }

            if (report.Variables != null && report.Variables.Count > 0)
            {
                foreach (string key in report.Variables.Keys)
                {
                    reportCopy[key] = report[key];
                }
            }

            reportCopy.RegData(report.DataStore);
            reportCopy.RegBusinessObject(report.BusinessObjectsStore);
            reportCopy.Dictionary.Resources.Clear();
            reportCopy.Dictionary.Resources.AddRange(report.Dictionary.Resources);

            return reportCopy;
        }

        public static string GetCompileErrorHtmlPage(StiReport report)
        {
            if (report.CompilerResults == null || report.CompilerResults.Errors.Count == 0) return null;

            string reportName = report.ReportName;
            if (report.ReportAlias.Length > 0) reportName = report.ReportAlias;

            #region Create html page

            var result = string.Empty;
            result += "<html><head><title>Compilation Error</title><style>\n";
            result += "span.header { font-family: Verdana; font-weight: normal; font-size: 18pt; color: Maroon }\n";
            result += "span.status { font-family: Verdana; font-weight: normal; font-size: 9pt; color: #505050 }\n";
            result += "table { border-right: 1px solid DarkGray; border-top: 1px solid DarkGray }\n";
            result += "td.header { border-left: 1px solid DarkGray; border-bottom: 1px solid DarkGray; background-color: #e0e0e0; font-family: Verdana; font-size: 12pt; padding: 3px 0px 3px 0px; text-align: Center }\n";
            result += "td.error { border-left: 1px solid DarkGray; border-bottom: 1px solid DarkGray; padding: 3px 0px 3px 4px; font-family: Verdana; font-weight: normal; font-size: 12pt; color: #266294 }\n";
            result += "</style></head>\n";
            result += "<body bgcolor=\"White\"><span class=\"header\">Compilation Error in " + reportName + "</span><br><br>\n";
            result += "<table cellspacing=0 cellpadding=0 border=0 width=\"100%\"><tr>\n";
            result += "<td class=\"header\" width=\"30%\">File Name</td><td class=\"header\" width=\"10%\">Line, Column</td>\n";
            result += "<td class=\"header\" width=\"10%\">Error Number</td><td class=\"header\" width=\"50%\">Error Text</td></tr>\n";

            foreach (CompilerError error in report.CompilerResults.Errors)
            {
                result += string.Format("<tr><td class=\"error\">{0}</td><td class=\"error\">{1}, {2}</td><td class=\"error\">{3}</td><td class=\"error\">{4}</td></tr>\n",
                                        error.FileName, error.Line, error.Column, error.ErrorNumber, error.ErrorText);
            }
            
            result += string.Format("</table><br><span class=\"status\">Report {0}, Version: {1}</span></body></html>\n",
                                    report.EngineVersion, StiVersionHelper.ProductVersion);
            #endregion

            return result;
        }

        public static CookieContainer GetCookieContainer()
        {
            if (HttpContext.Current != null) return GetCookieContainer(HttpContext.Current.Request);
            return null;
        }

        public static CookieContainer GetCookieContainer(HttpRequest request)
        {
            if (request != null && request.Cookies.Count > 0)
            {
                var cookieContainer = new CookieContainer();
                foreach (string key in request.Cookies.AllKeys)
                {
                    if (!string.IsNullOrEmpty(request.Cookies[key].Value))
                    {
                        cookieContainer.Add(new Cookie(request.Cookies[key].Name, HttpUtility.UrlEncode(request.Cookies[key].Value)) { Domain = request.Url.Host });
                    }
                }
                if (cookieContainer.Count > 0) return cookieContainer;
            }

            return null;
        }

        public static string GetReportFileName(StiReport report)
        {
            string fileName = (report.ReportAlias == null || report.ReportAlias.Trim().Length == 0)
                ? report.ReportName
                : report.ReportAlias;

            if (fileName == null || fileName.Trim().Length == 0)
            {
                if (report.ReportFile != null && report.ReportFile.Trim().Length > 0)
                {
                    fileName = report.ReportFile.Replace(".mrt", "").Replace(".mrz", "").Replace(".mrx", "").Replace(".mdc", "").Replace(".mdz", "").Replace(".mdx", "");
                    fileName = fileName.Substring(fileName.LastIndexOf("/") + 1);
                }
                else
                    fileName = "Report";
            }

            return string.Join("_", fileName.Split('<', '>', ':', ';', '"', '/', '\\', '|', '?', '*', '%'));
        }
        
        public static void ApplyQueryParameters(StiRequestParams requestParams, StiReport report)
        {
            if (requestParams.Server.PassQueryParametersToReport && report != null && report.Dictionary.Variables.Count > 0)
            {
                foreach (string key in requestParams.HttpContext.Request.QueryString)
                {
                    if (key != null && !key.StartsWith("stiweb_") && report.Dictionary.Variables.Contains(key))
                    {
                        if (report.IsCompiled) report[key] = requestParams.HttpContext.Request.QueryString[key];
                        else report.Dictionary.Variables[key].Value = requestParams.HttpContext.Request.QueryString[key];
                    }
                }
            }
        }
        #endregion

        #region Interactions
        /// <summary>
        /// Returns information about the presence of interactive report.
        /// </summary>
        public static bool IsReportHasInteractions(StiReport report)
        {
            foreach (StiPage page in report.Pages)
            {
                foreach (StiComponent comp in page.GetComponents())
                {
                    if (IsComponentHasInteraction(comp)) return true;
                }
            }

            return false;
        }

        public static bool IsComponentHasInteraction(StiComponent comp)
        {
            if (comp.Interaction != null)
            {
                if (comp.Interaction.SortingEnabled && !string.IsNullOrWhiteSpace(comp.Interaction.SortingColumn)) return true;
                if (comp.Interaction.DrillDownEnabled && (!string.IsNullOrEmpty(comp.Interaction.DrillDownPageGuid) || !string.IsNullOrEmpty(comp.Interaction.DrillDownReport))) return true;
                if (comp.Interaction is StiBandInteraction && ((StiBandInteraction)comp.Interaction).CollapsingEnabled) return true;
                if (comp is StiChart && StiOptions.Engine.AllowInteractionInChartWithComponents) return true;
            }
            if (comp is StiChart)
            {
                foreach (StiSeries series in ((StiChart)comp).Series)
                {
                    if (series.Interaction != null && series.Interaction.DrillDownEnabled) return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Apply the parameters for sorting and rebuild the report
        /// </summary>
        public static void ApplySorting(StiReport report, Hashtable parameters)
        {
            if (parameters == null || parameters.Count == 0) return;
            string[] values = parameters["ComponentName"].ToString().Split(';');
            StiComponent comp = report.GetComponentByName(values[0]);
            var isCtrl = bool.Parse(values[1]);

            values = parameters["DataBand"].ToString().Split(';');
            StiDataBand dataBand = report.GetComponentByName(values[0]) as StiDataBand;
            if (dataBand != null) dataBand.Sort = values.Where((val, i) => i != 0).ToArray();

            #region Apply interaction sorting

            if (comp != null && dataBand != null)
            {
                string dataBandColumnString = comp.Interaction.GetSortColumnsString();
                if (dataBand.Sort == null || dataBand.Sort.Length == 0)
                {
                    dataBand.Sort = StiSortHelper.AddColumnToSorting(dataBand.Sort, dataBandColumnString, true);
                }
                else
                {
                    int sortIndex = StiSortHelper.GetColumnIndexInSorting(dataBand.Sort, dataBandColumnString);
                    if (isCtrl)
                    {
                        if (sortIndex == -1) dataBand.Sort = StiSortHelper.AddColumnToSorting(dataBand.Sort, dataBandColumnString, true);
                        else dataBand.Sort = StiSortHelper.ChangeColumnSortDirection(dataBand.Sort, dataBandColumnString);
                    }
                    else
                    {
                        if (sortIndex != -1)
                        {
                            StiInteractionSortDirection direction = StiSortHelper.GetColumnSortDirection(dataBand.Sort, dataBandColumnString);

                            if (direction == StiInteractionSortDirection.Ascending) direction = StiInteractionSortDirection.Descending;
                            else direction = StiInteractionSortDirection.Ascending;

                            dataBand.Sort = StiSortHelper.AddColumnToSorting(new string[0], dataBandColumnString, direction == StiInteractionSortDirection.Ascending);
                            comp.Interaction.SortingDirection = direction;
                        }
                        else
                        {
                            dataBand.Sort = StiSortHelper.AddColumnToSorting(new string[0], dataBandColumnString, true);
                            comp.Interaction.SortingDirection = StiInteractionSortDirection.Ascending;
                        }
                    }
                }

                report.IsRendered = false;
            }

            #endregion
        }

        /// <summary>
        /// Apply the parameters for collapsing and rebuild the report
        /// </summary>
        public static void ApplyCollapsing(StiReport report, Hashtable parameters)
        {
            string componentName = parameters["ComponentName"] as string;

            StiComponent comp = report.GetComponentByName(componentName);
            IStiInteraction interactionComp = comp as IStiInteraction;

            if (interactionComp != null && interactionComp.Interaction != null)
            {
                report.InteractionCollapsingStates = parameters["InteractionCollapsingStates"] as Hashtable;

                #region Replase 'string' keys to 'int' keys

                foreach (Hashtable states in report.InteractionCollapsingStates.Values)
                {
                    Hashtable statesCopy = states.Clone() as Hashtable;
                    foreach (var obj in statesCopy.Keys)
                    {
                        if (obj is string)
                        {
                            int num = int.Parse((string)obj);
                            states.Add(num, states[obj]);
                            states.Remove(obj);
                        }
                    }
                    statesCopy = null;
                }

                #endregion

                #region CrossHeader Collapsing

                StiCrossHeaderInteraction crossHeaderInteraction = interactionComp.Interaction as StiCrossHeaderInteraction;
                if (crossHeaderInteraction != null && crossHeaderInteraction.CollapsingEnabled)
                {
                    StiCrossHeader header = comp as StiCrossHeader;
                    StiCrossTabV2Builder.SetCollapsed(header, !StiCrossTabV2Builder.IsCollapsed(header));
                }

                #endregion

                report.IsRendered = false;
            }
        }

        /// <summary>
        /// Apply the parameters for drill-down and build the detailed report
        /// </summary>
        public static StiReport ApplyDrillDown(StiReport report, StiReport renderedReport, Hashtable parameters)
        {
            if (renderedReport == null) renderedReport = report;
            if (parameters == null || parameters.Count == 0) return renderedReport;

            int pageIndex = Convert.ToInt32(parameters["PageIndex"]);
            int componentIndex = Convert.ToInt32(parameters["ComponentIndex"]);
            int elementIndex = Convert.ToInt32(parameters["ElementIndex"]);
            string pageGuid = Convert.ToString(parameters["PageGuid"]);
            string reportFile = Convert.ToString(parameters["ReportFile"]);

            StiPage drillDownPage = null;
            StiReport newReport = report;
            if (renderedReport == null) renderedReport = report;
            if (!renderedReport.IsRendered)
            {
                try
                {
                    renderedReport.Render(false);
                }
                catch { }
            }

            #region Drill-down page
            if (!string.IsNullOrEmpty(pageGuid))
            {
                #region Get drill-down page and disable all pages except drill-down page

                foreach (StiPage page in report.Pages)
                {
                    if (page.Guid == pageGuid)
                    {
                        drillDownPage = page;
                        page.Enabled = true;
                        page.Skip = false;
                    }
                    else
                        page.Enabled = false;
                }

                #endregion

                #region Clear any reference to drill-down page from other components in report
                // We need do this because during report rendering drill-down pages is skipped

                StiComponentsCollection comps = report.GetComponents();
                foreach (StiComponent comp in comps)
                {
                    #region Components
                    if (comp.Interaction != null &&
                        comp.Interaction.DrillDownEnabled &&
                        comp.Interaction.DrillDownPageGuid == drillDownPage.Guid)
                    {
                        comp.Interaction.DrillDownPage = null;
                    }
                    #endregion

                    #region Charts
                    if (comp is StiChart)
                    {
                        StiChart chart = comp as StiChart;
                        foreach (StiSeries series in chart.Series)
                        {
                            StiSeriesInteraction seriesInteraction = (StiSeriesInteraction)series.Interaction;
                            if (series.Interaction != null &&
                                seriesInteraction.DrillDownEnabled &&
                                seriesInteraction.DrillDownPageGuid == drillDownPage.Guid)
                            {
                                seriesInteraction.DrillDownPage = null;
                            }
                        }
                    }
                    #endregion
                }

                #endregion
            }
            #endregion

            #region Check drill-down for a report file
            else if (!string.IsNullOrEmpty(reportFile))
            {
                newReport = new StiReport();
                newReport.Load(reportFile);
            }
            #endregion

            #region Fill report alias, description
            if (report.ReportAlias == newReport.ReportAlias && drillDownPage != null) newReport.ReportAlias = string.IsNullOrEmpty(drillDownPage.Alias) ? drillDownPage.Name : drillDownPage.Alias;
            if (report.ReportDescription == newReport.ReportDescription) newReport.ReportDescription = newReport.ReportAlias;
            #endregion

            #region Fill drill-down parameters
            StiPage renderedPage = renderedReport.RenderedPages[pageIndex];
            StiComponent interactionComp = renderedPage.Components[componentIndex];
            if (interactionComp != null && interactionComp.DrillDownParameters != null)
            {
                foreach (KeyValuePair<string, object> entry in interactionComp.DrillDownParameters)
                {
                    newReport[entry.Key] = entry.Value;
                }
            }

            #region Fill chart drill-down parameters
            var interactionChart = interactionComp as StiChart;
            if (interactionChart != null)
            {
                StiGdiContextPainter painter = new StiGdiContextPainter(StiReport.GlobalMeasureGraphics);
                StiContext context = new StiContext(painter, true, false, false, 1);
                RectangleD rect = interactionChart.Report.Unit.ConvertToHInches(interactionChart.ClientRectangle);
                StiCellGeom chartGeom = interactionChart.Core.Render(context, new RectangleF(0, 0, (float)rect.Width, (float)rect.Height), true);
                List<StiCellGeom> seriesGeomItems = chartGeom.GetSeriesElementGeoms();
                StiSeriesElementGeom seriesElementGeom = seriesGeomItems[elementIndex] as StiSeriesElementGeom;
                if (seriesElementGeom != null && seriesElementGeom.Interaction != null)
                {
                    newReport["Series"] = seriesElementGeom.Series.Core;
                    newReport["SeriesIndex"] = seriesElementGeom.Series.Core.Series.Chart.Series.IndexOf(seriesElementGeom.Series.Core.Series);
                    newReport["SeriesArgument"] = seriesElementGeom.Interaction.Argument;
                    newReport["SeriesValue"] = seriesElementGeom.Interaction.Value;
                    newReport["SeriesPointIndex"] = seriesElementGeom.Interaction.PointIndex;
                    newReport["SeriesTag"] = seriesElementGeom.Interaction.Tag;
                    newReport["SeriesHyperlink"] = seriesElementGeom.Interaction.Hyperlink;
                    newReport["SeriesTooltip"] = seriesElementGeom.Interaction.Tooltip;
                    newReport["SeriesTitle"] = seriesElementGeom.Series.CoreTitle;

                    #region Report Alias

                    string arg1 = seriesElementGeom.Interaction.Series != null ? seriesElementGeom.Interaction.Series.CoreTitle : null;
                    string arg2 = seriesElementGeom.Interaction.Argument != null ? seriesElementGeom.Interaction.Argument.ToString() : null;

                    if (string.IsNullOrEmpty(arg2)) arg2 = seriesElementGeom.Interaction.Value.ToString();
                    
                    if (!string.IsNullOrEmpty(arg1) && !string.IsNullOrEmpty(arg2)) newReport.ReportAlias = string.Format("{0} - {1}", arg1, arg2);
                    else if (!string.IsNullOrEmpty(arg2)) newReport.ReportAlias = arg1;
                    else newReport.ReportAlias = arg2;

                    #endregion
                }
            }
            #endregion

            #endregion

            #region Render new report
            try
            {
                newReport.IsInteractionRendering = true;
                try
                {
                    newReport.Render(false);
                }
                catch { }
            }
            finally
            {
                newReport.IsInteractionRendering = false;
            }
            #endregion

            return newReport;
        }
        #endregion

        #region Report Pages
        private static Hashtable GetReportPage(StiReport report, StiHtmlExportService service, int pageIndex, StiRequestParams requestParams)
        {
            var settings = new StiHtmlExportSettings();
            settings.PageRange = new StiPagesRange(pageIndex);
            settings.Zoom = requestParams.Viewer.Zoom;
            settings.ImageFormat = ImageFormat.Png;
            settings.ExportQuality = StiHtmlExportQuality.High;
            settings.ExportBookmarksMode = StiHtmlExportBookmarksMode.ReportOnly;
            settings.RemoveEmptySpaceAtBottom = false;
            settings.OpenLinksTarget = requestParams.Viewer.OpenLinksWindow;
            settings.ChartType = (StiHtmlChartType)Enum.Parse(typeof(StiHtmlChartType), requestParams.Viewer.ChartRenderType.ToString());
            settings.UseWatermarkMargins = true;

            switch (requestParams.Viewer.ReportDisplayMode)
            {
                case StiReportDisplayMode.Table: settings.ExportMode = StiHtmlExportMode.Table; break;
                case StiReportDisplayMode.Div: settings.ExportMode = StiHtmlExportMode.Div; break;
                case StiReportDisplayMode.Span: settings.ExportMode = StiHtmlExportMode.Span; break;
            }

            var stream = new MemoryStream();
            service.ExportHtml(report, stream, settings);
            string htmlPageContent = Encoding.UTF8.GetString(stream.ToArray()).Substring(1);
            stream.Close();

            Hashtable pageAttr = new Hashtable();
            pageAttr["content"] = report.RenderedPages.Count > 0 ? htmlPageContent : string.Empty;

            StiPage page = report.RenderedPages.Count > 0 ? report.RenderedPages[pageIndex] : new StiPage(report);
            pageAttr["margins"] = string.Format("{0}px {1}px {2}px {3}px",
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Top) * requestParams.Viewer.Zoom),
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Right) * requestParams.Viewer.Zoom),
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Bottom) * requestParams.Viewer.Zoom),
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Left) * requestParams.Viewer.Zoom));
            pageAttr["sizes"] = string.Format("{0};{1}",
                    Math.Round(report.Unit.ConvertToHInches(page.PageWidth) * requestParams.Viewer.Zoom),
                    Math.Round(report.Unit.ConvertToHInches(page.PageHeight) * requestParams.Viewer.Zoom));
            pageAttr["background"] = GetHtmlColor(StiBrush.ToColor(page.Brush));

            return pageAttr;
        }

        internal static ArrayList GetPagesArray(StiReport report, StiRequestParams requestParams)
        {
            var service = new StiHtmlExportService();
            service.RenderWebViewer = true;
            service.RenderWebInteractions = true;
            service.RenderAsDocument = false;
            service.Styles = new ArrayList();
            service.ClearOnFinish = false;
            service.RenderStyles = false;

            var htmlText = string.Empty;
            var pageMargins = string.Empty;
            var pageSizes = string.Empty;
            var pageBackgrounds = string.Empty;

            #region Render Pages
            var pages = new ArrayList();

            if (requestParams.Viewer.ViewMode == StiWebViewMode.SinglePage)
            {
                Hashtable attributes = GetReportPage(report, service, requestParams.Viewer.PageNumber, requestParams);
                pages.Add(attributes);
            }
            else
            {
                for (int index = 0; index < report.RenderedPages.Count; index++)
                {
                    Hashtable attributes = GetReportPage(report, service, index, requestParams);
                    pages.Add(attributes);
                }
            }
            #endregion

            #region Render Styles
            var writer = new StringWriter();
            var htmlWriter = new StiHtmlTextWriter(writer);
            service.HtmlWriter = htmlWriter;
            if (service.TableRender != null) service.TableRender.RenderStylesTable(true, false, false);
            htmlWriter.Flush();
            writer.Flush();
            string htmlTextStyles = writer.GetStringBuilder().ToString();
            writer.Close();

            pages.Add(htmlTextStyles);
            #endregion

            //Add Chart Scripts
            string chartScript = service.GetChartScript();
            pages.Add(chartScript);
            service.Clear();

            return pages;
        }
        #endregion

        #region Bookmarks
        private class StiBookmarkTreeNode
        {
            public int Parent;
            public string Title;
            public string Url;
            public string ComponentGuid;
            public bool Used;
        }

        private static void AddBookmarkNode(StiBookmark bkm, int parentNode, ArrayList bookmarksTree)
        {
            var tn = new StiBookmarkTreeNode();
            tn.Parent = parentNode;
            string st = bkm.Text.Replace("'", "\\\'");
            tn.Title = st;
            tn.Url = "#" + st;
            tn.Used = true;
            tn.ComponentGuid = bkm.ComponentGuid;

            bookmarksTree.Add(tn);
            int currentNode = bookmarksTree.Count - 1;
            if (bkm.Bookmarks.Count != 0)
            {
                for (int tempCount = 0; tempCount < bkm.Bookmarks.Count; tempCount++)
                {
                    AddBookmarkNode(bkm.Bookmarks[tempCount], currentNode, bookmarksTree);
                }
            }
        }

        public static string GetBookmarksContent(StiReport report, string viewerId, int pageNumber)
        {
            #region Prepare bookmarksPageIndex
            var bookmarksPageIndex = new Hashtable();
            
            int tempPageNumber = 0;
            foreach (StiPage page in report.RenderedPages)
            {
                report.RenderedPages.GetPage(page);
                StiComponentsCollection components = page.GetComponents();
                foreach (StiComponent comp in components)
                {
                    if (comp.Enabled)
                    {
                        string bookmarkValue = comp.BookmarkValue as string;
                        string componentGuid = comp.Guid as string;
                        if (componentGuid != null && !bookmarksPageIndex.ContainsKey(componentGuid))
                            bookmarksPageIndex[componentGuid] = tempPageNumber;
                        if (bookmarkValue == null) bookmarkValue = string.Empty;
                        bookmarkValue = bookmarkValue.Replace("'", "\\\'");
                        if (bookmarkValue != string.Empty && !bookmarksPageIndex.ContainsKey(bookmarkValue))
                            bookmarksPageIndex.Add(bookmarkValue, tempPageNumber);
                    }
                }
                tempPageNumber++;
            }
            #endregion

            var bookmarksTree = new ArrayList();
            AddBookmarkNode(report.Bookmark, -1, bookmarksTree);

            var html = string.Empty;
            html += string.Format("bookmarks = new stiTree('bookmarks','{0}',{1}, imagesForBookmarks);", viewerId, pageNumber);
            for (int index = 0; index < bookmarksTree.Count; index++)
            {
                StiBookmarkTreeNode node = (StiBookmarkTreeNode)bookmarksTree[index];
                string pageTitle = string.Empty;
                if (node.ComponentGuid != null && bookmarksPageIndex.ContainsKey(node.ComponentGuid)) pageTitle = string.Format("Page {0}", (int)bookmarksPageIndex[node.ComponentGuid] + 1);
                else if (bookmarksPageIndex.ContainsKey(node.Title)) pageTitle = string.Format("Page {0}", (int)bookmarksPageIndex[node.Title] + 1);
                else pageTitle = "Page 0";
                html += string.Format("bookmarks.add({0},{1},'{2}','{3}','{4}','{5}');", index, node.Parent, node.Title, node.Url, pageTitle, node.ComponentGuid);
            }

            return html;
        }
        #endregion
        
        #region Result

        #region Viewer
        public static StiWebActionResult ViewerResult(StiRequestParams requestParams, StiReport report)
        {
            if (report == null) return StiWebActionResult.EmptyReportResult();
            
            if (!report.IsRendered)
            {
                try
                {
                    report.Render(false);
                }
                catch (Exception e)
                {
                    return new StiWebActionResult("ServerError:" + e.Message);
                }
            }

            StiEditableFieldsHelper.ApplyEditableFieldsToReport(report, requestParams.Interaction.Editable);

            // Correct current page number
            if (requestParams.Action == StiAction.DrillDown) requestParams.Viewer.PageNumber = 0;
            if (requestParams.Action == StiAction.Variables || requestParams.Action == StiAction.Collapsing)
                requestParams.Viewer.PageNumber = Math.Min(requestParams.Viewer.PageNumber, report.RenderedPages.Count - 1);

            // Create parameters table for viewer
            var parameters = new Hashtable();
            parameters["action"] = requestParams.Action;
            parameters["pagesArray"] = GetPagesArray(report, requestParams);

            if (requestParams.Action == StiAction.GetReport || requestParams.Action == StiAction.OpenReport)
            {
                parameters["customFonts"] = StiReportResourceHelper.GetFontResourcesArray(report);
            }

            if (requestParams.Action != StiAction.GetPages)
            {
                parameters["drillDownGuid"] = requestParams.Cache.DrillDownGuid;
                parameters["zoom"] = Convert.ToInt32(requestParams.Viewer.Zoom * 100);
                parameters["viewMode"] = requestParams.Viewer.ViewMode;
                parameters["isEditableReport"] = StiEditableFieldsHelper.CheckEditableReport(report);
                parameters["pagesCount"] = report.RenderedPages.Count;
                parameters["reportFileName"] = GetReportFileName(report);
                parameters["reportRefreshTime"] = report.IsDocument ? 0 : report.RefreshTime;
                parameters["interactionCollapsingStates"] = report.InteractionCollapsingStates;
                parameters["resources"] = StiReportResourceHelper.GetResourcesItems(report);
                parameters["cloudTrialMode"] = requestParams.Viewer.CloudTrialMode;
                if (requestParams.Action == StiAction.DrillDown) parameters["drillDownParameters"] = requestParams.Interaction.DrillDown;
                if (report.Bookmark != null && report.Bookmark.Bookmarks.Count > 0 && requestParams.Viewer.ShowBookmarks)
                    parameters["bookmarksContent"] = GetBookmarksContent(
                        report, requestParams.Id,
                        requestParams.Viewer.ViewMode == StiWebViewMode.SinglePage ? requestParams.Viewer.PageNumber : -1);
            }

#if CLOUD
            StiCloudReportLimits reportLimits = StiCloudReportResults.GetAndRemoveLimits(report.ReportGuid);
            parameters["maxDataRows"] = reportLimits.MaxDataRows;
            parameters["maxReportPages"] = reportLimits.MaxReportPages;
#endif

            return StiWebActionResult.JsonResult(requestParams, parameters);
        }
        #endregion

        #region Interaction
        public static StiWebActionResult InteractionResult(StiRequestParams requestParams, StiReport report)
        {
            if (requestParams.Action == StiAction.InitVars)
            {
                if (report == null || report.IsDocument) return new StiWebActionResult("null", "application/json");

                report = GetCompiledReport(report);
                StiVariablesHelper.ApplyReportBindingVariables(report, requestParams.Interaction.Variables);
                var variables = StiVariablesHelper.GetVariables(report, requestParams.Interaction.Variables);
                var json = JsonConvert.SerializeObject(variables, Formatting.None, new StringEnumConverter());
                return new StiWebActionResult(json, "application/json");
            }

            if (report == null) return StiWebActionResult.EmptyReportResult();

            // If report is renderen document, return current report
            if (report.IsDocument) return ViewerResult(requestParams, report);
            
            report = GetCompiledReport(report);

            // If report stored as StiReport object and process drill-down, we need to create report copy.
            if ((requestParams.Cache.Mode == StiServerCacheMode.ObjectCache || requestParams.Cache.Mode == StiServerCacheMode.ObjectSession) && requestParams.Interaction.DrillDown.Count > 0)
                report = CreateReportCopy(report);

            // Apply URL query parameters
            ApplyQueryParameters(requestParams, report);

            // Force build when updating a report
            if (requestParams.Action == StiAction.RefreshReport)
                report.IsRendered = false;

            // Apply report variables
            if (requestParams.Action == StiAction.Variables ||
                requestParams.Action == StiAction.Sorting ||
                requestParams.Action == StiAction.Collapsing) StiVariablesHelper.ApplyReportParameters(report, requestParams.Interaction.Variables);
            
            // Apply report interactions
            if (requestParams.Action == StiAction.Sorting) ApplySorting(report, requestParams.Interaction.Sorting);
            else if (requestParams.Action == StiAction.Collapsing) ApplyCollapsing(report, requestParams.Interaction.Collapsing);

            // Apply drill-down parameters
            if (requestParams.Interaction.DrillDown.Count > 0)
            {
                StiReport renderedReport = requestParams.Cache.Helper.GetReportInternal(requestParams, false);
                if (renderedReport == null) renderedReport = report;
                foreach (Hashtable parameters in requestParams.Interaction.DrillDown)
                    renderedReport = ApplyDrillDown(report, renderedReport, parameters);
                report = renderedReport;
            }
            
            if (!report.IsRendered)
            {
                try
                {
                    report.IsPreviewDialogs = true;
                    report.Render(false);
                    report.IsPreviewDialogs = false;
                }
                catch (Exception e)
                {
                    return new StiWebActionResult("ServerError:" + e.Message);
                }
            }

            requestParams.Cache.Helper.SaveReportInternal(requestParams, report);
            return ViewerResult(requestParams, report);
        }
        #endregion
        
        #region Open
        public static StiWebActionResult OpenReportResult(StiRequestParams requestParams)
        {
            var report = new StiReport();
            string fileName = requestParams.Viewer.OpeningFileName;
            string password = requestParams.Viewer.OpeningFilePassword;

            var stream = new MemoryStream();
            if (requestParams.Data != null) stream.Write(requestParams.Data, 0, requestParams.Data.Length);
            stream.Position = 0;

            try
            {
                if (string.IsNullOrEmpty(password)) report.LoadDocument(stream);
                else report.LoadEncryptedDocument(stream, password);

                requestParams.Cache.Helper.SaveReportInternal(requestParams, report);
            }
            catch (Exception e)
            {
                return new StiWebActionResult("ServerError:" + e.Message);
            }
            finally
            {
                stream.Close();
            }

            return ViewerResult(requestParams, report);
        }
        #endregion

        #endregion
    }
}
