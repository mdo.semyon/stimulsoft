﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Report.Dictionary;
using System;
using System.Collections;

namespace Stimulsoft.Report.Web
{
    internal class StiReportResourceHelper
    {
        public static ArrayList GetResourcesItems(StiReport report)
        {
            var items = new ArrayList();
            foreach (StiResource resource in report.Dictionary.Resources)
            {
                if (resource.AvailableInTheViewer)
                {
                    Hashtable resourceItem = new Hashtable();
                    resourceItem["name"] = resource.Name;
                    resourceItem["alias"] = resource.Alias;
                    resourceItem["type"] = resource.Type;
                    resourceItem["size"] = resource.Content != null ? resource.Content.Length : 0;

                    items.Add(resourceItem);
                }
            }

            return items.Count > 0 ? items : null;
        }

        public static string GetResourceContentType(StiResource resource)
        {
            switch (resource.Type)
            {
                case StiResourceType.ReportSnapshot:
                case StiResourceType.Report: return "application/octet-stream";
                case StiResourceType.Pdf: return "application/pdf";
                case StiResourceType.Txt: return "text/plain";
                case StiResourceType.Json: return "text/plain";
                case StiResourceType.Rtf: return "application/rtf";
                case StiResourceType.Word: return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case StiResourceType.Excel: return "application/vnd.ms-excel";
                case StiResourceType.Csv: return "text/csv";
                case StiResourceType.Dbf: return "application/dbf";
                case StiResourceType.Image:
                    {
                        if (Stimulsoft.Report.Helpers.StiImageHelper.IsPng(resource.Content)) return "image/png";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsMetafile(resource.Content)) return "image/x-wmf";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsBmp(resource.Content)) return "image/bmp";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsJpeg(resource.Content)) return "image/jpeg";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsGif(resource.Content)) return "image/gif";
                        else if (Stimulsoft.Base.Helpers.StiSvgHelper.IsSvg(resource.Content)) return "image/svg+xml";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsTiff(resource.Content)) return "image/tiff";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsEmf(resource.Content)) return "image/x-emf";

                        return "image/png";
                    }
            }
            return "text/plain";
        }

        public static string GetResourceFileExt(StiResource resource)
        {
            switch (resource.Type)
            {
                case StiResourceType.ReportSnapshot: return ".mdc";
                case StiResourceType.Report: return ".mrt";
                case StiResourceType.Pdf: return ".pdf";
                case StiResourceType.Txt: return ".txt";
                case StiResourceType.Rtf: return ".rtf";
                case StiResourceType.Word: return ".docx";
                case StiResourceType.Excel: return ".xlsx";
                case StiResourceType.Csv: return ".csv";
                case StiResourceType.Dbf: return ".dbf";
                case StiResourceType.Json: return ".json";
                case StiResourceType.Image:
                    {
                        if (Stimulsoft.Report.Helpers.StiImageHelper.IsPng(resource.Content)) return ".png";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsMetafile(resource.Content)) return ".wmf";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsBmp(resource.Content)) return ".bmp";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsJpeg(resource.Content)) return ".jpg";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsGif(resource.Content)) return ".gif";
                        else if (Stimulsoft.Base.Helpers.StiSvgHelper.IsSvg(resource.Content)) return ".svg";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsTiff(resource.Content)) return ".tiff";
                        else if (Stimulsoft.Report.Helpers.StiImageHelper.IsEmf(resource.Content)) return ".emf";

                        return ".png";
                    }
            }

            return string.Empty;
        }

        public static string GetBase64DataFromFontResourceContent(StiResourceType resourceType, byte[] content)
        {
            if (content != null)
            {
                var mimeType = "application/octet-stream";
                switch (resourceType)
                {
                    case StiResourceType.FontEot:
                        {
                            mimeType = "application/vnd.ms-fontobject";
                            break;
                        }
                    case StiResourceType.FontTtf:
                        {
                            mimeType = "application/x-font-ttf";
                            break;
                        }
                    case StiResourceType.FontWoff:
                        {
                            mimeType = "application/font-woff";
                            break;
                        }
                    case StiResourceType.FontOtf:
                        {
                            mimeType = "application/x-font-opentype";
                            break;
                        }
                }
                return String.Format("data:{0};base64,{1}", mimeType, Convert.ToBase64String(content));
            }
            else
                return String.Empty;
        }

        public static bool IsFontResourceType(StiResourceType resourceType)
        {
            return
                //resourceType == StiResourceType.FontEot ||
                //resourceType == StiResourceType.FontWoff |
                resourceType == StiResourceType.FontOtf ||
                resourceType == StiResourceType.FontTtc ||
                resourceType == StiResourceType.FontTtf;
        }

        public static ArrayList GetFontResourcesArray(StiReport report)
        {
            ArrayList fontResources = new ArrayList();

            if (report != null)
            {
                foreach (StiResource resource in report.Dictionary.Resources)
                {
                    if (IsFontResourceType(resource.Type))
                    {
                        Hashtable fontResourceItem = new Hashtable();
                        fontResourceItem["contentForCss"] = StiReportResourceHelper.GetBase64DataFromFontResourceContent(resource.Type, resource.Content);
                        fontResourceItem["originalFontFamily"] = StiFontCollection.GetFontFamily(report.GetResourceFontName(resource.Name)).Name;
                        fontResources.Add(fontResourceItem);
                    }
                }
            }

            return fontResources;
        }
    }
}