﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Collections;
using Stimulsoft.Report.Components;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Chart;
using Stimulsoft.Report.Dictionary;
using System.CodeDom.Compiler;
using System.Xml;
using Stimulsoft.Report.BarCodes;
using System.Drawing.Imaging;
using System.Drawing;

namespace Stimulsoft.Report.Web
{
    internal class StiReportHelperFx
    {
        /// <summary>
        /// Gets or sets an images quality for the RichText and Charts components.
        /// </summary>
        public static StiImagesQuality ImagesQuality = StiImagesQuality.Normal;

        /// <summary>
        /// Converting StiChart and StiRichText to the image (StiImage) for proper display on the WebViewerFx page.
        /// </summary>
        private static XmlDocument SaveReportSnapshot(StiReport report)
        {
            #region Replace components to images

            Hashtable storeImages = new Hashtable();
            foreach (StiPage page in report.RenderedPages)
            {
                report.RenderedPages.GetPage(page);
                for (int index = 0; index < page.Components.Count; index++)
                {
                    StiComponent comp = page.Components[index];
                    if ((comp is StiChart) || (comp is StiRichText) || (comp is StiBarCode && StiOptions.Engine.RenderBarCodeAsBitmap))
                    {
                        StiImage image = new StiImage(comp.ClientRectangle);
                        image.Name = comp.Name;
                        image.Guid = comp.Guid;
                        image.Stretch = true;
                        image.Smoothing = true;

                        float zoom = 0f;
                        switch (ImagesQuality)
                        {
                            case StiImagesQuality.Low: zoom = 0.5f; break;
                            case StiImagesQuality.Normal: zoom = 1f; break;
                            case StiImagesQuality.High: zoom = 2f; break;
                        }

                        if (comp is StiChart)
                        {
                            image.PutImage((comp as StiChart).GetImage(ref zoom, StiExportFormat.ImagePng));
                            image.Border = (comp as StiChart).Border.Clone() as StiBorder;
                        }

                        if (comp is StiRichText)
                        {
                            image.PutImage((comp as StiRichText).GetImage(ref zoom, StiExportFormat.ImagePng));
                            image.Border = (comp as StiRichText).Border.Clone() as StiBorder;
                        }

                        if (comp is StiBarCode)
                        {
                            image.PutImage((comp as StiBarCode).GetImage(ref zoom, StiExportFormat.ImagePng));
                            image.Border = (comp as StiBarCode).Border.Clone() as StiBorder;
                        }

                        page.Components[index] = image;
                        storeImages[image] = comp;
                    }
                }
            }

            #endregion

            #region Save document to XML string

            XmlDocument xml = new XmlDocument();
            Stream stream = new MemoryStream();
            report.SaveDocument(stream);
            stream.Position = 0;
            xml.Load(stream);
            stream.Flush();
            stream.Close();

            // Add the IsDocument attribute to main node, if there is a report template
            if (!report.IsDocument)
            {
                XmlAttribute attribute = xml.CreateAttribute("isDocument");
                attribute.InnerText = "False";
                xml.LastChild.Attributes.Append(attribute);
            }

            #endregion

            #region Restore original components

            foreach (StiPage page in report.RenderedPages)
            {
                report.RenderedPages.GetPage(page);
                for (int index = 0; index < page.Components.Count; index++)
                {
                    StiComponent comp = page.Components[index];
                    if (storeImages.ContainsKey(comp))
                    {
                        page.Components[index] = (StiComponent)storeImages[comp];
                    }
                }
            }
            storeImages = null;

            #endregion

            return xml;
        }

        /// <summary>
        /// Save report template to XML string.
        /// </summary>
        private static XmlDocument SaveReportTemplate(StiReport report)
        {
            XmlDocument xml = new XmlDocument();
            Stream stream = new MemoryStream();
            report.Save(stream);
            stream.Position = 0;
            xml.Load(stream);
            stream.Flush();
            stream.Close();

            return xml;
        }

        /// <summary>
        /// Prepare information about report interactions as XML.
        /// </summary>
        public static XmlDocument GetInteractions(StiReport report)
        {
            XmlDocument xml = new XmlDocument();
            XmlElement interactions = xml.CreateElement("Interactions");
            xml.AppendChild(interactions);

            XmlElement pageElement = null;
            XmlElement componentElement = null;
            bool isInteraction = false;
            int pageIndex = 0;
            Hashtable dataBandsSorting = new Hashtable();

            #region Search interactions in rendered report

            foreach (StiPage page in report.RenderedPages)
            {
                pageElement = xml.CreateElement("Page");
                pageElement.SetAttribute("index", pageIndex.ToString());
                interactions.AppendChild(pageElement);

                int componentIndex = 0;
                foreach (StiComponent comp in page.GetComponents())
                {
                    IStiInteraction interactionComp = comp as IStiInteraction;
                    if (interactionComp != null && interactionComp.Interaction != null)
                    {
                        StiInteraction interaction = interactionComp.Interaction;

                        componentElement = xml.CreateElement("Component");
                        componentElement.SetAttribute("index", componentIndex.ToString());

                        if (interaction.SortingEnabled && !string.IsNullOrEmpty(interaction.SortingColumn))
                        {
                            isInteraction = true;

                            // Saving the information about Data Band sorting
                            string dataBandName = interaction.GetSortDataBandName();
                            if (!dataBandsSorting.ContainsKey(dataBandName))
                            {
                                dataBandsSorting[dataBandName] = string.Empty;
                                StiDataBand dataBand = report.GetComponentByName(dataBandName) as StiDataBand;
                                foreach (string sort in dataBand.Sort)
                                {
                                    if ((string)dataBandsSorting[dataBandName] == string.Empty) dataBandsSorting[dataBandName] = sort;
                                    else dataBandsSorting[dataBandName] += ";" + sort;
                                }
                            }

                            string direction = string.Empty;
                            switch (interaction.SortingDirection)
                            {
                                case StiInteractionSortDirection.Ascending: direction = "Ascending"; break;
                                case StiInteractionSortDirection.Descending: direction = "Descending"; break;
                                case StiInteractionSortDirection.None: direction = "None"; break;
                            }

                            componentElement.SetAttribute("sortingEnabled", interaction.SortingEnabled.ToString());
                            componentElement.SetAttribute("column", XmlConvert.EncodeName(interaction.SortingColumn));
                            componentElement.SetAttribute("sortingIndex", interaction.SortingIndex.ToString());
                            componentElement.SetAttribute("direction", direction);
                        }

                        if (interaction.DrillDownEnabled && (interaction.DrillDownPage != null || !string.IsNullOrEmpty(interaction.DrillDownReport)))
                        {
                            isInteraction = true;

                            string drillDownPageGuid = string.Empty;
                            if (interaction.DrillDownPage != null) drillDownPageGuid = interaction.DrillDownPage.Guid;

                            componentElement.SetAttribute("drillDownEnabled", interaction.DrillDownEnabled.ToString());
                            componentElement.SetAttribute("pageGuid", drillDownPageGuid);
                            componentElement.SetAttribute("reportFile", XmlConvert.EncodeName(interaction.DrillDownReport));
                        }

                        pageElement.AppendChild(componentElement);
                    }

                    componentIndex++;
                }

                pageIndex++;
            }

            foreach (string dataBandName in dataBandsSorting.Keys)
            {
                pageElement = xml.CreateElement("DataBand");
                pageElement.SetAttribute("name", XmlConvert.EncodeName(dataBandName));
                pageElement.SetAttribute("sort", (string)dataBandsSorting[dataBandName]);
                interactions.AppendChild(pageElement);
            }

            #endregion

            if (isInteraction) return xml;
            return null;
        }

        /// <summary>
        /// Check variables for init by expression mode and set the correct values in this case
        /// </summary>
        private static void CheckForInitByExpression(StiReport report)
        {
            foreach (StiVariable variable in report.Dictionary.Variables)
            {
                if (variable.RequestFromUser && variable.InitBy == StiVariableInitBy.Expression)
                {
                    variable.ValueObject = report[variable.Name];
                    variable.InitBy = StiVariableInitBy.Value;
                }
            }
        }

        /// <summary>
        /// Creating a list of errors for the embedded preview of the report.
        /// </summary>
        private static string GetErrorList(StiReport report)
        {
            if (report.CompilerResults != null && report.CompilerResults.Errors.Count > 0)
            {
                XmlDocument xml = new XmlDocument();
                XmlDeclaration declaration = xml.CreateXmlDeclaration("1.0", "utf-8", null);
                xml.AppendChild(declaration);

                XmlElement errorNode;
                XmlElement element;
                XmlElement listNode = xml.CreateElement("ErrorList");
                xml.AppendChild(listNode);

                #region Create errors list

                int index = 0;
                foreach (CompilerError error in report.CompilerResults.Errors)
                {
                    index++;

                    errorNode = xml.CreateElement("Error" + index.ToString());
                    listNode.AppendChild(errorNode);

                    element = xml.CreateElement("FileName");
                    element.InnerXml = error.FileName;
                    errorNode.AppendChild(element);

                    element = xml.CreateElement("Line");
                    element.InnerXml = error.Line.ToString();
                    errorNode.AppendChild(element);

                    element = xml.CreateElement("Column");
                    element.InnerXml = error.Column.ToString();
                    errorNode.AppendChild(element);

                    element = xml.CreateElement("ErrorNumber");
                    element.InnerXml = error.ErrorNumber;
                    errorNode.AppendChild(element);

                    element = xml.CreateElement("ErrorText");
                    element.InnerXml = error.ErrorText;
                    errorNode.AppendChild(element);
                }

                #endregion

                return xml.OuterXml;
            }

            return string.Empty;
        }

        public static StiWebActionResult GetComponentImageResult(StiRequestParams requestParams)
        {
            if (requestParams.Report != null)
            {
                requestParams.Report.IsPageDesigner = true;
                StiChart chart = requestParams.Report.Pages[0].Components[0] as StiChart;
                StiRichText richText = requestParams.Report.Pages[0].Components[0] as StiRichText;

                float zoom = 0.5f;
                Image image = null;
                if (chart != null) image = chart.GetImage(ref zoom, StiExportFormat.ImagePng);
                if (richText != null) image = richText.GetImage(ref zoom, StiExportFormat.ImagePng);

                if (image != null)
                {
                    MemoryStream imageStream = new MemoryStream();
                    image.Save(imageStream, ImageFormat.Png);
                    return new StiWebActionResult(imageStream, "image/png");
                }
            }

            return new StiWebActionResult();
        }

        public static StiWebActionResult GetReportCodeResult(StiRequestParams requestParams)
        {
            if (requestParams.Report == null) return StiWebActionResult.EmptyReportResult();
            string reportCode = requestParams.Report.SaveReportSourceCode();
            return StiWebActionResult.StringResult(requestParams, reportCode);
        }

        /// <summary>
        /// Get the report template result for Designer
        /// </summary>
        public static StiWebActionResult GetReportTemplateResult(StiRequestParams requestParams, StiReport report, string errorString)
        {
            if (report == null) return StiWebActionResult.EmptyReportResult();

            XmlElement element;
            XmlDocument template = SaveReportTemplate(report);
            XmlDocument xml = new XmlDocument();

            XmlDeclaration declaration = xml.CreateXmlDeclaration("1.0", "utf-8", null);
            xml.AppendChild(declaration);

            XmlElement container = xml.CreateElement("Container");
            xml.AppendChild(container);

            if (!string.IsNullOrEmpty(errorString))
            {
                element = xml.CreateElement("ErrorString");
                element.InnerText = errorString;
                container.AppendChild(element);
            }

            element = xml.CreateElement("IsModified");
            element.InnerText = report.IsModified.ToString();
            container.AppendChild(element);

            element = xml.CreateElement("ReportTemplate");
            element.InnerXml = template.LastChild.OuterXml;
            container.AppendChild(element);

            return StiWebActionResult.StringResult(requestParams, xml.OuterXml);
        }

        /// <summary>
        /// Get the report snapshot result for Viewer or Designer Preview
        /// </summary>
        public static StiWebActionResult GetReportSnapshotResult(StiRequestParams requestParams, StiReport report)
        {
            if (report == null) return StiWebActionResult.EmptyReportResult();

            // For other actions this is already done in the GetInteractionResult
            if (!report.IsDocument && requestParams.Action == StiAction.GetReport)
            {
                #region Get the compilation errors

                report = StiReportHelper.GetCompiledReport(report);
                string errorsList = GetErrorList(report);
                if (!string.IsNullOrEmpty(errorsList)) return StiWebActionResult.StringResult(requestParams, errorsList);

                #endregion

                StiVariablesHelper.FillDialogInfoItems(report);
            }

            #region Prepare XML container

            XmlDocument interactions = GetInteractions(report);
            XmlDocument snapshot = SaveReportSnapshot(report);

            XmlDocument xml = new XmlDocument();
            XmlDeclaration declaration = xml.CreateXmlDeclaration("1.0", "utf-8", null);
            xml.AppendChild(declaration);

            XmlElement container = xml.CreateElement("Container");
            xml.AppendChild(container);

            XmlElement element = xml.CreateElement("ReportSnapshot");
            if (requestParams.Action == StiAction.DrillDown) element.SetAttribute("type", "DrillDown");
            if (requestParams.Action == StiAction.Sorting) element.SetAttribute("type", "Sorting");
            element.InnerXml = snapshot.LastChild.OuterXml;
            container.AppendChild(element);

            if (interactions != null)
            {
                element = xml.CreateElement("Interactions");
                element.InnerXml = interactions.LastChild.InnerXml;
                container.AppendChild(element);
            }

            #endregion

            return StiWebActionResult.StringResult(requestParams, xml.OuterXml);
        }

        /// <summary>
        /// Get the report snapshot, which is built using the parameters
        /// </summary>
        public static StiWebActionResult GetInteractionResult(StiRequestParams requestParams, StiReport report)
        {
            if (!report.IsDocument)
            {
                #region Get the compilation errors

                report = StiReportHelper.GetCompiledReport(report);

                // If report stored as StiReport object and process drill-down, we need to create report copy.
                if ((requestParams.Cache.Mode == StiServerCacheMode.ObjectCache || requestParams.Cache.Mode == StiServerCacheMode.ObjectSession) && requestParams.Interaction.DrillDown.Count > 0)
                    report = StiReportHelper.CreateReportCopy(report);

                string errorsList = GetErrorList(report);
                if (!string.IsNullOrEmpty(errorsList)) return StiWebActionResult.StringResult(requestParams, errorsList);

                #endregion

                #region Apply all interactions
                
                StiVariablesHelper.FillDialogInfoItems(report);

                if (requestParams.Action == StiAction.Variables || requestParams.Action == StiAction.Sorting)
                    StiVariablesHelper.ApplyReportParameters(report, requestParams.Interaction.Variables);

                if (requestParams.Action == StiAction.Sorting)
                    StiReportHelper.ApplySorting(report, requestParams.Interaction.Sorting);

                if (!report.IsRendered)
                {
                    try
                    {
                        report.IsPreviewDialogs = true;
                        report.Render(false);
                        report.IsPreviewDialogs = false;
                    }
                    catch (Exception e)
                    {
                        return StiWebActionResult.ErrorResult(requestParams, e.Message);
                    }
                }

                // Apply drill-down parameters
                if (requestParams.Interaction.DrillDown.Count > 0)
                {
                    StiReport renderedReport = report;
                    foreach (Hashtable parameters in requestParams.Interaction.DrillDown) renderedReport = StiReportHelper.ApplyDrillDown(report, renderedReport, parameters);
                    report = renderedReport;
                }

                CheckForInitByExpression(report);

                #endregion

                requestParams.Cache.Helper.SaveReportInternal(requestParams, report);
            }

            return GetReportSnapshotResult(requestParams, report);
        }
    }
}
