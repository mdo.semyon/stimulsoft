﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Text;
using System.Web;
using System.Collections;
using Stimulsoft.Base.Json;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base;
using System.Xml;
using System.IO;

#if NETCORE
using Stimulsoft.System.Web;
using Stimulsoft.System.Web.Caching;
#else
using System.Web.Caching;
#endif

namespace Stimulsoft.Report.Web
{
    internal class StiRequestParamsHelper
    {
        public static StiRequestParams Get()
        {
            return Get(HttpContext.Current);
        }

        public static StiRequestParams Get(HttpContext httpContext)
        {
            StiRequestParams requestParams = new StiRequestParams();
            requestParams.HttpContext = httpContext;

            if (httpContext != null)
            {
                foreach (string key in httpContext.Request.Params.Keys)
                {
                    if (key != null && key.StartsWith("stiweb_")) requestParams.All[key] = httpContext.Request.Params[key].ToString();
                }

                if (requestParams.Contains("stiweb_component")) requestParams.Component = (StiComponentType)requestParams.GetEnum("stiweb_component", typeof(StiComponentType));
                if (requestParams.Contains("stiweb_action")) requestParams.Action = (StiAction)requestParams.GetEnum("stiweb_action", typeof(StiAction));
                if (requestParams.Contains("stiweb_theme")) requestParams.Theme = requestParams.GetString("stiweb_theme");
                if (requestParams.Contains("stiweb_cloudmode")) requestParams.CloudMode = true;
                if (requestParams.Contains("stiweb_version")) requestParams.Version = requestParams.GetString("stiweb_version");
                if (requestParams.Contains("stiweb_loc"))
                {
                    string base64 = requestParams.GetString("stiweb_loc");
                    requestParams.Localization = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
                }
                if (requestParams.Contains("stiweb_data"))
                {
                    if (requestParams.Action == StiAction.Resource) requestParams.Resource = requestParams.GetString("stiweb_data");
                    else requestParams.Data = Convert.FromBase64String(requestParams.GetString("stiweb_data"));
                }
                if (requestParams.Contains("stiweb_parameters"))
                {
                    var data = Convert.FromBase64String(requestParams.GetString("stiweb_parameters"));
                    var json = Encoding.UTF8.GetString(data);
                    var container = JsonConvert.DeserializeObject<JContainer>(json);
                    ParseParameters(container, requestParams.All, null);
                    requestParams.HasParameters = true;
                    requestParams.Server.UseCompression = false;
                }
                if (requestParams.Contains("stiweb_packed_parameters"))
                {
                    var bytes = Convert.FromBase64String(requestParams.GetString("stiweb_packed_parameters"));
                    var data = StiGZipHelper.Unpack(bytes);
                    var json = Encoding.UTF8.GetString(data);
                    var container = JsonConvert.DeserializeObject<JContainer>(json);
                    ParseParameters(container, requestParams.All, null);
                    requestParams.HasParameters = true;
                    requestParams.Server.UseCompression = true;
                }
                if (requestParams.Contains("stiweb_cachemode"))
                {
                    var cacheMode = requestParams.GetString("stiweb_cachemode");
                    if (cacheMode == "none") requestParams.Cache.Mode = StiServerCacheMode.None;
                    else if (cacheMode == "cache") requestParams.Cache.Mode = StiServerCacheMode.ObjectCache;
                    else if (cacheMode == "session") requestParams.Cache.Mode = StiServerCacheMode.ObjectSession;
                }

                #region Get HTML5 Viewer parameters
                if (requestParams.HasParameters && requestParams.Component == StiComponentType.Viewer)
                {
                    requestParams.Id = requestParams.GetString("viewerId");
                    requestParams.Routes = requestParams.GetNameValueCollection("routes");
                    requestParams.FormValues = requestParams.GetNameValueCollection("formValues");
                    requestParams.CloudMode = requestParams.CloudMode || requestParams.GetBoolean("cloudMode");
                    requestParams.ExportFormat = (StiExportFormat)requestParams.GetEnum("exportFormat", typeof(StiExportFormat));
                    requestParams.ExportSettings = requestParams.GetHashtable("exportSettings");
                    requestParams.Version = requestParams.GetString("version");
                    requestParams.Cache.ClientGuid = requestParams.GetString("clientGuid");
                    requestParams.Cache.DrillDownGuid = requestParams.GetString("drillDownGuid");
                    requestParams.Cache.Timeout = new TimeSpan(0, int.Parse(requestParams.GetString("cacheTimeout")), 0);
                    requestParams.Cache.Mode = (StiServerCacheMode)requestParams.GetEnum("cacheMode", typeof(StiServerCacheMode));
                    requestParams.Cache.Priority = (CacheItemPriority)requestParams.GetEnum("cacheItemPriority", typeof(CacheItemPriority));
                    requestParams.Interaction.Variables = requestParams.GetHashtable("variables");
                    requestParams.Interaction.Sorting = requestParams.GetHashtable("sortingParameters");
                    requestParams.Interaction.Collapsing = requestParams.GetHashtable("collapsingParameters");
                    requestParams.Interaction.DrillDown = requestParams.GetArray("drillDownParameters");
                    requestParams.Interaction.Editable = requestParams.GetHashtable("editableParameters");
                    requestParams.Interaction.Editable = requestParams.GetHashtable("editableParameters");
                    requestParams.Viewer.PageNumber = requestParams.GetInt("pageNumber");
                    requestParams.Viewer.Zoom = Convert.ToDouble(requestParams.GetString("zoom")) / 100;
                    requestParams.Viewer.ShowBookmarks = requestParams.GetBoolean("showBookmarks");
                    requestParams.Viewer.BookmarksPrint = requestParams.GetBoolean("bookmarksPrint");
                    requestParams.Viewer.OpeningFileName = requestParams.GetString("openingFileName");
                    requestParams.Viewer.OpeningFilePassword = requestParams.GetString("openingFilePassword");
                    requestParams.Viewer.ViewMode = (StiWebViewMode)requestParams.GetEnum("viewMode", typeof(StiWebViewMode));
                    requestParams.Viewer.ChartRenderType = (StiChartRenderType)requestParams.GetEnum("chartRenderType", typeof(StiChartRenderType));
                    requestParams.Viewer.ReportDisplayMode = (StiReportDisplayMode)requestParams.GetEnum("reportDisplayMode", typeof(StiReportDisplayMode));
                    requestParams.Viewer.PrintAction = (StiPrintAction)requestParams.GetEnum("printAction", typeof(StiPrintAction));
                    requestParams.Viewer.OpenLinksWindow = requestParams.GetString("openLinksWindow");
                    requestParams.Viewer.ReportDesignerMode = requestParams.GetBoolean("reportDesignerMode");
                    requestParams.Server.UseRelativeUrls = requestParams.GetBoolean("useRelativeUrls");
                    requestParams.Server.PassQueryParametersForResources = requestParams.GetBoolean("passQueryParametersForResources");
                    requestParams.Server.PassQueryParametersToReport = requestParams.GetBoolean("passQueryParametersToReport");
                    requestParams.ReportResourceParams = requestParams.GetHashtable("reportResourceParams");

                }
                #endregion

                #region Get HTML5 Designer parameters
                if (requestParams.HasParameters && requestParams.Component == StiComponentType.Designer)
                {
                    requestParams.Id = requestParams.GetString("designerId");
                    requestParams.Routes = requestParams.GetNameValueCollection("routes");
                    requestParams.FormValues = requestParams.GetNameValueCollection("formValues");
                    requestParams.CloudMode = requestParams.CloudMode || requestParams.GetBoolean("cloudMode");
                    requestParams.Version = requestParams.GetString("version");
                    requestParams.Cache.ClientGuid = requestParams.GetString("clientGuid");
                    requestParams.Cache.Timeout = new TimeSpan(0, int.Parse(requestParams.GetString("cacheTimeout")), 0);
                    requestParams.Cache.Mode = (StiServerCacheMode)requestParams.GetEnum("cacheMode", typeof(StiServerCacheMode));
                    requestParams.Cache.Priority = (CacheItemPriority)requestParams.GetEnum("cacheItemPriority", typeof(CacheItemPriority));
                    requestParams.Designer.Command = (StiDesignerCommand)requestParams.GetEnum("command", typeof(StiDesignerCommand));
                    requestParams.Designer.IsSaveAs = requestParams.Designer.Command == StiDesignerCommand.SaveAsReport;
                    requestParams.Designer.UndoMaxLevel = requestParams.GetInt("undoMaxLevel");
                    requestParams.Server.UseRelativeUrls = requestParams.GetBoolean("useRelativeUrls");
                    requestParams.Server.PassQueryParametersForResources = requestParams.GetBoolean("passQueryParametersForResources");

                    if (requestParams.Contains("checkReportBeforePreview")) requestParams.Designer.CheckReportBeforePreview = requestParams.GetBoolean("checkReportBeforePreview");
                    if (requestParams.Contains("localization")) requestParams.Localization = requestParams.GetString("localization");

                    if (requestParams.Contains("openReportFile")) requestParams.Designer.FileName = requestParams.GetString("openReportFile");
                    else requestParams.Designer.FileName = requestParams.GetString("reportFile");

                    if (requestParams.Contains("encryptedPassword")) requestParams.Designer.Password = requestParams.GetString("encryptedPassword");
                    else requestParams.Designer.Password = requestParams.GetString("password");

                    if (requestParams.Contains("saveType")) requestParams.Designer.SaveType = requestParams.GetString("saveType");

                    #region Correct designer action
                    switch (requestParams.Designer.Command)
                    {
                        case StiDesignerCommand.GetReportForDesigner:
                            requestParams.Action = StiAction.GetReport;
                            break;

                        case StiDesignerCommand.OpenReport:
                            requestParams.Action = StiAction.OpenReport;
                            break;

                        case StiDesignerCommand.CreateReport:
                        case StiDesignerCommand.WizardResult:
                            requestParams.Action = StiAction.CreateReport;
                            break;

                        case StiDesignerCommand.SaveReport:
                        case StiDesignerCommand.SaveAsReport:
                            requestParams.Action = StiAction.SaveReport;
                            break;

                        case StiDesignerCommand.LoadReportToViewer:
                            requestParams.Action = StiAction.PreviewReport;
                            break;

                        case StiDesignerCommand.ExitDesigner:
                            requestParams.Action = StiAction.Exit;
                            break;
                    }
                    #endregion
                }
                #endregion

                #region Get Flash parameters
                if (requestParams.HasParameters && (requestParams.Component == StiComponentType.ViewerFx || requestParams.Component == StiComponentType.DesignerFx))
                {
                    requestParams.Id = requestParams.GetString("Id");
                    requestParams.Routes = requestParams.GetNameValueCollection("Routes");
                    requestParams.Cache.ClientGuid = requestParams.GetString("ClientGuid");
                    requestParams.Cache.Timeout = new TimeSpan(0, int.Parse(requestParams.GetString("CacheTimeout")), 0);
                    requestParams.Cache.Mode = (StiServerCacheMode)requestParams.GetEnum("CacheMode", typeof(StiServerCacheMode));
                    requestParams.Cache.Priority = (CacheItemPriority)requestParams.GetEnum("CacheItemPriority", typeof(CacheItemPriority));
                    requestParams.ExportFormat = (StiExportFormat)requestParams.GetEnum("Format", typeof(StiExportFormat));
                    requestParams.ExportSettings = requestParams.GetHashtable("Settings");
                    if (requestParams.All.Contains("OpenAfterExport")) requestParams.ExportSettings["OpenAfterExport"] = requestParams.GetBoolean("OpenAfterExport");
                    if (requestParams.Action == StiAction.GetLocalization) requestParams.Localization = requestParams.GetString("Data");
                    requestParams.Viewer.OpeningFileName = requestParams.GetString("ReportFileName");

                    #region Designer
                    requestParams.Designer.IsAutoSave = requestParams.GetBoolean("AutoSave");
                    requestParams.Designer.IsNewReport = requestParams.GetBoolean("NewReport");
                    requestParams.Designer.IsSaveAs = requestParams.GetBoolean("SaveAs");
                    requestParams.Designer.FileName = requestParams.GetString("ReportFile");

                    #region Dictionary
                    requestParams.Dictionary.ConnectionType = requestParams.GetString("ConnectionType");
                    requestParams.Dictionary.DataPath = requestParams.GetString("DataPath");
                    requestParams.Dictionary.SchemaPath = requestParams.GetString("SchemaPath");
                    requestParams.Dictionary.UserName = requestParams.GetString("UserName");
                    if (requestParams.All.Contains("Password"))
                    {
                        string base64 = requestParams.GetString("Password");
                        requestParams.Dictionary.Password = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
                    }
                    if (requestParams.All.Contains("ConnectionString"))
                    {
                        string base64 = requestParams.GetString("ConnectionString");
                        requestParams.Dictionary.ConnectionString = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
                    }
                    if (requestParams.All.Contains("Query"))
                    {
                        string base64 = requestParams.GetString("Query");
                        requestParams.Dictionary.Query = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
                    }
                    #endregion
                    
                    #endregion

                    #region Load report from client
                    if (requestParams.All.Contains("ReportSnapshot"))
                    {
                        byte[] buffer = Convert.FromBase64String(requestParams.GetString("ReportSnapshot"));
                        if (buffer.Length > 0)
                        {
                            requestParams.Report = new StiReport();
                            requestParams.Report.LoadDocument(buffer);
                        }
                    }

                    if (requestParams.All.Contains("ReportTemplate"))
                    {
                        byte[] buffer = Convert.FromBase64String(requestParams.GetString("ReportTemplate"));
                        if (buffer.Length > 0)
                        {
                            requestParams.Report = new StiReport();
                            requestParams.Report.Load(buffer);
                        }
                    }
                    #endregion

                    #region Convert Flash variables
                    Hashtable variablesList = requestParams.GetHashtable("VariablesList");
                    if (variablesList != null && variablesList.Count > 0)
                    {
                        requestParams.Interaction.Variables = new Hashtable();
                        foreach (string key in variablesList.Keys)
                        {
                            if (key.StartsWith("@"))
                            {
                                string valueKey = key.Substring(1);
                                if (variablesList.Contains(valueKey))
                                {
                                    Hashtable parameter = variablesList[key] as Hashtable;
                                    if (parameter.Contains("isNull")) requestParams.Interaction.Variables[valueKey] = null;
                                    else if (parameter.Contains("isRange") && variablesList[valueKey] is string)
                                    {
                                        Hashtable range = new Hashtable();
                                        string[] values = ((string)variablesList[valueKey]).Split(',');
                                        range["from"] = XmlConvert.DecodeName(values[1]);
                                        range["to"] = XmlConvert.DecodeName(values[2]);
                                        requestParams.Interaction.Variables[valueKey] = range;
                                    }
                                    else if (parameter.Contains("isList"))
                                    {
                                        ArrayList list = new ArrayList();
                                        Hashtable hashList = variablesList[valueKey] as Hashtable;
                                        if (hashList != null)
                                        {
                                            foreach (DictionaryEntry value in hashList)
                                            {
                                                list.Add(XmlConvert.DecodeName((string)value.Value));
                                            }
                                        }
                                        
                                        requestParams.Interaction.Variables[valueKey] = list;
                                    }
                                }
                            }
                            else if (!variablesList.Contains("@" + key))
                            {
                                requestParams.Interaction.Variables[key] = variablesList[key];
                            }
                        }
                    }
                    #endregion

                    #region Convert Flash email options
                    if (requestParams.ExportSettings != null)
                    {
                        Hashtable emailOptions = requestParams.GetHashtable("EmailOptions");
                        if (emailOptions != null)
                        {
                            requestParams.ExportSettings.Add("Email", emailOptions["Email"]);
                            requestParams.ExportSettings.Add("Subject", emailOptions["Subject"]);
                            requestParams.ExportSettings.Add("Message", emailOptions["Message"]);
                        }
                    }
                    #endregion

                    #region Convert Flash sorting parameters
                    requestParams.Interaction.Sorting = requestParams.GetHashtable("Sorting");
                    if (requestParams.Interaction.Sorting != null && requestParams.Interaction.Sorting.Count > 0)
                    {
                        requestParams.Interaction.Sorting["DataBand"] = string.Format("{0};{1}",
                            ((Hashtable)requestParams.Interaction.Sorting["@DataBand"])["name"],
                            ((Hashtable)requestParams.Interaction.Sorting["@DataBand"])["sort"]);

                        requestParams.Interaction.Sorting["ComponentName"] = string.Format("{0};{1}",
                            requestParams.Interaction.Sorting["ComponentName"],
                            ((Hashtable)requestParams.Interaction.Sorting["@ComponentName"])["isCtrl"]);

                        requestParams.Interaction.Sorting.Remove("@DataBand");
                        requestParams.Interaction.Sorting.Remove("@ComponentName");
                    }
                    #endregion

                    #region Convert Flash drill-down parameters
                    Hashtable drillDownParameters = requestParams.GetHashtable("DrillDownParameters");
                    if (drillDownParameters != null && drillDownParameters.Count > 0)
                    {
                        requestParams.Interaction.DrillDown = new ArrayList();
                        foreach (Hashtable parameters in drillDownParameters.Values)
                        {
                            requestParams.Interaction.DrillDown.Add(parameters);
                        }
                    }
                    #endregion

                    #region Correct Flash client action
                    if (requestParams.GetBoolean("Print") == true) requestParams.Action = StiAction.PrintReport;
                    if (requestParams.Interaction.Variables != null && requestParams.Interaction.Variables.Count > 0) requestParams.Action = StiAction.Variables;
                    if (requestParams.Interaction.Sorting != null && requestParams.Interaction.Sorting.Count > 0) requestParams.Action = StiAction.Sorting;
                    if (requestParams.GetString("Action") == "DrillDown" && requestParams.Interaction.DrillDown != null && requestParams.Interaction.DrillDown.Count > 0) requestParams.Action = StiAction.DrillDown;
                    #endregion
                }
                #endregion
            }

            return requestParams;
        }

        private static void ParseParameters(JContainer container, Hashtable hash, ArrayList array)
        {
            if (hash == null && array == null) return;

            var count = -1;
            foreach (JToken token in container.Children())
            {
                count++;
                string name = token.Type == JTokenType.Property ? ((JProperty)token).Name : count.ToString();
                JToken value = token.Type == JTokenType.Property ? ((JProperty)token).Value : token;

                var childHash = value is JObject ? new Hashtable() : null;
                var childArray = value is JArray ? new ArrayList() : null;
                if (value is JContainer) ParseParameters((JContainer)value, childHash != null ? childHash : null, childArray != null ? childArray : null);

                if (hash != null)
                {
                    if (childHash != null) hash[name] = childHash;
                    else if (childArray != null) hash[name] = childArray;
                    else hash[name] = ((JValue)value).Value;
                }
                else
                {
                    if (childHash != null) array.Add(childHash);
                    else if (childArray != null) array.Add(childArray);
                    else array.Add(((JValue)value).Value);
                }
            }
        }
    }
}
