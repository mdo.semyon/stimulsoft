﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.IO;
using System.Xml;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Web
{
    internal class StiLocalizationHelperFx
    {
        public static StiWebActionResult GetLocalizationResult(StiRequestParams requestParams)
        {
            if (string.IsNullOrEmpty(requestParams.Localization)) return new StiWebActionResult();

            string filePath = requestParams.Localization;
            if (!filePath.ToLower().EndsWith(".xml")) filePath += ".xml";
            string absolutePath = filePath;
            if (!File.Exists(absolutePath)) absolutePath = requestParams.HttpContext.Server.MapPath(filePath);
            if (!File.Exists(absolutePath)) absolutePath = requestParams.HttpContext.Server.MapPath(Path.Combine("Localization", filePath));
            if (!File.Exists(absolutePath)) absolutePath = requestParams.HttpContext.Server.MapPath("/" + filePath);
            if (!File.Exists(absolutePath)) absolutePath = requestParams.HttpContext.Server.MapPath("/" + Path.Combine("Localization", filePath));
            /*if (!File.Exists(absolutePath)) absolutePath = requestParams.HttpContext.Server.MapRootPath(filePath);
            if (!File.Exists(absolutePath)) absolutePath = requestParams.HttpContext.Server.MapRootPath(Path.Combine("Localization", filePath));*/

            if (File.Exists(absolutePath))
            {
                string localizationFileExt = absolutePath.Substring(0, absolutePath.Length - 3) + "ext.xml";
                if (File.Exists(localizationFileExt))
                {
                    XmlDocument xml = new XmlDocument();
                    xml.Load(localizationFileExt);
                    XmlNode xmlNode = xml.GetElementsByTagName("Localization").Item(0);

                    XmlDocument resultXml = new XmlDocument();
                    resultXml.Load(absolutePath);
                    XmlNode element = resultXml.GetElementsByTagName("Localization").Item(0);
                    XmlNode node = resultXml.ImportNode(xmlNode, true);
                    element.AppendChild(node);

                    string base64Ext = StiGZipHelper.Pack(resultXml.InnerXml);
                    return new StiWebActionResult(base64Ext, "text/plain");
                }

                StreamReader reader = new StreamReader(absolutePath);
                string xmlString = reader.ReadToEnd();
                reader.Close();

                return StiWebActionResult.StringResult(requestParams, xmlString);
            }

            return new StiWebActionResult();
        }
    }
}
