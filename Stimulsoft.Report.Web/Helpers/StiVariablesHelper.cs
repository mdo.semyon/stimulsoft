﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using System.Globalization;
using Stimulsoft.Report.Engine;
using System.Drawing;

namespace Stimulsoft.Report.Web
{
    internal class StiVariablesHelper
    {
        #region Fields

        private static CultureInfo en_us_culture = CultureInfo.CreateSpecificCulture("en-US");

        #endregion

        /// <summary>
        /// Filling the list of variables initialized from the database columns
        /// </summary>
        public static void FillDialogInfoItems(StiReport report)
        {
            var isColumnsInitializationTypeItems = false;
            foreach (StiVariable variable in report.Dictionary.Variables)
            {
                if (variable.RequestFromUser && variable.DialogInfo.ItemsInitializationType == StiItemsInitializationType.Columns &&
                        (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0 || 
                         variable.DialogInfo.Values == null || variable.DialogInfo.Values.Length == 0))
                {
                    isColumnsInitializationTypeItems = true;
                    break;
                }
            }

            if (isColumnsInitializationTypeItems)
            {
                report.Dictionary.Connect();
                StiVariableHelper.FillItemsOfVariables(report);
                report.Dictionary.Disconnect();
            }
        }

        private static string GetVariableAlias(StiVariable variable)
        {
            if (string.IsNullOrEmpty(variable.Alias)) return variable.Name;
            return variable.Alias;
        }

        private class StiVariableItemsComparer : IComparer
        {
            int IComparer.Compare(Object x, Object y)
            {
                Hashtable item1 = x as Hashtable;
                Hashtable item2 = y as Hashtable;
                if (item1 != null && item2 != null)
                {
                    string value1 = item1["value"] as string ?? item1["key"] as string;
                    string value2 = item2["value"] as string ?? item2["key"] as string;

                    if (value1 != null && value2 != null)
                        return value1.CompareTo(value2);
                    else
                        return 0;
                }
                return 0;
            }
        }

        private static ArrayList GetItems(StiVariable variable)
        {
            var items = new ArrayList();
            string valueBinding = variable.DialogInfo.BindingVariable != null ? variable.DialogInfo.BindingVariable.Value : null;
            int index = 0;

            if (variable.DialogInfo.Keys != null && variable.DialogInfo.Keys.Length != 0)
            {
                List<StiDialogInfoItem> itemsVariable = variable.DialogInfo.GetDialogInfoItems(variable.Type);
                Hashtable bindingValues = new Hashtable();

                foreach (StiDialogInfoItem itemVariable in itemsVariable)
                {
                    if (valueBinding == null || valueBinding == Convert.ToString(itemVariable.ValueBinding))
                    {
                        var item = new Hashtable();
                        item["value"] = itemVariable.Value;
                        item["key"] = itemVariable.KeyObject;
                        item["keyTo"] = itemVariable.KeyObjectTo;

                        if (variable.Type == typeof(DateTime) || variable.Type == typeof(DateTime?) || variable.Type == typeof(DateTimeRange) || variable.Type == typeof(DateTimeList))
                        {
                            if (itemVariable.KeyObject != null) item["key"] = GetDateTimeObject(itemVariable.KeyObject);
                            if (itemVariable.KeyObjectTo != null) item["keyTo"] = GetDateTimeObject(itemVariable.KeyObjectTo);
                        }

                        if (string.IsNullOrEmpty(valueBinding) || item["key"] == null || bindingValues[item["key"]] == null)
                        {
                            items.Add(item);
                        }

                        if (!string.IsNullOrEmpty(valueBinding))
                        {
                            bindingValues[item["key"]] = true;
                        }
                    }

                    index++;
                }
            }

            if (variable.DialogInfo.ItemsInitializationType == StiItemsInitializationType.Columns)
            {
                items.Sort(new StiVariableItemsComparer());
            }

            return index > 0 ? items : null;
        }

        private static Hashtable GetDateTimeObject(object value)
        {
            if (value is Hashtable) return (Hashtable)value;
            
            var dateValue = DateTime.Now;
            if (value != null && value is DateTime) dateValue = (DateTime)value;

            var dateTime = new Hashtable();
            dateTime["year"] = dateValue.Year;
            dateTime["month"] = dateValue.Month;
            dateTime["day"] = dateValue.Day;
            dateTime["hours"] = dateValue.Hour;
            dateTime["minutes"] = dateValue.Minute;
            dateTime["seconds"] = dateValue.Second;
            if (value == null) dateTime["isNull"] = true;

            return dateTime;
        }

        private static string GetBasicType(StiVariable variable)
        {
            if (StiTypeFinder.FindType(variable.Type, typeof(Range))) return "Range";
            if (StiTypeFinder.FindInterface(variable.Type, typeof(IStiList))) return "List";
            if (variable.Type.Name.StartsWith("Nullable")) return "NullableValue";
            return "Value";
        }

        private static string GetType(StiVariable variable)
        {
            if (variable.Type == typeof(string) || variable.Type == typeof(StringList) || variable.Type == typeof(StringRange)) return "String";
            if (variable.Type == typeof(char) || variable.Type == typeof(char?) || variable.Type == typeof(CharRange) || variable.Type == typeof(CharList)) return "Char";
            if (variable.Type == typeof(bool) || variable.Type == typeof(bool?) || variable.Type == typeof(BoolList)) return "Bool";
            if (variable.Type == typeof(DateTime) || variable.Type == typeof(DateTime?) || variable.Type == typeof(DateTimeList) || variable.Type == typeof(DateTimeRange)) return "DateTime";
            if (variable.Type == typeof(TimeSpan) || variable.Type == typeof(TimeSpan?) || variable.Type == typeof(TimeSpanList) || variable.Type == typeof(TimeSpanRange)) return "TimeSpan";
            if (variable.Type == typeof(Guid) || variable.Type == typeof(Guid?) || variable.Type == typeof(GuidList) || variable.Type == typeof(GuidRange)) return "Guid";
            if (variable.Type == typeof(Image) || variable.Type == typeof(Bitmap)) return "Image";
            if (variable.Type == typeof(float) || variable.Type == typeof(float?) || variable.Type == typeof(FloatList) || variable.Type == typeof(FloatRange)) return "Float";
            if (variable.Type == typeof(double) || variable.Type == typeof(double?) || variable.Type == typeof(DoubleList) || variable.Type == typeof(DoubleRange)) return "Double";
            if (variable.Type == typeof(Decimal) || variable.Type == typeof(Decimal?) || variable.Type == typeof(DecimalList) || variable.Type == typeof(DecimalRange)) return "Decimal";
            if (variable.Type == typeof(int) || variable.Type == typeof(int?) || variable.Type == typeof(IntList) || variable.Type == typeof(IntRange)) return "Int";
            if (variable.Type == typeof(uint) || variable.Type == typeof(uint?)) return "Uint";
            if (variable.Type == typeof(short) || variable.Type == typeof(short?) || variable.Type == typeof(ShortList) || variable.Type == typeof(ShortRange)) return "Short";
            if (variable.Type == typeof(ushort) || variable.Type == typeof(ushort?)) return "Ushort";
            if (variable.Type == typeof(long) || variable.Type == typeof(long?) || variable.Type == typeof(LongList) || variable.Type == typeof(LongRange)) return "Long";
            if (variable.Type == typeof(ulong) || variable.Type == typeof(ulong?)) return "Ulong";
            if (variable.Type == typeof(byte) || variable.Type == typeof(byte?) || variable.Type == typeof(ByteList) || variable.Type == typeof(ByteRange)) return "Byte";
            if (variable.Type == typeof(sbyte) || variable.Type == typeof(sbyte?)) return "Sbyte";

            return string.Empty;
        }

        /// <summary>
        /// Assigning sent by the client parameters to the report
        /// </summary>
        public static void ApplyReportParameters(StiReport report, Hashtable values)
        {
            if (values != null && values.Count > 0)
            {
                if (report.CalculationMode == StiCalculationMode.Interpretation)
                {
                    foreach (StiVariable variable in report.Dictionary.Variables)
                    {
                        report[variable.Name] = variable.ValueObject;
                    }
                }

                if (values != null)
                {
                    foreach (string key in values.Keys)
                    {
                        var variable = report.Dictionary.Variables[key];
                        if (variable != null) SetVariableValue(report, key, values[key], variable);
                    }
                }

                report.IsRendered = false;
            }
        }

        /// <summary>
        /// Assigning sent by the client parameters to the report
        /// </summary>
        public static void ApplyReportBindingVariables(StiReport report, Hashtable values)
        {
            if (values != null)
            {
                foreach (string key in values.Keys)
                {
                    foreach (StiVariable variable in report.Dictionary.Variables)
                    {
                        if (variable.Name == key)
                            variable.Value = Convert.ToString(values[key]);

                        if (variable.DialogInfo.BindingVariable != null && variable.DialogInfo.BindingVariable.Name == key)
                            variable.DialogInfo.BindingVariable.Value = Convert.ToString(values[key]);
                    }
                }
            }
        }

        /// <summary>
        /// Assigning the specified parameter to the report
        /// </summary>
        private static void SetVariableValue(StiReport report, string paramName, object paramValue, StiVariable variable)
        {
            string decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            #region Restore values

            string stringValue = null;
            Hashtable values = null;
            ArrayList array = null;

            if (paramValue != null)
            {
                if (paramValue is Hashtable) values = (Hashtable)paramValue;
                else if (paramValue is ArrayList) array = (ArrayList)paramValue;
                else stringValue = Convert.ToString(paramValue);
            }
            #endregion

            #region System types

            if (stringValue == null &&
                (variable.Type == typeof(float?) || variable.Type == typeof(double?) || variable.Type == typeof(decimal?) || variable.Type == typeof(int?) ||
                variable.Type == typeof(uint?) || variable.Type == typeof(short?) || variable.Type == typeof(ushort?) || variable.Type == typeof(long?) ||
                variable.Type == typeof(ulong?) || variable.Type == typeof(byte?) || variable.Type == typeof(sbyte?) || variable.Type == typeof(char?) ||
                variable.Type == typeof(bool?) || variable.Type == typeof(DateTime?) || variable.Type == typeof(TimeSpan?) || variable.Type == typeof(Guid?)))
            {
                report[paramName] = null;
            }
            else if (variable.Type == typeof(string))
            {
                report[paramName] = paramValue;
            }
            else if (variable.Type == typeof(float) || variable.Type == typeof(float?))
            {
                float value = 0;
                float.TryParse(stringValue.Replace(".", ",").Replace(",", decimalSeparator), out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(double) || variable.Type == typeof(double?))
            {
                double value = 0;
                double.TryParse(stringValue.Replace(".", ",").Replace(",", decimalSeparator), out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(decimal) || variable.Type == typeof(decimal?))
            {
                decimal value = 0;
                decimal.TryParse(stringValue.Replace(".", ",").Replace(",", decimalSeparator), out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(int) || variable.Type == typeof(int?))
            {
                int value = 0;
                int.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(uint) || variable.Type == typeof(uint?))
            {
                uint value = 0;
                uint.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(short) || variable.Type == typeof(short?))
            {
                short value = 0;
                short.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(ushort) || variable.Type == typeof(ushort?))
            {
                ushort value = 0;
                ushort.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(long) || variable.Type == typeof(long?))
            {
                long value = 0;
                long.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(ulong) || variable.Type == typeof(ulong?))
            {
                ulong value = 0;
                ulong.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(byte) || variable.Type == typeof(byte?))
            {
                byte value = 0;
                byte.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(sbyte) || variable.Type == typeof(sbyte?))
            {
                sbyte value = 0;
                sbyte.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(char) || variable.Type == typeof(char?))
            {
                char value = ' ';
                char.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(bool) || variable.Type == typeof(bool?))
            {
                bool value = false;
                bool.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(DateTime) || variable.Type == typeof(DateTime?))
            {
                DateTime value = DateTime.Now;
                DateTime.TryParse(stringValue, en_us_culture, DateTimeStyles.None, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(TimeSpan) || variable.Type == typeof(TimeSpan?))
            {
                TimeSpan value = TimeSpan.Zero;
                TimeSpan.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(Guid) || variable.Type == typeof(Guid?))
            {
                Guid variableGuid;
                try
                {
                    variableGuid = new Guid(stringValue);
                }
                catch
                {
                    variableGuid = Guid.Empty;
                }
                report[paramName] = variableGuid;
            }

            #endregion

            #region Ranges

            else if (variable.Type == typeof(StringRange))
            {
                report[paramName] = new StringRange(Convert.ToString(values["from"]), Convert.ToString(values["to"]));
            }
            else if (variable.Type == typeof(FloatRange))
            {
                float valueFrom = 0;
                float valueTo = 0;
                float.TryParse(Convert.ToString(values["from"]).Replace(",", decimalSeparator), out valueFrom);
                float.TryParse(Convert.ToString(values["to"]).Replace(".", ",").Replace(",", decimalSeparator), out valueTo);
                report[paramName] = new FloatRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(DoubleRange))
            {
                double valueFrom = 0;
                double valueTo = 0;
                double.TryParse(Convert.ToString(values["from"]).Replace(".", ",").Replace(",", decimalSeparator), out valueFrom);
                double.TryParse(Convert.ToString(values["to"]).Replace(".", ",").Replace(",", decimalSeparator), out valueTo);
                report[paramName] = new DoubleRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(DecimalRange))
            {
                decimal valueFrom = 0;
                decimal valueTo = 0;
                decimal.TryParse(Convert.ToString(values["from"]).Replace(".", ",").Replace(",", decimalSeparator), out valueFrom);
                decimal.TryParse(Convert.ToString(values["to"]).Replace(".", ",").Replace(",", decimalSeparator), out valueTo);
                report[paramName] = new DecimalRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(IntRange))
            {
                int valueFrom = 0;
                int valueTo = 0;
                int.TryParse(Convert.ToString(values["from"]), out valueFrom);
                int.TryParse(Convert.ToString(values["to"]), out valueTo);
                report[paramName] = new IntRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(ShortRange))
            {
                short valueFrom = 0;
                short valueTo = 0;
                short.TryParse(Convert.ToString(values["from"]), out valueFrom);
                short.TryParse(Convert.ToString(values["to"]), out valueTo);
                report[paramName] = new ShortRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(LongRange))
            {
                long valueFrom = 0;
                long valueTo = 0;
                long.TryParse(Convert.ToString(values["from"]), out valueFrom);
                long.TryParse(Convert.ToString(values["to"]), out valueTo);
                report[paramName] = new LongRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(ByteRange))
            {
                byte valueFrom = 0;
                byte valueTo = 0;
                byte.TryParse(Convert.ToString(values["from"]), out valueFrom);
                byte.TryParse(Convert.ToString(values["to"]), out valueTo);
                report[paramName] = new ByteRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(CharRange))
            {
                char valueFrom = ' ';
                char valueTo = ' ';
                char.TryParse(Convert.ToString(values["from"]), out valueFrom);
                char.TryParse(Convert.ToString(values["to"]), out valueTo);
                report[paramName] = new CharRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(DateTimeRange))
            {
                DateTime valueFrom = DateTime.Now;
                DateTime valueTo = DateTime.Now;
                DateTime.TryParse(Convert.ToString(values["from"]), en_us_culture, DateTimeStyles.None, out valueFrom);
                DateTime.TryParse(Convert.ToString(values["to"]), en_us_culture, DateTimeStyles.None, out valueTo);
                report[paramName] = new DateTimeRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(TimeSpanRange))
            {
                TimeSpan valueFrom = TimeSpan.Zero;
                TimeSpan valueTo = TimeSpan.Zero;
                TimeSpan.TryParse(Convert.ToString(values["from"]), out valueFrom);
                TimeSpan.TryParse(Convert.ToString(values["to"]), out valueTo);
                report[paramName] = new TimeSpanRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(GuidRange))
            {
                Guid valueFrom = Guid.Empty;
                Guid valueTo = Guid.Empty;
                try
                {
                    valueFrom = new Guid(Convert.ToString(values["from"]));
                    valueTo = new Guid(Convert.ToString(values["to"]));
                }
                catch
                {
                }
                report[paramName] = new GuidRange(valueFrom, valueTo);
            }

            #endregion

            #region Lists

            else if (variable.Type == typeof(StringList))
            {
                var list = new StringList();
                foreach (object value in array) list.Add(Convert.ToString(value));
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray();
            }
            else if (variable.Type == typeof(FloatList))
            {
                var list = new FloatList();
                foreach (object value in array)
                {
                    float listValue = 0;
                    float.TryParse(Convert.ToString(value).Replace(".", ",").Replace(",", decimalSeparator), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(DoubleList))
            {
                var list = new DoubleList();
                foreach (object value in array)
                {
                    double listValue = 0;
                    double.TryParse(Convert.ToString(value).Replace(".", ",").Replace(",", decimalSeparator), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(DecimalList))
            {
                var list = new DecimalList();
                foreach (object value in array)
                {
                    decimal listValue = 0;
                    decimal.TryParse(Convert.ToString(value).Replace(".", ",").Replace(",", decimalSeparator), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(ByteList))
            {
                var list = new ByteList();
                foreach (object value in array)
                {
                    byte listValue = 0;
                    byte.TryParse(Convert.ToString(value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(ShortList))
            {
                var list = new ShortList();
                foreach (object value in array)
                {
                    short listValue = 0;
                    short.TryParse(Convert.ToString(value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(IntList))
            {
                var list = new IntList();
                foreach (object value in array)
                {
                    int listValue = 0;
                    int.TryParse(Convert.ToString(value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(LongList))
            {
                var list = new LongList();
                foreach (object value in array)
                {
                    long listValue = 0;
                    long.TryParse(Convert.ToString(value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(CharList))
            {
                var list = new CharList();
                foreach (object value in array)
                {
                    char listValue = ' ';
                    char.TryParse(Convert.ToString(value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(DateTimeList))
            {
                var list = new DateTimeList();
                foreach (object value in array)
                {
                    DateTime listValue;
                    DateTime.TryParse(Convert.ToString(value), en_us_culture, DateTimeStyles.None, out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(TimeSpanList))
            {
                var list = new TimeSpanList();
                foreach (object value in array)
                {
                    TimeSpan listValue = TimeSpan.Zero;
                    TimeSpan.TryParse(Convert.ToString(value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(BoolList))
            {
                var list = new BoolList();
                foreach (object value in array)
                {
                    bool listValue;
                    bool.TryParse(Convert.ToString(value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(GuidList))
            {
                var list = new GuidList();
                foreach (object value in array)
                {
                    Guid listValue;
                    try
                    {
                        listValue = new Guid(Convert.ToString(value));
                    }
                    catch
                    {
                        listValue = Guid.Empty;
                    }

                    list.Add(listValue);
                }

                report[paramName] = list;
            }

            #endregion
        }

        public static Hashtable GetVariables(StiReport report, Hashtable values)
        {
            FillDialogInfoItems(report);

            var variables = new Hashtable();
            var binding = new Hashtable();
            var index = 0;
            
            foreach (StiVariable variable in report.Dictionary.Variables)
            {
                if (variable.RequestFromUser)
                {
                    if (variable.DialogInfo.BindingVariable != null) binding[variable.DialogInfo.BindingVariable.Name] = true;
                    
                    var var = new Hashtable();
                    var["name"] = variable.Name;
                    var["alias"] = GetVariableAlias(variable);
                    var["basicType"] = GetBasicType(variable);
                    var["type"] = GetType(variable);
                    var["allowUserValues"] = variable.DialogInfo.AllowUserValues;
                    var["dateTimeType"] = variable.DialogInfo.DateTimeType.ToString();

                    ArrayList items = GetItems(variable);
                    var["items"] = items;

                    // The 'values' collection is empty for first variables init, and not empty for get dependent variables
                    if (values != null && values.Contains(variable.Name)) var["value"] = values[variable.Name];
                    else if (variable.Selection == StiSelectionMode.Nothing) var["value"] = string.Empty;
                    else if (variable.Selection == StiSelectionMode.First) var["value"] = (items != null && items.Count > 0) ? ((Hashtable)items[0])["key"] : string.Empty;
                    else var["value"] = (variable.InitBy == StiVariableInitBy.Value) ? variable.Value : report[variable.Name];
                    var["key"] = (variable.InitBy == StiVariableInitBy.Value) ? variable.ValueObject : report[variable.Name];
                    var["keyTo"] = string.Empty;
                    
                    // Find selected item by key
                    Hashtable selectedItem = null;
                    if (items != null && items.Count > 0)
                    {
                        if (variable.Selection == StiSelectionMode.First) selectedItem = (Hashtable)items[0];
                        else
                        {
                            selectedItem = new Hashtable();
                            selectedItem["value"] = "";
                            selectedItem["key"] = variable.Type == typeof(DateTime) ? GetDateTimeObject(DateTime.Now) : null;
                            selectedItem["keyTo"] = variable.Type == typeof(DateTime) ? GetDateTimeObject(DateTime.Now) : null;
                        }
                        
                        string stringValue = Convert.ToString(var["value"]);
                        foreach (Hashtable item in items)
                        {
                            if (Convert.ToString(item["key"]) == stringValue)
                            {
                                selectedItem = item;
                                break;
                            }
                        }
                    }

                    // Value
                    if ((string)var["basicType"] == "Value" || (string)var["basicType"] == "NullableValue")
                    {
                        if (selectedItem != null)
                        {
                            var["key"] = selectedItem["key"];
                            var["value"] = selectedItem["value"];
                            if (variable.DialogInfo.AllowUserValues || var["value"] == null || (var["value"] is string && (string)var["value"] == "")) var["value"] = var["key"];

                            // Update binding variables for selected value
                            foreach (StiVariable bindingVariable in report.Dictionary.Variables)
                            {
                                if (bindingVariable.DialogInfo.BindingVariable != null && bindingVariable.DialogInfo.BindingVariable.Name == variable.Name)
                                {
                                    bindingVariable.DialogInfo.BindingVariable.ValueObject = var["key"];
                                }
                            }
                        }

                        if ((string)var["type"] == "DateTime") var["key"] = GetDateTimeObject(var["key"]);
                    }

                    // Range
                    if ((string)var["basicType"] == "Range")
                    {
                        if ((string)var["type"] == "DateTime")
                            var["key"] = GetDateTimeObject(variable.InitBy == StiVariableInitBy.Value ? ((Range)variable.ValueObject).FromObject : ((Range)report[variable.Name]).FromObject);
                        else
                            var["key"] = variable.InitBy == StiVariableInitBy.Value ? ((Range)variable.ValueObject).FromObject.ToString() : ((Range)report[variable.Name]).FromObject.ToString();
                        
                        if ((string)var["type"] == "DateTime")
                            var["keyTo"] = GetDateTimeObject(variable.InitBy == StiVariableInitBy.Value ? ((Range)variable.ValueObject).ToObject : ((Range)report[variable.Name]).ToObject);
                        else
                            var["keyTo"] = variable.InitBy == StiVariableInitBy.Value ? ((Range)variable.ValueObject).ToObject.ToString() : ((Range)report[variable.Name]).ToObject.ToString();
                    }

                    variables[index.ToString()] = var;
                    index++;
                }
            }

            if (variables.Count > 0)
            {
                foreach (string name in binding.Keys)
                    foreach (Hashtable var in variables.Values)
                        if ((string)var["name"] == name) var["binding"] = true;

                return variables;
            }

            return null;
        }
    }
}
