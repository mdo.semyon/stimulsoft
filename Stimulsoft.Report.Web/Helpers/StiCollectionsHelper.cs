﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Collections;
using Stimulsoft.Base.Localization;
using System.Text;
using System.IO;
using System.Collections.Concurrent;
using Stimulsoft.Base;
using System;
using System.Web;

#if NETCORE
using Stimulsoft.System.Web;
#endif

namespace Stimulsoft.Report.Web
{
    internal class StiCollectionsHelper
    {
        private static Hashtable Item(string value, string key)
        {
            var item = new Hashtable();
            item["key"] = key;
            item["value"] = value;

            return item;
        }

        public static ArrayList GetEncodingDataItems()
        {
            var items = new ArrayList();
            items.Add(Item(Encoding.Default.EncodingName, Encoding.Default.CodePage.ToString()));
            items.Add(Item(Encoding.ASCII.EncodingName, Encoding.ASCII.CodePage.ToString()));
            items.Add(Item(Encoding.BigEndianUnicode.EncodingName, Encoding.BigEndianUnicode.CodePage.ToString()));
            items.Add(Item(Encoding.Unicode.EncodingName, Encoding.Unicode.CodePage.ToString()));
            items.Add(Item(Encoding.UTF7.EncodingName, Encoding.UTF7.CodePage.ToString()));
            items.Add(Item(Encoding.UTF8.EncodingName, Encoding.UTF8.CodePage.ToString()));
            items.Add(Item(Encoding.GetEncoding(1250).EncodingName, "1250"));
            items.Add(Item(Encoding.GetEncoding(1251).EncodingName, "1251"));
            items.Add(Item(Encoding.GetEncoding(1252).EncodingName, "1252"));
            items.Add(Item(Encoding.GetEncoding(1253).EncodingName, "1253"));
            items.Add(Item(Encoding.GetEncoding(1254).EncodingName, "1254"));
            items.Add(Item(Encoding.GetEncoding(1255).EncodingName, "1255"));
            items.Add(Item(Encoding.GetEncoding(1256).EncodingName, "1256"));

            return items;
        }

        public static void LoadLocalizationFile(HttpContext httpContext, string directoryPath, string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                try
                {
                    if (filePath.ToLower() == "default")
                    {
                        StiLocalization.LoadDefaultLocalization();
                    }
                    else
                    {
                        if (!filePath.ToLower().EndsWith(".xml")) filePath += ".xml";
                        if (string.IsNullOrEmpty(directoryPath)) directoryPath = "Localization";

                        string absolutePath = filePath;
                        if (!File.Exists(absolutePath)) absolutePath = httpContext.Server.MapPath(filePath);
                        if (!File.Exists(absolutePath)) absolutePath = httpContext.Server.MapPath(Path.Combine(directoryPath, filePath));
                        if (!File.Exists(absolutePath)) absolutePath = httpContext.Server.MapPath("/" + filePath);
                        if (!File.Exists(absolutePath)) absolutePath = httpContext.Server.MapPath("/" + Path.Combine(directoryPath, filePath));
#if NETCORE
                        if (!File.Exists(absolutePath)) absolutePath = httpContext.Server.MapRootPath(filePath);
                        if (!File.Exists(absolutePath)) absolutePath = httpContext.Server.MapRootPath(Path.Combine(directoryPath, filePath));
#endif

                        if (File.Exists(absolutePath)) StiOptions.Localization.Load(absolutePath);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static ArrayList GetLocalizationsList(HttpContext httpContext, string directoryPath)
        {
            var list = new ArrayList();
            if (string.IsNullOrEmpty(directoryPath)) return list;

            string absolutePath = directoryPath;
            if (!Directory.Exists(absolutePath)) absolutePath = httpContext.Server.MapPath(directoryPath);
            if (!Directory.Exists(absolutePath)) absolutePath = httpContext.Server.MapPath("/" + directoryPath);
#if NETCORE
            if (!Directory.Exists(absolutePath)) absolutePath = httpContext.Server.MapRootPath(directoryPath);
#endif

            #region Get the localization files list

            if (Directory.Exists(absolutePath))
            {
                string language;
                string description;
                string cultureName;

                var dir = new DirectoryInfo(absolutePath);
                var files = dir.GetFiles();
                foreach (var file in files)
                {
                    if (file.FullName.EndsWith(".xml") && !file.FullName.EndsWith(".ext.xml"))
                    {
                        try
                        {
                            StiLocalization.GetParam(file.FullName, out language, out description, out cultureName);
                            if (!string.IsNullOrEmpty(language))
                            {
                                var loc = new Hashtable();
                                loc["FileName"] = file.Name;
                                loc["Language"] = language;
                                loc["Description"] = description;
                                loc["CultureName"] = cultureName;
                                list.Add(loc);
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }

            #endregion

            return list;
        }

        private static ConcurrentDictionary<string, Hashtable> LocalizationItems = new ConcurrentDictionary<string, Hashtable>();

        public static Hashtable GetLocalizationItems(StiRequestParams requestParams)
        {
            Hashtable words;
            string path = requestParams.Localization;
            string hash = StiMD5Helper.ComputeHash(path);
            LocalizationItems.TryGetValue(hash, out words);
            if (words != null) return words;

            LoadLocalizationFile(requestParams.HttpContext, string.Empty, path);

            words = new Hashtable();
            words["EditorToolTip"] = StiLocalization.Get("FormViewer", "Editor");
            words["TellMeMore"] = StiLocalization.Get("HelpDesigner", "TellMeMore");
            words["Print"] = StiLocalization.Get("A_WebViewer", "PrintReport");
            words["PrintToolTip"] = StiLocalization.Get("HelpViewer", "Print");
            words["Save"] = StiLocalization.Get("A_WebViewer", "SaveReport");
            words["SaveToolTip"] = StiLocalization.Get("HelpViewer", "Save");
            words["Open"] = StiLocalization.Get("Buttons", "Open");
            words["OpenToolTip"] = StiLocalization.Get("HelpViewer", "Open");
            words["SendEmail"] = StiLocalization.Get("FormViewer", "SendEMail").Replace("...", string.Empty);
            words["SendEmailToolTip"] = StiLocalization.Get("HelpViewer", "SendEMail");
            words["BookmarksToolTip"] = StiLocalization.Get("HelpViewer", "Bookmarks");
            words["ParametersToolTip"] = StiLocalization.Get("HelpViewer", "Parameters");
            words["FindToolTip"] = StiLocalization.Get("HelpViewer", "Find");
            words["FirstPageToolTip"] = StiLocalization.Get("HelpViewer", "PageFirst");
            words["PrevPageToolTip"] = StiLocalization.Get("HelpViewer", "PagePrevious");
            words["NextPageToolTip"] = StiLocalization.Get("HelpViewer", "PageNext");
            words["LastPageToolTip"] = StiLocalization.Get("HelpViewer", "PageLast");
            words["FullScreenToolTip"] = StiLocalization.Get("HelpViewer", "FullScreen");
            words["ZoomToolTip"] = StiLocalization.Get("FormViewer", "Zoom");
            words["Loading"] = StiLocalization.Get("A_WebViewer", "Loading").Replace("...", "");
            words["Bookmarks"] = StiLocalization.Get("FormViewer", "Bookmarks");
            words["Parameters"] = StiLocalization.Get("FormViewer", "Parameters");
            words["Time"] = StiLocalization.Get("FormFormatEditor", "Time");
            words["Version"] = StiLocalization.Get("PropertyMain", "Version");
            words["Maximum"] = StiLocalization.Get("PropertyMain", "Maximum");

            words["FindWhat"] = StiLocalization.Get("FormViewerFind", "FindWhat");
            words["FindPrevious"] = StiLocalization.Get("FormViewerFind", "FindPrevious");
            words["FindNext"] = StiLocalization.Get("FormViewerFind", "FindNext");
            words["MatchCase"] = StiLocalization.Get("Editor", "MatchCase");
            words["MatchWholeWord"] = StiLocalization.Get("Editor", "MatchWholeWord");
                        
            words["EmailOptions"] = StiLocalization.Get("A_WebViewer", "EmailOptions");
            words["Email"] = StiLocalization.Get("A_WebViewer", "Email");
            words["Subject"] = StiLocalization.Get("A_WebViewer", "Subject");
            words["Message"] = StiLocalization.Get("A_WebViewer", "Message");
            words["Attachment"] = StiLocalization.Get("A_WebViewer", "Attachment");

            words["SinglePage"] = StiLocalization.Get("FormViewer", "PageViewModeSinglePage");
            words["Continuous"] = StiLocalization.Get("FormViewer", "PageViewModeContinuous");
            words["MultiplePages"] = StiLocalization.Get("FormViewer", "PageViewModeMultiplePages");
            
            words["ViewModeToolTip"] = StiLocalization.Get("FormViewer", "ViewMode");
            words["Design"] = StiLocalization.Get("Buttons", "Design");
            words["Page"] = StiLocalization.Get("A_WebViewer", "Page");
            words["PageOf"] = StiLocalization.Get("A_WebViewer", "PageOf");

            words["SaveDocument"] = StiLocalization.Get("FormViewer", "DocumentFile");
            words["SavePdf"] = StiLocalization.Get("Export", "ExportTypePdfFile");
            words["SaveXps"] = StiLocalization.Get("Export", "ExportTypeXpsFile");
            words["SavePpt2007"] = StiLocalization.Get("Export", "ExportTypePpt2007File");
            words["SaveHtml"] = StiLocalization.Get("Export", "ExportTypeHtmlFile");
            words["SaveText"] = StiLocalization.Get("Export", "ExportTypeTxtFile");
            words["SaveRtf"] = StiLocalization.Get("Export", "ExportTypeRtfFile");
            words["SaveWord2007"] = StiLocalization.Get("Export", "ExportTypeWord2007File");
            words["SaveOdt"] = StiLocalization.Get("Export", "ExportTypeWriterFile");
            words["SaveExcel"] = StiLocalization.Get("Export", "ExportTypeExcelFile");
            words["SaveOds"] = StiLocalization.Get("Export", "ExportTypeCalcFile");
            words["SaveData"] = StiLocalization.Get("Export", "ExportTypeDataFile");
            words["SaveImage"] = StiLocalization.Get("Export", "ExportTypeImageFile");

            words["PrintPdf"] = StiLocalization.Get("A_WebViewer", "PrintToPdf");
            words["PrintWithPreview"] = StiLocalization.Get("A_WebViewer", "PrintWithPreview");
            words["PrintWithoutPreview"] = StiLocalization.Get("A_WebViewer", "PrintWithoutPreview");

            words["ZoomOnePage"] = StiLocalization.Get("Zoom", "PageHeight");
            words["ZoomPageWidth"] = StiLocalization.Get("FormViewer", "ZoomPageWidth");

            words["RemoveAll"] = StiLocalization.Get("Buttons", "RemoveAll");
            words["NewItem"] = StiLocalization.Get("FormDictionaryDesigner", "NewItem");
            words["Close"] = StiLocalization.Get("Buttons", "Close");

            words["Reset"] = StiLocalization.Get("Gui", "cust_pm_reset");
            words["Submit"] = StiLocalization.Get("Buttons", "Submit");

            words["RangeFrom"] = StiLocalization.Get("PropertyMain", "RangeFrom");
            words["RangeTo"] = StiLocalization.Get("PropertyMain", "RangeTo");

            words["ExportFormTitle"] = StiLocalization.Get("Export", "title");
            words["ButtonOk"] = StiLocalization.Get("Gui", "barname_ok");
            words["ButtonCancel"] = StiLocalization.Get("Gui", "barname_cancel");

            words["PagesRange"] = StiLocalization.Get("Report", "RangePage");
            words["PagesRangeAll"] = StiLocalization.Get("Report", "RangeAll");
            words["PagesRangeCurrentPage"] = StiLocalization.Get("Report", "RangeCurrentPage");
            words["PagesRangePages"] = StiLocalization.Get("Report", "RangePages");
            words["PagesRangeAllTooltip"] = StiLocalization.Get("HelpViewer", "PageAll");
            words["PagesRangeCurrentPageTooltip"] = StiLocalization.Get("HelpViewer", "CurrentPage");
            words["PagesRangePagesTooltip"] = StiLocalization.Get("HelpViewer", "RangePages");
            words["SettingsGroup"] = StiLocalization.Get("Export", "Settings");
            words["Type"] = StiLocalization.Get("PropertyMain", "Type") + ":";
            words["TypeTooltip"] = StiLocalization.Get("HelpViewer", "TypeExport");
            words["ZoomHtml"] = StiLocalization.Get("Export", "Scale");
            words["ZoomHtmlTooltip"] = StiLocalization.Get("HelpViewer", "ScaleHtml");
            words["ImageFormatForHtml"] = StiLocalization.Get("Export", "ImageFormat");
            words["ImageFormatForHtmlTooltip"] = StiLocalization.Get("HelpViewer", "ImageFormat");
            words["SavingReport"] = StiLocalization.Get("DesignerFx", "SavingReport");
            words["EmailSuccessfullySent"] = StiLocalization.Get("DesignerFx", "EmailSuccessfullySent");
            words["SaveReportMdc"] = StiLocalization.Get("FormViewer", "DocumentFile").Replace("...", "") + " (.mdc)";
            words["SaveReportMdz"] = StiLocalization.Get("FormViewer", "CompressedDocumentFile") + " (.mdz)";
            words["SaveReportMdx"] = StiLocalization.Get("FormViewer", "EncryptedDocumentFile") + " (.mdx)";
            words["PasswordEnter"] = StiLocalization.Get("Password", "lbPasswordLoad");
            words["PasswordSaveReport"] = StiLocalization.Get("Report", "LabelPassword");
            words["PasswordSaveReportTooltip"] = StiLocalization.Get("HelpViewer", "UserPassword");
            words["ExportMode"] = StiLocalization.Get("Export", "ExportMode");
            words["ExportModeTooltip"] = StiLocalization.Get("HelpViewer", "ExportMode");
            words["CompressToArchive"] = StiLocalization.Get("Export", "CompressToArchive");
            words["CompressToArchiveTooltip"] = StiLocalization.Get("HelpViewer", "CompressToArchive");
            words["EmbeddedImageData"] = StiLocalization.Get("Export", "EmbeddedImageData");
            words["EmbeddedImageDataTooltip"] = StiLocalization.Get("HelpViewer", "EmbeddedImageData");
            words["AddPageBreaks"] = StiLocalization.Get("Export", "AddPageBreaks");
            words["AddPageBreaksTooltip"] = StiLocalization.Get("HelpViewer", "AddPageBreaks");
            words["ImageResolution"] = StiLocalization.Get("Export", "ImageResolution");
            words["ImageResolutionTooltip"] = StiLocalization.Get("HelpViewer", "ImageResolution");
            words["ImageCompressionMethod"] = StiLocalization.Get("Export", "ImageCompressionMethod");
            words["ImageCompressionMethodTooltip"] = StiLocalization.Get("HelpViewer", "ImageCompressionMethod");
            words["ImageQuality"] = StiLocalization.Get("Export", "ImageQuality");
            words["ImageQualityTooltip"] = StiLocalization.Get("HelpViewer", "ImageQuality");
            words["ContinuousPages"] = StiLocalization.Get("Export", "ContinuousPages");
            words["ContinuousPagesTooltip"] = StiLocalization.Get("HelpViewer", "ContinuousPages");
            words["StandardPDFFonts"] = StiLocalization.Get("Export", "StandardPDFFonts");
            words["StandardPDFFontsTooltip"] = StiLocalization.Get("HelpViewer", "StandardPdfFonts");
            words["EmbeddedFonts"] = StiLocalization.Get("Export", "EmbeddedFonts");
            words["EmbeddedFontsTooltip"] = StiLocalization.Get("HelpViewer", "EmbeddedFonts");
            words["UseUnicode"] = StiLocalization.Get("Export", "UseUnicode");
            words["UseUnicodeTooltip"] = StiLocalization.Get("HelpViewer", "UseUnicode");
            words["Compressed"] = StiLocalization.Get("Export", "Compressed");
            words["CompressedTooltip"] = StiLocalization.Get("HelpViewer", "Compressed");
            words["ExportRtfTextAsImage"] = StiLocalization.Get("Export", "ExportRtfTextAsImage");
            words["ExportRtfTextAsImageTooltip"] = StiLocalization.Get("HelpViewer", "ExportRtfTextAsImage");
            words["PdfACompliance"] = StiLocalization.Get("Export", "PdfACompliance");
            words["PdfAComplianceTooltip"] = StiLocalization.Get("HelpViewer", "PdfACompliance");
            words["KillSpaceLines"] = StiLocalization.Get("Export", "TxtKillSpaceLines");
            words["KillSpaceLinesTooltip"] = StiLocalization.Get("HelpViewer", "KillSpaceLines");
            words["PutFeedPageCode"] = StiLocalization.Get("Export", "TxtPutFeedPageCode");
            words["PutFeedPageCodeTooltip"] = StiLocalization.Get("HelpViewer", "PutFeedPageCode");
            words["DrawBorder"] = StiLocalization.Get("Export", "TxtDrawBorder");
            words["DrawBorderTooltip"] = StiLocalization.Get("HelpViewer", "DrawBorder");
            words["CutLongLines"] = StiLocalization.Get("Export", "TxtCutLongLines");
            words["CutLongLinesTooltip"] = StiLocalization.Get("HelpViewer", "CutLongLines");
            words["BorderType"] = StiLocalization.Get("Export", "TxtBorderType");
            words["BorderTypeTooltip"] = StiLocalization.Get("HelpViewer", "BorderType");
            words["BorderTypeSimple"] = StiLocalization.Get("Export", "TxtBorderTypeSimple");
            words["BorderTypeSingle"] = StiLocalization.Get("Export", "TxtBorderTypeSingle");
            words["BorderTypeDouble"] = StiLocalization.Get("Export", "TxtBorderTypeDouble");
            words["ZoomXY"] = StiLocalization.Get("Export", "Zoom");
            words["ZoomXYTooltip"] = StiLocalization.Get("HelpViewer", "ZoomTxt");
            words["EncodingData"] = StiLocalization.Get("Export", "Encoding");
            words["EncodingDataTooltip"] = StiLocalization.Get("HelpViewer", "EncodingData");
            words["ImageFormat"] = StiLocalization.Get("Export", "ImageType");
            words["ImageFormatTooltip"] = StiLocalization.Get("HelpViewer", "ImageType");
            words["ImageFormatColor"] = StiLocalization.Get("PropertyMain", "Color");
            words["ImageFormatGrayscale"] = StiLocalization.Get("Export", "ImageGrayscale");
            words["ImageFormatMonochrome"] = StiLocalization.Get("Export", "ImageMonochrome");
            words["MonochromeDitheringType"] = StiLocalization.Get("Export", "MonochromeDitheringType");
            words["MonochromeDitheringTypeTooltip"] = StiLocalization.Get("HelpViewer", "DitheringType");
            words["TiffCompressionScheme"] = StiLocalization.Get("Export", "TiffCompressionScheme");
            words["TiffCompressionSchemeTooltip"] = StiLocalization.Get("HelpViewer", "TiffCompressionScheme");
            words["CutEdges"] = StiLocalization.Get("Export", "ImageCutEdges");
            words["CutEdgesTooltip"] = StiLocalization.Get("HelpViewer", "CutEdges");
            words["MultipleFiles"] = StiLocalization.Get("Export", "MultipleFiles");
            words["MultipleFilesTooltip"] = StiLocalization.Get("HelpViewer", "MultipleFiles");
            words["ExportDataOnly"] = StiLocalization.Get("Export", "ExportDataOnly");
            words["ExportDataOnlyTooltip"] = StiLocalization.Get("HelpViewer", "ExportDataOnly");
            words["UseDefaultSystemEncoding"] = StiLocalization.Get("Export", "UseDefaultSystemEncoding");
            words["UseDefaultSystemEncodingTooltip"] = StiLocalization.Get("HelpViewer", "UseDefaultSystemEncoding");
            words["EncodingDifFile"] = StiLocalization.Get("Export", "Encoding");
            words["EncodingDifFileTooltip"] = StiLocalization.Get("HelpViewer", "EncodingData");
            words["ExportModeRtf"] = StiLocalization.Get("Export", "ExportMode");
            words["ExportModeRtfTooltip"] = StiLocalization.Get("HelpViewer", "ExportModeRtf");
            words["ExportModeRtfTable"] = StiLocalization.Get("Export", "ExportModeTable");
            words["ExportModeRtfFrame"] = StiLocalization.Get("Export", "ExportModeFrame");
            words["UsePageHeadersFooters"] = StiLocalization.Get("Export", "UsePageHeadersAndFooters");
            words["UsePageHeadersFootersTooltip"] = StiLocalization.Get("HelpViewer", "UsePageHeadersAndFooters");
            words["RemoveEmptySpace"] = StiLocalization.Get("Export", "RemoveEmptySpaceAtBottom");
            words["RemoveEmptySpaceTooltip"] = StiLocalization.Get("HelpViewer", "RemoveEmptySpaceAtBottom");
            words["Separator"] = StiLocalization.Get("Export", "Separator");
            words["SeparatorTooltip"] = StiLocalization.Get("HelpViewer", "Separator");
            words["SkipColumnHeaders"] = StiLocalization.Get("Export", "SkipColumnHeaders");
            words["SkipColumnHeadersTooltip"] = StiLocalization.Get("HelpViewer", "SkipColumnHeaders");
            words["ExportObjectFormatting"] = StiLocalization.Get("Export", "ExportObjectFormatting");
            words["ExportObjectFormattingTooltip"] = StiLocalization.Get("HelpViewer", "ExportObjectFormatting");
            words["UseOnePageHeaderFooter"] = StiLocalization.Get("Export", "UseOnePageHeaderAndFooter");
            words["UseOnePageHeaderFooterTooltip"] = StiLocalization.Get("HelpViewer", "UseOnePageHeaderAndFooter");
            words["ExportEachPageToSheet"] = StiLocalization.Get("Export", "ExportEachPageToSheet");
            words["ExportEachPageToSheetTooltip"] = StiLocalization.Get("HelpViewer", "ExportEachPageToSheet");
            words["ExportPageBreaks"] = StiLocalization.Get("Export", "ExportPageBreaks");
            words["ExportPageBreaksTooltip"] = StiLocalization.Get("HelpViewer", "ExportPageBreaks");
            words["EncodingDbfFile"] = StiLocalization.Get("Export", "Encoding");
            words["EncodingDbfFileTooltip"] = StiLocalization.Get("HelpViewer", "EncodingData");
            words["DocumentSecurityButton"] = StiLocalization.Get("Export", "DocumentSecurity");
            words["DigitalSignatureButton"] = StiLocalization.Get("Export", "DigitalSignature");
            words["OpenAfterExport"] = StiLocalization.Get("Export", "OpenAfterExport");
            words["OpenAfterExportTooltip"] = StiLocalization.Get("HelpViewer", "OpenAfterExport");
            words["AllowEditable"] = StiLocalization.Get("Export", "AllowEditable");
            words["AllowEditableTooltip"] = StiLocalization.Get("HelpViewer", "AllowEditable");
            words["NameYes"] = StiLocalization.Get("FormFormatEditor", "nameYes");            
            words["NameNo"] = StiLocalization.Get("FormFormatEditor", "nameNo");
            words["UserPassword"] = StiLocalization.Get("Export", "labelUserPassword");
            words["UserPasswordTooltip"] = StiLocalization.Get("HelpViewer", "UserPassword");
            words["OwnerPassword"] = StiLocalization.Get("Export", "labelOwnerPassword");
            words["OwnerPasswordTooltip"] = StiLocalization.Get("HelpViewer", "OwnerPassword");
            words["BandsFilter"] = StiLocalization.Get("Export", "BandsFilter");
            words["BandsFilterTooltip"] = StiLocalization.Get("HelpViewer", "ExportMode");
            words["BandsFilterAllBands"] = StiLocalization.Get("Export", "AllBands");
            words["BandsFilterDataOnly"] = StiLocalization.Get("Export", "DataOnly");
            words["BandsFilterDataAndHeadersFooters"] = StiLocalization.Get("Export", "DataAndHeadersFooters");

            words["AllowPrintDocument"] = StiLocalization.Get("Export", "AllowPrintDocument");
            words["AllowPrintDocumentTooltip"] = StiLocalization.Get("HelpViewer", "AllowPrintDocument");
            words["AllowModifyContents"] = StiLocalization.Get("Export", "AllowModifyContents");
            words["AllowModifyContentsTooltip"] = StiLocalization.Get("HelpViewer", "AllowModifyContents");
            words["AllowCopyTextAndGraphics"] = StiLocalization.Get("Export", "AllowCopyTextAndGraphics");
            words["AllowCopyTextAndGraphicsTooltip"] = StiLocalization.Get("HelpViewer", "AllowCopyTextAndGraphics");
            words["AllowAddOrModifyTextAnnotations"] = StiLocalization.Get("Export", "AllowAddOrModifyTextAnnotations");
            words["AllowAddOrModifyTextAnnotationsTooltip"] = StiLocalization.Get("HelpViewer", "AllowAddOrModifyTextAnnotations");
            words["EncryptionKeyLength"] = StiLocalization.Get("Export", "labelEncryptionKeyLength");
            words["EncryptionKeyLengthTooltip"] = StiLocalization.Get("HelpViewer", "EncryptionKeyLength");

            words["UseDigitalSignature"] = StiLocalization.Get("Export", "UseDigitalSignature");
            words["UseDigitalSignatureTooltip"] = StiLocalization.Get("HelpViewer", "DigitalSignature");
            words["GetCertificateFromCryptoUI"] = StiLocalization.Get("Export", "GetCertificateFromCryptoUI");
            words["GetCertificateFromCryptoUITooltip"] = StiLocalization.Get("HelpViewer", "GetCertificateFromCryptoUI");
            words["SubjectNameString"] = StiLocalization.Get("Export", "labelSubjectNameString");
            words["SubjectNameStringTooltip"] = StiLocalization.Get("HelpViewer", "SubjectNameString");

            words["MonthJanuary"] = StiLocalization.Get("A_WebViewer", "MonthJanuary");
            words["MonthFebruary"] = StiLocalization.Get("A_WebViewer", "MonthFebruary");
            words["MonthMarch"] = StiLocalization.Get("A_WebViewer", "MonthMarch");
            words["MonthApril"] = StiLocalization.Get("A_WebViewer", "MonthApril");
            words["MonthMay"] = StiLocalization.Get("A_WebViewer", "MonthMay");
            words["MonthJune"] = StiLocalization.Get("A_WebViewer", "MonthJune");
            words["MonthJuly"] = StiLocalization.Get("A_WebViewer", "MonthJuly");
            words["MonthAugust"] = StiLocalization.Get("A_WebViewer", "MonthAugust");
            words["MonthSeptember"] = StiLocalization.Get("A_WebViewer", "MonthSeptember");
            words["MonthOctober"] = StiLocalization.Get("A_WebViewer", "MonthOctober");
            words["MonthNovember"] = StiLocalization.Get("A_WebViewer", "MonthNovember");
            words["MonthDecember"] = StiLocalization.Get("A_WebViewer", "MonthDecember");

            words["DayMonday"] = StiLocalization.Get("A_WebViewer", "DayMonday");
            words["DayTuesday"] = StiLocalization.Get("A_WebViewer", "DayTuesday");
            words["DayWednesday"] = StiLocalization.Get("A_WebViewer", "DayWednesday");
            words["DayThursday"] = StiLocalization.Get("A_WebViewer", "DayThursday");
            words["DayFriday"] = StiLocalization.Get("A_WebViewer", "DayFriday");
            words["DaySaturday"] = StiLocalization.Get("A_WebViewer", "DaySaturday");
            words["DaySunday"] = StiLocalization.Get("A_WebViewer", "DaySunday");

            words["FormViewerTitle"] = StiLocalization.Get("FormViewer", "title");
            words["Error"] = StiLocalization.Get("Errors", "Error");
            words["SelectAll"] = StiLocalization.Get("MainMenu", "menuEditSelectAll").Replace("&", "");
            
            words["CurrentMonth"] = StiLocalization.Get("DatePickerRanges", "CurrentMonth");
            words["CurrentQuarter"] = StiLocalization.Get("DatePickerRanges", "CurrentQuarter");
            words["CurrentWeek"] = StiLocalization.Get("DatePickerRanges", "CurrentWeek");
            words["CurrentYear"] = StiLocalization.Get("DatePickerRanges", "CurrentYear");
            words["NextMonth"] = StiLocalization.Get("DatePickerRanges", "NextMonth");
            words["NextQuarter"] = StiLocalization.Get("DatePickerRanges", "NextQuarter");
            words["NextWeek"]  = StiLocalization.Get("DatePickerRanges", "NextWeek");
            words["NextYear"] = StiLocalization.Get("DatePickerRanges", "NextYear");
            words["PreviousMonth"] = StiLocalization.Get("DatePickerRanges", "PreviousMonth");
            words["PreviousQuarter"] = StiLocalization.Get("DatePickerRanges", "PreviousQuarter");
            words["PreviousWeek"] = StiLocalization.Get("DatePickerRanges", "PreviousWeek");
            words["PreviousYear"] = StiLocalization.Get("DatePickerRanges", "PreviousYear");
            words["FirstQuarter"] = StiLocalization.Get("DatePickerRanges", "FirstQuarter");
            words["SecondQuarter"] = StiLocalization.Get("DatePickerRanges", "SecondQuarter");
            words["ThirdQuarter"] = StiLocalization.Get("DatePickerRanges", "ThirdQuarter");
            words["FourthQuarter"] = StiLocalization.Get("DatePickerRanges", "FourthQuarter");
            words["MonthToDate"] = StiLocalization.Get("DatePickerRanges", "MonthToDate");
            words["QuarterToDate"] = StiLocalization.Get("DatePickerRanges", "QuarterToDate");
            words["WeekToDate"] = StiLocalization.Get("DatePickerRanges", "WeekToDate");
            words["YearToDate"] = StiLocalization.Get("DatePickerRanges", "YearToDate");
            words["Today"] = StiLocalization.Get("DatePickerRanges", "Today");
            words["Tomorrow"] = StiLocalization.Get("DatePickerRanges", "Tomorrow");
            words["Yesterday"] = StiLocalization.Get("DatePickerRanges", "Yesterday");
            words["Resources"] = StiLocalization.Get("PropertyMain", "Resources");
            words["ResourcesToolTip"] = StiLocalization.Get("PropertyMain", "Resources");
            words["SaveFile"] = StiLocalization.Get("Cloud", "SaveFile");
            words["ButtonView"] = StiLocalization.Get("Cloud", "ButtonView");

            // Cloud
            words["QuotaMaximumReportPagesCountExceeded"] = StiLocalization.Get("Notices", "QuotaMaximumReportPagesCountExceeded");
            words["QuotaMaximumDataRowsCountExceeded"] = StiLocalization.Get("Notices", "QuotaMaximumDataRowsCountExceeded");

            words["New"] = StiLocalization.Get("MainMenu", "menuFileNew").Replace("&", "");
            words["Edit"] = StiLocalization.Get("MainMenu", "menuEditEdit");

            hash = StiMD5Helper.ComputeHash(path);
            LocalizationItems.GetOrAdd(hash, words);
            return words;
        }

        public static ArrayList GetDateRangesItems()
        {
            var items = new ArrayList();
            foreach (StiDateRangeKind item in StiOptions.Viewer.RequestFromUserDateRanges)
            {
                items.Add(item.ToString());
            }

            return items;
        }
    }
}