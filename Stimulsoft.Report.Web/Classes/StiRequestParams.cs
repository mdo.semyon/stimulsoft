﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Web;

#if NETCORE
using Stimulsoft.System.Web;
using Stimulsoft.System.Web.Caching;
#else
using System.Web.Caching;
#endif

namespace Stimulsoft.Report.Web
{
    public class StiRequestParams
    {
        #region StiCacheParams

        public class StiCacheParams
        {
            internal StiCacheHelper Helper { get; set; } = null;

            public string ClientGuid { get; set; }

            public string DrillDownGuid { get; set; }

            public StiServerCacheMode Mode { get; set; } = StiServerCacheMode.ObjectCache;

            public TimeSpan Timeout { get; set; } = new TimeSpan(0, 20, 0);

            public CacheItemPriority Priority { get; set; } = CacheItemPriority.Default;
        }

        #endregion

        #region StiInteractionParams

        public class StiInteractionParams
        {
            /// <summary>
            /// The the values of the request from user report variables after Submit.
            /// </summary>
            public Hashtable Variables { get; set; } = new Hashtable();

            /// <summary>
            /// The the values of the sorting parameters after Sorting action.
            /// </summary>
            public Hashtable Sorting { get; set; } = new Hashtable();

            /// <summary>
            /// The the values of the collapsing parameters after Collapsing action.
            /// </summary>
            public Hashtable Collapsing { get; set; } = new Hashtable();

            /// <summary>
            /// The the values of the parameters for all levels for drill-down report after DrillDown action.
            /// </summary>
            public ArrayList DrillDown { get; set; } = new ArrayList();

            /// <summary>
            /// The the values of the edited report fields.
            /// </summary>
            public Hashtable Editable { get; set; } = new Hashtable();
        }

        #endregion

        #region StiViewerParams

        public class StiViewerParams
        {
            public int PageNumber { get; set; }

            public double Zoom { get; set; } = 1;

            public StiWebViewMode ViewMode { get; set; } = StiWebViewMode.SinglePage;

            public bool ShowBookmarks { get; set; } = true;

            public string OpenLinksWindow { get; set; } = StiTargetWindow.Blank;

            public StiChartRenderType ChartRenderType { get; set; } = StiChartRenderType.AnimatedVector;

            public StiReportDisplayMode ReportDisplayMode { get; set; } = StiReportDisplayMode.Table;

            public StiPrintAction PrintAction { get; set; }

            public bool BookmarksPrint { get; set; } = true;

            public string OpeningFileName { get; set; }

            public string OpeningFilePassword { get; set; }

            internal bool CloudTrialMode { get; set; }

            internal bool ReportDesignerMode { get; set; }
        }

        #endregion
        
        #region StiDesignerParams

        public class StiDesignerParams
        {
            public StiDesignerCommand Command { get; set; } = StiDesignerCommand.Undefined;

            public bool IsAutoSave { get; set; }

            public bool IsNewReport { get; set; }

            public bool IsSaveAs { get; set; }

            public string FileName { get; set; }

            public string Password { get; set; }

            public string SaveType { get; set; }

            internal int UndoMaxLevel { get; set; } = 6;

            internal bool CheckReportBeforePreview { get; set; } = true;
        }

        #endregion
        
        #region StiDictionaryParams

        public class StiDictionaryParams
        {
            public string ConnectionType { get; set; }

            public string ConnectionString { get; set; }

            public string Query { get; set; }

            public string UserName { get; set; }

            public string Password { get; set; }

            public string DataPath { get; set; }

            public string SchemaPath { get; set; }
        }

        #endregion

        #region StiServerParams

        public class StiServerParams
        {
            public bool UseRelativeUrls { get; set; } = true;

            public bool UseCompression { get; set; } = false;

            public bool PassQueryParametersForResources { get; set; } = true;

            public bool PassQueryParametersToReport { get; set; } = false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// All parameters of the Web component received from the client side.
        /// </summary>
        public Hashtable All { get; } = new Hashtable();

        private HttpContext httpContext = null;
        public HttpContext HttpContext
        {
            get
            {
                return httpContext ?? (httpContext = HttpContext.Current);
            }
            set
            {
                httpContext = value;
            }
        }

        /// <summary>
        /// Web component type.
        /// </summary>
        public StiComponentType Component { get; set; }

        /// <summary>
        /// Current action in the web component request.
        /// </summary>
        public StiAction Action { get; set; }
        
        public string Resource { get; set; }

        public byte[] Data { get; set; }

        /// <summary>
        /// Report transferred from the Flash client side.
        /// </summary>
        public StiReport Report { get; set; }
        
        /// <summary>
        /// Web component ID
        /// </summary>
        public string Id { get; set; }

        public bool HasParameters { get; set; } = false;

        public StiCacheParams Cache { get; set; } = new StiCacheParams();

        public StiInteractionParams Interaction { get; set; } = new StiInteractionParams();

        public NameValueCollection Routes { get; set; } = new NameValueCollection();

        public NameValueCollection FormValues { get; set; } = new NameValueCollection();

        public Hashtable ExportSettings { get; set; } = new Hashtable();

        public StiExportFormat ExportFormat { get; set; }

        public string Localization { get; set; } = "default";

        public string Theme { get; set; } = string.Empty;

        public string Version { get; set; } = string.Empty;

        public StiViewerParams Viewer { get; set; } = new StiViewerParams();

        public StiDesignerParams Designer { get; set; } = new StiDesignerParams();

        public StiDictionaryParams Dictionary { get; set; } = new StiDictionaryParams();

        public StiServerParams Server { get; set; } = new StiServerParams();

        internal bool CloudMode { get; set; } = false;

        public Hashtable ReportResourceParams { get; set; } = new Hashtable();

        #endregion

        #region Methods

        public bool Contains(string name)
        {
            return All.ContainsKey(name);
        }

        public string GetString(string name)
        {
            if (All == null || All[name] == null) return null;
            return All[name].ToString();
        }

        public bool GetBoolean(string name)
        {
            return Convert.ToBoolean(GetString(name));
        }

        public int GetInt(string name)
        {
            return Convert.ToInt32(GetString(name));
        }

        public Hashtable GetHashtable(string name)
        {
            if (!All.Contains(name)) return null;
            return (Hashtable)All[name];
        }

        public ArrayList GetArray(string name)
        {
            if (!All.Contains(name)) return null;
            return (ArrayList)All[name];
        }

        public object GetEnum(string name, Type type)
        {
            if (!All.ContainsKey(name) || All[name] == null) return 0;

            try
            {
                return Enum.Parse(type, GetString(name));
            }
            catch
            {
                return 0;
            }
        }

        public NameValueCollection GetNameValueCollection(string name)
        {
            if (!All.Contains(name) || All[name] == null) return null;
            var hash = (Hashtable)All[name];
            var collection = new NameValueCollection();
            foreach (var key in hash.Keys)
            {
                collection.Add(key.ToString(), hash[key] != null ? hash[key].ToString() : null);
            }

            return collection;
        }

        #endregion
    }
}
