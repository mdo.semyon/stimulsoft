﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Json;
using Stimulsoft.Base.Json.Converters;
using System.Collections;
using System.IO;
using System.Text;

namespace Stimulsoft.Report.Web
{
    public class StiWebActionResult
    {
        #region Properties
        
        public byte[] Data { get; private set; }
        
        public string ContentType { get; private set; }
        
        public string FileName { get; private set; }

        #endregion

        public static StiWebActionResult StringResult(StiRequestParams requestParams, string data)
        {
            if (requestParams.Server.UseCompression)
            {
                string base64 = StiGZipHelper.Pack(data);
                return new StiWebActionResult(base64);
            }

            return new StiWebActionResult(data);
        }

        public static StiWebActionResult JsonResult(StiRequestParams requestParams, Hashtable data)
        {
            string json = JsonConvert.SerializeObject(data, Formatting.None, new StringEnumConverter());
            return StringResult(requestParams, json);
        }

        public static StiWebActionResult ErrorResult(StiRequestParams requestParams, string data)
        {
            if (requestParams.Component == StiComponentType.Designer)
            {
                Hashtable result = new Hashtable();
                result["error"] = data;
                return JsonResult(requestParams, result);
            }

            return StringResult(requestParams, "ServerError:" + data);
        }

        public static StiWebActionResult EmptyReportResult()
        {
            return new StiWebActionResult("ServerError:The report is not specified.");
        }

        public StiWebActionResult() :
            this(string.Empty)
        {
        }

        public StiWebActionResult(string data) :
            this(data, "text/plain")
        {
        }

        public StiWebActionResult(string data, string contentType) :
            this(data, contentType, null)
        {
        }

        public StiWebActionResult(string data, string contentType, string fileName) :
            this(Encoding.UTF8.GetBytes(data), contentType, fileName)
        {
        }

        public StiWebActionResult(Stream data, string contentType) :
            this(data, contentType, null)
        {
        }

        public StiWebActionResult(Stream data, string contentType, string fileName) :
            this(((MemoryStream)data).ToArray(), contentType, fileName)
        {
        }

        public StiWebActionResult(byte[] data, string contentType) :
            this(data, contentType, null)
        {
        }

        public StiWebActionResult(byte[] data, string contentType, string fileName)
        {
            this.Data = data;
            this.ContentType = contentType;
            this.FileName = fileName;
        }
    }
}
