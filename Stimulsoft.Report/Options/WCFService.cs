#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Design;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.Viewer;
using Stimulsoft.Report.WCFService;

namespace Stimulsoft.Report
{
    /// <summary>
    /// Class for adjustment all aspects of Stimulsoft Reports.
    /// </summary>
    public sealed partial class StiOptions
    {
        public sealed class WCFService
        {
            #region Events
            #region WCFRenderReport
            public static bool WCFRenderReportEventIsUsed => UseWCFService && WCFRenderReport != null;

            public static event StiWCFEventHandlers WCFRenderReport;

            public static void InvokeWCFRenderReport(object sender, IStiDesignerBase designer, string xml)
            {
                WCFRenderReport?.Invoke(sender, new StiWCFEventArgs(xml, designer));
            }
            #endregion

            #region WCFTestConnection
            public static bool WCFTestConnectionEventIsUsed => UseWCFService && WCFTestConnection != null;

            public static event StiWCFEventHandlers WCFTestConnection;

            public static void InvokeWCFTestConnection(object sender, IStiDesignerBase designer, string xml)
            {
                WCFTestConnection?.Invoke(sender, new StiWCFEventArgs(xml, designer));
            }
            #endregion

            #region WCFBuildObjects
            public static bool WCFBuildObjectsEventIsUsed => UseWCFService && WCFBuildObjects != null;

            public static event StiWCFEventHandlers WCFBuildObjects;

            public static void InvokeWCFBuildObjects(object sender, IStiDesignerBase designer, string xml)
            {
                WCFBuildObjects?.Invoke(sender, new StiWCFEventArgs(xml, designer));
            }
            #endregion

            #region WCFRetrieveColumns
            public static bool WCFRetrieveColumnsEventIsUsed => UseWCFService && WCFRetrieveColumns != null;

            public static event StiWCFEventHandlers WCFRetrieveColumns;

            public static void InvokeWCFRetrieveColumns(object sender, IStiDesignerBase designer, string xml)
            {
                WCFRetrieveColumns?.Invoke(sender, new StiWCFEventArgs(xml, designer));
            }
            #endregion

            #region WCFReportCheck
            public static bool WCFReportCheckEventIsUsed => UseWCFService && WCFReportCheck != null;

            public static event StiWCFReportCheckEventHandlers WCFReportCheck;

            public static void InvokeWCFReportCheck(IStiCheckStatusControl sender, IStiDesignerBase designer, string xml)
            {
                WCFReportCheck?.Invoke(sender, new StiWCFReportCheckEventArgs(xml, designer));
            }
            #endregion

            #region WCFOpeningReportInDesigner
            public static bool WCFOpeningReportInDesignerEventIsUsed => UseWCFService && WCFOpeningReportInDesigner != null;

            public static event StiWCFOpeningReportEventEventHandlers WCFOpeningReportInDesigner;

            public static bool InvokeWCFOpeningReportInDesigner(IStiDesignerBase designer)
            {
                var openingEventArgs = new StiWCFOpeningReportEventArgs(designer);

                WCFOpeningReportInDesigner?.Invoke(designer, new StiWCFOpeningReportEventArgs(designer));

                return openingEventArgs.Handled;
            }
            #endregion

            #region WCFExportDocument
            public static bool WCFExportDocumentEventIsUsed => UseWCFService && WCFExportDocument != null;

            public static event StiWCFExportEventHandlers WCFExportDocument;

            public static void InvokeWCFExportDocument(StiReport report, string xml, string filter)
            {
                WCFExportDocument?.Invoke(report, new StiWCFExportEventArgs(xml, filter, report));
            }
            #endregion

            #region WCFRenderingInteractions
            public static bool WCFRenderingInteractionsEventIsUsed => UseWCFService && WCFRenderingInteractions != null;

            public static event StiWCFRenderingInteractionsEventHandlers WCFRenderingInteractions;
            public static void InvokeWCFRenderingInteractions(IStiViewerControl viewer, string xml, StiInteractionType interactionType)
            {
                WCFRenderingInteractions?.Invoke(viewer, new StiWCFRenderingInteractionsEventArgs(xml, interactionType, viewer));
            }
            #endregion

            #region WCFRequestFromUserRenderReport
            public static bool WCFRequestFromUserRenderReportEventIsUsed => UseWCFService && WCFRequestFromUserRenderReport != null;

            public static event StiWCFEventHandlers WCFRequestFromUserRenderReport;

            public static void InvokeWCFRequestFromUserRenderReport(IStiViewerControl viewer, string xml)
            {
                WCFRequestFromUserRenderReport?.Invoke(viewer, new StiWCFEventArgs(xml, viewer));
            }
            #endregion

            #region WCFPrepareRequestFromUserVariables
            public static bool WCFPrepareRequestFromUserVariablesEventIsUsed => UseWCFService && WCFPrepareRequestFromUserVariables != null;

            public static event StiWCFEventHandlers WCFPrepareRequestFromUserVariables;

            public static void InvokeWCFPrepareRequestFromUserVariables(IStiViewerControl viewer, string xml)
            {
                WCFPrepareRequestFromUserVariables?.Invoke(viewer, new StiWCFEventArgs(xml, viewer));
            }
            #endregion

            #region WCFDataBandSelected
            public static bool WCFInteractiveDataBandSelectionEventIsUsed => UseWCFService && WCFInteractiveDataBandSelection != null;

            public static event StiWCFEventHandlers WCFInteractiveDataBandSelection;

            public static void InvokeWCFInteractiveDataBandSelection(IStiViewerControl viewer, string xml)
            {
                WCFInteractiveDataBandSelection?.Invoke(viewer, new StiWCFEventArgs(xml, viewer));
            }
            #endregion

            #region WCFFindDatabas eType
            public static event StiWCFFindDatabaseTypeEventHandlers WCFFindDatabaseType;
            public static string InvokeWCFFindDatabaseType(IStiDesignerBase designer, string databaseType)
            {
                if (WCFFindDatabaseType == null) return null;

                var args = new StiWCFFindDatabaseTypeEventArgs(databaseType, designer);
                WCFFindDatabaseType(designer, args);

                return args.UserDatabaseType;

            }
            #endregion
            #endregion

            #region Properties
            [DefaultValue(false)]
            [StiSerializable]
            public static bool UseWCFService { get; set; }
            #endregion
        }
    }
}
