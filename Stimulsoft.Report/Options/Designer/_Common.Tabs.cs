#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Export;

namespace Stimulsoft.Report
{
    /// <summary>
    /// Class for adjustment all aspects of Stimulsoft Reports.
    /// </summary>
    public sealed partial class StiOptions
	{
        /// <summary>
        /// Class which allows adjustment of the Designer of the report.
        /// </summary>
        public sealed partial class Designer
        {
            /// <summary>
            /// Gets or sets a value indicating to show or to hide Code Tab of the report designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value indicating to show or to hide Code Tab of the report designer.")]
            [StiSerializable]
            public static bool CodeTabVisible { get; set; } = true;

            /// <summary>
            /// Gets or sets a value, which shows or hides a Preview Tab in the report designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value, which shows or hides a Preview Tab in the report designer.")]
            [StiSerializable]
            public static bool PreviewReportVisible { get; set; } = true;

            /// <summary>
            /// Gets or sets a value, which shows or hides a WPF Preview Tab in the report designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value, which shows or hides a WPF Preview Tab in the report designer.")]
            [StiSerializable]
            public static bool WpfPreviewReportVisible { get; set; } = true;

            /// <summary>
            /// Gets or sets a value, which shows or hides a Web Preview Tab in the report designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value, which shows or hides a Web Preview Tab in the report designer.")]
            [StiSerializable]
            public static bool WebPreviewReportVisible { get; set; } = true;

            /// <summary>
            /// Gets or sets a value, which shows or hides a Silverlight Preview Tab in the report designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value, which shows or hides a Silverlight Preview Tab in the report designer.")]
            [StiSerializable]
            public static bool SLPreviewReportVisible { get; set; } = true;

            /// <summary>
            /// Gets or sets a value, which shows or hides a WinRT Preview Tab in the report designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value, which shows or hides a WinRT Preview Tab in the report designer.")]
            [StiSerializable]
            public static bool WinRTPreviewReportVisible { get; set; } = true;

            /// <summary>
            /// Gets or sets a value which controls a visibility of the HTML preview tab of the report Designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value which controls a visibility of the HTML preview tab of the report Designer.")]
            [StiSerializable]
            public static bool HtmlPreviewReportVisible { get; set; } = true;

            /// <summary>
            /// Gets or sets a value which controls a visibility of the JS preview tab of the report Designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value which controls a visibility of the JS preview tab of the report Designer.")]
            [StiSerializable]
            public static bool JsPreviewReportVisible { get; set; } = true;

            /// <summary>
            /// Gets or sets a value, which enables or disables using of the Web Preview in the report designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value, which enables or disables using of the Web Preview in the report designer.")]
            [StiSerializable]
            public static bool WebPreviewReportEnabled { get; set; } = true;

            /// <summary>
            /// Gets or sets a value, which enables or disables using of the HTML Preview in the report designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value, which enables or disables using of the HTML Preview in the report designer.")]
            [StiSerializable]
            public static bool HtmlPreviewReportEnabled { get; set; } = true;

            /// <summary>
            /// Gets or sets a value, which enables or disables using of the Silverlight Preview in the report designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value, which enables or disables using of the Silverlight Preview in the report designer.")]
            [StiSerializable]
            public static bool SLPreviewReportEnabled { get; set; } = true;

            /// <summary>
            /// Gets or sets a value, which enables or disables using of the Silverlight Preview in the report designer.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value, which enables or disables using of the WinRT Preview in the report designer.")]
            [StiSerializable]
            public static bool WinRTPreviewReportEnabled { get; set; } = true;

            [DefaultValue(StiHtmlExportMode.Table)]
            [StiSerializable]
            public static StiHtmlExportMode PreviewHtmlExportMode { get; set; } = StiHtmlExportMode.Table;

            /// <summary>
            /// Gets or sets a value indicating whether the Preview Window of the report will be showing as MDI window.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value indicating whether the Preview Window of the report will be showing as MDI window.")]
            [StiSerializable]
            public static bool ShowPreviewInMdi { get; set; } = true;
        }
    }
}