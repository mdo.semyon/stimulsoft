#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.QuickButtons;

namespace Stimulsoft.Report
{
    /// <summary>
    /// Class for adjustment all aspects of Stimulsoft Reports.
    /// </summary>
    public sealed partial class StiOptions
	{
        /// <summary>
        /// Class which allows adjustment of the Designer of the report.
        /// </summary>
        public sealed partial class Designer
        {
            #region Methods
            public static event StiGetCulturesEventHandler GetCulturesListForGlobalizationEditor;
            public static CultureInfo[] InvokeGetCulturesListForGlobalizationEditor()
            {
                if (GetCulturesListForGlobalizationEditor == null) return null;

                var args = new StiGetCulturesEventArgs();
                GetCulturesListForGlobalizationEditor(null, args);
                return args.Cultures;
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets or sets a value which controls of output of objects in the right to left mode.
            /// </summary>
            [DefaultValue(StiRightToLeftType.No)]
            [Description("Gets or sets a value which controls of output of objects in the right to left mode.")]
            [StiSerializable]
            public static StiRightToLeftType RightToLeft { get; set; } = StiRightToLeftType.No;

            /// <summary>
            /// Internal use only.
            /// </summary>
            public static bool IsRightToLeft => RightToLeft == StiRightToLeftType.Yes;

            /// <summary>
            /// Gets or sets value which indicates that font names will be drawing in simple view mode in font combobox in report designer.
            /// </summary>
            [Description("Gets or sets value which indicates that font names will be drawing in simple view mode in font combobox in report designer.")]
            [DefaultValue(true)]
            [StiSerializable]
            public static bool AllowCustomFontDrawing { get; set; } = true;

            [DefaultValue(true)]
            [StiSerializable]
            public static bool AllowUseDragDrop { get; set; } = true;

            [DefaultValue(true)]
            [StiSerializable]
            public static bool AllowCopyControlOperation { get; set; } = true;

            [DefaultValue(true)]
            [StiSerializable]
            public static bool ShowWatermarkInPageSetup { get; set; } = true;

            [DefaultValue(true)]
            [StiSerializable]
            public static bool IsComponentEditorEnabled { get; set; } = true;

            [DefaultValue(true)]
            [StiSerializable]
            public static bool IsHighlightComponentServiceEnabled { get; set; } = true;

            private static Font defaultPropertyGridFont;
            [StiOptionsFontHelper(2)]
            [StiSerializable]
            public static Font DefaultPropertyGridFont
            {
                get
                {
                    return defaultPropertyGridFont ?? (defaultPropertyGridFont = new Font("Arial", 8));
                }
                set
                {
                    defaultPropertyGridFont = value;
                }
            }

            [DefaultValue(true)]
            [StiSerializable]
            public static bool ShowSuperToolTip { get; set; } = true;

            [DefaultValue(StiQuickButtonVisibility.Always)]
            [StiSerializable]
            public static StiQuickButtonVisibility ShowQuickButtons { get; set; } = StiQuickButtonVisibility.Always;

            [DefaultValue(true)]
            [StiSerializable]
            public static bool CanCustomizeRibbon { get; set; } = true;

            [DefaultValue(false)]
            [StiSerializable]
            public static bool MarkComponentsWithErrors { get; set; }

            [DefaultValue(true)]
            [StiSerializable]
            public static bool AdvancedModeOfUndoRedoService { get; set; } = true;

            [DefaultValue(true)]
            [StiSerializable]
            public static bool SortDictionaryByAliases { get; set; } = true;

            /// <summary>
            /// If property equal true then main menu from designer control showes on StiDesigner form.
            /// </summary>
            [DefaultValue(false)]
            [Description("If property equal true then main menu from designer control showes on StiDesigner form.")]
            [StiSerializable]
            public static bool ShowDesignerControlMainMenu { get; set; }

            /// <summary>
            /// Gets or sets a value which allows to show or to hide Events Tab of the property editor in the report designer. 
            /// </summary>
            [DefaultValue(true)]
            [StiSerializable]
            public static bool ShowEventsTab { get; set; } = true;
            
            /// <summary>
            /// Gets or sets value which indicates that designer called from VisualStudio.Net.
            /// Internal use only.
            /// </summary>
            [DefaultValue(false)]
            [Description("Gets or sets value which indicates that designer called from VisualStudio.Net. Internal use only.")]
            [StiSerializable]
            public static bool RunInDesignMode { get; set; }

            [DefaultValue(true)]
            [StiSerializable]
            public static bool ShowSelectTypeOfGuiOption { get; set; } = true;

            [DefaultValue(false)]
            [StiSerializable]
            public static bool UseSimpleGlobalizationEditor { get; set; }

            [DefaultValue(false)]
            [StiSerializable]
            public static bool UseRightToLeftGlobalizationEditor { get; set; }

            [DefaultValue(false)]
            [StiSerializable]
            public static bool AllowUseWinControl { get; set; }

            private static bool hideConnectionString;
            [DefaultValue(false)]
            [StiSerializable]
            public static bool HideConnectionString
            {
                get
                {
                    return hideConnectionString;
                }
                set
                {
                    hideConnectionString = value;
                    if (hideConnectionString)
                    {
                        Properties.Hide("ConnectionString");
                        CodeTabVisible = false;
                    }
                    else
                    {
                        Properties.Show("ConnectionString");
                    }
                }
            }

            /// <summary>
            /// Gets or sets the maximal nested level of dictionary objects in report dictionary panel.
            /// </summary>
            [DefaultValue(0)]
            [Description("Gets or sets the maximal nested level of dictionary objects in report dictionary panel.")]
            [StiSerializable]
            public static int MaxLevelOfDictionaryObjects { get; set; }

            public static object BackgroundColor { get; set; }


            private static StiStylesCollection styles;
			public static StiStylesCollection Styles
			{
				get
				{
				    return styles ?? (styles = new StiStylesCollection
				    {
				        new StiStyle("Normal", "Normal")
				        {
				            Brush = new StiSolidBrush(Color.Transparent),
				            TextBrush = new StiSolidBrush(Color.Black)
				        },
				        new StiStyle("Bad", "Bad")
				        {
				            Brush = new StiSolidBrush(Color.FromArgb(0xFF, 0xFF, 0xC7, 0xCE)),
				            TextBrush = new StiSolidBrush(Color.FromArgb(0xFF, 0xD0, 0x37, 0x05))
				        },
				        new StiStyle("Good", "Good")
				        {
				            Brush = new StiSolidBrush(Color.FromArgb(0xFF, 0xC6, 0xEF, 0xCE)),
				            TextBrush = new StiSolidBrush(Color.FromArgb(0xFF, 0x00, 0x61, 0x5E))
				        },
				        new StiStyle("Neutral", "Neutral")
				        {
				            Brush = new StiSolidBrush(Color.FromArgb(0xFF, 0xFF, 0xEB, 0x9C)),
				            TextBrush = new StiSolidBrush(Color.FromArgb(0xFF, 0xAE, 0x7F, 0x2B))
				        },
				        new StiStyle("Warning", "Warning")
				        {
				            Brush = new StiSolidBrush(Color.Transparent),
				            TextBrush = new StiSolidBrush(Color.Red),
				            Font = new Font("Arial", 8, FontStyle.Bold)
				        },
				        new StiStyle("Note", "Note")
				        {
				            Brush = new StiSolidBrush(Color.FromArgb(0xFF, 0xFF, 0xFF, 0xCC)),
				            TextBrush = new StiSolidBrush(Color.Black)
				        }
				    });
				}
			}

            [DefaultValue(true)]
            [StiSerializable]
            public static bool AutoLargeHeight { get; set; } = true;

            [DefaultValue(true)]
            [StiSerializable]
            public static bool CalculateBarcodeValueInDesignMode { get; set; } = true;

            [DefaultValue(true)]
            [StiSerializable]
            public static bool UseGlobalizationManager { get; set; } = true;
            #endregion

        }
    }
}