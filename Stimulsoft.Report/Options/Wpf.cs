#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using System.Collections.Generic;
using System.ComponentModel;

namespace Stimulsoft.Report
{
    /// <summary>
    /// Class for adjustment all aspects of Stimulsoft Reports.
    /// </summary>
    public sealed partial class StiOptions
    {
        public sealed class Wpf
        {
            #region WpfBrowserApplication
            public sealed class WpfBrowserApplication
            {
                [DefaultValue(false)]
                [StiSerializable]
                public static bool IsWpfBrowserApplication { get; set; }
            }
            #endregion

            #region Viewer
            public sealed class Viewer
            {
                [DefaultValue(false)]
                [StiSerializable]
                public static bool UsePageShadow { get; set; } = true;

                #region CollapsingImages
                public sealed class CollapsingImages
                {
                    [DefaultValue(false)]
                    [StiSerializable]
                    public static bool UseCustomCollapsingImages { get; set; }

                    [DefaultValue(null)]
                    [StiSerializable]
                    public static string CollapsedImagePath { get; set; }

                    [DefaultValue(null)]
                    [StiSerializable]
                    public static string ExpandedImagePath { get; set; }
                }
                #endregion
            }
            #endregion

            #region ThemeNames
            public static class Themes
            {
                internal static StiWpfThemeInfo BaseOffice2013Theme { get; set; }

                public static List<StiWpfThemeInfo> ThemeList { get; set; } = new List<StiWpfThemeInfo>();
                
                /// <summary>
                /// Gets name of current theme.
                /// </summary>
                public static string DefaultTheme => Office2013Theme;

                /// <summary>
                /// Gets name of Office 2013 theme.
                /// </summary>
                public static string Office2013Theme => "Office 2013";

                /// <summary>
                /// Gets name of Office 2010 White theme.
                /// </summary>
                public static string Office2010WhiteTheme => "Office 2010 White";

                /// <summary>
                /// Gets name of Office 2010 Blue theme.
                /// </summary>
                public static string Office2010BlueTheme => "Office 2010 Blue";
                
                /// <summary>
                /// Gets name of Office 2007 Blue theme.
                /// </summary>
                public static string Office2007BlueTheme => "Office 2007 Blue";

                /// <summary>
                /// Gets name of Office 2007 Silver theme.
                /// </summary>
                public static string Office2007SilverTheme => "Office 2007 Silver";

                /// <summary>
                /// Gets name of Office 2007 Black theme.
                /// </summary>
                public static string Office2007BlackTheme => "Office 2007 Black";

                /// <summary>
                /// Gets name of Office 2003 Blue theme.
                /// </summary>
                public static string Office2003BlueTheme => "Office 2003 Blue";

                /// <summary>
                /// Gets name of Office 2003 Olive Green theme.
                /// </summary>
                public static string Office2003OliveGreenTheme => "Office 2003 Olive Green";

                /// <summary>
                /// Gets name of Office 2003 Silver theme.
                /// </summary>
                public static string Office2003SilverTheme => "Office 2003 Silver";

                /// <summary>
                /// Gets name of Black theme.
                /// </summary>
                public static string BlackTheme => "Black";
            }
            #endregion

            #region Properties
            [DefaultValue(true)]
            [StiSerializable]
            public static bool DisableAllAnimationsInViewer { get; set; } = true;

            [DefaultValue(true)]
            [StiSerializable]
            public static bool LoadTheme { get; set; } = true;

            [DefaultValue(false)]
            [StiSerializable]
            public static bool UseGDIPrintingInWPFViewer { get; set; }

            [DefaultValue(true)]
            [StiSerializable]
            public static bool UseNewWpfPrintingMethod { get; set; } = true;
            
            /// <summary>
            /// This property let to show in WPF designer properties of components, which unavailable in WPF.
            /// </summary>
            [Description("This property let to show in WPF designer properties of components, which unavailable in WPF.")]
            public static bool ShowAllPropertiesInWpf
            {
                get
                {
                    return StiPropertiesTab.ShowAllPropertiesInWpf;
                }
                set
                {
                    StiPropertiesTab.ShowAllPropertiesInWpf = value;
                }
            }

            public static bool IsGuiTypeSelected
            {
                get
                {
                    StiSettings.Load();
                    return StiSettings.GetBool("GuiHelper", "IsGuiTypeSelected", false);
                }
                set
                {
                    StiSettings.Load();
                    StiSettings.Set("GuiHelper", "IsGuiTypeSelected", value);
                    StiSettings.Save();
                }
            }

            /// <summary>
            /// Gets or sets name of current theme for Reports.Wpf applications.
            /// </summary>
            public static string CurrentTheme
            {
                get
                {
                    StiSettings.Load();
                    return StiSettings.GetStr("Wpf", "CurrentTheme", Themes.DefaultTheme);
                }
                set
                {
                    if (string.IsNullOrEmpty(value))
                        value = Themes.DefaultTheme;

                    StiSettings.Load();
                    StiSettings.Set("Wpf", "CurrentTheme", value);
                    StiSettings.Save();
                }
            }
            #endregion

            #region Methods
            public static StiWpfThemeInfo GetThemeInfoForCurrentTheme()
            {
                var name = CurrentTheme.Replace(" ", string.Empty);

                foreach (var theme in Themes.ThemeList)
                {
                    if (theme.Name.Replace(" ", string.Empty) != name) continue;

                    // ���� ������ ������� ���� �� ��������� � ���������� - �� ���������� ������� ����, ������� ���� ������ Office2010White
                    return !theme.IsLoaded ? Themes.BaseOffice2013Theme : theme;
                }
                return Themes.BaseOffice2013Theme;
            }
            #endregion

            static Wpf()
            {
                Themes.BaseOffice2013Theme = new StiWpfThemeInfo(Themes.Office2013Theme, null,
                    "/Stimulsoft.Report.Wpf;component/Themes/Office2013Theme.xaml",
                    "/Stimulsoft.Report.Wpf;component/Themes/Resources/CloudToolTip.xaml",
                    null);

                #region Black Theme
                Themes.ThemeList.Add(new StiWpfThemeInfo(
                        Themes.BlackTheme, "Stimulsoft.Report.Wpf.BlackTheme.dll",
                        "/Stimulsoft.Report.Wpf.BlackTheme;component/BlackTheme.xaml",
                        "/Stimulsoft.Report.Wpf.BlackTheme;component/Resources/CloudToolTip.xaml",
                        "Stimulsoft.Report.Wpf.BlackTheme.StiTheme, Stimulsoft.Report.Wpf.BlackTheme, "));
                #endregion
                
                #region Office 2003 Blue Theme
                Themes.ThemeList.Add(
                    new StiWpfThemeInfo(
                        Themes.Office2003BlueTheme, "Stimulsoft.Report.Wpf.Office2003BlueTheme.dll",
                        "/Stimulsoft.Report.Wpf.Office2003BlueTheme;component/Office2003BlueTheme.xaml",
                        "/Stimulsoft.Report.Wpf.Office2003BlueTheme;component/Resources/CloudToolTip.xaml",
                        "Stimulsoft.Report.Wpf.Office2003BlueTheme.StiTheme, Stimulsoft.Report.Wpf.Office2003BlueTheme, "));
                #endregion

                #region Office 2003 Olive Green Theme
                Themes.ThemeList.Add(
                    new StiWpfThemeInfo(
                        Themes.Office2003OliveGreenTheme, "Stimulsoft.Report.Wpf.Office2003OliveGreenTheme.dll",
                        "/Stimulsoft.Report.Wpf.Office2003OliveGreenTheme;component/Office2003OliveGreenTheme.xaml",
                        "/Stimulsoft.Report.Wpf.Office2003OliveGreenTheme;component/Resources/CloudToolTip.xaml",
                        "Stimulsoft.Report.Wpf.Office2003OliveGreenTheme.StiTheme, Stimulsoft.Report.Wpf.Office2003OliveGreenTheme, "));
                #endregion

                #region Office 2003 Silver Theme
                Themes.ThemeList.Add(
                    new StiWpfThemeInfo(
                        Themes.Office2003SilverTheme, "Stimulsoft.Report.Wpf.Office2003SilverTheme.dll",
                        "/Stimulsoft.Report.Wpf.Office2003SilverTheme;component/Office2003SilverTheme.xaml",
                        "/Stimulsoft.Report.Wpf.Office2003SilverTheme;component/Resources/CloudToolTip.xaml",
                        "Stimulsoft.Report.Wpf.Office2003SilverTheme.StiTheme, Stimulsoft.Report.Wpf.Office2003SilverTheme, "));
                #endregion

                #region Office 2007 Black Theme
                Themes.ThemeList.Add(
                    new StiWpfThemeInfo(
                        Themes.Office2007BlackTheme, "Stimulsoft.Report.Wpf.Office2007BlackTheme.dll",
                        "/Stimulsoft.Report.Wpf.Office2007BlackTheme;component/Office2007BlackTheme.xaml",
                        "/Stimulsoft.Report.Wpf.Office2007BlackTheme;component/Resources/CloudToolTip.xaml",
                        "Stimulsoft.Report.Wpf.Office2007BlackTheme.StiTheme, Stimulsoft.Report.Wpf.Office2007BlackTheme, "));
                #endregion

                #region Office 2007 Blue Theme
                Themes.ThemeList.Add(
                    new StiWpfThemeInfo(
                        Themes.Office2007BlueTheme, "Stimulsoft.Report.Wpf.Office2007BlueTheme.dll",
                        "/Stimulsoft.Report.Wpf.Office2007BlueTheme;component/Office2007BlueTheme.xaml",
                        "/Stimulsoft.Report.Wpf.Office2007BlueTheme;component/Resources/CloudToolTip.xaml",
                        "Stimulsoft.Report.Wpf.Office2007BlueTheme.StiTheme, Stimulsoft.Report.Wpf.Office2007BlueTheme, "));
                #endregion

                #region Office 2007 Silver Theme
                Themes.ThemeList.Add(
                    new StiWpfThemeInfo(
                        Themes.Office2007SilverTheme, "Stimulsoft.Report.Wpf.Office2007SilverTheme.dll",
                        "/Stimulsoft.Report.Wpf.Office2007SilverTheme;component/Office2007SilverTheme.xaml",
                        "/Stimulsoft.Report.Wpf.Office2007SilverTheme;component/Resources/CloudToolTip.xaml",
                        "Stimulsoft.Report.Wpf.Office2007SilverTheme.StiTheme, Stimulsoft.Report.Wpf.Office2007SilverTheme, "));
                #endregion                

                #region Office 2010 Blue Theme
                Themes.ThemeList.Add(
                    new StiWpfThemeInfo(
                        Themes.Office2010BlueTheme, "Stimulsoft.Report.Wpf.Office2010BlueTheme.dll",
                        "/Stimulsoft.Report.Wpf.Office2010BlueTheme;component/Office2010BlueTheme.xaml",
                        "/Stimulsoft.Report.Wpf.Office2010BlueTheme;component/Resources/CloudToolTip.xaml",
                        "Stimulsoft.Report.Wpf.Office2010BlueTheme.StiTheme, Stimulsoft.Report.Wpf.Office2010BlueTheme, "));
                #endregion

                #region Office 2010 White Theme
                Themes.ThemeList.Add(
                    new StiWpfThemeInfo(
                        Themes.Office2010WhiteTheme, "Stimulsoft.Report.Wpf.Office2010WhiteTheme.dll",
                        "/Stimulsoft.Report.Wpf.Office2010WhiteTheme;component/Office2010WhiteTheme.xaml",
                        "/Stimulsoft.Report.Wpf.Office2010WhiteTheme;component/Resources/CloudToolTip.xaml",
                        "Stimulsoft.Report.Wpf.Office2010WhiteTheme.StiTheme, Stimulsoft.Report.Wpf.Office2010WhiteTheme, "));
                #endregion                                

                #region Office 2013 Theme
                Themes.ThemeList.Add(
                    new StiWpfThemeInfo(
                        Themes.Office2013Theme, null,
                        "/Stimulsoft.Report.Wpf;component/Themes/Office2013Theme.xaml",
                        "/Stimulsoft.Report.Wpf;component/Themes/Resources/CloudToolTip.xaml",
                        null));
                #endregion
            }
        }
    }
}