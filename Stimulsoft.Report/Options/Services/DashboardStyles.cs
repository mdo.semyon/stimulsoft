#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.Dashboard.Styles;
using System.Collections.Generic;

namespace Stimulsoft.Report
{
    /// <summary>
    /// Class for adjustment all aspects of Stimulsoft Reports.
    /// </summary>
    public sealed partial class StiOptions
	{
        public sealed partial class Services
        {
            public static class Dashboards
            {
                private static List<StiDashboardStyle> dashboardStyles;
                public static List<StiDashboardStyle> DashboardStyles
                {
                    get
                    {
                        lock (lockObject)
                        {
                            return dashboardStyles ?? (dashboardStyles = new List<StiDashboardStyle>
                            {
                                new StiOrangeDashboardStyle(),
                                new StiGreenDashboardStyle(),
                                new StiTurquoiseDashboardStyle(),
                            });
                        }
                    }
                }

                private static List<StiIndicatorElementStyle> indicatorStyles;
                public static List<StiIndicatorElementStyle> IndicatorStyles
                {
                    get
                    {
                        lock (lockObject)
                        {
                            return indicatorStyles ?? (indicatorStyles = new List<StiIndicatorElementStyle>
                            {
                                new StiOrangeIndicatorElementStyle(),
                                new StiGreenIndicatorElementStyle(),
                                new StiTurquoiseIndicatorElementStyle(),
                            });
                        }
                    }
                }

                private static List<StiPivotElementStyle> pivotStyles;
                public static List<StiPivotElementStyle> PivotStyles
                {
                    get
                    {
                        lock (lockObject)
                        {
                            return pivotStyles ?? (pivotStyles = new List<StiPivotElementStyle>
                            {
                                new StiOrangePivotElementStyle(),
                                new StiGreenPivotElementStyle(),
                                new StiTurquoisePivotElementStyle(),
                            });
                        }
                    }
                }

                private static List<StiProgressElementStyle> progressStyles;
                public static List<StiProgressElementStyle> ProgressStyles
                {
                    get
                    {
                        lock (lockObject)
                        {
                            return progressStyles ?? (progressStyles = new List<StiProgressElementStyle>
                            {
                                new StiOrangeProgressElementStyle(),
                                new StiGreenProgressElementStyle(),
                                new StiTurquoiseProgressElementStyle(),
                            });
                        }
                    }
                }

                private static List<StiTableElementStyle> tableStyles;
                public static List<StiTableElementStyle> TableStyles
                {
                    get
                    {
                        lock (lockObject)
                        {
                            return tableStyles ?? (tableStyles = new List<StiTableElementStyle>
                            {
                                new StiOrangeTableElementStyle(),
                                new StiGreenTableElementStyle(),
                                new StiTurquoiseTableElementStyle(),
                            });
                        }
                    }
                }
            }
        }
	}
}