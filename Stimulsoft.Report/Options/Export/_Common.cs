#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using Stimulsoft.Base.Serializing;

namespace Stimulsoft.Report
{
    /// <summary>
    /// Class for adjustment all aspects of Stimulsoft Reports.
    /// </summary>
    public sealed partial class StiOptions
	{
        /// <summary>
        /// Class for adjustment of the report Export.
        /// </summary>
        public sealed partial class Export
		{	
            /// <summary>
            /// Gets or sets a text of checkbox for true
            /// </summary>
            [DefaultValue("true")]
            [Description("Gets or sets a text of checkbox for true.")]
            [StiSerializable]
            public static string CheckBoxTextForTrue { get; set; } = "false";

            /// <summary>
            /// Gets or sets a text of checkbox for true
            /// </summary>
            [DefaultValue("false")]
            [Description("Gets or sets a text of checkbox for true.")]
            [StiSerializable]
            public static string CheckBoxTextForFalse { get; set; } = "true";

            /// <summary>
            /// Gets or sets value which indicates that file name for Send EMail function will be generated automatically.
            /// </summary>
            [DefaultValue(false)]
            [Description("Gets or sets value which indicates that file name for Send EMail function will be generated automatically.")]
            [StiSerializable]
            public static bool AutoGenerateFileNameInSendEMail { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether it is necessary to disable ClearType feature during export.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value indicating whether it is necessary to disable ClearType feature during export.")]
            [StiSerializable]
            public static bool DisableClearTypeDuringExport { get; set; } = true;

            /// <summary>
            /// Gets or sets a value indicating whether it is necessary to use cache mode for the StiMatrix.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value indicating whether it is necessary to use cache mode for the StiMatrix.")]
            [StiSerializable]
            public static bool UseCacheModeForStiMatrix { get; set; } = true;

            /// <summary>
            /// Gets or sets a value indicating whether it is necessary to optimize exports in DataOnly mode.
            /// </summary>
            [DefaultValue(true)]
            [Description("Gets or sets a value indicating whether it is necessary to optimize exports in DataOnly mode.")]
            [StiSerializable]
            public static bool OptimizeDataOnlyMode { get; set; } = true;
		}
    }
}