﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiXRadarAxisLabelGeom : StiCellGeom
    {
        #region Properties
        private Color borderColor;
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
        }

        private StiBrush labelBrush;
        public StiBrush LabelBrush
        {
            get
            {
                return labelBrush;
            }
        }

        private string text;
        public string Text
        {
            get
            {
                return text;
            }
        }

        private float angle;
        public float Angle
        {
            get
            {
                return angle;
            }
        }

        private PointF point;
        public PointF Point
        {
            get
            {
                return point;
            }
        }

        private RectangleF labelRect;
        public RectangleF LabelRect
        {
            get
            {
                return labelRect;
            }
        }

        private IStiXRadarAxis axis;
        public IStiXRadarAxis Axis
        {
            get
            {
                return axis;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            Rectangle rect = Rectangle.Round(this.ClientRectangle);            

            StiPenGeom borderPen = new StiPenGeom(BorderColor);
            StiFontGeom font = StiFontGeom.ChangeFontSize(Axis.Labels.Font, (float)(Axis.Labels.Font.Size * context.Options.Zoom));
            StiStringFormatGeom sf = context.GetGenericStringFormat();

            sf.Trimming = StringTrimming.None;

            if (!this.Axis.Labels.WordWrap)
                sf.FormatFlags = StringFormatFlags.MeasureTrailingSpaces | StringFormatFlags.NoWrap;

            #region Draw label area
            if (Axis.Labels.Antialiasing)
                context.PushSmoothingModeToAntiAlias();

            //Округляем для того чтобы убрать смазывание бордюра labels
            int distX = (int)Math.Round(labelRect.X + labelRect.Width / 2, 0);
            int distY = (int)Math.Round(labelRect.Y + labelRect.Height / 2, 0);
            
            context.PushTranslateTransform(distX, distY);
            context.PushRotateTransform(Angle);                        
            
            if (!rect.IsEmpty)
            {
                context.FillRectangle(Axis.Labels.Brush, rect, null);

                if (Axis.Labels.DrawBorder)
                    context.DrawRectangle(borderPen, rect.X, rect.Y, rect.Width, rect.Height);
            }
                        
            context.PopTransform();
            context.PopTransform();
            
            if (Axis.Labels.Antialiasing)
                context.PopSmoothingMode();
            #endregion

            StiRotationMode mode = StiRotationMode.CenterBottom;

            float textAngle = Angle;
            if (Angle >= 90 && Angle <= 270)
            {
                textAngle = Angle + 180;
                mode = StiRotationMode.CenterTop;
            }

            #region Rotation Labels
            if (!(this.Axis.Labels.RotationLabels))
            {
                if (Angle > 0 && Angle < 180)
                {
                    mode = StiRotationMode.LeftCenter;
                }
                else if (Angle < 360 && Angle > 180)
                {
                    mode = StiRotationMode.RightCenter;
                }
                else if (Angle == 0)
                {
                    mode = StiRotationMode.CenterBottom;
                }
                else if (Angle == 180)
                {
                    mode = StiRotationMode.CenterTop;
                }

                textAngle = 0;
            }
            #endregion

            context.DrawRotatedString(
                Text, font, labelBrush, point,
                sf, mode, textAngle, Axis.Labels.Antialiasing, (int)(Axis.Labels.Width * context.Options.Zoom));

            //Red line
            //context.DrawRectangle(new StiPenGeom(Color.Red), point.X, point.Y, 4, 4);
        }
        #endregion

        public StiXRadarAxisLabelGeom(IStiXRadarAxis axis, string text, StiBrush labelBrush, Color borderColor,
            float angle, RectangleF clientRectangle, RectangleF labelRect, PointF point)
            : base(clientRectangle)
        {
            this.axis = axis;
            this.labelRect = labelRect;
            this.text = text;
            this.angle = angle;
            this.point = point;
            this.labelBrush = labelBrush;
            this.borderColor = borderColor;
            
        }
    }
}
