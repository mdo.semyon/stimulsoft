﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiAxisTitleGeom : StiCellGeom
    {
        #region Properties
        private IStiAxis axis;
        public IStiAxis Axis
        {
            get
            {
                return axis;
            }
        }

        private float angle = 0f;
        public float Angle
        {
            get
            {
                return angle;
            }
        }
        #endregion

        #region Methods
        public override void Draw(StiContext context)
        {
            StiSolidBrush brush = new StiSolidBrush(Axis.Title.Color);
            StiFontGeom font = StiFontGeom.ChangeFontSize(Axis.Title.Font, Axis.Title.Font.Size * context.Options.Zoom);
            StiStringFormatGeom sf = context.GetDefaultStringFormat();
            
            context.DrawRotatedString(Axis.Title.Text, font, brush, ClientRectangle, sf, StiRotationMode.CenterCenter, Angle, Axis.Title.Antialiasing);

            //Red line
            //context.DrawRectangle(new StiPenGeom(Color.Green), this.ClientRectangle.X, this.ClientRectangle.Y, this.ClientRectangle.Width, this.ClientRectangle.Height);
        }
        #endregion

        public StiAxisTitleGeom(IStiAxis axis, RectangleF clientRectangle, float angle, StringAlignment stringAlignment)
            : base(clientRectangle)
        {
            this.axis = axis;
            this.angle = angle;
        }
    }
}
