﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiTrendCurveGeom : StiCellGeom
    {
        #region Properties
        private PointF?[] points;
        public PointF?[] Points
        {
            get
            {
                return points;
            }
        }

        private IStiTrendLine trendLine;
        public IStiTrendLine TrendLine
        {
            get
            {
                return trendLine;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            PointF[] pointsNull = new PointF[points.Length];
            for (int index = 0; index < points.Length; index++)
            {
                pointsNull[index] = points[index].Value;
            }

            IStiTrendLine line = this.trendLine;

            Color lineColor = this.trendLine.LineColor;
            float lineWidth = this.trendLine.LineWidth;
            StiPenStyle style = this.trendLine.LineStyle;
            bool showShadow = this.trendLine.ShowShadow;

            float scaledLineWidth = lineWidth * context.Options.Zoom;

            context.PushSmoothingModeToAntiAlias();

            #region showShadow
            if (showShadow)
            {
                StiPenGeom penShadow = new StiPenGeom(Color.FromArgb(50, 0, 0, 0), scaledLineWidth + 0.5f * context.Options.Zoom);
                penShadow.PenStyle = style;

                context.PushTranslateTransform(scaledLineWidth, scaledLineWidth);
                StiNullableDrawing.DrawCurve(context, penShadow, this.points, 0);
                context.PopTransform();
            }
            #endregion

            #region Draw Lines
            StiPenGeom pen = new StiPenGeom(lineColor, lineWidth * context.Options.Zoom);
            pen.PenStyle = style;
            context.DrawCurve(pen, pointsNull, 0);
            #endregion

            context.PopSmoothingMode();

        }
        #endregion

        public StiTrendCurveGeom(PointF?[] points, IStiTrendLine trendLine)
            :
            base(StiBaseLineSeriesGeom.GetClientRectangle(points, trendLine.LineWidth))
        {
            this.points = points;
            this.trendLine = trendLine;
        }
    }
}