﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiConstantLinesVerticalGeom : StiCellGeom
    {
        #region Properties
        private IStiConstantLines line;
        public IStiConstantLines Line
        {
            get
            {
                return line;
            }
        }

        private PointF point;
        public PointF Point
        {
            get
            {
                return point;
            }
        }

        private StiRotationMode mode;
        public StiRotationMode Mode
        {
            get
            {
                return mode;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            RectangleF rect = this.ClientRectangle;
            
            #region Draw Lines
            StiPenGeom pen = new StiPenGeom(line.LineColor, line.LineWidth);
            pen.PenStyle = line.LineStyle;
            context.DrawLine(pen, rect.Left, rect.Top, rect.Left, rect.Bottom);
            #endregion

            #region Draw Text
            if (line.TitleVisible)
            {
                StiBrush brush = new StiSolidBrush(line.LineColor);
                StiFontGeom font = StiFontGeom.ChangeFontSize(line.Font, (float)(line.Font.Size * context.Options.Zoom));

                StiStringFormatGeom sf = context.GetGenericStringFormat();

                context.DrawRotatedString(line.Text, font, brush, Point, sf, Mode, 90, line.Antialiasing, 0);
            }
            #endregion
        }
        #endregion

        public StiConstantLinesVerticalGeom(IStiConstantLines line, RectangleF clientRectangle, PointF point, StiRotationMode mode)
            :
            base(clientRectangle)
        {
            this.line = line;
            this.point = point;
            this.mode = mode;
        }
    }
}
