﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;

namespace Stimulsoft.Report.Chart
{
    public class StiOutsidePieLabelsGeom : StiCenterPieLabelsGeom
    {
        #region Properties
        private Color lineColor;
        public Color LineColor
        {
            get
            {
                return lineColor;
            }
        }

        private PointF labelPoint;
        public PointF LabelPoint
        {
            get
            {
                return labelPoint;
            }
        }

        private PointF startPoint;
        public PointF StartPoint
        {
            get
            {
                return startPoint;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            IStiOutsidePieLabels outsidePieLabels = SeriesLabels as IStiOutsidePieLabels;
            base.Draw(context);
            
            if (outsidePieLabels.DrawBorder)
            {
                StiPenGeom borderPen = new StiPenGeom(LineColor);

                var chart = this.Series.Chart as StiChart;

                if (chart.IsAnimation)
                {
                    var animation = new StiOpacityAnimation(StiChartHelper.GlobalBeginTimeElement, StiChartHelper.GlobalBeginTimeElement);
                    context.DrawAnimationLines(borderPen, new PointF[] { labelPoint, StartPoint }, animation);
                }
                else
                    context.DrawLine(borderPen, LabelPoint.X, LabelPoint.Y, StartPoint.X, StartPoint.Y);
            }
        }
        #endregion

        public StiOutsidePieLabelsGeom(IStiSeriesLabels seriesLabels, IStiSeries series, int index, double value, RectangleF clientRectangle, string text,
            StiBrush seriesBrush, StiBrush labelBrush, StiBrush seriesLabelsBrush, Color borderColor, Color seriesBorderColor,
            StiRotationMode rotationMode, RectangleF labelRect, float angleToUse, Color lineColor, 
            PointF labelPoint, PointF startPoint)
            : base(seriesLabels, series, index, value, clientRectangle, text,
                seriesBrush, labelBrush, seriesLabelsBrush, borderColor, seriesBorderColor, 
                rotationMode, labelRect, angleToUse, null)
        {
            this.lineColor = lineColor;
            this.labelPoint = labelPoint;
            this.startPoint = startPoint;
        }
    }
}
