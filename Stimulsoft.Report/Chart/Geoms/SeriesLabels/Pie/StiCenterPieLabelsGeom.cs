﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;

namespace Stimulsoft.Report.Chart
{
    public class StiCenterPieLabelsGeom : StiSeriesLabelsGeom
    {
        #region Properties
        private StiBrush seriesBrush;
        public StiBrush SeriesBrush
        {
            get
            {
                return seriesBrush;
            }
        }

        private Color borderColor;
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
        }

        private Color seriesBorderColor;
        public Color SeriesBorderColor
        {
            get
            {
                return seriesBorderColor;
            }
        }


        private StiBrush seriesLabelsBrush;
        public StiBrush SeriesLabelsBrush
        {
            get
            {
                return seriesLabelsBrush;
            }
        }

        private StiBrush labelBrush;
        public StiBrush LabelBrush
        {
            get
            {
                return labelBrush;
            }
        }

        private string text;
        public string Text
        {
            get
            {
                return text;
            }
        }

        private StiRotationMode rotationMode;
        public StiRotationMode RotationMode
        {
            get
            {
                return rotationMode;
            }
        }

        private RectangleF labelRect;
        public RectangleF LabelRect
        {
            get
            {
                return labelRect;
            }
            set
            {
                labelRect = value;
            }
        }

        private float angleToUse;
        public float AngleToUse
        {
            get
            {
                return angleToUse;
            }
        }
        
        private StiAnimation animation;
        public StiAnimation Animation
        {
            get
            {
                return animation;
            }
            set
            {
                animation = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            base.Draw(context);

            Rectangle rect = Rectangle.Round(this.ClientRectangle);

            StiPenGeom borderPen = new StiPenGeom(BorderColor);
            StiFontGeom font = StiFontGeom.ChangeFontSize(SeriesLabels.Font, SeriesLabels.Font.Size * context.Options.Zoom);
            StiStringFormatGeom sf = SeriesLabels.Core.GetStringFormatGeom(context);
            
            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                if (this.animation == null)
                    animation = new StiOpacityAnimation(Duration, BeginTime);

                context.DrawAnimationLabel(Text, font, labelBrush, seriesLabelsBrush, borderPen, Rectangle.Round(labelRect), sf, rotationMode, SeriesLabels.Angle, SeriesLabels.DrawBorder, animation);
            }
            else
            {
                DrawMarker(context, Rectangle.Round(labelRect), SeriesBorderColor, SeriesBrush);

                //Округляем для того чтобы убрать смазывание бордюра labels
                int distX = (int)Math.Round(labelRect.X + labelRect.Width / 2, 0);
                int distY = (int)Math.Round(labelRect.Y + labelRect.Height / 2, 0);

                context.PushTranslateTransform(distX, distY);
                context.PushRotateTransform(AngleToUse);

                #region Draw label area
                if (!rect.IsEmpty)
                {
                    context.FillRectangle(seriesLabelsBrush, rect, null);

                    if (SeriesLabels.DrawBorder)
                        context.DrawRectangle(borderPen, rect.X, rect.Y, rect.Width, rect.Height);

                    if (IsMouseOver)
                        context.FillRectangle(StiMouseOverHelper.GetLineMouseOverColor(), rect.X, rect.Y, rect.Width, rect.Height, null);
                }
                #endregion

                context.PopTransform();
                context.PopTransform();

                context.DrawRotatedString(Text, font, labelBrush, Rectangle.Round(labelRect), sf, rotationMode, angleToUse, SeriesLabels.Antialiasing, rect.Width);
            }
        }
        #endregion

        public StiCenterPieLabelsGeom(IStiSeriesLabels seriesLabels, IStiSeries series, int index, double value,
            RectangleF clientRectangle, string text,
            StiBrush seriesBrush, StiBrush labelBrush, StiBrush seriesLabelsBrush, Color borderColor, Color seriesBorderColor,
            StiRotationMode rotationMode, RectangleF labelRect, float angleToUse, StiAnimation animation)
            : base(seriesLabels, series, index, value, clientRectangle)
        {
            this.text = text;
            this.labelBrush = labelBrush;
            this.borderColor = borderColor;
            this.seriesBorderColor = seriesBorderColor;
            this.seriesLabelsBrush = seriesLabelsBrush;
            this.seriesBrush = seriesBrush;
            this.rotationMode = rotationMode;
            this.labelRect = labelRect;
            this.angleToUse = angleToUse;
            this.animation = animation;
        }
    }
}
