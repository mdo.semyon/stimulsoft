﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiTwoColumnsPieLabelsGeom : StiSeriesLabelsGeom
    {
        #region Properties
        private StiBrush seriesBrush;
        public StiBrush SeriesBrush
        {
            get
            {
                return seriesBrush;
            }
        }

        private Color borderColor;
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
        }

        private Color seriesBorderColor;
        public Color SeriesBorderColor
        {
            get
            {
                return seriesBorderColor;
            }
        }

        private StiBrush labelBrush;
        public StiBrush LabelBrush
        {
            get
            {
                return labelBrush;
            }
        }

        private StiBrush seriesLabelsBrush;
        public StiBrush SeriesLabelsBrush
        {
            get
            {
                return seriesLabelsBrush;
            }
        }
        private string text;
        public string Text
        {
            get
            {
                return text;
            }
        }

        private RectangleF labelRect;
        public RectangleF LabelRect
        {
            get
            {
                return labelRect;
            }
        }

        private Color lineColor;
        public Color LineColor
        {
            get
            {
                return lineColor;
            }
        }

        
        private PointF startPoint;
        public PointF StartPoint
        {
            get
            {
                return startPoint;
            }
        }

        private PointF endPoint;
        public PointF EndPoint
        {
            get
            {
                return endPoint;
            }
            set
            {
                endPoint = value;
            }
        }

        private PointF arcPoint;
        public PointF ArcPoint
        {
            get
            {
                return arcPoint;
            }
        }

        private PointF centerPie;
        public PointF СenterPie
        {
            get
            {
                return centerPie;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            base.Draw(context);

            var labelRect = Rectangle.Round(this.ClientRectangle);

            var borderPen = new StiPenGeom(BorderColor, 1);
            var font = StiFontGeom.ChangeFontSize(SeriesLabels.Font, (float)(SeriesLabels.Font.Size * context.Options.Zoom));
            var sf = SeriesLabels.Core.GetStringFormatGeom(context);
            
            if (SeriesLabels.DrawBorder)
            {
                PointF? endPoint0 = null;

                var linePen = new StiPenGeom(LineColor, 1);

                if (((centerPie.Y > startPoint.Y) && (endPoint.Y > startPoint.Y)) || ((centerPie.Y < startPoint.Y) && (endPoint.Y < startPoint.Y)))
                {
                    if (centerPie.X > endPoint.X)
                    {
                        endPoint0 = new PointF(endPoint.X + 13, startPoint.Y);
                    }
                    else
                    {
                        endPoint0 = new PointF(endPoint.X - 13, startPoint.Y);
                    }
                }

                if (endPoint0 != null)
                {
                    context.DrawLines(linePen, new PointF[] { endPoint, endPoint0.GetValueOrDefault(), startPoint, arcPoint });
                }
                else
                {
                    context.DrawLines(linePen, new PointF[] { endPoint, startPoint, arcPoint });
                }
                
            }

            DrawMarker(context, Rectangle.Round(labelRect), SeriesBorderColor, SeriesBrush);

            if (!(float.IsNaN(labelRect.X) || float.IsNaN(labelRect.Y) || float.IsNaN(labelRect.Width) || float.IsNaN(labelRect.Height)))
            {
                context.FillRectangle(seriesLabelsBrush, labelRect, null);
                if (SeriesLabels.DrawBorder) context.DrawRectangle(borderPen, labelRect.X, labelRect.Y, labelRect.Width, labelRect.Height);
            }

            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            context.DrawRotatedString(this.Text,font, labelBrush, labelRect, sf, StiRotationMode.CenterCenter, 0f, SeriesLabels.Antialiasing, labelRect.Width);

            if (IsMouseOver)
                context.FillRectangle(StiMouseOverHelper.GetLineMouseOverColor(), labelRect, null);
        }

        protected override void DrawMarker(StiContext context, Rectangle itemRect, object markerColor, StiBrush markerBrush)
        {
            if (SeriesLabels.MarkerVisible)
            {
                var markerRect = Rectangle.Empty;
                var rightPosition = (int)(itemRect.Right + 2 * context.Options.Zoom);
                var leftPosition = (int)(itemRect.Left - (2 + SeriesLabels.MarkerSize.Width) * context.Options.Zoom);
                if (StiOptions.Engine.AllowFixPieChartMarkerAlignment)
                {
                    if (SeriesLabels.MarkerAlignment == StiMarkerAlignment.Center)
                    {
                        markerRect.X = ClientRectangle.X < centerPie.X ? rightPosition : leftPosition;
                    }
                    else if (SeriesLabels.MarkerAlignment == StiMarkerAlignment.Right)
                    {
                        markerRect.X = ClientRectangle.X < centerPie.X? rightPosition : leftPosition;
                    }
                    else
                    {
                        markerRect.X = ClientRectangle.X > centerPie.X ? rightPosition : leftPosition;
                    }
                }
                else
                {
                    if (SeriesLabels.MarkerAlignment == StiMarkerAlignment.Center)
                    {
                        markerRect.X = ClientRectangle.X < centerPie.X ? rightPosition : leftPosition;
                    }
                    else
                    {
                        markerRect.X = SeriesLabels.MarkerAlignment == StiMarkerAlignment.Right ? rightPosition : leftPosition;
                    }
                }

                markerRect.Y = (int)(itemRect.Y + (itemRect.Height - SeriesLabels.MarkerSize.Height * context.Options.Zoom) / 2);
                markerRect.Width = (int)(SeriesLabels.MarkerSize.Width * context.Options.Zoom);
                markerRect.Height = (int)(SeriesLabels.MarkerSize.Height * context.Options.Zoom);

                var color = markerColor is Color ? (Color)markerColor : Color.Black;
                var pen = new StiPenGeom(color, 1);

                context.FillRectangle(markerBrush, markerRect.X, markerRect.Y, markerRect.Width, markerRect.Height, null);
                context.DrawRectangle(pen, markerRect.X, markerRect.Y, markerRect.Width, markerRect.Height);
            }
        }

        #endregion

        public StiTwoColumnsPieLabelsGeom(IStiSeriesLabels seriesLabels, IStiSeries series, int index, double value,
            RectangleF clientRectangle, string text,
            StiBrush seriesBrush, StiBrush labelBrush, StiBrush seriesLabelsBrush, Color borderColor, Color seriesBorderColor,
            RectangleF labelRect, Color lineColor,
            PointF startPoint, PointF endPoint, PointF arcPoint, PointF centerPie)
            : base(seriesLabels, series, index, value, clientRectangle)
        {
            this.text = text;
            this.seriesLabelsBrush = seriesLabelsBrush;
            this.labelBrush = labelBrush;
            this.lineColor = lineColor;
            this.borderColor = borderColor;
            this.seriesBorderColor = seriesBorderColor;
            this.seriesBrush = seriesBrush;
            this.labelRect = labelRect;
            this.startPoint = startPoint;
            this.endPoint = endPoint;
            this.arcPoint = arcPoint;
            this.centerPie = centerPie;
        }
    }
}
