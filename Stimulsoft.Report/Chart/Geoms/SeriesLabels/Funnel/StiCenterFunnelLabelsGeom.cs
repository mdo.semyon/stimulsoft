﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiCenterFunnelLabelsGeom : StiSeriesLabelsGeom
    {
        #region Properties
        private StiBrush seriesBrush;
        public StiBrush SeriesBrush
        {
            get
            {
                return seriesBrush;
            }
        }

        private Color borderColor;
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
        }

        private Color seriesBorderColor;
        public Color SeriesBorderColor
        {
            get
            {
                return seriesBorderColor;
            }
        }

        private StiBrush labelBrush;
        public StiBrush LabelBrush
        {
            get
            {
                return labelBrush;
            }
        }

        private string text;
        public string Text
        {
            get
            {
                return text;
            }
        }
        
        private RectangleF labelRect;
        public RectangleF LabelRect
        {
            get
            {
                return labelRect;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            base.Draw(context);

            Rectangle rectCurrent = Rectangle.Round(this.ClientRectangle);

            StiPenGeom borderPen = new StiPenGeom(BorderColor);
            StiFontGeom font = StiFontGeom.ChangeFontSize(SeriesLabels.Font, (float)(SeriesLabels.Font.Size * context.Options.Zoom));

            StiStringFormatGeom sf = SeriesLabels.Core.GetStringFormatGeom(context);
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            
            DrawMarker(context, Rectangle.Round(labelRect), SeriesBorderColor, SeriesBrush);

            #region Draw label area
            if (!rectCurrent.IsEmpty)
            {
                int distX = rectCurrent.X + rectCurrent.Width / 2;
                int distY = rectCurrent.Y + rectCurrent.Height / 2;

                context.PushTranslateTransform(distX, distY);
                context.PushRotateTransform(SeriesLabels.Angle);

                Rectangle rect = rectCurrent;
                rect.X = -rect.Width / 2;
                rect.Y = -rect.Height / 2;

                context.FillRectangle(SeriesLabels.Brush, rect, null);

                if (SeriesLabels.DrawBorder)
                    context.DrawRectangle(borderPen, rect.X-1, rect.Y, rect.Width, rect.Height);

                context.PopTransform();
                context.PopTransform();

                context.DrawRotatedString(Text, font, labelBrush, labelRect, sf, StiRotationMode.CenterCenter, SeriesLabels.Angle, SeriesLabels.Antialiasing, rect.Width);

                if (IsMouseOver)
                    context.FillRectangle(StiMouseOverHelper.GetLineMouseOverColor(), rect.X, rect.Y, rect.Width, rect.Height, null);
            }
            #endregion
        }
        #endregion

        public StiCenterFunnelLabelsGeom(IStiSeriesLabels seriesLabels, IStiSeries series, int index, double value,
            RectangleF clientRectangle, string text,
            StiBrush seriesBrush, StiBrush labelBrush, Color borderColor, Color seriesBorderColor, RectangleF labelRect)
            : base(seriesLabels, series, index, value, clientRectangle)
        {
            this.text = text;
            this.labelBrush = labelBrush;
            this.borderColor = borderColor;
            this.seriesBorderColor = seriesBorderColor;
            this.seriesBrush = seriesBrush;
            this.labelRect = labelRect;
        }
    }
}
