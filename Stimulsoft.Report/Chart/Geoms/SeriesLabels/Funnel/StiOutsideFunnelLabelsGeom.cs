﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiOutsideFunnelLabelsGeom : StiCenterFunnelLabelsGeom
    {
        #region Properties
        private PointF startPointLine;
        public PointF StartPointLine
        {
            get
            {
                return startPointLine;
            }
        }

        private PointF endPointLine;
        public PointF EndPointLine
        {
            get
            {
                return endPointLine;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            StiPenGeom borderPen = new StiPenGeom(BorderColor);
            context.DrawLine(borderPen, startPointLine.X, startPointLine.Y, endPointLine.X, endPointLine.Y);

            base.Draw(context);            
        }
        #endregion

        public StiOutsideFunnelLabelsGeom(IStiSeriesLabels seriesLabels, IStiSeries series, int index, double value,
            RectangleF clientRectangle, string text,
            StiBrush seriesBrush, StiBrush labelBrush, Color borderColor, Color seriesBorderColor, RectangleF labelRect,
            PointF startPointLine, PointF endPointLine)
            : base(seriesLabels, series, index, value, clientRectangle, text, seriesBrush, labelBrush, borderColor, seriesBorderColor, labelRect)
        {
            this.startPointLine = startPointLine;
            this.endPointLine = endPointLine;
        }    
    }
}
