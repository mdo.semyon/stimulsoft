﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiOutsideAxisLabelsGeom : StiSeriesLabelsGeom
    {
        #region Properties
        private Color labelColor;
        public Color LabelColor
        {
            get
            {
                return labelColor;
            }
        }

        private Color labelBorderColor;
        public Color LabelBorderColor
        {
            get
            {
                return labelBorderColor;
            }
        }

        private StiBrush seriesBrush;
        public StiBrush SeriesBrush
        {
            get
            {
                return seriesBrush;
            }
        }

        private Color seriesBorderColor;
        public Color SeriesBorderColor
        {
            get
            {
                return seriesBorderColor;
            }
        }

        private StiFontGeom font;
        public StiFontGeom Font
        {
            get
            {
                return font;
            }
        }

        private string text;
        public string Text
        {
            get
            {
                return text;
            }
        }

        private PointF startPoint;
        public PointF StartPoint
        {
            get
            {
                return startPoint;
            }
        }

        private PointF endPoint;
        public PointF EndPoint
        {
            get
            {
                return endPoint;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            base.Draw(context);

            Rectangle labelRect = Rectangle.Round(this.ClientRectangle);

            StiBrush labelBrush = new StiSolidBrush(LabelColor);
            StiPenGeom borderPen = new StiPenGeom(LabelBorderColor);
            StiStringFormatGeom sf = SeriesLabels.Core.GetStringFormatGeom(context);

            DrawMarker(context, labelRect, this.SeriesBorderColor, this.SeriesBrush);
                        
            if (SeriesLabels.DrawBorder) context.DrawLine(borderPen, endPoint.X, endPoint.Y, startPoint.X, startPoint.Y);

            context.PushTranslateTransform(labelRect.X + labelRect.Width / 2, labelRect.Y + labelRect.Height / 2);
            context.PushRotateTransform(SeriesLabels.Angle);

            Rectangle rect = labelRect;

            rect.X = -rect.Width / 2;
            rect.Y = -rect.Height / 2;

            context.FillRectangle(SeriesLabels.Brush, rect, null);
            if (SeriesLabels.DrawBorder) 
                context.DrawRectangle(borderPen, rect.X, rect.Y, rect.Width, rect.Height);

            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            context.PopTransform();
            context.PopTransform();

            context.DrawRotatedString(Text, font, labelBrush, labelRect, sf, StiRotationMode.CenterCenter, SeriesLabels.Angle, SeriesLabels.Antialiasing, rect.Width);

            if (IsMouseOver)
                context.FillRectangle(StiMouseOverHelper.GetLineMouseOverColor(), labelRect, null);

        }
        #endregion

        public StiOutsideAxisLabelsGeom(IStiSeriesLabels seriesLabels, IStiSeries series, int index, double value,
            RectangleF clientRectangle, string text, Color labelColor, Color labelBorderColor, 
            StiBrush seriesBrush, Color seriesBorderColor, StiFontGeom font, PointF startPoint, PointF endPoint)
            : base(seriesLabels, series, index, value, clientRectangle)
        {
            this.text = text;
            this.labelColor = labelColor;
            this.labelBorderColor = labelBorderColor;
            this.seriesBrush = seriesBrush;
            this.seriesBorderColor = seriesBorderColor;
            this.font = font;
            this.startPoint = startPoint;
            this.endPoint = endPoint;
        }
    }
}
