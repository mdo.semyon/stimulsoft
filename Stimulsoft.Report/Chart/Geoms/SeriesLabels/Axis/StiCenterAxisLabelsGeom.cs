﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;

namespace Stimulsoft.Report.Chart
{
    public class StiCenterAxisLabelsGeom : StiSeriesLabelsGeom
    {
        #region Properties
        private Color labelColor;
        public Color LabelColor
        {
            get
            {
                return labelColor;
            }
        }

        private Color labelBorderColor;
        public Color LabelBorderColor
        {
            get
            {
                return labelBorderColor;
            }
        }

        private StiBrush seriesBrush;
        public StiBrush SeriesBrush
        {
            get
            {
                return seriesBrush;
            }
        }

        private StiBrush seriesLabelsBrush;
        public StiBrush SeriesLabelsBrush
        {
            get
            {
                return seriesLabelsBrush;
            }
        }

        private Color seriesBorderColor;
        public Color SeriesBorderColor
        {
            get
            {
                return seriesBorderColor;
            }
        }

        private StiFontGeom font;
        public StiFontGeom Font
        {
            get
            {
                return font;
            }
        }

        private string text;
        public string Text
        {
            get
            {
                return text;
            }
        }

        private StiAnimation animation;
        public StiAnimation Animation
        {
            get
            {
                return animation;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            base.Draw(context);

            Rectangle labelRect = Rectangle.Round(this.ClientRectangle);

            StiPenGeom borderPen = new StiPenGeom(LabelBorderColor, 1);
            StiBrush labelBrush = new StiSolidBrush(LabelColor);

            StiStringFormatGeom sf = SeriesLabels.Core.GetStringFormatGeom(context);
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                #region Draw Marker
                DrawMarker(context, labelRect, this.SeriesBorderColor, this.SeriesBrush);
                #endregion

                if (this.animation == null)
                    animation = new StiOpacityAnimation(Duration, BeginTime);

                context.DrawAnimationLabel(Text, font, labelBrush, seriesLabelsBrush, borderPen, labelRect, sf, StiRotationMode.CenterCenter, SeriesLabels.Angle, SeriesLabels.DrawBorder, animation);
            }
            else
            {
                #region Draw Marker
                DrawMarker(context, labelRect, this.SeriesBorderColor, this.SeriesBrush);
                #endregion

                #region Draw Label Rectangle
                int distX = labelRect.X + labelRect.Width / 2;
                int distY = labelRect.Y + labelRect.Height / 2;

                context.PushTranslateTransform(distX, distY);
                context.PushRotateTransform(SeriesLabels.Angle);

                Rectangle rect = labelRect;
                rect.X = -rect.Width / 2;
                rect.Y = -rect.Height / 2;

                context.FillRectangle(seriesLabelsBrush, rect, null);
                if (SeriesLabels.DrawBorder) context.DrawRectangle(borderPen, rect.X, rect.Y, rect.Width, rect.Height);

                context.PopTransform();
                context.PopTransform();
                #endregion

                #region Draw Label Text
                context.DrawRotatedString(Text, font, labelBrush, labelRect, sf, StiRotationMode.CenterCenter, SeriesLabels.Angle, SeriesLabels.Antialiasing, rect.Width);

                //red line
                //context.DrawRectangle(new StiPenGeom(Color.Red), labelRect.X, labelRect.Y, labelRect.Width, labelRect.Height);
                #endregion

                if (IsMouseOver)
                    context.FillRectangle(StiMouseOverHelper.GetLineMouseOverColor(), labelRect, null);
            }
        }
        #endregion

        public StiCenterAxisLabelsGeom(IStiSeriesLabels seriesLabels, IStiSeries series, int index, double value,
            RectangleF clientRectangle, string text, Color labelColor, Color labelBorderColor,
            StiBrush seriesBrush, StiBrush seriesLabelsBrush, Color seriesBorderColor, StiFontGeom font, StiAnimation animation)
            : base(seriesLabels, series, index, value, clientRectangle)
        {
            this.text = text;
            this.labelColor = labelColor;
            this.labelBorderColor = labelBorderColor;
            this.seriesBrush = seriesBrush;
            this.seriesLabelsBrush = seriesLabelsBrush;
            this.seriesBorderColor = seriesBorderColor;
            this.font = font;
            this.animation = animation;
        }
    }
}
