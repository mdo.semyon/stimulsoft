﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public abstract class StiAreaGeom : StiCellGeom
    {
        #region Properties
        private IStiArea area;
        public IStiArea Area
        {
            get
            {
                return area;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            RectangleF rect = ClientRectangle;
            if (rect.IsEmpty) return;

            if (rect.Width > 0 && rect.Height > 0)
            {
                #region Draw Shadow
                if (area.ShowShadow)
                    context.DrawCachedShadow(rect, StiShadowSides.All, context.Options.IsPrinting);
                #endregion

                #region Fill rectangle
                context.FillRectangle(area.Brush, rect.X, rect.Y, rect.Width, rect.Height, null);
                #endregion
            }
        }
        #endregion

        public StiAreaGeom(IStiArea area, RectangleF clientRectangle)
            : base(Rectangle.Round(clientRectangle))
        {
            this.area = area;
        }
    }
}
