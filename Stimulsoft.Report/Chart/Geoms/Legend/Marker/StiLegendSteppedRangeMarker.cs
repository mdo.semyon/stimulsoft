﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiLegendSteppedRangeMarker : IStiLegendMarker
    {
        public void Draw(StiContext context, IStiSeries series, RectangleF rect, int colorIndex, int colorCount)
        {
            var rangeSeries = series as StiSteppedRangeSeries;

            var marker = rangeSeries.Marker;
            var lineStyle = rangeSeries.LineStyle;
            var lineMarker = rangeSeries.LineMarker;
            var lineWidth = rangeSeries.LineWidth;
            var lineColor = rangeSeries.LineColor;
            var lighting = rangeSeries.Lighting;
            var seriesBrush = rangeSeries.Brush;

            var path = StiLegendMarkerHelper.GetSteppedMarkerPath(rect);

            StiPenGeom pen = new StiPenGeom(lineColor);

            context.PushSmoothingModeToAntiAlias();
            context.FillPath(seriesBrush, path, rect, null);
            context.DrawPath(pen, path, rect);
            context.PopSmoothingMode();
        }
    }
}
