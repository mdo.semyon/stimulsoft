#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Report.Chart
{
    public class StiLegendLineMarker : IStiLegendMarker
    {
        public void Draw(StiContext context, IStiSeries serie, RectangleF rect, int colorIndex, int colorCount)
        {
            var colorMarker = Color.Transparent;
            
            #region StiRadarSeries
            var radarSeries = serie as IStiRadarSeries;
            if (radarSeries != null)
            {
                colorMarker = StiBrush.ToColor(radarSeries.Marker.Brush);
            }
            #endregion

            #region StiRadarLineSeries
            var radarLineSeries = serie as IStiRadarLineSeries;
            if (radarLineSeries != null)
            {
                colorMarker = radarLineSeries.LineColor;
            }
            #endregion

            #region StiBaseLineSeries
            var lineSeries = serie as IStiBaseLineSeries;
            if (lineSeries != null)
            {
                colorMarker = lineSeries.LineColor;
            }
            #endregion

            #region StiStackedBaseLineSeries
            var stackedLineSeries = serie as IStiStackedBaseLineSeries;
            if (stackedLineSeries != null)
            {
                colorMarker = stackedLineSeries.LineColor;
            }
            #endregion

            #region StiScatterSeries
            var scatterSeries = serie as IStiScatterSeries;
            if (scatterSeries != null)
            {
                colorMarker = StiBrush.ToColor(scatterSeries.Marker.Brush);
            }
            #endregion

            #region StiScatterLineSeries
            var scatterLineSeries = serie as IStiScatterLineSeries;
            if (scatterLineSeries != null)
            {
                colorMarker = scatterLineSeries.LineColor;
            }
            #endregion
                       
            context.PushSmoothingModeToAntiAlias();
            context.FillEllipse(new StiSolidBrush(colorMarker), rect, null);
            context.PopSmoothingMode();            
        }
    }
}
