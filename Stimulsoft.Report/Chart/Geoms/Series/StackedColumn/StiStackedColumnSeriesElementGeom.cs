﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Report.Chart.Geoms.Series;

namespace Stimulsoft.Report.Chart
{
    public class StiStackedColumnSeriesElementGeom : StiSeriesElementGeom
    {
        #region Properties
        private StiBrush seriesBrush;
        public StiBrush SeriesBrush
        {
            get
            {
                return seriesBrush;
            }
        }

        private Color seriesBorderColor;
        public Color SeriesBorderColor
        {
            get
            {
                return seriesBorderColor;
            }
        }
        
        private TimeSpan? beginTime;
        public TimeSpan? BeginTime
        {
            get
            {
                return beginTime;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            RectangleF columnRect = this.ClientRectangle;

            StiPenGeom pen = new StiPenGeom(SeriesBorderColor, 1);

            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                if (Series.ShowShadow)
                {
                    var animationOpacityShadow = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, new TimeSpan(beginTime.Value.Ticks + StiChartHelper.GlobalDurationElement.Ticks));
                    context.DrawShadowRect(columnRect, 5, animationOpacityShadow);
                }
                
                var animationOpacity = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, beginTime);
                context.DrawAnimationColumn(seriesBrush, pen, columnRect, Value, this.GetToolTip(), this, animationOpacity, GetInteractionData());
            }
            else
            {
                Series.Chart.Style.Core.FillColumn(context, columnRect, seriesBrush, GetInteractionData());

                if (IsSelected)
                    context.FillRectangle(StiSelectedHelper.GetSelectedBrush(), columnRect.X, columnRect.Y, columnRect.Width, columnRect.Height, null);

                if (IsMouseOver || Series.Core.IsMouseOver)
                {
                    context.FillRectangle(StiMouseOverHelper.GetMouseOverColor(), columnRect.X, columnRect.Y, columnRect.Width, columnRect.Height, null);
                }

                PointF[] points = null;

                if (Value > 0)
                {
                    points = new PointF[]
								{
									new PointF(columnRect.X, columnRect.Bottom), 
									new PointF(columnRect.X, columnRect.Y),
									new PointF(columnRect.Right, columnRect.Y),
									new PointF(columnRect.Right, columnRect.Bottom)
								};
                }
                else
                {
                    points = new PointF[]
								{
									new PointF(columnRect.X, columnRect.Y), 
									new PointF(columnRect.X, columnRect.Bottom),
									new PointF(columnRect.Right, columnRect.Bottom),
									new PointF(columnRect.Right, columnRect.Y)
								};

                }
                context.DrawLines(pen, points);
            }
        }
        #endregion

        public StiStackedColumnSeriesElementGeom(StiAreaGeom areaGeom, double value, int index,
            StiBrush seriesBrush, Color seriesBorderColor, IStiSeries series, RectangleF clientRectangle, TimeSpan? beginTime)
            : base(areaGeom, value, index, series, clientRectangle)
        {
            this.seriesBrush = seriesBrush;
            this.seriesBorderColor = seriesBorderColor;

            this.beginTime = beginTime;
        }
    }
}
