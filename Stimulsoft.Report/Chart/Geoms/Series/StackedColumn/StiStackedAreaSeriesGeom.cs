﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiStackedAreaSeriesGeom : StiSeriesGeom
    {
        #region IStiGeomInteraction override
        public override void InvokeMouseEnter(StiInteractionOptions options)
        {
            if (!AllowMouseOver) return;

            if (!IsMouseOver)
            {
                IsMouseOver = true;
                options.UpdateContext = true;
            }
        }

        public override void InvokeMouseLeave(StiInteractionOptions options)
        {
            if (!AllowMouseOver) return;

            if (IsMouseOver)
            {
                IsMouseOver = false;
                options.UpdateContext = true;
            }
        }

        public virtual bool AllowMouseOver
        {
            get
            {
                return
                    (this.Series.Interaction.DrillDownEnabled && this.Series.Interaction.AllowSeries);
            }
        }

        public virtual bool IsMouseOver
        {
            get
            {
                if (this.Series == null)
                    return false;

                return this.Series.Core.IsMouseOver;
            }
            set
            {
                if (this.Series != null)
                    this.Series.Core.IsMouseOver = value;
            }
        }
        #endregion

        #region Properties
        private PointF?[] startPoints;
        public PointF?[] StartPoints
        {
            get
            {
                return startPoints;
            }
        }

        private PointF?[] endPoints;
        public PointF?[] EndPoints
        {
            get
            {
                return endPoints;
            }
        }
        #endregion

        #region Methods
        public override bool Contains(float x, float y)
        {
            IStiAxisArea axisArea = this.Series.Chart.Area as IStiAxisArea;
            float posY = axisArea.AxisCore.GetDividerY();

            if (Invisible) return false;

            for (int pointIndex = 0; pointIndex < (this.StartPoints.Length - 1); pointIndex++)
            {
                PointF? point1 = this.StartPoints[pointIndex];
                PointF? point4 = this.StartPoints[pointIndex + 1];
                PointF? point2 = this.EndPoints[pointIndex];
                PointF? point3 = this.EndPoints[pointIndex + 1];

                if (point1 == null || point2 == null || point3 == null || point4 == null)
                    continue;

                bool result = StiPointHelper.IsPointInPolygon(new PointF(x, y), new PointF[] { point1.Value, point4.Value, point3.Value, point2.Value });
                if (result)
                    return true;
            }
            return false;
        }

        internal static RectangleF GetClientRectangle(PointF?[] startPoints, PointF?[] endPoints)
        {
            if (startPoints == null || startPoints.Length == 0 || endPoints == null || endPoints.Length == 0)
                return RectangleF.Empty;

            PointF minPoint = PointF.Empty;
            PointF maxPoint = PointF.Empty;
            foreach (PointF? point in startPoints)
            {
                if (point == null) continue;

                if (minPoint == PointF.Empty)
                {
                    minPoint = point.Value;
                    maxPoint = point.Value;
                }
                else
                {
                    minPoint.X = Math.Min(minPoint.X, point.Value.X);
                    minPoint.Y = Math.Min(minPoint.Y, point.Value.Y);

                    maxPoint.X = Math.Max(maxPoint.X, point.Value.X);
                    maxPoint.Y = Math.Max(maxPoint.Y, point.Value.Y);
                }
            }

            foreach (PointF? point in endPoints)
            {
                if (point == null) continue;

                if (minPoint == PointF.Empty)
                {
                    minPoint = point.Value;
                    maxPoint = point.Value;
                }
                else
                {
                    minPoint.X = Math.Min(minPoint.X, point.Value.X);
                    minPoint.Y = Math.Min(minPoint.Y, point.Value.Y);

                    maxPoint.X = Math.Max(maxPoint.X, point.Value.X);
                    maxPoint.Y = Math.Max(maxPoint.Y, point.Value.Y);
                }
            }

            return new RectangleF(minPoint.X, minPoint.Y, maxPoint.X - minPoint.X, maxPoint.Y - minPoint.Y);
        }

        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            IStiStackedAreaSeries areaSeries = this.Series as IStiStackedAreaSeries;

            List<PointF[]> startList;
            List<PointF[]> endList;
            StiNullableDrawing.GetPointsList(this.StartPoints, this.EndPoints, out startList, out endList);

            int listIndex = 0;
            foreach (PointF[] newStartPoints in startList)
            {
                PointF[] newEndPoints = endList[listIndex];

                List<StiSegmentGeom> path = new List<StiSegmentGeom>();
                                
                path.Add(new StiLineSegmentGeom(newStartPoints[0], newEndPoints[0]));
                path.Add(new StiLinesSegmentGeom(newEndPoints));
                path.Add(new StiLineSegmentGeom(newEndPoints[newEndPoints.Length - 1], newStartPoints[newStartPoints.Length - 1]));

                // Для правильной заливки в Wpf меняем точки местами
                PointF[] revertStartPoint = new PointF[startPoints.Length];
                int index = 0;
                int index1 = newStartPoints.Length - 1;
                while (index < newStartPoints.Length)
                {
                    revertStartPoint[index] = newStartPoints[index1];
                    index++;
                    index1--;
                }

                path.Add(new StiLinesSegmentGeom(revertStartPoint));

                if (areaSeries.Brush != null)
                    context.FillPath(areaSeries.Brush, path, StiPathGeom.GetBoundsState, null);

                if (areaSeries.AllowApplyBrushNegative && areaSeries.BrushNegative != null)
                {
                    IStiAxisArea axisArea = this.Series.Chart.Area as IStiAxisArea;

                    float posY = axisArea.AxisCore.GetDividerY();

                    float width = (float)(axisArea.AxisCore.ScrollRangeX * axisArea.AxisCore.ScrollDpiX);
                    float height = (float)(axisArea.AxisCore.ScrollRangeY * axisArea.AxisCore.ScrollDpiY - posY);

                    RectangleF clipRect = new RectangleF(0, posY, width, height);
                    context.PushClip(clipRect);

                    context.FillPath(areaSeries.BrushNegative, path, StiPathGeom.GetBoundsState, null);

                    context.PopClip();
                }
                #region IsMouseOver
                if (IsMouseOver || Series.Core.IsMouseOver)
                {
                    context.FillPath(StiMouseOverHelper.GetMouseOverColor(), path, StiPathGeom.GetBoundsState, null);
                }
                #endregion

                listIndex++;
            }
        }
        #endregion

        public StiStackedAreaSeriesGeom(StiAreaGeom areaGeom, PointF?[] startPoints, PointF?[] endPoints, IStiSeries series)
            : base(areaGeom, series, GetClientRectangle(startPoints, endPoints))
        {
            this.startPoints = startPoints;
            this.endPoints = endPoints;
        }
    }
}
