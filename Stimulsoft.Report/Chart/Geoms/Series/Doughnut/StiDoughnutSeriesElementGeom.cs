﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Report.Chart.Geoms.Series;

namespace Stimulsoft.Report.Chart
{
    public class StiDoughnutSeriesElementGeom : StiSeriesElementGeom
    {
        #region Properties
        private List<StiSegmentGeom> path;
        public List<StiSegmentGeom> Path
        {
            get
            {
                return path;
            }
        }

        private List<StiSegmentGeom> pathLight;
        public List<StiSegmentGeom> PathLight
        {
            get
            {
                return pathLight;
            }
        }

        private List<StiSegmentGeom> pathDark;
        public List<StiSegmentGeom> PathDark
        {
            get
            {
                return pathDark;
            }
        }

        private Color borderColor;
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
        }

        private StiBrush brush;
        public StiBrush Brush
        {
            get
            {
                return brush;
            }
        }

        private StiBrush brushLight;
        public StiBrush BrushLight
        {
            get
            {
                return brushLight;
            }
        }

        private StiBrush brushDark;
        public StiBrush BrushDark
        {
            get
            {
                return brushDark;
            }
        }

        private float startAngle;
        public float StartAngle
        {
            get
            {
                return startAngle;
            }
        }

        private float endAngle;
        public float EndAngle
        {
            get
            {
                return endAngle;
            }
        }

        private float radiusFrom;
        public float RadiusFrom
        {
            get
            {
                return radiusFrom;
            }
        }

        private float radiusTo;
        public float RadiusTo
        {
            get
            {
                return radiusTo;
            }
        }

        private TimeSpan? beginTime;
        public TimeSpan? BeginTime
        {
            get
            {
                return beginTime;
            }
        }
        #endregion

        #region Methods
        public override bool Contains(float x, float y)
        {
            if (Invisible) return false;

            PointF center = new PointF(this.ClientRectangle.X + this.ClientRectangle.Width / 2, this.ClientRectangle.Y + this.ClientRectangle.Height / 2);

            float dx = x - center.X;
            float dy = y - center.Y;
            float radius = (float)Math.Sqrt(dx * dx + dy * dy);

            if (radius < this.RadiusTo || radius > this.RadiusFrom) return false;

            float alpha = (float)(Math.Atan2(dy, dx) * 180 / Math.PI);
            if (alpha < 0) alpha += 360;

            return alpha >= StartAngle && alpha <= EndAngle;
        }

        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            RectangleF rectPie = this.ClientRectangle;

            StiPenGeom pen = new StiPenGeom(BorderColor);
            pen.Alignment = StiPenAlignment.Inset;

            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                var animation = new StiOpacityAnimation(TimeSpan.FromSeconds(1), beginTime);
                context.FillDrawAnimationPath(brush, pen, path, rectPie, this, animation, GetInteractionData());
            }
            else
            {
                context.PushSmoothingModeToAntiAlias();

                if (Path != null)
                {
                    context.FillPath(brush, Path, rectPie, GetInteractionData());

                    if (IsSelected)
                        context.FillPath(StiSelectedHelper.GetSelectedBrush(), Path, rectPie, null);

                    if (IsMouseOver || Series.Core.IsMouseOver)
                        context.FillPath(StiMouseOverHelper.GetMouseOverColor(), Path, rectPie, null);
                }

                #region Draw light
                if (PathLight != null)
                    context.FillPath(BrushLight, PathLight, rectPie, null);

                if (PathDark != null)
                    context.FillPath(BrushDark, PathDark, rectPie, null);
                #endregion

                context.DrawPath(pen, path, StiPathGeom.GetBoundsState);
                context.PopSmoothingMode();
            }
        }
        #endregion

        public StiDoughnutSeriesElementGeom(StiAreaGeom areaGeom, double value, int index, 
            IStiDoughnutSeries series, RectangleF clientRectangle,
            List<StiSegmentGeom> path, List<StiSegmentGeom> pathLight, List<StiSegmentGeom> pathDark,
            Color borderColor, StiBrush brush, StiBrush brushLight, StiBrush brushDark,
            float startAngle, float endAngle, float radiusFrom, float radiusTo, TimeSpan? beginTime)
            : base(areaGeom, value, index, series, clientRectangle)
        {
            this.path = path;
            this.pathLight = pathLight;
            this.pathDark = pathDark;
            this.borderColor = borderColor;
            this.brush = brush;
            this.brushLight = brushLight;
            this.brushDark = brushDark;
            this.startAngle = startAngle;
            this.endAngle = endAngle;
            this.radiusFrom = radiusFrom;
            this.radiusTo = radiusTo;

            this.beginTime = beginTime;
        }
    }
}
