﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.Collections.Generic;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;

namespace Stimulsoft.Report.Chart
{
    public class StiSteppedAreaSeriesGeom : StiSteppedLineSeriesGeom
    {
        #region Methods
        public override bool Contains(float x, float y)
        {
            IStiAxisArea axisArea = this.Series.Chart.Area as IStiAxisArea;
            float posY = axisArea.AxisCore.GetDividerY();

            if (Invisible) return false;

            int pointIndex = 0;
            foreach (PointF? point in Points)
            {
                if (Points.Length <= pointIndex + 1) continue;

                PointF? nextPoint = Points[pointIndex + 1];

                if (point == null || nextPoint == null) continue;

                RectangleF pointRect = posY > point.Value.Y ?
                    new RectangleF(point.Value.X, point.Value.Y, nextPoint.Value.X - point.Value.X, posY - point.Value.Y) :
                    new RectangleF(point.Value.X, posY, nextPoint.Value.X - point.Value.X, point.Value.Y - posY);

                if (pointRect.Contains(x, y))
                    return true;

                pointIndex++;
                if (pointIndex == Points.Length - 1)
                    break;
            }
            return false;
        }

        private List<PointF[]> GetSteppedPoints(PointF?[] currentPoints)
        {
            var areaSeries = this.Series as IStiSteppedAreaSeries;

            var list = new List<PointF?>();

            float distX = (float)((IStiAxisArea)Series.Chart.Area).XAxis.Info.Dpi / 2;

            for (int index = 0; index < currentPoints.Length - 1; index++)
            {
                var point = currentPoints[index];
                var nextPoint = currentPoints[index + 1];

                if (point != null && nextPoint != null)
                {
                    if (areaSeries.PointAtCenter)
                    {
                        point = new PointF(point.Value.X - distX, point.Value.Y);
                        nextPoint = new PointF(nextPoint.Value.X - distX, nextPoint.Value.Y);
                    }


                    list.Add(point);
                    list.Add(new PointF(nextPoint.Value.X, point.Value.Y));
                }
                else
                {
                    list.Add(null);
                    list.Add(null);
                }
            }

            PointF? pos = currentPoints[currentPoints.Length - 1];

            if (pos != null)
            {
                if (areaSeries.PointAtCenter)
                {
                    pos = new PointF(pos.Value.X - distX, pos.Value.Y);

                    list.Add(pos);

                    pos = new PointF(pos.Value.X + distX * 2, pos.Value.Y);
                    list.Add(pos);
                }
                else list.Add(pos);
            }
            else list.Add(null);

            return StiNullableDrawing.GetPointsList(list.ToArray());
        }

        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            var areaSeries = this.Series as IStiSteppedAreaSeries;

            var chart = this.Series.Chart as StiChart;

            var axisArea = this.Series.Chart.Area as IStiAxisArea;

            float posY = axisArea.AxisCore.GetDividerY();
            
            var newPoints = GetSteppedPoints(Points);
            var newPointsFrom = this.PointsFrom != null ? GetSteppedPoints(this.PointsFrom) : null;

            if (chart.IsAnimationChangingValues && newPoints.Count > 0)
            {
                var curPoints = newPoints[0];
                var curPointsFrom = newPointsFrom[0];

                var path = new List<StiSegmentGeom>();
                
                var lineSegment1 = new StiLineSegmentGeom(new PointF(curPoints[0].X, posY), curPoints[0]);
                var lineSegments = new StiLinesSegmentGeom(curPoints);
                var lineSegment2 = new StiLineSegmentGeom(curPoints[curPoints.Length - 1], new PointF(curPoints[curPoints.Length - 1].X, posY));

                path.Add(lineSegment1);
                path.Add(lineSegments);
                path.Add(lineSegment2);

                lineSegment1.Animation = new StiPointsAnimation(new PointF[] { new PointF(curPointsFrom[0].X, posY), curPointsFrom[0] }, StiChartHelper.GlobalDurationElement, StiChartHelper.GlobalBeginTimeElement);
                lineSegments.Animation = new StiPointsAnimation(curPointsFrom, StiChartHelper.GlobalDurationElement, StiChartHelper.GlobalBeginTimeElement);
                lineSegment2.Animation = new StiPointsAnimation(new PointF[] { curPointsFrom[curPointsFrom.Length - 1], new PointF(curPointsFrom[curPointsFrom.Length - 1].X, posY) }, StiChartHelper.GlobalDurationElement, StiChartHelper.GlobalBeginTimeElement);

                context.FillDrawAnimationPath(areaSeries.Brush, null, path, StiPathGeom.GetBoundsState, null, null, null);

            }
            else
            {

                foreach (PointF[] newPoints2 in newPoints)
                {
                    var path = new List<StiSegmentGeom>();

                    path.Add(new StiLineSegmentGeom(new PointF(newPoints2[0].X, posY), newPoints2[0]));
                    path.Add(new StiLinesSegmentGeom(newPoints2));
                    path.Add(new StiLineSegmentGeom(newPoints2[newPoints2.Length - 1], new PointF(newPoints2[newPoints2.Length - 1].X, posY)));

                    if (areaSeries.Brush != null)
                    {
                        if (chart.IsAnimation)
                        {
                            var animation = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, StiChartHelper.GlobalBeginTimeElement);
                            context.FillDrawAnimationPath(areaSeries.Brush, null, path, StiPathGeom.GetBoundsState, null, animation, null);
                        }
                        else
                            context.FillPath(areaSeries.Brush, path, StiPathGeom.GetBoundsState, null);
                    }

                    if (areaSeries.AllowApplyBrushNegative && areaSeries.BrushNegative != null)
                    {
                        if (chart.IsAnimation)
                        {
                            var animation = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, StiChartHelper.GlobalBeginTimeElement);
                            context.FillDrawAnimationPath(areaSeries.BrushNegative, null, path, StiPathGeom.GetBoundsState, null, animation, null);
                        }
                        else
                        {
                            float width = (float)(axisArea.AxisCore.ScrollRangeX * axisArea.AxisCore.ScrollDpiX);
                            float height = (float)(axisArea.AxisCore.ScrollRangeY * axisArea.AxisCore.ScrollDpiY - posY);

                            RectangleF clipRect = new RectangleF(0, posY, width, height);
                            context.PushClip(clipRect);

                            context.FillPath(areaSeries.BrushNegative, path, StiPathGeom.GetBoundsState, null);

                            context.PopClip();
                        }
                    }

                    #region IsMouseOver
                    if (IsMouseOver || Series.Core.IsMouseOver)
                    {
                        context.FillPath(StiMouseOverHelper.GetMouseOverColor(), path, StiPathGeom.GetBoundsState, null);
                    }
                    #endregion
                }
            }
        }
        #endregion

        public StiSteppedAreaSeriesGeom(StiAreaGeom areaGeom, PointF?[] pointsFrom, PointF?[] points, IStiSeries series)
            : base(areaGeom, pointsFrom, points, series)
        {
        }
    }
}
