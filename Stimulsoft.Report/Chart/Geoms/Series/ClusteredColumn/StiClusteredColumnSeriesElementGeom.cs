﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Report.Chart.Geoms.Series;

namespace Stimulsoft.Report.Chart
{
    public class StiClusteredColumnSeriesElementGeom : StiSeriesElementGeom
    {
        #region Properties
        private StiBrush seriesBrush;
        public StiBrush SeriesBrush
        {
            get
            {
                return seriesBrush;
            }
        }

        private Color seriesBorderColor;
        public Color SeriesBorderColor
        {
            get
            {
                return seriesBorderColor;
            }
        }
        
        private StiAnimation animation;
        public StiAnimation Animation
        {
            get
            {
                return animation;
            }
        }  
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                #region Draw Column
                var columnRect = this.ClientRectangle;
                var pen = new StiPenGeom(SeriesBorderColor, 1);
                
                if (Series.ShowShadow && columnRect.Width > 1)
                {
                    var animationOpacity = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, new TimeSpan(animation.BeginTime.Value.Ticks + animation.Duration.Ticks));
                    context.DrawShadowRect(columnRect, 5, animationOpacity);
                }
                
                context.DrawAnimationColumn(seriesBrush, pen, columnRect, Value, this.GetToolTip(), this, animation, GetInteractionData());
                #endregion
            }
            else
            {
                base.Draw(context);

                var rect = this.ClientRectangle;

                #region Draw Shadow
                if (Series.ShowShadow && rect.Width > 4 && rect.Height > 4)
                {
                    if (Value > 0)
                    {
                        context.DrawCachedShadow(
                            rect,
                            StiShadowSides.Top |
                            StiShadowSides.Right,
                            context.Options.IsPrinting);
                    }
                    else if (Value < 0)
                    {
                        context.DrawCachedShadow(
                            new RectangleF(
                            rect.X,
                            rect.Y - 8,
                            rect.Width,
                            rect.Height + 8),
                            StiShadowSides.Right |
                            StiShadowSides.Edge |
                            StiShadowSides.Bottom |
                            StiShadowSides.Left,
                            context.Options.IsPrinting);
                    }
                }
                #endregion

                #region Draw Column
                StiPenGeom pen = new StiPenGeom(SeriesBorderColor, 1);
                Series.Chart.Style.Core.FillColumn(context, rect, seriesBrush, GetInteractionData());

                if (IsSelected)
                {
                    context.FillRectangle(StiSelectedHelper.GetSelectedBrush(), rect.X, rect.Y, rect.Width, rect.Height, null);
                }

                if (IsMouseOver || Series.Core.IsMouseOver)
                {
                    context.FillRectangle(StiMouseOverHelper.GetMouseOverColor(), rect.X, rect.Y, rect.Width, rect.Height, null);
                }

                #region Draw Column Border
                PointF[] points = null;

                if (Value > 0)
                {

                    points = new PointF[]
								    {
									    new PointF(rect.X, rect.Bottom), 
									    new PointF(rect.X, rect.Y),
									    new	PointF(rect.Right, rect.Y),
									    new PointF(rect.Right, rect.Bottom)
								    };
                }
                else
                {
                    points = new PointF[]
									{
										new PointF(rect.X, rect.Y), 
										new PointF(rect.X, rect.Bottom),
										new PointF(rect.Right, rect.Bottom),
										new PointF(rect.Right, rect.Y)
									};
                }
                context.DrawLines(pen, points);

                #endregion
                #endregion
            }
        }
        #endregion

        public StiClusteredColumnSeriesElementGeom(StiAreaGeom areaGeom, double value, int index,
            StiBrush seriesBrush, Color seriesBorderColor, IStiSeries series, RectangleF columnRect, StiAnimation animation)
            : base(areaGeom, value, index, series, columnRect)
        {
            this.seriesBrush = seriesBrush;
            this.seriesBorderColor = seriesBorderColor;

            this.animation = animation;
        }
    }
}
