﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiLineSeriesGeom : StiBaseLineSeriesGeom
    {        
        #region Methods
        public override bool Contains(float x, float y)
        {
            if (Invisible) return false;

            for (int pointIndex = 0; pointIndex < (this.Points.Length - 1); pointIndex++)
            {
                PointF? point1 = this.Points[pointIndex];
                PointF? point2 = this.Points[pointIndex + 1];

                if (point1 == null || point2 == null)
                    continue;

                bool result = StiPointHelper.IsLineContainsPoint(point1.Value, point2.Value, StiMouseOverHelper.MouseOverLineDistance, new PointF(x, y));
                if (result)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            var coreLineStyle = StiPenStyle.Solid;
            float lineWidth = 1f;
            var coreLineColor = Color.Black;
            bool showShadow = true;
            IStiLineMarker lineMarker = null;
            bool lighting = true;

            #region IStiBaseLineSeries
            var lineSeries = this.Series as IStiBaseLineSeries;
            if (lineSeries != null)
            {
                coreLineStyle = lineSeries.LineStyle;
                lineWidth = lineSeries.LineWidth;
                coreLineColor = lineSeries.LineColor;
                showShadow = lineSeries.ShowShadow;
                lineMarker = lineSeries.LineMarker;
                lighting = lineSeries.Lighting;
            }
            #endregion

            #region IStiRadarLineSeries
            IStiRadarLineSeries radarLineSeries = this.Series as IStiRadarLineSeries;
            if (radarLineSeries != null)
            {
                coreLineStyle = radarLineSeries.LineStyle;
                lineWidth = radarLineSeries.LineWidth;
                coreLineColor = radarLineSeries.LineColor;
                showShadow = radarLineSeries.ShowShadow;
                lighting = radarLineSeries.Lighting;
            }
            #endregion

            #region IStiRadarAreaSeries
            IStiRadarAreaSeries radarAreaSeries = this.Series as IStiRadarAreaSeries;
            if (radarAreaSeries != null)
            {
                coreLineStyle = radarAreaSeries.LineStyle;
                lineWidth = radarAreaSeries.LineWidth;
                coreLineColor = radarAreaSeries.LineColor;
                showShadow = radarAreaSeries.ShowShadow;
                lighting = radarAreaSeries.Lighting;
            }
            #endregion

            context.PushSmoothingModeToAntiAlias();
            
            StiPenStyle dashStyle = coreLineStyle;
            float scaledLineWidth = lineWidth * context.Options.Zoom;

            StiPenGeom pen = new StiPenGeom(coreLineColor, scaledLineWidth);
            pen.PenStyle = dashStyle;

            var chart = this.Series.Chart as StiChart;

            #region showShadow
            if (showShadow)
            {
                StiPenGeom penShadow = new StiPenGeom(Color.FromArgb(50, 0, 0, 0), scaledLineWidth + 0.5f * context.Options.Zoom);
                penShadow.PenStyle = dashStyle;

                context.PushTranslateTransform(scaledLineWidth, scaledLineWidth);
                StiNullableDrawing.DrawLines(context, penShadow, PointsFrom, Points, chart.IsAnimation);

                if (lineMarker != null && lineMarker.Visible)
                {
                    StiBrush brushShadow = new StiSolidBrush(Color.FromArgb(50, 0, 0, 0));

                    lineMarker.Core.DrawLines(context, Points, (float)context.Options.Zoom,
                        brushShadow, null, lineMarker.Type, 
                        (float)lineMarker.Step, (float)lineMarker.Size, lineMarker.Angle);

                }
                context.PopTransform();
            }
            #endregion

            #region IsMouseOver
            if (Series.Core.IsMouseOver)
            {                
                float zoom = context.Options.Zoom;
                float pointSize = 11 + lineWidth;
                foreach (PointF? point in Points)
                {
                    if (point == null) continue;
                    RectangleF pointRect = new RectangleF(point.Value.X - pointSize / 2 * zoom, point.Value.Y - pointSize / 2 * zoom, pointSize * zoom, pointSize * zoom);
                    context.FillEllipse(StiMouseOverHelper.GetLineMouseOverColor(), pointRect, null);
                }

                StiPenGeom penMouseOver = new StiPenGeom(StiMouseOverHelper.GetLineMouseOverColor(), (4 + lineWidth) * context.Options.Zoom);
                penMouseOver.StartCap = StiPenLineCap.Round;
                penMouseOver.EndCap = StiPenLineCap.Round;
                StiNullableDrawing.DrawLines(context, penMouseOver, PointsFrom, Points, chart.IsAnimation);
            }
            #endregion

            if ((this.Series is StiLineSeries) && ((StiLineSeries)(this.Series)).AllowApplyColorNegative)
            {
                Color coreLineColorNegative = ((StiLineSeries)this.Series).LineColorNegative;

                StiPenGeom penNegative = new StiPenGeom(coreLineColorNegative, scaledLineWidth);
                penNegative.PenStyle = dashStyle;

                IStiAxisArea axisArea = this.Series.Chart.Area as IStiAxisArea;
                float posY = axisArea.AxisCore.GetDividerY();

                List<PointF?> pointsNegative = new List<PointF?>();
                List<PointF?> pointsPositive = new List<PointF?>();

                for (int index = 0; index < Points.Length; index++)
                {
                    PointF? point = Points[index];
                    PointF? pointNext = (index != (Points.Length - 1)) ? Points[index + 1] : null;

                    if (point.Value.Y > posY)
                    {
                        #region Negative Line
                        pointsNegative.Add(point);
                        if (pointNext == null ||
                            pointNext.Value.Y < posY ||
                            pointNext.Value.Y == posY && ((index + 2) < Points.Length) && ((Points[index + 2].Value.Y) <= posY))
                        {
                            if (pointNext != null)
                            {
                                PointF? point0 = GetPointCross(point, Points[index + 1], posY);
                                pointsNegative.Add(point0);

                                if (pointNext.Value.Y == posY && ((index + 2) < Points.Length) && ((Points[index + 2].Value.Y) <= posY))
                                {
                                    pointsNegative.Add(Points[index + 2]);
                                }

                                pointsPositive.Add(point0);
                            }

                            DrawLine(context, penNegative, pointsNegative);

                            #region Draw Light
                            if (scaledLineWidth >= 2 * context.Options.Zoom && lighting)
                            {
                                float step = 0.5f * context.Options.Zoom;
                                context.PushTranslateTransform(-step, -step);
                                StiPenGeom penLight = new StiPenGeom(StiColorUtils.Light(coreLineColorNegative, 70), scaledLineWidth);
                                penLight.PenStyle = dashStyle;

                                DrawLine(context, penLight, pointsNegative);

                                context.PopTransform();
                            }
                            #endregion

                            pointsNegative.Clear();
                        }
                        #endregion
                    }
                    else
                    {
                        #region Positive Line
                        pointsPositive.Add(point);
                        if (pointNext == null || pointNext.Value.Y > posY)
                        {
                            if (pointNext != null)
                            {
                                PointF? point0 = GetPointCross(point, Points[index + 1], posY);

                                pointsNegative.Add(point0);
                                pointsPositive.Add(point0);
                                pointsPositive.Add(pointNext);
                            }

                            DrawLine(context, pen, pointsPositive);

                            #region Draw Light
                            if (scaledLineWidth >= 2 * context.Options.Zoom && lighting)
                            {
                                float step = 0.5f * context.Options.Zoom;
                                context.PushTranslateTransform(-step, -step);
                                StiPenGeom penLight = new StiPenGeom(StiColorUtils.Light(coreLineColor, 70), scaledLineWidth);
                                penLight.PenStyle = dashStyle;

                                DrawLine(context, penLight, pointsPositive);

                                context.PopTransform();
                            }
                            #endregion

                            pointsPositive.Clear();
                        }
                        #endregion
                    }
                }
            }
            else
            {
                StiNullableDrawing.DrawLines(context, pen, PointsFrom, Points, chart.IsAnimation);

                #region Draw Light
                if (scaledLineWidth >= 2 * context.Options.Zoom && lighting)
                {
                    float step = 0.5f * context.Options.Zoom;
                    context.PushTranslateTransform(-step, -step);
                    StiPenGeom penLight = new StiPenGeom(StiColorUtils.Light(coreLineColor, 70), scaledLineWidth);
                    penLight.PenStyle = dashStyle;
                    StiNullableDrawing.DrawLines(context, penLight, PointsFrom, Points, chart.IsAnimation);

                    context.PopTransform();
                }
                #endregion
            }            

            if (lineMarker != null && lineMarker.Visible)
            {
                StiPenGeom borderPen = new StiPenGeom(lineMarker.BorderColor);
                lineMarker.Core.DrawLines(context, Points, context.Options.Zoom, lineMarker.Brush, borderPen, lineMarker.Type,
                    (float)lineMarker.Step, lineMarker.Size, lineMarker.Angle);
            }

            context.PopSmoothingMode();
        }

        private PointF GetPointCross(PointF? point1, PointF? point2, float posY)
        {
            float y1 = point1.Value.Y;
            float x1 = point1.Value.X;

            float y2 = point2.Value.Y;
            float x2 = point2.Value.X;

            double x0 = (Math.Tan(Math.Atan((x2 - x1) / (y1 - y2)))) * (y1 - posY) + x1;
            return new PointF((float)x0, posY);
        }

        private void DrawLine(StiContext context, StiPenGeom pen, List<PointF?> listPoints)
        {
            var chart = this.Series.Chart as StiChart;

            StiNullableDrawing.DrawLines(context, pen, null, listPoints.ToArray(), chart.IsAnimation);
        }
        #endregion

        public StiLineSeriesGeom(StiAreaGeom areaGeom, PointF?[] pointsFrom, PointF?[] points, IStiSeries series)
            : base(areaGeom, pointsFrom, points, series)
        {
        }
    }
}
