﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiSeriesElementGeom : StiCellGeom, IStiSeriesElement
    {
        #region IStiGeomInteraction override
        public override void InvokeMouseEnter(StiInteractionOptions options)
        {
            if (!AllowMouseOver) return;

            if (!IsMouseOver)
            {
                IsMouseOver = true;
                options.UpdateContext = series.Interaction.DrillDownEnabled;
            }
            
            int valueIndex = GetValueIndex();

            options.InteractionToolTip = GetToolTip(valueIndex);
            options.InteractionHyperlink = GetHyperlink(valueIndex);
        }
               
        
        public override void InvokeMouseLeave(StiInteractionOptions options)
        {
            if (!AllowMouseOver) return;

            if (IsMouseOver)
            {
                IsMouseOver = false;
                options.UpdateContext = series.Interaction.DrillDownEnabled;
            }
        }

        public override void InvokeClick(StiInteractionOptions options)
        {
            int valueIndex = GetValueIndex();

            if (Series.Hyperlinks != null && valueIndex < Series.Hyperlinks.Length)
            {
                options.InteractionHyperlink = series.Hyperlinks[valueIndex];
            }

            if (Series.Interaction.DrillDownEnabled)
            {
                options.SeriesInteractionData = this.Interaction;

                IsMouseOver = false;
                options.UpdateContext = series.Interaction.DrillDownEnabled;
            }

            this.IsSelected = !this.IsSelected;
        }

        private int GetValueIndex()
        {
            int valueIndex = this.Index;

            if (this.Series is IStiClusteredBarSeries && !(this.Series is IStiGanttSeries) ||
                this.Series is IStiStackedBarSeries ||
                this.Series is IStiFullStackedBarSeries)
            {
                if (this.Series.Chart.Area is IStiAxisArea && !((IStiAxisArea)this.Series.Chart.Area).ReverseVert)
                    valueIndex = Series.Values.Length - valueIndex - 1;
            }
            else
            {
                if (this.Series.Chart.Area is IStiAxisArea && ((IStiAxisArea)this.Series.Chart.Area).ReverseHor)
                    valueIndex = Series.Values.Length - valueIndex - 1;
            }
            return valueIndex;
        }

        internal string GetHyperlink()
        {
            return GetHyperlink(GetValueIndex());
        }

        private string GetHyperlink(int valueIndex)
        {
            if (Series.Hyperlinks != null && valueIndex < Series.Hyperlinks.Length)
                return series.Hyperlinks[valueIndex];
            else
                return null;
        }

        internal string GetToolTip()
        {
            var valueIndex = GetValueIndex();

            if (Series.ToolTips != null && valueIndex < Series.ToolTips.Length)
                return series.ToolTips[valueIndex];
            else
                return null;
        }

        private string GetToolTip(int valueIndex)
        {
            if (Series.ToolTips != null && valueIndex < Series.ToolTips.Length)
                return series.ToolTips[valueIndex];
            else
                return null;
        }

        public virtual bool AllowMouseOver
        {
            get
            {
                int index = GetValueIndex();
                return
                    ((Series.Hyperlinks != null && index < Series.Hyperlinks.Length) ||
                    (Series.ToolTips != null && index < Series.ToolTips.Length)) || 
                    (this.Series.Interaction.DrillDownEnabled && this.Series.Interaction.AllowSeriesElements);
            }
        }

        public virtual bool IsMouseOver
        {
            get
            {
                return this.Series.Core.GetIsMouseOverSeriesElement(this.Index);
            }
            set
            {
                this.Series.Core.SetIsMouseOverSeriesElement(this.Index, value);
            }
        }

        internal bool IsSelected
        {
            get
            {
                if (!((StiChart)this.series.Chart).Page.IsDashboard) return false;

                return this.Series.Core.GetIsSelectedSeriesElement(this.Index);
            }
            set
            {
                this.Series.Core.SetIsSelectedSeriesElement(this.Index, value);
            }
        }
        #endregion

        #region Properties
        private double value;
        public double Value
        {
            get
            {
                return value;
            }
        }

        private int index;
        public int Index
        {
            get
            {
                return index;
            }
        }

        private IStiSeries series;
        public IStiSeries Series
        {
            get
            {
                return series;
            }
        }

        private StiSeriesInteractionData interaction;
        public StiSeriesInteractionData Interaction
        {
            get
            {
                return interaction;
            }
            set
            {
                interaction = value;
            }
        }

        private StiAreaGeom areaGeom;
        public StiAreaGeom AreaGeom
        {
            get
            {
                return areaGeom;
            }
            set
            {
                areaGeom = value;
            }
        }

        private string elementIndex;
        public string ElementIndex
        {
            get
            {
                return elementIndex;
            }
            set
            {
                elementIndex = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            RectangleF rect = this.ClientRectangle;
            
            //Red line
            //context.DrawRectangle(new StiPenGeom(Color.Blue), rect.X, rect.Y, rect.Width, rect.Height);
        }


        internal StiInteractionDataGeom GetInteractionData()
        {
            var chart = this.Series.Chart as StiChart;

            var interaction = 
                new StiInteractionDataGeom()
                {
                    ComponentName = chart.Name,
                    ComponentIndex = chart.Page.Components.IndexOf(chart).ToString(),
                    PageGuid = ((StiSeries)this.Series).DrillDownPageGuid,
                    PageIndex = chart.Page.Report.RenderedPages.IndexOf(chart.Page).ToString(),
                    ElementIndex = this.ElementIndex.ToString()
                };

            return interaction;
        }
        #endregion

        public StiSeriesElementGeom(StiAreaGeom areaGeom, double value, int index, IStiSeries series, RectangleF clientRectangle)
            : base(clientRectangle)
        {
            this.areaGeom = areaGeom;
            this.series = series;
            this.value = value;
            this.index = index;
        }
    }
}
