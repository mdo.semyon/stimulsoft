﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Report.Chart.Geoms.Series;

namespace Stimulsoft.Report.Chart
{
    public class StiStackedBarSeriesElementGeom : StiSeriesElementGeom
    {
        #region Properties
        private StiBrush seriesBrush;
        public StiBrush SeriesBrush
        {
            get
            {
                return seriesBrush;
            }
        }

        private Color seriesBorderColor;
        public Color SeriesBorderColor
        {
            get
            {
                return seriesBorderColor;
            }
        }

        private TimeSpan? beginTime;
        public TimeSpan? BeginTime
        {
            get
            {
                return beginTime;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            RectangleF rectColumn = this.ClientRectangle;

            StiPenGeom pen = new StiPenGeom(SeriesBorderColor, 1);

            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                if (Series.ShowShadow)
                {
                    var animationOpacityShadow = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, new TimeSpan(beginTime.Value.Ticks + StiChartHelper.GlobalDurationElement.Ticks));
                    context.DrawShadowRect(rectColumn, 5, animationOpacityShadow);
                }
                
                var animationOpacity = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, beginTime);
                context.DrawAnimationBar(seriesBrush, pen, rectColumn, Value, this.GetToolTip(), this, animationOpacity, GetInteractionData());  
            }
            else
            {
                Series.Chart.Style.Core.FillColumn(context, rectColumn, seriesBrush, GetInteractionData());
                PointF[] points = null;

                if (IsSelected)
                    context.FillRectangle(StiSelectedHelper.GetSelectedBrush(), rectColumn.X, rectColumn.Y, rectColumn.Width, rectColumn.Height, null);

                if (IsMouseOver || Series.Core.IsMouseOver)
                {
                    context.FillRectangle(StiMouseOverHelper.GetMouseOverColor(), rectColumn.X, rectColumn.Y, rectColumn.Width, rectColumn.Height, null);
                }

                if (Value > 0)
                {
                    points = new PointF[]
									{												
										new PointF(rectColumn.Right, rectColumn.Y),
										new PointF(rectColumn.X, rectColumn.Y), 
										new PointF(rectColumn.X, rectColumn.Bottom),
										new	PointF(rectColumn.Right, rectColumn.Bottom)
									};
                }
                else
                {
                    points = new PointF[]
									{
										new PointF(rectColumn.X, rectColumn.Y),
										new PointF(rectColumn.Right, rectColumn.Y),
										new PointF(rectColumn.Right, rectColumn.Bottom),
										new PointF(rectColumn.X, rectColumn.Bottom)
									};
                }
                context.DrawLines(pen, points);
            }
        }
        #endregion

        public StiStackedBarSeriesElementGeom(StiAreaGeom areaGeom, double value, int index,
            StiBrush seriesBrush, Color seriesBorderColor, IStiSeries series, RectangleF clientRectangle, TimeSpan? beginTime)
            : base(areaGeom, value, index, series, clientRectangle)
        {
            this.seriesBrush = seriesBrush;
            this.seriesBorderColor = seriesBorderColor;

            this.beginTime = beginTime;
        }
    }
}
