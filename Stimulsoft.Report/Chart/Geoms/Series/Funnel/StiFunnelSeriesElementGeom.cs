﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Report.Chart.Geoms.Series;

namespace Stimulsoft.Report.Chart
{
    public class StiFunnelSeriesElementGeom : StiSeriesElementGeom
    {
        #region Properties
        private List<StiSegmentGeom> path;
        public List<StiSegmentGeom> Path
        {
            get
            {
                return path;
            }
        }

        private Color borderColor;
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
        }

        private StiBrush brush;
        public StiBrush Brush
        {
            get
            {
                return brush;
            }
        }

        private TimeSpan? beginTime;
        public TimeSpan? BeginTime
        {
            get
            {
                return beginTime;
            }
        }
        #endregion

        public override void Draw(StiContext context)
        {
            var rect = this.ClientRectangle;

            var pen = new StiPenGeom(StiColorUtils.Dark(borderColor, 10));
            pen.Alignment = StiPenAlignment.Inset;

            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                var animationOpacity = new StiOpacityAnimation(TimeSpan.FromSeconds(1), beginTime);
                context.DrawAnimationPathElement(Brush, pen, path, rect, this.GetToolTip(), this, animationOpacity, GetInteractionData());
            }
            else
            {
                context.PushSmoothingModeToAntiAlias();

                #region Draw Funnel Element Border
                if (Series.ShowShadow)
                {
                    context.PushTranslateTransform(4, 4);
                    context.FillPath(Color.FromArgb(50, 100, 100, 100), Path, rect, null);
                    context.PopTransform();
                }
                #endregion

                #region Draw Funnel Element

                context.FillPath(Brush, Path, rect, GetInteractionData());
                if (IsSelected)
                    context.FillRectangle(StiSelectedHelper.GetSelectedBrush(), rect.X, rect.Y, rect.Width, rect.Height, null);

                if (IsMouseOver || Series.Core.IsMouseOver)
                    context.FillPath(StiMouseOverHelper.GetMouseOverColor(), Path, rect, null);
                #endregion

                #region Draw Funnel Element Border
                if (!Color.Transparent.Equals(BorderColor))
                    context.DrawPath(pen, path, null);
                #endregion

                context.PopSmoothingMode();
            }
            
        }
        public StiFunnelSeriesElementGeom(StiAreaGeom areaGeom, double value, int index,
            IStiSeries series, RectangleF clientRectangle, StiBrush brush, Color borderColor, List<StiSegmentGeom> path, TimeSpan? beginTime)
            : base(areaGeom, value, index, series, clientRectangle)
        {
            this.path = path;
            this.borderColor = borderColor;
            this.brush = brush;

            this.beginTime = beginTime;
        }
    }
}
