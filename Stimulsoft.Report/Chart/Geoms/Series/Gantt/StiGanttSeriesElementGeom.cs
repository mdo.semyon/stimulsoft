﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Report.Chart.Geoms.Series;

namespace Stimulsoft.Report.Chart
{
    public class StiGanttSeriesElementGeom : StiSeriesElementGeom
    {
        #region Properties
        private TimeSpan? beginTime;
        public TimeSpan? BeginTime
        {
            get
            {
                return beginTime;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            IStiGanttSeries currSeries = Series as IStiGanttSeries;
            RectangleF rectColumn = this.ClientRectangle;

            var chart = this.Series.Chart as StiChart;

            var brush = currSeries.ProcessSeriesBrushes(this.Index, currSeries.Brush);

            if (chart.IsAnimation)
            {
                var pen = new StiPenGeom(currSeries.BorderColor);
                
                var animationOpacity = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, beginTime);                                
                context.DrawAnimationBar(brush, pen, rectColumn, Value, this.GetToolTip(), this, animationOpacity, GetInteractionData());
            }
            else
            {
                base.Draw(context);

                #region Draw Shadow
                if (currSeries.ShowShadow && rectColumn.Width > 4 && rectColumn.Height > 4)
                {
                    context.DrawCachedShadow(rectColumn, StiShadowSides.All, context.Options.IsPrinting);
                }
                #endregion

                #region Draw Series
                StiPenGeom pen = new StiPenGeom(currSeries.BorderColor);
                context.FillRectangle(brush, rectColumn.X, rectColumn.Y, rectColumn.Width, rectColumn.Height, GetInteractionData());

                if (IsSelected)
                    context.FillRectangle(StiSelectedHelper.GetSelectedBrush(), rectColumn.X, rectColumn.Y, rectColumn.Width, rectColumn.Height, null);

                if (IsMouseOver || Series.Core.IsMouseOver)
                {
                    context.FillRectangle(StiMouseOverHelper.GetMouseOverColor(), rectColumn.X, rectColumn.Y, rectColumn.Width, rectColumn.Height, null);
                }
                context.DrawRectangle(pen, rectColumn.X, rectColumn.Y, rectColumn.Width, rectColumn.Height);
                #endregion
            }
        }
        #endregion

        public StiGanttSeriesElementGeom(StiAreaGeom areaGeom, double value, int index,
            IStiSeries series, RectangleF clientRectangle, TimeSpan? beginTime)
            : base(areaGeom, value, index, series, clientRectangle)
        {
            this.beginTime = beginTime;
        }
    }
}
