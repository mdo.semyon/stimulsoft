﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiPieSeriesFullElementGeom : StiCellGeom
    {
        #region Properties
        private IStiPieSeries series;
        public IStiPieSeries Series
        {
            get
            {
                return series;
            }
        }

        private StiBrush brush;
        public StiBrush Brush
        {
            get
            {
                return brush;
            }
        }

        private Color borderColor;
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            RectangleF rectPie = this.ClientRectangle;

            StiPenGeom pen = new StiPenGeom(BorderColor);

            context.FillEllipse(Brush, rectPie, null);
            context.DrawEllipse(pen, rectPie);
            
        }
        #endregion

        public StiPieSeriesFullElementGeom(IStiPieSeries series, RectangleF clientRectangle, StiBrush brush, Color borderColor)
            : base(clientRectangle)
        {
            this.series = series;
            this.brush = brush;
            this.borderColor = borderColor;
        }
    }
}
