﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Report.Chart.Geoms.Series;

namespace Stimulsoft.Report.Chart
{
    public class StiPieSeriesElementGeom : StiSeriesElementGeom
    {
        #region Properties
        private List<StiSegmentGeom> path;
        public List<StiSegmentGeom> Path
        {
            get
            {
                return path;
            }
        }

        private List<StiSegmentGeom> pathLight;
        public List<StiSegmentGeom> PathLight
        {
            get
            {
                return pathLight;
            }
        }

        private Color borderColor;
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
        }

        private StiBrush brush;
        public StiBrush Brush
        {
            get
            {
                return brush;
            }
        }

        private float startAngle;
        public float StartAngle
        {
            get
            {
                return startAngle;
            }
            set
            {
                startAngle = value;
            }
        }

        private float endAngle;
        public float EndAngle
        {
            get
            {
                return endAngle;
            }
            set
            {
                endAngle = value;
            }
        }

        private float radius;
        public float Radius
        {
            get
            {
                return radius;
            }
            set
            {
                radius = value;
            }
        }

        private StiAnimation animation;
        public StiAnimation Animation
        {
            get
            {
                return animation;
            }
            set
            {
                animation = value;
            }
        }
        #endregion

        #region Methods
        public override bool Contains(float x, float y)
        {
            if (Invisible) return false;
            
            PointF center = new PointF(this.ClientRectangle.X + this.ClientRectangle.Width / 2, this.ClientRectangle.Y + this.ClientRectangle.Height / 2);

            float dx = x - center.X;
            float dy = y - center.Y;
            float radius = (float)Math.Sqrt(dx * dx + dy * dy);

            if (radius >= this.Radius) return false;

            float alpha = (float)(Math.Atan2(dy, dx) * 180 / Math.PI);
            if (alpha < 0) alpha += 360;

            return alpha >= StartAngle && alpha <= EndAngle; 
        }

        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            var rectPie = this.ClientRectangle;

            var pen = new StiPenGeom(StiColorUtils.Dark(borderColor, 10))
            {
                Alignment = StiPenAlignment.Inset
            };

            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                #region Draw Pie Segment
                context.DrawAnimationPathElement(brush, pen, path, rectPie, this.GetToolTip(), this, this.animation, GetInteractionData());
                #endregion

                #region Render lights
                if (PathLight != null)
                {
                    var brLight = new StiSolidBrush(Color.FromArgb(30, Color.Black));
                    context.DrawAnimationPathElement(brLight, null, pathLight, rectPie, this.GetToolTip(), null, this.animation, GetInteractionData());
                }
                #endregion
            }
            else
            {
                context.PushSmoothingModeToAntiAlias();

                #region Draw Pie Segment
                context.FillPath(Brush, Path, rectPie, GetInteractionData());
                if (IsSelected)
                    context.FillPath(StiSelectedHelper.GetSelectedBrush(), Path, rectPie, null);
                if (IsMouseOver || Series.Core.IsMouseOver)
                    context.FillPath(StiMouseOverHelper.GetMouseOverColor(), Path, rectPie, null);
                #endregion

                #region Render lights
                if (PathLight != null)
                {
                    var brLight = new StiSolidBrush(Color.FromArgb(30, Color.Black));
                    context.FillPath(brLight, pathLight, rectPie, null);
                }
                #endregion

                #region Draw Pie Segment Border
                if (!Color.Transparent.Equals(BorderColor))
                    context.DrawPath(pen, path, null);
                #endregion

                context.PopSmoothingMode();
            }
        }
        #endregion

        public StiPieSeriesElementGeom(StiAreaGeom areaGeom, double value, int index, IStiPieSeries series, RectangleF clientRectangle,
            List<StiSegmentGeom> path, List<StiSegmentGeom> pathLight, Color borderColor, StiBrush brush, float startAngle, float endAngle,
            float radius, StiAnimation animation)
            : base(areaGeom, value, index, series, clientRectangle)
        {
            this.path = path;
            this.pathLight = pathLight;
            this.borderColor = borderColor;
            this.brush = brush;
            this.startAngle = startAngle;
            this.endAngle = endAngle;
            this.radius = radius;
            
            this.animation = animation;
        }
    }
}
