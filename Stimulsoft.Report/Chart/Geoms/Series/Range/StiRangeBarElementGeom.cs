﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Report.Chart.Geoms.Series;

namespace Stimulsoft.Report.Chart
{
    public class StiRangeBarElementGeom : StiSeriesElementGeom
    {
        #region Properties
        private TimeSpan? beginTime;
        public TimeSpan? BeginTime
        {
            get
            {
                return beginTime;
            }
        }
        #endregion

        #region Methods
        public override void Draw(StiContext context)
        {
            var currSeries = Series as IStiRangeBarSeries;
            var seriesRect = this.ClientRectangle;

            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                var pen = new StiPenGeom(currSeries.BorderColor);

                var valueStart = Series.ValuesStart.Length > Index ? Series.ValuesStart[Index] : 0;
                
                if (Series.ShowShadow)
                {
                    var animationOpacityShadow = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, new TimeSpan(beginTime.Value.Ticks + StiChartHelper.GlobalDurationElement.Ticks));
                    context.DrawShadowRect(seriesRect, 5, animationOpacityShadow);
                }
                
                var animationOpacity = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, beginTime);
                context.DrawAnimationColumn(currSeries.Brush, pen, seriesRect, Value, this.GetToolTip(), this, animationOpacity, GetInteractionData());
            }
            else
            {
                #region Draw Shadow
                if (currSeries.ShowShadow && seriesRect.Width > 4 && seriesRect.Height > 4)
                {
                    context.DrawCachedShadow(seriesRect, StiShadowSides.All, context.Options.IsPrinting);
                }
                #endregion

                #region Draw Series
                var pen = new StiPenGeom(currSeries.BorderColor);
                context.FillRectangle(currSeries.Brush, seriesRect.X, seriesRect.Y, seriesRect.Width, seriesRect.Height, GetInteractionData());

                if (IsSelected)
                    context.FillRectangle(StiSelectedHelper.GetSelectedBrush(), seriesRect.X, seriesRect.Y, seriesRect.Width, seriesRect.Height, null);

                if (IsMouseOver || Series.Core.IsMouseOver)
                {
                    context.FillRectangle(StiMouseOverHelper.GetMouseOverColor(), seriesRect.X, seriesRect.Y, seriesRect.Width, seriesRect.Height, null);
                }
                context.DrawRectangle(pen, seriesRect.X, seriesRect.Y, seriesRect.Width, seriesRect.Height);
                #endregion
            }
        }
        #endregion

        public StiRangeBarElementGeom(StiAreaGeom areaGeom, double value, int index,
            IStiSeries series, RectangleF clientRectangle, TimeSpan? beginTime)
            : base(areaGeom, value, index, series, clientRectangle)
        {
            this.beginTime = beginTime;
        }
    }
}
