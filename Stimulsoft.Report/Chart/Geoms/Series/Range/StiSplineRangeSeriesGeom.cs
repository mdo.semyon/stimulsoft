﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.Collections.Generic;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;

namespace Stimulsoft.Report.Chart
{
    public class StiSplineRangeSeriesGeom : StiSplineSeriesGeom
    {
        #region Properties
        private PointF?[] pointsEnd;
        public PointF?[] PointsEnd
        {
            get
            {
                return pointsEnd;
            }
            set
            {
                pointsEnd = value;
            }
        }

        #endregion

        #region Methods
        public override void Draw(StiContext context)
        {
            var list = StiNullableDrawing.GetPointsList(this.Points);
            var listEnd = StiNullableDrawing.GetPointsList(this.PointsEnd);
            
            if (list.Count < 1 || listEnd.Count < 1) return;

            var points = list[0];
            var pointsEnd = listEnd[0];            
            
            FillPath(context, points, pointsEnd);            
        }

        private void FillPath(StiContext context, PointF[] points, PointF[] pointsEnd)
        {
            var areaSeries = this.Series as IStiSplineRangeSeries;
            var brush = areaSeries.Brush;

            var path = new List<StiSegmentGeom>();

            var pointsEndBack = new PointF[pointsEnd.Length];

            int count = pointsEnd.Length;
            for (int index = 0; index < count; index++)
            {
                pointsEndBack[index] = pointsEnd[count - index - 1];
            }//for correct paint in WPF
            
            path.Add(new StiLineSegmentGeom(pointsEnd[0], points[0]));
            path.Add(new StiCurveSegmentGeom(points, areaSeries.Tension));
            path.Add(new StiLineSegmentGeom(points[points.Length - 1], pointsEnd[pointsEnd.Length - 1]));
            path.Add(new StiCurveSegmentGeom(pointsEndBack, areaSeries.Tension));

            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                var animation = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, StiChartHelper.GlobalBeginTimeElement);
                context.FillDrawAnimationPath(brush, null, path, StiPathGeom.GetBoundsState, null, animation, null);
            }
            else
                context.FillPath(brush, path, StiPathGeom.GetBoundsState, null);

            #region IsMouseOver
            if (IsMouseOver || Series.Core.IsMouseOver)
            {
                context.FillPath(StiMouseOverHelper.GetMouseOverColor(), path, StiPathGeom.GetBoundsState, null);
            }
            #endregion
        }
        #endregion

        public StiSplineRangeSeriesGeom(StiAreaGeom areaGeom, PointF?[] points, PointF?[] pointsEnd, IStiSeries series)
            : base(areaGeom, null, points, series)
        {
            this.pointsEnd = pointsEnd;
        }
    }
}
