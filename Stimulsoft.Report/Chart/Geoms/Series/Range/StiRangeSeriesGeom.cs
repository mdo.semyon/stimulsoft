﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;

namespace Stimulsoft.Report.Chart
{
    public class StiRangeSeriesGeom : StiLineSeriesGeom
    {
        #region Properties
        private PointF?[] pointsEnd;
        public PointF?[] PointsEnd
        {
            get
            {
                return pointsEnd;
            }
            set
            {
                pointsEnd = value;
            }
        }

        #endregion

        #region Methods
        public override void Draw(StiContext context)
        {
            var areaSeries = this.Series as IStiLineRangeSeries;

            var list = StiNullableDrawing.GetPointsList(this.Points);
            var listEnd = StiNullableDrawing.GetPointsList(this.PointsEnd);

            if (list.Count < 1 || listEnd.Count < 1) return;

            var points = list[0];
            var pointsEnd = listEnd[0];

            int count = Math.Min(points.Length, pointsEnd.Length);

            var pointsLine = new List<PointF>();
            var pointsLineEnd = new List<PointF>();

            for (int index = 0; index < count; index++)
            {
                #region Point and Point Next
                PointF? point = points[index];
                PointF? pointEnd = pointsEnd[index];

                PointF? pointNext = null;
                PointF? pointNextEnd = null;

                if (index != (count - 1))
                {
                    pointNext = points[index + 1];
                    pointNextEnd = pointsEnd[index + 1];
                }
                #endregion

                #region Cross point
                PointF? pointCross = null;

                if (pointNext != null || Intersection(point, pointEnd, pointNext, pointNextEnd))
                    pointCross = GetPointCross((PointF)point, (PointF)pointEnd, (PointF)pointNext, (PointF)pointNextEnd);
                #endregion
                
                pointsLine.Add((PointF)point);
                pointsLineEnd.Add((PointF)pointEnd);

                if (pointCross != null)
                {
                    pointsLine.Add((PointF)pointCross);
                    pointsLineEnd.Add((PointF)pointCross);
                    FillPath(context, GetBrush(areaSeries, point, pointEnd), pointsLine, pointsLineEnd);
                    pointsLine.Clear();
                    pointsLineEnd.Clear();
                    pointsLine.Add((PointF)pointCross);
                    pointsLineEnd.Add((PointF)pointCross);
                }
                else if (pointNext == null)
                {
                    FillPath(context, GetBrush(areaSeries, point, pointEnd), pointsLine, pointsLineEnd);
                }
            }
        }
                
        private StiBrush GetBrush(IStiLineRangeSeries areaSeries, PointF? point, PointF? pointEnd)
        {
            var brush = areaSeries.Brush;
            if (areaSeries.AllowApplyBrushNegative)
            {
                brush = point.Value.Y < pointEnd.Value.Y ? areaSeries.BrushNegative : areaSeries.Brush;
            }
            return brush;
        }

        private void FillPath(StiContext context, StiBrush brush, List<PointF> pointsLine, List<PointF> pointsLineEnd)
        {
            var path = new List<StiSegmentGeom>();

            var newPointsAll = new PointF[pointsLine.Count + pointsLineEnd.Count + 1];

            newPointsAll[0] = pointsLineEnd[0];
            pointsLine.CopyTo(newPointsAll, 1);

            var index = 0;
            for(var indexRev = pointsLineEnd.Count - 1; indexRev >= 0; indexRev--)
            {
                var value = pointsLineEnd[indexRev];
                newPointsAll[index + pointsLine.Count + 1] = value;
                index++;
            }

            path.Add(new StiLinesSegmentGeom(newPointsAll));

            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                var animation = new StiOpacityAnimation(StiChartHelper.GlobalDurationElement, StiChartHelper.GlobalBeginTimeElement);
                context.FillDrawAnimationPath(brush, null, path, StiPathGeom.GetBoundsState, null, animation, null);
            }
            else
                context.FillPath(brush, path, StiPathGeom.GetBoundsState, null);

            #region IsMouseOver
            if (IsMouseOver || Series.Core.IsMouseOver)
            {
                context.FillPath(StiMouseOverHelper.GetMouseOverColor(), path, StiPathGeom.GetBoundsState, null);
            }
            #endregion
        }

        private bool Intersection(PointF? point, PointF? pointEnd, PointF? pointNext, PointF? pointNextEnd)
        {
            if (pointNext == null)
                return false;
            if (point.Value.Y > pointEnd.Value.Y && pointNext.Value.Y < pointNextEnd.Value.Y ||
                point.Value.Y < pointEnd.Value.Y && pointNext.Value.Y > pointNextEnd.Value.Y ||
                pointNext.Value.Y == pointNextEnd.Value.Y)
                return true;
            else
                return false;
        }

        private PointF? GetPointCross(PointF point, PointF pointEnd, PointF pointNext, PointF pointNextEnd)
        {
            if (pointNext == pointNextEnd)
                return pointNext;

            float x1 = point.X;
            float y1 = point.Y;

            float x2 = pointNext.X;
            float y2 = pointNext.Y;

            float x3 = pointEnd.X;
            float y3 = pointEnd.Y;

            float x4 = pointNextEnd.X;
            float y4 = pointNextEnd.Y;

            float x = -((x1 * y2 - x2 * y1) * (x4 - x3) - (x3 * y4 - x4 * y3) * (x2 - x1)) / ((y1 - y2) * (x4 - x3) - (y3 - y4) * (x2 - x1));
            float y = ((y3 - y4) * (-x) - (x3 * y4 - x4 * y3)) / (x4 - x3);

            if (x > x1 && x < x2)
            {
                return new PointF(x, y);
            }
            else
            {
                return null;
            }

            
        }
        #endregion

        public StiRangeSeriesGeom(StiAreaGeom areaGeom, PointF?[] points, PointF?[] pointsEnd, IStiSeries series)
            : base(areaGeom, null, points, series)
        {
            this.pointsEnd = pointsEnd;
        }
    }
}
