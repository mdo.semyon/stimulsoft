﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Chart.Geoms.Series;
using System.Drawing;

namespace Stimulsoft.Report.Chart
{
    public class StiTreemapSeriesElementGeom : StiSeriesElementGeom
    {
        #region Properties
        private StiBrush seriesBrush;
        public StiBrush SeriesBrush
        {
            get
            {
                return seriesBrush;
            }
        }

        private Color seriesBorderColor;
        public Color SeriesBorderColor
        {
            get
            {
                return seriesBorderColor;
            }
        }

        private StiAnimation animation;
        public StiAnimation Animation
        {
            get
            {
                return animation;
            }
        }
        #endregion

        #region Methods
        public override void Draw(StiContext context)
        {
            var chart = this.Series.Chart as StiChart;
            var rect = this.ClientRectangle;

            if (chart.IsAnimation)
            {
                #region Draw Column
                var pen = new StiPenGeom(SeriesBorderColor, 1);
                
                context.DrawAnimationRectangle(seriesBrush, pen, rect, this, animation, GetInteractionData(), GetToolTip());
                #endregion
            }
            else
            {
                base.Draw(context);                

                #region Draw Box            
                Series.Chart.Style.Core.FillColumn(context, rect, seriesBrush, GetInteractionData());

                if (IsSelected)
                    context.FillRectangle(StiSelectedHelper.GetSelectedBrush(), rect.X, rect.Y, rect.Width, rect.Height, null);

                if (IsMouseOver || Series.Core.IsMouseOver)
                {
                    context.FillRectangle(StiMouseOverHelper.GetMouseOverColor(), rect.X, rect.Y, rect.Width, rect.Height, null);
                }
                #endregion

                #region Draw Column Border
                var pen = new StiPenGeom(SeriesBorderColor, 1);

                var points = new PointF[]
                                    {
                                        new PointF(rect.X, rect.Bottom),
                                        new PointF(rect.X, rect.Y),
                                        new PointF(rect.Right, rect.Y),
                                        new PointF(rect.Right, rect.Bottom),
                                        new PointF(rect.X, rect.Bottom),
                                    };

                context.DrawLines(pen, points);
                #endregion
            }
        }
        #endregion

        public StiTreemapSeriesElementGeom(StiAreaGeom areaGeom, double value, int index,
            StiBrush seriesBrush, Color seriesBorderColor, IStiSeries series, RectangleF clientRectangle, StiAnimation animation)
            : base(areaGeom, value, index, series, clientRectangle)
        {
            this.seriesBrush = seriesBrush;
            this.seriesBorderColor = seriesBorderColor;

            this.animation = animation;
        }
    }
}
