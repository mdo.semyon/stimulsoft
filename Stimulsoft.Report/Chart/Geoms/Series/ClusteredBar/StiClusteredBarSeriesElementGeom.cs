﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Report.Chart.Geoms.Series;

namespace Stimulsoft.Report.Chart
{
    public class StiClusteredBarSeriesElementGeom : StiSeriesElementGeom
    {
        #region Properties
        private StiBrush seriesBrush;
        public StiBrush SeriesBrush
        {
            get
            {
                return seriesBrush;
            }
        }

        private Color seriesBorderColor;
        public Color SeriesBorderColor
        {
            get
            {
                return seriesBorderColor;
            }
        }

        private TimeSpan? beginTime;
        public TimeSpan? BeginTime
        {
            get
            {
                return beginTime;
            }
        }

        private RectangleF columnRectStart;
        public RectangleF ColumnRectStart
        {
            get
            {
                return columnRectStart;
            }
        }

        private double valueStart;
        public double ValueStart
        {
            get
            {
                return valueStart;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            var chart = this.Series.Chart as StiChart;

            if (chart.IsAnimation)
            {
                #region Draw Bar
                var columnRect = this.ClientRectangle;
                var pen = new StiPenGeom(SeriesBorderColor, 1);
                
                var duration = StiChartHelper.GlobalDurationElement;

                if (Series.ShowShadow)
                {
                    var animationOpacity = new StiOpacityAnimation(duration, new TimeSpan(beginTime.Value.Ticks + duration.Ticks));
                    context.DrawShadowRect(columnRect, 5, animationOpacity);
                }
                
                var animationColumn = new StiColumnAnimation(ValueStart, ColumnRectStart, duration, beginTime);
                context.DrawAnimationBar(seriesBrush, pen, columnRect, Value, this.GetToolTip(), this, animationColumn, GetInteractionData());
                #endregion
            }
            else
            {
                base.Draw(context);

                RectangleF rect = this.ClientRectangle;                

                #region Draw Shadow
                if (Series.ShowShadow && rect.Width > 4 && rect.Height > 4)
                {
                    RectangleF shadowRect = rect;
                    if (Value > 0)
                    {
                        shadowRect.Y--;

                        context.DrawCachedShadow(
                            shadowRect,
                            StiShadowSides.Left |
                            StiShadowSides.Bottom,
                            context.Options.IsPrinting);
                    }
                    else if (Value < 0)
                    {

                        context.DrawCachedShadow(
                            new RectangleF(
                            shadowRect.X - 8,
                            shadowRect.Y,
                            shadowRect.Width + 8,
                            shadowRect.Height
                            ),
                            StiShadowSides.Top |
                            StiShadowSides.Right |
                            StiShadowSides.Edge |
                            StiShadowSides.Bottom,
                            context.Options.IsPrinting);
                    }
                }
                #endregion

                #region Draw Bar
                StiPenGeom pen = new StiPenGeom(SeriesBorderColor, 1);
                Series.Chart.Style.Core.FillColumn(context, rect, seriesBrush, GetInteractionData());
                PointF[] points = null;

                if (IsSelected)
                    context.FillRectangle(StiSelectedHelper.GetSelectedBrush(), rect.X, rect.Y, rect.Width, rect.Height, null);

                if (IsMouseOver || Series.Core.IsMouseOver)
                {
                    context.FillRectangle(StiMouseOverHelper.GetMouseOverColor(), rect.X, rect.Y, rect.Width, rect.Height, null);
                }

                if (Value > 0)
                {

                    points = new PointF[]
									{												
										new PointF(rect.Right, rect.Y),
										new PointF(rect.X, rect.Y), 
										new PointF(rect.X, rect.Bottom),
										new	PointF(rect.Right, rect.Bottom)
									};

                }
                else
                {
                    points = new PointF[]
									{
										new PointF(rect.X, rect.Y),
										new PointF(rect.Right, rect.Y),
										new PointF(rect.Right, rect.Bottom),
										new PointF(rect.X, rect.Bottom)
									};
                }

                context.DrawLines(pen, points);
                #endregion
            }
        }
        #endregion

        public StiClusteredBarSeriesElementGeom(StiAreaGeom areaGeom, double valueStart, double value, int index,
            StiBrush seriesBrush, Color seriesBorderColor, IStiSeries series, RectangleF columnRectStart, RectangleF columnRect, TimeSpan? beginTime)
            : base(areaGeom, value, index, series, columnRect)
        {
            this.seriesBrush = seriesBrush;
            this.seriesBorderColor = seriesBorderColor;

            this.valueStart = valueStart;
            this.columnRectStart = columnRectStart;

            this.beginTime = beginTime;
        }
    }
}
