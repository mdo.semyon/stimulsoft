﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiMarkerGeom : StiCellGeom, IStiSeriesElement
    {
        #region IStiGeomInteraction override
        public override void InvokeMouseEnter(StiInteractionOptions options)
        {
            if (!AllowMouseOver) return;

            if (!IsMouseOver)
            {
                IsMouseOver = true;
                options.UpdateContext = true;
            }

            int valueIndex = GetValueIndex();

            options.InteractionToolTip = GetToolTip(valueIndex);
            options.InteractionHyperlink = GetHyperlink(valueIndex);
        }

        public override void InvokeMouseLeave(StiInteractionOptions options)
        {
            if (!AllowMouseOver) return;

            if (IsMouseOver)
            {
                IsMouseOver = false;
                options.UpdateContext = true;
            }
        }

        public override void InvokeClick(StiInteractionOptions options)
        {
            int valueIndex = GetValueIndex();

            if (Series.Hyperlinks != null && valueIndex < Series.Hyperlinks.Length)
            {
                options.InteractionHyperlink = series.Hyperlinks[valueIndex];
            }

            if (Series.Interaction.DrillDownEnabled)
            {
                options.SeriesInteractionData = this.Interaction;

                IsMouseOver = false;
                options.UpdateContext = true;
            }

            this.IsSelected = !this.IsSelected;
        }

        private int GetValueIndex()
        {
            int valueIndex = this.Index;

            if (this.Series is IStiClusteredBarSeries ||
                this.Series is IStiStackedBarSeries ||
                this.Series is IStiFullStackedBarSeries)
            {
                if (this.Series.Chart.Area is IStiAxisArea && !((IStiAxisArea)this.Series.Chart.Area).ReverseVert)
                    valueIndex = Series.Values.Length - valueIndex - 1;
            }
            else
            {
                if (this.Series.Chart.Area is IStiAxisArea && ((IStiAxisArea)this.Series.Chart.Area).ReverseHor)
                    valueIndex = Series.Values.Length - valueIndex - 1;
            }
            return valueIndex;
        }

        internal string GetHyperlink()
        {
            return GetHyperlink(GetValueIndex());
        }

        private string GetHyperlink(int valueIndex)
        {
            if (Series.Hyperlinks != null && valueIndex < Series.Hyperlinks.Length)
                return series.Hyperlinks[valueIndex];
            else
                return null;
        }

        internal string GetToolTip()
        {
            return GetToolTip(GetValueIndex());
        }

        private string GetToolTip(int valueIndex)
        {
            if (Series.ToolTips != null && valueIndex < Series.ToolTips.Length)
                return series.ToolTips[valueIndex];
            else
                return null;
        }

        public virtual bool AllowMouseOver
        {
            get
            {
                int index = GetValueIndex();
                return 
                    GetHyperlink(GetValueIndex()) != null || 
                    (Series.ToolTips != null && index < Series.ToolTips.Length) ||
                    (this.Series.Interaction.DrillDownEnabled && this.Series.Interaction.AllowSeriesElements);
            }
        }

        public virtual bool IsMouseOver
        {
            get
            {
                if (this.Series == null)
                    return false;
                return this.Series.Core.GetIsMouseOverSeriesElement(this.Index);
            }
            set
            {
                if (this.Series != null)
                    this.Series.Core.SetIsMouseOverSeriesElement(this.Index, value);
            }
        }

        public virtual bool IsSelected
        {
            get
            {
                if (!((StiChart)this.series.Chart).Page.IsDashboard) return false;

                return this.Series.Core.GetIsSelectedSeriesElement(this.Index);
            }
            set
            {
                this.Series.Core.SetIsSelectedSeriesElement(this.Index, value);
            }
        }
        #endregion

        #region Properties
        private StiSeriesInteractionData interaction;
        public StiSeriesInteractionData Interaction
        {
            get
            {
                return interaction;
            }
            set
            {
                interaction = value;
            }
        }

        private int index;
        public int Index
        {
            get
            {
                return index;
            }
        }

        private PointF point;
        public PointF Point
        {
            get
            {
                return point;
            }
        }

        private IStiMarker marker;
        public IStiMarker Marker
        {
            get
            {
                return marker;
            }
        }

        private double value;
        public double Value
        {
            get
            {
                return value;
            }
        }

        private bool showShadow;
        public bool ShowShadow
        {
            get
            {
                return showShadow;
            }
        }

        private bool isTooltipMode;
        public bool IsTooltipMode
        {
            get
            {
                return isTooltipMode;
            }
        }

        private IStiSeries series;
        public IStiSeries Series
        {
            get
            {
                return series;
            }
        }

        private string elementIndex;
        public string ElementIndex
        {
            get
            {
                return elementIndex;
            }
            set
            {
                elementIndex = value;
            }
        }
        #endregion

        #region Methods
        public override bool Contains(float x, float y)
        {
            if (Invisible) return false;
            return GetMouseOverRect().Contains(x, y);
        }

        internal RectangleF GetMouseOverRect()
        {
            RectangleF rect = this.ClientRectangle;
            rect.Inflate(rect.Width / 2, rect.Height / 2);
            return rect;
        }

        /// <summary>
        /// Draws area geom object on spefied context.
        /// </summary>
        public override void Draw(StiContext context)
        {
            context.PushSmoothingModeToAntiAlias();

            float chartZoom = context.Options.Zoom;

            if (IsMouseOver)
               context.FillEllipse(StiMouseOverHelper.GetLineMouseOverColor(), GetMouseOverRect(), null);
            
            var chart = this.Series.Chart as StiChart;

            var markerClone = (IStiMarker)this.Marker.Clone();

            if (!IsTooltipMode)
                markerClone.Brush = this.series.ProcessSeriesBrushes(index, this.Marker.Brush);

            var interaction = !chart.IsAnimation ? null :
                new StiInteractionDataGeom()
                {
                    ComponentName = chart.Name,
                    ComponentIndex = chart.Page.Components.IndexOf(chart).ToString(),
                    PageGuid = ((StiSeries)this.Series).DrillDownPageGuid,
                    PageIndex = chart.Page.Report.RenderedPages.IndexOf(chart.Page).ToString(),
                    ElementIndex = this.ElementIndex.ToString()
                };

            this.Marker.Core.Draw(context, markerClone, this.Point, chartZoom, this.ShowShadow, this.IsMouseOver || this.Series.Core.IsMouseOver, isTooltipMode, chart.IsAnimation, this.GetToolTip(), this, interaction);
            
            context.PopSmoothingMode();
        }
        #endregion

        public StiMarkerGeom(IStiSeries series, int index, double value, PointF point, IStiMarker marker, bool showShadow, float zoom, bool isTooltipMode)
            : 
            base(StiMarkerCoreXF.GetMarkerRect(point, marker.Size, zoom))
        {
            this.series = series;
            this.index = index;
            this.value = value;
            this.point = point;
            this.marker = marker;
            this.showShadow = showShadow;
            this.isTooltipMode = isTooltipMode;
        }
    }
}
