using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Report.Chart
{
    public class StiGridLinesCoreXF : 
        IStiApplyStyle, 
        ICloneable
    {
        #region ICloneable
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion

        #region IStiApplyStyle
        public virtual void ApplyStyle(IStiChartStyle style)
        {
            if (GridLines.AllowApplyStyle)
            {
                if (this.GridLines is IStiGridLinesVert)
                {
                    this.GridLines.Color = style.Core.GridLinesVertColor;
                    this.GridLines.MinorColor = style.Core.GridLinesVertColor;
                }
                else
                {
                    this.GridLines.Color = style.Core.GridLinesHorColor;
                    this.GridLines.MinorColor = style.Core.GridLinesHorColor;
                }
            }
        }
        #endregion

        #region Properties
        private IStiGridLines gridLines;
        public IStiGridLines GridLines
        {
            get
            {
                return gridLines;
            }
            set
            {
                gridLines = value;
            }
        }
        #endregion

        public StiGridLinesCoreXF(IStiGridLines gridLines)
        {
            this.gridLines = gridLines;
        }
	}
}
