#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections.Generic;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;

namespace Stimulsoft.Report.Chart
{
    public class StiDoughnutSeriesCoreXF : StiPieSeriesCoreXF
    {
        #region Methods
        private StiDoughnutSeriesElementGeom RenderDoughnutElement(StiContext context, PointF center, float radius, float radiusDt,
            Color borderColor, StiBrush brush, float start, float angle, double? value, int index,
            IStiDoughnutSeries currentSeries, bool shadow, StiAreaGeom areaGeom, TimeSpan beginTime)
        {
            if (angle == 0 || float.IsNaN(angle)) return null;

            var rectPie = new RectangleF(center.X - radius, center.Y - radius, radius * 2, radius * 2);
            var rectPieDt = new RectangleF(center.X - radiusDt, center.Y - radiusDt, radiusDt * 2, radiusDt * 2);

            if (rectPie.Width <= 0 && rectPie.Height <= 0) return null;

            #region Correct brush
            if (!shadow)
            {
                if (brush is StiGradientBrush)
                {
                    brush = brush.Clone() as StiGradientBrush;
                    ((StiGradientBrush)brush).Angle = -45f;
                }

                if (brush is StiGlareBrush)
                {
                    brush = brush.Clone() as StiGlareBrush;
                    ((StiGlareBrush)brush).Angle = -45f;
                }
            }
            #endregion

            List<StiSegmentGeom> path = new List<StiSegmentGeom>();
            
            #region Init light
            StiBrush brLight = null;
            StiBrush brDark = null;
            List<StiSegmentGeom> pathLight = null;
            List<StiSegmentGeom> pathDark = null;
            float lightWidth = 0;

            if (currentSeries.Lighting && (!shadow))
            {
                pathLight = new List<StiSegmentGeom>();
                pathDark = new List<StiSegmentGeom>();

                lightWidth = radius * 0.02f;
                brLight = new StiGradientBrush(
                    Color.FromArgb(100, Color.White),
                    Color.FromArgb(50, Color.Black), 45);

                brDark = new StiGradientBrush(
                    Color.FromArgb(50, Color.Black),
                    Color.FromArgb(100, Color.White), 45);
            }
            #endregion

            var chart = this.Series.Chart as StiChart;

            #region Create path
            path.Add(new StiArcSegmentGeom(rectPie, start, angle));
            if (angle - start != 360)
                path.Add(new StiLineSegmentGeom(GetPoint(center, radius, start + angle), GetPoint(center, radiusDt, start + angle)));
            path.Add(new StiArcSegmentGeom(rectPieDt, start + angle, -angle));
            if (angle - start != 360)
                path.Add(new StiLineSegmentGeom(GetPoint(center, radiusDt, start), GetPoint(center, radius, start)));

            if (shadow)
            {
                if (chart.IsAnimation)
                {
                    var animation = new StiOpacityAnimation(TimeSpan.FromSeconds(1), beginTime);
                    context.FillDrawAnimationPath(brush, null, path, rectPie, null, animation, null);
                }                    
                else
                    context.FillPath(brush, path, rectPie, null);
            }
            #endregion

            #region Draw light
            if (brLight != null && (!shadow))
            {
                // Internal path
                pathLight.Add(new StiLineSegmentGeom(GetPoint(center, radius - lightWidth, start), GetPoint(center, radius, start)));
                pathLight.Add(new StiArcSegmentGeom(rectPie, start, angle));
                pathLight.Add(new StiLineSegmentGeom(GetPoint(center, radius, start + angle), GetPoint(center, radius - lightWidth, start + angle)));
                pathLight.Add(new StiArcSegmentGeom(new RectangleF(rectPie.X + lightWidth, rectPie.Y + lightWidth,
                    rectPie.Width - lightWidth * 2, rectPie.Height - lightWidth * 2), start + angle, -angle));

                // External path
                pathDark.Add(new StiLineSegmentGeom(GetPoint(center, radiusDt + lightWidth, start), GetPoint(center, radiusDt, start)));
                pathDark.Add(new StiArcSegmentGeom(rectPieDt, start, angle));
                pathDark.Add(new StiLineSegmentGeom(GetPoint(center, radiusDt, start + angle), GetPoint(center, radiusDt + lightWidth, start + angle)));
                pathDark.Add(new StiArcSegmentGeom(new RectangleF(rectPieDt.X - lightWidth, rectPieDt.Y - lightWidth,
                    rectPieDt.Width + lightWidth * 2, rectPieDt.Height + lightWidth * 2), start + angle, -angle));
            }
            #endregion

            if (!shadow)
            {
                var seriesGeom = new StiDoughnutSeriesElementGeom(areaGeom, value.GetValueOrDefault(), index, currentSeries, rectPie, path, pathLight, pathDark,
                        borderColor, brush, brLight, brDark, start, start + angle, radius, radiusDt, beginTime);
                return seriesGeom;
            }

            return null;
        }

        public override void RenderSeries(StiContext context, RectangleF rect, StiAreaGeom geom, IStiSeries[] seriesArray)
        {
            if (seriesArray == null || seriesArray.Length == 0 || this.Series.Chart == null) return;

            TimeSpan duration = StiChartHelper.GlobalDurationElement;
            TimeSpan beginTime = StiChartHelper.GlobalBeginTimeElement;

            int colorCount = 0;
            foreach (IStiSeries ser in seriesArray)
            {
                if (ser.Values != null) colorCount += ser.Values.Length;
            }

            if (colorCount == 0) return;

            float radius = GetRadius(context, rect);
            PointF pointCenter = GetPointCenter(rect);
            IStiPieSeries currentSeries = seriesArray[0] as IStiPieSeries;

            #region Measure Series Labels
            int colorIndex = 0;
            float step = radius / (seriesArray.Length + 1);

            RectangleF bounds = new RectangleF(10 * context.Options.Zoom, 10 * context.Options.Zoom,
                rect.Width - 20 * context.Options.Zoom, rect.Height - 20 * context.Options.Zoom);
            RectangleF rectPie2 = bounds;

            float angle = 0;

            float radiusDt = radius;
            foreach (IStiDoughnutSeries ser in seriesArray)
            {
                int index = 0;
                angle = ser.StartAngle;
                float gradPerValue = GetGradPerValue(ser);
                foreach (double? value in ser.Values)
                {
                    float percentPerValue = GetPercentPerValue(ser);
                    float arcWidth = (float)(gradPerValue * Math.Abs(value.GetValueOrDefault()));

                    if (this.Series.Chart != null && this.Series.Chart.SeriesLabels != null && this.Series.Chart.SeriesLabels.Visible)
                    {
                        IStiPieSeriesLabels labels = this.Series.Chart.SeriesLabels as IStiPieSeriesLabels;

                        if (labels != null && labels.Visible)
                        {
                            float radiusDt2 = radiusDt - step;

                            RectangleF measureRect;
                            ((StiPieSeriesLabelsCoreXF)labels.Core).RenderLabel
                            (ser, context, pointCenter, radiusDt, radiusDt2,
                                angle + arcWidth / 2, index, Math.Abs(value.GetValueOrDefault()), value,
                                GetArgumentText(ser, index),
                                currentSeries.Core.GetTag(index), true,
                                colorIndex, colorCount, percentPerValue, out measureRect, false, 0);

                            if (!measureRect.IsEmpty)
                                bounds = RectangleF.Union(bounds, measureRect);
                        }
                    }
                    colorIndex++;

                    angle += arcWidth;
                    index++;
                }
                radiusDt -= step * 1.2f;
            }

            float dist = 0;
            dist = Math.Min(dist, bounds.Left - rectPie2.Left);
            dist = Math.Min(dist, rectPie2.Right - bounds.Right);
            dist = Math.Min(dist, bounds.Top - rectPie2.Top);
            dist = Math.Min(dist, rectPie2.Bottom - bounds.Bottom);
            radius += dist;
            #endregion

            //if (radius <= 5) return;

            var chart = this.Series.Chart as StiChart;

            #region Render Shadow
            if (currentSeries.ShowShadow && !chart.IsAnimation)
            {
                RectangleF rectPie = new RectangleF(
                    pointCenter.X - radius,
                    pointCenter.Y - radius,
                    radius * 2, radius * 2);

                StiContext shadowContext = context.CreateShadowGraphics();

                StiBrush shadowBrush = new StiSolidBrush(Color.FromArgb(100, Color.Black));

                step = radius / (seriesArray.Length + 1);
                
                radiusDt = radius;
                foreach (IStiDoughnutSeries ser in seriesArray)
                {
                    angle = ser.StartAngle;
                    float gradPerValue = GetGradPerValue(ser);

                    int index = 0;
                    foreach (double? value in ser.Values)
                    {
                        float arcWidth = (float)(gradPerValue * Math.Abs(value.GetValueOrDefault()));

                        RenderDoughnutElement(shadowContext, new PointF(rectPie.Width / 2, rectPie.Height / 2),
                            radiusDt, radiusDt - step, Color.Black, shadowBrush,
                            angle, arcWidth, Math.Abs(value.GetValueOrDefault()), index, ser, true, geom, new TimeSpan(beginTime.Ticks));

                        angle += arcWidth;
                        index++;
                    }
                    radiusDt -= step * 1.2f;
                }

                var shadowGeom = new StiPieSeriesShadowElementGeom(currentSeries, rectPie, (radius * 0.01f + 2 * context.Options.Zoom), shadowContext, duration, beginTime);

                geom.CreateChildGeoms();
                geom.ChildGeoms.Add(shadowGeom);
            }
            #endregion

            #region Render DoughnutElements
            step = radius / (seriesArray.Length + 1);
            colorIndex = 0;
            radiusDt = radius;
            foreach (IStiDoughnutSeries ser in seriesArray)
            {
                angle = ser.StartAngle;
                float gradPerValue = GetGradPerValue(ser);

                #region Check for zeros
                int nonZeroValuesCount = 0;
                int firstNonZeroValueIndex = -1;
                int firstNonZeroValueIndexTemp = 0;

                foreach (double? value in ser.Values)
                {
                    if (!(value == 0 || value == null || double.IsNaN(value.Value)))
                    {
                        nonZeroValuesCount++;
                        if (nonZeroValuesCount == 1)
                            firstNonZeroValueIndex = firstNonZeroValueIndexTemp;
                    }
                    firstNonZeroValueIndexTemp++;
                }
                
                #endregion

                if (nonZeroValuesCount == 0)
                {
                }

                #region nonZeroValuesCount == 1
                else if (nonZeroValuesCount == 1)
                {
                    colorIndex = firstNonZeroValueIndex != -1 ? firstNonZeroValueIndex : 0;
                    StiBrush seriesBrush = ser.Brush;
                    if (ser.AllowApplyBrush)
                    {
                        seriesBrush = ser.Core.GetSeriesBrush(colorIndex, colorCount);
                        seriesBrush = ser.ProcessSeriesBrushes(colorIndex, seriesBrush);
                    }
                    
                    Color borderColor = (Color)currentSeries.Core.GetSeriesBorderColor(colorIndex, colorCount);
                    if (firstNonZeroValueIndex >= ser.Values.Length) continue;

                    var doughnutElementGeom = RenderDoughnutElement(context, pointCenter, radiusDt, radiusDt - step, borderColor, seriesBrush,
                                0, 360, ser.Values[firstNonZeroValueIndex], firstNonZeroValueIndex, ser, false, geom, new TimeSpan(beginTime.Ticks));

                    if (doughnutElementGeom != null)
                    {
                        geom.CreateChildGeoms();
                        geom.ChildGeoms.Add(doughnutElementGeom);
                    }

                    radiusDt -= step * 1.2f;
                    continue;
                }
                #endregion
                
                else
                {
                    int index = 0;
                    foreach (double? value in ser.Values)
                    {

                        float arcWidth = (float)(gradPerValue * Math.Abs(value.GetValueOrDefault()));

                        if (value != 0)
                        {
                            StiBrush seriesBrush = ser.Brush;
                            if (ser.AllowApplyBrush)
                            {
                                seriesBrush = ser.Core.GetSeriesBrush(colorIndex, colorCount);
                                seriesBrush = ser.ProcessSeriesBrushes(colorIndex, seriesBrush);
                            }

                            Color borderColor = ser.BorderColor;
                            if (ser.AllowApplyBorderColor)
                            {
                                borderColor = (Color)ser.Core.GetSeriesBorderColor(colorIndex, colorCount);
                            }
                            var doughnutElementGeom = RenderDoughnutElement(context, pointCenter, radiusDt, radiusDt - step, borderColor, seriesBrush,
                                angle, arcWidth, Math.Abs(value.GetValueOrDefault()), index, ser, false, geom, new TimeSpan(beginTime.Ticks / ser.Values.Length * colorIndex));

                            if (doughnutElementGeom != null)
                            {
                                #region Interaction
                                if (ser.Core.Interaction != null)
                                {
                                    StiSeriesInteractionData data = new StiSeriesInteractionData();
                                    data.Fill(geom.Area, ser, index);
                                    doughnutElementGeom.Interaction = data;
                                }
                                #endregion

                                geom.CreateChildGeoms();
                                geom.ChildGeoms.Add(doughnutElementGeom);
                            }


                            angle += arcWidth;
                        }
                        colorIndex++;
                        index++;
                    }
                }
                radiusDt -= step * 1.2f;

            }
            #endregion

            #region Draw Series Labels
            colorIndex = 0;
            radiusDt = radius;
            foreach (IStiDoughnutSeries ser in seriesArray)
            {
                int index = 0;
                angle = ser.StartAngle;
                float gradPerValue = GetGradPerValue(ser);
                foreach (double? value in ser.Values)
                {
                    float percentPerValue = GetPercentPerValue(ser);
                    float arcWidth = (float)(gradPerValue * Math.Abs(value.GetValueOrDefault()));

                    IStiSeriesLabels seriesLabels = null;
                    if (ser.ShowSeriesLabels == StiShowSeriesLabels.FromChart)
                        seriesLabels = this.Series.Chart.SeriesLabels;
                    if (ser.ShowSeriesLabels == StiShowSeriesLabels.FromSeries)
                        seriesLabels = currentSeries.SeriesLabels;

                    if (this.Series.Chart != null && seriesLabels != null && seriesLabels.Visible)
                    {
                        IStiPieSeriesLabels labels = seriesLabels as IStiPieSeriesLabels;

                        if (labels != null && labels.Visible && (labels.Step == 0 || (index % labels.Step == 0)))
                        {
                            RectangleF measureRect;
                            float radiusDt2 = radiusDt - step;

                            StiSeriesLabelsGeom seriesLabelsGeom =
                                ((StiPieSeriesLabelsCoreXF)labels.Core).RenderLabel(ser, context, pointCenter, radiusDt, radiusDt2,
                                    angle + arcWidth / 2,
                                    index, Math.Abs(value.GetValueOrDefault()), value,
                                    GetArgumentText(ser, index), 
                                    GetTag(index), false,
                                    colorIndex, colorCount, percentPerValue, out measureRect, false, 0);

                            if (seriesLabelsGeom != null)
                            {
                                seriesLabelsGeom.Duration = duration;
                                seriesLabelsGeom.BeginTime = new TimeSpan(beginTime.Ticks / ser.Values.Length * colorIndex);

                                geom.CreateChildGeoms();
                                geom.ChildGeoms.Add(seriesLabelsGeom);
                            }
                        }
                    }
                    colorIndex++;

                    angle += arcWidth;
                    index++;
                }
                radiusDt -= step * 1.2f;
            }
            #endregion	
        }

        private float GetGradPerValue(IStiSeries series)
        {
            double totals = 0;
            foreach (double? value in series.Values)
            {
                totals += Math.Abs(value.GetValueOrDefault());
            }
            return (float)(360 / totals);
        }

        private float GetPercentPerValue(IStiSeries series)
        {
            double totals = 0;
            foreach (double? value in series.Values)
            {
                totals += Math.Abs(value.GetValueOrDefault());
            }
            return (float)(1 / totals * 100);
        }

        private string GetArgumentText(IStiSeries series, int index)
        {
            if (series.Arguments.Length > index)
            {
                return series.Arguments[index].ToString();
            }
            return string.Empty;
        }
        #endregion        

        #region Properties.Localization
        /// <summary>
        /// Gets a service name.
        /// </summary>
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "Doughnut");
            }
        }
        #endregion

        public StiDoughnutSeriesCoreXF(IStiSeries series)
            : base(series)
        {
        }
	}
}
