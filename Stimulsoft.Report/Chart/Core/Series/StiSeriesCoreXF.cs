#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;

using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public abstract class StiSeriesCoreXF : 
        ICloneable,
        IStiApplyStyleSeries
    {
        #region ICloneable
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion

        #region IStiApplyStyleSeries
        public virtual void ApplyStyle(IStiChartStyle style, Color color)
        {
            if (this.Series.AllowApplyStyle)
            {
                this.Series.ShowShadow = style.Core.SeriesShowShadow;

                if (this.Series.SeriesLabels != null)
                    this.Series.SeriesLabels.Core.ApplyStyle(style);
            }

            this.series.TrendLine.Core.ApplyStyle(style);
        }
        #endregion

        #region Methods
        
        protected RectangleF CheckLabelsRect(IStiSeriesLabels labels, StiAreaGeom geom, RectangleF labelsRect)
        {
            if (labels != null && labels.PreventIntersection)
            {
                if (labelsRect.X < 0) labelsRect.X = 0;
                if (labelsRect.Y < 0) labelsRect.Y = 0;
                if (labelsRect.Right > geom.ClientRectangle.Width) labelsRect.X = geom.ClientRectangle.Width - labelsRect.Width;
                if (labelsRect.Bottom > geom.ClientRectangle.Height) labelsRect.Y = geom.ClientRectangle.Height - labelsRect.Height;
                
                var rectangle = GetRectangle(labelsRect, labels.Angle);
                if (rectangle.Y - rectangle.Height < 0) labelsRect.Y = rectangle.Height / 2;
                if (rectangle.Y + rectangle.Height > geom.ClientRectangle.Height) labelsRect.Y -= rectangle.Height / 2;
            }
            return labelsRect;
        }

        private RectangleF GetRectangle(RectangleF labelsRect, double angle)
        {
            var point1 = new PointF(labelsRect.Left, labelsRect.Top);
            var point2 = new PointF(labelsRect.Right, labelsRect.Top);
            var point3 = new PointF(labelsRect.Right, labelsRect.Bottom);
            var point4 = new PointF(labelsRect.Left, labelsRect.Bottom);

            var pointCenter = new PointF((labelsRect.Left + labelsRect.Right) / 2, (labelsRect.Top + labelsRect.Bottom) / 2);

            var pointRotate1 = RotatePoint(point1, pointCenter, angle);
            var pointRotate2 = RotatePoint(point2, pointCenter, angle);
            var pointRotate3 = RotatePoint(point3, pointCenter, angle);
            var pointRotate4 = RotatePoint(point4, pointCenter, angle);

            var minY = Math.Min(Math.Min(pointRotate1.Y, pointRotate2.Y), Math.Min(pointRotate3.Y, pointRotate4.Y));
            var maxY = Math.Max(Math.Max(pointRotate1.Y, pointRotate2.Y), Math.Max(pointRotate3.Y, pointRotate4.Y));
            var minX = Math.Min(Math.Min(pointRotate1.X, pointRotate2.X), Math.Min(pointRotate3.X, pointRotate4.X));
            var maxX = Math.Max(Math.Max(pointRotate1.X, pointRotate2.X), Math.Max(pointRotate3.X, pointRotate4.X));

            return new RectangleF(minX, minY, maxX - minX, maxY - minY);
        }

        private PointF RotatePoint(PointF pointToRotate, PointF centerPoint, double angleInDegrees)
        {
            double angleInRadians = angleInDegrees * (Math.PI / 180);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);
            return new PointF
            {
                X =(int)(cosTheta * (pointToRotate.X - centerPoint.X) -sinTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.X),
                Y =(int)(sinTheta * (pointToRotate.X - centerPoint.X) + cosTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.Y)
            };
        }

        protected void CheckIntersectionLabels(StiAreaGeom geom)
        {
            List<StiCellGeom> childGeoms = geom.ChildGeoms;
            List<StiSeriesLabelsGeom> labelGeoms = new List<StiSeriesLabelsGeom>();
            if (childGeoms != null)
            {
                foreach (StiCellGeom cellGeom in childGeoms)
                {
                    if (cellGeom is StiSeriesLabelsGeom)
                        labelGeoms.Add((StiSeriesLabelsGeom)cellGeom);
                }
            }           

            int count = labelGeoms.Count;

            bool intersection = true;
            int indexCheck = 0;

            while (intersection && indexCheck < 29)
            {
                indexCheck++;

                for (int index1 = 0; index1 < count; index1++)
                {
                    for (int index2 = 0; index2 < count; index2++)
                    {
                        if (index2 == index1) continue;

                        if (labelGeoms[index1].ClientRectangle.IntersectsWith(labelGeoms[index2].ClientRectangle))
                        {
                            RectangleF rect1 = labelGeoms[index1].ClientRectangle;
                            RectangleF rect2 = labelGeoms[index2].ClientRectangle;

                            if (rect1.IntersectsWith(rect2))
                            {
                                float overlay = rect1.Height - Math.Abs(labelGeoms[index2].ClientRectangle.Y - labelGeoms[index1].ClientRectangle.Y) + 2;
                                if (rect1.Y > rect2.Y)
                                {
                                    rect1.Y += overlay / 2;
                                    rect2.Y -= overlay / 2;
                                }
                                else
                                {
                                    rect1.Y -= overlay / 2;
                                    rect2.Y += overlay / 2;
                                }

                                if (rect1.Y < 0)
                                    rect1.Y = 0;

                                if (rect2.Y < 0)
                                    rect2.Y = 0;

                                if ((rect1.Y + rect1.Height) > geom.ClientRectangle.Height)
                                    rect1.Y = geom.ClientRectangle.Height - rect1.Height;

                                if ((rect2.Y + overlay / 2 + rect2.Height) > geom.ClientRectangle.Height)
                                    rect2.Y = geom.ClientRectangle.Height - rect2.Height;

                                labelGeoms[index1].ClientRectangle = rect1;
                                labelGeoms[index2].ClientRectangle = rect2;
                            }                            
                        }
                    }
                }
            }
        }

        private RectangleF GetLabelRectangle(float angle, RectangleF rect)
        {
            var hypotenuse = Math.Pow(Math.Pow(rect.Width, 2) + Math.Pow(rect.Height, 2), 0.5);
            var angleDelt = Math.Atan(rect.Height / rect.Width) / Math.PI * 180;
            angle += (float)angleDelt;
            return new RectangleF(rect.X, rect.Y, (float)(hypotenuse * Math.Cos(angle * Math.PI / 180)), (float)(hypotenuse * (float)Math.Sin(angle * Math.PI / 180)));
        }

        public virtual void RenderSeries(StiContext context, RectangleF rect, StiAreaGeom geom, IStiSeries[] seriesArray)
        {
        }        

        public virtual StiBrush GetSeriesBrush(int colorIndex, int colorCount)
        {
            if (this.Series.Chart == null || this.Series.Chart.Area == null) return null;

            if ((this.Series.Chart.Area.ColorEach || this is StiDoughnutSeriesCoreXF || this is StiPictorialSeriesCoreXF) && string.IsNullOrEmpty(((StiSeries)series).AutoSeriesColorDataColumn))
            {
                StiStyleCoreXF styleCore = this.Series.Chart.Style != null ? this.Series.Chart.Style.Core : new StiStyleCoreXF25();

                Color color = styleCore.GetColorByIndex(colorIndex, colorCount);
                StiBrush seriesBrush = styleCore.GetColumnBrush(color);

                if (this.Series.Chart.Area is IStiClusteredBarArea)
                {
                    if (seriesBrush is StiGradientBrush) ((StiGradientBrush)seriesBrush).Angle += 90;
                    if (seriesBrush is StiGlareBrush) ((StiGlareBrush)seriesBrush).Angle += 90;
                }
                return seriesBrush;
            }
            return null;
        }


        public virtual object GetSeriesBorderColor(int colorIndex, int colorCount)
        {
            if (this.Series.Chart == null || this.Series.Chart.Area == null) return null;

            if (this.Series.Chart.Area.ColorEach && this.Series.AllowApplyStyle || this is StiDoughnutSeriesCoreXF)
            {
                StiStyleCoreXF styleCore = this.Series.Chart.Style != null ? this.Series.Chart.Style.Core : new StiStyleCoreXF25();

                Color color = styleCore.GetColorByIndex(colorIndex, colorCount);

                return styleCore.GetColumnBorder(color);
            }
            return null;
        }

        
        public IStiAxisSeriesLabels GetSeriesLabels()
        {
            if (this.Series.ShowSeriesLabels == StiShowSeriesLabels.FromChart)
                return this.Series.Chart.SeriesLabels as IStiAxisSeriesLabels;

            if (this.Series.ShowSeriesLabels == StiShowSeriesLabels.FromSeries)
                return this.Series.SeriesLabels as IStiAxisSeriesLabels;

            return null;
        }


        public string GetTag(int tagIndex)
        {
            if (series.Tags != null && tagIndex < series.Tags.Length && series.Tags[tagIndex] != null)
                return series.Tags[tagIndex].ToString();
            else
                return string.Empty;
        }
        #endregion

        #region Interaction
        private static object FalseObject = new object();
        private static object TrueObject = new object();

        #region MouseOver
        private Hashtable isMouseOverSeriesElementHashtable = null;

        public bool GetIsMouseOverSeriesElement(int seriesIndex)
        {
            if (isMouseOverSeriesElementHashtable == null)
                return false;

            return isMouseOverSeriesElementHashtable[seriesIndex] == TrueObject;
        }

        public void SetIsMouseOverSeriesElement(int seriesIndex, bool value)
        {
            if (isMouseOverSeriesElementHashtable == null)
                isMouseOverSeriesElementHashtable = new Hashtable();

            isMouseOverSeriesElementHashtable[seriesIndex] = value ? TrueObject : FalseObject;
        }

        public bool IsMouseOver { get; set; }
        #endregion

        #region Selected
        private Hashtable isSelectedSeriesElementHashtable = null;

        public bool GetIsSelectedSeriesElement(int seriesIndex)
        {
            if (isSelectedSeriesElementHashtable == null)
                return false;

            return isSelectedSeriesElementHashtable[seriesIndex] == TrueObject;
        }

        public void SetIsSelectedSeriesElement(int seriesIndex, bool value)
        {
            if (isSelectedSeriesElementHashtable == null)
                isSelectedSeriesElementHashtable = new Hashtable();

            isSelectedSeriesElementHashtable[seriesIndex] = value ? TrueObject : FalseObject;
        }

        public bool IsSelected { get; set; }
        #endregion
        #endregion

        #region Properties.Localization
        /// <summary>
        /// Gets a service name.
        /// </summary>
        public abstract string LocalizedName
        {
            get;
        }
        #endregion

        #region Properties
        private bool isDateTimeValues = false;
        [Browsable(false)]
        [StiSerializable(StiSerializeTypes.SerializeToDocument)]
        public bool IsDateTimeValues
        {
            get
            {
                return isDateTimeValues;
            }
            set
            {
                isDateTimeValues = value;
            }
        }

        private bool isDateTimeArguments = false;
        [Browsable(false)]
        [StiSerializable(StiSerializeTypes.SerializeToDocument)]
        public bool IsDateTimeArguments
        {
            get
            {
                return isDateTimeArguments;
            }
            set
            {
                isDateTimeArguments = value;
            }
        }

        private IStiSeries series;
        public IStiSeries Series
        {
            get
            {
                return series;
            }
            set
            {
                series = value;
            }
        }
        #endregion

        #region Properties.Core.Interaction
        public IStiSeriesInteraction Interaction
        {
            get
            {
                return Series.Interaction;
            }
            set
            {
                Series.Interaction = value;
            }
        }
        #endregion

        public StiSeriesCoreXF(IStiSeries series)
        {
            this.series = series;
        }
	}
}

