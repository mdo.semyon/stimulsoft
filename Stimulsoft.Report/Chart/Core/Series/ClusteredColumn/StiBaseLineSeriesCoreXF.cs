#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Context.Animation;

namespace Stimulsoft.Report.Chart
{
    public abstract class StiBaseLineSeriesCoreXF :
        StiSeriesCoreXF,
        IStiApplyStyleSeries
    {
        #region IStiApplyStyleSeries
        public override void ApplyStyle(IStiChartStyle style, Color color)
        {
            base.ApplyStyle(style, color);

            IStiBaseLineSeries columnSeries = this.Series as IStiBaseLineSeries;

            if (columnSeries.AllowApplyStyle)
            {
                columnSeries.LineColor = color;

                columnSeries.Lighting = style.Core.SeriesLighting;
                columnSeries.Marker.Visible = style.Core.MarkerVisible;

                if (columnSeries.Marker != null)
                {
                    columnSeries.Marker.Brush = new StiSolidBrush(StiColorUtils.Light(color, 100));
                    columnSeries.Marker.BorderColor = StiColorUtils.Dark(color, 100);
                }

                if (columnSeries.LineMarker != null)
                {
                    columnSeries.LineMarker.Brush = new StiSolidBrush(StiColorUtils.Light(color, 50));
                    columnSeries.LineMarker.BorderColor = StiColorUtils.Dark(color, 150);
                }
            }
        }
        #endregion

        #region Methods
        protected PointF?[] ClipLinePoints(StiContext context, StiAreaGeom geom, PointF?[] points, out int startIndex, out int endIndex)
        {
            #region Auto Range
            if (((IStiAxisArea)this.Series.Chart.Area).XAxis.Range.Auto)
            {
                startIndex = 0;
                endIndex = points.Length;
                return points;
            }
            #endregion

            startIndex = -1;
            endIndex = -1;

            #region Create Clip Rect
            RectangleF clipRect = ((StiAxisAreaGeom)geom).View.ClientRectangle;
            clipRect.X = 0;
            clipRect.Y = 0;
            #endregion

            int pointIndex = 0;
            foreach (PointF? point in points)
            {
                if (point != null)
                {
                    #region Create Value Point
                    PointF valuePoint = point.Value;
                    valuePoint.X += geom.ClientRectangle.X;
                    valuePoint.Y += geom.ClientRectangle.Y;
                    #endregion

                    if (clipRect.X <= valuePoint.X && valuePoint.X < clipRect.Right && startIndex == -1)
                        startIndex = pointIndex;

                    if ((!(clipRect.X <= valuePoint.X && valuePoint.X < clipRect.Right)) && startIndex != -1)
                    {
                        endIndex = pointIndex;
                        break;
                    }
                }

                pointIndex++;
            }
            if (endIndex == -1)
                endIndex = points.Length - 1;

            startIndex--;
            endIndex++;
            if (startIndex < 0) startIndex = 0;
            if (endIndex >= points.Length) endIndex = points.Length - 1;

            int newCount = endIndex - startIndex + 1;
            if (newCount == points.Length)
                return points;

            PointF?[] newPoints = new PointF?[newCount];
            Array.Copy(points, startIndex, newPoints, 0, newCount);
            return newPoints;
        }

        protected virtual void RenderMarkers(StiContext context, StiAreaGeom geom, PointF?[] points)
        {
            IStiAxisArea axisArea = geom.Area as IStiAxisArea;
            IStiBaseLineSeries lineSeries = this.Series as IStiBaseLineSeries;

            if (points.Length == 0) return;

            var isTooltipMarkerMode = !lineSeries.Marker.Visible && lineSeries.ToolTips.Length > 0;

            if (lineSeries.Marker != null && (lineSeries.Marker.Visible || isTooltipMarkerMode))
            {
                int index = 0;
                foreach (PointF? point in points)
                {
                    if (point != null)
                    {
                        double? value = axisArea.ReverseHor ?
                                lineSeries.Values[lineSeries.Values.Length - index - 1] :
                                lineSeries.Values[index];

                        if (value == null && lineSeries.ShowNulls)
                            value = 0d;

                        #region Create Clip Rect
                        RectangleF clipRect = ((StiAxisAreaGeom)geom).View.ClientRectangle;
                        clipRect.X = 0;
                        clipRect.Y = 0;
                        clipRect.Inflate(10, 10);
                        #endregion

                        #region Create Value Point
                        PointF valuePoint = point.Value;
                        valuePoint.X += geom.ClientRectangle.X;
                        valuePoint.Y += geom.ClientRectangle.Y;
                        #endregion

                        if (clipRect.Contains(valuePoint))
                        {
                            StiMarkerGeom markerGeom = new StiMarkerGeom(this.Series, index, value.GetValueOrDefault(), point.Value, lineSeries.Marker, lineSeries.ShowShadow, context.Options.Zoom, isTooltipMarkerMode);

                            if (markerGeom != null)
                            {
                                #region Interaction
                                if (lineSeries.Core.Interaction != null)
                                {
                                    StiSeriesInteractionData data = new StiSeriesInteractionData();
                                    data.Fill(axisArea, lineSeries, index);
                                    markerGeom.Interaction = data;
                                }
                                #endregion

                                geom.CreateChildGeoms();
                                geom.ChildGeoms.Add(markerGeom);
                            }
                        }
                    }
                    index++;
                }
            }
        }

        protected List<StiSeriesInteractionData> GetInteractions(StiContext context, StiAreaGeom geom, PointF?[] points)
        {
            List<StiSeriesInteractionData> interactions = new List<StiSeriesInteractionData>();
            for (int index = 0; index < points.Length; index++)
            {
                StiSeriesInteractionData data = new StiSeriesInteractionData();
                data.Fill(geom.Area as IStiAxisArea, this.Series, index);
                data.Point = points[index];

                interactions.Add(data);
            }
            return interactions;
        }

        protected virtual void RenderLines(StiContext context, StiAreaGeom geom, PointF?[] pointsFrom, PointF?[] points)
        {
        }

        protected virtual void RenderAreas(StiContext context, StiAreaGeom geom, PointF?[] pointsFrom, PointF?[] points)
        {            
        }

        public override void RenderSeries(StiContext context, RectangleF rect, StiAreaGeom geom, IStiSeries[] series)
        {
            IStiArea area = geom.Area;

            if (series == null || series.Length == 0 || this.Series.Chart == null) return;

            bool isAnimationChangingValues = ((StiChart)this.Series.Chart).IsAnimationChangingValues;

            var axisArea = area as IStiAxisArea;

            bool getStartFromZero = axisArea.XAxis.Core.GetStartFromZero();

            rect.Width += 0.001f;

            #region Create List of Points, Render Areas
            float posY = 0;

            var pointLists = new List<PointF?[]>();
            var pointFromLists = isAnimationChangingValues? new List<PointF?[]>() : null;

            for (int seriesIndex = 0; seriesIndex < series.Length; seriesIndex++)
            {
                var currentSeries = series[seriesIndex] as IStiBaseLineSeries;

                int pointsCount = currentSeries.Values.Length;

                var points = new PointF?[pointsCount];
                var pointsFrom = isAnimationChangingValues ? new PointF?[pointsCount] : null;

                for (int pointIndex = 0; pointIndex < pointsCount; pointIndex++)
                {

                    var stripPoint = getStartFromZero ? pointIndex + 1 : pointIndex;

                    if (stripPoint >= axisArea.XAxis.Info.StripPositions.Length) break;

                    float posX = axisArea.XAxis.Info.StripPositions[stripPoint];

                    double? value = axisArea.ReverseHor ?
                            currentSeries.Values[currentSeries.Values.Length - pointIndex - 1] :
                            currentSeries.Values[pointIndex];

                    points[pointIndex] = GetPointValue(value, currentSeries, axisArea, posX);

                    #region Calculation Points From
                    if (isAnimationChangingValues)
                    {
                        double? valueFrom = 0;
                        if (currentSeries.ValuesStart.Length > pointIndex)
                        {
                            valueFrom = axisArea.ReverseHor ?
                                currentSeries.ValuesStart[currentSeries.ValuesStart.Length - pointIndex - 1] :
                                currentSeries.ValuesStart[pointIndex];
                        }

                        pointsFrom[pointIndex] = GetPointValue(valueFrom, currentSeries, axisArea, posX);
                    }
                    #endregion
                }

                if (points.Length > 0)
                {
                    points = OptimizePoints(points);

                    int startIndex;
                    int endIndex;
                    var newPointList = ClipLinePoints(context, geom, points, out startIndex, out endIndex);
                    var newPointFromList = isAnimationChangingValues ? ClipLinePoints(context, geom, pointsFrom, out startIndex, out endIndex) : null;

                    ((StiBaseLineSeriesCoreXF)currentSeries.Core).RenderAreas(context, geom, newPointFromList, newPointList);

                    if (!IsTopmostLine(currentSeries))
                    {
                        ((StiBaseLineSeriesCoreXF)currentSeries.Core).RenderLines(context, geom, newPointFromList, newPointList);
                    }
                }

                pointLists.Add(points);
                if (isAnimationChangingValues) pointFromLists.Add(pointsFrom);
            }        
            #endregion

            int index = 0;
            foreach (var pointList in pointLists)
            {
                int startIndex;
                int endIndex;
                var newPointList = ClipLinePoints(context, geom, pointList, out startIndex, out endIndex);
                var newPointFromList = isAnimationChangingValues ? ClipLinePoints(context, geom, pointFromLists[index], out startIndex, out endIndex) : null;

                var currentSeries = series[index] as IStiBaseLineSeries;

                if (IsTopmostLine(series[index]))
                {
                    ((StiBaseLineSeriesCoreXF)currentSeries.Core).RenderLines(context, geom, newPointFromList, newPointList);
                }
                
                #region Render Trend Line
                IStiTrendLine trendLine = ((StiSeries)currentSeries).TrendLine;

                if (trendLine != null)
                {
                    trendLine.Core.RenderTrendLine(geom, newPointList, axisArea.AxisCore.GetDividerY());
                }                 
                #endregion

                #region Render Series Labels
                IStiAxisSeriesLabels labels = currentSeries.Core.GetSeriesLabels();

                if (labels != null && labels.Visible)
                {
                    for (int pointIndex = startIndex; pointIndex <= endIndex; pointIndex++)
                    {
                        if (currentSeries.Values.Length > pointIndex)
                        {
                            double? value = currentSeries.Values[pointIndex];

                            if (value == null && currentSeries.ShowNulls)
                                value = 0d;

                            double? seriesValue = value;
                            if (axisArea.ReverseVert && value != null) 
                                seriesValue = -seriesValue;

                            if (currentSeries.YAxis == StiSeriesYAxis.LeftYAxis) posY = axisArea.AxisCore.GetDividerY();
                            else posY = axisArea.AxisCore.GetDividerRightY();

                            var endPoint = pointList.Length > pointIndex ? pointList[pointIndex] : null;

                            if (endPoint != null)
                            {

                                double? valueFrom = 0;
                                if (currentSeries.ValuesStart != null && currentSeries.ValuesStart.Length > pointIndex)
                                {
                                    valueFrom = currentSeries.ValuesStart[pointIndex];
                                }

                                var startPoint = new PointF(endPoint.Value.X, posY);

                                StiLabelAnimation animation = null;

                                if (isAnimationChangingValues)
                                {
                                    PointF? endPointFrom = pointFromLists[index].Length > pointIndex ? pointFromLists[index][pointIndex] : null;

                                    animation = new StiLabelAnimation(valueFrom, value, endPointFrom.GetValueOrDefault(), endPoint.GetValueOrDefault(), StiChartHelper.GlobalBeginTimeElement, TimeSpan.Zero);
                                }

                                if (labels.Step == 0 || (pointIndex % labels.Step == 0))
                                {
                                    int argumentIndex = axisArea.XAxis.StartFromZero ? pointIndex + 1 : pointIndex;
                                    var seriesLabelsGeom = ((StiAxisSeriesLabelsCoreXF)labels.Core).RenderLabel(currentSeries, context,
                                        CorrectPoint(endPoint.Value, rect, currentSeries.LabelsOffset * context.Options.Zoom),
                                        CorrectPoint(startPoint, rect, currentSeries.LabelsOffset * context.Options.Zoom),
                                        pointIndex, seriesValue, value,
                                        axisArea.AxisCore.GetArgumentLabel(axisArea.XAxis.Info.StripLines[argumentIndex], currentSeries),
                                        currentSeries.Core.GetTag(pointIndex),
                                        0, 1, rect, animation);

                                    if (seriesLabelsGeom != null)
                                    {
                                        geom.CreateChildGeoms();
                                        geom.ChildGeoms.Add(seriesLabelsGeom);

                                        seriesLabelsGeom.ClientRectangle = CheckLabelsRect(labels, geom, seriesLabelsGeom.ClientRectangle);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                index++;
            }
            if (geom.Area.Chart.SeriesLabels.PreventIntersection)
            {
                CheckIntersectionLabels(geom);
            }
        }

        private PointF? GetPointValue(double? value, IStiBaseLineSeries currentSeries, IStiAxisArea axisArea, float posX)
        {
            if (value == null && !currentSeries.ShowNulls) return null;
            if (value == null && currentSeries.ShowNulls) value = 0d;
            if (axisArea.ReverseVert && value != null) value = -value;

            double srY = 0;
            if (currentSeries.YAxis == StiSeriesYAxis.LeftYAxis)
            {
                srY = -value.GetValueOrDefault() * (float)axisArea.YAxis.Info.Dpi + axisArea.AxisCore.GetDividerY();
            }
            else
            {
                srY = -value.GetValueOrDefault() * (float)axisArea.YRightAxis.Info.Dpi + axisArea.AxisCore.GetDividerRightY();
            }

            return new PointF(posX, (float)srY);
        }

        private PointF?[] OptimizePoints(PointF?[] points)
        {
            if (points.Length < 800) return points;

            const float step = 1f;

            int index = 0;
            float minx = 0;
            float miny = 0;
            //float maxx = 0;
            float maxy = 0;

            List<PointF?> points2 = new List<PointF?>();

            while (index < points.Length)
            {
                if (points[index] == null)
                {
                    while ((index < points.Length) && (points[index] == null)) index++;
                    points2.Add(null);
                    continue;
                }

                PointF pf = points[index].Value;
                index++;
                if ((index < points.Length) && points[index] != null && (points[index] != null) && ((points[index].Value.X - pf.X) < step))
                {
                    minx = pf.X;
                    //maxx = pf.X;
                    miny = pf.Y;
                    maxy = pf.Y;
                    float firstY = pf.Y;
                    float lastY = pf.Y;

                    while ((index < points.Length) && points[index] != null && (points[index] != null) && (points[index].Value.X - minx) < step)
                    {
                        //maxx = Math.Max(maxx, points[index].Value.X);
                        miny = Math.Min(miny, points[index].Value.Y);
                        maxy = Math.Max(maxy, points[index].Value.Y);
                        lastY = points[index].Value.Y;
                        index++;
                    }

                    if (lastY > firstY)
                    {
                        points2.Add(new PointF?(new PointF(minx, miny)));
                        points2.Add(new PointF?(new PointF(minx + step / 2, maxy)));
                    }
                    else
                    {
                        points2.Add(new PointF?(new PointF(minx, maxy)));
                        points2.Add(new PointF?(new PointF(minx + step / 2, miny)));
                    }
                }
                else
                {
                    points2.Add(pf);
                }
            }

            return points2.ToArray();
        }

        private bool IsTopmostLine(IStiSeries series)
        {
            if (series is IStiSplineAreaSeries)
                return ((IStiSplineAreaSeries)series).TopmostLine;

            if (series is IStiSteppedAreaSeries)
                return ((IStiSteppedAreaSeries)series).TopmostLine;

            if (series is IStiAreaSeries)
                return ((IStiAreaSeries)series).TopmostLine;

            return true;
        }

        protected PointF CorrectPoint(PointF point, RectangleF rect, float correctY)
        {
            if (point.Y + correctY < 0) return new PointF(point.X, 0);
            if (point.Y + correctY > rect.Height) return new PointF(point.X, rect.Height);
            return new PointF(point.X, point.Y + correctY);
        }

        public override StiBrush GetSeriesBrush(int colorIndex, int colorCount)
        {
            IStiBaseLineSeries lineSeries = this.Series as IStiBaseLineSeries;

            StiBrush brush = base.GetSeriesBrush(colorIndex, colorCount);
            if (brush == null) return new StiSolidBrush(StiColorUtils.Dark(lineSeries.LineColor, 20));
            return brush;
        }

        public override object GetSeriesBorderColor(int colorIndex, int colorCount)
        {
            IStiBaseLineSeries lineSeries = this.Series as IStiBaseLineSeries;

            object color = base.GetSeriesBorderColor(colorIndex, colorCount);
            if (color == null) return lineSeries.LineColor;
            return color;
        }  
        #endregion

        public StiBaseLineSeriesCoreXF(IStiSeries series)
            : base(series)
        {
        }
	}
}
