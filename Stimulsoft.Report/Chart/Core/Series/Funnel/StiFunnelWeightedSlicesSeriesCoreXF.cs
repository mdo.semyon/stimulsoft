﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiFunnelWeightedSlicesSeriesCoreXF : StiSeriesCoreXF
    {
        #region Fields
        private IStiFunnelSeriesLabels labels;
        #endregion

        #region IStiApplyStyleSeries
        public override void ApplyStyle(IStiChartStyle style, Color color)
        {
            base.ApplyStyle(style, color);

            IStiFunnelWeightedSlicesSeries funnelSeries = this.Series as IStiFunnelWeightedSlicesSeries;

            if (funnelSeries.AllowApplyStyle)
            {
                funnelSeries.Brush = style.Core.GetColumnBrush(color);
                funnelSeries.BorderColor = style.Core.GetColumnBorder(color);
            }
        }
        #endregion

        #region Methods
        public override void RenderSeries(StiContext context, RectangleF rect, StiAreaGeom geom, IStiSeries[] seriesArray)
        {
            if (seriesArray == null || seriesArray.Length == 0 || this.Series.Chart == null) return;

            #region Calculate Colors
            int colorCount = 0;
            foreach (IStiSeries ser in seriesArray)
            {
                if (ser.Values != null) colorCount += ser.Values.Length;
            }
            if (colorCount == 0) return;
            #endregion

            #region Render FunnelElements
            int colorIndex = 0;
            foreach (IStiSeries ser in seriesArray)
            {
                IStiFunnelWeightedSlicesSeries funnelSeries = ser as IStiFunnelWeightedSlicesSeries;

                double[] values = GetValues(funnelSeries);

                this.labels = this.Series.Chart.SeriesLabels as IStiFunnelSeriesLabels;

                float singleValueHeight = rect.Height * 0.9f / GetSumValues();

                colorIndex = 0;

                float centerAxis;
                if (this.labels is StiOutsideLeftFunnelLabels)
                    centerAxis = rect.Width / 2 + rect.X;
                else
                    centerAxis = rect.Width / 2;

                var beginTime = StiChartHelper.GlobalBeginTimeElement;

                for (int index = 0; index < values.Length; index++)
                {
                    double value = values[index];

                    StiBrush seriesBrush = funnelSeries.Brush;
                    if (funnelSeries.AllowApplyBrush)
                    {
                        seriesBrush = funnelSeries.Core.GetSeriesBrush(colorIndex, colorCount);
                        seriesBrush = funnelSeries.ProcessSeriesBrushes(colorIndex, seriesBrush);
                    }

                    Color borderColor = funnelSeries.BorderColor;
                    if (funnelSeries.AllowApplyBorderColor)
                    {
                        borderColor = (Color)funnelSeries.Core.GetSeriesBorderColor(colorIndex, colorCount);
                    }

                    StiFunnelSeriesElementGeom funnelElementGeom = RenderFunnelElement(borderColor, seriesBrush, value, index, ser, geom, rect, singleValueHeight, new TimeSpan(beginTime.Ticks / ser.Values.Length * index));

                    if (funnelElementGeom != null)
                    {
                        #region Interaction
                        if (ser.Core.Interaction != null)
                        {
                            StiSeriesInteractionData data = new StiSeriesInteractionData();
                            data.Fill(geom.Area, ser, index);
                            funnelElementGeom.Interaction = data;
                        }
                        #endregion

                        geom.CreateChildGeoms();
                        geom.ChildGeoms.Add(funnelElementGeom);
                    }

                    colorIndex++;
                }

                #region Render Series Labels
                if (labels != null && labels.Visible)
                {
                    colorIndex = 0;
                    for (int pointIndex = 0; pointIndex < values.Length; pointIndex++)
                    {
                        if (ser.Values.Length > pointIndex)
                        {
                            double value = values[pointIndex];
                            double valueNext = (pointIndex == values.Length - 1) ? value : values[pointIndex + 1];

                            if (labels.Step == 0 || (pointIndex % labels.Step == 0))
                            {
                                StiSeriesLabelsGeom seriesLabelsGeom =
                                    ((StiFunnelSeriesLabelsCoreXF)labels.Core).RenderLabel(ser, context, pointIndex, value, valueNext,
                                    GetArgumentText(ser, pointIndex), ser.Core.GetTag(pointIndex), colorIndex, colorCount, rect, singleValueHeight, 0, centerAxis, out rect);

                                if (seriesLabelsGeom != null)
                                {
                                    geom.CreateChildGeoms();
                                    geom.ChildGeoms.Add(seriesLabelsGeom);

                                    seriesLabelsGeom.ClientRectangle = CheckLabelsRect(labels, geom, seriesLabelsGeom.ClientRectangle);
                                }
                            }
                        }
                        colorIndex++;
                    }
                }
                #endregion
            }
            #endregion
        }

        private double[] GetValues(IStiFunnelSeries funnelSeries)
        {
            List<double> values = new List<double>();

            foreach (double? value in funnelSeries.Values)
            {
                if (!funnelSeries.ShowZeros && value.GetValueOrDefault() == 0)
                    continue;

                values.Add(value.GetValueOrDefault());
            }

            return values.ToArray();
        }

        private string GetArgumentText(IStiSeries series, int index)
        {
            if (series.Arguments.Length > index && series.Arguments[index] != null)
            {
                return series.Arguments[index].ToString();
            }
            return string.Empty;
        }

        private StiFunnelSeriesElementGeom RenderFunnelElement(Color borderColor, StiBrush brush, double value, int index,
            IStiSeries currentSeries, StiAreaGeom geom, RectangleF rect, float singleValueHeight, TimeSpan beginTime)
        {
            List<StiSegmentGeom> path = MeasureFunnelElementCore(index, rect, singleValueHeight);

            return new StiFunnelSeriesElementGeom(geom, value, index, currentSeries, rect, brush, borderColor, path, beginTime);
        }

        private float GetSumValues()
        {
            double sumValues = 0;
            foreach (double value in this.Series.Values)
            {
                sumValues += Math.Abs(value);
            }
            return (float)sumValues;
        }

        private float GetSumLastValues(int indexCurrent)
        {
            float sumLastValues = 0;
            for (int index = 0; index < indexCurrent; index++)
            {
                if (index >= this.Series.Values.Length) break;
                sumLastValues += (float)Math.Abs(this.Series.Values[index].GetValueOrDefault());
            }
            return sumLastValues;
        }

        private List<StiSegmentGeom> MeasureFunnelElementCore(int index, RectangleF rect, float singleValueHeight)
        {
            List<StiSegmentGeom> path = new List<StiSegmentGeom>();
            float indent = rect.Height * 0.05f;

            float center;
            if (this.labels is StiOutsideLeftFunnelLabels)
                center = rect.Width / 2 + rect.X;
            else
                center = rect.Width / 2;

            PointF pointLeftTop;
            PointF pointRightTop;
            PointF pointLeftInflection = new PointF();
            PointF pointRightInflection = new PointF();
            PointF pointRightBottom;
            PointF pointLeftBottom;

            bool isInflection = false;

            float sumLastValues = GetSumLastValues(index);

            bool isInflectionValue = false;

            float valueHeight = rect.Height - singleValueHeight * sumLastValues;
            if (valueHeight < rect.Height / 5)
            {
                valueHeight = rect.Height / 5;
                isInflectionValue = true;
            }

            float valueWidth = rect.Width * 0.9f * valueHeight / rect.Height;

            pointLeftTop = new PointF(center - valueWidth / 2, singleValueHeight * sumLastValues + indent);
            pointRightTop = new PointF(center + valueWidth / 2, singleValueHeight * sumLastValues + indent);

            sumLastValues = GetSumLastValues(index + 1);

            valueHeight = rect.Height - singleValueHeight * sumLastValues;

            if (valueHeight < rect.Height / 5)
            {
                valueHeight = rect.Height / 5;
                if (!isInflectionValue)
                {
                    valueWidth = rect.Width * 0.9f * valueHeight / rect.Height;
                    pointLeftInflection = new PointF(center - valueWidth / 2, rect.Height + indent - valueHeight);
                    pointRightInflection = new PointF(center + valueWidth / 2, rect.Height + indent - valueHeight);
                    isInflection = true;
                }
            }

            valueWidth = rect.Width * 0.9f * valueHeight / rect.Height;

            pointRightBottom = new PointF(center + valueWidth / 2, singleValueHeight * sumLastValues + indent);
            pointLeftBottom = new PointF(center - valueWidth / 2, singleValueHeight * sumLastValues + indent);


            PointF[] points;
            if (isInflection)
                points = new PointF[] { pointLeftTop, pointRightTop, pointRightInflection, pointRightBottom, pointLeftBottom, pointLeftInflection };
            else
                points = new PointF[] { pointLeftTop, pointRightTop, pointRightBottom, pointLeftBottom };

            path.Add(new StiLinesSegmentGeom(points));
            path.Add(new StiCloseFigureSegmentGeom());
            return path;
        }
        #endregion

        #region Properties.Localization
        //<summary>
        //Gets a service name.
        //</summary>
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "FunnelWeightedSlices");
            }
        }
        #endregion

        public StiFunnelWeightedSlicesSeriesCoreXF(IStiSeries series)
            : base(series)
        {
        }
    }
}
