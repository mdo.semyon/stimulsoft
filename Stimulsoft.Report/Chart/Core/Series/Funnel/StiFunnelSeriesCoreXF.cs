﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using System.Collections.Generic;

namespace Stimulsoft.Report.Chart
{
    public class StiFunnelSeriesCoreXF : StiSeriesCoreXF
    {
        #region Fields
        private IStiFunnelSeriesLabels labels;
        #endregion

        #region IStiApplyStyleSeries
        public override void ApplyStyle(IStiChartStyle style, Color color)
        {
            base.ApplyStyle(style, color);

            IStiFunnelSeries funnelSeries = this.Series as IStiFunnelSeries;

            if (funnelSeries.AllowApplyStyle)
            {
                funnelSeries.Brush = style.Core.GetColumnBrush(color);
                funnelSeries.BorderColor = style.Core.GetColumnBorder(color);
            }
        }
        #endregion

        #region Methods
        public override void RenderSeries(StiContext context, RectangleF rect, StiAreaGeom geom, IStiSeries[] seriesArray)
        {
            if (seriesArray == null || seriesArray.Length == 0 || this.Series.Chart == null) return;

            #region Calculate Colors
            int colorCount = 0;
            foreach (IStiSeries ser in seriesArray)
            {
                if (ser.Values != null) colorCount += ser.Values.Length;
            }
            if (colorCount == 0) return;
            #endregion
            
            #region Render FunnelElements
            int colorIndex = 0;            
            foreach (IStiSeries ser in seriesArray)
            {                
                IStiFunnelSeries funnelSeries = ser as IStiFunnelSeries;

                double[] values = GetValues(funnelSeries);

                this.labels = this.Series.Chart.SeriesLabels as IStiFunnelSeriesLabels;

                float singleValueHeight = GetSingleValueHeight(values, rect);
                float singleValueWidth = GetSingleValueWidth(funnelSeries, rect);

                RectangleF measureRect = rect;
                RectangleF measureRectTemp = rect;

                #region Render Series Labels - First Pass
                if (labels != null && labels.Visible)
                {
                    colorIndex = 0;
                    for (int pointIndex = 0; pointIndex < values.Length; pointIndex++)
                    {
                        if (ser.Values.Length > pointIndex)
                        {
                            double value = values[pointIndex];
                            double valueNext = (pointIndex == values.Length - 1) ? value : values[pointIndex + 1];

                            if (labels.Step == 0 || (pointIndex % labels.Step == 0))
                            {
                                StiSeriesLabelsGeom seriesLabelsGeom =
                                    ((StiFunnelSeriesLabelsCoreXF)labels.Core).RenderLabel(ser, context, pointIndex, value, valueNext,
                                    GetArgumentText(ser, pointIndex), ser.Core.GetTag(pointIndex), colorIndex, colorCount, rect, singleValueHeight, singleValueWidth, 1, out measureRect);
                                measureRectTemp = measureRect.Width < measureRectTemp.Width ? measureRect : measureRectTemp;                               
                            }
                        }
                        colorIndex++;
                    }
                    measureRect = measureRectTemp;
                }
                #endregion

                colorIndex = 0;
                
                singleValueHeight = GetSingleValueHeight(values, measureRect);
                singleValueWidth = GetSingleValueWidth(funnelSeries, measureRect);

                float centerAxis;
                if (this.labels is StiOutsideLeftFunnelLabels)
                    centerAxis = measureRect.Width / 2 + measureRect.X;
                else
                    centerAxis = measureRect.Width / 2;

                var beginTime = StiChartHelper.GlobalBeginTimeElement;

                for (int index = 0; index < values.Length; index++)
                {
                    double value = values[index];
                    double valueNext = (index == values.Length - 1) ? value : values[index + 1];

                    StiBrush seriesBrush = funnelSeries.Brush;
                    if (funnelSeries.AllowApplyBrush)
                    {
                        seriesBrush = funnelSeries.Core.GetSeriesBrush(colorIndex, colorCount);
                        seriesBrush = funnelSeries.ProcessSeriesBrushes(colorIndex, seriesBrush);
                    }

                    Color borderColor = funnelSeries.BorderColor;
                    if (funnelSeries.AllowApplyBorderColor)
                    {
                        borderColor = (Color)funnelSeries.Core.GetSeriesBorderColor(colorIndex, colorCount);
                    }
                    var funnelElementGeom = RenderFunnelElement(borderColor, seriesBrush, value, valueNext, index, ser, geom, measureRect, singleValueHeight, singleValueWidth, new TimeSpan(beginTime.Ticks / ser.Values.Length * index));

                    if (funnelElementGeom != null)
                    {
                        #region Interaction
                        if (ser.Core.Interaction != null)
                        {
                            StiSeriesInteractionData data = new StiSeriesInteractionData();
                            data.Fill(geom.Area, ser, index);
                            funnelElementGeom.Interaction = data;
                        }
                        #endregion

                        geom.CreateChildGeoms();
                        geom.ChildGeoms.Add(funnelElementGeom);
                    }

                    colorIndex++;
                }

                #region Render Series Labels - Second Pass
                if (labels != null && labels.Visible)
                {
                    colorIndex = 0;
                    for (int pointIndex = 0; pointIndex < values.Length; pointIndex++)
                    {
                        if (ser.Values.Length > pointIndex)
                        {
                            double value = values[pointIndex];
                            double valueNext = (pointIndex == values.Length - 1) ? value : values[pointIndex + 1];

                            if (labels.Step == 0 || (pointIndex % labels.Step == 0))
                            {
                                StiSeriesLabelsGeom seriesLabelsGeom =
                                    ((StiFunnelSeriesLabelsCoreXF)labels.Core).RenderLabel(ser, context, pointIndex, value, valueNext,
                                    GetArgumentText(ser, pointIndex), ser.Core.GetTag(pointIndex), colorIndex, colorCount, rect, singleValueHeight, singleValueWidth, centerAxis, out measureRect);

                                if (seriesLabelsGeom != null)
                                {
                                    geom.CreateChildGeoms();
                                    geom.ChildGeoms.Add(seriesLabelsGeom);

                                    seriesLabelsGeom.ClientRectangle = CheckLabelsRect(labels, geom, seriesLabelsGeom.ClientRectangle);
                                }
                            }
                        }
                        colorIndex++;
                    }
                }
                #endregion
            }
            #endregion
        }

        private double[] GetValues(IStiFunnelSeries funnelSeries)
        {
            List<double> values = new List<double>();

            foreach (double? value in funnelSeries.Values)
            {
                if (!funnelSeries.ShowZeros && value.GetValueOrDefault() == 0)
                    continue;

                values.Add(value.GetValueOrDefault());
            }

            return values.ToArray();
        }

        private string GetArgumentText(IStiSeries series, int index)
        {
            if (series.Arguments.Length > index && series.Arguments[index] != null)
            {
                return series.Arguments[index].ToString();
            }
            return string.Empty;
        }

        private StiFunnelSeriesElementGeom RenderFunnelElement(Color borderColor, StiBrush brush, double value, double valueNext, int index,
            IStiSeries currentSeries, StiAreaGeom geom, RectangleF rect, float singleValueHeight, float singleValueWidth, TimeSpan beginTime)
        {
            List<StiSegmentGeom> path;
            
            MeasureFunnelElementCore(out path, value, valueNext, index, rect, singleValueHeight, singleValueWidth);
            
            return new StiFunnelSeriesElementGeom(geom, value, index, currentSeries, rect, brush, borderColor, path, beginTime);
        }

        private float GetSingleValueHeight(double[] values, RectangleF rect)
        {
            return rect.Height * 0.9f / values.Length;
        }

        private float GetSingleValueWidth(IStiFunnelSeries funnelSeries, RectangleF rect)
        {
            float maxValue = 0;
            foreach (double? value in funnelSeries.Values)
            {
                maxValue = maxValue > value.GetValueOrDefault() ? maxValue : (float)value.GetValueOrDefault();
            }            

            return rect.Width * 0.9f / maxValue;
        }

        private void MeasureFunnelElementCore(out List<StiSegmentGeom> path, double value, double valueNext, int index,
            RectangleF rect, float singleValueHeight, float singleValueWidth)
        {
            path = new List<StiSegmentGeom>();
            float indent = rect.Height * 0.05f;

            float center;
            if (this.labels is StiOutsideLeftFunnelLabels)
                center = rect.Width / 2 + rect.X;
            else
                center = rect.Width / 2;

            PointF pointLeftTop = new PointF(center - (float)value / 2 * singleValueWidth, singleValueHeight * index + indent);
            PointF pointRightTop = new PointF(center + (float)value / 2 * singleValueWidth, singleValueHeight * index + indent);

            PointF pointRightBottom = new PointF(center + (float)valueNext / 2 * singleValueWidth, singleValueHeight * (index + 1) + indent);
            PointF pointLeftBottom = new PointF(center - (float)valueNext / 2 * singleValueWidth, singleValueHeight * (index + 1) + indent);

            PointF[] points = new PointF[] { pointLeftTop, pointRightTop, pointRightBottom, pointLeftBottom };

            path.Add(new StiLinesSegmentGeom(points));
            path.Add(new StiCloseFigureSegmentGeom());
        }         
        #endregion

        #region Properties.Localization
         //<summary>
         //Gets a service name.
         //</summary>
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "Funnel");
            }
        }
        #endregion 

        public StiFunnelSeriesCoreXF(IStiSeries series)
            : base(series)
        {
        }
    }
}
