#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Context;
using Stimulsoft.Base.Drawing;
using System;
using System.Drawing;

namespace Stimulsoft.Report.Chart
{
    public class StiStripsCoreXF :
        IStiApplyStyle, 
        ICloneable
    {
        #region ICloneable
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion

        #region IStiApplyStyle
        /// <summary>
        /// Applying specified style to this area.
        /// </summary>
        /// <param name="style"></param>
        public void ApplyStyle(IStiChartStyle style)
        {
            if (this.Strips.AllowApplyStyle)
            {
                this.Strips.TitleColor = style.Core.ChartAreaBorderColor;
                this.Strips.StripBrush = new StiSolidBrush(Color.FromArgb(150, style.Core.BasicStyleColor));
            }
        }
        #endregion

        #region Methods.Render
        public void RenderXStrips(StiContext context, StiAxisAreaGeom geom, RectangleF rect)
        {
            IStiAxisArea area = Strips.Chart.Area as IStiAxisArea;
            if (area == null) return;
            
            #region Calculate coodrs
            float value = 0f;
            float.TryParse(Strips.MinValue, out value);

            float minLeft = area.AxisCore.GetDividerX() + value * (float)area.XAxis.Info.Dpi;

            value = 0f;
            float.TryParse(Strips.MaxValue, out value);

            float maxLeft = area.AxisCore.GetDividerX() + value * (float)area.XAxis.Info.Dpi;

            if (area.ReverseHor)
            {
                minLeft = rect.Width - minLeft;
                maxLeft = rect.Width - maxLeft;
            }

            if (minLeft > maxLeft)
            {
                float tmpTop = minLeft;
                minLeft = maxLeft;
                maxLeft = tmpTop;
            }
                        
            RectangleF stripRect = new RectangleF(minLeft, 0, maxLeft - minLeft, rect.Height);
            #endregion

            StiStripsXGeom stripGeom = new StiStripsXGeom(this.Strips, stripRect);

            geom.CreateChildGeoms();
            geom.ChildGeoms.Add(stripGeom);
        }

        public void RenderYStrips(StiContext context, StiAxisAreaGeom geom, RectangleF rect)
        {
            IStiAxisArea area = Strips.Chart.Area as IStiAxisArea;
            if (area == null) return;

            #region Calculate coodrs
            float value = 0f;
            float.TryParse(Strips.MinValue, out value);

            if (area.ReverseVert) value = -value;
            float minTop;
            if (Strips.Orientation == StiStrips.StiOrientation.Horizontal)
                minTop = area.AxisCore.GetDividerY() - value * (float)area.YAxis.Info.Dpi;
            else
                minTop = area.AxisCore.GetDividerRightY() - value * (float)area.YRightAxis.Info.Dpi;

            value = 0f;
            float.TryParse(Strips.MaxValue, out value);

            if (area.ReverseVert) value = -value;
            
            float maxTop;
            if (Strips.Orientation == StiStrips.StiOrientation.Horizontal)
                maxTop = area.AxisCore.GetDividerY() - value * (float)area.YAxis.Info.Dpi;
            else
                maxTop = area.AxisCore.GetDividerRightY() - value * (float)area.YRightAxis.Info.Dpi;

            if (minTop > maxTop)
            {
                float tmpTop = minTop;
                minTop = maxTop;
                maxTop = tmpTop;
            }

            RectangleF stripRect = new RectangleF(0, minTop, rect.Width, maxTop - minTop);
            #endregion

            StiStripsYGeom stripGeom = new StiStripsYGeom(this.Strips, stripRect);

            geom.CreateChildGeoms();
            geom.ChildGeoms.Add(stripGeom);
        }

        public void Render(StiContext context, StiAxisAreaGeom geom, RectangleF rect)
        {
            if (!Strips.Visible) return;

            if (Strips.Orientation == StiStrips.StiOrientation.Vertical)
                RenderXStrips(context, geom, rect);

            if (Strips.Orientation == StiStrips.StiOrientation.Horizontal ||
                Strips.Orientation == StiStrips.StiOrientation.HorizontalRight)
                RenderYStrips(context, geom, rect);
        }
        #endregion

        #region Properties
        private IStiStrips strips;
        public IStiStrips Strips
        {
            get
            {
                return strips;
            }
            set
            {
                strips = value;
            }
        }
        #endregion

        public StiStripsCoreXF(IStiStrips strips)
        {
            this.strips = strips;
        }
	}
}
