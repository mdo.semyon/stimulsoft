﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Context;
using Stimulsoft.Base.Localization;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Stimulsoft.Report.Chart
{
    public class StiTreemapAreaCoreXF : StiAreaCoreXF
    {
        #region Methods
        public override StiCellGeom Render(StiContext context, RectangleF rect)
        {   
            var treeAreaGeom = new StiTreemapAreaGeom(this.Area, rect);
            var seriesCollection = GetSeries();
            var mergedData = new List<double?>();

            foreach (var series in seriesCollection)
                mergedData.Add(series.Values.Sum());

            var rectSeries = new RectangleF(0, 0, rect.Width, rect.Height);

            var normalizeData = NormalizeDataForArea(mergedData, rectSeries.Width * rectSeries.Height);
            var rects = Squarify(normalizeData, new List<double?>(), rectSeries, new List<RectangleF>());

            for (var index = 0; index < seriesCollection.Count(); index++)
            {
                seriesCollection[index].Core.RenderSeries(context, rects[index], treeAreaGeom, seriesCollection.ToArray());
            }
            
            return treeAreaGeom;
        }
        

        private RectangleF CutArea(RectangleF container, double area)
        {
            RectangleF newcontainer;

            if (container.Width >= container.Height)
            {
                var areawidth = area / container.Height;
                var newwidth = container.Width - areawidth;
                newcontainer = new RectangleF(container.X + (float)areawidth, container.Y, (float)newwidth, container.Height);
            }
            else
            {
                var areaheight = area / container.Width;
                var newheight = container.Height - areaheight;
                newcontainer = new RectangleF(container.X, container.Y + (float)areaheight, container.Width, (float)newheight);
            }
            return newcontainer;
        }

        public List<RectangleF> Squarify(List<double?> data, List<double?> currentrow, RectangleF container, List<RectangleF> stack)
        {
            if (data.Count == 0)
            {
                stack.AddRange(GetCoordinates(container, currentrow));
                return stack;
            }
            
            var length = Math.Min(container.Width, container.Height);
            var nextdatapoint = data[0];

            if (ImprovesRatio(currentrow, nextdatapoint, length))
            {
                currentrow.Add(nextdatapoint);
                Squarify(data.Skip(1).ToList(), currentrow, container, stack);
            }
            else
            {
                var newcontainer = CutArea(container, currentrow.Sum().GetValueOrDefault());//?stack
                stack.AddRange(GetCoordinates(container, currentrow));
                var newRow = new List<double?>();
                Squarify(data, newRow, newcontainer, stack);
            }
            return stack;
        }

        private bool ImprovesRatio(List<double?> currentrow, double? nextnode, float length)
        {
            if (currentrow.Count == 0)
            {
                return true;
            }

            var newrow = currentrow.ToList();
            newrow.Add(nextnode);

            var currentratio = CalculateRatio(currentrow, length);
            var newratio = CalculateRatio(newrow, length);
            
            return currentratio >= newratio;
        }

        private double CalculateRatio(List<double?> row, float length)
        {
            var sum = row.Sum().GetValueOrDefault();
            var v1 = Math.Pow(length, 2) * row.Max() / Math.Pow(sum, 2);
            var v2 = Math.Pow(sum, 2) / (Math.Pow(length, 2) * row.Min());
            return Math.Max(v1.GetValueOrDefault(), v2.GetValueOrDefault());
        }

        public List<double?> NormalizeDataForArea(List<double?> data, float area)
        {
            var normalizeddata = new List<double?>();
            var sum = data.Sum();
            var multiplier = area / sum;
            
            for (var index = 0; index < data.Count; index++)
            {
                normalizeddata.Add(data[index] * multiplier);
            }
            return normalizeddata;
        }

        private List<RectangleF> GetCoordinates(RectangleF rootContainer, List<double?> rowData)
        {
            var coordinates = new List<RectangleF>();
            var subxoffset = rootContainer.X;
            var subyoffset = rootContainer.Y;
            var areawidth = rowData.Sum() / rootContainer.Height;
            var areaheight = rowData.Sum() / rootContainer.Width;

            if (rootContainer.Width >= rootContainer.Height)
            {
                for (var i = 0; i < rowData.Count; i++)
                {
                    coordinates.Add(new RectangleF(subxoffset, subyoffset, /*subxoffset +*/ (float)areawidth, /*subyoffset +*/ (float)(rowData[i] / areawidth)));
                    subyoffset = subyoffset + (float)(rowData[i] / areawidth);
                }
            }
            else
            {
                for (var i = 0; i < rowData.Count; i++)
                {
                    coordinates.Add(new RectangleF(subxoffset, subyoffset, /*subxoffset +*/ (float)(rowData[i] / areaheight), /*subyoffset +*/ (float)areaheight));
                    subxoffset = subxoffset + (float)(rowData[i] / areaheight);
                }
            }
            return coordinates;
        }

        protected override void PrepareInfo(RectangleF rect)
        {
        }

        #endregion

        #region Properties.Localization
        /// <summary>
        /// Gets a service name.
        /// </summary>
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "Treemap");
            }
        }
        #endregion

        #region Properties.Settings
        public override int Position
        {
            get
            {
                return (int)StiChartAreaPosition.Treemap;
            }
        }
        #endregion  

        public StiTreemapAreaCoreXF(IStiArea area)
            : base(area)
        {
        }
    }
}
