﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using System.Drawing;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF26 : StiStyleCoreXF22
    {
        #region Properties.Localization
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "Style") + "26";
            }
        }
        #endregion

        #region Properties
        
        public override Color[] StyleColors
        {
            get
            {
                return new Color[]
                {
                    ColorTranslator.FromHtml("#2ec6c8"),
                    ColorTranslator.FromHtml("#b5a1dd"),
                    ColorTranslator.FromHtml("#5ab0ee"),
                    ColorTranslator.FromHtml("#f4984e"),
                    ColorTranslator.FromHtml("#d77a80"),
                    ColorTranslator.FromHtml("#d04456"),
                };
            }
        }

        public override StiBrush ChartAreaBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ffffff"));
            }
        }

        #region Legend
        public override bool LegendShowShadow
        {
            get
            {
                return false;
            }
        }

        public override Color LegendBorderColor
        {
            get
            {
                return StiColorUtils.Dark(BasicStyleColor, 30);
            }
        }
        #endregion

        #region SeriesLabels
        public override StiBrush SeriesLabelsBrush
        {
            get
            {
                return new StiSolidBrush(Color.Transparent);
            }
        }

        public override Color SeriesLabelsColor
        {
            get
            {
                return ColorTranslator.FromHtml("#33475B");
            }
        }

        public override Color SeriesLabelsBorderColor
        {
            get
            {
                return Color.Transparent;
            }
        }

        public override Font SeriesLabelsFont
        {
            get
            {
                return new Font("Arial", 10);
            }
        }
        #endregion

        #region Series
        public override bool SeriesLighting
        {
            get
            {
                return false;
            }
        }

        public override bool SeriesShowShadow
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region Marker
        public override bool MarkerVisible
        {
            get
            {
                return false;
            }
        }
        #endregion

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle26;
            }
        }
        #endregion

        public override Color GetColumnBorder(Color color)
        {
            return Color.Transparent;
        }
    }
}