#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF01 : StiStyleCoreXF
	{
		#region Properties.Localization
		/// <summary>
		/// Gets a localized name of style.
		/// </summary>
		public override string LocalizedName
		{
			get
			{
				return StiLocalization.Get("Chart", "Style") + "01";
			}
		}
		#endregion

        #region Properties
		private Color[] styleColor = new Color[]
		{
			Color.FromArgb(0xFF, 0xC2, 0x75, 0x35),
			Color.FromArgb(0xFF, 0xE7, 0x8C, 0x41),
			Color.FromArgb(0xFF, 0xF8, 0xAA, 0x79),
			Color.FromArgb(0xFF, 0xFA, 0xCB, 0xB4),
			Color.FromArgb(0xFF, 0xFD, 0xE6, 0xDC)
		};

		public override Color[] StyleColors
		{
			get
			{
				return styleColor;
			}
		}

		public override Color BasicStyleColor
		{
			get
			{
				return Color.Wheat;
			}
		}

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle01;
            }
        }
        #endregion

		#region Methods
		public override StiBrush GetColumnBrush(Color color)
		{
			return new StiGlareBrush(StiColorUtils.Dark(color, 50), color, 0);
		}
		#endregion
	}
}