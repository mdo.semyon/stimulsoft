#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF03 : StiStyleCoreXF
    {
        #region Properties.Localization
        /// <summary>
        /// Gets a localized name of style.
		/// </summary>
        public override string LocalizedName
		{
			get
			{
				return StiLocalization.Get("Chart", "Style") + "03";
			}
		}
		#endregion

        #region Properties
		private Color[] styleColor = new Color[]
		{
			Color.FromArgb(0xFF, 0x96, 0x3D, 0x3B),
			Color.FromArgb(0xFF, 0xB3, 0x4A, 0x47),
			Color.FromArgb(0xFF, 0xC9, 0x7E, 0x7D),
			Color.FromArgb(0xFF, 0xDD, 0xB6, 0xB5),
			Color.FromArgb(0xFF, 0xEE, 0xDD, 0xDD)
		};

		public override Color[] StyleColors
		{
			get
			{
				return styleColor;
			}
		}       

		public override Color BasicStyleColor
		{
			get
			{
				return Color.WhiteSmoke;
			}
		}


        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle03;
            }
        }
        #endregion		

		#region Methods
		public override StiBrush GetColumnBrush(Color color)
		{
			return new StiSolidBrush(color);
		}
		#endregion
	}
}