#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;


namespace Stimulsoft.Report.Chart
{
	public class StiCustomStyleCoreXF : StiStyleCoreXF01
	{
        #region Properties.Localization
        /// <summary>
        /// Gets a service name.
        /// </summary>
        public override string LocalizedName
        {
            get
            {
                return "CustomStyle";
            }
        }
        #endregion

        #region Fields
        public Stimulsoft.Report.StiChartStyle ReportChartStyle = null;
        #endregion

        #region Properties

        #region Area
        public override StiBrush ChartAreaBrush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.ChartAreaBrush;
                return base.ChartAreaBrush;
            }
        }

        public override Color ChartAreaBorderColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.ChartAreaBorderColor;
                return base.ChartAreaBorderColor;
            }
        }

        public override bool ChartAreaShowShadow
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.ChartAreaShowShadow;
                return base.ChartAreaShowShadow;
            }
        }
        #endregion

        #region Series

        public override bool SeriesLighting
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.SeriesLighting;
                return base.SeriesLighting;
            }
        }

        public override bool SeriesShowShadow
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.SeriesShowShadow;
                return base.SeriesShowShadow;
            }
        }

        #endregion

        #region SeriesLabels
        public override StiBrush SeriesLabelsBrush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.SeriesLabelsBrush;
                return base.SeriesLabelsBrush;
            }
        }

        public override Color SeriesLabelsColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.SeriesLabelsColor;
                return base.SeriesLabelsColor;
            }
        }

        public override Color SeriesLabelsBorderColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.SeriesLabelsBorderColor;
                return base.SeriesLabelsBorderColor;
            }
        }
        #endregion

        #region Trend Line
        public override Color TrendLineColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.TrendLineColor;
                return base.TrendLineColor;
            }
        }

        public override bool TrendLineShowShadow
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.TrendLineShowShadow;
                return base.TrendLineShowShadow;
            }
        }
        #endregion

        #region Legend
        public override StiBrush LegendBrush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.LegendBrush;
                return base.LegendBrush;
            }
        }

        public override Color LegendLabelsColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.LegendLabelsColor;
                return base.LegendLabelsColor;
            }
        }

        public override Color LegendBorderColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.LegendBorderColor;
                return base.LegendBorderColor;
            }
        }

        public override Color LegendTitleColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.LegendTitleColor;
                return base.LegendTitleColor;
            }
        }
        #endregion

        #region Marker
        public override bool MarkerVisible
    {
            get
            {
                if (ReportStyle != null) return ReportStyle.MarkerVisible;
                return base.MarkerVisible;
            }
        }
        #endregion

        #region Axis
        public override Color AxisTitleColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.AxisTitleColor;
                return base.AxisTitleColor;
            }
        }

        public override Color AxisLineColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.AxisLineColor;
                return base.AxisLineColor;
            }
        }

        public override Color AxisLabelsColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.AxisLabelsColor;
                return base.AxisLabelsColor;
            }
        }
	    #endregion

        #region Interlacing
        public override StiBrush InterlacingHorBrush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.InterlacingHorBrush;
                return base.InterlacingHorBrush;
            }
        }

        public override StiBrush InterlacingVertBrush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.InterlacingVertBrush;
                return base.InterlacingVertBrush;
            }
        }
        #endregion

        #region GridLines
        public override Color GridLinesHorColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.GridLinesHorColor;
                return base.GridLinesHorColor;
            }
        }

        public override Color GridLinesVertColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.GridLinesVertColor;
                return base.GridLinesVertColor;
            }
        }
        #endregion

        public override Color[] StyleColors
		{
            get
            {
                if (ReportStyle != null) return ReportStyle.StyleColors;
                return base.StyleColors;
            }
		}

		public override Color BasicStyleColor
		{
            get
            {
                if (ReportStyle != null) return ReportStyle.BasicStyleColor;
                return base.BasicStyleColor;
            }
		}

		private string reportStyleName = null;
		public string ReportStyleName
		{
			get
			{
				return reportStyleName;
			}
			set
			{
				reportStyleName = value;
			}
		}

        public Stimulsoft.Report.StiChartStyle ReportStyle
        {
            get
            {
				string styleName = this.ReportStyleName == null ? ((StiChart)this.Chart).CustomStyleName : this.ReportStyleName;

				if (Chart == null || ((StiChart)Chart).Report == null || styleName == null || styleName.Length == 0)
                    return null;

				var style = ((StiChart)Chart).Report.Styles[styleName] as Stimulsoft.Report.StiChartStyle;
                return style;
            }
        }

        private StiCustomStyle customStyle;
        public StiCustomStyle CustomStyle
        {
            get
            {
                return customStyle;
            }
        }
        #endregion

		#region Methods
		public override StiBrush GetColumnBrush(Color color)
		{
			if (this.ReportStyle != null)
			{
				switch (this.ReportStyle.BrushType)
				{
					case StiBrushType.Glare:
						return new StiGlareBrush(StiColorUtils.Dark(color, 50), color, 0);

					case StiBrushType.Gradient0:
						return new StiGradientBrush(StiColorUtils.Dark(color, 50), color, 0);

					case StiBrushType.Gradient90:
						return new StiGradientBrush(StiColorUtils.Dark(color, 50), color, 90);

					case StiBrushType.Gradient180:
						return new StiGradientBrush(StiColorUtils.Dark(color, 50), color, 180);

					case StiBrushType.Gradient270:
						return new StiGradientBrush(StiColorUtils.Dark(color, 50), color, 270);

					case StiBrushType.Gradient45:
						return new StiGradientBrush(StiColorUtils.Dark(color, 50), color, 45);

					case StiBrushType.Solid:
						return new StiSolidBrush(color);
				}
			}
			
			return new StiSolidBrush(color);
		}
		#endregion

        public StiCustomStyleCoreXF(StiCustomStyle customStyle)
        {
            this.customStyle = customStyle;
        }
	}
}