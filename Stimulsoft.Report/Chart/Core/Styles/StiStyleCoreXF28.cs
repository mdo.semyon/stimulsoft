﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF28 : StiStyleCoreXF26
    {
        #region Properties.Localization
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "Style") + "28";
            }
        }
        #endregion

        #region Properties
        public override Color[] StyleColors
        {
            get
            {
                return new Color[]
                {
                    ColorTranslator.FromHtml("#165d9e"),
                    ColorTranslator.FromHtml("#577eb6"),
                    ColorTranslator.FromHtml("#569436"),
                    ColorTranslator.FromHtml("#225056"),
                    ColorTranslator.FromHtml("#d4dae0")
                };
            }
        }

        public override StiBrush ChartBrush
        {
            get
            {
                return new StiSolidBrush(StiColorUtils.Light(ColorTranslator.FromHtml("#0a325a"), 30));
            }
        }

        public override StiBrush ChartAreaBrush
        {
            get
            {
                return
                    new StiSolidBrush(ColorTranslator.FromHtml("#0a325a"));
            }
        }

        #region Axis
        public override Color AxisTitleColor
        {
            get
            {
                return StiColorUtils.Dark(BasicStyleColor, 50);
            }
        }

        public override Color AxisLineColor
        {
            get
            {
                return StiColorUtils.Dark(BasicStyleColor, 50);
            }
        }

        public override Color AxisLabelsColor
        {
            get
            {
                return StiColorUtils.Dark(BasicStyleColor, 50);
            }
        }
        #endregion

        #region SeriesLabels
        public override Color SeriesLabelsColor
        {
            get
            {
                return Color.White;
            }
        }
        #endregion

        #region Legend
        public override StiBrush LegendBrush
        {
            get
            {
                return new StiSolidBrush(Color.Transparent);
            }
        }

        public override Color LegendLabelsColor
        {
            get
            {
                return Color.White;
            }
        }

        public override Color LegendBorderColor
        {
            get
            {
                return Color.Transparent;
            }
        }

        public override Color LegendTitleColor
        {
            get
            {
                return Color.White;
            }
        }

        public override bool LegendShowShadow
        {
            get
            {
                return false;
            }
        }

        public override Font LegendFont
        {
            get
            {
                return new Font("Arial", 9);
            }
        }
        #endregion

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle28;
            }
        }
        #endregion
    }
}