#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.Drawing.Drawing2D;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF18 : StiStyleCoreXF
    {
        #region Properties.Localization
        /// <summary>
        /// Gets a localized name of style.
		/// </summary>
        public override string LocalizedName
		{
			get
			{
				return StiLocalization.Get("Chart", "Style") + "18";
			}
		}
		#endregion

		#region Methods
        public override void FillColumn(StiContext context, RectangleF rect, StiBrush brush, StiInteractionDataGeom interaction)
		{
			HatchStyle style = HatchStyle.Cross;
			StiSolidBrush solidBrush = brush as StiSolidBrush;
			if (solidBrush.Color == Color.White)
			{
				style = HatchStyle.BackwardDiagonal;
			}
			else if (solidBrush.Color == Color.Black)
			{
				style = HatchStyle.DottedGrid;
			}
			else if (solidBrush.Color == Color.Silver)
			{
				style = HatchStyle.Horizontal;
			}
			else if (solidBrush.Color == Color.Red)
			{
				style = HatchStyle.Percent25;
			}
			else if (solidBrush.Color == Color.Green)
			{
				style = HatchStyle.OutlinedDiamond;
			}
			else if (solidBrush.Color == Color.Blue)
			{
				style = HatchStyle.ForwardDiagonal;
			}

            StiHatchBrush br = new StiHatchBrush(style, Color.Black, Color.White);
            context.FillRectangle(br, rect.X, rect.Y, rect.Width, rect.Height, interaction);
			
		}

		public override StiBrush GetColumnBrush(Color color)
		{
			return new StiSolidBrush(color);
		}
		#endregion

		#region Properties
		private Color[] styleColor = new Color[]
		{
			Color.White,
			Color.Black,
			Color.Silver,
			Color.Red,
			Color.Green,
			Color.Blue
		};

		public override Color[] StyleColors
		{
			get
			{
				return styleColor;
			}
		}

		public override Color BasicStyleColor
		{
			get
			{
				return Color.White;
			}
		}

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle18;
            }
        }
        #endregion
	}
}