﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF25 : StiStyleCoreXF22
    {
        #region Properties.Localization
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "Style") + "25";
            }
        }
        #endregion

        #region Properties

        private Color[] styleColor = new Color[]
        {
            ColorTranslator.FromHtml("#70ad47"),
            ColorTranslator.FromHtml("#4472c4"),
            ColorTranslator.FromHtml("#ffc000"),
            ColorTranslator.FromHtml("#43682b"),
            ColorTranslator.FromHtml("#fd6a37"),
            ColorTranslator.FromHtml("#997300")
        };

        public override Color[] StyleColors
        {
            get
            {
                return styleColor;
            }
        }

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle25;
            }
        }

        #region Legend
        public override bool LegendShowShadow
        {
            get
            {
                return false;
            }
        }

        public override Color LegendBorderColor
        {
            get
            {
                return StiColorUtils.Dark(BasicStyleColor, 30);
            }
        }
        #endregion

        #region SeriesLabels
        public override StiBrush SeriesLabelsBrush
        {
            get
            {
                return new StiSolidBrush(Color.Transparent);
            }
        }

        public override Color SeriesLabelsColor
        {
            get
            {
                return ColorTranslator.FromHtml("#33475B");
            }
        }

        public override Color SeriesLabelsBorderColor
        {
            get
            {
                return Color.Transparent;
            }
        }

        public override Font SeriesLabelsFont
        {
            get
            {
                return new Font("Arial", 10);
            }
        }
        #endregion

        #region Series
        public override bool SeriesLighting
        {
            get
            {
                return false;
            }
        }

        public override bool SeriesShowShadow
        {
            get
            {
                return false;
            }
        }
        #endregion
        #endregion
    }
}
