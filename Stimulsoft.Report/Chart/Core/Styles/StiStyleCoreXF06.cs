#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF06 : StiStyleCoreXF
    {
        #region Properties.Localization
        /// <summary>
        /// Gets a localized name of style.
		/// </summary>
        public override string LocalizedName
		{
			get
			{
				return StiLocalization.Get("Chart", "Style") + "06";
			}
		}
		#endregion

        #region Properties
		private Color[] styleColor = new Color[]
		{
			Color.FromArgb(0xff, 0xCE, 0xB9, 0x66),
			Color.FromArgb(0xff, 0x9C, 0xB0, 0x84),
			Color.FromArgb(0xff, 0x6B, 0xB1, 0xC9),
			Color.FromArgb(0xff, 0x65, 0x85, 0xCF),
			Color.FromArgb(0xff, 0x7E, 0x6B, 0xC9),
			Color.FromArgb(0xff, 0xA3, 0x79, 0xBB)
		};

		public override Color[] StyleColors
		{
			get
			{
				return styleColor;
			}
		}

		public override Color BasicStyleColor
		{
			get
			{
				return Color.FromArgb(0xff, 0xCA, 0xC6, 0xCE);
			}
		}

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle06;
            }
        }
        #endregion

		#region Methods
		public override StiBrush GetColumnBrush(Color color)
		{
			return new StiSolidBrush(color);
		}
		#endregion

	}
}