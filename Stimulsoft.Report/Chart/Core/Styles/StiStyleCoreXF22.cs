﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF22 : StiStyleCoreXF
    {
        #region Properties.Localization
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "Style") + "22";
            }
        }
        #endregion

        #region Properties
        #region Chart
        public override StiBrush ChartBrush
        {
            get
            {
                return new StiSolidBrush(StiColorUtils.Light(BasicStyleColor, 100));
            }
        }

        public override StiBrush ChartAreaBrush
        {
            get
            {
                return
                    new StiSolidBrush(BasicStyleColor);
            }
        }

        public override Color ChartAreaBorderColor
        {
            get
            {
                return ColorTranslator.FromHtml("#abacad");
            }
        }
        #endregion

        #region SeriesLabels
        public override StiBrush SeriesLabelsBrush
        {
            get
            {
                return new StiSolidBrush(StiColorUtils.Light(BasicStyleColor, 100));
            }
        }

        public override Color SeriesLabelsColor
        {
            get
            {
                return ColorTranslator.FromHtml("#5a5a5a");
            }
        }

        public override Color SeriesLabelsBorderColor
        {
            get
            {
                return ColorTranslator.FromHtml("#8c8c8c");
            }
        }
        #endregion

        #region Legend
        public override StiBrush LegendBrush
        {
            get
            {
                return new StiSolidBrush(StiColorUtils.Light(BasicStyleColor, 100));
            }
        }

        public override Color LegendLabelsColor
        {
            get
            {
                return ColorTranslator.FromHtml("#8c8c8c");
            }
        }
        #endregion

        #region Axis
        public override Color AxisTitleColor
        {
            get
            {
                return ColorTranslator.FromHtml("#8c8c8c");
            }
        }

        public override Color AxisLineColor
        {
            get
            {
                return ColorTranslator.FromHtml("#8c8c8c");
            }
        }

        public override Color AxisLabelsColor
        {
            get
            {
                return ColorTranslator.FromHtml("#8c8c8c");
            }
        }
        #endregion

        #region GridLines
        public override Color GridLinesHorColor
        {
            get
            {
                return Color.FromArgb(50, StiColorUtils.Dark(BasicStyleColor, 150));
            }
        }

        public override Color GridLinesVertColor
        {
            get
            {
                return Color.FromArgb(50, StiColorUtils.Dark(BasicStyleColor, 150));
            }
        }
        #endregion

        private Color[] styleColor = new Color[]
        {
            ColorTranslator.FromHtml("#5b9bd5"),
            ColorTranslator.FromHtml("#ed7d31"),
            ColorTranslator.FromHtml("#9f9f9f"),
            ColorTranslator.FromHtml("#ffc000"),
            ColorTranslator.FromHtml("#4472c4"),
            ColorTranslator.FromHtml("#70ad47")
        };

        public override Color[] StyleColors
        {
            get
            {
                return styleColor;
            }
        }

        public override Color BasicStyleColor
        {
            get
            {
                return Color.White;
            }
        }

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle22;
            }
        }
        #endregion

        #region Methods
        public override StiBrush GetColumnBrush(Color color)
        {
            return new StiSolidBrush(color);
        }

        public override Color GetColumnBorder(Color color)
        {
            return StiColorUtils.Light(color, 255);
        }
        #endregion
    }
}