#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF04 : StiStyleCoreXF
    {
        #region Properties.Localization
        /// <summary>
        /// Gets a localized name of style.
		/// </summary>
        public override string LocalizedName
		{
			get
			{
				return StiLocalization.Get("Chart", "Style") + "04";
			}
		}
		#endregion

        #region Properties
		private Color[] styleColor = new Color[]
		{
			Color.FromArgb(0xFF, 0x48, 0x39, 0x5A),
			Color.FromArgb(0xFF, 0x6D, 0x57, 0x89),
			Color.FromArgb(0xFF, 0x92, 0x7B, 0xAD),
			Color.FromArgb(0xFF, 0xB5, 0xA6, 0xC8)
		};

		public override Color BasicStyleColor
		{
			get
			{
				return Color.FromArgb(0xFF, 0xB5, 0xA6, 0xC8);
			}
		}

		public override Color[] StyleColors
		{
			get
			{
				return styleColor;
			}
		}

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle04;
            }
        }
        #endregion
    }
}