﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;

using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Serializing;

namespace Stimulsoft.Report.Chart
{
	public abstract class StiStyleCoreXF
	{
        #region Properties.Localization
        /// <summary>
        /// Gets a localized name of style.
        /// </summary>
        public abstract string LocalizedName
        {
            get;
        }
        #endregion

        #region Properties
        [StiNonSerialized]
        public virtual StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle01;
            }
        }
        #endregion

        #region Properties
        #region Chart
        public virtual StiBrush ChartBrush
		{
			get
			{
				return new StiSolidBrush(StiColorUtils.Light(BasicStyleColor, 100));
			}
		}

		public virtual StiBrush ChartAreaBrush
		{
			get
			{
				return  
					new StiGradientBrush(
					StiColorUtils.Light(BasicStyleColor, 80),
					StiColorUtils.Light(BasicStyleColor, 40),
					90f);
			}
		}

		public virtual Color ChartAreaBorderColor
		{
			get
			{
				return StiColorUtils.Dark(BasicStyleColor, 150);
			}
        }

        private IStiChart chart = null;
        [Browsable(false)]
        public IStiChart Chart
        {
            get
            {
                return chart;
            }
            set
            {
                chart = value;
            }
        }

        public virtual bool ChartAreaShowShadow
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region SeriesLabels
        public virtual StiBrush SeriesLabelsBrush
		{
			get
			{
				return new StiSolidBrush(BasicStyleColor);
			}
		}

		public virtual Color SeriesLabelsColor
		{
			get
			{
				return StiColorUtils.Dark(BasicStyleColor, 150);
			}
		}

		public virtual Color SeriesLabelsBorderColor
		{
			get
			{
				return StiColorUtils.Dark(BasicStyleColor, 150);
			}
		}

        public virtual Font SeriesLabelsFont
        {
            get
            {
                return new Font("Arial", 7);
            }
        }
        #endregion

        #region Trend Line
        public virtual Color TrendLineColor
        {
            get
            {
                return StiColorUtils.Dark(BasicStyleColor, 150);
            }
        }

        public virtual bool TrendLineShowShadow
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region Legend
        public virtual StiBrush LegendBrush
		{
			get
			{
				return new StiGradientBrush(
					StiColorUtils.Light(BasicStyleColor, 80),
					StiColorUtils.Light(BasicStyleColor, 20),
					90f);
			}
		}

		public virtual Color LegendLabelsColor
		{
			get
			{
				return StiColorUtils.Dark(BasicStyleColor, 150);
			}
		}

		public virtual Color LegendBorderColor
		{
			get
			{
				return StiColorUtils.Dark(BasicStyleColor, 100);
			}
		}

		public virtual Color LegendTitleColor
		{
			get
			{
				return StiColorUtils.Dark(BasicStyleColor, 150);
			}
		}

        public virtual bool LegendShowShadow { get; }

        public virtual Font LegendFont
        {
            get
            {
                return new Font("Arial", 8);
            }
        }

        #endregion

        #region Axis
        public virtual Color AxisTitleColor
		{
			get
			{
				return StiColorUtils.Dark(BasicStyleColor, 150);
			}
		}

		public virtual Color AxisLineColor
		{
			get
			{
				return StiColorUtils.Dark(BasicStyleColor, 150);
			}
		}

		public virtual Color AxisLabelsColor
		{
			get
			{
				return StiColorUtils.Dark(BasicStyleColor, 150);
			}
		}
        #endregion

		#region Interlacing
		public virtual StiBrush InterlacingHorBrush
		{
			get
			{
				return new StiSolidBrush(Color.FromArgb(10, StiColorUtils.Dark(BasicStyleColor, 100)));
			}
		}

		public virtual StiBrush InterlacingVertBrush
		{
			get
			{
				return new StiSolidBrush(Color.FromArgb(10, StiColorUtils.Dark(BasicStyleColor, 100)));
			}
		}
		#endregion	

		#region GridLines
		public virtual Color GridLinesHorColor
		{
			get
			{
				return Color.FromArgb(100, StiColorUtils.Dark(BasicStyleColor, 150));
			}
		}

		public virtual Color GridLinesVertColor
		{
			get
			{
				return Color.FromArgb(100, StiColorUtils.Dark(BasicStyleColor, 150));
			}
		}
        #endregion

        #region Series
        public virtual bool SeriesLighting
        {
            get
            {
                return true;
            }
        }

        public virtual bool SeriesShowShadow { get; }
        #endregion

        #region Marker
        public virtual bool MarkerVisible
        {
            get
            {
                return true;
            }
        }
        #endregion

        public virtual Color FirstStyleColor
		{
			get
			{
				return StyleColors[0];
			}
		}

		public virtual Color LastStyleColor
		{
			get
			{
				return StyleColors[StyleColors.Length - 1];
			}
		}
		
		public abstract Color[] StyleColors
		{
			get;
		}

		public abstract Color BasicStyleColor
		{
			get;
		}
        #endregion		

		#region Methods
        public virtual void FillColumn(StiContext context, RectangleF rect, StiBrush brush, StiInteractionDataGeom interaction)
		{
            context.FillRectangle(brush, rect.X, rect.Y, rect.Width, rect.Height, interaction);
		}

		public virtual StiBrush GetAreaBrush(Color color)
		{
			return new StiSolidBrush(Color.FromArgb(200, color));
		}

		public virtual StiBrush GetColumnBrush(Color color)
		{
			return new StiGradientBrush(color, StiColorUtils.Dark(color, 50), 0); ;
		}

		public virtual Color GetColumnBorder(Color color)
		{
			return StiColorUtils.Dark(color, 100);
		}
        
		public virtual Color[] GetColors(int seriesCount)
		{
			var colors = new Color[seriesCount];
			var styleColors = StyleColors;
            
			int styleColorIndex = 0;

			int dist = 0;

			for (int colorIndex = 0; colorIndex < seriesCount; colorIndex++)
			{
				if (dist != 0)
				{
					Color color = styleColors[styleColorIndex];

					int a = Math.Min(color.A + dist, 255);
					int r = Math.Min(color.R + dist, 255);
					int g = Math.Min(color.G + dist, 255);
					int b = Math.Min(color.B + dist, 255);

					colors[colorIndex] = Color.FromArgb(a, r, g, b);
				}
				else
				{
					colors[colorIndex] = styleColors[styleColorIndex];
				}

				styleColorIndex++;
				if (styleColorIndex == styleColors.Length)
				{
					styleColorIndex = 0;
					dist = 50;
				}
			}
			return colors;
		}


		public Color GetColorByIndex(int index, int count)
		{
			return GetColors(count)[index];
		}


        public Color GetColorBySeries(IStiSeries series)
		{
			return GetColors(series.Chart.Series.Count)[series.Chart.Series.IndexOf(series)];
		}


        /*public bool CompareChartStyle(StiStyleCoreXF style)
        {
            if (style == null) return false;
            StiCustomStyle style1 = this as StiCustomStyle;
            StiCustomStyle style2 = style as StiCustomStyle;

            if (style1 != null && style2 != null)
            {
                string styleName1 = style1.ReportStyleName;
                if (style1.ReportStyle != null) styleName1 = style1.ReportStyle.Name;

                string styleName2 = style2.ReportStyleName;
                if (style2.ReportStyle != null) styleName2 = style2.ReportStyle.Name;

                return (styleName1 == styleName2);
            }
            
            return this.GetType() == style.GetType();
        }*/
        #endregion
	}
}
