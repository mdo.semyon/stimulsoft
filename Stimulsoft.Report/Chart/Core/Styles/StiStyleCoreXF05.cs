#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF05 : StiStyleCoreXF
    {
        #region Properties.Localization
        /// <summary>
        /// Gets a localized name of style.
		/// </summary>
        public override string LocalizedName
		{
			get
			{
				return StiLocalization.Get("Chart", "Style") + "05";
			}
		}
		#endregion

        #region Properties
		private Color[] styleColor = new Color[]
		{
			Color.FromArgb(0xFF, 0xE0, 0xA9, 0x3D),
			Color.FromArgb(0xFF, 0xB4, 0xB5, 0x00),
			Color.FromArgb(0xFF, 0x0E, 0x98, 0x0D),
			Color.FromArgb(0xFF, 0xAA, 0x48, 0x24),
			Color.FromArgb(0xFF, 0x85, 0xDD, 0x4D),
			Color.FromArgb(0xFF, 0xE6, 0xC7, 0x45),
			Color.FromArgb(0xFF, 0xDB, 0x64, 0x34),
			Color.FromArgb(0xFF, 0x83, 0xBE, 0x2F)
		};

		public override Color[] StyleColors
		{
			get
			{
				return styleColor;
			}
		}

		public override Color BasicStyleColor
		{
			get
			{
				return Color.LemonChiffon;
			}
		}

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle05;
            }
        }
        #endregion
	}
}