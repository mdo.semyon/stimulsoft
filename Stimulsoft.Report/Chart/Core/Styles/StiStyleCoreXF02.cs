#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF02 : StiStyleCoreXF
    {
        #region Properties.Localization
        /// <summary>
        /// Gets a localized name of style.
		/// </summary>
        public override string LocalizedName
		{
			get
			{
				return StiLocalization.Get("Chart", "Style") + "02";
			}
		}
		#endregion

        #region Properties
		private Color[] styleColor = new Color[]
		{
			Color.FromArgb(0xFF, 0x80, 0x91, 0xA5),
			Color.FromArgb(0xFF, 0xaB, 0x7B, 0x7A),
			Color.FromArgb(0xFF, 0x9B, 0xA7, 0x81),
			Color.FromArgb(0xFF, 0x90, 0x85, 0x9D),
			Color.FromArgb(0xFF, 0x79, 0xA1, 0xAD),
			Color.FromArgb(0xFF, 0xBD, 0x98, 0x7A),
			Color.FromArgb(0xFF, 0x8B, 0xA4, 0xC2),
			Color.FromArgb(0xFF, 0xCA, 0x84, 0x82),
			Color.FromArgb(0xFF, 0xB3, 0xC6, 0x8D),
			Color.FromArgb(0xFF, 0xA2, 0x92, 0xB6)
		};

		public override Color BasicStyleColor
		{
			get
			{
				return Color.WhiteSmoke;
			}
		}

		public override Color[] StyleColors
		{
			get
			{
				return styleColor;
			}
		}


        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle02;
            }
        }
        #endregion

		#region Methods
		public override StiBrush GetColumnBrush(Color color)
		{
			return new StiGlareBrush(color, StiColorUtils.Light(color, 50), 0);
		}
		#endregion
	}
}