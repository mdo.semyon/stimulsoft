#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF09 : StiStyleCoreXF
    {
        #region Properties.Localization
        /// <summary>
        /// Gets a localized name of style.
		/// </summary>
        public override string LocalizedName
		{
			get
			{
				return StiLocalization.Get("Chart", "Style") + "09";
			}
		}
		#endregion

        #region Properties
		private Color[] styleColor = new Color[]
		{
			Color.FromArgb(0xFF, 0x4F, 0x81, 0xBD),
			Color.FromArgb(0xFF, 0xC0, 0x50, 0x4D),
			Color.FromArgb(0xFF, 0x9B, 0xBB, 0x59),
			Color.FromArgb(0xFF, 0x80, 0x64, 0xA2),
			Color.FromArgb(0xFF, 0x4B, 0xAC, 0xC6),
			Color.FromArgb(0xFF, 0xF7, 0x96, 0x46)
		};

		public override Color[] StyleColors
		{
			get
			{
				return styleColor;
			}
		}

		public override Color BasicStyleColor
		{
			get
			{
				return Color.FromArgb(0xEE, 0xEE, 0xEC, 0xE1);
			}
		}

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle09;
            }
        }
        #endregion
	}
}