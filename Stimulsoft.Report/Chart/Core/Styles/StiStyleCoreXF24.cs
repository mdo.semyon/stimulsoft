﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF24 : StiStyleCoreXF22
    {
        #region Properties.Localization
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "Style") + "24";
            }
        }
        #endregion

        #region Properties
        
        private Color[] styleColor = new Color[]
        {
            ColorTranslator.FromHtml("#ed7d31"),
            ColorTranslator.FromHtml("#ffc000"),
            ColorTranslator.FromHtml("#70ad47"),
            ColorTranslator.FromHtml("#9e480e"),
            ColorTranslator.FromHtml("#997300"),
            ColorTranslator.FromHtml("#43682b")
        };

        public override Color[] StyleColors
        {
            get
            {
                return styleColor;
            }
        }

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle24;
            }
        }
        #endregion
    }
}