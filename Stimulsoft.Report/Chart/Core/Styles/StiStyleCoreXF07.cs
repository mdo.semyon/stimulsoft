#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF07 : StiStyleCoreXF
    {
        #region Properties.Localization
        /// <summary>
        /// Gets a localized name of style.
		/// </summary>
        public override string LocalizedName
		{
			get
			{
				return StiLocalization.Get("Chart", "Style") + "07";
			}
		}
		#endregion

        #region Properties
		private Color[] styleColor = new Color[]
		{
			Color.FromArgb(0xFF, 0xD9, 0xFB, 0xA8),
			Color.FromArgb(0xFF, 0xC8, 0xB5, 0xE7),
			Color.FromArgb(0xFF, 0x9E, 0xE8, 0xFF),
			Color.FromArgb(0xFF, 0xFF, 0xBD, 0x86),
			Color.FromArgb(0xFF, 0xA3, 0xC3, 0xFE)
		};

		public override Color[] StyleColors
		{
			get
			{
				return styleColor;
			}
		}

		public override Color BasicStyleColor
		{
			get
			{
				return Color.White;
			}
		}

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle07;
            }
        }
        #endregion

		#region Methods
		public override StiBrush GetColumnBrush(Color color)
		{
			return new StiGradientBrush( 
				StiColorUtils.Dark(color, 50), 
				StiColorUtils.Light(color, 50), -90);
		}
		#endregion
	}
}