﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Report.Chart
{
    public class StiStyleCoreXF27 : StiStyleCoreXF22
    {
        #region Properties.Localization
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "Style") + "27";
            }
        }
        #endregion

        #region Properties
        public override Color[] StyleColors
        {
            get
            {
                return new Color[]
                {
                    ColorTranslator.FromHtml("#0bac45"),
                    ColorTranslator.FromHtml("#FF585257"),
                    ColorTranslator.FromHtml("#ec334d"),
                    ColorTranslator.FromHtml("#a1ae94"),
                    ColorTranslator.FromHtml("#ed7d31"),
                    ColorTranslator.FromHtml("#5ab0ee"),
                };
            }
        }

        public override StiBrush ChartBrush
        {
            get
            {
                return new StiSolidBrush(StiColorUtils.Dark(ColorTranslator.FromHtml("#33475b"), 50));
            }
        }

        public override StiBrush ChartAreaBrush
        {
            get
            {
                return
                    new StiSolidBrush(ColorTranslator.FromHtml("#33475b"));
            }
        }

        #region SeriesLabels
        public override StiBrush SeriesLabelsBrush
        {
            get
            {
                return new StiSolidBrush(Color.Transparent);
            }
        }

        public override Color SeriesLabelsColor
        {
            get
            {
                return Color.White;
            }
        }

        public override Color SeriesLabelsBorderColor
        {
            get
            {
                return Color.Transparent;
            }
        }

        public override Font SeriesLabelsFont
        {
            get
            {
                return new Font("Arial", 10);
            }
        }
        #endregion

        #region Legend
        public override StiBrush LegendBrush
        {
            get
            {
                return new StiSolidBrush(Color.Transparent);
            }
        }

        public override Color LegendLabelsColor
        {
            get
            {
                return Color.White;
            }
        }

        public override Color LegendBorderColor
        {
            get
            {
                return Color.Transparent;
            }
        }

        public override Color LegendTitleColor
        {
            get
            {
                return Color.White;
            }
        }

        public override bool LegendShowShadow
        {
            get
            {
                return false;
            }
        }

        public override Font LegendFont
        {
            get
            {
                return new Font("Arial", 9);
            }
        }
        #endregion

        public override bool SeriesLighting
        {
            get
            {
                return false;
            }
        }

        public override Color GetColumnBorder(Color color)
        {
            return Color.Transparent;
        }

        public override StiChartStyleId StyleId
        {
            get
            {
                return StiChartStyleId.StiStyle27;
            }
        }
        #endregion
    }
}