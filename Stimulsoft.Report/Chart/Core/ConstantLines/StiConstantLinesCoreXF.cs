#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Chart
{
    public class StiConstantLinesCoreXF :
        IStiApplyStyle,
        ICloneable
    {
        #region ICloneable
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion

        #region IStiApplyStyle
        /// <summary>
        /// Applying specified style to this area.
        /// </summary>
        /// <param name="style"></param>
        public void ApplyStyle(IStiChartStyle style)
        {
            if (this.ConstantLines.AllowApplyStyle)
            {
                this.ConstantLines.LineColor = style.Core.ChartAreaBorderColor;
            }
        }
        #endregion

        #region Methods
        public void RenderXConstantLines(StiContext context, StiAxisAreaGeom geom, RectangleF rect)
        {
            IStiAxisArea area = geom.Area as IStiAxisArea;
            if (area == null) return;

            #region Calculate coodrs
            float value = 0f;
            float.TryParse(ConstantLines.AxisValue, out value);

            if (area.ReverseVert) value = -value;
            float left = area.AxisCore.GetDividerX() + value * (float)area.XAxis.Info.Dpi;

            foreach (StiStripLineXF tick in area.XAxis.Info.StripLines)
            {
                if (tick.ValueObject != null && tick.ValueObject.ToString() == ConstantLines.AxisValue)
                {
                    value = (float)tick.Value;
                    if (area.ReverseHor) value = -value;
                    left = (float)(value * area.XAxis.Info.Dpi) + area.AxisCore.GetDividerX();
                }
            }
            #endregion

            #region Render Title
            PointF point = new PointF(0, 0);
            StiRotationMode mode = StiRotationMode.LeftTop;

            switch (ConstantLines.Position)
            {
                case StiConstantLines.StiTextPosition.LeftTop:
                    mode = StiRotationMode.LeftBottom;
                    point = new PointF(left, 0);
                    break;

                case StiConstantLines.StiTextPosition.LeftBottom:
                    mode = StiRotationMode.LeftTop;
                    point = new PointF(left, 0);
                    break;

                case StiConstantLines.StiTextPosition.CenterTop:
                    mode = StiRotationMode.CenterBottom;
                    point = new PointF(left, rect.Height / 2);
                    break;

                case StiConstantLines.StiTextPosition.CenterBottom:
                    mode = StiRotationMode.CenterTop;
                    point = new PointF(left, rect.Height / 2);
                    break;

                case StiConstantLines.StiTextPosition.RightTop:
                    mode = StiRotationMode.RightBottom;
                    point = new PointF(left, rect.Height);
                    break;

                case StiConstantLines.StiTextPosition.RightBottom:
                    mode = StiRotationMode.RightTop;
                    point = new PointF(left, rect.Height);
                    break;
            }
            #endregion
                        
            StiConstantLinesVerticalGeom lineGeom = new StiConstantLinesVerticalGeom(this.ConstantLines,
                new RectangleF(left, 0, left, rect.Height), point, mode);

            geom.CreateChildGeoms();
            geom.ChildGeoms.Add(lineGeom);
        }

        public void RenderYConstantLines(StiContext context, StiAxisAreaGeom geom, RectangleF rect)
        {
            IStiAxisArea area = geom.Area as IStiAxisArea;
            if (area == null) return;

            #region Calculate coodrs
            float value = 0f;
            float.TryParse(ConstantLines.AxisValue, out value);

            if (area.ReverseVert) value = -value;
            float top;
            if (ConstantLines.Orientation == StiConstantLines.StiOrientation.Horizontal)
                top = area.AxisCore.GetDividerY() - value * (float)area.YAxis.Info.Dpi;
            else
                top = area.AxisCore.GetDividerRightY() - value * (float)area.YRightAxis.Info.Dpi;
            #endregion

            #region Draw Title
            PointF point = new PointF(0, 0);
            StiRotationMode mode = StiRotationMode.LeftTop;

            switch (ConstantLines.Position)
            {
                case StiConstantLines.StiTextPosition.LeftTop:
                    mode = StiRotationMode.LeftBottom;
                    point = new PointF(-rect.X, top);
                    break;

                case StiConstantLines.StiTextPosition.LeftBottom:
                    mode = StiRotationMode.LeftTop;
                    point = new PointF(-rect.X, top);
                    break;

                case StiConstantLines.StiTextPosition.CenterTop:
                    mode = StiRotationMode.CenterBottom;
                    point = new PointF(rect.Width / 2, top);
                    break;

                case StiConstantLines.StiTextPosition.CenterBottom:
                    mode = StiRotationMode.CenterTop;
                    point = new PointF(rect.Width / 2, top);
                    break;

                case StiConstantLines.StiTextPosition.RightTop:
                    mode = StiRotationMode.RightBottom;
                    point = new PointF(rect.Width, top);
                    break;

                case StiConstantLines.StiTextPosition.RightBottom:
                    mode = StiRotationMode.RightTop;
                    point = new PointF(rect.Width, top);
                    break;
            }
            #endregion

            StiConstantLinesYGeom lineGeom = new StiConstantLinesYGeom(this.ConstantLines,
                new RectangleF(0, top, rect.Width, top), point, mode);

            geom.CreateChildGeoms();
            geom.ChildGeoms.Add(lineGeom);
        }

        public void Render(StiContext context, StiAxisAreaGeom geom, RectangleF rect)
        {
            if (!ConstantLines.Visible) return;

            if (ConstantLines.Orientation == StiConstantLines.StiOrientation.Vertical)
                RenderXConstantLines(context, geom, rect);

            if (ConstantLines.Orientation == StiConstantLines.StiOrientation.Horizontal ||
                ConstantLines.Orientation == StiConstantLines.StiOrientation.HorizontalRight)
                RenderYConstantLines(context, geom, rect);
        }
        #endregion

        #region Properties
        private IStiConstantLines constantLines;
        public IStiConstantLines ConstantLines
        {
            get
            {
                return constantLines;
            }
            set
            {
                constantLines = value;
            }
        }
        #endregion

        public StiConstantLinesCoreXF(IStiConstantLines constantLines)
        {
            this.constantLines = constantLines;
        }
    }
}
