#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

namespace Stimulsoft.Report.Chart
{
    public class StiCenterPieLabels : 
        StiPieSeriesLabels,
        IStiCenterPieLabels
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            jObject.AddPropertyBool("AutoRotate", AutoRotate);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AutoRotate":
                        this.AutoRotate = property.Value.ToObject<bool>();
                        break;
                }
            }
        }

        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiCenterPieLabels;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[] 
            {
                propHelper.SeriesCenterPieLabels()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }
        #endregion

		#region Properties
		private bool autoRotate = false;
        /// <summary>
        /// Gets or sets value which enables or disables auto rotate mode drawing of series labels.
        /// </summary>
		[StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.AutoRotate)]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which enables or disables auto rotate mode drawing of series labels.")]
        [StiCategory("Common")]
		public virtual bool AutoRotate
		{
			get
			{
				return autoRotate;
			}
			set
			{
				autoRotate = value;
			}
		}
        #endregion

        #region Methods.override
        public override StiSeriesLabels CreateNew()
        {
            return new StiCenterPieLabels();
        }
        #endregion

        public StiCenterPieLabels()
        {
            this.Core = new StiCenterPieLabelsCoreXF(this);
        }
	}
}
