﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Linq;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
    [StiServiceBitmap(typeof(StiSeriesLabels), "Stimulsoft.Report.Images.Components.StiChart.png")]
    [StiServiceCategoryBitmap(typeof(StiSeriesLabels), "Stimulsoft.Report.Images.Components.StiChart.png")]
	[TypeConverter(typeof(Design.StiSeriesLabelsConverter))]
    public abstract partial class StiSeriesLabels : 
		StiService,
		IStiSerializeToCodeAsClass,
        IStiSeriesLabels,
        IStiPropertyGridObject
	{
        #region IStiJsonReportObject.override
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyIdent("Ident", this.GetType().Name);

            jObject.AddPropertyBool("PreventIntersection", preventIntersection);
            jObject.AddPropertyBool("AllowApplyStyle", AllowApplyStyle, true);
            jObject.AddPropertyJObject("Conditions", Conditions.SaveToJsonObject(mode));
            jObject.AddPropertyBool("ShowZeros", ShowZeros);
            jObject.AddPropertyBool("ShowNulls", showNulls, true);
            jObject.AddPropertyBool("MarkerVisible", markerVisible);
            jObject.AddPropertyJObject("MarkerSize", StiJsonReportObjectHelper.Serialize.Size(markerSize));
            jObject.AddPropertyEnum("MarkerAlignment", MarkerAlignment, StiMarkerAlignment.Left);
            jObject.AddPropertyInt("Step", step);
            jObject.AddPropertyEnum("ValueType", ValueType, StiSeriesLabelsValueType.Value);
            jObject.AddPropertyStringNullOrEmpty("ValueTypeSeparator", ValueTypeSeparator);
            jObject.AddPropertyEnum("LegendValueType", LegendValueType, StiSeriesLabelsValueType.Value);
            jObject.AddPropertyStringNullOrEmpty("TextBefore", textBefore);
            jObject.AddPropertyStringNullOrEmpty("TextAfter", textAfter);
            jObject.AddPropertyFloat("Angle", Angle, 0f);
            jObject.AddPropertyStringNullOrEmpty("Format", format);
            jObject.AddPropertyBool("Antialiasing", Antialiasing, true);
            jObject.AddPropertyBool("Visible", Visible, true);
            jObject.AddPropertyBool("DrawBorder", DrawBorder, true);
            jObject.AddPropertyBool("UseSeriesColor", UseSeriesColor);
            jObject.AddPropertyStringNullOrEmpty("LabelColor", StiJsonReportObjectHelper.Serialize.JColor(LabelColor, Color.Black));
            jObject.AddPropertyStringNullOrEmpty("BorderColor", StiJsonReportObjectHelper.Serialize.JColor(BorderColor, Color.Black));
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.Font(Font, "Arial", 7));
            jObject.AddPropertyBool("WordWrap", wordWrap);
            jObject.AddPropertyInt("Width", width);

            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "PreventIntersection":
                        this.preventIntersection = property.Value.ToObject<bool>();
                        break;

                    case "AllowApplyStyle":
                        this.AllowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "Conditions":
                        this.Conditions.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "ShowZeros":
                        this.showZeros = property.Value.ToObject<bool>();
                        break;

                    case "ShowNulls":
                        this.showNulls = property.Value.ToObject<bool>();
                        break;

                    case "MarkerVisible":
                        this.markerVisible = property.Value.ToObject<bool>();
                        break;

                    case "MarkerSize":
                        this.markerSize = StiJsonReportObjectHelper.Deserialize.Size((JObject)property.Value);
                        break;

                    case "MarkerAlignment":
                        this.MarkerAlignment = (StiMarkerAlignment)Enum.Parse(typeof(StiMarkerAlignment), property.Value.ToObject<string>());
                        break;

                    case "Step":
                        this.step = property.Value.ToObject<int>();
                        break;

                    case "ValueType":
                        this.ValueType = (StiSeriesLabelsValueType)Enum.Parse(typeof(StiSeriesLabelsValueType), property.Value.ToObject<string>());
                        break;

                    case "ValueTypeSeparator":
                        this.ValueTypeSeparator = property.Value.ToObject<string>();
                        break;

                    case "LegendValueType":
                        this.LegendValueType = (StiSeriesLabelsValueType)Enum.Parse(typeof(StiSeriesLabelsValueType), property.Value.ToObject<string>());
                        break;

                    case "TextBefore":
                        this.textBefore = property.Value.ToObject<string>();
                        break;

                    case "TextAfter":
                        this.textAfter = property.Value.ToObject<string>();
                        break;
                        
                    case "Angle":
                        this.Angle = property.Value.ToObject<float>();
                        break;

                    case "Format":
                        this.format = property.Value.ToObject<string>();
                        break;
                        
                    case "Antialiasing":
                        this.Antialiasing = property.Value.ToObject<bool>();
                        break;

                    case "Visible":
                        this.Visible = property.Value.ToObject<bool>();
                        break;

                    case "DrawBorder":
                        this.DrawBorder = property.Value.ToObject<bool>();
                        break;

                    case "UseSeriesColor":
                        this.UseSeriesColor = property.Value.ToObject<bool>();
                        break;

                    case "LabelColor":
                        this.LabelColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "BorderColor":
                        this.BorderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Brush":
                        this.Brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "Font":
                        this.Font = StiJsonReportObjectHelper.Deserialize.Font(property, Font);
                        break;

                    case "WordWrap":
                        this.wordWrap = property.Value.ToObject<bool>();
                        break;

                    case "Width":
                        this.width = property.Value.ToObject<int>();
                        break;
                }
            }
        }

        internal static IStiSeriesLabels LoadFromJsonObjectInternal(JObject jObject, StiChart chart)
        {
            var ident = jObject.Properties().FirstOrDefault(x => x.Name == "Ident").Value.ToObject<string>();
            var service = StiOptions.Services.ChartSerieLabels.FirstOrDefault(x => x.GetType().Name == ident);

            if (service == null)
                throw new Exception(string.Format("Type {0} is not found!", ident));

            StiSeriesLabels seriesLabels = service.CreateNew();
            seriesLabels.Chart = chart;
            seriesLabels.LoadFromJsonObject(jObject);

            return seriesLabels;
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public virtual StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiSeriesLabels;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public virtual StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            return null;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public override object Clone()
		{
			IStiSeriesLabels labels = base.Clone() as IStiSeriesLabels;
			labels.Brush =	this.Brush.Clone() as StiBrush;
			labels.Font =	this.Font.Clone() as Font;

            if (this.core != null)
            {
                labels.Core = this.core.Clone() as StiSeriesLabelsCoreXF;
                labels.Core.SeriesLabels = labels;
            }
			
			return labels;
		}
        #endregion
                
        #region StiService override
        /// <summary>
        /// Gets a service name.
        /// </summary>
        public override string ServiceName
        {
            get
            {
                return Core.LocalizedName;
            }
        }
        

		/// <summary>
		/// Gets a service category.
		/// </summary>
		[Browsable(false)]
		public sealed override string ServiceCategory
		{
			get
			{
				return "Chart";
			}
		}

		/// <summary>
		/// Gets a service type.
		/// </summary>
		[Browsable(false)]
		public sealed override Type ServiceType
		{
			get
			{
				return typeof(StiSeriesLabels);
			}
		}
		#endregion

        #region Properties
        private bool preventIntersection = false;
        /// <summary>
        /// Gets or sets value which indicates that whether it is necessary to avoid intersections between border of series labels and border of series.
        /// </summary>
        [StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.PreventIntersection)]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that whether it is necessary to avoid intersections between border of series labels and border of series.")]
        [StiCategory("Common")]
        public bool PreventIntersection
        {
            get
            {
                return preventIntersection;
            }
            set
            {
                preventIntersection = value;
            }
        }

        private StiSeriesLabelsCoreXF core;
        [Browsable(false)]
        public StiSeriesLabelsCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        [Browsable(false)]
        public StiAxisSeriesLabelsCoreXF AxisCore
        {
            get
            {
                return Core as StiAxisSeriesLabelsCoreXF;
            }
        }

        [Browsable(false)]
        public StiPieSeriesLabelsCoreXF PieCore
        {
            get
            {
                return Core as StiPieSeriesLabelsCoreXF;
            }
        }

        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [StiOrder(StiSeriesLabelsPropertyOrder.AllowApplyStyle)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                    if (value && Chart != null)
                        this.Core.ApplyStyle(this.Chart.Style);
                }
            }
        }

        /// <summary>
        /// Gets or sets collection of conditions which can be used to change behavior of series labels.
        /// </summary>
        [StiOrder(StiSeriesLabelsPropertyOrder.Conditions)]
        [TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiChartConditionsCollectionConverter))]
        [Browsable(false)]
        [Description("Gets or sets collection of conditions which can be used to change behavior of series labels.")]
        [StiCategory("Common")]
        public StiChartConditionsCollection Conditions
        {
            get
            {
                if (Chart == null)
                    return null;
                return Chart.SeriesLabelsConditions;
            }
            set
            {
                if (Chart == null)
                    return;
                Chart.SeriesLabelsConditions = value;
            }
        }

        [Browsable(false)]
        [Obsolete("ShowOnZeroValues property is obsolete. Please use ShowZeros property instead it.")]
		public bool ShowOnZeroValues
		{
			get
			{
				return ShowZeros;
			}
			set
			{
				ShowZeros = value;
			}
		}


        private bool showZeros = false;
        /// <summary>
        /// Gets or sets value which indicates that series labels will be shown or not if value equal to zero.
        /// </summary>
        [DefaultValue(false)]
        [StiOrder(StiSeriesLabelsPropertyOrder.ShowZeros)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that series labels will be shown or not if value equal to zero.")]
        [StiCategory("Common")]
        public bool ShowZeros
        {
            get
            {
                return showZeros;
            }
            set
            {
                showZeros = value;
            }
        }


        private bool showNulls = true;
        /// <summary>
        /// Gets or sets value which indicates that series labels will be shown or not if value equal to null.
        /// </summary>
        [DefaultValue(true)]
        [StiOrder(StiSeriesLabelsPropertyOrder.ShowNulls)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that series labels will be shown or not if value equal to null.")]
        [StiCategory("Common")]
        public bool ShowNulls
        {
            get
            {
                return showNulls;
            }
            set
            {
                showNulls = value;
            }
        }


		private bool markerVisible = false;
        /// <summary>
        /// Gets or sets vibility of marker.
        /// </summary>
		[DefaultValue(false)]
        [StiOrder(StiSeriesLabelsPropertyOrder.MarkerVisible)]
		[StiSerializable]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets vibility of marker.")]
        [StiCategory("Common")]
		public bool MarkerVisible
		{
			get
			{
				return markerVisible;
			}
			set
			{
				markerVisible = value;
			}
		}


		private Size markerSize = new Size(8, 6);
        /// <summary>
        /// Gets or sets marker size.
        /// </summary>
		[StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.MarkerSize)]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiSizeConverter))]
        [Description("Gets or sets marker size.")]
        [StiCategory("Common")]
		public Size MarkerSize
		{
			get
			{
				return markerSize;
			}
			set
			{
				markerSize = value;
			}
		}


		private StiMarkerAlignment markerAlignment = StiMarkerAlignment.Left;
        /// <summary>
        /// Gets or sets marker alignment related to label text.
        /// </summary>
		[StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.MarkerAlignment)]
		[DefaultValue(StiMarkerAlignment.Left)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets marker alignment related to label text.")]
        [StiCategory("Common")]
		public virtual StiMarkerAlignment MarkerAlignment
		{
			get 
			{
				return markerAlignment;
			}
			set 
			{
				markerAlignment = value;
			}
		}


        private int step = 0;
        /// <summary>
        /// Gets or sets value which indicates with what steps do labels be shown.
        /// </summary>
        [DefaultValue(0)]
        [StiOrder(StiSeriesLabelsPropertyOrder.Step)]
        [StiSerializable]
        [Description("Gets or sets value which indicates with what steps do labels be shown.")]
        [StiCategory("Common")]
        public int Step
        {
            get
            {
                return step;
            }
            set
            {
                step = value;
            }
        }


        private StiSeriesLabelsValueType valueType = StiSeriesLabelsValueType.Value;
        /// <summary>
        /// Gets or sets which type of information will be shown in series labels.
        /// </summary>
        [StiOrder(StiSeriesLabelsPropertyOrder.ValueType)]
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		[DefaultValue(StiSeriesLabelsValueType.Value)]
        [Description("Gets or sets which type of information will be shown in series labels.")]
        [StiCategory("Common")]
		public virtual StiSeriesLabelsValueType ValueType
		{
			get
			{
				return valueType;
			}
			set
			{
				if (valueType != value)
				{
					valueType = value;
				}
			}
		}


        private string valueTypeSeparator = "-";
        /// <summary>
        /// Gets or sets string which contain separator for value information (if applicated).
        /// </summary>
        [StiOrder(StiSeriesLabelsPropertyOrder.ValueTypeSeparator)]
        [StiSerializable]
        [DefaultValue("")]
        [Description("Gets or sets string which contain separator for value information (if applicated).")]
        [StiCategory("Common")]
        public virtual string ValueTypeSeparator
        {
            get
            {
                return valueTypeSeparator;
            }
            set
            {
                if (valueTypeSeparator != value)
                {
                    valueTypeSeparator = value;
                }
            }
        }


		private StiSeriesLabelsValueType legendValueType = StiSeriesLabelsValueType.Value;
        /// <summary>
        /// Gets or sets which type of information will be shown in legend.
        /// </summary>
        [StiOrder(StiSeriesLabelsPropertyOrder.LegendValueType)]
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		[DefaultValue(StiSeriesLabelsValueType.Value)]
        [Description("Gets or sets which type of information will be shown in legend.")]
        [StiCategory("Common")]
		public virtual StiSeriesLabelsValueType LegendValueType
		{
			get
			{
				return legendValueType;
			}
			set
			{
				legendValueType = value;
			}
		}


		private string textBefore = "";
        /// <summary>
        /// Gets or sets text which will be shown before label text.
        /// </summary>
		[DefaultValue("")]
        [StiOrder(StiSeriesLabelsPropertyOrder.TextBefore)]
		[StiSerializable]
        [Description("Gets or sets text which will be shown before label text.")]
        [StiCategory("Common")]
		public string TextBefore
		{
			get
			{
				return textBefore;
			}
			set
			{
				textBefore = value;
			}
		}


		private string textAfter = "";
        /// <summary>
        /// Gets or sets text which will be shown after label text.
        /// </summary>
		[DefaultValue("")]
        [StiOrder(StiSeriesLabelsPropertyOrder.TextAfter)]
		[StiSerializable]
        [Description("Gets or sets text which will be shown after label text.")]
        [StiCategory("Common")]
		public string TextAfter
		{
			get
			{
				return textAfter;
			}
			set
			{
				textAfter = value;
			}
		}


		private float angle = 0f;
        /// <summary>
        /// Gets or sets angle of text rotation.
        /// </summary>
		[DefaultValue(0f)]
        [StiOrder(StiSeriesLabelsPropertyOrder.Angle)]
		[StiSerializable]
        [Description("Gets or sets angle of text rotation.")]
        [StiCategory("Common")]
		public virtual float Angle
		{
			get
			{
				return angle;
			}
			set
			{
				angle = value;
			}
		}


		private string format = "";
        /// <summary>
        /// Gets or sets format string which used for formating series values (if applicable).
        /// </summary>
		[DefaultValue("")]
        [StiOrder(StiSeriesLabelsPropertyOrder.Format)]
        [Description("Gets or sets format string which used for formating series values (if applicable).")]
		[StiSerializable]
		[Editor("Stimulsoft.Report.Chart.Design.StiFormatEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Common")]
		public string Format
		{
			get
			{
				return format;
			}
			set
			{
				format = value;
			}
		}


		private bool antialiasing = true;
        /// <summary>
        /// Gets or sets value which control antialiasing drawing mode of series labels.
        /// </summary>
		[StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.Antialiasing)]
        [DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which control antialiasing drawing mode of series labels.")]
        [StiCategory("Appearance")]
		public virtual bool Antialiasing
		{
			get
			{
				return antialiasing;
			}
			set
			{
				antialiasing = value;
			}
		}


		private bool visible = true;
        /// <summary>
        /// Gets or sets visiblity of series labels.
        /// </summary>
		[DefaultValue(true)]
        [StiOrder(StiSeriesLabelsPropertyOrder.Visible)]
		[StiSerializable]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visiblity of series labels.")]
        [StiCategory("Common")]
		public virtual bool Visible
		{
			get
			{
				return visible;
			}
			set
			{
				visible = value;
			}
		}


		private bool drawBorder = true;
        /// <summary>
        /// Gets or sets value which incates that border will be drawn or not.
        /// </summary>
		[DefaultValue(true)]
        [StiOrder(StiSeriesLabelsPropertyOrder.DrawBorder)]
		[StiSerializable]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which incates that border will be drawn or not.")]
        [StiCategory("Common")]
		public virtual bool DrawBorder
		{
			get
			{
				return drawBorder;
			}
			set
			{
				drawBorder = value;
			}
		}

		
		private bool useSeriesColor = false;
        /// <summary>
        /// Gets or sets value which indicates that series colors must be used.
        /// </summary>
		[DefaultValue(false)]
        [StiOrder(StiSeriesLabelsPropertyOrder.UseSeriesColor)]
		[StiSerializable]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that series colors must be used.")]
        [StiCategory("Common")]
		public virtual bool UseSeriesColor
		{
			get
			{
				return useSeriesColor;
			}
			set
			{
				useSeriesColor = value;
			}
		}

		
		private Color labelColor = Color.Black;
        /// <summary>
        /// Gets or sets foreground color of series labels.
        /// </summary>
		[StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.LabelColor)]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets foreground color of series labels.")]
        [StiCategory("Appearance")]
		public virtual Color LabelColor
		{
			get
			{
				return labelColor;
			}
			set
			{
				labelColor = value;
			}
		}


		private Color borderColor = Color.Black;
        /// <summary>
        /// Gets or sets border color of series labels.
        /// </summary>
		[StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.BorderColor)]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets border color of series labels.")]
        [StiCategory("Appearance")]
		public virtual Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
			}
		}


		private StiBrush brush = new StiSolidBrush(Color.White);
        /// <summary>
        /// Gets or sets brush which will be used to fill area of series labels.
        /// </summary>
        [StiOrder(StiSeriesLabelsPropertyOrder.Brush)]
		[StiSerializable]
        [Description("Gets or sets brush which will be used to fill area of series labels.")]
        [StiCategory("Appearance")]
		public virtual StiBrush Brush
		{
			get 
			{
				return brush;
			}
			set 
			{
				brush = value;
			}
		}


		private Font font = new Font("Arial", 7);
        /// <summary>
        /// Gets or sets font which will be used to draw series labels.
        /// </summary>
		[StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.Font)]
        [Description("Gets or sets font which will be used to draw series labels.")]
        [StiCategory("Appearance")]
		public virtual Font Font
		{
			get
			{
				return font;
			}
			set
			{
				font = value;
			}
		}


        private IStiChart chart = null;
        [StiSerializable(StiSerializationVisibility.Reference)]
        [Browsable(false)]
        public IStiChart Chart
        {
            get
            {
                return chart;
            }
            set
            {
                chart = value;
            }
        }

        /// <summary>
        /// Gets or sets word wrap.
        /// </summary>
        private bool wordWrap = false;
        [StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.WordWrap)]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets word wrap.")]
        [StiCategory("Common")]
        public bool WordWrap
        {
            get
            {
                return wordWrap;
            }
            set
            {
                wordWrap = value;
            }
        }

        private int width = 0;
        /// <summary>
        /// Gets or sets fixed width of series labels.
        /// </summary>
        [DefaultValue(0f)]
        [StiOrder(StiSeriesLabelsPropertyOrder.Width)]
        [StiSerializable]
        [Description("Gets or sets fixed width of axis labels.")]
        [StiCategory("Common")]
        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return ServiceName;
        }        
        #endregion

        #region Methods.virtual
        public virtual StiSeriesLabels CreateNew()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}