#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Json.Linq;

namespace Stimulsoft.Report.Chart
{
    [TypeConverter(typeof(StiUniversalConverter))]
	public class StiRadarAxis : 
        IStiRadarAxis,
        ICloneable
    {
        #region IStiJsonReportObject.override
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyBool("AllowApplyStyle", allowApplyStyle, true);
            jObject.AddPropertyBool("Visible", Visible, true);
            if (area != null)
                jObject.AddPropertyBool("Area", true);

            return jObject;
        }

        internal bool jsonLoadFromJsonObjectArea = false;
        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AllowApplyStyle":
                        this.allowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "Visible":
                        this.Visible = property.Value.ToObject<bool>();
                        break;

                    case "Area":
                        this.jsonLoadFromJsonObjectArea = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public virtual object Clone()
        {
            IStiRadarAxis axis = this.MemberwiseClone() as IStiRadarAxis;

            if (this.core != null)
            {
                axis.Core = this.core.Clone() as StiRadarAxisCoreXF;
                axis.Core.Axis = axis;
            }

            return axis;
        }
        #endregion

        #region Properties
        private StiRadarAxisCoreXF core;
        [Browsable(false)]
        public StiRadarAxisCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }


        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.2")]
        [DefaultValue(true)]
        [Browsable(false)]
        public virtual bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                    if (value && this.Area != null && this.Area.Chart != null)
                        this.Core.ApplyStyle(this.Area.Chart.Style);
                }
            }
        }


        private bool visible = true;
        /// <summary>
        /// Gets or sets visibility of axis.
        /// </summary>
        [StiSerializable]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of axis.")]
        [StiCategory("Common")]
        public virtual bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }


        private IStiRadarArea area;
        [StiSerializable(StiSerializationVisibility.Reference)]
        [Browsable(false)]
        public IStiRadarArea Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
            }
        }
        #endregion

        public StiRadarAxis()
		{
		}

        [StiUniversalConstructor("Axis")]
        public StiRadarAxis(
			bool visible,
            bool allowApplyStyle
			)
        {
			this.visible = visible;
            this.allowApplyStyle = allowApplyStyle;
 		}
	}
}
