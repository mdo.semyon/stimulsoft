#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{    
	
	[TypeConverter(typeof(StiUniversalConverter))]
    public class StiRadarAxisLabels : 
        IStiRadarAxisLabels     
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyBool("RotationLabels", rotationLabels, true);
            jObject.AddPropertyStringNullOrEmpty("TextBefore", textBefore);
            jObject.AddPropertyStringNullOrEmpty("TextAfter", textAfter);
            jObject.AddPropertyBool("AllowApplyStyle", allowApplyStyle, true);
            jObject.AddPropertyBool("DrawBorder", drawBorder);
            jObject.AddPropertyStringNullOrEmpty("Format", format);
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.Font(font, "Tahoma", 8));
            jObject.AddPropertyBool("Antialiasing", antialiasing, true);
            jObject.AddPropertyStringNullOrEmpty("Color", StiJsonReportObjectHelper.Serialize.JColor(color, Color.Black));
            jObject.AddPropertyStringNullOrEmpty("BorderColor", StiJsonReportObjectHelper.Serialize.JColor(borderColor, Color.Black));
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(brush));
            jObject.AddPropertyFloat("Width", width, 0f);
            jObject.AddPropertyBool("WordWrap", wordWrap);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "RotationLabels":
                        this.rotationLabels = property.Value.ToObject<bool>();
                        break;

                    case "TextBefore":
                        this.textBefore = property.Value.ToObject<string>();
                        break;

                    case "TextAfter":
                        this.textAfter = property.Value.ToObject<string>();
                        break;

                    case "AllowApplyStyle":
                        this.allowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "DrawBorder":
                        this.drawBorder = property.Value.ToObject<bool>();
                        break;

                    case "Format":
                        this.format = property.Value.ToObject<string>();
                        break;

                    case "Font":
                        this.font = StiJsonReportObjectHelper.Deserialize.Font(property, font);
                        break;

                    case "Antialiasing":
                        this.antialiasing = property.Value.ToObject<bool>();
                        break;

                    case "Color":
                        this.color = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "BorderColor":
                        this.borderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Brush":
                        this.brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "Width":
                        this.width = property.Value.ToObject<float>();
                        break;

                    case "WordWrap":
                        this.wordWrap = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region ICloneable override
        /// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone()
		{
			IStiRadarAxisLabels labels =	this.MemberwiseClone() as IStiRadarAxisLabels;
			labels.Font = this.Font.Clone() as Font;
            labels.Brush = this.Brush.Clone() as StiBrush;

            if (this.core != null)
            {
                labels.Core = this.core.Clone() as StiRadarAxisLabelsCoreXF;
                labels.Core.Labels = labels;
            }
			
			return labels;
		}
		#endregion

		#region Properties
        private StiRadarAxisLabelsCoreXF core;
        [Browsable(false)]
        public StiRadarAxisLabelsCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        private bool rotationLabels = true;
        /// <summary>
        /// Gets or sets a value which indicates that Axis Labels will be rotated.
        /// </summary>
        [StiSerializable]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates that Axis Labels will be rotated.")]
        [StiCategory("Common")]
        public bool RotationLabels
        {
            get
            {
                return rotationLabels;
            }
            set
            {
                rotationLabels = value;
            }
        }

        private string textBefore = "";
        /// <summary>
        /// Gets or sets string which will be output before argument string representation.
        /// </summary>
        [DefaultValue("")]
        [StiSerializable]
        [Description("Gets or sets string which will be output before argument string representation.")]
        [StiCategory("Common")]
        public string TextBefore
        {
            get
            {
                return textBefore;
            }
            set
            {
                textBefore = value;
            }
        }


        private string textAfter = "";
        /// <summary>
        /// Gets or sets string which will be output after argument string representation.
        /// </summary>
        [DefaultValue("")]
        [StiSerializable]
        [Description("Gets or sets string which will be output after argument string representation.")]
        [StiCategory("Common")]
        public string TextAfter
        {
            get
            {
                return textAfter;
            }
            set
            {
                textAfter = value;
            }
        }



        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                }
            }
        }


        private bool drawBorder = false;
        /// <summary>
        /// Gets or sets value which indicates that label border will be shown.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that label border will be shown.")]
        [DefaultValue(false)]
        [StiCategory("Common")]
        public bool DrawBorder
        {
            get
            {
                return drawBorder;
            }
            set
            {
                if (drawBorder != value)
                {
                    drawBorder = value;
                }
            }
        }


		private string format = "";
        /// <summary>
        /// Gets os sets format string which is used for formating argument values.
        /// </summary>
		[DefaultValue("")]
		[StiSerializable]
		[Editor("Stimulsoft.Report.Chart.Design.StiFormatEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets os sets format string which is used for formating argument values.")]
        [StiCategory("Common")]
		public string Format
		{
			get
			{
				return format;
			}
			set
			{
				format = value;
			}
		}


		private Font font = new Font("Tahoma", 8);
        /// <summary>
        /// Gets or sets font which will be used for axis label drawing.
        /// </summary>
		[StiSerializable]
        [Description("Gets or sets font which will be used for axis label drawing.")]
        [StiCategory("Common")]
		public Font Font
		{
			get
			{
				return font;
			}
			set
			{
				font = value;
			}
		}


		private bool antialiasing = true;
        /// <summary>
        /// Gets or sets value which control antialiasing drawing mode.
        /// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which control antialiasing drawing mode.")]
        [StiCategory("Appearance")]
		public bool Antialiasing
		{
			get
			{
				return antialiasing;
			}
			set
			{
				antialiasing = value;
			}
		}


		private Color color = Color.Black;
        /// <summary>
        /// Gets or sets color of labels drawing.
        /// </summary>
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets color of labels drawing.")]
        [StiCategory("Appearance")]
		public Color Color
		{
			get
			{
				return color;
			}
			set
			{
				color = value;
			}
		}


        private Color borderColor = Color.Black;
        /// <summary>
        /// Gets or sets color of labels drawing.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets color of labels drawing.")]
        [StiCategory("Appearance")]
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
            set
            {
                borderColor = value;
            }
        }

        private StiBrush brush = new StiSolidBrush(Color.Gainsboro);
        /// <summary>
        /// Gets or sets brush which will used to fill label area.
        /// </summary>
        [RefreshProperties(RefreshProperties.All)]
        [StiSerializable]
        [StiCategory("Appearance")]
        [Description("Gets or sets brush which will used to fill label area.")]
        public StiBrush Brush
        {
            get
            {
                return brush;
            }
            set
            {
                brush = value;
            }
        }

        private float width = 0f;
        /// <summary>
        /// Gets or sets fixed width of axis labels.
        /// </summary>
        [DefaultValue(0f)]
        [StiSerializable]
        [Description("Gets or sets fixed width of axis labels.")]
        [StiCategory("Common")]
        public float Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        private bool wordWrap = false;
        /// <summary>
        /// Gets or sets word wrap.
        /// </summary>
        [StiSerializable]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets word wrap.")]
        [StiCategory("Common")]
        public bool WordWrap
        {
            get
            {
                return wordWrap;
            }
            set
            {
                wordWrap = value;
            }
        }
		#endregion

        public StiRadarAxisLabels()
		{
            this.core = new StiRadarAxisLabelsCoreXF(this);
		}

        public StiRadarAxisLabels(
            string format,
            Font font,
            bool antialiasing,
            bool drawBorder,
            Color color,
            Color borderColor,
            StiBrush brush,
            bool allowApplyStyle
            )
        {
            this.format = format;
            this.font = font;
            this.antialiasing = antialiasing;
            this.drawBorder = drawBorder;
            this.color = color;
            this.borderColor = borderColor;
            this.allowApplyStyle = allowApplyStyle;
            this.brush = brush;
            this.width = 0;
            this.wordWrap = false;

            this.core = new StiRadarAxisLabelsCoreXF(this);
        }
        
        public StiRadarAxisLabels(
			string format,
			Font font,
			bool antialiasing,
            bool drawBorder,
			Color color,
            Color borderColor,	
		    StiBrush brush,
            bool allowApplyStyle,
            bool rotationLabels
			)
		{
			this.format = format;
			this.font = font;
			this.antialiasing = antialiasing;
            this.drawBorder = drawBorder;
			this.color = color;
            this.borderColor = borderColor;
            this.allowApplyStyle = allowApplyStyle;
            this.brush = brush;
            this.rotationLabels = rotationLabels;
            this.width = 0;
            this.wordWrap = false;

            this.core = new StiRadarAxisLabelsCoreXF(this);
		}

        [StiUniversalConstructor("Labels")]
        public StiRadarAxisLabels(
            string format,
            Font font,
            bool antialiasing,
            bool drawBorder,
            Color color,
            Color borderColor,
            StiBrush brush,
            bool allowApplyStyle,
            bool rotationLabels,
            float width,
            bool wordWrap
            )
        {
            this.format = format;
            this.font = font;
            this.antialiasing = antialiasing;
            this.drawBorder = drawBorder;
            this.color = color;
            this.borderColor = borderColor;
            this.allowApplyStyle = allowApplyStyle;
            this.brush = brush;
            this.rotationLabels = rotationLabels;
            this.width = width;
            this.wordWrap = wordWrap;

            this.core = new StiRadarAxisLabelsCoreXF(this);
        }
	}
}
