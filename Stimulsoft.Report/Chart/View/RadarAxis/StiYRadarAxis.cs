#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Localization;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
    [TypeConverter(typeof(StiUniversalConverter))]
	public class StiYRadarAxis : 
        StiRadarAxis,
        IStiYRadarAxis,
        ICloneable,
        IStiPropertyGridObject
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            jObject.AddPropertyJObject("Labels", labels.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Ticks", ticks.SaveToJsonObject(mode));
            jObject.AddPropertyEnum("LineStyle", lineStyle, StiPenStyle.Solid);
            jObject.AddPropertyStringNullOrEmpty("LineColor", StiJsonReportObjectHelper.Serialize.JColor(lineColor, Color.Gray));
            jObject.AddPropertyFloat("LineWidth", lineWidth, 1f);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Labels":
                        this.labels.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "Ticks":
                        this.ticks.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "LineStyle":
                        this.lineStyle = (StiPenStyle)Enum.Parse(typeof(StiPenStyle), property.Value.ToObject<string>());
                        break;

                    case "LineColor":
                        this.lineColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "LineWidth":
                        this.lineWidth = property.Value.ToObject<float>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiYRadarAxis;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.YRadarAxis()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public override object Clone()
        {
            IStiYRadarAxis axis = base.Clone() as IStiYRadarAxis;
            axis.LineStyle = this.LineStyle;
            axis.Labels = this.labels.Clone() as IStiAxisLabels;
            axis.Ticks = this.ticks.Clone() as IStiAxisTicks;

            return axis;
        }
        #endregion

        #region Properties
        [Browsable(false)]
        public StiYRadarAxisCoreXF YCore
        {
            get
            {
                return Core as StiYRadarAxisCoreXF;
            }
        }


        [Browsable(true)]
        public override bool AllowApplyStyle
        {
            get
            {
                return base.AllowApplyStyle;
            }
            set
            {
                base.AllowApplyStyle = value;
            }
        }


        private IStiAxisLabels labels = new StiAxisLabels();
        /// <summary>
        /// Gets or sets axis labels settings.
        /// </summary>
        [StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(StiUniversalConverter))]
        [StiCategory("Common")]
        public IStiAxisLabels Labels
        {
            get
            {
                return labels;
            }
            set
            {
                labels = value;
            }
        }


        private IStiAxisTicks ticks = new StiAxisTicks();
        /// <summary>
        /// Gets or sets ticks settings.
        /// </summary>
        [StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(StiUniversalConverter))]
        [StiCategory("Common")]
        public IStiAxisTicks Ticks
        {
            get
            {
                return ticks;
            }
            set
            {
                ticks = value;
            }
        }


        private StiPenStyle lineStyle = StiPenStyle.Solid;
        /// <summary>
        /// Gets or sets line style of axis.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StiPenStyle.Solid)]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiCategory("Common")]
        public StiPenStyle LineStyle
        {
            get
            {
                return lineStyle;
            }
            set
            {
                lineStyle = value;
            }
        }


        private Color lineColor = Color.Gray;
        /// <summary>
        /// Gets or sets line color which used to draw axis.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Appearance")]
        public Color LineColor
        {
            get
            {
                return lineColor;
            }
            set
            {
                lineColor = value;
            }
        }


        private float lineWidth = 1f;
        /// <summary>
        /// Gets or sets line width which used to draw axis.
        /// </summary>
        [DefaultValue(1f)]
        [StiSerializable]
        [StiCategory("Common")]
        public float LineWidth
        {
            get
            {
                return lineWidth;
            }
            set
            {
                lineWidth = value;
            }
        }


        private StiAxisInfoXF info = new StiAxisInfoXF();
        [Browsable(false)]
        public StiAxisInfoXF Info
        {
            get
            {
                return info;
            }
            set
            {
                info = value;
            }
        }
        #endregion

        public StiYRadarAxis() : base()
		{
            this.Core = new StiYRadarAxisCoreXF(this);
		}

        [StiUniversalConstructor("Axis")]
        public StiYRadarAxis(
			IStiAxisLabels labels,
			IStiAxisTicks ticks,
			StiPenStyle lineStyle,
			Color lineColor,
			float lineWidth,
			bool visible,
            bool allowApplyStyle) : 
            base
            (
                visible, 
                allowApplyStyle                
            )
		{
            this.Core = new StiYRadarAxisCoreXF(this);

            this.labels = labels;
			this.ticks = ticks;
			this.lineStyle = lineStyle;
			this.lineColor = lineColor;
			this.lineWidth = lineWidth;
 		}
	}
}
