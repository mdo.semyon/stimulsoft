#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Xml;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Text;
using Stimulsoft.Base;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using System.Globalization;
using System.Threading;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
    [StiServiceBitmap(typeof(StiChartStyle), "Stimulsoft.Report.Images.Components.StiChart.png")]
    [StiServiceCategoryBitmap(typeof(StiArea), "Stimulsoft.Report.Images.Components.StiChart.png")]
    public abstract class StiSeries :
        StiService,
        IStiSeriesParent,
        IStiSeries,
        IStiPropertyGridObject,
        IStiJsonReportObject
    {
        #region IStiJsonReportObject.override
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyIdent("Ident", this.GetType().Name);

            jObject.AddPropertyBool("AllowApplyStyle", AllowApplyStyle, true);
            jObject.AddPropertyStringNullOrEmpty("Format", format);
            jObject.AddPropertyEnum("SortBy", sortBy, StiSeriesSortType.None);
            jObject.AddPropertyEnum("SortDirection", sortDirection, StiSeriesSortDirection.Ascending);
            jObject.AddPropertyBool("ShowInLegend", showInLegend, true);
            jObject.AddPropertyEnum("ShowSeriesLabels", showSeriesLabels, StiShowSeriesLabels.FromChart);
            jObject.AddPropertyBool("ShowShadow", showShadow, true);
            jObject.AddPropertyEnum("FilterMode", filterMode, StiFilterMode.And);
            jObject.AddPropertyJObject("Filters", filters.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Conditions", conditions.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("TopN", TopN.SaveToJsonObject(mode));
            jObject.AddPropertyEnum("YAxis", yAxis, StiSeriesYAxis.LeftYAxis);
            jObject.AddPropertyJObject("SeriesLabels", seriesLabels.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("TrendLine", TrendLine.SaveToJsonObject(mode));
            jObject.AddPropertyStringNullOrEmpty("ValueDataColumn", ValueDataColumn);
            jObject.AddPropertyStringNullOrEmpty("ArgumentDataColumn", argumentDataColumn);
            jObject.AddPropertyStringNullOrEmpty("AutoSeriesTitleDataColumn", autoSeriesTitleDataColumn);
            jObject.AddPropertyStringNullOrEmpty("AutoSeriesKeyDataColumn", autoSeriesKeyDataColumn);
            jObject.AddPropertyStringNullOrEmpty("AutoSeriesColorDataColumn", autoSeriesColorDataColumn);
            jObject.AddPropertyStringNullOrEmpty("ToolTipDataColumn", toolTipDataColumn);
            jObject.AddPropertyStringNullOrEmpty("TagDataColumn", tagDataColumn);
            jObject.AddPropertyStringNullOrEmpty("HyperlinkDataColumn", hyperlinkDataColumn);
            jObject.AddPropertyBool("DrillDownEnabled", DrillDownEnabled);
            jObject.AddPropertyStringNullOrEmpty("DrillDownReport", DrillDownReport);
            jObject.AddPropertyStringNullOrEmpty("DrillDownPageGuid", drillDownPageGuid);
            jObject.AddPropertyBool("AllowSeries", allowSeries, true);
            jObject.AddPropertyBool("AllowSeriesElements", allowSeriesElements, true);
            jObject.AddPropertyJObject("Interaction", Interaction.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("NewAutoSeriesEvent", newAutoSeriesEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetValueEvent", getValueEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetListOfValuesEvent", getListOfValuesEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetArgumentEvent", getArgumentEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetListOfArgumentsEvent", getListOfArgumentsEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetTitleEvent", getTitleEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetToolTipEvent", getToolTipEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetListOfToolTipsEvent", getListOfToolTipsEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetTagEvent", getTagEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetListOfTagsEvent", getListOfTagsEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetHyperlinkEvent", GetHyperlinkEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetListOfHyperlinksEvent", getListOfHyperlinksEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Value", Value.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("ListOfValues", ListOfValues.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Argument", Argument.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("ListOfArguments", ListOfArguments.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Title", Title.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("ToolTip", ToolTip.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("ListOfToolTips", ListOfToolTips.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Tag", Tag.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("ListOfTags", ListOfTags.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Hyperlink", Hyperlink.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("ListOfHyperlinks", ListOfHyperlinks.SaveToJsonObject(mode));

            if (mode == StiJsonSaveMode.Document)
            {
                jObject.AddPropertyStringNullOrEmpty("ValuesString", ValuesString);
                jObject.AddPropertyStringNullOrEmpty("ArgumentsString", ArgumentsString);
                jObject.AddPropertyStringNullOrEmpty("ToolTipsString", ToolTipsString);
                jObject.AddPropertyStringNullOrEmpty("TagString", TagString);
                jObject.AddPropertyStringNullOrEmpty("HyperlinkString", HyperlinkString);
                jObject.AddPropertyStringNullOrEmpty("TitleValue", TitleValue);
            }

            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AllowApplyStyle":
                        this.AllowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "Format":
                        this.format = property.Value.ToObject<string>();
                        break;

                    case "SortBy":
                        this.sortBy = (StiSeriesSortType)Enum.Parse(typeof(StiSeriesSortType), property.Value.ToObject<string>());
                        break;

                    case "SortDirection":
                        this.sortDirection = (StiSeriesSortDirection)Enum.Parse(typeof(StiSeriesSortDirection), property.Value.ToObject<string>());
                        break;

                    case "ShowInLegend":
                        this.showInLegend = property.Value.ToObject<bool>();
                        break;

                    case "ShowSeriesLabels":
                        this.showSeriesLabels = (StiShowSeriesLabels)Enum.Parse(typeof(StiShowSeriesLabels), property.Value.ToObject<string>());
                        break;

                    case "ShowShadow":
                        this.showShadow = property.Value.ToObject<bool>();
                        break;

                    case "FilterMode":
                        this.filterMode = (StiFilterMode)Enum.Parse(typeof(StiFilterMode), property.Value.ToObject<string>());
                        break;

                    case "Filters":
                        this.filters.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "Conditions":
                        this.conditions.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "TopN":
                        this.TopN.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "YAxis":
                        this.yAxis = (StiSeriesYAxis)Enum.Parse(typeof(StiSeriesYAxis), property.Value.ToObject<string>());
                        break;

                    case "SeriesLabels":
                        this.seriesLabels = StiSeriesLabels.LoadFromJsonObjectInternal((JObject)property.Value, (StiChart)this.chart);
                        break;

                    case "TrendLine":
                        this.TrendLine = StiTrendLine.CreateFromJsonObject((JObject)property.Value);
                        break;

                    case "ValueDataColumn":
                        this.ValueDataColumn = property.Value.ToObject<string>();
                        break;

                    case "ArgumentDataColumn":
                        this.argumentDataColumn = property.Value.ToObject<string>();
                        break;

                    case "AutoSeriesTitleDataColumn":
                        this.autoSeriesTitleDataColumn = property.Value.ToObject<string>();
                        break;

                    case "AutoSeriesKeyDataColumn":
                        this.autoSeriesKeyDataColumn = property.Value.ToObject<string>();
                        break;

                    case "AutoSeriesColorDataColumn":
                        this.autoSeriesColorDataColumn = property.Value.ToObject<string>();
                        break;

                    case "ToolTipDataColumn":
                        this.toolTipDataColumn = property.Value.ToObject<string>();
                        break;

                    case "TagDataColumn":
                        this.tagDataColumn = property.Value.ToObject<string>();
                        break;

                    case "HyperlinkDataColumn":
                        this.hyperlinkDataColumn = property.Value.ToObject<string>();
                        break;

                    case "DrillDownEnabled":
                        this.DrillDownEnabled = property.Value.ToObject<bool>();
                        break;

                    case "DrillDownReport":
                        this.DrillDownReport = property.Value.ToObject<string>();
                        break;

                    case "DrillDownPageGuid":
                        this.drillDownPageGuid = property.Value.ToObject<string>();
                        break;

                    case "AllowSeries":
                        this.allowSeries = property.Value.ToObject<bool>();
                        break;

                    case "AllowSeriesElements":
                        this.allowSeriesElements = property.Value.ToObject<bool>();
                        break;

                    case "Interaction":
                        this.Interaction.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "NewAutoSeriesEvent":
                        {
                            var _newAutoSeriesEvent = new StiNewAutoSeriesEvent();
                            _newAutoSeriesEvent.LoadFromJsonObject((JObject)property.Value);
                            this.newAutoSeriesEvent = _newAutoSeriesEvent;
                        }
                        break;

                    case "GetValueEvent":
                        {
                            var _getValueEvent = new StiGetValueEvent();
                            _getValueEvent.LoadFromJsonObject((JObject)property.Value);
                            this.getValueEvent = _getValueEvent;
                        }
                        break;

                    case "GetListOfValuesEvent":
                        {
                            var _getListOfValuesEvent = new StiGetListOfValuesEvent();
                            _getListOfValuesEvent.LoadFromJsonObject((JObject)property.Value);
                            this.getListOfValuesEvent = _getListOfValuesEvent;
                        }
                        break;

                    case "GetArgumentEvent":
                        {
                            var _getArgumentEvent = new StiGetArgumentEvent();
                            _getArgumentEvent.LoadFromJsonObject((JObject)property.Value);
                            this.getArgumentEvent = _getArgumentEvent;
                        }
                        break;

                    case "GetListOfArgumentsEvent":
                        {
                            var _getListOfArgumentsEvent = new StiGetListOfArgumentsEvent();
                            _getListOfArgumentsEvent.LoadFromJsonObject((JObject)property.Value);
                            this.getListOfArgumentsEvent = _getListOfArgumentsEvent;
                        }
                        break;

                    case "GetTitleEvent":
                        {
                            var _getTitleEvent = new StiGetTitleEvent();
                            _getTitleEvent.LoadFromJsonObject((JObject)property.Value);
                            this.getTitleEvent = _getTitleEvent;
                        }
                        break;

                    case "GetToolTipEvent":
                        {
                            var _getToolTipEvent = new StiGetToolTipEvent();
                            _getToolTipEvent.LoadFromJsonObject((JObject)property.Value);
                            this.getToolTipEvent = _getToolTipEvent;
                        }
                        break;

                    case "GetListOfToolTipsEvent":
                        {
                            var _getListOfToolTipsEvent = new StiGetListOfToolTipsEvent();
                            _getListOfToolTipsEvent.LoadFromJsonObject((JObject)property.Value);
                            this.getListOfToolTipsEvent = _getListOfToolTipsEvent;
                        }
                        break;

                    case "GetTagEvent":
                        {
                            var _getTagEvent = new StiGetTagEvent();
                            _getTagEvent.LoadFromJsonObject((JObject)property.Value);
                            this.getTagEvent = _getTagEvent;
                        }
                        break;

                    case "GetListOfTagsEvent":
                        {
                            var _getListOfTagsEvent = new StiGetListOfTagsEvent();
                            _getListOfTagsEvent.LoadFromJsonObject((JObject)property.Value);
                            this.getListOfTagsEvent = _getListOfTagsEvent;
                        }
                        break;

                    case "GetHyperlinkEvent":
                        {
                            var _getHyperlinkEvent = new StiGetHyperlinkEvent();
                            _getHyperlinkEvent.LoadFromJsonObject((JObject)property.Value);
                            this.GetHyperlinkEvent = _getHyperlinkEvent;
                        }
                        break;

                    case "GetListOfHyperlinksEvent":
                        {
                            var _getListOfHyperlinksEvent = new StiGetListOfHyperlinksEvent();
                            _getListOfHyperlinksEvent.LoadFromJsonObject((JObject)property.Value);
                            this.getListOfHyperlinksEvent = _getListOfHyperlinksEvent;
                        }
                        break;

                    case "Value":
                        {
                            var _valueObj = new StiExpression();
                            _valueObj.LoadFromJsonObject((JObject)property.Value);
                            this.Value = _valueObj;
                        }
                        break;

                    case "ListOfValues":
                        {
                            var _expression = new StiListOfValuesExpression();
                            _expression.LoadFromJsonObject((JObject)property.Value);
                            this.ListOfValues = _expression;
                        }
                        break;

                    case "Argument":
                        {
                            var _argument = new StiArgumentExpression();
                            _argument.LoadFromJsonObject((JObject)property.Value);
                            this.Argument = _argument;
                        }
                        break;

                    case "ListOfArguments":
                        {
                            var _listOfArguments = new StiListOfArgumentsExpression();
                            _listOfArguments.LoadFromJsonObject((JObject)property.Value);
                            this.ListOfArguments = _listOfArguments;
                        }
                        break;

                    case "Title":
                        {
                            var _title = new StiTitleExpression();
                            _title.LoadFromJsonObject((JObject)property.Value);
                            this.Title = _title;
                        }
                        break;

                    case "ToolTip":
                        {
                            var _toolTip = new StiToolTipExpression();
                            _toolTip.LoadFromJsonObject((JObject)property.Value);
                            this.ToolTip = _toolTip;
                        }
                        break;

                    case "ListOfToolTips":
                        {
                            var _listOfToolTips = new StiListOfToolTipsExpression();
                            _listOfToolTips.LoadFromJsonObject((JObject)property.Value);
                            this.ListOfToolTips = _listOfToolTips;
                        }
                        break;

                    case "Tag":
                        {
                            var _tag = new StiTagExpression();
                            _tag.LoadFromJsonObject((JObject)property.Value);
                            this.Tag = _tag;
                        }
                        break;

                    case "ListOfTags":
                        {
                            var _listOfTags = new StiListOfTagsExpression();
                            _listOfTags.LoadFromJsonObject((JObject)property.Value);
                            this.ListOfTags = _listOfTags;
                        }
                        break;

                    case "Hyperlink":
                        {
                            var _hyperlink = new StiHyperlinkExpression();
                            _hyperlink.LoadFromJsonObject((JObject)property.Value);
                            this.Hyperlink = _hyperlink;
                        }
                        break;

                    case "ListOfHyperlinks":
                        {
                            var _expression = new StiListOfHyperlinksExpression();
                            _expression.LoadFromJsonObject((JObject)property.Value);
                            this.ListOfHyperlinks = _expression;
                        }
                        break;

                    case "ValuesString":
                        this.ValuesString = property.Value.ToObject<string>();
                        break;

                    case "ArgumentsString":
                        this.ArgumentsString = property.Value.ToObject<string>();
                        break;

                    case "ToolTipsString":
                        this.ToolTipsString = property.Value.ToObject<string>();
                        break;

                    case "TagString":
                        this.TagString = property.Value.ToObject<string>();
                        break;

                    case "HyperlinkString":
                        this.HyperlinkString = property.Value.ToObject<string>();
                        break;

                    case "TitleValue":
                        this.TitleValue = property.Value.ToObject<string>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public virtual StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiSeries;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public abstract StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level);

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // RenderEventsCategory
            var list = new[]
            {
                StiPropertyEventId.NewAutoSeriesEvent
            };
            objectHelper.Add(StiPropertyCategories.RenderEvents, list);

            list = new[] {
                StiPropertyEventId.GetValueEvent,
                StiPropertyEventId.GetListOfValuesEvent,
                StiPropertyEventId.GetArgumentEvent,
                StiPropertyEventId.GetListOfArgumentsEvent,
                StiPropertyEventId.GetTitleEvent,
                StiPropertyEventId.GetToolTipEvent,
                StiPropertyEventId.GetListOfToolTipsEvent,
                StiPropertyEventId.GetTagEvent,
                StiPropertyEventId.GetListOfTagsEvent,
                StiPropertyEventId.GetHyperlinkEvent,
                StiPropertyEventId.GetListOfHyperlinksEvent
            };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            return objectHelper;
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public override object Clone()
        {
            StiSeries series = base.Clone() as StiSeries;
            series.Title = this.Title.Clone() as StiTitleExpression;
            series.Values = this.Values.Clone() as double?[];
            series.Arguments = this.Arguments.Clone() as object[];
            series.Tags = this.Tags.Clone() as object[];
            series.ToolTips = this.ToolTips.Clone() as string[];
            series.Hyperlinks = this.Hyperlinks.Clone() as string[];
            series.TopN = this.TopN.Clone() as StiSeriesTopN;
            series.Interaction = this.Interaction.Clone() as IStiSeriesInteraction;
            series.SeriesLabels = this.SeriesLabels.Clone() as IStiSeriesLabels;

            if (this.Core != null)
            {
                series.Core = this.Core.Clone() as StiSeriesCoreXF;
                series.Core.Series = series;
            }

            return series;
        }
        #endregion        
                
        #region IStiChartPainter
        protected virtual void BaseTransform(object context, float x, float y, float angle, float dx, float dy)
        {

        }
        #endregion

        #region IStiSeriesParent
        [Browsable(false)]
        public StiComponent Parent
        {
            get
            {
                return Chart as StiComponent;
            }
        }
        #endregion

        #region StiService override
        /// <summary>
        /// Gets a service name.
        /// </summary>
        public override string ServiceName
        {
            get
            {
                return Core.LocalizedName;
            }
        }

        /// <summary>
        /// Gets a service category.
        /// </summary>
        [Browsable(false)]
        public sealed override string ServiceCategory
        {
            get
            {
                return "Chart";
            }
        }

        /// <summary>
        /// Gets a service type.
        /// </summary>
        [Browsable(false)]
        public sealed override Type ServiceType
        {
            get
            {
                return typeof(StiSeries);
            }
        }
        #endregion

        #region Properties
        private StiSeriesCoreXF core;
        [Browsable(false)]
        public StiSeriesCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }


        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public virtual bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                }
            }
        }


        private string format = "";
        /// <summary>
        /// Gets or sets string that is used to format series values.
        /// </summary>
		[StiOrder(StiSeriesPropertyOrder.DataFormat)]
        [DefaultValue("")]
        [StiCategory("Common")]
        [StiSerializable]
        [Editor("Stimulsoft.Report.Chart.Design.StiFormatEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets string that is used to format series values.")]
        public string Format
        {
            get
            {
                return format;
            }
            set
            {
                format = value;
            }
        }


        private StiSeriesSortType sortBy = StiSeriesSortType.None;
        /// <summary>
        /// Gets or sets mode of series values sorting.
        /// </summary>
		[StiOrder(StiSeriesPropertyOrder.DataSortBy)]
        [DefaultValue(StiSeriesSortType.None)]
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets mode of series values sorting.")]
        public virtual StiSeriesSortType SortBy
        {
            get
            {
                return sortBy;
            }
            set
            {
                sortBy = value;
            }
        }


        private StiSeriesSortDirection sortDirection = StiSeriesSortDirection.Ascending;
        /// <summary>
        /// Gets or sets sort direction.
        /// </summary>
		[StiOrder(StiSeriesPropertyOrder.DataSortDirection)]
        [DefaultValue(StiSeriesSortDirection.Ascending)]
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets sort direction.")]
        public virtual StiSeriesSortDirection SortDirection
        {
            get
            {
                return sortDirection;
            }
            set
            {
                sortDirection = value;
            }
        }


        private bool showInLegend = true;
        /// <summary>
        /// Gets or sets value which indicates that series must be shown in legend.
        /// </summary>
		[DefaultValue(true)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets value which indicates that series must be shown in legend.")]
        public bool ShowInLegend
        {
            get
            {
                return showInLegend;
            }
            set
            {
                showInLegend = value;
            }
        }


        [DefaultValue(true)]
        [Browsable(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Common")]
        [Obsolete("Please use ShowSeriesLabels property instead ShowLabels property.")]
        public bool ShowLabels
        {
            get
            {
                return true;
            }
            set
            {
                if (value) ShowSeriesLabels = StiShowSeriesLabels.FromChart;
            }
        }


        private StiShowSeriesLabels showSeriesLabels = StiShowSeriesLabels.FromChart;
        /// <summary>
        /// Gets or sets series labels output mode.
        /// </summary>
        [DefaultValue(StiShowSeriesLabels.FromChart)]
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiShowSeriesLabelsEnumConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets series labels output mode.")]
        public StiShowSeriesLabels ShowSeriesLabels
        {
            get
            {
                return showSeriesLabels;
            }
            set
            {
#pragma warning disable 612, 618
                showSeriesLabels = (value == StiShowSeriesLabels.None) ? StiShowSeriesLabels.FromChart : value;
#pragma warning restore 612, 618
            }
        }


        private bool showShadow = true;
        /// <summary>
        /// Gets or sets value which indicates draw shadow or no.
        /// </summary>
		[DefaultValue(true)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Appearance")]
        [Description("Gets or sets value which indicates draw shadow or no.")]
        public bool ShowShadow
        {
            get
            {
                return showShadow;
            }
            set
            {
                showShadow = value;
            }
        }


        private StiFilterMode filterMode = StiFilterMode.And;
        /// <summary>
        /// Gets or sets filter mode of series.
        /// </summary>
		[StiOrder(StiSeriesPropertyOrder.DataFilterMode)]
        [DefaultValue(StiFilterMode.And)]
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiCategory("Common")]
        [Browsable(false)]
        [Description("Gets or sets filter mode of series.")]
        public StiFilterMode FilterMode
        {
            get
            {
                return filterMode;
            }
            set
            {
                filterMode = value;
            }
        }


        private StiChartFiltersCollection filters = new StiChartFiltersCollection();
        /// <summary>
        /// Gets or sets filters which used to filter series values.
        /// </summary>
		[StiOrder(StiSeriesPropertyOrder.DataFilters)]
        [StiSerializable(StiSerializationVisibility.List)]
        [StiCategory("Common")]
        [Description("Gets or sets filters which used to filter series values.")]
        [Browsable(false)]
        public StiChartFiltersCollection Filters
        {
            get
            {
                return filters;
            }
            set
            {
                filters = value;
            }
        }


        private StiChartConditionsCollection conditions = new StiChartConditionsCollection();
        /// <summary>
        /// Gets or sets collection of conditions which can be used to change behavior of series.
        /// </summary>
		[StiOrder(StiSeriesPropertyOrder.DataConditions)]
        [StiSerializable(StiSerializationVisibility.List)]
        [StiCategory("Common")]
        [Browsable(false)]
        [TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiChartConditionsCollectionConverter))]
        [Description("Gets or sets collection of conditions which can be used to change behavior of series.")]
        public virtual StiChartConditionsCollection Conditions
        {
            get
            {
                return conditions;
            }
            set
            {
                conditions = value;
            }
        }

        private IStiSeriesTopN topN = new StiSeriesTopN();
        /// <summary>
        /// Gets or sets parameters of displaying top results.
        /// </summary>
        [StiOrder(StiSeriesPropertyOrder.DataTopN)]
        [StiCategory("Common")]
        [StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiSeriesTopNConverter))]
        [Description("Gets or sets parameters of displaying top results.")]
        [Browsable(false)]
        public virtual IStiSeriesTopN TopN
        {
            get
            {
                return topN;
            }
            set
            {
                topN = value;
            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public virtual bool TopNAllowed
        {
            get
            {
                return true;
            }
        }

        private StiSeriesYAxis yAxis = StiSeriesYAxis.LeftYAxis;
        /// <summary>
        /// Gets or sets Y Axis for series on which will output string representation of arguments.
        /// </summary>
        [DefaultValue(StiSeriesYAxis.LeftYAxis)]
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets Y Axis for series on which will output string representation of arguments.")]
        public virtual StiSeriesYAxis YAxis
        {
            get
            {
                return yAxis;
            }
            set
            {
                yAxis = value;
            }
        }


        private IStiSeriesLabels seriesLabels;
        /// <summary>
        /// Gets or sets series labels settings.
        /// </summary>
        [StiSerializable(StiSerializationVisibility.Class)]
        [StiCategory("Common")]
        [Description("Gets or sets series labels settings.")]
        [TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiSeriesLabelsConverter))]
        [Browsable(false)]
        public IStiSeriesLabels SeriesLabels
        {
            get
            {
                return seriesLabels;
            }
            set
            {
                seriesLabels = value;
                seriesLabels.Chart = this.Chart;
            }
        }

        private IStiTrendLine trendLine;
        /// <summary>
        /// Gets or sets trend line settings.
        /// </summary>
        [StiSerializable(StiSerializationVisibility.Class)]
        [StiCategory("Common")]
        [Description("Gets or sets trend line settings.")]
        [Browsable(false)]
        [TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiTrendLineConverter))]
        public virtual IStiTrendLine TrendLine
        {
            get
            {
                return trendLine;
            }
            set
            {
                if (trendLine != value)
                {
                    trendLine = value;
                }
            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public virtual bool TrendLineAllowed
        {
            get
            {
                return true;
            }
        }

        private IStiChart chart;
        [StiSerializable(StiSerializationVisibility.Reference)]
        [Browsable(false)]
        public IStiChart Chart
        {
            get
            {
                return chart;
            }
            set
            {
                if (chart != value)
                {
                    chart = value;

                    if (value != null)
                    {
                        this.SeriesLabels.Chart = value;
                    }
                }
            }
        }
        #endregion

        #region Properties.Data

        #region Values Old
        private double?[] valuesOld = null;
        [Browsable(false)]
        public double?[] ValuesStart
        {
            get
            {
                return valuesOld;
            }
        }
        #endregion

        #region Values
        private double?[] values = { 1, 3, 2 };
        [Browsable(false)]
        public double?[] Values
        {
            get
            {
                if (Chart == null || !Chart.IsDesigning || IsDashboard)
                    return values;

                if (!string.IsNullOrEmpty(ListOfValues.Value) && !ListOfValues.Value.Contains("{"))
                    return StiSeries.GetNullableValuesFromString(this, ListOfValues.Value);

                var seriesIndex = Chart.Series.IndexOf(this);
                if (this is IStiFunnelSeries)
                    return new double?[] { 3, 2, 1 };

                if (this is IStiGanttSeries)
                    return new double?[] { 1 + seriesIndex * 3, 3 + seriesIndex * 4, 7 + seriesIndex * 3 };

                if (this is IStiScatterSeries)
                    return new double?[] { 1 + seriesIndex, 6 + seriesIndex, 2 + seriesIndex };

                if (this is IStiPieSeries)
                {
                    return seriesIndex == 0 
                        ? new double?[] { 1, 3, 2 } 
                        : new double?[] { 1 + seriesIndex, 3 + seriesIndex, 4 + seriesIndex };
                }
                else
                {
                    if (this is StiRadarSeries)
                        return new double?[] { 1 + seriesIndex, 2 + seriesIndex, 3 + seriesIndex, 4 + seriesIndex, 5 + seriesIndex };

                    if (seriesIndex == 0)
                        return values;

                    return new double?[] { 1 + seriesIndex, 3 + seriesIndex, 2.5 + seriesIndex };
                }
            }
            set
            {
                this.valuesOld = values;
                values = value;
            }
        }


        private string valueDataColumn = string.Empty;
        /// <summary>
        /// Gets or sets a name of the column that contains the point value.
        /// </summary>
        [DefaultValue("")]
        [StiSerializable]
        [StiOrder(StiSeriesPropertyOrder.ValueValueDataColumn)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Value")]
        [Description("Gets or sets a name of the column that contains the value.")]
        public virtual string ValueDataColumn
        {
            get
            {
                return valueDataColumn;
            }
            set
            {
                valueDataColumn = value;
            }
        }


        [Browsable(false)]
        [StiSerializable(StiSerializeTypes.SerializeToDocument)]
        public string ValuesString
        {
            get
            {
                var sb = new StringBuilder();
                bool first = true;
                foreach (double? value in values)
                {
                    if (first) sb.AppendFormat("{0}", value == null ? 0d : value);
                    else sb.AppendFormat(";{0}", value == null ? 0d : value);
                    first = false;
                }
                return sb.ToString();
            }
            set
            {
                if (value == null || value.Trim().Length == 0)
                {
                    values = new double?[0];
                }
                else
                {
                    string[] strs = value.Split(new char[] { ';' });

                    values = new double?[strs.Length];

                    int index = 0;
                    foreach (string str in strs)
                    {
                        values[index++] = double.Parse(str);
                    }
                }
            }
        }
        #endregion

        #region Arguments
        private object[] arguments = new object[0];
        [Browsable(false)]
        public virtual object[] Arguments
        {
            get
            {
                if (Chart == null || !Chart.IsDesigning || IsDashboard)
                    return arguments;

                if (!string.IsNullOrEmpty(ListOfArguments.Value))
                    return GetArgumentsFromString(ListOfArguments.Value);

                if ((this is IStiGanttSeries || this is IStiRangeBarSeries || this is IStiCandlestickSeries) && (arguments == null || arguments.Length == 0))
                    return new object[] { "A", "B", "C" };

                if ((this is IStiScatterSeries || this is IStiScatterLineSeries || this is IStiScatterSplineSeries) && (arguments == null || arguments.Length == 0))
                    return new object[] { "1", "5", "4" };

                return arguments;
            }
            set
            {
                arguments = value;
            }
        }


        private string argumentDataColumn = string.Empty;
        /// <summary>
        /// Gets or sets a name of the column that contains the argument value.
        /// </summary>
        [StiSerializable]
        [StiOrder(StiSeriesPropertyOrder.ArgumentArgumentDataColumn)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Argument")]
        [DefaultValue("")]
        [Description("Gets or sets a name of the column that contains the argument value.")]
        public string ArgumentDataColumn
        {
            get
            {
                return argumentDataColumn;
            }
            set
            {
                argumentDataColumn = value;
            }
        }


        [Browsable(false)]
        [StiSerializable(StiSerializeTypes.SerializeToDocument)]
        public string ArgumentsString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                bool first = true;
                foreach (object arg in arguments)
                {
                    if (arg != null)
                    {
                        if (first) sb.AppendFormat("{0}", XmlConvert.EncodeName(arg.ToString()));
                        else sb.AppendFormat(";{0}", XmlConvert.EncodeName(arg.ToString()));
                        first = false;
                    }
                }
                return sb.ToString();
            }
            set
            {
                if (value == null || value.Trim().Length == 0)
                {
                    arguments = new object[0];
                }
                else
                {
                    string[] strs = value.Split(new char[] { ';' });

                    arguments = new object[strs.Length];

                    int index = 0;
                    foreach (string str in strs)
                    {
                        arguments[index++] = XmlConvert.DecodeName(str);
                    }
                }
            }
        }
        #endregion

        #region AutoSeries
        private string autoSeriesTitleDataColumn = string.Empty;
        /// <summary>
        /// Gets or sets a name of the column that contains the title of auto created series.
        /// </summary>
        [StiSerializable]
        [StiOrder(StiSeriesPropertyOrder.DataAutoSeriesTitleDataColumn)]
        [DefaultValue("")]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Common")]
        [Description("Gets or sets a name of the column that contains the title of auto created series.")]
        public string AutoSeriesTitleDataColumn
        {
            get
            {
                return autoSeriesTitleDataColumn;
            }
            set
            {
                autoSeriesTitleDataColumn = value;
            }
        }


        private string autoSeriesKeyDataColumn = string.Empty;
        /// <summary>
        /// Gets or sets a name of the column that contains the key of auto created series.
        /// </summary>
        [StiSerializable]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Common")]
        [StiOrder(StiSeriesPropertyOrder.DataAutoSeriesKeyDataColumn)]
        [DefaultValue("")]
        [Description("Gets or sets a name of the column that contains the key of auto created series.")]
        public string AutoSeriesKeyDataColumn
        {
            get
            {
                return autoSeriesKeyDataColumn;
            }
            set
            {
                autoSeriesKeyDataColumn = value;
            }
        }


        private string autoSeriesColorDataColumn = string.Empty;
        /// <summary>
        /// Gets or sets a name of the column that contains the color of auto created series. Color must be presented as string.
        /// </summary>
        [StiSerializable]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Common")]
        [StiOrder(StiSeriesPropertyOrder.DataAutoSeriesColorDataColumn)]
        [DefaultValue("")]
        [Description("Gets or sets a name of the column that contains the color of auto created series. Color must be presented as string.")]
        public string AutoSeriesColorDataColumn
        {
            get
            {
                return autoSeriesColorDataColumn;
            }
            set
            {
                autoSeriesColorDataColumn = value;
            }
        }
        #endregion

        #region ToolTips
        private string[] toolTips = new string[0];
        [Browsable(false)]
        public virtual string[] ToolTips
        {
            get
            {
                if (Chart == null || !Chart.IsDesigning || IsDashboard)
                    return toolTips;

                if (!string.IsNullOrEmpty(ListOfToolTips.Value))
                    return GetStringsFromString(ListOfToolTips.Value);

                return toolTips;
            }
            set
            {
                toolTips = value;
            }
        }


        private string toolTipDataColumn = string.Empty;
        /// <summary>
        /// Gets or sets a name of the column that contains the tool tip value.
        /// </summary>
        [StiSerializable]
        [Browsable(false)]
        [StiOrder(StiSeriesPropertyOrder.ToolTipDataColumn)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Argument")]
        [DefaultValue("")]
        [Description("Gets or sets a name of the column that contains the tool tip value.")]
        public string ToolTipDataColumn
        {
            get
            {
                return toolTipDataColumn;
            }
            set
            {
                toolTipDataColumn = value;
            }
        }


        [Browsable(false)]
        [StiSerializable(StiSerializeTypes.SerializeToDocument)]
        public string ToolTipsString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                bool first = true;
                foreach (object arg in toolTips)
                {
                    if (arg != null)
                    {
                        if (first) sb.AppendFormat("{0}", XmlConvert.EncodeName(arg.ToString()));
                        else sb.AppendFormat(";{0}", XmlConvert.EncodeName(arg.ToString()));
                        first = false;
                    }
                }
                return sb.ToString();
            }
            set
            {
                if (value == null || value.Trim().Length == 0)
                {
                    toolTips = new string[0];
                }
                else
                {
                    string[] strs = value.Split(new char[] { ';' });

                    toolTips = new string[strs.Length];

                    int index = 0;
                    foreach (string str in strs)
                    {
                        toolTips[index++] = XmlConvert.DecodeName(str);
                    }
                }
            }
        }
        #endregion

        #region Tags
        private object[] tags = new object[0];
        [Browsable(false)]
        public virtual object[] Tags
        {
            get
            {
                if (Chart == null || !Chart.IsDesigning || IsDashboard)
                    return tags;

                if (!string.IsNullOrEmpty(ListOfTags.Value))
                    return GetArgumentsFromString(ListOfTags.Value);

                return tags;
            }
            set
            {
                tags = value;
            }
        }


        private string tagDataColumn = string.Empty;
        /// <summary>
        /// Gets or sets a name of the column that contains the tag value.
        /// </summary>
        [StiSerializable]
        [Browsable(false)]
        [StiOrder(StiSeriesPropertyOrder.TagDataColumn)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Argument")]
        [DefaultValue("")]
        [Description("Gets or sets a name of the column that contains the tag value.")]
        public string TagDataColumn
        {
            get
            {
                return tagDataColumn;
            }
            set
            {
                tagDataColumn = value;
            }
        }


        [Browsable(false)]
        [StiSerializable(StiSerializeTypes.SerializeToDocument)]
        public string TagString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                bool first = true;
                foreach (object arg in tags)
                {
                    if (arg != null)
                    {
                        if (first) sb.AppendFormat("{0}", XmlConvert.EncodeName(arg.ToString()));
                        else sb.AppendFormat(";{0}", XmlConvert.EncodeName(arg.ToString()));
                        first = false;
                    }
                }
                return sb.ToString();
            }
            set
            {
                if (value == null || value.Trim().Length == 0)
                {
                    tags = new object[0];
                }
                else
                {
                    string[] strs = value.Split(new char[] { ';' });

                    tags = new object[strs.Length];

                    int index = 0;
                    foreach (string str in strs)
                    {
                        tags[index++] = XmlConvert.DecodeName(str);
                    }
                }
            }
        }
        #endregion

        #region Hyperlinks
        private string[] hyperlinks = new string[0];
        [Browsable(false)]
        public virtual string[] Hyperlinks
        {
            get
            {
                if (Chart == null || !Chart.IsDesigning || IsDashboard)
                    return hyperlinks;

                if (!string.IsNullOrEmpty(ListOfHyperlinks.Value))
                    return GetStringsFromString(ListOfHyperlinks.Value);

                return hyperlinks;
            }
            set
            {
                hyperlinks = value;
            }
        }


        private string hyperlinkDataColumn = string.Empty;
        /// <summary>
        /// Gets or sets a name of the column that contains the hyperlink value.
        /// </summary>
        [StiSerializable]
        [Browsable(false)]
        [StiOrder(StiSeriesPropertyOrder.HyperlinkDataColumn)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Argument")]
        [DefaultValue("")]
        [Description("Gets or sets a name of the column that contains the hyperlink value.")]
        public string HyperlinkDataColumn
        {
            get
            {
                return hyperlinkDataColumn;
            }
            set
            {
                hyperlinkDataColumn = value;
            }
        }


        [Browsable(false)]
        [StiSerializable(StiSerializeTypes.SerializeToDocument)]
        public string HyperlinkString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                bool first = true;
                foreach (object arg in hyperlinks)
                {
                    if (arg != null)
                    {
                        if (first) sb.AppendFormat("{0}", XmlConvert.EncodeName(arg.ToString()));
                        else sb.AppendFormat(";{0}", XmlConvert.EncodeName(arg.ToString()));
                        first = false;
                    }
                }
                return sb.ToString();
            }
            set
            {
                if (value == null || value.Trim().Length == 0)
                {
                    hyperlinks = new string[0];
                }
                else
                {
                    string[] strs = value.Split(new char[] { ';' });

                    hyperlinks = new string[strs.Length];

                    int index = 0;
                    foreach (string str in strs)
                    {
                        hyperlinks[index++] = XmlConvert.DecodeName(str);
                    }
                }
            }
        }
        #endregion
        #endregion

        #region Properties.DrillDown
        private bool drillDownEnabled = false;
        /// <summary>
        /// Gets or sets value which indicates whether the Drill-Down operation can be executed.
        /// </summary>
        [StiSerializable]
        [Browsable(false)]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates whether the Drill-Down operation can be executed.")]
        [StiOrder(StiSeriesPropertyOrder.DrillDownEnabled)]
        public virtual bool DrillDownEnabled
        {
            get
            {
                return drillDownEnabled;
            }
            set
            {
                drillDownEnabled = value;
            }
        }

        private string drillDownReport = string.Empty;
        /// <summary>
        /// Gets or sets a path to a report for the Drill-Down operation.
        /// </summary>
        [StiSerializable]
        [Browsable(false)]
        [DefaultValue("")]
        [Description("Gets or sets a path to a report for the Drill-Down operation.")]
        [StiOrder(StiSeriesPropertyOrder.DrillDownReport)]
        public virtual string DrillDownReport
        {
            get
            {
                return drillDownReport;
            }
            set
            {
                drillDownReport = value;
            }
        }

        /// <summary>
        /// Gets or sets a page for the Drill-Down operation.
        /// </summary>
        [StiNonSerialized]
        [Browsable(false)]
        [Editor("Stimulsoft.Report.Components.Design.StiInteractionDrillDownPageEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [DefaultValue(null)]
        [Description("Gets or sets a page for the Drill-Down operation.")]
        [StiOrder(StiSeriesPropertyOrder.DrillDownPage)]
        public virtual StiPage DrillDownPage
        {
            get
            {
                if (Chart == null || ((StiChart)Chart).Report == null) return null;
                foreach (StiPage page in ((StiChart)Chart).Report.Pages)
                {
                    if (page.Guid == this.DrillDownPageGuid) return page;
                }
                return null;
            }
            set
            {
                if (value == null) this.drillDownPageGuid = null;
                else
                {
                    if (value.Guid == null) value.Guid = StiGuidUtils.NewGuid();
                    this.drillDownPageGuid = value.Guid;
                }
            }
        }

        private string drillDownPageGuid = null;
        [Browsable(false)]
        [StiSerializable]
        [DefaultValue(null)]
        public string DrillDownPageGuid
        {
            get
            {
                return drillDownPageGuid;
            }
            set
            {
                drillDownPageGuid = value;
            }
        }


        private bool allowSeries = true;
        [Browsable(false)]
        [StiSerializable]
        [DefaultValue(true)]
        public bool AllowSeries
        {
            get
            {
                return allowSeries;
            }
            set
            {
                allowSeries = value;
            }
        }

        private bool allowSeriesElements = true;
        [Browsable(false)]
        [StiSerializable]
        [DefaultValue(true)]
        public bool AllowSeriesElements
        {
            get
            {
                return allowSeriesElements;
            }
            set
            {
                allowSeriesElements = value;
            }
        }
        #endregion

        #region Properties.Core
        [Browsable(false)]
        public string CoreTitle
        {
            get
            {
                var value = Chart != null && Chart.IsDesigning ? Title.Value : TitleValue;

                if (value == null)
                    value = string.Empty;

                return value;
            }
            set
            {
                Title.Value = value;
                TitleValue = value;
            }
        }

        /// <summary>
        /// Gets or sets value which indicates that this series is used in dashboards mode and IsDesign property will ingore.
        /// </summary>
        [Browsable(false)]
        internal bool IsDashboard { get; set; }
        #endregion

        #region Interaction
        private IStiSeriesInteraction interaction;
        /// <summary>
        /// Gets interaction options of this series.
        /// </summary>
        [StiCategory("Common")]
        [Description("Gets interaction options of this series.")]
        [TypeConverter(typeof(Stimulsoft.Report.Components.Design.StiSeriesInteractionConverter))]
        [RefreshProperties(RefreshProperties.All)]
        [Browsable(false)]
        public virtual IStiSeriesInteraction Interaction
        {
            get
            {
                return interaction;
            }
            set
            {
                if (this.interaction != value)
                {
                    interaction = value;
                    if (value != null)
                        ((StiSeriesInteraction)interaction).ParentSeries = this;
                }
            }
        }
        #endregion

        #region Methods
        public Color ProcessSeriesColors(int pointIndex, Color seriesColor)
        {
            foreach (StiChartCondition condition in this.Conditions)
            {
                if (GetConditionResult(pointIndex, condition))
                {
                    return condition.Color;
                }
            }
            return seriesColor;
        }

        public StiBrush ProcessSeriesBrushes(int pointIndex, StiBrush seriesBrush)
        {
            foreach (StiChartCondition condition in this.Conditions)
            {
                if (GetConditionResult(pointIndex, condition))
                {
                    if (this.AllowApplyStyle && this.Chart.Style != null)
                        seriesBrush = this.Chart.Style.Core.GetColumnBrush(condition.Color);
                    else
                    {
                        seriesBrush = new StiSolidBrush(condition.Color);
                    }
                    if (this.Chart.Area is IStiClusteredBarArea)
                    {
                        if (seriesBrush is StiGradientBrush) ((StiGradientBrush)seriesBrush).Angle += 90;
                        if (seriesBrush is StiGlareBrush) ((StiGlareBrush)seriesBrush).Angle += 90;
                    }
                    return seriesBrush;
                }
            }
            return seriesBrush;
        }

        private bool GetConditionResult(int pointIndex, StiChartCondition condition)
        {
            if ((this.Values == null || this.Values.Length <= pointIndex) && !(this is IStiFinancialSeries)) return false;

            object itemValue = null;
            object itemValueEnd = null;

            object itemValueOpen = null;
            object itemValueClose = null;
            object itemValueLow = null;
            object itemValueHigh = null;

            object itemArgument = null;

            if ((this.chart.Area is IStiAxisArea && ((IStiAxisArea)this.chart.Area).ReverseHor && !(this.Chart.Area is IStiClusteredBarArea)) ||
                (this.Chart.Area is IStiClusteredBarArea && ((IStiAxisArea)this.chart.Area).ReverseVert))
            {
                if (this.Values != null && pointIndex < this.Values.Length)
                    itemValue = this.Values[this.Values.Length - pointIndex - 1];

                if (this is IStiRangeSeries && pointIndex < ((IStiRangeSeries)this).ValuesEnd.Length)
                    itemValueEnd = ((IStiRangeSeries)this).ValuesEnd[((IStiRangeSeries)this).ValuesEnd.Length - pointIndex - 1];

                if (this is IStiFinancialSeries)
                {
                    if (pointIndex < ((IStiFinancialSeries)this).ValuesOpen.Length)
                        itemValueOpen = ((IStiFinancialSeries)this).ValuesOpen[((IStiFinancialSeries)this).ValuesOpen.Length - pointIndex - 1];

                    if (pointIndex < ((IStiFinancialSeries)this).ValuesClose.Length)
                        itemValueClose = ((IStiFinancialSeries)this).ValuesClose[((IStiFinancialSeries)this).ValuesClose.Length - pointIndex - 1];

                    if (pointIndex < ((IStiFinancialSeries)this).ValuesLow.Length)
                        itemValueLow = ((IStiFinancialSeries)this).ValuesLow[((IStiFinancialSeries)this).ValuesLow.Length - pointIndex - 1];

                    if (pointIndex < ((IStiFinancialSeries)this).ValuesHigh.Length)
                        itemValueHigh = ((IStiFinancialSeries)this).ValuesHigh[((IStiFinancialSeries)this).ValuesHigh.Length - pointIndex - 1];
                }

                if (this.Arguments != null && pointIndex < this.Arguments.Length)
                    itemArgument = this.Arguments[this.Arguments.Length - pointIndex - 1];
            }
            else
            {
                if (this.Values != null && pointIndex < this.Values.Length)
                    itemValue = this.Values[pointIndex];

                if (this is IStiRangeSeries && pointIndex < ((IStiRangeSeries)this).ValuesEnd.Length)
                    itemValueEnd = ((IStiRangeSeries)this).ValuesEnd[pointIndex];

                if (this is IStiFinancialSeries)
                {
                    if (pointIndex < ((IStiFinancialSeries)this).ValuesOpen.Length)
                        itemValueOpen = ((IStiFinancialSeries)this).ValuesOpen[pointIndex];

                    if (pointIndex < ((IStiFinancialSeries)this).ValuesClose.Length)
                        itemValueClose = ((IStiFinancialSeries)this).ValuesClose[pointIndex];

                    if (pointIndex < ((IStiFinancialSeries)this).ValuesLow.Length)
                        itemValueLow = ((IStiFinancialSeries)this).ValuesLow[pointIndex];

                    if (pointIndex < ((IStiFinancialSeries)this).ValuesHigh.Length)
                        itemValueHigh = ((IStiFinancialSeries)this).ValuesHigh[pointIndex];
                }

                if (this.Arguments != null && pointIndex < this.Arguments.Length)
                    itemArgument = this.Arguments[pointIndex];
            }
            object data = StiChartHelper.GetFilterData(null, condition, null);

            return StiChartHelper.GetFilterResult(condition, itemArgument, itemValue, itemValueEnd, itemValueOpen,
                                                  itemValueClose, itemValueLow, itemValueHigh, data);
        }

        public override string ToString()
        {
            return ServiceName;
        }

        public static double?[] GetNullableValuesFromString(StiSeries series, string list)
        {
            var alValue = new List<double?>();

            string[] points = list.Split(new char[] { ';' });
            foreach (string point in points)
            {
                if (point != null && point.Trim().Length > 0)
                {
                    double? value = 0d;

                    double result;
                    if (double.TryParse(point, out result))
                    {
                        value = result;
                    }
                    else
                    {
                        DateTime resultDateTime;
                        if (!string.IsNullOrEmpty(((StiChart)series.Chart).Report.Culture))
                        {
                            #region Try Parse with report Culture
                            try
                            {
                                var culture = new CultureInfo(((StiChart)series.Chart).Report.Culture);

                                if (DateTime.TryParse(point, culture, DateTimeStyles.None, out resultDateTime))
                                {
                                    series.Core.IsDateTimeValues = true;
                                    value = resultDateTime.ToOADate();
                                }
                            }
                            catch
                            {
                            }
                            #endregion
                        }
                        else
                        {
                            if (DateTime.TryParse(point, out resultDateTime))
                            {
                                #region Try Parse with System Culture
                                series.Core.IsDateTimeValues = true;
                                value = resultDateTime.ToOADate();
                                #endregion
                            }
                            else
                            {
                                if (DateTime.TryParse(point, new CultureInfo("en-US", false), DateTimeStyles.None, out resultDateTime))
                                {
                                    #region Try Parse with en-US Culture
                                    series.Core.IsDateTimeValues = true;
                                    value = resultDateTime.ToOADate();
                                    #endregion
                                }
                            }
                        }
                    }

                    alValue.Add(value);
                }
            }

            return alValue.ToArray();
        }

        public static double[] GetValuesFromString(string list)
        {
            var alValue = new List<double>();

            string[] points = list.Split(new char[] { ';' });
            foreach (string point in points)
            {
                if (point != null && point.Trim().Length > 0)
                {
                    double value = 0d;
                    try
                    {
                        value = (double)StiReport.ChangeType(point, typeof(double));
                    }
                    catch
                    {
                    }
                    alValue.Add(value);
                }
            }

            return alValue.ToArray();
        }

        public static string[] GetStringsFromString(string list)
        {
            var alValue = new List<string>();

            var points = list.Split(new char[] { ';' });
            foreach (string point in points)
            {
                if (point != null && point.Trim().Length > 0)
                {
                    alValue.Add(point);
                }
            }

            return alValue.ToArray();
        }


        public static object[] GetArgumentsFromString(string list)
        {
            return list.Split(new char[] { ';' });
        }
        #endregion

        #region Methods.virtual
        public virtual StiSeries CreateNew()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Methods.Types
        public abstract Type GetDefaultAreaType();
        #endregion

        #region Events
        #region NewAutoSeries
        /// <summary>
        /// Occurs when new auto series is created.
        /// </summary>
        public event StiNewAutoSeriesEventHandler NewAutoSeries;

        public void InvokeNewAutoSeries(StiNewAutoSeriesEventArgs e)
        {
            if (this.NewAutoSeries != null) this.NewAutoSeries(e.Series, e);
        }


        private StiNewAutoSeriesEvent newAutoSeriesEvent = new StiNewAutoSeriesEvent();
        /// <summary>
        /// Occurs when new auto series is created.
        /// </summary>
        [StiSerializable]
        [StiCategory("RenderEvents")]
        [Browsable(false)]
        [Description("Occurs when new auto series is created.")]
        public StiNewAutoSeriesEvent NewAutoSeriesEvent
        {
            get
            {
                return newAutoSeriesEvent;
            }
            set
            {
                newAutoSeriesEvent = value;
            }
        }
        #endregion

        #region GetValue
        /// <summary>
        /// Occurs when getting the property Value.
        /// </summary>
        public event StiGetValueEventHandler GetValue;

        /// <summary>
        /// Raises the GetValue event.
        /// </summary>
        protected virtual void OnGetValue(StiGetValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetValue event.
        /// </summary>
        public virtual void InvokeGetValue(StiComponent sender, StiGetValueEventArgs e)
        {
            try
            {
                OnGetValue(e);
                if (sender.Report.CalculationMode == StiCalculationMode.Interpretation)
                {
                    StiText tempText = new StiText();
                    tempText.Name = "**ChartSeriesValue**";
                    tempText.Page = sender.Report.Pages[0];
                    object parserResult = Engine.StiParser.ParseTextValue(Value.Value, tempText);
                    e.Value = sender.Report.ToString(parserResult);
                }
                if (this.GetValue != null) this.GetValue(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in GetValue property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)this.Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetValueEvent getValueEvent = new StiGetValueEvent();
        /// <summary>
        /// Occurs when getting the property Value.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the property Value.")]
        public StiGetValueEvent GetValueEvent
        {
            get
            {
                return getValueEvent;
            }
            set
            {
                getValueEvent = value;
            }
        }

        #endregion

        #region GetListOfValues
        /// <summary>
        /// Occurs when getting the list of values.
        /// </summary>
        public event StiGetValueEventHandler GetListOfValues;

        /// <summary>
        /// Raises the values event.
        /// </summary>
        protected virtual void OnGetListOfValues(StiGetValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetListOfValues event.
        /// </summary>
        public void InvokeGetListOfValues(StiComponent sender, StiGetValueEventArgs e)
        {
            try
            {
                OnGetListOfValues(e);
                if (sender.Report.CalculationMode == StiCalculationMode.Interpretation)
                {
                    StiText tempText = new StiText();
                    tempText.Name = "**ChartSeriesListOfValues**";
                    tempText.Page = sender.Report.Pages[0];
                    object parserResult = Engine.StiParser.ParseTextValue(listOfValues, tempText);
                    e.Value = sender.Report.ToString(parserResult);
                }
                if (this.GetListOfValues != null) this.GetListOfValues(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in GetListOfValues property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetListOfValuesEvent getListOfValuesEvent = new StiGetListOfValuesEvent();
        /// <summary>
        /// Occurs when getting the list of values.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the list of values.")]
        public StiGetListOfValuesEvent GetListOfValuesEvent
        {
            get
            {
                return getListOfValuesEvent;
            }
            set
            {
                getListOfValuesEvent = value;
            }
        }
        #endregion

        #region GetArgument
        /// <summary>
        /// Occurs when getting the property Argument.
        /// </summary>
        public event StiValueEventHandler GetArgument;

        /// <summary>
        /// Raises the GetArgument event.
        /// </summary>
        protected virtual void OnGetArgument(StiValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetArgument event.
        /// </summary>
        public virtual void InvokeGetArgument(StiComponent sender, StiValueEventArgs e)
        {
            try
            {
                OnGetArgument(e);
                if (sender.Report.CalculationMode == StiCalculationMode.Interpretation)
                {
                    StiText tempText = new StiText();
                    tempText.Name = "**ChartSeriesArgument**";
                    tempText.Page = sender.Report.Pages[0];
                    object parserResult = Engine.StiParser.ParseTextValue(Argument.Value, tempText);
                    e.Value = sender.Report.ToString(parserResult);
                }
                if (this.GetArgument != null) this.GetArgument(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in GetArgument property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetArgumentEvent getArgumentEvent = new StiGetArgumentEvent();
        /// <summary>
        /// Occurs when getting the property Argument.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the property Argument.")]
        public StiGetArgumentEvent GetArgumentEvent
        {
            get
            {
                return getArgumentEvent;
            }
            set
            {
                getArgumentEvent = value;
            }
        }

        #endregion

        #region GetListOfArguments
        /// <summary>
        /// Occurs when getting the list of arguments.
        /// </summary>
        public event StiGetValueEventHandler GetListOfArguments;

        /// <summary>
        /// Raises the Arguments event.
        /// </summary>
        protected virtual void OnGetListOfArguments(StiGetValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetListOfArguments event.
        /// </summary>
        public void InvokeGetListOfArguments(StiComponent sender, StiGetValueEventArgs e)
        {
            try
            {
                OnGetListOfArguments(e);
                if (sender.Report.CalculationMode == StiCalculationMode.Interpretation)
                {
                    StiText tempText = new StiText();
                    tempText.Name = "**ChartSeriesListOfArguments**";
                    tempText.Page = sender.Report.Pages[0];
                    object parserResult = Engine.StiParser.ParseTextValue(listOfArguments, tempText);
                    e.Value = sender.Report.ToString(parserResult);
                }
                if (this.GetListOfArguments != null) this.GetListOfArguments(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in GetListOfArguments property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetListOfArgumentsEvent getListOfArgumentsEvent = new StiGetListOfArgumentsEvent();
        /// <summary>
        /// Occurs when getting the list of arguments.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the list of arguments.")]
        public StiGetListOfArgumentsEvent GetListOfArgumentsEvent
        {
            get
            {
                return getListOfArgumentsEvent;
            }
            set
            {
                getListOfArgumentsEvent = value;
            }
        }
        #endregion

        #region GetTitle
        /// <summary>
        /// Occurs when getting the property Title.
        /// </summary>
        public event StiGetTitleEventHandler GetTitle;

        /// <summary>
        /// Raises the GetTitle event.
        /// </summary>
        protected virtual void OnGetTitle(StiGetTitleEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetTitle event.
        /// </summary>
        public virtual void InvokeGetTitle(StiComponent sender, StiGetTitleEventArgs e)
        {
            try
            {
                OnGetTitle(e);
                if (sender.Report.CalculationMode == StiCalculationMode.Interpretation)
                {
                    StiText tempText = new StiText();
                    tempText.Name = "**ChartSeriesTitle**";
                    tempText.Page = sender.Report.Pages[0];
                    object parserResult = Engine.StiParser.ParseTextValue(Title.Value, tempText);
                    e.Value = sender.Report.ToString(parserResult);
                }
                if (this.GetTitle != null) this.GetTitle(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in Title property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetTitleEvent getTitleEvent = new StiGetTitleEvent();
        /// <summary>
        /// Occurs when getting the property Title.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the property Title.")]
        public StiGetTitleEvent GetTitleEvent
        {
            get
            {
                return getTitleEvent;
            }
            set
            {
                getTitleEvent = value;
            }
        }
        #endregion

        #region GetToolTip
        private static readonly object EventGetToolTip = new object();

        /// <summary>
        /// Occurs when getting the ToolTip for the series.
        /// </summary>
        public event StiValueEventHandler GetToolTip;


        /// <summary>
        /// Raises the GetToolTip event.
        /// </summary>
        /// <param name="e">A parameter which contains event data.</param>
        protected virtual void OnGetToolTip(StiValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetToolTip event.
        /// </summary>
        /// <param name="sender">A sender which invokes an event.</param>
        /// <param name="e">A parameter which contains event data.</param>
        public void InvokeGetToolTip(object sender, StiValueEventArgs e)
        {
            try
            {
                OnGetToolTip(e);

                if (GetToolTip != null) GetToolTip(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in GetToolTip property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);

                if (Chart != null && ((StiChart)Chart).Report != null)
                    ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetToolTipEvent getToolTipEvent = new StiGetToolTipEvent();
        /// <summary>
        /// Occurs when getting the ToolTip for the series.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the ToolTip for the series.")]
        public virtual StiGetToolTipEvent GetToolTipEvent
        {
            get
            {
                return getToolTipEvent;
            }
            set
            {
                getToolTipEvent = value;
            }
        }
        #endregion

        #region GetListOfToolTips
        /// <summary>
        /// Occurs when getting the list of tooltips.
        /// </summary>
        public event StiGetValueEventHandler GetListOfToolTips;

        /// <summary>
        /// Raises the GetListOfToolTips event.
        /// </summary>
        protected virtual void OnGetListOfToolTips(StiGetValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetListOfToolTips event.
        /// </summary>
        public void InvokeGetListOfToolTips(StiComponent sender, StiGetValueEventArgs e)
        {
            try
            {
                OnGetListOfToolTips(e);
                if (this.GetListOfToolTips != null) this.GetListOfToolTips(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in GetListOfToolTips property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetListOfToolTipsEvent getListOfToolTipsEvent = new StiGetListOfToolTipsEvent();
        /// <summary>
        /// Occurs when getting the list of ToolTips.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the list of ToolTips.")]
        public StiGetListOfToolTipsEvent GetListOfToolTipsEvent
        {
            get
            {
                return getListOfToolTipsEvent;
            }
            set
            {
                getListOfToolTipsEvent = value;
            }
        }
        #endregion

        #region GetTag
        private static readonly object EventGetTag = new object();

        /// <summary>
        /// Occurs when getting the Tag for the series.
        /// </summary>
        public event StiValueEventHandler GetTag;


        /// <summary>
        /// Raises the GetTag event.
        /// </summary>
        /// <param name="e">A parameter which contains event data.</param>
        protected virtual void OnGetTag(StiValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetTag event.
        /// </summary>
        /// <param name="sender">A sender which invokes an event.</param>
        /// <param name="e">A parameter which contains event data.</param>
        public void InvokeGetTag(object sender, StiValueEventArgs e)
        {
            try
            {
                OnGetTag(e);

                if (GetTag != null) GetTag(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in GetTag property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);

                if (Chart != null && ((StiChart)Chart).Report != null)
                    ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetTagEvent getTagEvent = new StiGetTagEvent();
        /// <summary>
        /// Occurs when getting the Tag for the series.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the Tag for the series.")]
        public virtual StiGetTagEvent GetTagEvent
        {
            get
            {
                return getTagEvent;
            }
            set
            {
                getTagEvent = value;
            }
        }
        #endregion

        #region GetListOfTags
        /// <summary>
        /// Occurs when getting the list of Tags.
        /// </summary>
        public event StiGetValueEventHandler GetListOfTags;

        /// <summary>
        /// Raises the GetListOfTags event.
        /// </summary>
        protected virtual void OnGetListOfTags(StiGetValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetListOfTags event.
        /// </summary>
        public void InvokeGetListOfTags(StiComponent sender, StiGetValueEventArgs e)
        {
            try
            {
                OnGetListOfTags(e);
                if (this.GetListOfTags != null) this.GetListOfTags(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in GetListOfTags property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetListOfTagsEvent getListOfTagsEvent = new StiGetListOfTagsEvent();
        /// <summary>
        /// Occurs when getting the list of Tags.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the list of Tags.")]
        public StiGetListOfTagsEvent GetListOfTagsEvent
        {
            get
            {
                return getListOfTagsEvent;
            }
            set
            {
                getListOfTagsEvent = value;
            }
        }
        #endregion

        #region GetHyperlink
        private static readonly object EventGetHyperlink = new object();

        /// <summary>
        /// Occurs when getting the Hyperlink for the series.
        /// </summary>
        public event StiValueEventHandler GetHyperlink;


        /// <summary>
        /// Raises the GetHyperlink event.
        /// </summary>
        /// <param name="e">A parameter which contains event data.</param>
        protected virtual void OnGetHyperlink(StiValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetHyperlink event.
        /// </summary>
        /// <param name="sender">A sender which invokes an event.</param>
        /// <param name="e">A parameter which contains event data.</param>
        public void InvokeGetHyperlink(object sender, StiValueEventArgs e)
        {
            try
            {
                OnGetHyperlink(e);

                if (GetHyperlink != null) GetHyperlink(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in GetHyperlink property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);

                if (Chart != null && ((StiChart)Chart).Report != null)
                    ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetHyperlinkEvent getHyperlinkEvent = new StiGetHyperlinkEvent();
        /// <summary>
        /// Occurs when getting the Hyperlink for the series.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the Hyperlink for the series.")]
        public virtual StiGetHyperlinkEvent GetHyperlinkEvent
        {
            get
            {
                return getHyperlinkEvent;
            }
            set
            {
                getHyperlinkEvent = value;
            }
        }
        #endregion

        #region GetListOfHyperlinks
        /// <summary>
        /// Occurs when getting the list of Hyperlinks.
        /// </summary>
        public event StiGetValueEventHandler GetListOfHyperlinks;

        /// <summary>
        /// Raises the Hyperlinks event.
        /// </summary>
        protected virtual void OnGetListOfHyperlinks(StiGetValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetListOfHyperlinks event.
        /// </summary>
        public void InvokeGetListOfHyperlinks(StiComponent sender, StiGetValueEventArgs e)
        {
            try
            {
                OnGetListOfHyperlinks(e);
                if (this.GetListOfHyperlinks != null) this.GetListOfHyperlinks(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in GetListOfHyperlinks property of '{0}' series from '{1}' chart can't be evaluated!", this.ServiceName, ((StiChart)Chart).Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                ((StiChart)Chart).Report.WriteToReportRenderingMessages(str);
            }
        }


        private StiGetListOfHyperlinksEvent getListOfHyperlinksEvent = new StiGetListOfHyperlinksEvent();
        /// <summary>
        /// Occurs when getting the list of Hyperlinks.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when getting the list of Hyperlinks.")]
        public StiGetListOfHyperlinksEvent GetListOfHyperlinksEvent
        {
            get
            {
                return getListOfHyperlinksEvent;
            }
            set
            {
                getListOfHyperlinksEvent = value;
            }
        }
        #endregion
        #endregion

        #region Expressions
        #region Value
        private StiExpression valueObj = new StiExpression();
        /// <summary>
        /// Gets or sets point value expression. Example: {Order.Value}
        /// </summary>
        [StiCategory("Value")]
        [StiOrder(StiSeriesPropertyOrder.ValueValue)]
        [StiSerializable]
        [Description("Gets or sets point value expression. Example: {Order.Value}")]
        public virtual StiExpression Value
        {
            get
            {
                return this.valueObj;
            }
            set
            {
                this.valueObj = value;
            }
        }
        #endregion

        #region ListOfValues
        private StiListOfValuesExpression listOfValues = new StiListOfValuesExpression();
        /// <summary>
        /// Gets or sets the expression to fill a list of values.  Example: 1;2;3
        /// </summary>
        [StiCategory("Value")]
        [StiOrder(StiSeriesPropertyOrder.ValueListOfValues)]
        [StiSerializable(
             StiSerializeTypes.SerializeToCode |
             StiSerializeTypes.SerializeToDesigner |
             StiSerializeTypes.SerializeToSaveLoad)]
        [Description("Gets or sets the expression to fill a list of values. Example: 1;2;3")]
        public virtual StiListOfValuesExpression ListOfValues
        {
            get
            {
                return listOfValues;
            }
            set
            {
                listOfValues = value;
            }
        }
        #endregion

        #region Argument
        private StiArgumentExpression argument = new StiArgumentExpression();
        [StiCategory("Argument")]
        [StiSerializable]
        [StiOrder(StiSeriesPropertyOrder.ArgumentArgument)]
        [Description("Gets or sets argument expression. Example: {Order.Argument}")]
        public virtual StiArgumentExpression Argument
        {
            get
            {
                return argument;
            }
            set
            {
                argument = value;
            }
        }
        #endregion

        #region ListOfArguments
        private StiListOfArgumentsExpression listOfArguments = new StiListOfArgumentsExpression();
        /// <summary>
        /// Gets or sets the expression to fill a list of arguments.  Example: 1;2;3
        /// </summary>
        [StiCategory("Argument")]
        [StiOrder(StiSeriesPropertyOrder.ArgumentListOfArguments)]
        [StiSerializable(
             StiSerializeTypes.SerializeToCode |
             StiSerializeTypes.SerializeToDesigner |
             StiSerializeTypes.SerializeToSaveLoad)]
        [Description("Gets or sets the expression to fill a list of arguments.  Example: 1;2;3")]
        public virtual StiListOfArgumentsExpression ListOfArguments
        {
            get
            {
                return listOfArguments;
            }
            set
            {
                listOfArguments = value;
            }
        }
        #endregion

        #region Title
        private string titleValue = null;
        /// <summary>
        /// Gets or sets title of series.
        /// </summary>
        [StiSerializable(StiSerializeTypes.SerializeToDocument)]
        [Browsable(false)]
        public virtual string TitleValue
        {
            get
            {
                return this.titleValue;
            }
            set
            {
                this.titleValue = value;
            }
        }


        private StiTitleExpression title = new StiTitleExpression();
        /// <summary>
        /// Gets or sets title of series.
        /// </summary>
        [StiCategory("Common")]
        [StiSerializable(
             StiSerializeTypes.SerializeToCode |
             StiSerializeTypes.SerializeToDesigner |
             StiSerializeTypes.SerializeToSaveLoad)]
        [Description("Gets or sets title of series.")]
        public virtual StiTitleExpression Title
        {
            get
            {
                return this.title;
            }
            set
            {
                this.title = value;
            }
        }
        #endregion

        #region ToolTip
        private StiToolTipExpression toolTip = new StiToolTipExpression();
        /// <summary>
        /// Gets or sets the expression to fill a series tooltip.
        /// </summary>
        [StiCategory("Navigation")]
        [Browsable(false)]
        [StiOrder(StiPropertyOrder.NavigationToolTip)]
        [StiSerializable(
             StiSerializeTypes.SerializeToCode |
             StiSerializeTypes.SerializeToDesigner |
             StiSerializeTypes.SerializeToSaveLoad)]
        [Description("Gets or sets the expression to fill a series tooltip.")]
        public virtual StiToolTipExpression ToolTip
        {
            get
            {
                return toolTip;
            }
            set
            {
                toolTip = value;
            }
        }
        #endregion

        #region ListOfToolTips
        private StiListOfToolTipsExpression listOfToolTips = new StiListOfToolTipsExpression();
        /// <summary>
        /// Gets or sets the expression to fill a list of tool tips.  Example: 1;2;3
        /// </summary>
        [StiCategory("Value")]
        [Browsable(false)]
        [StiOrder(StiSeriesPropertyOrder.ListOfToolTips)]
        [StiSerializable(
             StiSerializeTypes.SerializeToCode |
             StiSerializeTypes.SerializeToDesigner |
             StiSerializeTypes.SerializeToSaveLoad)]
        [Description("Gets or sets the expression to fill a list of tool tips. Example: 1;2;3")]
        public virtual StiListOfToolTipsExpression ListOfToolTips
        {
            get
            {
                return listOfToolTips;
            }
            set
            {
                listOfToolTips = value;
            }
        }
        #endregion

        #region Tag
        private StiTagExpression tag = new StiTagExpression();
        /// <summary>
        /// Gets or sets the expression to fill a series tag.
        /// </summary>
        [StiCategory("Navigation")]
        [Browsable(false)]
        [StiOrder(StiSeriesPropertyOrder.Tag)]
        [StiSerializable(
             StiSerializeTypes.SerializeToCode |
             StiSerializeTypes.SerializeToDesigner |
             StiSerializeTypes.SerializeToSaveLoad)]
        [Description("Gets or sets the expression to fill a series tag.")]
        public virtual StiTagExpression Tag
        {
            get
            {
                return tag;
            }
            set
            {
                tag = value;
            }
        }
        #endregion

        #region ListOfTags
        private StiListOfTagsExpression listOfTags = new StiListOfTagsExpression();
        /// <summary>
        /// Gets or sets the expression to fill a list of tags.  Example: 1;2;3
        /// </summary>
        [StiCategory("Value")]
        [Browsable(false)]
        [StiOrder(StiSeriesPropertyOrder.ListOfTags)]
        [StiSerializable(
             StiSerializeTypes.SerializeToCode |
             StiSerializeTypes.SerializeToDesigner |
             StiSerializeTypes.SerializeToSaveLoad)]
        [Description("Gets or sets the expression to fill a list of tags. Example: 1;2;3")]
        public virtual StiListOfTagsExpression ListOfTags
        {
            get
            {
                return listOfTags;
            }
            set
            {
                listOfTags = value;
            }
        }
        #endregion

        #region Hyperlink
        private StiHyperlinkExpression hyperlink = new StiHyperlinkExpression();
        /// <summary>
        /// Gets or sets the expression to fill a series hyperlink.
        /// </summary>
        [StiCategory("Navigation")]
        [Browsable(false)]
        [StiOrder(StiSeriesPropertyOrder.Hyperlink)]
        [StiSerializable(
             StiSerializeTypes.SerializeToCode |
             StiSerializeTypes.SerializeToDesigner |
             StiSerializeTypes.SerializeToSaveLoad)]
        [Description("Gets or sets the expression to fill a series hyperlink.")]
        public virtual StiHyperlinkExpression Hyperlink
        {
            get
            {
                return hyperlink;
            }
            set
            {
                hyperlink = value;
            }
        }
        #endregion

        #region ListOfHyperlinks
        private StiListOfHyperlinksExpression listOfHyperlinks = new StiListOfHyperlinksExpression();
        /// <summary>
        /// Gets or sets the expression to fill a list of hyperlinks.  Example: 1;2;3
        /// </summary>
        [Browsable(false)]
        [StiCategory("Value")]
        [StiOrder(StiSeriesPropertyOrder.ListOfHyperlinks)]
        [StiSerializable(
             StiSerializeTypes.SerializeToCode |
             StiSerializeTypes.SerializeToDesigner |
             StiSerializeTypes.SerializeToSaveLoad)]
        [Description("Gets or sets the expression to fill a list of hyperlinks. Example: 1;2;3")]
        public virtual StiListOfHyperlinksExpression ListOfHyperlinks
        {
            get
            {
                return listOfHyperlinks;
            }
            set
            {
                listOfHyperlinks = value;
            }
        }
        #endregion
        #endregion

        public StiSeries()
        {
            this.SeriesLabels = new StiCenterAxisLabels();
            this.TrendLine = new StiTrendLineNone();
            this.Interaction = new StiSeriesInteraction();
        }
    }
}