#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Design;

namespace Stimulsoft.Report.Chart
{
    public class StiSeriesInteractionData
    {
        #region Properties
        private bool isElements = true;
        public bool IsElements
        {
            get
            {
                return isElements;
            }
            set
            {
                isElements = value;
            }
        }

        private object tag = null;
        public object Tag
        {
            get
            {
                return tag;
            }
        }

        private string tooltip = null;
        public string Tooltip
        {
            get
            {
                return tooltip;
            }
        }

        private string hyperlink = null;
        public string Hyperlink
        {
            get
            {
                return hyperlink;
            }
        }

        private object argument = null;
        public object Argument
        {
            get
            {
                return argument;
            }
        }

        private double? value = 0d;
        public double? Value
        {
            get
            {
                return value;
            }
        }

        private IStiSeries series;
        public IStiSeries Series
        {
            get
            {
                return series;
            }
            set
            {
                series = value;
            }
        }

        private int pointIndex;
        public int PointIndex
        {
            get
            {
                return pointIndex;
            }
        }

        private PointF? point;
        public PointF? Point
        {
            get
            {
                return point;
            }
            set
            {
                point = value;
            }
        }
        #endregion

        #region Methods
        public void Fill(IStiArea area, IStiSeries series, int pointIndex)
        {
            if (area is IStiAxisArea && ((IStiAxisArea)area).ReverseHor)
            {
                this.pointIndex = series.Values.Length - pointIndex - 1;
                this.series = series;

                if (pointIndex >= 0 && pointIndex < series.Arguments.Length)
                    argument = series.Arguments[series.Arguments.Length - pointIndex - 1];

                if (pointIndex >= 0 && pointIndex < series.Values.Length)
                    value = series.Values[series.Values.Length - pointIndex - 1];

                if (pointIndex >= 0 && pointIndex < series.Tags.Length)
                    tag = series.Tags[series.Tags.Length - pointIndex - 1];

                if (pointIndex >= 0 && pointIndex < series.Hyperlinks.Length)
                    hyperlink = series.Hyperlinks[series.Hyperlinks.Length - pointIndex - 1];

                if (pointIndex >= 0 && pointIndex < series.ToolTips.Length)
                    tooltip = series.ToolTips[series.ToolTips.Length - pointIndex - 1];
            }
            else
            {
                this.pointIndex = pointIndex;
                this.series = series;

                if (pointIndex >= 0 && pointIndex < series.Arguments.Length)
                    argument = series.Arguments[pointIndex];

                if (pointIndex >= 0 && pointIndex < series.Values.Length)
                    value = series.Values[pointIndex];

                if (pointIndex >= 0 && pointIndex < series.Tags.Length)
                    tag = series.Tags[pointIndex];

                if (pointIndex >= 0 && pointIndex < series.Hyperlinks.Length)
                    hyperlink = series.Hyperlinks[pointIndex];

                if (pointIndex >= 0 && pointIndex < series.ToolTips.Length)
                    tooltip = series.ToolTips[pointIndex];
            }
        }
        #endregion
    }
}
