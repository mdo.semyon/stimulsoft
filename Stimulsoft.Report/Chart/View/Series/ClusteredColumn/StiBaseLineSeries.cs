#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing.Design;
using System.ComponentModel;
using System.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
    public abstract class StiBaseLineSeries : 
        StiSeries,
        IStiBaseLineSeries
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // Old
            jObject.RemoveProperty("Conditions");

            jObject.AddPropertyBool("ShowNulls", ShowNulls, true);
            jObject.AddPropertyJObject("Marker", Marker.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("LineMarker", LineMarker.SaveToJsonObject(mode));
            jObject.AddPropertyStringNullOrEmpty("LineColor", StiJsonReportObjectHelper.Serialize.JColor(LineColor, Color.Black));
            jObject.AddPropertyEnum("LineStyle", LineStyle, StiPenStyle.Solid);
            jObject.AddPropertyBool("Lighting", Lighting, true);
            jObject.AddPropertyFloat("LineWidth", LineWidth, 2f);
            jObject.AddPropertyInt("LabelsOffset", labelsOffset);
            jObject.AddPropertyStringNullOrEmpty("LineColorNegative", StiJsonReportObjectHelper.Serialize.JColor(LineColorNegative, Color.Firebrick));
            jObject.AddPropertyBool("AllowApplyColorNegative", AllowApplyColorNegative);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "ShowNulls":
                        this.ShowNulls = property.Value.ToObject<bool>();
                        break;

                    case "Marker":
                        this.Marker.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "LineMarker":
                        this.LineMarker.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "LineColor":
                        this.LineColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;
                        
                    case "LineStyle":
                        this.LineStyle = (StiPenStyle)Enum.Parse(typeof(StiPenStyle), property.Value.ToObject<string>());
                        break;

                    case "Lighting":
                        this.Lighting = property.Value.ToObject<bool>();
                        break;
                        
                    case "LineWidth":
                        this.LineWidth = property.Value.ToObject<float>();
                        break;
                        
                    case "LabelsOffset":
                        this.labelsOffset = property.Value.ToObject<int>();
                        break;
                        
                    case "LineColorNegative":
                        this.LineColorNegative = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;
                        
                    case "AllowApplyColorNegative":
                        this.AllowApplyColorNegative = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public override object Clone()
		{
			IStiBaseLineSeries series =	base.Clone() as IStiBaseLineSeries;
            
            series.Marker = this.Marker.Clone() as IStiMarker;

            series.LineStyle = this.LineStyle;            

			return series;
		}
		#endregion

        #region Properties
        private bool showNulls = true;
        /// <summary>
        /// Gets or sets value which indicates whether it is necessary to show the series element, if the series value of this bar is null.
        /// </summary>
        [StiSerializable]
        [StiCategory("Common")]
        [DefaultValue(true)]
        [Description("Gets or sets value which indicates whether it is necessary to show the series element, if the series value of this bar is null.")]
        [TypeConverter(typeof(StiBoolConverter))]
        public virtual bool ShowNulls
        {
            get
            {
                return showNulls;
            }
            set
            {
                showNulls = value;
            }
        }


        [Obsolete("Please use Marker.Visible property instead ShowMarker property.")]
        [Browsable(false)]
        [StiNonSerialized]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        public bool ShowMarker
        {
            get
            {
                return this.Marker.Visible;
            }
            set
            {
                this.Marker.Visible = value;
            }
        }


        [Obsolete("Please use Marker.Brush property instead MarkerColor property.")]
        [Browsable(false)]
        [StiNonSerialized]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        public Color MarkerColor
        {
            get
            {
                return StiBrush.ToColor(this.Marker.Brush);
            }
            set
            {
                this.Marker.Brush = new StiSolidBrush(value);
                this.Marker.BorderColor = StiColorUtils.Dark(value, 50);
            }
        }


        [Obsolete("Please use Marker.Size property instead MarkerSize property.")]
        [Browsable(false)]
        [StiNonSerialized]
        [DefaultValue(6f)]
        public float MarkerSize
        {
            get
            {
                return this.Marker.Size;
            }
            set
            {
                this.Marker.Size = value;
            }
        }


        [Obsolete("Please use Marker.Type property instead MarkerType property.")]
        [Browsable(false)]
        [StiNonSerialized]
        [DefaultValue(StiMarkerType.Circle)]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        public StiMarkerType MarkerType
        {
            get
            {
                return ((StiMarker)this.Marker).Type;
            }
            set
            {
                ((StiMarker)this.Marker).Type = value;
            }
        }


        private IStiMarker marker = new StiMarker();
        /// <summary>
        /// Gets or sets marker settings.
        /// </summary>
        [StiSerializable(StiSerializationVisibility.Class)]
        [StiCategory("Appearance")]
        [Description("Gets or sets marker settings.")]
        [Browsable(false)]
        [TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiMarkerConverter))]
        public virtual IStiMarker Marker
        {
            get
            {
                return marker;
            }
            set
            {
                marker = value;
            }
        }


        private IStiLineMarker lineMarker = new StiLineMarker();
        /// <summary>
        /// Gets or sets line marker settings.
        /// </summary>
        [StiSerializable(StiSerializationVisibility.Class)]
        [StiCategory("Appearance")]
        [Description("Gets or sets line marker settings.")]
        [Browsable(false)]
        [TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiLineMarkerConverter))]
        public virtual IStiLineMarker LineMarker
        {
            get
            {
                return lineMarker;
            }
            set
            {
                lineMarker = value;
            }
        }       

        
        private Color lineColor = Color.Black;
        /// <summary>
        /// Gets or sets line color of series.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Appearance")]
        [Description("Gets or sets line color of series.")]
        public virtual Color LineColor
        {
            get
            {
                return lineColor;
            }
            set
            {
                lineColor = value;
            }
        }

                
        private StiPenStyle lineStyle = StiPenStyle.Solid;
		/// <summary>
		/// Gets or sets a line style of series.
		/// </summary>
		[Editor("Stimulsoft.Base.Drawing.Design.StiPenStyleEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[DefaultValue(StiPenStyle.Solid)]
        [Description("Gets or sets a line style of series.")]
		[StiSerializable]
		[StiCategory("Common")]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		public virtual StiPenStyle LineStyle
		{
			get
			{
				return lineStyle;
			}
			set
			{
				lineStyle = value;
			}
		}


		private bool lighting = true;
        /// <summary>
        /// Gets or sets value which indicates that light effect will be shown.
        /// </summary>
		[DefaultValue(true)]
		[StiSerializable]
		[StiCategory("Appearance")]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that light effect will be shown.")]
		public virtual bool Lighting
		{
			get
			{
				return lighting;
			}
			set
			{
				lighting = value;
			}
		} 
		

		private float lineWidth = 2f;
        /// <summary>
        /// Gets or sets line width of series.
        /// </summary>
		[DefaultValue(2f)]
		[StiSerializable]
		[StiCategory("Common")]
        [Description("Gets or sets line width of series.")]
		public virtual float LineWidth
		{
			get
			{
				return lineWidth;
			}
			set
			{
				if (value > 0)
				{
					lineWidth = value;
				}
			}
		}


        private int labelsOffset = 0;
        /// <summary>
        /// Gets or sets vertical labels offset.
        /// </summary>
        [DefaultValue(0)]
        [StiSerializable]
        [StiCategory("Common")]
        [Description("Gets or sets vertical labels offset.")]
        public int LabelsOffset
        {
            get
            {
                return labelsOffset;
            }                                   
            set
            {
                labelsOffset = value;
            }
        }

        private Color lineColorNegative = Color.Firebrick;
        /// <summary>
        /// Gets or sets a line color of series for negative values.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Appearance")]
        [Description("Gets or sets a line color of series for negative values.")]
        public virtual Color LineColorNegative
        {
            get
            {
                return lineColorNegative;
            }
            set
            {
                lineColorNegative = value;
            }
        }

        private bool allowApplyColorNegative = false;
        /// <summary>
        /// Gets or sets a value which indicates that the specific color for negative values will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Common")]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates that the specific color for negative values will be used.")]
        public virtual bool AllowApplyColorNegative
        {
            get
            {
                return allowApplyColorNegative;
            }
            set
            {
                allowApplyColorNegative = value;
            }
        }
        #endregion

        public StiBaseLineSeries()
        {
            this.SeriesLabels = new StiOutsideEndAxisLabels();
        }
    }
}
