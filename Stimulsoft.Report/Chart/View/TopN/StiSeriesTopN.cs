﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using System;

namespace Stimulsoft.Report.Chart
{
    [TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiSeriesTopNConverter))]
    public class StiSeriesTopN :
        IStiSeriesTopN,
        IStiSerializeToCodeAsClass,
        IStiPropertyGridObject
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyEnum("Mode", this.mode, StiTopNMode.None);
            jObject.AddPropertyInt("Count", count, 5);
            jObject.AddPropertyBool("ShowOthers", showOthers, true);
            jObject.AddPropertyString("OthersText", othersText, "Others");

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Mode":
                        this.mode = (StiTopNMode)Enum.Parse(typeof(StiTopNMode), property.Value.ToObject<string>());
                        break;

                    case "Count":
                        this.count = property.Value.ToObject<int>();
                        break;

                    case "ShowOthers":
                        this.showOthers = property.Value.ToObject<bool>();
                        break;

                    case "OthersText":
                        this.othersText = property.Value.ToObject<string>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiSeriesTopN;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.ChartTopN()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            var topN = this.MemberwiseClone() as IStiSeriesTopN;
            return topN;
        }
        #endregion

        #region Properties
        private StiTopNMode mode = StiTopNMode.None;
        /// <summary>
        /// Gets or sets output values mode.
        /// </summary>
        [DefaultValue(StiTopNMode.None)]
        [StiSerializable]
        [StiCategory("Common")]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets output values mode.")]
        public StiTopNMode Mode
        {
            get
            {
                return mode;
            }
            set
            {
                mode = value;
            }
        }

        private int count = 5;
        /// <summary>
        /// Gets or sets the number of output values.
        /// </summary>
        [DefaultValue(5)]
        [StiSerializable]
        [StiCategory("Common")]
        [Description("Gets or sets the number of output values.")]
        public int Count
        {
            get
            {
                return count;
            }
            set
            {
                count = value;
            }
        }

        private bool showOthers = true;
        /// <summary>
        /// Gets or sets value which indicates whether to display other values.
        /// </summary>
        [DefaultValue(true)]
        [StiSerializable]
        [StiCategory("Common")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates whether to display other values.")]
        public bool ShowOthers
        {
            get
            {
                return showOthers;
            }
            set
            {
                showOthers = value;
            }
        }

        private string othersText = "Others";
        /// <summary>
        /// Gets or sets signature for other values.
        /// </summary>
        [StiSerializable]
        [StiCategory("Common")]
        [DefaultValue("Others")]
        [Description("Gets or sets signature for other values.")]
        public string OthersText
        {
            get
            {
                return othersText;
            }
            set
            {
                othersText = value;
            }
        }
        #endregion
    }
}
