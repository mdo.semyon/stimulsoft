#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;

namespace Stimulsoft.Report.Chart
{
	internal class StiDataItem
	{
		internal object Argument;
		internal object Value;
	    internal object ValueEnd;

        internal object Weight;

	    internal object ValueOpen;
        internal object ValueClose;
        internal object ValueLow;
        internal object ValueHigh;

		internal object Title;
		internal object Key;
		internal object Color;

        internal object ToolTip;

        internal object Tag;

        public StiDataItem(object argument, object value, object valueEnd, object weight,
            object valueOpen, object valueClose, object valueLow, object valueHight,
            object title, object key, object color, object toolTip, object tag)
		{
			this.Argument = argument;
			this.Value = value;
		    this.ValueEnd = valueEnd;

            this.Weight = weight;

		    this.ValueOpen = valueOpen;
		    this.ValueClose = valueClose;
		    this.ValueLow = valueLow;
		    this.ValueHigh = valueHight;

			this.Title = title;
			this.Key = key;
			this.Color = color;

            this.ToolTip = toolTip;

            this.Tag = tag;
		}
	}

    internal class StiDataItemComparer : IComparer<StiDataItem>
	{
		#region IComparer
        public int Compare(StiDataItem x, StiDataItem y)
		{
			IComparable value1 = null;
			IComparable value2 = null;

			if (sortType == StiSeriesSortType.Value)
			{
				value1 = x.Value as IComparable;
				value2 = y.Value as IComparable;				
			}
			else 
			{
				value1 = x.Argument as IComparable;
				value2 = y.Argument as IComparable;
			}

			if (value1 == null && value2 == null)return 0;
			if (value1 == null)return 1;
			if (value2 == null)return -1;

			return value1.CompareTo(value2) * directionFactor;
		}
		#endregion

		private int directionFactor = 1;
		private StiSeriesSortType sortType = StiSeriesSortType.None;

		internal StiDataItemComparer(StiSeriesSortType sortType, StiSeriesSortDirection sortDirection)
		{
			directionFactor = 1;
			if (sortDirection == StiSeriesSortDirection.Descending)directionFactor = -1;

			this.sortType = sortType;
		}
    }
}
