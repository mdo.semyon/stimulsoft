﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Design;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
    [TypeConverter(typeof(StiUniversalConverter))]
    public class StiChartTableDataCells : IStiChartTableDataCells
    {        
        public StiChartTableDataCells()
        {
        }

        [StiUniversalConstructor("DataCells")]
        public StiChartTableDataCells(
            bool shrinkFontToFit,
            float shrinkFontToFitMinimumSize,
            Font font,
            Color textColor)
        {
            this.font = font;
            this.shrinkFontToFit = shrinkFontToFit;
            this.shrinkFontToFitMinimumSize = shrinkFontToFitMinimumSize;
            this.textColor = textColor;

        }

        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyStringNullOrEmpty("TextColor", StiJsonReportObjectHelper.Serialize.JColor(textColor, Color.DarkGray));
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.FontDefault(Font));
            jObject.AddPropertyFloat("ShrinkFontToFitMinimumSize", ShrinkFontToFitMinimumSize, 1);
            jObject.AddPropertyBool("ShrinkFontToFit", ShrinkFontToFit);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Font":
                        this.Font = StiJsonReportObjectHelper.Deserialize.Font(property, Font);
                        break;

                    case "TextColor":
                        this.textColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "ShrinkFontToFitMinimumSize":
                        this.ShrinkFontToFitMinimumSize = property.Value.ToObject<float>();
                        break;

                    case "ShrinkFontToFit":
                        this.ShrinkFontToFit = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            var dataCells = this.MemberwiseClone() as IStiChartTableDataCells;            
            dataCells.Font = this.Font.Clone() as Font;

            return dataCells;
        }
        #endregion

        #region Properties
        private bool shrinkFontToFit = false;
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiPropertyLevel(StiLevel.Professional)]
        public bool ShrinkFontToFit
        {
            get
            {
                return shrinkFontToFit;
            }
            set
            {
                shrinkFontToFit = value;
            }
        }

        private float shrinkFontToFitMinimumSize = 1;
        [StiSerializable]
        [DefaultValue(1f)]
        [Description("Gets or sets value that indicates minimum font size for ShrinkFontToFit operation.")]
        [StiPropertyLevel(StiLevel.Professional)]
        public float ShrinkFontToFitMinimumSize
        {
            get
            {
                return shrinkFontToFitMinimumSize;
            }
            set
            {
                shrinkFontToFitMinimumSize = value;
            }
        }

        private Font font = new Font("Arial", 8);
        /// <summary>
        /// Gets or sets font which will be used to draw chart table header.
        /// </summary>
        [StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.Font)]
        [Description("Gets or sets font which will be used to draw chart table cells.")]
        public Font Font
        {
            get
            {
                return font;
            }
            set
            {
                font = value;
            }
        }

        private Color textColor = Color.DarkGray;
        /// <summary>
        /// Gets or sets text color.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets text color.")]
        [StiCategory("Appearance")]
        public Color TextColor
        {
            get
            {
                return textColor;
            }
            set
            {
                textColor = value;
            }
        }
        #endregion
    }
}
