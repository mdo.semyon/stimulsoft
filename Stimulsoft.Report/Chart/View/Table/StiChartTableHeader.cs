﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Design;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
    [TypeConverter(typeof(StiUniversalConverter))]
    public class StiChartTableHeader : IStiChartTableHeader
    {
        public StiChartTableHeader()
        {
        }
        
        public StiChartTableHeader(
            StiBrush brush,
            Font font,
            Color textColor,
            bool wordWrap) : this(
                string.Empty, brush, font, textColor, wordWrap)
        {            
        }

        [StiUniversalConstructor("Header")]
        public StiChartTableHeader(
            string textAfter,
            StiBrush brush,
            Font font,
            Color textColor,
            bool wordWrap)
        {
            this.textAfter = textAfter;
            this.brush = brush;
            this.font = font;
            this.textColor = textColor;
            this.wordWrap = wordWrap;
        }

        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyStringNullOrEmpty("TextAfter", TextAfter);
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.FontDefault(Font));
            jObject.AddPropertyStringNullOrEmpty("TextColor", StiJsonReportObjectHelper.Serialize.JColor(textColor, Color.DarkGray));
            jObject.AddPropertyBool("WordWrap", WordWrap);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "TextAfter":
                        this.textAfter = property.Value.ToObject<string>();
                        break;

                    case "Brush":
                        this.Brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "Font":
                        this.Font = StiJsonReportObjectHelper.Deserialize.Font(property, Font);
                        break;

                    case "TextColor":
                        this.textColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "WordWrap":
                        this.WordWrap = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            IStiChartTableHeader header = this.MemberwiseClone() as IStiChartTableHeader;
            header.Brush = this.Brush.Clone() as StiBrush;
            header.Font = this.Font.Clone() as Font;

            return header;
        }
        #endregion

        #region Properties
        private string textAfter = "";
        /// <summary>
        /// Gets or sets string which will be output after text.
        /// </summary>
		[DefaultValue("")]
        [StiSerializable]
        [Description("Gets or sets string which will be output after text.")]
        public string TextAfter
        {
            get
            {
                return textAfter;
            }
            set
            {
                textAfter = value;
            }
        }

        private StiBrush brush = new StiSolidBrush(Color.White);
        /// <summary>
        /// Gets or sets brush which will be used to fill chart table header.
        /// </summary>
        [StiOrder(StiSeriesLabelsPropertyOrder.Brush)]
        [StiSerializable]
        [Description("Gets or sets brush which will be used to fill chart table header.")]
        public StiBrush Brush
        {
            get
            {
                return brush;
            }
            set
            {
                brush = value;
            }
        }

        private Font font = new Font("Arial", 8);
        /// <summary>
        /// Gets or sets font which will be used to draw chart table header.
        /// </summary>
        [StiSerializable]
        [StiOrder(StiSeriesLabelsPropertyOrder.Font)]
        [Description("Gets or sets font which will be used to draw chart table header.")]
        public Font Font
        {
            get
            {
                return font;
            }
            set
            {
                font = value;
            }
        }

        private Color textColor = Color.DarkGray;
        /// <summary>
        /// Gets or sets text color.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets text color.")]
        public Color TextColor
        {
            get
            {
                return textColor;
            }
            set
            {
                textColor = value;
            }
        }

        /// <summary>
        /// Gets or sets word wrap.
        /// </summary>
        private bool wordWrap = false;
        [StiSerializable]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets word wrap.")]
        public bool WordWrap
        {
            get
            {
                return wordWrap;
            }
            set
            {
                wordWrap = value;
            }
        }
        #endregion
    }
}
