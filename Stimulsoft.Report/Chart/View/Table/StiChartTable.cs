#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using System.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using System;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
    [TypeConverter(typeof(StiUniversalConverter))]
    public class StiChartTable : 
        IStiChartTable, 
        IStiSerializeToCodeAsClass,
        IStiPropertyGridObject
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();
            
            jObject.AddPropertyBool("Visible", visible);
            jObject.AddPropertyBool("AllowApplyStyle", allowApplyStyle, true);
            jObject.AddPropertyBool("MarkerVisible", markerVisible, true);
            jObject.AddPropertyStringNullOrEmpty("GridLineColor", StiJsonReportObjectHelper.Serialize.JColor(gridLineColor, Color.Gray));            
            jObject.AddPropertyBool("GridLinesHor", gridLinesHor, true);
            jObject.AddPropertyBool("GridLinesVert", gridLinesVert, true);
            jObject.AddPropertyBool("GridOutline", gridOutline, true);
            jObject.AddPropertyStringNullOrEmpty("Format", format);
            jObject.AddPropertyJObject("Header", header.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("DataCells", dataCells.SaveToJsonObject(mode));

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Font":
                        this.dataCells.Font = StiJsonReportObjectHelper.Deserialize.Font(property, dataCells.Font);
                        break;

                    case "Visible":
                        this.visible = property.Value.ToObject<bool>();
                        break;

                    case "AllowApplyStyle":
                        this.allowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "MarkerVisible":
                        this.markerVisible = property.Value.ToObject<bool>();
                        break;

                    case "GridLineColor":
                        this.gridLineColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "TextColor":
                        this.DataCells.TextColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "GridLinesHor":
                        this.gridLinesHor = property.Value.ToObject<bool>();
                        break;

                    case "GridLinesVert":
                        this.gridLinesVert = property.Value.ToObject<bool>();
                        break;

                    case "GridOutline":
                        this.gridOutline = property.Value.ToObject<bool>();
                        break;

                    case "Format":
                        this.format = property.Value.ToObject<string>();
                        break;

                    case "Header":
                        this.header.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "DataCells":
                        this.dataCells.LoadFromJsonObject((JObject)property.Value);
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiChartTable;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.ChartTable()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            IStiChartTable table = this.MemberwiseClone() as IStiChartTable;
            
            table.Header = this.header.Clone() as IStiChartTableHeader;
            table.DataCells = this.dataCells.Clone() as IStiChartTableDataCells;
            
            if (this.core != null)
            {
                table.Core = this.core.Clone() as StiChartTableCore;
                table.Core.ChartTable = table;
            }

            return table;
        }
        #endregion
                
        #region Properties
        [Browsable(false)]
        [StiNonSerialized]
        [Obsolete("Font property is obsolete. Please use DataCells.Font property.")]
        public Font Font
        {
            get
            {
                return dataCells.Font;
            }
            set
            {
                dataCells.Font = value;
            }
        }

        private bool visible = false;
        /// <summary>
        /// Gets or sets visibility of table.
        /// </summary>
        [StiSerializable]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of table.")]
        [StiCategory("Common")]
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }

        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                    //if (value && Chart != null)
                    //    this.Core.ApplyStyle(this.Chart.Style);
                }
            }
        }

        private bool markerVisible = true;
        /// <summary>
        /// Gets or sets visibility of markers.
        /// </summary>
        [StiSerializable]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of markers.")]
        [StiCategory("Common")]
        public bool MarkerVisible
        {
            get
            {
                return markerVisible;
            }
            set
            {
                markerVisible = value;
            }
        }

        private Color gridLineColor = Color.Gray;
        /// <summary>
        /// Gets or sets grid lines color.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets grid lines color.")]
        [StiCategory("Appearance")]
        public Color GridLineColor
        {
            get
            {
                return gridLineColor;
            }
            set
            {
                gridLineColor = value;
            }
        }

        private Color textColor = Color.DarkGray;
        [Browsable(false)]
        [StiNonSerialized]
        [Obsolete("TextColor property is obsolete. Please use DataCells.TextColor property.")]
        public Color TextColor
        {
            get
            {
                return dataCells.TextColor;
            }
            set
            {
                dataCells.TextColor = value;
            }
        }

        private bool gridLinesHor = true;
        /// <summary>
        /// Gets or sets visibility of grid lines horizontal.
        /// </summary>
        [StiSerializable]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of grid lines horizontal.")]
        [StiCategory("Common")]
        public bool GridLinesHor
        {
            get
            {
                return gridLinesHor;
            }
            set
            {
                gridLinesHor = value;
            }
        }

        private bool gridLinesVert = true;
        /// <summary>
        /// Gets or sets visibility of grid lines vertical.
        /// </summary>
        [StiSerializable]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of grid lines vertical.")]
        [StiCategory("Common")]
        public bool GridLinesVert
        {
            get
            {
                return gridLinesVert;
            }
            set
            {
                gridLinesVert = value;
            }
        }

        private bool gridOutline = true;
        /// <summary>
        /// Gets or sets visibility of grid outline.
        /// </summary>
        [StiSerializable]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of grid outline.")]
        [StiCategory("Common")]
        public bool GridOutline
        {
            get
            {
                return gridOutline;
            }
            set
            {
                gridOutline = value;
            }
        }

        private string format = "";
        /// <summary>
        /// Gets or sets format string which used for formating of the chart table.
        /// </summary>
        [DefaultValue("")]
        [Description("Gets or sets format string which used for formating of the chart table.")]
        [StiSerializable]
        [Editor("Stimulsoft.Report.Chart.Design.StiFormatEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Common")]
        public string Format
        {
            get
            {
                return format;
            }
            set
            {
                format = value;
            }
        }
        
        private IStiChartTableHeader header;
        /// <summary>
        /// Gets or sets header settings.
        /// </summary>
        [Description("Gets or sets header settings.")]
        [StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(StiUniversalConverter))]
        [StiCategory("Common")]
        public IStiChartTableHeader Header
        {
            get
            {
                return header;
            }
            set
            {
                header = value;
            }
        }

        private IStiChartTableDataCells dataCells;
        /// <summary>
        /// Gets or sets Data Cells settings.
        /// </summary>
        [Description("Gets or sets data cells settings.")]
        [StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(StiUniversalConverter))]
        [StiCategory("Common")]
        public IStiChartTableDataCells DataCells
        {
            get
            {
                return dataCells;
            }
            set
            {
                dataCells = value;
            }
        }

        private StiChartTableCore core;
        [Browsable(false)]
        public StiChartTableCore Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        private IStiChart chart = null;
        [Browsable(false)]
        public IStiChart Chart
        {
            get
            {
                return chart;
            }
            set
            {
                chart = value;
            }
        }
        #endregion

        [StiUniversalConstructor("Table")]
        public StiChartTable()
        {
            Header = new StiChartTableHeader();
            DataCells = new StiChartTableDataCells();

            this.core = new StiChartTableCore(this);
        }
    }
}
