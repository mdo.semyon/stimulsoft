#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing.Design;
using System.ComponentModel;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using System;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
    [TypeConverter(typeof(Design.StiMarkerConverter))]
	public class StiMarker: 
        IStiMarker,
		IStiSerializeToCodeAsClass,
        IStiPropertyGridObject
    {
        #region IStiJsonReportObject.override
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyBool("ShowInLegend", ShowInLegend, true);
            jObject.AddPropertyBool("Visible", Visible, true);
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyStringNullOrEmpty("BorderColor", StiJsonReportObjectHelper.Serialize.JColor(borderColor, Color.Black));
            jObject.AddPropertyFloat("Size", Size, 7f);
            jObject.AddPropertyFloat("Angle", Angle, 0f);
            jObject.AddPropertyEnum("Type", type, StiMarkerType.Circle);

            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "ShowInLegend":
                        this.showInLegend = property.Value.ToObject<bool>();
                        break;

                    case "Visible":
                        this.visible = property.Value.ToObject<bool>();
                        break;

                    case "Brush":
                        this.Brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "BorderColor":
                        this.borderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Size":
                        this.Size = property.Value.ToObject<float>();
                        break;

                    case "Angle":
                        this.Angle = property.Value.ToObject<float>();
                        break;

                    case "Type":
                        this.type = (StiMarkerType)Enum.Parse(typeof(StiMarkerType), property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public virtual StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiMarker;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public virtual StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.Marker()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            IStiMarker marker = this.MemberwiseClone() as IStiMarker;

            if (this.Core != null)
            {
                marker.Core = this.Core.Clone() as StiMarkerCoreXF;
                marker.Core.Marker = marker;
            }

            return marker;
        }
        #endregion

        #region Properties
        private StiMarkerCoreXF core;
        [Browsable(false)]
        public StiMarkerCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        private bool showInLegend = true;
        /// <summary>
        /// Gets or sets value which indicates that marker will be visible in legend marker.
        /// </summary>
        [DefaultValue(true)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets value which indicates that marker will be visible in legend marker.")]
        public virtual bool ShowInLegend
        {
            get
            {
                return showInLegend;
            }
            set
            {
                showInLegend = value;
            }
        }

        private bool visible = true;
        /// <summary>
        /// Gets or sets visibility of marker.
        /// </summary>
        [DefaultValue(true)]
        [StiSerializable]
        [StiCategory("Common")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of marker.")]
        public virtual bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }

        private StiBrush brush = new StiSolidBrush(Color.White);
        /// <summary>
        /// Gets or sets brush which will be used to fill marker area.
        /// </summary>
        [StiCategory("Appearance")]
        [StiSerializable]
        [Description("Gets or sets brush which will be used to fill marker area.")]
        public virtual StiBrush Brush
        {
            get
            {
                return brush;
            }
            set
            {
                brush = value;
            }
        }


        private Color borderColor = Color.Black;
        /// <summary>
        /// Gets or sets border color of marker.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Appearance")]
        [Description("Gets or sets border color of marker.")]
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
            set
            {
                borderColor = value;
            }
        }


        private float size = 7f;
        /// <summary>
        /// Gets or sets size of the marker.
        /// </summary>
        [DefaultValue(7f)]
        [StiSerializable]
        [StiCategory("Common")]
        [Description("Gets or sets size of the marker.")]
        public virtual float Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }

        private float angle = 0f;
        /// <summary>
        /// Gets or sets rotation angle of the marker.
        /// </summary>
        [DefaultValue(0f)]
        [StiSerializable]
        [StiCategory("Common")]
        [Description("Gets or sets rotation angle of the marker.")]
        public virtual float Angle
        {
            get
            {
                return angle;
            }
            set
            {
                angle = value;
            }
        }


        private StiMarkerType type = StiMarkerType.Circle;
        /// <summary>
        /// Gets or sets type of the marker.
        /// </summary>
        [DefaultValue(StiMarkerType.Circle)]
        [StiSerializable]
        [StiCategory("Common")]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets type of the marker.")]
        public StiMarkerType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
        #endregion

        public StiMarker()
        {
            this.Core = new StiMarkerCoreXF(this);
        }
    }
}
