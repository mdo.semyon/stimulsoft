﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Linq;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
    [StiServiceBitmap(typeof(StiTrendLine), "Stimulsoft.Report.Images.Components.StiChart.png")]
    [StiServiceCategoryBitmap(typeof(StiTrendLine), "Stimulsoft.Report.Images.Components.StiChart.png")]
    [TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiTrendLineConverter))]
    public abstract class StiTrendLine :
        StiService,
        IStiTrendLine,
        IStiSerializeToCodeAsClass,
        IStiPropertyGridObject
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyIdent("Ident", this.GetType().Name);

            jObject.AddPropertyStringNullOrEmpty("LineColor", StiJsonReportObjectHelper.Serialize.JColor(LineColor, Color.Black));
            jObject.AddPropertyFloat("LineWidth", lineWidth, 1f);
            jObject.AddPropertyEnum("LineStyle", lineStyle, StiPenStyle.Solid);
            jObject.AddPropertyBool("ShowShadow", showShadow);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "LineColor":
                        this.LineColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "LineWidth":
                        this.lineWidth = property.Value.ToObject<float>();
                        break;

                    case "LineStyle":
                        this.lineStyle = (StiPenStyle)Enum.Parse(typeof(StiPenStyle), property.Value.ToObject<string>());
                        break;

                    case "ShowShadow":
                        this.showShadow = property.Value.ToObject<bool>();
                        break;
                }
            }
        }

        internal static IStiTrendLine CreateFromJsonObject(JObject jObject)
        {
            var ident = jObject.Properties().FirstOrDefault(x => x.Name == "Ident").Value.ToObject<string>();
            var service = StiOptions.Services.ChartTrendLines.FirstOrDefault(x => x.GetType().Name == ident);

            if (service == null)
                throw new Exception($"Type {ident} is not found!");

            return service.CreateNew();
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public virtual StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiTrendLine;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.ChartTrendLine()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public override object Clone()
        {
            IStiTrendLine line = base.Clone() as IStiTrendLine;

            if (this.Core != null)
            {
                line.Core = this.Core.Clone() as StiTrendLineCoreXF;
                line.Core.TrendLine = line;
            }

            return line;
        }
        #endregion

        #region StiService override
        /// <summary>
        /// Gets a service name.
        /// </summary>
        public override string ServiceName
        {
            get
            {
                return Core.LocalizedName;
            }
        }

        /// <summary>
        /// Gets a service category.
        /// </summary>
        [Browsable(false)]
        public sealed override string ServiceCategory
        {
            get
            {
                return "Chart";
            }
        }

        /// <summary>
        /// Gets a service type.
        /// </summary>
        [Browsable(false)]
        public sealed override Type ServiceType
        {
            get
            {
                return typeof(StiTrendLine);
            }
        }
        #endregion

        #region Properties
        private StiTrendLineCoreXF core;
        [Browsable(false)]
        public StiTrendLineCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        private Color lineColor = Color.Black;
        /// <summary>
        /// Gets or sets color which will be used for drawing trend line.
        /// </summary>
        [StiSerializable]
        [StiOrder(StiTrendLinePropertyOrder.LineColor)]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets color which will be used for drawing trend line.")]
        public virtual Color LineColor
        {
            get
            {
                return lineColor;
            }
            set
            {
                lineColor = value;
            }
        }

        private float lineWidth = 1f;
        /// <summary>
        /// Gets or sets trend line width.
        /// </summary>
        [DefaultValue(1f)]
        [StiSerializable]
        [StiCategory("Common")]
        [StiOrder(StiTrendLinePropertyOrder.LineWidth)]
        [Description("Gets or sets trend line width.")]
        public float LineWidth
        {
            get
            {
                return lineWidth;
            }
            set
            {
                lineWidth = value;
            }
        }

        private StiPenStyle lineStyle = StiPenStyle.Solid;
        /// <summary>
        /// Gets or sets trend line style.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StiPenStyle.Solid)]
        [StiCategory("Common")]
        [StiOrder(StiTrendLinePropertyOrder.LineStyle)]
        [TypeConverter(typeof(StiEnumConverter))]
        [Description("Gets or sets trend line style.")]
        public StiPenStyle LineStyle
        {
            get
            {
                return lineStyle;
            }
            set
            {
                lineStyle = value;
            }
        }

        private bool showShadow = true;
        /// <summary>
        /// Gets or sets value which indicates draw shadow or no.
        /// </summary>
        [StiOrder(StiTrendLinePropertyOrder.ShowShadow)]
        [DefaultValue(true)]
        [StiCategory("Appearance")]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates draw shadow or no.")]
        public bool ShowShadow
        {
            get
            {
                return showShadow;
            }
            set
            {
                showShadow = value;
            }
        }

        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                }
            }
        }
        #endregion

        #region Methods
        public abstract StiTrendLine CreateNew();

        public override string ToString()
        {
            return ServiceName;
        }
        #endregion
    }
}
