#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;

using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
    public class StiConstantLines : 
        StiService, 
        IStiConstantLines,
        ICloneable,
        IStiPropertyGridObject
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            // StiBand
            jObject.AddPropertyBool("AllowApplyStyle", AllowApplyStyle, true);
            jObject.AddPropertyBool("Antialiasing", Antialiasing, true);
            jObject.AddPropertyEnum("Position", position, StiTextPosition.LeftTop);
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.Font(Font, "Arial", 7));
            jObject.AddPropertyStringNullOrEmpty("Text", text);
            jObject.AddPropertyBool("TitleVisible", titleVisible, true);
            jObject.AddPropertyEnum("Orientation", orientation, StiOrientation.Horizontal);
            jObject.AddPropertyFloat("LineWidth", lineWidth, 1f);
            jObject.AddPropertyEnum("LineStyle", lineStyle, StiPenStyle.Solid);
            jObject.AddPropertyStringNullOrEmpty("LineColor", StiJsonReportObjectHelper.Serialize.JColor(lineColor, Color.Black));
            jObject.AddPropertyBool("ShowInLegend", ShowInLegend, true);
            jObject.AddPropertyBool("ShowBehind", ShowBehind);
            jObject.AddPropertyString("AxisValue", AxisValue, "1");
            jObject.AddPropertyBool("Visible", Visible, true);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AllowApplyStyle":
                        this.AllowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "Antialiasing":
                        this.Antialiasing = property.Value.ToObject<bool>();
                        break;

                    case "Position":
                        this.position = (StiTextPosition)Enum.Parse(typeof(StiTextPosition), property.Value.ToObject<string>());
                        break;

                    case "Font":
                        this.Font = StiJsonReportObjectHelper.Deserialize.Font(property, this.font);
                        break;

                    case "Text":
                        this.text = property.Value.ToObject<string>();
                        break;

                    case "TitleVisible":
                        this.titleVisible = property.Value.ToObject<bool>();
                        break;

                    case "Orientation":
                        this.orientation = (StiOrientation)Enum.Parse(typeof(StiOrientation), property.Value.ToObject<string>());
                        break;

                    case "LineWidth":
                        this.lineWidth = property.Value.ToObject<float>();
                        break;

                    case "LineStyle":
                        this.lineStyle = (StiPenStyle)Enum.Parse(typeof(StiPenStyle), property.Value.ToObject<string>());
                        break;

                    case "LineColor":
                        this.LineColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "ShowInLegend":
                        this.ShowInLegend = property.Value.ToObject<bool>();
                        break;

                    case "ShowBehind":
                        this.ShowBehind = property.Value.ToObject<bool>();
                        break;

                    case "AxisValue":
                        this.AxisValue = property.Value.ToObject<string>();
                        break;

                    case "Visible":
                        this.Visible = property.Value.ToObject<bool>();
                        break;

                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiConstantLines;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, Base.StiLevel level)
        {
            var objHelper = new StiPropertyCollection();

            var propHelper = propertyGrid.PropertiesHelper;

            // BehaviorCategory
            var list = new[] 
            { 
                propHelper.AxisValue(),
                propHelper.LineColor(),
                propHelper.LineStyle(),
                propHelper.LineWidth(),
                propHelper.Orientation(),
                propHelper.ShowBehind(),
                propHelper.Visible()
            };
            objHelper.Add(StiPropertyCategories.Behavior, list);

            list = new[] 
            { 
                propHelper.AllowApplyStyle()
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            // TitleCategory
            list = new[]
            { 
                propHelper.Antialiasing(),
                propHelper.Font(),
                propHelper.Position(),
                propHelper.TextNotEdit(),
                propHelper.TitleVisible()
            };
            objHelper.Add(StiPropertyCategories.Title, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

        #region enum.StiOrientation
        public enum StiOrientation
        {
            Horizontal,
            Vertical,
            HorizontalRight
        }
        #endregion

        #region enum.StiTextPosition
        public enum StiTextPosition
        {
            LeftTop,
            LeftBottom,
            CenterTop,
            CenterBottom,
            RightTop,
            RightBottom
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public override object Clone()
        {
            var lines = this.MemberwiseClone() as IStiConstantLines;

            if (this.core != null)
            {
                lines.Core = this.core.Clone() as StiConstantLinesCoreXF;
                lines.Core.ConstantLines = lines;
            }

            return lines;
        }
        #endregion
                
        #region StiService override
        /// <summary>
        /// Gets a service category.
        /// </summary>
        [Browsable(false)]
        public sealed override string ServiceCategory
        {
            get
            {
                return "Chart";
            }
        }

        /// <summary>
        /// Gets a service type.
        /// </summary>
        [Browsable(false)]
        public sealed override Type ServiceType
        {
            get
            {
                return typeof(StiConstantLines);
            }
        }
        #endregion

        #region Properties
        private StiConstantLinesCoreXF core;
        [Browsable(false)]
        public StiConstantLinesCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }


        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        [StiCategory("Appearance")]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                    if (value && Chart != null)
                        this.Core.ApplyStyle(this.Chart.Style);
                }
            }
        }


        private bool antialiasing = true;
        /// <summary>
        /// Gets or sets value which control antialiasing drawing mode.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which control antialiasing drawing mode.")]
        public bool Antialiasing
        {
            get
            {
                return antialiasing;
            }
            set
            {
                antialiasing = value;
            }
        }


        private StiTextPosition position = StiTextPosition.LeftTop;
        /// <summary>
        /// Gets or sets text position at contant line.
        /// </summary>
        [StiSerializable]
        [StiCategory("Common")]
        [DefaultValue(StiTextPosition.LeftTop)]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets text position at contant line.")]
        public StiTextPosition Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }


        private Font font = new Font("Arial", 7);
        /// <summary>
        /// Gets or sets font which used for drawing constant line text.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [Description("Gets or sets font which used for drawing constant line text.")]
        public virtual Font Font
        {
            get
            {
                return font;
            }
            set
            {
                font = value;
            }
        }


        private string text = "";
        /// <summary>
        /// Gets or sets constant line text.
        /// </summary>
        [StiSerializable]
        [StiCategory("Common")]
        [DefaultValue("")]
        [Description("Gets or sets constant line text.")]
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }


        private bool titleVisible = true;
        /// <summary>
        /// Gets or sets visibility of constant lines title.
        /// </summary>
        [StiSerializable]
        [StiCategory("Common")]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of constant lines title.")]
        public bool TitleVisible
        {
            get 
            {
                return titleVisible;
            }
            set
            {
                titleVisible = value;
            }
        }


        private StiOrientation orientation = StiOrientation.Horizontal;
        /// <summary>
        /// Gets or sets horizontal or vertical orientation of constant line.
        /// </summary>
        [DefaultValue(StiOrientation.Horizontal)]
        [StiSerializable]
        [StiCategory("Common")]
        [Description("Gets or sets horizontal or vertical orientation of constant line.")]
        [TypeConverter(typeof(StiEnumConverter))]
        public StiOrientation Orientation
        {
            get
            {
                return orientation;
            }
            set
            {
                orientation = value;
            }
        }


        private float lineWidth = 1f;
        /// <summary>
        /// Gets or sets constant line width.
        /// </summary>
        [DefaultValue(1f)]
        [StiSerializable]
        [StiCategory("Common")]
        [Description("Gets or sets constant line width.")]
        public float LineWidth
        {
            get
            {
                return lineWidth;
            }
            set
            {
                lineWidth = value;
            }
        }


        private StiPenStyle lineStyle = StiPenStyle.Solid;
        /// <summary>
        /// Gets or sets constant line style.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StiPenStyle.Solid)]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets constant line style.")]
        public StiPenStyle LineStyle
        {
            get
            {
                return lineStyle;
            }
            set
            {
                lineStyle = value;
            }
        }


        private Color lineColor = Color.Black;
        /// <summary>
        /// Gets or sets color which will be used for drawing constant line.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Appearance")]
        [Description("Gets or sets color which will be used for drawing constant line.")]
        public virtual Color LineColor
        {
            get
            {
                return lineColor;
            }
            set
            {
                lineColor = value;
            }
        }


        private bool showInLegend = true;
        /// <summary>
        /// Gets or sets constant lines in chart legend.
        /// </summary>
        [Browsable (false)]
        [DefaultValue(true)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets constant lines in chart legend.")]
        public virtual bool ShowInLegend
        {
            get
            {
                return showInLegend;
            }
            set
            {
                showInLegend = value;
            }
        }


        private bool showBehind = false;
        /// <summary>
        /// Gets or sets value which indicates that constant lines will be shown behind chart series or in front of chart series.
        /// </summary>
        [DefaultValue(false)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets value which indicates that constant lines will be shown behind chart series or in front of chart series.")]
        public virtual bool ShowBehind
        {
            get
            {
                return showBehind;
            }
            set
            {
                showBehind = value;
            }
        }


        private string axisValue = "1";
        /// <summary>
        /// Gets or sets Y axis value through what the constant line is drawn.
        /// </summary>
        [DefaultValue("1")]
        [StiSerializable]
        [StiCategory("Common")]
        [Description("Gets or sets Y axis value through what the constant line is drawn.")]
        public virtual string AxisValue
        {                  
            get
            {
                return axisValue;
            }
            set
            {
                axisValue = value;
            }
        }


        private bool visible = true;
        /// <summary>
        /// Gets or sets visibility of constant line.
        /// </summary>
        [DefaultValue(true)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets visibility of constant line.")]
        public virtual bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }


        private IStiChart chart;
        [StiSerializable(StiSerializationVisibility.Reference)]
        [Browsable(false)]
        public IStiChart Chart
        {
            get
            {
                return chart;
            }
            set
            {
                chart = value;
            }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return ServiceName;
        }
        #endregion

        public StiConstantLines()
        {
            this.Core = new StiConstantLinesCoreXF(this);
        }
    }
}