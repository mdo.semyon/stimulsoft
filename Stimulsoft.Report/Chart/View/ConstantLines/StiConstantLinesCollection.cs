#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Collections;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;


namespace Stimulsoft.Report.Chart
{
	[TypeConverter(typeof(Stimulsoft.Report.Chart.Design.StiSerialConverter))]
    public class StiConstantLinesCollection :
        CollectionBase,
        IStiApplyStyle,
        IStiJsonReportObject
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            if (List.Count == 0)
                return null;

            var jObject = new JObject();

            int index = 0;
            foreach (IStiConstantLines constantLines in List)
            {
                jObject.AddPropertyJObject(index.ToString(), constantLines.SaveToJsonObject(mode));
                index++;
            }

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                var constantLines = new StiConstantLines();
                constantLines.Chart = this.chart;

                Add(constantLines);
                constantLines.LoadFromJsonObject((JObject)property.Value);
            }
        }
        #endregion

        #region IStiApplyStyle
        /// <summary>
        /// Applying specified style to this area.
        /// </summary>
        /// <param name="style"></param>
        public void ApplyStyle(IStiChartStyle style)
        {
            foreach (IStiConstantLines lines in this)
            {
                lines.Core.ApplyStyle(style);
            }
        }
        #endregion

		#region Methods
        private string GetConstantLineTitle()
        {
            string baseTitle = StiLocalization.Get("Chart", "ConstantLine");
            string title = baseTitle;

            int index = 1;
            bool finded = true;

            while (finded)
            {
                title = baseTitle + " " + index.ToString();
                finded = false;
                foreach (IStiConstantLines line in this)
                {
                    if (line.Text == title)
                    {
                        finded = true;
                        break;
                    }
                }
                index++;
            }
            return title;
        }

        private void AddCore(IStiConstantLines value)
        {
            if (Chart != null)
            {
                if (string.IsNullOrEmpty(value.Text)) value.Text = GetConstantLineTitle();
                value.Chart = Chart;
            }

            base.List.Add(value);
        }

        public void Add(IStiConstantLines value)
        {
            AddCore(value);
            InvokeConstantLinesAdded(value, List.Count - 1);
        }

        public void AddRange(IStiConstantLines[] values)
        {
            foreach (IStiConstantLines value in values)
            {
                AddCore(value);
                InvokeConstantLinesAdded(value, List.Count - 1);
            }
        }

        public void AddRange(StiConstantLinesCollection values)
        {
            foreach (IStiConstantLines value in values)
            {
                AddCore(value);
                InvokeConstantLinesAdded(value, List.Count - 1);
            }
        }

        public bool Contains(IStiConstantLines value)
        {
            return List.Contains(value);
        }

        public int IndexOf(IStiConstantLines value)
        {
            return List.IndexOf(value);
        }

        public void Insert(int index, IStiConstantLines value)
        {
            List.Insert(index, value);

            InvokeConstantLinesAdded(value, index);
        }

        protected override void OnClear()
        {
            while (List.Count > 0)
            {
                InvokeConstantLinesRemoved(List[0], 0);
                List.RemoveAt(0);
            }
        }

        public void Remove(IStiConstantLines value)
        {
            InvokeConstantLinesRemoved(value, List.IndexOf(value));
            List.Remove(value);
        }

        public IStiConstantLines this[int index]
        {
            get
            {
                return (IStiConstantLines)InnerList[index];
            }
            set
            {
                InnerList[index] = value;
            }
        }
		#endregion

        #region Events

        #region ConstantLinesAdded
        public event EventHandler ConstantLinesAdded;

        protected virtual void OnConstantLinesAdded(EventArgs e)
        {
        }

        private void InvokeConstantLinesAdded(object sender, int index)
        {
            if (ConstantLinesAdded != null) ConstantLinesAdded(sender, EventArgs.Empty);
        }
        #endregion

        #region ConstantLinesRemoved
        public event EventHandler ConstantLinesRemoved;

        protected virtual void OnConstantLinesRemoved(EventArgs e)
        {
        }

        private void InvokeConstantLinesRemoved(object sender, int index)
        {
            if (ConstantLinesRemoved != null) ConstantLinesRemoved(sender, EventArgs.Empty);
        }
        #endregion

        #endregion

        #region Properties
        private StiChart chart = null;
		[Browsable(false)]
		public StiChart Chart
		{
			get
			{
				return chart;
			}
			set
			{
				chart = value;
			}
        }
        #endregion

        [StiUniversalConstructor("ConstantLines")]
        public StiConstantLinesCollection()
        {

        }
    }
}