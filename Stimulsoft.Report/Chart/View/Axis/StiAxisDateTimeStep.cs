﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using System;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    [TypeConverter(typeof(StiUniversalConverter))]
    public class StiAxisDateTimeStep :
        IStiAxisDateTimeStep
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyEnum("Step", step, StiTimeDateStep.None);
            jObject.AddPropertyInt("NumberOfValues", numberOfValues, 1);
            jObject.AddPropertyBool("Interpolation", interpolation);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Step":
                        this.step = (StiTimeDateStep)Enum.Parse(typeof(StiTimeDateStep), property.Value.ToObject<string>());
                        break;

                    case "NumberOfValues":
                        this.numberOfValues = property.Value.ToObject<int>();
                        break;

                    case "Interpolation":
                        this.interpolation = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {  
            return this.MemberwiseClone();
        }
        #endregion

        #region Properties
        private StiTimeDateStep step = StiTimeDateStep.None;
        /// <summary>
        /// Gets or sets a value that indicates with what the time step values will be shown.
        /// </summary>
        [DefaultValue(StiTimeDateStep.None)]
        [StiCategory("Common")]
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets a value that indicates with what the time step values will be shown.")]
        public StiTimeDateStep Step
        {
            get
            {
                return step;
            }
            set
            {
                step = value;
            }
        }

        private int numberOfValues = 1;
        /// <summary>
        /// Gets or sets number of values in step.
        /// </summary>
        [DefaultValue(1)]
        [StiCategory("Common")]
        [StiSerializable]
        [Description("Gets or sets number of values in step.")]
        public int NumberOfValues
        {
            get
            {
                return numberOfValues;
            }
            set
            {
                numberOfValues = value;
            }
        }

        private bool interpolation = false;
        /// <summary>
        /// Gets or sets A value indicates that the values ​​will be interpolated.
        /// </summary>
        [DefaultValue(false)]
        [StiCategory("Common")]
        [StiSerializable]
        [Description("Gets or sets A value indicates that the values ​​will be interpolated.")]
        public  bool Interpolation
        {
            get
            {
                return interpolation;
            }
            set
            {
                interpolation = value;
            }
        }
        #endregion

        public StiAxisDateTimeStep()
        {
        }

        public StiAxisDateTimeStep(StiTimeDateStep step, int numberOfValues)
        {
            this.step = step;
            this.numberOfValues = numberOfValues;
        }

        [StiUniversalConstructor("DateTimeStep")]
        public StiAxisDateTimeStep(StiTimeDateStep step, int numberOfValues, bool interpolation)
        {
            this.step = step;
            this.numberOfValues = numberOfValues;
            this.interpolation = interpolation;
        }
    }
}
