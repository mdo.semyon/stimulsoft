#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{       

	[TypeConverter(typeof(StiUniversalConverter))]
    public abstract class StiAxis : IStiAxis       
	{
        #region IStiJsonReportObject.override
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyIdent("Ident", this.GetType().Name);

            jObject.AddPropertyBool("LogarithmicScale", LogarithmicScale);
            jObject.AddPropertyBool("AllowApplyStyle", AllowApplyStyle, true);
            jObject.AddPropertyBool("StartFromZero", StartFromZero, true);
            jObject.AddPropertyJObject("Interaction", interaction.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Labels", labels.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Ticks", ticks.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Range", Range.SaveToJsonObject(mode));
            jObject.AddPropertyBool("Visible", visible, true);
            if (title != null)
                jObject.AddPropertyJObject("Title", title.SaveToJsonObject(mode));

            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "LogarithmicScale":
                        this.LogarithmicScale = property.Value.ToObject<bool>();
                        break;

                    case "AllowApplyStyle":
                        this.AllowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "StartFromZero":
                        this.StartFromZero = property.Value.ToObject<bool>();
                        break;

                    case "Interaction":
                        this.interaction.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "Labels":
                        this.labels.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "Ticks":
                        this.ticks.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "Range":
                        this.Range.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "Title":
                        this.Title.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "Visible":
                        this.visible = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone()
		{
            IStiAxis axis = this.MemberwiseClone() as IStiAxis;
			axis.ArrowStyle =	 this.ArrowStyle;
			axis.LineStyle =	 this.LineStyle;
			axis.Labels =		 this.labels.Clone() as IStiAxisLabels;
			axis.Range =		 this.range.Clone() as IStiAxisRange;
			axis.Title =		 this.Title.Clone() as IStiAxisTitle;
			axis.Ticks =		 this.ticks.Clone() as IStiAxisTicks;

            if (this.core != null)
            {
                axis.Core = this.core.Clone() as StiAxisCoreXF;
                axis.Core.Axis = axis;
            }

            return axis;
		}
        #endregion
        
        #region Properties
        private bool logarithmicScale = false;
        /// <summary>
        /// Gets or sets value which indicates that logarithmic scale will be used.
        /// </summary>
        [StiCategory("Common")]
        [StiSerializable]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that logarithmic scale will be used.")]
        public virtual bool LogarithmicScale
        {
            get
            {
                return logarithmicScale;
            }
            set
            {
                logarithmicScale = value;
            }
        }

        private StiAxisCoreXF core;
        [Browsable(false)]
        public StiAxisCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        
        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                    if (value && this.Area != null && this.Area.Chart != null)
                        this.Core.ApplyStyle(this.Area.Chart.Style);
                }
            }
        }
                
        
        private bool startFromZero = true;
        /// <summary>
        /// Gets or sets value which indicates that all arguments will be shows from zero.
        /// </summary>
        [StiSerializable]
        [StiCategory("Common")]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that all arguments will be shows from zero.")]
        public virtual bool StartFromZero
        {
            get
            {
                return startFromZero;
            }
            set
            {
                startFromZero = value;
            }
        }


        /// <summary>
        /// Gets or sets value which indicates with what steps do labels be shown on axis.
        /// </summary>
        [StiSerializable]
        [DefaultValue(0f)]
        [Obsolete("Step property is obsolete. Please use Labels.Step property instead it.")]
        [Browsable(false)]
        [StiCategory("Common")]
        public virtual float Step
        {
            get
            {
                if (Labels == null)
                    return 0;
                return Labels.Step;
            }
            set
            {
                if (Labels != null)
                    Labels.Step = value;
            }
        }

        private IStiAxisInteraction interaction = new StiAxisInteraction();
        /// <summary>
        /// Gets or sets axis interaction settings.
        /// </summary>
        [StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(StiUniversalConverter))]
        [StiCategory("Common")]
        public IStiAxisInteraction Interaction
        {
            get
            {
                return interaction;
            }
            set
            {
                interaction = value;
            }
        }


		private IStiAxisLabels labels = new StiAxisLabels();
        /// <summary>
        /// Gets or sets axis labels settings.
        /// </summary>
		[StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(StiUniversalConverter))]
        [StiCategory("Common")]
		public IStiAxisLabels Labels
		{
			get
			{
				return labels;
			}
			set
			{
				labels = value;
			}
		}


		private IStiAxisRange range = new StiAxisRange();
        /// <summary>
        /// Gets or sets axis range settings.
        /// </summary>
		[StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(StiUniversalConverter))]
        [StiCategory("Common")]
		public virtual IStiAxisRange Range
		{
			get
			{
				return range;
			}
			set
			{
				range = value;
			}
		}


        private IStiAxisTitle title;
        /// <summary>
        /// Gets or sets axis title settings.
        /// </summary>
		[StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(StiUniversalConverter))]
        [StiCategory("Common")]
		public IStiAxisTitle Title
		{
			get
			{
                if (title == null)
                {
                    title = new StiAxisTitle();
                    if (this is StiXBottomAxis) title.Direction = StiDirection.LeftToRight;
                    else if (this is StiXTopAxis) title.Direction = StiDirection.LeftToRight;
                    else if (this is StiYRightAxis) title.Direction = StiDirection.TopToBottom;
                    else if (this is StiYLeftAxis) title.Direction = StiDirection.BottomToTop;
                }

				return title;
			}
			set
			{
			    if (title == null)
			    {
			        title = new StiAxisTitle();
                    if (this is StiXBottomAxis) title.Direction = StiDirection.LeftToRight;
                    else if (this is StiXTopAxis) title.Direction = StiDirection.LeftToRight;
                    else if (this is StiYRightAxis) title.Direction = StiDirection.TopToBottom;
                    else if (this is StiYLeftAxis) title.Direction = StiDirection.BottomToTop;
			    }

				title = value;
			}
		}       


		private IStiAxisTicks ticks = new StiAxisTicks();
        /// <summary>
        /// Gets or sets ticks settings.
        /// </summary>
		[StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(StiUniversalConverter))]
        [StiCategory("Common")]
		public IStiAxisTicks Ticks
		{
			get
			{
				return ticks;
			}
			set
			{
				ticks = value;
			}
		}


		private StiArrowStyle arrowStyle = StiArrowStyle.None;
        /// <summary>
        /// Gets or sets style of axis arrow.
        /// </summary>
        [DefaultValue(StiArrowStyle.None)]
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiCategory("Common")]
        public StiArrowStyle ArrowStyle
        {
            get
            {
                return arrowStyle;
            }
            set
            {
                arrowStyle = value;
            }
        }


        private StiPenStyle lineStyle = StiPenStyle.Solid;
        /// <summary>
        /// Gets or sets line style of axis.
        /// </summary>
		[StiSerializable]
		[DefaultValue(StiPenStyle.Solid)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiCategory("Common")]
        public StiPenStyle LineStyle
        {
            get
            {
                return lineStyle;
            }
            set
            {
                lineStyle = value;
            }
        }


        private Color lineColor = Color.Gray;
        /// <summary>
        /// Gets or sets line color which used to draw axis.
        /// </summary>
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Appearance")]
        public Color LineColor
        {
            get
            {
                return lineColor;
            }
            set
            {
                lineColor = value;
            }
        }


        private float lineWidth = 1f;
        /// <summary>
        /// Gets or sets line width which used to draw axis.
        /// </summary>
        [DefaultValue(1f)]
		[StiSerializable]
        [StiCategory("Common")]
        public float LineWidth
        {
            get
            {
                return lineWidth;
            }
            set
            {
                lineWidth = value;
            }
        }
        		

        private bool visible = true;
        /// <summary>
        /// Gets or sets visibility of axis.
        /// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of axis.")]
        [StiCategory("Common")]
        public virtual bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }


        /// <summary>
        /// 'TitleDirection' property is obsolete. Please Title.Direction property instead it.
        /// </summary>
        [Description("'TitleDirection' property is obsolete. Please Title.Direction property instead it.")]
        [Obsolete("'TitleDirection' property is obsolete. Please Title.Direction property instead it.")]
        [Browsable(false)]
        public virtual StiLegendDirection TitleDirection
        {
            get
            {
                if (Title == null)
                    return StiLegendDirection.BottomToTop;

                switch (Title.Direction)
                {
                    case StiDirection.BottomToTop:
                        return StiLegendDirection.BottomToTop;

                    case StiDirection.LeftToRight:
                        return StiLegendDirection.LeftToRight;

                    case StiDirection.RightToLeft:
                        return StiLegendDirection.RightToLeft;

                    case StiDirection.TopToBottom:
                        return StiLegendDirection.TopToBottom;
                }

                return StiLegendDirection.BottomToTop;
            }
            set
            {
                if (Title == null) return;
                switch (value)
                {
                    case StiLegendDirection.BottomToTop:
                        Title.Direction = StiDirection.BottomToTop;
                        break;

                    case StiLegendDirection.LeftToRight:
                        Title.Direction = StiDirection.LeftToRight;
                        break;

                    case StiLegendDirection.RightToLeft:
                        Title.Direction = StiDirection.RightToLeft;
                        break;

                    case StiLegendDirection.TopToBottom:
                        Title.Direction = StiDirection.TopToBottom;
                        break;
                }
            }
        }              


        private IStiAxisArea area;
        [StiSerializable(StiSerializationVisibility.Reference)]
        [Browsable(false)]
        public IStiAxisArea Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
            }
        }


        private StiAxisInfoXF info = new StiAxisInfoXF();
        [Browsable(false)]
        public StiAxisInfoXF Info
        {
            get
            {
                return info;
            }
            set
            {
                info = value;
            }
        } 
        #endregion

        public StiAxis()
		{
        }

        public StiAxis(
            IStiAxisLabels labels,
            IStiAxisRange range,
            IStiAxisTitle title,
            IStiAxisTicks ticks,
            StiArrowStyle arrowStyle,
            StiPenStyle lineStyle,
            Color lineColor,
            float lineWidth,
            bool visible,
            bool startFromZero,
            bool allowApplyStyle)
        {
            this.labels = labels;
            this.range = range;
            this.title = title;
            this.ticks = ticks;
            this.arrowStyle = arrowStyle;
            this.lineStyle = lineStyle;
            this.lineColor = lineColor;
            this.lineWidth = lineWidth;
            this.visible = visible;
            this.startFromZero = startFromZero;
            this.allowApplyStyle = allowApplyStyle;
        }

		public StiAxis(
			IStiAxisLabels labels,
			IStiAxisRange range,
			IStiAxisTitle title,
			IStiAxisTicks ticks,
            IStiAxisInteraction interaction,
			StiArrowStyle arrowStyle,
			StiPenStyle lineStyle,
			Color lineColor,
			float lineWidth,
			bool visible,
            bool startFromZero,
            bool allowApplyStyle)
		{
			this.labels = labels;
			this.range = range;
			this.title = title;
			this.ticks = ticks;
            this.interaction = interaction;
			this.arrowStyle = arrowStyle;
			this.lineStyle = lineStyle;
			this.lineColor = lineColor;
			this.lineWidth = lineWidth;
			this.visible = visible;
            this.startFromZero = startFromZero;
            this.allowApplyStyle = allowApplyStyle;     
        }

        public StiAxis(
            IStiAxisLabels labels,
            IStiAxisRange range,
            IStiAxisTitle title,
            IStiAxisTicks ticks,
            IStiAxisInteraction interaction,
            StiArrowStyle arrowStyle,
            StiPenStyle lineStyle,
            Color lineColor,
            float lineWidth,
            bool visible,
            bool startFromZero,
            bool allowApplyStyle,
            bool logarithmicScale)
        {
            this.labels = labels;
            this.range = range;
            this.title = title;
            this.ticks = ticks;
            this.interaction = interaction;
            this.arrowStyle = arrowStyle;
            this.lineStyle = lineStyle;
            this.lineColor = lineColor;
            this.lineWidth = lineWidth;
            this.visible = visible;
            this.startFromZero = startFromZero;
            this.allowApplyStyle = allowApplyStyle;
            this.logarithmicScale = logarithmicScale;

        }
    }
}
