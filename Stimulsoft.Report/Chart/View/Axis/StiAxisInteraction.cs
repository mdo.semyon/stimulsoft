#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Text;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;


namespace Stimulsoft.Report.Chart
{    
	
	[TypeConverter(typeof(StiUniversalConverter))]
    public class StiAxisInteraction : 
        IStiAxisInteraction,
        ICloneable,
        IStiJsonReportObject
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyBool("ShowScrollBar", showScrollBar);
            jObject.AddPropertyBool("RangeScrollEnabled", rangeScrollEnabled, true);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "ShowScrollBar":
                        this.showScrollBar = property.Value.ToObject<bool>();
                        break;

                    case "RangeScrollEnabled":
                        this.rangeScrollEnabled = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region ICloneable override
        /// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone()
		{
            IStiAxisInteraction interaction = this.MemberwiseClone() as IStiAxisInteraction;

            return interaction;
		}
		#endregion

		#region Properties
        private bool showScrollBar = false;
        /// <summary>
        /// Gets or sets value which indicates that scroll bar will be shown.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that scroll bar will be shown.")]
        [DefaultValue(false)]
        [StiCategory("Common")]
        public bool ShowScrollBar
        {
            get
            {
                return showScrollBar;
            }
            set
            {
                if (showScrollBar != value)
                {
                    showScrollBar = value;
                }
            }
        }


        private bool rangeScrollEnabled = true;
        /// <summary>
        /// Gets or sets value which indicates whether the range of axis can be scrolled.
        /// </summary>
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates whether the range of axis can be scrolled.")]
        [StiCategory("Common")]
        public virtual bool RangeScrollEnabled
        {
            get
            {
                return rangeScrollEnabled;
            }
            set
            {
                if (rangeScrollEnabled != value)
                {
                    rangeScrollEnabled = value;
                }
            }
        }
		#endregion

        public StiAxisInteraction()
		{
		}


        [StiUniversalConstructor("Interaction")]
        public StiAxisInteraction(
            bool showScrollBar,
            bool rangeScrollEnabled)
		{
            this.showScrollBar = showScrollBar;
            this.rangeScrollEnabled = rangeScrollEnabled;
		}
	}
}
