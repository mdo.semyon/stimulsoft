#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Json.Linq;
using System;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
	[TypeConverter(typeof(StiUniversalConverter))]
    public class StiAxisTitle : 
        IStiAxisTitle
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyBool("AllowApplyStyle", allowApplyStyle, true);
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.Font(font, "Tahoma", 12, FontStyle.Bold));
            jObject.AddPropertyStringNullOrEmpty("Text", text);
            jObject.AddPropertyStringNullOrEmpty("Color", StiJsonReportObjectHelper.Serialize.JColor(color, Color.Black));
            jObject.AddPropertyBool("Antialiasing", antialiasing, true);
            jObject.AddPropertyEnum("Alignment", alignment, StringAlignment.Center);
            jObject.AddPropertyEnum("Position", position, StiTitlePosition.Outside);
            jObject.AddPropertyEnum("Direction", Direction, StiDirection.LeftToRight);
            
            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AllowApplyStyle":
                        this.allowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "Font":
                        this.font = StiJsonReportObjectHelper.Deserialize.Font(property, font);
                        break;

                    case "Text":
                        this.text = property.Value.ToObject<string>();
                        break;

                    case "Color":
                        this.color = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Antialiasing":
                        this.antialiasing = property.Value.ToObject<bool>();
                        break;

                    case "Alignment":
                        this.alignment = (StringAlignment)Enum.Parse(typeof(StringAlignment), property.Value.ToObject<string>());
                        break;

                    case "Position":
                        this.position = (StiTitlePosition)Enum.Parse(typeof(StiTitlePosition), property.Value.ToObject<string>());
                        break;

                    case "Direction":
                        this.Direction = (StiDirection)Enum.Parse(typeof(StiDirection), property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region ICloneable override
        /// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone()
		{
			IStiAxisTitle title =	this.MemberwiseClone() as IStiAxisTitle;
			title.Alignment =		this.Alignment;
            title.Direction =       this.Direction;
			title.Font =			this.Font.Clone() as Font;

            if (this.core != null)
            {
                title.Core = this.core.Clone() as StiAxisTitleCoreXF;
                title.Core.Title = title;
            }
			
			return title;
		}
		#endregion

		#region Properties
        private StiAxisTitleCoreXF core;
        [Browsable(false)]
        public StiAxisTitleCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                }
            }
        }

		private Font font = new Font("Tahoma", 12, FontStyle.Bold);
        /// <summary>
        /// Gets or set font which will be used for axis title drawing.
        /// </summary>
		[StiSerializable]
        [Description("Gets or set font which will be used for axis title drawing.")]
        [StiCategory("Appearance")]
		public Font Font
		{
			get
			{
				return font;
			}
			set
			{
				font = value;
			}
		}


		private string text = "";
        /// <summary>
        /// Gets or sets title text.
        /// </summary>
		[DefaultValue("")]
		[StiSerializable]
        [Description("Gets or sets title text.")]
        [StiCategory("Common")]
		public string Text
		{
			get
			{
				return text;
			}
			set
			{
				text = value;
			}
		}

		
		private Color color = Color.Black;
        /// <summary>
        /// Gets or sets color which will be used for title drawing.
        /// </summary>
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets color which will be used for title drawing.")]
        [StiCategory("Appearance")]
		public Color Color
		{
			get
			{
				return color;
			}
			set
			{
				color = value;
			}
		}


		private bool antialiasing = true;
        /// <summary>
        /// Gets or sets value which control antialiasing drawing mode.
        /// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which control antialiasing drawing mode.")]
        [StiCategory("Appearance")]
		public bool Antialiasing
		{
			get
			{
				return antialiasing;
			}
			set
			{
				antialiasing = value;
			}
		}


		private StringAlignment alignment = StringAlignment.Center;
        /// <summary>
        /// Gets or sets title text alignment.
        /// </summary>
		[StiSerializable]
		[DefaultValue(StringAlignment.Center)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets title text alignment.")]
        [StiCategory("Common")]
		public StringAlignment Alignment
		{
			get
			{
				return alignment;
			}
			set
			{
				alignment = value;
			}
		}

        private StiTitlePosition position = StiTitlePosition.Outside;
        /// <summary>
        /// Gets or sets title text position.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StiTitlePosition.Outside)]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets title text position.")]
        [StiCategory("Common")]
        public StiTitlePosition Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }


        private StiDirection direction = StiDirection.LeftToRight;
        /// <summary>
        /// Gets or set text direction for axis title drawing.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StiDirection.LeftToRight)]
        [Description("Gets or set text direction for axis title drawing.")]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiCategory("Common")]
        public virtual StiDirection Direction
        {
            get
            {
                return direction;
            }
            set
            {
                direction = value;
            }
        }
		#endregion

        public StiAxisTitle()
		{
            this.core = new StiAxisTitleCoreXF(this);
		}

        public StiAxisTitle(
            Font font,
            string text,
            Color color,
            bool antialiasing,
            StringAlignment alignment,
            StiDirection direction,
            bool allowApplyStyle
            )
        {
            this.font = font;
            this.text = text;
            this.color = color;
            this.antialiasing = antialiasing;
            this.alignment = alignment;
            this.direction = direction;
            this.allowApplyStyle = allowApplyStyle;

            this.core = new StiAxisTitleCoreXF(this);
        }

        [StiUniversalConstructor("Title")]
        public StiAxisTitle(
            Font font,
            string text,
            Color color,
            bool antialiasing,
            StringAlignment alignment,
            StiDirection direction,
            bool allowApplyStyle,
            StiTitlePosition position
            )
        {
            this.font = font;
            this.text = text;
            this.color = color;
            this.antialiasing = antialiasing;
            this.alignment = alignment;
            this.direction = direction;
            this.allowApplyStyle = allowApplyStyle;
            this.position = position;

            this.core = new StiAxisTitleCoreXF(this);
        }
	}
}
