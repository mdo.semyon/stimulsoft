#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Text;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;


namespace Stimulsoft.Report.Chart
{    
	[TypeConverter(typeof(StiUniversalConverter))]
	public class StiAxisRange : 
        IStiAxisRange,
        ICloneable        
	{
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyDouble("Minimum", minimum, 0d);
            jObject.AddPropertyDouble("Maximum", maximum, 0d);
            jObject.AddPropertyBool("Auto", auto, true);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Minimum":
                        this.minimum = property.Value.ToObject<double>();
                        break;

                    case "Maximum":
                        this.maximum = property.Value.ToObject<double>();
                        break;

                    case "Auto":
                        this.auto = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone()
		{
			return this.MemberwiseClone();
		}
		#endregion

		#region Properties
		private double minimum = 0d;
        /// <summary>
        /// Gets or sets minimum value of axis range.
        /// </summary>
		[DefaultValue(0d)]
		[StiSerializable]
        [Description("Gets or sets minimum value of axis range.")]
        [StiOrder(100)]
        [StiCategory("Common")]
		public double Minimum
		{
			get
			{
				return minimum;
			}
			set
			{
				minimum = value;
			}
		}


		private double maximum = 0d;
        /// <summary>
        /// Gets or sets maximum value of axis range.
        /// </summary>
		[DefaultValue(0d)]
		[StiSerializable]
        [Description("Gets or sets maximum value of axis range.")]
        [StiOrder(110)]
        [StiCategory("Common")]
		public double Maximum
		{
			get
			{
				return maximum;
			}
			set
			{
				maximum = value;
			}
		}


		private bool auto = true;
        /// <summary>
        /// Gets or sets value which indicates that minimum and maximum values will be calculated automatically.
        /// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that minimum and maximum values will be calculated automatically.")]
        [StiOrder(90)]
        [StiCategory("Common")]
		public bool Auto
		{
			get
			{
				return auto;
			}
			set
			{
				auto = value;
			}
		}
		#endregion

		public StiAxisRange()
		{
		}


		[StiUniversalConstructor("Range")]
		public StiAxisRange(
			bool auto,
			double minimum,
			double maximum
			)
		{
			this.auto = auto;
			this.minimum = minimum;
			this.maximum = maximum;
		}
	}
}
