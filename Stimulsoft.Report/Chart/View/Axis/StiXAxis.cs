#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using System;

namespace Stimulsoft.Report.Chart
{
    public abstract class StiXAxis : 
        StiAxis,
        IStiXAxis,
        IStiPropertyGridObject
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            jObject.AddPropertyBool("ShowEdgeValues", showEdgeValues);
            jObject.AddPropertyEnum("ShowXAxis", ShowXAxis, StiShowXAxis.Both);
            jObject.AddPropertyJObject("DateTimeStep", DateTimeStep.SaveToJsonObject(mode));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "ShowEdgeValues":
                        this.ShowEdgeValues = property.Value.ToObject<bool>();
                        break;

                    case "ShowXAxis":
                        this.ShowXAxis = (StiShowXAxis)Enum.Parse(typeof(StiShowXAxis), property.Value.ToObject<string>());
                        break;

                    case "DateTimeStep":
                        this.DateTimeStep.LoadFromJsonObject((JObject)property.Value);
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public virtual StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiXAxis;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public virtual StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.XAxis()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

		#region Properties
        // Always Show Edge Values   (Added 2007.02.26)
        private bool showEdgeValues = false;
        /// <summary>
        /// Gets or sets value which indicates that first and last arguments on axis will be shown anyway.
        /// </summary>
        [DefaultValue(false)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that first and last arguments on axis will be shown anyway.")]
        [StiCategory("Common")]
        public bool ShowEdgeValues
        {
            get
            {
                return showEdgeValues;
            }
            set
            {
                showEdgeValues = value;
            }
        }


        private StiShowXAxis showXAxis = StiShowXAxis.Both;
        /// <summary>
        /// Gets or sets type of drawing X axis.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StiShowXAxis.Both)]
        [Description("Gets or sets type of drawing X axis.")]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiCategory("Common")]
        public virtual StiShowXAxis ShowXAxis
        {
            get
            {
                return showXAxis;
            }
            set
            {
                showXAxis = value;
            }
        }

        private IStiAxisDateTimeStep dateTimeStep = new StiAxisDateTimeStep();
        /// <summary>
        /// Gets or sets date time step settings.
        /// </summary>
        [StiSerializable(StiSerializationVisibility.Class)]
        [TypeConverter(typeof(StiUniversalConverter))]
        [Description("Gets or sets date time step settings.")]
        [StiCategory("Common")]
        public virtual IStiAxisDateTimeStep DateTimeStep
        {
            get
            {
                return dateTimeStep;
            }
            set
            {
                dateTimeStep = value;
            }
        }
        #endregion

        public StiXAxis()
		{
		}

        public StiXAxis(
            IStiAxisLabels labels,
            IStiAxisRange range,
            IStiAxisTitle title,
            IStiAxisTicks ticks,
            StiArrowStyle arrowStyle,
            StiPenStyle lineStyle,
            Color lineColor,
            float lineWidth,
            bool visible,
            bool startFromZero,
            StiShowXAxis showXAxis,
            bool allowApplyStyle
            ): this(labels, range, title, ticks, arrowStyle, 
                    lineStyle, lineColor, lineWidth, visible, 
                    startFromZero, showXAxis, false, allowApplyStyle)
        {
        }

		public StiXAxis(
			IStiAxisLabels labels,
			IStiAxisRange range,
			IStiAxisTitle title,
			IStiAxisTicks ticks,
			StiArrowStyle arrowStyle,
			StiPenStyle lineStyle,
			Color lineColor,
			float lineWidth,
			bool visible,
            bool startFromZero,
            StiShowXAxis showXAxis,
            bool showEdgeValues,
            bool allowApplyStyle) 
            : 
			
			base(
			labels,
			range,
			title,
			ticks,
			arrowStyle,
			lineStyle,
			lineColor,
			lineWidth,
			visible,
            startFromZero,
            allowApplyStyle)
		{
            this.showXAxis = showXAxis;
            this.showEdgeValues = showEdgeValues;
		}


        public StiXAxis(
            IStiAxisLabels labels,
            IStiAxisRange range,
            IStiAxisTitle title,
            IStiAxisTicks ticks,
            IStiAxisInteraction interaction,
            StiArrowStyle arrowStyle,
            StiPenStyle lineStyle,
            Color lineColor,
            float lineWidth,
            bool visible,
            bool startFromZero,
            StiShowXAxis showXAxis,
            bool showEdgeValues,
            bool allowApplyStyle)
            :

            base(
            labels,
            range,
            title,
            ticks,
            interaction,
            arrowStyle,
            lineStyle,
            lineColor,
            lineWidth,
            visible,
            startFromZero,
            allowApplyStyle)
        {
            this.showXAxis = showXAxis;
            this.dateTimeStep = new StiAxisDateTimeStep();
            this.showEdgeValues = showEdgeValues;
        }

        public StiXAxis(
            IStiAxisLabels labels,
            IStiAxisRange range,
            IStiAxisTitle title,
            IStiAxisTicks ticks,
            IStiAxisInteraction interaction,
            StiArrowStyle arrowStyle,
            StiPenStyle lineStyle,
            Color lineColor,
            float lineWidth,
            bool visible,
            bool startFromZero,
            StiShowXAxis showXAxis,
            bool showEdgeValues,
            bool allowApplyStyle,
            IStiAxisDateTimeStep dateTimeStep)
            :

            base(
            labels,
            range,
            title,
            ticks,
            interaction,
            arrowStyle,
            lineStyle,
            lineColor,
            lineWidth,
            visible,
            startFromZero,
            allowApplyStyle)
        {
            this.showXAxis = showXAxis;
            this.dateTimeStep = dateTimeStep;
            this.showEdgeValues = showEdgeValues;
        }

        public StiXAxis(
            IStiAxisLabels labels,
            IStiAxisRange range,
            IStiAxisTitle title,
            IStiAxisTicks ticks,
            IStiAxisInteraction interaction,
            StiArrowStyle arrowStyle,
            StiPenStyle lineStyle,
            Color lineColor,
            float lineWidth,
            bool visible,
            bool startFromZero,
            StiShowXAxis showXAxis,
            bool showEdgeValues,
            bool allowApplyStyle,
            IStiAxisDateTimeStep dateTimeStep,
            bool logarithmicScale)
            :

            base(
            labels,
            range,
            title,
            ticks,
            interaction,
            arrowStyle,
            lineStyle,
            lineColor,
            lineWidth,
            visible,
            startFromZero,
            allowApplyStyle,
            logarithmicScale)
        {
            this.showXAxis = showXAxis;
            this.dateTimeStep = dateTimeStep;
            this.showEdgeValues = showEdgeValues;
        }
	}
}
