#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Text;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Json.Linq;


namespace Stimulsoft.Report.Chart
{
	[TypeConverter(typeof(StiUniversalConverter))]
	public class StiAxisTicks : 
        IStiAxisTicks,
        ICloneable
	{
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyFloat("LengthUnderLabels", lengthUnderLabels, 5f);
            jObject.AddPropertyFloat("Length", length, 5f);
            jObject.AddPropertyFloat("MinorLength", minorLength, 2f);
            jObject.AddPropertyInt("MinorCount", minorCount, 4);
            jObject.AddPropertyInt("Step", step);
            jObject.AddPropertyBool("MinorVisible", MinorVisible);
            jObject.AddPropertyBool("Visible", Visible, true);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "LengthUnderLabels":
                        this.lengthUnderLabels = property.Value.ToObject<float>();
                        break;

                    case "Length":
                        this.length = property.Value.ToObject<float>();
                        break;

                    case "MinorLength":
                        this.minorLength = property.Value.ToObject<float>();
                        break;

                    case "MinorCount":
                        this.minorCount = property.Value.ToObject<int>();
                        break;

                    case "Step":
                        this.step = property.Value.ToObject<int>();
                        break;

                    case "MinorVisible":
                        this.MinorVisible = property.Value.ToObject<bool>();
                        break;

                    case "Visible":
                        this.visible = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone()
		{	
            return this.MemberwiseClone();
		}
		#endregion	

		#region Properties
        private float lengthUnderLabels = 5f;
        /// <summary>
        /// Gets or sets length of one major tick under labels.
        /// </summary>
        [DefaultValue(5f)]
        [StiSerializable]
        [Description("Gets or sets length of one major tick under labels.")]
        [StiCategory("Common")]
        public float LengthUnderLabels
        {
            get
            {
                return lengthUnderLabels;
            }
            set
            {
                if (value > 0) lengthUnderLabels = value;
            }
        }


		private float length = 5f;
        /// <summary>
        /// Gets or sets length of one major tick.
        /// </summary>
		[DefaultValue(5f)]
		[StiSerializable]
        [Description("Gets or sets length of one major tick.")]
        [StiCategory("Common")]
		public float Length
		{
			get
			{
				return length;
			}
			set
			{
				if (value > 0)length = value;
			}
		}


		private float minorLength = 2f;
        /// <summary>
        /// Gets or sets length of one minor tick.
        /// </summary>
		[DefaultValue(2f)]
		[StiSerializable]
        [Description("Gets or sets length of one minor tick.")]
        [StiCategory("Common")]
		public float MinorLength
		{
			get
			{
				return minorLength;
			}
			set
			{
				if (value > 0)minorLength = value;
			}
		}


		private int minorCount = 4;
        /// <summary>
        /// Gets or sets count of minor ticks between two major ticks.
        /// </summary>
		[DefaultValue(4)]
		[StiSerializable]
        [Description("Gets or sets count of minor ticks between two major ticks.")]
        [StiCategory("Common")]
		public int MinorCount
		{
			get
			{
				return minorCount;
			}
			set
			{
				if (value >= 0)minorCount = value;
			}
		}


		private int step = 0;
        /// <summary>
        /// Gets or sets value which indicates on which steps major ticks will be displayed.
        /// </summary>
		[DefaultValue(0)]
		[StiSerializable]
        [Description("Gets or sets value which indicates on which steps major ticks will be displayed.")]
        [StiCategory("Common")]
		public int Step
		{
			get
			{
				return step;
			}
			set
			{
				if (value >= 0)step = value;
			}
		}


		private bool minorVisible = false;
        /// <summary>
        /// Gets or sets visibility of minor ticks.
        /// </summary>
		[StiSerializable]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of minor ticks.")]
        [StiCategory("Common")]
		public virtual bool MinorVisible
		{
			get
			{
				return minorVisible;
			}
			set
			{
				minorVisible = value;
			}
		}	


		private bool visible = true;
        /// <summary>
        /// Gets or sets visility of major ticks.
        /// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visility of major ticks.")]
        [StiCategory("Common")]
		public virtual bool Visible
		{
			get
			{
				return visible;
			}
			set
			{
				visible = value;
			}
		}
		#endregion

		public StiAxisTicks()
		{
		}

        public StiAxisTicks(
            bool visible,
            float length,
            bool minorVisible,
            float minorLength,
            int minorCount,
            int step
            )
            : this(visible, length, minorVisible, minorLength, minorCount, step, length)
        {
        }

		[StiUniversalConstructor("Ticks")]
		public StiAxisTicks(
			bool visible,
			float length,
			bool minorVisible,			
			float minorLength,
			int minorCount,
			int step,
            float lengthUnderLabels
			)
		{
			this.visible = visible;
			this.length = length;
			this.minorVisible = minorVisible;			
			this.minorLength = minorLength;
			this.minorCount = minorCount;
			this.step = step;
            this.lengthUnderLabels = lengthUnderLabels;
		}
	}
}
