#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{    
	[TypeConverter(typeof(StiUniversalConverter))]
    public class StiAxisLabels : 
        IStiAxisLabels,
        ICloneable
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyBool("AllowApplyStyle", allowApplyStyle, true);
            jObject.AddPropertyStringNullOrEmpty("Format", format);
            jObject.AddPropertyFloat("Angle", angle, 0f);
            jObject.AddPropertyFloat("Width", width, 0f);
            jObject.AddPropertyStringNullOrEmpty("TextBefore", textBefore);
            jObject.AddPropertyStringNullOrEmpty("TextAfter", textAfter);
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.Font(font, "Tahoma", 8));
            jObject.AddPropertyBool("Antialiasing", antialiasing, true);
            jObject.AddPropertyEnum("Placement", placement, StiLabelsPlacement.OneLine);
            jObject.AddPropertyStringNullOrEmpty("Color", StiJsonReportObjectHelper.Serialize.JColor(color, Color.Black));
            jObject.AddPropertyEnum("TextAlignment", TextAlignment, StiMarkerAlignment.Right);
            jObject.AddPropertyFloat("Step", Step, 0f);
            jObject.AddPropertyBool("WordWrap", wordWrap, false);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AllowApplyStyle":
                        this.allowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "Format":
                        this.format = property.Value.ToObject<string>();
                        break;

                    case "Angle":
                        this.angle = property.Value.ToObject<float>();
                        break;

                    case "Width":
                        this.width = property.Value.ToObject<float>();
                        break;

                    case "TextBefore":
                        this.textBefore = property.Value.ToObject<string>();
                        break;

                    case "TextAfter":
                        this.textAfter = property.Value.ToObject<string>();
                        break;

                    case "Font":
                        this.font = StiJsonReportObjectHelper.Deserialize.Font(property, font);
                        break;

                    case "Antialiasing":
                        this.antialiasing = property.Value.ToObject<bool>();
                        break;

                    case "Placement":
                        this.placement = (StiLabelsPlacement)Enum.Parse(typeof(StiLabelsPlacement), property.Value.ToObject<string>());
                        break;

                    case "Color":
                        this.color = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "TextAlignment":
                        this.TextAlignment = (StiHorAlignment)Enum.Parse(typeof(StiHorAlignment), property.Value.ToObject<string>());
                        break;

                    case "Step":
                        this.Step = property.Value.ToObject<float>();
                        break;

                    case "WordWrap":
                        this.wordWrap = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region ICloneable override
        /// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone()
		{
			IStiAxisLabels labels =	this.MemberwiseClone() as IStiAxisLabels;
			labels.Placement =		this.Placement;
			labels.Font =			this.Font.Clone() as Font;

            if (this.core != null)
            {
                labels.Core = this.core.Clone() as StiAxisLabelsCoreXF;
                labels.Core.Labels = labels;
            }
			
			return labels;
		}
		#endregion

		#region Properties
        private StiAxisLabelsCoreXF core;
        [Browsable(false)]
        public StiAxisLabelsCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                }
            }
        }

		private string format = "";
        /// <summary>
        /// Gets os sets format string which is used for formating argument values.
        /// </summary>
		[DefaultValue("")]
		[StiSerializable]
		[Editor("Stimulsoft.Report.Chart.Design.StiFormatEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets os sets format string which is used for formating argument values.")]
        [StiCategory("Common")]
		public string Format
		{
			get
			{
				return format;
			}
			set
			{
				format = value;
			}
		}


		private float angle = 0f;
        /// <summary>
        /// Gets or sets angle of label rotation.
        /// </summary>
		[DefaultValue(0f)]
		[StiSerializable]
        [Description("Gets or sets angle of label rotation.")]
        [StiCategory("Common")]
		public float Angle
		{
			get
			{
				return angle;
			}
			set
			{
				angle = value;
			}
		}


		private float width = 0f;
        /// <summary>
        /// Gets or sets fixed width of axis labels.
        /// </summary>
		[DefaultValue(0f)]
		[StiSerializable]
        [Description("Gets or sets fixed width of axis labels.")]
        [StiCategory("Common")]
		public float Width
		{
			get
			{
				return width;
			}
			set
			{
				width = value;
			}
		}


		private string textBefore = "";
        /// <summary>
        /// Gets or sets string which will be output before argument string representation.
        /// </summary>
		[DefaultValue("")]
		[StiSerializable]
        [Description("Gets or sets string which will be output before argument string representation.")]
        [StiCategory("Common")]
		public string TextBefore
		{
			get
			{
				return textBefore;
			}
			set
			{
				textBefore = value;
			}
		}


		private string textAfter = "";
        /// <summary>
        /// Gets or sets string which will be output after argument string representation.
        /// </summary>
		[DefaultValue("")]
		[StiSerializable]
        [Description("Gets or sets string which will be output after argument string representation.")]
        [StiCategory("Common")]
		public string TextAfter
		{
			get
			{
				return textAfter;
			}
			set
			{
				textAfter = value;
			}
		}


		private Font font = new Font("Tahoma", 8);
        /// <summary>
        /// Gets or sets font which will be used for axis label drawing.
        /// </summary>
		[StiSerializable]
        [Description("Gets or sets font which will be used for axis label drawing.")]
        [StiCategory("Appearance")]
		public Font Font
		{
			get
			{
				return font;
			}
			set
			{
				font = value;
			}
		}


		private bool antialiasing = true;
        /// <summary>
        /// Gets or sets value which control antialiasing drawing mode.
        /// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which control antialiasing drawing mode.")]
        [StiCategory("Appearance")]
		public bool Antialiasing
		{
			get
			{
				return antialiasing;
			}
			set
			{
				antialiasing = value;
			}
		}


		private StiLabelsPlacement placement = StiLabelsPlacement.OneLine;
        /// <summary>
        /// Gets or set mode of labels placement on axis.
        /// </summary>
		[StiSerializable]
		[DefaultValue(StiLabelsPlacement.OneLine)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or set mode of labels placement on axis.")]
        [StiCategory("Common")]
		public StiLabelsPlacement Placement
		{
			get
			{
				return placement;
			}
			set
			{
				placement = value;
			}
		}	


		private Color color = Color.Black;
        /// <summary>
        /// Gets or sets color of labels drawing.
        /// </summary>
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets color of labels drawing.")]
        [StiCategory("Appearance")]
		public Color Color
		{
			get
			{
				return color;
			}
			set
			{
				color = value;
			}
		}


        private StiHorAlignment textAlignment = StiHorAlignment.Right;
        /// <summary>
        /// Gets or sets label text alignment.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StiHorAlignment.Right)]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets label text alignment.")]
        [StiCategory("Common")]
        public virtual StiHorAlignment TextAlignment
        {
            get
            {
                return textAlignment;
            }
            set
            {
                textAlignment = value;
            }
        }

        private float step = 0f;
        /// <summary>
        /// Gets or sets value which indicates with what steps do labels be shown on axis.
        /// </summary>
        [StiSerializable]
        [DefaultValue(0f)]
        [Description("Gets or sets value which indicates with what steps do labels be shown on axis.")]
        [StiCategory("Common")]
        public virtual float Step
        {
            get
            {
                return step;
            }
            set
            {
                step = value;
            }
        }

        /// <summary>
        /// Gets or sets word wrap.
        /// </summary>
        private bool wordWrap = false;        
        [StiSerializable]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets word wrap.")]
        [StiCategory("Common")]
        public bool WordWrap
        {
            get
            {
                return wordWrap;
            }
            set
            {
                wordWrap = value;
            }
        }
		#endregion

        public StiAxisLabels()
		{
            this.core = new StiAxisLabelsCoreXF(this);
		}

		public StiAxisLabels(
			string format,
			string textBefore,
			string textAfter,
			float angle,
			Font font,
			bool antialiasing,
			StiLabelsPlacement placement,
			Color color,
			float width,
            StiHorAlignment textAlignment,
            float step,
            bool allowApplyStyle)
            : this(format, textBefore, textAfter, angle, font, antialiasing, placement, color, width, textAlignment, step, allowApplyStyle, false)
		{

		}

        [StiUniversalConstructor("Labels")]
        public StiAxisLabels(
            string format,
            string textBefore,
            string textAfter,
            float angle,
            Font font,
            bool antialiasing,
            StiLabelsPlacement placement,
            Color color,
            float width,
            StiHorAlignment textAlignment,
            float step,
            bool allowApplyStyle,
            bool wordWrap)
        {
            this.format = format;
            this.textBefore = textBefore;
            this.textAfter = textAfter;
            this.angle = angle;
            this.font = font;
            this.antialiasing = antialiasing;
            this.placement = placement;
            this.color = color;
            this.width = width;
            this.textAlignment = textAlignment;
            this.step = step;
            this.allowApplyStyle = allowApplyStyle;
            this.wordWrap = wordWrap;

            this.core = new StiAxisLabelsCoreXF(this);
        }
	}
}
