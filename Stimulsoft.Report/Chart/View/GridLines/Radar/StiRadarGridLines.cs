#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
	[TypeConverter(typeof(StiUniversalConverter))]
    public abstract class StiRadarGridLines :
        IStiRadarGridLines,
        ICloneable,
        IStiPropertyGridObject
	{
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyBool("AllowApplyStyle", allowApplyStyle, true);
            jObject.AddPropertyStringNullOrEmpty("Color", StiJsonReportObjectHelper.Serialize.JColor(color, Color.Silver));
            jObject.AddPropertyEnum("Style", style, StiPenStyle.Solid);
            jObject.AddPropertyBool("Visible", visible, true);

            if (area != null)
                jObject.AddPropertyBool("Area", true);

            return jObject;
        }

        internal bool needSetAreaJsonPropertyInternal = false;
        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AllowApplyStyle":
                        this.allowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "Color":
                        this.color = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Style":
                        this.style = (StiPenStyle)Enum.Parse(typeof(StiPenStyle), property.Value.ToObject<string>());
                        break;

                    case "Visible":
                        this.visible = property.Value.ToObject<bool>();
                        break;

                    case "Area":
                        this.needSetAreaJsonPropertyInternal = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiRadarGridLines;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.RadarGridLines()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone()
		{
            IStiRadarGridLines gridLines = this.MemberwiseClone() as IStiRadarGridLines;
			gridLines.Style =	    this.Style;

            if (this.Core != null)
            {
                gridLines.Core = this.Core.Clone() as StiRadarGridLinesCoreXF;
                gridLines.Core.GridLines = gridLines;
            }
			
			return gridLines;
		}
		#endregion

		#region Properties
        private StiRadarGridLinesCoreXF core;
        [Browsable(false)]
        public StiRadarGridLinesCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                    if (value && Area != null && Area.Chart != null)
                        this.Core.ApplyStyle(this.Area.Chart.Style);
                }
            }
        }

		private Color color = Color.Silver;
        /// <summary>
        /// Gets or sets color which will be used for drawing major grid lines.
        /// </summary>
		[StiSerializable]
        [StiCategory("Appearance")]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets color which will be used for drawing major grid lines.")]
		public Color Color
		{
			get
			{
				return color;
			}
			set
			{
				color = value;
			}
		}


		private StiPenStyle style = StiPenStyle.Solid;
        /// <summary>
        /// Gets or sets style which will be used for drawing major grid lines.
        /// </summary>
		[StiSerializable]
        [DefaultValue(StiPenStyle.Solid)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets style which will be used for drawing major grid lines.")]
        [StiCategory("Common")]
		public StiPenStyle Style
		{
			get
			{
				return style;
			}
			set
			{
				style = value;
			}
		}


		private bool visible = true;
        /// <summary>
        /// Gets or sets visibility of major grid lines.
        /// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of major grid lines.")]
        [StiCategory("Common")]
		public bool Visible
		{
			get
			{
				return visible;
			}
			set
			{
				visible = value;
			}
		}


        private IStiArea area;
        [StiSerializable(StiSerializationVisibility.Reference)]
        [Browsable(false)]
        public IStiArea Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
            }
        }
        #endregion

        public StiRadarGridLines()
		{
            this.Core = new StiRadarGridLinesCoreXF(this);
		}

        public StiRadarGridLines(
			Color color,
			StiPenStyle style,
			bool visible,
            bool allowApplyStyle
			)
		{
			this.color = color;
			this.style = style;
			this.visible = visible;
            this.allowApplyStyle = allowApplyStyle;

            this.Core = new StiRadarGridLinesCoreXF(this);
		}
	}
}
