#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

namespace Stimulsoft.Report.Chart
{
    
	[TypeConverter(typeof(StiUniversalConverter))]
    public partial class StiChartTitle : 
        IStiChartTitle,
        IStiPropertyGridObject
	{
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyBool("AllowApplyStyle", allowApplyStyle, true);
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.Font(font, "Tahoma", 12f, FontStyle.Bold));
            jObject.AddPropertyStringNullOrEmpty("Text", text);
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(brush));
            jObject.AddPropertyBool("Antialiasing", antialiasing, true);
            jObject.AddPropertyEnum("Alignment", alignment, StringAlignment.Center);
            jObject.AddPropertyEnum("Dock", dock, StiChartTitleDock.Top);
            jObject.AddPropertyInt("Spacing", spacing, 2);
            jObject.AddPropertyBool("Visible", visible);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AllowApplyStyle":
                        this.AllowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "Font":
                        this.font = StiJsonReportObjectHelper.Deserialize.Font(property, this.font);
                        break;

                    case "Text":
                        this.text = property.Value.ToObject<string>();
                        break;

                    case "Brush":
                        this.brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "Antialiasing":
                        this.antialiasing = property.Value.ToObject<bool>();
                        break;

                    case "Alignment":
                        this.alignment = (StringAlignment)Enum.Parse(typeof(StringAlignment), property.Value.ToObject<string>());
                        break;

                    case "Dock":
                        this.dock = (StiChartTitleDock)Enum.Parse(typeof(StiChartTitleDock), property.Value.ToObject<string>());
                        break;

                    case "Spacing":
                        this.spacing = property.Value.ToObject<int>();
                        break;

                    case "Visible":
                        this.visible = property.Value.ToObject<bool>();
                        break;

                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiChartTitle;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.ChartTitle()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

		#region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            IStiChartTitle title =	this.MemberwiseClone() as IStiChartTitle;
            title.Alignment =		this.alignment;
            title.Font =			this.font.Clone() as Font;
			title.Brush =			this.brush.Clone() as StiBrush;

            if (this.core != null)
            {
                title.Core = this.core.Clone() as StiChartTitleCoreXF;
                title.Core.ChartTitle = title;
            }
    			
            return title;
        }
        #endregion
        
        #region Properties
        private StiChartTitleCoreXF core;
        [Browsable(false)]
        public StiChartTitleCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                    if (value && this.Chart != null)
                        this.Core.ApplyStyle(this.Chart.Style);
                }
            }
        }

        private Font font = new Font("Tahoma", 12, FontStyle.Bold);
        /// <summary>
        /// Gets or sets font of the chart title.
        /// </summary>
        [StiSerializable]
        [Description("Gets or sets font of the chart title.")]
        [StiCategory("Appearance")]
        public Font Font
        {
            get
            {
                return font;
            }
            set
            {
                font = value;
            }
        }


        private string text = "";
        /// <summary>
        /// Gets or sets text of the chart title.
        /// </summary>
        [DefaultValue("")]
        [StiSerializable]
        [Description("Gets or sets text of the chart title.")]
        [StiCategory("Common")]
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }

        private StiBrush brush = new StiSolidBrush(Color.SaddleBrown);
        /// <summary>
        /// Gets or sets text brush of the chart title.
        /// </summary>
        [StiSerializable]
        [Description("Gets or sets text brush of the chart title.")]
        [StiCategory("Appearance")]
        public StiBrush Brush
        {
            get 
            {
                return brush;
            }
            set 
            {
                brush = value;
            }
        }
         
        private bool antialiasing = true;
        /// <summary>
        /// Gets or sets value which control antialiasing drawing mode of chart title.
        /// </summary>
        [StiSerializable]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which control antialiasing drawing mode of chart title.")]
        [StiCategory("Appearance")]
        public bool Antialiasing
        {
            get
            {
                return antialiasing;
            }
            set
            {
                antialiasing = value;
            }
        }


        private StringAlignment alignment = StringAlignment.Center;
        /// <summary>
        /// Gets os sets alignment of chart title.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StringAlignment.Center)]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets os sets alignment of chart title.")]
        [StiCategory("Common")]
        public StringAlignment Alignment
        {
            get
            {
                return alignment;
            }
            set
            {
                alignment = value;
            }
        }


        private StiChartTitleDock dock = StiChartTitleDock.Top;
        /// <summary>
        /// Gets or sets docking ot chart title.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StiChartTitleDock.Top)]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets docking ot chart title.")]
        [StiCategory("Common")]
        public StiChartTitleDock Dock
        {
            get
            {
                return dock;
            }
            set
            {
                dock = value;
            }
        }

        private int spacing = 2;
        /// <summary>
        /// Gets or sets spacing between chart title and chart area.
        /// </summary>
        [StiSerializable]
        [DefaultValue(2)]
        [Description("Gets or sets spacing between chart title and chart area.")]
        [StiCategory("Common")]
        public int Spacing
        {
            get
            {
                return spacing;
            }
            set
            {
                spacing = value;
            }
        }

        private bool visible = false;
        /// <summary>
        /// Gets or sets visibility of chart title.
        /// </summary>
        [StiSerializable]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of chart title.")]
        [StiCategory("Common")]
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }

        private IStiChart chart = null;
        [Browsable(false)]
        public IStiChart Chart
        {
            get
            {
                return chart;
            }
            set
            {
                chart = value;
            }
        }
        #endregion

        public StiChartTitle()
        {
            this.core = new StiChartTitleCoreXF(this);
        }


        [StiUniversalConstructor("Title")]
        public StiChartTitle(
            Font font,
            string text,
            StiBrush brush,
            bool antialiasing,
            StringAlignment alignment,
            StiChartTitleDock dock,
            int spacing,
            bool visible,
	        bool allowApplyStyle
            )
        {
            this.core = new StiChartTitleCoreXF(this);

            this.font = font;
            this.text = text;
            this.brush = brush;
            this.antialiasing = antialiasing;
            this.alignment = alignment;
            this.dock = dock;
            this.spacing = spacing;
            this.visible = visible;
            this.allowApplyStyle = allowApplyStyle;            
        }
	}
}