#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

namespace Stimulsoft.Report.Chart
{
	[TypeConverter(typeof(StiUniversalConverter))]
    public abstract class StiInterlacing : 
        IStiInterlacing,
        ICloneable,
        IStiPropertyGridObject
	{
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyBool("AllowApplyStyle", allowApplyStyle, true);
            jObject.AddPropertyStringNullOrEmpty("InterlacedBrush", StiJsonReportObjectHelper.Serialize.JBrush(interlacedBrush));
            jObject.AddPropertyBool("Visible", visible, true);
            if (area != null)
                jObject.AddPropertyBool("Area", true);

            return jObject;
        }

        internal bool needSetAreaJsonPropertyInternal = false;
        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AllowApplyStyle":
                        this.allowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "InterlacedBrush":
                        this.interlacedBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "Visible":
                        this.visible = property.Value.ToObject<bool>();
                        break;

                    case "Area":
                        this.needSetAreaJsonPropertyInternal = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiInterlacing;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.Interlacing()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone()
		{
			IStiInterlacing interlacing =	this.MemberwiseClone() as IStiInterlacing;
			interlacing.InterlacedBrush =	this.InterlacedBrush.Clone() as StiBrush;

            if (this.core != null)
            {
                interlacing.Core = this.Core.Clone() as StiInterlacingCoreXF;
                interlacing.Core.Interlacing = interlacing;
            }
			
			return interlacing;
		}
		#endregion

		#region Properties
        private StiInterlacingCoreXF core;
        [Browsable(false)]
        public StiInterlacingCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                    if (value && this.Area != null && this.Area.Chart != null)
                        this.Core.ApplyStyle(this.Area.Chart.Style);
                }
            }
        }

		private StiBrush interlacedBrush = new StiSolidBrush(Color.Transparent);
        /// <summary>
        /// Gets or sets brush which used for drawing interlaced bars.
        /// </summary>
		[StiSerializable]
        [Description("Gets or sets brush which used for drawing interlacing bar.")]
        [StiCategory("Appearance")]
		public StiBrush InterlacedBrush
		{
			get
			{
				return interlacedBrush;
			}
			set
			{
				interlacedBrush = value;
			}
		}

	
		private bool visible = true;
        /// <summary>
        /// Gets or sets visibility of interlaced bars.
        /// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets visibility of interlaced bars.")]
        [StiCategory("Common")]
		public bool Visible
		{
			get
			{
				return visible;
			}
			set
			{
				visible = value;
			}
		}

        private IStiArea area;
        [StiSerializable(StiSerializationVisibility.Reference)]
        [Browsable(false)]
        public IStiArea Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
            }
        }
		#endregion

        public StiInterlacing()
		{
            this.Core = new StiInterlacingCoreXF(this);
		}

		public StiInterlacing(
			StiBrush interlacedBrush,
			bool visible,
            bool allowApplyStyle
			)
		{
			this.interlacedBrush = interlacedBrush;
			this.visible = visible;
            this.allowApplyStyle = allowApplyStyle;

            this.Core = new StiInterlacingCoreXF(this);
		}
	}
}
