﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Stimulsoft.Report.Chart
{
    public class StiFunnelWeightedSlicesArea : StiFunnelArea
    {
        #region Methods.Types
        public override Type GetDefaultSeriesType()
        {
            return typeof(StiFunnelWeightedSlicesSeries);
        }

        public override Type[] GetSeriesTypes()
        {
            return new Type[]{
                                 typeof(StiFunnelWeightedSlicesSeries)
            };
        }

        public override Type[] GetSeriesLabelsTypes()
        {
            return new Type[]{
                                typeof(StiNoneLabels),
                                typeof(StiCenterFunnelLabels)
                };
        }
        #endregion

            #region Methods.override
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiFunnelWeightedSlicesArea;
            }
        }

        public override StiArea CreateNew()
        {
            return new StiFunnelWeightedSlicesArea();
        }
        #endregion

        [StiUniversalConstructor("Area")]
        public StiFunnelWeightedSlicesArea()
        {
            this.Core = new StiFunnelAreaCoreXF(this);
            this.ColorEach = true;
        }
    }
}
