#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using Stimulsoft.Base;
using Stimulsoft.Report.PropertyGrid;
using System.ComponentModel;


namespace Stimulsoft.Report.Chart
{
    public class StiClusteredColumnArea : 
        StiAxisArea,
        IStiClusteredColumnArea
	{
        #region IStiPropertyGridObject
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiClusteredColumnArea;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.ClusteredColumnArea(),
            };

            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }
        #endregion

        #region Methods.Types
        public override Type GetDefaultSeriesType()
        {
            return typeof(StiClusteredColumnSeries);
        }


        public override Type[] GetSeriesTypes()
        {
            return new Type[]{
								 typeof(StiClusteredColumnSeries), 
								 typeof(StiLineSeries),
								 typeof(StiSteppedLineSeries),
								 typeof(StiSplineSeries),
								 typeof(StiAreaSeries),
								 typeof(StiSteppedAreaSeries),
								 typeof(StiSplineAreaSeries)
			};
        }
        #endregion     
                
        #region Methods.override
        public override StiArea CreateNew()
        {
            return new StiClusteredColumnArea();
        }
        #endregion

		[StiUniversalConstructor("Area")]
		public StiClusteredColumnArea()
		{
            this.Core = new StiClusteredColumnAreaCoreXF(this);
		}
	}
}
