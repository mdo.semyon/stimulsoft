#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Text;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Chart
{
    public class StiGanttArea : 
        StiClusteredBarArea,
        IStiGanttArea
    {
        #region Methods.Types
        public override Type GetDefaultSeriesType()
        {
            return typeof(StiGanttSeries);
        }

        public override Type[] GetSeriesTypes()
        {
            return new Type[]{
                                typeof(StiGanttSeries)
					         };
        }
        public override Type[] GetSeriesLabelsTypes()
        {
            return new Type[]{		
                                typeof(StiNoneLabels),								                                 
                                //typeof(StiLeftAxisLabels),
                                typeof(StiValueAxisLabels),
                                //typeof(StiRightAxisLabels),
                                typeof(StiCenterAxisLabels),
                                typeof(StiOutsideAxisLabels)
                             };
        }
        #endregion

        #region Methods.override
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiGanttArea;
            }
        }

        public override StiArea CreateNew()
        {
            return new StiGanttArea();
        }
        #endregion

        [StiUniversalConstructor("Area")]
        public StiGanttArea()
		{
            this.Core = new StiGanttAreaCoreXF(this);
		}
    }
}