#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Painters;
using Stimulsoft.Base.Json.Linq;


namespace Stimulsoft.Report.Chart
{
    public class StiDoughnutArea : 
        StiPieArea,
        IStiDoughnutArea
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            jObject.RemoveProperty("ColorEach");

            return jObject;
        }
        #endregion

        #region Methods.Types
        public override Type GetDefaultSeriesLabelsType()
        {
            return typeof(StiCenterPieLabels);
        }


        public override Type[] GetSeriesLabelsTypes()
        {
            return new Type[]{		
								 typeof(StiNoneLabels),
								 typeof(StiCenterPieLabels)
			};
        }


        public override Type GetDefaultSeriesType()
        {
            return typeof(StiDoughnutSeries);
        }


        public override Type[] GetSeriesTypes()
        {
            return new Type[]{
								 typeof(StiDoughnutSeries)
							 };
        }
        #endregion       

		#region Properties
		[DefaultValue(true)]
		[StiSerializable]
		[Browsable(true)]
		public override bool ColorEach
		{
			get
			{
				return base.ColorEach;
			}
			set
			{
				base.ColorEach = value;
			}
		}

        [StiNonSerialized]
        [Browsable(false)]
        public override bool ColorEachAllowed
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region Methods.override
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiDoughnutArea;
            }
        }

        public override StiArea CreateNew()
        {
            return new StiDoughnutArea();
        }
        #endregion

		[StiUniversalConstructor("Area")]
		public StiDoughnutArea()
		{
            this.Core = new StiDoughnutAreaCoreXF(this);
			this.ColorEach = true;            
		}
	}
}
