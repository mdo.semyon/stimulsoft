#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Linq;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{	
	/// <summary>
	/// Describes base class for all chart areas.
	/// </summary>
    [StiServiceBitmap(typeof(StiArea), "Stimulsoft.Report.Images.Components.StiChart.png")]
    [StiServiceCategoryBitmap(typeof(StiArea), "Stimulsoft.Report.Images.Components.StiChart.png")]
	[TypeConverter(typeof(StiUniversalConverter))]
    public abstract class StiArea : 
		StiService,
        IStiArea,
		IStiSerializeToCodeAsClass,
        IStiPropertyGridObject
	{
        #region IStiJsonReportObject.override
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyIdent("Ident", this.GetType().Name);

            jObject.AddPropertyBool("AllowApplyStyle", allowApplyStyle, true);
            jObject.AddPropertyBool("ColorEach", ColorEach);
            jObject.AddPropertyBool("ShowShadow", showShadow);
            jObject.AddPropertyStringNullOrEmpty("BorderColor", StiJsonReportObjectHelper.Serialize.JColor(borderColor, Color.Gray));
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(brush));
            
            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AllowApplyStyle":
                        this.allowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "ColorEach":
                        this.ColorEach = property.Value.ToObject<bool>();
                        break;

                    case "ShowShadow":
                        this.showShadow = property.Value.ToObject<bool>();
                        break;

                    case "BorderColor":
                        this.borderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Brush":
                        this.brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;
                }
            }
        }

        internal static IStiArea CreateFromJsonObject(JObject jObject)
        {
            var ident = jObject.Properties().FirstOrDefault(x => x.Name == "Ident").Value.ToObject<string>();
            var service = StiOptions.Services.ChartAreas.FirstOrDefault(x => x.GetType().Name == ident);

            if (service == null)
                throw new Exception($"Type {ident} is not found!");

            var area = service.CreateNew();
            area.LoadFromJsonObject(jObject);

            return area;
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
	    public abstract StiComponentId ComponentId
	    {
	        get;
	    }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

	    public abstract StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level);

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public override object Clone()
        {
            IStiArea area = base.Clone() as IStiArea;
            area.Brush = this.Brush.Clone() as StiBrush;

            if (this.core != null)
            {
                area.Core = this.Core.Clone() as StiAreaCoreXF;
                area.Core.Area = area;
            }

            return area;
        }
        #endregion

        #region Methods
        public abstract StiArea CreateNew();

        public override string ToString()
        {
            return ServiceName;
        }
        #endregion

        #region Methods.Types
        public abstract Type GetDefaultSeriesType();
        public abstract Type[] GetSeriesTypes();
        public abstract Type GetDefaultSeriesLabelsType();
        public abstract Type[] GetSeriesLabelsTypes();
        #endregion

		#region StiService override
        /// <summary>
        /// Gets a service name.
        /// </summary>
        public override string ServiceName
        {
            get
            {
                return Core.LocalizedName;
            }
        }

		/// <summary>
		/// Gets a service category.
		/// </summary>
		[Browsable(false)]
		public sealed override string ServiceCategory
		{
			get
			{
				return "Chart";
			}
		}

		/// <summary>
		/// Gets a service type.
		/// </summary>
		[Browsable(false)]
		public sealed override Type ServiceType
		{
			get
			{
				return typeof(StiArea);
			}
		}
		#endregion        

		#region Properties
        [Browsable(false)]
        public bool IsDefaultSeriesTypeFullStackedColumnSeries
        {
            get
            {
                return GetDefaultSeriesType() == typeof(StiFullStackedColumnSeries);
            }
        }

        
        [Browsable(false)]
        public bool IsDefaultSeriesTypeFullStackedBarSeries
        {
            get
            {
                return GetDefaultSeriesType() == typeof(StiFullStackedBarSeries);
            }
        }


        private StiAreaCoreXF core;
        [Browsable(false)]
        public StiAreaCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }


        private IStiChart chart = null;
        /// <summary>
        /// Gets or sets reference to chart component which contain this area.
        /// </summary>
        [Browsable(false)]
        [StiSerializable(StiSerializationVisibility.Reference)]
        public virtual IStiChart Chart
        {
            get
            {
                return chart;
            }
            set
            {
                chart = value;
            }
        }


        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                    if (value && Chart != null)
                        this.core.ApplyStyle(this.Chart.Style);
                }
            }
        }


		private bool colorEach = false;
		/// <summary>
		/// Gets or sets value which indicates that each series is drawn by its own colour.
		/// </summary>
		[StiSerializable]
        [StiCategory("Common")]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value which indicates that each series is drawn by its own colour.")]
		public virtual bool ColorEach
		{
			get
			{
				return colorEach;
			}
			set
			{
				colorEach = value;
			}
		}

        [StiNonSerialized]
        [Browsable(false)]
        public virtual bool ColorEachAllowed
        {
            get
            {
                return true;
            }
        }


		private bool showShadow = false;
		/// <summary>
		/// Gets or sets value which indicates necessary draw shadod or no.
		/// </summary>
		[DefaultValue(false)]
        [StiCategory("Appearance")]
        [StiSerializable]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value which indicates necessary draw shadod or no.")]
		public bool ShowShadow
		{
			get
			{
				return showShadow;
			}
			set
			{
				showShadow = value;
			}
		}


		private Color borderColor = Color.Gray;
		/// <summary>
		/// Gets or sets border color of this area.
		/// </summary>
		[StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[Description("Gets or sets border color of this area.")]
		public Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
			}
		}


		private StiBrush brush = new StiSolidBrush(Color.White);
		/// <summary>
		/// Gets or sets brush to fill a area.
		/// </summary>
        [StiCategory("Appearance")]
		[StiSerializable]
		[Description("Gets or sets a brush to fill a area.")]
		public StiBrush Brush
		{
			get 
			{
				return brush;
			}
			set 
			{
				brush = value;
			}
		}
		#endregion	
	}
}
