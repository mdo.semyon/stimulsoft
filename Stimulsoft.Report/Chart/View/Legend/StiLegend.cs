#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Chart
{
	[TypeConverter(typeof(StiUniversalConverter))]
    public class StiLegend : 
        IStiLegend,
		IStiSerializeToCodeAsClass,
		ICloneable,
        IStiPropertyGridObject
	{
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyBool("AllowApplyStyle", allowApplyStyle, true);
            jObject.AddPropertyBool("HideSeriesWithEmptyTitle", hideSeriesWithEmptyTitle);
            jObject.AddPropertyBool("ShowShadow", showShadow, true);
            jObject.AddPropertyStringNullOrEmpty("BorderColor", StiJsonReportObjectHelper.Serialize.JColor(borderColor, Color.Gray));
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(brush));
            jObject.AddPropertyStringNullOrEmpty("TitleColor", StiJsonReportObjectHelper.Serialize.JColor(titleColor, Color.Gray));
            jObject.AddPropertyStringNullOrEmpty("LabelsColor", StiJsonReportObjectHelper.Serialize.JColor(labelsColor, Color.Gray));
            jObject.AddPropertyEnum("Direction", Direction, StiLegendDirection.TopToBottom);
            jObject.AddPropertyEnum("HorAlignment", HorAlignment, StiLegendHorAlignment.Left);
            jObject.AddPropertyEnum("VertAlignment", VertAlignment, StiLegendVertAlignment.Top);
            jObject.AddPropertyStringNullOrEmpty("TitleFont", StiJsonReportObjectHelper.Serialize.Font(titleFont, "Arial", 14, FontStyle.Bold));
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.Font(font, "Arial", 8));
            jObject.AddPropertyBool("Visible", visible, true);
            jObject.AddPropertyBool("MarkerVisible", markerVisible, true);
            jObject.AddPropertyBool("MarkerBorder", markerBorder, true);
            jObject.AddPropertyJObject("MarkerSize", StiJsonReportObjectHelper.Serialize.Size(markerSize));
            jObject.AddPropertyEnum("MarkerAlignment", markerAlignment, StiMarkerAlignment.Left);
            jObject.AddPropertyInt("Columns", columns, 2);
            jObject.AddPropertyInt("HorSpacing", horSpacing, 4);
            jObject.AddPropertyInt("VertSpacing", vertSpacing, 2);
            jObject.AddPropertyStringNullOrEmpty("Size", StiJsonReportObjectHelper.Serialize.SizeD(size));
            jObject.AddPropertyStringNullOrEmpty("Title", title);

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "AllowApplyStyle":
                        this.AllowApplyStyle = property.Value.ToObject<bool>();
                        break;

                    case "HideSeriesWithEmptyTitle":
                        this.hideSeriesWithEmptyTitle = property.Value.ToObject<bool>();
                        break;

                    case "ShowShadow":
                        this.showShadow = property.Value.ToObject<bool>();
                        break;

                    case "BorderColor":
                        this.borderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Brush":
                        this.brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "TitleColor":
                        this.titleColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "LabelsColor":
                        this.labelsColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Direction":
                        this.Direction = (StiLegendDirection)Enum.Parse(typeof(StiLegendDirection), property.Value.ToObject<string>());
                        break;

                    case "HorAlignment":
                        this.HorAlignment = (StiLegendHorAlignment)Enum.Parse(typeof(StiLegendHorAlignment), property.Value.ToObject<string>());
                        break;

                    case "VertAlignment":
                        this.VertAlignment = (StiLegendVertAlignment)Enum.Parse(typeof(StiLegendVertAlignment), property.Value.ToObject<string>());
                        break;

                    case "TitleFont":
                        this.titleFont = StiJsonReportObjectHelper.Deserialize.Font(property, this.font);
                        break;

                    case "Font":
                        this.font = StiJsonReportObjectHelper.Deserialize.Font(property, this.font);
                        break;

                    case "Visible":
                        this.visible = property.Value.ToObject<bool>();
                        break;

                    case "MarkerVisible":
                        this.markerVisible = property.Value.ToObject<bool>();
                        break;

                    case "MarkerBorder":
                        this.markerBorder = property.Value.ToObject<bool>();
                        break;

                    case "MarkerSize":
                        this.markerSize = StiJsonReportObjectHelper.Deserialize.Size((JObject)property.Value);
                        break;

                    case "MarkerAlignment":
                        this.MarkerAlignment = (StiMarkerAlignment)Enum.Parse(typeof(StiMarkerAlignment), property.Value.ToObject<string>());
                        break;

                    case "Columns":
                        this.columns = property.Value.ToObject<int>();
                        break;

                    case "HorSpacing":
                        this.horSpacing = property.Value.ToObject<int>();
                        break;

                    case "VertSpacing":
                        this.vertSpacing = property.Value.ToObject<int>();
                        break;

                    case "Size":
                        this.size = StiJsonReportObjectHelper.Deserialize.SizeD(property);
                        break;

                    case "Title":
                        this.title = property.Value.ToObject<string>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiLegend;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[] 
            {
                propHelper.Legend()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion
        
        #region ICloneable
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            IStiLegend legend = this.MemberwiseClone() as IStiLegend;
            legend.Brush = this.Brush.Clone() as StiBrush;
            legend.Font = this.Font.Clone() as Font;
            legend.TitleFont = this.TitleFont.Clone() as Font;
            legend.Direction = this.Direction;
            legend.HorAlignment = this.HorAlignment;
            legend.VertAlignment = this.VertAlignment;
            legend.MarkerAlignment = this.MarkerAlignment;

            if (this.Core != null)
            {
                legend.Core = this.Core.Clone() as StiLegendCoreXF;
                legend.Core.Legend = legend;
            }

            return legend;
        }
        #endregion
                
        #region Properties
        private StiLegendCoreXF core;
        [Browsable(false)]
        public StiLegendCoreXF Core
        {
            get
            {
                return core;
            }
            set
            {
                core = value;
            }
        }

        private bool allowApplyStyle = true;
        /// <summary>
        /// Gets or sets value which indicates that chart style will be used.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates that chart style will be used.")]
        [DefaultValue(true)]
        public bool AllowApplyStyle
        {
            get
            {
                return allowApplyStyle;
            }
            set
            {
                if (allowApplyStyle != value)
                {
                    allowApplyStyle = value;
                    if (value && Chart != null)
                        this.Core.ApplyStyle(this.Chart.Style);
                }
            }
        }

        private IStiChart chart = null;
        [StiSerializable(StiSerializationVisibility.Reference)]
        [Browsable(false)]
        public IStiChart Chart
        {
            get
            {
                return chart;
            }
            set
            {
                chart = value;
            }
        }		


        private bool hideSeriesWithEmptyTitle = false;
        /// <summary>
        /// Gets or sets value which shows/hides series with empty title.
        /// </summary>
        [DefaultValue(false)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets value which shows/hides series with empty title.")]
        public bool HideSeriesWithEmptyTitle
        {
            get
            {
                return hideSeriesWithEmptyTitle;
            }
            set
            {
                hideSeriesWithEmptyTitle = value;
            }
        }


		private bool showShadow = true;
        /// <summary>
        /// Gets or sets value which indicates draw shadow or no.
        /// </summary>
		[DefaultValue(true)]
		[StiSerializable]
		[TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Appearance")]
        [Description("Gets or sets value which indicates draw shadow or no.")]
		public bool ShowShadow
		{
			get
			{
				return showShadow;
			}
			set
			{
				showShadow = value;
			}
		}


		private Color borderColor = Color.Gray;
        /// <summary>
        /// Gets or sets border color.
        /// </summary>
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Appearance")]
        [Description("Gets or sets border color.")]
		public Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
			}
		}


		private StiBrush brush = new StiSolidBrush(Color.White);
        /// <summary>
        /// Gets or sets background brush of legend.
        /// </summary>
        [StiCategory("Appearance")]
		[StiSerializable]
        [Description("Gets or sets background brush of legend.")]
		public StiBrush Brush
		{
			get 
			{
				return brush;
			}
			set 
			{
				brush = value;
			}
		}


		private Color titleColor = Color.Gray;
        /// <summary>
        /// Gets or sets title color of legend.
        /// </summary>
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Appearance")]
        [Description("Gets or sets title color of legend.")]
		public Color TitleColor
		{
			get
			{
				return titleColor;
			}
			set
			{
				titleColor = value;
			}
		}


		private Color labelsColor = Color.Gray;
        /// <summary>
        /// Gets or sets color of the labels.
        /// </summary>
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Appearance")]
        [Description("Gets or sets color of the labels.")]
		public Color LabelsColor
		{
			get
			{
				return labelsColor;
			}
			set
			{
				labelsColor = value;
			}
		}

	
		private StiLegendDirection direction = StiLegendDirection.TopToBottom;
        /// <summary>
        /// Gets or sets direction which used for series drawing in legend.
        /// </summary>
		[StiSerializable]
		[DefaultValue(StiLegendDirection.TopToBottom)]
        [StiCategory("Common")]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets direction which used for series drawing in legend.")]
		public virtual StiLegendDirection Direction
		{
			get 
			{
				return direction;
			}
			set 
			{
				direction = value;
			}
		}	
		

		private StiLegendHorAlignment horAlignment = StiLegendHorAlignment.Left;
		/// <summary>
		/// Gets or sets horizontal alignment of legend placement.
		/// </summary>
		[StiSerializable]
		[DefaultValue(StiLegendHorAlignment.Left)]
        [StiCategory("Common")]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets horizontal alignment of legend placement.")]
		public virtual StiLegendHorAlignment HorAlignment
		{
			get 
			{
				return horAlignment;
			}
			set 
			{
				horAlignment = value;
			}
		}


		private StiLegendVertAlignment vertAlignment = StiLegendVertAlignment.Top;
		/// <summary>
        /// Gets or sets vertical alignment of legend placement.
		/// </summary>
		[StiSerializable]
		[DefaultValue(StiLegendVertAlignment.Top)]
        [StiCategory("Common")]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets vertical alignment of legend placement.")]
		public virtual StiLegendVertAlignment VertAlignment
		{
			get
			{
				return vertAlignment;
			}
			set
			{
				vertAlignment = value;
			}
		}


		private Font titleFont = new Font("Arial", 14, FontStyle.Bold);
        /// <summary>
        /// Gets or sets title font of the chart legend.
        /// </summary>
		[StiSerializable]
        [StiCategory("Appearance")]
        [Description("Gets or sets title font of the chart legend.")]
		public Font TitleFont
		{
			get
			{
				return titleFont;
			}
			set
			{
				titleFont = value;
			}
		}


		private Font font = new Font("Arial", 8);
        /// <summary>
        /// Gets or sets font which used for series title drawing in chart legend.
        /// </summary>
		[StiSerializable]
        [StiCategory("Appearance")]
        [Description("Gets or sets font which used for series title drawing in chart legend.")]
		public Font Font
		{
			get
			{
				return font;
			}
			set
			{
				font = value;
			}
		}


		private bool visible = true;
        /// <summary>
        /// Gets or sets visibility of chart legend.
        /// </summary>
		[DefaultValue(true)]
		[StiSerializable]
		[TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets visibility of chart legend.")]
		public bool Visible
		{
			get
			{
				return visible;
			}
			set
			{
				visible = value;
			}
		}


		private bool markerVisible = true;
        /// <summary>
        /// Gets or sets visibility of markers.
        /// </summary>
		[DefaultValue(true)]
		[StiSerializable]
		[TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets visibility of markers.")]
		public bool MarkerVisible
		{
			get
			{
				return markerVisible;
			}
			set
			{
				markerVisible = value;
			}
		}


        private bool markerBorder = true;
        /// <summary>
        /// Gets or sets show a border marker.
        /// </summary>
        [DefaultValue(true)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("Common")]
        [Description("Gets or sets show a border marker.")]
        public bool MarkerBorder
        {
            get
            {
                return markerBorder;
            }
            set
            {
                markerBorder = value;
            }
        }


		private Size markerSize = new Size(10, 10);
        /// <summary>
        /// Gets or sets marker size.
        /// </summary>
		[StiSerializable]
        [StiCategory("Common")]
        [Description("Gets or sets marker size.")]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiSizeConverter))]
		public Size MarkerSize
		{
			get
			{
				return markerSize;
			}
			set
			{
				markerSize = value;
			}
		}


		private StiMarkerAlignment markerAlignment = StiMarkerAlignment.Left;
        /// <summary>
        /// Gets or sets alignment of markers related to series title.
        /// </summary>
		[StiSerializable]
		[DefaultValue(StiMarkerAlignment.Left)]
        [StiCategory("Common")]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets alignment of markers related to series title.")]
		public virtual StiMarkerAlignment MarkerAlignment
		{
			get 
			{
				return markerAlignment;
			}
			set 
			{
				markerAlignment = value;
			}
		}


		private int columns = 0;
        /// <summary>
        /// Gets or sets amount of columns.
        /// </summary>
		[StiSerializable]
		[DefaultValue(0)]
        [StiCategory("Common")]
        [Description("Gets or sets amount of columns.")]
        [Editor("Stimulsoft.Report.Design.Components.StiLegendColumnsEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        public int Columns
		{
			get
			{
				return columns;
			}
			set
			{
				if (value >= 0)
				{
					columns = value;
				}
			}
		}


		private int horSpacing = 4;
        /// <summary>
        /// Gets or sets horizontal spacing between items in legend.
        /// </summary>
		[StiSerializable]
		[DefaultValue(4)]
        [StiCategory("Common")]
        [Description("Gets or sets horizontal spacing between items in legend.")]
		public int HorSpacing
		{
			get
			{
				return horSpacing;
			}
			set
			{
				horSpacing = value;
			}
		}


		private int vertSpacing = 2;
        /// <summary>
        /// Gets or sets vertical spacing between items in legend.
        /// </summary>
		[StiSerializable]
		[DefaultValue(2)]
        [StiCategory("Common")]
        [Description("Gets or sets vertical spacing between items in legend.")]
		public int VertSpacing
		{
			get
			{
				return vertSpacing;
			}
			set
			{
				vertSpacing = value;
			}
		}


		private SizeD size;
        /// <summary>
        /// Gets or sets size of legend.
        /// </summary>
		[StiSerializable]
        [StiCategory("Common")]
        [Description("Gets or sets size of legend.")]
		public SizeD Size
		{
			get
			{
				return size;
			}
			set
			{
				size = value;
			}
		}

		private string title = "";
        /// <summary>
        /// Gets or sets title of the legend.
        /// </summary>
		[DefaultValue("")]
		[StiSerializable]
        [StiCategory("Common")]
        [Description("Gets or sets title of the legend.")]
		public string Title
		{
			get
			{
				return title;
			}
			set
			{
				title = value;
			}
		}
	    #endregion

		[StiUniversalConstructor("Legend")]
		public StiLegend()
		{
            this.Core = new StiLegendCoreXF(this);
		}
	}
}
