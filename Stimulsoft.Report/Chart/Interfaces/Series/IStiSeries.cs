#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Chart
{
    public interface IStiSeries :
        ICloneable,
        IStiJsonReportObject
    {
        #region Properties
        StiSeriesCoreXF Core
        {
            get;
            set;
        }
        
        bool AllowApplyStyle
        {
            get;
            set;
        }

        IStiTrendLine TrendLine
        {
            get;
            set;
        }

        string Format
        {
            get;
            set;
        }


        string CoreTitle
        {
            get;
            set;
        }


        StiSeriesSortType SortBy
        {
            get;
            set;
        }


        StiSeriesSortDirection SortDirection
        {
            get;
            set;
        }


        bool ShowInLegend
        {
            get;
            set;
        }



        StiShowSeriesLabels ShowSeriesLabels
        {
            get;
            set;
        }



        bool ShowShadow
        {
            get;
            set;
        }


        StiChartFiltersCollection Filters
        {
            get;
            set;
        }


        IStiSeriesTopN TopN
        {
            get;
            set;
        }


        StiChartConditionsCollection Conditions
        {
            get;
            set;
        }


        StiSeriesYAxis YAxis
        {
            get;
            set;
        }


        IStiSeriesLabels SeriesLabels
        {
            get;
            set;
        }


        IStiChart Chart
        {
            get;
            set;
        }
        #endregion

        #region Properties.Data

        #region Values Start
        double?[] ValuesStart
        {
            get;
        }
        #endregion

        #region Values
        double?[] Values
        {
            get;
            set;
        }
        #endregion

        #region Arguments
        object[] Arguments
        {
            get;
            set;
        }
        #endregion

        #region ToolTips
        string[] ToolTips
        {
            get;
            set;
        }
        #endregion

        #region Tags
        object[] Tags
        {
            get;
            set;
        }
        #endregion

        #region Hyperlinks
        string[] Hyperlinks
        {
            get;
            set;
        }
        #endregion
        #endregion

        #region Interaction
        IStiSeriesInteraction Interaction
        {
            get;
            set;
        }
        #endregion

        #region Methods
        Color ProcessSeriesColors(int pointIndex, Color seriesColor);

        StiBrush ProcessSeriesBrushes(int pointIndex, StiBrush seriesBrush);
        #endregion

        #region Methods.Types
        Type GetDefaultAreaType();
        #endregion
    }
}