﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Windows.Forms;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report
{
    public partial class StiSavePasswordForm : Form
    {
        public StiSavePasswordForm()
        {
            InitializeComponent();
            Localize();
            tbPassword.Text = string.Empty;
            tbPassword.Focus();
        }

        #region Properties
        public string Password { get; private set; }
        #endregion

        #region Method.Localize
        private void Localize()
        {
            lbPassword.Text = StiLocalization.Get("Password", "lbPasswordSave");
            this.Text = StiLocalization.Get("Password", "StiSavePasswordForm");
            gbPassword.Text = StiLocalization.Get("Password", "gbPassword");
            btOk.Text = StiLocalization.Get("Buttons", "Ok");
            btCancel.Text = StiLocalization.Get("Buttons", "Cancel");
        }
        #endregion

        #region Handlers
        private void OnOkClick(object sender, EventArgs e)
        {
            if (tbPassword.Text.Length == 0)
            {
                tbPassword.Width = 219;
                errorProvider.SetError(tbPassword,
                    string.Format(
                    StiLocalization.Get("Password", "PasswordNotEntered"),
                    StiLocalization.Get("PropertyMain", "Alias")));
                tbPassword.Focus();
                return;
            }

            DialogResult = DialogResult.OK;
            Password = tbPassword.Text;
            this.Close();
        }

        private void OnFormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.Handled = true;
                Close();
            }
        }

        private void OnCancelClick(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
