﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing.Imaging;
using System.Globalization;
using System.Text;
using System.Threading;
using Stimulsoft.Base;
using Stimulsoft.Report.Export;

namespace Stimulsoft.Report.WCFService
{
    public static class StiExportSettingsHelper
    {
        #region static Fields
        private static int[,] codePageCodes =
        {
            {0x00,  0},
            {0x01,  437},
            {0x69,  620},
            {0x6A,  737},
            {0x02,  850},
            {0x64,  852},
            {0x67,  861},
            {0x66,  865},
            {0x65,  866},
            {0x68,  895},
            {0x6B,  857},
            {0xC8,  1250},
            {0xC9,  1251},
            {0x03,  1252},
            {0xCB,  1253},
            {0xCA,  1254},
            {0x04,  10000},
            {0x98,  10006},
            {0x96,  10007},
            {0x97,  10029}
        };
        #endregion

        #region Methods
        public static string GetCsvExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();

            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Csv);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("Encoding", (form["Encoding"] as Encoding).CodePage);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("Separator", form["Separator"] as string);
            writer.WriteStartElementAndContent("SkipColumnHeaders", (bool)form["SkipColumnHeaders"]);
            writer.WriteStartElementAndContent("DataExportMode", (int)form["DataExportMode"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetDbfExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            var codePage = (StiDbfCodePages)Enum.Parse(typeof(StiDbfCodePages), codePageCodes[(int)form["EncodingSelectedIndex"], 1].ToString(), false);

            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Dbf);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("CodePage", (int)codePage);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetDifExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;

            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Dif);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("Encoding", (form["Encoding"] as Encoding).CodePage);
            writer.WriteStartElementAndContent("ExportDataOnly", (bool)form["ExportDataOnly"]);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("UseDefaultSystemEncoding", (bool)form["UseDefaultSystemEncoding"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetExcelExportSettings(IStiFormRunner form, StiReport report, StiSLExportType format)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;

            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)format);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("ExportDataOnly", (bool)form["ExportDataOnly"]);
            writer.WriteStartElementAndContent("ExportEachPageToSheet", (bool)form["ExportEachPageToSheet"]);
            writer.WriteStartElementAndContent("ExportObjectFormatting", (bool)form["ExportObjectFormatting"]);
            writer.WriteStartElementAndContent("ExportPageBreaks", (bool)form["ExportPageBreaks"]);
            writer.WriteStartElementAndContent("ImageQuality", (float)form["ImageQuality"]);
            writer.WriteStartElementAndContent("ImageResolution", (float)form["Resolution"]);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("UseOnePageHeaderAndFooter", (bool)form["UseOnePageHeaderAndFooter"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetExcelXmlExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.ExcelXml);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetHtmlExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Html);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("AddPageBreaks", (bool)form["AddPageBreaks"]);
            writer.WriteStartElementAndContent("ExportMode", (int)form["ExportMode"]);
            writer.WriteStartElementAndContent("ExportQuality", (int)StiHtmlExportQuality.High);
            writer.WriteStartElementAndContent("ImageFormat", ((ImageFormat)form["ImageFormat"]).Guid.ToString());
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("Zoom", (float)form["Zoom"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetHtml5ExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;

            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Html5);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("ImageQuality", (float)form["ImageQuality"]);
            writer.WriteStartElementAndContent("ImageResolution", (float)form["Resolution"]);
            writer.WriteStartElementAndContent("ImageFormat", ((ImageFormat)form["ImageFormat"]).Guid.ToString());
            writer.WriteStartElementAndContent("ContinuousPages", (bool)form["ContinuousPages"]);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetMhtExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;

            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Mht);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("AddPageBreaks", (bool)form["AddPageBreaks"]);
            writer.WriteStartElementAndContent("Encoding", Encoding.UTF8.CodePage);
            writer.WriteStartElementAndContent("ExportMode", (int)form["ExportMode"]);
            writer.WriteStartElementAndContent("ExportQuality", (int)StiHtmlExportQuality.High);
            writer.WriteStartElementAndContent("ImageFormat", ((ImageFormat)form["ImageFormat"]).Guid.ToString());
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("Zoom", (float)form["Zoom"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetImageExportSettings(IStiFormRunner form, StiReport report, StiSLExportType format)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)format);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("CutEdges", (bool)form["CutEdges"]);
            writer.WriteStartElementAndContent("DitheringType", (int)form["MonochromeDitheringType"]);
            writer.WriteStartElementAndContent("ImageFormat", (int)form["ImageFormat"]);
            writer.WriteStartElementAndContent("ImageResolution", (int)form["ImageResolution"]);
            writer.WriteStartElementAndContent("ImageZoom", (double)form["ImageZoom"]);
            writer.WriteStartElementAndContent("MultipleFiles", (bool)form["MultipleFiles"]);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("TiffCompressionScheme", (int)form["TiffCompressionScheme"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetOdsExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Ods);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("ImageQuality", (float)form["ImageQuality"]);
            writer.WriteStartElementAndContent("ImageResolution", (float)form["Resolution"]);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetOdtExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Odt);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("ImageQuality", (float)form["ImageQuality"]);
            writer.WriteStartElementAndContent("ImageResolution", (float)form["Resolution"]);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("UsePageHeadersAndFooters", (bool)form["UsePageHeadersAndFooters"]);
            writer.WriteStartElementAndContent("RemoveEmptySpaceAtBottom", (bool)form["RemoveEmptySpaceAtBottom"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetPdfExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Pdf);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("ImageQuality", (float)form["ImageQuality"]);
            writer.WriteStartElementAndContent("ImageCompressionMethod", (int)form["ImageCompressionMethod"]);
            writer.WriteStartElementAndContent("ImageResolution", (float)form["Resolution"]);
            writer.WriteStartElementAndContent("EmbeddedFonts", (bool)form["EmbeddedFonts"] & (bool)form["EmbeddedFontsEnabled"]);
            writer.WriteStartElementAndContent("ExportRtfTextAsImage", (bool)form["ExportRtfTextAsImage"]);
            writer.WriteStartElementAndContent("PasswordInputUser", form["UserPassword"] as string);
            writer.WriteStartElementAndContent("PasswordInputOwner", form["OwnerPassword"] as string);
            writer.WriteStartElementAndContent("UserAccessPrivileges", (int)form["UserAccessPrivileges"]);
            writer.WriteStartElementAndContent("KeyLength", (int)form["EncryptionKeyLength"]);
            writer.WriteStartElementAndContent("GetCertificateFromCryptoUI", (bool)form["GetCertificateFromCryptoUI"]);
            writer.WriteStartElementAndContent("UseDigitalSignature", (bool)form["UseDigitalSignature"]);
            writer.WriteStartElementAndContent("SubjectNameString", form["SubjectNameString"] as string);
            writer.WriteStartElementAndContent("PdfACompliance", (bool)form["PdfACompliance"]);
            writer.WriteStartElementAndContent("ImageFormat", (int)form["ImageFormat"]);
            writer.WriteStartElementAndContent("DitheringType", (int)form["MonochromeDitheringType"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetRtfExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Rtf);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("ExportMode", (int)form["ExportMode"]);
            writer.WriteStartElementAndContent("ImageQuality", (float)form["ImageQuality"]);
            writer.WriteStartElementAndContent("ImageResolution", (float)form["Resolution"]);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("RemoveEmptySpaceAtBottom", (bool)form["RemoveEmptySpaceAtBottom"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetSylkExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Sylk);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("Encoding", (form["Encoding"] as Encoding).CodePage);
            writer.WriteStartElementAndContent("ExportDataOnly", (bool)form["ExportDataOnly"]);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("UseDefaultSystemEncoding", (bool)form["UseDefaultSystemEncoding"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetTextExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Text);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("BorderType", (int)form["BorderType"]);
            writer.WriteStartElementAndContent("CutLongLines", (bool)form["CutLongLines"]);
            writer.WriteStartElementAndContent("Encoding", (form["Encoding"] as Encoding).CodePage);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("ZoomX", (float)form["ZoomX"]);
            writer.WriteStartElementAndContent("ZoomY", (float)form["ZoomY"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetWord2007ExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Word2007);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteStartElementAndContent("UsePageHeadersAndFooters", (bool)form["UsePageHeadersAndFooters"]);
            writer.WriteStartElementAndContent("ImageQuality", (float)form["ImageQuality"]);
            writer.WriteStartElementAndContent("ImageResolution", (float)form["Resolution"]);
            writer.WriteStartElementAndContent("RemoveEmptySpaceAtBottom", (bool)form["RemoveEmptySpaceAtBottom"]);
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetXpsExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Xps);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("ExportRtfTextAsImage", (bool)form["ExportRtfTextAsImage"]);
            writer.WriteStartElementAndContent("ImageQuality", (float)form["ImageQuality"]);
            writer.WriteStartElementAndContent("ImageResolution", (float)form["Resolution"]);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetPpt2007ExportSettings(IStiFormRunner form, StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var pagesRange = form["PagesRange"] as StiPagesRange;
            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Ppt2007);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElement("settings");
            writer.WriteStartElementAndContent("ImageQuality", (float)form["ImageQuality"]);
            writer.WriteStartElementAndContent("ImageResolution", (float)form["Resolution"]);
            writer.WriteStartElement("PageRange");
            writer.WriteStartElementAndContent("CurrentPage", pagesRange.CurrentPage);
            writer.WriteStartElementAndContent("PageRanges", pagesRange.PageRanges);
            writer.WriteStartElementAndContent("RangeType", (int)pagesRange.RangeType);
            writer.WriteEndElement();
            writer.WriteEndElement();

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }

        public static string GetXmlExportSettings(StiReport report)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            string reportStr = report.SaveDocumentToString();
            var writer = new StiXmlWriter();
            writer.WriteInt((int)StiSLExportType.Xml);
            writer.WriteString(",");
            writer.WriteInt(reportStr.Length);
            writer.WriteString(",");
            writer.WriteString(reportStr);

            writer.WriteStartElementAndEmptyContent("settings");

            writer.IsEncodeString = true;
            string result = writer.ToString();
            Thread.CurrentThread.CurrentCulture = currentCulture;

            return result;
        }
        #endregion
    }
}

