﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports.SL											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.CodeDom.Compiler;
using System.Collections.Generic;

namespace Stimulsoft.Report.WCFService
{
    public class StiCheckObject
    {
        #region Fields

        public string Name;
        public string ElementName;
        public StiWCFCheckObjectType ObjectType;

        #region Additional Properties

        public CompilerError Error;
        public string ComponentName;
        public string PropertyName;

        public string Columns;
        public string DataSources;

        public string LostPointsNames;

        public bool IsDataSource;

        public string RelationsNames;
        public string RelationsNameInSource;

        #endregion

        public List<StiActionObject> Actions;

        #endregion
    }
}
