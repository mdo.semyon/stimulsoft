﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using Stimulsoft.Base.Json;
using Stimulsoft.Base.Json.Converters;

namespace Stimulsoft.Report.WCFService
{
    #region StiSLExportType
    public enum StiSLExportType
    {
        Csv,
        Dbf,
        Dif,
        Excel,
        Excel2007,
        ExcelXml,
        Html,
        Html5,
        Mht,
        Bmp,
        Gif,
        Jpeg,
        Emf,
        Pcx,
        Png,
        Svg,
        Svgz,
        Tiff,
        Ods,
        Odt,
        Pdf,
        Rtf,
        Sylk,
        Text,
        Word2007,
        Xps,
        Ppt2007,
        Xml
    }
    #endregion

    #region StiWCFCheckObjectType
    [JsonConverter(typeof(StringEnumConverter))]
    public enum StiWCFCheckObjectType
    {
        Report,
        Page,
        Component,
        Database,
        DataSource,
        DataRelation,
        DataColumn,
        Variable
    }
    #endregion
}
