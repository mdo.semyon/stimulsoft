#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Reflection;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using Stimulsoft.Report.Components;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Controls;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dictionary
{
    /// <summary>
    /// Used to build trees by the data source.
    /// </summary>
    public class StiDataBuilder
    {
        #region Fields
        private static string stepScaleName = SystemWinApi.GetWindowsStepScaleName();

        /// <summary>
        /// Relations being used in the dictionary.
        /// </summary>
        private Hashtable usedRelations;

        /// <summary>
        /// Data sources being used.
        /// </summary>
        private Hashtable usedDataSources;

        /// <summary>
        /// Columns being used.
        /// </summary>
        private Hashtable usedColumns;

        /// <summary>
        /// Data source level.
        /// </summary>
        private int level;

        private StiTreeView treeView;
        #endregion

        #region Properties
        public bool ShowParameters { get; set; } = false;

        private static ImageList images;
        public static ImageList Images
        {
            get
            {
                if (images == null)
                    FillImages();

                return images;
            }
        }
        #endregion

        #region Methods.Get
        private static StiReport GetReport(object nodeTag)
        {
            if (nodeTag is StiDataColumn)
                return ((StiDataColumn)nodeTag).DataSource?.Dictionary?.Report;

            if (nodeTag is StiDataSource)
                return ((StiDataSource)nodeTag).Dictionary?.Report;

            if (nodeTag is StiBusinessObject)
                return ((StiBusinessObject)nodeTag).Dictionary?.Report;

            if (nodeTag is StiDataRelation)
                return ((StiDataRelation)nodeTag).Dictionary?.Report;

            return null;
        }

        public static string GetParameterPathFromNode(TreeNode node)
        {
            var parameter = node.Tag as StiDataParameter;
            var dataSource = parameter.DataSource;
            var report = dataSource.Dictionary.Report;

            var dataSourceName = StiNameValidator.CorrectName(dataSource.Name, report);
            return $"{dataSourceName}.Parameters[\"{parameter.Name}\"].ParameterValue";
        }

        /// <summary>
        /// Returns the ColumnPath from TreeNode.
        /// </summary>
        /// <param name="node">TreeNode to build the string view of a column.</param>
        /// <returns>ColumnPath.</returns>
        public static string GetColumnPathFromNode(TreeNode node)
        {
            return GetColumnPathFromNode(node, false);
        }

        /// <summary>
        /// Returns the ColumnPath from TreeNode.
        /// </summary>
        /// <param name="node">TreeNode to build the string view of a column.</param>
        /// <returns>ColumnPath.</returns>
        public static string GetColumnPathFromNode(TreeNode node, bool useRelationName)
        {
            var path = string.Empty;

            if (node.Tag is StiDataColumn)
                path = ((StiDataColumn)node.Tag).Name;

            if (node.Tag is StiDataSource)
                path = ((StiDataSource)node.Tag).Name;

            if (node.Tag is StiBusinessObject)
                path = ((StiBusinessObject)node.Tag).Name;

            path = StiNameValidator.CorrectName(path, GetReport(node.Tag));

            while (true)
            {
                node = node.Parent;
                if (node == null) break;

                var dataRelation = node.Tag as StiDataRelation;
                if (dataRelation != null)
                {
                    var relationName = useRelationName ? dataRelation.NameInSource : dataRelation.Name;
                    relationName = StiNameValidator.CorrectName(relationName, GetReport(node.Tag));
                    path = $"{relationName}.{path}";
                }

                var dataSource = node.Tag as StiDataSource;
                if (dataSource != null)
                {
                    var dataSourceName = StiNameValidator.CorrectName(dataSource.Name, GetReport(dataSource));
                    path = $"{dataSourceName}.{path}";
                    break;
                }

                var businessObject = node.Tag as StiBusinessObject;
                if (businessObject != null)
                {
                    var businessObjectName = StiNameValidator.CorrectBusinessObjectName(businessObject.GetCorrectFullName(), GetReport(businessObject));
                    path = $"{businessObjectName}.{path}";
                    break;
                }
            }
            return path;
        }

        /// <summary>
        /// Returns the ColumnPath from TreeNode for Interaction.
        /// </summary>
        /// <param name="node">TreeNode to build the string view of a column.</param>
        /// <returns>ColumnPath.</returns>
        public static string GetDataBandColumnPathFromNode(TreeNode node, bool useRelationName)
        {
            var path = string.Empty;

            if (node.Tag is StiDataColumn)
                path = ((StiDataColumn)node.Tag).Name;

            if (node.Tag is StiDataBand)
                path = ((StiDataBand)node.Tag).Name;

            path = StiNameValidator.CorrectName(path, GetReport(node.Tag));

            while (true)
            {
                node = node.Parent;

                var dataRelation = node.Tag as StiDataRelation;
                if (dataRelation != null)
                {
                    var relationName = useRelationName ? dataRelation.NameInSource : dataRelation.Name;
                    relationName = StiNameValidator.CorrectName(relationName, GetReport(dataRelation));
                    path = $"{relationName}.{path}";
                }

                var dataBand = node.Tag as StiDataBand;
                if (dataBand != null)
                {
                    var dataBandName = StiNameValidator.CorrectName(dataBand.Name, GetReport(dataBand));
                    path = $"{dataBandName}.{path}";
                    break;
                }
            }

            return path;
        }
        
        /// <summary>
        /// Returns the PropertyInfoPath from TreeNode.
        /// </summary>
        /// <param name="node">TreeNode to build the string view of a PropertyInfo.</param>
        /// <returns>PropertyInfoPath.</returns>
        public static string GetPropertyInfoPathFromNode(TreeNode node)
        {
            var path = StiNameValidator.CorrectName(((PropertyInfo)node.Tag).Name, GetReport(node.Tag));

            while (true)
            {
                node = node.Parent;

                var dataColumn = node.Tag as StiDataColumn;
                if (dataColumn != null)
                {
                    var dataColumnName = StiNameValidator.CorrectName(dataColumn.Name, GetReport(dataColumn));
                    path = $"{dataColumnName}.{path}";
                }

                var propertyInfo = node.Tag as PropertyInfo;
                if (propertyInfo != null)
                {
                    var propertyName = StiNameValidator.CorrectName(propertyInfo.Name, GetReport(propertyInfo));
                    path = $"{propertyName}.{path}";
                }

                var dataSource = node.Tag as StiDataSource;
                if (dataSource != null)
                {
                    var dataSourceName = StiNameValidator.CorrectName(dataSource.Name, GetReport(dataSource));
                    path = $"{dataSourceName}.{path}";
                    break;
                }

                var dataRelation = node.Tag as StiDataRelation;
                if (dataRelation != null)
                {
                    var dataRelationName = StiNameValidator.CorrectName(dataRelation.Name, GetReport(dataRelation));
                    path = $"{dataRelationName}.{path}";
                }
            }
            return path;
        }
        
        /// <summary>
        /// Returns the string view of a column from TreeNode taking into consideration aliases.
        /// </summary>
        /// <param name="node">TreeNode to build the string view of a column.</param>
        /// <returns></returns>
        public static string GetColumnPathFromNodeWithAlias(TreeNode node)
        {
            var path = StiNameValidator.CorrectName(((StiDataColumn)node.Tag).Name, GetReport(node.Tag));

            while (true)
            {
                node = node.Parent;
                if (node == null) break;

                var dataRelation = node.Tag as StiDataRelation;
                if (dataRelation != null)
                {
                    var dataRelationName = StiNameValidator.CorrectName(dataRelation.Name, GetReport(dataRelation));
                    path = $"{dataRelationName}.{path}";
                }

                var dataSource = node.Tag as StiDataSource;
                if (dataSource != null)
                {
                    var dataSourceName = StiNameValidator.CorrectName(dataSource.Name, GetReport(dataSource));
                    path = $"{dataSourceName}.{path}";
                    break;
                }

                var businessObject = node.Tag as StiBusinessObject;
                if (businessObject != null)
                {
                    var businessObjectName = StiNameValidator.CorrectBusinessObjectName(businessObject.GetCorrectFullName(), GetReport(businessObject));
                    path = $"{businessObjectName}.{path}";
                    break;
                }
            }
            return path;
        }
        
        public static string GetColumnPathFromNodeWithAliasWithoutReplace(TreeNode node)
        {
            var path = ((StiDataColumn)node.Tag).Name;

            while (true)
            {
                node = node.Parent;
                if (node == null) break;

                var dataRelation = node.Tag as StiDataRelation;
                if (dataRelation != null)
                {
                    path = $"{dataRelation.Name}.{path}";
                }

                var dataSource = node.Tag as StiDataSource;
                if (dataSource != null)
                {
                    path = $"{dataSource.Name}.{path}";
                    break;
                }
            }
            return path;
        }
        
        public static StiDataColumn GetColumnFromPath(string path, StiDictionary dictionary)
        {
            var strs = path.Split('.');

            var dataSourceStr = strs[0];
            foreach (StiDataSource dataSource in dictionary.DataSources)
            {
                if (StiNameValidator.CorrectName(dataSource.Name, dictionary.Report) == dataSourceStr && path.Length > dataSource.Name.Length + 1)
                {
                    var newPath = path.Substring(dataSource.Name.Length + 1);
                    return GetColumnFromPath(newPath, dataSource);
                }
            }
            return null;
        }
        
        public static StiDataColumn GetColumnFromPath(string path, StiDataSource dataSource)
        {
            var strs = path.Split('.');

            var index = 0;
            var str = strs[index];
            while (true)
            {
                foreach (StiDataColumn dataColumn in dataSource.Columns)
                {
                    if (StiNameValidator.CorrectName(dataColumn.Name, dataSource.Dictionary?.Report) == str && index == strs.Length - 1)
                    {
                        return dataColumn;
                    }
                }

                var relations = dataSource.GetParentRelations();
                foreach (StiDataRelation relation in relations)
                {
                    if (StiNameValidator.CorrectName(relation.Name, dataSource.Dictionary?.Report) == str)
                    {
                        var newPath = path.Substring(relation.Name.Length + 1);
                        return GetColumnFromPath(newPath, relation.ParentSource);
                    }
                }

                if (index == strs.Length - 1) return null;

                index++;
                str += "." + strs[index];
            }
        }
        #endregion

        #region Methods.Helpers
        internal static string GetImageKeyFromType(Type type)
        {
            #region Simple Types
            if (type == typeof(bool) || type == typeof(bool?))
                return "Bool";

            if (type == typeof(char) || type == typeof(char?))
                return "Char";

            if (type == typeof(DateTime) ||
                type == typeof(TimeSpan) ||
                type == typeof(DateTime?) ||
                type == typeof(TimeSpan?))
                return "DateTime";

            if (type == typeof(Decimal) ||
                type == typeof(Decimal?))
                return "Decimal";

            if (type == typeof(int) ||
                type == typeof(uint) ||
                type == typeof(long) ||
                type == typeof(ulong) ||
                type == typeof(byte) ||
                type == typeof(sbyte) ||
                type == typeof(short) ||
                type == typeof(ushort) ||
                type == typeof(int?) ||
                type == typeof(uint?) ||
                type == typeof(long?) ||
                type == typeof(ulong?) ||
                type == typeof(byte?) ||
                type == typeof(sbyte?) ||
                type == typeof(short?) ||
                type == typeof(ushort?))
                return "Int";

            if (type == typeof(float) ||
                type == typeof(double) ||
                type == typeof(float?) ||
                type == typeof(double?))
                return "Float";

            if (type == typeof(Image) ||
                type == typeof(Bitmap))
                return "Image";
            #endregion

            #region Range Types
            if (type == typeof(CharRange))
                return "RangeChar";

            if (type == typeof(DateTimeRange) ||
                type == typeof(TimeSpanRange))
                return "RangeDateTime";

            if (type == typeof(DecimalRange))
                return "RangeDecimal";

            if (type == typeof(IntRange) ||
                type == typeof(LongRange) ||
                type == typeof(ByteRange) ||
                type == typeof(ShortRange))
                return "RangeInt";

            if (type == typeof(FloatRange) ||
                type == typeof(DoubleRange))
                return "RangeFloat";

            if (type == typeof(StringRange) ||
                type == typeof(GuidRange))
                return "RangeString";
            #endregion

            #region List Types
            if (type == typeof(BoolList))
                return "ListBool";

            if (type == typeof(StringList) ||
                type == typeof(GuidList))
                return "ListString";

            if (type == typeof(CharList))
                return "ListChar";

            if (type == typeof(DateTimeList) ||
                type == typeof(TimeSpanList))
                return "ListDateTime";

            if (type == typeof(DecimalList))
                return "ListDecimal";

            if (type == typeof(IntList) ||
                type == typeof(LongList) ||
                type == typeof(ByteList) ||
                type == typeof(ShortList))
                return "ListInt";

            if (type == typeof(FloatList) ||
                type == typeof(DoubleList))
                return "ListFloat";
            #endregion

            if (type != null && type.IsArray)
                return "Binary";

            return "String";
        }

        private static string GetImageKeyFromType(StiResourceType type)
        {
            return type.ToString();
        }

        public static string GetImageKeyFromColumn(StiDataColumn column)
        {
            var imageKey = GetImageKeyFromType(column?.Type);
            if (column is StiCalcDataColumn)
            {
                if (column.DataSource != null)
                {
                    return column.DataSource.Inherited 
                        ? $"LockedCalcColumn{imageKey}"
                        : $"CalcColumn{imageKey}";
                }
                else
                {
                    return column.BusinessObject.Inherited 
                        ? $"LockedCalcColumn{imageKey}"
                        : $"CalcColumn{imageKey}";
                }
            }

            if (column.DataSource != null)
            {
                return column.DataSource.Inherited 
                    ? $"LockedDataColumn{imageKey}"
                    : $"DataColumn{imageKey}";
            }
            else if (column.BusinessObject != null)
            {
                return column.BusinessObject.Inherited 
                    ? $"LockedDataColumn{imageKey}"
                    : $"DataColumn{imageKey}";
            }

            return string.Empty;
        }

        public static string GetImageKeyFromVariable(StiVariable variable)
        {
            var imageKey = GetImageKeyFromType(variable?.Type);

            return variable.Inherited 
                ? $"LockedVariable{imageKey}"
                : $"Variable{imageKey}";
        }

        public static string GetImageKeyFromResource(StiResource resource)
        {
            return $"Resource{GetImageKeyFromType(resource.Type)}";
        }


        private bool IsSkipColumn(StiDataColumn column)
        {
            if (column.DataSource is StiBusinessObjectSource)
            {
                if (column.Name.StartsWith("_ID", StringComparison.InvariantCulture) ||
                    column.Name.StartsWith("_parentID", StringComparison.InvariantCulture) ||
                    column.Name.StartsWith("_Current", StringComparison.InvariantCulture)) return true;
            }
            return false;
        }
        #endregion

        #region Methods.Images
        private static void FillImages()
        {
            var stepScale = SystemWinApi.GetWindowsStepScale();
            images = new ImageList
            {
                ColorDepth = ColorDepth.Depth24Bit,
                ImageSize = new Size((int)Math.Ceiling(16 * stepScale), (int)Math.Ceiling(16 * stepScale))
            };

            var path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.Close{stepScaleName}.png" : "Close.bmp";
            images.Images.Add("Close.bmp", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));

            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.DataSource{stepScaleName}.png" : "DataSource.bmp";
            images.Images.Add("DataSource", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            images.Images.Add("DataTransformation", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.Office2013.DataTransformation{stepScaleName}.png"));

            var pathOffice2013 = StiOptions.Windows.IsOffice2013Enabled() ? "Office2013." : string.Empty;

            #region DataColumn
            images.Images.Add("DataColumn",         StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}DataColumn{stepScaleName}.png"));
            images.Images.Add("DataColumnBinary",   StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}DataColumnBinary{stepScaleName}.png"));
            images.Images.Add("DataColumnBool",     StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}DataColumnBool{stepScaleName}.png"));
            images.Images.Add("DataColumnChar",     StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}DataColumnChar{stepScaleName}.png"));
            images.Images.Add("DataColumnDateTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}DataColumnDateTime{stepScaleName}.png"));
            images.Images.Add("DataColumnDecimal",  StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}DataColumnDecimal{stepScaleName}.png"));
            images.Images.Add("DataColumnFloat",    StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}DataColumnFloat{stepScaleName}.png"));
            images.Images.Add("DataColumnImage",    StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}DataColumnImage{stepScaleName}.png"));
            images.Images.Add("DataColumnInt",      StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}DataColumnInt{stepScaleName}.png"));
            images.Images.Add("DataColumnString",   StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}DataColumnString{stepScaleName}.png"));
            #endregion

            #region CalcColumn
            images.Images.Add("CalcColumn", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}CalcColumn{stepScaleName}.png"));
            images.Images.Add("CalcColumnBinary", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}CalcColumnBinary{stepScaleName}.png"));
            images.Images.Add("CalcColumnBool", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}CalcColumnBool{stepScaleName}.png"));
            images.Images.Add("CalcColumnChar", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}CalcColumnChar{stepScaleName}.png"));
            images.Images.Add("CalcColumnDateTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}CalcColumnDateTime{stepScaleName}.png"));
            images.Images.Add("CalcColumnDecimal", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}CalcColumnDecimal{stepScaleName}.png"));
            images.Images.Add("CalcColumnFloat", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}CalcColumnFloat{stepScaleName}.png"));
            images.Images.Add("CalcColumnImage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}CalcColumnImage{stepScaleName}.png"));
            images.Images.Add("CalcColumnInt", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}CalcColumnInt{stepScaleName}.png"));
            images.Images.Add("CalcColumnString", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}CalcColumnString{stepScaleName}.png"));
            #endregion

            #region LockedDataColumn
            images.Images.Add("LockedDataColumn", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedDataColumn{stepScaleName}.png"));
            images.Images.Add("LockedDataColumnBinary", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedDataColumnBinary{stepScaleName}.png"));
            images.Images.Add("LockedDataColumnBool", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedDataColumnBool{stepScaleName}.png"));
            images.Images.Add("LockedDataColumnChar", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedDataColumnChar{stepScaleName}.png"));
            images.Images.Add("LockedDataColumnDateTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedDataColumnDateTime{stepScaleName}.png"));
            images.Images.Add("LockedDataColumnDecimal", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedDataColumnDecimal{stepScaleName}.png"));
            images.Images.Add("LockedDataColumnFloat", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedDataColumnFloat{stepScaleName}.png"));
            images.Images.Add("LockedDataColumnImage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedDataColumnImage{stepScaleName}.png"));
            images.Images.Add("LockedDataColumnInt", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedDataColumnInt{stepScaleName}.png"));
            images.Images.Add("LockedDataColumnString", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedDataColumnString{stepScaleName}.png"));
            #endregion

            #region LockedCalcColumn
            images.Images.Add("LockedCalcColumn", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedCalcColumn{stepScaleName}.png"));
            images.Images.Add("LockedCalcColumnBinary", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedCalcColumnBinary{stepScaleName}.png"));
            images.Images.Add("LockedCalcColumnBool", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedCalcColumnBool{stepScaleName}.png"));
            images.Images.Add("LockedCalcColumnChar", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedCalcColumnChar{stepScaleName}.png"));
            images.Images.Add("LockedCalcColumnDateTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedCalcColumnDateTime{stepScaleName}.png"));
            images.Images.Add("LockedCalcColumnDecimal", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedCalcColumnDecimal{stepScaleName}.png"));
            images.Images.Add("LockedCalcColumnFloat", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedCalcColumnFloat{stepScaleName}.png"));
            images.Images.Add("LockedCalcColumnImage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedCalcColumnImage{stepScaleName}.png"));
            images.Images.Add("LockedCalcColumnInt", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedCalcColumnInt{stepScaleName}.png"));
            images.Images.Add("LockedCalcColumnString", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedCalcColumnString{stepScaleName}.png"));
            #endregion

            #region Total
            images.Images.Add("Total", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.Total{stepScaleName}.png"));
            images.Images.Add("Totals", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.Totals{stepScaleName}.png"));
            #endregion

            #region LockedTotal
            images.Images.Add("LockedTotal", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.LockedTotal{stepScaleName}.png"));
            #endregion

            #region Variable
            images.Images.Add("Variable", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}Variable{stepScaleName}.png"));
            images.Images.Add("Variables", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}Variables{stepScaleName}.png"));
            images.Images.Add("VariableBinary", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableBinary{stepScaleName}.png"));
            images.Images.Add("VariableBool", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableBool{stepScaleName}.png"));
            images.Images.Add("VariableChar", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableChar{stepScaleName}.png"));
            images.Images.Add("VariableDateTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableDateTime{stepScaleName}.png"));
            images.Images.Add("VariableDecimal", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableDecimal{stepScaleName}.png"));
            images.Images.Add("VariableFloat", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableFloat{stepScaleName}.png"));
            images.Images.Add("VariableImage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableImage{stepScaleName}.png"));
            images.Images.Add("VariableInt", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableInt{stepScaleName}.png"));
            images.Images.Add("VariableString", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableString{stepScaleName}.png"));
            #endregion

            #region Resource
            images.Images.Add("Resource", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}Resource{stepScaleName}.png"));
            images.Images.Add("Resources", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}Resources{stepScaleName}.png"));

            images.Images.Add("ResourceCsv", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceCsv{stepScaleName}.png"));
            images.Images.Add("ResourceDbf", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceDbf{stepScaleName}.png"));
            images.Images.Add("ResourceExcel", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceExcel{stepScaleName}.png"));
            images.Images.Add("ResourceImage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceImage{stepScaleName}.png"));
            images.Images.Add("ResourceJson", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceJson{stepScaleName}.png"));
            images.Images.Add("ResourceReport", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceReport{stepScaleName}.png"));
            images.Images.Add("ResourceReportSnapshot", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceReportSnapshot{stepScaleName}.png"));
            images.Images.Add("ResourceRtf", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceRtf{stepScaleName}.png"));
            images.Images.Add("ResourceTxt", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceTxt{stepScaleName}.png"));
            images.Images.Add("ResourceXml", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceXml{stepScaleName}.png"));
            images.Images.Add("ResourceXsd", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceXsd{stepScaleName}.png"));
            images.Images.Add("ResourcePdf", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourcePdf{stepScaleName}.png"));
            images.Images.Add("ResourceWord", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceWord{stepScaleName}.png"));

            images.Images.Add("ResourceFontEot", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceFontEot{stepScaleName}.png"));
            images.Images.Add("ResourceFontOtf", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceFontOtf{stepScaleName}.png"));
            images.Images.Add("ResourceFontTtc", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceFontTtc{stepScaleName}.png"));
            images.Images.Add("ResourceFontTtf", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceFontTtf{stepScaleName}.png"));
            images.Images.Add("ResourceFontWoff", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}ResourceFontWoff{stepScaleName}.png"));
            #endregion

            #region VariableRange
            images.Images.Add("VariableRangeChar", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableRangeChar{stepScaleName}.png"));
            images.Images.Add("VariableRangeDateTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableRangeDateTime{stepScaleName}.png"));
            images.Images.Add("VariableRangeDecimal", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableRangeDecimal{stepScaleName}.png"));
            images.Images.Add("VariableRangeFloat", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableRangeFloat{stepScaleName}.png"));
            images.Images.Add("VariableRangeInt", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableRangeInt{stepScaleName}.png"));
            images.Images.Add("VariableRangeString", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableRangeString{stepScaleName}.png"));
            #endregion

            #region VariableList
            images.Images.Add("VariableListImage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableListImage{stepScaleName}.png"));
            images.Images.Add("VariableListBool", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableListBool{stepScaleName}.png"));
            images.Images.Add("VariableListChar", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableListChar{stepScaleName}.png"));
            images.Images.Add("VariableListDateTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableListDateTime{stepScaleName}.png"));
            images.Images.Add("VariableListDecimal", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableListDecimal{stepScaleName}.png"));
            images.Images.Add("VariableListFloat", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableListFloat{stepScaleName}.png"));
            images.Images.Add("VariableListInt", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableListInt{stepScaleName}.png"));
            images.Images.Add("VariableListString", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}VariableListString{stepScaleName}.png"));
            #endregion

            #region LockedVariable
            images.Images.Add("LockedVariable", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariable{stepScaleName}.png"));
            images.Images.Add("LockedVariableBinary", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableBinary{stepScaleName}.png"));
            images.Images.Add("LockedVariableBool", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableBool{stepScaleName}.png"));
            images.Images.Add("LockedVariableChar", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableChar{stepScaleName}.png"));
            images.Images.Add("LockedVariableDateTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableDateTime{stepScaleName}.png"));
            images.Images.Add("LockedVariableDecimal", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableDecimal{stepScaleName}.png"));
            images.Images.Add("LockedVariableFloat", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableFloat{stepScaleName}.png"));
            images.Images.Add("LockedVariableImage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableImage{stepScaleName}.png"));
            images.Images.Add("LockedVariableInt", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableInt{stepScaleName}.png"));
            images.Images.Add("LockedVariableString", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableString{stepScaleName}.png"));
            #endregion

            #region LockedVariableRange
            images.Images.Add("LockedVariableRangeChar", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableRangeChar{stepScaleName}.png"));
            images.Images.Add("LockedVariableRangeDateTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableRangeDateTime{stepScaleName}.png"));
            images.Images.Add("LockedVariableRangeDecimal", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableRangeDecimal{stepScaleName}.png"));
            images.Images.Add("LockedVariableRangeFloat", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableRangeFloat{stepScaleName}.png"));
            images.Images.Add("LockedVariableRangeInt", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableRangeInt{stepScaleName}.png"));
            images.Images.Add("LockedVariableRangeString", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableRangeString{stepScaleName}.png"));
            #endregion

            #region LockedVariableList
            images.Images.Add("LockedVariableListImage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableListImage{stepScaleName}.png"));
            images.Images.Add("LockedVariableListBool", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableListBool{stepScaleName}.png"));
            images.Images.Add("LockedVariableListChar", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableListChar{stepScaleName}.png"));
            images.Images.Add("LockedVariableListDateTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableListDateTime{stepScaleName}.png"));
            images.Images.Add("LockedVariableListDecimal", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableListDecimal{stepScaleName}.png"));
            images.Images.Add("LockedVariableListFloat", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableListFloat{stepScaleName}.png"));
            images.Images.Add("LockedVariableListInt", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableListInt{stepScaleName}.png"));
            images.Images.Add("LockedVariableListString", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}LockedVariableListString{stepScaleName}.png"));
            #endregion

            #region SystemVariable
            images.Images.Add("SystemVariable", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariable{stepScaleName}.png"));
            images.Images.Add("SystemVariables", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariables{stepScaleName}.png"));
            images.Images.Add("SystemVariableColumn", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableColumn{stepScaleName}.png"));
            images.Images.Add("SystemVariableGroupLine", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableGroupLine{stepScaleName}.png"));
            images.Images.Add("SystemVariableIsFirstPage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableIsFirstPage{stepScaleName}.png"));
            images.Images.Add("SystemVariableIsFirstPageThrough", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableIsFirstPageThrough{stepScaleName}.png"));
            images.Images.Add("SystemVariableIsLastPage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableIsLastPage{stepScaleName}.png"));
            images.Images.Add("SystemVariableIsLastPageThrough", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableIsLastPageThrough{stepScaleName}.png"));
            images.Images.Add("SystemVariableLine", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableLine{stepScaleName}.png"));
            images.Images.Add("SystemVariableLineABC", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableLineABC{stepScaleName}.png"));
            images.Images.Add("SystemVariableLineThrough", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableLineThrough{stepScaleName}.png"));
            images.Images.Add("SystemVariableLineRoman", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableLineRoman{stepScaleName}.png"));
            images.Images.Add("SystemVariablePageNofM", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariablePageNofM{stepScaleName}.png"));
            images.Images.Add("SystemVariablePageNofMThrough", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariablePageNofMThrough{stepScaleName}.png"));
            images.Images.Add("SystemVariablePageNumber", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariablePageNumber{stepScaleName}.png"));
            images.Images.Add("SystemVariablePageNumberThrough", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariablePageNumberThrough{stepScaleName}.png"));
            images.Images.Add("SystemVariableReportAlias", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableReportAlias{stepScaleName}.png"));
            images.Images.Add("SystemVariableReportAuthor", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableReportAuthor{stepScaleName}.png"));
            images.Images.Add("SystemVariableReportChanged", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableReportChanged{stepScaleName}.png"));
            images.Images.Add("SystemVariableReportCreated", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableReportCreated{stepScaleName}.png"));
            images.Images.Add("SystemVariableReportDescription", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableReportDescription{stepScaleName}.png"));
            images.Images.Add("SystemVariableReportName", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableReportName{stepScaleName}.png"));
            images.Images.Add("SystemVariableTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableTime{stepScaleName}.png"));
            images.Images.Add("SystemVariableToday", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableToday{stepScaleName}.png"));
            images.Images.Add("SystemVariableTotalPageCount", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableTotalPageCount{stepScaleName}.png"));
            images.Images.Add("SystemVariableTotalPageCountThrough", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariableTotalPageCountThrough{stepScaleName}.png"));
            #endregion

            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.DataRelation{stepScaleName}.png" : "DataRelation.bmp";
            images.Images.Add("DataRelation", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.DataRelationActive{stepScaleName}.png" : "DataRelationActive.bmp";
            images.Images.Add("DataRelationActive", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.Parameter{stepScaleName}.png" : "Parameter.bmp";
            images.Images.Add("Parameter", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));

            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.Function{stepScaleName}.png" : "Function.bmp";
            images.Images.Add("Function", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.{path}"));
            images.Images.Add("AggregateFunction", StiImageUtils.GetImage("Stimulsoft.Report", "Stimulsoft.Report.Bmp.AggregateFunction.bmp"));

            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.Category{stepScaleName}.png" : "Category.bmp";
            images.Images.Add("Category", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.Database{stepScaleName}.png" : "Database.bmp";
            images.Images.Add("Database", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));

            images.Images.Add("Format", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.TextFormats.Format{stepScaleName}.png"));
            images.Images.Add("FormatGeneral", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.TextFormats.FormatGeneral{stepScaleName}.png"));
            images.Images.Add("FormatBoolean", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.TextFormats.FormatBoolean{stepScaleName}.png"));
            images.Images.Add("FormatCurrency", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.TextFormats.FormatCurrency{stepScaleName}.png"));
            images.Images.Add("FormatPercentage", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.TextFormats.FormatPercentage{stepScaleName}.png"));
            images.Images.Add("FormatNumber", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.TextFormats.FormatNumber{stepScaleName}.png"));
            images.Images.Add("FormatDate", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.TextFormats.FormatDate{stepScaleName}.png"));
            images.Images.Add("FormatTime", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.TextFormats.FormatTime{stepScaleName}.png"));

            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.LockedCategory{stepScaleName}.png" : "LockedCategory.gif";
            images.Images.Add("LockedCategory", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.LockedDataSource{stepScaleName}.png" : "LockedDataSource.gif";
            images.Images.Add("LockedDataSource", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            images.Images.Add("LockedDataTransformation", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.Office2013.LockedDataTransformation{stepScaleName}.png"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.LockedRelation{stepScaleName}.png" : "LockedRelation.gif";
            images.Images.Add("LockedDataRelation", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.LockedRelationActive{stepScaleName}.png" : "LockedRelationActive.gif";
            images.Images.Add("LockedDataRelationActive", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.LockedParameter{stepScaleName}.png" : "LockedParameter.gif";
            images.Images.Add("LockedParameter", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.LockedDatabase{stepScaleName}.png" : "LockedDatabase.gif";
            images.Images.Add("LockedDatabase", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));

            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.UndefinedDataSource{stepScaleName}.png" : "UndefinedDataSource.gif";
            images.Images.Add("UndefinedDataSource", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.UndefinedDatabase{stepScaleName}.png" : "UndefinedDatabase.bmp";
            images.Images.Add("UndefinedDatabase", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));

            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.Function{stepScaleName}.png" : "Function.bmp";
            images.Images.Add("Function", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.DataStore{stepScaleName}.png" : "DataStore.bmp";
            images.Images.Add("DataStore", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.DatabaseFail{stepScaleName}.png" : "DatabaseFail.bmp";
            images.Images.Add("DatabaseFail", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            images.Images.Add("DatabaseTransformation", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.Office2013.DatabaseTransformation{stepScaleName}.png"));
            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.HtmlTag{stepScaleName}.png" : "HtmlTag.png";
            images.Images.Add("HtmlTag", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));

            path = StiOptions.Windows.IsOffice2013Enabled() ? $"Office2013.BusinessObject{stepScaleName}.png" : "BusinessObject.png";
            images.Images.Add("BusinessObject", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{path}"));
            images.Images.Add("LockedBusinessObject", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.LockedBusinessObject{stepScaleName}.png"));

            //Artem Not change the sequence of adding.
            images.Images.Add("SystemVariablePageCopyNumber", StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.{pathOffice2013}SystemVariablePageNumber{stepScaleName}.png"));
        }
        #endregion

        #region Methods.Build
        /// <summary>
        /// Builds TreeNodeCollection from dictionary.
        /// </summary>
        /// <param name="nodes">Collection TreeNode.</param>
        /// <param name="dictionary">Dictionary data.</param>
        /// <param name="dictionaryDesign">If true then, when rendering, aliases are used.</param>
        /// <param name="includeCalcColumn">If true then, when rendering, CalcDataColumn are included.</param>
        public void Build(TreeNodeCollection nodes, StiDictionary dictionary, bool dictionaryDesign,
            bool includeCalcColumn)
        {
            level = 0;

            #region Build DataSources
            var dataSources = dictionary.DataSources;
            var businessObjects = dictionary.BusinessObjects;

            foreach (StiDataSource data in dataSources)
            {
                if (dictionary.Restrictions.IsAllowShow(data.Name, StiDataType.Variable))
                {
                    var dataNode = new TreeNodeEx();
                    BuildDataSource(data, ref dataNode, includeCalcColumn);
                    nodes.Add(dataNode);
                }
            }

            foreach (StiBusinessObject data in businessObjects)
            {
                if (dictionary.Restrictions.IsAllowShow(data.Name, StiDataType.Variable))
                {
                    var dataNode = new TreeNode();

                    var useAliases = data.Dictionary?.Report?.Designer?.UseAliases;
                    dataNode.Text = data.ToString(StiOptions.Dictionary.ShowOnlyAliasForBusinessObject && useAliases.GetValueOrDefault(false));
                    dataNode.ImageKey = dataNode.SelectedImageKey = "BusinessObject";
                    dataNode.Tag = data;

                    BuildBusinessObject(data, dataNode, true, includeCalcColumn);
                    nodes.Add(dataNode);
                }
            }
            #endregion
        }

        #region Methods.BuildDataSource
        /// <summary>
        /// Builds TreeNode from the Data Source.
        /// </summary>
        /// <param name="dataSource">Data Source for building.</param>
        /// <param name="dataNode">Builded TreeNode.</param>
        public void BuildDataSource(StiDataSource dataSource, ref TreeNodeEx dataNode, bool includeCalcColumn)
        {
            if (dataSource != null)
            {
                var useAliases = dataSource.Dictionary?.Report?.Designer?.UseAliases;
                dataNode.Text = dataSource.ToString(StiOptions.Dictionary.ShowOnlyAliasForDataSource && useAliases.GetValueOrDefault(false));
                dataNode.ImageKey = dataNode.SelectedImageKey = "DataSource";
                dataNode.Tag = dataSource;

                if (usedDataSources != null && usedDataSources[dataSource] != null)
                {
                    if (treeView != null) treeView.SetBold(dataNode, true);
                }

                var isExpanded = CheckDataSource(dataSource, includeCalcColumn);

                if (isExpanded)
                {
                    dataNode.Nodes.Add(new TreeNode("Loading"));
                }
            }
            else dataNode = null;
        }

        private void ExpandedDataSource(StiDataSource dataSource, TreeNodeEx parentNode, bool includeCalcColumn)
        {
            var datas = new StiDataSourcesCollection(null);
            parentNode.FillFromNodeEx(ref datas, ref this.level);
            ExpandedDataSource(dataSource, parentNode, datas, includeCalcColumn);
        }

        private void ExpandedDataSource(StiDataSource dataSource, TreeNode parentNode,
            StiDataSourcesCollection datas, bool includeCalcColumn)
        {
            level++;
            try
            {
                if (StiOptions.Designer.MaxLevelOfDictionaryObjects > 0 && StiOptions.Designer.MaxLevelOfDictionaryObjects < level) return;

                if (!datas.Contains(dataSource))
                {
                    datas.Add(dataSource);
                    BuildRelations(dataSource.GetParentRelations(), parentNode, datas, includeCalcColumn);
                    datas.Remove(dataSource);
                }

                BuildColumns(dataSource, dataSource.Columns, parentNode, includeCalcColumn);

                var sqlDataSource = dataSource as StiSqlSource;
                if (sqlDataSource != null && sqlDataSource.Parameters.Count > 0)
                {
                    BuildParameters(sqlDataSource, parentNode);
                }
            }
            finally
            {
                level--;
            }
        }
        #endregion

        #region BuildRelations
        /// <summary>
        /// Builds TreeNode fom the collection of relations.
        /// </summary>
        /// <param name="relations">Collection of relations.</param>
        /// <param name="parentNode">TreeNode in which TreeNode obtained will be added.</param>
        /// <param name="datas">Data Source collection.</param>
        private void BuildRelations(StiDataRelationsCollection relations, TreeNode parentNode,
            StiDataSourcesCollection datas, bool includeCalcColumn)
        {
            if (relations.Count > 0)
            {
                foreach (StiDataRelation relation in relations)
                {
                    if (relation.Dictionary == null || relation.Dictionary.Restrictions.IsAllowShow(relation.Name, StiDataType.DataRelation))
                    {
                        var useAliases = relation.Dictionary?.Report?.Designer?.UseAliases;
                        var text = relation.ToString(StiOptions.Dictionary.ShowOnlyAliasForDataRelation && useAliases.GetValueOrDefault(false));

                        var relationNode = new TreeNodeEx(text);
                        relationNode.FillNodeEx(datas, this.level);
                        relationNode.Tag = relation;
                        relationNode.ImageKey = relationNode.SelectedImageKey = relation.Inherited ? "LockedDataRelation" : "DataRelation";
                        if (relation.Active) relationNode.ImageKey = relationNode.SelectedImageKey += "Active";

                        if (usedRelations != null && usedRelations[relation] != null)
                            if (treeView != null) treeView.SetBold(relationNode, true);

                        if (relation.ParentSource != null)
                        {
                            bool isExpanded = CheckDataSource(relation.ParentSource, includeCalcColumn);

                            if (isExpanded)
                            {
                                relationNode.Nodes.Add(new TreeNode("Loading"));
                            }
                        }

                        parentNode.Nodes.Add(relationNode);
                    }
                }
            }
        }

        private void ExpandedRelations(TreeNodeEx parentNode, bool includeCalcColumn)
        {
            var relation = parentNode.Tag as StiDataRelation;
            var dataSource = relation.ParentSource;

            if (dataSource != null)
            {
                var datas = new StiDataSourcesCollection(null);
                parentNode.FillFromNodeEx(ref datas, ref this.level);

                ExpandedDataSource(relation.ParentSource, parentNode, datas, includeCalcColumn);
            }
        }
        #endregion

        #region BusinessObjects
        public void BuildBusinessObject(StiBusinessObject businessObject, ref TreeNodeEx dataNode, bool includeCalcColumn)
        {
            if (businessObject != null)
            {
                var useAliases = businessObject.Dictionary?.Report?.Designer?.UseAliases;
                dataNode.Text = businessObject.ToString(StiOptions.Dictionary.ShowOnlyAliasForBusinessObject && useAliases.GetValueOrDefault(false));
                dataNode.ImageKey = dataNode.SelectedImageKey = "BusinessObject";
                dataNode.Tag = businessObject;

                if (usedDataSources != null && usedDataSources[businessObject] != null)
                {
                    if (treeView != null) treeView.SetBold(dataNode, true);
                }

                bool isExpanded = CheckBusinessObject(businessObject, includeCalcColumn, true);
                if (isExpanded)
                {
                    dataNode.Nodes.Add(new TreeNode("Loading"));
                }
            }
            else dataNode = null;
        }

        public void BuildBusinessObjects(StiBusinessObjectsCollection objects, TreeNode parentNode, bool includeColumns, bool includeCalcColumns)
        {
            BuildBusinessObjects(objects, parentNode, includeColumns, includeCalcColumns, true);
        }

        private void BuildBusinessObjects(StiBusinessObjectsCollection objects, TreeNode parentNode, bool includeColumns, bool includeCalcColumns, bool showCategories)
        {
            foreach (StiBusinessObject obj in objects)
            {
                if (obj.Dictionary == null || obj.Dictionary.Restrictions.IsAllowShow(obj.Name, StiDataType.BusinessObject))
                {
                    var objNode = new TreeNodeEx(obj.ToString());
                    objNode.ImageKey = objNode.SelectedImageKey =
                        obj.Inherited ? "LockedBusinessObject" : "BusinessObject";

                    objNode.Tag = obj;

                    if (showCategories)
                    {
                        TreeNode categoryNode = null;
                        if (!string.IsNullOrEmpty(obj.Category))
                        {
                            #region Search Category Node
                            foreach (TreeNode node in parentNode.Nodes)
                            {
                                if (node.Text == obj.Category)
                                {
                                    categoryNode = node;
                                    break;
                                }
                            }
                            #endregion

                            #region Create category node
                            if (categoryNode == null)
                            {
                                categoryNode = new TreeNode(obj.Category)
                                {
                                    Tag = new StiBusinessObjectCategory(obj.Category),
                                    ImageKey = "Category",
                                    SelectedImageKey = "Category"
                                };
                                parentNode.Nodes.Add(categoryNode);
                            }
                            #endregion

                            categoryNode.Nodes.Add(objNode);

                        }
                        else parentNode.Nodes.Add(objNode);
                    }
                    else
                    {
                        parentNode.Nodes.Add(objNode);
                    }

                    if (CheckBusinessObject(obj, includeColumns, includeCalcColumns))
                    {
                        objNode.Nodes.Add(new TreeNode("Loading"));
                    }
                }
            }
        }

        private void ExpandedBusinessObject(StiBusinessObject businessObject, TreeNode parentNode, bool includeColumns, bool includeCalcColumns)
        {
            BuildBusinessObject(businessObject, parentNode, includeColumns, includeCalcColumns);
        }

        /// <summary>
        /// Builds TreeNode from the Business Object.
        /// </summary>
        public void BuildBusinessObject(StiBusinessObject businessObject, TreeNode parentNode, bool includeColumns, bool includeCalcColumns)
        {
            BuildBusinessObjects(businessObject.BusinessObjects, parentNode, includeColumns, includeCalcColumns, false);
            if (includeColumns)
                BuildColumns(businessObject, businessObject.Columns, parentNode, includeCalcColumns);
        }
        #endregion

        /// <summary>
        /// Build TreeNode from the column collection.
        /// </summary>
        /// <param name="dataSource">Data source.</param>
        /// <param name="columns">Column collection.</param>
        /// <param name="parentNode">TreeNode in which TreeNode obtained will be added.</param>
        private void BuildColumns(StiDataSource dataSource, StiDataColumnsCollection columns,
            TreeNode parentNode, bool includeCalcColumn)
        {
            if (dataSource == null || dataSource.Dictionary == null) return;

            StiDictionary dictionary = dataSource.Dictionary;

            foreach (StiDataColumn column in columns)
            {
                if (IsSkipColumn(column)) continue;

                if (column.DataSource == null ||
                    dictionary.Restrictions.IsAllowShow(column.DataSource.Name + "." + column.Name, StiDataType.DataColumn))
                {
                    if (!(column is StiCalcDataColumn) || (includeCalcColumn))
                    {
                        var useAliases = dictionary?.Report?.Designer?.UseAliases;
                        var text = column.ToString(StiOptions.Dictionary.ShowOnlyAliasForDataColumn && useAliases.GetValueOrDefault(false));
                        var imageKey = GetImageKeyFromColumn(column);
                        var columnNode = new TreeNode
                        {
                            Text = text,
                            ImageKey = imageKey,
                            SelectedImageKey = imageKey,
                            Tag = column
                        };

                        BuildType(column.Type, imageKey, columnNode);

                        parentNode.Nodes.Add(columnNode);
                        if (usedColumns != null && usedColumns[column] != null)
                        {
                            if (treeView != null) treeView.SetBold(columnNode, true);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Build TreeNode from the column collection.
        /// </summary>
        private void BuildColumns(StiBusinessObject businessObject, StiDataColumnsCollection columns,
            TreeNode parentNode, bool includeCalcColumn)
        {
            if (businessObject == null || businessObject.Dictionary == null) return;

            foreach (StiDataColumn column in columns)
            {
                if (IsSkipColumn(column)) continue;

                if (column.BusinessObject == null ||
                    businessObject.Dictionary.Restrictions.IsAllowShow(column.BusinessObject.Name + "." + column.Name, StiDataType.DataColumn))
                {
                    if (!(column is StiCalcDataColumn) || (includeCalcColumn))
                    {
                        string nameColumn = column.ToString();
                        string imageKey = GetImageKeyFromColumn(column);

                        TreeNode columnNode = new TreeNode(nameColumn);
                        columnNode.ImageKey = columnNode.SelectedImageKey = imageKey;
                        columnNode.Tag = column;

                        Type type = column.Type;
                        BuildType(type, imageKey, columnNode);

                        parentNode.Nodes.Add(columnNode);
                        if (usedColumns != null && usedColumns[column] != null)
                        {
                            if (treeView != null) treeView.SetBold(columnNode, true);
                        }
                    }
                }
            }
        }

        private void BuildParameters(StiSqlSource dataSource, TreeNode parentNode)
        {
            if (!ShowParameters) return;
            TreeNode parametersNode = new TreeNode(StiLocalization.Get("PropertyMain", "Parameters"));
            parametersNode.ImageKey = parametersNode.SelectedImageKey = "Parameter";

            if (dataSource != null && dataSource.Dictionary != null)
            {
                StiDataParametersCollection parameters = dataSource.Parameters;
                foreach (StiDataParameter parameter in parameters)
                {
                    if (parameter.DataSource != null)
                    {
                        string nameParameter = parameter.ToString();


                        TreeNode parameterNode = new TreeNode(nameParameter);
                        parameterNode.ImageKey =
                            parameterNode.SelectedImageKey =
                            dataSource.Inherited ? "LockedParameter" : "Parameter";

                        parameterNode.Tag = parameter;
                        parametersNode.Nodes.Add(parameterNode);
                    }
                }
            }

            parentNode.Nodes.Add(parametersNode);
        }

        private void BuildType(Type propertyType, string imageKey, TreeNode parentNode)
        {
            int maxLevel = 5;
            if (StiTypeFinder.FindInterface(propertyType, typeof(IComponent))) maxLevel = 2;
            level++;

            if (level < maxLevel && propertyType != typeof(Image) && 
                propertyType != typeof(string) &&
                (propertyType.IsClass || propertyType.IsInterface) &&
                (!StiTypeFinder.FindInterface(propertyType, typeof(IEnumerable))))
            {
                #region Process Properties
                if (StiOptions.Dictionary.BusinessObjects.AllowUseProperties)
                {
                    PropertyInfo[] props = propertyType.GetProperties();

                    foreach (PropertyInfo prop in props)
                    {
                        object[] attrs = prop.GetCustomAttributes(typeof(BrowsableAttribute), true);
                        if (attrs != null && attrs.Length > 0 && (!((BrowsableAttribute)attrs[0]).Browsable) &&
                            StiOptions.Dictionary.BusinessObjects.PropertiesProcessingType == StiPropertiesProcessingType.Browsable) continue;

                        TreeNode columnNode = new TreeNode(prop.Name);
                        columnNode.ImageKey = columnNode.SelectedImageKey = imageKey;
                        columnNode.Tag = prop;

                        BuildType(prop.PropertyType, imageKey, columnNode);

                        parentNode.Nodes.Add(columnNode);
                    }
                }
                #endregion

                #region Process Fields
                if (StiOptions.Dictionary.BusinessObjects.AllowUseFields)
                {
                    FieldInfo[] fields = propertyType.GetFields();

                    foreach (FieldInfo field in fields)
                    {
                        object[] attrs = field.GetCustomAttributes(typeof(BrowsableAttribute), true);
                        if (attrs != null && attrs.Length > 0 && (!((BrowsableAttribute)attrs[0]).Browsable) &&
                            StiOptions.Dictionary.BusinessObjects.PropertiesProcessingType == StiPropertiesProcessingType.Browsable) continue;

                        TreeNode columnNode = new TreeNode(field.Name);
                        columnNode.ImageKey = columnNode.SelectedImageKey = imageKey;
                        columnNode.Tag = field;

                        BuildType(field.FieldType, imageKey, columnNode);

                        parentNode.Nodes.Add(columnNode);
                    }
                }
                #endregion
            }

            level--;
        }
        #endregion

        #region Methods.Check
        private bool CheckDataSource(StiDataSource dataSource, bool includeCalcColumn)
        {
            bool isExpanded = false;

            isExpanded = CheckRelations(dataSource);

            if (!isExpanded)
                isExpanded = CheckColumns(dataSource, includeCalcColumn);

            if (!isExpanded)
            {
                StiSqlSource sqlDataSource = dataSource as StiSqlSource;
                if (sqlDataSource != null && sqlDataSource.Parameters.Count > 0)
                    isExpanded = CheckParameters(sqlDataSource);
            }

            return isExpanded;
        }

        public bool CheckBusinessObject(StiBusinessObject businessObject, bool includeColumns, bool includeCalcColumns)
        {
            bool isExpanded = false;

            isExpanded = CheckBusinessObjects(businessObject.BusinessObjects);

            if (!isExpanded && includeColumns)
                isExpanded = CheckColumns(businessObject, includeCalcColumns);

            return isExpanded;
        }

        private bool CheckBusinessObjects(StiBusinessObjectsCollection objects)
        {
            foreach (StiBusinessObject obj in objects)
            {
                if (obj.Dictionary == null || obj.Dictionary.Restrictions.IsAllowShow(obj.Name, StiDataType.BusinessObject))
                {
                    return true;
                }
            }

            return false;
        }

        private bool CheckColumns(StiBusinessObject businessObject, bool includeCalcColumn)
        {
            if (businessObject == null || businessObject.Dictionary == null) return false;

            StiDictionary dictionary = businessObject.Dictionary;
            StiDataColumnsCollection columns = businessObject.Columns;
            foreach (StiDataColumn column in columns)
            {
                if (IsSkipColumn(column)) continue;

                if (column.DataSource == null || dictionary.Restrictions.IsAllowShow(column.BusinessObject.Name + "." + column.Name, StiDataType.DataColumn))
                {
                    if (!(column is StiCalcDataColumn) || includeCalcColumn)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool CheckRelations(StiDataSource dataSource)
        {
            StiDictionary dictionary = dataSource.Dictionary;
            if (dictionary != null)
            {
                foreach (StiDataRelation relation in dictionary.Relations)
                {
                    if (relation.ChildSource == dataSource && dictionary.Restrictions.IsAllowShow(relation.Name, StiDataType.DataRelation))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool CheckColumns(StiDataSource dataSource, bool includeCalcColumn)
        {
            if (dataSource == null || dataSource.Dictionary == null) return false;
            StiDictionary dictionary = dataSource.Dictionary;
            StiDataColumnsCollection columns = dataSource.Columns;

            foreach (StiDataColumn column in columns)
            {
                if (IsSkipColumn(column)) continue;

                if (column.DataSource == null || dictionary.Restrictions.IsAllowShow(column.DataSource.Name + "." + column.Name, StiDataType.DataColumn))
                {
                    if (!(column is StiCalcDataColumn) || (includeCalcColumn))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool CheckParameters(StiSqlSource dataSource)
        {
            if (!ShowParameters || dataSource == null || dataSource.Dictionary == null) return false;

            StiDataParametersCollection parameters = dataSource.Parameters;
            foreach (StiDataParameter parameter in parameters)
            {
                if (parameter.DataSource != null)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region Methods.Expanded&Collapsed
        public void ExpandedNode(TreeNodeEx node, bool includeCalcColumn)
        {
            object tag = node.Tag;
            ExpandedNode(node, includeCalcColumn, tag);
        }

        public void ExpandedNode(TreeNodeEx node, bool includeColumn, bool includeCalcColumn)
        {
            object tag = node.Tag;
            ExpandedNode(node, includeColumn, includeCalcColumn, tag);
        }

        public void ExpandedNode(TreeNodeEx node, bool includeCalcColumn, object tag)
        {
            ExpandedNode(node, true, includeCalcColumn, tag);
        }

        public void ExpandedNode(TreeNodeEx node, bool includeColumn, bool includeCalcColumn, object tag)
        {
            if (tag != null && node.Nodes.Count == 1 && node.Nodes[0].Text == "Loading")
            {
                node.Nodes.Clear();

                if (tag is StiDataSource)
                    ExpandedDataSource(tag as StiDataSource, node, includeCalcColumn);
                else if (tag is StiDataRelation)
                    ExpandedRelations(node, includeCalcColumn);
                else if (tag is StiBusinessObject)
                    ExpandedBusinessObject(tag as StiBusinessObject, node, includeColumn, includeCalcColumn);
            }
        }

        public void CollapsedNode(TreeNode node)
        {
            if (node is TreeNodeEx)
            {
                object tag = node.Tag;
                if (tag != null && node.Nodes.Count == 1 && node.Nodes[0].Text == "Loading")
                {
                    node.Nodes.Clear();

                    if (tag is StiDataSource || tag is StiDataRelation || tag is StiBusinessObject)
                    {
                        TreeNode emptyNode = new TreeNode("Loading");
                        node.Nodes.Add(emptyNode);
                    }
                }
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new object of the type StiDataBuilderEx.
        /// </summary>
        public StiDataBuilder()
            : this(null)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiDataBuilderEx.
        /// </summary>
        public StiDataBuilder(StiTreeView treeView)
            : this(null, null, null, treeView)
        {
        }


        /// <summary>
        /// Creates a new object of the type StiDataBuilderEx.
        /// </summary>
        /// <param name="usedRelations">Relations being used in the dictionary.</param>
        /// <param name="usedDataSources">Data sources being used.</param>
        /// <param name="usedColumns">Columns being used.</param>
        public StiDataBuilder(Hashtable usedRelations, Hashtable usedDataSources,
            Hashtable usedColumns, StiTreeView treeView)
        {
            this.usedRelations = usedRelations;
            this.usedDataSources = usedDataSources;
            this.usedColumns = usedColumns;
            this.treeView = treeView;
        }
        #endregion

        #region Consts
        private static Image imageLocked;
        public static Image ImageLocked
        {
            get
            {
                return imageLocked ?? (imageLocked = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Dictionary.Locked{stepScaleName}.gif"));
            }
        }

        private static Image imageCondition;
        public static Image ImageCondition
        {
            get
            {
                return imageCondition ?? (imageCondition = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.ConditionIcon{stepScaleName}.gif"));
            }
        }

        private static Image imageInteractions;
        public static Image ImageInteractions
        {
            get
            {
                return imageInteractions ?? (imageInteractions = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Interactions{stepScaleName}.png"));
            }
        }

        private static Image imageFilter;
        public static Image ImageFilter
        {
            get
            {
                return imageFilter ?? (imageFilter = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.FilterIcon{stepScaleName}.png"));
            }
        }

        private static Image imageDataColumn;
        public static Image ImageDataColumn
        {
            get
            {
                return imageDataColumn ?? (imageDataColumn = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.DataColumnIcon{stepScaleName}.gif"));
            }
        }

        private static Image imageSystemVariable;
        public static Image ImageSystemVariable
        {
            get
            {
                return imageSystemVariable ?? (imageSystemVariable = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.SystemVariableIcon{stepScaleName}.gif"));
            }
        }

        private static Image imageTotal;
        public static Image ImageTotal
        {
            get
            {
                return imageTotal ?? (imageTotal = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.TotalIcon{stepScaleName}.gif"));
            }
        }

        private static Image imageSortAsc;
        public static Image ImageSortAsc
        {
            get
            {
                return imageSortAsc ?? (imageSortAsc = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Images.SortAsc{stepScaleName}.png"));
            }
        }

        private static Image imageSortDesc;
        public static Image ImageSortDesc
        {
            get
            {
                return imageSortDesc ?? (imageSortDesc = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Images.SortDesc{stepScaleName}.png"));
            }
        }

        private static Image imageCollapsed;
        public static Image ImageCollapsed
        {
            get
            {
                return imageCollapsed ?? (imageCollapsed = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Images.Collapsed{stepScaleName}.png"));
            }
        }

        private static Image imageExpanded;
        public static Image ImageExpanded
        {
            get
            {
                return imageExpanded ?? (imageExpanded = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Images.Expanded{stepScaleName}.png"));
            }
        }
        #endregion
    }
}