#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports       										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Report.Dictionary.Design;
using System;
using System.ComponentModel;

namespace Stimulsoft.Report.Dictionary
{
    [TypeConverter(typeof(StiSqlDatabaseConverter))]
    public class StiODataDatabase : StiSqlDatabase
    {
        #region Methods.override
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiODataDatabase;
            }
        }

        public override StiDatabase CreateNew()
        {
            return new StiODataDatabase();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns new data connector for this database.
        /// </summary>
        /// <returns>Created connector.</returns>
        public override StiDataConnector CreateConnector(string connectionString = null)
        {
            return StiODataConnector.Get(connectionString);
        }

        /// <summary>
        /// Returns new data source for this database.
        /// </summary>
        /// <returns>Created data source.</returns>
        public override StiSqlSource CreateDataSource(string nameInSource, string name)
        {
            return new StiODataSource(nameInSource, name);
        }

        protected override Type GetDataAdapterType()
        {
            return typeof(StiODataAdapterService);
        }

        public override string GetConnectionStringHelper()
        {
            return "StiODataConnectionHelper";
        }

        public override string MapUserNameAndPassword(string userName, string password)
        {
            return string.Format("UserName = {0}; Password = {1}", userName, password);
        }

        public override void RegData(StiDictionary dictionary, bool loadData)
        {
            
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a connection type.
        /// </summary>
        public override StiConnectionType ConnectionType
        {
            get
            {
                return StiConnectionType.Rest;
            }
        }
        #endregion

        /// <summary>
        /// Creates a new object of the type StiMySqlDatabase.
        /// </summary>
        public StiODataDatabase()
            : this(string.Empty, string.Empty)
        {
        }


        /// <summary>
        /// Creates a new object of the type StiMySqlDatabase.
        /// </summary>
        public StiODataDatabase(string name, string connectionString)
            : base(name, connectionString)
        {
        }


        /// <summary>
        /// Creates a new object of the type StiMySqlDatabase.
        /// </summary>
        public StiODataDatabase(string name, string alias, string connectionString)
            : base(name, alias, connectionString)
        {
        }


        /// <summary>
        /// Creates a new object of the type StiMySqlDatabase.
        /// </summary>
        public StiODataDatabase(string name, string alias, string connectionString, bool promptUserNameAndpassword)
            :
            base(name, alias, connectionString, promptUserNameAndpassword)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiMySqlDatabase.
        /// </summary>
        public StiODataDatabase(string name, string alias, string connectionString, bool promptUserNameAndpassword, string key)
            :
            base(name, alias, connectionString, promptUserNameAndpassword, key)
        {
        }

    }
}
