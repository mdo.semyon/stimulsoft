#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;

using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Dictionary.Design;
using Stimulsoft.Report.Helpers;
using Stimulsoft.Report.PropertyGrid;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dictionary
{
    [TypeConverter(typeof(StiCsvDatabaseConverter))]
    public class StiCsvDatabase : StiFileDatabase
    {
        #region Methods.override
        public override StiDatabase CreateNew()
        {
            return new StiCsvDatabase();
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get { return StiComponentId.StiCsvDatabase; }
        }
        #endregion

        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiJsonDatabase
            jObject.AddPropertyStringNullOrEmpty("Separator", this.Separator);
            jObject.AddPropertyInt("CodePage", this.CodePage, 0);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Separator":
                        this.Separator = property.Value.ToObject<string>();
                        break;
                    case "CodePage":
                        this.CodePage = property.Value.ToObject<int>();
                        break;
                }
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a list separator.
        /// </summary>
        [StiSerializable]
        [StiCategory("Data")]
        [DefaultValue(null)]
        [Description("Gets or sets a list separator.")]
        public string Separator { get; set; }


        /// <summary>
        /// Gets or sets a code page.
        /// </summary>
        [StiSerializable]
        [StiCategory("Data")]
        [DefaultValue(0)]
        [Description("Gets or sets a code page.")]
        public int CodePage { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns new data connector for this database.
        /// </summary>
        /// <returns>Created connector.</returns>
        public override StiDataConnector CreateConnector(string connectionString = null)
        {
            return StiCsvConnector.Get();
        }


        /// <summary>
        /// Returns full database information.
        /// </summary>
        public override StiDatabaseInformation GetDatabaseInformation(StiReport report)
        {
            DataSet dataSet = null;

            try
            {
                var datas = StiUniversalDataLoader.LoadMutiple(report, PathData, "*.csv");
                if (datas == null) return null;

                foreach (var data in datas)
                {
                    try
                    {
                        var dataTable = StiCsvHelper.GetTable(data.Array, this.CodePage, this.Separator);
                        if (dataTable == null) continue;

                        if (dataSet == null) dataSet = new DataSet {EnforceConstraints = true};

                        dataTable.TableName = data.Name;
                        dataTable.Rows.Clear();
                        dataSet.Tables.Add(dataTable);
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }

            return dataSet == null ? null : new StiDatabaseInformation(dataSet.Tables);
        }

        public override DialogResult Edit(StiDictionary dictionary, bool newDatabase)
        {
            return StiDataEditorsHelper.Get().CsvDatabaseEdit(this, dictionary, newDatabase);
        }

        public override void RegData(StiDictionary dictionary, bool loadData)
        {
            DataSet dataSet = null;
            var path = ParsePath(PathData, dictionary.Report);

            var datas = StiUniversalDataLoader.LoadMutiple(dictionary.Report, path, "*.csv");
            if (datas != null)
            {
                foreach (var data in datas)
                {
                    try
                    {
                        var dataTable = StiDataAdapterHelper.Fill(dictionary, StiCsvHelper.GetTable(data.Array, this.CodePage, this.Separator));
                        if (dataTable == null) continue;

                        if (dataSet == null) dataSet = new DataSet {EnforceConstraints = false};

                        dataTable.TableName = data.Name;
                        dataSet.Tables.Add(dataTable);
                    }
                    catch (Exception e)
                    {
                        if (!StiRenderingMessagesHelper.WriteConnectionException(dictionary, Name, e)) throw;
                    }
                }
            }

            RegDataSetInDataStore(dictionary, dataSet);
        }
        #endregion

        public StiCsvDatabase()
		{
		}

        public StiCsvDatabase(string name, string pathData)
            : this(name, pathData, 0, null)
        {
        }

        public StiCsvDatabase(string name, string pathData, int codePage, string separator)
            : base(name, pathData)
		{
            this.CodePage = codePage;
            this.Separator = separator;
		}

        public StiCsvDatabase(string name, string pathData, int codePage, string separator, string key)
            : base(name, pathData, key)
        {
            this.CodePage = codePage;
            this.Separator = separator;
        }
	}
}
