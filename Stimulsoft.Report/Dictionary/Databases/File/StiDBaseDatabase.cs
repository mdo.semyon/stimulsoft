#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.ComponentModel;
using System.Data.SqlClient;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Dictionary.Design;
using Stimulsoft.Report.Helpers;
using Stimulsoft.Report.PropertyGrid;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dictionary
{
	[TypeConverter(typeof(StiDBaseDatabaseConverter))]
	public class StiDBaseDatabase : StiFileDatabase
    {
        #region Methods.override
        public override StiDatabase CreateNew()
        {
            return new StiDBaseDatabase();
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiDBaseDatabase;
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a code page.
        /// </summary>
        [StiSerializable]
        [StiCategory("Data")]
        [DefaultValue(0)]
        [Description("Gets or sets a code page.")]
        public int CodePage { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns new data connector for this database.
        /// </summary>
        /// <returns>Created connector.</returns>
        public override StiDataConnector CreateConnector(string connectionString = null)
        {
            return StiDBaseConnector.Get();
        }


        /// <summary>
        /// Returns full database information.
        /// </summary>
        public override StiDatabaseInformation GetDatabaseInformation(StiReport report)
        {
            DataSet dataSet = null;
            
            try
            {
                var datas = StiUniversalDataLoader.LoadMutiple(report, PathData, "*.dbf");
                if (datas == null) return null;

                foreach (var data in datas)
                {
                    try
                    {
                        var dataTable = StiDBaseHelper.GetTable(data.Array, this.CodePage);
                        if (dataTable == null) continue;

                        if (dataSet == null) dataSet = new DataSet {EnforceConstraints = true};

                        dataTable.TableName = data.Name;
                        dataTable.Rows.Clear();
                        dataSet.Tables.Add(dataTable);
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }

            return dataSet == null ? null : new StiDatabaseInformation(dataSet.Tables);
        }

        public override DialogResult Edit(StiDictionary dictionary, bool newDatabase)
		{
            return StiDataEditorsHelper.Get().DBaseDatabaseEdit(this, dictionary, newDatabase);
		}

        public override void RegData(StiDictionary dictionary, bool loadData)
        {
            DataSet dataSet = null;
            
            var path = ParsePath(PathData, dictionary.Report);

            var datas = StiUniversalDataLoader.LoadMutiple(dictionary.Report, path, "*.dbf");
            if (datas != null)
            {
                foreach (var data in datas)
                {
                    try
                    {
                        var dataTable = StiDataAdapterHelper.Fill(dictionary, StiDBaseHelper.GetTable(data.Array, this.CodePage));
                        if (dataTable == null) continue;

                        if (dataSet == null) dataSet = new DataSet {EnforceConstraints = false};

                        dataTable.TableName = data.Name;
                        dataSet.Tables.Add(dataTable);
                    }
                    catch (Exception e)
                    {
                        if (!StiRenderingMessagesHelper.WriteConnectionException(dictionary, Name, e)) throw;
                    }
                }
            }

            RegDataSetInDataStore(dictionary, dataSet);
        }
        #endregion

        public StiDBaseDatabase()
		{
		}

        public StiDBaseDatabase(string name, string pathData)
            : this(name, pathData, 0)
        {
        }

        public StiDBaseDatabase(string name, string pathData, int codePage)
            : base(name, pathData)
		{
            this.CodePage = codePage;
		}

        public StiDBaseDatabase(string name, string pathData, int codePage, string key)
            : base(name, pathData, key)
        {
            this.CodePage = codePage;
        }
	}
}
