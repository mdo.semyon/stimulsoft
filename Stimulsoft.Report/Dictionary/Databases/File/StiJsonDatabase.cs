#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Data;
using System.ComponentModel;
using System.Data.SqlClient;
using Stimulsoft.Base;
using Stimulsoft.Report.Helpers;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dictionary
{
	[TypeConverter(typeof(Stimulsoft.Report.Dictionary.Design.StiJsonDatabaseConverter))]
    public class StiJsonDatabase : StiFileDatabase
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);
            
            jObject.AddPropertyBool("RelationDirectionChildToParent", RelationDirectionChildToParent);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "RelationDirectionChildToParent":
                        this.RelationDirectionChildToParent = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region Methods.override
        public override StiDatabase CreateNew()
        {
            return new StiJsonDatabase();
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiJsonDatabase;
            }
        }
        #endregion

        #region Properties
        [StiSerializable]
        [DefaultValue(false)]
        public bool RelationDirectionChildToParent { get; set; } = false;
        #endregion

        #region Methods
        /// <summary>
        /// Returns new data connector for this database.
        /// </summary>
        /// <returns>Created connector.</returns>
        public override StiDataConnector CreateConnector(string connectionString = null)
        {
            return StiJsonConnector.Get();
        }

        /// <summary>
        /// Returns full database information.
        /// </summary>
        public override StiDatabaseInformation GetDatabaseInformation(StiReport report)
        {
            DataSet dataSet = null;
            
            try
            {
                var data = StiUniversalDataLoader.LoadSingle(report, ParsePath(PathData, report));
                if (data != null && data.Array != null)
                {
                    dataSet = StiBaseOptions.DefaultJsonConverterVersion == StiJsonConverterVersion.ConverterV2 ?
                        StiJsonToDataSetConverterV2.GetDataSet(data.Array) :
                        StiJsonToDataSetConverter.GetDataSet(data.Array);
                }
            }
            catch
            {
            }

            return dataSet == null ? null : new StiDatabaseInformation(dataSet.Tables);
        }

        public override DialogResult Edit(StiDictionary dictionary, bool newDatabase)
		{
            return StiDataEditorsHelper.Get().JsonDatabaseEdit(this, dictionary, newDatabase);
		}

        public override void RegData(StiDictionary dictionary, bool loadData)
        {
            DataSet dataSet = null;
            
            try
            {
                var data = StiUniversalDataLoader.LoadSingle(dictionary.Report, ParsePath(PathData, dictionary.Report));
                if (data != null && data.Array != null)
                {
                    dataSet = StiDataAdapterHelper.Fill(dictionary, StiBaseOptions.DefaultJsonConverterVersion == StiJsonConverterVersion.ConverterV2 ?
                        StiJsonToDataSetConverterV2.GetDataSet(data.Array, RelationDirectionChildToParent) :
                        StiJsonToDataSetConverter.GetDataSet(data.Array), this);
                }
            }
            catch (Exception e)
            {
                if (!StiRenderingMessagesHelper.WriteConnectionException(dictionary, Name, e)) throw;
            }

            RegDataSetInDataStore(dictionary, dataSet);
        }
        #endregion

        public StiJsonDatabase()
		{
		}

		public StiJsonDatabase(string name, string pathData) : base(name, pathData)
		{
			this.PathData = pathData;
		}

        public StiJsonDatabase(string name, string pathData, string key)
            : base(name, pathData, key)
        {
            this.PathData = pathData;
        }
	}
}
