#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Data;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Dictionary.Design;
using Stimulsoft.Report.PropertyGrid;

namespace Stimulsoft.Report.Dictionary
{
	[TypeConverter(typeof(StiCsvDatabaseConverter))]
	public abstract class StiFileDatabase : StiDatabase
    {
        #region IStiPropertyGridObject

        #region IStiPropertyGridObject
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiFileDatabase;
            }
        }
        #endregion

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            // DataCategory
            var list = new[]
            {
                propHelper.Name(),
                propHelper.Alias(),

                propHelper.PathData()
            };
            objHelper.Add(StiPropertyCategories.Data, list);

            return objHelper;
        }
        #endregion

        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiJsonDatabase
            jObject.AddPropertyStringNullOrEmpty("PathData", PathData);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "PathData":
                        this.PathData = property.Value.ToObject<string>();
                        break;
                }
            }
        }
        #endregion

        #region Properties
	    /// <summary>
        /// Gets or sets a path to the json data.
        /// </summary>
		[StiSerializable]
        [Description("Gets or sets a path to the xml data.")]
        [StiOrder((int)Order.PathData)]
		public string PathData { get; set; }
        #endregion

        #region Methods
        public void CreateDataSources(StiDictionary dictionary)
        {
            var information = this.GetDatabaseInformation(dictionary.Report);
            information.Tables.Select(t => new StiDataTableSource
            {
                Dictionary = dictionary,
                NameInSource = string.Format("{0}.{1}", this.Name, t.TableName),
                Name = t.TableName,
                Alias = t.TableName,
                Columns = new StiDataColumnsCollection(t.Columns.Cast<DataColumn>().ToList())
            }).ToList().ForEach(d => dictionary.DataSources.Add(d));
        }

        protected string ParsePath(string path, StiReport report)
        {
            if (!string.IsNullOrEmpty(path) && path.Contains("{") && path.Contains("}"))
            {
                try
                {
                    var page = new Components.StiPage(report);
                    var textComp = new Components.StiText();
                    page.Components.Add(textComp);

                    return Engine.StiParser.ParseTextValue(path, textComp) as string;
                }
                catch { }
            }

            return path;
        }

        protected virtual void RegDataSetInDataStore(StiDictionary dictionary, DataSet dataSet)
        {
            if (dataSet == null) return;

            dataSet.DataSetName = Name;
            foreach (DataTable table in dataSet.Tables)
            {
                var data = new StiData(string.Format("{0}.{1}", Name, table.TableName), table) { IsReportData = true };
                dictionary.DataStore[data.Name] = data;
            }
        }

        /// <summary>
        /// Returns new file connector for this database.
        /// </summary>
        /// <returns>Created connector.</returns>
        public StiFileDataConnector CreateFileConnector()
        {
            return CreateConnector() as StiFileDataConnector;
        }

        /// <summary>
        /// Adds tables, views and stored procedures to report dictionary from database information.
        /// </summary>
        public override void ApplyDatabaseInformation(StiDatabaseInformation information, StiReport report, StiDatabaseInformation informationAll)
        {
            foreach (var dataTable in information.Tables)
            {
                var source = new StiDataTableSource(string.Format("{0}.{1}", this.Name, dataTable.TableName), StiNameCreation.CreateName(report, dataTable.TableName, false, false, true));

                foreach (DataColumn dataColumn in dataTable.Columns)
                {
                    source.Columns.Add(new StiDataColumn(dataColumn.ColumnName, dataColumn.DataType));
                }
                report.Dictionary.DataSources.Add(source);
            }
        }

        public override void ApplyDatabaseInformation(StiDatabaseInformation information, StiReport report)
        {
            ApplyDatabaseInformation(information, report, null);
        }
        #endregion

        public StiFileDatabase()
            : this(string.Empty, string.Empty, string.Empty)
		{
		}

		public StiFileDatabase(string name, string pathData) : base(name)
		{
			this.PathData = pathData;
		}

        public StiFileDatabase(string name, string pathData, string key)
            : base(name, name, key)
        {
            this.PathData = pathData;
        }
	}
}
