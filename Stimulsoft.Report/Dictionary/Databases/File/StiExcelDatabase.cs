#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Data;
using System.ComponentModel;
using System.Data.SqlClient;

using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Drawing.Design;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Dictionary.Design;
using Stimulsoft.Report.Helpers;
using Stimulsoft.Report.PropertyGrid;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dictionary
{
    [TypeConverter(typeof(StiExcelDatabaseConverter))]
    public class StiExcelDatabase : StiFileDatabase
    {
        #region Methods.override
        public override StiDatabase CreateNew()
        {
            return new StiExcelDatabase();
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiExcelDatabase;
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets value which indicates that data from the data sources should be stored in the report resources.
        /// </summary>
        [StiSerializable]
        [Description("Gets or sets value which indicates that data from the data sources should be stored in the report resources.")]
        [StiOrder((int) Order.FirstRowIsHeader)]
        [DefaultValue(true)]
        public bool FirstRowIsHeader { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns new data connector for this database.
        /// </summary>
        /// <returns>Created connector.</returns>
        public override StiDataConnector CreateConnector(string connectionString = null)
        {
            return StiExcelConnector.Get();
        }

        /// <summary>
        /// Returns full database information.
        /// </summary>
        public override StiDatabaseInformation GetDatabaseInformation(StiReport report)
        {
            DataSet dataSet = null;
            
            try
            {
                var data = StiUniversalDataLoader.LoadSingle(report, PathData);
                if (data != null && data.Array != null)
                {
                    var dataSchema =
                        CreateFileConnector().RetrieveSchema(new StiExcelOptions(data.Array, this.FirstRowIsHeader));
                    if (dataSchema != null)
                        dataSet = dataSchema.GetDataSet();
                }
            }
            catch
            {
            }

            return dataSet == null ? null : new StiDatabaseInformation(dataSet.Tables);
        }

        public override DialogResult Edit(StiDictionary dictionary, bool newDatabase)
        {
            return StiDataEditorsHelper.Get().ExcelDatabaseEdit(this, dictionary, newDatabase);
        }
        
        public override void RegData(StiDictionary dictionary, bool loadData)
        {
            DataSet dataSet = null;
            
            try
            {
                var data = StiUniversalDataLoader.LoadSingle(dictionary.Report, ParsePath(PathData, dictionary.Report));
                if (data != null && data.Array != null)
                {
                    dataSet = StiDataAdapterHelper.Fill(dictionary, CreateFileConnector().GetDataSet(new StiExcelOptions(data.Array, this.FirstRowIsHeader)), this);
                }
            }
            catch (Exception e)
            {
                if (!StiRenderingMessagesHelper.WriteConnectionException(dictionary, Name, e)) throw;
            }

            RegDataSetInDataStore(dictionary, dataSet);
        }
        #endregion

        public StiExcelDatabase()
        {
            this.FirstRowIsHeader = true;
        }

        public StiExcelDatabase(string name, string pathData) : base(name, pathData)
        {
            this.PathData = pathData;
            this.FirstRowIsHeader = true;
        }

        public StiExcelDatabase(string name, string pathData, string key)
            : base(name, pathData, key)
        {
            this.PathData = pathData;
            this.FirstRowIsHeader = true;
        }

        public StiExcelDatabase(string name, string pathData, string key, bool firstRowIsHeader)
            : base(name, pathData, key)
        {
            this.PathData = pathData;
            this.FirstRowIsHeader = firstRowIsHeader;
        }
    }
}