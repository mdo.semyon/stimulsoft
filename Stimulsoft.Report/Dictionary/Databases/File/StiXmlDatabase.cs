#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Data;
using System.ComponentModel;
using System.Data.SqlClient;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Helpers;
using Stimulsoft.Report.PropertyGrid;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dictionary
{
    [TypeConverter(typeof(Stimulsoft.Report.Dictionary.Design.StiXmlDatabaseConverter))]
    public class StiXmlDatabase : StiFileDatabase
    {
        #region Methods.override
        public override StiDatabase CreateNew()
        {
            return new StiXmlDatabase();
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiXmlDatabase;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            // DataCategory
            var list = new[]
            {
                propHelper.Name(),
                propHelper.Alias(),

                propHelper.PathData(),
                propHelper.PathSchema(),
                propHelper.XmlType()
            };
            objHelper.Add(StiPropertyCategories.Data, list);

            return objHelper;
        }
        #endregion

        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiXmlDatabase
            jObject.AddPropertyStringNullOrEmpty("PathSchema", PathSchema);
            jObject.AddPropertyStringNullOrEmpty("PathData", PathData);
            jObject.AddPropertyEnum("XmlType", XmlType, StiXmlType.AdoNetXml);
            jObject.AddPropertyBool("RelationDirectionChildToParent", RelationDirectionChildToParent);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "PathSchema":
                        this.pathSchema = property.Value.ToObject<string>();
                        break;

                    case "PathData":
                        this.PathData = property.Value.ToObject<string>();
                        break;

                    case "XmlType":
                        this.xmlType = (StiXmlType) Enum.Parse(typeof(StiXmlType), property.Value.ToObject<string>());
                        break;

                    case "RelationDirectionChildToParent":
                        this.RelationDirectionChildToParent = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region Properties
        [StiSerializable]
        [DefaultValue(false)]
        public bool RelationDirectionChildToParent { get; set; } = false;

        private string pathSchema;

        /// <summary>
        /// Gets or sets a path to the xml schema.
        /// </summary>
        [StiSerializable]
        [Description("Gets or sets a path to the xml schema.")]
        [StiOrder((int) Order.PathSchema)]
        public string PathSchema
        {
            get
            {
                return pathSchema;
            }
            set
            {
                pathSchema = value;
            }
        }


        private StiXmlType xmlType = StiXmlType.AdoNetXml;

        [StiSerializable]
        [Browsable(true)]
        [DefaultValue(typeof(StiXmlType), "AdoNetXml")]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiOrder((int) Order.XmlType)]
        public StiXmlType XmlType
        {
            get
            {
                return xmlType;
            }
            set
            {
                xmlType = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns new data connector for this database.
        /// </summary>
        /// <returns>Created connector.</returns>
        public override StiDataConnector CreateConnector(string connectionString = null)
        {
            return StiXmlConnector.Get();
        }

        /// <summary>
        /// Returns full database information.
        /// </summary>
        public override StiDatabaseInformation GetDatabaseInformation(StiReport report)
        {
            DataSet dataSet = null;
            
            try
            {
                if (xmlType == StiXmlType.AdoNetXml)
                {
                    var schema = StiUniversalDataLoader.LoadSingle(report, PathSchema);
                    var data = StiUniversalDataLoader.LoadSingle(report, PathData);

                    dataSet = new DataSet {EnforceConstraints = false};
                    dataSet.Read(schema != null ? schema.Array : null, data != null ? data.Array : null);
                }
                else
                {
                    var data = StiUniversalDataLoader.LoadSingle(report, PathData);
                    if (data != null && data.Array != null)
                        dataSet = StiBaseOptions.DefaultJsonConverterVersion == StiJsonConverterVersion.ConverterV2 ?
                            StiJsonToDataSetConverterV2.GetDataSetFromXml(data.Array) :
                            StiJsonToDataSetConverter.GetDataSetFromXml(data.Array);
                }
            }
            catch
            {
            }

            return dataSet == null ? null : new StiDatabaseInformation(dataSet.Tables);
        }

        public override DialogResult Edit(StiDictionary dictionary, bool newDatabase)
        {
            return StiDataEditorsHelper.Get().XmlDatabaseEdit(this, dictionary, newDatabase);
        }

        public override void RegData(StiDictionary dictionary, bool loadData)
        {
            DataSet dataSet = null;

            try
            {
                if (xmlType == StiXmlType.AdoNetXml)
                {
                    var schema = StiUniversalDataLoader.LoadSingle(dictionary.Report,
                        ParsePath(PathSchema, dictionary.Report));
                    var data = StiUniversalDataLoader.LoadSingle(dictionary.Report,
                        ParsePath(PathData, dictionary.Report));

                    dataSet = new DataSet {EnforceConstraints = false};
                    dataSet.Read(schema?.Array, data?.Array);
                    dataSet = StiDataAdapterHelper.Fill(dictionary, dataSet, this);
                }
                else
                {
                    var data = StiUniversalDataLoader.LoadSingle(dictionary.Report,
                        ParsePath(PathData, dictionary.Report));

                    if (data != null && data.Array != null)
                    {
                        dataSet = StiBaseOptions.DefaultJsonConverterVersion == StiJsonConverterVersion.ConverterV2
                            ? StiJsonToDataSetConverterV2.GetDataSetFromXml(data.Array, RelationDirectionChildToParent)
                            : StiJsonToDataSetConverter.GetDataSetFromXml(data.Array);

                        dataSet = StiDataAdapterHelper.Fill(dictionary, dataSet, this);
                    }
                }
            }
            catch (Exception e)
            {
                if (!StiRenderingMessagesHelper.WriteConnectionException(dictionary, Name, e)) throw;
            }

            RegDataSetInDataStore(dictionary, dataSet);
        }
        #endregion

        public StiXmlDatabase()
		{
		}

        public StiXmlDatabase(string name, string pathData)
            : this(name, null, pathData)
        {
            this.xmlType = StiXmlType.Xml;
        }

		public StiXmlDatabase(string name, string pathSchema, string pathData) : base(name, pathData)
		{
			this.pathSchema = pathSchema;
		}

        public StiXmlDatabase(string name, string pathSchema, string pathData, string key)
            : base(name, pathData, key)
        {
            this.pathSchema = pathSchema;
        }

        public StiXmlDatabase(string name, string pathSchema, string pathData, string key, StiXmlType xmlType)
            : base(name, pathData, key)
        {
            this.pathSchema = pathSchema;
            this.xmlType = xmlType;
        }
	}
}
