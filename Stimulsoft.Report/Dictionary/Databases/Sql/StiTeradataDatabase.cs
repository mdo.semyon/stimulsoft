﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports       										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using Stimulsoft.Base;
using System.ComponentModel;
using Stimulsoft.Report.Dictionary.Design;

namespace Stimulsoft.Report.Dictionary
{
    [TypeConverter(typeof(StiSqlDatabaseConverter))]
    public class StiTeradataDatabase : StiSqlDatabase
    {
        #region Methods.override
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiTeradataDatabase;
            }
        }

        public override StiDatabase CreateNew()
        {
            return new StiTeradataDatabase();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns new data connector for this database.
        /// </summary>
        /// <returns>Created connector.</returns>
        public override StiDataConnector CreateConnector(string connectionString = null)
        {
            return StiTeradataConnector.Get(connectionString);
        }

        protected override Type GetDataAdapterType()
        {
            return typeof(StiTeradataAdapterService);
        }

        public override string GetConnectionStringHelper()
        {
            return null;
        }

        public override string MapUserNameAndPassword(string userName, string password)
        {
            return string.Format("User Id = {0}; Password = {1}", userName, password);
        }
        #endregion

        /// <summary>
        /// Creates a new object of the type StiTeradataDatabase.
        /// </summary>
        public StiTeradataDatabase()
            : this(string.Empty, string.Empty)
        {
        }
        
        /// <summary>
        /// Creates a new object of the type StiTeradataDatabase.
        /// </summary>
        public StiTeradataDatabase(string name, string connectionString)
            : base(name, connectionString)
        {
        }
        
        /// <summary>
        /// Creates a new object of the type StiTeradataDatabase.
        /// </summary>
        public StiTeradataDatabase(string name, string alias, string connectionString) :
            base(name, alias, connectionString)
        {
        }
        
        /// <summary>
        /// Creates a new object of the type StiTeradataDatabase.
        /// </summary>
        public StiTeradataDatabase(string name, string alias, string connectionString, bool promptUserNameAndpassword) :
            base(name, alias, connectionString, promptUserNameAndpassword)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiTeradataDatabase.
        /// </summary>
        public StiTeradataDatabase(string name, string alias, string connectionString, bool promptUserNameAndpassword, string key) :
            base(name, alias, connectionString, promptUserNameAndpassword, key)
        {
        }
    }
}
