#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Dictionary
{
    [TypeConverter(typeof(Stimulsoft.Report.Dictionary.Design.StiSqlDatabaseConverter))]
	public class StiOdbcDatabase : StiSqlDatabase
	{
        #region Methods.override
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiOdbcDatabase;
            }
        }

        public override StiDatabase CreateNew()
        {
            return new StiOdbcDatabase();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns new data connector for this database.
        /// </summary>
        /// <returns>Created connector.</returns>
        public override StiDataConnector CreateConnector(string connectionString = null)
        {
            return StiOdbcConnector.Get(connectionString);
        }

        /// <summary>
        /// Returns new data source for this database.
        /// </summary>
        /// <returns>Created data source.</returns>
        public override StiSqlSource CreateDataSource(string nameInSource, string name)
        {
            return new StiOdbcSource(nameInSource, name);
        }

        protected override Type GetDataAdapterType()
        {
            return typeof(StiOdbcAdapterService);
        }

        public override string GetConnectionStringHelper()
        {
            return "StiOdbcConnectionHelper";
        }

        public override string MapUserNameAndPassword(string userName, string password)
        {
            return string.Format("Uid = {0}; Pwd = {1}", userName, password);
        }
        #endregion


        /// <summary>
		/// Creates a new object of the type StiOdbcDatabase.
		/// </summary>
		public StiOdbcDatabase() : this(string.Empty, string.Empty)
		{
		}


		/// <summary>
		/// Creates a new object of the type StiOdbcDatabase.
		/// </summary>
		public StiOdbcDatabase(string name, string connectionString) : base(name, connectionString)
		{
		}
		

		/// <summary>
		/// Creates a new object of the type StiOdbcDatabase.
		/// </summary>
		public StiOdbcDatabase(string name, string alias, string connectionString) : 
			base(name, alias, connectionString)
		{
		}

		/// <summary>
		/// Creates a new object of the type StiOdbcDatabase.
		/// </summary>
		public StiOdbcDatabase(string name, string alias, string connectionString, bool promptUserNameAndpassword) : base(name, alias, connectionString, promptUserNameAndpassword)
		{
		}

        /// <summary>
        /// Creates a new object of the type StiOdbcDatabase.
        /// </summary>
        public StiOdbcDatabase(string name, string alias, string connectionString, bool promptUserNameAndpassword, string key)
            : base(name, alias, connectionString, promptUserNameAndpassword, key)
        {
        }	

	}
}

