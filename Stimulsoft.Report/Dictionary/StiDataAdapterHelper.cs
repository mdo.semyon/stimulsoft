#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Cloud;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Stimulsoft.Report.Dictionary
{
    internal class StiDataAdapterHelper
	{
	    public static void Fill(StiDictionary dictionary, DbDataAdapter dataAdapter, DataTable dataTable, bool schemaOnly)
	    {
            if (schemaOnly)
	        {
	            dataAdapter.FillSchema(dataTable, SchemaType.Source);
	        }
	        else
	        {
#if CLOUD
	            var reportGuid = GetReportGuid(dictionary);
	            var usedRows = StiCloudReport.GetUsedDataRows(reportGuid);
	            var maxRows = StiCloudReport.GetMaxDataRows(reportGuid);
	            var distRows = maxRows - usedRows;
	            distRows = distRows <= 10 ? 10 : distRows;

	            var rowsCount = dataAdapter.Fill(0, distRows, dataTable);
	            var resultCount = StiCloudReport.AddDataRows(reportGuid, rowsCount);

	            if (resultCount >= maxRows)
	                StiCloudReportResults.InitMaxDataRows(reportGuid, maxRows);
#else
                dataAdapter.Fill(dataTable);
#endif
	        }
	    }

	    public static DataTable Fill(StiDictionary dictionary, DataTable dataTable)
	    {
#if CLOUD
            var reportGuid = GetReportGuid(dictionary);
	        var usedRows = StiCloudReport.GetUsedDataRows(reportGuid);
	        var maxRows = StiCloudReport.GetMaxDataRows(reportGuid);
	        var distRows = maxRows - usedRows;
	        distRows = distRows <= 10 ? 10 : distRows;

	        Filter(dataTable, distRows);
	        var rowsCount = dataTable.Rows.Count;
	        var resultCount = StiCloudReport.AddDataRows(reportGuid, rowsCount);

	        if (resultCount >= maxRows)
	            StiCloudReportResults.InitMaxDataRows(reportGuid, maxRows);
#endif
            return dataTable;
        }

	    public static DataSet Fill(StiDictionary dictionary, DataSet dataSet, StiDatabase database)
	    {
#if CLOUD
            var dataSources = StiDataSourceHelper.GetDataSourcesFromDatabase(dictionary.Report, database);

            var tables = new List<DataTable>();
            foreach (StiDataStoreSource dataSource in dataSources)
            {
                foreach (DataTable table in dataSet.Tables)
                {
                    var name = $"{database.Name}.{table.TableName}";
                    if (name == dataSource.NameInSource)
                    {
                        Fill(dictionary, table);
                        tables.Add(table);
                        break;
                    }
                }                
            }

            foreach (DataTable table in dataSet.Tables)
            {
                if (!tables.Contains(table))
                {
                    try
                    {
                        table.Rows.Clear();
                    }
                    catch
                    {
                    }
                }
            }
#endif
            return dataSet;
	    }

	    private static void Filter(DataTable dataTable, int maxRows)
	    {
	        if (dataTable.Rows.Count <= maxRows) return;

	        try
	        {
	            while (dataTable.Rows.Count > maxRows)
	            {
	                dataTable.Rows.RemoveAt(dataTable.Rows.Count - 1);
	            }
	        }
	        catch
	        {
	        }
	    }

	    private static string GetReportGuid(StiDictionary dictionary)
	    {
            if (dictionary == null) return null;
            if (dictionary.Report == null) return null;
	        return dictionary.Report.ReportGuid;
	    }
	}
}
