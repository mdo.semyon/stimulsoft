using System;
using System.Collections;
using System.Text;

namespace Stimulsoft.Report.Dictionary
{
	public class StiFunctionsProgrammingShortcut
	{
		public static object Choose(int index, params object[] args)
		{
			if (args.Length == 0)
				return null;

			if (index < 1 || index > args.Length)
				return null;

			return args[index - 1];
		}

		public static object Choose(int? index, params object[] args)
		{
			if (!index.HasValue) 
				return null;

			if (args.Length == 0)
				return null;

			if (index.Value < 1 || index.Value > args.Length)
				return null;

			return args[index.Value - 1];
		}

		public static object Switch(params object[] args)
		{
			for (int index = 0; index < args.Length; index += 2)
			{
				object condition = args[index];
				object value = args[index + 1];

				if (condition is bool && ((bool)condition) == true)return value;
                if (condition is bool? && ((bool?)condition) == true) return value;
			}
			return null;
		}
	}
}
