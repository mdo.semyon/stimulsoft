#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Drawing;
using System.ComponentModel;
using Stimulsoft.Report.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using System.Threading;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dictionary
{
    #region StiDialogInfo
    [TypeConverter(typeof(Stimulsoft.Report.Dictionary.Design.StiDialogInfoConverter))]
    public class StiDialogInfo
    {
        #region IStiJsonReportObject.override
        internal string jsonLoadedBindingVariableName;
        public JObject SaveToJsonObject()
        {
            var jObject = new JObject();

            // StiDialogInfo
            jObject.AddPropertyEnum("DateTimeType", DateTimeType, StiDateTimeType.Date);
            jObject.AddPropertyEnum("ItemsInitializationType", ItemsInitializationType, StiItemsInitializationType.Items);
            jObject.AddPropertyStringNullOrEmpty("KeysColumn", KeysColumn);
            jObject.AddPropertyStringNullOrEmpty("ValuesColumn", ValuesColumn);
            jObject.AddPropertyStringNullOrEmpty("BindingValuesColumn", BindingValuesColumn);
            jObject.AddPropertyStringNullOrEmpty("Mask", Mask);
            jObject.AddPropertyBool("AllowUserValues", AllowUserValues, true);
            jObject.AddPropertyBool("BindingValue", BindingValue);
            jObject.AddPropertyJObject("Keys", StiJsonReportObjectHelper.Serialize.StringArray(Keys));
            jObject.AddPropertyJObject("Values", StiJsonReportObjectHelper.Serialize.StringArray(Values));
            if (bindingVariable != null)
                jObject.AddPropertyStringNullOrEmpty("BindingVariable", bindingVariable.Name);
            //jObject.Add("Values", ValuesBinding);                                                 // Anton

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject, StiReport report)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "DateTimeType":
                        this.dateTimeType = (StiDateTimeType)Enum.Parse(typeof(StiDateTimeType), property.Value.ToObject<string>());
                        break;

                    case "ItemsInitializationType":
                        this.itemsInitializationType = (StiItemsInitializationType)Enum.Parse(typeof(StiItemsInitializationType), property.Value.ToObject<string>());
                        break;

                    case "KeysColumn":
                        this.keysColumn = property.Value.ToObject<string>();
                        break;

                    case "ValuesColumn":
                        this.valuesColumn = property.Value.ToObject<string>();
                        break;

                    case "BindingValuesColumn":
                        this.bindingValuesColumn = property.Value.ToObject<string>();
                        break;

                    case "Mask":
                        this.mask = property.Value.ToObject<string>();
                        break;

                    case "AllowUserValues":
                        this.allowUserValues = property.Value.ToObject<bool>();
                        break;

                    case "BindingValue":
                        this.bindingValue = property.Value.ToObject<bool>();
                        break;

                    case "Keys":
                        this.keys = StiJsonReportObjectHelper.Deserialize.StringArray((JObject)property.Value);
                        break;

                    case "Values":
                        this.values = StiJsonReportObjectHelper.Deserialize.StringArray((JObject)property.Value);
                        break;

                    case "BindingVariable":
                        this.jsonLoadedBindingVariableName = property.Value.ToObject<string>();
                        report.jsonLoaderHelper.DialogInfo.Add(this);
                        break;
                }
            }
        }
        #endregion

        #region Properties
        private StiDateTimeType dateTimeType = StiDateTimeType.Date;
        [StiSerializable]
        [DefaultValue(StiDateTimeType.Date)]
        public StiDateTimeType DateTimeType
        {
            get
            {
                return dateTimeType;
            }
            set
            {
                dateTimeType = value;
            }
        }

        private StiItemsInitializationType itemsInitializationType = StiItemsInitializationType.Items;
        [StiSerializable]
        [DefaultValue(StiItemsInitializationType.Items)]
        public StiItemsInitializationType ItemsInitializationType
        {
            get
            {
                return itemsInitializationType;
            }
            set
            {
                itemsInitializationType = value;
            }
        }

        private string keysColumn = string.Empty;
        [StiSerializable]
        [DefaultValue("")]
        public string KeysColumn
        {
            get
            {
                return keysColumn;
            }
            set
            {
                keysColumn = value;
            }
        }

        private string valuesColumn = string.Empty;
        [StiSerializable]
        [DefaultValue("")]
        public string ValuesColumn
        {
            get
            {
                return valuesColumn;
            }
            set
            {
                valuesColumn = value;
            }
        }

        private StiVariable bindingVariable = null;
        [StiSerializable]
        [DefaultValue("")]
        public StiVariable BindingVariable
        {
            get
            {
                return bindingVariable;
            }
            set
            {
                bindingVariable = value;
            }
        }

        private string bindingValuesColumn = string.Empty;
        [StiSerializable]
        [DefaultValue("")]
        public string BindingValuesColumn
        {
            get
            {
                return bindingValuesColumn;
            }
            set
            {
                bindingValuesColumn = value;
            }
        }

        private string mask = string.Empty;
        [StiSerializable]
        [DefaultValue("")]
        public string Mask
        {
            get
            {
                return mask;
            }
            set
            {
                mask = value;
            }
        }

        private bool allowUserValues = true;
        [StiSerializable]
        [DefaultValue(true)]
        public bool AllowUserValues
        {
            get
            {
                return allowUserValues;
            }
            set
            {
                allowUserValues = value;
            }
        }

        private bool bindingValue = false;
        [StiSerializable]
        [DefaultValue(false)]
        public bool BindingValue
        {
            get
            {
                return bindingValue;
            }
            set
            {
                bindingValue = value;
            }
        }

        private string[] keys = new string[0];
        [StiSerializable(StiSerializationVisibility.List)]
        public string[] Keys
        {
            get
            {
                return keys;
            }
            set
            {
                keys = value;
            }
        }

        private string[] values = new string[0];
        [StiSerializable(StiSerializationVisibility.List)]
        public string[] Values
        {
            get
            {
                return values;
            }
            set
            {
                values = value;
            }
        }

        private object[] valuesBinding = new object[0];
        [StiSerializable(StiSerializationVisibility.List)]
        public object[] ValuesBinding
        {
            get
            {
                return valuesBinding;
            }
            set
            {
                valuesBinding = value;
            }
        }

        public bool IsDefault
        {
            get
            {
                return
                    AllowUserValues &&
                    DateTimeType == StiDateTimeType.Date &&
                    BindingVariable == null &&
                    (Keys == null || Keys.Length == 0) &&
                    (Values == null || Values.Length == 0) &&
                    string.IsNullOrEmpty(Mask) &&
                    string.IsNullOrEmpty(KeysColumn) &&
                    string.IsNullOrEmpty(ValuesColumn) &&
                    string.IsNullOrEmpty(BindingValuesColumn);
            }
        }
        #endregion

        #region Methods
        public static string Convert(object value)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

                if (value == null)
                    return string.Empty;
                else
                    return value.ToString();
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = currentCulture;
            }
        }

        public List<StiDialogInfoItem> GetDialogInfoItems(Type type, CultureInfo culture = null)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = culture ?? new CultureInfo("en-US", false);

                var items = new List<StiDialogInfoItem>();

                if (Keys == null)
                    return items;

                int index = 0;
                foreach (string key in Keys)
                {
                    StiDialogInfoItem item = null;

                    object keyObject = null;
                    object keyObjectTo = null;
                    try
                    {
                        #region StiExpressionDialogInfoItem
                        if (key.StartsWith("{") && key.EndsWith("}"))
                        {
                            if (StiTypeFinder.FindType(type, typeof(Range)))
                            {
                                if (key.Contains("<<|>>"))
                                {
                                    string str = key.Substring(1, key.Length - 2);

                                    string[] strs = str.Split(new string[] { "<<|>>" }, StringSplitOptions.None);
                                    if (strs.Length == 2)
                                    {
                                        keyObject = strs[0];
                                        keyObjectTo = strs[0];
                                    }
                                    else
                                        keyObject = key.Substring(1, key.Length - 2);
                                }
                                else
                                {
                                    keyObject = key.Substring(1, key.Length - 2);
                                }
                                item = new StiExpressionRangeDialogInfoItem();
                            }
                            else
                            {
                                keyObject = key.Substring(1, key.Length - 2);
                                item = new StiExpressionDialogInfoItem();
                            }
                        }
                        #endregion

                        #region StiLongDialogInfoItem
                        else if (
                            type == typeof(sbyte) || 
                            type == typeof(byte) || 
                            type == typeof(short) || 
                            type == typeof(ushort) ||
                            type == typeof(int) || 
                            type == typeof(uint) || 
                            type == typeof(long) || 
                            type == typeof(ulong) ||
                            type == typeof(sbyte?) ||
                            type == typeof(byte?) ||
                            type == typeof(short?) ||
                            type == typeof(ushort?) ||
                            type == typeof(int?) ||
                            type == typeof(uint?) ||
                            type == typeof(long?) ||
                            type == typeof(ulong?) ||
                            type == typeof(ByteList) ||
                            type == typeof(ShortList) ||
                            type == typeof(IntList) ||
                            type == typeof(LongList))
                        {
                            keyObject = long.Parse(key);
                            item = new StiLongDialogInfoItem();
                        }
                        #endregion

                        #region StiStringDialogInfoItem
                        else if (type == typeof(string) || type == typeof(StringList))
                        {
                            keyObject = key;
                            item = new StiStringDialogInfoItem();
                        }
                        #endregion

                        #region StiDoubleDialogInfoItem
                        else if (
                            type == typeof(double) || 
                            type == typeof(float) ||
                            type == typeof(double?) ||
                            type == typeof(float?) || 
                            type == typeof(DoubleList) || 
                            type == typeof(FloatList))
                        {
                            keyObject = double.Parse(key);
                            item = new StiDoubleDialogInfoItem();
                        }
                        #endregion

                        #region StiDecimalDialogInfoItem
                        else if (
                            type == typeof(decimal) ||
                            type == typeof(decimal?) || 
                            type == typeof(DecimalList))
                        {
                            keyObject = decimal.Parse(key);
                            item = new StiDecimalDialogInfoItem();
                        }
                        #endregion

                        #region StiDateTimeDialogInfoItem
                        else if (type == typeof(DateTime) || type == typeof(DateTime?) || type == typeof(DateTimeList))
                        {
                            DateTime outDate;
                            if (DateTime.TryParse(key, out outDate))
                            {
                                keyObject = outDate;
                                item = new StiDateTimeDialogInfoItem();
                            }
                            else
                            {
                                //fix - empty date is ignored
                                index++;
                                continue;
                            }
                        }
                        #endregion

                        #region StiTimeSpanDialogInfoItem
                        else if (type == typeof(TimeSpan) || type == typeof(TimeSpan?) || type == typeof(TimeSpanList))
                        {
                            keyObject = TimeSpan.Parse(key);
                            item = new StiTimeSpanDialogInfoItem();
                        }
                        #endregion

                        #region StiBoolDialogInfoItem
                        else if (type == typeof(bool) || type == typeof(bool?) || type == typeof(BoolList))
                        {
                            keyObject = bool.Parse(key);
                            item = new StiBoolDialogInfoItem();
                        }
                        #endregion

                        #region StiCharDialogInfoItem
                        else if (type == typeof(char) || type == typeof(char?) || type == typeof(CharList))
                        {
                            if (key == string.Empty) keyObject = ' ';
                            else keyObject = char.Parse(key);
                            item = new StiCharDialogInfoItem();
                        }
                        #endregion

                        #region StiGuidDialogInfoItem
                        else if (type == typeof(Guid) || type == typeof(Guid?) || type == typeof(GuidList))
                        {
                            keyObject = new Guid(key);
                            item = new StiGuidDialogInfoItem();
                        }
                        #endregion

                        #region StiImageDialogInfoItem
                        else if (type == typeof(Image) || type == typeof(ImageList) || type == typeof(Bitmap))
                        {
                            keyObject = StiImageConverter.StringToImage(key);
                            item = new StiImageDialogInfoItem();
                        }
                        #endregion

                        #region StiLongRangeDialogInfoItem
                        else if (type == typeof(ByteRange) || type == typeof(ShortRange) ||
                            type == typeof(IntRange) || type == typeof(LongRange))
                        {
                            Range range = RangeConverter.StringToRange(key);
                            item = new StiLongRangeDialogInfoItem();
                            keyObject = StiConvert.ChangeType(range.FromObject, typeof(long));
                            keyObjectTo = StiConvert.ChangeType(range.ToObject, typeof(long));
                        }
                        #endregion

                        #region StiStringRangeDialogInfoItem
                        else if (type == typeof(StringRange))
                        {
                            Range range = RangeConverter.StringToRange(key);
                            item = new StiStringRangeDialogInfoItem();
                            keyObject = range.FromObject.ToString();
                            keyObjectTo = range.ToObject.ToString();
                        }
                        #endregion

                        #region StiDoubleRangeDialogInfoItem
                        else if (type == typeof(DoubleRange) || type == typeof(FloatRange))
                        {
                            Range range = RangeConverter.StringToRange(key);
                            item = new StiDoubleRangeDialogInfoItem();
                            keyObject = StiConvert.ChangeType(range.FromObject, typeof(double));
                            keyObjectTo = StiConvert.ChangeType(range.ToObject, typeof(double));
                        }
                        #endregion

                        #region StiDecimalRangeDialogInfoItem
                        else if (type == typeof(DecimalRange))
                        {
                            Range range = RangeConverter.StringToRange(key);
                            item = new StiDecimalRangeDialogInfoItem();
                            keyObject = StiConvert.ChangeType(range.FromObject, typeof(decimal));
                            keyObjectTo = StiConvert.ChangeType(range.ToObject, typeof(decimal));
                        }
                        #endregion

                        #region StiDateTimeRangeDialogInfoItem
                        else if (type == typeof(DateTimeRange))
                        {
                            Range range = RangeConverter.StringToRange(key);
                            item = new StiDateTimeRangeDialogInfoItem();
                            keyObject = range.FromObject;
                            keyObjectTo = range.ToObject;
                        }
                        #endregion

                        #region StiTimeSpanRangeDialogInfoItem
                        else if (type == typeof(TimeSpanRange))
                        {
                            Range range = RangeConverter.StringToRange(key);
                            item = new StiTimeSpanRangeDialogInfoItem();
                            keyObject = range.FromObject;
                            keyObjectTo = range.ToObject;
                        }
                        #endregion

                        #region StiCharRangeDialogInfoItem
                        else if (type == typeof(CharRange))
                        {
                            Range range = RangeConverter.StringToRange(key);
                            item = new StiCharRangeDialogInfoItem();
                            keyObject = range.FromObject;
                            keyObjectTo = range.ToObject;
                        }
                        #endregion

                        #region StiGuidRangeDialogInfoItem
                        else if (type == typeof(GuidRange))
                        {
                            Range range = RangeConverter.StringToRange(key);
                            item = new StiGuidRangeDialogInfoItem();
                            keyObject = range.FromObject;
                            keyObjectTo = range.ToObject;
                        }
                        #endregion

                        item.KeyObject = keyObject;
                        item.KeyObjectTo = keyObjectTo;
                    }
                    catch
                    {
                    }

                    item.Value = (values.Length > index && !string.IsNullOrEmpty(values[index])) ? values[index] : string.Empty;
                    item.ValueBinding = (valuesBinding.Length > index) ? valuesBinding[index] : null;
                    items.Add(item);

                    index++;
                }
                return items;

            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = currentCulture;
            }
        }

        public void SetDialogInfoItems(List<StiDialogInfoItem> items, Type type)
        {
            if (items == null || items.Count == 0)
            {
                this.keys = null;
                this.values = null;
                return;
            }

            var currentCulture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

                this.keys = new string[items.Count];
                this.values = new string[items.Count];
                this.valuesBinding = new object[items.Count];

                int index = 0;
                foreach (StiDialogInfoItem item in items)
                {
                    object keyObject = null;

                    try
                    {
                        #region Expression
                        if (item is StiExpressionDialogInfoItem)
                        {
                            keyObject = string.Format("{{{0}}}", item.KeyObject);
                        }
                        #endregion

                        #region Expression Range
                        else if (item is StiExpressionRangeDialogInfoItem)
                        {
                            keyObject = string.Format("{{{0}<<|>>{1}}}", item.KeyObject, item.KeyObjectTo);
                        }
                        #endregion

                        #region Integers, Numeric
                        else if (item is StiLongDialogInfoItem || item is StiDoubleDialogInfoItem ||
                            item is StiDecimalDialogInfoItem || item is StiDateTimeDialogInfoItem ||
                            item is StiTimeSpanDialogInfoItem || item is StiBoolDialogInfoItem ||
                            item is StiCharDialogInfoItem || item is StiGuidDialogInfoItem ||
                            item is StiStringDialogInfoItem)
                        {
                            keyObject = item.KeyObject.ToString();
                        }
                        #endregion

                        #region Image
                        else if (item is StiImageDialogInfoItem)
                        {
                            keyObject = StiImageConverter.ImageToString(item.KeyObject as Image);
                        }
                        #endregion

                        #region Range
                        else if (
                            item is StiLongRangeDialogInfoItem || item is StiDoubleRangeDialogInfoItem ||
                            item is StiDecimalRangeDialogInfoItem || item is StiDateTimeRangeDialogInfoItem ||
                            item is StiTimeSpanRangeDialogInfoItem || item is StiCharRangeDialogInfoItem ||
                            item is StiGuidRangeDialogInfoItem || item is StiStringRangeDialogInfoItem)
                        {
                            Range range = (Range)StiActivator.CreateObject(type);
                            range.Parse(item.KeyObject.ToString(), item.KeyObjectTo.ToString());
                            keyObject = RangeConverter.RangeToString(range);
                        }
                        #endregion
                    }
                    catch
                    {
                    }

                    this.keys[index] = keyObject == null ? string.Empty : keyObject.ToString();
                    this.values[index] = item.Value;
                    this.valuesBinding[index] = item.ValueBinding;
                    index++;
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = currentCulture;
            }
        }
        #endregion

        public StiDialogInfo()
        {
        }

        public StiDialogInfo(StiDateTimeType type, string mask, bool allowUserValues, string[] keys, string[] values)
        {
            this.dateTimeType = type;
            this.mask = mask;
            this.allowUserValues = allowUserValues;
            this.keys = keys;
            this.values = values;
            this.itemsInitializationType = StiItemsInitializationType.Items;
        }

        public StiDialogInfo(StiDateTimeType type, string mask, bool allowUserValues, string keysColumn, string valuesColumn)
        {
            this.dateTimeType = type;
            this.mask = mask;
            this.allowUserValues = allowUserValues;
            this.itemsInitializationType = StiItemsInitializationType.Columns;
            this.keysColumn = keysColumn;
            this.valuesColumn = valuesColumn;
        }

        public StiDialogInfo(StiDateTimeType type, string mask, bool allowUserValues, string keysColumn, string valuesColumn,
            bool bindingValue, StiVariable bindingVariable, string bindingValuesColumn)
        {
            this.dateTimeType = type;
            this.mask = mask;
            this.allowUserValues = allowUserValues;
            this.itemsInitializationType = StiItemsInitializationType.Columns;
            this.keysColumn = keysColumn;
            this.valuesColumn = valuesColumn;
            this.bindingValue = bindingValue;
            this.bindingVariable = bindingVariable;
            this.bindingValuesColumn = bindingValuesColumn;
        }
    }
    #endregion

    #region StiDialogInfoItem
    public abstract class StiDialogInfoItem :
        IStiPropertyGridObject
    {
        #region IStiComponentId Members
        [Browsable(false)]
        public virtual StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiDialogInfoItem;
            }
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return string.Empty;
            }
        }

        public virtual StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            return null;
        }

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

        #region Properties
        private object keyObject;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        public object KeyObject
        {
            get
            {
                return keyObject;
            }
            set
            {
                keyObject = value;
            }
        }

        private object keyObjectTo;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        public object KeyObjectTo
        {
            get
            {
                return keyObjectTo;
            }
            set
            {
                keyObjectTo = value;
            }
        }

        private object valueBinding = string.Empty;
        [Browsable(false)]
        public object ValueBinding
        {
            get
            {
                return this.valueBinding;
            }
            set
            {
                this.valueBinding = value;
            }
        }

        private string value = string.Empty;
        /// <summary>
        /// Gets or sets Value which will be displayed instead of the Key value in GUI.
        /// </summary>
        [StiOrder(300)]
        [Description("Gets or sets Value which will be displayed instead of the Key value in GUI.")]
        public string Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }
        #endregion

        #region Methods
        public string ToString(StiDateTimeType dateTimeType)
        {
            if (this is StiStringRangeDialogInfoItem ||
                this is StiGuidRangeDialogInfoItem ||
                this is StiCharRangeDialogInfoItem ||
                this is StiTimeSpanRangeDialogInfoItem ||
                this is StiDoubleRangeDialogInfoItem ||
                this is StiDecimalRangeDialogInfoItem ||
                this is StiLongRangeDialogInfoItem ||
                this is StiExpressionRangeDialogInfoItem)
            {
                string str1 = KeyObject == null ? StiLocalization.Get("Report", "NotAssigned") : KeyObject.ToString();
                string str2 = KeyObjectTo == null ? StiLocalization.Get("Report", "NotAssigned") : KeyObjectTo.ToString();

                string strKey = string.IsNullOrEmpty(str1) && string.IsNullOrEmpty(str2) ? string.Empty : string.Format("{0}-{1}", str1, str2);

                if (this is StiExpressionRangeDialogInfoItem)
                    return string.Format("{{{0}}}", string.IsNullOrEmpty(Value) ? strKey : Value);
                else
                    return string.IsNullOrEmpty(Value) ? strKey : Value;
            }
            else if (this is StiDateTimeRangeDialogInfoItem)
            {
                StiDateTimeRangeDialogInfoItem range = this as StiDateTimeRangeDialogInfoItem;

                string str1 = null;
                string str2 = null;

                if (dateTimeType == StiDateTimeType.DateAndTime)
                {
                    str1 = range.From.ToString();
                    str2 = range.To.ToString();
                }
                else if (dateTimeType == StiDateTimeType.Date)
                {
                    str1 = range.From.ToShortDateString();
                    str2 = range.To.ToShortDateString();
                }
                else if (dateTimeType == StiDateTimeType.Time)
                {
                    str1 = range.From.ToShortTimeString();
                    str2 = range.To.ToShortTimeString();
                }

                string strKey = string.Format("{0}-{1}", str1, str2);

                return string.IsNullOrEmpty(Value) ? strKey : Value;
            }
            else if (this is StiImageDialogInfoItem)
            {
                StiImageDialogInfoItem item = this as StiImageDialogInfoItem;
                if (item.Key == null)
                    return StiLocalization.Get("Report", "NotAssigned");
                else
                    return StiLocalization.Get("Components", "StiImage");
            }
            else if (this is StiDateTimeDialogInfoItem)
            {
                StiDateTimeDialogInfoItem item = this as StiDateTimeDialogInfoItem;
                string str = null;

                if (dateTimeType == StiDateTimeType.DateAndTime)
                    str = item.Key.ToString();
                else if (dateTimeType == StiDateTimeType.Date)
                    str = item.Key.ToShortDateString();
                else if (dateTimeType == StiDateTimeType.Time)
                    str = item.Key.ToShortTimeString();

                return string.IsNullOrEmpty(Value) ? str : Value;
            }
            else
            {
                string str = KeyObject == null ? StiLocalization.Get("Report", "NotAssigned") : KeyObject.ToString();

                if (this is StiExpressionDialogInfoItem)
                    return string.Format("{{{0}}}", string.IsNullOrEmpty(Value) ? str : Value);
                else
                    return string.IsNullOrEmpty(Value) ? str : Value;
            }           
        }
        #endregion
    }
    #endregion

    #region StiRangeDialogInfoItem
    public class StiRangeDialogInfoItem : StiDialogInfoItem
    {    
    }
    #endregion

    #region StiStringDialogInfoItem
    public class StiStringDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiStringDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new StiPropertyObject[] 
            { 
                propHelper.StringDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);
            return objHelper;
        }
        #endregion


        /// <summary>
        /// Gets or sets Key which will be used in variables when user select associated with it Value in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets Key which will be used in variables when user select associated with it Value in GUI.")]
        public string Key
        {
            get
            {
                return KeyObject as string;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiStringDialogInfoItem()
        {
            this.KeyObject = string.Empty;
        }
    }
    #endregion

    #region StiGuidDialogInfoItem
    public class StiGuidDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiGuidDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new StiPropertyObject[] 
            { 
                propHelper.GuidDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        [StiOrder(100)]
        public Guid Key
        {
            get
            {
                return (Guid)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiGuidDialogInfoItem()
        {
            this.KeyObject = Guid.NewGuid();
        }
    }
    #endregion

    #region StiCharDialogInfoItem
    public class StiCharDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiCharDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new StiPropertyObject[] 
            { 
                propHelper.CharDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);
            return objHelper;
        }
        #endregion

        [StiOrder(100)]
        public char Key
        {
            get
            {
                return (char)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiCharDialogInfoItem()
        {
            this.KeyObject = ' ';
        }
    }
    #endregion

    #region StiBoolDialogInfoItem
    public class StiBoolDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiBoolDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new StiPropertyObject[] 
            { 
                propHelper.BoolDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        [StiOrder(100)]
        public bool Key
        {
            get
            {
                return (bool)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiBoolDialogInfoItem()
        {
            this.KeyObject = false;
        }
    }
    #endregion

    #region StiImageDialogInfoItem
    public class StiImageDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiImageDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new StiPropertyObject[] 
            { 
                propHelper.ImageDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        [StiOrder(100)]
        public Image Key
        {
            get
            {
                return (Image)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiImageDialogInfoItem()
        {
            this.KeyObject = null;
        }
    }
    #endregion

    #region StiDateTimeDialogInfoItem
    public class StiDateTimeDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiDateTimeDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new StiPropertyObject[] 
            { 
                propHelper.DateDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as key of item in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets value which will be used as key of item in GUI.")]
        public DateTime Key
        {
            get
            {
                return (DateTime)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiDateTimeDialogInfoItem()
        {
            this.KeyObject = DateTime.Now;
        }
    }
    #endregion

    #region StiTimeSpanDialogInfoItem
    public class StiTimeSpanDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiTimeSpanDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] 
            { 
                propHelper.TimeDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as key of item in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets value which will be used as key of item in GUI.")]
        public TimeSpan Key
        {
            get
            {
                return (TimeSpan)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiTimeSpanDialogInfoItem()
        {
            this.KeyObject = TimeSpan.Zero;
        }
    }
    #endregion

    #region StiDoubleDialogInfoItem
    public class StiDoubleDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiDoubleDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] 
            { 
                propHelper.DoubleDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as key of item in GUI.
        /// </summary>
        [StiOrder(100)]
        [DefaultValue(0d)]        
        [Description("Gets or sets value which will be used as key of item in GUI.")]
        public double Key
        {
            get
            {
                return (double)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiDoubleDialogInfoItem()
        {
            this.KeyObject = 0d;
        }
    }
    #endregion

    #region StiDecimalDialogInfoItem
    public class StiDecimalDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiDecimalDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] 
            { 
                propHelper.DecimalDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as key of item in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets value which will be used as key of item in GUI.")]
        public decimal Key
        {
            get
            {
                return (decimal)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiDecimalDialogInfoItem()
        {
            this.KeyObject = 0m;
        }
    }
    #endregion

    #region StiLongDialogInfoItem
    public class StiLongDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiLongDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] 
            { 
                propHelper.LongDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as key of item in GUI.
        /// </summary>
        [StiOrder(100)]
        [DefaultValue(0)]
        [Description("Gets or sets value which will be used as key of item in GUI.")]
        public long Key
        {
            get
            {
                return (long)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiLongDialogInfoItem()
        {
            this.KeyObject = (long)0;
        }
    }
    #endregion

    #region StiExpressionDialogInfoItem
    public class StiExpressionDialogInfoItem : StiDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiExpressionDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] 
            { 
                propHelper.StringDialogInfoKey(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets expression which will be used as key of item in GUI."
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets expression which will be used as key of item in GUI.")]
        public string Key
        {
            get
            {
                return (string)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        public StiExpressionDialogInfoItem()
        {
            this.KeyObject = string.Empty;
        }
    }
    #endregion

    #region StiStringRangeDialogInfoItem
    public class StiStringRangeDialogInfoItem : StiRangeDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiStringRangeDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new StiPropertyObject[] 
            { 
                propHelper.RangeStringFromDialogInfo(), 
                propHelper.RangeStringToDialogInfo(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as From key of range item in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets value which will be used as From key of range item in GUI.")]
        public string From
        {
            get
            {
                return (string)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }


        /// <summary>
        /// Gets or sets value which will be used as To key of range item in GUI.
        /// </summary>
        [StiOrder(200)]
        [Description("Gets or sets value which will be used as To key of range item in GUI.")]
        public string To
        {
            get
            {
                return (string)KeyObjectTo;
            }
            set
            {
                KeyObjectTo = value;
            }
        }

        public StiStringRangeDialogInfoItem()
        {
            this.KeyObject = string.Empty;
            this.KeyObjectTo = string.Empty;
        }
    }
    #endregion

    #region StiGuidRangeDialogInfoItem
    public class StiGuidRangeDialogInfoItem : StiRangeDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiGuidRangeDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new [] 
            { 
                propHelper.RangeGuidFromDialogInfo(), 
                propHelper.RangeGuidToDialogInfo(), 
                propHelper.DialogInfoValue()
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as From key of range item in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets value which will be used as From key of range item in GUI.")]
        public Guid From
        {
            get
            {
                return (Guid)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        /// <summary>
        /// Gets or sets value which will be used as To key of range item in GUI.
        /// </summary>
        [StiOrder(200)]
        [Description("Gets or sets value which will be used as To key of range item in GUI.")]
        public Guid To
        {
            get
            {
                return (Guid)KeyObjectTo;
            }
            set
            {
                KeyObjectTo = value;
            }
        }

        public StiGuidRangeDialogInfoItem()
        {
            this.KeyObject = Guid.NewGuid();
            this.KeyObjectTo = Guid.NewGuid();
        }
    }
    #endregion

    #region StiCharRangeDialogInfoItem
    public class StiCharRangeDialogInfoItem : StiRangeDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiCharRangeDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] 
            { 
                propHelper.RangeCharFromDialogInfo(), 
                propHelper.RangeCharToDialogInfo(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as From key of range item in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets value which will be used as From key of range item in GUI.")]
        public char From
        {
            get
            {
                return (char)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        /// <summary>
        /// Gets or sets value which will be used as To key of range item in GUI.
        /// </summary>
        [StiOrder(200)]
        [Description("Gets or sets value which will be used as To key of range item in GUI.")]
        public char To
        {
            get
            {
                return (char)KeyObjectTo;
            }
            set
            {
                KeyObjectTo = value;
            }
        }

        public StiCharRangeDialogInfoItem()
        {
            this.KeyObject = 'A';
            this.KeyObjectTo = 'Z';
        }
    }
    #endregion

    #region StiDateTimeRangeDialogInfoItem
    public class StiDateTimeRangeDialogInfoItem : StiRangeDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiDateTimeRangeDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] 
            { 
                propHelper.RangeDateFromDialogInfo(), 
                propHelper.RangeDateToDialogInfo(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as From key of range item in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets value which will be used as From key of range item in GUI.")]
        public DateTime From
        {
            get
            {
                return (DateTime)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        /// <summary>
        /// Gets or sets value which will be used as To key of range item in GUI.
        /// </summary>
        [StiOrder(200)]
        [Description("Gets or sets value which will be used as To key of range item in GUI.")]
        public DateTime To
        {
            get
            {
                return (DateTime)KeyObjectTo;
            }
            set
            {
                KeyObjectTo = value;
            }
        }

        public StiDateTimeRangeDialogInfoItem()
        {
            this.KeyObject = DateTime.Now;
            this.KeyObjectTo = DateTime.Now;
        }
    }
    #endregion

    #region StiTimeSpanRangeDialogInfoItem
    public class StiTimeSpanRangeDialogInfoItem : StiRangeDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiTimeSpanRangeDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] { 
                propHelper.RangeTimeFromDialogInfo(), 
                propHelper.RangeTimeToDialogInfo(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as From key of range item in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets value which will be used as From key of range item in GUI.")]
        public TimeSpan From
        {
            get
            {
                return (TimeSpan)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        /// <summary>
        /// Gets or sets value which will be used as To key of range item in GUI.
        /// </summary>
        [StiOrder(200)]
        [Description("Gets or sets value which will be used as To key of range item in GUI.")]
        public TimeSpan? To
        {
            get
            {
                return (TimeSpan)KeyObjectTo;
            }
            set
            {
                KeyObjectTo = value;
            }
        }

        public StiTimeSpanRangeDialogInfoItem()
        {
            this.KeyObject = TimeSpan.Zero;
            this.KeyObjectTo = TimeSpan.Zero;
        }
    }
    #endregion

    #region StiDoubleRangeDialogInfoItem
    public class StiDoubleRangeDialogInfoItem : StiRangeDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiDoubleRangeDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            IStiPropertiesHelper propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] 
            { 
                propHelper.RangeDoubleFromDialogInfo(), 
                propHelper.RangeDoubleToDialogInfo(), 
                propHelper.DialogInfoValue()
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as From key of range item in GUI.
        /// </summary>
        [StiOrder(100)]
        [DefaultValue(0d)]
        [Description("Gets or sets value which will be used as From key of range item in GUI.")]
        public double From
        {
            get
            {
                return (double)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        /// <summary>
        /// Gets or sets value which will be used as To key of range item in GUI.
        /// </summary>
        [StiOrder(200)]
        [DefaultValue(0d)]
        [Description("Gets or sets value which will be used as To key of range item in GUI.")]
        public double To
        {
            get
            {
                return (double)KeyObjectTo;
            }
            set
            {
                KeyObjectTo = value;
            }
        }

        public StiDoubleRangeDialogInfoItem()
        {
            this.KeyObject = 0d;
            this.KeyObjectTo = 0d;
        }
    }
    #endregion

    #region StiDecimalRangeDialogInfoItem
    public class StiDecimalRangeDialogInfoItem : StiRangeDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiDecimalRangeDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] 
            { 
                propHelper.RangeDecimalFromDialogInfo(), 
                propHelper.RangeDecimalToDialogInfo(), 
                propHelper.DialogInfoValue()
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as From key of range item in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets value which will be used as From key of range item in GUI.")]
        public decimal From
        {
            get
            {
                return (decimal)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        /// <summary>
        /// Gets or sets value which will be used as To key of range item in GUI.
        /// </summary>
        [StiOrder(200)]
        [Description("Gets or sets value which will be used as To key of range item in GUI.")]
        public decimal To
        {
            get
            {
                return (decimal)KeyObjectTo;
            }
            set
            {
                KeyObjectTo = value;
            }
        }

        public StiDecimalRangeDialogInfoItem()
        {
            this.KeyObject = 0m;
            this.KeyObjectTo = 0m;
        }
    }
    #endregion

    #region StiLongRangeDialogInfoItem
    public class StiLongRangeDialogInfoItem : StiRangeDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiLongRangeDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            StiPropertyObject[] list = new[] 
            { 
                propHelper.RangeLongFromDialogInfo(), 
                propHelper.RangeLongToDialogInfo(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets value which will be used as From key of range item in GUI.
        /// </summary>
        [StiOrder(100)]
        [DefaultValue(0)]
        [Description("Gets or sets value which will be used as From key of range item in GUI.")]
        public long From
        {
            get
            {
                return (long)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        /// <summary>
        /// Gets or sets value which will be used as To key of range item in GUI.
        /// </summary>
        [StiOrder(200)]
        [DefaultValue(0)]
        [Description("Gets or sets value which will be used as To key of range item in GUI.")]
        public long To
        {
            get
            {
                return (long)KeyObjectTo;
            }
            set
            {
                KeyObjectTo = value;
            }
        }

        public StiLongRangeDialogInfoItem()
        {
            this.KeyObject = (long)0;
            this.KeyObjectTo = (long)0;
        }
    }
    #endregion

    #region StiExpressionRangeDialogInfoItem
    public class StiExpressionRangeDialogInfoItem : StiRangeDialogInfoItem
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiExpressionRangeDialogInfoItem;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[] 
            { 
                propHelper.RangeExpressionFromDialogInfo(), 
                propHelper.RangeExpressionToDialogInfo(), 
                propHelper.DialogInfoValue() 
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            return objHelper;
        }
        #endregion

        /// <summary>
        /// Gets or sets expression which will be used as From key of range item in GUI.
        /// </summary>
        [StiOrder(100)]
        [Description("Gets or sets expression which will be used as From key of range item in GUI.")]
        public string From
        {
            get
            {
                return (string)KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }

        /// <summary>
        /// Gets or sets expression which will be used as To key of range item in GUI.
        /// </summary>
        [StiOrder(200)]
        [Description("Gets or sets expression which will be used as To key of range item in GUI.")]
        public string To
        {
            get
            {
                return (string)KeyObjectTo;
            }
            set
            {
                KeyObjectTo = value;
            }
        }

        public StiExpressionRangeDialogInfoItem()
        {
            this.KeyObject = string.Empty;
            this.KeyObjectTo = string.Empty;
        }
    }
    #endregion
}