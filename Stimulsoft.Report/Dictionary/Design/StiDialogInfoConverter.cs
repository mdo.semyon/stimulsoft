#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft


using System;
using System.Xml;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Drawing.Text;
using System.Drawing;
using System.Reflection;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Dictionary.Design
{
	/// <summary>
	/// Converts StiDialogInfo from one data type to another. 
	/// </summary>
    public class StiDialogInfoConverter : TypeConverter
	{
		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, 
			object value, Attribute[] attributes)
		{
			return TypeDescriptor.GetProperties(value, attributes); 
		} 


		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return true; 
		}


		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, 
			object value, Type destinationType)
        {
            #region InstanceDescriptor
            if (destinationType == typeof(InstanceDescriptor) && value != null)
            {
                var dialogInfo = (StiDialogInfo)value;

                #region StiItemsInitializationType.Columns
                if (dialogInfo.ItemsInitializationType == StiItemsInitializationType.Columns)
                {
                    if (dialogInfo.BindingValue)
                    {
                        var types = new Type[]
                        {
							typeof(StiDateTimeType),
							typeof(string),
                            typeof(bool),
                            typeof(string),
                            typeof(string),
                            typeof(bool),
                            typeof(StiVariable),
                            typeof(string)
                        };

                        var info = typeof(StiDialogInfo).GetConstructor(types);
                        if (info != null)
                        {
                            string keysColumn = dialogInfo.KeysColumn ?? string.Empty;
                            string valuesColumn = dialogInfo.ValuesColumn ?? string.Empty;
                            string bindingValuesColumn = dialogInfo.BindingValuesColumn ?? string.Empty;

                            var objs = new object[]	
                            {	
                                dialogInfo.DateTimeType,
								dialogInfo.Mask,
                                dialogInfo.AllowUserValues,
                                keysColumn,
                                valuesColumn,
                                dialogInfo.BindingValue,
                                dialogInfo.BindingVariable,
                                bindingValuesColumn
                            };

                            return CreateNewInstanceDescriptor(info, objs);
                        }
                    }
                    else
                    {
                        var types = new Type[]
                        {
							typeof(StiDateTimeType),
							typeof(string),
                            typeof(bool),
                            typeof(string),
                            typeof(string)
                        };

                        var info = typeof(StiDialogInfo).GetConstructor(types);
                        if (info != null)
                        {
                            string keysColumn = dialogInfo.KeysColumn == null ? string.Empty : dialogInfo.KeysColumn;
                            string valuesColumn = dialogInfo.ValuesColumn == null ? string.Empty : dialogInfo.ValuesColumn;

                            var objs = new object[]	
                            {	
                                dialogInfo.DateTimeType,
								dialogInfo.Mask,
                                dialogInfo.AllowUserValues,
                                keysColumn,
                                valuesColumn
                            };

                            return CreateNewInstanceDescriptor(info, objs);
                        }
                    }

                    
                }
                #endregion

                #region StiItemsInitializationType.Items
                else
                {
                    var types = new Type[]
                    {
						typeof(StiDateTimeType),
						typeof(string),
                        typeof(bool),
						typeof(string[]),
                        typeof(string[])
                    };

                    var info = typeof(StiDialogInfo).GetConstructor(types);
                    if (info != null)
                    {
                        string[] keys = dialogInfo.Keys == null ? new string[0] : dialogInfo.Keys;
                        string[] values = dialogInfo.Values == null ? new string[0] : dialogInfo.Values;

                        var objs = new object[]	
                        {	
                            dialogInfo.DateTimeType,
							dialogInfo.Mask,
                            dialogInfo.AllowUserValues,
                            keys,
                            values
                        };

                        return CreateNewInstanceDescriptor(info, objs);
                    }
                }
                #endregion
            }
            #endregion

            #region String
            else if (destinationType == typeof(string))
			{
                var info = value as StiDialogInfo;
                if (info != null)
				{
                    return StiObjectStateSaver.WriteObjectStateToString(info);
				}
            }
            #endregion

            return base.ConvertTo(context, culture, value, destinationType);
		}


		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))return true; 
			return base.CanConvertFrom(context, sourceType); 
		} 

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(InstanceDescriptor)) return true;
			if (destinationType == typeof(string)) return true;
			return base.CanConvertTo(context, destinationType); 
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
                var info = new StiDialogInfo();
                StiObjectStateSaver.ReadObjectStateFromString(info, value as string);

                return info;
            }
			return base.ConvertFrom(context, culture, value); 
		}

        public static string ConvertDialogInfoToString(StiDialogInfo info)
        {
            if (info == null || info.IsDefault)
                return "null";
            else
            {
                var converter = new StiDialogInfoConverter();
                return XmlConvert.EncodeName(converter.ConvertToInvariantString(info));
            }
        }

        public static StiDialogInfo ConvertFromStringToDialogInfo(string str)
        {
            if (str == "null" || string.IsNullOrWhiteSpace(str))
                return new StiDialogInfo();

            return new StiDialogInfoConverter().
                ConvertFromInvariantString(XmlConvert.DecodeName(str)) as StiDialogInfo;
        }

        private object CreateNewInstanceDescriptor(ConstructorInfo info, object[] objs)
        {
            return new InstanceDescriptor(info, objs);
        }
	}
}
