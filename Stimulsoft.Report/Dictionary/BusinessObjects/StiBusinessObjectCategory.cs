﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Dictionary
{
    /// <summary>
    /// Class which describe category of business object. Class used only in design-time.
    /// </summary>
    public class StiBusinessObjectCategory
    {
        private string category;
        /// <summary>
        /// Gets or sets category name of business object.
        /// </summary>
        [Browsable(false)]
        public string Category
        {
            get
            {
                return category;
            }
        }

        public StiBusinessObjectCategory(string category)
        {
            this.category = category;
        }
    }
}
