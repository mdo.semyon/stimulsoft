#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft


using System;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;
using System.Data;
using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.CodeDom;
using Stimulsoft.Report.Engine;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.PropertyGrid;

namespace Stimulsoft.Report.Dictionary
{
	/// <summary>
	/// Describes the datasource which based on other datasource.
	/// </summary>
	[TypeConverter(typeof(Stimulsoft.Report.Dictionary.Design.StiVirtualSourceConverter))]
	public class StiVirtualSource : 
		StiDataStoreSource, 
		IStiFilter
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiVirtualSource
            jObject.AddPropertyEnum("FilterMode", FilterMode, StiFilterMode.And);
            jObject.AddPropertyJObject("Filters", Filters.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GroupColumns", StiJsonReportObjectHelper.Serialize.StringArray(GroupColumns));
            jObject.AddPropertyJObject("Results", StiJsonReportObjectHelper.Serialize.StringArray(Results));
            jObject.AddPropertyJObject("Sort", StiJsonReportObjectHelper.Serialize.StringArray(Sort));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "FilterMode":
                        this.filterMode = (StiFilterMode)Enum.Parse(typeof(StiFilterMode), property.Value.ToObject<string>());
                        break;

                    case "Filters":
                        this.filters.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "GroupColumns":
                        this.groupColumns = StiJsonReportObjectHelper.Deserialize.StringArray((JObject)property.Value);
                        break;

                    case "Results":
                        this.results = StiJsonReportObjectHelper.Deserialize.StringArray((JObject)property.Value);
                        break;

                    case "Sort":
                        this.sort = StiJsonReportObjectHelper.Deserialize.StringArray((JObject)property.Value);
                        break;
                }
            }
        }
        #endregion

        #region IStiFilter
        private StiFilterEventHandler filterMethodHandler;
		[Browsable(false)]
		public StiFilterEventHandler FilterMethodHandler
		{
			get
			{
				return filterMethodHandler;
			}
			set
			{
				filterMethodHandler = value;
			}
		}


		private bool filterOn = true;
		[Browsable(false)]
		public virtual bool FilterOn
		{
			get
			{
				return filterOn;
			}
			set
			{
				filterOn = value;
			}
		}


		private StiFilterMode filterMode = StiFilterMode.And;
		/// <summary>
		/// Gets or sets filter mode.
		/// </summary>
		[DefaultValue(StiFilterMode.And)]
		[StiSerializable]
		[Browsable(false)]
		public StiFilterMode FilterMode
		{
			get
			{
				return filterMode;
			}
			set
			{
				filterMode = value;
			}
		}


		private StiFiltersCollection filters = new StiFiltersCollection();
		/// <summary>
		/// Gets or sets the collection of data filters.
		/// </summary>
		[StiSerializable(StiSerializationVisibility.List)]
		[Browsable(false)]
		public virtual StiFiltersCollection Filters
		{
			get
			{
				return filters;
			}
			set
			{
				filters = value;
			}
		}

		#endregion

		#region DataAdapter
        protected override Type GetDataAdapterType()
        {
            return typeof(StiVirtualAdapterService);
        }
		#endregion

        #region Properties
        private string[] groupColumns = new string[0];
        [StiSerializable(StiSerializationVisibility.List)]
        [Browsable(false)]
        public string[] GroupColumns
        {
            get
            {
                return groupColumns;
            }
            set
            {
                groupColumns = value;
            }
        }


        private string[] results = new string[0];
        [StiSerializable(StiSerializationVisibility.List)]
        [Browsable(false)]
        public string[] Results
        {
            get
            {
                return results;
            }
            set
            {
                results = value;
            }
        }


        private string[] sort = new string[0];
        [StiSerializable(StiSerializationVisibility.List)]
        [Browsable(false)]
        public virtual string[] Sort
        {
            get
            {
                return sort;
            }
            set
            {
                sort = value;
            }
        }
        #endregion

        #region Methods
        internal void ConnectToData()
		{
			object filterMethodHandler = null;

			#region Get Filter Method
			string correctedDataName = StiNameValidator.CorrectName(this.Name, Dictionary.Report);

			Type type = Dictionary.Report.GetType();
			MethodInfo method = type.GetMethod(correctedDataName + "__GetFilter", 
				new Type[]{typeof(object), typeof(StiFilterEventArgs)});
				
			if (method != null)
			{
                filterMethodHandler = MulticastDelegate.CreateDelegate(
                    typeof(StiFilterEventHandler), this.Dictionary.Report, correctedDataName + "__GetFilter")
                    as StiFilterEventHandler;
			}

            if ((filterMethodHandler == null && Dictionary.Report.CalculationMode == StiCalculationMode.Interpretation) || 
                Dictionary.Report.IsDesigning)
            {
                StiDataBand tempBand = new StiDataBand();
                tempBand.Name = "VirtualSourceBand";
                tempBand.Page = this.Dictionary.Report.Pages[0];
                tempBand.DataSourceName = this.NameInSource;
                tempBand.Filters = this.Filters;
                filterMethodHandler = StiDataHelper.GetFilterEventHandler(tempBand, tempBand);
            }
			#endregion

			var masterSource = Dictionary.DataSources[this.NameInSource];
			if (masterSource == null)
				throw new ArgumentException(string.Format("'{0}' filter. Datasource '{1}' is not found.", this.Name, this.NameInSource));

			#region Create Sort
			var listSort = new List<string>();
			foreach (string group in GroupColumns)
			{
				string groupField = group;
                if (groupField.StartsWith("DESC", StringComparison.InvariantCulture))
				{
                    if (masterSource.Columns[groupField] != null && masterSource.Columns[groupField.Substring(4)] == null)
                    {
                        listSort.Add("ASC");
                    }
                    else
                    {
                        listSort.Add("DESC");
                        groupField = groupField.Substring(4);
                    }
				}
                else if (groupField.StartsWith("NONE", StringComparison.InvariantCulture)) continue;
				else listSort.Add("ASC");
				
				string []strs = groupField.Split(new char[]{'.'});
				int index = 0;
				foreach (string str in strs)
				{
					string col = str;
					if ((index == 0 && strs.Length > 1) || index < strs.Length - 1)
					{
						col = StiDataColumn.GetRelationName(Dictionary, masterSource, str);
					}
					listSort.Add(col);
				}
			}
			foreach (string strSort in Sort)
			{
				listSort.Add(strSort);
			}
			var sort = listSort.ToArray();

            if (sort.Length == 0) 
                sort = null;
			#endregion

            masterSource.ResetDetailsRows();
            masterSource.First();
			masterSource.SetSort(null, sort, null, null, null);
			masterSource.SetFilter(filterMethodHandler);

			#region Create Table
			DataTable table = new DataTable();

			foreach (StiDataColumn column in Columns)
			{
				DataColumn dataColumn = new DataColumn(column.Name, column.Type);
				table.Columns.Add(dataColumn);
			}
			#endregion

			#region Prepare Totals
			bool totalsExist = false;

			var totalsHash = new Hashtable();
            var totalsList = new List<StiAggregateFunctionService>();

			foreach (string group in GroupColumns)
			{
				string groupField = group;
                if (groupField.StartsWith("DESC", StringComparison.InvariantCulture))
                {
                    if (!(masterSource.Columns[groupField] != null && masterSource.Columns[groupField.Substring(4)] == null))
                    {
                        groupField = groupField.Substring(4);
                    }
                }
                else if (groupField.StartsWith("NONE", StringComparison.InvariantCulture)) groupField = groupField.Substring(4);


				var totalGroup = new StiFirstFunctionService();
				totalsHash[groupField] = totalGroup;
				totalsList.Add(totalGroup);
			}

			int totalIndex = 0;
			while (totalIndex < this.results.Length)
			{
				string column = results[totalIndex++];
				string function = results[totalIndex++];
				string name = results[totalIndex++];

				switch (function)
				{
					case "Sum":
						StiAggregateFunctionService total1 = new StiSumDecimalFunctionService();
						totalsHash[name] = total1;
						totalsList.Add(total1);
						totalsExist = true;
						break;

                    case "SumDistinct":
                        StiAggregateFunctionService total10 = new StiSumDistinctDecimalFunctionService();
                        totalsHash[name] = total10;
                        totalsList.Add(total10);
                        totalsExist = true;
                        break;

					case "Count":
						StiAggregateFunctionService total7 = new StiCountFunctionService();
						totalsHash[name] = total7;
						totalsList.Add(total7);
						totalsExist = true;
						break;

					case "CountDistinct":
						StiAggregateFunctionService total8 = new StiCountDistinctFunctionService();
						totalsHash[name] = total8;
						totalsList.Add(total8);
						totalsExist = true;
						break;

					case "Min":
						StiAggregateFunctionService total2 = new StiMinDecimalFunctionService();
						totalsHash[name] = total2;
						totalsList.Add(total2);
						totalsExist = true;
						break;

					case "Max":
						StiAggregateFunctionService total3 = new StiMaxDecimalFunctionService();
						totalsHash[name] = total3;
						totalsList.Add(total3);
						totalsExist = true;
						break;

					case "Avg":
						StiAggregateFunctionService total4 = new StiAvgDecimalFunctionService();
						totalsHash[name] = total4;
						totalsList.Add(total4);
						totalsExist = true;
						break;
					
					case "First":
						StiAggregateFunctionService total5 = new StiFirstFunctionService();
						totalsHash[name] = total5;
						totalsList.Add(total5);
						totalsExist = true;
						break;

					case "Last":
						StiAggregateFunctionService total6 = new StiLastFunctionService();
						totalsHash[name] = total6;
						totalsList.Add(total6);
						totalsExist = true;
						break;

					case "MinDate":
						StiAggregateFunctionService total11 = new StiMinDateFunctionService();
						totalsHash[name] = total11;
						totalsList.Add(total11);
						totalsExist = true;
						break;

					case "MaxDate":
						StiAggregateFunctionService total12 = new StiMaxDateFunctionService();
						totalsHash[name] = total12;
						totalsList.Add(total12);
						totalsExist = true;
						break;

                    case "MinTime":
                        StiAggregateFunctionService total1_1 = new StiMinTimeFunctionService();
                        totalsHash[name] = total1_1;
                        totalsList.Add(total1_1);
                        totalsExist = true;
                        break;

                    case "MaxTime":
                        StiAggregateFunctionService total1_2 = new StiMaxTimeFunctionService();
                        totalsHash[name] = total1_2;
                        totalsList.Add(total1_2);
                        totalsExist = true;
                        break;

                    case "MinStr":
                        StiAggregateFunctionService total1_3 = new StiMinStrFunctionService();
                        totalsHash[name] = total1_3;
                        totalsList.Add(total1_3);
                        totalsExist = true;
                        break;

                    case "MaxStr":
                        StiAggregateFunctionService total1_4 = new StiMaxStrFunctionService();
                        totalsHash[name] = total1_4;
                        totalsList.Add(total1_4);
                        totalsExist = true;
                        break;

                    case "Mode":
                        StiAggregateFunctionService total1_5 = new StiModeDecimalFunctionService();
                        totalsHash[name] = total1_5;
                        totalsList.Add(total1_5);
                        totalsExist = true;
                        break;

                    case "Median":
                        StiAggregateFunctionService total1_6 = new StiMedianDecimalFunctionService();
                        totalsHash[name] = total1_6;
                        totalsList.Add(total1_6);
                        totalsExist = true;
                        break;

					default:
						StiAggregateFunctionService total0 = new StiFirstFunctionService();
						totalsHash[name] = total0;
						totalsList.Add(total0);
						break;
				}
			}
			#endregion

			#region Fill Data
			bool groupExist = GroupColumns.Length > 0;

			//Collection of previous values
			object[] prevValues = new object[this.GroupColumns.Length];

			//Is first row
            bool firstRow = true;
			int rowCount = 0;

			if ((!groupExist) && totalsExist)InitTotals(totalsList);

			bool needAddRow = (!totalsExist) && (!groupExist);

			masterSource.First();
			Dictionary.Report.Line = 1;
			Dictionary.Report.LineThrough = 1;
			while (!masterSource.IsEof)
			{
				#region Group Exist
				if (groupExist)
				{
					#region Prepare Current groups values
					object[] currentValues = new object[this.GroupColumns.Length];
					int groupIndex = 0;
					foreach (string group in this.GroupColumns)
					{
						string groupField = group;
                        if (groupField.StartsWith("DESC", StringComparison.InvariantCulture))
                        {
                            if (!(masterSource.Columns[groupField] != null && masterSource.Columns[groupField.Substring(4)] == null))
                            {
                                groupField = groupField.Substring(4);
                            }
                        }
                        else if (groupField.StartsWith("NONE", StringComparison.InvariantCulture)) groupField = groupField.Substring(4);

						currentValues[groupIndex++] = StiDataColumn.GetDataFromDataColumn(Dictionary, StiNameValidator.CorrectName(masterSource.Name, Dictionary.Report) + "." + groupField, false);
					}
					#endregion

					bool newGroup = false;
				
					if (firstRow)InitTotals(totalsList);
					else newGroup = Compare(prevValues, currentValues);
				
					if (newGroup)
					{					
						AddRow(table, totalsHash);
						InitTotals(totalsList);
						rowCount = 0;
					}
				
					prevValues = currentValues;
					firstRow = false;
				}
				#endregion
			
				DataRow row = null;
				//Create new row if result table does not contain totals or groups
				if (needAddRow)row = table.NewRow();

				#region Fill columns from group
				try
				{
					foreach (string group in GroupColumns)
					{
						string groupField = group;
                        if (groupField.StartsWith("DESC", StringComparison.InvariantCulture))
                        {
                            if (!(masterSource.Columns[groupField] != null && masterSource.Columns[groupField.Substring(4)] == null))
                            {
                                groupField = groupField.Substring(4);
                            }
                        }
                        else if (groupField.StartsWith("NONE", StringComparison.InvariantCulture)) groupField = groupField.Substring(4);

						object rowObject = StiDataColumn.GetDataFromDataColumn(Dictionary, StiNameValidator.CorrectName(masterSource.Name, Dictionary.Report) + "." + groupField, false);
						if (rowObject == null)rowObject = DBNull.Value;

						if (needAddRow)row[group] = rowObject;
						else
						{
							StiAggregateFunctionService total = totalsHash[groupField] as StiAggregateFunctionService;
							if (total != null)total.CalcItem(rowObject);
						}
					}
				}
				catch
				{
				}
				#endregion
				
				#region Fill columns from result
				int resultIndex = 0;
				while (resultIndex < this.results.Length)
				{
					try
					{
						string column = results[resultIndex++];
						string function = results[resultIndex++];
						string name = results[resultIndex++];

						object rowObject = DBNull.Value;
						if (!string.IsNullOrEmpty(column))
						{
							rowObject = StiDataColumn.GetDataFromDataColumn(Dictionary, StiNameValidator.CorrectName(masterSource.Name, Dictionary.Report) + "." + StiNameValidator.CorrectName(column, false), false);
							if (rowObject == null)rowObject = DBNull.Value;
						}
						
						if (needAddRow)row[name] = rowObject;
						else
						{
							StiAggregateFunctionService total = totalsHash[name] as StiAggregateFunctionService;
							if (total != null)total.CalcItem(rowObject);
						}
					}
					catch
					{
					}
				}
				#endregion

				rowCount ++;				

				//Add created row to result table does not contain totals or groups
				if (needAddRow)table.Rows.Add(row);

				//Move on next line
                masterSource.Next();

				Dictionary.Report.Line++;
				Dictionary.Report.LineThrough++;
			}

			if (rowCount > 0 && (totalsExist || groupExist))
			{
				AddRow(table, totalsHash);
			}
			#endregion

			base.DataTable = table;

			masterSource.ResetData();
			masterSource.First();
		}


		private bool Compare(object[] values1, object[] values2)
		{
			int index = 0;
			foreach (object obj in values1)
			{
                if (obj == null)
                {
                    if (values2[index] == null)
                    {
                        index++;
                        continue;
                    }
                    else
                    {
                        return true;
                    }
                }
				if (!obj.Equals(values2[index++]))return true;
			}
			return false;
		}


        private void InitTotals(List<StiAggregateFunctionService> totalsList)
		{
			foreach (var total in totalsList)
			{
				total.Init();
			}
		}


		private void AddRow(DataTable table, Hashtable totalsHash)
		{
			DataRow row = table.NewRow();
			foreach (StiDataColumn column in this.Columns)
			{
				StiAggregateFunctionService total = totalsHash[column.Name] as StiAggregateFunctionService;
                if (total != null)
                {
                    object value = total.GetValue();
                    if (value == null) value = DBNull.Value;
                    row[column.Name] = value;
                }
			}
			table.Rows.Add(row);
		}
        #endregion

        #region Methods.override
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiVirtualSource;
            }
        }

        public override StiDataSource CreateNew()
        {
            return new StiVirtualSource();
        }
        #endregion

        #region this
        /// <summary>
		/// Creates a new object of the type StiVirtualSource.
		/// </summary>
		public StiVirtualSource()
		{
		}


		/// <summary>
		/// Creates a new object of the type StiVirtualSource.
		/// </summary>
		public StiVirtualSource(string nameInSource, string name) : base(nameInSource, name)
		{
			this.ConnectionOrder = (int)StiConnectionOrder.None;
		}


        /// <summary>
        /// Creates a new object of the type StiVirtualSource.
        /// </summary>
        public StiVirtualSource(string nameInSource, string name, string key) : base(nameInSource, name, name, key)
        {
            this.ConnectionOrder = (int)StiConnectionOrder.None;
        }
		#endregion
	}
}