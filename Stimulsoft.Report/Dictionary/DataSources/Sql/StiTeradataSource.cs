﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Dictionary;
using System.ComponentModel;
using Stimulsoft.Report.Dictionary.Design;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Dictionary
{
    [TypeConverter(typeof(StiSqlSourceConverter))]
    public class StiTeradataSource : StiSqlSource
    {
        #region Methods
        /// <summary>
        /// Returns new data connector for this datasource.
        /// </summary>
        /// <returns>Created connector.</returns>
        public override StiSqlDataConnector CreateConnector(string connectionString = null)
        {
            return StiTeradataConnector.Get(connectionString);
        }

        protected override Type GetDataAdapterType()
        {
            return typeof(StiTeradataAdapterService);
        }
        #endregion

        #region Methods.override
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiTeradataSource;
            }
        }

        public override StiDataSource CreateNew()
        {
            return new StiTeradataSource();
        }
        #endregion

        public StiTeradataSource() : this("", "", "")
		{
		}

		public StiTeradataSource(string dataName, string name) : this(dataName, name, name)
		{
		}

		public StiTeradataSource(string dataName, string name, string alias) : this(dataName, name, alias, string.Empty)
		{

		}

		public StiTeradataSource(string dataName, string name, string alias, string sqlCommand) : 
			base(dataName, name, alias, sqlCommand)			
		{
		}

		public StiTeradataSource(string dataName, string name, string alias, string sqlCommand, 
			bool connectOnStart) : 
		base(dataName, name, alias, sqlCommand, connectOnStart)
		{
		}

		public StiTeradataSource(string dataName, string name, string alias, string sqlCommand, 
			bool connectOnStart, bool reconnectOnEachRow) : 
			base(dataName, name, alias, sqlCommand, connectOnStart, reconnectOnEachRow)
		{
		}

        public StiTeradataSource(string dataName, string name, string alias, string sqlCommand, 
			bool connectOnStart, bool reconnectOnEachRow, int commandTimeout) : 
			base(dataName, name, alias, sqlCommand, connectOnStart, reconnectOnEachRow, commandTimeout)
		{
		}

        public StiTeradataSource(string dataName, string name, string alias, string sqlCommand,
            bool connectOnStart, bool reconnectOnEachRow, int commandTimeout, string key) :
            base(dataName, name, alias, sqlCommand, connectOnStart, reconnectOnEachRow, commandTimeout, key)
        {
        }
    }
}