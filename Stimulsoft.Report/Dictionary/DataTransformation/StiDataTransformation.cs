#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Dictionary.Design;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;

using Stimulsoft.Base.Meters;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Data.Engine;
using Stimulsoft.Data.Exceptions;
using Stimulsoft.Data.Helpers;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Extensions;

namespace Stimulsoft.Report.Dictionary
{
    /// <summary>
    /// Describes the object which helps in data transformation.
    /// </summary>
    [TypeConverter(typeof(StiDataTransformationConverter))]
	public class StiDataTransformation : 
        StiDataStoreSource,
        IStiDataSortCase,
        IStiDataFilterCase,
        IStiDataActionCase,
        IStiQueryObject
    {
        #region IStiJsonReportObject.Override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);
            
            jObject.AddPropertyJObject("SortRules", StiJsonReportObjectHelper.Serialize.JObjectCollection(SortRules, mode));
            jObject.AddPropertyJObject("FilterRules", StiJsonReportObjectHelper.Serialize.JObjectCollection(FilterRules, mode));
            jObject.AddPropertyJObject("ActionRules", StiJsonReportObjectHelper.Serialize.JObjectCollection(ActionRules, mode));
            
            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "SortRules":
                        SortRules.AddRange(((JObject)property.Value).Properties().Select(p => StiDataSortRule.LoadFromJson((JObject)p.Value)));
                        break;

                    case "FilterRules":
                        FilterRules.AddRange(((JObject)property.Value).Properties().Select(p => StiDataFilterRule.LoadFromJson((JObject)p.Value)));
                        break;

                    case "ActionRules":
                        ActionRules.AddRange(((JObject)property.Value).Properties().Select(p => StiDataActionRule.LoadFromJson((JObject)p.Value)));
                        break;
                }
            }
        }
        #endregion

	    #region IStiDataSortCase
	    [StiSerializable(StiSerializationVisibility.List)]
	    [Browsable(false)]
	    public List<StiDataSortRule> SortRules { get; set; } = new List<StiDataSortRule>();
	    #endregion

	    #region IStiDataFilterCase
	    [StiSerializable(StiSerializationVisibility.List)]
	    [Browsable(false)]
	    public List<StiDataFilterRule> FilterRules { get; set; } = new List<StiDataFilterRule>();
        #endregion

	    #region IStiDataActionCase
	    [StiSerializable(StiSerializationVisibility.List)]
	    [Browsable(false)]
	    public List<StiDataActionRule> ActionRules { get; set; } = new List<StiDataActionRule>();
        #endregion

        #region IStiRetrieval
        public List<string> RetrieveUsedDataNames()
        {
            return StiUsedDataHelper.GetMany(GetMeters());
        }
        #endregion

        #region IStiQueryObject
        public IStiAppDictionary GetDictionary()
        {
            return Dictionary;
        }

        public IEnumerable<IStiAppDataSource> GetDataSources(IEnumerable<string> dataNames)
        {
            var dict = GetDictionary();
            if (dict == null) return null;

            var dataSources = dict.FetchDataSources()
                .Where(d => !(d is StiDataTransformation) && (d.GetKey() != GetKey() || GetKey() == null));

            return StiDataSourcePicker.Fetch(this, dataNames, dataSources);
        }

        public string GetKey()
        {
            return Key;
        }
        #endregion

        #region DataAdapter
        protected override Type GetDataAdapterType()
        {
            return typeof(StiDataTransformationAdapterService);
        }
        #endregion

        #region Methods
        public DataTable RetrieveDataTable()
        {
            var dataTable = StiDataAnalyzer.Analyse(this, this.GetMeters());
            var types = this.Columns.ToList().Select(c => c.Type).ToArray();
            return StiDataTableConverter.ToNetTable(dataTable, types);
        }

        internal void ConnectToData()
        {
            DataTable = RetrieveDataTable();
            if (DataTable == null) return;
            
            var columns = Columns.Cast<StiDataColumn>();            
            var filter = StiDataFilterRuleHelper.GetDataTableFilterQuery(FilterRules, columns);
            var sort = StiDataSortRuleHelper.GetDataTableSortQuery(SortRules, columns);

            if (filter.Length > 0 || sort.Length > 0)
            {
                DataTable.DefaultView.RowFilter = filter;
                DataTable.DefaultView.Sort = sort;

                DataTable = DataTable.DefaultView.ToTable();
            }
        }

        public List<IStiMeter> GetMeters()
	    {
	        return Columns
	            .ToList()
	            .Where(c => c is StiDataTransformationColumn)
                .Cast<StiDataTransformationColumn>()
	            .Select(GetMeter).ToList();
	    }

	    private IStiMeter GetMeter(StiDataTransformationColumn column)
	    {
	        switch (column.Mode)
	        {
	            case StiDataTransformationMode.Dimension:
	                return new StiDimensionTransformationMeter(column.Expression, column.Name);

	            case StiDataTransformationMode.Measure:
	                return new StiMeasureTransformationMeter(column.Expression, column.Name);

                default:
                    throw new StiTypeNotRecognizedException(column.Mode);
            }
	    }

        /// <summary>
        /// Get all possible data links for transformation columns
        /// </summary>
        public List<StiDataLink> GetDataLinks()
        {
            var dataSources = this.GetDataSources(null)?.Cast<StiDataSource>();
            if (dataSources == null || dataSources.Count() < 2)
                return new List<StiDataLink>();

            var primarySource = dataSources.First();
            var relations = primarySource.ParentRelationList()
                .Where(r => dataSources.Contains(r.ParentSource))
                .Union(primarySource.ChildRelationList().Where(r => dataSources.Contains(r.ChildSource)));

            return relations.Select(r => new StiDataLink(
                r.ParentSource.Name,
                r.ChildSource.Name,
                r.ParentColumns.FirstOrDefault(),
                r.ChildColumns.FirstOrDefault(),
                r.Active,
                r.Key)).ToList();
        }

        public StiDataRelation GetDataRelation(StiDataLink link)
        {
            if (link == null) return null;

            return Dictionary.Relations.ToList().FirstOrDefault(r =>
                (r.Key != null && r.Key == link.Key) ||
                (r.ParentSource.Name == link.ParentTable && r.ChildSource.Name == link.ChildTable &&
                 r.ParentColumns.FirstOrDefault() == link.ParentColumn && r.ChildColumns.FirstOrDefault() == link.ChildColumn));
        }
        #endregion

        #region Methods.Override
        public override StiComponentId ComponentId => StiComponentId.StiDataTransformation;

	    public override StiDataSource CreateNew()
        {
            return new StiDataTransformation();
        }
        #endregion

        /// <summary>
		/// Creates a new object of the type StiDataTransformation.
		/// </summary>
		public StiDataTransformation()
            : this(null, null)
		{
		}

        /// <summary>
        /// Creates a new object of the type StiDataTransformation.
        /// </summary>
        public StiDataTransformation(string nameInSource, string name) 
            : this(nameInSource, name, StiKeyHelper.GenerateKey())
		{
			this.ConnectionOrder = (int)StiConnectionOrder.None;
		}

        /// <summary>
        /// Creates a new object of the type StiDataTransformation.
        /// </summary>
        public StiDataTransformation(string nameInSource, string name, string key) : base(nameInSource, name, name, key)
        {
            ConnectionOrder = (int)StiConnectionOrder.None;
            Key = StiKeyHelper.GetOrGeneratedKey(key);
        }
	}
}