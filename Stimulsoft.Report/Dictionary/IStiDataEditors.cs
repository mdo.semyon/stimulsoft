#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dictionary
{
	public interface IStiDataEditors
	{
        DialogResult CsvDatabaseEdit(StiCsvDatabase database, StiDictionary dictionary, bool newDatabase);

        DialogResult DBaseDatabaseEdit(StiDBaseDatabase database, StiDictionary dictionary, bool newDatabase);

        DialogResult ExcelDatabaseEdit(StiExcelDatabase database, StiDictionary dictionary, bool newDatabase);

        DialogResult JsonDatabaseEdit(StiJsonDatabase database, StiDictionary dictionary, bool newDatabase);

        DialogResult SqlDatabaseEdit(StiSqlDatabase database, StiDictionary dictionary, bool newDatabase);

        DialogResult NoSqlDatabaseEdit(StiNoSqlDatabase database, StiDictionary dictionary, bool newDatabase);

		DialogResult XmlDatabaseEdit(StiXmlDatabase database, StiDictionary dictionary, bool newDatabase);
		
		StiUserNameAndPassword PromptUserNameAndPassword();
		
		bool DataStoreAdapterEdit(StiDataAdapterService adapter, StiDictionary dictionary, StiDataSource dataSource);
		
		bool DataStoreAdapterNew(StiDataAdapterService adapter, StiDictionary dictionary, StiDataSource dataSource);
		
		bool VirtualAdapterEdit(StiVirtualAdapterService adapter, StiDictionary dictionary, StiDataSource dataSource);

		bool VirtualAdapterNew(StiVirtualAdapterService adapter, StiDictionary dictionary, StiDataSource dataSource);

        bool CrossTabAdapterNew(StiCrossTabAdapterService adapter, StiDictionary dictionary, StiDataSource dataSource);

        bool CrossTabAdapterEdit(StiCrossTabAdapterService adapter, StiDictionary dictionary, StiDataSource dataSource);
	}
}
