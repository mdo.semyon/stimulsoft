#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using Stimulsoft.Base.Drawing;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
using Stimulsoft.System.Windows.Forms.PropertyGridInternal;
#else
using System.Windows.Forms;
using System.Windows.Forms.PropertyGridInternal;
#endif

namespace Stimulsoft.Base.Design
{
    /// <summary>
    /// Describes the control StiPropertyGrid.
    /// </summary>
    [ToolboxItem(false)]
    public class StiPropertyGrid : PropertyGrid
    {
        #region Properties
        public int ScrollOffset
        {
            get
            {
                var field = typeof(PropertyGrid).GetField("gridView", BindingFlags.NonPublic | BindingFlags.Instance);
                if (field != null)
                {
                    var obj = field.GetValue(this);
                    var method = obj.GetType().GetMethod("GetScrollOffset", BindingFlags.Public | BindingFlags.InvokeMethod | BindingFlags.Instance);
                    var value = method.Invoke(obj, new object[0]);
                    if (value is int) return (int)value;
                }
                return 0;
            }
            set
            {
                var field = typeof(PropertyGrid).GetField("gridView", BindingFlags.NonPublic | BindingFlags.Instance);
                if (field == null) return;

                var obj = field.GetValue(this);
                var method = obj.GetType().GetMethod("SetScrollOffset", BindingFlags.Public | BindingFlags.InvokeMethod | BindingFlags.Instance);
                method.Invoke(obj, new object[] { value });
            }
        }

        public override bool HelpVisible
        {
            get
            {
                return StiPropertyGridOptions.ShowDescription;
            }
            set
            {
                base.HelpVisible = StiPropertyGridOptions.ShowDescription;
            }
        }
        #endregion

        #region Methods
        private static string GetPropertyKey(Type type, string propertyName)
        {
            return GetPropertyKey(type.ToString(), propertyName);
        }

        private static string GetPropertyKey(string type, string propertyName)
        {
            return $"{type}::{propertyName}";
        }

        public static void HideProperty(Type type, string propertyName)
        {
            HideProperty(type.ToString(), propertyName);
        }

        public static void HideProperty(string type, string propertyName)
        {
            var str = GetPropertyKey(type, propertyName);
            hidedProperties[str] = str;
        }

        public static void ShowProperty(Type type, string propertyName)
        {
            ShowProperty(type.ToString(), propertyName);
        }

        public static void ShowProperty(string type, string propertyName)
        {
            var str = GetPropertyKey(type, propertyName);
            if (hidedProperties[str] != null) hidedProperties.Remove(str);
        }

        public static bool IsAllowedProperty(Type type, string propertyName)
        {
            if (type == typeof(Font))
                return !(propertyName == "GdiCharSet" || propertyName == "GdiVerticalFont" || propertyName == "Unit");

            var str1 = GetPropertyKey(type, propertyName);
            var str2 = GetPropertyKey("All", propertyName);

            return hidedProperties[str1] == null && hidedProperties[str2] == null;
        }

        public static bool IsAllowedProperty(string propertyName)
        {
            var str = GetPropertyKey("All", propertyName);

            return hidedProperties[str] == null;
        }

        public virtual void SetStandardPropertyTabs()
        {
            PropertyTabs.RemoveTabType(typeof(PropertiesTab));
            PropertyTabs.AddTabType(typeof(StiPropertiesTab), PropertyTabScope.Document);

            SetPropertyGrid();
        }

        public virtual void SetServicesPropertyTabs()
        {
            PropertyTabs.RemoveTabType(typeof(PropertiesTab));
            PropertyTabs.AddTabType(typeof(StiServicesTab), PropertyTabScope.Document);

            SetPropertyGrid();
        }

        private void SetPropertyGrid()
        {
            foreach (object tab in this.PropertyTabs)
            {
                var propTab = tab as StiPropertiesTab;
                if (propTab != null)
                    propTab.PropertyGrid = this;
            }
        }
        protected override void OnLayout(LayoutEventArgs e)
        {
            base.OnLayout(e);

            if (toolStrip != null && toolStrip.Items.Count == 7)
            {
                var stepScaleName = SystemWinApi.GetWindowsStepScaleName();
                toolStrip.Items[0].Image = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Office2013.Categorized{stepScaleName}.png");
                toolStrip.Items[1].Image = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Office2013.Alphabetical{stepScaleName}.png");
                toolStrip.Items[3].Image = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Office2013.Properties{stepScaleName}.png");
                toolStrip.Items[4].Image = StiImageUtils.GetImage("Stimulsoft.Report", $"Stimulsoft.Report.Bmp.Office2013.Events{stepScaleName}.png");
            }
        }
        private void PropertyGridView_FontChanged(object sender, EventArgs e)
        {
            foreach (Control control in this.Controls)
            {
                Type controlType = control.GetType();
                if (controlType.Name == "PropertyGridView")
                {
                    var scale = SystemWinApi.GetWindowsScale();
                    var cachedRowHeightField = controlType.GetField("cachedRowHeight", BindingFlags.NonPublic | BindingFlags.Instance);
                    cachedRowHeightField.SetValue(control, (Int32)(control.Font.Height + (int)Math.Ceiling(2 * scale.ScaleY)));

                    var outlineSizeExplorerTreeStyleField = controlType.GetField("outlineSizeExplorerTreeStyle", BindingFlags.NonPublic | BindingFlags.Static);
                    outlineSizeExplorerTreeStyleField.SetValue(control, control.Font.Height);
                }
            }

        }

        private void DropDownListBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            var listBox = sender as ListBox;
            Graphics g = e.Graphics;
            Rectangle itemRect = e.Bounds;
            itemRect.Height++;
            if ((e.State & DrawItemState.Selected) > 0) itemRect.Width--;
            DrawItemState state = e.State;

            if (e.Index != -1 && listBox.Items.Count > 0)
            {
                StiControlPaint.DrawItem(g, itemRect, state, listBox.GetItemText(listBox.Items[e.Index]),
                    null, 0, listBox.Font, listBox.BackColor, listBox.ForeColor, listBox.RightToLeft);
            }
        }

        #endregion

        #region Fields
        private static Hashtable hidedProperties = new Hashtable();
        private ToolStrip toolStrip;
        #endregion



        public StiPropertyGrid()
        {
            //var SelectedItemWithFocusBackColorProperty = typeof(PropertyGrid).GetProperty("SelectedItemWithFocusBackColor");
            //SelectedItemWithFocusBackColorProperty.SetValue(this, Color.FromArgb(185, 217, 244), null);

            //var SelectedItemWithFocusForeColorProperty = typeof(PropertyGrid).GetProperty("SelectedItemWithFocusForeColor");
            //SelectedItemWithFocusForeColorProperty.SetValue(this, Color.Black, null);

            this.LineColor = Color.FromArgb(198, 198, 198);
            this.HelpBackColor = Color.White;

            foreach (Control control in this.Controls)
            {
                Type controlType = control.GetType();
                if (controlType.Name == "PropertyGridView")
                {
                    var scale = SystemWinApi.GetWindowsScale();
                    
                    control.FontChanged += PropertyGridView_FontChanged;
                    control.Font = new Font(control.Font.FontFamily, (float)(control.Font.Size/* + (2.6 * (scale.ScaleY - 1))*/), control.Font.Style);

                    var paintWidthField = controlType.GetField("paintWidth", BindingFlags.NonPublic | BindingFlags.Static);
                    paintWidthField.SetValue(control, (Int32)Math.Ceiling(20 * scale.ScaleX));

                    var paintIndentField = controlType.GetField("paintIndent", BindingFlags.NonPublic | BindingFlags.Static);
                    paintIndentField.SetValue(control, (Int32)Math.Ceiling(26 * scale.ScaleX));

                    var DropDownListBoxProperty = controlType.GetProperty("DropDownListBox", BindingFlags.NonPublic | BindingFlags.Instance);
                    var DropDownListBox = DropDownListBoxProperty.GetValue(control, null) as ListBox;
                    DropDownListBox.DrawMode = DrawMode.OwnerDrawVariable;
                    DropDownListBox.DrawItem += DropDownListBox_DrawItem;

                }
                if (controlType.Name == "ToolStrip")
                {
                    var stepScale = SystemWinApi.GetWindowsStepScale();
                    var imageSize = (int)Math.Ceiling(16 * stepScale);
                    var normalButtonSizeField = typeof(PropertyGrid).GetField("normalButtonSize", BindingFlags.NonPublic | BindingFlags.Static);
                    normalButtonSizeField.SetValue(this, new Size(imageSize, imageSize));

                    toolStrip = control as ToolStrip;
                    toolStrip.ImageScalingSize = new Size(imageSize, imageSize);
                    toolStrip.GripMargin = new Padding((int)Math.Ceiling(2 * stepScale));

                    var SetupToolbarMethod = typeof(PropertyGrid).GetMethod("SetupToolbar", BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(bool) }, null);
                    SetupToolbarMethod.Invoke(this, new object[] { true });
                }
            }
        }
    }
}