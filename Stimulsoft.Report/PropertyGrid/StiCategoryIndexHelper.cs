#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

namespace Stimulsoft.Base.Design
{	
	internal sealed class StiCategoryIndexHelper
	{
		public static int GetCategoryIndex(string name)
		{
			switch (name)
			{
                case "ValueOpen":
                    return 24;
                                    
                case "Cell":
                case "Value":
                case "ValueEnd":                
                case "ValueClose":
                case "ValueHigh":
                case "ValueLow":
                    return 25;

                case "Argument":
                    return 50;

                case "Weight":
                    return 75;

                case "Map":
                case "Progress":
                case "Main":
				case "Text":
				case "SubReport":
				case "Image":
				case "BarCode":
				case "Check":
				case "Primitive":
				case "Shape":
				case "ZipCode":
				case "CrossTab":
				case "Page":
				case "Chart":
				case "WinControl":
				case "Hierarchical":
				case "Description":
                case "Gauge":
                case "Scale":
                case "Tick":
                case "Indicator":
					return 100;

                case "Data":
                    return 200;

				case "TextAdditional":
				case "ImageAdditional":
				case "BarCodeAdditional":
				case "PageAdditional":
					return 250;				

                case "Table":
                    return 260;

                case "HeaderTable":
                    return 261;

                case "FooterTable":
                    return 262;

				case "PageColumnBreak":				
					return 280;

				case "Position":
				case "Columns":
					return 300;

				case "Appearance":
					return 400;

				case "Behavior":
					return 450;

				case "Design":
					return 460;

				case "Navigation":
					return 470;

				case "Export":
					return 500;							
				
				case "Colors":
					return 500;

				case "Control":
					return 500;

				case "ControlsEvents":
					return 500;

				case "Display":
					return 500;
				
				case "ExportEvents":
					return 500;
				
				case "Marker":
					return 500;

				case "Misc":
					return 500;

				case "Options":
					return 500;

				case "Parameters":
					return 500;

				case "ReportStop":
					return 500;				

				case "Globalization":
					return 600;

				case "Script":
					return 700;

				case "MouseEvents":
				case "NavigationEvents":
				case "PrintEvents":
				case "RenderEvents":
				case "ValueEvents":		
					return 0;
			}

			return int.MaxValue;
		}
	}
}
