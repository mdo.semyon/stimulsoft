﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Globalization;
using System.Collections;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Data.Engine;

namespace Stimulsoft.Report
{
    public partial class StiReport
    {
        #region Properties
        private bool needsCompiling;
        /// <summary>
        /// Gets or sets a value which indicates whether it is necessary to compile a report or it has just been compiled.
        /// </summary>
        [Browsable(false)]
        [StiBrowsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool NeedsCompiling
        {
            get
            {
                return CalculationMode != StiCalculationMode.Interpretation && needsCompiling;
            }
            set
            {
                needsCompiling = value;
            }
        }

        /// <summary>
        /// Gets or sets compiler results.
        /// </summary>
        [Browsable(false)]
        [StiBrowsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public CompilerResults CompilerResults { get; set; }

        /// <summary>
        /// Gets or sets a compiled report.
        /// </summary>
        [Browsable(false)]
        [StiBrowsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public StiReport CompiledReport { get; set; }
        #endregion

        #region Methods
        private void CreateInstance(string path)
        {
            var compiledInMemory = string.IsNullOrEmpty(path);

            if (!compiledInMemory)
                CompiledReport = GetReportFromAssembly(path);
            else
            {
                CompiledReport = GetReportFromAssembly(CompilerResults.CompiledAssembly);
                CompiledReport.ReportVersion = this.ReportVersion;
                CompiledReport.ReportName = this.ReportName;
                CompiledReport.ReportAlias = this.ReportAlias;
                CompiledReport.ReportDescription = this.ReportDescription;
            }

            CompiledReport.Dictionary.DataStore.RegData(this.Dictionary.DataStore);
            CompiledReport.RegBusinessObject(this.BusinessObjectsStore);
            CompiledReport.GlobalizationManager = this.GlobalizationManager;
            CompiledReport.ReportFile = this.ReportFile;
            CompiledReport.IsWpf = this.IsWpf;

            if (compiledInMemory)
                StiReportResourcesLoader.LoadReportResourcesFromReport(this, CompiledReport);
            else
                StiReportResourcesLoader.LoadReportResourcesFromAssembly(CompiledReport);
        }

        /// <summary>
        /// Compiles a report.
        /// </summary>
        /// <param name="outputType">A parameter which sets a type of the output object.</param>
        public void Compile(StiOutputType outputType)
        {
            Compile("", outputType);
        }

        /// <summary>
        /// Compiles a report.
        /// </summary>
        /// <param name="path">A path for the report location.</param>
        public void Compile(string path)
        {
            Compile(path, StiOutputType.ClassLibrary);
        }

        /// <summary>
        /// Compiles a report.
        /// </summary>
        public void Compile()
        {
            Compile(StiOutputType.ClassLibrary);
        }

        /// <summary>
        /// Compiles a report and locates it to the specified path.
        /// </summary>
        /// <param name="path">A path for the report location.</param>
        /// <param name="outputType">A parameter which sets the type of the output object.</param>
        public void Compile(string path, StiOutputType outputType)
        {
            Compile(path, outputType, true);
        }

        /// <summary>
        /// Compiles a report and locates it in the specified stream.
        /// </summary>
        /// <param name="stream">A stream for the report location.</param>
        public void Compile(Stream stream)
        {
            Compile(null, stream, StiOutputType.ClassLibrary, true);
        }

        /// <summary>
        /// Compiles a report and locates it in the specified path.
        /// </summary>
        /// <param name="path">A parameter which sets the location of the compiled report.</param>
        /// <param name="outputType">A parameter which sets the type of the output object.</param>
        /// <param name="autoCreate">If true then the compiled report will be created.</param>
        public void Compile(string path, StiOutputType outputType, bool autoCreate)
        {
            Compile(path, null, outputType, autoCreate);
        }

        private string[] GetReferencedAssemblies()
        {
            var needRunningAssembly = false;
            var asms = new List<string>();

            #region Check datasources
            foreach (StiDataSource dataSource in Dictionary.DataSources)
            {
                #region StiOptions.Dictionary.BusinessObjects.AddBusinessObjectAssemblyToReferencedAssembliesAutomatically
                if (StiOptions.Dictionary.BusinessObjects.AddBusinessObjectAssemblyToReferencedAssembliesAutomatically)
                {
                    var dataStore = dataSource as StiDataStoreSource;
                    if (dataSource != null && !needRunningAssembly)
                    {
                        foreach (StiData data in this.DataStore)
                        {
                            if (!data.IsReportData && data.Name == dataStore.NameInSource && data.IsBusinessObjectData)
                            {
                                if (data.ViewData != null)
                                {
                                    var a = Assembly.GetEntryAssembly();
                                    if (a != null)
                                    {
                                        var path = Path.GetFileName(a.Location).ToLowerInvariant();
                                        if (ReferencedAssemblies.All(str => str.ToLowerInvariant() != path))
                                            asms.Add(a.Location);

                                        needRunningAssembly = true;
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            #endregion

            #region Find assembly location
            if (ReferencedAssemblies != null)
            {
                foreach (var referencedAssembly in ReferencedAssemblies)
                {
                    if (referencedAssembly == null || referencedAssembly.Trim().Length == 0) continue;

                    #region Check for postgre, firebird, VistaDb, SqlServerCe MySql, Oracle, DB2 and etc.
                    var refAssemblyStr = referencedAssembly.Trim().ToLowerInvariant();
                    #endregion

                    if (refAssemblyStr == "stimulsoft.controls.dll") continue;
                    if (refAssemblyStr == "stimulsoft.base.dll") continue;
                    if (refAssemblyStr == "stimulsoft.data.dll") continue;
                    if (refAssemblyStr == "stimulsoft.report.dll") continue;                    

                    #region Remove reference to old dlls
                    if (refAssemblyStr == "stimulsoft.report.design.dll") continue;
                    if (refAssemblyStr == "stimulsoft.report.odbcdatabase.dll") continue;
                    if (refAssemblyStr == "stimulsoft.report.oracledatabase.dll") continue;
                    #endregion

                    var assembly = StiAssemblyFinder.GetAssembly(referencedAssembly);
                    if (assembly == null)
                        throw new Exception($"Referenced assembly '{referencedAssembly}' not found");

                    asms.Add(assembly.Location);
                }
            }

            asms.Add(typeof(StiActivator).Assembly.Location);
            asms.Add(typeof(StiReport).Assembly.Location);
            asms.Add(typeof(StiDataSortDirection).Assembly.Location);

            if (this.GetType().Name != "StiReport")
                asms.Add(this.GetType().Assembly.Location);
            #endregion

            #region Remove Duplicates
            var asmHash = new Hashtable();
            foreach (var asm in asms)
            {
                asmHash[asm.ToLower(CultureInfo.InvariantCulture)] = asm;
            }
            #endregion

            #region Check Functions
            var functionAssemblies = StiFunctions.GetAssebliesOfFunctions();
            foreach (var functionAssembly in functionAssemblies)
            {
                if (asmHash[functionAssembly.ToLower(CultureInfo.InvariantCulture)] == null)
                    asmHash[functionAssembly.ToLower(CultureInfo.InvariantCulture)] = functionAssembly;
            }
            #endregion

            #region Charts
            try
            {
                var stimulsoftChartsAssembly = typeof(Stimulsoft.Report.Chart.StiChartOptions).Assembly.Location;
                asmHash[stimulsoftChartsAssembly] = stimulsoftChartsAssembly;
            }
            catch
            {
            }
            #endregion

            #region DBS-Dashboards
            try
            {
                if (StiDashboardAssembly.IsAssemblyLoaded)
                    asmHash[StiDashboardAssembly.Assembly.Location] = StiDashboardAssembly.Assembly.Location;
            }
            catch
            {
            }
            #endregion

            var refAsm = new string[asmHash.Keys.Count];
            asmHash.Values.CopyTo(refAsm, 0);

            return refAsm;
        }

        /// <summary>
        /// Compiles a report and locates it in the specified path.
        /// </summary>
        /// <param name="path">Path for the report location.</param>
        /// <param name="stream">A stream for the report location.</param>
        /// <param name="outputType">Type of output object.</param>
        /// <param name="autoCreate">If true then the compiled report will be created.</param>
        private void Compile(string path, Stream stream, StiOutputType outputType, bool autoCreate)
        {
            Compile(path, stream, outputType, autoCreate, null);
        }

        /// <summary>
        /// Compiles a standalone report and locates it in the specified path.
        /// </summary>
        /// <param name="path">Path for the report location.</param>
        public void CompileStandaloneReport(string path, StiStandaloneReportType standaloneReportType)
        {
            Compile(path, null, StiOutputType.WindowsApplication, false, standaloneReportType);
        }

        /// <summary>
        /// Compiles a report and locates it in the specified path.
        /// </summary>
        /// <param name="path">Path for the report location.</param>
        /// <param name="stream">A stream for the report location.</param>
        /// <param name="outputType">Type of output object.</param>
        /// <param name="autoCreate">If true then the compiled report will be created.</param>
        private void Compile(string path, Stream stream, StiOutputType outputType, bool autoCreate, object standaloneReportType)
        {
            if (CalculationMode == StiCalculationMode.Interpretation)
            {
                Report.Engine.StiParser.PrepareReportVariables(this);
                return;
            }
            if (!StiOptions.Engine.FullTrust) return;

            StiOptions.Engine.GlobalEvents.InvokeReportCompiling(this, EventArgs.Empty);

            if (!string.IsNullOrEmpty(path) && File.Exists(path))
                File.Delete(path);

            if (!this.NeedsCompiling)
                throw new Exception("Report already compiled");

            #region Compile to stream
            bool flagPathIsEmpty = string.IsNullOrEmpty(path);
            if (stream != null && flagPathIsEmpty)
            {
                var temp = Environment.GetEnvironmentVariable("Temp");
                if (temp == null)
                    throw new Exception("Can't get Temp directory");

                path = $"{temp}\\{Guid.NewGuid().ToString().Replace("-", "")}.dll";
            }
            #endregion

            if (StiOptions.Engine.AllowSetCurrentDirectory)
                Directory.SetCurrentDirectory(StiOptions.Configuration.ApplicationDirectory);

            StiLogService.Write(this.GetType(), "Compiling report");

            var refAsm = GetReferencedAssemblies();

            #region AutoLocalizeReportOnRun
            if (AutoLocalizeReportOnRun)
            {
                this.GlobalizationStrings.SkipException = true;

                var ci = CultureInfo.CurrentCulture;
                if (!string.IsNullOrWhiteSpace(Culture))
                {
                    try
                    {
                        ci = new CultureInfo(Culture, false);
                    }
                    catch
                    {
                    }
                }
                this.GlobalizationStrings.LocalizeReport(ci);
                this.GlobalizationStrings.SkipException = false;
            }
            #endregion

            #region RemoveUnusedDataBeforeReportRendering
            if (StiOptions.Dictionary.RemoveUnusedDataBeforeReportRendering)
                this.Dictionary.RemoveUnusedData();
            #endregion

            var resScript = this.Script;

            try
            {
                var inMemory = string.IsNullOrWhiteSpace(path);
                using (var resources = new StiReportResourcesCompiler(this, inMemory))
                {
                    ScriptUpdate(standaloneReportType, false);

                    #region LanguageType
                    var languageType = StiCompiler.LanguageType.CSharp;
                    if (this.ScriptLanguage == StiReportLanguageType.VB)
                        languageType = StiCompiler.LanguageType.VB;
                    #endregion

                    #region Sign Assembly
                    var reportScript = this.Script;

                    if (!string.IsNullOrEmpty(StiOptions.Engine.PathToAssemblyKeyFile) && File.Exists(StiOptions.Engine.PathToAssemblyKeyFile))
                    {
                        var assemblyKeyFile = StiOptions.Engine.PathToAssemblyKeyFile;
                        assemblyKeyFile = assemblyKeyFile.Replace("\\", "\\\\");
                        string attributeStr;
                        string namespaceStr;

                        if (this.ScriptLanguage == StiReportLanguageType.CSharp)
                        {
                            namespaceStr = "namespace";
                            attributeStr = $"[assembly: System.Reflection.AssemblyKeyFile(\"{assemblyKeyFile}\")]\n";
                        }
                        else
                        {
                            namespaceStr = "Namespace";
                            attributeStr = $"<Assembly: System.Reflection.AssemblyKeyFile(\"{assemblyKeyFile}\")>\n";
                        }

                        var namespaceIndex = reportScript.IndexOf(namespaceStr, StringComparison.InvariantCulture);
                        reportScript = reportScript.Insert(namespaceIndex, attributeStr);
                    }
                    #endregion

                    CompilerResults = StiCompiler.Compile(reportScript, path, languageType, outputType, refAsm, resources.Files);
                }
            }
            catch (Exception e)
            {
                #region Create Report Compilation Error
                if (CompilerResults == null)
                    CompilerResults = new CompilerResults(null);

                CompilerResults.Errors.Add(new CompilerError
                {
                    Column = -1,
                    Line = -1,
                    IsWarning = false,
                    ErrorText = e.Message
                });
                #endregion

                StiLogService.Write(this.GetType(), "Compiling report...ERROR");
                StiLogService.Write(this.GetType(), e);

                throw;
            }
            finally
            {
                this.Script = resScript;
            }

            #region CompilerResults
            var allowAssemblyCreation = true;
            if (CompilerResults != null && CompilerResults.Errors != null && CompilerResults.Errors.Count > 0 && CompilerResults.Errors.HasErrors)
            {
                var sb = new StringBuilder();
                foreach (CompilerError error in CompilerResults.Errors)
                {
                    sb.Append(error);
                    StiLogService.Write(this.GetType(), error.ToString());
                    if (!error.IsWarning) allowAssemblyCreation = false;
                }
                if (!this.IsDesigning)
                    throw new Exception(sb.ToString());
            }
            #endregion

            #region AssemblyIsCreated
            if (allowAssemblyCreation)
            {
                if (autoCreate)
                    CreateInstance(path);

                if (CompiledReport != null)
                    CompiledReport.Variables = this.Variables;

                if (StiOptions.Engine.AllowResetAssemblyAfterCompilation)
                    CompilerResults.CompiledAssembly = null;

                #region Write dll to stream
                if (stream != null)
                {
                    FileStream dllStream = null;
                    try
                    {
                        StiFileUtils.ProcessReadOnly(path);
                        dllStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                        var bytes = new byte[dllStream.Length];

                        dllStream.Read(bytes, 0, (int)dllStream.Length);
                        stream.Write(bytes, 0, bytes.Length);
                    }
                    finally
                    {
                        if (dllStream != null)
                            dllStream.Close();

                        if (flagPathIsEmpty && File.Exists(path))
                        {
                            File.Delete(path);
                        }
                    }
                }
                #endregion
            }
            #endregion

            IsDocument = false;

            StiOptions.Engine.GlobalEvents.InvokeReportCompiled(this, EventArgs.Empty);
        }

        public CompilerResults CompileReportsToAssembly(string assemblyPath, StiReport[] reports)
        {
            return CompileReportsToAssembly(assemblyPath, reports, StiReportLanguageType.CSharp);
        }

        public static CompilerResults CompileReportsToAssembly(string assemblyPath, StiReport[] reports, StiReportLanguageType languageType)
        {
            var sources = new string[reports.Length];
            var index = 0;
            var refAsm = new Hashtable();
            var resources = new StiReportResourcesCompiler(null, false);

            foreach (StiReport report in reports)
            {
                StiOptions.Engine.GlobalEvents.InvokeReportCompiling(report, EventArgs.Empty);

                resources.ProcessReport(report);

                report.ScriptUpdate();

                string textToCompile = report.Script;
                if (StiCompiler.AssemblyVersion != null)
                {
                    int namespaceIndex = textToCompile.IndexOf("namespace", StringComparison.Ordinal);
                    textToCompile = textToCompile.Insert(namespaceIndex, $"[assembly: System.Reflection.AssemblyVersion(\"{StiCompiler.AssemblyVersion}\")]\n");
                }
                sources[index++] = textToCompile;

                var asmStrs = report.GetReferencedAssemblies();
                foreach (var str in asmStrs)
                    refAsm[str] = str;

                StiOptions.Engine.GlobalEvents.InvokeReportCompiled(report, EventArgs.Empty);
            }

            var referencedAsms = new string[refAsm.Keys.Count];
            refAsm.Keys.CopyTo(referencedAsms, 0);

            var parms = new CompilerParameters();
            parms.ReferencedAssemblies.AddRange(referencedAsms);
            parms.OutputAssembly = assemblyPath;
            parms.CompilerOptions = parms.CompilerOptions + " /target:library";
            parms.GenerateExecutable = false;
            parms.ReferencedAssemblies.AddRange(referencedAsms);

            if (resources.Files != null)
            {
                foreach (var resource in resources.Files)
                {
                    parms.EmbeddedResources.Add(resource);
                }
            }

            var provider = languageType == StiReportLanguageType.CSharp 
                ? (CodeDomProvider) new Microsoft.CSharp.CSharpCodeProvider() 
                : new Microsoft.VisualBasic.VBCodeProvider();

            var results = provider.CompileAssemblyFromSource(parms, sources);

            provider.Dispose();
            resources.Dispose();

            return results;
        }
        #endregion
    }
}