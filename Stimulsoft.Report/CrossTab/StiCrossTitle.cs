#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.TextFormats;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

namespace Stimulsoft.Report.CrossTab
{
	public class StiCrossTitle : StiCrossField
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // Old
            jObject.RemoveProperty("TextFormat");
            jObject.RemoveProperty("HideZeros");
            jObject.RemoveProperty("Conditions");

            // StiCrossTitle
            jObject.AddPropertyBool("PrintOnAllPages", PrintOnAllPages, true);
            jObject.AddPropertyStringNullOrEmpty("TypeOfComponent", TypeOfComponent);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "PrintOnAllPages":
                        this.PrintOnAllPages = property.Value.ToObject<bool>();
                        break;

                    case "TypeOfComponent":
                        this.TypeOfComponent = property.Value.ToObject<string>();
                        break;
                }
            }
        }

        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiCrossTitle;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            // TextCategory
            var list = new[] 
            { 
                propHelper.Text()
            };
            objHelper.Add(StiPropertyCategories.Text, list);

            // TextAdditionalCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.TextBrush(),
                    propHelper.fAngle(),
                    propHelper.Font(),
                    propHelper.WordWrap()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[] 
                { 
                    propHelper.AllowHtmlTags(),
                    propHelper.TextBrush(),
                    propHelper.fAngle(),
                    propHelper.Font(),
                    propHelper.Margins(),
                    propHelper.WordWrap()
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.AllowHtmlTags(),
                    propHelper.TextBrush(),
                    propHelper.fAngle(),
                    propHelper.Font(),
                    propHelper.Margins(),
                    propHelper.TextOptions(),
                    propHelper.WordWrap()
                };
            }
            objHelper.Add(StiPropertyCategories.TextAdditional, list);

            if (level != StiLevel.Basic)
            {
                // PositionCategory
                list = new[] 
                { 
                    propHelper.MinSize(),
                    propHelper.MaxSize()
                };
                objHelper.Add(StiPropertyCategories.Position, list);
            }

            // AppearanceCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.Brush(),
                    propHelper.Border(),
                    propHelper.ComponentStyle(),
                    propHelper.HorAlignment(),
                    propHelper.VertAlignment()
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.Brush(),
                    propHelper.Border(),
                    propHelper.ComponentStyle(),
                    propHelper.HorAlignment(),
                    propHelper.VertAlignment(),
                    propHelper.UseParentStyles()
                };
            }
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.MergeHeaders(),
                    propHelper.Enabled(),
                    propHelper.PrintOnAllPages()
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.InteractionEditor(),
                    propHelper.MergeHeaders(),
                    propHelper.Enabled(),
                    propHelper.PrintOnAllPages()
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            return objHelper;
        }
        #endregion

        #region StiComponent override
        /// <summary>
        /// Gets a localized component name.
        /// </summary>
        public override string LocalizedName => StiLocalization.Get("Components", "StiCrossTitle");
        #endregion

        #region Properties
        /// <summary>
		/// Gets or sets value indicates that the component is printed on all pages.
		/// </summary>
		[DefaultValue(true)]
		[StiSerializable]
		[StiCategory("Behavior")]
		[StiOrder(StiPropertyOrder.BehaviorPrintOnAllPages)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value indicates that the component is printed on all pages.")]
		public virtual bool PrintOnAllPages { get; set; } = true;

        [StiSerializable]
		[Browsable(false)]
		public string TypeOfComponent { get; set; } = string.Empty;

        [Browsable(true)]
		public sealed override bool Enabled
		{
			get 
			{
				return base.Enabled;
			}
			set 
			{
				base.Enabled = value;
			}
		}

		[StiNonSerialized]
		[Browsable(false)]
		public override StiFormatService TextFormat
		{
			get 
			{
				return base.TextFormat;
			}
			set 
			{
			}
		}

		[StiNonSerialized]
		[Browsable(false)]
		public override bool HideZeros
		{
			get 
			{
				return base.HideZeros;
			}
			set 
			{
			}
		}

		[StiNonSerialized]
		[Browsable(false)]
		public override StiConditionsCollection Conditions
		{
			get
			{
				return base.Conditions;
			}
			set
			{
			}
		}

        public override string CellText => this.GetTextInternal();
        #endregion

        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiCrossTitle();
        }
        #endregion

        public StiCrossTitle()
		{
			Brush = new StiSolidBrush(Color.LightGray);
		}
	}
}