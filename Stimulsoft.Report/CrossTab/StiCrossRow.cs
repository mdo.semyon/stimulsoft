#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Report.CrossTab.Core;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using System;
using System.Linq;
using Stimulsoft.Report.Components;

namespace Stimulsoft.Report.CrossTab
{
    public class StiCrossRow : StiCrossHeader
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiCrossRow
            jObject.AddPropertyEnum("EnumeratorType", EnumeratorType, StiEnumeratorType.None);
            jObject.AddPropertyString("EnumeratorSeparator", EnumeratorSeparator, ".");

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "EnumeratorType":
                        this.EnumeratorType = (StiEnumeratorType)Enum.Parse(typeof(StiEnumeratorType), property.Value.ToObject<string>());
                        break;

                    case "EnumeratorSeparator":
                        this.EnumeratorSeparator = property.Value.ToObject<string>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiCrossRow;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var objHelper = new StiPropertyCollection();

            var propHelper = propertyGrid.PropertiesHelper;
            // DataCategory
            var list = new[] {
                propHelper.DisplayValue(),
                propHelper.SortDirection(),
                propHelper.SortType(),
                propHelper.Value()
            };
            objHelper.Add(StiPropertyCategories.Data, list);

            // TextAdditionalCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.TextBrush(),
                    propHelper.fAngle(),
                    propHelper.Font(),
                    propHelper.TextFormat(),
                    propHelper.WordWrap()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.AllowHtmlTags(),
                    propHelper.TextBrush(),
                    propHelper.fAngle(),
                    propHelper.Font(),
                    propHelper.Margins(),
                    propHelper.TextFormat(),
                    propHelper.WordWrap()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.AllowHtmlTags(),
                    propHelper.TextBrush(),
                    propHelper.fAngle(),
                    propHelper.Font(),
                    propHelper.Margins(),
                    propHelper.TextFormat(),
                    propHelper.TextOptions(),
                    propHelper.WordWrap()
                };
            }
            objHelper.Add(StiPropertyCategories.TextAdditional, list);

            if (level != StiLevel.Basic)
            {
                // PositionCategory
                list = new[]
                {
                    propHelper.MinSize(),
                    propHelper.MaxSize()
                };
                objHelper.Add(StiPropertyCategories.Position, list);
            }

            // AppearanceCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Brush(),
                    propHelper.Border(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle(),
                    propHelper.HorAlignment(),
                    propHelper.VertAlignment()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Brush(),
                    propHelper.Border(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle(),
                    propHelper.HorAlignment(),
                    propHelper.VertAlignment(),
                    propHelper.UseParentStyles()
                };
            }
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.EnumeratorSeparator(),
                    propHelper.EnumeratorType(),
                    propHelper.MergeHeaders(),
                    propHelper.PrintOnAllPages(),
                    propHelper.ShowTotal()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.EnumeratorSeparator(),
                    propHelper.EnumeratorType(),
                    propHelper.MergeHeaders(),
                    propHelper.PrintOnAllPages(),
                    propHelper.ShowTotal()
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            return objHelper;
        }
        #endregion

        #region Properties
        /// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName => StiLocalization.Get("Components", "StiCrossRow");

        [DefaultValue(StiEnumeratorType.None)]
        [StiSerializable]
        [StiCategory("Behavior")]
        [TypeConverter(typeof(StiEnumConverter))]
        public StiEnumeratorType EnumeratorType { get; set; } = StiEnumeratorType.None;

        [DefaultValue(".")]
        [StiSerializable]
        [StiCategory("Behavior")]
        public string EnumeratorSeparator { get; set; } = ".";
        #endregion

        #region Methods
        public StiCrossTitle GetCrossRowTitle()
        {
            var crossTab = this.Parent as StiCrossTab;
            if (crossTab == null) return null;

            var name = $"{this.Name}_Title";

            return crossTab.Components
                .ToList()
                .FirstOrDefault(c => c is StiCrossTitle && c.Name == name) as StiCrossTitle;

        }

        public StiCrossRowTotal GetCrossRowTotal()
        {
            var crossTab = this.Parent as StiCrossTab;
            if (crossTab == null) return null;


            var name = this.Name.Replace("Row", "RowTotal");

            return crossTab.Components
                .ToList()
                .FirstOrDefault(c => c is StiCrossRowTotal && c.Name == name) as StiCrossRowTotal;

        }
        #endregion

        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiCrossRow();
        }
        #endregion
    }
}