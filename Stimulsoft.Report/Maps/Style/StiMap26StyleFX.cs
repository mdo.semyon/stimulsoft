﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Dashboard;
using System.Drawing;

namespace Stimulsoft.Report.Maps
{
    public class StiMap26StyleFX : StiMapStyleFX
    {
        #region Properties
        public override bool AllowDashboard => true;

        public override StiElementStyleIdent StyleIdent => StiElementStyleIdent.Turquoise;

        public override StiMapStyleIdent StyleId => StiMapStyleIdent.Style26;

        public override string LocalizeName => StiLocalization.Get("Chart", "Style") + 26;

        public override Color IndividualColor
        {
            get
            {
                return ColorTranslator.FromHtml("#2ec6c8");
            }
            set { }
        }

        public override Color BorderColor
        {
            get
            {
                return Color.FromArgb(255, 211, 212, 213);
            }
            set { }
        }

        public override Color[] Colors
        {
            get
            {
                return new[]
                {
                    ColorTranslator.FromHtml("#2ec6c8"),
                    ColorTranslator.FromHtml("#b5a1dd"),
                    ColorTranslator.FromHtml("#5ab0ee"),
                    ColorTranslator.FromHtml("#f4984e"),
                    ColorTranslator.FromHtml("#d77a80"),
                    ColorTranslator.FromHtml("#d04456"),
                };
            }
            set
            {
            }
        }

        public override Color[] HeatmapColors
        {
            get
            {
                return new[]
                {
                    ColorTranslator.FromHtml("#2ec6c8"),
                    ColorTranslator.FromHtml("#f4984e"),
                };
            }
            set
            {
            }
        }

        public override Color DefaultColor
        {
            get
            {
                return ColorTranslator.FromHtml("#d0d0d0");
            }
            set
            {
            }
        }

        public override Color BackColor
        {
            get
            {
                return ColorTranslator.FromHtml("#ffffff");
            }
            set
            {
            }
        }

        public override Color ForeColor
        {
            get
            {
                return ColorTranslator.FromHtml("#ffffff");
            }
            set
            {
            }
        }
        #endregion
    }
}