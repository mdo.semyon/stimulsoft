﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Dashboard;
using System.Drawing;

namespace Stimulsoft.Report.Maps
{
    public class StiMap24StyleFX : StiMapStyleFX
    {
        #region Properties
        public override bool AllowDashboard => true;

        public override StiElementStyleIdent StyleIdent => StiElementStyleIdent.Orange;

        public override StiMapStyleIdent StyleId => StiMapStyleIdent.Style24;

        public override string LocalizeName => StiLocalization.Get("Chart", "Style") + 24;

        public override Color BorderColor
        {
            get
            {
                return Color.FromArgb(255, 211, 212, 213);
            }
            set { }
        }

        public override Color IndividualColor
        {
            get
            {
                return ColorTranslator.FromHtml("#ed7d31");
            }
            set { }
        }

        public override Color[] Colors
        {
            get
            {
                return new[]
                {
                    ColorTranslator.FromHtml("#ed7d31"),
                    ColorTranslator.FromHtml("#ffc000"),
                    ColorTranslator.FromHtml("#70ad47"),
                    ColorTranslator.FromHtml("#9e480e"),
                    ColorTranslator.FromHtml("#997300"),
                    ColorTranslator.FromHtml("#43682b")
                };
            }
            set { }
        }

        public override Color[] HeatmapColors
        {
            get
            {
                return new[]
                {
                    ColorTranslator.FromHtml("#ed7d31"),
                    ColorTranslator.FromHtml("#70ad47"),
                };
            }
            set
            {
            }
        }

        public override Color DefaultColor
        {
            get
            {
                return ColorTranslator.FromHtml("#d0d0d0");
            }
            set
            {
            }
        }

        public override Color BackColor
        {
            get
            {
                return ColorTranslator.FromHtml("#ffffff");
            }
            set
            {

            }
        }

        public override Color ForeColor
        {
            get
            {
                return ColorTranslator.FromHtml("#000000");
            }
            set
            {
            }
        }
        #endregion
    }
}