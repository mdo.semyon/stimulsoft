﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Localization;
using System.Drawing;

namespace Stimulsoft.Report.Maps
{
    public class StiMap21StyleFX : StiMapStyleFX
    {
        #region Properties
        public override StiMapStyleIdent StyleId => StiMapStyleIdent.Style21;

        public override string LocalizeName => StiLocalization.Get("Chart", "Style") + 21;

        public override Color IndividualColor
        {
            get
            {
                return ColorTranslator.FromHtml("#239fd9");
            }
            set { }
        }

        public override Color[] Colors
        {
            get
            {
                return new[]
                {
                    ColorTranslator.FromHtml("#239fd9"),
                    ColorTranslator.FromHtml("#b2b2b2"),
                    ColorTranslator.FromHtml("#55d1ff"),
                    ColorTranslator.FromHtml("#e4e4e4"),
                    ColorTranslator.FromHtml("#55d1ff"),
                    ColorTranslator.FromHtml("#e4e4e4")
                };
            }
            set
            {
            }
        }

        public override Color[] HeatmapColors
        {
            get
            {
                return new[]
                {
                    ColorTranslator.FromHtml("#239fd9"),
                    ColorTranslator.FromHtml("#b2b2b2")
                };
            }
            set
            {
            }
        }

        public override Color BackColor
        {
            get
            {
                return ColorTranslator.FromHtml("#666666");
            }
            set
            {
            }
        }

        public override Color DefaultColor
        {
            get
            {
                return ColorTranslator.FromHtml("#ffffff");
            }
            set
            {
            }
        }

        public override Color ForeColor
        {
            get
            {
                return ColorTranslator.FromHtml("#ffffff");
            }
            set
            {
            }
        }
        #endregion
    }
}