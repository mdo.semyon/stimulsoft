﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Localization;
using System.Drawing;

namespace Stimulsoft.Report.Maps
{
    public class StiMap27StyleFX : StiMapStyleFX
    {
        #region Properties
        public override StiMapStyleIdent StyleId => StiMapStyleIdent.Style27;

        public override string LocalizeName => StiLocalization.Get("Chart", "Style") + 27;

        public override Color IndividualColor
        {
            get
            {
                return ColorTranslator.FromHtml("#0bac45");
            }
            set { }
        }

        public override Color[] Colors
        {
            get
            {
                return new[]
                {
                    ColorTranslator.FromHtml("#0bac45"),
                    ColorTranslator.FromHtml("#585257"),
                    ColorTranslator.FromHtml("#ec334d"),
                    ColorTranslator.FromHtml("#a1ae94"),
                    ColorTranslator.FromHtml("#ed7d31"),
                    ColorTranslator.FromHtml("#5ab0ee"),
                };
            }
            set
            {
            }
        }

        public override Color[] HeatmapColors
        {
            get
            {
                return new[]
                {
                    ColorTranslator.FromHtml("#0bac45"),
                    ColorTranslator.FromHtml("#ec334d"),
                };
            }
            set
            {
            }
        }

        public override Color DefaultColor
        {
            get
            {
                return ColorTranslator.FromHtml("#ffffff");
            }
            set
            {
            }
        }

        public override Color BackColor
        {
            get
            {
                return ColorTranslator.FromHtml("#33475B");
            }
            set
            {

            }
        }

        public override Color ForeColor
        {
            get
            {
                return ColorTranslator.FromHtml("#ffffff");
            }
            set
            {

            }
        }
        #endregion
    }
}