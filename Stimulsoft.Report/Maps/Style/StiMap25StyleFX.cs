﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Dashboard;
using System.Drawing;

namespace Stimulsoft.Report.Maps
{
    public class StiMap25StyleFX : StiMapStyleFX
    {
        #region Properties
        public override bool AllowDashboard => true;

        public override StiElementStyleIdent StyleIdent => StiElementStyleIdent.Green;

        public override StiMapStyleIdent StyleId => StiMapStyleIdent.Style25;

        public override string LocalizeName => StiLocalization.Get("Chart", "Style") + 25;

        public override Color IndividualColor
        {
            get
            {
                return ColorTranslator.FromHtml("#70ad47");
            }
            set { }
        }

        public override Color BorderColor
        {
            get
            {
                return Color.FromArgb(255, 211, 212, 213);
            }
            set { }
        }

        public override Color[] Colors
        {
            get
            {
                return new[]
                {
                    ColorTranslator.FromHtml("#70ad47"),
                    ColorTranslator.FromHtml("#4472c4"),
                    ColorTranslator.FromHtml("#ffc000"),
                    ColorTranslator.FromHtml("#43682b"),
                    ColorTranslator.FromHtml("#fd6a37"),
                    ColorTranslator.FromHtml("#997300"),
                };
            }
            set
            {
            }
        }

        public override Color[] HeatmapColors
        {
            get
            {
                return new[]
                {
                    ColorTranslator.FromHtml("#70ad47"),
                    ColorTranslator.FromHtml("#ffc000"),
                };
            }
            set
            {
            }
        }

        public override Color DefaultColor
        {
            get
            {
                return ColorTranslator.FromHtml("#d0d0d0");
            }
            set
            {
            }
        }

        public override Color BackColor
        {
            get
            {
                return ColorTranslator.FromHtml("#ffffff");
            }
            set
            {
            }
        }

        public override Color ForeColor
        {
            get
            {
                return ColorTranslator.FromHtml("#ffffff");
            }
            set
            {
            }
        }
        #endregion
    }
}