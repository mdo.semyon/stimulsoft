﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.Maps.Helpers;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.PropertyGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Maps
{
    [StiToolbox(true)]
    [StiServiceBitmap(typeof(StiComponent), "Stimulsoft.Report.Images.Components.StiMap.png")]
    [StiServiceCategoryBitmap(typeof(StiComponent), "Stimulsoft.Report.Images.Components.catInfographics.png")]
    [StiGdiPainter(typeof(StiMapGdiPainter))]
    [StiDesigner("Stimulsoft.Report.Map.Design.StiMapDesigner, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiMapWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
    [StiWpfDesigner("Stimulsoft.Report.WpfDesign.StiWpfMapDesigner, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
    [StiV2Builder(typeof(StiMapV2Builder))]
    [StiV1Builder(typeof(StiMapV1Builder))]
    [StiV2Builder(typeof(StiMapV2Builder))]
    [StiContextTool(typeof(IStiComponentDesigner))]
    [StiContextTool(typeof(IStiDataSource))]
    public class StiMap : 
        StiComponent,
        IStiExportImageExtended,
        IStiBorder
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // NonSerialized
            jObject.RemoveProperty("UseParentStyles");
            jObject.RemoveProperty("CanGrow");
            jObject.RemoveProperty("CanShrink");
            jObject.RemoveProperty("GrowToHeight");
            jObject.RemoveProperty("Interaction");

            // StiGauge            
            jObject.AddPropertyStringNullOrEmpty("Border", StiJsonReportObjectHelper.Serialize.JBorder(Border));
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));

            jObject.AddPropertyEnum("MapStyle", MapStyle, StiMapStyleIdent.Style25);
            jObject.AddPropertyEnum("DataFrom", DataFrom, StiMapSource.Manual);

            jObject.AddPropertyBool("ColorEach", ColorEach);
            jObject.AddPropertyBool("Stretch", Stretch, true);
            jObject.AddPropertyBool("ShowValue", ShowValue, true);
            jObject.AddPropertyEnum("ShowName", DisplayNameType, StiDisplayNameType.Full);
            jObject.AddPropertyEnum("MapID", MapID, StiMapID.USA);
            jObject.AddPropertyEnum("MapMode", MapMode, StiMapMode.Choropleth);
            //jObject.AddPropertyBool("ShowLegend", ShowLegend);
            jObject.AddPropertyEnum("MapType", MapType, StiMapType.Group);
            jObject.AddPropertyStringNullOrEmpty("MapData", MapData);
            jObject.AddPropertyStringNullOrEmpty("KeyDataColumn", KeyDataColumn);
            jObject.AddPropertyStringNullOrEmpty("NameDataColumn", NameDataColumn);
            jObject.AddPropertyStringNullOrEmpty("ValueDataColumn", ValueDataColumn);
            jObject.AddPropertyStringNullOrEmpty("GroupDataColumn", GroupDataColumn);
            jObject.AddPropertyStringNullOrEmpty("ColorDataColumn", ColorDataColumn);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Border":
                        this.Border = StiJsonReportObjectHelper.Deserialize.Border(property);
                        break;

                    case "Brush":
                        this.Brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "MapStyle":
                        this.MapStyle = (StiMapStyleIdent)Enum.Parse(typeof(StiMapStyleIdent), property.Value.ToObject<string>());
                        break;

                    case "DataFrom":
                        this.DataFrom = (StiMapSource)Enum.Parse(typeof(StiMapSource), property.Value.ToObject<string>());
                        break;

                    case "Stretch":
                        this.Stretch = property.Value.ToObject<bool>();
                        break;

                    case "ShowValue":
                        this.ShowValue = property.Value.ToObject<bool>();
                        break;

                    case "ColorEach":
                        this.ColorEach = property.Value.ToObject<bool>();
                        break;

                    case "ShowName":
                        this.DisplayNameType = (StiDisplayNameType)Enum.Parse(typeof(StiDisplayNameType), property.Value.ToObject<string>());
                        break;

                    case "MapID":
                        this.MapID = (StiMapID)Enum.Parse(typeof(StiMapID), property.Value.ToObject<string>());
                        break;

                    case "MapMode":
                        this.MapMode = (StiMapMode)Enum.Parse(typeof(StiMapMode), property.Value.ToObject<string>());
                        break;

                    case "MapType":
                        this.MapType = (StiMapType)Enum.Parse(typeof(StiMapType), property.Value.ToObject<string>());
                        break;

                    case "MapData":
                        this.mapData = property.Value.ToObject<string>();
                        break;

                    case "KeyDataColumn":
                    case "DataColumnKey":
                        this.KeyDataColumn = property.Value.ToObject<string>();
                        break;

                    case "NameDataColumn":
                    case "DataColumnName":
                        this.NameDataColumn = property.Value.ToObject<string>();
                        break;

                    case "ValueDataColumn":
                    case "DataColumnValue":
                        this.ValueDataColumn = property.Value.ToObject<string>();
                        break;

                    case "GroupDataColumn":
                    case "DataColumnGroup":
                        this.GroupDataColumn = property.Value.ToObject<string>();
                        break;

                    case "ColorDataColumn":
                    case "DataColumnColor":
                        this.ColorDataColumn = property.Value.ToObject<string>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public override StiComponentId ComponentId => StiComponentId.StiMap;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();
            StiPropertyObject[] list;

            // DataCategory
            list = new[]
            {
                propHelper.MapEditor()
            };
            objHelper.Add(StiPropertyCategories.ComponentEditor, list);

            // MapCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    //propHelper.MapID(),
                    //propHelper.ShowLegend(),
                    propHelper.ShowValue(),
                    propHelper.Stretch(),
                };
            }
            else
            {
                list = new[]
                {
                    //propHelper.MapID(),
                    propHelper.MapType(),
                    //propHelper.ShowLegend(),
                    propHelper.ShowValue(),
                    propHelper.Stretch(),
                };
            }
            objHelper.Add(StiPropertyCategories.Map, list);

            // DataCategory
            list = new[]
            {
                propHelper.MapKeyDataColumn(),
                propHelper.MapNameDataColumn(),
                propHelper.MapValueDataColumn(),
                propHelper.MapGroupDataColumn(),
                propHelper.MapColorDataColumn(),
            };
            objHelper.Add(StiPropertyCategories.Data, list);

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height(),
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height(),
                    propHelper.MinSize(),
                    propHelper.MaxSize()
                };
            }
            objHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            list = new[]
            {
                propHelper.Border(),
                propHelper.Conditions()
            };
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Enabled()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.AnchorMode(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.AnchorMode(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.Printable(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Name()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias(),
                    propHelper.Restrictions(),
                    propHelper.Locked(),
                    propHelper.Linked()
                };
            }
            objHelper.Add(StiPropertyCategories.Design, list);

            return objHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }

        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public override object Clone(bool cloneProperties)
        {
            var clone = (StiMap)base.Clone(cloneProperties);
            if (this.hashData != null)
            {
                clone.hashData = new List<StiMapData>();
                foreach (var data in this.hashData)
                {
                    clone.hashData.Add(data.Clone());
                }
            }

            return clone;
        }
        #endregion		

        #region IStiExportImageExtended
        public virtual Image GetImage(ref float zoom)
        {
            return GetImage(ref zoom, StiExportFormat.None);
        }

        public virtual Image GetImage(ref float zoom, StiExportFormat format)
        {
            var painter = StiPainter.GetPainter(this.GetType(), StiGuiMode.Gdi);
            return painter.GetImage(this, ref zoom, format);
        }

        public override bool IsExportAsImage(StiExportFormat format)
        {
            return format != StiExportFormat.Pdf;
        }
        #endregion

        #region IStiBorder
        /// <summary>
        /// Gets or sets frame of the component.
        /// </summary>
        [StiCategory("Appearance")]
        [StiOrder(StiPropertyOrder.AppearanceBorder)]
        [StiSerializable]
        [Description("Gets or sets frame of the component.")]
        public StiBorder Border { get; set; } = new StiBorder();
        #endregion

        #region StiComponent override
        public override int ToolboxPosition => (int)StiComponentToolboxPosition.Map;

        /// <summary>
        /// Gets a localized name of the component category.
        /// </summary>
        public override string LocalizedCategory => StiLocalization.Get("Report", "Infographics");

        public override string LocalizedName => StiLocalization.Get("Components", "StiMap");

        public override RectangleD DefaultClientRectangle => new RectangleD(0, 0, 240, 240);

        [Browsable(false)]
        public override string ComponentStyle
        {
            get
            {
                return base.ComponentStyle;
            }
            set
            {
                base.ComponentStyle = value;
            }
        }

        public override RectangleD ClientRectangle
        {
            get
            {
                return base.ClientRectangle;
            }
            set
            {
                base.ClientRectangle = value;
                StiMapDrawingCache.RemoveImage(this);
            }
        }
        #endregion

        #region Fields
        private bool isMapDataChanged;
        private List<StiMapData> hashData;
        #endregion

        #region Properties.Obsolete
        [Browsable(false)]
        [StiBrowsable(false)]
        [Obsolete("Please use property KeyDataColumn")]
        public string DataColumnKey
        {
            get
            {
                return null;
            }
            set
            {
                this.KeyDataColumn = value;
            }
        }

        [Browsable(false)]
        [StiBrowsable(false)]
        [Obsolete("Please use property NameDataColumn")]
        public string DataColumnName
        {
            get
            {
                return null;
            }
            set
            {
                this.NameDataColumn = value;
            }
        }

        [Browsable(false)]
        [StiBrowsable(false)]
        [Obsolete("Please use property ValueDataColumn")]
        public string DataColumnValue
        {
            get
            {
                return null;
            }
            set
            {
                this.ValueDataColumn = value;
            }
        }

        [Browsable(false)]
        [StiBrowsable(false)]
        [Obsolete("Please use property GroupDataColumn")]
        public string DataColumnGroup
        {
            get
            {
                return null;
            }
            set
            {
                this.GroupDataColumn = value;
            }
        }

        [Browsable(false)]
        [StiBrowsable(false)]
        [Obsolete("Please use property ColorDataColumn")]
        public string DataColumnColor
        {
            get
            {
                return null;
            }
            set
            {
                this.ColorDataColumn = value;
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a brush to fill a component.
        /// </summary>
        [StiBrowsable(false)]
        [Browsable(false)]
        [StiSerializable]
        public StiBrush Brush { get; set; } = new StiSolidBrush(Color.Transparent);

        [StiSerializable]
        [DefaultValue(StiMapStyleIdent.Style25)]
        [StiCategory("Map")]
        [Browsable(false)]
        [StiBrowsable(false)]
        [TypeConverter(typeof(StiEnumConverter))]
        [StiPropertyLevel(StiLevel.Basic)]
        public StiMapStyleIdent MapStyle { get; set; } = StiMapStyleIdent.Style25;

        [StiSerializable]
        [DefaultValue(StiMapSource.Manual)]
        [StiCategory("Map")]
        [Browsable(false)]
        [StiBrowsable(false)]
        [TypeConverter(typeof(StiEnumConverter))]
        [StiPropertyLevel(StiLevel.Basic)]
        public StiMapSource DataFrom { get; set; } = StiMapSource.Manual;

        [StiSerializable]
        [DefaultValue(true)]
        [StiCategory("Map")]
        [StiPropertyLevel(StiLevel.Basic)]
        [TypeConverter(typeof(StiBoolConverter))]
        public bool Stretch { get; set; } = true;

        [StiSerializable]
        [DefaultValue(false)]
        [StiCategory("Map")]
        [StiPropertyLevel(StiLevel.Basic)]
        [TypeConverter(typeof(StiBoolConverter))]
        public bool ColorEach { get; set; }
        

        [StiSerializable]
        [DefaultValue(true)]
        [StiBrowsable(true)]
        [Browsable(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        public bool ShowValue { get; set; } = true;

        [StiSerializable]
        [StiBrowsable(false)]
        [Browsable(false)]
        [DefaultValue(StiMapID.USA)]
        [StiCategory("Map")]
        [TypeConverter(typeof(StiEnumConverter))]
        [StiPropertyLevel(StiLevel.Basic)]
        public StiMapID MapID { get; set; } = StiMapID.USA;

        //[StiSerializable]
        [DefaultValue(false)]
        [StiBrowsable(false)]
        [Browsable(false)]
        public bool ShowLegend { get; set; }

        [StiSerializable]
        [DefaultValue(StiDisplayNameType.Full)]
        [StiCategory("Map")]
        [TypeConverter(typeof(StiEnumConverter))]
        [StiPropertyLevel(StiLevel.Basic)]
        public StiDisplayNameType DisplayNameType { get; set; } = StiDisplayNameType.Full;

        [StiSerializable]
        [StiBrowsable(false)]
        [Browsable(false)]
        [DefaultValue(StiMapType.Individual)]
        [TypeConverter(typeof(StiEnumConverter))]
        public StiMapType MapType { get; set; } = StiMapType.Individual;

        private string mapData;
        [StiSerializable]
        [Browsable(false)]
        [StiPropertyLevel(StiLevel.Basic)]
        public string MapData
        {
            get
            {
                return mapData;
            }
            set
            {
                this.mapData = value;
                this.IsHashDataEmpty = true;
                this.isMapDataChanged = true;
                this.hashData = null;
            }
        }

        [StiSerializable]
        [Browsable(true)]
        [DefaultValue("")]
        [StiCategory("Data")]
        [StiOrder(StiPropertyOrder.MapKeyDataColumn)]
        [StiPropertyLevel(StiLevel.Basic)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets a name of the column that contains the values.")]
        public string KeyDataColumn { get; set; } = string.Empty;

        [StiSerializable]
        [Browsable(true)]
        [DefaultValue("")]
        [StiCategory("Data")]
        [StiOrder(StiPropertyOrder.MapNameDataColumn)]
        [StiPropertyLevel(StiLevel.Basic)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets a name of the column that contains the values.")]
        public string NameDataColumn { get; set; } = string.Empty;

        [StiSerializable]
        [Browsable(true)]
        [DefaultValue("")]
        [StiCategory("Data")]
        [StiOrder(StiPropertyOrder.MapValueDataColumn)]
        [StiPropertyLevel(StiLevel.Basic)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets a name of the column that contains the values.")]
        public string ValueDataColumn { get; set; } = string.Empty;

        [StiSerializable]
        [Browsable(true)]
        [DefaultValue("")]
        [StiCategory("Data")]
        [StiOrder(StiPropertyOrder.MapGroupDataColumn)]
        [StiPropertyLevel(StiLevel.Basic)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets a name of the column that contains the values.")]
        public string GroupDataColumn { get; set; } = string.Empty;

        [StiSerializable]
        [Browsable(true)]
        [DefaultValue("")]
        [StiCategory("Data")]
        [StiOrder(StiPropertyOrder.MapColorDataColumn)]
        [StiPropertyLevel(StiLevel.Basic)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets a name of the column that contains the values.")]
        public string ColorDataColumn { get; set; } = string.Empty;

        [StiSerializable]
        [DefaultValue("")]
        [StiCategory("Data")]
        [StiOrder(StiPropertyOrder.MapElementLatitude)]
        [StiPropertyLevel(StiLevel.Basic)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        public string Latitude { get; set; } = string.Empty;

        [StiSerializable]
        [DefaultValue("")]
        [StiCategory("Data")]
        [StiOrder(StiPropertyOrder.MapElementLongitude)]
        [StiPropertyLevel(StiLevel.Basic)]
        [Editor("Stimulsoft.Report.Components.Design.StiDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        public string Longitude { get; set; } = string.Empty;

        [StiNonSerialized]
        [Browsable(false)]
        public bool IsHashDataEmpty { get; private set; } = true;

        [StiSerializable]
        [StiBrowsable(false)]
        [Browsable(false)]
        [DefaultValue(StiMapMode.Choropleth)]
        [TypeConverter(typeof(StiEnumConverter))]
        public StiMapMode MapMode { get; set; } = StiMapMode.Choropleth;
        #endregion

        #region Properties Browsable(false)
        [StiNonSerialized]
        [Browsable(false)]
        public override bool UseParentStyles
        {
            get
            {
                return base.UseParentStyles;
            }
            set
            {
                
            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public override bool CanGrow
        {
            get
            {
                return base.CanGrow;
            }
            set
            {
                
            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public override bool CanShrink
        {
            get
            {
                return base.CanShrink;
            }
            set
            {

            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public override bool GrowToHeight
        {
            get
            {
                return base.GrowToHeight;
            }
            set
            {

            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public override StiInteraction Interaction
        {
            get
            {
                return base.Interaction;
            }
            set
            {
                base.Interaction = value;
            }
        }
        #endregion

        #region Methods override
        public override StiComponent CreateNew()
        {
            return new StiMap();
        }
        #endregion

        #region Methods
        internal static List<StiMapData> GetMapDataInternal(string mapData, StiMapID mapID)
        {
            bool res = false;
            return GetMapDataInternal(mapData, mapID, out res);
        }

        internal static List<StiMapData> GetDefaultMapData(StiMapID mapID)
        {
            var result = new List<StiMapData>();

            byte offset = 10;
            bool isUp = true;
            int index = 0;
            var baseColors = StiMapHelper.GetColors();
            var colors = new List<Color>();

            foreach (var color in baseColors) colors.Add(color);

            var svgContainer = StiMapLoader.LoadResource(mapID.ToString());
            foreach (string key in svgContainer.HashPaths.Keys)
            {
                var data = new StiMapData(key)
                {
                    Color = ColorTranslator.ToHtml(colors[index])
                };

                var pt = svgContainer.HashPaths[key];
                if (pt != null)
                {
                    data.Name = pt.EnglishName;
                }

                result.Add(data);

                index++;
                if (index >= colors.Count)
                {
                    index = 0;
                    if (isUp)
                    {
                        foreach (var color in baseColors)
                            colors.Add(StiColorUtils.Dark(color, offset));
                    }
                    else
                    {
                        foreach (var color in baseColors)
                            colors.Add(StiColorUtils.Light(color, offset));

                        offset += 10;
                    }

                    isUp = !isUp;
                }
            }

            return result;
        }

        internal static List<StiMapData> GetMapDataInternal(string mapData, StiMapID mapID, out bool isHashDataEmpty)
        {
            isHashDataEmpty = true;
            var result = new List<StiMapData>();

            if (!string.IsNullOrEmpty(mapData))
                StiJsonHelper.LoadFromJsonString(mapData, result);

            var svgContainer = StiMapLoader.LoadResource(mapID.ToString());
            if (svgContainer != null)
            {
                if (result.Count > 0)
                {
                    #region Сначало проверяем, чтобы в данных, небыло лишних данных
                    int index = 0;
                    while (index < result.Count)
                    {
                        var data = result[index];
                        if (string.IsNullOrEmpty(data.Key) || !svgContainer.HashPaths.ContainsKey(data.Key))
                        {
                            result.RemoveAt(index);
                            continue;
                        }

                        index++;
                    }
                    #endregion

                    #region Проверяем возможно какихто данных не хватает - добавляем их
                    if (svgContainer.HashPaths.Count != result.Count)
                    {
                        foreach (string key in svgContainer.HashPaths.Keys)
                        {
                            if (result.FirstOrDefault(x => x.Key == key) == null)
                            {
                                var data = new StiMapData(key);
                                result.Add(data);
                            }
                        }
                    }
                    #endregion

                    #region Проверяем, являются ли данные пустыми
                    // Если хотябы одно значение, влияющее на отрисовку изменено - то фиксируем это
                    foreach (var data in result)
                    {
                        if (!string.IsNullOrEmpty(data.Color) ||
                            !string.IsNullOrEmpty(data.Group) ||
                            !string.IsNullOrEmpty(data.Value))
                        {
                            isHashDataEmpty = false;
                            break;
                        }
                    }
                    #endregion
                }
                else
                {
                    byte offset = 10;
                    bool isUp = true;
                    int index = 0;
                    var baseColors = StiMapHelper.GetColors();
                    var colors = new List<Color>();

                    foreach (var color in baseColors) colors.Add(color);

                    foreach (string key in svgContainer.HashPaths.Keys)
                    {
                        var data = new StiMapData(key)
                        {
                            Color = ColorTranslator.ToHtml(colors[index])
                        };

                        var pt = svgContainer.HashPaths[key];
                        if (pt != null)
                        {
                            data.Name = pt.EnglishName;
                        }

                        result.Add(data);

                        index++;
                        if (index >= colors.Count)
                        {
                            index = 0;
                            if (isUp)
                            {
                                foreach (var color in baseColors)
                                    colors.Add(StiColorUtils.Dark(color, offset));
                            }
                            else
                            {
                                foreach (var color in baseColors)
                                    colors.Add(StiColorUtils.Light(color, offset));

                                offset += 10;
                            }

                            isUp = !isUp;
                        }
                    }
                }
            }

            return result;
        }

        public List<StiMapData> GetMapData()
        {
            if (this.hashData != null && this.hashData.Count > 0 && this.isMapDataChanged)
                return hashData;

            this.IsHashDataEmpty = true;

            bool isHashDataEmpty = true;
            var result = GetMapDataInternal(this.mapData, this.MapID, out isHashDataEmpty);
            if (!isHashDataEmpty)
                this.IsHashDataEmpty = false;

            this.isMapDataChanged = true;
            hashData = result;
            return result;
        }

        public Color[] GetCurrentStyleColors()
        {
            if (!string.IsNullOrEmpty(this.ComponentStyle))
            {
                var style = this.Report.Styles[this.ComponentStyle] as StiMapStyle;
                if (style != null)
                    return style.Colors;
            }

            return GetStyleColors(this.MapStyle);
        }

        public static Color[] GetStyleColors(StiMapStyleIdent style)
        {
            return GetMapStyle(style).Colors;
        }

        internal StiBrush GetStyleBackground()
        {
            if (!string.IsNullOrEmpty(this.ComponentStyle))
            {
                var style = this.Report.Styles[this.ComponentStyle] as StiMapStyle;
                if (style != null)
                    return new StiSolidBrush(style.BackColor);
            }

            return new StiSolidBrush(GetMapStyle(this.MapStyle).BackColor);
        }

        public static StiMapStyle GetMapStyle(StiMap map)
        {
            StiMapStyle mapStyle = null;
            if (!string.IsNullOrEmpty(map.ComponentStyle))
                mapStyle = map.Report.Styles[map.ComponentStyle] as StiMapStyle;

            if (mapStyle == null)
                mapStyle = GetMapStyle(map.MapStyle);

            return mapStyle;
        }

        public static StiMapStyle GetMapStyle(StiMapStyleIdent style)
        {
            switch (style)
            {
                case StiMapStyleIdent.Style21:
                    return new StiMap21StyleFX();

                case StiMapStyleIdent.Style24:
                    return new StiMap24StyleFX();

                case StiMapStyleIdent.Style25:
                    return new StiMap25StyleFX();

                case StiMapStyleIdent.Style26:
                    return new StiMap26StyleFX();

                case StiMapStyleIdent.Style27:
                    return new StiMap27StyleFX();

                default:
                    throw new Exception("Style is not supported!");
            }
        }
        #endregion

        /// <summary>
        /// Creates a new StiGauge.
        /// </summary>
        public StiMap() : this(RectangleD.Empty)
		{
		}

		/// <summary>
		/// Creates a new StiGauge.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiMap(RectangleD rect) : base(rect)
		{
            PlaceOnToolbox = false;
		}
    }
}