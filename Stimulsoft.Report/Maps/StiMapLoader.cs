﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports.Net											}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json;
using Stimulsoft.Base.Map;
using Stimulsoft.Base.Maps.Geoms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

namespace Stimulsoft.Report.Maps
{
    public static class StiMapLoader
    {
        #region enum StiGeomIdent
        private enum StiGeomIdent
        {
            None = 1,
            MoveToM,
            MoveTom,
            Line_L,
            Line_l,
            Bezier_C,
            Bezier_c,
            Beziers_S,
            Beziers_s,
            VerticalLineto_V,
            VerticalLineto_v,
            HorizontalLineto_H,
            HorizontalLineto_h,
            Close
        }
        #endregion

        #region Fields
        private static Hashtable hashMaps;
        private static IStiMapResourceFinder resourceFinder;
        private static bool isLoading = false;
        #endregion

        #region Properties
        public static bool IsMapAssemblyLoaded { get; private set; }
        #endregion

        #region Methods
        public static StiMapSvgContainer LoadResource(string resourceName)
        {
            if (hashMaps == null)
                hashMaps = new Hashtable();

            if (!hashMaps.ContainsKey(resourceName))
            {
                if (resourceFinder == null)
                {
                    if (isLoading) return null;

                    try
                    {
                        isLoading = true;
                        var asm = StiAssemblyFinder.GetAssembly($"Stimulsoft.Map, {StiVersion.VersionInfo}");
                        if (asm == null)
                            return null;

                        IsMapAssemblyLoaded = true;
                        var type = StiTypeFinder.GetType("Stimulsoft.Map.StiMapResourceFinder, Stimulsoft.Map");
                        
                        resourceFinder = Activator.CreateInstance(type) as IStiMapResourceFinder;

                        if (resourceFinder == null)
                            return null;
                    }
                    catch { }
                }

                string jsonText = resourceFinder.Get(resourceName);

                var container = new StiMapSvgContainer();
                JsonConvert.PopulateObject(jsonText, container, StiJsonHelper.DefaultSerializerSettings);
                container.Prepare();

                if (!hashMaps.ContainsKey(resourceName))
                    hashMaps.Add(resourceName, container);

                return container;
            }

            return (StiMapSvgContainer)hashMaps[resourceName];
        }

        public static StiMapGeomsContainer GetGeomsObject(string resourceName)
        {
            var container = LoadResource(resourceName);
            if (container == null)
                return new StiMapGeomsContainer();

            var result = new StiMapGeomsContainer
            {
                Width = container.Width,
                Height = container.Height,
                Name = container.Name
            };

            if (container.Geoms == null)
            {
                foreach (string key in container.HashPaths.Keys)
                {
                    var pt = container.HashPaths[key];
                    var obj = new StiMapGeomsObject
                    {
                        Key = key,
                        Geoms = ParsePath(pt.Data)
                    };
                    result.Geoms.Add(obj);
                }

                container.Geoms = result.Geoms;
            }
            else
            {
                result.Geoms = container.Geoms;
            }

            return result;
        }
        #endregion

        #region Methods.Helper
        private static void CreateGeom(StiGeomIdent ident, List<double> values, int startIndex, int index, StiMapGeomCollection commands, bool skipException = false)
        {
            switch (ident)
            {
                case StiGeomIdent.MoveToM:
                    {
                        if (values.Count != 2)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var geom = new StiMoveToMapGeom
                        {
                            X = Math.Round(values[0], 3),
                            Y = Math.Round(values[1], 3)
                        };
                        commands.Add(geom);
                    }
                    break;

                case StiGeomIdent.MoveTom:
                    {
                        if (values.Count != 2)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var lastPos = commands.GetLastPoint();
                        var geom = new StiMoveToMapGeom
                        {
                            X = Math.Round(lastPos.X + values[0], 3),
                            Y = Math.Round(lastPos.Y + values[1], 3)
                        };
                        commands.Add(geom);
                    }
                    break;

                case StiGeomIdent.Line_L:
                    {
                        if (values.Count != 2)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var geom = new StiLineMapGeom
                        {
                            X = Math.Round(values[0], 3),
                            Y = Math.Round(values[1], 3)
                        };
                        commands.Add(geom);
                    }
                    break;

                case StiGeomIdent.Line_l:
                    {
                        if (values.Count % 2 != 0)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var lastPos = commands[commands.Count - 1].GetLastPoint();

                        if (values.Count == 2)
                        {
                            var geom = new StiLineMapGeom
                            {
                                X = Math.Round(lastPos.X + values[0], 3),
                                Y = Math.Round(lastPos.Y + values[1], 3)
                            };
                            commands.Add(geom);
                        }
                        else
                        {
                            int index5 = 0;
                            while (index5 < values.Count)
                            {
                                var geom = new StiLineMapGeom
                                {
                                    X = Math.Round(lastPos.X + values[index5], 3),
                                    Y = Math.Round(lastPos.Y + values[index5 + 1], 3)
                                };
                                commands.Add(geom);

                                lastPos = new PointD(geom.X, geom.Y);
                                index5 += 2;
                            }
                        }
                    }
                    break;

                case StiGeomIdent.Bezier_C:
                    {
                        if (values.Count != 6)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var geom = new StiBezierMapGeom
                        {
                            X1 = Math.Round(values[0], 3),
                            Y1 = Math.Round(values[1], 3),

                            X2 = Math.Round(values[2], 3),
                            Y2 = Math.Round(values[3], 3),

                            X3 = Math.Round(values[4], 3),
                            Y3 = Math.Round(values[5], 3)
                        };
                        commands.Add(geom);
                    }
                    break;

                case StiGeomIdent.Bezier_c:
                    {
                        if (values.Count != 6)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var lastPos = commands[commands.Count - 1].GetLastPoint();

                        var geom = new StiBezierMapGeom
                        {
                            X1 = Math.Round(lastPos.X + values[0], 3),
                            Y1 = Math.Round(lastPos.Y + values[1], 3),

                            X2 = Math.Round(lastPos.X + values[2], 3),
                            Y2 = Math.Round(lastPos.Y + values[3], 3),

                            X3 = Math.Round(lastPos.X + values[4], 3),
                            Y3 = Math.Round(lastPos.Y + values[5], 3)
                        };
                        commands.Add(geom);
                    }
                    break;

                case StiGeomIdent.VerticalLineto_V:
                    {
                        if (values.Count != 1)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var lastPoint = commands[commands.Count - 1].GetLastPoint();
                        var geom = new StiLineMapGeom
                        {
                            X = Math.Round(lastPoint.X, 3),
                            Y = Math.Round(values[0], 3)
                        };
                        commands.Add(geom);
                    }
                    break;

                case StiGeomIdent.VerticalLineto_v:
                    {
                        if (values.Count != 1)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var lastPoint = commands[commands.Count - 1].GetLastPoint();
                        var geom = new StiLineMapGeom
                        {
                            X = Math.Round(lastPoint.X, 3),
                            Y = Math.Round(lastPoint.Y + values[0], 3)
                        };
                        commands.Add(geom);
                    }
                    break;

                case StiGeomIdent.HorizontalLineto_H:
                    {
                        if (values.Count != 1)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var lastPoint = commands[commands.Count - 1].GetLastPoint();
                        var geom = new StiLineMapGeom
                        {
                            X = Math.Round(values[0], 3),
                            Y = Math.Round(lastPoint.Y, 3)
                        };
                        commands.Add(geom);
                    }
                    break;

                case StiGeomIdent.HorizontalLineto_h:
                    {
                        if (values.Count != 1)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var lastPoint = commands[commands.Count - 1].GetLastPoint();
                        var geom = new StiLineMapGeom
                        {
                            X = Math.Round(lastPoint.X + values[0], 3),
                            Y = Math.Round(lastPoint.Y, 3)
                        };
                        commands.Add(geom);
                    }
                    break;

                case StiGeomIdent.Beziers_S:
                    {
                        if (values.Count < 6)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var list = new List<double>();
                        foreach (var value in values)
                        {
                            list.Add(Math.Round(value, 3));
                        }

                        var geom = new StiBeziersMapGeom
                        {
                            Array = list.ToArray()
                        };
                        commands.Add(geom);
                    }
                    break;


                case StiGeomIdent.Beziers_s:
                    {
                        if (values.Count < 6)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        var lastPoint = commands[commands.Count - 1].GetLastPoint();
                        lastPoint.X += values[4];
                        lastPoint.Y += values[5];

                        bool state = true;
                        var list = new List<double>();
                        foreach (double value in values)
                        {
                            double newValue = state ? value + lastPoint.X : value + lastPoint.Y;
                            list.Add(Math.Round(newValue, 3));

                            state = !state;
                        }

                        var geom = new StiBeziersMapGeom
                        {
                            SvgValues = values.ToArray(),
                            Array = list.ToArray()
                        };
                        commands.Add(geom);

                        list.Clear();
                        list = null;
                    }
                    break;

                case StiGeomIdent.Close:
                    {
                        if (values.Count != 0)
                        {
                            if (skipException) return;
                            throw new NotSupportedException();
                        }

                        commands.Add(new StiCloseMapGeom());
                    }
                    break;
            }

            values.Clear();
        }

        public static List<StiMapGeom> ParsePath(string text)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

                var commands = new StiMapGeomCollection();

                int startIndex = 0;
                int valuesCount = 0;
                var ident = StiGeomIdent.None;

                int valueIndex = 0;
                var values = new List<double>();

                double temp = 0.0;

                int index = 0;
                int count = text.Length;
                while (index < count)
                {
                    char ch = text[index];

                    switch (ch)
                    {
                        case 'M':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                {
                                    CreateGeom(ident, values, startIndex, index, commands);
                                    valuesCount = 0;
                                }

                                ident = StiGeomIdent.MoveToM;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'm':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                {
                                    CreateGeom(ident, values, startIndex, index, commands);
                                    valuesCount = 0;
                                }

                                ident = StiGeomIdent.MoveTom;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'C':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                    CreateGeom(ident, values, startIndex, index, commands);

                                ident = StiGeomIdent.Bezier_C;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'c':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                    CreateGeom(ident, values, startIndex, index, commands);

                                ident = StiGeomIdent.Bezier_c;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'S':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                ident = StiGeomIdent.Beziers_S;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 's':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);
                                
                                ident = StiGeomIdent.Beziers_s;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'L':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                    CreateGeom(ident, values, startIndex, index, commands);

                                ident = StiGeomIdent.Line_L;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'l':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                    CreateGeom(ident, values, startIndex, index, commands);

                                ident = StiGeomIdent.Line_l;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'Z':
                        case 'z':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                    CreateGeom(ident, values, startIndex, index, commands);

                                ident = StiGeomIdent.Close;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'V':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                    CreateGeom(ident, values, startIndex, index, commands);

                                ident = StiGeomIdent.VerticalLineto_V;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'v':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                    CreateGeom(ident, values, startIndex, index, commands);

                                ident = StiGeomIdent.VerticalLineto_v;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'H':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                    CreateGeom(ident, values, startIndex, index, commands);

                                ident = StiGeomIdent.HorizontalLineto_H;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case 'h':
                            {
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                if (ident != StiGeomIdent.None)
                                    CreateGeom(ident, values, startIndex, index, commands);

                                ident = StiGeomIdent.HorizontalLineto_h;
                                startIndex = index + 1;
                                valueIndex = startIndex;
                            }
                            break;

                        case '-':
                            {
                                valuesCount++;
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);

                                valueIndex = index;
                            }
                            break;

                        case ',':
                        case ' ':
                            {
                                valuesCount++;
                                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                                    values.Add(temp);
                                valueIndex = index + 1;
                            }
                            break;

                        case 'Q':
                        case 'q':
                        case 'T':
                        case 't':
                        case 'A':
                        case 'a':
                            throw new Exception("");
                    }

                    index++;
                }

                if (double.TryParse(text.Substring(valueIndex, index - valueIndex), out temp))
                    values.Add(temp);

                if (ident != StiGeomIdent.None && values.Count > 1)
                    CreateGeom(ident, values, startIndex, index, commands);

                return commands;
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = currentCulture;
            }
        }
        #endregion
    }
}