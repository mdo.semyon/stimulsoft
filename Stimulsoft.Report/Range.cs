#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.CodeDom;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Design;

namespace Stimulsoft.Report
{
    #region Range
    /// <summary>
    /// Base class for all Range classes.
    /// </summary>
    [TypeConverter(typeof(Stimulsoft.Report.Design.RangeConverter))]
    [StiSerializable]
    public abstract class Range
    {
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public abstract string RangeName
        {
            get;
        }
        
        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public abstract Type RangeType
        {
            get;
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public abstract object FromObject
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public abstract object ToObject
        {
            get;
            set;
        }

        /// <summary>
        /// Fill From and To item of range with it string representation.
        /// </summary>
        public void Parse(string from, string to)
        {
            if (RangeType == typeof(char))
            {
                if (!string.IsNullOrEmpty(from))
                    this.FromObject = from[0];

                if (!string.IsNullOrEmpty(to))
                    this.ToObject = to[0];
            }
            else if (RangeType == typeof(DateTime))
            {
                try
                {
                    if (string.IsNullOrEmpty(from.Trim()))
                        this.FromObject = null;
                    else
                        this.FromObject = DateTime.Parse(from);

                    if (string.IsNullOrEmpty(to.Trim()))
                        this.ToObject = null;
                    else
                        this.ToObject = DateTime.Parse(to);
                }
                catch
                {
                }
            }
            else if (RangeType == typeof(TimeSpan))
            {
                try
                {
                    if (string.IsNullOrEmpty(from.Trim()))
                        this.FromObject = null;
                    else
                        this.FromObject = TimeSpan.Parse(from);

                    if (string.IsNullOrEmpty(to.Trim()))
                        this.ToObject = null;
                    else
                        this.ToObject = TimeSpan.Parse(to);
                }
                catch
                {
                }
            }
            else if (RangeType == typeof(decimal))
            {
                try
                {
                    if (string.IsNullOrEmpty(from.Trim()))
                        this.FromObject = 0m;
                    else
                        this.FromObject = decimal.Parse(from);

                    if (string.IsNullOrEmpty(to.Trim()))
                        this.ToObject = 0m;
                    else
                        this.ToObject = decimal.Parse(to);
                }
                catch
                {
                }
            }
            else if (RangeType == typeof(float))
            {
                try
                {
                    if (string.IsNullOrEmpty(from.Trim()))
                        this.FromObject = 0f;
                    else
                        this.FromObject = float.Parse(from);

                    if (string.IsNullOrEmpty(to.Trim()))
                        this.ToObject = 0f;
                    else
                        this.ToObject = float.Parse(to);
                }
                catch
                {
                }
            }
            else if (RangeType == typeof(double))
            {
                try
                {
                    if (string.IsNullOrEmpty(from.Trim()))
                        this.FromObject = 0d;
                    else
                        this.FromObject = double.Parse(from);

                    if (string.IsNullOrEmpty(to.Trim()))
                        this.ToObject = 0d;
                    else
                        this.ToObject = double.Parse(to);
                }
                catch
                {
                }
            }
            else if (RangeType == typeof(byte))
            {
                try
                {
                    if (string.IsNullOrEmpty(from.Trim()))
                        this.FromObject = 0;
                    else
                        this.FromObject = byte.Parse(from);

                    if (string.IsNullOrEmpty(to.Trim()))
                        this.ToObject = 0;
                    else
                        this.ToObject = byte.Parse(to);
                }
                catch
                {
                }
            }
            else if (RangeType == typeof(short))
            {
                try
                {
                    if (string.IsNullOrEmpty(from.Trim()))
                        this.FromObject = 0;
                    else
                        this.FromObject = short.Parse(from);

                    if (string.IsNullOrEmpty(to.Trim()))
                        this.ToObject = 0;
                    else
                        this.ToObject = short.Parse(to);
                }
                catch
                {
                }
            }
            else if (RangeType == typeof(int))
            {
                try
                {
                    if (string.IsNullOrEmpty(from.Trim()))
                        this.FromObject = 0;
                    else
                        this.FromObject = int.Parse(from);

                    if (string.IsNullOrEmpty(to.Trim()))
                        this.ToObject = 0;
                    else
                        this.ToObject = int.Parse(to);
                }
                catch
                {
                }
            }
            else if (RangeType == typeof(long))
            {
                try
                {
                    if (string.IsNullOrEmpty(from.Trim()))
                        this.FromObject = 0;
                    else
                        this.FromObject = long.Parse(from);

                    if (string.IsNullOrEmpty(to.Trim()))
                        this.ToObject = 0;
                    else
                        this.ToObject = long.Parse(to);
                }
                catch
                {
                }
            }
            else if (RangeType == typeof(Guid))
            {
                try
                {
                    this.FromObject = new Guid(from);
                    this.ToObject = new Guid(to);
                }
                catch
                {
                }
            }
            else if (RangeType == typeof(string))
            {
                this.FromObject = from;
                this.ToObject = to;
            }
        }

        public override bool Equals(object obj)
        {
            var range2 = obj as Range;
            if (range2 == null) return false;
            
            return 
                range2 != null &&
                Comparer.Default.Compare(this.FromObject, range2.FromObject) == 0 && 
                Comparer.Default.Compare(this.ToObject, range2.ToObject) == 0;
        }

        public string FromStrLoc
        {
            get
            {
                return StiLocalization.Get("PropertyMain", "RangeFrom");
            }
        }

        public string ToStrLoc
        {
            get
            {
                return StiLocalization.Get("PropertyMain", "RangeTo");
            }
        }


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    #endregion    

    #region CharRange
    public class CharRange : Range
    {
        #region Fields
        public char From = 'A';
        public char To = 'Z';
        #endregion

        #region Properties
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "CharRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(char);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is char)
                    From = (char)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is char)
                    To = (char)value;
            }
        }        
        #endregion

        #region Methods
        public bool Contains(char value)
        {
            return From <= value && To >= value;
        }

        public override string ToString()
        {
            return string.Format("{0} '{1}' {2} '{3}'", FromStrLoc, From, ToStrLoc.ToLowerInvariant(), To);
        }
        #endregion

        public CharRange()
        {
        }

        public CharRange(char from, char to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion

    #region DateTimeRange
    public class DateTimeRange : Range
    {
        #region Fields
        public DateTime? From = null;
        public DateTime? To = null;
        #endregion

        #region Properties
        public DateTime FromDate
        {
            get
            {
                if (From == null)
                    return DateTime.MinValue;
                return From.Value;
            }
        }

        public DateTime ToDate
        {
            get
            {
                if (To == null)
                    return DateTime.MaxValue;
                return To.Value;
            }
        }

        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "DateTimeRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(DateTime);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is DateTime)
                    From = (DateTime)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is DateTime)
                    To = (DateTime)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(DateTime value)
        {
            if (From == null && To == null)
                return true;

            if (From == null && To != null)
                return To.Value >= value;

            if (From != null && To == null)
                return From.Value <= value;

            return From.Value <= value && To.Value >= value;
        }

        public bool Contains(DateTime? value)
        {
            if (From == null && To == null)
                return true;

            if (value == null)
                return false;

            if (From == null && To != null)
                return To.Value >= value.Value;

            if (From != null && To == null)
                return From.Value <= value.Value;

            return From.Value <= value.Value && To.Value >= value.Value;
        }

        public override string ToString()
        {
            var strFrom = From != null ? string.Format("{0:d}", From) : "-";
            var strTo = To != null ? string.Format("{0:d}", To) : "-";

            return string.Format("{0} {1} {2} {3}", FromStrLoc, strFrom, ToStrLoc.ToLowerInvariant(), strTo);
        }
        #endregion

        public DateTimeRange()
        {
        }

        public DateTimeRange(DateTime from, DateTime to)
        {
            this.From = from;
            this.To = to;
        }

        public DateTimeRange(DateTime? from, DateTime? to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion

    #region TimeSpanRange
    public class TimeSpanRange : Range
    {
        #region Fields
        public TimeSpan? From = null;
        public TimeSpan? To = null;
        #endregion

        #region Properties
        public TimeSpan FromTime
        {
            get
            {
                if (From == null)
                    return TimeSpan.MinValue;
                return From.Value;
            }
        }

        public TimeSpan ToTime
        {
            get
            {
                if (To == null)
                    return TimeSpan.MaxValue;
                return To.Value;
            }
        }

        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "TimeSpanRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(TimeSpan);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is TimeSpan)
                    From = (TimeSpan)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is TimeSpan)
                    To = (TimeSpan)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(TimeSpan value)
        {
            if (From == null && To == null)
                return true;

            if (From == null && To != null)
                return To.Value >= value;

            if (From != null && To == null)
                return From.Value <= value;

            return From.Value <= value && To.Value >= value;
        }

        public bool Contains(TimeSpan? value)
        {
            if (From == null && To == null)
                return true;

            if (value == null)
                return false;

            if (From == null && To != null)
                return To.Value >= value.Value;

            if (From != null && To == null)
                return From.Value <= value.Value;

            return From.Value <= value.Value && To.Value >= value.Value;
        }

        public override string ToString()
        {
            var strFrom = From != null ? string.Format("{0:t}", From) : "-";
            var strTo = To != null ? string.Format("{0:t}", To) : "-";

            return string.Format("{0} {1} {2} {3}", FromStrLoc, strFrom, ToStrLoc.ToLowerInvariant(), strTo);
        }
        #endregion

        public TimeSpanRange()
        {
        }

        public TimeSpanRange(TimeSpan from, TimeSpan to)
        {
            this.From = from;
            this.To = to;
        }

        public TimeSpanRange(TimeSpan? from, TimeSpan? to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion

    #region DecimalRange
    public class DecimalRange : Range
    {
        #region Fields
        public decimal From = 0m;
        public decimal To = 0m;
        #endregion

        #region Properties
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "DecimalRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(decimal);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is decimal)
                    From = (decimal)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is decimal)
                    To = (decimal)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(decimal value)
        {
            return From <= value && To >= value;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", FromStrLoc, From, ToStrLoc.ToLowerInvariant(), To);
        }
        #endregion

        public DecimalRange()
        {
        }

        public DecimalRange(decimal from, decimal to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion

    #region FloatRange
    public class FloatRange : Range
    {
        #region Fields
        public float From = 0f;
        public float To = 0f;
        #endregion

        #region Properties
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "FloatRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(float);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is float)
                    From = (float)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is float)
                    To = (float)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(float value)
        {
            return From <= value && To >= value;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", FromStrLoc, From, ToStrLoc.ToLowerInvariant(), To);
        }
        #endregion

        public FloatRange()
        {
        }

        public FloatRange(float from, float to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion

    #region DoubleRange
    public class DoubleRange : Range
    {
        #region Fields
        public double From = 0d;
        public double To = 0d;
        #endregion

        #region Properties
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "DoubleRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(double);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is double)
                    From = (double)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is double)
                    To = (double)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(double value)
        {
            return From <= value && To >= value;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", FromStrLoc, From, ToStrLoc.ToLowerInvariant(), To);
        }
        #endregion

        public DoubleRange()
        {
        }

        public DoubleRange(double from, double to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion

    #region ByteRange
    public class ByteRange : Range
    {
        #region Fields
        public byte From = 0;
        public byte To = 0;
        #endregion

        #region Properties
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "ByteRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(byte);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is byte)
                    From = (byte)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is byte)
                    To = (byte)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(byte value)
        {
            return From <= value && To >= value;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", FromStrLoc, From, ToStrLoc.ToLowerInvariant(), To);
        }
        #endregion

        public ByteRange()
        {
        }

        public ByteRange(byte from, byte to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion

    #region ShortRange
    public class ShortRange : Range
    {
        #region Fields
        public short From = 0;
        public short To = 0;
        #endregion

        #region Properties
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "ShortRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(short);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is short)
                    From = (short)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is short)
                    To = (short)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(short value)
        {
            return From <= value && To >= value;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", FromStrLoc, From, ToStrLoc.ToLowerInvariant(), To);
        }
        #endregion

        public ShortRange()
        {
        }

        public ShortRange(short from, short to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion
    
    #region IntRange
    public class IntRange : Range
    {
        #region Fields
        public int From = 0;
        public int To = 0;
        #endregion

        #region Properties
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "IntRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(int);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is int)
                    From = (int)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is int)
                    To = (int)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(int value)
        {
            return From <= value && To >= value;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", FromStrLoc, From, ToStrLoc.ToLowerInvariant(), To);
        }
        #endregion

        public IntRange()
        {
        }

        public IntRange(int from, int to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion

    #region LongRange
    public class LongRange : Range
    {
        #region Fields
        public long From = 0;
        public long To = 0;
        #endregion

        #region Properties
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "LongRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(long);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is long)
                    From = (long)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is long)
                    To = (long)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(long value)
        {
            return From <= value && To >= value;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", FromStrLoc, From, ToStrLoc.ToLowerInvariant(), To);
        }
        #endregion

        public LongRange()
        {
        }

        public LongRange(long from, long to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion

    #region GuidRange
    public class GuidRange : Range
    {
        #region Fields
        public Guid From = Guid.Empty;
        public Guid To = Guid.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "GuidRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(Guid);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is Guid)
                    From = (Guid)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is Guid)
                    To = (Guid)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(Guid value)
        {
            return From.CompareTo(value) <= 0 && To.CompareTo(value) >= 0;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", FromStrLoc, From, ToStrLoc.ToLowerInvariant(), To);
        }
        #endregion

        public GuidRange()
        {
        }

        public GuidRange(Guid from, Guid to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion

    #region StringRange
    public class StringRange : Range
    {
        #region Fields
        public string From = string.Empty;
        public string To = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// Gets specified name of range. Range name equal to name of range class.
        /// </summary>
        public override string RangeName
        {
            get
            {
                return "StringRange";
            }
        }

        /// <summary>
        /// Gets the type of range items. 
        /// </summary>
        public override Type RangeType
        {
            get
            {
                return typeof(string);
            }
        }

        /// <summary>
        /// Gets or sets From item of range.
        /// </summary>
        public override object FromObject
        {
            get
            {
                return From;
            }
            set
            {
                if (value is string)
                    From = (string)value;
            }
        }

        /// <summary>
        /// Gets or set To item of range.
        /// </summary>
        public override object ToObject
        {
            get
            {
                return To;
            }
            set
            {
                if (value is string)
                    To = (string)value;
            }
        }
        #endregion

        #region Methods
        public bool Contains(string value)
        {
            if (From == null && To == null)
                return true;

            if (value == null)
                return true;

            if (From == null && To != null)
                return To.CompareTo(value) >= 0;

            if (From != null && To == null)
                return From.CompareTo(value) <= 0;

            return From.CompareTo(value) <= 0 && To.CompareTo(value) >= 0;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", FromStrLoc, From, ToStrLoc.ToLowerInvariant(), To);
        }
        #endregion

        public StringRange()
        {
        }

        public StringRange(string from, string to)
        {
            this.From = from;
            this.To = to;
        }
    }
    #endregion
}
