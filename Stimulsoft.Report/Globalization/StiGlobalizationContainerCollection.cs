#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Globalization;
using System.Collections;
using Stimulsoft.Report.Components;
using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace Stimulsoft.Report
{
	public class StiGlobalizationContainerCollection :
        CollectionBase, 
        IStiJsonReportObject
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            if (List.Count == 0)
                return null;

            var jObject = new JObject();
            int index = 0;
            foreach (StiGlobalizationContainer container in List)
            {
                jObject.AddPropertyJObject(index.ToString(), container.SaveToJsonObject(mode));
                index++;
            }

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                var container = new StiGlobalizationContainer();
                container.LoadFromJsonObject((JObject)property.Value);

                List.Add(container);
            }
        }
        #endregion

        #region Collection
        public List<StiGlobalizationContainer> ToList()
        {
            return this.Cast<object>().Cast<StiGlobalizationContainer>().ToList();
        }

        public void Add(StiGlobalizationContainer data)
		{
			List.Add(data);
		}

		public void AddRange(StiGlobalizationContainer[] data)
		{
			base.InnerList.AddRange(data);
		}

		public bool Contains(StiGlobalizationContainer data)
		{
			return List.Contains(data);
		}
		
		public int IndexOf(StiGlobalizationContainer data)
		{
			return List.IndexOf(data);
		}

		public void Insert(int index, StiGlobalizationContainer data)
		{
			lock (this)List.Insert(index, data);
		}

		public void Remove(StiGlobalizationContainer data)
		{
			lock (this)List.Remove(data);
		}
		
		
		public StiGlobalizationContainer this[int index]
		{
			get
			{
				return (StiGlobalizationContainer)List[index];
			}
			set
			{
				List[index] = value;
			}
		}

		public StiGlobalizationContainer this[string name]
		{
			get
			{
				name = name.ToLower(CultureInfo.InvariantCulture);
				foreach (StiGlobalizationContainer container in List)
					if (container.CultureName.ToLower(CultureInfo.InvariantCulture) == name)return container;
				return null;
			}
			set
			{
				name = name.ToLower(CultureInfo.InvariantCulture);
				for (int index = 0; index < List.Count; index++)				
				{
					StiGlobalizationContainer container = List[index] as StiGlobalizationContainer;
					
					if (container.CultureName.ToLower(CultureInfo.InvariantCulture) == name)
					{
						List[index] = value;
						return;
					}
				}
				Add(value);
			}
		}
		#endregion

        #region Fields
        private StiReport report = null;
        #endregion

        #region Properties
        internal bool SkipException { get; set; }
        #endregion

        #region Methods
        public void LocalizeReport(string cultureName)
		{
			var container = this[cultureName];

			if (container == null && !SkipException)
				throw new Exception($"Can't find globalized strings for culture {cultureName}");
			
			if (container != null)
			    container.LocalizeReport(report);
		}

		public void LocalizeReport(CultureInfo info)
		{
			var cultureName = info.Name;
            var index = cultureName.IndexOf("-", StringComparison.InvariantCulture);
		    if (index > -1)
		        cultureName = cultureName.Substring(0, index);

		    LocalizeReport(cultureName);
		}

		public void FillItemsFromReport()
		{
			foreach (StiGlobalizationContainer container in this)
			{
				container.FillItemsFromReport(report);
			}
		}


		public void RemoveUnlocalizedItemsFromReport()
		{
			foreach (StiGlobalizationContainer container in this)
			{
				container.RemoveUnlocalizedItemsFromReport(report);
			}
		}

        public void RemoveComponent(StiComponent comp)
        {
            var provider = comp as IStiGlobalizationProvider;
            if (provider != null)
            {
                var strs = provider.GetAllStrings();

                foreach (var str in strs)
                {
                    var data = $"{comp.Name}.{str}";

                    foreach (StiGlobalizationContainer container in this)
                    {
                        var index = 0;
                        while (index < container.Items.Count)
                        {
                            var item = container.Items[index];
                            if (item.PropertyName == data)
                            {
                                container.Items.RemoveAt(index);
                            }
                            else index++;
                        }
                    }
                }                                    
            }
        }

        public void RenameComponent(StiComponent comp, string oldName, string newName)
        {
            var provider = comp as IStiGlobalizationProvider;
            if (provider != null)
            {
                var strs = provider.GetAllStrings();

                foreach (var str in strs)
                {
                    var oldData = $"{oldName}.{str}";
                    var newData = $"{newName}.{str}";

                    foreach (StiGlobalizationContainer container in this)
                    {
                        foreach (StiGlobalizationItem item in container.Items)
                        {
                            if (item.PropertyName == oldData)
                            {
                                item.PropertyName = newData;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        public StiGlobalizationContainerCollection(StiReport report)
		{
			this.report = report;
		}
    }
}
