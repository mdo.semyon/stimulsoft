﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Reflection;
using System.Text;
using System.Drawing;
using System.Threading;
using System.Linq;
using System.Collections;
using System.Globalization;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.SaveLoad;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.CodeDom;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Json;
using Stimulsoft.Report.Viewer;

namespace Stimulsoft.Report
{
    public partial class StiReport
    {
        #region class StiJsonLoaderHelper
        internal class StiJsonLoaderHelper
        {
            #region Fields
            public List<IStiMasterComponent> MasterComponents = new List<IStiMasterComponent>();
            public List<StiClone> Clones = new List<StiClone>();
            public List<StiDialogInfo> DialogInfo = new List<StiDialogInfo>();
            #endregion

            #region Methods
            public void Clean()
            {
                MasterComponents.Clear();
                MasterComponents = null;

                Clones.Clear();
                Clones = null;

                DialogInfo.Clear();
                DialogInfo = null;
            }
            #endregion
        }
        #endregion

        #region Fields
        internal StiJsonLoaderHelper jsonLoaderHelper;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the last opened or saved file name.
        /// </summary>
        [Browsable(false)]
        [StiBrowsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [StiSerializable]
        [DefaultValue("")]
        public string ReportFile { get; set; } = string.Empty;
        
        /// <summary>
        /// Gets or sets a value that allows to save the interaction settings of components in a document (report snapshot) file.
        /// </summary>
        [Browsable(false)]
        [StiBrowsable(false)]
        internal bool SaveInteractionParametersToDocument { get; set; }
        #endregion

        #region Methods.Detect
        /// <summary>
        /// Returns true if specified stream contains packed report.
        /// </summary>
        public static bool IsPackedFile(Stream stream)
        {
            if (stream.Length < 4) return false;

            var first = stream.ReadByte();
            var second = stream.ReadByte();
            var third = stream.ReadByte();
            stream.Seek(-3, SeekOrigin.Current);

            return IsPackedFile(first, second, third);
        }

        /// <summary>
        /// Returns true if specified stream contains packed report.
        /// </summary>
        public static bool IsJsonFile(Stream stream)
        {
            if (stream.Length < 2) return false;

            var first = stream.ReadByte();
            if (first == 0xEF && stream.Length > 3) //maybe BOM
            {
                var second = stream.ReadByte();
                var third = stream.ReadByte();
                first = stream.ReadByte();
                stream.Seek(-3, SeekOrigin.Current);
                if (!(second == 0xBB && third == 0xBF)) first = 0;
            }
            stream.Seek(-1, SeekOrigin.Current);

            return IsJsonFile(first);
        }
        
        internal static bool IsJsonFile(int first)
        {
            return first == 0x0000007b;
        }

        internal static bool IsPackedFile(byte[] bytes)
        {
            if (bytes == null || bytes.Length < 3)
                return false;

            return IsPackedFile(bytes[0], bytes[1], bytes[2]);
        }

        internal static bool IsPackedFile(int first, int second, int third)
        {
            //Variant checks bytes for zip signature
            if (first == 0x1F && second == 0x8B && third == 0x08) return true;//Gzip
            if (first == 0x50 && second == 0x4B && third == 0x03) return true;//PKZip

            return false;
        }

        /// <summary>
        /// Returns true if specified stream contains encrypted report.
        /// </summary>
        public static bool IsEncryptedFile(Stream stream)
        {
            if (stream.Length < 4) return false;

            var first = stream.ReadByte();
            var second = stream.ReadByte();
            var third = stream.ReadByte();
            stream.Seek(-3, SeekOrigin.Current);

            return IsEncryptedFile(first, second, third);
        }

        internal static bool IsEncryptedFile(byte[] bytes)
        {
            if (bytes == null || bytes.Length < 3)
                return false;

            return IsEncryptedFile(bytes[0], bytes[1], bytes[2]);
        }

        internal static bool IsEncryptedFile(int first, int second, int third)
        {
            return first == 0x6D && second == 0x64 && third == 0x78;
        }
        #endregion

        #region Methods.Load report from resources
        /// <summary>
        /// Loads a report template from the resource.
        /// </summary>
        /// <param name="reportAssembly">Assembly in which is the report is placed.</param>
        /// <param name="reportResource">A name of resources which contains a report template.</param>
        public virtual void LoadReportFromResource(Assembly reportAssembly, string reportResource)
        {
            var stream = reportAssembly.GetManifestResourceStream(reportResource);
            if (stream != null)
            {
                this.Load(stream);
                stream.Close();
                stream.Dispose();
            }
            else
                throw new Exception($"Can't find report '{reportResource}' in resources");
        }

        /// <summary>
        /// Loads a report template from the resource.
        /// </summary>
        /// <param name="assemblyName">The name of assembly in which the report is placed.</param>
        /// <param name="reportResource">A name of resources which contains a report template.</param>
        public virtual void LoadReportFromResource(string assemblyName, string reportResource)
        {
            var assembly = AppDomain.CurrentDomain.GetAssemblies()
                .FirstOrDefault(a => a.GetName().Name == assemblyName);

            if (assembly != null)
            {
                LoadReportFromResource(assembly, reportResource);
                return;
            }

            throw new Exception($"Can't find assembly '{assemblyName}'");
        }
        #endregion

        #region Methods.Save report source code
        /// <summary>
        /// Saves the report source code to the string.
        /// </summary>
        public string SaveReportSourceCode()
        {
            return SaveReportSourceCode(false);
        }

        /// <summary>
        /// Saves the report source code to the string.
        /// </summary>
        public string SaveReportSourceCode(bool saveForInheritedReports)
        {
            if (saveForInheritedReports)
            {
                var serializator = new StiCodeDomSerializator();
                return serializator.Serialize(this, this.GetReportName(), Language, saveForInheritedReports);
            }
            else
            {
                string storedScript = this.Script;
                try
                {
                    this.ScriptUnpack(saveForInheritedReports);
                    return this.Script;
                }
                finally
                {
                    this.Script = storedScript;
                }
            }
        }

        /// <summary>
        /// Saves the report source code to the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the source code.</param>
        public void SaveReportSourceCode(Stream stream)
        {
            SaveReportSourceCode(stream, false);
        }

        /// <summary>
        /// Saves the report source code to the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the source code.</param>
        public void SaveReportSourceCode(Stream stream, bool saveForInheritedReports)
        {
            StiLogService.Write(typeof(StiReport), "Saving report to source code");
            StreamWriter writer = null;
            var encoding = Encoding.UTF8;
            try
            {
                if (StiOptions.Engine.SourceCodeEncoding != null)
                    encoding = StiOptions.Engine.SourceCodeEncoding;

                writer = new StreamWriter(stream, encoding);
                writer.Write(SaveReportSourceCode(saveForInheritedReports));
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving report to source code...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            finally
            {
                if (writer != null) writer.Flush();
            }
        }

        /// <summary>
        /// Saves the report source code to the file.
        /// </summary>
        /// <param name="path">Parameter specifies a path to the file the report is exported to.</param>
        public virtual void SaveReportSourceCode(string path)
        {
            SaveReportSourceCode(path, false);
        }

        /// <summary>
        /// Saves the report source code to the file.
        /// </summary>
        /// <param name="path">Parameter specifies a path to the file the report is exported to.</param>
        public virtual void SaveReportSourceCode(string path, bool saveForInheritedReports)
        {
            FileStream stream = null;
            try
            {
                if (!this.IsDesigning || !StiOptions.Designer.ReadOnlyAlertOnSave)
                {
                    StiFileUtils.ProcessReadOnly(path);
                } 
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                SaveReportSourceCode(stream, saveForInheritedReports);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving report to source code...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
        }

        /// <summary>
        /// Saves the report source code for Silverlight to the file.
        /// </summary>
        /// <param name="path">Parameter specifies a path to the file the report is exported to.</param>
        public virtual void SaveReportSourceCodeForSilverlight(string path)
        {
            FileStream stream = null;
            try
            {
                if (!this.IsDesigning || !StiOptions.Designer.ReadOnlyAlertOnSave)
                {
                    StiFileUtils.ProcessReadOnly(path);
                }
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                SaveReportSourceCodeForSilverlight(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving report to source code...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
        }

        /// <summary>
        /// Saves the report source code for Silverlight to the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the source code.</param>
        public void SaveReportSourceCodeForSilverlight(Stream stream)
        {
            StiLogService.Write(typeof(StiReport), "Saving report to source code for Silverlight");
            StreamWriter writer = null;
            var encoding = Encoding.UTF8;
            try
            {
                if (StiOptions.Engine.SourceCodeEncoding != null)
                    encoding = StiOptions.Engine.SourceCodeEncoding;

                writer = new StreamWriter(stream, encoding);
                writer.Write(SaveReportSourceCodeForSilverlight());
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving report to source code for Silverlight...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            finally
            {
                if (writer != null) writer.Flush();
            }
        }

        /// <summary>
        /// Saves the report source code for Silverlight to the string.
        /// </summary>
        public string SaveReportSourceCodeForSilverlight()
        {
            this.ReportResources.Clear();

            var resRelations = this.Dictionary.Relations;
            var resDataSources = this.Dictionary.DataSources;
            var resDatabases = this.Dictionary.Databases;

            try
            {
                this.Dictionary.DataSources = new StiDataSourcesCollection(this.Dictionary);
                this.Dictionary.Relations = new StiDataRelationsCollection(this.Dictionary);
                this.Dictionary.Databases = new StiDatabaseCollection();

                string storedScript = this.Script;
                this.ScriptUnpack();
                string text = this.Script;
                this.Script = storedScript;

                text = text.Replace("System.Drawing.Color.FromArgb", "System.Windows.Media.Color.FromArgb");

                var colorStr = "System.Drawing.Color.";
                while (true)
                {
                    int index = text.IndexOf(colorStr, StringComparison.Ordinal);
                    if (index != -1)
                    {
                        int colorNameIndex = index + colorStr.Length;
                        string colorName = "";
                        while (true)
                        {
                            char chr = text[colorNameIndex];
                            if (char.IsLetter(chr))
                                colorName += chr;
                            else
                                break;
                            colorNameIndex++;
                        }
                        var color = Color.FromName(colorName);
                        var oldColorName = colorStr + colorName;
                        var newColorName = $"System.Windows.Media.Color.FromArgb({color.A}, {color.R}, {color.G}, {color.B})";

                        var oldLen = oldColorName.Length;
                        var newLen = newColorName.Length;
                        var indexStr = 0;
                        while (true)
                        {
                            indexStr = text.IndexOf(oldColorName, indexStr, StringComparison.Ordinal);
                            if (indexStr < 0) break;
                            if (indexStr + oldLen < text.Length && !char.IsLetter(text[indexStr + oldLen]))
                            {
                                text = text.Substring(0, indexStr) + newColorName + text.Substring(indexStr + oldLen);
                                indexStr += newLen;
                            }
                            else
                            {
                                indexStr += oldLen;
                            }
                        }
                    }
                    else break;
                }
                return text;
            }
            finally
            {
                this.Dictionary.Relations = resRelations;
                this.Dictionary.DataSources = resDataSources;
                this.Dictionary.Databases = resDatabases;
            }
        }
        #endregion

        #region Methods.Save Editable Fields
        /// <summary>
        /// Saves the editable fields of rendered report to the stream.
        /// </summary>
        public void SaveEditableFields(string path)
        {
            FileStream stream = null;
            try
            {
                StiFileUtils.ProcessReadOnly(path);
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                SaveEditableFields(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving the editable fields of rendered report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
        }

        /// <summary>
        /// Saves the editable fields of rendered report to the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the editable fields.</param>
        public void SaveEditableFields(Stream stream)
        {
            StiLogService.Write(typeof(StiReport), "Saving the editable fields of rendered report.");
            try
            {
                var container = new StiEditableItemsContainer();
                int pageIndex = 0;
                foreach (StiPage page in this.RenderedPages)
                {
                    var pos = new Hashtable();
                    var comps = page.GetComponents();
                    foreach (StiComponent comp in comps)
                    {
                        var editable = comp as IStiEditable;
                        if (editable != null && editable.Editable)
                        {
                            int position = 1;
                            if (pos[comp.Name] != null) position = ((int)pos[comp.Name]) + 1;
                            container.Items.Add(new StiEditableItem(pageIndex, position, comp.Name, editable.SaveState()));
                            //position ++;
                            pos[comp.Name] = position;

                        }
                    }
                    pageIndex++;
                }

                StiSerializing ser = new StiSerializing(new StiReportObjectStringConverter());
                ser.Serialize(container, stream, "Editable Fields of Rendered Report");
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving the editable fields of rendered report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
        }

        /// <summary>
        /// Loads the editable fields of rendered report from the stream.
        /// </summary>
        public void LoadEditableFields(string path)
        {
            FileStream stream = null;
            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                LoadEditableFields(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading the editable fields of rendered report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
        }

        /// <summary>
        /// Loads the editable fields of rendered report from the stream.
        /// </summary>
        /// <param name="stream">Stream for loading the editable fields.</param>
        public void LoadEditableFields(Stream stream)
        {
            StiLogService.Write(typeof(StiReport), "Loading the editable fields of rendered report.");
            try
            {
                var container = new StiEditableItemsContainer();
                var ser = new StiSerializing(new StiReportObjectStringConverter());
                ser.Deserialize(container, stream, "Editable Fields of Rendered Report");

                foreach (StiEditableItem item in container.Items)
                {
                    var pageIndex = item.PageIndex;
                    var position = item.Position;

                    if (pageIndex < this.RenderedPages.Count)
                    {
                        var page = this.RenderedPages[pageIndex];
                        var comps = page.GetComponents();
                        foreach (StiComponent comp in comps)
                        {
                            var editable = comp as IStiEditable;
                            if (editable != null && editable.Editable && comp.Name == item.ComponentName)
                            {
                                if (position == 1)
                                {
                                    editable.RestoreState(item.TextValue);
                                    break;
                                }
                                else position--;
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading the editable fields of rendered report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
        }
        #endregion

        #region Methods.Load report from assembly
        /// <summary>
        /// Returns the array of rendered reports from assembly.
        /// </summary>
        /// <param name="assembly">Assembly with reports.</param>
        /// <returns>The array of created reports.</returns>
        public static StiReport[] GetReportsFromAssembly(Assembly assembly)
        {
            try
            {
                var reports = new List<StiReport>();

                var types = assembly.GetTypes();
                foreach (var type in types)
                {
                    if (type.IsSubclassOf(typeof(StiReport)))
                    {
                        var report = StiActivator.CreateObject(type) as StiReport;
                        StiReportResourcesLoader.LoadReportResourcesFromAssembly(report);
                        report.ApplyStyles();
                        reports.Add(report);
                    }
                }

                return reports.ToArray();
            }
            catch (Exception e)
            {
                StiLogService.Write(typeof(StiReport), "Get report from assembly...ERROR");
                StiLogService.Write(typeof(StiReport), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            return null;
        }

        /// <summary>
        /// Returns a created report from assembly.
        /// </summary>
        /// <param name="assembly">Assembly with a report.</param>
        /// <returns>Created report.</returns>
        public static StiReport GetReportFromAssembly(Assembly assembly)
        {
            try
            {
                var report = GetReportsFromAssembly(assembly)[0];
                report.UpdateReportVersion();
                return report;
            }
            catch (Exception e)
            {
                StiLogService.Write(typeof(StiReport), "Get report from assembly...ERROR");
                StiLogService.Write(typeof(StiReport), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            return null;
        }

        /// <summary>
        /// Returns a created report from assembly.
        /// </summary>
        /// <param name="assemblyFile">The path to assembly.</param>
        /// <returns>Created report.</returns>
        public static StiReport GetReportFromAssembly(string assemblyFile)
        {
            return GetReportFromAssembly(assemblyFile, false);
        }

        /// <summary>
        /// Returns a created report from assembly.
        /// </summary>
        /// <param name="assemblyFile">The path to assembly.</param>
        /// <param name="lockFile">If true then file with assembly is locked but assembly is loaded to memory only once.</param>
        /// <returns>Created report.</returns>
        public static StiReport GetReportFromAssembly(string assemblyFile, bool lockFile)
        {
            if (lockFile)
            {
                try
                {
                    var a = Assembly.LoadFrom(assemblyFile);
                    return GetReportFromAssembly(a);
                }
                catch (Exception e)
                {
                    StiLogService.Write(typeof(StiReport), "Get report from assembly...ERROR");
                    StiLogService.Write(typeof(StiReport), e);

                    if (!StiOptions.Engine.HideExceptions) throw;
                }
            }
            else
            {

                FileStream stream = null;
                try
                {
                    stream = new FileStream(assemblyFile, FileMode.Open, FileAccess.Read, FileShare.Read);
                    return GetReportFromAssembly(stream);
                }
                catch (Exception e)
                {
                    StiLogService.Write(typeof(StiReport), "Get report from assembly...ERROR");
                    StiLogService.Write(typeof(StiReport), e);

                    if (!StiOptions.Engine.HideExceptions) throw;
                }
                finally
                {
                    stream.Close();
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a created report from assembly.
        /// </summary>
        /// <param name="assemblyStream">The stream with assembly.</param>
        /// <returns>Created report.</returns>
        public static StiReport GetReportFromAssembly(Stream assemblyStream)
        {
            try
            {
                var bytes = new byte[assemblyStream.Length];
                assemblyStream.Seek(0, SeekOrigin.Begin);
                assemblyStream.Read(bytes, 0, (int)assemblyStream.Length);
                var a = Assembly.Load(bytes);

                return GetReportsFromAssembly(a)[0];
            }
            catch (Exception e)
            {
                StiLogService.Write(typeof(StiReport), "Get report from assembly...ERROR");
                StiLogService.Write(typeof(StiReport), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            return null;
        }

        /// <summary>
        /// Returns a created report from the byte array.
        /// </summary>
        /// <param name="bytes">The byte array.</param>
        /// <returns>Created report.</returns>
        public static StiReport GetReportFromAssembly(byte[] bytes)
        {
            var stream = new MemoryStream(bytes);
            var report = GetReportFromAssembly(stream);
            report.IsDocument = false;
            stream.Close();
            return report;
        }
        #endregion

        #region Methods.Load document
        /// <summary>
        /// Loads a rendered report from the string.
        /// </summary>
        /// <param name="json">A string for loading a report from it.</param>
        public void LoadDocumentFromJson(string json)
        {
            LoadFromJsonInternal(json);

            IsRendered = true;
            NeedsCompiling = false;
            IsDocument = true;
            LoadDocumentFonts();
        }

        public void LoadDocumentFromJson(Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                this.LoadDocumentFromJson(reader.ReadToEnd());
            }
        }

        /// <summary>
        /// Loads a encrypted rendered report from the stream.
        /// </summary>
        /// <param name="stream">The stream to load a encrypted rendered report.</param>
        /// <param name="key">The key for encryption.</param>
        public virtual void LoadEncryptedDocument(Stream stream, string key)
        {
            #region Stream can't provide Seek operation
            if (!stream.CanSeek)
            {
                var tempBuffer = new byte[stream.Length - stream.Position];
                stream.Read(tempBuffer, (int)stream.Position, (int)(stream.Length - stream.Position));
                stream = new MemoryStream(tempBuffer);
            }
            #endregion

            var service = new StiEncryptedDocumentSLService { Key = key };
            LoadDocument(service, stream);
        }

        /// <summary>
        /// Loads a encrypted rendered report from the file.
        /// </summary>
        /// <param name="path">The file which contains a encrypted rendered report.</param>
        /// <param name="key">The key for encryption.</param>
        public virtual void LoadEncryptedDocument(string path, string key)
        {
            FileStream stream = null;
            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                LoadEncryptedDocument(stream, key);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading encrypted rendered report from file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Loads a encrypted rendered report from the string.
        /// </summary>
        /// <param name="reportStr">A string from which the encrypted rendered report will be loaded from.</param>
        /// <param name="key">The key for encryption.</param>
        public virtual void LoadEncryptedDocumentFromString(string reportStr, string key)
        {
            var bytes = global::System.Convert.FromBase64String(reportStr);

            if ((char)bytes[0] != 'm' || (char)bytes[1] != 'd' || (char)bytes[2] != 'x')
            {
                throw new Exception("This file is a not '.mdx' format.");
            }

            var dest = new byte[bytes.Length - 3];
            Array.Copy(bytes, 3, dest, 0, bytes.Length - 3);
            dest = StiEncryption.Decrypt(dest, key);

            try
            {
                dest = StiGZipHelper.Unpack(dest);
            }
            catch
            {
                throw new Exception("File decryption error: wrong key.");
            }

            var str = Encoding.UTF8.GetString(dest);
            this.LoadDocumentFromString(str);
        }

        /// <summary>
        /// Loads a encrypted rendered report from the byte array.
        /// </summary>
        /// <param name="bytes">The byte array.</param>
        /// <param name="key">The key for encryption.</param>
        public virtual void LoadEncryptedDocument(byte[] bytes, string key)
        {
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream(bytes);
                LoadEncryptedDocument(stream, key);
            }
            finally
            {
                if (stream != null) stream.Close();
            }
        }

        /// <summary>
        /// Loads a packed rendered report from the stream.
        /// </summary>
        /// <param name="stream">The stream to load a packed rendered report.</param>
        public virtual void LoadPackedDocument(Stream stream)
        {
            #region Stream can't provide Seek operation
            if (!stream.CanSeek)
            {
                var tempBuffer = new byte[stream.Length - stream.Position];
                stream.Read(tempBuffer, (int)stream.Position, (int)(stream.Length - stream.Position));
                stream = new MemoryStream(tempBuffer);
            }
            #endregion

            if (!IsPackedFile(stream)) LoadDocument(stream);
            else
            {
                var service = new StiPackedDocumentSLService();
                LoadDocument(service, stream);
            }
        }

        /// <summary>
        /// Loads a packed rendered report from the file.
        /// </summary>
        /// <param name="path">The file which contains a packed rendered report.</param>
        public virtual void LoadPackedDocument(string path)
        {
            FileStream stream = null;
            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                LoadPackedDocument(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading packed rendered report from file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Loads a packed rendered report from the string.
        /// </summary>
        /// <param name="reportStr">A string from which the packed rendered report will be loaded from.</param>
        public virtual void LoadPackedDocumentFromString(string reportStr)
        {
            var str = StiGZipHelper.Unpack(reportStr);
            this.LoadDocumentFromString(str);
        }

        /// <summary>
        /// Loads a packed rendered report from the byte array.
        /// </summary>
        /// <param name="bytes">The byte array.</param>
        public virtual void LoadPackedDocument(byte[] bytes)
        {
            if (!IsPackedFile(bytes))
                LoadDocument(bytes);
            else
            {
                MemoryStream stream = null;
                try
                {
                    stream = new MemoryStream(bytes);
                    LoadPackedDocument(stream);
                }
                finally
                {
                    if (stream != null) stream.Close();
                }
            }
        }

        /// <summary>
        /// Loads a rendered report from the string.
        /// </summary>
        /// <param name="reportStr">A string for loading a report from it.</param>
        public virtual void LoadDocumentFromString(string reportStr)
        {
            MemoryStream ms = null;
            StreamWriter sw = null;
            try
            {
                ms = new MemoryStream();
                sw = new StreamWriter(ms);
                sw.Write(reportStr);
                sw.Flush();
                ms.Flush();

                ms.Seek(0, SeekOrigin.Begin);

                LoadDocument(ms);

            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading rendered report from string");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (sw != null) sw.Close();
                if (ms != null) ms.Close();
            }
        }

        /// <summary>
        /// Loads a rendered report from the byte array.
        /// </summary>
        /// <param name="bytes">The byte frray for loading a rendered report from it.</param>
        public virtual void LoadDocument(byte[] bytes)
        {
            if (IsPackedFile(bytes))
                LoadPackedDocument(bytes);
            else
            {
                MemoryStream stream = null;
                try
                {
                    stream = new MemoryStream(bytes);
                    LoadDocument(stream);
                }
                finally
                {
                    if (stream != null) stream.Close();
                }
            }
        }

        /// <summary>
        /// Loads a rendered report from the stream.
        /// </summary>
        /// <param name="stream">A stream for loading a rendered report.</param>
        public virtual void LoadDocument(Stream stream)
        {
            #region Stream can't provide Seek operation
            if (!stream.CanSeek)
            {
                var tempBuffer = new byte[stream.Length - stream.Position];
                stream.Read(tempBuffer, (int)stream.Position, (int)(stream.Length - stream.Position));
                stream = new MemoryStream(tempBuffer);
            }
            #endregion

            if (IsJsonFile(stream))
                LoadDocumentFromJson(stream);

            else if (IsPackedFile(stream))
                LoadPackedDocument(stream);

            else
            {
                var service = new StiXmlDocumentSLService();
                LoadDocument(service, stream);
            }
        }

        /// <summary>
        /// Loads a rendered report from the file.
        /// </summary>
        /// <param name="path">A file which contains a rendered report.</param>
        public virtual void LoadDocument(string path)
        {
            FileStream stream = null;
            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                LoadDocument(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading rendered report from file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Loads a rendered report from the stream using the provider.
        /// </summary>
        /// <param name="service">A provider for loading a rendered report.</param>
        /// <param name="stream">A stream for loading a rendered report.</param>
        public virtual void LoadDocument(StiDocumentSLService service, Stream stream)
        {
            try
            {
                StiLogService.Write(typeof(StiReport), "Loading rendered report");
                service.Load(this, stream);
                IsRendered = true;
                NeedsCompiling = false;
                IsDocument = true;
                LoadDocumentFonts();
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading rendered report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
        }

        /// <summary>
        /// Loads a rendered report from the file using the provider.
        /// </summary>
        /// <param name="service">A provider for loading a rendered report.</param>
        /// <param name="path">A file for loading a rendered report.</param>
        public virtual void LoadDocument(StiDocumentSLService service, string path)
        {
            StiLogService.Write(typeof(StiReport), "Loading rendered report");
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read);

            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                service.Load(this, stream);
                IsRendered = true;
                NeedsCompiling = false;
                IsDocument = true;
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading rendered report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            finally
            {
                stream.Close();
            }
        }

        /// <summary>
        /// Loads a rendered report template from specified url.
        /// </summary>
        /// <param name="url">Url which will be used for report loading.</param>
        public virtual void LoadDocumentFromUrl(string url)
        {
            using (var cl = new WebClient())
            {
                cl.Credentials = CredentialCache.DefaultCredentials;

                var bytes = cl.DownloadData(url);
                var stream = new MemoryStream(bytes);
                LoadDocument(stream);
            }
        }
        #endregion

        #region Methods.Load report
        /// <summary>
        /// Loads a report template from the string.
        /// </summary>
        /// <param name="json">A string which contains the report template.</param>
        public void LoadFromJson(string json)
        {
            LoadFromJsonInternal(json);
            LoadFonts();
        }

        /// <summary>
        /// Loads a encrypted report template from the stream.
        /// </summary>
        /// <param name="stream">A stream to load a encrypted report template.</param>
        /// <param name="key">The key for encryption.</param>
        public virtual void LoadEncryptedReport(Stream stream, string key)
        {
            #region Stream can't provide Seek operation
            if (!stream.CanSeek)
            {
                var tempBuffer = new byte[stream.Length - stream.Position];
                stream.Read(tempBuffer, (int)stream.Position, (int)(stream.Length - stream.Position));
                stream = new MemoryStream(tempBuffer);
            }
            #endregion

            ReportFile = string.Empty;
            Clear();
            var service = new StiEncryptedReportSLService { Key = key };

            StiOptions.Engine.GlobalEvents.InvokeReportLoading(this, EventArgs.Empty);
            Load(service, stream);
            StiOptions.Engine.GlobalEvents.InvokeReportLoaded(this, EventArgs.Empty);

            IsPackedReport = true;
            IsJsonReport = false;
            isModified = false;
            ReportFile = string.Empty;
            Password = key;
        }

        /// <summary>
        /// Loads a encrypted report template from the file.
        /// </summary>
        /// <param name="path">A file which contains a encrypted report template.</param>
        /// <param name="key">The key for encryption.</param>
        public virtual void LoadEncryptedReport(string path, string key)
        {
            FileStream stream = null;
            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                LoadEncryptedReport(stream, key);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading encrypted report from file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
                this.Password = key;
            }
            ReportFile = path;
        }

        /// <summary>
        /// Loads a encrypted report template from the string.
        /// </summary>
        /// <param name="reportStr">A string which contains the encrypted template.</param>
        /// <param name="key">The key for encryption.</param>
        public virtual void LoadEncryptedReportFromString(string reportStr, string key)
        {
            var bytes = global::System.Convert.FromBase64String(reportStr);

            if ((char)bytes[0] != 'm' || (char)bytes[1] != 'r' || (char)bytes[2] != 'x')
            {
                throw new Exception("This file is a not '.mrx' format.");
            }

            var dest = new byte[bytes.Length - 3];
            Array.Copy(bytes, 3, dest, 0, bytes.Length - 3);
            dest = StiEncryption.Decrypt(dest, key);

            try
            {
                dest = StiGZipHelper.Unpack(dest);
            }
            catch
            {
                throw new Exception("File decryption error: wrong key.");
            }

            var str = Encoding.UTF8.GetString(dest);
            this.LoadFromString(str);
            this.Password = key;
        }

        /// <summary>
        /// Loads a encrypted report template from the byte array.
        /// </summary>
        /// <param name="bytes">The byte array.</param>
        /// <param name="key">The key for encryption.</param>
        public virtual void LoadEncryptedReport(byte[] bytes, string key)
        {
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream(bytes);
                LoadEncryptedReport(stream, key);
            }
            finally
            {
                if (stream != null) stream.Close();
                this.Password = key;
            }
        }

        /// <summary>
        /// Loads a packed report template from the stream.
        /// </summary>
        /// <param name="stream">A stream to load a packed report template.</param>
        public virtual void LoadPackedReport(Stream stream)
        {
            #region Stream can't provide Seek operation
            if (!stream.CanSeek)
            {
                var tempBuffer = new byte[stream.Length - stream.Position];
                stream.Read(tempBuffer, (int)stream.Position, (int)(stream.Length - stream.Position));
                stream = new MemoryStream(tempBuffer);
            }
            #endregion

            if (!IsPackedFile(stream)) Load(stream);
            else
            {
                ReportFile = string.Empty;
                Clear();
                var service = new StiPackedReportSLService();

                StiOptions.Engine.GlobalEvents.InvokeReportLoading(this, EventArgs.Empty);
                Load(service, stream);
                StiOptions.Engine.GlobalEvents.InvokeReportLoaded(this, EventArgs.Empty);

                IsPackedReport = true;
                IsJsonReport = true;
                isModified = false;
                ReportFile = string.Empty;
            }
        }

        /// <summary>
        /// Loads a packed report template from the file.
        /// </summary>
        /// <param name="path">A file which contains a packed report template.</param>
        public virtual void LoadPackedReport(string path)
        {
            FileStream stream = null;
            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                LoadPackedReport(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading packed report from file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Loads a packed report template from the string.
        /// </summary>
        /// <param name="reportStr">A string which contains the report template.</param>
        public virtual void LoadPackedReportFromString(string reportStr)
        {
            var str = StiGZipHelper.Unpack(reportStr);
            this.LoadFromString(str);
        }

        /// <summary>
        /// Loads a packed report template from the byte array.
        /// </summary>
        /// <param name="bytes">The byte array.</param>
        public virtual void LoadPackedReport(byte[] bytes)
        {
            if (!IsPackedFile(bytes))
                Load(bytes);
            else
            {
                MemoryStream stream = null;
                try
                {
                    stream = new MemoryStream(bytes);
                    LoadPackedReport(stream);
                }
                finally
                {
                    if (stream != null) stream.Close();
                }
            }
        }

        /// <summary>
        /// Loads a report template from the stream.
        /// </summary>
        /// <param name="stream">A stream for loading a report template.</param>
        public virtual void Load(Stream stream)
        {
            #region Stream can't provide Seek operation
            if (!stream.CanSeek)
            {
                var tempBuffer = new byte[stream.Length - stream.Position];
                stream.Read(tempBuffer, (int)stream.Position, (int)(stream.Length - stream.Position));
                stream = new MemoryStream(tempBuffer);
            }
            #endregion

            if (IsPackedFile(stream))
                LoadPackedReport(stream);

            else
            {
                ReportFile = string.Empty;
                IsJsonReport = false;
                Clear(false);

                StiReportSLService service;

                if (IsJsonFile(stream))
                {
                    service = new StiJsonReportSLService();
                    IsJsonReport = true;
                }
                else
                    service = new StiXmlReportSLService();

                Load(service, stream);
                IsPackedReport = false;
                isModified = false;
                ReportFile = string.Empty;
                IsDocument = false;
            }
        }

        /// <summary>
        /// Loads a report template from the byte array.
        /// </summary>
        /// <param name="bytes">A byte array which contains the report template.</param>
        public virtual void Load(byte []bytes)
        {
            if (IsPackedFile(bytes))
                LoadPackedReport(bytes);

            else
            {
                MemoryStream stream = null;
                try
                {
                    stream = new MemoryStream(bytes);
                    Load(stream);
                }
                finally
                {
                    if (stream != null) stream.Close();
                }
            }
        }

        /// <summary>
        /// Loads a report template from the file.
        /// </summary>
        /// <param name="path">A file which contains the report template.</param>
        public virtual void Load(string path)
        {
            FileStream stream = null;
            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                Load(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading report from file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Loads a report template from the string.
        /// </summary>
        /// <param name="reportStr">A string which contains the report template.</param>
        public virtual void LoadFromString(string reportStr)
        {
            MemoryStream ms = null;
            StreamWriter sw = null;
            try
            {
                ms = new MemoryStream();
                sw = new StreamWriter(ms);
                sw.Write(reportStr);
                sw.Flush();
                ms.Flush();

                ms.Seek(0, SeekOrigin.Begin);

                Load(ms);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading report from string");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (sw != null) sw.Close();
                if (ms != null) ms.Close();
            }
        }

        /// <summary>
        /// Loads a report template from the stream using provider.
        /// </summary>
        /// <param name="service">A provider which loads a report template.</param>
        /// <param name="stream">A stream for loading a report template.</param>
        public virtual void Load(StiReportSLService service, Stream stream)
        {
            try
            {
                Clear(false);
                StiLogService.Write(typeof(StiReport), "Loading report");
                StiOptions.Engine.GlobalEvents.InvokeReportLoading(this, EventArgs.Empty);

                this.EngineVersion = StiEngineVersion.EngineV1;
                service.Load(this, stream);

                StiOptions.Engine.GlobalEvents.InvokeReportLoaded(this, EventArgs.Empty);
                ApplyStyles();
                LoadFonts();
                CorrectFormatProperties();
                if (StiOptions.Engine.FullTrust && StiOptions.Engine.PackScriptAfterReportLoaded) ScriptPack();
                this.UpdateInheritedReport();

                IsJsonReport = service is StiJsonReportSLService;

                IsPackedReport = false;
                isModified = false;
                IsDocument = false;

                ReportFile = string.Empty;
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
        }

        /// <summary>
        /// Fix - correct Format property of the Text components. 
        /// It's serialization order issue, Format property is calculated before all properties are applied to the TextFormat.
        /// </summary>
        private void CorrectFormatProperties()
        {
            var comps = this.GetComponents();
            foreach (StiComponent comp in comps)
            {
                var text = comp as StiText;
                if (text != null && !(text.TextFormat is Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService))
                {
                    var storedService = text.TextFormat;
                    text.TextFormat = null;
                    text.TextFormat = storedService;
                }
            }
        }

        /// <summary>
        /// Loads a report template from the file using the provider.
        /// </summary>
        /// <param name="service">A provider which loads a report template.</param>
        /// <param name="path">A file for loading a report template.</param>
        public virtual void Load(StiReportSLService service, string path)
        {
            Clear(false);
            StiLogService.Write(typeof(StiReport), "Loading report");
            FileStream stream = null;

            try
            {
                this.EngineVersion = StiEngineVersion.EngineV1;
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                service.Load(this, stream);
                IsDocument = false;

                this.UpdateInheritedReport();
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Loading report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;

        }

        /// <summary>
        /// Loads a report template from specified url.
        /// </summary>
        /// <param name="url">Url which will be used for report loading.</param>
        public virtual void LoadFromUrl(string url)
        {
            using (var cl = new WebClient())
            {
                cl.Credentials = CredentialCache.DefaultCredentials;
                var bytes = cl.DownloadData(url);
                var stream = new MemoryStream(bytes);
                Load(stream);
            }
        }
        #endregion

        #region Methods.Save&Load Json Helper
        internal string SaveToJsonInternal(StiJsonSaveMode mode)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

                #region Save
                var rootJsonObject = new JObject();

                rootJsonObject.AddPropertyStringNullOrEmpty("ReportVersion", ReportVersion);
                rootJsonObject.AddPropertyStringNullOrEmpty("ReportGuid", ReportGuid);
                rootJsonObject.AddPropertyStringNullOrEmpty("ReportName", PropName);
                rootJsonObject.AddPropertyStringNullOrEmpty("ReportAlias", ReportAlias);
                rootJsonObject.AddPropertyStringNullOrEmpty("ReportAuthor", ReportAuthor);
                rootJsonObject.AddPropertyStringNullOrEmpty("ReportDescription", ReportDescription);
                rootJsonObject.AddPropertyDateTime("ReportCreated", ReportCreated);              // DateTime !!!!!!!!!!!!
                rootJsonObject.AddPropertyDateTime("ReportChanged", ReportChanged);              // DateTime !!!!!!!!!!!!
                rootJsonObject.AddPropertyEnum("EngineVersion", EngineVersion);
                rootJsonObject.AddPropertyEnum("NumberOfPass", NumberOfPass, StiNumberOfPass.SinglePass);
                rootJsonObject.AddPropertyEnum("CalculationMode", CalculationMode, StiCalculationMode.Compilation);
                rootJsonObject.AddPropertyEnum("ReportUnit", ReportUnit, StiReportUnitType.Centimeters);
                rootJsonObject.AddPropertyBool("CacheAllData", CacheAllData);
                rootJsonObject.AddPropertyEnum("ReportCacheMode", ReportCacheMode, StiReportCacheMode.Off);
                rootJsonObject.AddPropertyBool("ConvertNulls", ConvertNulls, true);
                rootJsonObject.AddPropertyBool("RetrieveOnlyUsedData", RetrieveOnlyUsedData);
                rootJsonObject.AddPropertyEnum("PreviewMode", PreviewMode, StiPreviewMode.Standard);
                rootJsonObject.AddPropertyInt("StopBeforePage", StopBeforePage);
                rootJsonObject.AddPropertyInt("PreviewSettings", PreviewSettings, (int)(StiPreviewSettings.Default));
                rootJsonObject.AddPropertyInt("Collate", Collate, 1);
                rootJsonObject.AddPropertyEnum("ScriptLanguage", ScriptLanguage, StiReportLanguageType.CSharp);
                rootJsonObject.AddPropertyBool("AutoLocalizeReportOnRun", AutoLocalizeReportOnRun);
                rootJsonObject.AddPropertyEnum("ParametersOrientation", ParametersOrientation, StiOrientation.Horizontal);
                rootJsonObject.AddPropertyBool("RequestParameters", RequestParameters);
                rootJsonObject.AddPropertyBool("CacheTotals", CacheTotals);
                rootJsonObject.AddPropertyStringNullOrEmpty("Culture", Culture);

                rootJsonObject.AddPropertyStringNullOrEmpty("Script", Script);

                if (mode == StiJsonSaveMode.Document)
                {
                    rootJsonObject.AddPropertyJObject("RenderedPages", this.RenderedPages.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("Styles", Styles.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("Resources", Dictionary.Resources.SaveToJsonObjectEx(mode));
                }
                else
                {
                    rootJsonObject.AddPropertyInt("RefreshTime", RefreshTime, 0);

                    rootJsonObject.AddPropertyJObject("PrinterSettings", PrinterSettings.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("MetaTags", MetaTags.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("Styles", Styles.SaveToJsonObject(mode));
                    rootJsonObject.Add("ReferencedAssemblies", StiJsonReportObjectHelper.Serialize.StringArray(ReferencedAssemblies));
                    
                    rootJsonObject.AddPropertyJObject("BeginRenderEvent", BeginRenderEvent.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("RenderingEvent", RenderingEvent.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("EndRenderEvent", EndRenderEvent.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("ExportingEvent", ExportingEvent.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("ExportedEvent", ExportedEvent.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("PrintingEvent", PrintingEvent.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("PrintedEvent", PrintedEvent.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("ReportCacheProcessingEvent", ReportCacheProcessingEvent.SaveToJsonObject(mode));

                    rootJsonObject.AddPropertyJObject("ReportResources", ReportResources.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("GlobalizationStrings", GlobalizationStrings.SaveToJsonObject(mode));
                    rootJsonObject.AddPropertyJObject("Dictionary", Dictionary.SaveToJsonObject(mode));

                    rootJsonObject.AddPropertyJObject("Pages", Pages.SaveToJsonObject(mode));
                }

                return rootJsonObject.ToString(new CultureInfo("en-us"));
                #endregion
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = currentCulture;
            }
        }

        internal void LoadFromJsonInternal(string text)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

                #region Load

                jsonLoaderHelper = new StiJsonLoaderHelper();

                this.Pages.Clear();
                this.Dictionary.Clear();
                this.renderedPages.Clear();
                this.ReportUnit = StiReportUnitType.Centimeters;

                var jObject = (JObject)JsonConvert.DeserializeObject(text);

                foreach (var property in jObject.Properties())
                {
                    switch (property.Name)
                    {
                        case "ReportVersion":
                            this.ReportVersion = property.Value.ToObject<string>();
                            break;

                        case "ReportGuid":
                            this.ReportGuid = property.Value.ToObject<string>();
                            break;

                        case "ReportName":
                            this.PropName = property.Value.ToObject<string>();
                            break;

                        case "ReportAlias":
                            this.ReportAlias = property.Value.ToObject<string>();
                            break;

                        case "ReportFile":
                            this.ReportFile = property.Value.ToObject<string>();
                            break;

                        case "ReportAuthor":
                            this.ReportAuthor = property.Value.ToObject<string>();
                            break;

                        case "RetrieveOnlyUsedData":
                            this.RetrieveOnlyUsedData = property.Value.ToObject<bool>();
                            break;

                        case "ReportDescription":
                            this.ReportDescription = property.Value.ToObject<string>();
                            break;

                        case "ReportCreated":
                            this.ReportCreated = StiJsonReportObjectHelper.Deserialize.DateTime(property);
                            break;

                        case "ReportChanged":
                            this.ReportChanged = StiJsonReportObjectHelper.Deserialize.DateTime(property);
                            break;

                        case "EngineVersion":
                            this.EngineVersion = (StiEngineVersion)Enum.Parse(typeof(StiEngineVersion), property.Value.ToObject<string>());
                            break;

                        case "NumberOfPass":
                            this.NumberOfPass = (StiNumberOfPass)Enum.Parse(typeof(StiNumberOfPass), property.Value.ToObject<string>());
                            break;

                        case "CalculationMode":
                            this.CalculationMode = (StiCalculationMode)Enum.Parse(typeof(StiCalculationMode), property.Value.ToObject<string>());
                            break;

                        case "ReportUnit":
                            this.ReportUnit = (StiReportUnitType)Enum.Parse(typeof(StiReportUnitType), property.Value.ToObject<string>());
                            break;

                        case "CacheAllData":
                            this.CacheAllData = property.Value.ToObject<bool>();
                            break;

                        case "ReportCacheMode":
                            this.ReportCacheMode = (StiReportCacheMode)Enum.Parse(typeof(StiReportCacheMode), property.Value.ToObject<string>());
                            break;

                        case "ConvertNulls":
                            this.ConvertNulls = property.Value.ToObject<bool>();
                            break;

                        case "PreviewMode":
                            this.PreviewMode = (StiPreviewMode)Enum.Parse(typeof(StiPreviewMode), property.Value.ToObject<string>());
                            break;

                        case "StopBeforePage":
                            this.StopBeforePage = property.Value.ToObject<int>();
                            break;

                        case "PreviewSettings":
                            this.PreviewSettings = property.Value.ToObject<int>();
                            break;

                        case "Collate":
                            this.Collate = property.Value.ToObject<int>();
                            break;

                        case "ReferencedAssemblies":
                            this.ReferencedAssemblies = StiJsonReportObjectHelper.Deserialize.StringArray((JObject)property.Value);
                            break;

                        case "ScriptLanguage":
                            this.ScriptLanguage = (StiReportLanguageType)Enum.Parse(typeof(StiReportLanguageType), property.Value.ToObject<string>());
                            break;

                        case "AutoLocalizeReportOnRun":
                            this.AutoLocalizeReportOnRun = property.Value.ToObject<bool>();
                            break;

                        case "ParametersOrientation":
                            this.ParametersOrientation = (StiOrientation)Enum.Parse(typeof(StiOrientation), property.Value.ToObject<string>());
                            break;

                        case "RequestParameters":
                            this.RequestParameters = property.Value.ToObject<bool>();
                            break;

                        case "CacheTotals":
                            this.CacheTotals = property.Value.ToObject<bool>();
                            break;

                        case "Culture":
                            this.Culture = property.Value.ToObject<string>();
                            break;

                        case "Script":
                            this.Script = property.Value.ToObject<string>();
                            break;

                        case "BeginRenderEvent":
                            this.BeginRenderEvent.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "RenderingEvent":
                            this.RenderingEvent.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "EndRenderEvent":
                            this.EndRenderEvent.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "ExportingEvent":
                            this.ExportingEvent.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "ExportedEvent":
                            this.ExportedEvent.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "PrintingEvent":
                            this.PrintingEvent.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "PrintedEvent":
                            this.PrintedEvent.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "ReportCacheProcessingEvent":
                            this.ReportCacheProcessingEvent.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "MetaTags":
                            this.MetaTags.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "ReportResources":
                            this.ReportResources.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "GlobalizationStrings":
                            this.GlobalizationStrings.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "PrinterSettings":
                            this.PrinterSettings.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "Styles":
                            this.Styles.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "Dictionary":
                            this.Dictionary.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "Resources":
                            this.Dictionary.Resources.LoadFromJsonObjectEx((JObject)property.Value, this);
                            break;

                        case "Pages":
                            this.Pages.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "RenderedPages":
                            this.RenderedPages.LoadFromJsonObject((JObject)property.Value);
                            break;

                        case "RefreshTime":
                            this.RefreshTime = property.Value.ToObject<int>();
                            break;

                        default:
                            throw new Exception("Property is not supported!");
                    }
                }

                #region Check

                StiComponentsCollection reportComps = null;

                if (jsonLoaderHelper.MasterComponents.Count > 0)
                {
                    reportComps = this.GetComponents();

                    foreach (var masterComponent in jsonLoaderHelper.MasterComponents)
                    {
                        var chart = masterComponent as Stimulsoft.Report.Chart.StiChart;
                        if (chart != null)
                        {
                            chart.MasterComponent = reportComps[chart.jsonMasterComponentTemp];
                            chart.jsonMasterComponentTemp = null;
                            continue;
                        }

                        var dataBand = masterComponent as StiDataBand;
                        if (dataBand != null)
                        {
                            dataBand.MasterComponent = reportComps[dataBand.jsonMasterComponentTemp];
                            dataBand.jsonMasterComponentTemp = null;
                            continue;
                        }
                    }
                }

                if (jsonLoaderHelper.Clones.Count > 0)
                {
                    if (reportComps == null)
                        reportComps = this.GetComponents();

                    foreach (var clone in jsonLoaderHelper.Clones)
                    {
                        clone.Container = reportComps[clone.jsonContainerValueTemp] as StiContainer;
                        clone.jsonContainerValueTemp = null;
                    }
                }

                if (jsonLoaderHelper.DialogInfo.Count > 0)
                {
                    foreach (var info in jsonLoaderHelper.DialogInfo)
                    {
                        info.BindingVariable = this.Dictionary.Variables[info.jsonLoadedBindingVariableName];
                        info.jsonLoadedBindingVariableName = null;
                    }
                }

                jsonLoaderHelper.Clean();
                jsonLoaderHelper = null;

                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                StiLogService.Write(this.GetType(), "Loading report...ERROR");
                StiLogService.Write(this.GetType(), ex);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = currentCulture;
            }
        }
        #endregion

        #region Methods.Save report
        /// <summary>
        /// Saves a report template to the string.
        /// </summary>
        /// <returns>A string which contains the report template.</returns>
        public string SaveToJsonString()
        {
            return SaveToJsonInternal(StiJsonSaveMode.Report);
        }

        /// <summary>
        /// Saves a encrypted report template in the stream.
        /// </summary>
        /// <param name="stream">A stream to save a encrypted report template.</param>
        public virtual void SaveEncryptedReport(Stream stream, string key)
        {
            var service = new StiEncryptedReportSLService { Key = key };

            StiOptions.Engine.GlobalEvents.InvokeReportSaving(this, EventArgs.Empty);
            Save(service, stream);
            StiOptions.Engine.GlobalEvents.InvokeReportSaved(this, EventArgs.Empty);

            ReportFile = string.Empty;
            IsModified = false;
            Password = key;
        }

        /// <summary>
        /// Saves a encrypted report template in the file.
        /// </summary>
        /// <param name="path">A file to save a encrypted report template.</param>
        public virtual void SaveEncryptedReport(string path, string key)
        {
            FileStream stream = null;
            try
            {
                if (!IsDesigning || !StiOptions.Designer.ReadOnlyAlertOnSave)
                {
                    StiFileUtils.ProcessReadOnly(path);
                }
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                SaveEncryptedReport(stream, key);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving encrypted report to file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
                Password = key;
            }
            ReportFile = path;
        }

        /// <summary>
        /// Saves a encrypted report template in the byte array.
        /// </summary>
        /// <returns>A new byte array containing the encrypted report template.</returns>
        public virtual byte[] SaveEncryptedReportToByteArray(string key)
        {
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream();
                SaveEncryptedReport(stream, key);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Flush();
                    stream.Close();
                }
            }
            return stream.ToArray();
        }

        /// <summary>
        /// Saves a encrypted report template to the string.
        /// </summary>
        /// <returns>A string which contains a encrypted report template.</returns>
        public virtual string SaveEncryptedReportToString(string key)
        {
            var str = this.SaveToString();
            var bytes = Encoding.UTF8.GetBytes(str);
            bytes = StiEncryption.Encrypt(StiGZipHelper.Pack(bytes), key);

            var dest = new byte[bytes.Length + 3];
            dest[0] = (byte)'m';
            dest[1] = (byte)'r';
            dest[2] = (byte)'x';
            Array.Copy(bytes, 0, dest, 3, bytes.Length);

            return global::System.Convert.ToBase64String(dest);
        }

        /// <summary>
        /// Saves a packed report template in the stream.
        /// </summary>
        /// <param name="stream">A stream to save a packed report template.</param>
        public virtual void SavePackedReport(Stream stream)
        {
            var service = new StiPackedReportSLService();
            StiOptions.Engine.GlobalEvents.InvokeReportSaving(this, EventArgs.Empty);
            Save(service, stream);
            StiOptions.Engine.GlobalEvents.InvokeReportSaved(this, EventArgs.Empty);
            ReportFile = string.Empty;
            IsModified = false;
        }

        /// <summary>
        /// Saves a packed report template in the file.
        /// </summary>
        /// <param name="path">A file to save a packed report template.</param>
        public virtual void SavePackedReport(string path)
        {
            FileStream stream = null;
            try
            {
                if (!this.IsDesigning || !StiOptions.Designer.ReadOnlyAlertOnSave)
                {
                    StiFileUtils.ProcessReadOnly(path);
                }
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                SavePackedReport(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving packed report to file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Saves a packed report template in the byte array.
        /// </summary>
        /// <returns>A new byte array containing the packed report template.</returns>
        public virtual byte[] SavePackedReportToByteArray()
        {
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream();
                SavePackedReport(stream);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Flush();
                    stream.Close();
                }
            }
            return stream.ToArray();
        }

        /// <summary>
        /// Saves a packed report template to the string.
        /// </summary>
        /// <returns>A string which contains a packed report template.</returns>
        public virtual string SavePackedReportToString()
        {
            var str = this.SaveToString();
            return StiGZipHelper.Pack(str);
        }

        /// <summary>
        /// Saves a report template in the stream.
        /// </summary>
        /// <param name="stream">A stream for saving a report template.</param>
        public virtual void Save(Stream stream)
        {
            StiReportSLService service;
            if (this.IsJsonReport)
                service = new StiJsonReportSLService();
            else
                service = new StiXmlReportSLService();
                
            Save(service, stream);
            ReportFile = string.Empty;
            IsModified = false;
        }

        /// <summary>
        /// Saves a report template in the file using the provider.
        /// </summary>
        /// <param name="service">A provider which saves the report template.</param>
        /// <param name="stream">A stream for saving a report template.</param>
        public virtual void Save(StiReportSLService service, Stream stream)
        {
            try
            {
                UpdateReportVersion();
                StiLogService.Write(typeof(StiReport), "Saving report");
                StiOptions.Engine.GlobalEvents.InvokeReportSaving(this, EventArgs.Empty);
                service.Save(this, stream);
                StiOptions.Engine.GlobalEvents.InvokeReportSaved(this, EventArgs.Empty);
                IsModified = false;
                ReportFile = string.Empty;

                IsJsonReport = service is StiJsonReportSLService;
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions || this.IsDesigning) throw;
            }
        }

        /// <summary>
        /// Saves a report template in the file using the provider.
        /// </summary>
        /// <param name="service">A provider for saving a rendered report.</param>
        /// <param name="path">A file for saving the report template.</param>
        public virtual void Save(StiReportSLService service, string path)
        {
            UpdateReportVersion();
            StiLogService.Write(typeof(StiReport), "Saving report");
            FileStream stream = null;

            try
            {
                if (!this.IsDesigning || !StiOptions.Designer.ReadOnlyAlertOnSave)
                {
                    StiFileUtils.ProcessReadOnly(path);
                }
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                service.Save(this, stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
            IsModified = false;

            IsJsonReport = service is StiJsonReportSLService;
        }

        /// <summary>
        /// Saves a report template in the file.
        /// </summary>
        /// <param name="path">A file to save a report template.</param>
        public virtual void Save(string path)
        {
            FileStream stream = null;
            try
            {
                if (!this.IsDesigning || !StiOptions.Designer.ReadOnlyAlertOnSave)
                {
                    StiFileUtils.ProcessReadOnly(path);
                }
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                Save(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving report to file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Saves a report template in the file.
        /// </summary>
        /// <param name="path">A file to save a report template.</param>
        public virtual void SaveToJson(string path)
        {
            FileStream stream = null;
            try
            {
                if (!this.IsDesigning || !StiOptions.Designer.ReadOnlyAlertOnSave)
                    StiFileUtils.ProcessReadOnly(path);

                stream = new FileStream(path, FileMode.Create, FileAccess.Write);

                var service = new StiJsonReportSLService();

                StiOptions.Engine.GlobalEvents.InvokeReportSaving(this, EventArgs.Empty);
                Save(service, stream);
                StiOptions.Engine.GlobalEvents.InvokeReportSaved(this, EventArgs.Empty);

                ReportFile = string.Empty;
                IsModified = false;
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving report to file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Saves a report template in the byte array.
        /// </summary>
        /// <returns>A byte array which contains the report template.</returns>
        public virtual byte[] SaveToByteArray()
        {
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream();
                Save(stream);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Flush();
                    stream.Close();
                }
            }

            return stream.ToArray();
        }
        
        /// <summary>
        /// Saves a report template to the string.
        /// </summary>
        /// <returns>A string which contains the report template.</returns>
        public virtual string SaveToString()
        {
            MemoryStream ms = null;
            StreamReader sr = null;
            string reportStr = null;
            try
            {
                ms = new MemoryStream();
                sr = new StreamReader(ms);

                this.Save(ms);
                ms.Flush();
                ms.Seek(0, SeekOrigin.Begin);

                reportStr = sr.ReadToEnd();

            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving report to string");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions || this.IsDesigning) throw;
            }
            finally
            {
                if (sr != null) sr.Close();
                if (ms != null) ms.Close();
            }
            return reportStr;
        }
        #endregion

        #region Methods.Save document
        /// <summary>
        /// Saves a rendered report to the string.
        /// </summary>
        /// <returns>A string which contains the report template.</returns>
        public string SaveDocumentJsonToString()
        {
            return SaveToJsonInternal(StiJsonSaveMode.Document);
        }

        /// <summary>
        /// Saves a encrypted rendered report to the stream.
        /// </summary>
        /// <param name="stream">A stream to save a encrypted rendered report.</param>
        /// <param name="key">The key for encryption.</param>
        public virtual void SaveEncryptedDocument(Stream stream, string key)
        {
            var service = new StiEncryptedDocumentSLService { Key = key };
            SaveDocument(service, stream);
            ReportFile = string.Empty;
        }

        /// <summary>
        /// Saves a encrypted rendered report to the string.
        /// </summary>
        /// <returns>A string which contains a encrypted rendered report.</returns>
        public virtual string SaveEncryptedDocumentToString(string key)
        {
            var str = this.SaveDocumentToString();
            var bytes = Encoding.UTF8.GetBytes(str);
            bytes = StiEncryption.Encrypt(StiGZipHelper.Pack(bytes), key);

            var dest = new byte[bytes.Length + 3];
            dest[0] = (byte)'m';
            dest[1] = (byte)'d';
            dest[2] = (byte)'x';
            Array.Copy(bytes, 0, dest, 3, bytes.Length);

            return global::System.Convert.ToBase64String(dest);
        }

        /// <summary>
        /// Saves a encrypted rendered report to the byte array.
        /// </summary>
        /// <returns>A byte array which contains a encrypted rendered report.</returns>
        public virtual byte[] SaveEncryptedDocumentToByteArray(string key)
        {
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream();
                SaveEncryptedDocument(stream, key);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Flush();
                    stream.Close();
                }
            }
            return stream.ToArray();
        }

        /// <summary>
        /// Saves a packed rendered report in the file.
        /// </summary>
        /// <param name="path">A file for saving a packed rendered report.</param>
        public virtual void SaveEncryptedDocument(string path, string key)
        {
            FileStream stream = null;
            try
            {
                StiFileUtils.ProcessReadOnly(path);
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                SaveEncryptedDocument(stream, key);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving encrypted rendered report to file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Saves a packed rendered report to the stream.
        /// </summary>
        /// <param name="stream">A stream to save a packed rendered report.</param>
        public virtual void SavePackedDocument(Stream stream)
        {
            var service = new StiPackedDocumentSLService();
            SaveDocument(service, stream);
            ReportFile = string.Empty;
        }

        /// <summary>
        /// Saves a packed rendered report to the string.
        /// </summary>
        /// <returns>A string which contains a packed rendered report.</returns>
        public virtual string SavePackedDocumentToString()
        {
            var str = this.SaveDocumentToString();
            return StiGZipHelper.Pack(str);
        }

        /// <summary>
        /// Saves a packed rendered report to the byte array.
        /// </summary>
        /// <returns>A byte array which contains a packed rendered report.</returns>
        public virtual byte[] SavePackedDocumentToByteArray()
        {
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream();
                SavePackedDocument(stream);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Flush();
                    stream.Close();
                }
            }
            return stream.ToArray();
        }

        /// <summary>
        /// Saves a packed rendered report in the file.
        /// </summary>
        /// <param name="path">A file for saving a packed rendered report.</param>
        public virtual void SavePackedDocument(string path)
        {
            FileStream stream = null;
            try
            {
                StiFileUtils.ProcessReadOnly(path);
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                SavePackedDocument(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving packed rendered report to file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Saves a rendered report in the stream.
        /// </summary>
        /// <param name="stream">A stream to save a rendered report.</param>
        public virtual void SaveDocument(Stream stream)
        {
            var service = new StiXmlDocumentSLService();
            SaveDocument(service, stream);
            ReportFile = string.Empty;
        }

        /// <summary>
        /// Saves a rendered report to the string.
        /// </summary>
        /// <returns>A string which contains the report template.</returns>
        public virtual string SaveDocumentToString()
        {
            MemoryStream ms = null;
            StreamReader sr = null;
            string reportStr = null;
            try
            {
                ms = new MemoryStream();
                sr = new StreamReader(ms);
                this.SaveDocument(ms);
                ms.Flush();
                ms.Seek(0, SeekOrigin.Begin);

                reportStr = sr.ReadToEnd();
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving rendered report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if ((!StiOptions.Engine.HideExceptions) || this.IsDesigning) throw;
            }
            finally
            {
                sr.Close();
                ms.Close();
            }
            return reportStr;
        }

        /// <summary>
        /// Saves a rendered report to the byte array.
        /// </summary>
        /// <returns>Returns a byte array which contains a rendered report.</returns>
        public virtual byte[] SaveDocumentToByteArray()
        {
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream();
                SaveDocument(stream);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Flush();
                    stream.Close();
                }
            }

            return stream.ToArray();
        }

        /// <summary>
        /// Saves a rendered report to the stream.
        /// </summary>
        /// <param name="service">A provider which saves a rendered report.</param>
        /// <param name="stream">A stream to save a rendered report.</param>
        public virtual void SaveDocument(StiDocumentSLService service, Stream stream)
        {
            try
            {
                StiLogService.Write(typeof(StiReport), "Saving rendered report");
                service.Save(this, stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving rendered report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }

        }

        /// <summary>
        /// Saves a rendered report using the provider in the file.
        /// </summary>
        /// <param name="service">A provider that saves a rendered report.</param>
        /// <param name="path">A file to save a rendered report.</param>
        public virtual void SaveDocument(StiDocumentSLService service, string path)
        {
            StiLogService.Write(typeof(StiReport), "Saving rendered report");
            FileStream stream = null;
            try
            {
                StiFileUtils.ProcessReadOnly(path);
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                service.Save(this, stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving rendered report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }

        /// <summary>
        /// Saves a rendered report in the file.
        /// </summary>
        /// <param name="path">A file to save a rendered report.</param>
        public virtual void SaveDocument(string path)
        {
            FileStream stream = null;
            try
            {
                StiFileUtils.ProcessReadOnly(path);
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                SaveDocument(stream);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Saving rendered report to file '" + path + "'...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            finally
            {
                if (stream != null) stream.Close();
            }
            ReportFile = path;
        }
        #endregion
    }
}