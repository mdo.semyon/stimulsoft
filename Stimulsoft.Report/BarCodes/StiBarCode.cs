#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.BarCodes
{
	[StiServiceBitmap(typeof(StiComponent), "Stimulsoft.Report.Images.Components.StiBarCode.png")]
    [StiGdiPainter(typeof(Stimulsoft.Report.Painters.StiBarCodeGdiPainter))]
    [StiDesigner("Stimulsoft.Report.Components.Design.StiBarCodeDesigner, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiBarCodeWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
    [StiWpfDesigner("Stimulsoft.Report.WpfDesign.StiWpfBarCodeDesigner, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
	[StiToolbox(true)]
	[StiContextTool(typeof(IStiShift))]
	[StiContextTool(typeof(IStiGrowToHeight))]
	public class StiBarCode : 
		StiComponent,
		IStiBarCode,
		IStiExportImageExtended,
		IStiVertAlignment,
		IStiHorAlignment,
		IStiEnumAngle,
		IStiBorder
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // NonSerialized
            jObject.RemoveProperty("CanShrink");
            jObject.RemoveProperty("CanGrow");

            // StiBarCode
            jObject.AddPropertyEnum("Angle", Angle, StiAngle.Angle0);
            jObject.AddPropertyStringNullOrEmpty("Border", StiJsonReportObjectHelper.Serialize.JBorder(Border));
            jObject.AddPropertyStringNullOrEmpty("ForeColor", StiJsonReportObjectHelper.Serialize.JColor(ForeColor, Color.Black));
            jObject.AddPropertyStringNullOrEmpty("BackColor", StiJsonReportObjectHelper.Serialize.JColor(BackColor, Color.White));
            jObject.AddPropertyBool("AutoScale", AutoScale);
            jObject.AddPropertyBool("ShowLabelText", ShowLabelText, true);
            jObject.AddPropertyBool("ShowQuietZones", ShowQuietZones, true);
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.Font(Font, "Arial", 8, FontStyle.Bold, GraphicsUnit.Pixel));
            jObject.AddPropertyEnum("HorAlignment", HorAlignment, StiHorAlignment.Left);
            jObject.AddPropertyEnum("VertAlignment", VertAlignment, StiVertAlignment.Top);
            jObject.AddPropertyJObject("GetBarCodeEvent", GetBarCodeEvent.SaveToJsonObject(mode));

            jObject.AddPropertyJObject("BarCodeType", BarCodeType.SaveToJsonObject(mode));

            if (mode == StiJsonSaveMode.Document)
            {
                jObject.AddPropertyStringNullOrEmpty("CodeValue", CodeValue);
            }
            else
            {
                jObject.AddPropertyJObject("Code", Code.SaveToJsonObject(mode));
            }

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Angle":
                        this.angle = (StiAngle)Enum.Parse(typeof(StiAngle), property.Value.ToObject<string>());
                        break;

                    case "Border":
                        this.border = StiJsonReportObjectHelper.Deserialize.Border(property);
                        break;

                    case "ForeColor":
                        this.foreColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "BackColor":
                        this.backColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "AutoScale":
                        this.autoScale = property.Value.ToObject<bool>();
                        break;

                    case "ShowLabelText":
                        this.showLabelText = property.Value.ToObject<bool>();
                        break;

                    case "ShowQuietZones":
                        this.showQuietZones = property.Value.ToObject<bool>();
                        break;

                    case "Font":
                        this.font = StiJsonReportObjectHelper.Deserialize.Font(property, this.Font);
                        break;

                    case "HorAlignment":
                        this.horAlignment = (StiHorAlignment)Enum.Parse(typeof(StiHorAlignment), property.Value.ToObject<string>());
                        break;

                    case "VertAlignment":
                        this.vertAlignment = (StiVertAlignment)Enum.Parse(typeof(StiVertAlignment), property.Value.ToObject<string>());
                        break;

                    case "CodeValue":
                        this.codeValue = property.Value.ToObject<string>();
                        break;

                    case "Code":
                        {
                            var exp = new StiBarCodeExpression();
                            exp.LoadFromJsonObject((JObject)property.Value);
                            this.Code = exp;
                        }
                        break;

                    case "GetBarCodeEvent":
                        {
                            var _event = new StiGetBarCodeEvent();
                            _event.LoadFromJsonObject((JObject)property.Value);
                            this.GetBarCodeEvent = _event;
                        }
                        break;

                    case "BarCodeType":
                        {
                            this.barCodeType = StiBarCodeTypeService.CreateFromJsonObject((JObject)property.Value);
                        }
                        break;

                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiBarCode;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var barCodeHelper = new StiPropertyCollection();
            var propHelper = propertyGrid.PropertiesHelper;

            var list = new[] 
            { 
                propHelper.BarCodeEditor(), 
            };
            barCodeHelper.Add(StiPropertyCategories.ComponentEditor, list);

            // BarCodeCategory
            if (level != StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.HorAlignment(), 
                    propHelper.VertAlignment()
                };

                barCodeHelper.Add(StiPropertyCategories.BarCode, list);
            }

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                {
                    propHelper.Left(), 
                    propHelper.Top(), 
                    propHelper.Width(), 
                    propHelper.Height() 
                };
            }
            else
            {
                list = new[] 
                {
                    propHelper.Left(), 
                    propHelper.Top(), 
                    propHelper.Width(), 
                    propHelper.Height(), 
                    propHelper.MinSize(), 
                    propHelper.MaxSize()
                };
            }
            barCodeHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.Border(), 
                    propHelper.Conditions(),
                    propHelper.ComponentStyle()
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.Border(), 
                    propHelper.Conditions(),
                    propHelper.ComponentStyle(),
                    propHelper.UseParentStyles()
                };
            }
            barCodeHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                { 
                    propHelper.GrowToHeight(), 
                    propHelper.Enabled()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[] 
                { 
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.GrowToHeight(), 
                    propHelper.DockStyle(), 
                    propHelper.Enabled(), 
                    propHelper.PrintOn(), 
                    propHelper.ShiftMode()
                };
            }
            else
            {
                list = new[]
                { 
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.GrowToHeight(), 
                    propHelper.DockStyle(), 
                    propHelper.Enabled(), 
                    propHelper.Printable(), 
                    propHelper.PrintOn(), 
                    propHelper.ShiftMode()
                };
            }
            barCodeHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[] { propHelper.Name() };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[] 
                { 
                    propHelper.Name(), 
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.Name(), 
                    propHelper.Alias(), 
                    propHelper.Restrictions(), 
                    propHelper.Locked(), 
                    propHelper.Linked()
                };
            }
            barCodeHelper.Add(StiPropertyCategories.Design, list);

            return barCodeHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // ValueEventsCategory
            var list = new[]
                {
                    StiPropertyEventId.GetBarCodeEvent, 
                    StiPropertyEventId.GetToolTipEvent, 
                    StiPropertyEventId.GetTagEvent
                };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            // NavigationEventsCategory
            list = new[]
                {
                    StiPropertyEventId.GetHyperlinkEvent, 
                    StiPropertyEventId.GetBookmarkEvent
                };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[]
                {
                    StiPropertyEventId.BeforePrintEvent, 
                    StiPropertyEventId.AfterPrintEvent
                };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // MouseEventsCategory
            list = new[]
                {
                    StiPropertyEventId.GetDrillDownReportEvent,
                    StiPropertyEventId.ClickEvent, 
                    StiPropertyEventId.DoubleClickEvent,
                    StiPropertyEventId.MouseEnterEvent, 
                    StiPropertyEventId.MouseLeaveEvent
                };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties

        public override string HelpUrl
        {
            get
            {
                return "user-manual/report_internals_barcodes.htm";
            }
        }

        #endregion

		#region IStiCanShrink override
		[Browsable(false)]
		[StiNonSerialized]
		public override bool CanShrink
		{
			get
			{
				return base.CanShrink;
			}
			set
			{
			}
		}
		#endregion

		#region IStiCanGrow override
		[Browsable(false)]
		[StiNonSerialized]
		public override bool CanGrow
		{
			get
			{
				return base.CanGrow;
			}
			set
			{
			}
		}
		#endregion

		#region StiComponent override
		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.BarCode;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Components;
            }
        }

		/// <summary>
		/// Gets a localized name of the component category.
		/// </summary>
		public override string LocalizedCategory
		{
			get 
			{
				return StiLocalization.Get("Report", "Components");
			}
		}

		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiBarCode");
			}
		}
		#endregion

		#region IStiExportImageExtended
		[Browsable(false)]
		public override bool IsExportAsImage(StiExportFormat format)
		{
            if (format == StiExportFormat.ImageSvg) return false;
            if (format == StiExportFormat.Pdf && !StiOptions.Export.Pdf.RenderBarCodeAsImage) return false;
			return true;
		}


		public virtual Image GetImage(ref float zoom)
		{
			return GetImage(ref zoom, StiExportFormat.None);
		}


		public virtual Image GetImage(ref float zoom, StiExportFormat format)
		{
            StiPainter painter = StiPainter.GetPainter(this.GetType(), StiGuiMode.Gdi);
            return painter.GetImage(this, ref zoom, format);
		}
		#endregion

		#region IStiEnumAngle
		private StiAngle angle = StiAngle.Angle0;
        /// <summary>
        /// Gets or sets angle of a bar code rotation.
        /// </summary>
		[StiSerializable]
		[DefaultValue(StiAngle.Angle0)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		[StiCategory("BarCodeAdditional")]
		[StiOrder(StiPropertyOrder.BarCodeAngle)]
        [Description("Gets or sets angle of a bar code rotation.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiAngle Angle
		{
			get
			{
				return angle;
			}
			set
			{
				angle = value;
			}
		}
		#endregion

		#region IStiBorder
		private StiBorder border = new StiBorder();
		/// <summary>
		/// Gets or sets frame of the component.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.AppearanceBorder)]
		[StiSerializable]
		[Description("Gets or sets frame of the component.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiBorder Border
		{
			get 
			{
				return border;
			}
			set 
			{
				border = value;
			}
		}
		#endregion

		#region IStiBarCode
		private Color foreColor = Color.Black;
		/// <summary>
		/// Gets or sets bar code color.
		/// </summary>
		[StiCategory("BarCodeAdditional")]
		[StiOrder(StiPropertyOrder.BarCodeForeColor)]
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets bar code color.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public Color ForeColor
		{
			get 
			{
				return foreColor;
			}
			set 
			{
				foreColor = value;
			}
		}


		private Color backColor = Color.White;
		/// <summary>
		/// Gets or sets background color of bar code.
		/// </summary>
		[StiCategory("BarCodeAdditional")]
		[StiOrder(StiPropertyOrder.BarCodeBackColor)]
		[StiSerializable()]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets background color of bar code.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public Color BackColor
		{
			get 
			{
				return backColor;
			}
			set 
			{
				backColor = value;
			}
		}


		private bool autoScale = false;
		/// <summary>
		/// Gets or sets value which indicates how bar code will scale its size.
		/// </summary>
		[DefaultValue(false)]
		[StiSerializable]
		[StiCategory("BarCodeAdditional")]
		[StiOrder(StiPropertyOrder.BarCodeAutoScale)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates how bar code will scale its size.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool AutoScale
		{
			get
			{
				return autoScale;
			}
			set
			{
				autoScale = value;
			}
		}


		private bool showLabelText = true;
        /// <summary>
        /// Gets or sets value which indicates will this bar code show label text or no.
        /// </summary>
		[DefaultValue(true)]
		[StiSerializable]
		[StiCategory("BarCodeAdditional")]
		[StiOrder(StiPropertyOrder.BarCodeShowLabelText)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates will this bar code show label text or no.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool ShowLabelText
		{
			get
			{
				return showLabelText;
			}
			set
			{
				showLabelText = value;
			}
		}


        private bool showQuietZones = true;
        /// <summary>
        /// Gets or sets value which indicates will this bar code show quiet zones or no.
        /// </summary>
        [DefaultValue(true)]
        [StiSerializable]
        [StiCategory("BarCodeAdditional")]
        [StiOrder(StiPropertyOrder.BarCodeShowQuietZones)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value which indicates will this bar code show quiet zones or no.")]
        [StiPropertyLevel(StiLevel.Professional)]
        public virtual bool ShowQuietZones
        {
            get
            {
                return showQuietZones;
            }
            set
            {
                showQuietZones = value;
            }
        }


		private StiBarCodeTypeService barCodeType = new StiEAN13BarCodeType();
		/// <summary>
		/// Gets or sets type of the bar code.
		/// </summary>
		[StiCategory("BarCode")]
		[StiOrder(StiPropertyOrder.BarCodeBarCodeType)]
		[StiSerializable(StiSerializationVisibility.Class)]
		[Editor("Stimulsoft.Report.BarCodes.Design.StiBarCodeTypeServiceEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets type of the bar code.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiBarCodeTypeService BarCodeType
		{
			get 
			{
				return barCodeType;
			}
			set 
			{
				barCodeType = value;
			}
		}


		public string GetBarCodeString()
		{
			if (CodeValue != null) return CodeValue;
            if (StiOptions.Designer.CalculateBarcodeValueInDesignMode)
            {
                try
                {
                    string result = global::System.Convert.ToString(Stimulsoft.Report.Engine.StiParser.ParseTextValue(Code.Value, this));
                    if (!string.IsNullOrWhiteSpace(result)) return result;
                }
                catch
                {
                }
            }
            return Code.Value;
		}
		#endregion

		#region IStiHorAlignment
		private StiHorAlignment horAlignment = StiHorAlignment.Left;
		/// <summary>
		/// Gets or sets the horizontal alignment of an barcode.
		/// </summary>
		[StiSerializable]
		[DefaultValue(StiHorAlignment.Left)]
		[StiCategory("BarCode")]
		[StiOrder(StiPropertyOrder.BarCodeHorAlignment)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		[Description("Gets or sets the horizontal alignment of an object.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public StiHorAlignment HorAlignment
		{
			get 
			{
				return horAlignment;
			}
			set 
			{
				horAlignment = value;
			}
		}
		#endregion

		#region IStiVertAlignment
		private StiVertAlignment vertAlignment = StiVertAlignment.Top;
		/// <summary>
		/// Gets or sets the vertical alignment of an barcode.
		/// </summary>
		[StiSerializable]
		[DefaultValue(StiVertAlignment.Top)]
		[StiCategory("BarCode")]
		[StiOrder(StiPropertyOrder.BarCodeVertAlignment)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		[Description("Gets or sets the vertical alignment of an object.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public StiVertAlignment VertAlignment
		{
			get
			{
				return vertAlignment;
			}
			set
			{
				vertAlignment = value;
			}
		}
		#endregion

		#region Expressions
		#region BarCode expression
		private string codeValue = null;
		/// <summary>
		/// Gets or sets the component bar code.
		/// </summary>
		[Browsable(false)]
		[StiSerializable(StiSerializeTypes.SerializeToDocument)]
		[Description("Gets or sets the component bar code.")]
		public string CodeValue
		{
			get
			{
				return codeValue;
			}
			set
			{
				codeValue = value;
			}
		}


		/// <summary>
		/// Gets or sets the expression to fill a code of bar code.
		/// </summary>
		[StiCategory("BarCode")]
		[StiOrder(StiPropertyOrder.BarCodeCode)]
		[StiSerializable(
			 StiSerializeTypes.SerializeToCode |
			 StiSerializeTypes.SerializeToDesigner |
			 StiSerializeTypes.SerializeToSaveLoad)]
		[Description("Gets or sets the expression to fill a code of bar code.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual StiBarCodeExpression Code
		{
			get
			{
                if (barCodeType != null)
                {
                    string combinedCode = barCodeType.GetCombinedCode();
                    if (combinedCode != null) return new StiBarCodeExpression(combinedCode);
                }
				return new StiBarCodeExpression(this, "Code");
			}
			set
			{
				if (value != null)value.Set(this, "Code", value.Value);
			}
		}
		#endregion
		#endregion

		#region Events
		/// <summary>
		/// Invokes all events for this components.
		/// </summary>
		public override void InvokeEvents()
		{
			base.InvokeEvents();
			try
			{
				#region Code
                if (Report.CalculationMode == StiCalculationMode.Compilation)
                {
                    if (this.Events[EventGetBarCode] != null && codeValue == null)
                    {
                        StiValueEventArgs e = new StiValueEventArgs();
                        InvokeGetBarCode(this, e);
                        if (e.Value != null) this.codeValue = e.Value.ToString();
                    }
                }
                else
                {
                    if (codeValue == null)
                    {
                        StiValueEventArgs e = new StiValueEventArgs();
                        InvokeGetBarCode(this, e);
                        if (e.Value != null) this.codeValue = e.Value.ToString();
                    }
                }
				#endregion
			}
			catch (Exception e)
			{
				StiLogService.Write(this.GetType(), "DoEvents...ERROR");
				StiLogService.Write(this.GetType(), e);
			}
		}

		#region GetBarCode
		private static readonly object EventGetBarCode = new object();

		/// <summary>
		/// Occurs when getting the code of barcode.
		/// </summary>
		public event StiValueEventHandler GetBarCode
		{
			add
			{
				base.Events.AddHandler(EventGetBarCode, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventGetBarCode, value);
			}
		}

		/// <summary>
		/// Raises the BarCode event.
		/// </summary>
		protected virtual void OnGetBarCode(StiValueEventArgs e)
		{
		}
		

		/// <summary>
		/// Raises the GetBarCode event.
		/// </summary>
		public void InvokeGetBarCode(StiComponent sender, StiValueEventArgs e)
		{
			try
			{
				OnGetBarCode(e);
                if (Report.CalculationMode == StiCalculationMode.Compilation)
                {
                    StiValueEventHandler handler = base.Events[EventGetBarCode] as StiValueEventHandler;
                    if (handler != null) handler(sender, e);
                }
                else
                {
                    object parserResult = Stimulsoft.Report.Engine.StiParser.ParseTextValue(this.Code.Value, this, sender);
                    if (parserResult != null) e.Value = parserResult;

                    StiValueEventHandler handler = base.Events[EventGetBarCode] as StiValueEventHandler;
                    if (handler != null) handler(sender, e);
                }
			}
			catch (Exception ex)
			{
				string str = string.Format("Expression in BarCode property of '{0}' can't be evaluated!", this.Name);
				StiLogService.Write(this.GetType(), str);
				StiLogService.Write(this.GetType(), ex.Message);
				Report.WriteToReportRenderingMessages(str);

			}
		}
		

		/// <summary>
		/// Occurs when getting the code of bar code.
		/// </summary>
		[StiSerializable]
		[StiCategory("ValueEvents")]
		[Browsable(false)]
		[Description("Occurs when getting the code of bar code.")]
		public StiGetBarCodeEvent GetBarCodeEvent
		{
			get
			{				
				return new StiGetBarCodeEvent(this);
			}
			set
			{
				if (value != null)value.Set(this, value.Script);
			}
		}
		#endregion
		#endregion		
        
        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiBarCode();
        }
        #endregion

        #region this
        private Font font = null;
        /// <summary>
        /// Gets or sets font of bar code.
        /// </summary>
        [StiSerializable]
        [StiCategory("BarCodeAdditional")]
        [StiOrder(StiPropertyOrder.BarCodeFont)]
        [Description("Gets or sets font of bar code.")]
        [StiPropertyLevel(StiLevel.Standard)]
        public virtual Font Font
        {
            get
            {
                if (font == null)
                {
                    font = new Font("Arial", 8, FontStyle.Bold, GraphicsUnit.Pixel);
                }
                return font;
            }
            set
            {
                font = value;
            }
        }

        /// <summary>
        /// Gets or sets the default client area of a component.
        /// </summary>
        [Browsable(false)]
		public override RectangleD DefaultClientRectangle
		{
			get
			{
				return new RectangleD(0, 0, 240, 110);
			}
		}		


		/// <summary>
		/// Creates a new component of the type StiBarCode.
		/// </summary>
		public StiBarCode() : this(RectangleD.Empty)
		{
		}


		/// <summary>
		/// Creates a new component of the type StiBarCode.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiBarCode(RectangleD rect) : base(rect)
		{
			PlaceOnToolbox = false;
			Code.Value = this.barCodeType.DefaultCodeValue;
		}
		#endregion
	}	
}