#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Components;

namespace Stimulsoft.Report.Export
{
    public sealed class StiSegmentPagesDivider
    {
        public static StiPagesCollection Divide(StiPagesCollection pages)
        {
            return Divide(pages, null);
        }

        public static StiPagesCollection Divide(StiPagesCollection pages, StiExportService service)
        {
            var finded = false;
            foreach (StiPage page in pages)
            {
                if (page.SegmentPerWidth > 1 || page.SegmentPerHeight > 1)
                {
                    finded = true;
                    break;
                }
            }
            if (!finded)
                return pages;

            if (service != null)
                service.StatusString = StiLocalization.Get("Report", "PreparingReport");

            var newPages = new StiPagesCollection(pages.Report, pages);
            newPages.CacheMode = pages.CacheMode;

            if (newPages.CacheMode)
                newPages.CanUseCacheMode = true;

            foreach (StiPage page in pages)
            {
                pages.GetPage(page);    //fix 2012.05.31

                if (service != null)
                    service.InvokeExporting(page, pages, service.CurrentPassNumber, service.MaximumPassNumber);

                if (page.SegmentPerWidth > 1 || page.SegmentPerHeight > 1)
                {
                    for (var x = 0; x < page.SegmentPerWidth; x++)
                    {
                        for (var y = 0; y < page.SegmentPerHeight; y++)
                        {
                            var newPage = page.Clone(false, false) as StiPage;
                            newPage.CacheGuid = StiGuidUtils.NewGuid();
                            newPage.SegmentPerWidth = 1;
                            newPage.SegmentPerHeight = 1;

                            var rectM = new RectangleM(
                                (decimal)(x * newPage.Width),
                                (decimal)(y * newPage.Height),
                                (decimal)newPage.Width,
                                (decimal)newPage.Height);

                            var rect = rectM.ToRectangleD();

                            foreach (StiComponent comp in page.Components)
                            {
                                if (comp.Enabled)
                                {
                                    if (rectM.Left <= (decimal)comp.Left && (decimal)comp.Left < rectM.Right 
                                                                         && rectM.Top <= (decimal)comp.Top && (decimal)comp.Top < rectM.Bottom)
                                    {
                                        var needClip = comp.Right > (x + 1.5) * newPage.Width;
                                        if (x == 0 && y == 0 && !needClip)
                                            newPage.Components.Add(comp);

                                        else
                                        {
                                            var newComp = comp.Clone() as StiComponent;
                                            newComp.Left -= rect.Left;
                                            newComp.Top -= rect.Top;

                                            if (needClip)
                                                newComp.Width = newPage.Width * 1.5 - newComp.Left;

                                            newPage.Components.Add(newComp);
                                        }

                                        continue;
                                    }

                                    if ((decimal)comp.Left < rectM.Right && (decimal)comp.Right > rectM.Left 
                                                                         && (decimal)comp.Top < rectM.Bottom && (decimal)comp.Bottom > rectM.Top)
                                    {
                                        var needClip = !(service is StiPdfExportService);

                                        #region Make new component
                                        StiComponent newComp = null;
                                        if (comp is StiContainer) newComp = comp.Clone() as StiContainer;
                                        if (comp is StiText)
                                        {
                                            var txtComp = comp.Clone() as StiText;
                                            if (needClip)
                                                txtComp.Text = string.Empty;

                                            newComp = txtComp;
                                        }
                                        if (comp is StiImage)
                                        {
                                            var cont = new StiContainer
                                            {
                                                Border = (comp as StiImage).Border,
                                                Brush = (comp as StiImage).Brush
                                            };
                                            newComp = cont;
                                        }
                                        if (comp is StiCrossLinePrimitive || comp is StiHorizontalLinePrimitive)
                                            newComp = comp.Clone() as StiComponent;
                                        #endregion

                                        if (newComp != null)
                                        {
                                            var border = new StiBorder();
                                            if (newComp is IStiBorder)
                                                border = ((IStiBorder)newComp).Border;

                                            #region Clipping
                                            if ((decimal) comp.Left < rectM.Left && needClip)
                                            {
                                                newComp.Left = 0;
                                                border.Side &= StiBorderSides.All ^ StiBorderSides.Left;
                                            }
                                            else
                                                newComp.Left -= rect.Left;

                                            if ((decimal) comp.Right > rectM.Right && needClip)
                                            {
                                                newComp.Width = newPage.Width - newComp.Left;
                                                border.Side &= StiBorderSides.All ^ StiBorderSides.Right;
                                            }
                                            else
                                                newComp.Width = comp.Right - (rect.Left + newComp.Left);

                                            if ((decimal) comp.Top < rectM.Top && needClip)
                                            {
                                                newComp.Top = 0;
                                                border.Side &= StiBorderSides.All ^ StiBorderSides.Top;
                                            }
                                            else
                                                newComp.Top -= rect.Top;

                                            if ((decimal) comp.Bottom > rectM.Bottom && needClip)
                                            {
                                                newComp.Height = newPage.Height - newComp.Top;
                                                border.Side &= StiBorderSides.All ^ StiBorderSides.Bottom;
                                            }
                                            else
                                                newComp.Height = comp.Bottom - (rect.Top + newComp.Top);
                                            #endregion

                                            newPage.Components.Add(newComp);
                                        }
                                    }
                                }
                            }

                            newPages.AddV2Internal(newPage);
                        }
                    }
                }
                else
                {
                    newPages.CanUseCacheMode = false;
                    newPages.AddV2Internal(page);
                    newPages.CanUseCacheMode = newPages.CacheMode;
                }
            }
            newPages.CanUseCacheMode = false;

            return newPages;
        }
    }
}
