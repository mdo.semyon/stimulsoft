#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections;
using System.Text;

using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.Helpers;

namespace Stimulsoft.Report.Export
{

    #region StiHtmlUnit
    public class StiHtmlUnit
    {
        public enum StiHtmlUnitType
        {
            Pixel,
            Point
        }

        private const double hiToPt = 0.716;   //must be 0.72, but it's correction of browsers calculation

        public double Value = 0;
        public StiHtmlUnitType UnitType = StiHtmlUnitType.Pixel;

        public override string ToString()
        {
            //if (UnitType == StiHtmlUnitType.Point)
            //{
            //    //return (Math.Round(Value, 1) * hiToPt).ToString() + "pt";
            //    return ((int)(Value * hiToPt * 10) / 10d).ToString() + "pt";
            //    //return (Value / 100d).ToString() + "in";
            //}
            return ((int)Value).ToString() + "px";
        }

        public static string ToPixelString(int value)
        {
            return ((int)value).ToString() + "px";
        }

        //public static StiHtmlUnit Pixel(int value)
        //{
        //    StiHtmlUnit unit = new StiHtmlUnit();
        //    unit.UnitType = StiHtmlUnitType.Pixel;
        //    unit.Value = value;
        //    return unit;
        //}

        public static StiHtmlUnit NewUnit(double value)
        {
            return NewUnit(value, StiHtmlUnitType.Pixel);
        }

        public static StiHtmlUnit NewUnit(double value, StiHtmlUnitType unitType)
        {
            StiHtmlUnit unit = new StiHtmlUnit();
            unit.UnitType = unitType;
            unit.Value = value;
            return unit;
        }

        public static StiHtmlUnit NewUnit(double value, bool usePoints)
        {
            return NewUnit(value, usePoints ? StiHtmlUnitType.Point : StiHtmlUnitType.Pixel);
        }

        public static bool IsNullOrZero(StiHtmlUnit unit)
        {
            return (unit == null) || (unit.Value == 0);
        }
    }
    #endregion

    #region StiHtmlHyperlink
    public class StiHtmlHyperlink
    {
        public string Text;
        public string ToolTip;
        public string NavigateUrl;
        public Hashtable Attributes;
        public Hashtable Style;
        public string ImageUrl;
        public string CssClass;
        public StiHtmlUnit Width;
        public StiHtmlUnit Height;
        public string OpenLinksTarget;
        public string Id;

        public StiHtmlHyperlink()
        {
            Attributes = new Hashtable();
            Style = new Hashtable();
        }
    }
    #endregion

    #region StiHtmlImage
    public class StiHtmlImage
    {
        public string ToolTip;
        public string ImageUrl;
        public StiHtmlUnit Width;
        public StiHtmlUnit Height;
        public StiHtmlUnit Margin;
    }
    #endregion

    #region StiHtmlTableCell
    public class StiHtmlTableCell
    {
        public StiHtmlUnit Width;
        public StiHtmlUnit Height;
        public Hashtable Style;
        public int ColumnSpan;
        public int RowSpan;
        public string CssClass;
        public string Text;
        public string ToolTip;
        public ArrayList Controls;
        public string Id;

        // Fields for HTML viewer interactions
        public string Interaction;
        public string Collapsed;
        public string SortDirection;
        public string DataBandSort;
        public string PageGuid;
        public string PageIndex;
        public string ReportFile;
        public string ComponentIndex;
        public string Editable;

        public StiHtmlTableCell()
        {
            Style = new Hashtable();
            Controls = new ArrayList();
        }
    }
    #endregion

    #region StiHtmlTableRow
    public class StiHtmlTableRow
    {
        public Hashtable Style;
        public ArrayList Cells;
        public StiHtmlUnit Height;

        public StiHtmlTableRow()
        {
            Style = new Hashtable();
            Cells = new ArrayList();
        }
    }
    #endregion

    #region StiHtmlTable
    public class StiHtmlTable
    {
        public string BackImageUrl;
        public StiHtmlUnit Width;
        public int BorderWidth;
        public int CellPadding;
        public int CellSpacing;
        public ArrayList Rows;
        public StiHorAlignment Align = StiHorAlignment.Left;

        public const string MarginsKey = "padding";
        public const string PageBreakBeforeKey = "page-break-before";
        public const string VertAlignKey = "div:vertical-align";
        public const string HorAlignKey = "div:text-align";
        public const string WordwrapKey = "div:wordwrap";

        #region Methods.StringToUrl
        public static string StringToUrl(string input)
        {
            UTF8Encoding enc = new UTF8Encoding();
            byte[] buf = enc.GetBytes(input);
            StringBuilder output = new StringBuilder();
            foreach (byte byt in buf)
            {
                if ((byt <= 0x20) || (byt > 0x7f) || (wrongUrlSymbols.IndexOf((char)byt) != -1))
                {
                    if ((byt <= 0x20) || (byt > 0x7f)) output.Append(string.Format("%{0:x2}", byt));
                    if (byt == 0x22) output.Append("&quot;");
                    if (byt == 0x26) output.Append("&amp;");
                    if (byt == 0x3c) output.Append("&lt;");
                    //if (byt == 0x3e) output.Append("&gt;");
                }
                else
                {
                    output.Append((char)byt);
                }
            }
            return output.ToString();
        }
        //private static string wrongUrlSymbols   = "\x22\x26\x3c\x3e";
        private static string wrongUrlSymbols = "\x22\x26\x3c";
        #endregion

        #region Methods.RenderControl
        public void RenderControl(StiHtmlTextWriter writer)
        {
            WriteTableBegin(writer, false);

            foreach (StiHtmlTableRow row in Rows)
            {
                if (row.Style.ContainsKey(PageBreakBeforeKey))
                {
                    row.Style.Remove(PageBreakBeforeKey);
                    WriteTableEnd(writer);
                    writer.WriteLine();
                    WriteTableBegin(writer, true);
                }

                #region Write table row
                writer.WriteBeginTag("tr");
                if (StiOptions.Export.Html.UseExtendedStyle)
                {
                    writer.WriteAttribute("class", "sBaseStyleFix");
                }
                if (!StiHtmlUnit.IsNullOrZero(row.Height))
                {
                    row.Style["height"] = row.Height.ToString();
                }

                if (row.Style.Count > 0)
                {
                    writer.Write(" style=\"");
                    foreach (DictionaryEntry de in row.Style)
                    {
                        writer.WriteStyleAttribute((string)de.Key, (string)de.Value);
                    }
                    writer.Write("\"");
                }

                writer.WriteLine(">");
                writer.Indent++;

                foreach (StiHtmlTableCell cell in row.Cells)
                {
                    #region Write table cell
                    writer.WriteBeginTag("td");

                    if (!string.IsNullOrEmpty(cell.ToolTip)) writer.WriteAttribute("title", cell.ToolTip);
                    if (!string.IsNullOrEmpty(cell.CssClass)) writer.WriteAttribute("class", cell.CssClass);
                    if (!string.IsNullOrEmpty(cell.Id)) writer.WriteAttribute("id", cell.Id);
                    if (!string.IsNullOrEmpty(cell.Editable)) writer.WriteAttribute("editable", cell.Editable);
                    if (cell.ColumnSpan > 0) writer.WriteAttribute("colspan", cell.ColumnSpan.ToString());
                    if (cell.RowSpan > 0) writer.WriteAttribute("rowspan", cell.RowSpan.ToString());
                    if (!string.IsNullOrEmpty(cell.Interaction)) writer.WriteAttribute("interaction", cell.Interaction);
                    if (!string.IsNullOrEmpty(cell.Collapsed)) writer.WriteAttribute("collapsed", cell.Collapsed);
                    if (!string.IsNullOrEmpty(cell.SortDirection)) writer.WriteAttribute("sort", cell.SortDirection);
                    if (!string.IsNullOrEmpty(cell.DataBandSort)) writer.WriteAttribute("databandsort", cell.DataBandSort);
                    if (!string.IsNullOrEmpty(cell.PageGuid)) writer.WriteAttribute("pageguid", cell.PageGuid);
                    if (!string.IsNullOrEmpty(cell.PageIndex)) writer.WriteAttribute("pageindex", cell.PageIndex);
                    if (!string.IsNullOrEmpty(cell.ReportFile)) writer.WriteAttribute("reportfile", cell.ReportFile);
                    if (!string.IsNullOrEmpty(cell.ComponentIndex)) writer.WriteAttribute("compindex", cell.ComponentIndex);
                    if (!StiHtmlUnit.IsNullOrZero(cell.Height)) cell.Style["height"] = cell.Height.ToString();
                    if (!StiHtmlUnit.IsNullOrZero(cell.Width)) cell.Style["width"] = cell.Width.ToString();

                    object marginsEntry = null;
                    string cellVertAlign = null;
                    string cellHorAlign = null;
                    if (StiOptions.Export.Html.UseStrictTableCellSize && (cell.Style.Count > 0))
                    {
                        if (cell.Style.ContainsKey(MarginsKey))
                        {
                            marginsEntry = cell.Style[MarginsKey];
                            cell.Style.Remove(MarginsKey);
                        }
                        if (cell.Style.ContainsKey(VertAlignKey) && cell.Style.ContainsKey("height"))
                        {
                            cellVertAlign = (string)cell.Style[VertAlignKey];
                            cell.Style.Remove(VertAlignKey);
                        }
                        if (cell.Style.ContainsKey(HorAlignKey) && cell.Style.ContainsKey("width"))
                        {
                            cellHorAlign = (string)cell.Style[HorAlignKey];
                            cell.Style.Remove(HorAlignKey);
                        }
                    }

                    bool cellWordwrap = false;
                    if (cell.Style.ContainsKey(WordwrapKey) && cell.Style.ContainsKey("width"))
                    {
                        cellWordwrap = true;
                        cell.Style.Remove(WordwrapKey);
                    }

                    StiHtmlHyperlink hyperLink = null;
                    StiHtmlImage image = null;
                    if (cell.Controls.Count > 0)
                    {
                        hyperLink = cell.Controls[0] as StiHtmlHyperlink;
                        image = cell.Controls[0] as StiHtmlImage;
                    }

                    if ((image != null) && !StiOptions.Export.Html.UseStrictTableCellSize)
                    {
                        cell.Style["line-height"] = "0";
                    }

                    if (cell.Style.Count > 0)
                    {
                        writer.Write(" style=\"");
                        foreach (DictionaryEntry de in cell.Style)
                        {
                            writer.WriteStyleAttribute((string)de.Key, (string)de.Value);
                        }
                        writer.Write("\"");
                    }

                    writer.Write(">");

                    string stFixStyle = null;

                    #region For UseStrictTableCellSize
                    bool isCellNotEmpty = (cell.Controls.Count > 0) || !string.IsNullOrEmpty(cell.Text);
                    if (isCellNotEmpty && StiOptions.Export.Html.UseStrictTableCellSize)
                    {
                        writer.Write("<div");
                        if (StiOptions.Export.Html.UseExtendedStyle)
                        {
                            writer.WriteAttribute("class", "sBaseStyleFix");

                            string stFont = cell.Style["Font"] as string;
                            string stDecor = cell.Style["text-decoration"] as string;
                            string stColor = cell.Style["color"] as string;
                            if (!string.IsNullOrEmpty(stFont)) stFixStyle = "Font:" + stFont + ";";
                            if (!string.IsNullOrEmpty(stDecor)) stFixStyle += "text-decoration:" + stDecor + ";";
                            if (!string.IsNullOrEmpty(stColor)) stFixStyle += "color:" + stColor + ";";
                        }
                        if (cell.Style.ContainsKey("width") || cell.Style.ContainsKey("height") || (stFixStyle != null))
                        {
                            writer.Write(" style=\"");
                            if (cell.Style.ContainsKey("width"))
                            {
                                writer.WriteStyleAttribute("width", (string)cell.Style["width"]);
                            }
                            if (cell.Style.ContainsKey("height"))
                            {
                                writer.WriteStyleAttribute("height", (string)cell.Style["height"]);
                            }
                            if (!string.IsNullOrEmpty(stFixStyle))
                            {
                                writer.Write(stFixStyle);
                            }
                            writer.Write("\"");
                        }
                        writer.Write(">");

                        if ((cellVertAlign != null) || (cellHorAlign != null))
                        {
                            writer.Write("<div ");
                            if (StiOptions.Export.Html.UseExtendedStyle)
                            {
                                writer.Write("class=\"sBaseStyleFix\" ");
                            }
                            writer.Write("style=\"");
                            writer.WriteStyleAttribute("display", "table-cell");
                            if (cellVertAlign != null)
                            {
                                writer.WriteStyleAttribute("height", (string)cell.Style["height"]);
                                writer.WriteStyleAttribute("vertical-align", cellVertAlign);
                            }
                            if (cellHorAlign != null)
                            {
                                writer.WriteStyleAttribute("width", (string)cell.Style["width"]);
                                writer.WriteStyleAttribute("text-align", cellHorAlign);
                            }
                            if (!string.IsNullOrEmpty(stFixStyle))
                            {
                                writer.Write(stFixStyle);
                            }
                            writer.Write("\">");
                        }
                        if ((marginsEntry != null) || cellWordwrap)
                        {
                            writer.Write("<div ");
                            if (StiOptions.Export.Html.UseExtendedStyle)
                            {
                                writer.Write("class=\"sBaseStyleFix\" ");
                            }
                            writer.Write("style=\"");
                            if (marginsEntry != null)
                            {
                                writer.WriteStyleAttribute("margin", (string)marginsEntry);
                            }
                            if (cellWordwrap)
                            {
                                try
                                {
                                    var margins = ((string)marginsEntry).Replace("px", "").Split(' ');
                                    cell.Width.Value -= (Double.Parse(margins[1]) + Double.Parse(margins[3]));
                                }
                                catch { }
                                writer.WriteStyleAttribute("width", cell.Width.ToString());
                            }
                            if (!string.IsNullOrEmpty(stFixStyle))
                            {
                                writer.Write(stFixStyle);
                            }
                            writer.Write("\">");
                        }
                    }
                    #endregion

                    if (image != null)
                    {
                        writer.WriteBeginTag("img");
                        if (!string.IsNullOrEmpty(image.ToolTip)) writer.WriteAttribute("title", image.ToolTip);
                        if (!string.IsNullOrEmpty(image.ImageUrl)) writer.WriteAttribute("src", StringToUrl(image.ImageUrl));
                        writer.Write(" style=\"");
                        //writer.WriteStyleAttribute("height", image.Height.ToString());
                        //writer.WriteStyleAttribute("width", image.Width.ToString());
                        if (image.Margin == null)
                        {
                            writer.WriteStyleAttribute("height", cell.Height.ToString());
                            writer.WriteStyleAttribute("width", cell.Width.ToString());
                        }
                        else
                        {
                            writer.WriteStyleAttribute("height", StiHtmlUnit.NewUnit(cell.Height.Value + Math.Abs(image.Margin.Value * 2)).ToString());
                            writer.WriteStyleAttribute("width", StiHtmlUnit.NewUnit(cell.Width.Value + Math.Abs(image.Margin.Value * 2)).ToString());
                            writer.WriteStyleAttribute("margin", image.Margin.ToString());
                        }
                        writer.WriteStyleAttribute("border-width", StiHtmlUnit.ToPixelString(0));
                        writer.Write("\" />");
                    }
                    else
                    {
                        if (hyperLink != null)
                        {
                            writer.WriteBeginTag("a");
                            if (!string.IsNullOrEmpty(hyperLink.OpenLinksTarget)) writer.WriteAttribute("target", hyperLink.OpenLinksTarget);
                            if (!string.IsNullOrEmpty(hyperLink.ToolTip)) writer.WriteAttribute("title", hyperLink.ToolTip);
                            if (!string.IsNullOrEmpty(hyperLink.CssClass)) writer.WriteAttribute("class", hyperLink.CssClass);
                            if (hyperLink.Attributes.ContainsKey("name")) writer.WriteAttribute("name", (string)hyperLink.Attributes["name"]);
                            if (hyperLink.Attributes.ContainsKey("guid")) writer.WriteAttribute("guid", (string)hyperLink.Attributes["guid"]);
                            if (!string.IsNullOrEmpty(hyperLink.NavigateUrl)) writer.WriteAttribute("href", StringToUrl(hyperLink.NavigateUrl));
                            if (!string.IsNullOrEmpty(hyperLink.ImageUrl))
                            {
                                hyperLink.Style["display"] = "inline-block";
                                hyperLink.Style["height"] = hyperLink.Height.ToString();
                                hyperLink.Style["width"] = hyperLink.Width.ToString();
                            }
                            if (StiOptions.Export.Html.UseExtendedStyle)
                            {
                                hyperLink.Style["border"] = "0";
                            }
                            if (hyperLink.Style.Count > 0)
                            {
                                writer.Write(" style=\"");
                                foreach (DictionaryEntry de in hyperLink.Style)
                                {
                                    writer.WriteStyleAttribute((string)de.Key, (string)de.Value);
                                }
                                if (!string.IsNullOrEmpty(stFixStyle))
                                {
                                    writer.Write(stFixStyle);
                                }
                                writer.Write("\"");
                            }
                            writer.Write(">");
                            if (!string.IsNullOrEmpty(hyperLink.ImageUrl))
                            {
                                writer.WriteBeginTag("img");
                                if (!string.IsNullOrEmpty(hyperLink.ToolTip)) writer.WriteAttribute("title", hyperLink.ToolTip);
                                writer.WriteAttribute("src", StringToUrl(hyperLink.ImageUrl));
                                writer.Write(" style=\"");
                                writer.WriteStyleAttribute("height", hyperLink.Height.ToString());
                                writer.WriteStyleAttribute("width", hyperLink.Width.ToString());
                                writer.WriteStyleAttribute("border", "0");
                                writer.Write("\" />");
                            }
                            writer.Write(hyperLink.Text ?? cell.Text);
                            writer.WriteFullEndTag("a");
                        }
                        else
                        {
                            writer.Write(cell.Text);
                        }
                    }

                    if (isCellNotEmpty && StiOptions.Export.Html.UseStrictTableCellSize)
                    {
                        if ((marginsEntry != null) || cellWordwrap)
                        {
                            writer.Write("</div>");
                        }
                        if ((cellVertAlign != null) || (cellHorAlign != null))
                        {
                            writer.Write("</div>");
                        }
                        writer.Write("</div>");
                    }

                    writer.WriteFullEndTag("td");
                    #endregion
                }
                writer.WriteLine();
                writer.Indent--;
                writer.WriteFullEndTag("tr");
                #endregion
            }

            WriteTableEnd(writer);
        }

        private void WriteTableBegin(StiHtmlTextWriter writer, bool writePageBreak)
        {
            writer.WriteBeginTag("table");
            writer.WriteAttribute("cellspacing", CellSpacing.ToString());
            writer.WriteAttribute("cellpadding", CellPadding.ToString());
            writer.WriteAttribute("border", "0");
            if (Align != StiHorAlignment.Left)
            {
                writer.WriteAttribute("align", (Align == StiHorAlignment.Center ? "center" : "right"));
            }
            if (StiOptions.Export.Html.UseExtendedStyle)
            {
                writer.WriteAttribute("class", "sBaseStyleFix");
            }

            writer.Write(" style=\"");
            writer.WriteStyleAttribute("border-width", StiHtmlUnit.ToPixelString(BorderWidth));
            writer.WriteStyleAttribute("width", Width.ToString());
            if (!string.IsNullOrEmpty(BackImageUrl))
            {
                writer.WriteStyleAttribute("background-image", string.Format("url('{0}')", StringToUrl(BackImageUrl)));
            }
            writer.WriteStyleAttribute("border-collapse", "collapse");
            if (writePageBreak)
            {
                writer.WriteStyleAttribute(PageBreakBeforeKey, "always");
            }
            writer.Write("\">");

            writer.WriteLine();
            writer.Indent++;

            if (StiOptions.Export.Html.UseExtendedStyle)
            {
                writer.WriteBeginTag("tbody");
                writer.WriteAttribute("class", "sBaseStyleFix");
                writer.WriteLine(">");
                writer.Indent++;
            }
        }

        private void WriteTableEnd(StiHtmlTextWriter writer)
        {
            if (StiOptions.Export.Html.UseExtendedStyle)
            {
                writer.WriteLine();
                writer.Indent--;
                writer.WriteFullEndTag("tbody");
            }

            writer.WriteLine();
            writer.Indent--;
            writer.WriteFullEndTag("table");
        }
        #endregion

        public StiHtmlTable()
        {
            Rows = new ArrayList();
        }
    }
    #endregion

    #region StiHtmlTextWriter
    public class StiHtmlTextWriter
    {
        #region Enums
        public enum WriterMode
        {
            None,
            BeginTag,
            Attribute,
            Data
        }
        #endregion

        #region Fields
        private TextWriter stream = null;
        private WriterMode mode = WriterMode.None;
        private int indent = 0;
        #endregion

        #region Properties
        public int Indent
        {
            get
            {
                return indent;
            }
            set
            {
                indent = value;
            }
        }
        #endregion

        #region Public Methods
        public void Write(string st)
        {
            CheckIndent();
            stream.Write(st);
            mode = WriterMode.Data;
        }
        public void WriteLine()
        {
            stream.WriteLine();
            mode = WriterMode.None;
        }
        public void WriteLine(string st)
        {
            if (string.IsNullOrEmpty(st))
            {
                stream.WriteLine();
            }
            else
            {
                CheckIndent();
                stream.WriteLine(st);
            }
            mode = WriterMode.None;
        }

        public void WriteBeginTag(string st)
        {
            CloseTag();
            CheckIndent();
            stream.Write("<" + st);
            mode = WriterMode.BeginTag;
        }
        public void WriteFullBeginTag(string st)
        {
            CloseTag();
            CheckIndent();
            stream.Write("<" + st + ">");
            mode = WriterMode.Data;
        }

        public void WriteEndTag(string st)
        {
            if (mode == WriterMode.BeginTag)
            {
                stream.Write("/>");
            }
            else
            {
                CloseTag();
                CheckIndent();
                stream.Write("</" + st + ">");
            }
            mode = WriterMode.Data;
        }
        public void WriteFullEndTag(string st)
        {
            CloseTag();
            CheckIndent();
            stream.Write("</" + st + ">");
            mode = WriterMode.Data;
        }

        public void WriteAttribute(string attr, string value)
        {
            stream.Write(" " + attr);
            if (value != null)
            {
                stream.Write("=\"" + value + "\"");
            }
            mode = WriterMode.Attribute;
        }
        public void WriteStyleAttribute(string attr, string value)
        {
            stream.Write(attr + ":" + value + ";");
            mode = WriterMode.Attribute;
        }

        public void Flush()
        {
            stream.Flush();
        }
        #endregion

        #region Private Methods
        private void CloseTag()
        {
            if (mode == WriterMode.Attribute ||
                mode == WriterMode.BeginTag)
            {
                stream.Write(">");
            }
        }

        private void CheckIndent()
        {
            if (mode == WriterMode.None)
            {
                for (int index = 0; index < indent; index++) stream.Write("\t");
            }
        }
        #endregion

        public StiHtmlTextWriter(TextWriter baseStream)
        {
            stream = baseStream;
            mode = WriterMode.None;
            indent = 0;
        }
    }
    #endregion

    public sealed class StiHtmlTableRender
    {
        #region Fields
        private StiHtmlExportService htmlExport = null;
		private StiHtmlExportSettings htmlExportSettings = null;
		#endregion

		#region Properties
		private StiMatrix matrix;
		public StiMatrix Matrix
		{
			get
			{
				return matrix;
			}
			set
			{
				matrix = value;
			}
		}
        #endregion

		#region Methods
		public void RenderStyle(StiCellStyle style)
		{	
			htmlExport.RenderBackColor(null, style.Color);
			htmlExport.RenderTextColor(null, style.TextColor);
			htmlExport.RenderFont(null, style.Font);
			
            htmlExport.RenderBorder(null, style.Border, "top");
            htmlExport.RenderBorder(null, style.BorderL, "left");
            htmlExport.RenderBorder(null, style.BorderR, "right");
            htmlExport.RenderBorder(null, style.BorderB, "bottom");

			htmlExport.RenderTextDirection(null, style.TextOptions);
			htmlExport.RenderTextHorAlignment(null, style);
			htmlExport.RenderVertAlignment(null, style);

			if (style.AbsolutePosition)htmlExport.HtmlWriter.WriteStyleAttribute("position", "absolute");
            htmlExport.HtmlWriter.Write("overflow:hidden;");
            htmlExport.HtmlWriter.Write(string.Format("line-height:{0}em;", Math.Round(1.15 * style.LineSpacing, 2)));
            if (style.TextOptions != null && style.TextOptions.Trimming != StringTrimming.None)
            {
                htmlExport.HtmlWriter.Write("text-overflow:ellipsis;");
            }
        }

        public void RenderStyleTable(StiHtmlTableCell cell, StiCellStyle style)
		{	
			htmlExport.RenderBackColor(cell, style.Color);
			htmlExport.RenderTextColor(cell, style.TextColor);
			htmlExport.RenderFont(cell, style.Font);

            if (cell == null) htmlExport.HtmlWriter.Write("border:0px;");
			htmlExport.RenderBorder(cell, style.Border, "top");
			htmlExport.RenderBorder(cell, style.BorderL, "left");
			htmlExport.RenderBorder(cell, style.BorderR, "right");
			htmlExport.RenderBorder(cell, style.BorderB, "bottom");

			htmlExport.RenderTextDirection(cell, style.TextOptions);
			htmlExport.RenderTextHorAlignment(cell, style);
			htmlExport.RenderVertAlignment(cell, style);

            if (cell == null)
            {
                if (style.AbsolutePosition) htmlExport.HtmlWriter.WriteStyleAttribute("position", "absolute");
                htmlExport.HtmlWriter.Write(string.Format("line-height:{0}em;", Math.Round(1.15 * style.LineSpacing, 2)));
                if (StiOptions.Export.Html.UseStrictTableCellSize || StiOptions.Export.Html.UseStrictTableCellSizeV2)
                {
                    if (style.WordWrap)
                    {
                        htmlExport.HtmlWriter.Write("word-wrap:break-word;");
                    }
                    else
                    {
                        htmlExport.HtmlWriter.Write("white-space:nowrap;");
                    }
                    htmlExport.HtmlWriter.Write("overflow:hidden;");
                    if (style.TextOptions != null && style.TextOptions.Trimming != StringTrimming.None)
                    {
                        htmlExport.HtmlWriter.Write("text-overflow:ellipsis;");
                    }
                }
                else
                {
                    if (!style.WordWrap && StiOptions.Export.Html.UseWordWrapBreakWordMode) htmlExport.HtmlWriter.Write("word-wrap:break-word;");
                }
            }
            else
            {
                if (style.AbsolutePosition) cell.Style["position"] = "absolute";
                cell.Style["line-height"] = string.Format("{0}em", Math.Round(1.15 * style.LineSpacing, 2));
                if (StiOptions.Export.Html.UseStrictTableCellSize || StiOptions.Export.Html.UseStrictTableCellSizeV2)
                {
                    if (style.WordWrap)
                    {
                        cell.Style["word-wrap"] = "break-word";
                    }
                    else
                    {
                        cell.Style["white-space"] = "nowrap";
                    }
                    cell.Style["overflow"] = "hidden";
                    if (style.TextOptions != null && style.TextOptions.Trimming != StringTrimming.None)
                    {
                        cell.Style["text-overflow"] = "ellipsis";
                    }
                }
                else
                {
                    if (!style.WordWrap && StiOptions.Export.Html.UseWordWrapBreakWordMode) cell.Style["word-wrap"] = "break-word";
                }
            }
		}

        public void RenderStyles(bool useBookmarks, bool exportBookmarksOnly, Hashtable cssStyles)
		{			
			htmlExport.HtmlWriter.WriteLine("<style type=\"text/css\">");

            if (StiOptions.Export.Html.UseExtendedStyle)
            {
                htmlExport.HtmlWriter.WriteLine(".sBaseStyleFix { border: 0; }");
            }

			if ((!exportBookmarksOnly) && (htmlExport.useStylesTable))
			{
				for (int index = 0; index < matrix.Styles.Count; index++)
				{
					StiCellStyle style = matrix.Styles[index] as StiCellStyle;
					htmlExport.HtmlWriter.Write(".s" + style.StyleName);
					htmlExport.HtmlWriter.Write("{");
					RenderStyle(style);
					htmlExport.HtmlWriter.WriteLine("}");
				}
			}
			if ((cssStyles != null) && (cssStyles.Count > 0))
			{
				foreach (DictionaryEntry entry in cssStyles)
				{
					htmlExport.HtmlWriter.WriteLine("." + (string)entry.Key + " {" + (string)entry.Value + ";overflow:hidden;}");
				}
			}
			if (useBookmarks)
			{
                if (StiOptions.Export.Html.UseExtendedStyle)
                {
                    htmlExport.HtmlWriter.WriteLine(".dtree {border:0; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;font-size:11px;color:#666;white-space:nowrap;}");
                    htmlExport.HtmlWriter.WriteLine(".dTreeNode {border:0;}");
                    htmlExport.HtmlWriter.WriteLine(".dtreeStyleFix {border:0;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree img {border:0; vertical-align: middle;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a {border:0;line-height:0; color:#333;text-decoration:none;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.node, .dtree a.nodeSel {border:0; white-space: nowrap;padding: 1px 2px 1px 2px;font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;font-size:11px;color:#666;text-decoration: none;font-weight:normal;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.node:hover, .dtree a.nodeSel:hover {border:0; color: #333;text-decoration: underline;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.nodeSel {border:0; background-color: #c0d2ec;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree .clip {border:0; overflow: hidden;}");
                    htmlExport.HtmlWriter.WriteLine(".dtreeframe {border:0; border-right:1px;border-right-style:solid;border-right-color:Gray;}");
                }
                else
                {
                    htmlExport.HtmlWriter.WriteLine(".dtree {font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;font-size:11px;color:#666;white-space:nowrap;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree img {border: 0px;vertical-align: middle;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a {color: #333;text-decoration: none;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.node, .dtree a.nodeSel {white-space: nowrap;padding: 1px 2px 1px 2px;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.node:hover, .dtree a.nodeSel:hover {color: #333;text-decoration: underline;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.nodeSel {background-color: #c0d2ec;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree .clip {overflow: hidden;}");
                    htmlExport.HtmlWriter.WriteLine(".dtreeframe {border-right:1px;border-right-style:solid;border-right-color:Gray;}");
                }
			}
			htmlExport.HtmlWriter.WriteLine("</style>");
		}

		public void RenderStylesTable(bool useBookmarks, bool exportBookmarksOnly)
		{
			RenderStylesTable(useBookmarks, exportBookmarksOnly, null);
		}

        public void RenderStylesTable(bool useBookmarks, bool exportBookmarksOnly, bool addStyleTag)
        {
            RenderStylesTable(useBookmarks, exportBookmarksOnly, addStyleTag, null);
        }

        public void RenderStylesTable(bool useBookmarks, bool exportBookmarksOnly, Hashtable cssStyles)
        {
            RenderStylesTable(useBookmarks, exportBookmarksOnly, true, cssStyles);
        }

		public void RenderStylesTable(bool useBookmarks, bool exportBookmarksOnly, bool addStyleTag, Hashtable cssStyles)
		{
			if (addStyleTag) htmlExport.HtmlWriter.WriteLine("<style type=\"text/css\">");

            if (StiOptions.Export.Html.UseExtendedStyle)
            {
                htmlExport.HtmlWriter.WriteLine(".sBaseStyleFix { border: 0; }");
            }

            if ((!exportBookmarksOnly) && (htmlExport.useStylesTable))
			{
				for (int index = 0; index < matrix.Styles.Count; index++)
				{
					StiCellStyle style = matrix.Styles[index] as StiCellStyle;
					htmlExport.HtmlWriter.Write(".s" + style.StyleName);
					htmlExport.HtmlWriter.Write("{");
                    RenderStyleTable(null, style);
					htmlExport.HtmlWriter.WriteLine("}");
				}
			}
			if ((cssStyles != null) && (cssStyles.Count > 0))
			{
				foreach (DictionaryEntry entry in cssStyles)
				{
					htmlExport.HtmlWriter.WriteLine("." + (string)entry.Key + " {" + (string)entry.Value + ";}");
				}
			}
			if (useBookmarks)
			{
                if (StiOptions.Export.Html.UseExtendedStyle)
                {
                    htmlExport.HtmlWriter.WriteLine(".dtree {border:0; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;font-size:11px;color:#666;white-space:nowrap;}");
                    htmlExport.HtmlWriter.WriteLine(".dTreeNode {border:0;}");
                    htmlExport.HtmlWriter.WriteLine(".dtreeStyleFix {border:0;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree img {border:0; vertical-align: middle;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a {border:0;line-height:0; color:#333;text-decoration:none;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.node, .dtree a.nodeSel {border:0; white-space: nowrap;padding: 1px 2px 1px 2px;font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;font-size:11px;color:#666;text-decoration: none;font-weight:normal;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.node:hover, .dtree a.nodeSel:hover {border:0; color: #333;text-decoration: underline;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.nodeSel {border:0; background-color: #c0d2ec;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree .clip {border:0; overflow: hidden;}");
                    htmlExport.HtmlWriter.WriteLine(".dtreeframe {border:0; border-right:1px;border-right-style:solid;border-right-color:Gray;}");
                }
                else
                {
                    htmlExport.HtmlWriter.WriteLine(".dtree {font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;font-size:11px;color:#666;white-space:nowrap;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree img {border: 0px;vertical-align: middle;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a {color: #333;text-decoration: none;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.node, .dtree a.nodeSel {white-space: nowrap;padding: 1px 2px 1px 2px;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.node:hover, .dtree a.nodeSel:hover {color: #333;text-decoration: underline;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree a.nodeSel {background-color: #c0d2ec;}");
                    htmlExport.HtmlWriter.WriteLine(".dtree .clip {overflow: hidden;}");
                    htmlExport.HtmlWriter.WriteLine(".dtreeframe {border-right:1px;border-right-style:solid;border-right-color:Gray;}");
                }
			}
            if (addStyleTag) htmlExport.HtmlWriter.WriteLine("</style>");
		}

		private double GetWidth(IList listX, int columnIndex, double zoom)
		{
            return ((double)listX[columnIndex + 1] - (double)listX[columnIndex]) * zoom;
		}

		private double GetHeight(IList listY, int rowIndex, double zoom)
		{
			return ((double)listY[rowIndex + 1] - (double)listY[rowIndex]) * zoom;
		}

        //private string PrepareTextForHtml(string text)
        //{
        //    if (text == null) return null;
        //    //return text.Replace("\n", "<br>");

        //    string[] txt = text.Split(new char[] {'\n'});
        //    StringBuilder sbFull = new StringBuilder();
        //    for (int index = 0; index < txt.Length; index++)
        //    {
        //        string st = txt[index];
        //        int pos = 0;
        //        while ((pos < st.Length) && (st[pos] == ' ')) pos++;
        //        if (pos > 0)
        //        {
        //            for (int indexSp = 0; indexSp < pos; indexSp++)
        //            {
        //                sbFull.Append("&nbsp;");
        //            }
        //            sbFull.Append(st.Substring(pos));
        //        }
        //        else
        //        {
        //            sbFull.Append(st);
        //        }
        //        if (index < txt.Length - 1)
        //        {
        //            sbFull.Append("<br>");
        //        }
        //    }
        //    return sbFull.ToString();
        //}

        public void RenderTable(bool renderStyles, string backGroundImageString, bool useBookmarks, bool exportBookmarksOnly, Hashtable cssStyles)
		{
			if (renderStyles)RenderStylesTable(useBookmarks, exportBookmarksOnly, cssStyles);

			StiHtmlTable table = new StiHtmlTable();

            table.Align = htmlExport.PageHorAlignment;

			table.BackImageUrl = backGroundImageString;

			table.Width = StiHtmlUnit.NewUnit(Math.Round(matrix.TotalWidth * htmlExport.zoom, 0), StiOptions.Export.Html.PrintLayoutOptimization);
				
			table.BorderWidth = 0;
			table.CellPadding = 0;
			table.CellSpacing = 0;

			IList listX = matrix.CoordX.GetValueList();
			IList listY = matrix.CoordY.GetValueList();

			bool[,] lines = new bool[matrix.CoordX.Count, matrix.CoordY.Count];

            Hashtable hashStyles = new Hashtable();
            foreach (var hashStyle in matrix.Styles)
            {
                hashStyles[hashStyle] = matrix.Styles.IndexOf(hashStyle);
            }

			htmlExport.StatusString = StiLocalization.Get("Export", "ExportingCreatingDocument");
            StiComponent prevPage = null;

            for (int rowIndex = 0; rowIndex < matrix.CoordY.Count - 1; rowIndex++)
            {
                htmlExport.InvokeExporting(rowIndex, matrix.CoordY.Count, 2, 3);
                if (htmlExport.IsStopped) return;

                double rowHeight = GetHeight(listY, rowIndex, htmlExport.zoom);

                #region Render Row
                StiHtmlTableRow row = new StiHtmlTableRow();
                row.Height = StiHtmlUnit.NewUnit(rowHeight, StiOptions.Export.Html.PrintLayoutOptimization);
                table.Rows.Add(row);

                Color[] cellsColors = new Color[matrix.CoordX.Count - 1];

                for (int columnIndex = 0; columnIndex < matrix.CoordX.Count - 1; columnIndex++)
                {
                    if (!lines[columnIndex, rowIndex])
                    {
                        #region Render Column
                        StiHtmlTableCell cell = new StiHtmlTableCell();
                        row.Cells.Add(cell);

                        double width = GetWidth(listX, columnIndex, htmlExport.zoom);
                        double height = rowHeight;

                        StiCell matrixCell = matrix.Cells[rowIndex, columnIndex];

                        if (matrixCell != null)
                        {
                            #region AddPageBreakAfterCss
                            if (htmlExportSettings.AddPageBreaks)
                            {
                                if (matrixCell.Component != null && matrixCell.Component.Page != prevPage && prevPage != null)
                                {
                                    row.Style[StiHtmlTable.PageBreakBeforeKey] = "always";
                                }
                                prevPage = matrixCell.Component.Page;
                            }
                            #endregion

                            string cellText = (matrixCell.Text != null ? matrixCell.Text : string.Empty);

                            IStiTextOptions textOptions = matrixCell.Component as IStiTextOptions;
                            if (textOptions != null &&
                                StiOptions.Export.Html.ConvertDigitsToArabic &&
                                textOptions.TextOptions.RightToLeft)
                            {
                                cellText = StiExportUtils.ConvertDigitsToArabic(cellText, StiOptions.Export.Html.ArabicDigitsType);
                            }

                            bool flag = true;
                            if (matrixCell.Component != null)
                            {
                                StiText stiText = matrixCell.Component as StiText;
                                if ((stiText != null) && (stiText.CheckAllowHtmlTags()))
                                {
                                    cellText = StiHtmlExportService.ConvertTextWithHtmlTagsToHtmlText(stiText, cellText);
                                    flag = false;
                                }

                                //added 15.04.2008
                                if (StiOptions.Export.Html.ForceWysiwygWordwrap &&
                                    (stiText != null) &&
                                    (!stiText.CheckAllowHtmlTags()) &&
                                    (stiText.TextQuality == StiTextQuality.Wysiwyg) &&
                                    (textOptions != null) &&
                                    (textOptions.TextOptions.WordWrap))
                                {
                                    var newTextLines = StiTextRenderer.GetTextLines(
                                        htmlExport.report.ReportMeasureGraphics,
                                        ref cellText,
                                        stiText.Font,
                                        htmlExport.report.Unit.ConvertToHInches(matrixCell.Component.ComponentToPage(matrixCell.Component.ClientRectangle)),
                                        1,
                                        true,
                                        textOptions.TextOptions.RightToLeft,
                                        StiDpiHelper.GraphicsScale,
                                        textOptions.TextOptions.Angle,
                                        textOptions.TextOptions.Trimming,
                                        stiText.CheckAllowHtmlTags(),
                                        textOptions.TextOptions);
                                    var sb = new StringBuilder();
                                    for (int index = 0; index < newTextLines.Count; index++)
                                    {
                                        string st = newTextLines[index];
                                        sb.Append(st);
                                        if (index < newTextLines.Count - 1) sb.Append("\n");
                                    }
                                    cellText = sb.ToString();
                                }
                            }

                            if (StiOptions.Export.Html.ReplaceSpecialCharacters && flag)
                            {
                                cellText = cellText.Replace("&", "&amp;").Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\xA0", "&nbsp;");
                            }

                            if (StiOptions.Export.Html.PreserveWhiteSpaces && !string.IsNullOrWhiteSpace(cellText) && cellText.Contains("  "))
                            {
                                cell.Style["white-space"] = "pre-wrap";
                                cellText = htmlExport.PrepareTextForHtml(cellText, false);
                            }
                            else
                            {
                                cellText = htmlExport.PrepareTextForHtml(cellText);
                            }

                            #region Colspan & rowspan
                            int colSpan = matrixCell.Width + 1;
                            int rowSpan = matrixCell.Height + 1;

                            for (int cIndex = columnIndex; cIndex < (columnIndex + colSpan); cIndex++)
                            {
                                for (int rIndex = rowIndex; rIndex < (rowIndex + rowSpan); rIndex++)
                                {
                                    lines[cIndex, rIndex] = true;
                                }
                            }
                            if (colSpan > 1) cell.ColumnSpan = colSpan;
                            if (rowSpan > 1) cell.RowSpan = rowSpan;
                            #endregion

                            #region Render Style
                            StiCellStyle cellStyle = matrix.CellStyles[rowIndex, columnIndex];
                            if (cellStyle == null) cellStyle = matrixCell.CellStyle;
                            if (cellStyle != null)
                            {
                                object styleObj = hashStyles[cellStyle];
                                if ((styleObj != null) && (htmlExport.useStylesTable))
                                {
                                    //cell.CssClass = "s" + styleIndex.ToString();
                                    cell.CssClass = "s" + cellStyle.StyleName;
                                }

                                if (StiOptions.Export.Html.UseStrictTableCellSize)
                                {
                                    if (cellStyle.VertAlignment != StiVertAlignment.Top)
                                    {
                                        cell.Style[StiHtmlTable.VertAlignKey] = cellStyle.VertAlignment == StiVertAlignment.Center ? "middle" : "bottom";
                                    }

                                    bool rightToLeft = (cellStyle.TextOptions != null) && (cellStyle.TextOptions.RightToLeft);
                                    string textAlign = null;
                                    if (cellStyle.HorAlignment == StiTextHorAlignment.Left)
                                    {
                                        textAlign = (!rightToLeft ? null : "right");
                                    }
                                    if (cellStyle.HorAlignment == StiTextHorAlignment.Right)
                                    {
                                        textAlign = (rightToLeft ? null : "right");
                                    }
                                    if (cellStyle.HorAlignment == StiTextHorAlignment.Center) textAlign = "center";
                                    if (cellStyle.HorAlignment == StiTextHorAlignment.Width) textAlign = "justify";
                                    if (textAlign != null)
                                    {
                                        cell.Style[StiHtmlTable.HorAlignKey] = textAlign;
                                    }

                                    for (int index = 0; index < colSpan; index++)
                                    {
                                        cellsColors[columnIndex + index] = cellStyle.Color;
                                    }
                                }
                            }
                            if (matrixCell.Component != null)
                            {
                                string sTag = matrixCell.Component.TagValue as string;
                                if (!string.IsNullOrEmpty(sTag))
                                {
                                    string[] sTagArray = matrix.SplitTagWithCache(sTag);
                                    for (int index = 0; index < sTagArray.Length; index++)
                                    {
                                        if (sTagArray[index].ToLower().StartsWith("css", StringComparison.InvariantCulture))
                                        {
                                            string[] stArr = StiMatrix.GetStringsFromTag(sTagArray[index], 3);
                                            if ((stArr.Length > 1) && (htmlExport.useStylesTable))
                                            {
                                                cell.CssClass = stArr[0].Trim();
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (!htmlExport.useStylesTable)
                            {
                                RenderStyleTable(cell, cellStyle);
                            }
                            else
                            {
                                if (StiOptions.Export.Html.UseExtendedStyle)
                                {
                                    htmlExport.RenderTextColor(cell, cellStyle.TextColor, true);
                                    htmlExport.RenderFont(cell, cellStyle.Font);
                                    //htmlExport.RenderTextDirection(cell, cellStyle.TextOptions);
                                }
                            }
                            #endregion

                            //fix for border width
                            if (StiOptions.Export.Html.PrintLayoutOptimization && (cellStyle != null) && (cellStyle.Border != null) && (cellStyle.Border.Style != StiPenStyle.None))
                            {
                                height -= cellStyle.Border.Size;
                                width -= cellStyle.Border.Size;
                                if (height < 0) height = 0;
                                if (width < 0) width = 0;
                            }

                            if (htmlExport.exportQuality == StiHtmlExportQuality.High)
                            {
                                if (StiOptions.Export.Html.ForceIE6Compatibility)
                                {
                                    if (colSpan == 1) cell.Width = StiHtmlUnit.NewUnit(width, StiOptions.Export.Html.PrintLayoutOptimization);
                                    if (rowSpan == 1) cell.Height = StiHtmlUnit.NewUnit(height, StiOptions.Export.Html.PrintLayoutOptimization);
                                }
                                else
                                {
                                    // following code work perfectly for FireFox, but not correctly for IE6
                                    if (colSpan > 1)
                                    {
                                        for (int indexColSpan = 1; indexColSpan < colSpan; indexColSpan++)
                                        {
                                            width += GetWidth(listX, columnIndex + indexColSpan, htmlExport.zoom);
                                        }
                                    }
                                    if (rowSpan > 1)
                                    {
                                        for (int indexRowSpan = 1; indexRowSpan < rowSpan; indexRowSpan++)
                                        {
                                            height += GetHeight(listY, rowIndex + indexRowSpan, htmlExport.zoom);
                                        }
                                    }
                                    cell.Width = StiHtmlUnit.NewUnit(width, StiOptions.Export.Html.PrintLayoutOptimization);
                                    cell.Height = StiHtmlUnit.NewUnit(height, StiOptions.Export.Html.PrintLayoutOptimization);

                                    if (StiOptions.Export.Html.UseStrictTableCellSizeV2 && cellText.Length > 3)
                                    {
                                        cell.Style["max-width"] = cell.Width.ToString();
                                        cell.Style["max-height"] = cell.Height.ToString();
                                    }
                                }
                            }

                            // HTML Viewer Interactions
                            if (htmlExport.RenderWebInteractions && matrixCell.Component != null &&
                                (matrix.IsComponentHasInteraction(matrixCell.Component) || matrix.Interactions[rowIndex, columnIndex, 1] > 0))
                            {
                                StiComponent comp = matrixCell.Component;

                                if (!matrix.IsComponentHasInteraction(comp))
                                {
                                    StiPage page = htmlExport.report.RenderedPages[matrix.Interactions[rowIndex, columnIndex, 0] - 1];
                                    htmlExport.report.RenderedPages.GetPage(page);
                                    comp = page.Components[matrix.Interactions[rowIndex, columnIndex, 1] - 1];
                                }

                                cell.Interaction = comp.Name;

                                // Sorting
                                if (comp.Interaction.SortingEnabled)
                                {
                                    string dataBandName = comp.Interaction.GetSortDataBandName();
                                    StiDataBand dataBand = comp.Report.GetComponentByName(dataBandName) as StiDataBand;
                                    if (dataBand != null)
                                    {
                                        cell.DataBandSort = dataBandName + ";" + string.Join(";", dataBand.Sort);

                                        var index = 0;
                                        string sortDirection = string.Empty;
                                        while (index < dataBand.Sort.Length)
                                        {
                                            var columnName = string.Empty;
                                            sortDirection = dataBand.Sort[index++];

                                            while (
                                                index < dataBand.Sort.Length &&
                                                dataBand.Sort[index] != "ASC" &&
                                                dataBand.Sort[index] != "DESC")
                                            {
                                                if (columnName.Length == 0)
                                                    columnName = dataBand.Sort[index];
                                                else
                                                    columnName += '.' + dataBand.Sort[index];
                                                index++;
                                            }

                                            if (columnName == comp.Interaction.GetSortColumnsString())
                                            {
                                                cell.SortDirection = sortDirection.ToLower();
                                            }
                                        }
                                    }
                                }

                                // Drill-down
                                if (comp.Interaction.DrillDownEnabled && (!string.IsNullOrEmpty(comp.Interaction.DrillDownPageGuid) || !string.IsNullOrEmpty(comp.Interaction.DrillDownReport)))
                                {
                                    if (!string.IsNullOrEmpty(comp.Interaction.DrillDownPageGuid)) cell.PageGuid = comp.Interaction.DrillDownPageGuid;
                                    if (!string.IsNullOrEmpty(comp.Interaction.DrillDownReport)) cell.ReportFile = comp.Interaction.DrillDownReport;
                                    cell.PageIndex = comp.Page.Report.RenderedPages.IndexOf(comp.Page).ToString();
                                    cell.ComponentIndex = comp.Page.Components.IndexOf(comp).ToString();
                                }

                                // Collapsing
                                StiBandInteraction bandInteraction = comp.Interaction as StiBandInteraction;
                                if (bandInteraction != null && bandInteraction.CollapsingEnabled && comp is StiContainer)
                                {
                                    StiContainer cont = comp as StiContainer;
                                    cell.Collapsed = StiDataBandV2Builder.IsCollapsed(cont, false).ToString().ToLower();
                                    cell.ComponentIndex = cont.CollapsingIndex.ToString();
                                }
                            }

                            string hyperlinkValue = null;
                            string tooltipValue = null;
                            string bookmarkValue = null;
                            string bookmarkGuid = null;

                            bool isChart = false;
                            bool isGauge = false;
                            bool isMap = false;

                            IStiExportImage exportImage = matrixCell.ExportImage;

                            if (matrixCell.Component != null)
                            {
                                hyperlinkValue = matrixCell.Component.HyperlinkValue as string;
                                tooltipValue = matrixCell.Component.ToolTipValue as string;
                                bookmarkValue = matrixCell.Component.BookmarkValue as string;

                                if (!string.IsNullOrWhiteSpace(hyperlinkValue) && hyperlinkValue.StartsWith("##")) hyperlinkValue = hyperlinkValue.Substring(1);

                                if (string.IsNullOrEmpty(bookmarkValue))
                                {
                                    #region check range
                                    for (int yy = 0; yy <= matrixCell.Height; yy++)
                                    {
                                        bool breakLoop = false;
                                        for (int xx = 0; xx <= matrixCell.Width; xx++)
                                        {
                                            string bkm = matrix.Bookmarks[rowIndex + yy, columnIndex + xx];
                                            if (!string.IsNullOrEmpty(bkm))
                                            {
                                                bookmarkValue = bkm;
                                                breakLoop = true;
                                                break;
                                            }
                                        }
                                        if (breakLoop) break;
                                    }
                                    #endregion
                                }
                                if (!string.IsNullOrWhiteSpace(matrixCell.Component.Guid) && htmlExport.hashBookmarkGuid.ContainsKey(matrixCell.Component.Guid))
                                {
                                    bookmarkGuid = matrixCell.Component.Guid;
                                }

                                if (StiOptions.Export.Html.AllowStrippedImages && exportImage == null)
                                {
                                    IStiExportImage exp = matrixCell.Component as IStiExportImage;
                                    if (exp != null && exp is IStiExportImageExtended)
                                    {
                                        if (((IStiExportImageExtended)exp).IsExportAsImage(StiExportFormat.HtmlTable) && (textOptions == null || textOptions.TextOptions.Angle == 0))
                                            exportImage = exp;
                                    }
                                }

                                var chart = matrixCell.Component as Stimulsoft.Report.Chart.StiChart;
                                if ((chart != null) && (htmlExport.chartType != StiHtmlChartType.Image))
                                {
                                    isChart = true;
                                }

                                var gauge = matrixCell.Component as Stimulsoft.Report.Gauge.StiGauge;
                                if ((gauge != null) && (htmlExport.chartType != StiHtmlChartType.Image))
                                {
                                    isGauge = true;
                                }

                                var map = matrixCell.Component as Stimulsoft.Report.Maps.StiMap;
                                if ((map != null) && (htmlExport.chartType != StiHtmlChartType.Image))
                                {
                                    isMap = true;
                                }

                                var editable = matrixCell.Component as IStiEditable;
                                if (editable != null && editable.Editable)
                                {
                                    StringBuilder attr = new StringBuilder();
                                    int editableIndex = matrixCell.Component.Page.Components.IndexOf(matrixCell.Component);
                                    attr.AppendFormat("{0};", editableIndex);

                                    var checkBox = matrixCell.Component as StiCheckBox;
                                    if (checkBox != null)
                                    {
                                        Color backColor = Color.Transparent;
                                        if (checkBox.TextBrush is StiSolidBrush) backColor = ((StiSolidBrush)checkBox.TextBrush).Color;
                                        else if (checkBox.TextBrush is StiGradientBrush) backColor = ((StiGradientBrush)checkBox.TextBrush).StartColor;
                                        else if (checkBox.TextBrush is StiGlareBrush) backColor = ((StiGlareBrush)checkBox.TextBrush).StartColor;
                                        else if (checkBox.TextBrush is StiGlassBrush) backColor = ((StiGlassBrush)checkBox.TextBrush).Color;
                                        else if (checkBox.TextBrush is StiHatchBrush) backColor = ((StiHatchBrush)checkBox.TextBrush).ForeColor;

                                        attr.AppendFormat("CheckBox;{0};{1};{2};#{3:X2}{4:X2}{5:X2};{6};#{7:X2}{8:X2}{9:X2}",
                                            checkBox.CheckedValue,
                                            checkBox.CheckStyleForFalse.ToString(),
                                            checkBox.CheckStyleForTrue.ToString(),
                                            checkBox.ContourColor.R, checkBox.ContourColor.G, checkBox.ContourColor.B,
                                            checkBox.Size,
                                            backColor.R, backColor.G, backColor.B);
                                    }

                                    var textBox = matrixCell.Component as StiText;
                                    if (textBox != null)
                                    {
                                        attr.AppendFormat("Text");
                                    }

                                    var richTextBox = matrixCell.Component as StiRichText;
                                    if (richTextBox != null)
                                    {
                                        attr.AppendFormat("RichText");
                                    }

                                    cell.Editable = attr.ToString();
                                }
                            }

                            #region Render Bookmark
                            bool notImageProcessed = true;
                            if (!string.IsNullOrWhiteSpace(bookmarkValue) || !string.IsNullOrEmpty(bookmarkGuid))
                            {
                                StiHtmlHyperlink bookmark = new StiHtmlHyperlink();
                                if (!string.IsNullOrWhiteSpace(bookmarkValue))
                                {
                                    //bookmark.Attributes["name"] = "#" + bookmarkValue;
                                    bookmark.Attributes["name"] = bookmarkValue;
                                }
                                if (!string.IsNullOrEmpty(bookmarkGuid))
                                {
                                    bookmark.Attributes["guid"] = bookmarkGuid;
                                }
                                //bookmark.Text = cellText;
                                bookmark.ToolTip = tooltipValue;
                                bookmark.NavigateUrl = hyperlinkValue;
                                //bookmark.CssClass = cell.CssClass;

                                if (isChart)
                                {
                                    cell.Id = htmlExport.GetGuid(matrixCell.Component);
                                    cell.Text = htmlExport.PrepareChartData(null, matrixCell.Component as Stimulsoft.Report.Chart.StiChart, width, height);
                                }
                                else if (isGauge)
                                {
                                    cell.Id = htmlExport.GetGuid(matrixCell.Component);
                                    cell.Text = htmlExport.PrepareGaugeData(null, matrixCell.Component as Stimulsoft.Report.Gauge.StiGauge, width, height);
                                }
                                else if (isMap)
                                {
                                    cell.Id = htmlExport.GetGuid(matrixCell.Component);
                                    cell.Text = htmlExport.PrepareMapData(null, matrixCell.Component as Stimulsoft.Report.Maps.StiMap, width, height);
                                }
                                else if (exportImage != null)
                                {
                                    float zoom = (float)htmlExport.zoom;

                                    float resolution = htmlExport.imageResolution;
                                    var imageComp = exportImage as StiImage;
                                    if (StiOptions.Export.Html.UseImageResolution && imageComp != null && imageComp.ExistImageToDraw())
                                    {
                                        using (var gdiImage = imageComp.TakeGdiImageToDraw())
                                        {
                                            if (gdiImage != null)
                                            {
                                                var dpix = gdiImage.HorizontalResolution;
                                                if (dpix >= 50 && dpix <= 1250) resolution = dpix;
                                            }
                                        }
                                    }
                                    if (resolution != 100) zoom *= resolution / 100f;

                                    Image image = null;

                                    htmlExport.SetCurrentCulture();
                                    IStiExportImageExtended exportImageExtended = exportImage as IStiExportImageExtended;
                                    if (exportImageExtended != null && exportImageExtended.IsExportAsImage(StiExportFormat.Html))
                                    {
                                        if (htmlExport.imageFormat == ImageFormat.Png)
                                            image = exportImageExtended.GetImage(ref zoom, StiExportFormat.ImagePng);
                                        else
                                            image = exportImageExtended.GetImage(ref zoom, StiExportFormat.HtmlTable);
                                    }
                                    else image = exportImage.GetImage(ref zoom);
                                    htmlExport.RestoreCulture();

                                    if (image != null)
                                    {
                                        Image imgPart = matrix.GetRealImageData(matrixCell, image);
                                        if (imgPart != null) image = imgPart;

                                        if (htmlExport.HtmlImageHost != null)
                                        {
                                            bookmark.ImageUrl = htmlExport.HtmlImageHost.GetImageString(image as Bitmap);
                                        }
                                        //bookmark.CssClass = cell.CssClass;
                                        bookmark.Width = StiHtmlUnit.NewUnit(image.Width / zoom * htmlExport.zoom, StiOptions.Export.Html.PrintLayoutOptimization);
                                        bookmark.Height = StiHtmlUnit.NewUnit(image.Height / zoom * htmlExport.zoom, StiOptions.Export.Html.PrintLayoutOptimization);

                                        image.Dispose();

                                        notImageProcessed = false;
                                    }
                                }

                                if (notImageProcessed && !isChart)
                                {
                                    bookmark.Text = cellText;

                                    if ((matrixCell.Component != null) && (matrixCell.Component is IStiTextBrush))
                                    {
                                        IStiTextBrush textBrush = matrixCell.Component as IStiTextBrush;
                                        Color color = StiBrush.ToColor(textBrush.TextBrush);
                                        bookmark.Style["color"] = htmlExport.FormatColor(color);
                                    }
                                    if ((matrixCell.Component != null) && (matrixCell.Component is IStiFont))
                                    {
                                        IStiFont font = matrixCell.Component as IStiFont;
                                        if (font.Font.Underline)
                                        {
                                            bookmark.Style["text-decoration"] = "underline";
                                        }
                                        else
                                        {
                                            bookmark.Style["text-decoration"] = "none";
                                        }
                                    }
                                }

                                cell.Controls.Add(bookmark);
                            }
                            #endregion

                            #region Render Chart
                            else if (isChart)
                            {
                                if (!string.IsNullOrEmpty(hyperlinkValue))
                                {
                                    StiHtmlHyperlink hyperlink = new StiHtmlHyperlink();
                                    //hyperlink.Text = cellText;
                                    hyperlink.ToolTip = tooltipValue;
                                    hyperlink.NavigateUrl = hyperlinkValue;
                                    hyperlink.OpenLinksTarget = htmlExport.openLinksTarget;
                                    hyperlink.Style["display"] = "block";

                                    cell.Controls.Add(hyperlink);
                                }
                                else
                                {
                                    cell.ToolTip = tooltipValue;
                                }

                                cell.Id = htmlExport.GetGuid(matrixCell.Component);
                                cell.Text = htmlExport.PrepareChartData(null, matrixCell.Component as Stimulsoft.Report.Chart.StiChart, width, height);
                            }
                            #endregion

                            else if (isGauge)
                            {
                                cell.Id = htmlExport.GetGuid(matrixCell.Component);
                                cell.Text = htmlExport.PrepareGaugeData(null, matrixCell.Component as Stimulsoft.Report.Gauge.StiGauge, width, height);
                            }
                            else if (isMap)
                            {
                                cell.Id = htmlExport.GetGuid(matrixCell.Component);
                                cell.Text = htmlExport.PrepareMapData(null, matrixCell.Component as Stimulsoft.Report.Maps.StiMap, width, height);
                            }

                            #region Render Text
                            else if (exportImage == null)
                            {
                                if (hyperlinkValue != null && hyperlinkValue.Length > 0)
                                {
                                    StiHtmlHyperlink hyperlink = new StiHtmlHyperlink();
                                    hyperlink.Text = cellText;
                                    hyperlink.ToolTip = tooltipValue;
                                    hyperlink.NavigateUrl = hyperlinkValue;
                                    hyperlink.OpenLinksTarget = htmlExport.openLinksTarget;
                                    //									hyperlink.CssClass = cell.CssClass;
                                    hyperlink.Style["display"] = "block";
                                    if ((matrixCell.Component != null) && (matrixCell.Component is IStiTextBrush))
                                    {
                                        IStiTextBrush textBrush = matrixCell.Component as IStiTextBrush;
                                        Color color = StiBrush.ToColor(textBrush.TextBrush);
                                        hyperlink.Style["color"] = htmlExport.FormatColor(color);
                                    }
                                    if ((matrixCell.Component != null) && (matrixCell.Component is IStiFont))
                                    {
                                        IStiFont font = matrixCell.Component as IStiFont;
                                        if (font.Font.Underline)
                                        {
                                            hyperlink.Style["text-decoration"] = "underline";
                                        }
                                        else
                                        {
                                            hyperlink.Style["text-decoration"] = "none";
                                        }
                                    }
                                    cell.Controls.Add(hyperlink);
                                }
                                else
                                {
                                    cell.Text = cellText;
                                    cell.ToolTip = tooltipValue;
                                }
                            }
                            #endregion

                            #region Render Image
                            else
                            {
                                float zoom = (float)htmlExport.zoom;

                                float resolution = htmlExport.imageResolution;
							    var imageComp = exportImage as StiImage;
                                if (StiOptions.Export.Html.UseImageResolution && imageComp != null && imageComp.ExistImageToDraw())
                                {
                                    using (var gdiImage = imageComp.TakeGdiImageToDraw())
                                    {
                                        if (gdiImage != null)
                                        {
                                            var dpix = gdiImage.HorizontalResolution;
                                            if (dpix >= 50 && dpix <= 1250) resolution = dpix;
                                        }
                                    }
                                }
                                if (resolution != 100) zoom *= resolution / 100f;

                                Image image = null;

                                htmlExport.SetCurrentCulture();
                                IStiExportImageExtended exportImageExtended = exportImage as IStiExportImageExtended;
                                if (exportImageExtended != null && exportImageExtended.IsExportAsImage(StiExportFormat.Html))
                                {
                                    if (htmlExport.imageFormat == ImageFormat.Png)
                                        image = exportImageExtended.GetImage(ref zoom, StiExportFormat.ImagePng);
                                    else
                                        image = exportImageExtended.GetImage(ref zoom, StiExportFormat.HtmlTable);
                                }
                                else image = exportImage.GetImage(ref zoom);
                                htmlExport.RestoreCulture();

                                if (image != null)
                                {
                                    Image imgPart = matrix.GetRealImageData(matrixCell, image);
                                    if (imgPart != null) image = imgPart;

                                    if (hyperlinkValue != null && hyperlinkValue.Length > 0)
                                    {
                                        StiHtmlHyperlink hyperlink = new StiHtmlHyperlink();
                                        hyperlink.NavigateUrl = hyperlinkValue;
                                        hyperlink.ToolTip = tooltipValue;
                                        if (htmlExport.HtmlImageHost != null)
                                        {
                                            hyperlink.ImageUrl = htmlExport.HtmlImageHost.GetImageString(image as Bitmap);
                                        }
                                        hyperlink.CssClass = cell.CssClass;
                                        hyperlink.Width = StiHtmlUnit.NewUnit(image.Width / zoom * htmlExport.zoom, StiOptions.Export.Html.PrintLayoutOptimization);
                                        hyperlink.Height = StiHtmlUnit.NewUnit(image.Height / zoom * htmlExport.zoom, StiOptions.Export.Html.PrintLayoutOptimization);
                                        hyperlink.OpenLinksTarget = htmlExport.openLinksTarget;
                                        cell.Controls.Add(hyperlink);
                                    }
                                    else
                                    {
                                        string imageURLStr = null;
                                        if (matrixCell.Component != null && matrixCell.Component is StiImage)
                                        {
                                            StiImage imageComponent = matrixCell.Component as StiImage;
                                            if (!(!imageComponent.Stretch || imageComponent.AspectRatio == true || imageComponent.ImageRotation != StiImageRotation.None))
                                            {
                                                string stTemp = imageComponent.ImageURLValue as string;
                                                if (!StiHyperlinkProcessor.IsResourceHyperlink(stTemp))
                                                {
                                                    imageURLStr = stTemp;
                                                }
                                            }
                                        }

                                        StiHtmlImage img = new StiHtmlImage();

                                        if (imageURLStr != null && imageURLStr.Length != 0) img.ImageUrl = imageURLStr;
                                        else
                                        {
                                            if (htmlExport.HtmlImageHost != null)
                                            {
                                                img.ImageUrl = htmlExport.HtmlImageHost.GetImageString(image as Bitmap);
                                            }
                                        }
                                        img.ToolTip = tooltipValue;
                                        //img.Width = StiHtmlUnit.NewUnit(image.Width / zoom * htmlExport.zoom, StiOptions.Export.Html.PrintLayoutOptimization);
                                        //img.Height = StiHtmlUnit.NewUnit(image.Height / zoom * htmlExport.zoom, StiOptions.Export.Html.PrintLayoutOptimization);                                        
                                        StiShape shape = matrixCell.Component as StiShape;
                                        if (shape == null)
                                        {
                                            img.Width = StiHtmlUnit.NewUnit(image.Width / zoom * htmlExport.zoom, false);
                                            img.Height = StiHtmlUnit.NewUnit(image.Height / zoom * htmlExport.zoom, false);
                                        }
                                        else
                                        {
                                            int shift = (int)Math.Round(shape.Size * zoom / 2);
                                            img.Margin = StiHtmlUnit.NewUnit(-shift);
                                            img.Width = StiHtmlUnit.NewUnit((image.Width + shape.Size * zoom) / zoom * htmlExport.zoom, false);
                                            img.Height = StiHtmlUnit.NewUnit((image.Height + shape.Size * zoom) / zoom * htmlExport.zoom, false);
                                            cell.Style.Add("overflow", "visible");
                                        }
                                        cell.Controls.Add(img);
                                    }

                                    image.Dispose();

                                    notImageProcessed = false;
                                }
                            }
                            #endregion

                            #region Cell margins
                            StiText text = matrixCell.Component as StiText;
                            if ((text != null) && (!text.Margins.IsEmpty) && notImageProcessed)
                            {
                                cell.Style[StiHtmlTable.MarginsKey] = string.Format("{0} {1} {2} {3}",
                                    StiHtmlUnit.NewUnit(text.Margins.Top),
                                    StiHtmlUnit.NewUnit(text.Margins.Right),
                                    StiHtmlUnit.NewUnit(text.Margins.Bottom),
                                    StiHtmlUnit.NewUnit(text.Margins.Left));
                            }
                            #endregion

                            if ((textOptions != null) && (textOptions.TextOptions.WordWrap) &&
                                (cellStyle != null) && (cellStyle.VertAlignment != StiVertAlignment.Top || cellStyle.HorAlignment != StiTextHorAlignment.Left))
                            {
                                cell.Style[StiHtmlTable.WordwrapKey] = null;
                            }
                        }
                        else
                        {
                            if (htmlExport.exportQuality == StiHtmlExportQuality.High)
                            {
                                cell.Width = StiHtmlUnit.NewUnit(width, StiOptions.Export.Html.PrintLayoutOptimization);
                                cell.Height = StiHtmlUnit.NewUnit(height, StiOptions.Export.Html.PrintLayoutOptimization);
                            }

                            #region Render Style
                            StiCellStyle cellStyle = matrix.CellStyles[rowIndex, columnIndex];
                            if (cellStyle != null)
                            {
                                object styleObj = hashStyles[cellStyle];
                                if ((styleObj != null) && (htmlExport.useStylesTable))
                                {
                                    //cell.CssClass = "s" + styleIndex.ToString();
                                    cell.CssClass = "s" + cellStyle.StyleName;
                                }
                            }
                            if (string.IsNullOrEmpty(cell.CssClass))
                            {
                                cell.Style["border"] = "0px";
                            }
                            #endregion

                        }
                        #endregion
                    }
                }

                #region UseStrictTableCellSize, background color check
                if (StiOptions.Export.Html.UseStrictTableCellSize)
                {
                    if (cellsColors[0].A != 0)
                    {
                        bool flag = true;
                        for (int index1 = 0; index1 < cellsColors.Length - 1; index1++)
                        {
                            if (!cellsColors[index1].Equals(cellsColors[index1 + 1]))
                            {
                                flag = false;
                                break;
                            }
                        }
                        if (flag)
                        {
                            row.Style["background-color"] = htmlExport.FormatColor(cellsColors[0]);
                        }
                    }
                }
                #endregion

                #endregion
            }


            table.RenderControl(htmlExport.HtmlWriter);
		}
		#endregion
		
		internal StiHtmlTableRender(StiHtmlExportService htmlExport, StiHtmlExportSettings htmlExportSettings, 
			StiPagesCollection pages)
		{
			matrix = new StiMatrix(pages, htmlExport, htmlExport.Styles);
			this.htmlExport = htmlExport;
			this.htmlExportSettings = htmlExportSettings;
		}
	}	
}
