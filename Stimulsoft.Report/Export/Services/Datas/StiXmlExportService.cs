#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Globalization;
using System.Xml;
using System.Data;
using System.Text;
using System.Collections;
using System.IO;
using Stimulsoft.Report.Components;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.WCFService;
using System.Threading;

namespace Stimulsoft.Report.Export
{
    /// <summary>
    /// A class for the XML export.
    /// </summary>
    [StiServiceBitmap(typeof(StiExportService), "Stimulsoft.Report.Images.Exports.Xml.png")]
	public class StiXmlExportService : StiExportService
    {
        #region StiExportService override
        /// <summary>
		/// Gets or sets a default extension of export. 
		/// </summary>
		public override string DefaultExtension
		{
			get
			{
				return "xml";
			}
		}

		public override StiExportFormat ExportFormat
		{
			get
			{
				return StiExportFormat.Xml;
			}
		}

		/// <summary>
        /// Gets a group of the export in the context menu.
		/// </summary>
		public override string GroupCategory
		{
			get
			{
				return "Data";
			}
		}

		/// <summary>
        /// Gets a position of the export in the context menu.
		/// </summary>
		public override int Position
		{
			get
			{
				return (int)StiExportPosition.Data;
			}
		}

        /// <summary>
        /// Gets a name of the export in the context menu.
        /// </summary>
		public override string ExportNameInMenu
		{
			get
			{
				return StiLocalization.Get("Export", "ExportTypeXmlFile");
			}
		}

        /// <summary>
        /// Exports a document to the stream without dialog of the saving file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream in which report will be exported.</param>
        /// <param name="settings">A settings for the report exporting.</param>
        public override void ExportTo(StiReport report, Stream stream, StiExportSettings settings)
        {
            ExportXml(report, stream, settings as StiDataExportSettings);
        }
        
        /// <summary>
        /// Exports a rendered report to the XML file. Also the file can be sent via e-mail.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="fileName">A name of the file for exporting a rendered report.</param>
        /// <param name="sendEMail">A parameter indicating whether the exported report will be sent via e-mail.</param>
        public override void Export(StiReport report, string fileName, bool sendEMail, StiGuiMode guiMode)
		{
            using (var form = StiGuiOptions.GetExportFormRunner("StiXmlExportSetupForm", guiMode, this.OwnerWindow))
			{
                form["OpenAfterExportEnabled"] = !sendEMail;

                this.report = report;
                this.fileName = fileName;
                this.sendEMail = sendEMail;
                this.guiMode = guiMode;

                form.Complete += form_Complete;
                form.ShowDialog();
			}
		}

        private StiReport report;
        private string fileName;
        private bool sendEMail;
        private StiGuiMode guiMode;
        private void form_Complete(IStiFormRunner form, StiShowDialogCompleteEvetArgs e)
        {
            if (e.DialogResult)
            {
                if (StiOptions.WCFService.WCFExportDocumentEventIsUsed)
                {
                    StiOptions.WCFService.InvokeWCFExportDocument(report, StiExportSettingsHelper.GetXmlExportSettings(report), "xml");
                    return;
                }

                if (string.IsNullOrEmpty(fileName))
                    fileName = base.GetFileName(report, sendEMail);

                if (fileName != null)
                {
                    StiFileUtils.ProcessReadOnly(fileName);
                    var stream = new FileStream(fileName, FileMode.Create);

                    StartProgress(guiMode);

                    var settings = new StiDataExportSettings();

                    base.StartExport(report, stream, settings, sendEMail, (bool)form["OpenAfterExport"], fileName, guiMode);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating a number of files in exported document as a result of export
        /// of one page of the rendered report.
        /// </summary>        
        public override bool MultipleFiles
		{
			get
			{
				return false;
			}
		}

        /// <summary>
        /// Returns a filter for xml files.
        /// </summary>
        /// <returns>Returns a filter for xml files.</returns>
		public override string GetFilter()
		{
			return StiLocalization.Get("FileFilters", "XmlFiles");
		}	

		#endregion

		#region Methods
        /// <summary>
        /// Exports a rendered report to the XML file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="fileName">A name of the file for exporting a rendered report.</param>
        public void ExportXml(StiReport report, string fileName)
		{
			StiFileUtils.ProcessReadOnly(fileName);
			FileStream stream = new FileStream(fileName, FileMode.Create);
			ExportXml(report, stream);
			stream.Flush();
			stream.Close();
		}

        /// <summary>
        /// Exports a rendered report to the XML file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream for export of a document.</param>
        public void ExportXml(StiReport report, Stream stream)
		{
            StiDataExportSettings settings = new StiDataExportSettings();
            ExportXml(report, stream, settings);
        }
    
		/// <summary>
        /// Exports a rendered report to the XML file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream for export of a document.</param>
        /// <param name="settings">A export settings.</param>
        public void ExportXml(StiReport report, Stream stream, StiDataExportSettings settings)
		{
			StiLogService.Write(this.GetType(), "Export report to Xml format");

            #region Read settings
            if (settings == null)
                throw new ArgumentNullException("The 'settings' argument cannot be equal in null.");

            //StiPagesRange pageRange = settings.PageRange;
            StiDataExportMode mode = settings.DataExportMode;
            #endregion

			string separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            CurrentPassNumber = 0;
            MaximumPassNumber = 3;

			StiPagesCollection pages = StiPagesRange.All.GetSelectedPages(report.RenderedPages);
			StiMatrix matrix = new StiMatrix(pages, false, this);
			matrix.ScanComponentsPlacement(false);
			if (IsStopped) return;

			matrix.PrepareDocument(this, mode);

			#region Check fields names
			Hashtable hs = new Hashtable();
			for (int index = 0; index < matrix.Fields.Length; index ++)
			{
				string st = matrix.Fields[index].Name;
                if (StiOptions.Export.Xml.ConvertTagsToUpperCase) st = st.ToUpper(CultureInfo.InvariantCulture);
				string stNum = string.Empty;
				int num = 0;
				while (true)
				{
					if (!hs.Contains(st + stNum)) break;
					num ++;
					stNum = num.ToString();
				}
				st += stNum;
				matrix.Fields[index].Name = st;
				hs.Add(st, st);
			}
			#endregion

			string title = report.ReportAlias;
		    if (string.IsNullOrWhiteSpace(title)) title = "Report";
			DataSet dataSet = new DataSet(title);
			dataSet.EnforceConstraints = false;
			DataTable dataTable = new DataTable();
            if (!string.IsNullOrWhiteSpace(settings.TableName)) dataTable.TableName = settings.TableName;
            for (int columnIndex = 0; columnIndex < matrix.Fields.Length; columnIndex ++)
			{
				switch (matrix.Fields[columnIndex].Info[0])
				{
					case (int)StiExportDataType.String:
						dataTable.Columns.Add(matrix.Fields[columnIndex].Name, typeof(string));
						break;
					case (int)StiExportDataType.Int:
						dataTable.Columns.Add(matrix.Fields[columnIndex].Name, typeof(int));
						break;
					case (int)StiExportDataType.Long:
						dataTable.Columns.Add(matrix.Fields[columnIndex].Name, typeof(long));
						break;
					case (int)StiExportDataType.Float:
						dataTable.Columns.Add(matrix.Fields[columnIndex].Name, typeof(float));
						break;
					case (int)StiExportDataType.Double:
						dataTable.Columns.Add(matrix.Fields[columnIndex].Name, typeof(double));
						break;
					case (int)StiExportDataType.Date:
						dataTable.Columns.Add(matrix.Fields[columnIndex].Name, typeof(DateTime));
						break;
				}
			}
			dataSet.Tables.Add(dataTable);

			#region Write records
			StatusString = StiLocalization.Get("Export", "ExportingCreatingDocument");
            CurrentPassNumber = 2;

            for (int rowIndex = 0; rowIndex < matrix.DataArrayLength; rowIndex++)
			{
				InvokeExporting(rowIndex, matrix.DataArrayLength, CurrentPassNumber, MaximumPassNumber);
				if (IsStopped) return;

				DataRow row = dataTable.NewRow();
				for (int columnIndex = 0; columnIndex < matrix.Fields.Length; columnIndex ++)
				{
					string text = matrix.Fields[columnIndex].DataArray[rowIndex];

					#region convert text
					switch (matrix.Fields[columnIndex].Info[0])
					{
						case (int)StiExportDataType.String:
						{
							row[columnIndex] = text;
						}
							break;

						case (int)StiExportDataType.Int:
						{
							int val = 0;
						    if (int.TryParse(text, out val))
						    {
                                row[columnIndex] = val;
						    }
						    else
						    {
                                val = 0;
						    }
						}
                        break;

						case (int)StiExportDataType.Long:
						{
							long val = 0;
                            if (long.TryParse(text, out val))
						    {
                                row[columnIndex] = val;
						    }
						    else
						    {
                                val = 0;
						    }
						}
                        break;

						case (int)StiExportDataType.Float:
						{
							float val = 0;
						    if (float.TryParse(text.Replace(".", ",").Replace(",", separator), out val))
						    {
						        row[columnIndex] = val;
						    }
						    else
						    {
						        val = 0;
						    }
						}
                        break;

						case (int)StiExportDataType.Double:
						{
							double val = 0;
						    if (double.TryParse(text.Replace(".", ",").Replace(",", separator), out val))
						    {
						        row[columnIndex] = val;
						    }
						    else
						    {
						        val = 0;
						    }
						}
                        break;

						case (int)StiExportDataType.Date:
						{
							DateTime val;
                            if (DateTime.TryParse(text, out val))
						    {
                                row[columnIndex] = val;
						    }
                            else
                            {
                                val = DateTime.MinValue;
                            }
						}
                        break;
					}
					#endregion

				}
				dataTable.Rows.Add(row);
			}
			#endregion

            XmlTextWriter xmlWriter = new XmlTextWriter(stream, Encoding.UTF8);
            xmlWriter.Indentation = 1;
            xmlWriter.Formatting = Formatting.Indented;
            if (StiOptions.Export.Xml.WriteXmlDocumentDeclaration)
            {
                xmlWriter.WriteStartDocument(true);
            }

			dataSet.WriteXml(xmlWriter);
            dataSet.Dispose();

            xmlWriter.Flush();

			if (matrix != null)
			{
				matrix.Clear();
				matrix = null;
			}

//			report.SaveDocument(stream);
		}
		#endregion
	}
}
