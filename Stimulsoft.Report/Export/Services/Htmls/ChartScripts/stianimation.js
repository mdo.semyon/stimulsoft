animateSti = function (chartId) {
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                                window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
    var animations = [];
    var chart_ = document.getElementById(chartId);
    var isMap = chart_ ? chart_.isMap : false;
    var createTooltip = function () {
        var table = document.createElement("table");
        table.style.position = "absolute";
        table.style.opacity = "0";
        table.style.background = "white";
        table.style.padding = "5px";
        table.style.border = "1px solid #bebebe";
        table.style.fontFamily = "Arial";
        table.style.fontSize = "12px";
        table.style.color = "#111111";
        document.body.appendChild(table);
        document._stiTooltip = table;
        var tr = document.createElement("tr");
        table.appendChild(tr);
        var td = document.createElement("td");
        td.style.verticalAlign = "top";
        td.rowSpan = 2;
        table._round = document.createElement('div');
        table._round.style.width = "20px";
        table._round.style.height = "20px";
        table._round.style.borderRadius = "20px";
        td.appendChild(table._round);
        tr.appendChild(td);
        table._text1 = document.createElement("td");
        table._text1.style.paddingTop = "3px";
        tr.appendChild(table._text1);
        tr = document.createElement("tr");
        table.appendChild(tr);
        table._text2 = document.createElement("td");
        tr.appendChild(table._text2);
        setInterval(function () {
            var t = document._stiTooltip;
            var op = parseFloat(t.style.opacity);
            if ((t.cx > 0 && op < 1) || (t.cx < 0 && op > 0)) {
                op += t.cx;
                op = Math.min(1, Math.max(0, op));
                t.style.opacity = op;
            }
        }, 50);
    }

    var lightenDarkenColor = function (col, amt) {
        var usePound = false;
        if (col[0] == "#") {
            col = col.slice(1);
            usePound = true;
        }
        var num = parseInt(col, 16);
        var r = (num >> 16) + amt;
        if (r > 255) r = 255;
        else if (r < 0) r = 0;
        var b = ((num >> 8) & 0x00FF) + amt;
        if (b > 255) b = 255;
        else if (b < 0) b = 0;
        var g = (num & 0x0000FF) + amt;
        if (g > 255) g = 255;
        else if (g < 0) g = 0;
        return (usePound ? "#" : "") + String("000000" + (g | (b << 8) | (r << 16)).toString(16)).slice(-6);
    }


    var addTooltip = function (el) {
        var t = document._stiTooltip;
        el.onmouseover = function (event) {
            t.cx = 0.1;
            t.style.left = event.pageX + 1 + "px";
            t.style.top = event.pageY + 1 + "px";
            t._text1.innerHTML = event.target.getAttribute("_text1");
            t._text2.innerHTML = event.target.getAttribute("_text2");
            t._round.style.background = event.target.getAttribute("_color");
            event.target.style.fill = lightenDarkenColor(event.target.getAttribute("_color"), -35);
        }
        el.onmouseout = function (event) {
            t.cx = -0.2;
            event.target.style.fill = event.target.getAttribute("_color");
        }
    }


    var inspect = function (element, animations) {

        if (!element) return;
        
        for (var i in element.childNodes) {
            var el = element.childNodes[i];
            if (el.attributes) {
                for (var j in el.attributes) {
                    if (el.attributes[j] && el.attributes[j].name && el.attributes[j].name.indexOf('_animation') >= 0) {
                        if (!el._animations) {
                            el._animations = [];
                            el._animations.push(JSON.parse(el.getAttribute(el.attributes[j].name)));
                            animations.push(el);
                        } else {
                            el._animations.push(JSON.parse(el.getAttribute(el.attributes[j].name)));
                        }
                        if (el.getAttribute("_ismap")) {
                            if (!document._stiTooltip) {
                                createTooltip();
                            }
                            addTooltip(el);
                        }
                    }
                }
            }
            inspect(el, animations);
        }
    }
    /*var chartDiv = document.getElementById("chart");
    var tempDiv = document.createElement("div");
    tempDiv.innerHTML = data;
    var chart = tempDiv.firstChild;
    chart.id = "chartSvg";
    if (chartDiv.childNodes.length > 1) {
        var destSvg = chartDiv.childNodes[1];
        var deleteNodes = [];
        for (var i in destSvg.childNodes) {
            deleteNodes.push(destSvg.childNodes[i]);
        }
        for (var i in chart.childNodes) {
            if (chart.childNodes[i].nodeType != 3 && chart.childNodes[i].nodeType) {
                destSvg.appendChild(chart.childNodes[i]);
            }
        }
        for (var i in deleteNodes) {
            if (deleteNodes[i].nodeType != 3 && deleteNodes[i].nodeType) {
                destSvg.removeChild(deleteNodes[i]);
            }
        }
    } else {
        chartDiv.appendChild(chart);
    }
    animateSti("chartSvg");*///TODO replace chart data

    inspect(chart_, animations);
    var easeInOutQuad = function (t) { return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t };
    var setScale = function (element, scaleX, scaleY, invertX, invertY) {
        var cx = !invertX ? element.bbox.x : element.bbox.x + element.bbox.width;
        var cy = !invertY ? element.bbox.y : element.bbox.y + element.bbox.height;
        var saclestr = scaleX + ',' + scaleY;
        var tx = -cx * (scaleX - 1);
        var ty = -cy * (scaleY - 1);
        var translatestr = tx + ',' + ty;
        element.setAttribute('transform', 'translate(' + translatestr + ') scale(' + saclestr + ')');
        element.setAttribute('opacity', '1');
    }
    var extractPoints = function (pointsStr) {
        var pointsA = pointsStr.split(" ");
        var result = [];
        for (var i = 0; i < pointsA.length - 1; i++) {
            var xy = pointsA[i].split(",");
            result.push({ x: parseFloat(xy[0]), y: parseFloat(xy[1]) });
        }
        return result;
    }


    var convertArcToCubicBezier = function (rect, startAngle1, sweepAngle1) {
        var centerX = rect.x + rect.width / 2;
        var centerY = rect.y + rect.height / 2;
        var radius = Math.min(rect.width / 2, rect.height / 2);
        var startAngle = startAngle1 * Math.PI / 180;
        var sweepAngle = sweepAngle1 * Math.PI / 180;
        var endAngle = (startAngle1 + sweepAngle1) * Math.PI / 180;

        var x1 = centerX + radius * Math.cos(startAngle);
        var y1 = centerY + radius * Math.sin(startAngle);

        var x2 = centerX + radius * Math.cos(endAngle);
        var y2 = centerY + radius * Math.sin(endAngle);

        var l = radius * 4 / 3 * Math.tan(0.25 * sweepAngle);
        var aL = Math.atan(l / radius);
        var radL = radius / Math.cos(aL);

        aL += startAngle;
        var ax1 = centerX + radL * Math.cos(aL);
        var ay1 = centerY + radL * Math.sin(aL);

        aL = Math.atan(-l / radius);
        aL += endAngle;
        var ax2 = centerX + radL * Math.cos(aL);
        var ay2 = centerY + radL * Math.sin(aL);
        return [{ x: x1, y: y1 }, { x: ax1, y: ay1 }, { x: ax2, y: ay2 }, { x: x2, y: y2 }];
    }

    var Round = function (value) {
        var value1 = parseInt(value);
        var rest = value - value1;
        return (rest > 0) ? value1 + 1 : value1;
    }

    var animatePie = function (dataStr, percent) {
        var data = JSON.parse(atob(dataStr));
        var result = "";
        var centerX = data.x + data.dx + data.width / 2;
        var centerY = data.y + data.dy + data.height / 2;
        var radius = data.width / 2;
        var startAngleFrom = data.startAngleFrom + (data.startAngle - data.startAngleFrom) * percent;
        var sweepAngleFrom = data.sweepAngleFrom + (data.sweepAngle - data.sweepAngleFrom) * percent;
        var startAngle = startAngleFrom * Math.PI / 180;

        var x1 = centerX + radius * Math.cos(startAngle);
        var y1 = centerY + radius * Math.sin(startAngle);

        result += "M" + centerX + "," + centerY;
        result += "L" + x1 + "," + y1;

        var step = Round(Math.abs(sweepAngleFrom / 90));
        var stepAngle = sweepAngleFrom / step;
        startAngle = startAngleFrom;

        for (var indexStep = 0; indexStep < step; indexStep++) {
            var points = convertArcToCubicBezier(data, startAngle, stepAngle);

            for (var index = 1; index < points.length - 1; index += 3) {
                if (index == 1)
                    result += "C" + (points[index].x + data.dx) + "," + (points[index].y + data.dy) + "," +
                        (points[index + 1].x + data.dx) + "," + (points[index + 1].y + data.dy) + "," +
                        (points[index + 2].x + data.dx) + "," + (points[index + 2].y + data.dy);
                else
                    result += "," + (points[index].x + data.dx) + "," + (points[index].y + data.dy) + "," +
                        (points[index + 1].x + data.dx) + "," + (points[index + 1].y + data.dy) + "," +
                        (points[index + 2].x + data.dx) + "," + (points[index + 2].y + data.dy);
            }
            startAngle += stepAngle;
        }
        result += "L" + centerX + "," + centerY;
        return result;
    }

    var animatePath = function (data, percent) {
        var result = "";
        while (data.length > 0) {
            result += data[0];
            var endIndex = data.substring(1).search(/[MLC]/) + 1;
            var els = data.substring(1, endIndex > 0 ? endIndex : data.length).split(/[, ]/);
            for (var i = 0; i < els.length; i++) {
                if (els[i] != "") {
                    var se = els[i].split(":");
                    result += (parseFloat(se[0]) + (parseFloat(se[1]) - parseFloat(se[0])) * percent);
                    if (i != els.length - 1) {
                        result += ",";
                    }
                }
            }
            data = endIndex > 0 ? data.substring(endIndex) : "";
        }
        return result;
    }


    var begin = new Date().getTime();
    var step = function (timestamp) {
        var finished = true;
        var now = new Date().getTime() - begin;
        for (var i in animations) {
            var an = animations[i];
            for (var k in an._animations) {
                var anim = an._animations[k];
                if (anim.begin <= now && anim.begin + anim.duration >= now) {
                    var percent = easeInOutQuad((now - anim.begin) / anim.duration);
                    for (var j in anim.actions) {
                        var ac = anim.actions[j];
                        var prefix = ac.length == 5 ? ac[4] : "";
                        if (ac[0] == "scaleCenter") {
                            an.bbox = an.getBBox();
                            setScale(an, (ac[1] + (ac[2] - ac[1]) * percent), (ac[3] + (ac[4] - ac[3]) * percent), ac[5], ac[6]);
                        } else if (ac[0] == "points") {
                            var pointsFrom = extractPoints(ac[1]);
                            var pointsTo = extractPoints(ac[2]);
                            var points = "";
                            for (var l in pointsFrom) {
                                points += (pointsFrom[l].x + (pointsTo[l].x - pointsFrom[l].x) * percent) + "," +
                                          (pointsFrom[l].y + (pointsTo[l].y - pointsFrom[l].y) * percent) + " ";
                            }
                            an.setAttribute(ac[0], points);
                        } else if (ac[0] == "value") {
                            var value = ac[1] + (ac[2] - ac[1]) * percent;
                            value = Math.round(value * Math.pow(10, ac[3])) / Math.pow(10, ac[3]);
                            an.textContent = value;
                        } else if (ac[0] == "translate") {
                            var from = ac[1].split(":");
                            var to = ac[2].split(":");
                            an.setAttribute("transform", "translate(" + (parseFloat(from[0]) + (parseFloat(to[0]) - parseFloat(from[0])) * percent) + " " +
                                                         (parseFloat(from[1]) + (parseFloat(to[1]) - parseFloat(from[1])) * percent) + ")" + ac[3]);
                        } else if (ac[0] == "path") {
                            an.setAttribute("d", animatePath(ac[1], percent));
                        } else if (ac[0] == "pie") {
                            an.setAttribute("d", animatePie(ac[1], percent));
                        } else {
                            an.setAttribute(ac[0], prefix + (ac[1] + (ac[2] - ac[1]) * percent) + ac[3]);
                        }
                    }
                    finished = false;
                } else if (anim.begin + anim.duration < now) {
                    for (var j in anim.actions) {
                        var ac = anim.actions[j];
                        var prefix = ac.length == 5 ? ac[4] : "";
                        if (ac[0] == "scaleCenter") {
                            setScale(an, ac[2], ac[4], ac[5], ac[6]);
                        } if (ac[0] == "points") {
                            var pointsTo = extractPoints(ac[2]);
                            var points = "";
                            for (var l in pointsTo) {
                                points += pointsTo[l].x + "," + pointsTo[l].y + " ";
                            }
                            an.setAttribute(ac[0], points);
                        } else if (ac[0] == "value") {
                            an.textContent = ac[2];
                        } else if (ac[0] == "translate") {
                            var to = ac[2].split(":");
                            an.setAttribute("transform", "translate(" + to[0] + " " + to[1] + ")" + ac[3]);
                        } else if (ac[0] == "path") {
                            an.setAttribute("d", animatePath(ac[1], 1));
                        } else if (ac[0] == "pie") {
                            an.setAttribute("d", animatePie(ac[1], 1));
                        } else {
                            an.setAttribute(ac[0], prefix + ac[2] + ac[3]);
                        }
                    }
                } else if (anim.begin > now) {
                    finished = false;
                }
            }
        }
        if (!finished) {
            requestAnimationFrame(step);
        }
    }
    requestAnimationFrame(step);
}