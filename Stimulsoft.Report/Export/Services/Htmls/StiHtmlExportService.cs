#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Xml;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Collections;
using System.Text;
using System.Reflection;
using System.Security.Cryptography;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.Units;
using Stimulsoft.Report.Components;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Zip;
using Stimulsoft.Base.Json;
using Stimulsoft.Base.Licenses;
using Stimulsoft.Report.Gauge;
using Stimulsoft.Report.WCFService;
using Stimulsoft.Report.Chart;
using Stimulsoft.Report.Maps;
using System.Threading;
using Stimulsoft.Report.Engine;
using Stimulsoft.Base.Cloud;

namespace Stimulsoft.Report.Export
{

    /// <summary>
    /// A class for the HTML export.
    /// </summary>
    [StiServiceBitmap(typeof(StiExportService), "Stimulsoft.Report.Images.Exports.Html.png")]
    public class StiHtmlExportService : StiExportService
    {
        #region StiExportService override
        /// <summary>
		/// Gets or sets a default extension of export. 
		/// </summary>
		public override string DefaultExtension
        {
            get
            {
                if (compressToArchive)
                    return "zip";

                if (exportSettings is StiMhtExportSettings) return "mht";
                if (exportSettings is StiHtmlExportSettings && (exportSettings as StiHtmlExportSettings).HtmlType == StiHtmlType.Mht) return "mht";

                return "html";
            }
        }

        public override StiExportFormat ExportFormat
        {
            get
            {
                if (exportSettings is StiHtml5ExportSettings) return StiExportFormat.Html5;
                if (exportSettings is StiMhtExportSettings) return StiExportFormat.Mht;
                if (exportSettings is StiHtmlExportSettings)
                {
                    StiHtmlType htmlType = (exportSettings as StiHtmlExportSettings).HtmlType;
                    if (htmlType == StiHtmlType.Html5) return StiExportFormat.Html5;
                    if (htmlType == StiHtmlType.Mht) return StiExportFormat.Mht;
                }
                return StiExportFormat.Html;
            }
        }

        /// <summary>
        /// Gets a group of the export in the context menu.
        /// </summary>
        public override string GroupCategory
        {
            get
            {
                return "Web";
            }
        }

        /// <summary>
        /// Gets a position of the export in the context menu.
        /// </summary>
        public override int Position
        {
            get
            {
                return (int)StiExportPosition.Html;
            }
        }

        /// <summary>
        /// Gets a export name in the context menu.
        /// </summary>
		public override string ExportNameInMenu
        {
            get
            {
                return StiLocalization.Get("Export", "ExportTypeHtmlFile");
            }
        }

        /// <summary>
        /// Exports a document to the stream without dialog of the saving file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream in which report will be exported.</param>
        /// <param name="settings">A settings for the report exporting.</param>
        public override void ExportTo(StiReport report, Stream stream, StiExportSettings settings)
        {
            exportSettings = settings as StiHtmlExportSettings;

            var htmlSettings = settings as StiHtmlExportSettings;
            var htmlType = htmlSettings != null ? htmlSettings.HtmlType : StiHtmlType.Html;

            if (htmlType == StiHtmlType.Html5 || htmlSettings is StiHtml5ExportSettings)
            {
                var html5ExportService = new StiHtml5ExportService();
                html5ExportService.ExportHtml(report, stream, htmlSettings as StiHtml5ExportSettings);
            }
            else if (htmlType == StiHtmlType.Mht || htmlSettings is StiMhtExportSettings)
            {
                var mhtExportService = new StiMhtExportService();
                mhtExportService.ExportMht(report, stream, htmlSettings as StiMhtExportSettings);
            }
            else
                ExportHtml(report, stream, htmlSettings);

        }

        /// <summary>
        /// Exports a rendered report to the HTML file.
        /// Also rendered report can be sent via e-mail.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="fileName">A name of the file for exporting a rendered report.</param>
        /// <param name="sendEMail">A parameter indicating whether the exported report will be sent via e-mail.</param>
        public override void Export(StiReport report, string fileName, bool sendEMail, StiGuiMode guiMode)
        {
            using (var form = StiGuiOptions.GetExportFormRunner("StiHtmlSetupForm", guiMode, this.OwnerWindow))
            {
                form["CurrentPage"] = report.CurrentPrintPage;
                form["OpenAfterExportEnabled"] = !sendEMail;

                this.reportTmp = report;
                this.documentFileName = fileName;
                this.sendEMail = sendEMail;
                this.guiMode = guiMode;
                form.Complete += form_Complete;
                form.ShowDialog();
            }
        }

        private StiHtmlExportSettings exportSettings;
        private StiReport reportTmp;
        private string documentFileName;
        private bool sendEMail;
        private StiGuiMode guiMode;
        private void form_Complete(IStiFormRunner form, StiShowDialogCompleteEvetArgs e)
        {
            if (e.DialogResult)
            {
                var exportFormat = (StiExportFormat)form["ExportFormat"];

                #region Export Html5

                if (exportFormat == StiExportFormat.Html5)
                {
                    var html5ExportService = new StiHtml5ExportService();
                    html5ExportService.compressToArchive = (bool)form["CompressToArchive"];

                    if (string.IsNullOrEmpty(documentFileName))
                        documentFileName = html5ExportService.GetFileName(reportTmp, sendEMail);

                    if (documentFileName != null)
                    {
                        if (StiOptions.WCFService.WCFExportDocumentEventIsUsed)
                        {
                            StiOptions.WCFService.InvokeWCFExportDocument(report, StiExportSettingsHelper.GetHtml5ExportSettings(form, report), "html");
                            return;
                        }

                        StiFileUtils.ProcessReadOnly(documentFileName);
                        var stream = new FileStream(documentFileName, FileMode.Create);

                        html5ExportService.StartProgress(guiMode);

                        var settings = new StiHtml5ExportSettings();
                        settings.PageRange = form["PagesRange"] as StiPagesRange;
                        settings.ImageFormat = (ImageFormat)form["ImageFormat"];
                        settings.ImageResolution = (float)form["Resolution"];
                        settings.ImageQuality = (float)form["ImageQuality"];
                        settings.ContinuousPages = (bool)form["ContinuousPages"];
                        settings.CompressToArchive = (bool)form["CompressToArchive"];

                        exportSettings = settings;

                        html5ExportService.StartExport(reportTmp, stream, settings, sendEMail, (bool)form["OpenAfterExport"], documentFileName, guiMode);
                    }
                }

                #endregion

                #region Mht

                if (exportFormat == StiExportFormat.Mht)
                {
                    var mhtExportService = new StiMhtExportService();
                    mhtExportService.compressToArchive = (bool)form["CompressToArchive"];

                    if (string.IsNullOrEmpty(documentFileName))
                        documentFileName = mhtExportService.GetFileName(reportTmp, sendEMail);

                    if (documentFileName != null)
                    {
                        if (StiOptions.WCFService.WCFExportDocumentEventIsUsed)
                        {
                            StiOptions.WCFService.InvokeWCFExportDocument(report, StiExportSettingsHelper.GetMhtExportSettings(form, report), "mht");
                            return;
                        }

                        StiFileUtils.ProcessReadOnly(documentFileName);
                        var stream = new FileStream(documentFileName, FileMode.Create);

                        mhtExportService.StartProgress(guiMode);

                        var settings = new StiMhtExportSettings
                        {
                            PageRange = form["PagesRange"] as StiPagesRange,
                            Zoom = (float)form["Zoom"],
                            ImageFormat = (ImageFormat)form["ImageFormat"],
                            ExportMode = (StiHtmlExportMode)form["ExportMode"],
                            ExportQuality = StiHtmlExportQuality.High,
                            AddPageBreaks = (bool)form["AddPageBreaks"],
                            CompressToArchive = (bool)form["CompressToArchive"],
                            Encoding = Encoding.UTF8
                        };

                        mhtExportService.fileName = documentFileName;

                        exportSettings = settings;

                        mhtExportService.StartExport(reportTmp, stream, settings, sendEMail, (bool)form["OpenAfterExport"], documentFileName, guiMode);
                    }
                }

                #endregion

                #region Html

                if (exportFormat == StiExportFormat.Html)
                {
                    compressToArchive = (bool)form["CompressToArchive"];

                    if (string.IsNullOrEmpty(documentFileName))
                        documentFileName = base.GetFileName(reportTmp, sendEMail);

                    if (documentFileName != null)
                    {
                        if (StiOptions.WCFService.WCFExportDocumentEventIsUsed)
                        {
                            StiOptions.WCFService.InvokeWCFExportDocument(report, StiExportSettingsHelper.GetHtmlExportSettings(form, report), "html");
                            return;
                        }

                        StiFileUtils.ProcessReadOnly(documentFileName);
                        var stream = new FileStream(documentFileName, FileMode.Create);

                        StartProgress(guiMode);

                        var settings = new StiHtmlExportSettings();
                        settings.PageRange = form["PagesRange"] as StiPagesRange;
                        settings.Zoom = (float)form["Zoom"];
                        settings.ImageFormat = (ImageFormat)form["ImageFormat"];
                        settings.ExportMode = (StiHtmlExportMode)form["ExportMode"];
                        settings.ExportQuality = StiHtmlExportQuality.High;
                        settings.AddPageBreaks = (bool)form["AddPageBreaks"];
                        settings.CompressToArchive = (bool)form["CompressToArchive"];
                        settings.UseEmbeddedImages = (bool)form["UseEmbeddedImages"];

                        exportSettings = settings;

                        base.StartExport(reportTmp, stream, settings, sendEMail, (bool)form["OpenAfterExport"],
                            documentFileName, guiMode);
                    }
                }

                #endregion
            }
        }

        /// <summary>
        /// Gets a value indicating a number of files in exported document as a result of export
        /// of one page of the rendered report.
        /// </summary>
        public override bool MultipleFiles
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Returns a filter for Html files.
        /// </summary>
        /// <returns>Returns a filter for Html files.</returns>
        public override string GetFilter()
        {
            if (compressToArchive)
                return StiLocalization.Get("FileFilters", "ZipArchives");
            return StiLocalization.Get("FileFilters", "HtmlFiles");
        }
        #endregion

        #region Properties
        private bool clearOnFinish = true;
        public bool ClearOnFinish
        {
            get
            {
                return clearOnFinish;
            }
            set
            {
                clearOnFinish = value;
            }
        }
        #endregion

        #region Fields
        public StiHtmlTableRender TableRender = null;
        public StiHtmlTextWriter HtmlWriter = null;
        internal StiZipWriter20 zip = null;
        internal StiReport report;
        internal string fileName = string.Empty;
        private double startPage = 0;
        internal int imageNumber = 1;
        internal double zoom = 0.75;
        internal ImageFormat imageFormat;
        private NumberFormatInfo numberFormat = new NumberFormatInfo();
        internal StiHtmlExportQuality exportQuality;
        internal bool useStylesTable = true;
        internal bool isFileStreamMode = true;
        internal float imageQuality = 0.75f;
        internal float imageResolution = 96;
        internal bool compressToArchive = false;
        internal bool useEmbeddedImages = false;
        internal string openLinksTarget = null;
        internal StiHtmlChartType chartType = StiHtmlChartType.Image;
        internal StiHtmlExportMode exportMode = StiHtmlExportMode.Table;

        private SortedList coordX;
        private SortedList coordY;
        private string strSpanDiv = "span";
        private Hashtable hyperlinksToTag = null;

        internal Hashtable chartData = new Hashtable();

        private CultureInfo currentCulture = null;
        private CultureInfo storedCulture = null;

        internal Hashtable hashBookmarkGuid = null;
        #endregion

        #region Properties
        private bool renderStyles = true;
        public bool RenderStyles
        {
            get
            {
                return renderStyles;
            }
            set
            {
                renderStyles = value;
            }
        }

        private ArrayList styles = null;
        public ArrayList Styles
        {
            get
            {
                return styles;
            }
            set
            {
                styles = value;
            }
        }

        private bool renderWebViewer = false;
        /* Use export service for HTML viewer */
        public bool RenderWebViewer
        {
            get
            {
                return renderWebViewer;
            }
            set
            {
                renderWebViewer = value;
            }
        }

        private bool renderWebInteractions = false;
        /* Render interaction parameters for HTML viewer */
        public bool RenderWebInteractions
        {
            get
            {
                return renderWebInteractions;
            }
            set
            {
                renderWebInteractions = value;
            }
        }

        private StiHtmlImageHost htmlImageHost;
        /// <summary>
        /// Internal use only.
        /// </summary>
		public StiHtmlImageHost HtmlImageHost
        {
            get
            {
                return htmlImageHost;
            }
            set
            {
                htmlImageHost = value;
            }
        }


        private double totalPageWidth = 0;
        /// <summary>
        /// Internal use only.
        /// </summary>
		public double TotalPageWidth
        {
            get
            {
                return totalPageWidth;
            }
            set
            {
                totalPageWidth = value;
            }
        }


        private double totalPageHeight = 0;
        /// <summary>
        /// Internal use only.
        /// </summary>
		public double TotalPageHeight
        {
            get
            {
                return totalPageHeight;
            }
            set
            {
                totalPageHeight = value;
            }
        }


        private bool renderAsDocument = true;
        /// <summary>
        /// Internal use only.
        /// </summary>
		public bool RenderAsDocument
        {
            get
            {
                return renderAsDocument;
            }
            set
            {
                renderAsDocument = value;
            }
        }

        private bool removeEmptySpaceAtBottom = StiOptions.Export.Html.RemoveEmptySpaceAtBottom;
        internal bool RemoveEmptySpaceAtBottom
        {
            get
            {
                return removeEmptySpaceAtBottom;
            }
        }

        private StiHorAlignment pageHorAlignment = StiHorAlignment.Center;
        internal StiHorAlignment PageHorAlignment
        {
            get
            {
                return pageHorAlignment;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Internal use only.
        /// </summary>
        /// <param name="rect">A rectangle.</param>
        public void AddCoord(RectangleD rect)
        {
            AddCoord(rect.Left, rect.Top);
            AddCoord(rect.Right, rect.Bottom);
        }


        private void AddCoord(double x, double y)
        {
            coordX[x] = x;
            coordY[y] = y;
        }


        private void FormatCoords(StiReport report)
        {
            SortedList newCoordX = new SortedList();
            foreach (double key in coordX.Keys)
            {
                newCoordX[key] = (double)Math.Round((decimal)(report.Unit.ConvertToHInches((double)coordX[key]) * zoom * .75), 2);
            }
            coordX = newCoordX;

            SortedList newCoordY = new SortedList();
            foreach (double key in coordY.Keys)
            {
                newCoordY[key] = (double)Math.Round((decimal)(report.Unit.ConvertToHInches((double)coordY[key]) * zoom * .75), 2);
            }
            coordY = newCoordY;
        }


        private string FormatCoord(double value)
        {
            return (Math.Round((decimal)value, 2)).ToString(numberFormat) + "pt";
        }


        internal string FormatColor(Color color)
        {
            if (color.A < 255 && color.A > 0)
            {
                return string.Format("rgba({0},{1},{2},{3})",
                    color.R,
                    color.G,
                    color.B,
                    Math.Round(color.A/255f, 3));
            }
            return ColorTranslator.ToHtml(color);
        }


        private string GetBorderStyle(StiPenStyle style)
        {
            switch (style)
            {
                case StiPenStyle.Dot:
                    return " dotted";

                case StiPenStyle.Dash:
                case StiPenStyle.DashDot:
                case StiPenStyle.DashDotDot:
                    return " dashed";

                case StiPenStyle.Double:
                    return " double";

                default:
                    return " solid";
            }
        }

        internal void SetCurrentCulture()
        {
            storedCulture = Thread.CurrentThread.CurrentCulture;
            if (StiOptions.Export.Html.ForcedCultureForCharts != null)
            {
                Thread.CurrentThread.CurrentCulture = StiOptions.Export.Html.ForcedCultureForCharts;
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = currentCulture;
            }
        }

        internal void RestoreCulture()
        {
            Thread.CurrentThread.CurrentCulture = storedCulture;
        }

        #region Renders
        internal void RenderFont(StiHtmlTableCell cell, Font font)
        {
            string fontStr = "";
            if (font.Bold) fontStr += "bold ";
            if (font.Italic) fontStr += "italic ";
            fontStr += Math.Round((font.SizeInPoints * zoom), 0).ToString();
            var fontName = font.FontFamily.GetName(0);
            if (fontName.Contains(" ")) fontName = "'" + fontName + "'";
            fontStr += "pt " + fontName;

            string decoration = (font.Underline ? (font.Strikeout ? "underline line-through" : "underline") : (font.Strikeout ? "line-through" : null));

            if (cell == null)
            {
                HtmlWriter.WriteStyleAttribute("Font", fontStr);
                if (font.Underline || font.Strikeout) HtmlWriter.WriteStyleAttribute("text-decoration", decoration);
            }
            else
            {
                cell.Style["Font"] = fontStr;
                if (font.Underline || font.Strikeout) cell.Style["text-decoration"] = decoration;
            }
        }


        internal void RenderTextHorAlignment(StiHtmlTableCell cell, StiCellStyle style)
        {
            bool rightToLeft = (style.TextOptions != null) && (style.TextOptions.RightToLeft);
            StiTextHorAlignment textHorAlignment = style.HorAlignment;
            string align = string.Empty;
            if (textHorAlignment == StiTextHorAlignment.Left)
            {
                align = (!rightToLeft ? "left" : "right");
            }
            if (textHorAlignment == StiTextHorAlignment.Right)
            {
                align = (rightToLeft ? "left" : "right");
            }
            if (textHorAlignment == StiTextHorAlignment.Center) align = "center";
            if (textHorAlignment == StiTextHorAlignment.Width) align = "justify";

            if (align != string.Empty)
            {
                if (cell == null)
                {
                    HtmlWriter.WriteStyleAttribute("text-align", align);
                    if (exportMode != StiHtmlExportMode.Table)
                    {
                        if (align == "left") HtmlWriter.WriteStyleAttribute("justify-content", "flex-start");
                        if (align == "right") HtmlWriter.WriteStyleAttribute("justify-content", "flex-end");
                        if (align == "center") HtmlWriter.WriteStyleAttribute("justify-content", "center");
                        if (align == "justify") HtmlWriter.WriteStyleAttribute("justify-content", "space-between");
                    }
                }
                else
                {
                    cell.Style["text-align"] = align;
                }
            }
        }

        internal void RenderVertAlignment(StiHtmlTableCell cell, StiCellStyle style)
        {
            StiVertAlignment textVertAlignment = style.VertAlignment;
            bool textEllipsis = style.TextOptions != null && style.TextOptions.Trimming != StringTrimming.None;

            string align = null;
            if (textVertAlignment == StiVertAlignment.Top) align = "top";
            if (textVertAlignment == StiVertAlignment.Center) align = "middle";
            if (textVertAlignment == StiVertAlignment.Bottom) align = "bottom";
            if (!string.IsNullOrEmpty(align))
            {
                if (cell == null)
                {
                    if (exportMode == StiHtmlExportMode.Table)
                    {
                        HtmlWriter.WriteStyleAttribute("vertical-align", align);
                    }
                    else
                    {
                        if (textVertAlignment == StiVertAlignment.Top) align = "flex-start";
                        if (textVertAlignment == StiVertAlignment.Center) align = "center";
                        if (textVertAlignment == StiVertAlignment.Bottom) align = "flex-end";

                        if (textEllipsis)
                        {
                            HtmlWriter.WriteStyleAttribute("display", "block");
                            HtmlWriter.WriteStyleAttribute("white-space", "nowrap");
                        }
                        else
                        {
                            HtmlWriter.WriteStyleAttribute("display", "flex");
                        }
                        HtmlWriter.WriteStyleAttribute("align-items", align);
                    }
                }
                else
                {
                    cell.Style["vertical-align"] = align;
                }
            }
        }


        //		private void RenderTextHorAlignment(StiComponent comp)
        //		{
        //			if (comp is IStiTextHorAlignment)
        //			{
        //				IStiTextHorAlignment align = comp as IStiTextHorAlignment;
        //				RenderTextHorAlignment(align.HorAlignment);
        //			}
        //		}


        internal void RenderTextAngle(StiTextOptions textOptions)
        {
            if (textOptions != null &&
                (textOptions.Angle == 90f ||
                textOptions.Angle == 270f)) HtmlWriter.WriteStyleAttribute("writing-mode", "tb-rl");
        }


        internal void RenderTextDirection(StiHtmlTableCell cell, StiTextOptions textOptions)
        {
            if (textOptions != null &&
                (textOptions.RightToLeft))
            {
                if (cell == null)
                {
                    HtmlWriter.WriteStyleAttribute("direction", "rtl");
                }
                else
                {
                    cell.Style["direction"] = "rtl";
                }
            }
        }


        //private void RenderBackColor(StiComponent comp)
        //{
        //    if (comp is IStiBrush)
        //    {
        //        IStiBrush brush = comp as IStiBrush;
        //        Color color = StiBrush.ToColor(brush.Brush);
        //        RenderBackColor(null, color);
        //    }
        //}


        internal void RenderBackColor(StiHtmlTableCell cell, Color color)
        {
            if (color.A > 0)
            {
                if (cell == null)
                {
                    HtmlWriter.WriteStyleAttribute("background-color", FormatColor(color));
                }
                else
                {
                    cell.Style["background-color"] = FormatColor(color);
                }
            }
        }


        //private void RenderTextColor(StiComponent comp)
        //{
        //    if (comp is IStiTextBrush)
        //    {
        //        IStiTextBrush textBrush = comp as IStiTextBrush;
        //        Color color = StiBrush.ToColor(textBrush.TextBrush);
        //        RenderTextColor(null, color);
        //    }
        //}


        internal void RenderTextColor(StiHtmlTableCell cell, Color color, bool forceAnyColor = false)
        {
            if ((color != Color.Black) || forceAnyColor)
            {
                if (cell == null)
                {
                    HtmlWriter.WriteStyleAttribute("color", FormatColor(color));
                }
                else
                {
                    cell.Style["color"] = FormatColor(color);
                }
            }
        }


        private void RenderBorder(StiComponent comp)
        {
            if (comp is IStiBorder && (!(comp is IStiIgnoreBorderWhenExport)))
            {
                IStiBorder border = comp as IStiBorder;
                RenderBorder(border.Border);
            }
        }


        internal void RenderBorder(StiBorder border)
        {
            if (border != null)
            {
                StiBorderSide borderL = null;
                StiBorderSide borderR = null;
                StiBorderSide borderT = null;
                StiBorderSide borderB = null;
                StiAdvancedBorder advBorder = border as StiAdvancedBorder;
                if (advBorder != null)
                {
                    borderL = advBorder.LeftSide;
                    borderR = advBorder.RightSide;
                    borderT = advBorder.TopSide;
                    borderB = advBorder.BottomSide;
                }
                else
                {
                    borderL = new StiBorderSide(border.Color, border.Size, border.Style);
                    if (border.IsRightBorderSidePresent) borderR = borderL;
                    if (border.IsTopBorderSidePresent) borderT = borderL;
                    if (border.IsBottomBorderSidePresent) borderB = borderL;
                    if (!border.IsLeftBorderSidePresent) borderL = null;
                }
                RenderBorder(null, borderL, "left");
                RenderBorder(null, borderR, "right");
                RenderBorder(null, borderT, "top");
                RenderBorder(null, borderB, "bottom");
            }
        }


        internal void RenderBorder(StiHtmlTableCell cell, StiBorderSide border, string side)
        {
            if ((border != null) && (border.Style != StiPenStyle.None))
            {
                string color = FormatColor(border.Color);
                string style = GetBorderStyle(border.Style);
                double sizeD = border.Size;
                if (sizeD > 0 && sizeD < 1) sizeD = 1;
                if (sizeD < 1 &&
                    (border.Style == StiPenStyle.Dash ||
                    border.Style == StiPenStyle.DashDot ||
                    border.Style == StiPenStyle.DashDotDot ||
                    border.Style == StiPenStyle.Dot))
                {
                    sizeD = 1;
                }
                if (border.Style == StiPenStyle.Double)
                {
                    sizeD = 2.5;
                }
                string size = Math.Round(sizeD, MidpointRounding.AwayFromZero).ToString() + "px";

                if (cell == null)
                {
                    HtmlWriter.WriteStyleAttribute(string.Format("border-{0}-color", side), color);
                    HtmlWriter.WriteStyleAttribute(string.Format("border-{0}-style", side), style);
                    HtmlWriter.WriteStyleAttribute(string.Format("border-{0}-width", side), size);
                }
                else
                {
                    cell.Style[string.Format("border-{0}-color", side)] = color;
                    cell.Style[string.Format("border-{0}-style", side)] = style;
                    cell.Style[string.Format("border-{0}-width", side)] = size;
                }
            }
            else
            {
                if (StiOptions.Export.Html.UseStrictTableCellSize && (cell == null))
                {
                    HtmlWriter.WriteStyleAttribute(string.Format("border-{0}-color", side), "transparent");
                    HtmlWriter.WriteStyleAttribute(string.Format("border-{0}-style", side), "solid");
                    HtmlWriter.WriteStyleAttribute(string.Format("border-{0}-width", side), "1px");
                }
            }
        }


        private void RenderPosition(StiComponent comp)
        {
            RectangleD rect = comp.ComponentToPage(comp.ClientRectangle);

            StiUnit unit = comp.Page.Unit;

            string left = FormatCoord((double)coordX[rect.Left]);
            string top = FormatCoord((double)coordY[rect.Top] + startPage * zoom * .75);
            double widthD = (double)coordX[rect.Right] - (double)coordX[rect.Left];
            double heightD = (double)coordY[rect.Bottom] - (double)coordY[rect.Top];

            //correction for border width
            IStiBorder bord = comp as IStiBorder;
            if (bord != null && bord.Border != null)
            {
                if (bord.Border is StiAdvancedBorder)
                {

                }
                else
                {
                    if ((bord.Border.Style != StiPenStyle.None) && (bord.Border.Size > 0) && (bord.Border.Side != StiBorderSides.None))
                    {
                        if ((bord.Border.Side & StiBorderSides.Left) > 0) widthD -= bord.Border.Size * 0.375;
                        if ((bord.Border.Side & StiBorderSides.Right) > 0) widthD -= bord.Border.Size * 0.375;
                        if ((bord.Border.Side & StiBorderSides.Top) > 0) heightD -= bord.Border.Size * 0.375;
                        if ((bord.Border.Side & StiBorderSides.Bottom) > 0) heightD -= bord.Border.Size * 0.375;
                    }
                }
            }

            //correction for padding
            StiText text = comp as StiText;
            if (text != null && (!text.Margins.IsEmpty))
            {
                widthD -= ((int)text.Margins.Left + (int)text.Margins.Right) * 0.75;
                heightD -= ((int)text.Margins.Top + (int)text.Margins.Bottom) * 0.75;
            }

            if (widthD < 0) widthD = 0;
            if (heightD < 0) heightD = 0;

            string width = FormatCoord(widthD);
            string height = FormatCoord(heightD);

            HtmlWriter.WriteStyleAttribute("left", left);
            HtmlWriter.WriteStyleAttribute("top", top);
            HtmlWriter.WriteStyleAttribute("width", width);
            HtmlWriter.WriteStyleAttribute("height", height);

            if (text != null && (!text.Margins.IsEmpty))
            {
                HtmlWriter.WriteStyleAttribute("padding", string.Format("{0} {1} {2} {3}",
                   StiHtmlUnit.NewUnit(text.Margins.Top).ToString(),
                   StiHtmlUnit.NewUnit(text.Margins.Right).ToString(),
                   StiHtmlUnit.NewUnit(text.Margins.Bottom).ToString(),
                   StiHtmlUnit.NewUnit(text.Margins.Left).ToString()));
            }
            if (comp is StiShape)
            {
                HtmlWriter.WriteStyleAttribute("overflow", "visible");
            }
        }


        private void RenderImage(StiComponent comp)
        {
            string imageURL = null;
            if (comp is StiImage) imageURL = ((StiImage)comp).ImageURLValue as string;
            SetCurrentCulture();
            RenderImage(comp as IStiExportImage, imageURL);
            RestoreCulture();
        }


        private void RenderImage(IStiExportImage exportImage, string imageURL)
        {
            if (exportImage != null)
            {
                IStiExportImageExtended exportImageExtended = exportImage as IStiExportImageExtended;

                float zoom = (float)this.zoom;

                float resolution = imageResolution;
                var imageComp = exportImage as StiImage;
                if (StiOptions.Export.Html.UseImageResolution && imageComp != null && imageComp.ExistImageToDraw())
                {
                    using (var gdiImage = imageComp.TryTakeGdiImageToDraw())
                    {
                        var dpix = gdiImage.HorizontalResolution;
                        if (dpix >= 50 && dpix <= 1250) resolution = dpix;
                    }
                }
                if (resolution != 100) zoom *= resolution / 100f;

                Image image = null;
                bool isForceExportAsImage = ForceExportAsImage(exportImage);

                if (exportImageExtended != null || isForceExportAsImage)
                {
                    if (exportImageExtended.IsExportAsImage(StiExportFormat.Html) || isForceExportAsImage)
                    {
                        if (imageFormat == ImageFormat.Png)
                        {
                            image = exportImageExtended.GetImage(ref zoom, StiExportFormat.ImagePng);
                        }
                        else
                        {
                            if (strSpanDiv == "span")
                                image = exportImageExtended.GetImage(ref zoom, StiExportFormat.HtmlSpan);
                            else
                                image = exportImageExtended.GetImage(ref zoom, StiExportFormat.HtmlDiv);
                        }
                    }
                    else return;
                }
                else image = exportImage.GetImage(ref zoom);

                RestoreCulture();
                var shape = exportImage as StiShape;
                RenderImage(image, imageURL, zoom, shape != null ? -(int)Math.Round(shape.Size * zoom / 2) : 0);
                if (image != null) image.Dispose();
            }
        }


        private bool ForceExportAsImage(object exportImage)
        {
            IStiTextOptions textOptions = exportImage as IStiTextOptions;
            return textOptions != null && textOptions.TextOptions.Angle != 0;
        }


        private void RenderImage(Image image, string imageURL, float zoom, int margin)
        {
            if (image != null)
            {
                if (!string.IsNullOrEmpty(imageURL) && !(imageURL.StartsWith("http") || imageURL.StartsWith("ftp")))
                {
                    imageURL = null;
                }

                string imageString = imageURL;

                if (string.IsNullOrEmpty(imageURL))
                {
                    if (HtmlImageHost != null) imageString = HtmlImageHost.GetImageString(image as Bitmap);
                    if (imageString == null) imageString = string.Empty;
                }

                HtmlWriter.WriteBeginTag("img ");
                HtmlWriter.Write("style=\"width:" + ((int)(image.Width / zoom * this.zoom)).ToString() + "px;");
                if (margin != 0)
                {
                    HtmlWriter.Write("margin:" + margin + "px;");
                }
                HtmlWriter.Write("height:" + ((int)(image.Height / zoom * this.zoom)).ToString() + "px;border:0px\"");
                HtmlWriter.WriteAttribute("src", imageString);
                HtmlWriter.Write(">");
                HtmlWriter.WriteEndTag("img");
            }
        }


        private bool RenderHyperlink(StiComponent comp)
        {
            string hyperlinkValue = comp.HyperlinkValue as string;
            if (!string.IsNullOrWhiteSpace(hyperlinkValue) && hyperlinkValue.StartsWith("##")) hyperlinkValue = hyperlinkValue.Substring(1);

            string bookmarkValue = comp.BookmarkValue as string;
            string tag = comp.TagValue as string;
            if ((bookmarkValue == null) && !string.IsNullOrEmpty(tag) && hyperlinksToTag.ContainsKey(tag)) bookmarkValue = tag;

            string bookmarkGuid = null;
            if (!string.IsNullOrWhiteSpace(comp.Guid) && hashBookmarkGuid.ContainsKey(comp.Guid))
            {
                bookmarkGuid = comp.Guid;
            }

            if (!string.IsNullOrWhiteSpace(hyperlinkValue))
            {
                RectangleD rect = comp.ComponentToPage(comp.ClientRectangle);
                string height = FormatCoord((double)coordY[rect.Bottom] - (double)coordY[rect.Top]);
                StringBuilder style = new StringBuilder();
                style.Append("display:block;height:" + height + ";text-decoration:none;");
                if (comp is IStiTextBrush)
                {
                    IStiTextBrush textBrush = comp as IStiTextBrush;
                    Color color = StiBrush.ToColor(textBrush.TextBrush);
                    style.Append("color:" + FormatColor(color) + ";");
                }
                if (comp is IStiFont)
                {
                    IStiFont font = comp as IStiFont;
                    if (font.Font.Underline)
                    {
                        style.Append("text-decoration:underline;");
                    }
                    else
                    {
                        style.Append("text-decoration:none;");
                    }
                }


                HtmlWriter.WriteBeginTag("a");
                if (!string.IsNullOrWhiteSpace(bookmarkValue))
                {
                    HtmlWriter.WriteAttribute("name", bookmarkValue.Replace("'", ""));
                }
                if (!string.IsNullOrWhiteSpace(bookmarkGuid))
                {
                    HtmlWriter.WriteAttribute("guid", bookmarkGuid);
                }
                HtmlWriter.WriteAttribute("style", style.ToString());
                HtmlWriter.WriteAttribute("href", hyperlinkValue);
                HtmlWriter.Write(">");
                return true;
            }
            if (!string.IsNullOrWhiteSpace(bookmarkValue) || !string.IsNullOrWhiteSpace(bookmarkGuid))
            {
                HtmlWriter.WriteBeginTag("a");
                if (!string.IsNullOrWhiteSpace(bookmarkValue))
                {
                    HtmlWriter.WriteAttribute("name", bookmarkValue.Replace("'", ""));
                }
                if (!string.IsNullOrWhiteSpace(bookmarkGuid))
                {
                    HtmlWriter.WriteAttribute("guid", bookmarkGuid);
                }
                if (StiOptions.Export.Html.UseExtendedStyle)
                {
                    HtmlWriter.WriteAttribute("class", "sBaseStyleFix");
                }
                HtmlWriter.Write(">");
                return true;
            }
            return false;
        }


        private void RenderPage(StiPagesCollection pages, bool useBookmarksTree, int bookmarkWidth)
        {
            if (pageHorAlignment != StiHorAlignment.Left)
            {
                HtmlWriter.WriteBeginTag(strSpanDiv + " style=\"");
                HtmlWriter.WriteStyleAttribute("text-align", (pageHorAlignment == StiHorAlignment.Center ? "center" : "right"));
                if (useBookmarksTree)
                {
                    HtmlWriter.WriteStyleAttribute("margin-left", string.Format("{0}px", bookmarkWidth + 4));
                }
                HtmlWriter.Write("\">");
                HtmlWriter.Indent++;
                HtmlWriter.WriteLine();
            }

            HtmlWriter.WriteBeginTag(strSpanDiv + " class=\"StiPageContainer\" style=\"");
            if (RenderAsDocument)
            {
                if (pageHorAlignment != StiHorAlignment.Left)
                {
                    HtmlWriter.WriteStyleAttribute("display", "inline-block");
                }
                else
                {
                    if (useBookmarksTree) HtmlWriter.WriteStyleAttribute("left", string.Format("{0}px", bookmarkWidth + 4));
                }
                HtmlWriter.WriteStyleAttribute("width", FormatCoord(totalPageWidth * zoom * .75));
                HtmlWriter.WriteStyleAttribute("height", FormatCoord(totalPageHeight * zoom * .75));
                HtmlWriter.WriteStyleAttribute("position", "relative");

                Color backColor = Color.Transparent;
                if ((pages != null) && (pages.Count > 0))
                {
                    if (pages[0].Brush != null)
                    {
                        backColor = StiBrush.ToColor(pages[0].Brush);
                    }
                    if (backColor.A == 0) backColor = Color.White;
                    if (pages[0].Border != null)
                    {
                        RenderBorder(pages[0]);
                    }
                }
                HtmlWriter.WriteStyleAttribute("background-color", FormatColor(backColor));

                string imagePath = GetBackgroundImagePath(pages, zoom, exportSettings.UseWatermarkMargins);
                if (!string.IsNullOrWhiteSpace(imagePath))
                {
                    HtmlWriter.WriteStyleAttribute("background-image", string.Format("url('{0}')", imagePath));
                }
            }
            HtmlWriter.Write("\">");
            HtmlWriter.Indent++;
            HtmlWriter.WriteLine();
        }


        private void RenderEndPage()
        {
            HtmlWriter.Indent--;
            HtmlWriter.WriteEndTag(strSpanDiv);
            HtmlWriter.WriteLine();

            if (pageHorAlignment != StiHorAlignment.Left)
            {
                HtmlWriter.Indent--;
                HtmlWriter.WriteEndTag(strSpanDiv);
                HtmlWriter.WriteLine();
            }
        }


        private void RenderStartDoc(StiHtmlTableRender render, bool asTable, bool useBookmarks, bool exportBookmarksOnly, bool useStimulsoftFont, Hashtable cssStyles, StiPagesCollection pages, Encoding encoding)
        {
            HtmlWriter.WriteLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            HtmlWriter.Write("<html xmlns=\"http://www.w3.org/1999/xhtml\">");

            HtmlWriter.WriteLine();
            HtmlWriter.Indent++;

            HtmlWriter.WriteFullBeginTag("head");
            HtmlWriter.WriteLine();
            HtmlWriter.Indent++;

            if (htmlImageHost is StiMhtImageHost)
            {
                HtmlWriter.WriteBeginTag("meta");
                HtmlWriter.WriteAttribute("http-equiv", "X-UA-Compatible");
                HtmlWriter.WriteAttribute("content", "IE=11.0000");
                HtmlWriter.WriteEndTag("meta");
            }

            HtmlWriter.WriteFullBeginTag("title");
            HtmlWriter.Write(report.ReportAlias);
            HtmlWriter.WriteEndTag("title");
            HtmlWriter.WriteLine();

            HtmlWriter.WriteBeginTag("meta");
            HtmlWriter.WriteAttribute("http-equiv", "Content-Type");
            HtmlWriter.WriteAttribute("content", string.Format("text/html; charset={0}", encoding.WebName));
            HtmlWriter.WriteEndTag("meta");
            HtmlWriter.WriteLine();

            if (useStimulsoftFont)
            {
                HtmlWriter.Write(@"<style>@font-face {font-family: 'Stimulsoft';src: url(data:font/ttf;base64,AAEAAAALAIAAAwAwT1MvMg8SB94AAAC8AAAAYGNtYXB84HyCAAABHAAAAKxnYXNwAAAAEAAAAcgAAAAIZ2x5ZnsqufkAAAHQAAAcDGhlYWQPpKc5AAAd3AAAADZoaGVhB8UD/AAAHhQAAAAkaG10eOjAB1QAAB44AAAA9GxvY2G5XMAMAAAfLAAAAHxtYXhwAEoAgQAAH6gAAAAgbmFtZSWAbToAAB/IAAABqnBvc3QAAwAAAAAhdAAAACAAAwPyAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADqwgPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAkAAAACAAIAAEAAAAAQAg6SrpOulC6Vjpdumi6bDpyune6r7qwOrC//3//wAAAAAAIOkA6TrpQulY6Xbpoumv6crp3Oq+6sDqwv/9//8AAf/jFwQW9RbuFtkWvBaRFoUWbBZbFXwVexV6AAMAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAf//AA8AAQAAAAAAAAAAAAIAADc5AQAAAAABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAA/8AEAAPAAAYAAAERIQkBIREBIP7gAgACAP7gA8D+AP4AAgACAAAAAAABAAD/wAQAA8AABgAAEyERCQERIQACAAIA/gD+AAKgASD+AP4AASAAAQAA/8AEAAPAAAcAABMBByERBwkBAAGk7ANI7f5d/pACUP5d7QNI7AGk/pAAAAAAAQAA/8AEAAPAAAcAABMBJyERJwkBAAGk7ANI7f5d/pABMAGk7Py47P5cAXAAAAAAAQAA/8AEAAPAAAYAABMhESERIQEAASABwAEg/gABwP4AAgACAAAAAAEABwACBAIDcwAeAAA3JzcXHgEfAQE2Nz4BNzY3NhYfAQEGBw4BBwYxIiYnvrdXJA9UK2oBEzkyM0wWFwEDER0s/rxDOztZGhoBbUy/vlwlD1YubQFcSEBAYB0dAQUNHi7+cVJISWwgH29OAAEAAP/ABAADwAAbAAABFAcOAQcGIyInLgEnJjU0Nz4BNzYzMhceARcWBAAoKIteXWpqXV6LKCgoKIteXWpqXV6LKCgBwGpdXosoKCgoi15dampdXosoKCgoi15dAAAAAAIAAP/ABAADwAAdACMAAAEiBw4BBwYVFBceARcWMzI3PgE3NjUxNCcuAScmIwMnNxcBFwIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qPudGoQFCRQPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/NvlRaABhEQAAAIAAP/ABAADwAAdACkAAAEiBw4BBwYVFBceARcWMzI3PgE3NjUxNCcuAScmIwEHJwcnNyc3FzcXBwIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qASVK29tK3NxK29tK3APAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/SVK3NxK29tK3NxK2wADAAD/wAQAA8AAHQArAEEAAAEiBw4BBwYVFBceARcWMzI3PgE3NjUxNCcuAScmIxEiJjU0NjMyFhUxFAYjNxQGIyImNTE0Jy4BJyYxNDYzMhYVMQIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qHisrHh4rKx4lFg8PFgUGDgUGKx4eKwPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/LcrHh4rKx4eK9sPFRUPDjs7gjQ0HisrHgAAAQAA/8AEAAPAAAwAABM3CQEXCQEHCQEnCQEAkgFuAW6S/pIBbpL+kv6SkgFu/pIDLpL+kgFukv6S/pKSAW7+kpIBbgFuAAAAAgGO/8ACcgPAAAsAIgAAJRQGIyImNTQ2MzIWJyImNTE0Jy4BJyYxNDYzMhYVMQMUBiMCckMvL0NDLy9DchAWDAwcDAxDLy9DTBYQMi9DQy8vQ0OOFw8PWlvOVFMvQ0Mv/ccPFwAAAAABAFP/wAOtA8AAIAAAARMHAT4BNzMyFx4BFxYXFhceARcWOwEwBw4BBwYjIiYjATrnY/6VLXZDATEqK0geHxoaHyBKKysyIwUFPkNDfGNjYwIT/c4hA50rNAQODisZGBYVGxswEBAfH0sfHkIAAAAAAQEU/8AC7APAACMAAAE1IRUwBw4BBwYVETAXHgEXFjMVITUwNz4BNzY1ETAnLgEnJgEUAdgYGTsZGAIDHSAgO/4oGBk7GRgCAx0gIANxT08CAx0gIDv92BgZOxkYT08CAx0gIDsCKBgZOxkYAAAAAAIAdv/AA4oDwAAjAEcAAAE1IRUwBw4BBwYVETAXHgEXFjMVITUwNz4BNzY1ETAnLgEnJiE1IRUwBw4BBwYVETAXHgEXFjMVITUwNz4BNzY1ETAnLgEnJgGxAdkZGDsZGQMCHiAgO/4nGRg8GBkDAh4gIP6KAdkZGDwYGQMCHiAgO/4nGRg7GRkDAh4gIANxT08CAx0gIDv92BgZOxkYT08CAx0gIDsCKBgZOxkYT08CAx0gIDv92BgZOxkYT08CAx0gIDsCKBgZOxkYAAMAAP/lBAADmwATACgAPAAAATUhFTAGFREwFjMVITUwNjURMCYhNSEVMAYVETAWMxUhNTA2NREwJiMhNSEVMAYVETAWMxUhNTA2NREwJgJJAbeSJG7+SZIk/m4BtpIlbf5KkiVt/tsBt5Ikbv5JkiQDUklJJG7+AJJJSSRuAgCSSUkkbv4AkklJJG4CAJJJSSRu/gCSSUkkbgIAkgAAAAADAAAAIAQAA2AAEwAuADIAABM1IRUwBhURMBYzFSE1MDY1ETAmITUhFQ4BBxUDIwM1MxUwBhcWFx4BFxYxEzA0JRUzNQABgIAgYP6AgCACYAFAOlgOoIDA4DgYDBARHgoLgP4ggAMgQEAgYP5AgEBAIGABwIBAQAFINgH9gAMAQEAoeDxXWKI5OgIggEBAQAABAAABLgQAAlIABAAAExEhESEABAD8AAJS/twBJAAAAAEAAP/ABAADwAAbAAABFAcOAQcGIyInLgEnJjU0Nz4BNzYzMhceARcWBAAoKIteXWpqXV6LKCgoKIteXWpqXV6LKCgBwGpdXosoKCgoi15dampdXosoKCgoi15dAAAAAAMAAP/ABAADwAAdADsASwAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTE0Jy4BJyYjESInLgEnJjU0Nz4BNzYzMhceARcWFTEUBw4BBwYjGQEyNz4BNzY1NCcuAScmIwIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qY1hXgyUmJiWDV1hjY1hXgyUmJiWDV1hjal1eiygoKCiLXl1qA8AoKIteXWpqXV6LKCgoKIteXWpqXV6LKCj8ICYlg1dYY2NYV4MlJiYlg1dYY2NYV4MlJgPg/AAoKIteXWpqXV6LKCgAAgAA/8AEAAPAAB0AOwAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTE0Jy4BJyYjESInLgEnJjU0Nz4BNzYzMhceARcWFTEUBw4BBwYjAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXWpjWFeDJSYmJYNXWGNjWFeDJSYmJYNXWGMDwCgoi15dampdXosoKCgoi15dampdXosoKPwgJiWDV1hjY1hXgyUmJiWDV1hjY1hXgyUmAAAAAwAA/8AEAAPAAB0AOwBFAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1MTQnLgEnJiMRIicuAScmNTQ3PgE3NjMyFx4BFxYVMRQHDgEHBiMZASE0Jy4BJyYjAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXWpjWFeDJSYmJYNXWGNjWFeDJSYmJYNXWGMCACgoi15dagPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/CAmJYNXWGNjWFeDJSYmJYNXWGNjWFeDJSYD4P4Aal1eiygoAAMAAP/ABAADwAAdADsAUwAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTE0Jy4BJyYjESInLgEnJjU0Nz4BNzYzMhceARcWFTEUBw4BBwYjGQEhFBceARcWMzI3PgE3NjU0Jy4BJyYjAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXWpjWFeDJSYmJYNXWGNjWFeDJSYmJYNXWGP+ICYlg1dYY2NYV4MlJiYlg1dYYwPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/CAmJYNXWGNjWFeDJSYmJYNXWGNjWFeDJSYDwP4gY1hXgyUmJiWDV1hjY1hXgyUmAAAACgAA/8ADwAPAAAMABwALAA8AEQAVABkAGwAfACMAAAERMxEDIxEzBREzEQMjETMFMTcjETMnETMRATE3IxEzJxEzEQMAwCCAgP5gwCCAgP5wsMDAoID+cLDAwKCAA8D8AAQA/CADwKD8wANA/OADALAQ/YAgAkD9wAGQEP5AIAGA/oAACgAA/8ADwAPAAAMABwALAA8AEwAXABsAHQAhACUAABMzESMTESMRNyMRMwERMxEDIxEzBREzEQMjETMFMTcjETMnETMREKCgkICgwMACQMAggID+YMAggID+cLDAwKCAAXD+YAGQ/oABgCD+QAQA/AAEAPwgA8Cg/MADQPzgAwCwEP2AIAJA/cAAAAAKAAD/wAPAA8AAAwAHAAsADwATABcAGwAfACMAJwAAEzMRIxMRIxE3IxEzEzMRIxMRIxE3IxEzAREzEQMjETMFETMRAyMRMxCgoJCAoMDAUKCgkICgwMABQMAggID+YMAggIABcP5gAZD+gAGAIP5AAnD9oAJQ/cACQCD9gAQA/AAEAPwgA8Cg/MADQPzgAwAAAAAACgAA/8ADwAPAAAMABwALAA8AEwAXABsAHwAjACcAABMzESMTESMRNyMRMxMzESMTESMRNyMRMxMzESMTMxEjExEzEQMjETMQoKCQgKDAwFCgoJCAoMDAQMDAIICA4MAggIABcP5gAZD+gAGAIP5AAnD9oAJQ/cACQCD9gANA/MADIP0AA+D8AAQA/CADwAAACQAA/8ADwAPAAAMABwALAA8AEwAXABsAHwAjAAATMxEjExEjETcjETMTMxEjExEjETcjETMTMxEjATMRIwMzESMQoKCQgKDAwFCgoJCAoMDAQMDAAQDAwOCAgAFw/mABkP6AAYAg/kACcP2gAlD9wAJAIP2AA0D8wAQA/AADIP0AAAAAAQAA/8AEAAPAAAMAAAkDAgD+AAIAAgADwP4A/gACAAAMAAD/wAQAA8AAAwAHAAsADwATABcAGwAfACMAJwArAC8AABMhESEBESERJSERIRMhESEBESERJSERIQUhESEBESERJSERIRMhESEBESERJSERIRABwP5AAbD+YAHA/iAB4FABwP5AAbD+YAHA/iAB4PwQAcD+QAGw/mABwP4gAeBQAcD+QAGw/mABwP4gAeADsP5AAbD+YAGgIP4gAdD+QAGw/mABoCD+IFD+QAGw/mABoCD+IAHQ/kABsP5gAaAg/iAABQAA/8AEAAPAAAMABwALAA8AEwAAAREhEQMhESEBIREhASERIQEhESECIAHgIP5gAaD8IAHg/iACIAHg/iD94AHg/iADwP4gAeD+QAGg/gD+IAHg/iAEAP4gAAAGAAD/wAQAA8AAAwAHAAsADwATABcAABMRIREDIREhNxEhEQMhESEBIREhASERIQAB4CD+YAGgYAHgIP5gAaD8IAHg/iACIAHg/iADwP4gAeD+QAGgIP4gAeD+QAGg/gD+IAHg/iAAAAcAAP/ABAADwAADAAcACwAPABMAFwAbAAATESERAyERIRMRIREDIREhAREhEQMhESEBIREhAAHgIP5gAaBgAeAg/mABoP5AAeAg/mABoPwgAeD+IAPA/iAB4P5AAaD+AP4gAeD+QAGgAkD+IAHg/kABoP4A/iAAAAgAAP/ABAADwAADAAcACwAPABMAFwAbAB8AABMRIREDIREhExEhEQMhESElESERAyERIRMRIREDIREhAAHgIP5gAaBgAeAg/mABoPwgAeAg/mABoGAB4CD+YAGgA8D+IAHg/kABoP4A/iAB4P5AAaAg/iAB4P5AAaACQP4gAeD+QAGgAAEAAP/QBAADsAAJAAABGwEFARMlBRMBAWCgoAFg/wBA/sD+wED/AAKQASD+4ED/AP6AwMABgAEAAAAAAAMAAP/QBAADsAAJABMAGQAAASULAQUBAyUFAxMlBRMnJRsBBQcBBQEDJREEAP6goKD+oAEAQAFAAUBALP7U/tU88QFLlZYBSvD+cP6gAQBAAUACUEABIP7gQP8A/oDAwAGA/qC0tAFo7zwBDf7zPO8BOED/AP6AwAMgAAACAAD/0AQAA7AACQATAAABJQsBBQEDJQUDEyUFEyclGwEFBwQA/qCgoP6gAQBAAUABQEAs/tT+1TzxAUuVlgFK8AJQQAEg/uBA/wD+gMDAAYD+oLS0AWjvPAEN/vM87wADAAD/0AQAA7AACQATABgAAAElCwEFAQMlBQMTJQUTJyUbAQUHAREFAQMEAP6goKD+oAEAQAFAAUBALP7U/tU88QFLlZYBSvD+cP6gAQBAAlBAASD+4ED/AP6AwMABgP6gtLQBaO88AQ3+8zzv/tgCYED/AP6AAAADAAD/0AQAA7AACQATABsAAAElCwEFAQMlBQMTJQUTJyUbAQUHCwIFAQMlFwQA/qCgoP6gAQBAAUABQEAs/tT+1TzxAUuVlgFK8FCgoP6gAQBAAUCgAlBAASD+4ED/AP6AwMABgP6gtLQBaO88AQ3+8zzvATgBIP7gQP8A/oDAYAAAAAABAAAAIAQAA2AAAgAANyEBAAQA/gAgA0AAAAAAAQAAAJsEAALlAAIAABMhAQAEAP4AAuX9tgAAAAEAAACbBAAC5QACAAA3IQEABAD+AJsCSgAAAAABAAAAAAQAA2AAEAAAAScRIxUnARUzESE1MxUhETMEAMCAwP4AgAFAgAFAgAFgwAEgoMD+ACD+wMDAAUAAAAAAAwAA/8AEAAOAAAsAFwAwAAAlFAYjIiY1NDYzMhYFFAYjIiY1NDYzMhYZASE0JisBFTMTDgEVFBYzITUhIiY1OAE1AYA4KCg4OCgoOAKAOCgoODgoKDj9ACUbwIAwFhpLNQMA/QAbJSAoODgoKDg4KCg4OCgoODgBeAGAGyVA/mQSNB41S0AlGwEAAAABAAD/wAPAA4AANQAAAQ4BIyImJy4BNTQ2NzYnLgEnJiMiBw4BBwYxFBceARcWFxYXHgEXFjMwNz4BNzY1NCcuAScmAsAwIDAwYDAwUFAwGBISSCoqGBghITwVFRYXSS0uLy9EQ5FFRDAeHkgeHh8fVCsrAUAwUFAwMGAwMCAwGCsrVB8fHh5IHh4wREWRQ0QvLy4tSRcWFRU8ISEYGCoqSBISAAQAwP/AA0ADwAAPABMAHwAjAAABISIGFREUFjMhMjY1ETQmBSEVIRMiJjU0NjMyFhUUBjchESEC4P5AKDg4KAHAKDg4/ngBAP8AgBslJRsbJSXl/gACAAPAOCj8wCg4OCgDQCg4MCD8kCUbGyUlGxslwAKAAAAAAAIAgAAAA4ADwAALABwAAAE0NjMyFhUUBiMiJgUjAxMnBxMDIyIGFREhETQmAUBwUFBwcFBQcAHAI8dKYGBKxyNgIAMAIAMAUHBwUFBwcLD+bAF0YGD+jAGUcFD+wAFAUHAAAAADAAD/wAQAA4AAJwA/AEMAAAEjNTQnLgEnJiMiBw4BBwYVERQXHgEXFjMyNz4BNzY9ATMyNjURNCYlLgEnPgE3PgEzMhYXHgEXDgEHDgEjIiYBIzUzA8DAHh5pRkVQUEVGaR4eHh5pRkVQUEVGaR4ewBslJfzVHCIJCSIcLGs5OWssHCIJCSIcLGs5OWsCpICAAoBgIR0dLAwNDQwsHR0h/YAhHR0sDA0NDCwdHSFgJRsBQBslPgkSBwcSCQ8PDw8JEgcHEgkPDw/+kcAAAAAAAQAA/8AEAAPAABYAAAEnAScFJy4BBwYWHwEDFwEXETM/ATUhAwC3AbeA/dusJlobGg8mrNuAAUm3gEDA/wABQLcBSYDbrCYPGhtaJqz924ABt7f/AMBAgAAAAAACAAAAAAQAA0AAJwArAAABAyM1NCYjISIGFREXMw4BFRQWMzI2NTQmJyEOARUUFjMyNjU0JiczJTUzFwQAgMAmGv3AGiZAUQgJSzU1SwkIAWIICUs1NUsJCFH+wIVgAYABAIAaJiYa/gBADiERNUtLNREhDg4hETVLSzURIQ7AwMAAAAACAAD/wAQAA8AAGwBzAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1NCcuAScmAyImJxM+AT0BNCYjIicuAScmNS4BKwEiBh0BFBYfARUmJy4BJyY1NDY3MzI2PwE+AT0BPgEzMhYXDgEHDgEVFBYXHgEzOgEzFhceAQcGBxQGFQYHDgEHBgIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qL1kp6QQEEw0qKipCFBUFDAaADRMKCG4sIyMzDQ4WFXUGDAWABAUeQSE1YywDBgMbHR0bHEYmAgUCBgYFBAYFEgEeJSRRLC0DwCgoi15dampdXosoKCgoi15dampdXosoKPxAExEBBwQLBmANExITLBMSAQQFEw3ACRAEN7wfKCdfNTU5NGAsBQSABQwGTQkKFxYDBQMbRyYmRxsbHRMjIl88O0YBAwEgGBkjCgkAAAIBQP/AAoADwAALAB0AAAEUBiMiJjU0NjMyFhUjIgYVETMRMxEzETMRMxE0JgJAOCgoODgoKDjAGyVAUCBQQCUDYCg4OCgoODjIJRv+wP6AAYD+gAGAAUAbJQAAAgDA/8ADAAPAAAsAJgAAARQGIyImNTQ2MzIWEzcnLgEjISIGDwEXNxcHMxMzETMRMxMzJzcXAkA4KCg4OCgoOI8xhQUOCP8ACA4FhTFvJoZ7FUAgQBV7hiZvA2AoODgoKDg4/jgjzwYICAbOJJBa9v7AAUD+wAFA9lqQAAQAAP/ABAADwAALABcAKQBEAAABFAYjIiY1NDYzMhYFFAYjIiY1NDYzMhYFIyIGFREzETMRMxEzETMRNCYBNycuASMhIgYPARc3FwczEzMRMxEzEzMnNxcBADgoKDg4KCg4AkA4KCg4OCgoOP3AwBslQFAgUEAlArQxhQUOCP8ACA4FhTFvJoZ7FUAgQBV7hiZvA2AoODgoKDg4KCg4OCgoODjIJRv+wP6AAYD+gAGAAUAbJf8AI88GCAgGziSQWvb+wAFA/sABQPZakAAAAAACAGL/wAOhA8AALAA5AAABNDY3LgEnJgYjIiYHDgEHBgcGFhcWFx4BNz4BMzIWNz4BNz4BNyInLgEnJicDPgEnDgEHDgEXFjY3AxdrBC11GTxqHh9ZMUFxIiIGBxkbGyEgTzIxPDs7OzM1SCAlIQEBFRUzFhUBgBogBSdUHBkjBitSGwGgYWACQiIBBjUuAQFFOjtGRo1AQS8vVQICKCoBAU4vNlkDCwoxJyg6AXwhVi0CKyEcViwDKyAAAAYAQP/AA8ADvQANABsANgBZAGsAfgAAASIGFREUFjMyNjURNCYhIgYVERQWMzI2NRE0JhMUFjMxFRQWMzI2PQEzFRQWMzI2PQEyNjURISUuASc3NiYnJgYPAScuASMiBg8BJy4BBw4BHwEOAQcVITUjJSImNTQ2MzgBMTgBMTIWFRQGMyImNTQ2MzgBMTgBMTIWFRQGIwOAGiYmGhomJvzmGiYmGhomJkY4KCYaGiaAJhoaJig4/cACPgdFNSAGCQwMGQYgCBYtGBgtFgggBhkMDAkGIDVFBwI+Av6CDRMTDQ0TE7MNExMNDRMTDQJAJhr/ABomJhoBABomJhr/ABomJhoBABom/qAoOIAaJiYagIAaJiYagDgoAWBAQm0jQAwZBgYJDEADBwgIBwNADAkGBhkMQCNtQiAgQBMNDRMTDQ0TEw0NExMNDRMABAAA/8ADwAOAAAMABwALAA8AABMRJRETJREhBRElEQMlESEAAYBAAgD+AAIA/gBA/oABgAHAATg0/pQBdkr+QED+QEgBeP6QNQE7AAEAAAABAABxUvu9Xw889QALBAAAAAAA1kGxXgAAAADWQbFeAAD/wAQCA8AAAAAIAAIAAAAAAAAAAQAAA8D/wAAABAAAAP/+BAIAAQAAAAAAAAAAAAAAAAAAAD0EAAAAAAAAAAAAAAACAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAHBAAAAAQAAAAEAAAABAAAAAQAAAAEAAGOBAAAUwQAARQEAAB2BAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAADwAAAA8AAAAPAAAADwAAAA8AAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAwAQAAIAEAAAABAAAAAQAAAAEAAAABAABQAQAAMAEAAAABAAAYgQAAEAEAAAAAAAAAAAKABQAHgA0AEgAYAB4AIwAwgDyAS4BcgHOAfICJgJeApYC/ANMA5QDpAPUBEQEngUGBYIFxAYKBlQGnAbeBu4HUAd+B7IH7ggwCE4IjAi8CPgJOglICVYJZAmECcoKHApYCooK8gsgC2IMCAw2DHQM3A06DeAOBgABAAAAPQB/AAwAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAADgCuAAEAAAAAAAEACgAAAAEAAAAAAAIABwB7AAEAAAAAAAMACgA/AAEAAAAAAAQACgCQAAEAAAAAAAUACwAeAAEAAAAAAAYACgBdAAEAAAAAAAoAGgCuAAMAAQQJAAEAFAAKAAMAAQQJAAIADgCCAAMAAQQJAAMAFABJAAMAAQQJAAQAFACaAAMAAQQJAAUAFgApAAMAAQQJAAYAFABnAAMAAQQJAAoANADIU3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0VmVyc2lvbiAxLjAAVgBlAHIAcwBpAG8AbgAgADEALgAwU3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0U3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0UmVndWxhcgBSAGUAZwB1AGwAYQByU3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0Rm9udCBnZW5lcmF0ZWQgYnkgSWNvTW9vbi4ARgBvAG4AdAAgAGcAZQBuAGUAcgBhAHQAZQBkACAAYgB5ACAASQBjAG8ATQBvAG8AbgAuAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==) format('truetype');font-weight: normal;font-style: normal;}</style>");
            }

            if (render != null)
            {
                if (asTable)
                {
                    render.RenderStylesTable(useBookmarks, exportBookmarksOnly, cssStyles);
                }
                else
                {
                    render.RenderStyles(useBookmarks, exportBookmarksOnly, cssStyles);
                }
            }

            if (useBookmarks)
            {
                RenderBookmarkScript();
            }

            HtmlWriter.Indent--;
            HtmlWriter.WriteLine();

            HtmlWriter.WriteEndTag("head");
            HtmlWriter.WriteLine();

            HtmlWriter.WriteBeginTag("body");
            if ((pages != null) && (pages.Count > 0) && (pages[0].Brush != null))
            {
                Color backColor = StiBrush.ToColor(pages[0].Brush);
                if (backColor.A > 0)
                {
                    HtmlWriter.WriteAttribute("bgcolor", FormatColor(backColor));
                }
            }
            if (StiOptions.Export.Html.UseExtendedStyle)
            {
                HtmlWriter.WriteAttribute("class", "sBaseStyleFix");
            }
            if (StiOptions.Export.Html.PrintLayoutOptimization)
            {
                HtmlWriter.WriteAttribute("style", "margin:0;");
            }
            HtmlWriter.Write(">");
            HtmlWriter.Indent++;
            HtmlWriter.WriteLine();
            //            HtmlWriter.Indent++;
        }

        private void FillBitmapBackground(ref Bitmap bmp, Color fillColor)
        {
            Color transparentColor = Color.LightGray;
            if ((bmp.Height > 0) && (bmp.Width > 0))
            {
                transparentColor = bmp.GetPixel(0, bmp.Size.Height - 1);
            }
            if (transparentColor.A != 0xff)
            {
                Size size = bmp.Size;
                PixelFormat pixelFormat = bmp.PixelFormat;
                Bitmap image = new Bitmap(size.Width, size.Height, PixelFormat.Format32bppArgb);
                Graphics graphics = Graphics.FromImage(image);
                graphics.Clear(fillColor);
                Rectangle destRect = new Rectangle(0, 0, size.Width, size.Height);
                ImageAttributes imageAttrs = new ImageAttributes();
                imageAttrs.SetColorKey(transparentColor, transparentColor);
                graphics.DrawImage(bmp, destRect, 0, 0, size.Width, size.Height, GraphicsUnit.Pixel, imageAttrs, null, IntPtr.Zero);
                imageAttrs.Dispose();
                graphics.Dispose();
                bmp.Dispose();
                bmp = image;
            }
        }

        private void RenderBookmarkScript()
        {
            HtmlWriter.WriteBeginTag("script");
            HtmlWriter.WriteAttribute("type", "text/javascript");
            HtmlWriter.WriteLine(">");

            //load script from resources
            byte[] buf = GetFile("Stimulsoft.Report", "Stimulsoft.Report.Export.Dtree.DtreeScript.js");
            StringBuilder sb = new StringBuilder();
            for (int index = 0; index < buf.Length; index++) sb.Append((char)buf[index]);

            string[,] fileList = new string[,]
                    {
                            {"img/base.gif",        "DtreeBase.png"},
                            {"img/page.gif",        "DtreePage.png"},
                            {"img/folder.gif",      "DtreeFolder.png"},
                            {"img/folderopen.gif",  "DtreeFolderopen.png"},
                            {"img/empty.gif",       "DtreeEmpty.png"},
                            {"img/line.gif",        "DtreeLine.png"},
                            {"img/join.gif",        "DtreeJoin.png"},
                            {"img/joinbottom.gif",  "DtreeJoinbottom.png"},
                            {"img/plus.gif",        "DtreePlus.png"},
                            {"img/plusbottom.gif",  "DtreePlusbottom.png"},
                            {"img/minus.gif",       "DtreeMinus.png"},
                            {"img/minusbottom.gif", "DtreeMinusbottom.png"}
                    };

            //replace images path
            htmlImageHost.ForcePng = true;
            for (int index = 0; index < fileList.GetLength(0); index++)
            {
                Bitmap bmp = GetImage("Stimulsoft.Report", "Stimulsoft.Report.Export.Dtree." + fileList[index, 1], false);
                FillBitmapBackground(ref bmp, Color.FromArgb(0xf0, 0xf0, 0xf0));
                string bmpPath = htmlImageHost.GetImageString(bmp).Replace("\\", "/");
                sb.Replace(fileList[index, 0], bmpPath);
            }
            htmlImageHost.ForcePng = false;

            //write script
            string[] lineList = sb.ToString().Split(new char[] { '\r' });
            for (int index = 0; index < lineList.Length; index++)
            {
                HtmlWriter.WriteLine(lineList[index]);
            }

            HtmlWriter.WriteEndTag("script");
            HtmlWriter.WriteLine();
        }

        private void RenderChartScripts(bool writeScriptTag = true)
        {
            if (chartData.Count == 0) return;

            if (writeScriptTag)
            {
                HtmlWriter.WriteBeginTag("script");
                HtmlWriter.WriteAttribute("type", "text/javascript");
                HtmlWriter.WriteLine(">");
            }
            String guid = Guid.NewGuid().ToString().Replace("-", "");
            string[] fileList = new string[]
                    {
                        "stianimation.js"
                    };

            //load scripts from resources
            for (int index = 0; index < fileList.Length; index++)
            {
                byte[] buf = GetFile(this.GetType().Assembly, "Stimulsoft.Report.Export.Services.Htmls.ChartScripts." + fileList[index]);
                string st = Encoding.ASCII.GetString(buf);
                HtmlWriter.WriteLine(st.Replace("animateSti", "animateSti" + guid) + ";");
            }

            HtmlWriter.WriteLine("setTimeout(function() {");

            foreach (var key in chartData.Keys)
            {
                HtmlWriter.WriteLine(
                    string.Format("animateSti{0}(\"{1}\");", guid,
                    chartData[key]));
            }
            HtmlWriter.WriteLine("}, 300);");
            if (writeScriptTag)
            {
                HtmlWriter.WriteEndTag("script");
                HtmlWriter.WriteLine();
            }
        }

        internal string GetGuid(StiComponent comp)
        {
            if (!chartData.ContainsKey(comp))
            {
                chartData.Add(comp, Guid.NewGuid().ToString().Replace("-", ""));
            }
            return chartData[comp] as string;
        }


        private void RenderEndDoc()
        {
            HtmlWriter.Indent--;
            HtmlWriter.WriteEndTag("body");
            HtmlWriter.Indent--;
            HtmlWriter.WriteLine();
            HtmlWriter.WriteEndTag("html");
        }

        private void RenderBookmarkTree(StiBookmark root, int bookmarkWidth, Hashtable bookmarksPageIndex)
        {
            var bookmarksTree = new List<StiBookmarkTreeNode>();
            AddBookmarkNode(root, -1, bookmarksTree);

            //			HtmlWriter.WriteBeginTag(strSpanDiv + " class=\"dtree\" style=\"");
            //			HtmlWriter.WriteStyleAttribute("position", "absolute");
            HtmlWriter.WriteStyleAttribute("width", string.Format("{0}px", bookmarkWidth));
            //			HtmlWriter.WriteStyleAttribute("border-right", "1px");
            //			HtmlWriter.WriteStyleAttribute("border-right-style", "solid");
            //			HtmlWriter.WriteStyleAttribute("border-right-color", "Gray");
            HtmlWriter.WriteStyleAttribute("background-color", "#f0f0f0");
            HtmlWriter.Write("\">");
            HtmlWriter.Indent++;
            HtmlWriter.WriteLine();

            HtmlWriter.Indent++;
            HtmlWriter.WriteFullBeginTag("frame");
            HtmlWriter.WriteLine();
            HtmlWriter.WriteBeginTag("script");
            HtmlWriter.WriteAttribute("type", "text/javascript");
            HtmlWriter.WriteLine(">");
            HtmlWriter.Indent++;
            HtmlWriter.WriteLine("<!--");
            HtmlWriter.WriteLine("bmrk = new dTree('bmrk');");
            for (int index = 0; index < bookmarksTree.Count; index++)
            {
                var node = bookmarksTree[index];
                string pageIndex = string.Empty;
                string url = node.Url;
                if (bookmarksPageIndex.ContainsKey(node.Title))
                {
                    pageIndex = string.Format("Page {0}", (int)bookmarksPageIndex[node.Title] + 1);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(node.FullUrl) && bookmarksPageIndex.ContainsKey(node.FullUrl))
                    {
                        pageIndex = string.Format("Page {0}", (int)bookmarksPageIndex[node.FullUrl] + 1);
                        url = "#" + node.FullUrl.Replace("\\", "\\\\");
                    }
                    else
                    {
                        pageIndex = "Page 0";
                    }
                }

                HtmlWriter.WriteLine(string.Format("bmrk.add({0}, {1}, '{2}', '{3}', '{4}');", index, node.Parent, node.Title, url, pageIndex));
            }
            HtmlWriter.WriteLine("document.write(bmrk);");
            HtmlWriter.WriteLine("//-->");
            HtmlWriter.Indent--;
            HtmlWriter.WriteEndTag("script");
            HtmlWriter.Indent--;
            HtmlWriter.WriteLine();

            HtmlWriter.Indent--;
            //			HtmlWriter.WriteEndTag(strSpanDiv);
            //			HtmlWriter.WriteLine();
        }

        private class StiBookmarkTreeNode
        {
            public int Parent;
            public string Title;
            public string Url;
            public string FullUrl;
            public bool Used;
        }

        private void AddBookmarkNode(StiBookmark bkm, int parentNode, List<StiBookmarkTreeNode> bookmarksTree, string path = null)
        {
            var tn = new StiBookmarkTreeNode();
            tn.Parent = parentNode;
            string st = bkm.Text.Replace("'", "");
            tn.Title = st;
            tn.Url = "#" + st;
            if (!string.IsNullOrWhiteSpace(path)) path += "\\" + st;
            tn.FullUrl = path;
            tn.Used = true;
            bookmarksTree.Add(tn);
            int currentNode = bookmarksTree.Count - 1;
            if (bkm.Bookmarks.Count != 0)
            {
                if (string.IsNullOrWhiteSpace(path)) path = "%";
                for (int tempCount = 0; tempCount < bkm.Bookmarks.Count; tempCount++)
                {
                    AddBookmarkNode(bkm.Bookmarks[tempCount], currentNode, bookmarksTree, path);
                }
            }
        }
        #endregion

        internal string PrepareTextForHtml(string text, bool processWhiteSpaces = true)
        {
            if (text == null) return null;
            text = text.Replace("\r", "");
            StringBuilder sbFull = new StringBuilder();

            if (processWhiteSpaces)
            {
                string[] txt = text.Split(new char[] { '\n' });
                for (int index = 0; index < txt.Length; index++)
                {
                    string st = txt[index];
                    int pos = 0;
                    while ((pos < st.Length) && (st[pos] == ' ')) pos++;
                    if (pos > 0)
                    {
                        for (int indexSp = 0; indexSp < pos; indexSp++)
                        {
                            sbFull.Append("&nbsp;");
                        }
                        sbFull.Append(st.Substring(pos));
                    }
                    else
                    {
                        sbFull.Append(st);
                    }
                    if (index < txt.Length - 1)
                    {
                        sbFull.Append("<br>");
                    }
                }
            }
            else
            {
                sbFull.Append(text.Replace("\n", "<br>"));
            }
            return sbFull.ToString();
        }


        internal static string ConvertTextWithHtmlTagsToHtmlText(StiText stiText, string text)
        {
            string inputText = text;
            var baseTagsState = new StiTextRenderer.StiHtmlTagsState(
                stiText.Font.Bold,
                stiText.Font.Italic,
                stiText.Font.Underline,
                stiText.Font.Strikeout,
                stiText.Font.SizeInPoints,
                stiText.Font.Name,
                StiBrush.ToColor(stiText.TextBrush),
                StiBrush.ToColor(stiText.Brush),
                false,
                false,
                0,
                0,
                stiText.LineSpacing,
                stiText.HorAlignment);
            StiTextRenderer.StiHtmlState baseState = new StiTextRenderer.StiHtmlState(
            baseTagsState,
            0);
            var statesList = StiTextRenderer.ParseHtmlToStates(inputText, baseState);

            var finalText = new StringBuilder();
            var textAlign = StiTextHorAlignment.Left;
            double lineHeight = stiText.LineSpacing;
            double lastLineHeight;
            bool needNbsp = true;
            bool needCloseFont = false;
            var outputText = new StringBuilder();
            StiTextRenderer.StiHtmlTagsState prevState = baseTagsState;
            for (int index = 0; index < statesList.Count; index++)
            {
                StiTextRenderer.StiHtmlState htmlState = statesList[index];
                StiTextRenderer.StiHtmlTagsState state = htmlState.TS;

                if (state.Bold != prevState.Bold && !state.Bold)
                {
                    outputText.Append("</b>");
                }
                if (state.Italic != prevState.Italic && !state.Italic)
                {
                    outputText.Append("</i>");
                }
                if (state.Underline != prevState.Underline && !state.Underline)
                {
                    outputText.Append("</u>");
                }
                if (state.Strikeout != prevState.Strikeout && !state.Strikeout)
                {
                    outputText.Append("</s>");
                }
                if (state.Superscript != prevState.Superscript && !state.Superscript)
                {
                    outputText.Append("</sup>");
                }
                if (state.Subsript != prevState.Subsript && !state.Subsript)
                {
                    outputText.Append("</sub>");
                }

                if (state.Bold != prevState.Bold && state.Bold)
                {
                    outputText.Append("<b>");
                }
                if (state.Italic != prevState.Italic && state.Italic)
                {
                    outputText.Append("<i>");
                }
                if (state.Underline != prevState.Underline && state.Underline)
                {
                    outputText.Append("<u>");
                }
                if (state.Strikeout != prevState.Strikeout && state.Strikeout)
                {
                    outputText.Append("<s>");
                }
                if (state.Superscript != prevState.Superscript && state.Superscript)
                {
                    outputText.Append("<sup>");
                }
                if (state.Subsript != prevState.Subsript && state.Subsript)
                {
                    outputText.Append("<sub>");
                }

                if ((state.FontColor != prevState.FontColor) ||
                    (state.BackColor != prevState.BackColor) ||
                    (state.FontName != prevState.FontName) ||
                    (state.FontSize != prevState.FontSize) ||
                    (state.LetterSpacing != prevState.LetterSpacing) ||
                    (state.WordSpacing != prevState.WordSpacing) ||
                    (state.HtmlStyle != prevState.HtmlStyle))
                {
                    string fontStyle = "";
                    if (state.HtmlStyle != baseTagsState.HtmlStyle)
                    {
                        fontStyle += state.HtmlStyle + ";";
                    }
                    if (state.FontColor != baseTagsState.FontColor)
                    {
                        if (!fontStyle.Contains("color:") && (state.FontColor.A > 0))
                            fontStyle += string.Format("color:#{0:X6};", state.FontColor.ToArgb() & 0xFFFFFF);
                    }
                    if (state.BackColor != baseTagsState.BackColor)
                    {
                        if (!fontStyle.Contains("background-color:") && (state.BackColor.A > 0))
                            fontStyle += string.Format("background-color:#{0:X6};", state.BackColor.ToArgb() & 0xFFFFFF);
                    }
                    if (state.FontName != baseTagsState.FontName)
                    {
                        if (!fontStyle.Contains("font-family:"))
                            fontStyle += string.Format("font-family:{0};", state.FontName);
                    }
                    if (state.FontSize != baseTagsState.FontSize)
                    {
                        if (!fontStyle.Contains("font-size:"))
                            fontStyle += string.Format("font-size:{0}pt;", state.FontSize).Replace(",", ".");
                    }
                    if (state.LetterSpacing != baseTagsState.LetterSpacing)
                    {
                        if (!fontStyle.Contains("letter-spacing:"))
                            fontStyle += string.Format("letter-spacing:{0}em;", state.LetterSpacing).Replace(",", ".");
                    }
                    if (state.WordSpacing != baseTagsState.WordSpacing)
                    {
                        if (!fontStyle.Contains("word-spacing:"))
                            fontStyle += string.Format("word-spacing:{0}em;", state.WordSpacing).Replace(",", ".");
                    }

                    if (needCloseFont) outputText.Append("</font>");
                    needCloseFont = false;
                    if (fontStyle.Length > 0)
                    {
                        needCloseFont = true;
                        outputText.Append(string.Format("<font style=\"{0}\">", fontStyle));
                    }
                }

                textAlign = state.TextAlign;
                lastLineHeight = state.LineHeight;

                if (htmlState.Text.ToString() == "\x0A")
                {
                    if (needNbsp)
                    {
                        outputText.Append("&nbsp;");
                    }
                    finalText.Append(GetParagraphString(outputText, textAlign, lineHeight, stiText.LineSpacing, stiText.RightToLeft));
                    outputText = new StringBuilder();
                    needNbsp = true;
                    lineHeight = lastLineHeight;
                }
                else
                {
                    outputText.Append(htmlState.Text);
                    if (htmlState.Text.ToString().Trim().Length > 0) needNbsp = false;
                }

                prevState = state;
            }

            if (outputText.Length > 0)
            {
                finalText.Append(GetParagraphString(outputText, textAlign, lineHeight, stiText.LineSpacing, stiText.RightToLeft));
            }
            if (needCloseFont) finalText.Append("</font>");
            if (prevState.Bold != baseTagsState.Bold)
            {
                finalText.Append(baseTagsState.Bold ? "<b>" : "</b>");
            }
            if (prevState.Italic != baseTagsState.Italic)
            {
                finalText.Append(baseTagsState.Italic ? "<i>" : "</i>");
            }
            if (prevState.Underline != baseTagsState.Underline)
            {
                finalText.Append(baseTagsState.Underline ? "<u>" : "</u>");
            }
            if (prevState.Strikeout != baseTagsState.Strikeout)
            {
                finalText.Append(baseTagsState.Strikeout ? "<s>" : "</s>");
            }
            if (prevState.Superscript != baseTagsState.Superscript)
            {
                finalText.Append(baseTagsState.Superscript ? "<sup>" : "</sup>");
            }
            if (prevState.Subsript != baseTagsState.Subsript)
            {
                finalText.Append(baseTagsState.Subsript ? "<sub>" : "</sub>");
            }

            return finalText.ToString();
        }

        private static string GetParagraphString(StringBuilder text, StiTextHorAlignment textAlign, double lineHeight, double baseLineSpacing, bool rtl)
        {
            StringBuilder outputText = new StringBuilder();
            outputText.Append("<p ");
            outputText.Append("style=\"margin:0px;");
            string align = rtl ? "right" : "left";
            if (textAlign == StiTextHorAlignment.Center) align = "center";
            if (textAlign == StiTextHorAlignment.Right) align = rtl ? "left" : "right";
            if (textAlign == StiTextHorAlignment.Width) align = "justify";
            outputText.Append(string.Format("text-align:{0};", align));
            if (lineHeight != baseLineSpacing)
            {
                outputText.Append(string.Format("line-height:{0}em;", Math.Round(lineHeight * 1.12, 2)));
            }
            outputText.Append("\">");
            outputText.Append(text);
            outputText.Append("</p>");
            return outputText.ToString();
        }

        private string GetBackgroundImagePath(StiPagesCollection pages, double zoomBase, bool useMargins)
        {
            string backGroundImageString = string.Empty;
            if (pages.Count > 0)
            {
                #region watermark
                StiPage page = pages[0];
                StiWatermark watermark = page.Watermark;
                if (watermark != null && watermark.Enabled &&
                    (watermark.ExistImage() || !string.IsNullOrWhiteSpace(watermark.ImageHyperlink) || !string.IsNullOrEmpty(watermark.Text)))
                {
                    var painter = StiPainter.GetPainter(typeof(StiPage), StiGuiMode.Gdi) as IStiPagePainter;
                    var zoom = StiOptions.Export.Html.PrintLayoutOptimization ? zoomBase * 0.96 : zoomBase;
                    var image = painter.GetWatermarkImage(page, zoom, useMargins) as Bitmap;

                    if (HtmlImageHost != null) backGroundImageString = HtmlImageHost.GetImageString(image as Bitmap).Replace('\\', '/');
                    if (backGroundImageString == null) backGroundImageString = string.Empty;
                    if (image != null) image.Dispose();
                }
                #endregion
            }
            return backGroundImageString.Replace('\\', '/');
        }

        public static Bitmap GetImage(string assemblyName, string imageName, bool makeTransparent)
        {
            Assembly[] assemblys = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly a in assemblys)
            {
                if (StiOptions.Engine.FullTrust)
                {
                    string str = a.GetName().Name;
                    if (str == assemblyName) return StiImageUtils.GetImage(a, imageName, makeTransparent);
                }
                else
                {
                    string str = a.FullName;
                    if (str.StartsWith(assemblyName + ", ")) return StiImageUtils.GetImage(a, imageName, makeTransparent);
                }
            }
            throw new Exception(string.Format("Can't find assembly '{0}'", assemblyName));
        }

        /// <summary>
        /// Gets the object placed in assembly.
        /// </summary>
        /// <param name="assemblyName">The name of assembly in which the object is placed.</param>
        /// <param name="fileName">The name of the file to look for.</param>
        /// <returns>The object.</returns>
        public static byte[] GetFile(string assemblyName, string fileName)
        {
            Assembly[] assemblys = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly a in assemblys)
            {
                if (StiOptions.Engine.FullTrust)
                {
                    string str = a.GetName().Name;
                    if (str == assemblyName) return GetFile(a, fileName);
                }
                else
                {
                    string str = a.FullName;
                    if (str.StartsWith(assemblyName + ", ")) return GetFile(a, fileName);
                }
            }
            throw new Exception(string.Format("Can't find assembly '{0}'", assemblyName));
        }

        /// <summary>
        /// Gets the Image object placed in assembly.
        /// </summary>
        /// <param name="imageAssembly">Assembly in which is the Image object is placed.</param>
        /// <param name="fileName">The name of the image file to look for.</param>
        /// <returns>The Image object.</returns>
        public static byte[] GetFile(Assembly imageAssembly, string fileName)
        {
            Stream stream = imageAssembly.GetManifestResourceStream(fileName);
            if (stream != null)
            {
                byte[] buf = new byte[stream.Length];
                stream.Read(buf, 0, buf.Length);
                return buf;
            }
            else throw new Exception(string.Format("Can't find file '{0}' in resources", fileName));
        }

        private void AssembleGuidUsedInBookmark(StiBookmark node, Hashtable hash)
        {
            if (!string.IsNullOrWhiteSpace(node.ComponentGuid))
            {
                hash[node.ComponentGuid] = node.Text;
            }
            if (node.Bookmarks != null && node.Bookmarks.Count > 0)
            {
                foreach (StiBookmark bookmark in node.Bookmarks)
                {
                    AssembleGuidUsedInBookmark(bookmark, hash);
                }
            }
        }

        internal void PrepareSvg(StiHtmlTextWriter sWriter, double width, double height)
        {
            sWriter.WriteBeginTag("svg");
            sWriter.WriteAttribute("version", "1.1");
            sWriter.WriteAttribute("baseProfile", "full");
            sWriter.WriteAttribute("xmlns", "http://www.w3.org/2000/svg");
            sWriter.WriteAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
            sWriter.WriteAttribute("xmlns:ev", "http://www.w3.org/2001/xml-events");
            sWriter.WriteAttribute("height", width.ToString().Replace(",", "."));
            sWriter.WriteAttribute("width", height.ToString().Replace(",", "."));
            sWriter.Write(">");
        }

        internal String PrepareChartData(StiHtmlTextWriter writer, StiChart chart, double width, double height)
        {
            using (var img = new Bitmap(1, 1))
            {
                using (var g = Graphics.FromImage(img))
                {
                    var painter = new StiGdiContextPainter(g);
                    var context = new Stimulsoft.Base.Context.StiContext(painter, true, false, false, (float)zoom);
                    StringBuilder sb = new StringBuilder();
                    StiHtmlTextWriter sWriter = null;
                    if (writer == null)
                    {
                        StringWriter sw = new StringWriter(sb);
                        sWriter = new StiHtmlTextWriter(sw);
                    }
                    else
                    {
                        sWriter = writer;
                    }
                    float scale = 0.96f;
                    PrepareSvg(sWriter, Math.Round(height * scale, 2), Math.Round(width * scale, 2));
                    StiSvgData pp = new StiSvgData();

                    pp.X = 0;
                    pp.Y = 0;
                    pp.Width = (float)(width * scale);
                    pp.Height = (float)(height * scale);
                    pp.Component = chart;

                    MemoryStream ms = new MemoryStream();
                    XmlTextWriter xmlsWriter = new XmlTextWriter(ms, Encoding.UTF8);
                    StiChartSvgHelper.WriteChart(xmlsWriter, pp, (float)this.zoom, chartType == StiHtmlChartType.AnimatedVector);
                    xmlsWriter.Flush();
                    sWriter.Write(Encoding.UTF8.GetString(ms.ToArray()));
                    sWriter.WriteEndTag("svg");
                    GetGuid(chart);
                    if (writer == null) return sb.ToString();
                    else return null;
                }
            }
        }

        internal String PrepareGaugeData(StiHtmlTextWriter writer, StiGauge gauge, double width, double height)
        {
            using (var img = new Bitmap(1, 1))
            {
                using (var g = Graphics.FromImage(img))
                {
                    var painter = new StiGdiContextPainter(g);
                    var context = new Stimulsoft.Base.Context.StiContext(painter, true, false, false, (float)zoom);
                    StringBuilder sb = new StringBuilder();
                    StiHtmlTextWriter sWriter = null;
                    if (writer == null)
                    {
                        StringWriter sw = new StringWriter(sb);
                        sWriter = new StiHtmlTextWriter(sw);
                    }
                    else
                    {
                        sWriter = writer;
                    }
                    float scale = 0.96f;
                    PrepareSvg(sWriter, Math.Round(height * scale, 2), Math.Round(width * scale, 2));
                    StiSvgData pp = new StiSvgData();

                    pp.X = 0;
                    pp.Y = 0;
                    pp.Width = (float)(width * scale);
                    pp.Height = (float)(height * scale);
                    pp.Component = gauge;

                    MemoryStream ms = new MemoryStream();
                    XmlTextWriter xmlsWriter = new XmlTextWriter(ms, Encoding.UTF8);
                    StiGaugeSvgHelper.WriteGauge(xmlsWriter, pp, (float)this.zoom, (chartType == StiHtmlChartType.AnimatedVector));
                    xmlsWriter.Flush();
                    sWriter.Write(Encoding.UTF8.GetString(ms.ToArray()));
                    sWriter.WriteEndTag("svg");
                    GetGuid(gauge);
                    if (writer == null) return sb.ToString();
                    else return null;
                }
            }
        }

        internal String PrepareMapData(StiHtmlTextWriter writer, StiMap map, double width, double height)
        {

            StringBuilder sb = new StringBuilder();
            StiHtmlTextWriter sWriter = null;
            if (writer == null)
            {
                StringWriter sw = new StringWriter(sb);
                sWriter = new StiHtmlTextWriter(sw);
            }
            else
            {
                sWriter = writer;
            }
            float scale = 1;// 0.96f;
            PrepareSvg(sWriter, Math.Round(height * scale, 2), Math.Round(width * scale, 2));
            MemoryStream ms = new MemoryStream();
            XmlTextWriter xmlsWriter = new XmlTextWriter(ms, Encoding.UTF8);
            StiMapSvgHelper.DrawMap(xmlsWriter, map, width, height, chartType == StiHtmlChartType.AnimatedVector);
            xmlsWriter.Flush();
            sWriter.Write(Encoding.UTF8.GetString(ms.ToArray()));
            sWriter.WriteEndTag("svg");
            GetGuid(map);
            if (writer == null) return sb.ToString();
            else return null;
        }

        public string GetChartScript()
        {
            StiHtmlTextWriter tempHtmlWriter = HtmlWriter;
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlWriter = new StiHtmlTextWriter(sw);

            RenderChartScripts(false);

            HtmlWriter.Flush();
            HtmlWriter = tempHtmlWriter;
            sw.Flush();
            sw.Close();

            return sb.ToString();
        }

        public void Clear()
        {
            if ((TableRender != null) && (TableRender.Matrix != null))
            {
                TableRender.Matrix.Clear();
                TableRender.Matrix = null;
            }
            TableRender = null;
            coordX = null;
            coordY = null;
            styles = null;
            chartData.Clear();
        }

        private bool IsComponentHasInteraction(StiComponent comp)
        {
            if (this.RenderWebInteractions && comp.Interaction != null)
            {
                if (comp.Interaction.SortingEnabled && !string.IsNullOrWhiteSpace(comp.Interaction.SortingColumn)) return true;
                if (comp.Interaction.DrillDownEnabled && (!string.IsNullOrEmpty(comp.Interaction.DrillDownPageGuid) || !string.IsNullOrEmpty(comp.Interaction.DrillDownReport))) return true;
                if (comp.Interaction is StiBandInteraction && ((StiBandInteraction)comp.Interaction).CollapsingEnabled) return true;
            }
            return false;
        }
        #endregion

        #region this
        /// <summary>
		/// Exports a document to the HTML.
		/// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="fileName">A name of the file for exporting a rendered report.</param>
        public void ExportHtml(StiReport report, string fileName)
        {
            StiFileUtils.ProcessReadOnly(fileName);
            FileStream stream = new FileStream(fileName, FileMode.Create);
            ExportHtml(report, stream);
            stream.Flush();
            stream.Close();
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        public void ExportHtml(StiReport report, Stream stream)
        {
            ExportHtml(report, stream, 1, ImageFormat.Gif);
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        /// <param name="zoom">Sets zoom of the exported images.</param>
        /// <param name="imageFormat">Specifies a format of the images in the resulted HTML document.</param>
        public void ExportHtml(StiReport report, Stream stream, double zoom, ImageFormat imageFormat)
        {
            ExportHtml(report, stream, zoom, imageFormat, StiPagesRange.All);
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        /// <param name="pageRange">Describes range of pages of the document for the export.</param>
        public void ExportHtml(StiReport report, Stream stream, StiPagesRange pageRange)
        {
            ExportHtml(report, stream, 1, ImageFormat.Gif, pageRange, StiHtmlExportMode.Table, StiHtmlExportQuality.High);
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        /// <param name="zoom">A zoom of the exported document. Default value is 1.</param>
        /// <param name="imageFormat">Specifies a format of the images in the resulted HTML document.</param>
        /// <param name="pageRange">Describes range of pages of the document for the export.</param>
        public void ExportHtml(StiReport report, Stream stream, double zoom, ImageFormat imageFormat, StiPagesRange pageRange)
        {
            ExportHtml(report, stream, zoom, imageFormat, pageRange, StiHtmlExportMode.Table, StiHtmlExportQuality.High);
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        /// <param name="zoom">A zoom of the exported document. Default value is 1.</param>
        /// <param name="imageFormat">Specifies format of the image.</param>
        /// <param name="pageRange">Describes range of pages of the document for the export.</param>
        /// <param name="exportMode">A parameter that sets modes for the HTML export.</param>
        /// <param name="exportQuality">A parameter which specifies a quality of the images in the resulted HTML document.</param>
        public void ExportHtml(StiReport report, Stream stream, double zoom, ImageFormat imageFormat,
            StiPagesRange pageRange, StiHtmlExportMode exportMode, StiHtmlExportQuality exportQuality)
        {
            ExportHtml(report, stream, zoom, imageFormat, pageRange, exportMode, exportQuality, Encoding.UTF8);
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        /// <param name="zoom">A zoom of the exported document. Default value is 1.</param>
        /// <param name="imageFormat">Specifies a format of the images in the resulted HTML document.</param>
        /// <param name="pageRange">Describes range of pages of the document for the export.</param>
        /// <param name="exportMode">A parameter that sets modes for the HTML export.</param>
        /// <param name="exportQuality">A parameter that specifies a quality of images in the resulted HTML document.</param>
        /// <param name="encoding">A parameter that controls the character encoding of the resulted document.</param>
        public void ExportHtml(StiReport report, Stream stream, double zoom, ImageFormat imageFormat,
            StiPagesRange pageRange, StiHtmlExportMode exportMode, StiHtmlExportQuality exportQuality, Encoding encoding)
        {
            FileStream fileStream = stream as FileStream;
            if (fileStream != null) fileName = fileStream.Name;
            StreamWriter streamWriter = new StreamWriter(stream, encoding);
            HtmlWriter = new StiHtmlTextWriter(streamWriter);
            StiPagesCollection pages = pageRange.GetSelectedPages(report.RenderedPages);

            ExportHtml(report, HtmlWriter, zoom, imageFormat, pages, exportMode, exportQuality);

            streamWriter.Flush();
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="writer">A writer that can write a sequential series of characters to the HTML format.</param>
        /// <param name="zoom">A zoom of the exported document. Default value is 1.</param>
        /// <param name="imageFormat">Specifies a format of images in the resulted HTML document.</param>
        /// <param name="pageRange">Describes range of pages of the document for the export.</param>
        public void ExportHtml(StiReport report, StiHtmlTextWriter writer, double zoom, ImageFormat imageFormat,
            StiPagesRange pageRange)
        {
            StiPagesCollection pages = pageRange.GetSelectedPages(report.RenderedPages);

            StiHtmlExportSettings settings = new StiHtmlExportSettings();

            settings.PageRange = pageRange;
            settings.Zoom = zoom;
            settings.ImageFormat = imageFormat;

            ExportHtml(report, writer, settings, pages);
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="writer">A writer that can write a sequential series of characters to the HTML format.</param>
        /// <param name="zoom">A zoom of the exported document. Default value is 1.</param>
        /// <param name="imageFormat">Specifies a format of images in the resulted HTML document.</param>
        /// <param name="pageIndex">An index of the exported page.</param>
        public void ExportHtml(StiReport report, StiHtmlTextWriter writer, double zoom, ImageFormat imageFormat,
            int pageIndex)
        {
            StiPagesCollection pages = new StiPagesCollection(report, report.RenderedPages);
            pages.CacheMode = report.RenderedPages.CacheMode;
            pages.AddV2Internal(report.RenderedPages[pageIndex]);

            StiHtmlExportSettings settings = new StiHtmlExportSettings();

            settings.Zoom = zoom;
            settings.ImageFormat = imageFormat;

            ExportHtml(report, writer, settings, pages);


        }

        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="writer">A writer that can write a sequential series of characters to the HTML format.</param>
        /// <param name="zoom">A zoom of the exported document. Default value is 1.</param>
        /// <param name="imageFormat">Specifies a format of images in the resulted HTML document.</param>
        /// <param name="pageIndex">An index of the exported page.</param>
        /// <param name="exportMode">A parameter that sets modes for the HTML export.</param>
        /// <param name="exportQuality">A parameter that specifies a quality of images in the resulted HTML document.</param>
        public void ExportHtml(StiReport report, StiHtmlTextWriter writer, double zoom, ImageFormat imageFormat,
            int pageIndex, StiHtmlExportMode exportMode, StiHtmlExportQuality exportQuality)
        {
            ExportHtml(report, writer, zoom, imageFormat,
                pageIndex, exportMode, exportQuality, StiHtmlExportBookmarksMode.All, 150);
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="writer">A writer that can write a sequential series of characters to the HTML format.</param>
        /// <param name="zoom">A zoom of the exported document. Default value is 1.</param>
        /// <param name="imageFormat">Specifies a format of images in the resulted HTML document.</param>
        /// <param name="pageIndex">An index of the exported page.</param>
        /// <param name="exportMode">A parameter that sets modes for the HTML export.</param>
        /// <param name="exportQuality">A parameter that specifies a quality of images in the resulted HTML document.</param>
        public void ExportHtml(StiReport report, StiHtmlTextWriter writer, double zoom, ImageFormat imageFormat,
            int pageIndex, StiHtmlExportMode exportMode, StiHtmlExportQuality exportQuality,
            StiHtmlExportBookmarksMode exportBookmarksMode, int bookmarksTreeWidth)
        {
            StiPagesCollection pages = new StiPagesCollection(report, report.RenderedPages);
            pages.CacheMode = report.RenderedPages.CacheMode;
            pages.AddV2Internal(report.RenderedPages[pageIndex]);

            StiHtmlExportSettings settings = new StiHtmlExportSettings();

            settings.Zoom = zoom;
            settings.ImageFormat = imageFormat;
            settings.ExportMode = exportMode;
            settings.ExportQuality = exportQuality;
            settings.ExportBookmarksMode = exportBookmarksMode;
            settings.BookmarksTreeWidth = bookmarksTreeWidth;

            ExportHtml(report, writer, settings, pages);
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="writer">A writer that can write a sequential series of characters to the HTML format.</param>
        /// <param name="zoom">A zoom of the exported document. Default value is 1.</param>
        /// <param name="imageFormat">Specifies a format of the images in the resulted HTML document.</param>
        /// <param name="pages">A collection of pages for the export.</param>
        public void ExportHtml(StiReport report, StiHtmlTextWriter writer, double zoom, ImageFormat imageFormat,
            StiPagesCollection pages)
        {
            StiHtmlExportSettings settings = new StiHtmlExportSettings();

            settings.Zoom = zoom;
            settings.ImageFormat = imageFormat;

            ExportHtml(report, writer, settings, pages);
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="writer">A writer that can write a sequential series of characters to the HTML format.</param>
        /// <param name="zoom">A zoom of the exported document. Default value is 1.</param>
        /// <param name="imageFormat">Specifies a format of images in the resulted HTML document.</param>
        /// <param name="pages">A collection of pages for the export.</param>
        /// <param name="exportMode">A parameter that sets modes for the HTML export.</param>
        /// <param name="exportQuality">A parameter which specifies a quality of images in the resulted HTML document.</param>
        public void ExportHtml(StiReport report, StiHtmlTextWriter writer, double zoom, ImageFormat imageFormat,
            StiPagesCollection pages, StiHtmlExportMode exportMode, StiHtmlExportQuality exportQuality)
        {
            StiHtmlExportSettings settings = new StiHtmlExportSettings();

            settings.Zoom = zoom;
            settings.ImageFormat = imageFormat;
            settings.ExportMode = exportMode;
            settings.ExportQuality = exportQuality;

            ExportHtml(report, writer, settings, pages);
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        /// <param name="report">A rendered report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        public void ExportHtml(StiReport report, Stream stream, StiHtmlExportSettings settings)
        {
            FileStream fileStream = stream as FileStream;
            fileName = string.Empty;
            if (fileStream != null) fileName = fileStream.Name;

            MemoryStream ms = null;
            if (settings.CompressToArchive)
            {
                zip = new StiZipWriter20();
                zip.Begin(stream, true);
                ms = new MemoryStream();
                stream = ms;

                try
                {
                    if (!string.IsNullOrEmpty(fileName)) fileName = Path.GetFileNameWithoutExtension(fileName);
                }
                catch
                {
                }
                if (string.IsNullOrEmpty(fileName)) fileName = report.ReportName;
                if (string.IsNullOrEmpty(fileName)) fileName = "report";
            }

            StreamWriter streamWriter = new StreamWriter(stream, settings.Encoding);
            HtmlWriter = new StiHtmlTextWriter(streamWriter);
            StiPagesCollection pages = settings.PageRange.GetSelectedPages(report.RenderedPages);

            ExportHtml(report, HtmlWriter, settings, pages);

            streamWriter.Flush();

            if (settings.CompressToArchive)
            {
                zip.AddFile(fileName + "." + "html", ms);
                zip.End();
                zip = null;
            }
        }


        /// <summary>
        /// Exports a document to the HTML.
        /// </summary>
        public void ExportHtml(StiReport report, StiHtmlTextWriter writer, StiHtmlExportSettings settings, StiPagesCollection pages)
        {
            StiLogService.Write(this.GetType(), "Export report to Html format");

            this.exportSettings = settings;

            #region Read settings
            if (settings == null)
                throw new ArgumentNullException("The 'settings' argument cannot be equal in null.");

            StiPagesRange pageRange = settings.PageRange;
            this.zoom = settings.Zoom;
            this.imageFormat = settings.ImageFormat;
            this.exportQuality = settings.ExportQuality;
            this.exportMode = settings.ExportMode;
            bool useBookmarks = settings.ExportBookmarksMode != StiHtmlExportBookmarksMode.ReportOnly;
            int bookmarksWidth = settings.BookmarksTreeWidth;
            bool exportBookmarksOnly = settings.ExportBookmarksMode == StiHtmlExportBookmarksMode.BookmarksOnly;
            this.useStylesTable = settings.UseStylesTable;
            this.imageResolution = settings.ImageResolution;
            this.imageQuality = settings.ImageQuality;
            this.removeEmptySpaceAtBottom = settings.RemoveEmptySpaceAtBottom;
            this.pageHorAlignment = settings.PageHorAlignment;
            this.compressToArchive = settings.CompressToArchive;
            this.useEmbeddedImages = settings.UseEmbeddedImages;
            this.openLinksTarget = settings.OpenLinksTarget;
            this.chartType = settings.ChartType;
            #endregion

            bool modifyGradient = true;

            useBookmarks &= (report.Bookmark != null) && (report.Bookmark.Bookmarks.Count != 0);

            if (exportMode == StiHtmlExportMode.Span) strSpanDiv = "span";
            else if (exportMode == StiHtmlExportMode.Div) strSpanDiv = "div";

            isFileStreamMode = !string.IsNullOrEmpty(fileName);
            if (useEmbeddedImages) isFileStreamMode = false;

            currentCulture = Thread.CurrentThread.CurrentCulture;
            try
            {
                if (htmlImageHost == null)
                {
                    htmlImageHost = new StiHtmlImageHost(this);                     // !!!!!
                }

                htmlImageHost.ImageCache = new StiImageCache(StiOptions.Export.Html.AllowImageComparer, imageFormat, imageQuality);

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

                this.HtmlWriter = writer;
                this.report = report;

                hashBookmarkGuid = new Hashtable();
                AssembleGuidUsedInBookmark(report.Bookmark, hashBookmarkGuid);

                totalPageWidth = 0;
                totalPageHeight = 0;
                startPage = 0;
                imageNumber = 1;

                #region Prepare bookmarksPageIndex
                Hashtable bookmarksPageIndex = new Hashtable();
                if (useBookmarks)
                {
                    int tempPageNumber = 0;
                    foreach (StiPage page in pages)
                    {
                        pages.GetPage(page);
                        StiComponentsCollection components = page.GetComponents();
                        components.Add(page);
                        foreach (StiComponent comp in components)
                        {
                            if (comp.Enabled)
                            {
                                string bookmarkValue = comp.BookmarkValue as string;
                                if (!string.IsNullOrWhiteSpace(bookmarkValue))
                                {
                                    bookmarkValue = bookmarkValue.Replace("'", "");
                                    if (bookmarkValue.Length > 0)
                                    {
                                        if (!bookmarksPageIndex.ContainsKey(bookmarkValue)) bookmarksPageIndex.Add(bookmarkValue, tempPageNumber);
                                    }
                                }
                            }
                        }
                        tempPageNumber++;
                    }
                }
                #endregion

                bool useStimulsoftFont = false;
                foreach (StiPage page in pages)
                {
                    pages.GetPage(page);
                    StiComponentsCollection components = page.GetComponents();

                    foreach (StiComponent compp in components)
                    {
                        StiComponent comp = compp;
                        if (comp.Enabled)
                        {
                            var chart = comp as StiChart;
                            if (chart != null && chart.Series.Count > 0 && chart.Series[0] is StiPictorialSeries)
                            {
                                useStimulsoftFont = true;
                            }
                        }
                    }
                }

                #region StiHtmlExportMode.Span || StiHtmlExportMode.Div
                if (exportMode == StiHtmlExportMode.Span || exportMode == StiHtmlExportMode.Div)
                {
                    StiHtmlTableRender tableRender = new StiHtmlTableRender(this, settings, new StiPagesCollection(report, pages));
                    if (IsStopped) return;

                    #region Prepare coordinates
                    coordX = new SortedList();
                    coordY = new SortedList();
                    hyperlinksToTag = new Hashtable();

                    foreach (StiPage page in pages)
                    {
                        totalPageWidth = Math.Max(totalPageWidth, page.Unit.ConvertToHInches(page.Width));
                        totalPageHeight += page.Unit.ConvertToHInches(page.Height);
                    }

                    foreach (StiPage page in pages)
                    {
                        pages.GetPage(page);

                        this.InvokeExporting(page, pages, 0, 2);

                        StiComponentsCollection components = page.GetComponents();
                        RectangleD pageRect = page.Unit.ConvertToHInches(page.ClientRectangle);

                        foreach (StiComponent comp in components)
                        {
                            RectangleD rect = page.Unit.ConvertToHInches(comp.DisplayRectangle);
                            bool needAdd = true;
                            if (comp is StiPointPrimitive) needAdd = false;
                            if ((rect.Right < pageRect.Left) ||
                                (rect.Left > pageRect.Right) ||
                                (rect.Bottom < pageRect.Top) ||
                                (rect.Top > pageRect.Bottom)) needAdd = false;
                            if (comp.Enabled && needAdd)
                            {
                                AddCoord(comp.DisplayRectangle);

                                if (comp.HyperlinkValue != null)
                                {
                                    string hyperlink = comp.HyperlinkValue as string;
                                    if (!string.IsNullOrEmpty(hyperlink) && (hyperlink.Length > 2) && hyperlink.StartsWith("##"))
                                    {
                                        hyperlinksToTag[hyperlink.Substring(2)] = null;
                                    }
                                }
                            }
                        }
                    }
                    FormatCoords(report);
                    #endregion

                    #region Prepare styles
                    Hashtable hashStyles = new Hashtable();
                    Hashtable cssStyles = new Hashtable();


                    foreach (StiPage page in pages)
                    {
                        pages.GetPage(page);
                        StiComponentsCollection components = page.GetComponents();

                        foreach (StiComponent compp in components)
                        {
                            StiComponent comp = compp;
                            if (comp.Enabled)
                            {
                                if (modifyGradient && !ForceExportAsImage(comp))
                                {
                                    StiText txt = comp as StiText;
                                    if ((txt != null) && (txt.Brush != null) && (!(txt.Brush is StiSolidBrush)))
                                    {
                                        comp = (StiComponent)compp.Clone();
                                        (comp as StiText).Brush = new StiSolidBrush(Color.Transparent);
                                    }
                                }

                                StiCellStyle cellStyle = tableRender.Matrix.GetStyleFromComponent(comp, -1, -1);
                                cellStyle.AbsolutePosition = true;
                                if (!pages.CacheMode)
                                {
                                    hashStyles[compp] = cellStyle;
                                }

                                #region scan tag for css-style
                                string sTag = comp.TagValue as string;
                                if (!string.IsNullOrEmpty(sTag))
                                {
                                    string[] sTagArray = StiMatrix.SplitTag(sTag);
                                    for (int index = 0; index < sTagArray.Length; index++)
                                    {
                                        if (sTagArray[index].ToLower().StartsWith("css", StringComparison.InvariantCulture))
                                        {
                                            string[] stArr = StiMatrix.GetStringsFromTag(sTagArray[index], 3);
                                            if (stArr.Length > 1)
                                            {
                                                string styleName = stArr[0].Trim();
                                                cssStyles[styleName] = stArr[1].Trim() + ";position:absolute;";
                                                hashStyles[compp] = styleName;
                                                break;
                                            }
                                        }
                                    }
                                }
                                #endregion
                               
                            }
                        }
                    }
                    tableRender.Matrix.CheckStylesNames();
                    #endregion

                    if (RenderAsDocument) RenderStartDoc(tableRender, false, useBookmarks, exportBookmarksOnly, useStimulsoftFont, cssStyles, pages, settings.Encoding);

                    if (!RenderAsDocument && useBookmarks) RenderBookmarkScript();

                    if (useBookmarks)
                    {
                        HtmlWriter.WriteBeginTag(strSpanDiv + " class=\"dtreeframe\" style=\"");
                        HtmlWriter.WriteStyleAttribute("position", "absolute");
                        if (!exportBookmarksOnly) HtmlWriter.WriteStyleAttribute("height", FormatCoord(totalPageHeight * zoom * .75));

                        RenderBookmarkTree(report.Bookmark, bookmarksWidth, bookmarksPageIndex);

                        HtmlWriter.WriteEndTag(strSpanDiv);
                        HtmlWriter.WriteLine();
                    }

                    if (!exportBookmarksOnly) RenderPage(pages, useBookmarks, bookmarksWidth);

                    if (!RenderAsDocument)
                    {
                        tableRender.RenderStyles(useBookmarks, exportBookmarksOnly, cssStyles);
                    }

                    StatusString = StiLocalization.Get("Export", "ExportingCreatingDocument");

                    if (!exportBookmarksOnly)
                    {
                        foreach (StiPage page in pages)
                        {
                            pages.GetPage(page);

                            this.InvokeExporting(page, pages, 1, 2);
                            if (IsStopped) return;

                            StiComponentsCollection components = page.GetComponents();
                            RectangleD pageRect = page.Unit.ConvertToHInches(page.ClientRectangle);

                            int compIndex = 0;
                            foreach (StiComponent compp in components)
                            {
                                StiComponent comp = compp;

                                RectangleD rect = page.Unit.ConvertToHInches(comp.DisplayRectangle);
                                bool needAdd = true;
                                if (comp is StiPointPrimitive) needAdd = false;
                                if ((rect.Right < pageRect.Left) ||
                                    (rect.Left > pageRect.Right) ||
                                    (rect.Bottom < pageRect.Top) ||
                                    (rect.Top > pageRect.Bottom)) needAdd = false;
                                if (rect.Width == 0 || rect.Height == 0) needAdd = false;

                                if (comp.Enabled && needAdd)
                                {
                                    #region Prepare Class name
                                    string className = null;
                                    StiCellStyle cellStyle = hashStyles[compp] as StiCellStyle;
                                    if (cellStyle == null && pages.CacheMode)
                                    {
                                        cellStyle = tableRender.Matrix.GetStyleFromComponent(compp, -1, -1);
                                        cellStyle.AbsolutePosition = true;
                                    }
                                    int styleIndex = tableRender.Matrix.Styles.IndexOf(cellStyle);
                                    if ((styleIndex != -1) && (useStylesTable))
                                    {
                                        className = "s" + cellStyle.StyleName;
                                    }
                                    string styleName = hashStyles[compp] as string;
                                    if ((!string.IsNullOrEmpty(styleName)) && (useStylesTable))
                                    {
                                        className = styleName;
                                    }
                                    #endregion

                                    if (modifyGradient && !ForceExportAsImage(comp))
                                    {
                                        #region modifyGradient
                                        StiText txt = comp as StiText;
                                        if ((txt != null) && (txt.Brush != null) && (!(txt.Brush is StiSolidBrush)))
                                        {
                                            writer.WriteBeginTag(strSpanDiv);

                                            if (!string.IsNullOrEmpty(className))
                                            {
                                                writer.WriteAttribute("class", className);
                                            }

                                            writer.Write(" style=\"");
                                            writer.Write("");
                                            RenderPosition(comp);
                                            writer.Write("position:absolute;\">");

                                            StiText cont = new StiText();
                                            cont.Page = page;
                                            cont.ClientRectangle = comp.ClientRectangle;
                                            cont.Brush = (comp as StiText).Brush;
                                            RenderImage(cont);

                                            writer.WriteEndTag(strSpanDiv);
                                            writer.WriteLine("");

                                            comp = (StiComponent)compp.Clone();
                                            (comp as StiText).Brush = new StiSolidBrush(Color.Transparent);
                                        }
                                        #endregion
                                    }

                                    #region render component
                                    bool isExportAsImage = comp.IsExportAsImage(StiExportFormat.Html) || ForceExportAsImage(comp);
                                    bool needHyperlink = false;

                                    //if (!isExportAsImage) needHyperlink = RenderHyperlink(comp);
                                    writer.WriteBeginTag(strSpanDiv);

                                    if (!string.IsNullOrEmpty(className))
                                    {
                                        writer.WriteAttribute("class", className);
                                    }

                                    if (exportMode == StiHtmlExportMode.Div)
                                    {
                                        if (comp.ToolTipValue != null)
                                        {
                                            writer.WriteAttribute("title", comp.ToolTipValue.ToString());
                                        }
                                        if ((cellStyle != null) && (cellStyle.TextOptions != null) && (cellStyle.TextOptions.WordWrap == false))
                                        {
                                            writer.Write(" nowrap");
                                        }
                                    }

                                    #region HTML5 Viewer Interactions
                                    if (this.RenderWebInteractions)
                                    {
                                        if (compp.Report != null && this.IsComponentHasInteraction(compp))
                                        {
                                            writer.Write(string.Format(" interaction=\"{0}\"", comp.Name));

                                            #region Sorting
                                            if (comp.Interaction.SortingEnabled)
                                            {
                                                string dataBandName = comp.Interaction.GetSortDataBandName();
                                                StiDataBand dataBand = comp.Report.GetComponentByName(dataBandName) as StiDataBand;
                                                if (dataBand != null)
                                                {
                                                    writer.Write(string.Format(" databandsort=\"{0};{1}\"", dataBandName, string.Join(";", dataBand.Sort)));

                                                    for (int i = 0; i < dataBand.Sort.Length; i += 2)
                                                    {
                                                        if (dataBand.Sort[i + 1] == comp.Interaction.GetSortColumnsString()) writer.Write(string.Format(" sort=\"{0}\"", dataBand.Sort[i].ToLower()));
                                                    }
                                                }
                                            }
                                            #endregion

                                            #region Drill-down
                                            if (comp.Interaction.DrillDownEnabled && (!string.IsNullOrEmpty(comp.Interaction.DrillDownPageGuid) || !string.IsNullOrEmpty(comp.Interaction.DrillDownReport)))
                                            {
                                                if (!string.IsNullOrEmpty(comp.Interaction.DrillDownPageGuid)) writer.Write(string.Format(" pageguid=\"{0}\"", comp.Interaction.DrillDownPageGuid));
                                                if (!string.IsNullOrEmpty(comp.Interaction.DrillDownReport)) writer.Write(string.Format(" reportfile=\"{0}\"", comp.Interaction.DrillDownReport));
                                                writer.Write(string.Format(" pageindex=\"{0}\"", comp.Page.Report.RenderedPages.IndexOf(comp.Page).ToString()));
                                                writer.Write(string.Format(" compindex=\"{0}\"", comp.Page.Components.IndexOf(comp).ToString()));
                                            }
                                            #endregion

                                            #region Collapsing
                                            StiBandInteraction bandInteraction = comp.Interaction as StiBandInteraction;
                                            if (bandInteraction != null && bandInteraction.CollapsingEnabled && comp is StiContainer)
                                            {
                                                StiContainer cont = comp as StiContainer;
                                                writer.Write(string.Format(" collapsed=\"{0}\"", StiDataBandV2Builder.IsCollapsed(cont, false).ToString().ToLower()));
                                                writer.Write(string.Format(" compindex=\"{0}\"", cont.CollapsingIndex.ToString()));
                                            }
                                            #endregion
                                        }
                                        
                                        if (compp.Page != null && compp is IStiEditable && ((IStiEditable)compp).Editable)
                                        {
                                            #region Editable
                                            StringBuilder attr = new StringBuilder();
                                            int editableIndex = compp.Page.Components.IndexOf(compp);
                                            attr.AppendFormat("{0};", editableIndex);

                                            var checkBox = compp as StiCheckBox;
                                            if (checkBox != null)
                                            {
                                                Color backColor = Color.Transparent;
                                                if (checkBox.TextBrush is StiSolidBrush) backColor = ((StiSolidBrush)checkBox.TextBrush).Color;
                                                else if (checkBox.TextBrush is StiGradientBrush) backColor = ((StiGradientBrush)checkBox.TextBrush).StartColor;
                                                else if (checkBox.TextBrush is StiGlareBrush) backColor = ((StiGlareBrush)checkBox.TextBrush).StartColor;
                                                else if (checkBox.TextBrush is StiGlassBrush) backColor = ((StiGlassBrush)checkBox.TextBrush).Color;
                                                else if (checkBox.TextBrush is StiHatchBrush) backColor = ((StiHatchBrush)checkBox.TextBrush).ForeColor;

                                                attr.AppendFormat("CheckBox;{0};{1};{2};#{3:X2}{4:X2}{5:X2};{6};#{7:X2}{8:X2}{9:X2}",
                                                    checkBox.CheckedValue,
                                                    checkBox.CheckStyleForFalse.ToString(),
                                                    checkBox.CheckStyleForTrue.ToString(),
                                                    checkBox.ContourColor.R, checkBox.ContourColor.G, checkBox.ContourColor.B,
                                                    checkBox.Size,
                                                    backColor.R, backColor.G, backColor.B);
                                            }

                                            var textBox = compp as StiText;
                                            if (textBox != null)
                                            {
                                                attr.AppendFormat("Text");
                                            }

                                            var richTextBox = compp as StiRichText;
                                            if (richTextBox != null)
                                            {
                                                attr.AppendFormat("RichText");
                                            }

                                            writer.Write(string.Format(" editable=\"{0}\"", attr.ToString()));
                                            #endregion
                                        }
                                    }
                                    #endregion

                                    writer.Write(" style=\"");

                                    //if (compIndex == components.Count - 1)
                                    //	writer.Write("page-break-after:always;");

                                    writer.Write("");

                                    RenderPosition(comp);

                                    if (!useStylesTable)
                                    {
                                        if (styleIndex != -1)
                                        {
                                            tableRender.RenderStyle(cellStyle);
                                        }
                                        if (!string.IsNullOrEmpty(styleName))
                                        {
                                            writer.WriteLine(cssStyles[styleName] + ";overflow:hidden;");
                                        }
                                    }

                                    var chart = comp as StiChart;
                                    var gauge = comp as StiGauge;
                                    var map = comp as StiMap;
                                    if ((chart != null) && (chartType != StiHtmlChartType.Image))
                                    {
                                        #region Chart
                                        writer.Write("\" ");
                                        writer.WriteAttribute("id", GetGuid(chart));
                                        writer.Write(">");
                                        PrepareChartData(writer, chart, rect.Width, rect.Height);
                                        needHyperlink = RenderHyperlink(comp);

                                        if (needHyperlink) writer.WriteEndTag("a");
                                        #endregion
                                    }
                                    else if (gauge != null)
                                    {
                                        writer.Write("\" ");
                                        writer.WriteAttribute("id", GetGuid(gauge));
                                        writer.Write(">");
                                        PrepareGaugeData(writer, gauge, rect.Width, rect.Height);
                                        needHyperlink = RenderHyperlink(comp);
                                        if (needHyperlink) writer.WriteEndTag("a");
                                    }
                                    else if (map != null)
                                    {
                                        writer.Write("\" ");
                                        writer.WriteAttribute("id", GetGuid(map));
                                        writer.Write(">");
                                        PrepareMapData(writer, map, rect.Width, rect.Height);
                                        needHyperlink = RenderHyperlink(comp);
                                        if (needHyperlink) writer.WriteEndTag("a");
                                    }
                                    else if (isExportAsImage)
                                    {
                                        #region Image
                                        writer.Write("\">");
                                        needHyperlink = RenderHyperlink(comp);
                                        RenderImage(comp);
                                        if (needHyperlink) writer.WriteEndTag("a");
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Text
                                        IStiTextOptions textOptions = comp as IStiTextOptions;
                                        if (textOptions != null) RenderTextDirection(null, textOptions.TextOptions);

                                        bool isText = comp is IStiText && !(comp is StiRichText);
                                        string text = null;
                                        bool needProcessPreserveWhiteSpaces = true;
                                        if (isText)
                                        {
                                            text = ((IStiText)comp).Text;
                                            if (StiOptions.Export.Html.PreserveWhiteSpaces && !string.IsNullOrWhiteSpace(text) && text.Contains("  "))
                                            {
                                                writer.Write("white-space:pre-wrap;");
                                                needProcessPreserveWhiteSpaces = false;
                                            }
                                        }

                                        writer.Write("\">");
                                        needHyperlink = RenderHyperlink(comp);
                                        bool needBr = true;

                                        if (textOptions != null && (!textOptions.TextOptions.WordWrap))
                                        {
                                            if (isText)
                                            {
                                                if (text != null)
                                                {
                                                    StiText stiText = comp as StiText;

                                                    if (stiText != null && stiText.TextQuality == StiTextQuality.Wysiwyg && !string.IsNullOrEmpty(text) && text.EndsWith(StiTextRenderer.StiForceWidthAlignTag))
                                                    {
                                                        text = text.Substring(0, text.Length - StiTextRenderer.StiForceWidthAlignTag.Length);
                                                    }

                                                    if ((stiText != null) && (stiText.CheckAllowHtmlTags()))
                                                    {
                                                        text = ConvertTextWithHtmlTagsToHtmlText(stiText, text);
                                                    }
                                                    else
                                                    {
                                                        if (StiOptions.Export.Html.ReplaceSpecialCharacters)
                                                        {
                                                            text = text.Replace("&", "&amp;").Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\xA0", "&nbsp;");
                                                        }
                                                    }

                                                    if (StiOptions.Export.Html.ConvertDigitsToArabic && textOptions.TextOptions.RightToLeft)
                                                    {
                                                        text = StiExportUtils.ConvertDigitsToArabic(text, StiOptions.Export.Html.ArabicDigitsType);
                                                    }

                                                    writer.Write(PrepareTextForHtml(text, needProcessPreserveWhiteSpaces));
                                                }
                                                needBr = false;
                                            }
                                        }
                                        else
                                        {
                                            if (comp is IStiText && (!(comp is StiRichText)))
                                            {
                                                if (text != null)
                                                {
                                                    StiText stiText = comp as StiText;

                                                    if (stiText != null && stiText.TextQuality == StiTextQuality.Wysiwyg && !string.IsNullOrEmpty(text) && text.EndsWith(StiTextRenderer.StiForceWidthAlignTag))
                                                    {
                                                        text = text.Substring(0, text.Length - StiTextRenderer.StiForceWidthAlignTag.Length);
                                                    }

                                                    if ((stiText != null) && (stiText.CheckAllowHtmlTags()))
                                                    {
                                                        text = ConvertTextWithHtmlTagsToHtmlText(stiText, text);
                                                    }
                                                    else
                                                    {
                                                        if ((stiText != null) &&
                                                            (StiOptions.Export.Html.ForceWysiwygWordwrap) &&
                                                            (!stiText.CheckAllowHtmlTags()) &&
                                                            (stiText.TextQuality == StiTextQuality.Wysiwyg) &&
                                                            (textOptions != null) &&
                                                            (textOptions.TextOptions.WordWrap))
                                                        {
                                                            var newTextLines = StiTextRenderer.GetTextLines(
                                                                report.ReportMeasureGraphics,
                                                                ref text,
                                                                stiText.Font,
                                                                page.Unit.ConvertToHInches(comp.ComponentToPage(comp.ClientRectangle)),
                                                                1,
                                                                true,
                                                                textOptions.TextOptions.RightToLeft,
                                                                StiDpiHelper.GraphicsScale,
                                                                textOptions.TextOptions.Angle,
                                                                textOptions.TextOptions.Trimming,
                                                                stiText.CheckAllowHtmlTags(),
                                                                textOptions.TextOptions);
                                                            string delimiter = "\n";
                                                            var sb = new StringBuilder();
                                                            for (int index = 0; index < newTextLines.Count; index++)
                                                            {
                                                                string st = newTextLines[index];
                                                                sb.Append(st);
                                                                if (index < newTextLines.Count - 1) sb.Append(delimiter);
                                                            }
                                                            text = sb.ToString();
                                                        }

                                                        if (StiOptions.Export.Html.ReplaceSpecialCharacters)
                                                        {
                                                            text = text.Replace("&", "&amp;").Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\xA0", "&nbsp;");
                                                        }
                                                    }

                                                    writer.Write(PrepareTextForHtml(text, needProcessPreserveWhiteSpaces));
                                                }
                                                needBr = false;
                                            }
                                        }

                                        if (needHyperlink)
                                        {
                                            if (needBr) writer.Write("<br>");
                                            writer.WriteEndTag("a");
                                        }
                                        #endregion
                                    }
                                    
                                    writer.WriteEndTag(strSpanDiv);

                                    //if (!isExportAsImage && needHyperlink) writer.WriteEndTag("a");

                                    writer.WriteLine("");
                                    compIndex++;
                                    #endregion
                                }
                            }

                            writer.WriteLine("<!-- end page -->");
                            startPage += (int)page.Unit.ConvertToHInches(page.Height);

                        }
                    }

                    #region Trial
#if SERVER
                    var isTrial = StiVersionX.IsSvr;
#else
				    var key = StiLicenseKeyValidator.GetLicenseKey();
				    var isTrial = !StiLicenseKeyValidator.IsValidInWebViewer(renderWebViewer, key);
				    if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key))isTrial = true;

                    #region IsValidLicenseKey
				    if (!isTrial)
				    {
				        try
				        {
				            using (var rsa = new RSACryptoServiceProvider(512))
				            using (var sha = new SHA1CryptoServiceProvider())
				            {
				                rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
				                isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
				            }
				        }
				        catch (Exception)
				        {
				            isTrial = true;
				        }
				    }
                    #endregion
#endif
                    if (pages.Count > 0 && isTrial)
                    {
                        var rectPage = pages[0].Unit.ConvertToHInches(pages[0].ClientRectangle);
                        var fontSize = (int)(100 * zoom);
                        writer.WriteLine(string.Format("<div style=\"position: absolute; pointer-events: none; filter: alpha(Opacity=30); opacity: 0.3;" +
                            " -moz-opacity: 0.3; -khtml-opacity: 0.3; font-size: {1}px; font-weight: bold; width: {2}; margin-top: {3}; text-align: center;" +
                            " font-family: Arial; color: black; z-index: 9999; -ms-transform: rotate(-45deg); -webkit-transform: rotate(-45deg); transform: rotate(-45deg);\">{4}</div>",
                            strSpanDiv,
                            fontSize,
                            StiHtmlUnit.NewUnit(rectPage.Width * zoom, StiOptions.Export.Html.PrintLayoutOptimization),
                            StiHtmlUnit.NewUnit(rectPage.Height * zoom / 2 - fontSize, StiOptions.Export.Html.PrintLayoutOptimization),
                            "Trial"));
                    }
                    #endregion

                    RenderEndPage();

                    if (RenderAsDocument) RenderChartScripts();

                    if (RenderAsDocument) RenderEndDoc();
                }
                #endregion

                #region StiHtmlExportMode.Table
                if (exportMode == StiHtmlExportMode.Table)
                {
                    CurrentPassNumber = 0;
                    MaximumPassNumber = 3;
                    TableRender = new StiHtmlTableRender(this, settings, pages);
                    if (IsStopped) return;

                    #region check for css-styles
                    Hashtable cssStyles = new Hashtable();
                    bool[,] readyCells = new bool[TableRender.Matrix.CoordY.Count, TableRender.Matrix.CoordX.Count];
                    for (int rowIndex = 1; rowIndex < TableRender.Matrix.CoordY.Count; rowIndex++)
                    {
                        for (int columnIndex = 1; columnIndex < TableRender.Matrix.CoordX.Count; columnIndex++)
                        {
                            if (!readyCells[rowIndex - 1, columnIndex - 1])
                            {
                                StiCell cell = TableRender.Matrix.Cells[rowIndex - 1, columnIndex - 1];
                                if (cell != null)
                                {
                                    #region range
                                    for (int yy = 0; yy <= cell.Height; yy++)
                                    {
                                        for (int xx = 0; xx <= cell.Width; xx++)
                                        {
                                            readyCells[rowIndex - 1 + yy, columnIndex - 1 + xx] = true;
                                        }
                                    }
                                    #endregion

                                    if (cell.Component != null)
                                    {
                                        #region scan tag for css-style
                                        string sTag = cell.Component.TagValue as string;
                                        if (!string.IsNullOrEmpty(sTag))
                                        {
                                            string[] sTagArray = StiMatrix.SplitTag(sTag);
                                            for (int index = 0; index < sTagArray.Length; index++)
                                            {
                                                if (sTagArray[index].ToLower().StartsWith("css", StringComparison.InvariantCulture))
                                                {
                                                    string[] stArr = StiMatrix.GetStringsFromTag(sTagArray[index], 3);
                                                    if (stArr.Length > 1)
                                                    {
                                                        string styleName = stArr[0].Trim();
                                                        cssStyles[styleName] = stArr[1].Trim();
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    if (RenderAsDocument) RenderStartDoc(TableRender, true, useBookmarks, exportBookmarksOnly, useStimulsoftFont, cssStyles, pages, settings.Encoding);

                    if (!RenderAsDocument && useBookmarks) RenderBookmarkScript();

                    if (useBookmarks)
                    {
                        HtmlWriter.WriteBeginTag("table");
                        if (pageHorAlignment != StiHorAlignment.Left)
                        {
                            HtmlWriter.WriteAttribute("width", "100%");
                        }
                        if (StiOptions.Export.Html.UseExtendedStyle)
                        {
                            HtmlWriter.WriteAttribute("class", "sBaseStyleFix");
                        }
                        HtmlWriter.Write(">");

                        if (StiOptions.Export.Html.UseExtendedStyle)
                        {
                            writer.WriteBeginTag("tbody");
                            writer.WriteAttribute("class", "sBaseStyleFix");
                            writer.WriteLine(">");
                        }

                        HtmlWriter.WriteBeginTag("tr");
                        if (StiOptions.Export.Html.UseExtendedStyle)
                        {
                            HtmlWriter.WriteAttribute("class", "sBaseStyleFix");
                        }
                        HtmlWriter.Write(">");
                        HtmlWriter.WriteBeginTag("td class=\"dtreeframe\" style=\"");
                        HtmlWriter.WriteStyleAttribute("vertical-align", "top");

                        RenderBookmarkTree(report.Bookmark, bookmarksWidth, bookmarksPageIndex);

                        HtmlWriter.WriteEndTag("td");
                        HtmlWriter.WriteLine();
                        HtmlWriter.WriteBeginTag("td");
                        if (StiOptions.Export.Html.UseExtendedStyle)
                        {
                            HtmlWriter.WriteAttribute("class", "sBaseStyleFix");
                        }
                        HtmlWriter.WriteLine(">");
                        HtmlWriter.Indent++;
                    }

                    #region Trial
#if CLOUD
                    var isTrial = StiCloudPlan.IsTrialPlan(report != null ? report.ReportGuid : null);
#elif SERVER
                    var isTrial = StiVersionX.IsSvr;
#else
				    var key = StiLicenseKeyValidator.GetLicenseKey();
				    var isTrial = !StiLicenseKeyValidator.IsValidOnNetFramework(key);
				    if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key))isTrial = true;

                    #region IsValidLicenseKey
				    if (!isTrial)
				    {
				        try
				        {
				            using (var rsa = new RSACryptoServiceProvider(512))
				            using (var sha = new SHA1CryptoServiceProvider())
				            {
				                rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
				                isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
				            }
				        }
				        catch (Exception)
				        {
				            isTrial = true;
				        }
				    }
                    #endregion
#endif
                    if (pages.Count > 0 && isTrial)
                    {
                        var rectPage = pages[0].Unit.ConvertToHInches(pages[0].ClientRectangle);
                        var fontSize = (int)(100 * zoom);
                        writer.WriteLine(string.Format("<div style=\"position: absolute; pointer-events: none; filter: alpha(Opacity=30); opacity: 0.3; -moz-opacity: 0.3;" +
                            " -khtml-opacity: 0.3; font-size: {1}px; font-weight: bold; width: {2}; margin-top: {3}; text-align: center; font-family: Arial; color: black;" +
                            " z-index: 9999; -ms-transform: rotate(-45deg); -webkit-transform: rotate(-45deg); transform: rotate(-45deg);\">{4}</div>",
                            strSpanDiv,
                            fontSize,
                            StiHtmlUnit.NewUnit(rectPage.Width * zoom, StiOptions.Export.Html.PrintLayoutOptimization),
                            StiHtmlUnit.NewUnit(rectPage.Height * zoom / 2 - fontSize, StiOptions.Export.Html.PrintLayoutOptimization),
                            "Trial"));
                    }
                    #endregion

                    if (!exportBookmarksOnly) TableRender.RenderTable((!renderAsDocument) && RenderStyles, GetBackgroundImagePath(pages, zoom, settings.UseWatermarkMargins), useBookmarks, exportBookmarksOnly, cssStyles);
                    if (IsStopped) return;

                    if (useBookmarks)
                    {
                        HtmlWriter.Indent--;
                        HtmlWriter.WriteEndTag("td");
                        HtmlWriter.WriteEndTag("tr");
                        HtmlWriter.WriteEndTag("tbody");
                        HtmlWriter.WriteEndTag("table");
                        HtmlWriter.WriteLine();
                    }

                    if (RenderAsDocument) RenderChartScripts();

                    if (RenderAsDocument) RenderEndDoc();
                }
                #endregion

                writer.Flush();
            }
            finally
            {
                StiExportUtils.EnableFontSmoothing(report);
                Thread.CurrentThread.CurrentCulture = currentCulture;

                if (!htmlImageHost.IsMhtExport) htmlImageHost.ImageCache.Clear();

                report = null;
                if (ClearOnFinish) Clear();
                //htmlImageHost = null;
            }
        }


        #endregion

        /// <summary>
        /// Creates an instance of the class for the HTML export.
        /// </summary>
        public StiHtmlExportService()
        {
            numberFormat.NumberDecimalSeparator = ".";
        }
    }
}
