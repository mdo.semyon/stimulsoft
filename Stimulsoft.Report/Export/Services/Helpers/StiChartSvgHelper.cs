﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Drawing;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml;

using Stimulsoft.Report.Chart;
using Stimulsoft.Base.Context.Animation;
using Stimulsoft.Report.Helpers;
using Stimulsoft.Report.Components;

namespace Stimulsoft.Report.Export
{
    public static class StiChartSvgHelper
    {
        #region Methods
        private static void addAnimation(XmlTextWriter writer, String actions, TimeSpan? begin, TimeSpan duration, String number = "")
        {
            String animation = String.Format("{{\"actions\":{0}, \"begin\":{1}, \"duration\":{2}}}", actions, (begin.HasValue ? begin.Value.TotalMilliseconds : 0), duration.TotalMilliseconds);
            writer.WriteAttributeString("_animation" + number, animation);
        }

        public static void WriteChart(XmlTextWriter writer, StiSvgData svgData, float zoom, Boolean needAnimation)
        {
            float dx = 0;
            float dy = 0;
            List<StiGeom> listTransformGeom = new List<StiGeom>();

            var chart = svgData.Component as StiChart;
            Stimulsoft.Base.Context.StiContext context = null;

            using (var img = new Bitmap(1, 1))
            {
                using (var g = Graphics.FromImage(img))
                {
                    var painter = new Stimulsoft.Report.Painters.StiGdiContextPainter(g);
                    context = new StiContext(painter, true, false, false, zoom);

                    bool storeIsAnimation = chart.IsAnimation;
                    chart.IsAnimation = needAnimation;
                    
                    var report = chart.Report;
                    var svgWidth = (float)svgData.Width;
                    var svgHeight = (float)svgData.Height;
                    if (chart.Rotation == StiImageRotation.Rotate90CCW || chart.Rotation == StiImageRotation.Rotate90CW)
                    {
                        svgWidth = (float)svgData.Height;
                        svgHeight = (float)svgData.Width;
                    }
                    var chartGeom = chart.Core.Render(
                        context,
                        new RectangleF(0, 0, svgWidth, svgHeight),
                        true);

                    chartGeom.DrawGeom(context);

                    chart.IsAnimation = storeIsAnimation;

                    double finishTime = 0;
                    var needStaticShadow = true;
                    foreach (StiGeom geom in context.geoms)
                    {
                        StiAnimationGeom ag = geom as StiAnimationGeom;
                        if (ag != null && ag.Animation != null && ag.Animation.Duration.TotalMilliseconds + (ag.Animation.BeginTime.HasValue ? ag.Animation.BeginTime.Value.TotalMilliseconds : 0) > finishTime)
                        {
                            finishTime = ag.Animation.Duration.TotalMilliseconds + (ag.Animation.BeginTime.HasValue ? ag.Animation.BeginTime.Value.TotalMilliseconds : 0);
                        }
                        if (geom is StiShadowAnimationGeom)
                            needStaticShadow = false;
                    }

                    String clip = String.Empty;
                    writer.WriteStartElement("g");
                    var translateX = svgData.X + 0.5;
                    var translateY = svgData.Y + 0.5;
                    var rotation = "";
                    switch (chart.Rotation)
                    {
                        case StiImageRotation.Rotate90CW:
                            rotation = " rotate(90)";
                            translateX += svgHeight;
                            break;
                        case StiImageRotation.Rotate90CCW:
                            rotation = " rotate(-90)";
                            translateY += svgWidth;
                            break;
                        case StiImageRotation.Rotate180:
                            rotation = " rotate(180)";
                            translateY += svgHeight;
                            translateX += svgWidth;
                            break;
                        case StiImageRotation.FlipHorizontal:
                            rotation = " scale(-1, 1)";
                            translateX += svgWidth;
                            break;
                        case StiImageRotation.FlipVertical :
                            rotation = " scale(1, -1)";
                            translateY += svgHeight;
                            break;
                    }
                    
                    writer.WriteAttributeString("transform",string.Format("translate({0},{1}){2}", p(translateX), p(translateY), rotation));

                    foreach (StiGeom geom in context.geoms)
                    {
                        if (geom is StiPushTranslateTransformGeom)
                        {
                            listTransformGeom.Add(geom);

                            writer.WriteStartElement("g");

                            dx += (geom as StiPushTranslateTransformGeom).X;
                            dy += (geom as StiPushTranslateTransformGeom).Y;
                        }

                        if (geom is StiPushRotateTransformGeom)
                        {
                            listTransformGeom.Add(geom);

                            writer.WriteStartElement("g");

                            writer.WriteAttributeString("transform", string.Format("rotate({0})", (geom as StiPushRotateTransformGeom).Angle.ToString().Replace(",", ".")));
                        }

                        if (geom is StiPopTransformGeom)
                        {
                            var lastTransformGeom = listTransformGeom[listTransformGeom.Count - 1];

                            var lastTranslateTransformGeom = lastTransformGeom as StiPushTranslateTransformGeom;

                            if (lastTranslateTransformGeom != null)
                            {
                                dx -= lastTranslateTransformGeom.X;
                                dy -= lastTranslateTransformGeom.Y;
                            }

                            listTransformGeom.Remove(lastTransformGeom);

                            writer.WriteEndElement();
                        }

                        if (geom is StiClusteredBarSeriesAnimationGeom)
                        {
                            var border = geom as StiClusteredBarSeriesAnimationGeom;
                            var rect = RectToRectangleF(border.ColumnRect);
                            StiColumnAnimation columnAnimation = border.Animation as StiColumnAnimation;
                            var rectFrom = columnAnimation != null ? columnAnimation.RectFrom : RectangleF.Empty;

                            var style = new StringBuilder();
                            if (border.Background != null)
                            {
                                style.Append(WriteFillBrush(writer, border.Background, rect, dx, dy));
                            }
                            else
                            {
                                style.Append("fill:none;");
                            }

                            if (CheckPenGeom(border.BorderPen))
                            {
                                var stroke = string.Format("{0}", WriteBorderStroke(writer, border.BorderPen.Brush, rect));
                                style.Append(string.Format("{0};stroke-width:{1};", stroke, border.BorderPen.Thickness));
                            }

                            //border.UpMove = true;
                            String rectWidth = rect.Width.ToString().Replace(",", ".");
                            writer.WriteStartElement("rect");
                            if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                            writer.WriteAttributeString("y", (rect.Y + dy).ToString().Replace(",", "."));
                            writer.WriteAttributeString("height", rect.Height.ToString().Replace(",", "."));
                            writer.WriteAttributeString("shape-rendering", "crispEdges");


                            if (needAnimation)
                            {
                                if (border.Value < 0)
                                {
                                    if (rectFrom.Width == 0)
                                    {
                                        writer.WriteAttributeString("x", p(rect.X + dx));
                                        addAnimation(writer, "[[\"width\", 0, " + rectWidth + ",\"\"]]", border.Animation.BeginTime, border.Animation.Duration);
                                    }
                                    else
                                    {
                                        if (columnAnimation.ValueFrom <= 0)
                                        {
                                            writer.WriteAttributeString("x", p(rectFrom.X + dx));
                                            addAnimation(writer, "[[\"width\", " + p(rectFrom.Width) + ", " + rectWidth + ",\"\"]]", border.Animation.BeginTime, border.Animation.Duration);
                                        }
                                        else
                                        {
                                            writer.WriteAttributeString("x", p(rectFrom.X + dx));
                                            addAnimation(writer, "[[\"width\", " + p(rectFrom.Width) + ", 0, \"\"], [\"x\"," + p(rectFrom.X + dx) + ", " +
                                                                 p(rectFrom.Width + rectFrom.X + dx) + ", \"\"]]", border.Animation.BeginTime,
                                                border.Animation.Duration.Add(-TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)));
                                            addAnimation(writer, "[[\"width\", 0, " + rectWidth + ", \"\"]]", border.Animation.BeginTime.Value.Add(TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)),
                                                border.Animation.Duration.Add(-TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)), "1");
                                        }
                                    }
                                }
                                else
                                {
                                    if (rectFrom.Width == 0)
                                    {
                                        writer.WriteAttributeString("x", p(rect.Width + rect.X + dx));
                                        addAnimation(writer, "[[\"width\", 0, " + rectWidth + ", \"\"], [\"x\"," + p(rect.Width + rect.X + dx) + ", " +
                                                             p(rect.X + dx) + ", \"\"]]", border.Animation.BeginTime, border.Animation.Duration);
                                    }
                                    else
                                    {
                                        writer.WriteAttributeString("x", p(rectFrom.X + dx));
                                        if (columnAnimation.ValueFrom <= 0)
                                        {
                                            addAnimation(writer, "[[\"width\"," + p(rectFrom.Width) + ", 0,\"\"]]", border.Animation.BeginTime,
                                                border.Animation.Duration.Add(-TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)));
                                            addAnimation(writer, "[[\"width\", 0, " + p(rect.Width) + ", \"\"], [\"x\"," + p(rect.Width + rect.X + dx) + ", " +
                                                                 p(rect.X + dx) + ", \"\"]]", border.Animation.BeginTime.Value.Add(TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)),
                                                border.Animation.Duration.Add(-TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)), "1");
                                        }
                                        else
                                        {
                                            addAnimation(writer, "[[\"width\", " + p(rectFrom.Width) + ", " + rectWidth + ", \"\"], [\"x\"," + p(rectFrom.X + dx) + ", " +
                                                                 p(rect.X + dx) + ", \"\"]]", border.Animation.BeginTime, border.Animation.Duration);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                writer.WriteAttributeString("width", rectWidth);
                                writer.WriteAttributeString("x", p(rect.X + dx));
                            }

                            WriteInteracrion(writer, border.Interaction);
                            writer.WriteAttributeString("style", style.ToString());
                            if (!string.IsNullOrEmpty(border.ToolTip))
                            {
                                writer.WriteStartElement("title");
                                writer.WriteValue(border.ToolTip);
                                writer.WriteEndElement();
                            }
                            writer.WriteEndElement();
                        }
                        else if (geom is StiLabelAnimationGeom)
                        {
                            StiLabelAnimationGeom textGeom = geom as StiLabelAnimationGeom;
                            StiLabelAnimation labelAnimation = textGeom.Animation as StiLabelAnimation;
                            StiPieLabelAnimation animationPieLabel = textGeom.Animation as StiPieLabelAnimation;
                            var rect = RectToRectangleF(textGeom.Rectangle);

                            rect = CorrectRectLabel(textGeom.RotationMode.GetValueOrDefault(), rect);

                            var style = new StringBuilder();

                            if (textGeom.LabelBrush != null)
                            {
                                style.Append(WriteFillBrush(writer, textGeom.LabelBrush, rect, dx, dy));
                            }
                            else
                            {
                                style.Append("fill:none;");
                            }

                            if (textGeom.DrawBorder && CheckPenGeom(textGeom.PenBorder))
                            {
                                var stroke = string.Format("{0}", WriteBorderStroke(writer, textGeom.PenBorder.Brush, rect));
                                style.Append(string.Format("{0};stroke-width:{1};", stroke, textGeom.PenBorder.Thickness));
                            }

                            writer.WriteStartElement("rect");
                            if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                            writer.WriteAttributeString("x", labelAnimation != null ? p(labelAnimation.LabelRect.X + dx) : (animationPieLabel != null ? p(animationPieLabel.RectLabelFrom.X + dx) : p(rect.X + dx)));
                            writer.WriteAttributeString("y", labelAnimation != null ? p(labelAnimation.LabelRect.Y + dy) : (animationPieLabel != null ? p(animationPieLabel.RectLabelFrom.Y + dy) : p(rect.Y + dy)));
                            writer.WriteAttributeString("width", rect.Width.ToString().Replace(",", "."));
                            writer.WriteAttributeString("height", rect.Height.ToString().Replace(",", "."));
                            writer.WriteAttributeString("style", style.ToString());
                            writer.WriteAttributeString("shape-rendering", "crispEdges");
                            writer.WriteAttributeString("opacity", labelAnimation != null || animationPieLabel != null ? "1" : "0");
                            writer.WriteAttributeString("fill", "rgba(0,0,0,0)");
                            if (labelAnimation != null)
                            {
                                addAnimation(writer, "[[\"x\", " + p(labelAnimation.LabelRect.X) + ", " + p(rect.X) + ", \"\"]," +
                                                     "[\"y\", " + p(labelAnimation.LabelRect.Y) + ", " + p(rect.Y) + ", \"\"]]",
                                    labelAnimation.BeginTime, labelAnimation.Duration);
                            }
                            else if (animationPieLabel != null)
                            {
                                addAnimation(writer, "[[\"x\", " + p(animationPieLabel.RectLabelFrom.X) + ", " + p(rect.X) + ", \"\"]," +
                                                     "[\"y\", " + p(animationPieLabel.RectLabelFrom.Y) + ", " + p(rect.Y) + ", \"\"]]",
                                    animationPieLabel.BeginTime, animationPieLabel.Duration);
                            }
                            else
                            {
                                addAnimation(writer, "[[\"opacity\", 0, 1, \"\"]]", TimeSpan.FromMilliseconds(finishTime), TimeSpan.FromMilliseconds(500));
                            }

                            writer.WriteEndElement();

                            var font = new Font(StiFontCollection.GetFontFamily(textGeom.Font.FontName), textGeom.Font.FontSize, textGeom.Font.FontStyle, textGeom.Font.Unit, textGeom.Font.GdiCharSet, textGeom.Font.GdiVerticalFont);

                            StringFormat sf = textGeom.StringFormat.IsGeneric ? StringFormat.GenericDefault.Clone() as StringFormat : new StringFormat();
                            sf.Alignment = textGeom.StringFormat.Alignment;
                            sf.FormatFlags = textGeom.StringFormat.FormatFlags;
                            sf.HotkeyPrefix = textGeom.StringFormat.HotkeyPrefix;
                            sf.LineAlignment = textGeom.StringFormat.LineAlignment;
                            sf.Trimming = textGeom.StringFormat.Trimming;

                            style = new StringBuilder();

                            var size = font.Size * 4 / 3;

                            writer.WriteStartElement("text");
                            //if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));

                            writer.WriteAttributeString("transform", string.Format("translate({0}, {1}) rotate({2} 0,0)",
                                labelAnimation != null ? p(labelAnimation.LabelRect.X + dx) : (animationPieLabel != null ? p(animationPieLabel.RectLabelFrom.X + dx) : p(rect.X + dx)),
                                labelAnimation != null ? p(labelAnimation.LabelRect.Y + dy) : (animationPieLabel != null ? p(animationPieLabel.RectLabelFrom.Y + dy) : p(rect.Y + dy)),
                                p(textGeom.Angle)));

                            writer.WriteAttributeString("font-size", size.ToString().Replace(",", "."));
                            writer.WriteAttributeString("font-family", font.FontFamily.Name);
                            writer.WriteAttributeString("dy", "0.9em");
                            Color textColor = StiBrush.ToColor(textGeom.TextBrush as StiBrush);
                            style.Append(string.Format("fill:#{0:X2}{1:X2}{2:X2};", textColor.R, textColor.G, textColor.B));
                            if (textColor.A != 0xFF)
                            {
                                style.Append(string.Format("fill-opacity:{0}", Math.Round(textColor.A / 255f, 3).ToString().Replace(",", ".")));
                            }

                            writer.WriteAttributeString("style", style.ToString());
                            writer.WriteAttributeString("opacity", labelAnimation != null || animationPieLabel != null ? "1" : "0");
                            if (labelAnimation != null)
                            {
                                addAnimation(writer, String.Format("[[\"translate\",\"" + p(labelAnimation.LabelRect.X + dx) + ":" + p(labelAnimation.LabelRect.Y + dy) + "\",\"" +
                                                                   p(rect.X + dx) + ":" + p(rect.Y + dy) + "\",\" rotate({0} 0, 0)\"]]", p(textGeom.Angle)),
                                    labelAnimation.BeginTime, labelAnimation.Duration);
                                if (labelAnimation.ValueFrom != null && labelAnimation.Value != null)
                                {
                                    decimal val = (decimal)labelAnimation.ValueFrom.Value - (decimal)labelAnimation.Value.Value;
                                    int decimals = BitConverter.GetBytes(decimal.GetBits(val)[3])[2];
                                    addAnimation(writer, "[[\"value\", " + p(labelAnimation.ValueFrom.Value) + ", " + p(labelAnimation.Value.Value) + ", \"" + decimals + "\"]]",
                                        labelAnimation.BeginTime, labelAnimation.Duration, "a1");
                                }
                            }
                            else if (animationPieLabel != null)
                            {
                                addAnimation(writer, String.Format("[[\"translate\",\"" + p(animationPieLabel.RectLabelFrom.X + dx) + ":" + p(animationPieLabel.RectLabelFrom.Y + dy) + "\",\"" +
                                                                   p(rect.X + dx) + ":" + p(rect.Y + dy) + "\",\" rotate({0} 0, 0)\"]]", p(textGeom.Angle)),
                                    animationPieLabel.BeginTime, animationPieLabel.Duration);
                                if (animationPieLabel.ValueFrom != null && animationPieLabel.Value != null)
                                {
                                    decimal val = (decimal)animationPieLabel.ValueFrom.Value - (decimal)animationPieLabel.Value.Value;
                                    int decimals = BitConverter.GetBytes(decimal.GetBits(val)[3])[2];
                                    addAnimation(writer, "[[\"value\", " + p(animationPieLabel.ValueFrom.Value) + ", " + p(animationPieLabel.Value.Value) + ", \"" + decimals + "\"]]",
                                        animationPieLabel.BeginTime, animationPieLabel.Duration, "a1");
                                }
                            }
                            else
                            {
                                addAnimation(writer, "[[\"opacity\", 0, 1, \"\"]]", TimeSpan.FromMilliseconds(finishTime), TimeSpan.FromMilliseconds(500));
                            }

                            if (labelAnimation != null && labelAnimation.ValueFrom != null)
                            {
                                writer.WriteValue(p(labelAnimation.ValueFrom.Value));
                            }
                            else if (animationPieLabel != null && animationPieLabel.ValueFrom != null)
                            {
                                writer.WriteValue(p(animationPieLabel.ValueFrom.Value));
                            }
                            else
                            {
                                writer.WriteValue(textGeom.Text);
                            }

                            writer.WriteEndElement();
                        }
                        else if (geom is StiPushClipGeom)
                        {
                            var clipGeom = geom as StiPushClipGeom;
                            var rect = RectToRectangleF(clipGeom.ClipRectangle);
                            String guid = "s" + StiGuidUtils.NewGuid();
                            writer.WriteStartElement("defs");
                            writer.WriteStartElement("clipPath");
                            writer.WriteAttributeString("id", guid);
                            writer.WriteStartElement("rect");
                            writer.WriteAttributeString("x", (rect.X + dx).ToString().Replace(",", "."));
                            writer.WriteAttributeString("y", (rect.Y + dy).ToString().Replace(",", "."));
                            writer.WriteAttributeString("width", rect.Width.ToString().Replace(",", "."));
                            writer.WriteAttributeString("height", rect.Height.ToString().Replace(",", "."));
                            writer.WriteEndElement();
                            writer.WriteEndElement();
                            writer.WriteEndElement();
                            clip = guid;
                        }
                        else if (geom is StiPopClipGeom)
                        {
                            clip = String.Empty;
                        }
                        else if (geom is StiShadowAnimationGeom)
                        {
                            var shadow = geom as StiShadowAnimationGeom;
                            var rect = RectToRectangleF(shadow.Rect);
                            String guid = "s" + StiGuidUtils.NewGuid();
                            writer.WriteStartElement("defs");
                            writer.WriteStartElement("filter");
                            writer.WriteAttributeString("id", guid);
                            writer.WriteAttributeString("x", "0");
                            writer.WriteAttributeString("y", "0");
                            writer.WriteAttributeString("width", "200%");
                            writer.WriteAttributeString("height", "200%");

                            writer.WriteStartElement("feOffset");
                            writer.WriteAttributeString("result", "offOut");
                            writer.WriteAttributeString("in", "SourceGraphic");
                            writer.WriteAttributeString("dx", "1.111111111111111");
                            writer.WriteAttributeString("dy", "1.111111111111111");
                            writer.WriteEndElement();
                            writer.WriteStartElement("feColorMatrix");
                            writer.WriteAttributeString("result", "matrixOut");
                            writer.WriteAttributeString("in", "offOut");
                            writer.WriteAttributeString("type", "matrix");
                            writer.WriteAttributeString("values", "0.58 0 0 0 0 0 0.58 0 0 0 0 0 0.58 0 0 0 0 0 1 0");
                            writer.WriteEndElement();
                            writer.WriteStartElement("feGaussianBlur");
                            writer.WriteAttributeString("result", "blurOut");
                            writer.WriteAttributeString("in", "matrixOut");
                            writer.WriteAttributeString("stdDeviation", "1.111111111111111");
                            writer.WriteEndElement();
                            writer.WriteStartElement("feBlend");
                            writer.WriteAttributeString("mode", "normal");
                            writer.WriteAttributeString("in", "SourceGraphic");
                            writer.WriteAttributeString("in2", "blurOut");
                            writer.WriteEndElement();
                            writer.WriteEndElement();
                            writer.WriteEndElement();
                            writer.WriteStartElement("rect");
                            if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                            writer.WriteAttributeString("x", (rect.X + dx).ToString().Replace(",", "."));
                            writer.WriteAttributeString("y", (rect.Y + dy).ToString().Replace(",", "."));
                            writer.WriteAttributeString("height", rect.Height.ToString().Replace(",", "."));
                            writer.WriteAttributeString("width", rect.Width.ToString().Replace(",", "."));
                            writer.WriteAttributeString("fill", "rgb(150,150,150)");
                            writer.WriteAttributeString("filter", String.Format("url(#{0})", guid));
                            writer.WriteAttributeString("rx", shadow.RadiusX.ToString().Replace(",", "."));
                            writer.WriteAttributeString("ry", shadow.RadiusY.ToString().Replace(",", "."));
                            if (needAnimation)
                            {
                                writer.WriteAttributeString("opacity", "0");
                                addAnimation(writer, "[[\"opacity\", 0, 1, \"\"]]", shadow.Animation.BeginTime, shadow.Animation.Duration);
                            }

                            writer.WriteEndElement();
                        }
                        else if (geom is StiBorderAnimationGeom)
                        {
                            var border = geom as StiBorderAnimationGeom;
                            var rect = RectToRectangleF(border.Rect);
                            var animation = border.Animation as StiOpacityAnimation;

                            var style = new StringBuilder();
                            if (border.Background != null)
                            {
                                style.Append(WriteFillBrush(writer, border.Background, rect, dx, dy));
                            }
                            else
                            {
                                style.Append("fill:none;");
                            }

                            if (CheckPenGeom(border.BorderPen))
                            {
                                var stroke = string.Format("{0}", WriteBorderStroke(writer, border.BorderPen.Brush, rect));
                                style.Append(string.Format("{0};stroke-width:{1};", stroke, border.BorderPen.Thickness));
                            }

                            writer.WriteStartElement("rect");
                            if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                            writer.WriteAttributeString("x", (rect.X + dx).ToString().Replace(",", "."));
                            writer.WriteAttributeString("y", (rect.Y + dy).ToString().Replace(",", "."));
                            writer.WriteAttributeString("width", rect.Width.ToString().Replace(",", "."));
                            writer.WriteAttributeString("height", rect.Height.ToString().Replace(",", "."));
                            writer.WriteAttributeString("style", style.ToString());
                            writer.WriteAttributeString("shape-rendering", "crispEdges");
                            if (animation != null)
                            {
                                writer.WriteAttributeString("opacity", "0");
                                addAnimation(writer, "[[\"opacity\", 0 , 1,\"\"]]", animation.BeginTime, animation.Duration);
                            }
                            WriteInteracrion(writer, border.Interaction);
                            if (!string.IsNullOrEmpty(border.ToolTip))
                            {
                                writer.WriteStartElement("title");
                                writer.WriteValue(border.ToolTip);
                                writer.WriteEndElement();
                            }
                            writer.WriteEndElement();
                        }
                        else if (geom is StiClusteredColumnSeriesAnimationGeom)
                        {
                            var border = geom as StiClusteredColumnSeriesAnimationGeom;
                            var rect = RectToRectangleF(border.ColumnRect);
                            StiColumnAnimation columnAnimation = border.Animation as StiColumnAnimation;
                            var rectFrom = columnAnimation != null ? columnAnimation.RectFrom : RectangleF.Empty;
                            var style = new StringBuilder();
                            if (border.Background != null)
                            {
                                style.Append(WriteFillBrush(writer, border.Background, rect, dx, dy));
                            }
                            else
                            {
                                style.Append("fill:none;");
                            }

                            if (CheckPenGeom(border.BorderPen))
                            {
                                var stroke = string.Format("{0}", WriteBorderStroke(writer, border.BorderPen.Brush, rect));
                                style.Append(string.Format("{0};stroke-width:{1};", stroke, border.BorderPen.Thickness));
                            }

                            //border.UpMove = true;
                            String rectHeight = rect.Height.ToString().Replace(",", ".");
                            writer.WriteStartElement("rect");
                            if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                            writer.WriteAttributeString("shape-rendering", "crispEdges");
                            writer.WriteAttributeString("width", p(rect.Width));
                            writer.WriteAttributeString("x", p(rect.X + dx));
                            if (needAnimation)
                            {
                                /*writer.WriteAttributeString("x", p(rectFrom.X + dx));
                                writer.WriteAttributeString("width", p(rectFrom.Width));
                                writer.WriteAttributeString("height", p(rectFrom.Height));
                                writer.WriteAttributeString("y", p(rectFrom.Y + dy));
                                addAnimation(writer, "[[\"height\", " + p(rectFrom.Height) + ", " + p(rect.Height) + ", \"\"]," + 
                                                     "[\"y\"," + p(rectFrom.Y + dy) + ", " + p(rect.Y + dy) + ", \"\"], " +
                                                     "[\"width\"," + p(rectFrom.Width + dy) + ", " + p(rect.Width + dy) + ", \"\"], " +
                                                     "[\"x\"," + p(rectFrom.X + dy) + ", " + p(rect.X + dy) + ", \"\"]]",
                                                      border.Animation.BeginTime, border.Animation.Duration);*/
                                if (border.Value > 0)
                                {
                                    if (rectFrom.Height == 0)
                                    {
                                        writer.WriteAttributeString("y", p(rect.Height + rect.Y + dy));
                                        addAnimation(writer, "[[\"height\", 0, " + rectHeight + ", \"\"], [\"y\"," + p(rect.Height + rect.Y + dy) + ", " +
                                                             p(rect.Y + dy) + ", \"\"]]", border.Animation.BeginTime, border.Animation.Duration);
                                    }
                                    else
                                    {
                                        writer.WriteAttributeString("y", p(rectFrom.Y + dy));
                                        if (columnAnimation.ValueFrom >= 0)
                                        {
                                            addAnimation(writer, "[[\"height\", " + p(rectFrom.Height) + ", " + rectHeight + ", \"\"], [\"y\"," + p(rectFrom.Y + dy) + ", " +
                                                                 p(rect.Y + dy) + ", \"\"]]", border.Animation.BeginTime, border.Animation.Duration);
                                        }
                                        else
                                        {
                                            addAnimation(writer, "[[\"height\"," + p(rectFrom.Height) + ", 0,\"\"]]", border.Animation.BeginTime,
                                                border.Animation.Duration.Add(-TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)));
                                            addAnimation(writer, "[[\"height\", 0, " + p(rect.Height) + ", \"\"], [\"y\"," + p(rect.Height + rect.Y + dy) + ", " +
                                                                 p(rect.Y + dy) + ", \"\"]]", border.Animation.BeginTime.Value.Add(TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)),
                                                border.Animation.Duration.Add(-TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)), "1");
                                        }
                                    }
                                }
                                else
                                {
                                    if (rectFrom.Height == 0)
                                    {
                                        writer.WriteAttributeString("y", p(rect.Y + dy));
                                        addAnimation(writer, "[[\"height\", 0, " + rectHeight + ",\"\"]]", border.Animation.BeginTime, border.Animation.Duration);
                                    }
                                    else
                                    {
                                        if (columnAnimation.ValueFrom >= 0)
                                        {
                                            writer.WriteAttributeString("y", p(rectFrom.Y + dy));
                                            addAnimation(writer, "[[\"height\", " + p(rectFrom.Height) + ", 0, \"\"], [\"y\"," + p(rectFrom.Y + dy) + ", " +
                                                                 p(rectFrom.Height + rectFrom.Y + dy) + ", \"\"]]", border.Animation.BeginTime,
                                                border.Animation.Duration.Add(-TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)));
                                            addAnimation(writer, "[[\"height\", 0, " + rectHeight + ", \"\"]]", border.Animation.BeginTime.Value.Add(TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)),
                                                border.Animation.Duration.Add(-TimeSpan.FromMilliseconds(border.Animation.Duration.TotalMilliseconds / 2)), "1");
                                        }
                                        else
                                        {
                                            writer.WriteAttributeString("y", p(rectFrom.Y + dy));
                                            addAnimation(writer, "[[\"height\", " + p(rectFrom.Height) + ", " + rectHeight + ",\"\"]]", border.Animation.BeginTime, border.Animation.Duration);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                writer.WriteAttributeString("height", p(rect.Height));
                                writer.WriteAttributeString("y", p(rect.Y + dy));
                            }

                            WriteInteracrion(writer, border.Interaction);

                            writer.WriteAttributeString("style", style.ToString());

                            if (!string.IsNullOrEmpty(border.ToolTip))
                            {
                                writer.WriteStartElement("title");
                                writer.WriteValue(border.ToolTip);
                                writer.WriteEndElement();
                            }

                            writer.WriteEndElement();
                        }
                        else if (geom is StiLinesAnimationGeom)
                        {
                            var lines = geom as StiLinesAnimationGeom;
                            if (CheckPenGeom(lines.Pen))
                            {
                                String guid = "g" + StiGuidUtils.NewGuid();
                                if (lines.Animation.Type == StiAnimationType.Translation)
                                {
                                    Color color = (Color)lines.Pen.Brush;
                                    writer.WriteStartElement("g");
                                    writer.WriteStartElement("defs");
                                    writer.WriteStartElement("linearGradient");
                                    writer.WriteAttributeString("id", guid);
                                    writer.WriteAttributeString("x1", "0%");
                                    writer.WriteAttributeString("y1", "0%");
                                    writer.WriteAttributeString("x2", "100%");
                                    writer.WriteAttributeString("y2", "0%");
                                    writer.WriteStartElement("stop");
                                    writer.WriteAttributeString("offset", "0%");
                                    writer.WriteAttributeString("stop-color", string.Format("rgba({0},{1},{2},{3})", color.R, color.G, color.B,
                                        Math.Round(color.A / 255f, 3).ToString().Replace(",", ".")));
                                    writer.WriteAttributeString("stop-opacity", "1");
                                    writer.WriteAttributeString("style", "x: 0px;");
                                    addAnimation(writer, "[[\"offset\", 0 , 100,\"%\"]]", lines.Animation.BeginTime, lines.Animation.Duration);
                                    writer.WriteEndElement();
                                    writer.WriteStartElement("stop");
                                    writer.WriteAttributeString("offset", "0%");
                                    writer.WriteAttributeString("stop-color", "transparent");
                                    writer.WriteAttributeString("stop-opacity", "0");
                                    writer.WriteAttributeString("style", "x: 00px;");
                                    writer.WriteEndElement();

                                    writer.WriteEndElement();
                                    writer.WriteEndElement();
                                    writer.WriteEndElement();
                                }

                                var animationPoints = lines.Animation as StiPointsAnimation;

                                writer.WriteStartElement("polyline");
                                if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                                var sb = new StringBuilder();
                                var pointsTo = new StringBuilder();

                                for (var indexPoint = 0; lines.Points.Length > indexPoint; indexPoint++)
                                {
                                    var point = lines.Points[indexPoint];
                                    float deltaFix = 0;
                                    if (indexPoint == lines.Points.Length - 1)
                                        deltaFix = 0.0001f * indexPoint;
                                    //fix: animation if line parallels XAxis
                                    if (animationPoints == null)
                                    {
                                        sb.AppendFormat("{0},{1} ", p(point.X + dx), p(point.Y + dy + deltaFix));
                                    }
                                    else
                                    {
                                        sb.AppendFormat("{0},{1} ", p(animationPoints.PointsFrom[indexPoint].X + dx), p(animationPoints.PointsFrom[indexPoint].Y + dy + deltaFix));
                                        pointsTo.AppendFormat("{0},{1} ", p(point.X + dx), p(point.Y + dy + deltaFix));
                                    }
                                }

                                writer.WriteAttributeString("fill", "none");
                                writer.WriteAttributeString("points", sb.ToString());
                                if (lines.Animation.Type == StiAnimationType.Opacity || animationPoints != null)
                                {
                                    var stroke = string.Format("{0}", WriteBorderStroke(writer, lines.Pen.Brush, new RectangleF()));
                                    var style = new StringBuilder();
                                    style.AppendFormat("{0};stroke-width:{1};", stroke, p(lines.Pen.Thickness));
                                    writer.WriteAttributeString("style", style.ToString());
                                    if (animationPoints == null)
                                    {
                                        writer.WriteAttributeString("opacity", "0");
                                        addAnimation(writer, "[[\"opacity\", 0 , 1,\"\"]]", lines.Animation.BeginTime, lines.Animation.Duration);
                                    }
                                    else
                                    {
                                        addAnimation(writer, "[[\"points\", \"" + sb.ToString() + "\", \"" + pointsTo.ToString() + "\",\"\"]]", lines.Animation.BeginTime, lines.Animation.Duration);
                                    }
                                }
                                else if (lines.Animation.Type == StiAnimationType.Translation)
                                {
                                    writer.WriteAttributeString("stroke-width", lines.Pen.Thickness.ToString().Replace(",", "."));
                                    writer.WriteAttributeString("stroke", String.Format("url(#{0})", guid));
                                }
                                if (lines.Pen.PenStyle != StiPenStyle.Solid)
                                {
                                    writer.WriteAttributeString("stroke-dasharray", StiSvgHelper.GetLineStyleDash(lines.Pen.PenStyle, lines.Pen.Thickness));
                                }

                                writer.WriteEndElement();
                            }
                        }
                        else if (geom is StiEllipseAnimationGeom)
                        {
                            #region Draw Ellipse
                            var ellipse = geom as StiEllipseAnimationGeom;

                            var rect = RectToRectangleF(ellipse.Rect);

                            var style = string.Empty;
                            var fill = string.Empty;

                            if (ellipse.Background != null)
                            {
                                style = WriteFillBrush(writer, ellipse.Background, rect, dx, dy);
                            }
                            else
                            {
                                style = "fill:none;";
                            }

                            if (CheckPenGeom(ellipse.BorderPen))
                            {
                                var stroke = string.Format("{0}", WriteBorderStroke(writer, ellipse.BorderPen.Brush, rect));
                                style += string.Format("{0};stroke-width:{1};", stroke, ellipse.BorderPen.Thickness.ToString().Replace(",", "."));
                            }

                            writer.WriteStartElement("ellipse");
                            //if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                            writer.WriteAttributeString("rx", (rect.Width / 2f).ToString().Replace(",", "."));
                            writer.WriteAttributeString("ry", (rect.Height / 2f).ToString().Replace(",", "."));
                            if (ellipse.Animation.Type != StiAnimationType.Scale)
                            {
                                writer.WriteAttributeString("cx", (rect.X + dx + rect.Width / 2f).ToString().Replace(",", "."));
                                writer.WriteAttributeString("cy", (rect.Y + dy + rect.Height / 2f).ToString().Replace(",", "."));
                            }

                            writer.WriteAttributeString("style", style);

                            if (ellipse.Animation.Type == StiAnimationType.Opacity)
                            {
                                writer.WriteAttributeString("opacity", "0");
                                addAnimation(writer, "[[\"opacity\", 0 , 1,\"\"]]", ellipse.Animation.BeginTime, ellipse.Animation.Duration);
                            }
                            else if (ellipse.Animation.Type == StiAnimationType.Scale)
                            {
                                writer.WriteAttributeString("transform", "scale(0)");
                                addAnimation(writer, String.Format("[[\"transform\", 0 , 1,\")\",\"translate({0},{1}) scale(\"]]",
                                    (rect.X + dx + rect.Width / 2f).ToString().Replace(",", "."),
                                    (rect.Y + dy + rect.Height / 2f).ToString().Replace(",", ".")), ellipse.Animation.BeginTime, ellipse.Animation.Duration);
                            }

                            WriteInteracrion(writer, ellipse.Interaction);

                            if (!string.IsNullOrEmpty(ellipse.ToolTip))
                            {
                                writer.WriteStartElement("title");
                                writer.WriteValue(ellipse.ToolTip);
                                writer.WriteEndElement();
                            }

                            writer.WriteEndElement();
                            #endregion
                        }
                        else if (geom is StiPathElementAnimationGeom)
                        {
                            #region Draw path
                            var path = geom as StiPathElementAnimationGeom;

                            var rect = RectToRectangleF(path.Rect);

                            var style = string.Empty;
                            var fill = string.Empty;

                            if (path.Background != null)
                            {
                                style = WriteFillBrush(writer, path.Background, rect, dx, dy);
                            }
                            else
                            {
                                style = "fill:none;";
                            }

                            if (CheckPenGeom(path.BorderPen))
                            {
                                var stroke = string.Format("{0}", WriteBorderStroke(writer, path.BorderPen.Brush, rect));
                                style += string.Format("{0};stroke-width:{1};", stroke, path.BorderPen.Thickness.ToString().Replace(",", "."));
                            }

                            StringBuilder animatedPath;
                            TimeSpan? duration;
                            var pathData = GetPathData(path.PathGeoms, dx, dy, out animatedPath, out duration);

                            writer.WriteStartElement("path");
                            if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                            writer.WriteAttributeString("d", pathData);
                            writer.WriteAttributeString("style", style);
                            if (animatedPath.Length == 0)
                            {
                                if (path.Animation != null)
                                {
                                    writer.WriteAttributeString("opacity", "0");
                                    addAnimation(writer, "[[\"opacity\", 0 , 1,\"\"]]", path.Animation.BeginTime, path.Animation.Duration);
                                }
                            }
                            else
                            {
                                if (path.PathGeoms.Count == 1 && path.PathGeoms[0] is StiPieSegmentGeom)
                                {
                                    addAnimation(writer, "[[\"pie\", \"" + animatedPath + "\", 1,\"\"]]", TimeSpan.FromMilliseconds(0), duration.Value);
                                }
                                else
                                {
                                    addAnimation(writer, "[[\"path\", \"" + animatedPath + "\", 1,\"\"]]", TimeSpan.FromMilliseconds(0), duration.Value);
                                }
                            }

                            WriteInteracrion(writer, path.Interaction);

                            if (!string.IsNullOrEmpty(path.ToolTip))
                            {
                                writer.WriteStartElement("title");
                                writer.WriteValue(path.ToolTip);
                                writer.WriteEndElement();
                            }

                            writer.WriteEndElement();
                            #endregion
                        }
                        else if (geom is StiPathAnimationGeom)
                        {
                            #region Draw path
                            var path = geom as StiPathAnimationGeom;

                            var rect = RectToRectangleF(path.Rect);

                            var style = string.Empty;
                            var fill = string.Empty;

                            if (path.Background != null)
                            {
                                style = WriteFillBrush(writer, path.Background, rect, dx, dy);
                            }
                            else
                            {
                                style = "fill:none;";
                            }

                            if (CheckPenGeom(path.Pen))
                            {
                                var stroke = string.Format("{0}", WriteBorderStroke(writer, path.Pen.Brush, rect));
                                style += string.Format("{0};stroke-width:{1};", stroke, path.Pen.Thickness.ToString().Replace(",", "."));
                            }

                            StringBuilder animatedPath;
                            TimeSpan? duration;
                            var pathData = GetPathData(path.Geoms, dx, dy, out animatedPath, out duration);

                            writer.WriteStartElement("path");
                            if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                            writer.WriteAttributeString("d", pathData);
                            writer.WriteAttributeString("style", style);
                            if (path.Animation != null)
                            {
                                writer.WriteAttributeString("opacity", "0");
                                addAnimation(writer, "[[\"opacity\", 0 , 1,\"\"]]", path.Animation.BeginTime, path.Animation.Duration);
                            }
                            else if (animatedPath.Length != 0)
                            {
                                addAnimation(writer, "[[\"path\", \"" + animatedPath + "\", 1,\"\"]]", TimeSpan.FromMilliseconds(0), duration.Value);
                            }

                            ;

                            writer.WriteEndElement();
                            #endregion
                        }
                        else if (geom is StiCurveAnimationGeom)
                        {
                            var curve = geom as StiCurveAnimationGeom;
                            var animationPoints = curve.Animation as StiPointsAnimation;
                            if (CheckPenGeom(curve.Pen))
                            {
                                String guid = "g" + StiGuidUtils.NewGuid();
                                Color color = (Color)curve.Pen.Brush;
                                writer.WriteStartElement("g");
                                writer.WriteStartElement("defs");
                                writer.WriteStartElement("linearGradient");
                                writer.WriteAttributeString("id", guid);
                                writer.WriteAttributeString("x1", "0%");
                                writer.WriteAttributeString("y1", "0%");
                                writer.WriteAttributeString("x2", "100%");
                                writer.WriteAttributeString("y2", "0%");
                                writer.WriteStartElement("stop");
                                writer.WriteAttributeString("offset", "0%");
                                writer.WriteAttributeString("stop-color", string.Format("rgba({0},{1},{2},{3})", color.R, color.G, color.B,
                                    Math.Round(color.A / 255f, 3).ToString().Replace(",", ".")));
                                writer.WriteAttributeString("stop-opacity", "1");
                                writer.WriteAttributeString("style", "x: 0px;");
                                if (animationPoints == null)
                                {
                                    addAnimation(writer, "[[\"offset\", 0 , 100,\"%\"]]", curve.Animation.BeginTime, curve.Animation.Duration);
                                }

                                writer.WriteEndElement();
                                if (animationPoints == null)
                                {
                                    writer.WriteStartElement("stop");
                                    writer.WriteAttributeString("offset", "0%");
                                    writer.WriteAttributeString("stop-color", "transparent");
                                    writer.WriteAttributeString("stop-opacity", "0");
                                    writer.WriteAttributeString("style", "x: 0px;");
                                    writer.WriteEndElement();
                                }

                                writer.WriteEndElement();
                                writer.WriteEndElement();
                                writer.WriteEndElement();

                                writer.WriteStartElement("path");
                                if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                                var pts = ConvertSplineToCubicBezier(curve.Points, curve.Tension);
                                if (animationPoints != null)
                                {
                                    StringBuilder animatedPath = new StringBuilder();
                                    var fromPts = ConvertSplineToCubicBezier(animationPoints.PointsFrom, curve.Tension);
                                    animatedPath.AppendFormat("M{0}:{1},{2}:{3} C", p(fromPts[0].X + dx), p(pts[0].X + dx), p(fromPts[0].Y + dy), p(pts[0].Y + dy));
                                    for (int index = 1; index < pts.Length; index++)
                                    {
                                        animatedPath.AppendFormat("{0}:{1},{2}:{3} ", p(fromPts[index].X + dx), p(pts[index].X + dx), p(fromPts[index].Y + dy), p(pts[index].Y + dy + index * 0.0001));
                                    }

                                    addAnimation(writer, "[[\"path\", \"" + animatedPath + "\", 1,\"\"]]", TimeSpan.FromMilliseconds(0), TimeSpan.FromSeconds(1));

                                    StringBuilder stSpline = new StringBuilder();
                                    stSpline.AppendFormat("M{0},{1} C", (fromPts[0].X + dx).ToString().Replace(",", "."), (fromPts[0].Y + dy).ToString().Replace(",", "."));
                                    for (int index = 1; index < fromPts.Length; index++)
                                    {
                                        stSpline.AppendFormat("{0},{1} ", (fromPts[index].X + dx).ToString().Replace(",", "."), (fromPts[index].Y + dy + index * 0.0001).ToString().Replace(",", "."));
                                    }

                                    writer.WriteAttributeString("d", stSpline.ToString());
                                }
                                else
                                {
                                    StringBuilder stSpline = new StringBuilder();
                                    stSpline.AppendFormat("M{0},{1} C", (pts[0].X + dx).ToString().Replace(",", "."), (pts[0].Y + dy).ToString().Replace(",", "."));
                                    for (int index = 1; index < pts.Length; index++)
                                    {
                                        stSpline.AppendFormat("{0},{1} ", (pts[index].X + dx).ToString().Replace(",", "."), (pts[index].Y + dy + index * 0.0001).ToString().Replace(",", "."));
                                    }

                                    writer.WriteAttributeString("d", stSpline.ToString());
                                }

                                writer.WriteAttributeString("fill", "none");
                                writer.WriteAttributeString("stroke-width", curve.Pen.Thickness.ToString().Replace(",", "."));
                                writer.WriteAttributeString("stroke", String.Format("url(#{0})", guid));
                                if (curve.Pen.PenStyle != StiPenStyle.Solid)
                                {
                                    writer.WriteAttributeString("stroke-dasharray", StiSvgHelper.GetLineStyleDash(curve.Pen.PenStyle, curve.Pen.Thickness));
                                }

                                writer.WriteEndElement();
                            }
                        }


                        if (geom is StiBorderGeom)
                        {
                            #region Draw Border
                            var border = geom as StiBorderGeom;

                            var rect = RectToRectangleF(border.Rect);

                            var style = new StringBuilder();

                            if (border.Background != null)
                            {
                                style.Append(WriteFillBrush(writer, border.Background, rect, dx, dy));
                            }
                            else
                            {
                                style.Append("fill:none;");
                            }

                            if (CheckPenGeom(border.BorderPen))
                            {
                                var stroke = string.Format("{0}", WriteBorderStroke(writer, border.BorderPen.Brush, rect));
                                style.Append(string.Format("{0};stroke-width:{1};", stroke, border.BorderPen.Thickness));
                            }

                            writer.WriteStartElement("rect");
                            if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                            writer.WriteAttributeString("x", (rect.X + dx).ToString().Replace(",", "."));
                            writer.WriteAttributeString("y", (rect.Y + dy).ToString().Replace(",", "."));
                            writer.WriteAttributeString("width", rect.Width.ToString().Replace(",", "."));
                            writer.WriteAttributeString("height", rect.Height.ToString().Replace(",", "."));
                            writer.WriteAttributeString("style", style.ToString());
                            writer.WriteAttributeString("shape-rendering", "crispEdges");
                            WriteInteracrion(writer, border.Interaction);

                            writer.WriteEndElement();
                            #endregion
                        }

                        if (geom is StiLineGeom)
                        {
                            #region Draw Line
                            var line = geom as StiLineGeom;
                            if (CheckPenGeom(line.Pen))
                            {
                                writer.WriteStartElement("line");
                                if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));

                                writer.WriteAttributeString("x1", (line.X1 + dx).ToString().Replace(",", "."));
                                writer.WriteAttributeString("y1", (line.Y1 + dy).ToString().Replace(",", "."));
                                writer.WriteAttributeString("x2", (line.X2 + dx).ToString().Replace(",", "."));
                                writer.WriteAttributeString("y2", (line.Y2 + dy).ToString().Replace(",", "."));

                                var stroke = string.Format("{0}", WriteBorderStroke(writer, line.Pen.Brush, new RectangleF()));
                                var style = string.Format("{0};stroke-width:{1};", stroke, line.Pen.Thickness.ToString().Replace(",", "."));

                                writer.WriteAttributeString("style", style);

                                if (line.Pen.PenStyle != StiPenStyle.Solid)
                                {
                                    writer.WriteAttributeString("stroke-dasharray", StiSvgHelper.GetLineStyleDash(line.Pen.PenStyle, line.Pen.Thickness));
                                }

                                writer.WriteEndElement();
                            }
                            #endregion
                        }

                        if (geom is StiLinesGeom)
                        {
                            #region Draw Lines
                            var lines = geom as StiLinesGeom;
                            if (CheckPenGeom(lines.Pen))
                            {
                                writer.WriteStartElement("polyline");
                                if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));

                                var points = string.Empty;

                                var sb = new StringBuilder();

                                for (int i = 0; i < lines.Points.Length; i++)
                                {
                                    var point = lines.Points[i];
                                    sb.AppendFormat("{0},{1} ", p(point.X + dx), p(point.Y + dy + i * 0.0001));
                                }

                                writer.WriteAttributeString("fill", "none");

                                writer.WriteAttributeString("points", sb.ToString());

                                var stroke = string.Format("{0}", WriteBorderStroke(writer, lines.Pen.Brush, new RectangleF()));

                                var style = new StringBuilder();
                                style.AppendFormat("{0};stroke-width:{1};", stroke, lines.Pen.Thickness.ToString().Replace(",", "."));

                                writer.WriteAttributeString("style", style.ToString());

                                if (lines.Pen.PenStyle != StiPenStyle.Solid)
                                {
                                    writer.WriteAttributeString("stroke-dasharray", StiSvgHelper.GetLineStyleDash(lines.Pen.PenStyle, lines.Pen.Thickness));
                                }

                                writer.WriteEndElement();
                            }
                            #endregion
                        }

                        if (geom is StiCurveGeom)
                        {
                            #region Draw Curve
                            var curve = geom as StiCurveGeom;
                            if (CheckPenGeom(curve.Pen))
                            {
                                writer.WriteStartElement("path");
                                if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));

                                var pts = ConvertSplineToCubicBezier(curve.Points, curve.Tension);

                                StringBuilder stSpline = new StringBuilder();
                                stSpline.AppendFormat("M{0},{1} C", (pts[0].X + dx).ToString().Replace(",", "."), (pts[0].Y + dy).ToString().Replace(",", "."));

                                for (int index = 1; index < pts.Length; index++)
                                {
                                    stSpline.AppendFormat("{0},{1} ", (pts[index].X + dx).ToString().Replace(",", "."), (pts[index].Y + dy + index * 0.0001).ToString().Replace(",", "."));
                                }

                                writer.WriteAttributeString("d", stSpline.ToString());

                                writer.WriteAttributeString("fill", "none");

                                var stroke = string.Format("{0}", WriteBorderStroke(writer, curve.Pen.Brush, new RectangleF()));
                                var style = string.Format("{0};stroke-width:{1};", stroke, curve.Pen.Thickness.ToString().Replace(",", "."));

                                writer.WriteAttributeString("style", style);

                                if (curve.Pen.PenStyle != StiPenStyle.Solid)
                                {
                                    writer.WriteAttributeString("stroke-dasharray", StiSvgHelper.GetLineStyleDash(curve.Pen.PenStyle, curve.Pen.Thickness));
                                }

                                writer.WriteEndElement();
                            }
                            #endregion
                        }

                        if (geom is StiEllipseGeom)
                        {
                            #region Draw Ellipse
                            var ellipse = geom as StiEllipseGeom;

                            var rect = RectToRectangleF(ellipse.Rect);

                            var style = string.Empty;
                            var fill = string.Empty;

                            if (ellipse.Background != null)
                            {
                                style = WriteFillBrush(writer, ellipse.Background, rect, dx, dy);
                            }
                            else
                            {
                                style = "fill:none;";
                            }

                            if (CheckPenGeom(ellipse.BorderPen))
                            {
                                var stroke = string.Format("{0}", WriteBorderStroke(writer, ellipse.BorderPen.Brush, rect));
                                style += string.Format("{0};stroke-width:{1};", stroke, ellipse.BorderPen.Thickness.ToString().Replace(",", "."));
                            }

                            writer.WriteStartElement("ellipse");
                            if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));

                            writer.WriteAttributeString("cx", (rect.X + dx + rect.Width / 2f).ToString().Replace(",", "."));
                            writer.WriteAttributeString("cy", (rect.Y + dy + rect.Height / 2f).ToString().Replace(",", "."));
                            writer.WriteAttributeString("rx", (rect.Width / 2f).ToString().Replace(",", "."));
                            writer.WriteAttributeString("ry", (rect.Height / 2f).ToString().Replace(",", "."));
                            writer.WriteAttributeString("style", style);
                            WriteInteracrion(writer, ellipse.Interaction);

                            writer.WriteEndElement();
                            #endregion
                        }

                        if (geom is StiCachedShadowGeom && needStaticShadow)
                        {
                            var shadow = geom as StiCachedShadowGeom;
                            var rect = shadow.Rect;
                            rect.X += 2;
                            rect.Y += 2;
                            var guid = "s" + StiGuidUtils.NewGuid();
                            writer.WriteStartElement("defs");
                            writer.WriteStartElement("filter");
                            writer.WriteAttributeString("id", guid);
                            writer.WriteStartElement("feGaussianBlur");
                            writer.WriteAttributeString("in", "SourceGraphic");
                            writer.WriteAttributeString("stdDeviation", "2");
                            writer.WriteEndElement();
                            writer.WriteEndElement();
                            writer.WriteEndElement();
                            writer.WriteStartElement("rect");
                            writer.WriteAttributeString("x", (rect.X + dx).ToString().Replace(",", "."));
                            writer.WriteAttributeString("y", (rect.Y + dy).ToString().Replace(",", "."));
                            writer.WriteAttributeString("width", rect.Width.ToString().Replace(",", "."));
                            writer.WriteAttributeString("height", rect.Height.ToString().Replace(",", "."));
                            writer.WriteAttributeString("fill", "rgba(190,190,190,10)");
                            writer.WriteAttributeString("filter", "url(#" + guid + ")");
                            writer.WriteEndElement();
                        }

                        if (geom is StiShadowGeom)
                        {
                            var shadow = geom as StiShadowGeom;

                            String guid = "s" + StiGuidUtils.NewGuid();
                            writer.WriteStartElement("defs");
                            writer.WriteStartElement("filter");
                            writer.WriteAttributeString("id", guid);
                            writer.WriteAttributeString("x", "0");
                            writer.WriteAttributeString("y", "0");
                            writer.WriteAttributeString("width", "200%");
                            writer.WriteAttributeString("height", "200%");

                            writer.WriteStartElement("feOffset");
                            writer.WriteAttributeString("result", "offOut");
                            writer.WriteAttributeString("in", "SourceGraphic");
                            writer.WriteAttributeString("dx", "1.111111111111111");
                            writer.WriteAttributeString("dy", "1.111111111111111");
                            writer.WriteEndElement();
                            writer.WriteStartElement("feColorMatrix");
                            writer.WriteAttributeString("result", "matrixOut");
                            writer.WriteAttributeString("in", "offOut");
                            writer.WriteAttributeString("type", "matrix");
                            writer.WriteAttributeString("values", "0.58 0 0 0 0 0 0.58 0 0 0 0 0 0.58 0 0 0 0 0 1 0");
                            writer.WriteEndElement();
                            writer.WriteStartElement("feGaussianBlur");
                            writer.WriteAttributeString("result", "blurOut");
                            writer.WriteAttributeString("in", "matrixOut");
                            writer.WriteAttributeString("stdDeviation", "1.111111111111111");
                            writer.WriteEndElement();
                            writer.WriteStartElement("feBlend");
                            writer.WriteAttributeString("mode", "normal");
                            writer.WriteAttributeString("in", "SourceGraphic");
                            writer.WriteAttributeString("in2", "blurOut");
                            writer.WriteEndElement();
                            writer.WriteEndElement();
                            writer.WriteEndElement();

                            foreach (StiGeom sgeom in shadow.ShadowContext.geoms)
                            {
                                StringBuilder animatedPath;
                                TimeSpan? duration;
                                var pathGeom = sgeom as StiPathGeom;
                                if (pathGeom != null)
                                {
                                    var pathData = GetPathData(pathGeom.Geoms, dx, dy, out animatedPath, out duration);
                                    writer.WriteStartElement("path");
                                    if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                                    writer.WriteAttributeString("d", pathData);
                                    writer.WriteAttributeString("fill", "rgb(150,150,150)");
                                    writer.WriteAttributeString("filter", String.Format("url(#{0})", guid));
                                    writer.WriteEndElement();
                                }
                            }
                        }

                        if (geom is StiTextGeom)
                        {
                            #region Draw text
                            var textGeom = geom as StiTextGeom;
                            StringFormat sf = textGeom.StringFormat.IsGeneric ? StringFormat.GenericDefault.Clone() as StringFormat : new StringFormat();
                            sf.Alignment = textGeom.StringFormat.Alignment;
                            sf.FormatFlags = textGeom.StringFormat.FormatFlags;
                            sf.HotkeyPrefix = textGeom.StringFormat.HotkeyPrefix;
                            sf.LineAlignment = textGeom.StringFormat.LineAlignment;
                            sf.Trimming = textGeom.StringFormat.Trimming;

                            var pointF = new PointF();

                            if (textGeom.Angle == 0f && (!(textGeom.Location is PointF)) && !textGeom.IsRotatedText)
                            {
                                var rect = RectToRectangleF(textGeom.Location);
                                pointF = new PointF(rect.X, rect.Y);
                            }
                            else
                            {
                                if (!(textGeom.Location is PointF))
                                {
                                    var rect = RectToRectangleF(textGeom.Location);
                                    pointF = new PointF((float)(rect.X + rect.Width / 2), (float)(rect.Y + rect.Height / 2));
                                }
                                else
                                {
                                    pointF = (PointF)textGeom.Location;
                                }
                            }

                            var style = new StringBuilder();

                            var size = textGeom.Font.FontSize * 4 / 3;

                            writer.WriteStartElement("text");

                            switch (textGeom.RotationMode)
                            {
                                case StiRotationMode.LeftCenter:
                                    writer.WriteAttributeString("dy", "0.5em");
                                    break;

                                case StiRotationMode.LeftBottom:
                                    break;

                                case StiRotationMode.CenterTop:
                                    style.AppendFormat("text-anchor:{0};", "middle");
                                    writer.WriteAttributeString("dy", "1em");
                                    break;

                                case StiRotationMode.CenterCenter:
                                    style.AppendFormat("text-anchor:{0};", "middle");
                                    writer.WriteAttributeString("dy", "0.5em");
                                    break;

                                case StiRotationMode.CenterBottom:
                                    style.AppendFormat("text-anchor:{0};", "middle");
                                    break;

                                case StiRotationMode.RightTop:
                                    style.AppendFormat("text-anchor:{0};", "end");
                                    writer.WriteAttributeString("dy", "1em");
                                    break;

                                case StiRotationMode.RightCenter:
                                    style.AppendFormat("text-anchor:{0};", "end");
                                    writer.WriteAttributeString("dy", "0.5em");
                                    break;

                                case StiRotationMode.RightBottom:
                                    style.AppendFormat("text-anchor:{0};", "end");
                                    break;

                                default:
                                    writer.WriteAttributeString("dy", "1em");
                                    break;
                            }

                            writer.WriteAttributeString("transform", string.Format("translate({0}, {1}) rotate({2} 0,0)",
                                (pointF.X + dx).ToString().Replace(",", "."), (pointF.Y + dy).ToString().Replace(",", "."), textGeom.Angle.ToString().Replace(",", ".")));

                            writer.WriteAttributeString("font-size", size.ToString().Replace(",", "."));
                            writer.WriteAttributeString("font-family", textGeom.Font.FontName);
                            if ((textGeom.Font.FontStyle & FontStyle.Bold) > 0)
                                writer.WriteAttributeString("font-weight", "bold");
                            if ((textGeom.Font.FontStyle & FontStyle.Italic) > 0)
                                writer.WriteAttributeString("font-style", "italic");

                            var valueDecoration = "";
                            if ((textGeom.Font.FontStyle & FontStyle.Underline) > 0)
                                valueDecoration += "underline";
                            if ((textGeom.Font.FontStyle & FontStyle.Strikeout) > 0)
                                valueDecoration += " line-through";
                            if (!String.IsNullOrEmpty(valueDecoration))
                                writer.WriteAttributeString("text-decoration", valueDecoration);

                            Color textColor = StiBrush.ToColor(textGeom.Brush as StiBrush);
                            style.Append(string.Format("fill:#{0:X2}{1:X2}{2:X2};", textColor.R, textColor.G, textColor.B));
                            if (textColor.A != 0xFF)
                            {
                                style.Append(string.Format("fill-opacity:{0}", Math.Round(textColor.A / 255f, 3).ToString().Replace(",", ".")));
                            }

                            writer.WriteAttributeString("style", style.ToString());
                            if (textGeom.MaximalWidth != 0 && textGeom.MaximalWidth != null)
                            {

                                var length = textGeom.Text.Length;
                                var wrapCharCount = length;
                                var rectTemp = new SizeF(0, 0);

                                for (var index = 0; index < length; index++)
                                {

                                    rectTemp = context.MeasureString(textGeom.Text.Substring(0, index), textGeom.Font);
                                    if (rectTemp.Width > textGeom.MaximalWidth && index != 0)
                                    {
                                        wrapCharCount = index - 1;
                                        break;
                                    }
                                }

                                var countRow = Math.Ceiling((double)length / wrapCharCount);
                                var startPointY = 0d;

                                switch (textGeom.RotationMode)
                                {
                                    case StiRotationMode.LeftCenter:
                                    case StiRotationMode.CenterCenter:
                                    case StiRotationMode.RightCenter:
                                        startPointY = -countRow * rectTemp.Height / 2 + rectTemp.Height / 2;//offset is taken into account 0.5em parent <text>
                                        break;

                                    default:
                                        startPointY = 0;
                                        break;
                                }

                                if (wrapCharCount > 0)
                                {

                                    var startIndex = 0;
                                    var index = 0;
                                    while (startIndex < length)
                                    {

                                        writer.WriteStartElement("tspan");
                                        writer.WriteAttributeString("x", "0");

                                        if (index == 0)
                                            writer.WriteAttributeString("y", startPointY.ToString().Replace(",", "."));
                                        else
                                            writer.WriteAttributeString("dy", rectTemp.Height.ToString().Replace(",", "."));

                                        writer.WriteString(startIndex + wrapCharCount < textGeom.Text.Length 
                                            ? textGeom.Text.Substring(startIndex, wrapCharCount)
                                            : textGeom.Text.Substring(startIndex));

                                        writer.WriteEndElement();

                                        startIndex += wrapCharCount;
                                        index++;
                                    }
                                }
                            }
                            else
                            {
                                writer.WriteValue(textGeom.Text);
                            }
                            writer.WriteEndElement();
                            #endregion
                        }

                        if (geom is StiPathGeom)
                        {
                            #region Draw path
                            var path = geom as StiPathGeom;

                            var rect = RectToRectangleF(path.Rect);

                            var style = string.Empty;
                            var fill = string.Empty;

                            if (path.Background != null)
                            {
                                style = WriteFillBrush(writer, path.Background, rect, dx, dy);
                            }
                            else
                            {
                                style = "fill:none;";
                            }

                            if (CheckPenGeom(path.Pen))
                            {
                                var stroke = string.Format("{0}", WriteBorderStroke(writer, path.Pen.Brush, rect));
                                style += string.Format("{0};stroke-width:{1};", stroke, path.Pen.Thickness.ToString().Replace(",", "."));
                            }

                            StringBuilder animatedPath;
                            TimeSpan? duration;
                            var pathData = GetPathData(path.Geoms, dx, dy, out animatedPath, out duration);

                            writer.WriteStartElement("path");
                            if (!String.IsNullOrEmpty(clip)) writer.WriteAttributeString("clip-path", String.Format("url(#{0})", clip));
                            writer.WriteAttributeString("d", pathData);
                            writer.WriteAttributeString("style", style);
                            WriteInteracrion(writer, path.Interaction);

                            writer.WriteEndElement();
                            #endregion
                        }
                    }

                    writer.WriteEndElement();
                }
            }
        }

        public static void WriteChart(XmlTextWriter writer, StiSvgData svgData, Boolean needAnimation)
        {
            WriteChart(writer, svgData, 1f, needAnimation);
        }

        private static void WriteInteracrion(XmlTextWriter writer, StiInteractionDataGeom interaction)
        {
            if (interaction != null)
            {
                writer.WriteAttributeString("interaction", interaction.ComponentName);
                writer.WriteAttributeString("pageguid", interaction.PageGuid);
                writer.WriteAttributeString("compindex", interaction.ComponentIndex);
                writer.WriteAttributeString("pageindex", interaction.PageIndex);
                writer.WriteAttributeString("elementindex", interaction.ElementIndex);
            }
        }

        private static string GetPathData(List<StiSegmentGeom> geoms, float dx, float dy, out StringBuilder animatedPath, out TimeSpan? duration)
        {
            string path = string.Empty;
            animatedPath = new StringBuilder();
            duration = TimeSpan.FromMilliseconds(0);
            int geomIndex = 0;
            foreach (var geom in geoms)
            {
                if (geom is StiArcSegmentGeom)
                {
                    var arcSegment = geom as StiArcSegmentGeom;
                    path += AddArcPath(arcSegment, path, dx, dy);
                }
                else if (geom is StiCurveSegmentGeom)
                {
                    var curveSegment = geom as StiCurveSegmentGeom;
                    var animation = curveSegment.Animation as StiPointsAnimation;
                    var points = StiCurveHelper.CardinalSpline(curveSegment.Points, false);
                    var pointsFrom = animation != null ? StiCurveHelper.CardinalSpline(animation.PointsFrom, false) : null;
                    var sb = new StringBuilder();

                    for (int index = 1; index < points.Length; index += 3)
                    {
                        if (animation != null)
                        {
                            if (index == 1)
                                sb.AppendFormat("C{0},{1},{2},{3},{4},{5}",
                                    (pointsFrom[index].X + dx).ToString().Replace(",", "."), (pointsFrom[index].Y + dy).ToString().Replace(",", "."),
                                    (pointsFrom[index + 1].X + dx).ToString().Replace(",", "."), (pointsFrom[index + 1].Y + dy).ToString().Replace(",", "."),
                                    (pointsFrom[index + 2].X + dx).ToString().Replace(",", "."), (pointsFrom[index + 2].Y + dy).ToString().Replace(",", "."));
                            else
                                sb.AppendFormat(",{0},{1},{2},{3},{4},{5}",
                                    (pointsFrom[index].X + dx).ToString().Replace(",", "."), (pointsFrom[index].Y + dy).ToString().Replace(",", "."),
                                    (pointsFrom[index + 1].X + dx).ToString().Replace(",", "."), (pointsFrom[index + 1].Y + dy).ToString().Replace(",", "."),
                                    (pointsFrom[index + 2].X + dx).ToString().Replace(",", "."), (pointsFrom[index + 2].Y + dy + index * 0.0001).ToString().Replace(",", "."));

                            if (index == 1)
                                animatedPath.AppendFormat("C{0}:{1},{2}:{3},{4}:{5},{6}:{7},{8}:{9},{10}:{11}",
                                    p(pointsFrom[index].X + dx), p(points[index].X + dx),
                                    p(pointsFrom[index].Y + dy), p(points[index].Y + dy),
                                    p(pointsFrom[index + 1].X + dx), p(points[index + 1].X + dx),
                                    p(pointsFrom[index + 1].Y + dy), p(points[index + 1].Y + dy),
                                    p(pointsFrom[index + 2].X + dx), p(points[index + 2].X + dx),
                                    p(pointsFrom[index + 2].Y + dy), p(points[index + 2].Y + dy));
                            else
                                animatedPath.AppendFormat(",{0}:{1},{2}:{3},{4}:{5},{6}:{7},{8}:{9},{10}:{11}",
                                    p(pointsFrom[index].X + dx), p(points[index].X + dx),
                                    p(pointsFrom[index].Y + dy), p(points[index].Y + dy),
                                    p(pointsFrom[index + 1].X + dx), p(points[index + 1].X + dx),
                                    p(pointsFrom[index + 1].Y + dy), p(points[index + 1].Y + dy),
                                    p(pointsFrom[index + 2].X + dx), p(points[index + 2].X + dx),
                                    p(pointsFrom[index + 2].Y + dy), p(points[index + 2].Y + dy + index * 0.0001));
                            duration = TimeSpan.FromSeconds(1);
                        }
                        else
                        {
                            if (index == 1)
                                sb.AppendFormat("C{0},{1},{2},{3},{4},{5}",
                                    (points[index].X + dx).ToString().Replace(",", "."), (points[index].Y + dy).ToString().Replace(",", "."),
                                    (points[index + 1].X + dx).ToString().Replace(",", "."), (points[index + 1].Y + dy).ToString().Replace(",", "."),
                                    (points[index + 2].X + dx).ToString().Replace(",", "."), (points[index + 2].Y + dy).ToString().Replace(",", "."));
                            else
                                sb.AppendFormat(",{0},{1},{2},{3},{4},{5}",
                                    (points[index].X + dx).ToString().Replace(",", "."), (points[index].Y + dy).ToString().Replace(",", "."),
                                    (points[index + 1].X + dx).ToString().Replace(",", "."), (points[index + 1].Y + dy).ToString().Replace(",", "."),
                                    (points[index + 2].X + dx).ToString().Replace(",", "."), (points[index + 2].Y + dy + index * 0.0001).ToString().Replace(",", "."));
                        }
                    }

                    path += sb;
                }
                else if (geom is StiLineSegmentGeom)
                {
                    var lineSegment = geom as StiLineSegmentGeom;
                    var animation = lineSegment.Animation as StiPointsAnimation;

                    var sb = new StringBuilder();

                    if (!path.StartsWith("M"))
                    {
                        if (animation != null)
                        {
                            sb.AppendFormat("M{0},{1}", p(animation.PointsFrom[0].X + dx), p(animation.PointsFrom[0].Y + dy));
                            animatedPath.AppendFormat("M{0}:{1},{2}:{3}", p(animation.PointsFrom[0].X + dx), p(lineSegment.X1 + dx),
                                p(animation.PointsFrom[0].Y + dy), p(lineSegment.Y1 + dy));
                        }
                        else
                        {
                            sb.AppendFormat("M{0},{1}", (lineSegment.X1 + dx).ToString().Replace(",", "."), (lineSegment.Y1 + dy).ToString().Replace(",", "."));
                        }
                    }

                    if (animation != null)
                    {
                        sb.AppendFormat("L{0},{1}", p(animation.PointsFrom[1].X + dx), p(animation.PointsFrom[1].Y + dy));
                        animatedPath.AppendFormat("L{0}:{1},{2}:{3}", p(animation.PointsFrom[1].X + dx), p(lineSegment.X2 + dx),
                            p(animation.PointsFrom[1].Y + dy), p(lineSegment.Y2 + dy + 0.0001));
                        duration = animation.Duration;
                    }
                    else
                    {
                        sb.AppendFormat("L{0},{1}", (lineSegment.X2 + dx).ToString().Replace(",", "."), (lineSegment.Y2 + dy + 0.0001).ToString().Replace(",", "."));
                    }

                    path += sb;
                }
                else if (geom is StiLinesSegmentGeom)
                {
                    var linesSegment = geom as StiLinesSegmentGeom;
                    var animation = linesSegment.Animation as StiPointsAnimation;

                    var sb = new StringBuilder();

                    if (!path.StartsWith("M"))
                    {
                        if (animation != null)
                        {
                            sb.AppendFormat("M{0},{1}", p(animation.PointsFrom[0].X + dx), p(animation.PointsFrom[0].Y + dy));
                            animatedPath.AppendFormat("M{0}:{1},{2}:{3}", p(animation.PointsFrom[0].X + dx), p(animation.PointsFrom[1].X + dx),
                                p(animation.PointsFrom[0].Y + dy), p(animation.PointsFrom[1].Y + dy + 0.0001));
                        }
                        else
                        {
                            sb.AppendFormat("M{0},{1}", p(linesSegment.Points[0].X + dx), p(linesSegment.Points[0].Y + dy + 0.0001));
                        }
                    }

                    for (int index = 0; index < linesSegment.Points.Length; index++)
                    {
                        if (animation != null)
                        {
                            sb.AppendFormat("{0}{1},{2}", index == 0 ? "L" : ",", p(animation.PointsFrom[index].X + dx), p(animation.PointsFrom[index].Y + dy));
                            animatedPath.AppendFormat("{0}{1}:{2},{3}:{4}", index == 0 ? "L" : ",", p(animation.PointsFrom[index].X + dx), p(linesSegment.Points[index].X + dx),
                                p(animation.PointsFrom[index].Y + dy), p(linesSegment.Points[index].Y + dy));
                            duration = animation.Duration;
                        }
                        else
                        {
                            sb.AppendFormat("{0}{1},{2}", index == 0 ? "L" : ",", p(linesSegment.Points[index].X + dx), p(linesSegment.Points[index].Y + dy + index * 0.0001));
                        }
                    }

                    path += sb;
                }
                else if (geom is StiPieSegmentGeom)
                {
                    var pieSegment = geom as StiPieSegmentGeom;
                    path += AddPiePath(pieSegment, path, dx, dy, animatedPath, out duration);
                }
                else if (geom is StiCloseFigureSegmentGeom)
                {
                    //path.IsClosed = true;
                }

                geomIndex++;
            }

            return path;
        }

        private static string AddArcPath(StiArcSegmentGeom arcSegment, string path, float dx, float dy)
        {
            var sb = new StringBuilder();
            
            double centerX = arcSegment.Rect.X + dx + arcSegment.Rect.Width / 2;
            double centerY = arcSegment.Rect.Y + dy + arcSegment.Rect.Height / 2;
            double radius = arcSegment.Rect.Width / 2;
            double startAngle = arcSegment.StartAngle * Math.PI / 180;

            double x1 = centerX + radius * Math.Cos(startAngle);
            double y1 = centerY + radius * Math.Sin(startAngle);

            if (!path.StartsWith("M") || arcSegment.SweepAngle % 360 == 0)
                sb.AppendFormat("M{0},{1}", x1.ToString().Replace(",", "."), y1.ToString().Replace(",", "."));

            double step = Round(Math.Abs(arcSegment.SweepAngle / 90));
            double stepAngle = arcSegment.SweepAngle / step;
            startAngle = arcSegment.StartAngle;

            for (int indexStep = 0; indexStep < step; indexStep++)
            {
                var points = ConvertArcToCubicBezier(arcSegment.Rect, startAngle, stepAngle);

                if (indexStep == 0)
                    sb.AppendFormat("C{0},{1},{2},{3},{4},{5}",
                        (points[1].X + dx).ToString().Replace(",", "."), (points[1].Y + dy).ToString().Replace(",", "."),
                        (points[2].X + dx).ToString().Replace(",", "."), (points[2].Y + dy).ToString().Replace(",", "."),
                        (points[3].X + dx).ToString().Replace(",", "."), (points[3].Y + dy).ToString().Replace(",", "."));
                else
                    sb.AppendFormat(",{0},{1},{2},{3},{4},{5}",
                        (points[1].X + dx).ToString().Replace(",", "."), (points[1].Y + dy).ToString().Replace(",", "."),
                        (points[2].X + dx).ToString().Replace(",", "."), (points[2].Y + dy).ToString().Replace(",", "."),
                        (points[3].X + dx).ToString().Replace(",", "."), (points[3].Y + dy).ToString().Replace(",", "."));

                startAngle += stepAngle;
            }

            return sb.ToString();
        }

        private static string AddPiePath(StiPieSegmentGeom pieSegment, string path, float dx, float dy, StringBuilder animatedPath, out TimeSpan? duration)
        {
            StringBuilder sb = new StringBuilder();
            var animation = pieSegment.Animation as StiPieSegmentAnimation;

            double centerX = pieSegment.Rect.X + dx + pieSegment.Rect.Width / 2;
            double centerY = pieSegment.Rect.Y + dy + pieSegment.Rect.Height / 2;
            double radius = pieSegment.Rect.Width / 2;
            double startAngleFrom = animation == null ? pieSegment.StartAngle : animation.StartAngleFrom;
            double sweepAngleFrom = animation == null ? pieSegment.SweepAngle : animation.SweepAngleFrom;
            double startAngle = startAngleFrom * Math.PI / 180;

            double x1 = centerX + radius * Math.Cos(startAngle);
            double y1 = centerY + radius * Math.Sin(startAngle);

            sb.AppendFormat("M{0},{1}", p(centerX), p(centerY));
            sb.AppendFormat("L{0},{1}", p(x1), p(y1));

            double step = Round(Math.Abs(sweepAngleFrom / 90));
            double stepAngle = sweepAngleFrom / step;
            startAngle = startAngleFrom;

            for (int indexStep = 0; indexStep < step; indexStep++)
            {
                PointF[] points = ConvertArcToCubicBezier(pieSegment.Rect, startAngle, stepAngle);

                for (int index = 1; index < points.Length - 1; index += 3)
                {
                    if (index == 1)
                        sb.AppendFormat("C{0},{1},{2},{3},{4},{5}",
                            (points[index].X + dx).ToString().Replace(",", "."), (points[index].Y + dy).ToString().Replace(",", "."),
                            (points[index + 1].X + dx).ToString().Replace(",", "."), (points[index + 1].Y + dy).ToString().Replace(",", "."),
                            (points[index + 2].X + dx).ToString().Replace(",", "."), (points[index + 2].Y + dy).ToString().Replace(",", "."));
                    else
                        sb.AppendFormat(",{0},{1},{2},{3},{4},{5}",
                            (points[index].X + dx).ToString().Replace(",", "."), (points[index].Y + dy).ToString().Replace(",", "."),
                            (points[index + 1].X + dx).ToString().Replace(",", "."), (points[index + 1].Y + dy).ToString().Replace(",", "."),
                            (points[index + 2].X + dx).ToString().Replace(",", "."), (points[index + 2].Y + dy).ToString().Replace(",", "."));
                }

                startAngle += stepAngle;
            }

            sb.AppendFormat("L{0},{1}", centerX.ToString().Replace(",", "."), centerY.ToString().Replace(",", "."));
            if (animation != null)
            {
                duration = animation.Duration;
                animatedPath.Append(Convert.ToBase64String(Encoding.UTF8.GetBytes(String.Format("{{\"startAngle\":{0}, \"startAngleFrom\": {1}, \"sweepAngle\": {2}, \"sweepAngleFrom\": {3}, \"x\": {4}, \"y\": {5}," +
                                                                                                "\"width\": {6}, \"height\": {7}, \"dx\": {8}, \"dy\": {9}}}", p(pieSegment.StartAngle), p(animation.StartAngleFrom), p(pieSegment.SweepAngle),
                    p(animation.SweepAngleFrom), p(pieSegment.Rect.X), p(pieSegment.Rect.Y), p(pieSegment.Rect.Width),
                    p(pieSegment.Rect.Height), p(dx), p(dy)))));
            }
            else
            {
                duration = TimeSpan.FromSeconds(1);
            }

            return sb.ToString();
        }

        public static RectangleF CorrectRectLabel(StiRotationMode rotationMode, RectangleF textRect)
        {
            switch (rotationMode)
            {
                case StiRotationMode.LeftCenter:
                    return new RectangleF(textRect.X + textRect.Width / 2, textRect.Y, textRect.Width, textRect.Height);

                case StiRotationMode.LeftBottom:
                    return new RectangleF(textRect.X + textRect.Width / 2, textRect.Y - textRect.Height / 2, textRect.Width, textRect.Height);

                case StiRotationMode.LeftTop:
                    return new RectangleF(textRect.X + textRect.Width / 2, textRect.Y + textRect.Height / 2, textRect.Width, textRect.Height);

                case StiRotationMode.CenterTop:
                    return new RectangleF(textRect.X, textRect.Y + textRect.Height / 2, textRect.Width, textRect.Height);

                case StiRotationMode.CenterCenter:
                    return textRect;

                case StiRotationMode.CenterBottom:
                    return new RectangleF(textRect.X, textRect.Y - textRect.Height / 2, textRect.Width, textRect.Height);

                case StiRotationMode.RightTop:
                    return new RectangleF(textRect.X - textRect.Width / 2, textRect.Y + textRect.Height / 2, textRect.Width, textRect.Height);

                case StiRotationMode.RightCenter:
                    return new RectangleF(textRect.X - textRect.Width / 2, textRect.Y, textRect.Width, textRect.Height);

                case StiRotationMode.RightBottom:
                    return new RectangleF(textRect.X - textRect.Width / 2, textRect.Y - textRect.Height / 2, textRect.Width, textRect.Height);

                default:
                    return textRect;
            }
        }

        private static PointF[] ConvertArcToCubicBezier(RectangleF rect, double startAngle1, double sweepAngle1)
        {
            var centerX = rect.X + rect.Width / 2;
            var centerY = rect.Y + rect.Height / 2;

            var radius = Math.Min(rect.Width / 2, rect.Height / 2);

            var startAngle = startAngle1 * Math.PI / 180;
            var sweepAngle = sweepAngle1 * Math.PI / 180;
            var endAngle = (startAngle1 + sweepAngle1) * Math.PI / 180;

            var x1 = (float) (centerX + radius * Math.Cos(startAngle));
            var y1 = (float) (centerY + radius * Math.Sin(startAngle));

            var x2 = (float) (centerX + radius * Math.Cos(endAngle));
            var y2 = (float) (centerY + radius * Math.Sin(endAngle));

            var l = radius * 4 / 3 * Math.Tan(0.25 * sweepAngle);
            var aL = Math.Atan(l / radius);
            var radL = radius / Math.Cos(aL);

            aL += startAngle;
            var ax1 = (float) (centerX + radL * Math.Cos(aL));
            var ay1 = (float) (centerY + radL * Math.Sin(aL));

            aL = Math.Atan(-l / radius);
            aL += endAngle;
            var ax2 = (float) (centerX + radL * Math.Cos(aL));
            var ay2 = (float) (centerY + radL * Math.Sin(aL));

            return new PointF[4]
            {
                new PointF(x1, y1), new PointF(ax1, ay1), new PointF(ax2, ay2), new PointF(x2, y2)
            };
        }

        private static double Round(double value)
        {
            int value1 = (int) value;
            double rest = value - value1;

            return (rest > 0) ? (double) (value1 + 1) : (double) (value1);
        }

        private static PointF[] ConvertSplineToCubicBezier(PointF[] points, float tension)
        {
            var count = points.Length;
            int len_pt = count * 3 - 2;
            PointF[] pt = new PointF[len_pt];

            tension = tension * 0.3f;

            pt[0] = points[0];
            pt[1] = CalculateCurveBezierEndPoints(points[0], points[1], tension);

            for (int index = 0; index < count - 2; index++)
            {
                PointF[] temp = CalculateCurveBezier(points, index, tension);

                pt[3 * index + 2] = temp[0];
                pt[3 * index + 3] = points[index + 1];
                pt[3 * index + 4] = temp[1];
            }

            pt[len_pt - 2] = CalculateCurveBezierEndPoints(points[count - 1], points[count - 2], tension);
            pt[len_pt - 1] = points[count - 1];

            return pt;
        }

        private static PointF[] CalculateCurveBezier(PointF[] points, int index, float tension)
        {
            float xDiff = points[index + 2].X - points[index + 0].X;
            float yDiff = points[index + 2].Y - points[index + 0].Y;

            return new PointF[]
            {
                new PointF(points[index + 1].X - tension * xDiff, points[index + 1].Y - tension * yDiff),
                new PointF(points[index + 1].X + tension * xDiff, points[index + 1].Y + tension * yDiff)
            };
        }

        private static PointF CalculateCurveBezierEndPoints(PointF end, PointF adj, float tension)
        {
            return new PointF(tension * (adj.X - end.X) + end.X, tension * (adj.Y - end.Y) + end.Y);
        }

        private static bool CheckPenGeom(StiPenGeom penGeom)
        {
            return !((penGeom == null) || (penGeom.Brush == null) || (penGeom.PenStyle == StiPenStyle.None));
        }

        public static string WriteFillBrush(XmlTextWriter writer, object brush, RectangleF rect, float dx = 0, float dy = 0)
        {
            var sRect = rect;
            sRect.X += dx;
            sRect.Y += dy;
            if (brush is Color)
            {
                var color = (Color) brush;
                return string.Format("fill:rgb({0},{1},{2});fill-opacity:{3};", color.R, color.G, color.B, Math.Round(color.A / 255f, 3).ToString().Replace(",", "."));
            }
            else if (brush is StiGradientBrush)
            {
                var gradientId = StiBrushSvgHelper.WriteGradientBrush(writer, brush, sRect);

                return string.Format("fill:url(#{0});", gradientId);
            }
            else if (brush is StiGlareBrush)
            {
                var gradientId = StiBrushSvgHelper.WriteGlareBrush(writer, brush, sRect);

                return string.Format("fill:url(#{0});", gradientId);
            }
            else if (brush is StiGlassBrush)
            {
                var gradientId = StiBrushSvgHelper.WriteGlassBrush(writer, brush, sRect);

                return string.Format("fill:url(#{0});", gradientId);
            }
            else if (brush is StiHatchBrush)
            {
                var gradientId = StiBrushSvgHelper.WriteHatchBrush(writer, brush);

                return string.Format("fill:url(#{0});", gradientId);
            }
            else if (brush is StiBrush)
            {
                var color = StiBrush.ToColor(brush as StiBrush);
                return string.Format("fill:rgb({0},{1},{2});fill-opacity:{3};", color.R, color.G, color.B, Math.Round(color.A / 255f, 3).ToString().Replace(",", "."));
            }

            return "fill:none;";
        }

        private static string WriteBorderStroke(XmlTextWriter writer, object brush, RectangleF rect)
        {
            if (brush is Color)
            {
                var color = (Color) brush;
                var result = string.Format("stroke:rgb({0},{1},{2});", color.R, color.G, color.B);

                var alfa = Math.Round(color.A / 255f, 3);
                if (alfa != 1)
                    result += string.Format("stroke-opacity:{0};", alfa.ToString().Replace(",", "."));

                return result;
            }
            else if (brush is StiGradientBrush)
            {
                var gradientId = StiBrushSvgHelper.WriteGradientBrush(writer, brush, rect);

                return string.Format("fill:url(#{0});", gradientId);
            }
            else if (brush is StiGlareBrush)
            {
                var gradientId = StiBrushSvgHelper.WriteGlareBrush(writer, brush, rect);

                return string.Format("fill:url(#{0});", gradientId);
            }
            else if (brush is StiGlassBrush)
            {
                var gradientId = StiBrushSvgHelper.WriteGlassBrush(writer, brush, rect);

                return string.Format("fill:url(#{0});", gradientId);
            }
            else if (brush is StiHatchBrush)
            {
                var gradientId = StiBrushSvgHelper.WriteHatchBrush(writer, brush);

                return string.Format("fill:url(#{0});", gradientId);
            }
            else if (brush is StiBrush)
            {
                var color = StiBrush.ToColor(brush as StiBrush);
                var result = string.Format("stroke:rgb({0},{1},{2})", color.R, color.G, color.B);

                var alfa = Math.Round(color.A / 255f, 3);
                if (alfa != 1)
                    result += string.Format(";stroke-opacity:{0}", alfa.ToString().Replace(",", "."));
            }

            return "stroke-opacity:0";
        }


        private static String p(double f)
        {
            return f.ToString().Replace(",", ".");
        }


        private static RectangleF RectToRectangleF(object rect)
        {
            if (rect is RectangleF) return (RectangleF) rect;
            if (rect is Rectangle)
            {
                return (Rectangle) rect;
            }

            if (rect is RectangleD)
            {
                RectangleD rectangle = (RectangleD) rect;
                return new RectangleF((float) rectangle.X, (float) rectangle.Y, (float) rectangle.Width, (float) rectangle.Height);
            }

            return new RectangleF();
        }
        #endregion
    }
}