﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Services;

namespace Stimulsoft.Report.Export
{
    public static class StiImageHelper
    {
        public static string GetPathImageOffice2013FromBitmapService(StiService service, double xFactor = 1)
        {
            var xFactorName = "";
            if (xFactor == 1.5) xFactorName = "_x1_5";
            if (xFactor == 2) xFactorName = "_x2";
            if (xFactor == 3) xFactorName = "_x3";
            if (xFactor == 4) xFactorName = "_x4";
            
            if (service is StiPdfExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FilePDF{xFactorName}.png";

            if (service is StiXpsExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FileXPS{xFactorName}.png";

            if (service is StiPpt2007ExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FilePowerPoint{xFactorName}.png";

            if (service is StiHtmlExportService || 
                service is StiHtml5ExportService || 
                service is StiMhtExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FileHTML{xFactorName}.png";

            if (service is StiTxtExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FileTXT{xFactorName}.png";

            if (service is StiRtfExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FileRTF{xFactorName}.png";

            if (service is StiWord2007ExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FileWord{xFactorName}.png";

            if (service is StiOdtExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FileODW{xFactorName}.png";

            if (service is StiExcelExportService || 
                service is StiExcel2007ExportService || 
                service is StiExcelXmlExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FileExcel{xFactorName}.png";

            if (service is StiOdsExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FileODC{xFactorName}.png";

            if (service is StiImageExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FileImage{xFactorName}.png";

            if (service is StiDataExportService ||
                service is StiCsvExportService ||
                service is StiDbfExportService ||
                service is StiDifExportService ||
                service is StiSylkExportService ||
                service is StiXmlExportService)
                return $"Stimulsoft.Report.Images.Office2013.Exports.FileData{xFactorName}.png";

            return null;
        }
    }
}
