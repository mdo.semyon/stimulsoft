﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Drawing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml;

using Stimulsoft.Report.Maps;
using Stimulsoft.Report.Maps.Helpers;
using System.Collections;
using System.Text.RegularExpressions;
using Stimulsoft.Report.Painters;

namespace Stimulsoft.Report.Export
{
    public static class StiMapSvgHelper
    {
        #region
        private static NoneInfo noneInfo = new NoneInfo();
        #endregion

        #region class HeatmapInfo
        public class HeatmapInfo
        {
            #region Fields
            private double min;
            private double max;
            private Color[] colors;
            #endregion

            #region Methods
            public Brush GetBrush(StiMapData mapData, Brush defaultBrush)
            {
                if (min == max)
                    return new SolidBrush(colors[0]);

                double value = 0;
                if (mapData.Value == null || !double.TryParse(mapData.Value, out value))
                    return defaultBrush;

                return new SolidBrush(StiColorUtils.ChangeLightness(colors[0], (float)(0.85 * (max - value) / (max - min))));
            }
            #endregion

            public HeatmapInfo(StiMap map, List<StiMapData> mapData)
            {
                bool isFirst = true;
                foreach (var data in mapData)
                {
                    if (data.Value == null) continue;

                    double value = 0d;
                    if (!double.TryParse(data.Value, out value)) continue;

                    if (isFirst)
                    {
                        isFirst = false;

                        min = value;
                        max = value;
                    }
                    else
                    {
                        if (value < min)
                            min = value;
                        else if (value > max)
                            max = value;
                    }
                }

                this.colors = StiMap.GetMapStyle(map).HeatmapColors;
            }
        }
        #endregion

        #region class HeatmapWithGroupInfo
        private class HeatmapWithGroupInfo
        {
            #region Fields
            private Brush defaultBrush;
            private StiBrush defaultBrush1;
            private Dictionary<string, double[]> hash = new Dictionary<string, double[]>();
            private Dictionary<string, Color> hashColors = new Dictionary<string, Color>();
            #endregion

            #region Methods
            public Brush GetBrush(StiMapData data)
            {
                if (data.Group == null) return null;

                if (!hash.ContainsKey(data.Group))
                    return this.defaultBrush;

                var values = hash[data.Group];
                var color = hashColors[data.Group];

                if (values[0] == values[1])
                    return new SolidBrush(color);

                double value = 0;
                if (data.Value == null || !double.TryParse(data.Value, out value))
                    return this.defaultBrush;

                return new SolidBrush(StiColorUtils.ChangeLightness(color, (float)(0.85f * (values[1] - value) / (values[1] - values[0]))));
            }

            public StiBrush GetBrush1(StiMapData data)
            {
                if (data.Group == null) return null;

                if (!hash.ContainsKey(data.Group))
                    return this.defaultBrush1;

                var values = hash[data.Group];
                var color = hashColors[data.Group];

                if (values[0] == values[1])
                    return new StiSolidBrush(color);

                double value = 0;
                if (data.Value == null || !double.TryParse(data.Value, out value))
                    return this.defaultBrush1;

                return new StiSolidBrush(StiColorUtils.ChangeLightness(color, (float)(0.85f * (values[1] - value) / (values[1] - values[0]))));
            }
            #endregion

            public HeatmapWithGroupInfo(Brush defaultBrush, StiBrush defaultBrush1, StiMap map, List<StiMapData> mapData)
            {
                this.defaultBrush = defaultBrush;
                this.defaultBrush1 = defaultBrush1;

                var colors = StiMap.GetMapStyle(map).HeatmapColors;
                int index = 0;

                foreach (var data in mapData)
                {
                    var key = data.Group;
                    if (key == null) continue;
                    if (data.Value == null) continue;

                    double value = 0;
                    if (data.Value == null || !double.TryParse(data.Value, out value)) continue;

                    if (!hash.ContainsKey(key))
                    {
                        var values = new double[2] { value, value };
                        hash.Add(key, values);
                    }
                    else
                    {
                        var values = hash[key];
                        if (value < values[0])
                            values[0] = value;
                        else if (value > values[1])
                            values[1] = value;
                    }

                    if (!hashColors.ContainsKey(key))
                    {
                        var color = colors[index];

                        index++;
                        if (index >= colors.Length)
                            index = 0;

                        hashColors.Add(key, color);
                    }
                }
            }
        }
        #endregion

        #region class NoneInfo
        private class NoneInfo
        {
            public NoneInfo()
            {
                this.Colors = StiMapHelper.GetColors();
            }

            #region Fields

            private Color[] Colors;
            private int index = 0;

            #endregion

            #region Methods

            public StiSolidBrush GetBrush()
            {
                var color = this.Colors[index];

                index++;
                if (index >= this.Colors.Length)
                    index = 0;

                return new StiSolidBrush(color);
            }

            #endregion
        }
        #endregion

        #region class StiStyleColorsContainer
        public class StiStyleColorsContainer
        {
            #region Fields
            private List<Color> stackColors = new List<Color>();
            private int index;
            #endregion

            #region Methods
            public SolidBrush GetColor()
            {
                var brush = new SolidBrush(stackColors[index]);

                index++;
                if (index >= stackColors.Count)
                    index = 0;

                return brush;
            }

            public StiBrush GetColor1()
            {
                var brush = new StiSolidBrush(stackColors[index]);

                index++;
                if (index >= stackColors.Count)
                    index = 0;

                return brush;
            }

            public void Init(StiMap map)
            {
                this.stackColors.Clear();
                var colors = map.GetCurrentStyleColors();
                index = 0;

                foreach (var color in colors)
                {
                    this.stackColors.Add(color);
                }
            }
            #endregion
        }
        #endregion

        #region Methods
        public static void DrawMap(XmlTextWriter xmlsWriter, StiMap map, double width, double height, bool animated)
        {
            var resource = StiMapLoader.LoadResource(map.MapID.ToString());
            if (resource == null) return;

            var sScale = width / resource.Width < height / resource.Height ? width / resource.Width : height / resource.Height;

            xmlsWriter.WriteStartElement("rect");
            xmlsWriter.WriteAttributeString("width", width.ToString().Replace(",", "."));
            xmlsWriter.WriteAttributeString("height", height.ToString().Replace(",", "."));

            var color = ((StiSolidBrush)map.GetStyleBackground()).Color;
            xmlsWriter.WriteAttributeString("style", $"fill:rgb({color.R},{color.G},{color.B});fill-opacity:{Math.Round(color.A / 255f, 3).ToString().Replace(",", ".")};");
            xmlsWriter.WriteEndElement();

            xmlsWriter.WriteStartElement("g");
            xmlsWriter.WriteAttributeString("transform", String.Format("translate({0},{1})", p((float)((width - resource.Width * sScale) / 2)), p((float)((height - resource.Height * sScale) / 2))));

            Render(map, xmlsWriter, animated, sScale);

            xmlsWriter.WriteEndElement();
        }

        public static void Render(StiMap map, XmlTextWriter xmlsWriter, bool animated, double sScale)
        {
            var mapData = map.GetMapData();
            var hashGroup = new Dictionary<string, StiMapGroup>();

            HeatmapInfo heatmapInfo = null;
            var colorsContainer = new StiStyleColorsContainer();
            InitData(mapData, hashGroup);

            if (map.MapType == StiMapType.Heatmap)
                heatmapInfo = new HeatmapInfo(map, mapData);

            if (map.MapType == StiMapType.Group)
                FillGroupColors(colorsContainer, map, hashGroup);            

            StiMapStyle mapStyle = null;
            if (!string.IsNullOrEmpty(map.ComponentStyle))
                mapStyle = map.Report.Styles[map.ComponentStyle] as StiMapStyle;

            if (mapStyle == null)
                mapStyle = StiMap.GetMapStyle(map.MapStyle);
            Brush defaultBrush = new SolidBrush(mapStyle.DefaultColor);
            StiSolidBrush defaultBrush1 = new StiSolidBrush(mapStyle.DefaultColor);

            HeatmapWithGroupInfo heatmapWithGroupInfo = null;
            if (map.MapType == StiMapType.HeatmapWithGroup)
                heatmapWithGroupInfo = new HeatmapWithGroupInfo(defaultBrush, defaultBrush1, map, mapData);

            var svgContainer = StiMapLoader.LoadResource(map.MapID.ToString());
            var labels = new Hashtable();
            using (var img = new Bitmap(1, 1))
            {
                using (var graphics = Graphics.FromImage(img))
                {
                    if (svgContainer != null)
                    {
                        var count = 0;
                        var sCount = svgContainer.HashPaths.Keys.Count;
                        float individualStepValue = 0.5f / sCount;
                        float individualStep = individualStepValue;
                        var keys = new List<String>(svgContainer.HashPaths.Keys);
                        keys.Sort();
                        foreach (var key in keys)
                        {
                            var data = mapData[0];
                            foreach (var md in mapData)
                            {
                                if (md.Key == key)
                                    data = md;
                            }
                            var fill = GetFill(data, map, heatmapInfo, hashGroup, individualStep, mapStyle, heatmapWithGroupInfo, defaultBrush);
                            individualStep += individualStepValue;
                            xmlsWriter.WriteStartElement("path");
                            xmlsWriter.WriteAttributeString("d", svgContainer.HashPaths[key].Data);

                            var style = new StringBuilder();
                            style.Append(GetFillBrush(fill as SolidBrush));

                            var stroke = $"{GetBorderStroke(mapStyle.BorderColor)}";
                            style.Append($"{stroke};stroke-width:{p(mapStyle.BorderSize)};");

                            xmlsWriter.WriteAttributeString("style", style.ToString());
                            xmlsWriter.WriteAttributeString("transform", $"scale({p(sScale)})");
                            xmlsWriter.WriteAttributeString("_ismap", "true");
                            xmlsWriter.WriteAttributeString("_text1", key);
                            xmlsWriter.WriteAttributeString("_text2", data.Value != null ? data.Value.ToString() : string.Empty);

                            var color = (fill as SolidBrush).Color;
                            xmlsWriter.WriteAttributeString("_color", $"#{color.R:X2}{color.G:X2}{color.B:X2}");

                            if (animated)
                            {
                                xmlsWriter.WriteAttributeString("opacity", "0");
                                xmlsWriter.WriteAttributeString("_animation",
                                    string.Format("{{\"actions\":[[\"opacity\", 0, 1, \"\"], [\"scale\", {2}, {2},\"\"]], \"begin\":{0}, \"duration\":{1}}}",
                                    p(200 / sCount * count), "100", p(sScale)));
                            }
                            xmlsWriter.WriteEndElement();

                            if ((map.ShowValue || map.DisplayNameType != StiDisplayNameType.None) && sScale >= 0.2)
                            {
                                labels[key] = data;
                            }
                            count++;
                        }
                        var typeface = new Font("Calibri", 18);
                        var foregrounds = new Color[2] { Color.FromArgb(180, 251, 251, 251), Color.FromArgb(255, 37, 37, 37) };
                        foreach (string key in labels.Keys)
                        {                            
                            string text = null;
                            var info = labels[key] as StiMapData;
                            var path = svgContainer.HashPaths[key];
                            
                            switch (map.DisplayNameType)
                            {
                                case StiDisplayNameType.Full:
                                    {
                                        text = (info != null) ? info.Name : path.EnglishName;
                                    }
                                    break;

                                case StiDisplayNameType.Short:
                                    {
                                        text = StiMapHelper.PrepareIsoCode(path.ISOCode);
                                    }
                                    break;
                            }
                            if (map.ShowValue)
                            {
                                if (info != null && info.Value != null)
                                {
                                    if (text == null)
                                    {
                                        text = info.Value.ToString();
                                    }
                                    else
                                    {
                                        text += "\r" + Environment.NewLine;
                                        text += info.Value.ToString();
                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(text))
                            {
                                var bounds = path.Rect;
                                SizeF textSize = (path.SetMaxWidth)
                                    ? graphics.MeasureString(text, typeface, bounds.Width)
                                    : graphics.MeasureString(text, typeface, bounds.Width);

                                #region Calc position
                                int x = bounds.X + (int)((bounds.Width - textSize.Width) / 2);
                                int y = bounds.Y + (int)((bounds.Height - textSize.Height) / 2 + 40 * sScale);
                                if (path.HorAlignment != null)
                                {
                                    switch (path.HorAlignment.GetValueOrDefault())
                                    {
                                        case StiTextHorAlignment.Left:
                                            x = bounds.X;
                                            break;

                                        case StiTextHorAlignment.Right:
                                            x = bounds.Right - (int)textSize.Width;
                                            break;
                                    }
                                }

                                if (path.VertAlignment != null)
                                {
                                    switch (path.VertAlignment.GetValueOrDefault())
                                    {
                                        case StiVertAlignment.Top:
                                            y = bounds.Y;
                                            break;

                                        case StiVertAlignment.Bottom:
                                            y = bounds.Bottom - (int)textSize.Height;
                                            break;
                                    }
                                }
                                #endregion

                                for (var step = 0; step < 2; step++)
                                {
                                    xmlsWriter.WriteStartElement("text");
                                    xmlsWriter.WriteAttributeString("font-size", p(26 * sScale));
                                    xmlsWriter.WriteAttributeString("font-family", "Calibri");
                                    if (animated)
                                    {
                                        xmlsWriter.WriteAttributeString("opacity", "0");
                                        xmlsWriter.WriteAttributeString("_animation",
                                            string.Format("{{\"actions\":[[\"opacity\", 0, 1, \"\"], [\"scale\", {2}, {2},\"\"]], \"begin\":{0}, \"duration\":{1}}}",
                                            p(200 / sCount * count), "100", p(sScale)));
                                    }
                                    xmlsWriter.WriteAttributeString("transform", string.Format("translate({0}, {1})", p((x ) * sScale - step) , p((y ) * sScale - step)));
                                    xmlsWriter.WriteAttributeString("style", string.Format("fill:#{0:X2}{1:X2}{2:X2};pointer-events:none", foregrounds[step].R, foregrounds[step].G, foregrounds[step].B));
                                    if (path.SetMaxWidth)
                                    {
                                        var words = Regex.Split(text, @"[ ]|\r\n");
                                        var index = 0;
                                        while (index < words.Length)
                                        {
                                            var rectTemp = new SizeF(0, 0);
                                            var length = 1;
                                            var txt = words[index];
                                            var fitTxt = txt;
                                            rectTemp = graphics.MeasureString(txt, typeface);
                                            while (rectTemp.Width < textSize.Width + 20 * sScale && index + length < words.Length && !fitTxt.EndsWith("\r"))
                                            {
                                                fitTxt = txt;
                                                txt += " " + words[index + length];
                                                rectTemp = graphics.MeasureString(txt, typeface);
                                                length++;
                                            }
                                            xmlsWriter.WriteStartElement("tspan");
                                            xmlsWriter.WriteAttributeString("x", "0");
                                            if (index == 0)
                                                xmlsWriter.WriteAttributeString("y", "0");
                                            else
                                                xmlsWriter.WriteAttributeString("dy", p(rectTemp.Height * sScale));
                                            xmlsWriter.WriteString(fitTxt);
                                            xmlsWriter.WriteEndElement();
                                            index = index + length - (length == 1 ? 0 : 1);
                                        }
                                    }
                                    else
                                    {
                                        var lines = Regex.Split(text, @"\r\n");
                                        for (var i = 0; i < lines.Length; i++)
                                        {
                                            var rectTemp = graphics.MeasureString(lines[i], typeface);
                                            xmlsWriter.WriteStartElement("tspan");
                                            xmlsWriter.WriteAttributeString("x", "0");
                                            if (i == 0)
                                                xmlsWriter.WriteAttributeString("y", "0");
                                            else
                                                xmlsWriter.WriteAttributeString("dy", p(rectTemp.Height * sScale));
                                            xmlsWriter.WriteString(lines[i]);
                                            xmlsWriter.WriteEndElement();
                                        }
                                    }
                                    xmlsWriter.WriteEndElement();
                                }
                            }
                        }                        
                    }
                }
            }
            if (mapData != null)
                mapData.Clear();

            hashGroup.Clear();
        }

        private static String p(double f)
        {
            return f.ToString().Replace(",", ".");
        }

        private static string GetBorderStroke(Color color)
        {
            var result = $"stroke:rgb({color.R},{color.G},{color.B});";

            var alfa = Math.Round(color.A / 255f, 3);

            if (alfa != 1)
                result += $"stroke-opacity:{alfa.ToString().Replace(",", ".")};";

            return result;
        }

        public static string GetFillBrush(SolidBrush brush)
        {
            var color = brush.Color;
            return $"fill:rgb({color.R},{color.G},{color.B});fill-opacity:{Math.Round(color.A / 255f, 3).ToString().Replace(",", ".")};";
        }

        private static Brush GetFill(StiMapData mapData, StiMap map, HeatmapInfo heatmapInfo, Dictionary<string, StiMapGroup> hashGroup, float individualStep, StiMapStyle mapStyle, HeatmapWithGroupInfo heatmapWithGroupInfo, Brush defaultBrush)
        {
            Brush brush = null;
            if (map.MapType == StiMapType.Individual)
            {
                if (map.ColorEach)
                {
                    var brush2 = ParseHexColor(mapData.Color);
                    if (brush != null) return brush;

                    return new SolidBrush(noneInfo.GetBrush().Color);
                }
                else
                {
                    return new SolidBrush(StiColorUtils.ChangeLightness(mapStyle.IndividualColor, individualStep));
                }
            }

            switch (map.MapType)
            {
                case StiMapType.Individual:
                    return GetNoneBrush(mapData, map);

                case StiMapType.Group:
                    return (mapData.Group != null && hashGroup.ContainsKey(mapData.Group))
                                ? hashGroup[mapData.Group].Fill
                                : new SolidBrush(mapStyle.DefaultColor);            

                case StiMapType.HeatmapWithGroup:
                    if (mapData.Group == null)
                        return new SolidBrush(mapStyle.DefaultColor);
                    else
                        return heatmapWithGroupInfo.GetBrush(mapData);

                case StiMapType.Heatmap:
                    if (mapData.Value == null)
                        return defaultBrush;
                    else
                        return heatmapInfo.GetBrush(mapData, defaultBrush);
            }

            if (brush != null)
                return brush;

            return new SolidBrush(mapStyle.BackColor);
        }

        private static Brush ParseHexColor(string color)
        {
            try
            {
                if (!string.IsNullOrEmpty(color))
                {
                    if (color.StartsWith("#", StringComparison.InvariantCulture))
                        return new SolidBrush(ColorTranslator.FromHtml(color));
                    else
                        return new SolidBrush(Color.FromName(color));
                }
            }
            catch { }

            return null;
        }

        private static Brush GetGroupedBrush(StiMapData mapData, Dictionary<string, StiMapGroup> hashGroup, StiMap map)
        {
            if (!string.IsNullOrEmpty(mapData.Color))
            {
                try
                {
                    return mapData.Color.StartsWith("#") 
                        ? new SolidBrush(ColorTranslator.FromHtml(mapData.Color)) 
                        : new SolidBrush(Color.FromName(mapData.Color));
                }
                catch
                {

                }
            }

            if (mapData.Group != null && hashGroup.ContainsKey(mapData.Group))
                return hashGroup[mapData.Group].Fill;

            StiMapStyle mapStyle = null;

            if (!string.IsNullOrEmpty(map.ComponentStyle))
                mapStyle = map.Report.Styles[map.ComponentStyle] as StiMapStyle;

            if (mapStyle != null)
                mapStyle = StiMap.GetMapStyle(map.MapStyle);

            return new SolidBrush(mapStyle.BackColor);
        }

        private static Brush GetNoneBrush(StiMapData mapData, StiMap map)
        {
            if (!string.IsNullOrEmpty(mapData.Color))
            {
                try
                {
                    return mapData.Color.StartsWith("#") 
                        ? new SolidBrush(ColorTranslator.FromHtml(mapData.Color)) 
                        : new SolidBrush(Color.FromName(mapData.Color));
                }
                catch
                {

                }
            }

            StiMapStyle mapStyle = null;

            if (!string.IsNullOrEmpty(map.ComponentStyle))
                mapStyle = map.Report.Styles[map.ComponentStyle] as StiMapStyle;

            if (mapStyle != null)
                mapStyle = StiMap.GetMapStyle(map.MapStyle);

            return new SolidBrush(mapStyle.BackColor);
        }

        private static void FillGroupColors(StiStyleColorsContainer colorsContainer, StiMap map, Dictionary<string, StiMapGroup> hashGroup)
        {
            colorsContainer.Init(map);

            var keys = new string[hashGroup.Keys.Count];
            hashGroup.Keys.CopyTo(keys, 0);

            var sortedKeys = new List<string>(keys);
            sortedKeys.Sort();

            foreach (var key in sortedKeys)
            {
                var value = hashGroup[key];
                value.Fill = colorsContainer.GetColor();
                value.Fill1 = colorsContainer.GetColor1();
            }
        }

        private static void InitData(List<StiMapData> mapData, Dictionary<string, StiMapGroup> hashGroup)
        {
            foreach (var item in mapData)
            {
                if (!string.IsNullOrEmpty(item.Group))
                {
                    if (item.Value == null) continue;
                    double value = 0d;
                    if (!double.TryParse(item.Value, out value)) continue;

                    StiMapGroup group = null;
                    if (!hashGroup.ContainsKey(item.Group))
                    {
                        group = new StiMapGroup
                        {
                            MinValue = value,
                            MaxValue = value
                        };

                        hashGroup.Add(item.Group, group);
                    }
                    else
                    {
                        group = hashGroup[item.Group];

                        if (group.MinValue > value)
                            group.MinValue = value;

                        else if (group.MaxValue < value)
                            group.MaxValue = value;
                    }
                }
            }
        }

        #endregion
    }
}
