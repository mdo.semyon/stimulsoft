﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft   							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft 

using Stimulsoft.Base;
using Stimulsoft.Base.Context;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Licenses;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Maps.Geoms;
using Stimulsoft.Base.Services;
using Stimulsoft.Gauge.Painters;
using Stimulsoft.Report.BarCodes;
using Stimulsoft.Report.Chart;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.ShapeTypes;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.Gauge;
using Stimulsoft.Report.Gauge.GaugeGeoms;
using Stimulsoft.Report.Gauge.Helpers;
using Stimulsoft.Report.Maps;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.WCFService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace Stimulsoft.Report.Export
{
    /// <summary>
    /// Class for the Pdf export.
    /// </summary>
	[SuppressUnmanagedCodeSecurity]
    [StiServiceBitmap(typeof(StiExportService), "Stimulsoft.Report.Images.Exports.Pdf.png")]
    public partial class StiPdfExportService : StiExportService
    {
        #region StiExportService override
        /// <summary>
		/// Gets or sets a default extension of export. 
		/// </summary>
		public override string DefaultExtension
        {
            get
            {
                return "pdf";
            }
        }


        public override StiExportFormat ExportFormat
        {
            get
            {
                return StiExportFormat.Pdf;
            }
        }

        /// <summary>
        /// Gets a group of the export in the context menu.
        /// </summary>
        public override string GroupCategory
        {
            get
            {
                return "Document";
            }
        }

        /// <summary>
        /// Gets a position of the export in the context menu.
        /// </summary>
        public override int Position
        {
            get
            {
                return (int)StiExportPosition.Pdf;
            }
        }

        /// <summary>
        /// Gets a name of the export in menu.
        /// </summary>
		public override string ExportNameInMenu
        {
            get
            {
                return StiLocalization.Get("Export", "ExportTypePdfFile");
            }
        }

        /// <summary>
        /// Returns a filter for the pdf files.
        /// </summary>
        /// <returns>Returns a filter for the Pdf files.</returns>
		public override string GetFilter()
        {
            return StiLocalization.Get("FileFilters", "PdfFiles");
        }

        /// <summary>
        /// Exports a document to the stream without dialog of the saving file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream in which report will be exported.</param>
        /// <param name="settings">A settings for the report exporting.</param>
        public override void ExportTo(StiReport report, Stream stream, StiExportSettings settings)
        {
            ExportPdf(report, stream, settings as StiPdfExportSettings);
        }

        /// <summary>
        /// Exports rendered report to a pdf file.
        /// Also file may be sent via e-mail.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="fileName">A name of the file for exporting a rendered report.</param>
        /// <param name="sendEMail">A parameter indicating whether the exported report will be sent via e-mail.</param>
        public override void Export(StiReport report, string fileName, bool sendEMail, StiGuiMode guiMode)
        {
            using (var form = StiGuiOptions.GetExportFormRunner("StiPdfExportSetupForm", guiMode, this.OwnerWindow))
            {
                form["CurrentPage"] = report.CurrentPrintPage;
                form["OpenAfterExportEnabled"] = !sendEMail;

                this.report = report;
                this.fileName = fileName;
                this.sendEMail = sendEMail;
                this.guiMode = guiMode;

                form.Complete += form_Complete;
                form.ShowDialog();
            }
        }

        internal StiReport report;
        private string fileName;
        private bool sendEMail;
        private StiGuiMode guiMode;
        private void form_Complete(IStiFormRunner form, StiShowDialogCompleteEvetArgs e)
        {
            if (e.DialogResult)
            {
                if (StiOptions.WCFService.WCFExportDocumentEventIsUsed)
                {
                    StiOptions.WCFService.InvokeWCFExportDocument(report, StiExportSettingsHelper.GetPdfExportSettings(form, report), "pdf");
                    return;
                }

                if (string.IsNullOrEmpty(fileName))
                    fileName = base.GetFileName(report, sendEMail);

                if (fileName != null)
                {
                    StiFileUtils.ProcessReadOnly(fileName);
                    var stream = new FileStream(fileName, FileMode.Create);

                    StartProgress(guiMode);

                    var settings = new StiPdfExportSettings
                    {
                        PageRange = form["PagesRange"] as StiPagesRange,
                        ImageQuality = (float)form["ImageQuality"],
                        ImageCompressionMethod = (StiPdfImageCompressionMethod)form["ImageCompressionMethod"],
                        ImageResolution = (float)form["Resolution"],
                        EmbeddedFonts = (bool)form["EmbeddedFonts"] & (bool)form["EmbeddedFontsEnabled"],
                        ExportRtfTextAsImage = (bool)form["ExportRtfTextAsImage"],
                        PasswordInputUser = form["UserPassword"] as string,
                        PasswordInputOwner = form["OwnerPassword"] as string,
                        UserAccessPrivileges = (StiUserAccessPrivileges)form["UserAccessPrivileges"],
                        KeyLength = (StiPdfEncryptionKeyLength)form["EncryptionKeyLength"],
                        GetCertificateFromCryptoUI = (bool)form["GetCertificateFromCryptoUI"],
                        UseDigitalSignature = (bool)form["UseDigitalSignature"],
                        SubjectNameString = form["SubjectNameString"] as string,
                        PdfComplianceMode = (StiPdfComplianceMode)form["PdfComplianceMode"],
                        ImageFormat = (StiImageFormat)form["ImageFormat"],
                        DitheringType = (StiMonochromeDitheringType)form["MonochromeDitheringType"],
                        AllowEditable = (StiPdfAllowEditable)form["AllowEditable"],
                        ImageResolutionMode = (StiImageResolutionMode)form["ImageResolutionMode"]
                    };

                    base.StartExport(report, stream, settings, sendEMail, (bool)form["OpenAfterExport"], fileName, guiMode);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating a number of files in exported document as a result of export
        /// of one page of the rendered report.
        /// </summary>
		public override bool MultipleFiles
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region struct StiPdfData
        /// <summary>
        /// Inner representation of the export objects.
        /// </summary>
        internal struct StiPdfData
        {
            /// <summary>
            /// Coordinate of the border.
            /// </summary>
            public double X;

            /// <summary>
            /// Y coordinate of the border.
            /// </summary>
            public double Y;

            /// <summary>
            /// Width of the border.
            /// </summary>
            public double Width;

            /// <summary>
            /// Height of the border.
            /// </summary>
            public double Height;

            /// <summary>
            /// Component.
            /// </summary>
            public StiComponent Component;

            public double Right
            {
                get
                {
                    return X + Width;
                }
            }
            public double Top
            {
                get
                {
                    return Y + Height;
                }
            }
        }
        #endregion

        #region StiImageData
        private struct StiImageData
        {
            public int Width;
            public int Height;
            public string Name;
            public StiImageFormat ImageFormat;
        }
        #endregion

        #region StiShadingData
        private struct StiShadingData
        {
            public double X;
            public double Y;
            public double Width;
            public double Height;
            public int Page;
            public Color Color1;
            public Color Color2;
            public double Angle;
            public bool IsGlare;
        }
        #endregion

        #region Variables definition
        private float imageQuality = 0.75f;
        private float imageResolutionMain = 1f;
        private StiImageResolutionMode imageResolutionMode = StiImageResolutionMode.Exactly;
        private StreamWriter sw = null;
        private Stream stream2 = null;
        internal MemoryStream memoryPageStream = null;
        internal StreamWriter pageStream = null;
        private global::System.Text.Encoder enc = null;
        private ArrayList imageList = null;
        private StiImageCache imageCache = null;
        private Hashtable imageInterpolationTable = null;
        private Hashtable imageCacheIndexToList = null;
        private Hashtable imageInfoList = null;
        private int imageInfoCounter = 0;
        private int imagesCounter;
        private int imagesCurrent;
        private int fontsCounter;
        private int bookmarksCounter;
        //private int patternsCounter;
        private int linksCounter;
        private int annotsCounter;
        private int annotsCurrent;
        private int annots2Counter;
        private int annots2Current;
        private int unsignedSignaturesCounter;
        private int shadingCounter;
        private int shadingCurrent;
        private int hatchCounter;
        private int tooltipsCounter;
        private CultureInfo currentCulture = null;
        private NumberFormatInfo currentNumberFormat = null;
        private string[] colorTable = new string[256];
        private bool[] alphaTable = new bool[256];
        internal PdfFonts pdfFont = null;
        internal StiBidirectionalConvert bidi = null;
        private int precision_digits;
        private bool clipLongTextLines;
        private bool standardPdfFonts = true;
        private bool embeddedFonts = false;
        private bool useUnicodeMode = false;
        private bool reduceFontSize = true;
        private bool compressed = false;
        private bool compressedFonts = false;
        private bool encrypted = false;
        private bool usePdfA = false;
        private StiPdfComplianceMode pdfComplianceMode = StiPdfComplianceMode.None;
        private bool exportRtfTextAsImage = false;
        private StiPdfAutoPrintMode autoPrint = StiPdfAutoPrintMode.None;
        private StiPdfImageCompressionMethod imageCompressionMethod = StiPdfImageCompressionMethod.Jpeg;
        private StiImageFormat imageFormat = StiImageFormat.Color;
        private StiMonochromeDitheringType monochromeDitheringType = StiMonochromeDitheringType.FloydSteinberg;
        private StiPdfAllowEditable allowEditable = StiPdfAllowEditable.No;
        private List<StiPdfEmbeddedFileData> embeddedFiles = null;
        private bool useZUGFeRD = false;
        private bool useTransparency = true;

        private bool[] fontGlyphsReduceNotNeed = null;
        private SortedList xref;
        private ArrayList bookmarksTree;
        private ArrayList bookmarksTreeTemp;
        private ArrayList linksArray;
        private List<StiLinkObject> tagsArray;
        private List<StiLinkObject> tooltipsArray;
        private ArrayList annotsArray;
        private List<StiEditableObject> annots2Array;
        private List<StiEditableObject> unsignedSignaturesArray;
        private ArrayList shadingArray;
        private ArrayList hatchArray;
        private bool haveBookmarks = false;
        private bool haveLinks = false;
        private bool haveAnnots = false;
        private bool haveDigitalSignature = false;
        private bool haveTooltips = false;
        private int offsetSignatureData = 0;
        private int offsetSignatureFilter = 0;
        private int offsetSignatureName = 0;
        private int offsetSignatureLen2 = 0;

        private static int[] CodePage1252part80AF = {
            0x20AC, 0x2022, 0x201A, 0x0192, 0x201E, 0x2026, 0x2020, 0x2021, 0x02C6, 0x2030, 0x0160, 0x2039, 0x0152, 0x2022, 0x017D, 0x2022,
            0x2022, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014, 0x02DC, 0x2122, 0x0161, 0x203A, 0x0153, 0x2022, 0x017E, 0x0178 };
        private int[] CodePage1252 = new int[256];

        private const double hiToTwips = 0.72;  //convert from hi(hundredths of inch) to points
        private int precision_digits_font = 3;
        private const float pdfCKT = 0.55228f;     //pdf circle round koefficient
        private const float italicAngleTanValue = 0.325f;   //18 degrees
        private const float boldFontStrokeWidthValue = 0.031f;

        private byte[] IDValue = null;
        private string IDValueString = string.Empty;
        private string IDValueStringMeta = string.Empty;
        private string currentDateTime = string.Empty;
        private string currentDateTimeMeta = string.Empty;
        private string producerName = "Stimulsoft Reports";
        private string creatorName = string.Empty;
        private string keywords = string.Empty;

        private int currentObjectNumber;
        private int currentGenerationNumber;
        StiPdfEncryptionKeyLength keyLength = StiPdfEncryptionKeyLength.Bit40;
        private byte lastColorStrokeA = 0xFF;
        private byte lastColorNonStrokeA = 0xFF;
        private Stack colorStack = null;

        private const int signatureDataLen = 16384;
        private RectangleD signaturePlacement;
        private int signaturePageNumber = 0;
        private string digitalSignatureReason = null;
        private string digitalSignatureLocation = null;
        private string digitalSignatureContactInfo = null;
        private string digitalSignatureSignedBy = null;

        private Graphics graphicsForTextRenderer = null;
        private Image imageForGraphicsForTextRenderer = null;

        private StiPdfStructure info = null;

        private Tools.StiPdfMetafileRender mfRender = null;
        private Tools.StiPdfSecurity pdfSecurity = null;

        private bool isWpf = false;
        private static bool isWpfException = false;

        private static string[] hatchData =
            {
                "000000FF00000000",	//HatchStyleHorizontal = 0
				"1010101010101010",	//HatchStyleVertical = 1,			
				"8040201008040201",	//HatchStyleForwardDiagonal = 2,	
				"0102040810204080",	//HatchStyleBackwardDiagonal = 3,	
				"101010FF10101010",	//HatchStyleCross = 4,			
				"8142241818244281",	//HatchStyleDiagonalCross = 5,	
				"8000000008000000",	//HatchStyle05Percent = 6,		
				"0010000100100001",	//HatchStyle10Percent = 7,		
				"2200880022008800",	//HatchStyle20Percent = 8,		
				"2288228822882288",	//HatchStyle25Percent = 9,		
				"2255885522558855",	//HatchStyle30Percent = 10,		
				"AA558A55AA55A855",	//HatchStyle40Percent = 11,		
				"AA55AA55AA55AA55",	//HatchStyle50Percent = 12,		
				"BB55EE55BB55EE55",	//HatchStyle60Percent = 13,		
				"DD77DD77DD77DD77",	//HatchStyle70Percent = 14,		
				"FFDDFF77FFDDFF77",	//HatchStyle75Percent = 15,		
				"FF7FFFF7FF7FFFF7",	//HatchStyle80Percent = 16,		
				"FF7FFFFFFFF7FFFF",	//HatchStyle90Percent = 17,		
				"8844221188442211",	//HatchStyleLightDownwardDiagonal = 18,	
				"1122448811224488",	//HatchStyleLightUpwardDiagonal = 19,	
				"CC663399CC663399",	//HatchStyleDarkDownwardDiagonal = 20,	
				"993366CC993366CC",	//HatchStyleDarkUpwardDiagonal = 21,	
				"E070381C0E0783C1",	//HatchStyleWideDownwardDiagonal = 22,	
				"C183070E1C3870E0",	//HatchStyleWideUpwardDiagonal = 23,	
				"4040404040404040",	//HatchStyleLightVertical = 24,			
				"00FF000000FF0000",	//HatchStyleLightHorizontal = 25,		
				"AAAAAAAAAAAAAAAA",	//HatchStyleNarrowVertical = 26,		
				"FF00FF00FF00FF00",	//HatchStyleNarrowHorizontal = 27,		
				"CCCCCCCCCCCCCCCC",	//HatchStyleDarkVertical = 28,			
				"FFFF0000FFFF0000",	//HatchStyleDarkHorizontal = 29,		
				"8844221100000000",	//HatchStyleDashedDownwardDiagonal = 30,
				"1122448800000000",	//HatchStyleDashedUpwardDiagonal = 311,	
				"F00000000F000000",	//HatchStyleDashedHorizontal = 32,		
				"8080808008080808",	//HatchStyleDashedVertical = 33,		
				"0240088004200110",	//HatchStyleSmallConfetti = 34,			
				"0C8DB130031BD8C0",	//HatchStyleLargeConfetti = 35,		
				"8403304884033048",	//HatchStyleZigZag = 36,			
				"00304A8100304A81",	//HatchStyleWave = 37,				
				"0102040818244281",	//HatchStyleDiagonalBrick = 38,		
				"202020FF020202FF",	//HatchStyleHorizontalBrick = 39,	
				"1422518854224588",	//HatchStyleWeave = 40,				
				"F0F0F0F0AA55AA55",	//HatchStylePlaid = 41,				
				"0100201020000102",	//HatchStyleDivot = 42,				
				"AA00800080008000",	//HatchStyleDottedGrid = 43,		
				"0020008800020088",	//HatchStyleDottedDiamond = 44,		
				"8448300C02010103",	//HatchStyleShingle = 45,			
				"33FFCCFF33FFCCFF",	//HatchStyleTrellis = 46,			
				"98F8F877898F8F77",	//HatchStyleSphere = 47,			
				"111111FF111111FF",	//HatchStyleSmallGrid = 48,			
				"3333CCCC3333CCCC",	//HatchStyleSmallCheckerBoard = 49,	
				"0F0F0F0FF0F0F0F0",	//HatchStyleLargeCheckerBoard = 50,	
				"0502058850205088",	//HatchStyleOutlinedDiamond = 51,	
				"10387CFE7C381000",	//HatchStyleSolidDiamond = 52,
				"0000000000000000"	//HatchStyleTotal = 53
			};

        #region sRGBprofile
        private static byte[] sRGBprofile = {
              0,   0,  12,  72,  76, 105, 110, 111,   2,  16,   0,   0, 109, 110, 116, 114,
             82,  71,  66,  32,  88,  89,  90,  32,   7, 206,   0,   2,   0,   9,   0,   6,
              0,  49,   0,   0,  97,  99, 115, 112,  77,  83,  70,  84,   0,   0,   0,   0,
             73,  69,  67,  32, 115,  82,  71,  66,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0, 246, 214,   0,   1,   0,   0,   0,   0, 211,  45,
             72,  80,  32,  32,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,  17,  99, 112, 114, 116,   0,   0,   1,  80,   0,   0,   0,  51,
            100, 101, 115,  99,   0,   0,   1, 132,   0,   0,   0, 108, 119, 116, 112, 116,
              0,   0,   1, 240,   0,   0,   0,  20,  98, 107, 112, 116,   0,   0,   2,   4,
              0,   0,   0,  20, 114,  88,  89,  90,   0,   0,   2,  24,   0,   0,   0,  20,
            103,  88,  89,  90,   0,   0,   2,  44,   0,   0,   0,  20,  98,  88,  89,  90,
              0,   0,   2,  64,   0,   0,   0,  20, 100, 109, 110, 100,   0,   0,   2,  84,
              0,   0,   0, 112, 100, 109, 100, 100,   0,   0,   2, 196,   0,   0,   0, 136,
            118, 117, 101, 100,   0,   0,   3,  76,   0,   0,   0, 134, 118, 105, 101, 119,
              0,   0,   3, 212,   0,   0,   0,  36, 108, 117, 109, 105,   0,   0,   3, 248,
              0,   0,   0,  20, 109, 101,  97, 115,   0,   0,   4,  12,   0,   0,   0,  36,
            116, 101,  99, 104,   0,   0,   4,  48,   0,   0,   0,  12, 114,  84,  82,  67,
              0,   0,   4,  60,   0,   0,   8,  12, 103,  84,  82,  67,   0,   0,   4,  60,
              0,   0,   8,  12,  98,  84,  82,  67,   0,   0,   4,  60,   0,   0,   8,  12,
            116, 101, 120, 116,   0,   0,   0,   0,  67, 111, 112, 121, 114, 105, 103, 104,
            116,  32,  40,  99,  41,  32,  49,  57,  57,  56,  32,  72, 101, 119, 108, 101,
            116, 116,  45,  80,  97,  99, 107,  97, 114, 100,  32,  67, 111, 109, 112,  97,
            110, 121,   0,   0, 100, 101, 115,  99,   0,   0,   0,   0,   0,   0,   0,  18,
            115,  82,  71,  66,  32,  73,  69,  67,  54,  49,  57,  54,  54,  45,  50,  46,
             49,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  18, 115,  82,  71,
             66,  32,  73,  69,  67,  54,  49,  57,  54,  54,  45,  50,  46,  49,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
             88,  89,  90,  32,   0,   0,   0,   0,   0,   0, 243,  81,   0,   1,   0,   0,
              0,   1,  22, 204,  88,  89,  90,  32,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,  88,  89,  90,  32,   0,   0,   0,   0,
              0,   0, 111, 162,   0,   0,  56, 245,   0,   0,   3, 144,  88,  89,  90,  32,
              0,   0,   0,   0,   0,   0,  98, 153,   0,   0, 183, 133,   0,   0,  24, 218,
             88,  89,  90,  32,   0,   0,   0,   0,   0,   0,  36, 160,   0,   0,  15, 132,
              0,   0, 182, 207, 100, 101, 115,  99,   0,   0,   0,   0,   0,   0,   0,  22,
             73,  69,  67,  32, 104, 116, 116, 112,  58,  47,  47, 119, 119, 119,  46, 105,
            101,  99,  46,  99, 104,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
             22,  73,  69,  67,  32, 104, 116, 116, 112,  58,  47,  47, 119, 119, 119,  46,
            105, 101,  99,  46,  99, 104,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0, 100, 101, 115,  99,   0,   0,   0,   0,   0,   0,   0,  46,
             73,  69,  67,  32,  54,  49,  57,  54,  54,  45,  50,  46,  49,  32,  68, 101,
            102,  97, 117, 108, 116,  32,  82,  71,  66,  32,  99, 111, 108, 111, 117, 114,
             32, 115, 112,  97,  99, 101,  32,  45,  32, 115,  82,  71,  66,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,  46,  73,  69,  67,  32,  54,  49,  57,
             54,  54,  45,  50,  46,  49,  32,  68, 101, 102,  97, 117, 108, 116,  32,  82,
             71,  66,  32,  99, 111, 108, 111, 117, 114,  32, 115, 112,  97,  99, 101,  32,
             45,  32, 115,  82,  71,  66,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 100, 101, 115,  99,
              0,   0,   0,   0,   0,   0,   0,  44,  82, 101, 102, 101, 114, 101, 110,  99,
            101,  32,  86, 105, 101, 119, 105, 110, 103,  32,  67, 111, 110, 100, 105, 116,
            105, 111, 110,  32, 105, 110,  32,  73,  69,  67,  54,  49,  57,  54,  54,  45,
             50,  46,  49,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  44,  82,
            101, 102, 101, 114, 101, 110,  99, 101,  32,  86, 105, 101, 119, 105, 110, 103,
             32,  67, 111, 110, 100, 105, 116, 105, 111, 110,  32, 105, 110,  32,  73,  69,
             67,  54,  49,  57,  54,  54,  45,  50,  46,  49,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0, 118, 105, 101, 119,   0,   0,   0,   0,   0,  19, 164, 254,
              0,  20,  95,  46,   0,  16, 207,  20,   0,   3, 237, 204,   0,   4,  19,  11,
              0,   3,  92, 158,   0,   0,   0,   1,  88,  89,  90,  32,   0,   0,   0,   0,
              0,  76,   9,  86,   0,  80,   0,   0,   0,  87,  31, 231, 109, 101,  97, 115,
              0,   0,   0,   0,   0,   0,   0,   1,   0,   0,   0,   0,   0,   0,   0,   0,
              0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   2, 143,   0,   0,   0,   2,
            115, 105, 103,  32,   0,   0,   0,   0,  67,  82,  84,  32,  99, 117, 114, 118,
              0,   0,   0,   0,   0,   0,   4,   0,   0,   0,   0,   5,   0,  10,   0,  15,
              0,  20,   0,  25,   0,  30,   0,  35,   0,  40,   0,  45,   0,  50,   0,  55,
              0,  59,   0,  64,   0,  69,   0,  74,   0,  79,   0,  84,   0,  89,   0,  94,
              0,  99,   0, 104,   0, 109,   0, 114,   0, 119,   0, 124,   0, 129,   0, 134,
              0, 139,   0, 144,   0, 149,   0, 154,   0, 159,   0, 164,   0, 169,   0, 174,
              0, 178,   0, 183,   0, 188,   0, 193,   0, 198,   0, 203,   0, 208,   0, 213,
              0, 219,   0, 224,   0, 229,   0, 235,   0, 240,   0, 246,   0, 251,   1,   1,
              1,   7,   1,  13,   1,  19,   1,  25,   1,  31,   1,  37,   1,  43,   1,  50,
              1,  56,   1,  62,   1,  69,   1,  76,   1,  82,   1,  89,   1,  96,   1, 103,
              1, 110,   1, 117,   1, 124,   1, 131,   1, 139,   1, 146,   1, 154,   1, 161,
              1, 169,   1, 177,   1, 185,   1, 193,   1, 201,   1, 209,   1, 217,   1, 225,
              1, 233,   1, 242,   1, 250,   2,   3,   2,  12,   2,  20,   2,  29,   2,  38,
              2,  47,   2,  56,   2,  65,   2,  75,   2,  84,   2,  93,   2, 103,   2, 113,
              2, 122,   2, 132,   2, 142,   2, 152,   2, 162,   2, 172,   2, 182,   2, 193,
              2, 203,   2, 213,   2, 224,   2, 235,   2, 245,   3,   0,   3,  11,   3,  22,
              3,  33,   3,  45,   3,  56,   3,  67,   3,  79,   3,  90,   3, 102,   3, 114,
              3, 126,   3, 138,   3, 150,   3, 162,   3, 174,   3, 186,   3, 199,   3, 211,
              3, 224,   3, 236,   3, 249,   4,   6,   4,  19,   4,  32,   4,  45,   4,  59,
              4,  72,   4,  85,   4,  99,   4, 113,   4, 126,   4, 140,   4, 154,   4, 168,
              4, 182,   4, 196,   4, 211,   4, 225,   4, 240,   4, 254,   5,  13,   5,  28,
              5,  43,   5,  58,   5,  73,   5,  88,   5, 103,   5, 119,   5, 134,   5, 150,
              5, 166,   5, 181,   5, 197,   5, 213,   5, 229,   5, 246,   6,   6,   6,  22,
              6,  39,   6,  55,   6,  72,   6,  89,   6, 106,   6, 123,   6, 140,   6, 157,
              6, 175,   6, 192,   6, 209,   6, 227,   6, 245,   7,   7,   7,  25,   7,  43,
              7,  61,   7,  79,   7,  97,   7, 116,   7, 134,   7, 153,   7, 172,   7, 191,
              7, 210,   7, 229,   7, 248,   8,  11,   8,  31,   8,  50,   8,  70,   8,  90,
              8, 110,   8, 130,   8, 150,   8, 170,   8, 190,   8, 210,   8, 231,   8, 251,
              9,  16,   9,  37,   9,  58,   9,  79,   9, 100,   9, 121,   9, 143,   9, 164,
              9, 186,   9, 207,   9, 229,   9, 251,  10,  17,  10,  39,  10,  61,  10,  84,
             10, 106,  10, 129,  10, 152,  10, 174,  10, 197,  10, 220,  10, 243,  11,  11,
             11,  34,  11,  57,  11,  81,  11, 105,  11, 128,  11, 152,  11, 176,  11, 200,
             11, 225,  11, 249,  12,  18,  12,  42,  12,  67,  12,  92,  12, 117,  12, 142,
             12, 167,  12, 192,  12, 217,  12, 243,  13,  13,  13,  38,  13,  64,  13,  90,
             13, 116,  13, 142,  13, 169,  13, 195,  13, 222,  13, 248,  14,  19,  14,  46,
             14,  73,  14, 100,  14, 127,  14, 155,  14, 182,  14, 210,  14, 238,  15,   9,
             15,  37,  15,  65,  15,  94,  15, 122,  15, 150,  15, 179,  15, 207,  15, 236,
             16,   9,  16,  38,  16,  67,  16,  97,  16, 126,  16, 155,  16, 185,  16, 215,
             16, 245,  17,  19,  17,  49,  17,  79,  17, 109,  17, 140,  17, 170,  17, 201,
             17, 232,  18,   7,  18,  38,  18,  69,  18, 100,  18, 132,  18, 163,  18, 195,
             18, 227,  19,   3,  19,  35,  19,  67,  19,  99,  19, 131,  19, 164,  19, 197,
             19, 229,  20,   6,  20,  39,  20,  73,  20, 106,  20, 139,  20, 173,  20, 206,
             20, 240,  21,  18,  21,  52,  21,  86,  21, 120,  21, 155,  21, 189,  21, 224,
             22,   3,  22,  38,  22,  73,  22, 108,  22, 143,  22, 178,  22, 214,  22, 250,
             23,  29,  23,  65,  23, 101,  23, 137,  23, 174,  23, 210,  23, 247,  24,  27,
             24,  64,  24, 101,  24, 138,  24, 175,  24, 213,  24, 250,  25,  32,  25,  69,
             25, 107,  25, 145,  25, 183,  25, 221,  26,   4,  26,  42,  26,  81,  26, 119,
             26, 158,  26, 197,  26, 236,  27,  20,  27,  59,  27,  99,  27, 138,  27, 178,
             27, 218,  28,   2,  28,  42,  28,  82,  28, 123,  28, 163,  28, 204,  28, 245,
             29,  30,  29,  71,  29, 112,  29, 153,  29, 195,  29, 236,  30,  22,  30,  64,
             30, 106,  30, 148,  30, 190,  30, 233,  31,  19,  31,  62,  31, 105,  31, 148,
             31, 191,  31, 234,  32,  21,  32,  65,  32, 108,  32, 152,  32, 196,  32, 240,
             33,  28,  33,  72,  33, 117,  33, 161,  33, 206,  33, 251,  34,  39,  34,  85,
             34, 130,  34, 175,  34, 221,  35,  10,  35,  56,  35, 102,  35, 148,  35, 194,
             35, 240,  36,  31,  36,  77,  36, 124,  36, 171,  36, 218,  37,   9,  37,  56,
             37, 104,  37, 151,  37, 199,  37, 247,  38,  39,  38,  87,  38, 135,  38, 183,
             38, 232,  39,  24,  39,  73,  39, 122,  39, 171,  39, 220,  40,  13,  40,  63,
             40, 113,  40, 162,  40, 212,  41,   6,  41,  56,  41, 107,  41, 157,  41, 208,
             42,   2,  42,  53,  42, 104,  42, 155,  42, 207,  43,   2,  43,  54,  43, 105,
             43, 157,  43, 209,  44,   5,  44,  57,  44, 110,  44, 162,  44, 215,  45,  12,
             45,  65,  45, 118,  45, 171,  45, 225,  46,  22,  46,  76,  46, 130,  46, 183,
             46, 238,  47,  36,  47,  90,  47, 145,  47, 199,  47, 254,  48,  53,  48, 108,
             48, 164,  48, 219,  49,  18,  49,  74,  49, 130,  49, 186,  49, 242,  50,  42,
             50,  99,  50, 155,  50, 212,  51,  13,  51,  70,  51, 127,  51, 184,  51, 241,
             52,  43,  52, 101,  52, 158,  52, 216,  53,  19,  53,  77,  53, 135,  53, 194,
             53, 253,  54,  55,  54, 114,  54, 174,  54, 233,  55,  36,  55,  96,  55, 156,
             55, 215,  56,  20,  56,  80,  56, 140,  56, 200,  57,   5,  57,  66,  57, 127,
             57, 188,  57, 249,  58,  54,  58, 116,  58, 178,  58, 239,  59,  45,  59, 107,
             59, 170,  59, 232,  60,  39,  60, 101,  60, 164,  60, 227,  61,  34,  61,  97,
             61, 161,  61, 224,  62,  32,  62,  96,  62, 160,  62, 224,  63,  33,  63,  97,
             63, 162,  63, 226,  64,  35,  64, 100,  64, 166,  64, 231,  65,  41,  65, 106,
             65, 172,  65, 238,  66,  48,  66, 114,  66, 181,  66, 247,  67,  58,  67, 125,
             67, 192,  68,   3,  68,  71,  68, 138,  68, 206,  69,  18,  69,  85,  69, 154,
             69, 222,  70,  34,  70, 103,  70, 171,  70, 240,  71,  53,  71, 123,  71, 192,
             72,   5,  72,  75,  72, 145,  72, 215,  73,  29,  73,  99,  73, 169,  73, 240,
             74,  55,  74, 125,  74, 196,  75,  12,  75,  83,  75, 154,  75, 226,  76,  42,
             76, 114,  76, 186,  77,   2,  77,  74,  77, 147,  77, 220,  78,  37,  78, 110,
             78, 183,  79,   0,  79,  73,  79, 147,  79, 221,  80,  39,  80, 113,  80, 187,
             81,   6,  81,  80,  81, 155,  81, 230,  82,  49,  82, 124,  82, 199,  83,  19,
             83,  95,  83, 170,  83, 246,  84,  66,  84, 143,  84, 219,  85,  40,  85, 117,
             85, 194,  86,  15,  86,  92,  86, 169,  86, 247,  87,  68,  87, 146,  87, 224,
             88,  47,  88, 125,  88, 203,  89,  26,  89, 105,  89, 184,  90,   7,  90,  86,
             90, 166,  90, 245,  91,  69,  91, 149,  91, 229,  92,  53,  92, 134,  92, 214,
             93,  39,  93, 120,  93, 201,  94,  26,  94, 108,  94, 189,  95,  15,  95,  97,
             95, 179,  96,   5,  96,  87,  96, 170,  96, 252,  97,  79,  97, 162,  97, 245,
             98,  73,  98, 156,  98, 240,  99,  67,  99, 151,  99, 235, 100,  64, 100, 148,
            100, 233, 101,  61, 101, 146, 101, 231, 102,  61, 102, 146, 102, 232, 103,  61,
            103, 147, 103, 233, 104,  63, 104, 150, 104, 236, 105,  67, 105, 154, 105, 241,
            106,  72, 106, 159, 106, 247, 107,  79, 107, 167, 107, 255, 108,  87, 108, 175,
            109,   8, 109,  96, 109, 185, 110,  18, 110, 107, 110, 196, 111,  30, 111, 120,
            111, 209, 112,  43, 112, 134, 112, 224, 113,  58, 113, 149, 113, 240, 114,  75,
            114, 166, 115,   1, 115,  93, 115, 184, 116,  20, 116, 112, 116, 204, 117,  40,
            117, 133, 117, 225, 118,  62, 118, 155, 118, 248, 119,  86, 119, 179, 120,  17,
            120, 110, 120, 204, 121,  42, 121, 137, 121, 231, 122,  70, 122, 165, 123,   4,
            123,  99, 123, 194, 124,  33, 124, 129, 124, 225, 125,  65, 125, 161, 126,   1,
            126,  98, 126, 194, 127,  35, 127, 132, 127, 229, 128,  71, 128, 168, 129,  10,
            129, 107, 129, 205, 130,  48, 130, 146, 130, 244, 131,  87, 131, 186, 132,  29,
            132, 128, 132, 227, 133,  71, 133, 171, 134,  14, 134, 114, 134, 215, 135,  59,
            135, 159, 136,   4, 136, 105, 136, 206, 137,  51, 137, 153, 137, 254, 138, 100,
            138, 202, 139,  48, 139, 150, 139, 252, 140,  99, 140, 202, 141,  49, 141, 152,
            141, 255, 142, 102, 142, 206, 143,  54, 143, 158, 144,   6, 144, 110, 144, 214,
            145,  63, 145, 168, 146,  17, 146, 122, 146, 227, 147,  77, 147, 182, 148,  32,
            148, 138, 148, 244, 149,  95, 149, 201, 150,  52, 150, 159, 151,  10, 151, 117,
            151, 224, 152,  76, 152, 184, 153,  36, 153, 144, 153, 252, 154, 104, 154, 213,
            155,  66, 155, 175, 156,  28, 156, 137, 156, 247, 157, 100, 157, 210, 158,  64,
            158, 174, 159,  29, 159, 139, 159, 250, 160, 105, 160, 216, 161,  71, 161, 182,
            162,  38, 162, 150, 163,   6, 163, 118, 163, 230, 164,  86, 164, 199, 165,  56,
            165, 169, 166,  26, 166, 139, 166, 253, 167, 110, 167, 224, 168,  82, 168, 196,
            169,  55, 169, 169, 170,  28, 170, 143, 171,   2, 171, 117, 171, 233, 172,  92,
            172, 208, 173,  68, 173, 184, 174,  45, 174, 161, 175,  22, 175, 139, 176,   0,
            176, 117, 176, 234, 177,  96, 177, 214, 178,  75, 178, 194, 179,  56, 179, 174,
            180,  37, 180, 156, 181,  19, 181, 138, 182,   1, 182, 121, 182, 240, 183, 104,
            183, 224, 184,  89, 184, 209, 185,  74, 185, 194, 186,  59, 186, 181, 187,  46,
            187, 167, 188,  33, 188, 155, 189,  21, 189, 143, 190,  10, 190, 132, 190, 255,
            191, 122, 191, 245, 192, 112, 192, 236, 193, 103, 193, 227, 194,  95, 194, 219,
            195,  88, 195, 212, 196,  81, 196, 206, 197,  75, 197, 200, 198,  70, 198, 195,
            199,  65, 199, 191, 200,  61, 200, 188, 201,  58, 201, 185, 202,  56, 202, 183,
            203,  54, 203, 182, 204,  53, 204, 181, 205,  53, 205, 181, 206,  54, 206, 182,
            207,  55, 207, 184, 208,  57, 208, 186, 209,  60, 209, 190, 210,  63, 210, 193,
            211,  68, 211, 198, 212,  73, 212, 203, 213,  78, 213, 209, 214,  85, 214, 216,
            215,  92, 215, 224, 216, 100, 216, 232, 217, 108, 217, 241, 218, 118, 218, 251,
            219, 128, 220,   5, 220, 138, 221,  16, 221, 150, 222,  28, 222, 162, 223,  41,
            223, 175, 224,  54, 224, 189, 225,  68, 225, 204, 226,  83, 226, 219, 227,  99,
            227, 235, 228, 115, 228, 252, 229, 132, 230,  13, 230, 150, 231,  31, 231, 169,
            232,  50, 232, 188, 233,  70, 233, 208, 234,  91, 234, 229, 235, 112, 235, 251,
            236, 134, 237,  17, 237, 156, 238,  40, 238, 180, 239,  64, 239, 204, 240,  88,
            240, 229, 241, 114, 241, 255, 242, 140, 243,  25, 243, 167, 244,  52, 244, 194,
            245,  80, 245, 222, 246, 109, 246, 251, 247, 138, 248,  25, 248, 168, 249,  56,
            249, 199, 250,  87, 250, 231, 251, 119, 252,   7, 252, 152, 253,  41, 253, 186,
            254,  75, 254, 220, 255, 109, 255, 255};
        #endregion

        private static double fontCorrectValue
        {
            get
            {
                if (CompatibleMode160) return 1;
                else return 0.955;
            }
        }

        private static bool compatibleMode160 = false;
        public static bool CompatibleMode160
        {
            get
            {
                return compatibleMode160;
            }
            set
            {
                compatibleMode160 = value;
            }
        }

        private static bool printScaling = true;
        /// <summary>
        /// PrintScaling property shows, how Acrobat Reader must to use margins of the printer.
        /// if true, then default settings of the Acrobat Reader will be used (usually "Fit to printer margin")
        /// else PrintScaling parameter of the pdf file will be set to None.
        /// </summary>
		public static bool PrintScaling
        {
            get
            {
                return printScaling;
            }
            set
            {
                printScaling = value;
            }
        }
        #endregion

        #region GetHatchNumber
        /// <summary>
        /// Returns number of hatch in table of hatches.
        /// </summary>
        public int GetHatchNumber(StiHatchBrush brush)
        {
            if (hatchArray.Count > 0)
            {
                for (int index = 0; index < hatchArray.Count; index++)
                {
                    StiHatchBrush tempBrush = (StiHatchBrush)hatchArray[index];
                    if ((brush.Style == tempBrush.Style) &&
                        (brush.BackColor == tempBrush.BackColor) &&
                        (brush.ForeColor == tempBrush.ForeColor))
                    {
                        return index;
                    }
                }
            }
            //add hatch to table
            hatchArray.Add(brush);
            return hatchArray.Count - 1;
        }

        public int GetHatchNumber(HatchBrush brush)
        {
            return GetHatchNumber(new StiHatchBrush(brush.HatchStyle, brush.ForegroundColor, brush.BackgroundColor));
        }
        #endregion

        #region Render procedures

        #region IsWordWrapSymbol
        private bool IsWordWrapSymbol(StringBuilder sb, int index)
        {
            char sym1 = sb[index];
            if (((sym1 >= 0x3000) && (sym1 <= 0xd7af)) ||	//East Asian & Unified CJK
                (char.IsWhiteSpace(sym1) && (sym1 != 0xa0)) ||
                sym1 == '(' || sym1 == '{') return true;
            if (index > 0)
            {
                char sym2 = sb[index - 1];
                if (sym2 == '!' || sym2 == '%' || sym2 == ')' || sym2 == '}' || sym2 == '-' || sym2 == '?') return true;
            }
            return false;
        }

        //private bool IsWordWrapSymbol(StringBuilder sb, int index)
        //{
        //    char sym = ' ';
        //    if (index < sb.Length - 1) sym = sb[index + 1];
        //    return IsWordWrapSymbol(sb[index], sym);
        //}

        //private bool IsWordWrapSymbol(char sym1, char sym2)
        //{
        //    if ((sym1 >= 0x3000) && (sym1 <= 0xd7af)) return true;	//East Asian & Unified CJK
        //    UnicodeCategory cat1 = char.GetUnicodeCategory(sym1);
        //    UnicodeCategory cat2 = char.GetUnicodeCategory(sym2);
        //    return
        //        (cat1 == UnicodeCategory.OpenPunctuation) ||
        //        ((cat1 == UnicodeCategory.ClosePunctuation) && (cat2 != UnicodeCategory.OtherPunctuation)) ||
        //        (cat1 == UnicodeCategory.DashPunctuation) ||
        //        (cat1 == UnicodeCategory.InitialQuotePunctuation) ||
        //        (cat1 == UnicodeCategory.FinalQuotePunctuation) ||
        //        (cat1 == UnicodeCategory.SpaceSeparator) ||
        //        (char.IsWhiteSpace(sym1) ||
        //        sym1 == '!' || sym1 == '%' || sym1 == ',' || sym1 == '.' || sym1 == '/' || sym1 == ';');
        //}
        #endregion

        #region Get tabs size
        private float GetTabsSize(IStiTextOptions textOp, double sizeInPt, double currentPosition)
        {
            if ((textOp != null) && (textOp.TextOptions != null))
            {
                double position = currentPosition;

                float spaceWidth = 754f / (float)sizeInPt;      //empiric; for CourierNew must be 726 ?
                float otherTab = spaceWidth * textOp.TextOptions.DistanceBetweenTabs;
                float firstTab = spaceWidth * textOp.TextOptions.FirstTabOffset + otherTab;

                if (currentPosition < firstTab)
                {
                    position = firstTab;
                }
                else
                {
                    if (textOp.TextOptions.DistanceBetweenTabs > 0)
                    {
                        int kolTabs = (int)((currentPosition - firstTab) / otherTab);
                        kolTabs++;
                        position = firstTab + kolTabs * otherTab;
                    }
                }

                return (float)(position - currentPosition);
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region CheckGraphicsForTextRenderer
        private void CheckGraphicsForTextRenderer()
        {
            if (graphicsForTextRenderer == null)
            {
                if (StiOptions.Engine.FullTrust)
                {
                    graphicsForTextRenderer = Graphics.FromHwnd(IntPtr.Zero);
                }
                else
                {
                    imageForGraphicsForTextRenderer = new Bitmap(1, 1);
                    graphicsForTextRenderer = Graphics.FromImage(imageForGraphicsForTextRenderer);
                }
            }
        }
        #endregion

        #region Add Xref
        private void AddXref(int num)
        {
            sw.Flush();
            long offs = sw.BaseStream.Position;
            xref.Add(num, offs.ToString("D10") + " 00000 n");
            currentObjectNumber = num;
            currentGenerationNumber = 0;
        }
        #endregion

        #region Convert numeric value to string
        /// <summary>
        /// Convert numeric value to string 
        /// </summary>
        /// <param name="Value">Numeric value</param>
        /// <returns>String</returns>
        internal string ConvertToString(double Value)
        {
            int digits = precision_digits;
            decimal numValue = Math.Round((decimal)Value, digits);
            return numValue.ToString("G", currentNumberFormat);
        }
        internal string ConvertToString(double Value, int precision)
        {
            decimal numValue = Math.Round((decimal)Value, precision);
            return numValue.ToString("G", currentNumberFormat);
        }
        #endregion

        #region Convert string to Escape sequence
        internal static string ConvertToEscapeSequence(string value)
        {
            //			string escapeChars="()\\"+(char)13+(char)10+(char)9+(char)8+(char)12;
            //			string replaceChars="()\\rntbf";
            string escapeChars = "()\\" + (char)13 + (char)10 + (char)8 + (char)12;     //tabs processing in another place
            string replaceChars = "()\\rnbf";
            bool flg;
            string st;

            if (value == null)
            {
                return "";
            }
            else
            {
                st = string.Empty;
                for (int index = 0; index < value.Length; index++)
                {
                    flg = false;
                    for (int index2 = 0; index2 < escapeChars.Length; index2++)
                    {
                        if (value[index] == escapeChars[index2])
                        {
                            st += "\\" + replaceChars[index2];
                            flg = true;
                            break;
                        }
                    }
                    if (!flg)
                    {
                        st += value[index];
                    }
                }
            }
            return st;
        }

        internal static string ConvertToEscapeSequencePlusTabs(string value)
        {
            string escapeChars = "()\\" + (char)13 + (char)10 + (char)9 + (char)8 + (char)12;
            string replaceChars = "()\\rntbf";
            bool flg;
            StringBuilder sb;

            if (value == null)
            {
                return string.Empty;
            }
            else
            {
                sb = new StringBuilder();
                for (int index = 0; index < value.Length; index++)
                {
                    flg = false;
                    for (int index2 = 0; index2 < escapeChars.Length; index2++)
                    {
                        if (value[index] == escapeChars[index2])
                        {
                            sb.Append("\\" + replaceChars[index2]);
                            flg = true;
                            break;
                        }
                    }
                    if (!flg)
                    {
                        sb.Append(value[index]);
                    }
                }
            }

            return sb.ToString();
        }
        #endregion

        #region Store Wysiwyg symbols
        internal void StoreWysiwygSymbols(StiText text)
        {
            #region Native parsing of the Wysiwyg text
            CheckGraphicsForTextRenderer();
            var outRunsList = new List<Stimulsoft.Base.Drawing.StiTextRenderer.RunInfo>();
            var outFontsList = new List<Stimulsoft.Base.Drawing.StiTextRenderer.StiFontState>();
            string textForOutput = text.Text;

            RectangleD rectComp = text.Page.Unit.ConvertToHInches(text.ComponentToPage(text.ClientRectangle));
            RectangleD rectText = text.ConvertTextMargins(rectComp, false);
            rectText = text.ConvertTextBorders(rectText, false);
            IStiTextOptions textOpt = text as IStiTextOptions;

            StiTextRenderer.DrawTextForOutput(
                graphicsForTextRenderer,
                textForOutput,
                text.Font,
                rectText,
                StiBrush.ToColor(text.TextBrush),
                StiBrush.ToColor(text.Brush),
                1,
                text.HorAlignment,
                text.VertAlignment,
                textOpt.TextOptions.WordWrap,
                textOpt.TextOptions.RightToLeft,
                StiDpiHelper.GraphicsScale,
                textOpt.TextOptions.Angle,
                textOpt.TextOptions.Trimming,
                textOpt.TextOptions.LineLimit,
                text.CheckAllowHtmlTags(),
                outRunsList,
                outFontsList,
                textOpt.TextOptions);

            foreach (StiTextRenderer.RunInfo runInfo in outRunsList)
            {
                Font tempFont = ((StiTextRenderer.StiFontState)outFontsList[runInfo.FontIndex]).FontBase;
                int fnt = pdfFont.GetFontNumber(tempFont);
                pdfFont.CurrentFont = fnt;

                StringBuilder sb = new StringBuilder();
                for (int indexGlyph = 0; indexGlyph < runInfo.GlyphIndexList.Length; indexGlyph++)
                {
                    sb.Append((char)runInfo.GlyphIndexList[indexGlyph]);
                }
                pdfFont.StoreUnicodeSymbolsInMap(new StringBuilder(textForOutput));
                pdfFont.StoreGlyphsInMap(sb);
            }
            #endregion
        }
        #endregion

        #region Store colors
        internal void SetStrokeColor(Color tempColor)
        {
            string st = (string)colorHash1[tempColor];
            if (st == null)
            {
                st = string.Format("{0} {1} {2} RG",
                    colorTable[tempColor.R],
                    colorTable[tempColor.G],
                    colorTable[tempColor.B]);
                colorHash1[tempColor] = st;
            }
            pageStream.WriteLine(st);
            byte alpha = tempColor.A;
            if ((alpha != lastColorStrokeA) && (StiOptions.Export.Pdf.AllowExtGState && useTransparency))
            {
                pageStream.WriteLine(GsTable[alpha, 0]);
                lastColorStrokeA = alpha;
                alphaTable[alpha] = true;
            }
        }
        internal void SetNonStrokeColor(Color tempColor)
        {
            string st = (string)colorHash2[tempColor];
            if (st == null)
            {
                st = string.Format("{0} {1} {2} rg",
                    colorTable[tempColor.R],
                    colorTable[tempColor.G],
                    colorTable[tempColor.B]);
                colorHash2[tempColor] = st;
            }
            pageStream.WriteLine(st);
            byte alpha = tempColor.A;
            if ((alpha != lastColorNonStrokeA) && (StiOptions.Export.Pdf.AllowExtGState && useTransparency))
            {
                pageStream.WriteLine(GsTable[alpha, 1]);
                lastColorNonStrokeA = alpha;
                alphaTable[alpha] = true;
            }
        }

        private Hashtable colorHash1 = new Hashtable();
        private Hashtable colorHash2 = new Hashtable();

        private static string[,] gsTable = null;
        private static string[,] GsTable
        {
            get
            {
                if (gsTable == null)
                {
                    gsTable = new string[256, 2];
                    for (int index = 0; index < 256; index++)
                    {
                        gsTable[index, 0] = string.Format("/GS{0}S gs", string.Format("{0:X2}", index));
                        gsTable[index, 1] = string.Format("/GS{0}N gs", string.Format("{0:X2}", index));
                    }
                }
                return gsTable;
            }
        }

        internal void PushColorToStack()
        {
            colorStack.Push(lastColorStrokeA);
            colorStack.Push(lastColorNonStrokeA);
        }
        internal void PopColorFromStack()
        {
            lastColorNonStrokeA = (byte)colorStack.Pop();
            lastColorStrokeA = (byte)colorStack.Pop();
        }
        #endregion

        #region Store encoded data
        private void StoreStringLine(string noCryptString, string cryptString)
        {
            StoreStringLine(noCryptString, cryptString, false);
        }

        private void StoreStringLine(string noCryptString, string cryptString, bool escaping)
        {
            string tempSt = noCryptString + ConvertToHexString(cryptString, escaping);
            StoreString(tempSt);
            sw.WriteLine();
        }

        internal void StoreString(string st)
        {
            byte[] data = new byte[st.Length];
            for (int index = 0; index < st.Length; index++)
            {
                data[index] = (byte)st[index];
            }
            sw.Flush();
            stream2.Write(data, 0, data.Length);
        }

        internal string ConvertToHexString(string inString, bool escaping)
        {
            StringBuilder outString = new StringBuilder();
            if (!string.IsNullOrEmpty(inString))
            {
                bool needHex = false;
                for (int tempIndex = 0; tempIndex < inString.Length; tempIndex++)
                {
                    if ((int)inString[tempIndex] > 127)
                    {
                        needHex = true;
                    }
                }
                if (!encrypted)
                {
                    if (needHex == true)
                    {
#if TESTPDF
                        outString.Append("<FEFF");
                        for (int index = 0; index < inString.Length; index++)
                        {
                            int tempInt = (int)inString[index];
                            outString.Append(tempInt.ToString("X4"));
                        }
                        outString.Append(">");
#else
                        if (!escaping) outString.Append("(\xFE\xFF");
                        for (int index = 0; index < inString.Length; index++)
                        {
                            int charValue = (int)inString[index];
                            outString.Append((char)((charValue >> 8) & 0xff));
                            outString.Append((char)((charValue) & 0xff));
                        }
                        if (escaping)
                        {
                            outString = new StringBuilder("(\xFE\xFF" + ConvertToEscapeSequencePlusTabs(outString.ToString()));
                        }
                        outString.Append(")");
#endif
                    }
                    else
                    {
                        if (escaping)
                        {
                            outString.Append("(" + ConvertToEscapeSequencePlusTabs(inString) + ")");
                        }
                        else
                        {
                            outString.Append("(" + inString + ")");
                        }
                    }
                }
                else
                {
                    byte[] forCrypt = null;
                    if (needHex == true)
                    {
                        forCrypt = new byte[(inString.Length + 1) * 2];
                        for (int index = 0; index < inString.Length; index++)
                        {
                            //BitConverter.GetBytes(inString[index]).CopyTo(forCrypt, index * 2 + 2);
                            int charValue = (int)inString[index];
                            forCrypt[2 + index * 2 + 0] = (byte)((charValue >> 8) & 0xff);
                            forCrypt[2 + index * 2 + 1] = (byte)((charValue) & 0xff);
                        }
                        forCrypt[0] = 0xFE;
                        forCrypt[1] = 0xFF;
                    }
                    else
                    {
                        forCrypt = new byte[inString.Length];
                        for (int index = 0; index < inString.Length; index++)
                        {
                            forCrypt[index] = (byte)inString[index];
                        }
                    }
                    byte[] encryptedData = pdfSecurity.EncryptData(forCrypt, currentObjectNumber, currentGenerationNumber);
                    StringBuilder tempSB = new StringBuilder();
                    for (int index = 0; index < encryptedData.Length; index++)
                    {
                        tempSB.Append((char)encryptedData[index]);
                    }
                    outString.Append("(" + ConvertToEscapeSequencePlusTabs(tempSB.ToString()) + ")");
                }
            }
            else
            {
                if (encrypted)
                {
                    byte[] encryptedData = pdfSecurity.EncryptData(new byte[0], currentObjectNumber, currentGenerationNumber);
                    StringBuilder tempSB = new StringBuilder();
                    for (int index = 0; index < encryptedData.Length; index++)
                    {
                        tempSB.Append((char)encryptedData[index]);
                    }
                    outString.Append("(" + ConvertToEscapeSequencePlusTabs(tempSB.ToString()) + ")");
                }
                else
                {
                    outString.Append("()");
                }
            }
            return outString.ToString();
        }

        //private int StoreMemoryStream(MemoryStream mem)
        //{
        //    int result = -1;
        //    if (encrypted)
        //    {
        //        byte[] forCrypt = mem.ToArray();
        //        byte[] encryptedData = EncryptData(forCrypt);
        //        sw.Flush();
        //        stream2.Write(encryptedData, 0, encryptedData.Length);
        //        result = encryptedData.Length;
        //    }
        //    else
        //    {
        //        sw.Flush();
        //        mem.WriteTo(stream2);
        //        result = (int)mem.Length;
        //    }
        //    mem.Close();
        //    return result;
        //}

        private void StoreMemoryStream2(MemoryStream mem, string dictionaryString)
        {
            if (encrypted)
            {
                byte[] forCrypt = mem.ToArray();
                byte[] encryptedData = pdfSecurity.EncryptData(forCrypt, currentObjectNumber, currentGenerationNumber);
                sw.WriteLine(string.Format(dictionaryString, encryptedData.Length));
                sw.WriteLine(">>");
                sw.WriteLine("stream");
                sw.Flush();
                stream2.Write(encryptedData, 0, encryptedData.Length);
            }
            else
            {
                sw.WriteLine(string.Format(dictionaryString, mem.Length));
                sw.WriteLine(">>");
                sw.WriteLine("stream");
                sw.Flush();
                mem.WriteTo(stream2);
            }
            mem.Close();
        }
        private void StoreMemoryStream2(byte[] data, string dictionaryString)
        {
            if (encrypted)
            {
                byte[] encryptedData = pdfSecurity.EncryptData(data, currentObjectNumber, currentGenerationNumber);
                sw.WriteLine(string.Format(dictionaryString, encryptedData.Length));
                sw.WriteLine(">>");
                sw.WriteLine("stream");
                sw.Flush();
                stream2.Write(encryptedData, 0, encryptedData.Length);
            }
            else
            {
                sw.WriteLine(string.Format(dictionaryString, data.Length));
                sw.WriteLine(">>");
                sw.WriteLine("stream");
                sw.Flush();
                stream2.Write(data, 0, data.Length);
            }
        }
        #endregion

        #region RenderStartDoc
        private void RenderStartDoc(StiReport report, StiPagesCollection pages)
        {
            //header
            //sw.WriteLine(keyLength == StiPdfEncryptionKeyLength.Bit256_r5 || keyLength == StiPdfEncryptionKeyLength.Bit256_r6 ? "%PDF-1.7" : "%PDF-1.4");
            sw.WriteLine("%PDF-1.7");
            byte[] buff = new byte[5] { 37, 226, 227, 207, 211 };
            sw.Flush();
            stream2.Write(buff, 0, buff.Length);
            sw.WriteLine("");

            #region Catalog
            AddXref(1);
            sw.WriteLine("1 0 obj");
            sw.WriteLine("<<");

            if (keyLength == StiPdfEncryptionKeyLength.Bit256_r5)
                sw.WriteLine("/Extensions<</ADBE<</BaseVersion/1.7/ExtensionLevel 3>>>>");
            if (keyLength == StiPdfEncryptionKeyLength.Bit256_r6)
                sw.WriteLine("/Extensions<</ADBE<</BaseVersion/1.7/ExtensionLevel 8>>>>");

            sw.WriteLine("/Type /Catalog");
            sw.WriteLine("/Pages 4 0 R");

            sw.WriteLine("/MarkInfo<</Marked true>>");
            sw.WriteLine("/Metadata {0} 0 R", info.Metadata.Ref);
            sw.WriteLine("/OutputIntents {0} 0 R", info.OutputIntents.Ref);

            sw.WriteLine("/StructTreeRoot 5 0 R");

            if (haveBookmarks)
            {
                sw.WriteLine("/Outlines {0} 0 R", info.Outlines.Ref);
                sw.WriteLine("/PageMode /UseOutlines");
            }
            else
            {
                sw.WriteLine("/PageMode /UseNone");
            }
            if (!PrintScaling)
            {
                sw.WriteLine("/ViewerPreferences");
                sw.WriteLine("<<");
                sw.WriteLine("/PrintScaling /None");
                sw.WriteLine(">>");
            }
            if (haveAnnots || haveDigitalSignature)
            {
                sw.WriteLine("/AcroForm {0} 0 R", info.AcroForm.Ref);
            }

            bool needAutoPrint = autoPrint != StiPdfAutoPrintMode.None;
            bool needEmbeddedFiles = info.EmbeddedFilesList.Count > 0;
            if (needAutoPrint || needEmbeddedFiles)
            {
                sw.WriteLine("/Names <<");
                if (needAutoPrint)
                {
                    sw.WriteLine("/JavaScript {0} 0 R", info.EmbeddedJS.Ref);
                }
                if (needEmbeddedFiles)
                {
                    sw.WriteLine("/EmbeddedFiles <<");
                    sw.Write("/Names [ ");
                    for (int index = 0; index < info.EmbeddedFilesList.Count; index++)
                    {
                        var file = embeddedFiles[index];
                        sw.Write("{0} {1} 0 R ", ConvertToHexString(file.Name, true), info.EmbeddedFilesList[index].Ref);
                    }
                    sw.WriteLine("]");
                    sw.WriteLine(">>");
                }
                sw.WriteLine(">>");
            }

            if (useZUGFeRD)
            {
                sw.Write("/AF [ ");
                for (int index = 0; index < info.EmbeddedFilesList.Count; index++)
                {
                    sw.Write("{0} 0 R ", info.EmbeddedFilesList[index].Ref);
                }
                sw.WriteLine("]");
            }

            if (!usePdfA)
            {
                sw.WriteLine("/OCProperties << /OCGs [{0} 0 R] /D << /ON [{0} 0 R] /AS [<</Event /Print /OCGs [{0} 0 R] /Category [/Print]>>] >> >>", info.OptionalContentGroup.Ref);
            }

            sw.WriteLine(">>");
            sw.WriteLine("endobj");
            sw.WriteLine("");
            #endregion

            #region Info
            AddXref(2);
            sw.WriteLine("2 0 obj");
            sw.WriteLine("<<");

            StoreStringLine("/Producer ", producerName, true);
            StoreStringLine("/Creator ", creatorName, true);
            if (!string.IsNullOrEmpty(report.ReportAuthor))
            {
                StoreStringLine("/Author ", report.ReportAuthor, true);
            }
            if (!string.IsNullOrEmpty(report.ReportAlias))
            {
                StoreStringLine("/Subject ", report.ReportAlias, true);
            }
            if (!string.IsNullOrEmpty(report.ReportName))
            {
                StoreStringLine("/Title ", report.ReportName, true);
            }
            if (!string.IsNullOrEmpty(keywords))
            {
                StoreStringLine("/Keywords ", keywords, true);
            }
            StoreStringLine("/CreationDate ", "D:" + currentDateTime);
            StoreStringLine("/ModDate ", "D:" + currentDateTime);

            foreach (StiMetaTag meta in report.MetaTags)
            {
                if (meta.Name.StartsWith("pdf:"))
                {
                    StoreStringLine(string.Format("/{0} ", StiNameValidator.CorrectName(meta.Name.Substring(4), false, report)), meta.Tag);
                }
            }

            sw.WriteLine(">>");
            sw.WriteLine("endobj");
            sw.WriteLine("");
            #endregion

            #region Colorspace
            AddXref(3);
            sw.WriteLine("3 0 obj");
            sw.WriteLine("<<");
            sw.WriteLine("/Cs1 [/Pattern /DeviceRGB]");
            sw.WriteLine(">>");
            sw.WriteLine("endobj");
            sw.WriteLine("");
            #endregion

            #region make resources list
            ArrayList resourcesList = new ArrayList();
            resourcesList.Add("/Resources");
            resourcesList.Add("<<");
            resourcesList.Add("/ProcSet [/PDF /Text /ImageC]");
            resourcesList.Add("/Font");
            resourcesList.Add("<<");
            for (int index = 0; index < fontsCounter; index++)
            {
                resourcesList.Add(string.Format("/F{0} {1} 0 R", index, info.FontList[index].Ref));
            }
            resourcesList.Add(">>");
            if (imagesCounter > 0)
            {
                resourcesList.Add("/XObject");
                resourcesList.Add("<<");
                for (int index = 0; index < imagesCounter; index++)
                {
                    resourcesList.Add(string.Format("/Image{0} {1} 0 R", index, info.XObjectList[index].Ref));
                }
                resourcesList.Add(">>");
            }
            resourcesList.Add("/Pattern");
            resourcesList.Add("<<");
            resourcesList.Add(string.Format("/P1 {0} 0 R", info.Patterns.First.Ref));
            for (int indexPattern = 0; indexPattern < hatchCounter; indexPattern++)
            {
                resourcesList.Add(string.Format("/PH{0} {1} 0 R", 1 + indexPattern, info.Patterns.HatchItems[indexPattern].Ref));
            }
            for (int indexPattern = 0; indexPattern < shadingCounter; indexPattern++)
            {
                resourcesList.Add(string.Format("/P{0} {1} 0 R", 2 + indexPattern, info.Patterns.ShadingItems[indexPattern].Ref));
            }
            resourcesList.Add(">>");
            resourcesList.Add("/ColorSpace 3 0 R");
            if (StiOptions.Export.Pdf.AllowExtGState && useTransparency)
            {
                resourcesList.Add(string.Format("/ExtGState {0} 0 R", info.ExtGState.Ref));
            }
            if (!usePdfA)
            {
                resourcesList.Add(string.Format("/Properties << /oc1 {0} 0 R >>", info.OptionalContentGroup.Ref));
            }
            resourcesList.Add(">>");
            #endregion

            #region Pages
            AddXref(4);
            sw.WriteLine("4 0 obj");
            sw.WriteLine("<<");
            sw.WriteLine("/Type /Pages");
            sw.WriteLine("/Kids [");
            for (int index = 0; index < pages.Count; index++)
            {
                sw.WriteLine("{0} 0 R", info.PageList[index].Ref);
            }
            sw.WriteLine("]");
            sw.WriteLine("/Count {0}", pages.Count);
            if (StiOptions.Export.Pdf.AllowInheritedPageResources)
            {
                for (int indexResourcesList = 0; indexResourcesList < resourcesList.Count; indexResourcesList++)
                {
                    sw.WriteLine((string)resourcesList[indexResourcesList]);
                }
            }
            sw.WriteLine(">>");
            sw.WriteLine("endobj");
            sw.WriteLine("");
            #endregion

            #region StructTreeRoot
            AddXref(5);
            sw.WriteLine("5 0 obj");
            sw.WriteLine("<<");
            sw.WriteLine("/Type /StructTreeRoot");
            sw.WriteLine(">>");
            sw.WriteLine("endobj");
            sw.WriteLine("");
            #endregion

            #region OptionalContentGroup
            AddXref(6);
            sw.WriteLine("6 0 obj");
            sw.WriteLine("<<");
            sw.WriteLine("/Type /OCG");
            sw.WriteLine("/Name (Printable off)");
            sw.WriteLine("/Usage <<");
            sw.WriteLine("/Print << /PrintState /OFF >>");
            sw.WriteLine("/View << /ViewState /ON >>");
            sw.WriteLine(">>");
            sw.WriteLine(">>");
            sw.WriteLine("endobj");
            sw.WriteLine("");
            #endregion

            #region Page
            for (int index = 0; index < pages.Count; index++)
            {
                AddXref(info.PageList[index].Ref);
                sw.WriteLine("{0} 0 obj", info.PageList[index].Ref);
                sw.WriteLine("<<");
                sw.WriteLine("/Type /Page");
                sw.WriteLine("/Parent 4 0 R");
                var tPage = pages.GetPageWithoutCache(index);
                double pageHeight = hiToTwips * report.Unit.ConvertToHInches(tPage.PageHeight * tPage.SegmentPerHeight);
                double pageWidth = hiToTwips * report.Unit.ConvertToHInches(tPage.PageWidth * tPage.SegmentPerWidth);

                //Adobe Acrobat limitation of page size: maximum 200" * 200"
                if (pageHeight > 14400)
                {
                    pageHeight = 14400;
                }
                if (pageWidth > 14400)
                {
                    pageWidth = 14400;
                }

                sw.WriteLine("/MediaBox [ 0 0 {0} {1} ]", ConvertToString(pageWidth), ConvertToString(pageHeight));
                if (!StiOptions.Export.Pdf.AllowInheritedPageResources)
                {
                    for (int indexResourcesList = 0; indexResourcesList < resourcesList.Count; indexResourcesList++)
                    {
                        sw.WriteLine((string)resourcesList[indexResourcesList]);
                    }
                }
                sw.WriteLine("/Contents {0} 0 R", info.PageList[index].Content.Ref);
                if (useTransparency)
                {
                    sw.WriteLine("/Group");
                    sw.WriteLine("<<");
                    sw.WriteLine("/Type /Group");
                    sw.WriteLine("/S /Transparency");
                    sw.WriteLine("/CS /DeviceRGB");
                    sw.WriteLine(">>");
                }
                if (haveLinks || haveAnnots || haveDigitalSignature || haveTooltips)
                {
                    sw.WriteLine("/Annots [");
                    if (haveLinks)
                    {
                        for (int tempIndex = 0; tempIndex < linksCounter; tempIndex++)
                        {
                            StiLinkObject stl = (StiLinkObject)linksArray[tempIndex];
                            if (stl.Page == index)
                            {
                                sw.WriteLine("{0} 0 R ", info.LinkList[tempIndex].Ref);
                            }
                        }
                    }
                    if (haveAnnots)
                    {
                        for (int tempIndex = 0; tempIndex < annotsCounter; tempIndex++)
                        {
                            StiEditableObject seo = (StiEditableObject)annotsArray[tempIndex];
                            if (seo.Page == index)
                            {
                                sw.WriteLine("{0} 0 R ", info.AcroForm.Annots[tempIndex].Ref);
                            }
                        }
                        for (int tempIndex = 0; tempIndex < annots2Counter; tempIndex++)
                        {
                            StiEditableObject seo = annots2Array[tempIndex];
                            if (seo.Page == index)
                            {
                                for (int indexState = 0; indexState < info.AcroForm.CheckBoxes[tempIndex].Count; indexState++)
                                {
                                    sw.WriteLine("{0} 0 R ", info.AcroForm.CheckBoxes[tempIndex][indexState].Ref);
                                }
                            }
                        }
                        for (int tempIndex = 0; tempIndex < unsignedSignaturesCounter; tempIndex++)
                        {
                            StiEditableObject seo = (StiEditableObject)unsignedSignaturesArray[tempIndex];
                            if (seo.Page == index)
                            {
                                sw.WriteLine("{0} 0 R ", info.AcroForm.UnsignedSignatures[tempIndex].Ref);
                            }
                        }
                    }
                    if ((haveDigitalSignature) && (index == signaturePageNumber))
                    {
                        sw.WriteLine("{0} 0 R ", info.AcroForm.Signatures[0].Ref);
                    }
                    if (haveTooltips)
                    {
                        for (int tempIndex = 0; tempIndex < tooltipsCounter; tempIndex++)
                        {
                            StiLinkObject stl = (StiLinkObject)tooltipsArray[tempIndex];
                            if (stl.Page == index)
                            {
                                sw.WriteLine("{0} 0 R ", info.AcroForm.Tooltips[tempIndex].Ref);
                            }
                        }
                    }
                    sw.WriteLine("]");
                }

                //				sw.WriteLine("/Resources");
                //				sw.WriteLine("<<");
                //				sw.WriteLine("/Pattern");
                //				sw.WriteLine("<<");
                //				sw.WriteLine("/P1 {0} 0 R", xpatternOffset + 1);
                //				for (int indexPattern = 0; indexPattern < shadingCounter; indexPattern ++)
                //				{
                //					if (((StiShadingData)shadingArray[indexPattern]).Page == index)
                //					{
                //						sw.WriteLine("/P{0} {1} 0 R", 2 + indexPattern, xpatternOffset + 2 + indexPattern);
                //					}
                //				}
                //				sw.WriteLine(">>");
                //				sw.WriteLine(">>");

                sw.WriteLine(">>");
                sw.WriteLine("endobj");
                sw.WriteLine("");
            }
            #endregion
        }
        #endregion

        #region RenderEndDoc
        private void RenderEndDoc()
        {
            sw.Flush();
            long offs = sw.BaseStream.Position;

            sw.WriteLine("xref");
            sw.WriteLine("0 {0}", xref.Count + 1);
            sw.WriteLine("0000000000 65535 f");
            for (int index = 0; index < xref.Count; index++)
            {
                sw.WriteLine((string)xref.GetByIndex(index));
            }
            sw.WriteLine("trailer");
            sw.WriteLine("<<");
            sw.WriteLine("/Size {0}", xref.Count + 1);
            sw.WriteLine("/Root 1 0 R");
            sw.WriteLine("/Info 2 0 R");
            if (encrypted)
            {
                sw.WriteLine("/Encrypt {0} 0 R", info.Encode.Ref);
            }
            sw.WriteLine("/ID[<{0}><{0}>]", IDValueString);
            sw.WriteLine(">>");
#if NETCORE
            sw.WriteLine("%StimulsoftNetCore20");
#else
            sw.WriteLine("%Stimulsoft" + (StiOptions.Configuration.IsWPF ? "Wpf" : "Net") + (StiOptions.Configuration.IsWeb ? "Web" : ""));
#endif
            sw.WriteLine("%" + StiVersion.Version);
            sw.WriteLine("startxref");
            sw.WriteLine("{0}", offs);
            sw.WriteLine("%%EOF");
        }
        #endregion

        #region RenderPageHeader
        private void RenderPageHeader(int pageNumber)
        {
            AddXref(info.PageList[pageNumber].Content.Ref);
            sw.WriteLine("{0} 0 obj", info.PageList[pageNumber].Content.Ref);
            sw.WriteLine("<<");

            memoryPageStream = new MemoryStream();
            pageStream = new StreamWriter(memoryPageStream);

            pageStream.WriteLine("2 J");//**

            lastColorNonStrokeA = 255;
            lastColorStrokeA = 255;
        }
        #endregion

        #region RenderPageFooter
        private void RenderPageFooter(double pageH, double pageW)
        {
            #region Trial
#if CLOUD
            var isTrial = StiCloudPlan.IsTrialPlan(report != null ? report.ReportGuid : null);
#elif SERVER
            var isTrial = StiVersionX.IsSvr;
#else
            var key = StiLicenseKeyValidator.GetLicenseKey();
		    var isTrial = !StiLicenseKeyValidator.IsValidOnNetFramework(key);
		    if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key))isTrial = true;

            #region IsValidLicenseKey
		    if (!isTrial)
		    {
		        try
		        {
		            using (var rsa = new RSACryptoServiceProvider(512))
		            using (var sha = new SHA1CryptoServiceProvider())
		            {
		                rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
		                isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
		            }
		        }
		        catch (Exception)
		        {
		            isTrial = true;
		        }
		    }
            #endregion
#endif

            if (isTrial)
            {
                double tempX = pageW / 596d * 1.4d;
                double tempY = pageH / 840d * 1.4d;
                if (tempX > tempY) tempX = tempY;
                else tempY = tempX;
                pageStream.WriteLine("q");
                PushColorToStack();
                pageStream.WriteLine("1 J 1 j 20 w");
                //				pageStream.WriteLine("/Pattern CS /P1 SCN");
                SetStrokeColor(Color.FromArgb(64, 100, 100, 100));
                pageStream.WriteLine("{0} 0 0 {1} {2} {3} cm 0.707 0.707 -0.707 0.707 0 0 cm 1 0 0 1 -155 -50 cm",
                    ConvertToString(tempX), ConvertToString(tempY), ConvertToString(pageW / 2), ConvertToString(pageH / 2));
                pageStream.WriteLine("40 0 m 40 100 l 0 100 m 80 100 l S");
                pageStream.WriteLine("100 0 m 100 70 l 100 45 m 120 65 l 130 72 l 140 68 l S");
                pageStream.WriteLine("170 0 m 170 70 l 169 100 m 171 100 l S");
                pageStream.WriteLine("215 60 m 222 69 l 232 71 l 255 70 l 265 60 l 265 5 l 270 0 l 265 44 m 220 31 l 212 20 l 212 10 l 225 0 l 235 0 l 250 5 l 265 18 l S");
                pageStream.WriteLine("310 0 m 310 100 l S");
                pageStream.WriteLine("Q");
                PopColorFromStack();
            }
            #endregion

            pageStream.Flush();
            if (memoryPageStream.Position < memoryPageStream.Length)
            {
                memoryPageStream.SetLength(memoryPageStream.Position);
            }

            if (compressed == true)
            {
                byte[] bytedata = memoryPageStream.ToArray();
                MemoryStream TmpStream = StiExportUtils.MakePdfDeflateStream(bytedata);

                //sw.WriteLine("/Filter [/FlateDecode] /Length {0}", TmpStream.Length);
                //sw.WriteLine(">>");
                //sw.WriteLine("stream");
                //StoreMemoryStream(TmpStream);
                StoreMemoryStream2(TmpStream, "/Filter [/FlateDecode] /Length {0}");
            }
            else
            {
                //sw.WriteLine("/Filter [] /Length {0}", memoryPageStream.Length);
                //sw.WriteLine(">>");
                //sw.WriteLine("stream");
                //StoreMemoryStream(memoryPageStream);
                StoreMemoryStream2(memoryPageStream, "/Filter [] /Length {0}");
            }
            sw.WriteLine("");
            pageStream.Close();
            memoryPageStream.Close();

            sw.WriteLine("endstream");
            sw.WriteLine("endobj");
            sw.WriteLine("");
        }
        #endregion

        #region RenderFontTable
        private void RenderFontTable()
        {
            //write fonts table
            for (int index = 0; index < fontsCounter; index++)
            {
                StringBuilder tempSb = new StringBuilder();
                pdfFont.CurrentFont = index;
                PdfFonts.pfontInfo tempInfo = (PdfFonts.pfontInfo)pdfFont.fontList[index];

                if (useUnicodeMode)
                {
                    #region main font info for Unicode
                    tempSb = new StringBuilder(tempInfo.Name);
                    tempSb.Replace(" ", "#20");
                    if ((tempInfo.Bold == true) || (tempInfo.Italic == true))
                    {
                        tempSb.Append(",");
                        if (tempInfo.Bold == true) tempSb.Append("Bold");
                        if (tempInfo.Italic == true) tempSb.Append("Italic");
                    }
                    if (embeddedFonts)
                    {
                        Random rand = new Random();
                        tempSb.Insert(0, string.Format("SR{0}{1}{2}{3}+",
                            (char)rand.Next('A', 'Z'),
                            (char)rand.Next('A', 'Z'),
                            (char)rand.Next('A', 'Z'),
                            (char)rand.Next('A', 'Z')));
                    }

                    AddXref(info.FontList[index].Ref);
                    sw.WriteLine("{0} 0 obj", info.FontList[index].Ref);
                    sw.WriteLine("<<");
                    sw.WriteLine("/Type /Font");
                    sw.WriteLine("/Subtype /Type0");
                    sw.WriteLine("/BaseFont /{0}", tempSb);
                    sw.WriteLine("/DescendantFonts [{0} 0 R]", info.FontList[index].DescendantFont.Ref);
                    sw.WriteLine("/Encoding /Identity-H");
                    sw.WriteLine("/ToUnicode {0} 0 R", info.FontList[index].ToUnicode.Ref);
                    sw.WriteLine("/Name /F{0}", index);
                    sw.WriteLine(">>");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");

                    AddXref(info.FontList[index].DescendantFont.Ref);
                    sw.WriteLine("{0} 0 obj", info.FontList[index].DescendantFont.Ref);
                    sw.WriteLine("<<");
                    sw.WriteLine("/Type /Font");
                    sw.WriteLine("/Subtype /CIDFontType2");
                    sw.WriteLine("/BaseFont /{0}", tempSb);
                    sw.WriteLine("/CIDSystemInfo");
                    sw.WriteLine("<<");
                    //sw.WriteLine("/Registry (Adobe)");
                    //sw.WriteLine("/Ordering (Identity)");
                    StoreStringLine("/Registry", "Adobe");
                    StoreStringLine("/Ordering", "Identity");
                    sw.WriteLine("/Supplement 0");
                    sw.WriteLine(">>");
                    sw.WriteLine("/FontDescriptor {0} 0 R", info.FontList[index].FontDescriptor.Ref);
                    if (usePdfA)
                    {
                        sw.WriteLine("/CIDToGIDMap /Identity");
                    }
                    sw.WriteLine("/W [0 [1000]");

                    ushort[] pdfGlyphList = pdfFont.GlyphList;
                    int pdfGlyphListLength = pdfGlyphList.Length;

                    //make arrays
                    int[] glyphList = new int[pdfGlyphListLength];
                    int[] glyphListBack = new int[pdfGlyphListLength];
                    for (int glyphIndex = 32; glyphIndex < pdfGlyphListLength; glyphIndex++)
                    {
                        glyphList[glyphIndex] = pdfGlyphList[glyphIndex];
                        glyphListBack[glyphIndex] = glyphIndex;
                    }
                    //sort arrays
                    for (int index1 = 32; index1 < glyphList.Length - 1; index1++)
                    {
                        for (int index2 = index1 + 1; index2 < glyphList.Length; index2++)
                        {
                            if (glyphList[index1] > glyphList[index2])
                            {
                                int tempGlyph = glyphList[index1];
                                glyphList[index1] = glyphList[index2];
                                glyphList[index2] = tempGlyph;
                                int tempGlyphBack = glyphListBack[index1];
                                glyphListBack[index1] = glyphListBack[index2];
                                glyphListBack[index2] = tempGlyphBack;
                            }
                        }
                    }

                    StringBuilder sb = new StringBuilder();
                    int mapIndex = 32;
                    while (mapIndex < pdfFont.MappedSymbolsCount)
                    {
                        sb.Append(glyphList[mapIndex].ToString() + " [");
                        sb.Append(pdfFont.Widths[glyphListBack[mapIndex] - 32].ToString());
                        mapIndex++;
                        while ((mapIndex < pdfFont.MappedSymbolsCount) &&
                            (glyphList[mapIndex] - 1 == glyphList[mapIndex - 1]))
                        {
                            sb.Append(" " + pdfFont.Widths[glyphListBack[mapIndex] - 32].ToString());
                            mapIndex++;
                        }
                        //some symbols may be translated to one glyph
                        while ((mapIndex < pdfFont.MappedSymbolsCount) && (glyphList[mapIndex] == glyphList[mapIndex - 1]))
                        {
                            mapIndex++;
                        }
                        sb.Append("]");
                        sw.WriteLine("{0}", sb);
                        sb = new StringBuilder();
                    }
                    if (pdfFont.GlyphWidths != null && pdfFont.GlyphWidths[0xFFFF] != 0)
                    {
                        for (int indexGlyph = 0; indexGlyph < 65535; indexGlyph++)
                        {
                            if ((pdfFont.GlyphWidths[indexGlyph] > 0) && (pdfFont.GlyphBackList[indexGlyph] == 0))
                            {
                                sw.WriteLine(string.Format("{0} [{1}]", indexGlyph, pdfFont.GlyphWidths[indexGlyph]));
                            }
                        }
                    }
                    sw.WriteLine("]");
                    sw.WriteLine(">>");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");
                    #endregion

                    #region ToUnicode
                    AddXref(info.FontList[index].ToUnicode.Ref);
                    string cmapFontName = "SR+F" + index.ToString();
                    sw.WriteLine("{0} 0 obj", info.FontList[index].ToUnicode.Ref);
                    sw.WriteLine("<<");

                    #region prepare memory stream and buffer
                    MemoryStream mem = new MemoryStream();
                    StreamWriter swmem = new StreamWriter(mem);
                    swmem.WriteLine("/CIDInit /ProcSet findresource begin");
                    swmem.WriteLine("12 dict begin");
                    swmem.WriteLine("begincmap");
                    swmem.WriteLine("/CIDSystemInfo");
                    swmem.WriteLine("<<");
                    swmem.WriteLine("/Registry (Adobe)");
                    swmem.WriteLine("/Ordering ({0})", cmapFontName);
                    swmem.WriteLine("/Supplement 0");
                    swmem.WriteLine(">> def");
                    swmem.WriteLine("/CMapName /{0} def", cmapFontName);
                    swmem.WriteLine("/CMapType 2 def");
                    swmem.WriteLine("1 begincodespacerange");
                    swmem.WriteLine("<0000> <FFFF>");
                    swmem.WriteLine("endcodespacerange");

                    int countBfChar = pdfFont.MappedSymbolsCount - 32;
                    int offsetBfChar = 32;
                    while (countBfChar > 0)
                    {
                        int count = countBfChar;
                        if (count > 100) count = 100;
                        swmem.WriteLine("{0} beginbfchar", count);
                        for (int indexUni = 0; indexUni < count; indexUni++)
                        {
                            swmem.WriteLine("<{0:X4}> <{1:X4}>", pdfFont.GlyphList[offsetBfChar], pdfFont.UnicodeMapBack[offsetBfChar]);
                            offsetBfChar++;
                        }
                        swmem.WriteLine("endbfchar");
                        countBfChar -= count;
                    }

                    swmem.WriteLine("endcmap");
                    swmem.WriteLine("CMapName currentdict /CMap defineresource pop");
                    swmem.WriteLine("end");
                    swmem.WriteLine("end");
                    swmem.Flush();
                    byte[] buff = mem.ToArray();
                    mem.Close();
                    #endregion

                    if (compressed == true)
                    {
                        MemoryStream TmpStream = StiExportUtils.MakePdfDeflateStream(buff);

                        //sw.WriteLine("/Length {0} /Filter [/FlateDecode] /Length1 {1}", TmpStream.Length, buff.Length);
                        //sw.WriteLine(">>");
                        //sw.WriteLine("stream");
                        //StoreMemoryStream(TmpStream);
                        StoreMemoryStream2(TmpStream, "/Length {0} /Filter [/FlateDecode] /Length1 " + buff.Length.ToString());
                    }
                    else
                    {
                        //MemoryStream TmpStream = new MemoryStream();
                        //TmpStream.Write(buff, 0, buff.Length);
                        //sw.WriteLine("/Length {0} /Filter [] /Length1 {0}", buff.Length);
                        //sw.WriteLine(">>");
                        //sw.WriteLine("stream");
                        //StoreMemoryStream(TmpStream);
                        StoreMemoryStream2(buff, "/Length {0} /Filter [] /Length1 " + buff.Length.ToString());
                    }
                    sw.WriteLine("");
                    sw.WriteLine("endstream");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");
                    #endregion

                    #region CIDSet
                    AddXref(info.FontList[index].CIDSet.Ref);
                    sw.WriteLine("{0} 0 obj", info.FontList[index].CIDSet.Ref);
                    sw.WriteLine("<<");

                    #region prepare buffer
                    BitArray bits = new BitArray(65536 + 32, false);
                    int maxIndex = 0;
                    for (int indexGlyph = 0; indexGlyph < pdfFont.GlyphList.Length; indexGlyph++)
                    {
                        ushort glyph = pdfFont.GlyphList[indexGlyph];
                        if (glyph != 65535)
                        {
                            bits[glyph] = true;
                            if (glyph > maxIndex) maxIndex = glyph;
                        }
                    }
                    if (pdfFont.GlyphWidths != null && pdfFont.GlyphWidths[0xFFFF] != 0)
                    {
                        for (int indexGlyph = 0; indexGlyph < 65535; indexGlyph++)
                        {
                            if ((pdfFont.GlyphWidths[indexGlyph] > 0) && (pdfFont.GlyphBackList[indexGlyph] == 0))
                            {
                                bits[indexGlyph] = true;
                                if (indexGlyph > maxIndex) maxIndex = indexGlyph;
                            }
                        }
                    }
                    int bytesCount = maxIndex / 8 + 1;
                    byte[] buff2 = new byte[bytesCount + 1];
                    int pos = 0;
                    while (pos < bytesCount)
                    {
                        int offset = pos * 8;
                        byte val = 0;
                        if (bits[offset]) val |= 0x80;
                        if (bits[offset + 1]) val |= 0x40;
                        if (bits[offset + 2]) val |= 0x20;
                        if (bits[offset + 3]) val |= 0x10;
                        if (bits[offset + 4]) val |= 0x08;
                        if (bits[offset + 5]) val |= 0x04;
                        if (bits[offset + 6]) val |= 0x02;
                        if (bits[offset + 7]) val |= 0x01;
                        buff2[pos] = val;
                        pos++;
                    }
                    #endregion

                    if (compressed == true)
                    {
                        MemoryStream TmpStream = StiExportUtils.MakePdfDeflateStream(buff2);
                        //sw.WriteLine("/Length {0} /Filter [/FlateDecode] /Length1 {1}", TmpStream.Length, buff2.Length);
                        //sw.WriteLine(">>");
                        //sw.WriteLine("stream");
                        //StoreMemoryStream(TmpStream);
                        StoreMemoryStream2(TmpStream, "/Length {0} /Filter [/FlateDecode] /Length1 " + buff2.Length.ToString());
                    }
                    else
                    {
                        //sw.WriteLine("/Length {0} /Filter [] /Length1 {0}", buff2.Length);
                        //sw.WriteLine(">>");
                        //sw.WriteLine("stream");
                        //MemoryStream TmpStream = new MemoryStream();
                        //TmpStream.Write(buff2, 0, buff2.Length);
                        //StoreMemoryStream(TmpStream);
                        StoreMemoryStream2(buff2, "/Length {0} /Filter [] /Length1 " + buff2.Length.ToString());
                    }
                    sw.WriteLine("");
                    sw.WriteLine("endstream");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");
                    #endregion

                }
                else
                {
                    #region main font info
                    AddXref(info.FontList[index].Ref);
                    sw.WriteLine("{0} 0 obj", info.FontList[index].Ref);
                    sw.WriteLine("<<");
                    sw.WriteLine("/Type /Font");
                    if (standardPdfFonts == true)
                    {
                        sw.WriteLine("/Subtype /Type1");
                        sw.WriteLine("/BaseFont /{0}", tempInfo.PdfName);
                    }
                    else
                    {
                        sw.WriteLine("/Subtype /TrueType");
                        tempSb = new StringBuilder(tempInfo.Name);
                        tempSb.Replace(" ", "#20");
                        if ((tempInfo.Bold == true) || (tempInfo.Italic == true))
                        {
                            tempSb.Append(",");
                            if (tempInfo.Bold == true) tempSb.Append("Bold");
                            if (tempInfo.Italic == true) tempSb.Append("Italic");
                        }
                        sw.WriteLine("/BaseFont /{0}", tempSb);
                        sw.WriteLine("/FontDescriptor {0} 0 R", info.FontList[index].FontDescriptor.Ref);
                    }
                    sw.WriteLine("/Encoding {0} 0 R", info.FontList[index].Encoding.Ref);

                    //make string "Widths"
                    StringBuilder sbWidths = new StringBuilder(" ");
                    for (int indexUni = 32; indexUni < pdfFont.MappedSymbolsCount; indexUni++)
                    {
                        sbWidths.Append(pdfFont.Widths[indexUni - 32].ToString() + " ");
                    }
                    sw.WriteLine("/FirstChar {0}", 32);
                    sw.WriteLine("/LastChar {0}", pdfFont.MappedSymbolsCount - 1);
                    sw.WriteLine("/Widths [{0}]", sbWidths);
                    sw.WriteLine("/Name /F{0}", index);
                    sw.WriteLine(">>");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");
                    #endregion

                    #region Encoding
                    AddXref(info.FontList[index].Encoding.Ref);
                    sw.WriteLine("{0} 0 obj", info.FontList[index].Encoding.Ref);
                    sw.WriteLine("<<");
                    sw.WriteLine("/Type /Encoding");
                    sw.WriteLine("/BaseEncoding /WinAnsiEncoding");
                    if (pdfFont.MappedSymbolsCount > PdfFonts.FirstMappedSymbol)
                    {
                        //make string "Differences"
                        StringBuilder sbDifferences = new StringBuilder(PdfFonts.FirstMappedSymbol.ToString() + " ");
                        for (int indexUni = PdfFonts.FirstMappedSymbol; indexUni < pdfFont.MappedSymbolsCount; indexUni++)
                        {
                            sbDifferences.Append("/" + pdfFont.CharPdfNames[indexUni]);
                        }
                        sw.WriteLine("/Differences [{0}]", sbDifferences);
                    }
                    sw.WriteLine(">>");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");
                    #endregion
                }

                if (standardPdfFonts == false)
                {
                    #region FontDescriptor
                    AddXref(info.FontList[index].FontDescriptor.Ref);
                    sw.WriteLine("{0} 0 obj", info.FontList[index].FontDescriptor.Ref);
                    sw.WriteLine("<<");
                    sw.WriteLine("/Type /FontDescriptor");
                    sw.WriteLine("/FontName /{0}", tempSb);
                    sw.WriteLine("/Flags 32");
                    //sw.WriteLine("/Ascent {0}", pdfFont.ASC);
                    sw.WriteLine("/Ascent {0}", pdfFont.tmASC);
                    sw.WriteLine("/CapHeight {0}", pdfFont.CH);
                    //sw.WriteLine("/Descent {0}", pdfFont.DESC);
                    sw.WriteLine("/Descent {0}", pdfFont.tmDESC);
                    sw.WriteLine("/FontBBox [{0} {1} {2} {3}]", pdfFont.LLX, pdfFont.LLY, pdfFont.URX, pdfFont.URY);
                    sw.WriteLine("/ItalicAngle {0}", pdfFont.ItalicAngle);
                    sw.WriteLine("/StemV {0}", pdfFont.StemV);
                    if (embeddedFonts == true)
                    {
                        sw.WriteLine("/FontFile2 {0} 0 R", info.FontList[tempInfo.ParentFontNumber == -1 ? index : tempInfo.ParentFontNumber].FontFile2.Ref);
                    }

                    bool needCIDSet = useUnicodeMode == true;
                    if (pdfComplianceMode == StiPdfComplianceMode.A2 || pdfComplianceMode == StiPdfComplianceMode.A3) needCIDSet = false;
                    if (needCIDSet)
                    {
                        sw.WriteLine("/CIDSet {0} 0 R", info.FontList[index].CIDSet.Ref);
                    }

                    sw.WriteLine(">>");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");
                    #endregion
                }

                if (embeddedFonts == true)
                {
                    #region embedded
                    byte[] buff;
                    pdfFont.GetFontDataBuf(tempInfo.Font, out buff, pdfFont.GlyphBackList == null);
                    if (buff.Length > 0 && tempInfo.ParentFontNumber == -1)
                    {
                        if (reduceFontSize)
                        {
                            pdfFont.OpenTypeHelper.ReduceFontSize(ref buff, tempInfo.Name, !fontGlyphsReduceNotNeed[index], pdfFont.GlyphList, pdfFont.GlyphRtfList);
                        }

                        AddXref(info.FontList[index].FontFile2.Ref);
                        sw.WriteLine("{0} 0 obj", info.FontList[index].FontFile2.Ref);
                        sw.WriteLine("<<");
                        if (compressedFonts == true)
                        {
                            MemoryStream TmpStream = StiExportUtils.MakePdfDeflateStream(buff);
                            //sw.WriteLine("/Length {0} /Filter [/FlateDecode] /Length1 {1}", TmpStream.Length, buff.Length);
                            //sw.WriteLine(">>");
                            //sw.WriteLine("stream");
                            //StoreMemoryStream(TmpStream);
                            StoreMemoryStream2(TmpStream, "/Length {0} /Filter [/FlateDecode] /Length1 " + buff.Length.ToString());
                        }
                        else
                        {
                            //sw.WriteLine("/Length {0} /Filter [] /Length1 {0}", buff.Length);
                            //sw.WriteLine(">>");
                            //sw.WriteLine("stream");
                            //MemoryStream TmpStream = new MemoryStream();
                            //TmpStream.Write(buff, 0, buff.Length);
                            //StoreMemoryStream(TmpStream);
                            StoreMemoryStream2(buff, "/Length {0} /Filter [] /Length1 " + buff.Length.ToString());
                        }
                        sw.WriteLine("");
                        sw.WriteLine("endstream");
                        sw.WriteLine("endobj");
                        sw.WriteLine("");
                    }
                    else
                    {
                        AddXref(info.FontList[index].FontFile2.Ref);
                        sw.WriteLine("{0} 0 obj", info.FontList[index].FontFile2.Ref);
                        sw.WriteLine("<< >>");
                        sw.WriteLine("endobj");
                        sw.WriteLine("");
                    }
                    #endregion
                }

            }

        }
        #endregion

        #region RenderImageTable
        private void RenderImageTable()
        {
            if (imagesCounter > 0)
            {
                for (int index = 0; index < imagesCounter; index++)
                {
                    //StiImageData pd = (StiImageData)imageList[index];
                    StiImageData pd = (StiImageData)imageCacheIndexToList[index];
                    byte[] bytes = (byte[])imageCache.ImagePackedStore[index];
                    byte[] mask = (byte[])imageCache.ImageMaskStore[index];
                    StiImageFormat imageFormatCurrent = pd.ImageFormat;

                    #region write image
                    AddXref(info.XObjectList[index].Ref);
                    sw.WriteLine("{0} 0 obj", info.XObjectList[index].Ref);
                    sw.WriteLine("<<");
                    sw.WriteLine("/Type /XObject");
                    sw.WriteLine("/Subtype /Image");
                    sw.WriteLine("/ColorSpace /{0}", imageFormatCurrent == StiImageFormat.Monochrome ? "DeviceGray" : "DeviceRGB");
                    sw.WriteLine("/Width {0}", pd.Width);
                    sw.WriteLine("/Height {0}", pd.Height);
                    sw.WriteLine("/BitsPerComponent {0}", imageFormatCurrent == StiImageFormat.Monochrome ? "1" : "8");
                    if (!usePdfA && imageInterpolationTable.ContainsKey(index))
                    {
                        sw.WriteLine("/Interpolate true");
                    }
                    if (mask != null)
                    {
                        sw.WriteLine("/SMask {0} 0 R", info.XObjectList[index].Mask.Ref);
                    }
                    sw.WriteLine("/Name /{0}", pd.Name);

                    string compressionType = string.Empty;
                    switch (imageCompressionMethod)
                    {
                        case StiPdfImageCompressionMethod.Flate:
                            compressionType = "FlateDecode";
                            break;

                        //case StiPdfImageCompressionMethod.Jpeg:
                        default:
                            compressionType = "DCTDecode";
                            break;
                    }

#if TESTPDF
                    //sw.WriteLine("/Length {0}",  bytes.Length * 2);
                    //sw.WriteLine("/Filter [/ASCIIHexDecode /{0}]", compressionType);
                    string stImage = "/Length {0} " + string.Format("/Filter [/ASCIIHexDecode /{0}]", compressionType);
#else
                    //sw.WriteLine("/Length {0}", bytes.Length);
                    //sw.WriteLine("/Filter [/{0}]", compressionType);
                    string stImage = "/Length {0} " + string.Format("/Filter [/{0}]", compressionType);
#endif

                    //sw.WriteLine(">>");
                    //sw.WriteLine("stream");
                    MemoryStream TmpStream = new MemoryStream();

#if TESTPDF
					byte[] buf = new byte[bytes.Length * 2];
					for (int indexb = 0; indexb < bytes.Length; indexb++)
					{
						string st = string.Format("{0:X2}", bytes[indexb]);
						buf[indexb * 2 + 0] = (byte)st[0];
						buf[indexb * 2 + 1] = (byte)st[1];
					}
					TmpStream.Write(buf, 0, buf.Length);
#else
                    TmpStream.Write(bytes, 0, bytes.Length);
#endif

                    //StoreMemoryStream(TmpStream);
                    StoreMemoryStream2(TmpStream, stImage);

                    sw.WriteLine("");
                    sw.WriteLine("endstream");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");
                    #endregion

                    if (mask != null)
                    {
                        #region write mask
                        AddXref(info.XObjectList[index].Mask.Ref);
                        sw.WriteLine("{0} 0 obj", info.XObjectList[index].Mask.Ref);
                        sw.WriteLine("<<");
                        sw.WriteLine("/Type /XObject");
                        sw.WriteLine("/Subtype /Image");
                        sw.WriteLine("/ColorSpace /DeviceGray");
                        sw.WriteLine("/Matte [ 0 0 0 ]");
                        sw.WriteLine("/Width {0}", pd.Width);
                        sw.WriteLine("/Height {0}", pd.Height);
                        sw.WriteLine("/BitsPerComponent 8");
                        if (!usePdfA && imageInterpolationTable.ContainsKey(index))
                        {
                            sw.WriteLine("/Interpolate true");
                        }

#if TESTPDF
                        //sw.WriteLine("/Length {0}",  mask.Length * 2);
                        //sw.WriteLine("/Filter [/ASCIIHexDecode /FlateDecode]");
                        string stMask = "/Length {0} /Filter [/ASCIIHexDecode /FlateDecode]";
#else
                        //sw.WriteLine("/Length {0}", mask.Length);
                        //sw.WriteLine("/Filter /FlateDecode");
                        string stMask = "/Length {0} /Filter /FlateDecode";
#endif

                        //sw.WriteLine(">>");
                        //sw.WriteLine("stream");
                        MemoryStream TmpStream2 = new MemoryStream();

#if TESTPDF
						byte[] buf2 = new byte[mask.Length * 2];
						for (int indexb = 0; indexb < mask.Length; indexb++)
						{
							string st = string.Format("{0:X2}", mask[indexb]);
							buf2[indexb * 2 + 0] = (byte)st[0];
							buf2[indexb * 2 + 1] = (byte)st[1];
						}
						TmpStream2.Write(buf2, 0, buf2.Length);
#else
                        TmpStream2.Write(mask, 0, mask.Length);
#endif

                        //StoreMemoryStream(TmpStream2);
                        StoreMemoryStream2(TmpStream2, stMask);

                        sw.WriteLine("");
                        sw.WriteLine("endstream");
                        sw.WriteLine("endobj");
                        sw.WriteLine("");
                        #endregion
                    }
                }
            }
        }
        #endregion

        #region RenderBookmarksTable
        private void RenderBookmarksTable()
        {
            if (haveBookmarks == true)
            {
                AddXref(info.Outlines.Ref);
                sw.WriteLine("{0} 0 obj", info.Outlines.Ref);
                sw.WriteLine("<<");
                sw.WriteLine("/Type /Outlines");
                sw.WriteLine("/First {0} 0 R", info.Outlines.Items[0].Ref);
                sw.WriteLine("/Last {0} 0 R", info.Outlines.Items[0].Ref);
                sw.WriteLine("/Count {0}", 1);
                sw.WriteLine(">>");
                sw.WriteLine("endobj");
                sw.WriteLine("");

                for (int index = 0; index < bookmarksCounter; index++)
                {
                    AddXref(info.Outlines.Items[index].Ref);
                    StiBookmarkTreeNode tn = (StiBookmarkTreeNode)bookmarksTree[index];
                    sw.WriteLine("{0} 0 obj", info.Outlines.Items[index].Ref);
                    sw.WriteLine("<<");
                    //					sw.WriteLine("/Title {0}", ConvertToHexString(tn.Title));
                    StoreStringLine("/Title ", tn.Title, true);
                    if (tn.Parent != -1) sw.WriteLine("/Parent {0} 0 R", info.Outlines.Items[tn.Parent].Ref);
                    if (tn.Prev != -1) sw.WriteLine("/Prev {0} 0 R", info.Outlines.Items[tn.Prev].Ref);
                    if (tn.Next != -1) sw.WriteLine("/Next {0} 0 R", info.Outlines.Items[tn.Next].Ref);
                    if (tn.First != -1) sw.WriteLine("/First {0} 0 R", info.Outlines.Items[tn.First].Ref);
                    if (tn.Last != -1) sw.WriteLine("/Last {0} 0 R", info.Outlines.Items[tn.Last].Ref);
                    if (tn.Count > 0)
                    {
                        if (index == 0)
                        {
                            sw.WriteLine("/Count {0}", tn.Count);
                        }
                        else
                        {
                            sw.WriteLine("/Count {0}", -tn.Count);
                        }
                    }
                    if (tn.Y > -1) sw.WriteLine("/Dest [{0} 0 R /XYZ null {1} null]", info.PageList[tn.Page].Ref, ConvertToString(tn.Y));
                    sw.WriteLine(">>");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");
                }
            }
        }
        #endregion

        #region RenderPatternTable
        private void RenderPatternTable()
        {
            AddXref(info.Patterns.Resources.Ref);
            sw.WriteLine("{0} 0 obj", info.Patterns.Resources.Ref);
            sw.WriteLine("<< /ProcSet [/PDF] >>");
            sw.WriteLine("endobj");
            sw.WriteLine("");

            #region render first pattern
            AddXref(info.Patterns.First.Ref);
            sw.WriteLine("{0} 0 obj", info.Patterns.First.Ref);
            sw.WriteLine("<<");
            sw.WriteLine("/Type /Pattern");
            sw.WriteLine("/PatternType 1");
            sw.WriteLine("/PaintType 1");
            sw.WriteLine("/TilingType 1");
            sw.WriteLine("/BBox [0 0 2 2]");
            sw.WriteLine("/XStep 3");
            sw.WriteLine("/YStep 3");
            sw.WriteLine("/Resources {0} 0 R", info.Patterns.Resources.Ref);
            //
            memoryPageStream = new MemoryStream();
            StreamWriter swmemStream = new StreamWriter(memoryPageStream);
            swmemStream.WriteLine("1 J 1 j 1 w");
            swmemStream.WriteLine("1 0 0 RG");
            swmemStream.Write("1 1 m 1.1 1.1 l S");
            swmemStream.Flush();
            //
            //sw.WriteLine("/Length {0}", memoryPageStream.Length);
            //sw.WriteLine(">>");
            //sw.WriteLine("stream");
            //StoreMemoryStream(memoryPageStream);
            StoreMemoryStream2(memoryPageStream, "/Length {0}");

            sw.WriteLine("");
            sw.WriteLine("endstream");
            sw.WriteLine("endobj");
            sw.WriteLine("");
            #endregion

            if (hatchCounter > 0)
            {
                for (int indexHatch = 0; indexHatch < hatchCounter; indexHatch++)
                {
                    WriteHatchPattern(indexHatch);
                }
            }

            if (shadingCounter > 0)
            {
                for (int indexShading = 0; indexShading < shadingCounter; indexShading++)
                {
                    WriteShadingPattern(indexShading);
                }
            }

            memoryPageStream.Close();
        }

        private void WriteHatchPattern(int indexHatch)
        {
            StiHatchBrush hatch = (StiHatchBrush)hatchArray[indexHatch];
            int hatchNumber = (int)hatch.Style;
            if (hatchNumber > 53) hatchNumber = 53;

            //			Bitmap bmp = new Bitmap(8, 8);
            //			Graphics gr = Graphics.FromImage(bmp);
            //			using (Brush brush = new HatchBrush(hatch.Style, hatch.ForeColor, hatch.BackColor))
            //					gr.FillRectangle(brush, 0, 0, 8, 8);

            AddXref(info.Patterns.HatchItems[indexHatch].Ref);
            sw.WriteLine("{0} 0 obj", info.Patterns.HatchItems[indexHatch].Ref);
            sw.WriteLine("<<");
            sw.WriteLine("/Type /Pattern");
            sw.WriteLine("/PatternType 1");
            sw.WriteLine("/PaintType 1");
            sw.WriteLine("/TilingType 1");
            sw.WriteLine("/BBox [0 0 1 1]");
            sw.WriteLine("/XStep 1");
            sw.WriteLine("/YStep 1");
            sw.WriteLine("/Resources {0} 0 R", info.Patterns.Resources.Ref);
            sw.WriteLine("/Matrix [5.5 0 0 5.5 0 0]");
            //
            memoryPageStream = new MemoryStream();
            StreamWriter memsw = new StreamWriter(memoryPageStream);

            //			memsw.WriteLine("BI");
            //			memsw.WriteLine("/W 8");
            //			memsw.WriteLine("/H 8");
            //			memsw.WriteLine("/BPC 8");
            //			memsw.WriteLine("/CS /RGB");
            //			memsw.WriteLine("/F [/AHx]");
            //			memsw.WriteLine("ID");
            //			for (int indexY = 0; indexY < 8; indexY++)
            //			{
            //				StringBuilder sb = new StringBuilder();
            //				for (int indexX = 0; indexX < 8; indexX++)
            //				{
            //					Color c = bmp.GetPixel(indexX, indexY);
            //					sb.Append(string.Format("{0:X2}{1:X2}{2:X2}", c.R, c.G, c.B));
            //				}
            //				if (indexY == 7) sb.Append(">");
            //				memsw.WriteLine(sb.ToString());
            //			}
            //			memsw.WriteLine("EI");
            //
            //			gr.Dispose();
            //			bmp.Dispose();

            if (hatch.BackColor.A != 0)
            {
                memsw.WriteLine("{0} {1} {2} rg",
                    colorTable[hatch.BackColor.R],
                    colorTable[hatch.BackColor.G],
                    colorTable[hatch.BackColor.B]);
                memsw.WriteLine("0 0 1 1 re f");
            }
            memsw.WriteLine("{0} {1} {2} rg",
                colorTable[hatch.ForeColor.R],
                colorTable[hatch.ForeColor.G],
                colorTable[hatch.ForeColor.B]);
            memsw.WriteLine("BI");
            memsw.WriteLine("/W 8");
            memsw.WriteLine("/H 8");
            memsw.WriteLine("/BPC 1");
            memsw.WriteLine("/IM true");
            memsw.WriteLine("/D [1 0]");
            memsw.WriteLine("/F [/AHx]");
            memsw.WriteLine("ID");
            memsw.WriteLine(hatchData[hatchNumber] + ">");
            memsw.WriteLine("EI");
            memsw.Flush();
            //
            //sw.WriteLine("/Length {0}", memoryPageStream.Length);
            //sw.WriteLine(">>");
            //sw.WriteLine("stream");
            //StoreMemoryStream(memoryPageStream);
            StoreMemoryStream2(memoryPageStream, "/Length {0}");

            //sw.WriteLine("");
            sw.WriteLine("endstream");
            sw.WriteLine("endobj");
            sw.WriteLine("");
        }

        private void WriteShadingPattern(int indexShading)
        {
            StiShadingData ssd = (StiShadingData)shadingArray[indexShading];

            double xs = 1;
            double ys = 1;
            double angle = ssd.Angle;
            if (angle < 0) angle += 360;
            if ((angle >= 270) && (angle < 360))
            {
                angle = 360 - angle;
                ys = -1;
            };
            if ((angle >= 180) && (angle < 270))
            {
                angle = angle - 180;
                ys = -1;
                xs = -1;
            };
            if ((angle >= 90) && (angle < 180))
            {
                angle = 180 - angle;
                xs = -1;
            };
            angle = angle * Math.PI / 180f;

            double x0 = ssd.X + ssd.Width / 2f;
            double y0 = ssd.Y + ssd.Height / 2f;
            double r = Math.Sqrt(ssd.Width * ssd.Width + ssd.Height * ssd.Height) / 2f;
            double a2 = Math.Atan2(ssd.Height, ssd.Width);
            double st = Math.PI / 2f - angle + a2;
            double b = r * Math.Sin(st);
            double xr = b * Math.Cos(angle) * xs;
            double yr = b * Math.Sin(angle) * ys;

            double x1 = x0 - xr;
            double x2 = x0 + xr;
            double y1 = y0 + yr;
            double y2 = y0 - yr;

            AddXref(info.Patterns.ShadingItems[indexShading].Ref);
            sw.WriteLine("{0} 0 obj", info.Patterns.ShadingItems[indexShading].Ref);
            sw.WriteLine("<<");
            //			sw.WriteLine("/Type /Pattern");
            sw.WriteLine("/PatternType 2");
            sw.WriteLine("/Shading <<");
            sw.WriteLine("/ColorSpace /DeviceRGB");
            sw.WriteLine("/ShadingType 2");
            sw.WriteLine("/Coords [ {0} {1} {2} {3} ]",
                ConvertToString(x1),
                ConvertToString(y1),
                ConvertToString(x2),
                ConvertToString(y2));
            sw.WriteLine("/Extend [ true true ]");
            sw.WriteLine("/Function {0} 0 R", info.Patterns.ShadingItems[indexShading].Function.Ref);
            sw.WriteLine(">>");
            sw.WriteLine(">>");
            sw.WriteLine("endobj");
            sw.WriteLine("");

            AddXref(info.Patterns.ShadingItems[indexShading].Function.Ref);
            sw.WriteLine("{0} 0 obj", info.Patterns.ShadingItems[indexShading].Function.Ref);
            sw.WriteLine("<<");
            sw.WriteLine("/FunctionType 0");
            sw.WriteLine("/Size [ 3 ]");
            sw.WriteLine("/Decode [ 0 1 0 1 0 1 ]");
            sw.WriteLine("/Range [ 0 1 0 1 0 1 ]");
            sw.WriteLine("/BitsPerSample 8");
            sw.WriteLine("/Domain [ 0 1 ]");
            sw.WriteLine("/Encode [ 0 {0} ]", ssd.IsGlare ? 2 : 1);
            sw.WriteLine("/Order 1");
            //
            memoryPageStream = new MemoryStream();
#if TESTPDF
                string str = string.Format("{0:X2}{1:X2}{2:X2}{3:X2}{4:X2}{5:X2}{0:X2}{1:X2}{2:X2}",
                    ssd.Color1.R,
                    ssd.Color1.G,
                    ssd.Color1.B,
                    ssd.Color2.R,
                    ssd.Color2.G,
                    ssd.Color2.B);
                for (int indexb = 0; indexb < str.Length; indexb++)
                {
                    memoryPageStream.WriteByte((byte)str[indexb]);
                }
                //sw.WriteLine("/Length {0}", memoryPageStream.Length);
                //sw.WriteLine("/Filter [/ASCIIHexDecode]");
                string stShade = "/Length {0} /Filter [/ASCIIHexDecode]";
#else
            memoryPageStream.WriteByte(ssd.Color1.R);
            memoryPageStream.WriteByte(ssd.Color1.G);
            memoryPageStream.WriteByte(ssd.Color1.B);
            memoryPageStream.WriteByte(ssd.Color2.R);
            memoryPageStream.WriteByte(ssd.Color2.G);
            memoryPageStream.WriteByte(ssd.Color2.B);
            memoryPageStream.WriteByte(ssd.Color1.R);
            memoryPageStream.WriteByte(ssd.Color1.G);
            memoryPageStream.WriteByte(ssd.Color1.B);
            //sw.WriteLine("/Length {0}", memoryPageStream.Length);
            string stShade = "/Length {0}";
#endif
            //
            //sw.WriteLine(">>");
            //sw.WriteLine("stream");
            //StoreMemoryStream(memoryPageStream);
            StoreMemoryStream2(memoryPageStream, stShade);

            sw.WriteLine("");
            sw.WriteLine("endstream");
            sw.WriteLine("endobj");
            sw.WriteLine("");
        }
        #endregion

        #region RenderLinkTable
        private void RenderLinkTable()
        {
            if (haveLinks == true)
            {
                for (int index = 0; index < linksCounter; index++)
                {
                    StiLinkObject stl = (StiLinkObject)linksArray[index];

                    //check link - intenal or external; if internal - get destination
                    if (stl.Link.StartsWith("##") && tagsArray.Count > 0)
                    {
                        //internal link to tag
                        string st = stl.Link.Substring(2);
                        for (int indexTag = 0; indexTag < tagsArray.Count; indexTag++)
                        {
                            StiLinkObject tag = tagsArray[indexTag];
                            if (tag.Link == st)
                            {
                                stl.DestPage = tag.Page;
                                stl.DestY = tag.Y;
                                break;
                            }
                        }
                    }
                    else
                    {
                        //internal link to bookmark
                        if ((stl.Link[0] == '#') && (haveBookmarks == true))
                        {
                            string st = stl.Link.Substring(1);
                            if ((st.Length > 2) && (st[0] == '%'))
                            {
                                int pos = st.LastIndexOf(st[1]);
                                if (pos < st.Length - 1)
                                {
                                    st = st.Substring(pos + 1);
                                }
                            }
                            for (int indexBookmark = 0; indexBookmark < bookmarksCounter; indexBookmark++)
                            {
                                StiBookmarkTreeNode tn = (StiBookmarkTreeNode)bookmarksTree[indexBookmark];
                                if (tn.Title == st)
                                {
                                    stl.DestPage = tn.Page;
                                    stl.DestY = tn.Y;
                                    break;
                                }
                            }
                        }
                    }
                    linksArray[index] = stl;

                    AddXref(info.LinkList[index].Ref);
                    sw.WriteLine("{0} 0 obj", info.LinkList[index].Ref);
                    sw.WriteLine("<<");
                    sw.WriteLine("/Type /Annot");
                    sw.WriteLine("/Subtype /Link");
                    sw.WriteLine("/Rect [{0} {1} {2} {3}]", ConvertToString(stl.X), ConvertToString(stl.Y), ConvertToString(stl.X + stl.Width), ConvertToString(stl.Y + stl.Height));
                    sw.WriteLine("/Border [0 0 0]");
                    if (stl.Link[0] == '#')
                    {
                        if (stl.DestY > -1) sw.WriteLine("/Dest [{0} 0 R /XYZ null {1} null]", info.PageList[stl.DestPage].Ref, ConvertToString(stl.DestY));
                        //else sw.WriteLine("/Dest [{0} 0 R /XYZ null null null]", info.PageList[0].Ref);
                    }
                    else
                    {
                        string link = stl.Link.Replace('\\', '/');
                        bool flag = (link.Length > 6) &&
                            ((char.IsLetter(link, 0) && (link[1] == ':') && (link[2] == '/')) ||
                            ((link[0] == '/') && char.IsLetter(link, 1) && (link[2] == '/')));
                        if (flag && (link[1] == ':')) link = "/" + link[0] + link.Substring(2);

                        sw.WriteLine("/A <<");
                        sw.WriteLine("/Type /Action");
                        if (flag)
                        {
                            sw.WriteLine("/S /Launch");
                            sw.WriteLine("/F <<");
                            sw.WriteLine("/Type /Filespec");
                            StoreStringLine("/F ", link);
                            StoreStringLine("/UF ", link);
                            sw.WriteLine(">>");
                        }
                        else
                        {
                            sw.WriteLine("/S /URI");
                            StoreStringLine("/URI ", StiExportUtils.StringToUrl(link));
                        }
                        sw.WriteLine(">>");
                    }
                    sw.WriteLine("/F 4");
                    sw.WriteLine(">>");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");
                }
            }
        }
        #endregion

        #region RenderAnnotTable
        private void RenderAnnotTable()
        {
            if (haveAnnots || haveDigitalSignature || haveTooltips)
            {
                #region AcroForm object
                AddXref(info.AcroForm.Ref);
                sw.WriteLine("{0} 0 obj", info.AcroForm.Ref);
                sw.WriteLine("<<");
                sw.WriteLine("/Fields [");
                for (int index = 0; index < annotsCounter; index++)
                {
                    sw.WriteLine("{0} 0 R", info.AcroForm.Annots[index].Ref);
                }
                for (int index = 0; index < annots2Counter; index++)
                {
                    for (int indexState = 0; indexState < info.AcroForm.CheckBoxes[index].Count; indexState++)
                    {
                        sw.WriteLine("{0} 0 R", info.AcroForm.CheckBoxes[index][indexState].Ref);
                    }
                }
                for (int index = 0; index < unsignedSignaturesCounter; index++)
                {
                    sw.WriteLine("{0} 0 R", info.AcroForm.UnsignedSignatures[index].Ref);
                }
                if (haveDigitalSignature)
                {
                    sw.WriteLine("{0} 0 R", info.AcroForm.Signatures[0].Ref);
                }
                for (int index = 0; index < tooltipsCounter; index++)
                {
                    sw.WriteLine("{0} 0 R", info.AcroForm.Tooltips[index].Ref);
                }
                sw.WriteLine("]");

                sw.WriteLine("/DR <<");
                //if (haveAnnots)
                if (annotsCounter > 0 || annots2Counter > 0)
                {
                    sw.WriteLine("/Font <<");
                    for (int index = 0; index < fontsCounter; index++)
                    {
                        sw.WriteLine("/FA{0} {1} 0 R", index, info.AcroForm.AnnotFontItems[index].Ref);
                    }
                    sw.WriteLine(">>");
                    //sw.WriteLine("/Encoding << /PDFDocEncoding {0} 0 R >>", tempOffset);
                }
                sw.WriteLine(">>");
                if (haveAnnots && (fontsCounter > 0))
                {
                    StoreStringLine("/DA ", "/FA0 0 Tf 0 g");
                }
                if (haveDigitalSignature)
                {
                    sw.WriteLine("/SigFlags 3");
                }
                sw.WriteLine(">>");
                sw.WriteLine("endobj");
                sw.WriteLine("");
                #endregion

                if (haveAnnots)
                {
                    Hashtable fieldsNames = new Hashtable();

                    for (int index = 0; index < annotsCounter; index++)
                    {
                        StiEditableObject seo = (StiEditableObject)annotsArray[index];

                        #region Make field name
                        string fieldName = string.Empty;
                        if (StiOptions.Export.Pdf.UseEditableFieldName) fieldName = seo.Component.Name;
                        if (StiOptions.Export.Pdf.UseEditableFieldAlias) fieldName = seo.Component.Alias;
                        if (StiOptions.Export.Pdf.UseEditableFieldTag) fieldName = seo.Component.TagValue as string;
                        if (string.IsNullOrEmpty(fieldName)) fieldName = string.Format("Field{0}", index);
                        if (fieldsNames.ContainsKey(fieldName))
                        {
                            int indexName = 2;
                            string nameAdd = string.Empty;
                            while (fieldsNames.ContainsKey(fieldName + nameAdd))
                            {
                                nameAdd = "_" + indexName.ToString();
                                indexName++;
                            }
                            fieldName = fieldName + nameAdd;
                        }
                        fieldsNames.Add(fieldName, fieldName);
                        #endregion

                        #region Annot object
                        AddXref(info.AcroForm.Annots[index].Ref);
                        sw.WriteLine("{0} 0 obj", info.AcroForm.Annots[index].Ref);
                        sw.WriteLine("<<");
                        sw.WriteLine("/Type /Annot");
                        sw.WriteLine("/Subtype /Widget");
                        sw.WriteLine("/Rect [{0} {1} {2} {3}]",
                            ConvertToString(seo.X),
                            ConvertToString(seo.Y),
                            ConvertToString(seo.X + seo.Width),
                            ConvertToString(seo.Y + seo.Height));
                        sw.WriteLine("/F 4");
                        sw.WriteLine("/P {0} 0 R", info.PageList[seo.Page].Ref);
                        sw.WriteLine("/FT /Tx");
                        sw.WriteLine("/BS << /W 0 >>");
                        StoreStringLine("/T ", fieldName);
                        sw.WriteLine("/MK << /TP 2 >>");
                        sw.WriteLine("/H /P");
                        sw.WriteLine("/AP << /N {0} 0 R >>", info.AcroForm.Annots[index].AP.Ref);
                        int flagFf = seo.Multiline ? 0x1000 : 0;
                        if (((seo.Component as StiText) != null) && ((seo.Component as StiText).TextOptions != null))
                        {
                            if ((seo.Component as StiText).TextOptions.LineLimit) flagFf |= 0x800000;
                        }
                        sw.WriteLine("/Ff {0}", flagFf);
                        //  "/MaxLen 55"  //Limit of NN characters
                        StoreStringLine("/DA ", string.Format("/FA{0} {1} Tf {2} {3} {4} rg",
                            seo.FontNumber,
                            ConvertToString(seo.FontSize, precision_digits_font),
                            colorTable[seo.FontColor.R],
                            colorTable[seo.FontColor.G],
                            colorTable[seo.FontColor.B]));
                        StoreStringLine("/V ", seo.Text, true);
                        StoreStringLine("/DV ", seo.Text, true);
                        sw.WriteLine("/Q {0}", (seo.Alignment == StiTextHorAlignment.Center ? 1 : 0) +
                            (seo.Alignment == StiTextHorAlignment.Right ? 2 : 0));
                        sw.WriteLine(">>");
                        sw.WriteLine("endobj");
                        sw.WriteLine("");
                        #endregion

                        #region /AP object
                        AddXref(info.AcroForm.Annots[index].AP.Ref);
                        sw.WriteLine("{0} 0 obj", info.AcroForm.Annots[index].AP.Ref);
                        sw.WriteLine("<<");
                        sw.WriteLine("/Subtype /Form");
                        sw.WriteLine("/BBox [{0} {1} {2} {3}]",
                            ConvertToString(0),
                            ConvertToString(0),
                            ConvertToString(seo.Width),
                            ConvertToString(seo.Height));
                        sw.WriteLine("/Resources <<");
                        sw.WriteLine("/ProcSet [ /PDF /Text ]");
                        sw.WriteLine("/Font << /F{0} {1} 0 R >> >>", seo.FontNumber, info.FontList[seo.FontNumber].Ref);

                        MemoryStream annotMemSw = new MemoryStream();
                        StreamWriter annotSw = new StreamWriter(annotMemSw, Encoding.GetEncoding(1252));

                        //stream
                        annotSw.WriteLine("/Tx BMC");
                        annotSw.WriteLine("q");
                        int borderOffset = 1;
                        annotSw.WriteLine("{0} {1} {2} {3} re W n",
                            ConvertToString(borderOffset),
                            ConvertToString(borderOffset),
                            ConvertToString(seo.Width - borderOffset),
                            ConvertToString(seo.Height - borderOffset));
                        annotSw.Flush();
                        annotSw.BaseStream.Write(seo.Content, 0, seo.Content.Length);
                        annotSw.WriteLine("Q");
                        annotSw.WriteLine("EMC");
                        annotSw.Flush();

                        //sw.WriteLine("/Filter [] /Length {0}", annotMemSw.Length);
                        //sw.WriteLine(">>");
                        //sw.WriteLine("stream");
                        //StoreMemoryStream(annotMemSw);
                        StoreMemoryStream2(annotMemSw, "/Filter [] /Length {0}");

                        sw.WriteLine("");
                        annotSw.Close();
                        annotMemSw.Close();

                        sw.WriteLine("endstream");
                        sw.WriteLine("endobj");
                        sw.WriteLine("");
                        #endregion
                    }

                    for (int indexFont = 0; indexFont < fontsCounter; indexFont++)
                    {
                        #region Annot font 
                        pdfFont.CurrentFont = indexFont;
                        PdfFonts.pfontInfo tempInfo = (PdfFonts.pfontInfo)pdfFont.fontList[indexFont];
                        StringBuilder tempSb = new StringBuilder(tempInfo.Name);
                        tempSb.Replace(" ", "#20");
                        if (tempInfo.Bold || tempInfo.Italic)
                        {
                            tempSb.Append(",");
                            if (tempInfo.Bold) tempSb.Append("Bold");
                            if (tempInfo.Italic) tempSb.Append("Italic");
                        }

                        AddXref(info.AcroForm.AnnotFontItems[indexFont].Ref);
                        sw.WriteLine("{0} 0 obj", info.AcroForm.AnnotFontItems[indexFont].Ref);
                        sw.WriteLine("<<");
                        sw.WriteLine("/Type /Font");
                        sw.WriteLine("/Subtype /TrueType");
                        sw.WriteLine("/Name /FA{0}", indexFont);
                        sw.WriteLine("/BaseFont /{0}", tempSb);
                        sw.WriteLine("/Encoding /WinAnsiEncoding");
                        sw.WriteLine("/FontDescriptor {0} 0 R", info.AcroForm.AnnotFontItems[indexFont].FontDescriptor.Ref);

                        if (fontGlyphsReduceNotNeed[indexFont])
                        {
                            //make string "Widths"
                            StringBuilder sbWidths = new StringBuilder(" ");
                            for (int indexUni = 32; indexUni < 256; indexUni++)
                            {
                                sbWidths.Append(pdfFont.Widths[pdfFont.UnicodeMap[CodePage1252[indexUni]] - 32].ToString() + " ");
                            }
                            sw.WriteLine("/FirstChar 32");
                            sw.WriteLine("/LastChar 255");
                            sw.WriteLine("/Widths [{0}]", sbWidths);
                        }

                        sw.WriteLine(">>");
                        sw.WriteLine("endobj");
                        sw.WriteLine("");

                        AddXref(info.AcroForm.AnnotFontItems[indexFont].FontDescriptor.Ref);
                        sw.WriteLine("{0} 0 obj", info.AcroForm.AnnotFontItems[indexFont].FontDescriptor.Ref);
                        sw.WriteLine("<<");
                        sw.WriteLine("/Type /FontDescriptor");
                        sw.WriteLine("/FontName /{0}", tempSb);
                        sw.WriteLine("/Flags 32");
                        //sw.WriteLine("/Ascent {0}", pdfFont.ASC);
                        sw.WriteLine("/Ascent {0}", pdfFont.MacAscend);
                        sw.WriteLine("/CapHeight {0}", pdfFont.CH);
                        //sw.WriteLine("/Descent {0}", pdfFont.DESC);
                        sw.WriteLine("/Descent {0}", pdfFont.MacDescend);
                        sw.WriteLine("/FontBBox [{0} {1} {2} {3}]", pdfFont.LLX, pdfFont.LLY, pdfFont.URX, pdfFont.URY);
                        sw.WriteLine("/ItalicAngle {0}", pdfFont.ItalicAngle);
                        sw.WriteLine("/StemV {0}", pdfFont.StemV);
                        if (embeddedFonts == true)
                        {
                            sw.WriteLine("/FontFile2 {0} 0 R", info.FontList[indexFont].FontFile2.Ref);
                        }
                        sw.WriteLine(">>");
                        sw.WriteLine("endobj");
                        sw.WriteLine("");
                        #endregion
                    }

                    for (int index = 0; index < annots2Counter; index++)
                    {
                        StiEditableObject seo = annots2Array[index];

                        #region Make field name
                        string fieldName = string.Empty;
                        if (StiOptions.Export.Pdf.UseEditableFieldName) fieldName = seo.Component.Name;
                        if (StiOptions.Export.Pdf.UseEditableFieldAlias) fieldName = seo.Component.Alias;
                        if (StiOptions.Export.Pdf.UseEditableFieldTag) fieldName = seo.Component.TagValue as string;
                        if (string.IsNullOrEmpty(fieldName)) fieldName = string.Format("Checkbox{0}", index + 1);
                        if (fieldsNames.ContainsKey(fieldName))
                        {
                            int indexName = 2;
                            string nameAdd = string.Empty;
                            while (fieldsNames.ContainsKey(fieldName + nameAdd))
                            {
                                nameAdd = "_" + indexName.ToString();
                                indexName++;
                            }
                            fieldName = fieldName + nameAdd;
                        }
                        fieldsNames.Add(fieldName, fieldName);
                        #endregion

                        const double checkboxSizeCorrection = 0.01;

                        StiCheckBox checkbox = seo.Component as StiCheckBox;
                        bool checkBoxValue = GetCheckBoxValue(checkbox).GetValueOrDefault(false);

                        if (info.AcroForm.CheckBoxes[index].Count > 1)
                        {
                            #region Annot object - Yes
                            var annot = info.AcroForm.CheckBoxes[index][0];
                            AddXref(annot.Ref);
                            sw.WriteLine("{0} 0 obj", annot.Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/Type /Annot");
                            sw.WriteLine("/Subtype /Widget");
                            sw.WriteLine("/FT /Btn");
                            sw.WriteLine("/Rect [{0} {1} {2} {3}]",
                                ConvertToString(seo.X),
                                ConvertToString(seo.Y),
                                ConvertToString(seo.X + seo.Width - checkboxSizeCorrection),
                                ConvertToString(seo.Y + seo.Height - checkboxSizeCorrection));
                            sw.WriteLine("/F {0}", 4 + (info.AcroForm.CheckBoxes[index].Count == 3 || !checkBoxValue ? 2 : 0));
                            sw.WriteLine("/P {0} 0 R", info.PageList[seo.Page].Ref);
                            StoreStringLine("/T ", fieldName + "Yes");
                            sw.WriteLine("/MK << >>");
                            sw.WriteLine("/Ff 65536");
                            sw.WriteLine("/A {0} 0 R", annot.AA[0].Ref);
                            sw.WriteLine("/AP << /N {0} 0 R >>", annot.AP.Ref);
                            sw.WriteLine("/H /O");
                            sw.WriteLine(">>");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");
                            #endregion

                            #region /AP object
                            AddXref(annot.AP.Ref);
                            sw.WriteLine("{0} 0 obj", annot.AP.Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/Type /XObject");
                            sw.WriteLine("/Subtype /Form");
                            sw.WriteLine("/FormType 1");
                            sw.WriteLine("/BBox [{0} {1} {2} {3}]",
                                ConvertToString(0),
                                ConvertToString(0),
                                ConvertToString(seo.Width),
                                ConvertToString(seo.Height));
                            sw.WriteLine("/Matrix [ 1 0 0 1 0 0 ]");
                            sw.WriteLine("/Resources <<");
                            sw.WriteLine("/ProcSet [ /PDF ]");
                            sw.WriteLine("/Pattern");
                            sw.WriteLine("<<");
                            for (int indexPattern = 0; indexPattern < hatchCounter; indexPattern++)
                            {
                                sw.WriteLine(string.Format("/PH{0} {1} 0 R", 1 + indexPattern, info.Patterns.HatchItems[indexPattern].Ref));
                            }
                            for (int indexPattern = 0; indexPattern < shadingCounter; indexPattern++)
                            {
                                sw.WriteLine(string.Format("/P{0} {1} 0 R", 2 + indexPattern, info.Patterns.ShadingItems[indexPattern].Ref));
                            }
                            sw.WriteLine(">>");
                            sw.WriteLine("/ColorSpace 3 0 R");
                            sw.WriteLine(">>");

                            MemoryStream annotMemSw = new MemoryStream();
                            StreamWriter annotSw = new StreamWriter(annotMemSw, Encoding.GetEncoding(1252));

                            //stream
                            annotSw.WriteLine("/Tx BMC");
                            annotSw.WriteLine("q");
                            int borderOffset = 1;
                            annotSw.WriteLine("{0} {1} {2} {3} re W n",
                                ConvertToString(borderOffset),
                                ConvertToString(borderOffset),
                                ConvertToString(seo.Width - borderOffset),
                                ConvertToString(seo.Height - borderOffset));
                            annotSw.Flush();
                            annotSw.BaseStream.Write(seo.Content, 0, seo.Content.Length);
                            annotSw.WriteLine("Q");
                            annotSw.WriteLine("EMC");
                            annotSw.Flush();

                            StoreMemoryStream2(annotMemSw, "/Filter [] /Length {0}");

                            sw.WriteLine("");
                            annotSw.Close();
                            annotMemSw.Close();

                            sw.WriteLine("endstream");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");
                            #endregion

                            #region /AA objects
                            AddXref(annot.AA[0].Ref);
                            sw.WriteLine("{0} 0 obj", annot.AA[0].Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/Next {0} 0 R", annot.AA[1].Ref);
                            sw.WriteLine("/S /Hide");
                            sw.WriteLine("/T ({0})", fieldName + "Yes");
                            sw.WriteLine(">>");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");

                            AddXref(annot.AA[1].Ref);
                            sw.WriteLine("{0} 0 obj", annot.AA[1].Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/H false");
                            sw.WriteLine("/S /Hide");
                            sw.WriteLine("/T ({0})", fieldName + "No");
                            sw.WriteLine(">>");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");
                            #endregion

                            #region Annot object - No
                            annot = info.AcroForm.CheckBoxes[index][1];
                            AddXref(annot.Ref);
                            sw.WriteLine("{0} 0 obj", annot.Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/Type /Annot");
                            sw.WriteLine("/Subtype /Widget");
                            sw.WriteLine("/FT /Btn");
                            sw.WriteLine("/Rect [{0} {1} {2} {3}]",
                                ConvertToString(seo.X),
                                ConvertToString(seo.Y),
                                ConvertToString(seo.X + seo.Width - checkboxSizeCorrection),
                                ConvertToString(seo.Y + seo.Height - checkboxSizeCorrection));
                            sw.WriteLine("/F {0}", 4 + (info.AcroForm.CheckBoxes[index].Count == 3 || checkBoxValue ? 2 : 0));
                            sw.WriteLine("/P {0} 0 R", info.PageList[seo.Page].Ref);
                            StoreStringLine("/T ", fieldName + "No");
                            sw.WriteLine("/MK << >>");
                            sw.WriteLine("/Ff 65536");
                            sw.WriteLine("/A {0} 0 R", annot.AA[0].Ref);
                            sw.WriteLine("/AP << /N {0} 0 R >>", annot.AP.Ref);
                            sw.WriteLine("/H /O");
                            sw.WriteLine(">>");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");
                            #endregion

                            #region /AP object
                            AddXref(annot.AP.Ref);
                            sw.WriteLine("{0} 0 obj", annot.AP.Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/Type /XObject");
                            sw.WriteLine("/Subtype /Form");
                            sw.WriteLine("/FormType 1");
                            sw.WriteLine("/BBox [{0} {1} {2} {3}]",
                                ConvertToString(0),
                                ConvertToString(0),
                                ConvertToString(seo.Width),
                                ConvertToString(seo.Height));
                            sw.WriteLine("/Matrix [ 1 0 0 1 0 0 ]");
                            sw.WriteLine("/Resources <<");
                            sw.WriteLine("/ProcSet [ /PDF ]");
                            sw.WriteLine("/Pattern");
                            sw.WriteLine("<<");
                            for (int indexPattern = 0; indexPattern < hatchCounter; indexPattern++)
                            {
                                sw.WriteLine(string.Format("/PH{0} {1} 0 R", 1 + indexPattern, info.Patterns.HatchItems[indexPattern].Ref));
                            }
                            for (int indexPattern = 0; indexPattern < shadingCounter; indexPattern++)
                            {
                                sw.WriteLine(string.Format("/P{0} {1} 0 R", 2 + indexPattern, info.Patterns.ShadingItems[indexPattern].Ref));
                            }
                            sw.WriteLine(">>");
                            sw.WriteLine("/ColorSpace 3 0 R");
                            sw.WriteLine(">>");

                            annotMemSw = new MemoryStream();
                            annotSw = new StreamWriter(annotMemSw, Encoding.GetEncoding(1252));

                            //stream
                            annotSw.WriteLine("/Tx BMC");
                            annotSw.WriteLine("q");
                            borderOffset = 1;
                            annotSw.WriteLine("{0} {1} {2} {3} re W n",
                                ConvertToString(borderOffset),
                                ConvertToString(borderOffset),
                                ConvertToString(seo.Width - borderOffset),
                                ConvertToString(seo.Height - borderOffset));
                            annotSw.Flush();
                            annotSw.BaseStream.Write(seo.Content2, 0, seo.Content2.Length);
                            annotSw.WriteLine("Q");
                            annotSw.WriteLine("EMC");
                            annotSw.Flush();

                            StoreMemoryStream2(annotMemSw, "/Filter [] /Length {0}");

                            sw.WriteLine("");
                            annotSw.Close();
                            annotMemSw.Close();

                            sw.WriteLine("endstream");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");
                            #endregion

                            #region /AA objects
                            AddXref(annot.AA[0].Ref);
                            sw.WriteLine("{0} 0 obj", annot.AA[0].Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/H false");
                            sw.WriteLine("/Next {0} 0 R", annot.AA[1].Ref);
                            sw.WriteLine("/S /Hide");
                            sw.WriteLine("/T ({0})", fieldName + "Yes");
                            sw.WriteLine(">>");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");

                            AddXref(annot.AA[1].Ref);
                            sw.WriteLine("{0} 0 obj", annot.AA[1].Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/S /Hide");
                            sw.WriteLine("/T ({0})", fieldName + "No");
                            sw.WriteLine(">>");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");
                            #endregion
                        }

                        if (info.AcroForm.CheckBoxes[index].Count == 3)
                        {
                            #region Annot object - None
                            var annot = info.AcroForm.CheckBoxes[index][2];
                            AddXref(annot.Ref);
                            sw.WriteLine("{0} 0 obj", annot.Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/Type /Annot");
                            sw.WriteLine("/Subtype /Widget");
                            sw.WriteLine("/FT /Btn");
                            sw.WriteLine("/Rect [{0} {1} {2} {3}]",
                                ConvertToString(seo.X),
                                ConvertToString(seo.Y),
                                ConvertToString(seo.X + seo.Width - checkboxSizeCorrection),
                                ConvertToString(seo.Y + seo.Height - checkboxSizeCorrection));
                            sw.WriteLine("/F 4");
                            sw.WriteLine("/P {0} 0 R", info.PageList[seo.Page].Ref);
                            StoreStringLine("/T ", fieldName + "None");
                            sw.WriteLine("/MK << >>");
                            sw.WriteLine("/Ff 65536");
                            sw.WriteLine("/A {0} 0 R", annot.AA[0].Ref);
                            sw.WriteLine("/H /O");
                            sw.WriteLine(">>");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");
                            #endregion

                            #region /AA objects
                            AddXref(annot.AA[0].Ref);
                            sw.WriteLine("{0} 0 obj", annot.AA[0].Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/Next {0} 0 R", annot.AA[1].Ref);
                            sw.WriteLine("/S /Hide");
                            sw.WriteLine("/T ({0})", fieldName + "None");
                            sw.WriteLine(">>");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");

                            AddXref(annot.AA[1].Ref);
                            sw.WriteLine("{0} 0 obj", annot.AA[1].Ref);
                            sw.WriteLine("<<");
                            sw.WriteLine("/H false");
                            sw.WriteLine("/S /Hide");
                            sw.WriteLine("/T ({0})", fieldName + (checkbox.CheckStyleForTrue == StiCheckStyle.None ? "No" : "Yes"));
                            sw.WriteLine(">>");
                            sw.WriteLine("endobj");
                            sw.WriteLine("");
                            #endregion
                        }
                    }

                    for (int index = 0; index < unsignedSignaturesCounter; index++)
                    {
                        StiEditableObject seo = unsignedSignaturesArray[index];

                        #region Make field name
                        string fieldName = string.Empty;
                        if (StiOptions.Export.Pdf.UseEditableFieldName) fieldName = seo.Component.Name;
                        if (StiOptions.Export.Pdf.UseEditableFieldAlias) fieldName = seo.Component.Alias;
                        if (StiOptions.Export.Pdf.UseEditableFieldTag) fieldName = seo.Component.TagValue as string;
                        if (string.IsNullOrEmpty(fieldName)) fieldName = string.Format("UnsignedSignature{0}", index);
                        if (fieldsNames.ContainsKey(fieldName))
                        {
                            int indexName = 2;
                            string nameAdd = string.Empty;
                            while (fieldsNames.ContainsKey(fieldName + nameAdd))
                            {
                                nameAdd = "_" + indexName.ToString();
                                indexName++;
                            }
                            fieldName = fieldName + nameAdd;
                        }
                        fieldsNames.Add(fieldName, fieldName);
                        #endregion

                        #region Annot object
                        int indexOffset3 = info.AcroForm.UnsignedSignatures[index].Ref;
                        AddXref(indexOffset3);
                        sw.WriteLine("{0} 0 obj", indexOffset3);
                        sw.WriteLine("<<");
                        sw.WriteLine("/Type /Annot");
                        sw.WriteLine("/Subtype /Widget");
                        sw.WriteLine("/Rect [{0} {1} {2} {3}]",
                            ConvertToString(seo.X),
                            ConvertToString(seo.Y),
                            ConvertToString(seo.X + seo.Width),
                            ConvertToString(seo.Y + seo.Height));
                        sw.WriteLine("/F 4");
                        sw.WriteLine("/P {0} 0 R", info.PageList[seo.Page].Ref);
                        sw.WriteLine("/FT /Sig");
                        StoreStringLine("/T ", fieldName);
                        sw.WriteLine("/MK << >>");
                        sw.WriteLine(">>");
                        sw.WriteLine("endobj");
                        sw.WriteLine("");
                        #endregion
                    }
                }
            }
        }
        #endregion

        #region RenderSignatureTable
        private void RenderSignatureTable(StiReport report)
        {
            if (haveDigitalSignature == true)
            {
                string signedBy = "Stimulsoft Reports.Net";
                if (string.IsNullOrEmpty(digitalSignatureSignedBy))
                {
                    signedBy = pdfSecurity.MakeSignedByString(signedBy);
                }
                else
                {
                    signedBy = pdfSecurity.MakeSignedByString(digitalSignatureSignedBy, false);
                }

                AddXref(info.AcroForm.Signatures[0].Ref);
                sw.WriteLine("{0} 0 obj", info.AcroForm.Signatures[0].Ref);
                sw.WriteLine("<<");
                sw.WriteLine("/Rect [{0} {1} {2} {3}]",
                    ConvertToString(signaturePlacement.Left),
                    ConvertToString(signaturePlacement.Bottom),
                    ConvertToString(signaturePlacement.Right),
                    ConvertToString(signaturePlacement.Top));
                sw.WriteLine("/Type /Annot");
                sw.WriteLine("/Subtype /Widget");
                sw.WriteLine("/F 132");
                sw.WriteLine("/P {0} 0 R", info.PageList[signaturePageNumber].Ref);
                StoreStringLine("/T ", "Signature1");
                sw.WriteLine("/V {0} 0 R", info.AcroForm.Signatures[0].AP.Ref);
                StoreStringLine("/DA ", "0 g");
                sw.WriteLine("/FT /Sig");
                sw.WriteLine("/MK<<>>");
                sw.WriteLine(">>");
                sw.WriteLine("endobj");
                sw.WriteLine("");

                AddXref(info.AcroForm.Signatures[0].AP.Ref);
                sw.WriteLine("{0} 0 obj", info.AcroForm.Signatures[0].AP.Ref);
                sw.WriteLine("<<");
                sw.WriteLine("/Type /Sig");
                sw.Write("/Filter /");
                sw.Flush();
                offsetSignatureFilter = (int)(sw.BaseStream.Position);
                sw.WriteLine("Adobe.PPKLite          ");
                sw.WriteLine("/SubFilter /adbe.pkcs7.detached");

                //StoreStringLine("/Name ", signedBy);
                sw.Write("/Name ");
                sw.Flush();
                offsetSignatureName = (int)(sw.BaseStream.Position);
                sw.WriteLine(signedBy);

                sw.Write("/Contents<{0}>", new string('0', signatureDataLen));
                sw.Flush();
                offsetSignatureData = (int)(sw.BaseStream.Position - signatureDataLen - 2);
                sw.WriteLine("");
                StoreStringLine("/M ", currentDateTime);
                if (!string.IsNullOrEmpty(digitalSignatureReason))
                {
                    StoreStringLine("/Reason ", digitalSignatureReason);
                }
                if (!string.IsNullOrEmpty(digitalSignatureLocation))
                {
                    StoreStringLine("/Location ", digitalSignatureLocation);
                }
                if (!string.IsNullOrEmpty(digitalSignatureContactInfo))
                {
                    StoreStringLine("/ContactInfo ", digitalSignatureContactInfo);
                }
                sw.Write("/ByteRange [0 {0} {1} ",
                    offsetSignatureData,
                    offsetSignatureData + signatureDataLen + 2);
                sw.Flush();
                offsetSignatureLen2 = (int)(sw.BaseStream.Position);
                sw.WriteLine("0     ]");
                sw.WriteLine("/Prop_Build");
                sw.WriteLine("<<");

                sw.WriteLine("/App<<");
                string creatorString = StiOptions.Export.Pdf.CreatorString;
                int posFrom = creatorString.ToLowerInvariant().IndexOf(" from ");
                if (posFrom != -1) creatorString = creatorString.Substring(0, posFrom);
                sw.WriteLine("/Name/{0}", creatorString.Replace(" ", "#20"));
                sw.WriteLine(">>");

                sw.WriteLine(">>");
                sw.WriteLine(">>");
                sw.WriteLine("endobj");
                sw.WriteLine("");
            }
        }
        #endregion

        #region RenderTooltipTable
        private void RenderTooltipTable()
        {
            if (haveTooltips == true)
            {
                for (int index = 0; index < tooltipsCounter; index++)
                {
                    StiLinkObject stl = (StiLinkObject)tooltipsArray[index];

                    //find matches hyperlink if one exists
                    bool needHyperlink = false;
                    StiLinkObject stlink = new StiLinkObject();
                    for (int indexLink = 0; indexLink < linksCounter; indexLink++)
                    {
                        stlink = (StiLinkObject)linksArray[indexLink];
                        if (stlink.X == stl.X &&
                            stlink.Y == stl.Y &&
                            stlink.Width == stl.Width &&
                            stlink.Height == stl.Height &&
                            stlink.Page - 1 == stl.Page)
                        {
                            needHyperlink = true;
                            break;
                        }
                    }

                    // /Type /Annot
                    // /Subtype /Widget
                    // /Rect [ 10.2443 718.772 84.9895 784.032 ]
                    // /TU (It's tooltip for this text)
                    // /F 4
                    // /P 5 0 R
                    // /T (Check Box1)
                    // /FT /Btn
                    // /Ff 65536
                    // /H /N
                    //// /MK <<
                    //// /BG [ 1 ]
                    //// /BC [ 0 ]
                    //// >>
                    // /AP <<
                    //// /N 20 0 R
                    // >>

                    AddXref(info.AcroForm.Tooltips[index].Ref);
                    sw.WriteLine("{0} 0 obj", info.AcroForm.Tooltips[index].Ref);
                    sw.WriteLine("<<");
                    sw.WriteLine("/Type /Annot");
                    sw.WriteLine("/Subtype /Widget");
                    sw.WriteLine("/Rect [{0} {1} {2} {3}]",
                        ConvertToString(stl.X),
                        ConvertToString(stl.Y),
                        ConvertToString(stl.X + stl.Width),
                        ConvertToString(stl.Y + stl.Height));
                    StoreStringLine("/TU ", stl.Link);
                    sw.WriteLine("/F 0");
                    sw.WriteLine("/P {0} 0 R", info.PageList[stl.Page].Ref);
                    StoreStringLine("/T ", string.Format("Tooltip{0}", index));
                    sw.WriteLine("/FT /Btn");
                    sw.WriteLine("/Ff 65536");
                    sw.WriteLine("/H /N");
                    //sw.WriteLine("/MK <<");
                    //sw.WriteLine("/BG [ 1 ]");
                    //sw.WriteLine("/BC [ 0 ]");
                    //sw.WriteLine(">>");
                    //sw.WriteLine("/AP << /N {0} 0 R >>", xannotOffset + annotsCounter + index + 1);
                    sw.WriteLine("/AP <<");
                    sw.WriteLine(">>");
                    //sw.WriteLine("/N {0} 0 R", indexOffset + tooltipsCounter + index);
                    if (needHyperlink)
                    {
                        #region Hyperlink info
                        if (stlink.Link[0] == '#')
                        {
                            if (stlink.DestY > -1) sw.WriteLine("/Dest [{0} 0 R /XYZ null {1} null]", info.PageList[stlink.DestPage].Ref, ConvertToString(stlink.DestY));
                            else sw.WriteLine("/Dest [{0} 0 R /XYZ null null null]", info.PageList[0].Ref);
                        }
                        else
                        {
                            sw.WriteLine("/A <<");
                            sw.WriteLine("/Type /Action");
                            sw.WriteLine("/S /URI");
                            //StoreStringLine("/URI ", ConvertToEscapeSequence(stlink.Link.Replace('\\', '/')));
                            StoreStringLine("/URI ", StiExportUtils.StringToUrl(stlink.Link.Replace('\\', '/')));
                            sw.WriteLine(">>");
                        }
                        #endregion
                    }
                    sw.WriteLine(">>");
                    sw.WriteLine("endobj");
                    sw.WriteLine("");
                }
            }
        }
        #endregion

        #region RenderEncodeRecord
        private void RenderEncodeRecord()
        {
            if (encrypted)
            {
                AddXref(info.Encode.Ref);
                sw.WriteLine("{0} 0 obj", info.Encode.Ref);
                sw.WriteLine("<<");

                pdfSecurity.RenderEncodeRecord(sw);

                sw.WriteLine(">>");
                sw.WriteLine("endobj");
                sw.WriteLine("");
            }
        }
        #endregion

        #region RenderExtGStateRecord
        private void RenderExtGStateRecord()
        {
            AddXref(info.ExtGState.Ref);
            sw.WriteLine("{0} 0 obj", info.ExtGState.Ref);
            sw.WriteLine("<<");

            for (int index = 0; index < 256; index++)
            {
                if (alphaTable[index])
                {
                    string stNum = string.Format("{0:X2}", index);
                    sw.WriteLine("/GS{0}S <</Type /ExtGState /BM /Normal /CA {1}>>", stNum, colorTable[index]);
                    sw.WriteLine("/GS{0}N <</Type /ExtGState /BM /Normal /ca {1}>>", stNum, colorTable[index]);
                }
            }

            sw.WriteLine(">>");
            sw.WriteLine("endobj");
            sw.WriteLine("");
        }
        #endregion

        #region RenderBorder
        private void RenderBorder1(StiPdfData pp)
        {
            //fill band
            Color tempColor = Color.Transparent;
            IStiBrush mBrush = pp.Component as IStiBrush;
            if ((mBrush != null) && (mBrush.Brush != null))
            {
                tempColor = StiBrush.ToColor(mBrush.Brush);
            }
            StiRichText mRich = pp.Component as StiRichText;
            if (mRich != null)
            {
                tempColor = mRich.BackColor;
            }

            //fill color
            SetNonStrokeColor(tempColor);
            if (mBrush != null && mBrush.Brush != null)
            {
                StoreShadingData2(pp.X, pp.Y, pp.Width, pp.Height, mBrush.Brush);
                if (mBrush.Brush is StiGradientBrush || mBrush.Brush is StiGlareBrush)
                {
                    pageStream.WriteLine("/Pattern cs /P{0} scn", 1 + shadingCurrent);
                }
                if (mBrush.Brush is StiHatchBrush)
                {
                    StiHatchBrush hBrush = mBrush.Brush as StiHatchBrush;
                    pageStream.WriteLine("/Cs1 cs /PH{0} scn", GetHatchNumber(hBrush) + 1);
                }
                if (mBrush.Brush is StiGlassBrush)
                {
                    StiGlassBrush glass = mBrush.Brush as StiGlassBrush;
                    if (glass.DrawHatch)
                    {
                        pageStream.WriteLine("/Cs1 cs /PH{0} scn", GetHatchNumber(glass.GetTopBrush() as HatchBrush) + 1);
                    }
                    else
                    {
                        SetNonStrokeColor(glass.GetTopColor());
                    }
                    pageStream.WriteLine("{0} {1} {2} {3} re f",
                        ConvertToString(pp.X),
                        ConvertToString(pp.Y + pp.Height / 2),
                        ConvertToString(pp.Width),
                        ConvertToString(pp.Height / 2));
                    if (glass.DrawHatch)
                    {
                        pageStream.WriteLine("/Cs1 cs /PH{0} scn", GetHatchNumber(glass.GetBottomBrush() as HatchBrush) + 1);
                    }
                    else
                    {
                        SetNonStrokeColor(glass.GetBottomColor());
                    }
                    pageStream.WriteLine("{0} {1} {2} {3} re f",
                        ConvertToString(pp.X),
                        ConvertToString(pp.Y),
                        ConvertToString(pp.Width),
                        ConvertToString(pp.Height / 2));
                    tempColor = Color.Transparent;
                }
            }
            if (tempColor.A != 0)
            {
                pageStream.WriteLine("{0} {1} {2} {3} re f",
                    ConvertToString(pp.X),
                    ConvertToString(pp.Y),
                    ConvertToString(pp.Width),
                    ConvertToString(pp.Height));
            }
        }

        private void RenderBorder2(StiPdfData pp)
        {
            //draw border
            IStiBorder mBorder = pp.Component as IStiBorder;
            if (mBorder != null)
            {
                Color tempColor = Color.Transparent;

                #region draw shadow
                if ((mBorder.Border.DropShadow) && (mBorder.Border.ShadowBrush != null))
                {
                    tempColor = StiBrush.ToColor(mBorder.Border.ShadowBrush);
                    if (tempColor.A != 0)
                    {
                        double shadowSize = mBorder.Border.ShadowSize * 0.8;
                        //fill color
                        SetNonStrokeColor(tempColor);
                        pageStream.WriteLine("{0} {1} {2} {3} re f",
                            ConvertToString(pp.X + shadowSize),
                            ConvertToString(pp.Y - shadowSize),
                            ConvertToString(pp.Width - shadowSize),
                            ConvertToString(shadowSize));
                        pageStream.WriteLine("{0} {1} {2} {3} re f",
                            ConvertToString(pp.X + pp.Width),
                            ConvertToString(pp.Y - shadowSize),
                            ConvertToString(shadowSize),
                            ConvertToString(pp.Height));
                    }
                }
                #endregion

                StiBorderSide border = new StiBorderSide(mBorder.Border.Color, mBorder.Border.Size, mBorder.Border.Style);
                StiAdvancedBorder advBorder = mBorder.Border as StiAdvancedBorder;
                bool useAdvBorder = advBorder != null;

                bool needBorderLeft = mBorder.Border.IsLeftBorderSidePresent;
                bool needBorderRight = mBorder.Border.IsRightBorderSidePresent;
                bool needBorderTop = mBorder.Border.IsTopBorderSidePresent;
                bool needBorderBottom = mBorder.Border.IsBottomBorderSidePresent;
                bool needPush = (mBorder.Border.Style != StiPenStyle.None) && (mBorder.Border.Style != StiPenStyle.Solid);
                bool needDraw = (mBorder.Border.Side != StiBorderSides.None) && (mBorder.Border.Style != StiPenStyle.None);
                if (useAdvBorder)
                {
                    needDraw = advBorder.Side != StiBorderSides.None;
                }

                if (needDraw)
                {
                    double offset = 0;
                    if (!useAdvBorder)
                    {
                        if (needPush)
                        {
                            pageStream.WriteLine("q");
                            PushColorToStack();
                        }
                        offset = StoreBorderSideData(border);
                    }

                    if ((!useAdvBorder) && (mBorder.Border.Side == StiBorderSides.All))
                    {
                        #region stroke border
                        pageStream.WriteLine("{0} {1} {2} {3} re S",
                            ConvertToString(pp.X - offset),
                            ConvertToString(pp.Y - offset),
                            ConvertToString(pp.Width + offset * 2),
                            ConvertToString(pp.Height + offset * 2));
                        if (mBorder.Border.Style == StiPenStyle.Double)
                        {
                            pageStream.WriteLine("{0} {1} {2} {3} re S",
                                ConvertToString(pp.X + offset),
                                ConvertToString(pp.Y + offset),
                                ConvertToString(pp.Width - offset * 2),
                                ConvertToString(pp.Height - offset * 2));
                        }
                        #endregion
                    }
                    else
                    {
                        #region paint border by line
                        if (needBorderLeft)
                        {
                            if (useAdvBorder)
                            {
                                border = advBorder.LeftSide;
                                if (border.Style != StiPenStyle.Solid)
                                {
                                    pageStream.WriteLine("q");
                                    PushColorToStack();
                                }
                                offset = StoreBorderSideData(border);
                            }
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X - offset), ConvertToString(pp.Y - offset));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X - offset), ConvertToString(pp.Y + pp.Height + offset));
                            if (border.Style == StiPenStyle.Double)
                            {
                                pageStream.WriteLine("{0} {1} m",
                                    ConvertToString(pp.X + offset),
                                    ConvertToString(pp.Y + (needBorderBottom ? offset : -offset)));
                                pageStream.WriteLine("{0} {1} l S",
                                    ConvertToString(pp.X + offset),
                                    ConvertToString(pp.Y + pp.Height + (needBorderTop ? -offset : offset)));
                            }
                            if (useAdvBorder && (border.Style != StiPenStyle.Solid))
                            {
                                pageStream.WriteLine("Q");
                                PopColorFromStack();
                            }
                        }
                        if (needBorderRight)
                        {
                            if (useAdvBorder)
                            {
                                border = advBorder.RightSide;
                                if (border.Style != StiPenStyle.Solid)
                                {
                                    pageStream.WriteLine("q");
                                    PushColorToStack();
                                }
                                offset = StoreBorderSideData(border);
                            }
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X + pp.Width + offset), ConvertToString(pp.Y - offset));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X + pp.Width + offset), ConvertToString(pp.Y + pp.Height + offset));
                            if (border.Style == StiPenStyle.Double)
                            {
                                pageStream.WriteLine("{0} {1} m",
                                    ConvertToString(pp.X + pp.Width - offset),
                                    ConvertToString(pp.Y + (needBorderBottom ? offset : -offset)));
                                pageStream.WriteLine("{0} {1} l S",
                                    ConvertToString(pp.X + pp.Width - offset),
                                    ConvertToString(pp.Y + pp.Height + (needBorderTop ? -offset : offset)));
                            }
                            if (useAdvBorder && (border.Style != StiPenStyle.Solid))
                            {
                                pageStream.WriteLine("Q");
                                PopColorFromStack();
                            }
                        }
                        if (needBorderTop)
                        {
                            if (useAdvBorder)
                            {
                                border = advBorder.TopSide;
                                if (border.Style != StiPenStyle.Solid)
                                {
                                    pageStream.WriteLine("q");
                                    PushColorToStack();
                                }
                                offset = StoreBorderSideData(border);
                            }
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X - offset), ConvertToString(pp.Y + pp.Height + offset));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X + pp.Width + offset), ConvertToString(pp.Y + pp.Height + offset));
                            if (border.Style == StiPenStyle.Double)
                            {
                                pageStream.WriteLine("{0} {1} m",
                                    ConvertToString(pp.X + (needBorderLeft ? offset : -offset)),
                                    ConvertToString(pp.Y + pp.Height - offset));
                                pageStream.WriteLine("{0} {1} l S",
                                    ConvertToString(pp.X + pp.Width + (needBorderRight ? -offset : offset)),
                                    ConvertToString(pp.Y + pp.Height - offset));
                            }
                            if (useAdvBorder && (border.Style != StiPenStyle.Solid))
                            {
                                pageStream.WriteLine("Q");
                                PopColorFromStack();
                            }
                        }
                        if (needBorderBottom)
                        {
                            if (useAdvBorder)
                            {
                                border = advBorder.BottomSide;
                                if (border.Style != StiPenStyle.Solid)
                                {
                                    pageStream.WriteLine("q");
                                    PushColorToStack();
                                }
                                offset = StoreBorderSideData(border);
                            }
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X - offset), ConvertToString(pp.Y - offset));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X + pp.Width + offset), ConvertToString(pp.Y - offset));
                            if (border.Style == StiPenStyle.Double)
                            {
                                pageStream.WriteLine("{0} {1} m",
                                    ConvertToString(pp.X + (needBorderLeft ? offset : -offset)),
                                    ConvertToString(pp.Y + offset));
                                pageStream.WriteLine("{0} {1} l S",
                                    ConvertToString(pp.X + pp.Width + (needBorderRight ? -offset : offset)),
                                    ConvertToString(pp.Y + offset));
                            }
                            if (useAdvBorder && (border.Style != StiPenStyle.Solid))
                            {
                                pageStream.WriteLine("Q");
                                PopColorFromStack();
                            }
                        }
                        #endregion
                    }

                    if (!useAdvBorder && needPush)
                    {
                        pageStream.WriteLine("Q");
                        PopColorFromStack();
                    }
                }
            }
        }

        private double StoreBorderSideData(StiBorderSide border)
        {
            //set border size
            double borderSizeHi = border.Size;
            if (borderSizeHi < 0.5) borderSizeHi = 0.5;
            if (border.Style == StiPenStyle.Double) borderSizeHi = 1;
            double borderSize = borderSizeHi * hiToTwips * 0.9;
            pageStream.WriteLine("{0} w", ConvertToString(borderSize));

            //set border style
            string dash = GetPenStyleDashString(border.Style, borderSize * 0.04);
            if (dash != null)
            {
                pageStream.WriteLine(dash);
            }

            //stroke color
            Color tempColor2 = border.Color;
            SetStrokeColor(tempColor2);

            return (border.Style == StiPenStyle.Double ? borderSize : 0);
        }

        private string GetPenStyleDashString(StiPenStyle style, double step)
        {
            switch (style)
            {
                case StiPenStyle.Dot:
                    return string.Format("[{0} {1}] 0 d", ConvertToString(step), ConvertToString(step * 58));

                case StiPenStyle.Dash:
                    return string.Format("[{0} {1}] 0 d", ConvertToString(step * 49.5), ConvertToString(step * 62));

                case StiPenStyle.DashDot:
                    return string.Format("[{0} {1} {2} {1}] 0 d", ConvertToString(step * 50), ConvertToString(step * 55), ConvertToString(step));

                case StiPenStyle.DashDotDot:
                    return string.Format("[{0} {1} {2} {1} {2} {1}] 0 d", ConvertToString(step * 50), ConvertToString(step * 55), ConvertToString(step));
            }
            return null;
        }
        #endregion

        #region StoreImageData, WriteImageInfo
        private float StoreWatermarkMetafileData(Image image, float imageResolution, StiPage page)
        {
            double pageWidth = report.Unit.ConvertToHInches(page.PageWidth * page.SegmentPerWidth);
            double pageHeight = report.Unit.ConvertToHInches(page.PageHeight * page.SegmentPerHeight);

            int newWidth = (int)Math.Round(pageWidth * imageResolution);
            int newHeight = (int)Math.Round(pageHeight * imageResolution);
            using (Bitmap newBmp = new Bitmap(newWidth, newHeight))
            {
                using (Graphics g = Graphics.FromImage(newBmp))
                {
                    if (useTransparency)
                        g.Clear(Color.FromArgb(1, 255, 255, 255));
                    else
                        g.Clear(Color.White);

                    var destRect = new RectangleF(0, 0, newWidth, newHeight);
                    if (destRect.Width > 0 && destRect.Height > 0)
                        g.DrawImage(image, destRect);
                }
                StoreImageData(newBmp, imageResolution, false, false);
            }
            return imageResolution;
        }

        internal float StoreImageData(Image image, float imageResolution, bool isImageComponent, bool needSmoothing)
        {
            #region Check resolution for Image component
            if (isImageComponent && ((imageResolutionMode == StiImageResolutionMode.NoMoreThan) && (imageResolution > imageResolutionMain)))
            {
                int newWidth = (int)Math.Round(image.Width * imageResolutionMain / imageResolution);
                int newHeight = (int)Math.Round(image.Height * imageResolutionMain / imageResolution);
                Bitmap newBmp = new Bitmap(newWidth, newHeight);
                Graphics g = Graphics.FromImage(newBmp);
                if (!useTransparency) g.Clear(Color.White);
                g.DrawImage(image, new Rectangle(0, 0, newWidth, newHeight), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
                g.Dispose();
                image = newBmp;
                imageResolution = imageResolutionMain;

                imageCache.UsedMemoryCounter += newBmp.Width * newBmp.Height * 4;
            }
            #endregion

            #region Remove transparency for PDF/A mode
            if (isImageComponent && !useTransparency && ((imageResolutionMode == StiImageResolutionMode.Auto) || ((imageResolutionMode == StiImageResolutionMode.NoMoreThan) && (imageResolution <= imageResolutionMain))))
            {
                int newWidth = image.Width;
                int newHeight = image.Height;
                Bitmap newBmp = new Bitmap(newWidth, newHeight);
                Graphics g = Graphics.FromImage(newBmp);
                g.Clear(Color.White);
                g.DrawImage(image, new Rectangle(0, 0, newWidth, newHeight), 0, 0, newWidth, newHeight, GraphicsUnit.Pixel);
                g.Dispose();
                image = newBmp;

                imageCache.UsedMemoryCounter += newBmp.Width * newBmp.Height * 4;
            }
            #endregion

            #region Gray colors or monochrome
            if (imageFormat != StiImageFormat.Color)
            {
                Bitmap newBmp = new Bitmap(image.Width, image.Height);
                Graphics g = Graphics.FromImage(newBmp);

                ColorMatrix matrix = new ColorMatrix(new float[][]{
                                                                        new float[]{0.3f, 0.3f, 0.3f, 0, 0},
                                                                        new float[]{0.55f, 0.55f, 0.55f, 0, 0},
                                                                        new float[]{0.15f, 0.15f, 0.15f, 0, 0},
                                                                        new float[]{0, 0, 0, 1, 0, 0},
                                                                        new float[]{0, 0, 0, 0, 1, 0},
                                                                        new float[]{0, 0, 0, 0, 0, 1}});

                ImageAttributes attributes = new ImageAttributes();
                attributes.SetColorMatrix(matrix);
                g.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes);
                g.Dispose();

                //image.Dispose();
                image = newBmp;

                imageCache.UsedMemoryCounter += newBmp.Width * newBmp.Height * 4;
            }
            if (imageFormat == StiImageFormat.Monochrome)
            {
                image = StiTiffHelper.MakeMonochromeImage(image, monochromeDitheringType, 128);

                imageCache.UsedMemoryCounter += image.Width * image.Height * 4;
            }
            #endregion

            int imageIndex = imageCache.AddImageInt(image);

            StiImageData pd = new StiImageData();
            pd.Width = image.Width;
            pd.Height = image.Height;
            pd.Name = string.Format("Image{0}", imageIndex);
            pd.ImageFormat = imageFormat;
            if ((image.PixelFormat == PixelFormat.Format1bppIndexed) && (imageCompressionMethod == StiPdfImageCompressionMethod.Flate))
            {
                pd.ImageFormat = StiImageFormat.Monochrome;
            }

            imageList.Add(pd);
            imageCacheIndexToList[imageIndex] = pd;

            if (needSmoothing)
            {
                imageInterpolationTable[imageIndex] = true;
            }

            //}

            return imageResolution;
        }

        internal void StoreImageDataForGeom(StiImage image)
        {
            using (var gdiImage = image.TakeGdiImageToDraw(imageResolutionMain))
            {
                if (gdiImage != null)
                {
                    float rsImageResolution = gdiImage.HorizontalResolution / 100;
                    if (imageResolutionMode == StiImageResolutionMode.NoMoreThan)
                    {
                        if (image.Stretch)
                        {
                            rsImageResolution = (float) (gdiImage.Width / report.Unit.ConvertToHInches(image.Width));
                        }
                        else
                        {
                            rsImageResolution = (float) (1 / image.MultipleFactor);
                        }
                    }
                    rsImageResolution = StoreImageData(gdiImage, rsImageResolution, true, image.Smoothing);
                    imageInfoList[imageInfoCounter] = rsImageResolution;
                }
            }
        }

        private void WriteImageInfo(StiPdfData pp, float imageResolution)
        {
            StiImageData pd = (StiImageData)imageList[imagesCurrent];
            imagesCurrent++;

            StiImage view = pp.Component as StiImage;

            if ((imageResolutionMode != StiImageResolutionMode.Exactly) && (view != null) && !view.ImageToDrawIsMetafile() && !view.Margins.IsEmpty)
            {
                double marginsLeft = hiToTwips * view.Margins.Left;
                double marginsRight = hiToTwips * view.Margins.Right;
                double marginsTop = hiToTwips * view.Margins.Top;
                double marginsBottom = hiToTwips * view.Margins.Bottom;

                if (marginsLeft != 0)
                {
                    pp.X += marginsLeft;
                    pp.Width -= marginsLeft;
                }
                if (marginsBottom != 0)
                {
                    pp.Y += marginsBottom;
                    pp.Height -= marginsBottom;
                }
                if (marginsRight != 0) pp.Width -= marginsRight;
                if (marginsTop != 0) pp.Height -= marginsTop;
            }

            //values for fixed resolution
            double cx = pp.X;
            double cy = pp.Y;
            double cw = hiToTwips * (pd.Width - 1) / imageResolution;
            double ch = hiToTwips * (pd.Height - 1) / imageResolution;
            bool needClip = false;

            #region Check for StiImage
            if ((imageResolutionMode != StiImageResolutionMode.Exactly) && (view != null) && view.ExistImageToDraw() && !view.ImageToDrawIsMetafile())
            {
                using (var gdiImage = view.TakeGdiImageToDraw(imageResolutionMain))
                {
                    if (gdiImage != null)
                    {
                        var rect = view.GetPaintRectangle(true, false);
                        rect = view.ConvertImageMargins(rect, false);
                        var destRect = new RectangleF(0, 0, (float) rect.Width, (float) rect.Height);

                        #region !Stretch
                        if (!view.Stretch)
                        {
                            float imageWidth = (float) (gdiImage.Width * view.MultipleFactor);
                            float imageHeight = (float) (gdiImage.Height * view.MultipleFactor);

                            destRect.Width = imageWidth;
                            destRect.Height = imageHeight;

                            #region HorAlignment
                            switch (view.HorAlignment)
                            {
                                case StiHorAlignment.Center:
                                    destRect.X = (float) ((rect.Width - imageWidth) / 2);
                                    break;

                                case StiHorAlignment.Right:
                                    destRect.X = (float) (rect.Width - imageWidth);
                                    break;
                            }
                            #endregion

                            #region VertAlignment
                            switch (view.VertAlignment)
                            {
                                case StiVertAlignment.Center:
                                    destRect.Y = (float) ((rect.Height - imageHeight) / 2);
                                    break;

                                case StiVertAlignment.Top:
                                    destRect.Y = (float) (rect.Height - imageHeight);
                                    break;
                            }
                            #endregion

                            cx += destRect.X * hiToTwips;
                            cy += destRect.Y * hiToTwips;
                            cw = destRect.Width * hiToTwips;
                            ch = destRect.Height * hiToTwips;

                            needClip = true;
                        }
                        #endregion

                        #region Stretch
                        else
                        {
                            float imageWidth = gdiImage.Width;
                            float imageHeight = gdiImage.Height;

                            #region AspectRatio
                            if (view.AspectRatio)
                            {
                                float xRatio = destRect.Width / imageWidth;
                                float yRatio = destRect.Height / imageHeight;

                                if (xRatio > yRatio) destRect.Width = imageWidth * yRatio;
                                else destRect.Height = imageHeight * xRatio;

                                #region VertAlignment
                                switch (view.VertAlignment)
                                {
                                    case StiVertAlignment.Center:
                                        destRect.Y = (float) ((rect.Height - destRect.Height) / 2);
                                        break;

                                    case StiVertAlignment.Top:
                                        destRect.Y = (float) (rect.Height - destRect.Height);
                                        break;
                                }
                                #endregion

                                #region HorAlignment
                                switch (view.HorAlignment)
                                {
                                    case StiHorAlignment.Center:
                                        destRect.X = (float) ((rect.Width - destRect.Width) / 2);
                                        break;

                                    case StiHorAlignment.Right:
                                        destRect.X = (float) (rect.Width - destRect.Width);
                                        break;
                                }
                                #endregion

                                cx += destRect.X * hiToTwips;
                                cy += destRect.Y * hiToTwips;
                                cw = destRect.Width * hiToTwips;
                                ch = destRect.Height * hiToTwips;
                            }
                            #endregion

                            #region !AspectRatio
                            else
                            {
                                cw = pp.Width;
                                ch = pp.Height;
                            }
                            #endregion
                        }
                        #endregion
                    }
                }
            }
            #endregion

            pageStream.WriteLine("q");
            PushColorToStack();
            SetNonStrokeColor(Color.Black);
            if (needClip)
            {
                pageStream.WriteLine("{0} {1} {2} {3} re W n",
                        ConvertToString(pp.X),
                        ConvertToString(pp.Y),
                        ConvertToString(pp.Width),
                        ConvertToString(pp.Height));
            }
            pageStream.WriteLine("{0} 0 0 {1} {2} {3} cm",
                ConvertToString(cw),
                ConvertToString(ch),
                ConvertToString(cx),
                ConvertToString(cy));
            pageStream.WriteLine("/{0} Do", pd.Name);
            pageStream.WriteLine("Q");
            PopColorFromStack();
        }
        internal void WriteImageInfo2(StiPdfData pp, double imageResolutionX, double imageResolutionY)
        {
            StiImageData pd = (StiImageData)imageList[imagesCurrent];
            imagesCurrent++;

            pageStream.WriteLine("q");
            PushColorToStack();
            SetNonStrokeColor(Color.Black);
            pageStream.WriteLine("{0} 0 0 {1} {2} {3} cm",
                ConvertToString(hiToTwips * (pd.Width) / imageResolutionX),
                ConvertToString(hiToTwips * (pd.Height) / imageResolutionY),
                ConvertToString(pp.X),
                ConvertToString(pp.Y));
            pageStream.WriteLine("/{0} Do", pd.Name);
            pageStream.WriteLine("Q");
            PopColorFromStack();
        }
        #endregion

        #region RenderImage
        internal void RenderImage(StiPdfData pp, float imageResolution)
        {
            if (imageInfoList.ContainsKey(imageInfoCounter))
            {
                float rsImageResolution = (float)imageInfoList[imageInfoCounter];
                WriteImageInfo(pp, rsImageResolution);
            }
            else
            {
                IStiExportImageExtended exportImage = pp.Component as IStiExportImageExtended;
                if (exportImage != null && pp.Component.IsExportAsImage(StiExportFormat.Pdf))
                {
                    float rsImageResolution = imageResolution;
                    using (var image = exportImage.GetImage(ref rsImageResolution, StiExportFormat.Pdf))
                    {
                        if (image != null)
                        {
                            WriteImageInfo(pp, rsImageResolution);
                        }
                    }
                }
            }
        }
        #endregion

        #region CheckShape
        private bool CheckShape(StiShape shape)
        {
            if ((shape.ShapeType is StiVerticalLineShapeType) ||
                (shape.ShapeType is StiHorizontalLineShapeType) ||
                (shape.ShapeType is StiTopAndBottomLineShapeType) ||
                (shape.ShapeType is StiLeftAndRightLineShapeType) ||
                (shape.ShapeType is StiRectangleShapeType) ||
                (shape.ShapeType is StiRoundedRectangleShapeType) ||
                (shape.ShapeType is StiDiagonalDownLineShapeType) ||
                (shape.ShapeType is StiDiagonalUpLineShapeType) ||
                (shape.ShapeType is StiTriangleShapeType) ||
                (shape.ShapeType is StiOvalShapeType) ||
                (shape.ShapeType is StiArrowShapeType) ||
                (shape.ShapeType is StiOctagonShapeType) ||
                (shape.ShapeType is StiComplexArrowShapeType) ||
                (shape.ShapeType is StiBentArrowShapeType) ||
                (shape.ShapeType is StiChevronShapeType) ||
                (shape.ShapeType is StiDivisionShapeType) ||
                (shape.ShapeType is StiEqualShapeType) ||
                (shape.ShapeType is StiFlowchartCardShapeType) ||
                (shape.ShapeType is StiFlowchartCollateShapeType) ||
                (shape.ShapeType is StiFlowchartDecisionShapeType) ||
                (shape.ShapeType is StiFlowchartManualInputShapeType) ||
                (shape.ShapeType is StiFlowchartOffPageConnectorShapeType) ||
                (shape.ShapeType is StiFlowchartPreparationShapeType) ||
                (shape.ShapeType is StiFlowchartSortShapeType) ||
                (shape.ShapeType is StiFrameShapeType) ||
                (shape.ShapeType is StiMinusShapeType) ||
                (shape.ShapeType is StiMultiplyShapeType) ||
                (shape.ShapeType is StiParallelogramShapeType) ||
                (shape.ShapeType is StiPlusShapeType) ||
                (shape.ShapeType is StiRegularPentagonShapeType) ||
                (shape.ShapeType is StiTrapezoidShapeType) ||
                (shape.ShapeType is StiSnipSameSideCornerRectangleShapeType) ||
                (shape.ShapeType is StiSnipDiagonalSideCornerRectangleShapeType))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region RenderShape
        private void RenderShape(StiPdfData pp, float imageResolution)
        {
            StiShape shape = pp.Component as StiShape;
            if (shape != null)
            {
                if (CheckShape(shape) == true)
                {
                    #region Render primitive
                    IStiBrush mBrush = pp.Component as IStiBrush;

                    StiPdfGeomWriter geomWriter = new StiPdfGeomWriter(pageStream, this, false);

                    #region Fillcolor
                    Color tempColor = Color.Transparent;
                    if (mBrush != null) tempColor = StiBrush.ToColor(mBrush.Brush);
                    if (tempColor.A != 0)
                    {
                        SetNonStrokeColor(tempColor);
                    }

                    if (mBrush != null)
                    {
                        if (mBrush.Brush is StiGradientBrush || mBrush.Brush is StiGlareBrush)
                        {
                            StoreShadingData2(pp.X, pp.Y, pp.Width, pp.Height, mBrush.Brush);
                            pageStream.WriteLine("/Pattern cs /P{0} scn", 1 + shadingCurrent);
                        }
                        if (mBrush.Brush is StiHatchBrush)
                        {
                            StiHatchBrush hBrush = mBrush.Brush as StiHatchBrush;
                            pageStream.WriteLine("/Cs1 cs /PH{0} scn", GetHatchNumber(hBrush) + 1);
                        }
                    }
                    #endregion

                    //stroke color
                    Color tempColor2 = shape.BorderColor;
                    SetStrokeColor(tempColor2);

                    bool needFill = tempColor.A != 0;
                    bool needStroke = shape.Style != StiPenStyle.None;

                    if (!needFill && !needStroke) return;

                    string st = needFill ? (needStroke ? "B" : "f") : (needStroke ? "S" : "n");

                    pageStream.WriteLine("{0} w", ConvertToString(shape.Size * hiToTwips));

                    pageStream.WriteLine("q");

                    #region set line style
                    double step = shape.Size * hiToTwips * 0.04;
                    switch (shape.Style)
                    {
                        case StiPenStyle.Dot:
                            pageStream.WriteLine("[{0} {1}] 0 d", ConvertToString(step), ConvertToString(step * 55));
                            break;

                        case StiPenStyle.Dash:
                            pageStream.WriteLine("[{0} {1}] 0 d", ConvertToString(step * 50), ConvertToString(step * 55));
                            break;

                        case StiPenStyle.DashDot:
                            pageStream.WriteLine("[{0} {1} {2} {1}] 0 d", ConvertToString(step * 50), ConvertToString(step * 55), ConvertToString(step));
                            break;

                        case StiPenStyle.DashDotDot:
                            pageStream.WriteLine("[{0} {1} {2} {1} {2} {1}] 0 d", ConvertToString(step * 50), ConvertToString(step * 55), ConvertToString(step));
                            break;
                    }
                    #endregion

                    #region VerticalLine
                    if (shape.ShapeType is StiVerticalLineShapeType)
                    {
                        if (needFill)
                        {
                            pageStream.WriteLine("{0} {1} {2} {3} re f",
                                ConvertToString(pp.X),
                                ConvertToString(pp.Y),
                                ConvertToString(pp.Width),
                                ConvertToString(pp.Height));
                        }
                        if (needStroke)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y + pp.Height));
                        }
                    }
                    #endregion

                    #region HorizontalLine
                    if (shape.ShapeType is StiHorizontalLineShapeType)
                    {
                        if (needFill)
                        {
                            pageStream.WriteLine("{0} {1} {2} {3} re f",
                                ConvertToString(pp.X),
                                ConvertToString(pp.Y),
                                ConvertToString(pp.Width),
                                ConvertToString(pp.Height));
                        }
                        if (needStroke)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height / 2));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height / 2));
                        }
                    }
                    #endregion

                    #region TopAndBottomLine
                    if (shape.ShapeType is StiTopAndBottomLineShapeType)
                    {
                        if (needFill)
                        {
                            pageStream.WriteLine("{0} {1} {2} {3} re f",
                                ConvertToString(pp.X),
                                ConvertToString(pp.Y),
                                ConvertToString(pp.Width),
                                ConvertToString(pp.Height));
                        }
                        if (needStroke)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y));
                        }
                    }
                    #endregion

                    #region LeftAndRightLine
                    if (shape.ShapeType is StiLeftAndRightLineShapeType)
                    {
                        if (needFill)
                        {
                            pageStream.WriteLine("{0} {1} {2} {3} re f",
                                ConvertToString(pp.X),
                                ConvertToString(pp.Y),
                                ConvertToString(pp.Width),
                                ConvertToString(pp.Height));
                        }
                        if (needStroke)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height));
                        }
                    }
                    #endregion

                    #region Rectangle
                    if (shape.ShapeType is StiRectangleShapeType)
                    {
                        pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y));
                        pageStream.WriteLine("{0} {1} l " + st, ConvertToString(pp.X), ConvertToString(pp.Y));
                    }
                    #endregion

                    #region DiagonalDownLine
                    if (shape.ShapeType is StiDiagonalDownLineShapeType)
                    {
                        if (needFill)
                        {
                            pageStream.WriteLine("{0} {1} {2} {3} re f",
                                ConvertToString(pp.X),
                                ConvertToString(pp.Y),
                                ConvertToString(pp.Width),
                                ConvertToString(pp.Height));
                        }
                        if (needStroke)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y));
                        }
                    }
                    #endregion

                    #region DiagonalUpLine
                    if (shape.ShapeType is StiDiagonalUpLineShapeType)
                    {
                        if (needFill)
                        {
                            pageStream.WriteLine("{0} {1} {2} {3} re f",
                                ConvertToString(pp.X),
                                ConvertToString(pp.Y),
                                ConvertToString(pp.Width),
                                ConvertToString(pp.Height));
                        }
                        if (needStroke)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height));
                        }
                    }
                    #endregion

                    #region Triangle
                    if (shape.ShapeType is StiTriangleShapeType)
                    {
                        StiShapeDirection ssd = (shape.ShapeType as StiTriangleShapeType).Direction;
                        if (ssd == StiShapeDirection.Up)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l " + st, ConvertToString(pp.X), ConvertToString(pp.Y));
                        }
                        if (ssd == StiShapeDirection.Down)
                        {
                            if (needFill) st = needStroke ? "B*" : "f*"; else st = needStroke ? "S" : "n";
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l " + st, ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height));
                        }
                        if (ssd == StiShapeDirection.Left)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height / 2));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l " + st, ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y));
                        }
                        if (ssd == StiShapeDirection.Right)
                        {
                            if (needFill) st = needStroke ? "B*" : "f*"; else st = needStroke ? "S" : "n";
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height / 2));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l " + st, ConvertToString(pp.X), ConvertToString(pp.Y));
                        }
                    }
                    #endregion

                    #region Oval
                    if (shape.ShapeType is StiOvalShapeType)
                    {
                        pageStream.WriteLine(geomWriter.GetEllipseString(new RectangleF((float)pp.X, (float)pp.Y, (float)pp.Width, (float)pp.Height)) + st);
                    }
                    #endregion

                    #region RoundedRectangle
                    if (shape.ShapeType is StiRoundedRectangleShapeType)
                    {
                        float rnd = (shape.ShapeType as StiRoundedRectangleShapeType).Round;
                        double side = pp.Width;
                        if (side > pp.Height) side = pp.Height;
                        //double offs = Math.Min(side, 100 * shape.Page.Zoom) * rnd;
                        double offs = Math.Min(side, 70) * rnd;
                        double tmp = offs * (1 - pdfCKT);

                        pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y + offs));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height - offs));
                        pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                            ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height - tmp),
                            ConvertToString(pp.X + tmp), ConvertToString(pp.Y + pp.Height),
                            ConvertToString(pp.X + offs), ConvertToString(pp.Y + pp.Height));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width - offs), ConvertToString(pp.Y + pp.Height));
                        pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                            ConvertToString(pp.X + pp.Width - tmp), ConvertToString(pp.Y + pp.Height),
                            ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height - tmp),
                            ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height - offs));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + offs));
                        pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                            ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + tmp),
                            ConvertToString(pp.X + pp.Width - tmp), ConvertToString(pp.Y),
                            ConvertToString(pp.X + pp.Width - offs), ConvertToString(pp.Y));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + offs), ConvertToString(pp.Y));
                        pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c " + st,
                            ConvertToString(pp.X + tmp), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Y + tmp),
                            ConvertToString(pp.X), ConvertToString(pp.Y + offs));
                    }
                    #endregion

                    #region Arrow
                    if (shape.ShapeType is StiArrowShapeType)
                    {
                        StiShapeDirection ssd = (shape.ShapeType as StiArrowShapeType).Direction;
                        float arrowW = (shape.ShapeType as StiArrowShapeType).ArrowWidth;
                        float arrowH = (shape.ShapeType as StiArrowShapeType).ArrowHeight;
                        double arw = pp.Width * arrowW;
                        double arh = pp.Height * arrowH;
                        if ((ssd == StiShapeDirection.Left) || (ssd == StiShapeDirection.Right))
                        {
                            arw = pp.Height * arrowW;
                            arh = pp.Width * arrowH;
                        }
                        if (arrowH == 0) arh = Math.Min(pp.Width / 2, pp.Height / 2);

                        if (ssd == StiShapeDirection.Up)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X + arw), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arw), ConvertToString(pp.Y + pp.Height - arh));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height - arh));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height - arh));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width - arw), ConvertToString(pp.Y + pp.Height - arh));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width - arw), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l " + st, ConvertToString(pp.X + arw), ConvertToString(pp.Y));
                        }
                        if (ssd == StiShapeDirection.Down)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X + pp.Width - arw), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width - arw), ConvertToString(pp.Y + arh));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + arh));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + arh));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arw), ConvertToString(pp.Y + arh));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arw), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l " + st, ConvertToString(pp.X + pp.Width - arw), ConvertToString(pp.Y + pp.Height));
                        }
                        if (ssd == StiShapeDirection.Left)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + arw));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arh), ConvertToString(pp.Y + arw));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arh), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height / 2));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arh), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arh), ConvertToString(pp.Y + pp.Height - arw));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height - arw));
                            pageStream.WriteLine("{0} {1} l " + st, ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + arw));
                        }
                        if (ssd == StiShapeDirection.Right)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height - arw));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width - arh), ConvertToString(pp.Y + pp.Height - arw));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width - arh), ConvertToString(pp.Y + pp.Height));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height / 2));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width - arh), ConvertToString(pp.Y));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width - arh), ConvertToString(pp.Y + arw));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + arw));
                            pageStream.WriteLine("{0} {1} l " + st, ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height - arw));
                        }
                    }
                    #endregion

                    #region Octagon
                    if (shape.ShapeType is StiOctagonShapeType)
                    {
                        var octagonShape = (StiOctagonShapeType)shape.ShapeType;
                        double bevelx = (shape.Report != null ? (float)shape.Report.Unit.ConvertToHInches(octagonShape.Bevel) : octagonShape.Bevel) * hiToTwips;
                        double bevely = bevelx;
                        if (octagonShape.AutoSize)
                        {
                            bevelx = pp.Width / (2.414f * 1.414f);
                            bevely = pp.Height / (2.414f * 1.414f);
                        }
                        if (bevelx > pp.Width / 2) bevelx = pp.Width / 2;
                        if (bevely > pp.Height / 2) bevely = pp.Height / 2;

                        pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X + bevelx), ConvertToString(pp.Y));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width - bevelx), ConvertToString(pp.Y));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + bevely));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width), ConvertToString(pp.Y + pp.Height - bevely));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width - bevelx), ConvertToString(pp.Y + pp.Height));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + bevelx), ConvertToString(pp.Y + pp.Height));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height - bevely));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + bevely));
                        pageStream.WriteLine("{0} {1} l " + st, ConvertToString(pp.X + bevelx), ConvertToString(pp.Y));
                    }
                    #endregion

                    #region ComplexArrow
                    if (shape.ShapeType is StiComplexArrowShapeType)
                    {
                        double restHeight = (pp.Width < pp.Height) ? pp.Width / 2 : pp.Height / 2;
                        double topBottomSpace = (pp.Height / 3.8f);
                        double leftRightSpace = (pp.Width / 3.8f);
                        double restWidth = (pp.Height < pp.Width) ? pp.Height / 2 : pp.Width / 2;

                        switch ((shape.ShapeType as StiComplexArrowShapeType).Direction)
                        {
                            case StiShapeDirection.Left:
                            case StiShapeDirection.Right:
                                pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height / 2));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + restHeight), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + restHeight), ConvertToString(pp.Y + topBottomSpace));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - restHeight), ConvertToString(pp.Y + topBottomSpace));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - restHeight), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Y + pp.Height / 2));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - restHeight), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - restHeight), ConvertToString(pp.Top - topBottomSpace));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + restHeight), ConvertToString(pp.Top - topBottomSpace));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + restHeight), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l h " + st, ConvertToString(pp.X), ConvertToString(pp.Y + pp.Height / 2));
                                break;

                            case StiShapeDirection.Down:
                            case StiShapeDirection.Up:
                                pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y + restWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Y + restWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - leftRightSpace), ConvertToString(pp.Y + restWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - leftRightSpace), ConvertToString(pp.Top - restWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Top - restWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Top - restWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + leftRightSpace), ConvertToString(pp.Top - restWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + leftRightSpace), ConvertToString(pp.Y + restWidth));
                                pageStream.WriteLine("{0} {1} l h " + st, ConvertToString(pp.X), ConvertToString(pp.Y + restWidth));
                                break;
                        }
                    }
                    #endregion

                    #region BentArrow
                    if (shape.ShapeType is StiBentArrowShapeType)
                    {
                        double lineHeight = 0;
                        double arrowWidth = 0;
                        double space = 0;
                        if (pp.Height > pp.Width)
                        {
                            arrowWidth = pp.Width / 4;
                            lineHeight = arrowWidth;
                            space = arrowWidth / 2;
                        }
                        else
                        {
                            lineHeight = (int)(pp.Height / 4);
                            arrowWidth = lineHeight;
                            space = arrowWidth / 2;
                        }

                        switch ((shape.ShapeType as StiBentArrowShapeType).Direction)
                        {
                            #region Up
                            case StiShapeDirection.Up:
                                pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + lineHeight));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - (space + lineHeight)), ConvertToString(pp.Y + lineHeight));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - (space + lineHeight)), ConvertToString(pp.Top - arrowWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - arrowWidth * 2), ConvertToString(pp.Top - arrowWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - arrowWidth), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Top - arrowWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - space), ConvertToString(pp.Top - arrowWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - space), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l h " + st, ConvertToString(pp.X), ConvertToString(pp.Y));
                                break;
                            #endregion

                            #region Left
                            case StiShapeDirection.Left:
                                pageStream.WriteLine("{0} {1} m", ConvertToString(pp.Right), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Top - space));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arrowWidth), ConvertToString(pp.Top - space));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arrowWidth), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Top - arrowWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arrowWidth), ConvertToString(pp.Top - arrowWidth * 2));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arrowWidth), ConvertToString(pp.Top - arrowWidth - space));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - lineHeight), ConvertToString(pp.Top - arrowWidth - space));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - lineHeight), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l h " + st, ConvertToString(pp.Right), ConvertToString(pp.Y));
                                break;
                            #endregion

                            #region Down
                            case StiShapeDirection.Down:
                                pageStream.WriteLine("{0} {1} m", ConvertToString(pp.Right), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + space), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + space), ConvertToString(pp.Y + arrowWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + arrowWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arrowWidth), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arrowWidth * 2), ConvertToString(pp.Y + arrowWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arrowWidth + space), ConvertToString(pp.Y + arrowWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + arrowWidth + space), ConvertToString(pp.Top - lineHeight));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Top - lineHeight));
                                pageStream.WriteLine("{0} {1} l h " + st, ConvertToString(pp.Right), ConvertToString(pp.Top));
                                break;
                            #endregion

                            #region Right
                            case StiShapeDirection.Right:
                                pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + space));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - arrowWidth), ConvertToString(pp.Y + space));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - arrowWidth), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Y + arrowWidth));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - arrowWidth), ConvertToString(pp.Y + arrowWidth * 2));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - arrowWidth), ConvertToString(pp.Y + arrowWidth + space));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + lineHeight), ConvertToString(pp.Y + arrowWidth + space));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + lineHeight), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l h " + st, ConvertToString(pp.X), ConvertToString(pp.Top));
                                break;
                                #endregion
                        }
                    }
                    #endregion

                    #region Chevron
                    if (shape.ShapeType is StiChevronShapeType)
                    {
                        double rest = (pp.Width > pp.Height) ? (pp.Height / 2) : (pp.Width / 2);

                        switch ((shape.ShapeType as StiChevronShapeType).Direction)
                        {
                            #region Right
                            case StiShapeDirection.Right:
                                pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + rest), ConvertToString(pp.Top - pp.Height / 2));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - rest), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Top - pp.Height / 2));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - rest), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l h " + st, ConvertToString(pp.X), ConvertToString(pp.Top));
                                break;
                            #endregion

                            #region Left
                            case StiShapeDirection.Left:
                                pageStream.WriteLine("{0} {1} m", ConvertToString(pp.Right), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + rest), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Top - pp.Height / 2));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + rest), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right - rest), ConvertToString(pp.Top - pp.Height / 2));
                                pageStream.WriteLine("{0} {1} l h " + st, ConvertToString(pp.Right), ConvertToString(pp.Top));
                                break;
                            #endregion

                            #region Up
                            case StiShapeDirection.Up:
                                pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Top - rest));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Top - rest));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y + rest));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l h " + st, ConvertToString(pp.X), ConvertToString(pp.Top - rest));
                                break;
                            #endregion

                            #region Down
                            case StiShapeDirection.Down:
                                pageStream.WriteLine("{0} {1} m", ConvertToString(pp.X), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Top - rest));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.Right), ConvertToString(pp.Y + rest));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y));
                                pageStream.WriteLine("{0} {1} l", ConvertToString(pp.X), ConvertToString(pp.Y + rest));
                                pageStream.WriteLine("{0} {1} l h " + st, ConvertToString(pp.X), ConvertToString(pp.Top));
                                break;
                                #endregion
                        }
                    }
                    #endregion

                    #region Division
                    if (shape.ShapeType is StiDivisionShapeType)
                    {
                        double restHeight = pp.Height / 3;

                        float offset = (float)(4 * hiToTwips);
                        pageStream.WriteLine(geomWriter.GetRectString(pp.X, pp.Y + restHeight + offset, pp.Width, restHeight - offset * 2) + st);
                        pageStream.WriteLine(geomWriter.GetEllipseString(pp.X + pp.Width / 2 - restHeight / 2, pp.Top - hiToTwips - restHeight, restHeight, restHeight) + st);
                        pageStream.WriteLine(geomWriter.GetEllipseString(pp.X + pp.Width / 2 - restHeight / 2, pp.Y + 2 * hiToTwips, restHeight, restHeight) + st);
                    }
                    #endregion

                    #region Equal
                    if (shape.ShapeType is StiEqualShapeType)
                    {
                        double height = (pp.Height - (pp.Height / 6)) / 2;

                        pageStream.WriteLine(geomWriter.GetRectString(pp.X, pp.Top - height, pp.Width, height) + st);
                        pageStream.WriteLine(geomWriter.GetRectString(pp.X, pp.Y, pp.Width, height) + st);
                    }
                    #endregion

                    #region Flowchart: Card
                    if (shape.ShapeType is StiFlowchartCardShapeType)
                    {
                        pageStream.WriteLine("{0} {1} m {2} {3} l",
                            ConvertToString(pp.Right), ConvertToString(pp.Top),
                            ConvertToString(pp.Right), ConvertToString(pp.Y));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                            ConvertToString(pp.X), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Top - pp.Height / 5),
                            ConvertToString(pp.X + pp.Width / 5), ConvertToString(pp.Top),
                            ConvertToString(pp.Right), ConvertToString(pp.Top));
                    }
                    #endregion

                    #region Flowchart: Collate
                    if (shape.ShapeType is StiFlowchartCollateShapeType)
                    {
                        switch ((shape.ShapeType as StiFlowchartCollateShapeType).Direction)
                        {
                            case StiShapeDirection.Down:
                            case StiShapeDirection.Up:
                                pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l h " + st,
                                    ConvertToString(pp.X), ConvertToString(pp.Top),
                                    ConvertToString(pp.Right), ConvertToString(pp.Top),
                                    ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y + pp.Height / 2));
                                pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l h " + st,
                                    ConvertToString(pp.X), ConvertToString(pp.Y),
                                    ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y + pp.Height / 2),
                                    ConvertToString(pp.Right), ConvertToString(pp.Y));
                                break;

                            case StiShapeDirection.Left:
                            case StiShapeDirection.Right:
                                pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l h " + st,
                                    ConvertToString(pp.X), ConvertToString(pp.Y),
                                    ConvertToString(pp.X), ConvertToString(pp.Top),
                                    ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y + pp.Height / 2));
                                pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l h " + st,
                                    ConvertToString(pp.Right), ConvertToString(pp.Y),
                                    ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y + pp.Height / 2),
                                    ConvertToString(pp.Right), ConvertToString(pp.Top));
                                break;
                        }
                    }
                    #endregion

                    #region Flowchart: Decision
                    if (shape.ShapeType is StiFlowchartDecisionShapeType)
                    {
                        pageStream.WriteLine("{0} {1} m",
                            ConvertToString(pp.X), ConvertToString(pp.Top - pp.Height / 2));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                            ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Top),
                            ConvertToString(pp.Right), ConvertToString(pp.Top - pp.Height / 2),
                            ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Top - pp.Height / 2));
                    }
                    #endregion

                    #region Flowchart: Manual Input
                    if (shape.ShapeType is StiFlowchartManualInputShapeType)
                    {
                        pageStream.WriteLine("{0} {1} m",
                            ConvertToString(pp.X), ConvertToString(pp.Top - pp.Height / 5));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                            ConvertToString(pp.Right), ConvertToString(pp.Top),
                            ConvertToString(pp.Right), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Top - pp.Height / 5));
                    }
                    #endregion

                    #region Flowchart: Off Page Connector
                    if (shape.ShapeType is StiFlowchartOffPageConnectorShapeType)
                    {
                        double restHeight = pp.Height / 5;
                        double restWidth = pp.Width / 5;
                        switch ((shape.ShapeType as StiFlowchartOffPageConnectorShapeType).Direction)
                        {
                            case StiShapeDirection.Down:
                                pageStream.WriteLine("{0} {1} m {2} {3} l",
                                    ConvertToString(pp.X), ConvertToString(pp.Top),
                                    ConvertToString(pp.Right), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                                    ConvertToString(pp.Right), ConvertToString(pp.Y + restHeight),
                                    ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y),
                                    ConvertToString(pp.X), ConvertToString(pp.Y + restHeight),
                                    ConvertToString(pp.X), ConvertToString(pp.Top));
                                break;

                            case StiShapeDirection.Up:
                                pageStream.WriteLine("{0} {1} m {2} {3} l",
                                    ConvertToString(pp.X), ConvertToString(pp.Y),
                                    ConvertToString(pp.X), ConvertToString(pp.Top - restHeight));
                                pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                                    ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Top),
                                    ConvertToString(pp.Right), ConvertToString(pp.Top - restHeight),
                                    ConvertToString(pp.Right), ConvertToString(pp.Y),
                                    ConvertToString(pp.X), ConvertToString(pp.Y));
                                break;

                            case StiShapeDirection.Left:
                                pageStream.WriteLine("{0} {1} m {2} {3} l",
                                    ConvertToString(pp.X + restWidth), ConvertToString(pp.Top),
                                    ConvertToString(pp.Right), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                                    ConvertToString(pp.Right), ConvertToString(pp.Y),
                                    ConvertToString(pp.X + restWidth), ConvertToString(pp.Y),
                                    ConvertToString(pp.X), ConvertToString(pp.Top - pp.Height / 2),
                                    ConvertToString(pp.X + restWidth), ConvertToString(pp.Top));
                                break;

                            case StiShapeDirection.Right:
                                pageStream.WriteLine("{0} {1} m {2} {3} l",
                                    ConvertToString(pp.X), ConvertToString(pp.Top),
                                    ConvertToString(pp.Right - restWidth), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                                    ConvertToString(pp.Right), ConvertToString(pp.Top - pp.Height / 2),
                                    ConvertToString(pp.Right - restWidth), ConvertToString(pp.Y),
                                    ConvertToString(pp.X), ConvertToString(pp.Y),
                                    ConvertToString(pp.X), ConvertToString(pp.Top));
                                break;
                        }
                    }
                    #endregion

                    #region Flowchart: Preparation
                    if (shape.ShapeType is StiFlowchartPreparationShapeType)
                    {
                        double restWidth = pp.Width / 5;
                        double restHeight = pp.Height / 5;
                        double xCenter = pp.Width / 2;
                        double yCenter = pp.Height / 2;

                        switch ((shape.ShapeType as StiFlowchartPreparationShapeType).Direction)
                        {
                            case StiShapeDirection.Left:
                            case StiShapeDirection.Right:
                                pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l",
                                    ConvertToString(pp.X), ConvertToString(pp.Top - yCenter),
                                    ConvertToString(pp.X + restWidth), ConvertToString(pp.Top),
                                    ConvertToString(pp.Right - restWidth), ConvertToString(pp.Top));
                                pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                                    ConvertToString(pp.Right), ConvertToString(pp.Top - yCenter),
                                    ConvertToString(pp.Right - restWidth), ConvertToString(pp.Y),
                                    ConvertToString(pp.X + restWidth), ConvertToString(pp.Y),
                                    ConvertToString(pp.X), ConvertToString(pp.Top - yCenter));
                                break;

                            case StiShapeDirection.Down:
                            case StiShapeDirection.Up:
                                pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l",
                                    ConvertToString(pp.X + xCenter), ConvertToString(pp.Top),
                                    ConvertToString(pp.Right), ConvertToString(pp.Top - restHeight),
                                    ConvertToString(pp.Right), ConvertToString(pp.Y + restHeight));
                                pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                                    ConvertToString(pp.X + xCenter), ConvertToString(pp.Y),
                                    ConvertToString(pp.X), ConvertToString(pp.Y + restHeight),
                                    ConvertToString(pp.X), ConvertToString(pp.Top - restHeight),
                                    ConvertToString(pp.X + xCenter), ConvertToString(pp.Top));
                                break;
                        }
                    }
                    #endregion

                    #region Flowchart: Sort
                    if (shape.ShapeType is StiFlowchartSortShapeType)
                    {
                        pageStream.WriteLine("{0} {1} m {2} {3} l",
                            ConvertToString(pp.X), ConvertToString(pp.Top - pp.Height / 2),
                            ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Top));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                            ConvertToString(pp.Right), ConvertToString(pp.Top - pp.Height / 2),
                            ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Top - pp.Height / 2),
                            ConvertToString(pp.Right), ConvertToString(pp.Top - pp.Height / 2));
                    }
                    #endregion

                    #region Frame
                    if (shape.ShapeType is StiFrameShapeType)
                    {
                        double restWidth = pp.Width / 7;
                        double restHeight = pp.Height / 7;

                        pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l {6} {7} l h",
                            ConvertToString(pp.X), ConvertToString(pp.Top),
                            ConvertToString(pp.Right), ConvertToString(pp.Top),
                            ConvertToString(pp.Right), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Y));
                        pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l {6} {7} l h " + st,
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Top - restHeight),
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Y + restHeight),
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Y + restHeight),
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Top - restHeight));
                    }
                    #endregion

                    #region Minus
                    if (shape.ShapeType is StiMinusShapeType)
                    {
                        double restHeight = pp.Height / 3;

                        pageStream.WriteLine(geomWriter.GetRectString(pp.X, pp.Y + restHeight, pp.Width, restHeight) + st);
                    }
                    #endregion

                    #region Multiply
                    if (shape.ShapeType is StiMultiplyShapeType)
                    {
                        double restWidth = pp.Width / 4;
                        double restHeight = pp.Height / 4;

                        pageStream.WriteLine("{0} {1} m",
                            ConvertToString(pp.X), ConvertToString(pp.Top - restHeight));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l",
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Top),
                            ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Top - restHeight),
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Top),
                            ConvertToString(pp.Right), ConvertToString(pp.Top - restHeight));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l",
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Top - pp.Height / 2),
                            ConvertToString(pp.Right), ConvertToString(pp.Y + restHeight),
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Y),
                            ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y + restHeight));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l h " + st,
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Y + restHeight),
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Top - pp.Height / 2));
                    }
                    #endregion

                    #region Parallelogram
                    if (shape.ShapeType is StiParallelogramShapeType)
                    {
                        double restWidth = pp.Width / 7;
                        double restHeight = pp.Height / 7;

                        pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l {6} {7} l h " + st,
                            ConvertToString(pp.X), ConvertToString(pp.Y),
                            ConvertToString(pp.X + pp.Width / 5), ConvertToString(pp.Top),
                            ConvertToString(pp.Right), ConvertToString(pp.Top),
                            ConvertToString(pp.Right - pp.Width / 5), ConvertToString(pp.Y));
                    }
                    #endregion

                    #region Plus
                    if (shape.ShapeType is StiPlusShapeType)
                    {
                        double restWidth = pp.Width / 3;
                        double restHeight = pp.Height / 3;

                        pageStream.WriteLine("{0} {1} m",
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Top));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l",
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Top),
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Top - restHeight),
                            ConvertToString(pp.Right), ConvertToString(pp.Top - restHeight),
                            ConvertToString(pp.Right), ConvertToString(pp.Y + restHeight));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l",
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Y + restHeight),
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Y),
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Y),
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Y + restHeight));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                            ConvertToString(pp.X), ConvertToString(pp.Y + restHeight),
                            ConvertToString(pp.X), ConvertToString(pp.Top - restHeight),
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Top - restHeight),
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Top));
                    }
                    #endregion

                    #region Regular: Pentagon
                    if (shape.ShapeType is StiRegularPentagonShapeType)
                    {
                        double restTop = pp.Height / 2.6f;
                        double restLeft = pp.Width / 5.5f;

                        pageStream.WriteLine("{0} {1} m {2} {3} l",
                            ConvertToString(pp.X), ConvertToString(pp.Top - restTop),
                            ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Top));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l {6} {7} l h " + st,
                            ConvertToString(pp.Right), ConvertToString(pp.Top - restTop),
                            ConvertToString(pp.Right - restLeft), ConvertToString(pp.Y),
                            ConvertToString(pp.X + restLeft), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Top - restTop));
                    }
                    #endregion

                    #region Trapezoid
                    if (shape.ShapeType is StiTrapezoidShapeType)
                    {
                        double rest = pp.Width / 4.75f;
                        pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l {6} {7} l h " + st,
                            ConvertToString(pp.X), ConvertToString(pp.Y),
                            ConvertToString(pp.X + rest), ConvertToString(pp.Top),
                            ConvertToString(pp.Right - rest), ConvertToString(pp.Top),
                            ConvertToString(pp.Right), ConvertToString(pp.Y));
                    }
                    #endregion

                    #region Snip Same Side Corner Rectangle
                    if (shape.ShapeType is StiSnipSameSideCornerRectangleShapeType)
                    {
                        double restWidth = pp.Width / 7.2f;
                        double restHeight = pp.Height / 4.6f;

                        pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l",
                            ConvertToString(pp.X), ConvertToString(pp.Top - restHeight),
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Top),
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Top));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l h " + st,
                            ConvertToString(pp.Right), ConvertToString(pp.Top - restHeight),
                            ConvertToString(pp.Right), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Y));
                    }
                    #endregion

                    #region Snip Diagonal Side Corner Rectangle
                    if (shape.ShapeType is StiSnipDiagonalSideCornerRectangleShapeType)
                    {
                        double restWidth = pp.Width / 7.2f;
                        double restHeight = pp.Height / 4.6f;

                        pageStream.WriteLine("{0} {1} m {2} {3} l {4} {5} l",
                            ConvertToString(pp.X), ConvertToString(pp.Top),
                            ConvertToString(pp.Right - restWidth), ConvertToString(pp.Top),
                            ConvertToString(pp.Right), ConvertToString(pp.Top - restHeight));
                        pageStream.WriteLine("{0} {1} l {2} {3} l {4} {5} l h " + st,
                            ConvertToString(pp.Right), ConvertToString(pp.Y),
                            ConvertToString(pp.X + restWidth), ConvertToString(pp.Y),
                            ConvertToString(pp.X), ConvertToString(pp.Y + restHeight));
                    }
                    #endregion

                    pageStream.WriteLine("Q");

                    #endregion
                }
                else
                {
                    float ir = imageResolution;
                    using (Image image = shape.GetImage(ref imageResolution))
                    {
                        WriteImageInfo(pp, ir);
                    }
                }
            }
        }
        #endregion

        #region RenderCheckbox
        private void RenderCheckbox(StiPdfData pp, bool checkBoxValue, bool storeShading = true)
        {
            StiCheckBox checkbox = pp.Component as StiCheckBox;
            if (checkbox != null)
            {
                #region Get shape path
                string shape = string.Empty;
                switch (checkBoxValue ? checkbox.CheckStyleForTrue : checkbox.CheckStyleForFalse)
                {
                    case StiCheckStyle.Cross:
                        shape = "62.568 52.024 m 62.018 52.166 60.405 52.537 58.984 52.848 c 55.336 53.645 49.313 58.685 44.741 64.767 c 40.839 69.958 l 45.919 71.092 l \r\n" +
                                "53.272 72.735 59.559 76.81 67.746 85.239 c 74.954 92.661 l 68.543 100.174 l 56.77 113.972 54.196 123.193 59.915 131.088 c 62.809 135.083 71.734 143.458 73.097 143.458 c \r\n" +
                                "73.509 143.458 74.16 141.77 74.546 139.708 c 75.526 134.457 81.002 122.942 85.482 116.708 c 87.557 113.82 89.473 111.458 89.74 111.458 c \r\n" +
                                "90.006 111.458 93.515 114.945 97.537 119.208 c 113.934 136.584 127.211 138.972 135.818 126.095 c 139.973 119.877 140.004 118.024 135.958 117.739 c \r\n" +
                                "130.11 117.329 118.795 109.205 110.443 99.42 c 105.812 93.994 l 110.69 89.679 l 117.241 83.884 129.589 77.786 136.531 76.919 c \r\n" +
                                "139.576 76.539 142.068 75.813 142.068 75.307 c 142.068 72.526 132.802 60.889 129.038 58.942 c 121.077 54.825 112.668 58.23 96.273 72.209 c \r\n" +
                                "91.287 76.46 l 84.2 67.488 l 80.303 62.554 75.379 57.368 73.259 55.965 c 69.353 53.38 64.393 51.552 62.568 52.024 c h";
                        break;

                    case StiCheckStyle.Check:
                        shape = "60.972 37.503 m 51.173 63.277 43.562 76.623 35.37 82.397 c 30.912 85.54 l 33.664 88.435 l 37.539 92.513 43.698 95.935 48.566 96.713 c \r\n" +
                                "52.426 97.33 53.024 97.093 57.102 93.334 c 59.763 90.882 63.368 85.726 66.269 80.223 c 68.899 75.234 71.18 71.153 71.337 71.153 c \r\n" +
                                "71.493 71.153 73.65 74.19 76.13 77.903 c 96.259 108.044 129.683 141.214 157.565 158.718 c 166.414 164.274 l 168.677 161.643 l \r\n" +
                                "170.941 159.012 l 163.178 152.717 l 139.859 133.81 108.017 94.486 89.043 61.164 c 82.362 49.432 81.87 48.851 73.952 43.345 c \r\n" +
                                "69.45 40.214 64.908 37.04 63.858 36.292 c 62.149 35.074 61.848 35.2 60.972 37.503 c h";
                        break;

                    case StiCheckStyle.CrossRectangle:
                        shape = "24.153 97.958 m 24.153 170.458 l 98.653 170.458 l 173.153 170.458 l 173.153 97.958 l 173.153 25.458 l 98.653 25.458 l 24.153 25.458 l 24.153 97.958 l h \r\n" +
                                "157.911 97.708 m 157.653 154.958 l 98.653 154.958 l 39.653 154.958 l 39.393 98.958 l 39.25 68.158 39.348 42.395 39.611 41.708 c \r\n" +
                                "39.987 40.727 52.819 40.458 99.129 40.458 c 158.169 40.458 l 157.911 97.708 l h \r\n" +
                                "67.337 54.521 m 65.513 54.912 62.41 56.378 60.442 57.778 c 57.123 60.14 48.153 70.186 48.153 71.541 c 48.153 71.87 50.57 72.68 53.525 73.342 c \r\n" +
                                "60.71 74.95 67.272 79.277 75.328 87.718 c 82.003 94.713 l 75.624 102.027 l 64.931 114.288 61.644 123.705 65.472 131.108 c \r\n" +
                                "67.054 134.168 78.562 145.458 80.098 145.458 c 80.556 145.458 81.245 143.77 81.63 141.708 c 82.611 136.457 88.086 124.942 92.567 118.708 c \r\n" +
                                "94.642 115.82 96.558 113.458 96.824 113.458 c 97.091 113.458 100.6 116.945 104.622 121.208 c 121.019 138.584 134.296 140.972 142.903 128.095 c \r\n" +
                                "147.058 121.877 147.089 120.024 143.043 119.739 c 137.213 119.33 124.806 110.39 117.127 101.066 c 113.226 96.33 113.155 96.112 114.876 94.198 c \r\n" +
                                "118.066 90.648 128.579 83.654 133.847 81.578 c 136.682 80.461 141.285 79.244 144.077 78.873 c 146.868 78.503 149.153 77.878 149.153 77.484 c \r\n" +
                                "149.153 75.37 140.777 64.275 137.501 62.048 c 129.107 56.344 120.869 59.278 103.358 74.209 c 98.372 78.46 l 91.285 69.488 l \r\n" +
                                "81.563 57.18 74.76 52.928 67.337 54.521 c h";
                        break;

                    case StiCheckStyle.CheckRectangle:
                        shape = "19.915 96.5 m 19.915 169 l 91.857 169 l 163.8 169 l 170.357 173.111 l 176.914 177.223 l 178.882 174.861 l 179.963 173.563 180.864 172.217 180.882 171.872 c \r\n" +
                                "180.9 171.526 178.44 169.334 175.415 167 c 169.915 162.757 l 169.915 93.378 l 169.915 24 l 94.915 24 l 19.915 24 l 19.915 96.5 l h \r\n" +
                                "153.915 92.622 m 153.915 141.962 153.786 146.137 152.294 144.899 c 149.513 142.592 136.609 126.998 127.965 115.5 c 117.473 101.544 104.486 81.963 98.451 71 c \r\n" +
                                "93.993 62.903 93.316 62.192 84.16 56 c 78.873 52.425 74.256 49.375 73.9 49.223 c 73.544 49.07 71.988 52.22 70.441 56.223 c \r\n" +
                                "68.895 60.225 65.183 68.635 62.192 74.911 c 57.906 83.903 55.515 87.56 50.914 92.161 c 47.703 95.372 44.364 98 43.495 98 c \r\n" +
                                "40.697 98 41.79 99.66 47.479 104.049 c 53.073 108.365 60.662 111.14 64.28 110.194 c 67.84 109.263 73.689 102.039 78.2 93.002 c \r\n" +
                                "82.663 84.062 l 87.207 90.895 l 95.518 103.394 108.214 118.311 125.807 136.25 c 143.215 154 l 89.565 154 l 35.915 154 l 35.915 96.5 l \r\n" +
                                "35.915 39 l 94.915 39 l 153.915 39 l 153.915 92.622 l h";
                        break;

                    case StiCheckStyle.CrossCircle:
                        shape = "83.347 26.864 m 61.07 31.95 42.193 47.128 32.202 67.986 c 23.401 86.36 23.68 110.034 32.919 128.958 c 41.882 147.315 60.868 162.86 80.847 168.201 c \r\n" +
                                "91.083 170.936 112.112 170.628 121.812 167.6 c 147.999 159.425 167.881 138.673 173.432 113.721 c 175.869 102.768 175 85.662 171.452 74.743 c \r\n" +
                                "164.795 54.256 145.804 35.792 124.126 28.729 c 117.735 26.647 113.94 26.133 102.847 25.845 c 93.814 25.61 87.363 25.947 83.347 26.864 c h \r\n" +
                                "112.414 41.542 m 129.545 44.672 146.131 57.503 153.827 73.579 c 168.725 104.698 152.719 141.239 119.425 152.119 c 112.712 154.313 109.49 154.763 100.347 154.781 c \r\n" +
                                "90.993 154.8 88.185 154.404 81.579 152.131 c 64.423 146.231 51.91 134.6 45.14 118.265 c 42.988 113.072 42.446 109.911 42.069 100.368 c \r\n" +
                                "41.551 87.229 42.811 81.166 48.181 70.958 c 52.288 63.15 63.613 51.864 71.549 47.67 c 83.611 41.295 98.688 39.034 112.414 41.542 c h \r\n" +
                                "69.097 66.583 m 66.21 69.342 63.847 71.942 63.847 72.361 c 63.847 72.78 69.506 78.671 76.422 85.451 c 88.996 97.78 l 76.198 110.607 l 63.4 123.434 l \r\n" +
                                "68.336 128.446 l 71.051 131.202 73.641 133.458 74.091 133.458 c 74.542 133.458 80.666 127.846 87.7 120.988 c 100.49 108.517 l 104.919 113.071 l \r\n" +
                                "107.354 115.575 113.31 121.259 118.154 125.701 c 126.961 133.777 l 132.308 128.496 l 137.656 123.215 l 124.694 110.658 l 111.733 98.1 l 124.866 84.939 l \r\n" +
                                "137.999 71.779 l 132.815 67.118 l 129.964 64.555 127.11 62.458 126.474 62.458 c 125.837 62.458 119.93 67.858 113.347 74.458 c \r\n" +
                                "106.765 81.058 100.96 86.458 100.449 86.458 c 99.938 86.458 93.856 80.857 86.933 74.013 c 74.347 61.567 l 69.097 66.583 l h";
                        break;

                    case StiCheckStyle.DotCircle:
                        shape = "81.653 29.406 m 59.375 34.493 40.499 49.67 30.507 70.529 c 21.706 88.903 21.985 112.576 31.224 131.5 c 40.187 149.857 59.173 165.402 79.153 170.743 c \r\n" +
                                "89.388 173.479 110.417 173.17 120.117 170.142 c 146.304 161.968 166.186 141.215 171.737 116.263 c 174.174 105.311 173.305 88.205 169.757 77.285 c \r\n" +
                                "163.1 56.798 144.109 38.334 122.431 31.271 c 116.04 29.189 112.245 28.675 101.153 28.387 c 92.119 28.152 85.668 28.49 81.653 29.406 c h \r\n" +
                                "111.653 44.504 m 132.341 48.848 149.671 64.959 155.751 85.5 c 158.113 93.481 158.113 107.519 155.751 115.5 c 150.089 134.629 134.635 149.703 114.653 155.588 c \r\n" +
                                "106.553 157.973 90.741 157.974 82.695 155.589 c 62.46 149.592 46.687 133.961 41.605 114.869 c 39.656 107.547 39.74 91.753 41.764 84.932 c \r\n" +
                                "50.494 55.507 80.736 38.013 111.653 44.504 c h \r\n" +
                                "90.005 77.33 m 76.55 82.362 69.825 98.176 75.898 110.5 c 78.035 114.836 83.045 119.856 87.653 122.277 c 93.231 125.208 104.066 125.204 109.705 122.27 c \r\n" +
                                "127.735 112.887 128.781 89.485 111.62 79.428 c 106.047 76.162 95.789 75.166 90.005 77.33 c h";
                        break;

                    case StiCheckStyle.DotRectangle:
                        shape = "23.847 98.805 m 23.847 171.305 l 98.347 171.305 l 172.847 171.305 l 172.847 98.805 l 172.847 26.305 l 98.347 26.305 l 23.847 26.305 l 23.847 98.805 l h \r\n" +
                                "157.847 98.813 m 157.847 156.321 l 98.597 156.063 l 39.347 155.805 l 39.089 98.555 l 38.831 41.305 l 98.339 41.305 l 157.847 41.305 l 157.847 98.813 l h \r\n" +
                                "63.527 64.959 m 63.153 65.333 62.847 80.638 62.847 98.972 c 62.847 132.305 l 98.361 132.305 l 133.874 132.305 l 133.611 98.555 l 133.347 64.805 l \r\n" +
                                "98.777 64.542 l 79.763 64.398 63.901 64.585 63.527 64.959 c h";
                        break;

                    case StiCheckStyle.NoneCircle:
                        shape = "83.5 29.406 m 61.222 34.493 42.346 49.67 32.355 70.529 c 23.554 88.903 23.832 112.576 33.071 131.5 c 42.034 149.857 61.02 165.402 81 170.743 c \r\n" +
                                "91.235 173.479 112.265 173.17 121.965 170.142 c 148.151 161.968 168.034 141.215 173.585 116.263 c 176.022 105.311 175.152 88.205 171.605 77.285 c \r\n" +
                                "164.948 56.798 145.957 38.334 124.278 31.271 c 117.887 29.189 114.092 28.675 103 28.387 c 93.966 28.152 87.515 28.49 83.5 29.406 c h \r\n" +
                                "113.5 44.504 m 134.189 48.848 151.519 64.959 157.598 85.5 c 159.961 93.481 159.961 107.519 157.598 115.5 c 151.937 134.629 136.483 149.703 116.5 155.588 c \r\n" +
                                "108.401 157.973 92.589 157.974 84.543 155.589 c 64.308 149.592 48.534 133.961 43.453 114.869 c 41.504 107.547 41.588 91.753 43.612 84.932 c \r\n" +
                                "52.342 55.507 82.583 38.013 113.5 44.504 c h";
                        break;

                    case StiCheckStyle.NoneRectangle:
                        shape = "24.153 97.958 m 24.153 170.458 l 98.653 170.458 l 173.153 170.458 l 173.153 97.958 l 173.153 25.458 l 98.653 25.458 l 24.153 25.458 l 24.153 97.958 l h \r\n" +
                                "157.911 97.708 m 157.653 154.958 l 98.653 154.958 l 39.653 154.958 l 39.393 98.958 l 39.25 68.158 39.348 42.395 39.611 41.708 c \r\n" +
                                "39.987 40.727 52.819 40.458 99.129 40.458 c 158.169 40.458 l 157.911 97.708 l h";
                        break;
                }
                #endregion

                pageStream.WriteLine("q");
                PushColorToStack();

                #region Set style
                //Fill color
                Color tempColor = StiBrush.ToColor(checkbox.TextBrush);
                if (tempColor.A != 0)
                {
                    SetNonStrokeColor(tempColor);
                }
                if (checkbox.TextBrush is StiGradientBrush || checkbox.TextBrush is StiGlareBrush)
                {
                    if (storeShading)   //only for editable, second shape use the same data
                    {
                        StoreShadingData2(pp.X, pp.Y, pp.Width, pp.Height, checkbox.TextBrush);
                    }
                    pageStream.WriteLine("/Pattern cs /P{0} scn", 1 + shadingCurrent);
                }
                if (checkbox.TextBrush is StiHatchBrush)
                {
                    StiHatchBrush hBrush = checkbox.TextBrush as StiHatchBrush;
                    pageStream.WriteLine("/Cs1 cs /PH{0} scn", GetHatchNumber(hBrush) + 1);
                }

                //stroke color
                SetStrokeColor(checkbox.ContourColor);

                pageStream.WriteLine("{0} w", ConvertToString(checkbox.Size));
                #endregion

                pageStream.WriteLine("1 0 0 1 {0} {1} cm", ConvertToString(pp.X + pp.Width / 2), ConvertToString(pp.Y + pp.Height / 2));
                double size = Math.Min(pp.Width, pp.Height);
                double scale = size / 200;
                pageStream.WriteLine("{0} 0 0 {0} 0 0 cm", ConvertToString(scale));
                pageStream.WriteLine("1 0 0 1 -98 -98 cm");

                pageStream.WriteLine(shape);
                pageStream.WriteLine("B");

                pageStream.WriteLine("Q");
                PopColorFromStack();
            }
        }

        private bool? GetCheckBoxValue(StiCheckBox checkbox)
        {
            bool? checkBoxValue = null;
            if ((checkbox != null) && (checkbox.CheckedValue != null))
            {
                checkBoxValue = false;
                string checkedValueStr = checkbox.CheckedValue.ToString().Trim().ToLower(CultureInfo.InvariantCulture);
                string[] strs = checkbox.Values.Split(new char[] { '/', ';', ',' });
                if (strs != null && strs.Length > 0)
                {
                    string firstValue = strs[0].Trim().ToLower(CultureInfo.InvariantCulture);
                    checkBoxValue = checkedValueStr == firstValue;
                }
            }
            return checkBoxValue;
        }
        #endregion

        #region RenderChart
        private void RenderChart(StiPdfData pp, bool assemble, int pageNumber)
        {
            StiChart chart = pp.Component as StiChart;
            Stimulsoft.Base.Context.StiContext context = null;

            using (var img = new Bitmap(1, 1))
            {
                using (var g = Graphics.FromImage(img))
                {
                    var painter = new Stimulsoft.Report.Painters.StiGdiContextPainter(g);
                    context = new Stimulsoft.Base.Context.StiContext(painter, true, false, false, 1f);

                    bool storeIsAnimation = chart.IsAnimation;
                    chart.IsAnimation = false;

                    float scale = 0.96f;

                    var chartGeom = chart.Core.Render(
                        context,
                        new RectangleF(0, 0, (float)(report.Unit.ConvertToHInches(pp.Component.Width) * scale), (float)(report.Unit.ConvertToHInches(pp.Component.Height) * scale)),
                        true);

                    chartGeom.DrawGeom(context);

                    chart.IsAnimation = storeIsAnimation;
                }
            }

            Matrix matrix = new Matrix(1, 0, 0, 1, 0, 0);
            const float chartScale = (float)(hiToTwips / 0.96);
            matrix.Translate((float)(pp.X), (float)(pp.Y + pp.Height));
            matrix.Scale(1, -1);
            matrix.Scale(chartScale, chartScale);

            StiPdfGeomWriter pdfGeomWriter = new StiPdfGeomWriter(pageStream, this, assemble);
            pdfGeomWriter.pageNumber = pageNumber;
            pdfGeomWriter.matrixCache.Push(matrix);

            foreach (Stimulsoft.Base.Context.StiGeom geom in context.geoms)
            {
                if (geom is StiPushTranslateTransformGeom)
                {
                    pdfGeomWriter.SaveState();
                    pdfGeomWriter.TranslateTransform((geom as StiPushTranslateTransformGeom).X, (geom as StiPushTranslateTransformGeom).Y);
                }
                if (geom is StiPushRotateTransformGeom)
                {
                    pdfGeomWriter.SaveState();
                    pdfGeomWriter.RotateTransform((geom as StiPushRotateTransformGeom).Angle);
                }
                if (geom is StiPopTransformGeom)
                {
                    pdfGeomWriter.RestoreState();
                }

                if (geom is StiPushClipGeom)
                {
                    pdfGeomWriter.SaveState();
                    pdfGeomWriter.SetClip((geom as StiPushClipGeom).ClipRectangle);
                }
                if (geom is StiPopClipGeom)
                {
                    pdfGeomWriter.RestoreState();
                }

                if (geom is StiPushSmothingModeToAntiAliasGeom)
                {
                    pdfGeomWriter.SaveState();
                }
                if (geom is StiPopSmothingModeGeom)
                {
                    pdfGeomWriter.RestoreState();
                }
                if (geom is StiPushTextRenderingHintToAntiAliasGeom)
                {
                    pdfGeomWriter.SaveState();
                }
                if (geom is StiPopTextRenderingHintGeom)
                {
                    pdfGeomWriter.RestoreState();
                }

                if (geom is StiBorderGeom)
                {
                    var border = geom as StiBorderGeom;
                    if (border.Background != null)
                    {
                        pdfGeomWriter.FillRectangle(RectToRectangleF(border.Rect), border.Background);
                    }
                    if (CheckPenGeom(border.BorderPen))
                    {
                        pdfGeomWriter.DrawRectangle(RectToRectangleF(border.Rect), border.BorderPen);
                    }
                }

                if (geom is StiLineGeom)
                {
                    var line = geom as StiLineGeom;
                    if (CheckPenGeom(line.Pen))
                    {
                        pdfGeomWriter.DrawLine(new PointF(line.X1, line.Y1), new PointF(line.X2, line.Y2), line.Pen);
                    }
                }

                if (geom is StiLinesGeom)
                {
                    var lines = geom as StiLinesGeom;
                    if (CheckPenGeom(lines.Pen))
                    {
                        pdfGeomWriter.DrawPolyline(lines.Points, lines.Pen);
                    }
                }

                if (geom is StiCurveGeom)
                {
                    var curve = geom as StiCurveGeom;
                    if (CheckPenGeom(curve.Pen))
                    {
                        pdfGeomWriter.DrawSpline(curve.Points, curve.Tension, curve.Pen);
                    }
                }

                if (geom is StiEllipseGeom)
                {
                    var ellipse = geom as StiEllipseGeom;
                    if (ellipse.Background != null)
                    {
                        pdfGeomWriter.FillEllipse(RectToRectangleF(ellipse.Rect), ellipse.Background);
                    }
                    if (CheckPenGeom(ellipse.BorderPen))
                    {
                        pdfGeomWriter.DrawEllipse(RectToRectangleF(ellipse.Rect), ellipse.BorderPen);
                    }
                }

                if (geom is StiCachedShadowGeom)
                {
                    #region Draw shadow
                    var shadow = geom as StiCachedShadowGeom;
                    var rect = shadow.Rect;
                    var sides = shadow.Sides;
                    float yTop = rect.Y + 8;
                    float xLeft = rect.X + 8;
                    float edgeOffset = ((sides & StiShadowSides.Edge) > 0) ? 4 : 0;
                    if ((sides & StiShadowSides.Top) > 0)
                    {
                        yTop = rect.Y + 4;
                    }
                    if ((sides & StiShadowSides.Left) > 0)
                    {
                        xLeft = rect.X + 4;
                    }

                    for (int index = 0; index < 3; index++)
                    {
                        List<PointF> pts = new List<PointF>();
                        if ((sides & StiShadowSides.Right) > 0)
                        {
                            pts.Add(new PointF(rect.Right, yTop));
                            if ((sides & StiShadowSides.Bottom) > 0)
                            {
                                pts.Add(new PointF(rect.Right, rect.Bottom));
                                pts.Add(new PointF(xLeft, rect.Bottom));
                                pts.Add(new PointF(xLeft, rect.Bottom + index + 1));
                                pts.Add(new PointF(rect.Right + index + 1, rect.Bottom + index + 1));
                            }
                            else
                            {
                                pts.Add(new PointF(rect.Right, rect.Bottom + edgeOffset));
                                pts.Add(new PointF(rect.Right + index + 1, rect.Bottom + edgeOffset));
                            }
                            pts.Add(new PointF(rect.Right + index + 1, yTop));
                        }
                        else
                        {
                            pts.Add(new PointF(xLeft, rect.Bottom));
                            pts.Add(new PointF(rect.Right + edgeOffset, rect.Bottom));
                            pts.Add(new PointF(rect.Right + edgeOffset, rect.Bottom + index + 1));
                            pts.Add(new PointF(xLeft, rect.Bottom + index + 1));
                        }

                        PointF[] ptss = new PointF[pts.Count];
                        pts.CopyTo(ptss);

                        var shadowBrush = new StiSolidBrush(Color.FromArgb(40 - 10 * index, Color.Black));

                        pdfGeomWriter.FillPolygon(ptss, shadowBrush);
                    }
                    #endregion
                }

                if (geom is StiShadowGeom)
                {
                    var shadow = geom as StiShadowGeom;
                    // todo
                }

                if (geom is StiTextGeom)
                {
                    #region Draw text
                    var text = geom as StiTextGeom;

                    var font = new Font(StiFontCollection.GetFontFamily(text.Font.FontName), text.Font.FontSize, text.Font.FontStyle, text.Font.Unit, text.Font.GdiCharSet, text.Font.GdiVerticalFont);

                    StringFormat sf = text.StringFormat.IsGeneric ? StringFormat.GenericDefault.Clone() as StringFormat : new StringFormat();
                    sf.Alignment = text.StringFormat.Alignment;
                    sf.FormatFlags = text.StringFormat.FormatFlags;
                    sf.HotkeyPrefix = text.StringFormat.HotkeyPrefix;
                    sf.LineAlignment = text.StringFormat.LineAlignment;
                    sf.Trimming = text.StringFormat.Trimming;

                    var brush = BrushToStiBrush(text.Brush);

                    bool allowHtmlTags = StiOptions.Engine.AllowHtmlTagsInChart && StiTextRenderer.CheckTextForHtmlTags(text.Text);

                    if (text.IsRotatedText)
                    {
                        PointF point = new PointF();
                        if (text.Location is PointF)
                        {
                            point = (PointF)text.Location;
                        }
                        else
                        {
                            RectangleF rect = RectToRectangleF(text.Location);
                            point = new PointF(rect.X + rect.Width / 2, rect.Y + rect.Height / 2);
                        }

                        SizeF textSize;
                        using (var img2 = new Bitmap(1, 1))
                        {
                            using (var gg = Graphics.FromImage(img2))
                            {
                                textSize = gg.MeasureString(text.Text, font, text.MaximalWidth.GetValueOrDefault(), sf);
                            }
                        }
                        RectangleF textRect = new RectangleF(0, 0, textSize.Width, textSize.Height);
                        PointF startPoint = GetStartPoint(text.RotationMode.GetValueOrDefault(), textRect);
                        textRect.X -= startPoint.X;
                        textRect.Y -= startPoint.Y;

                        pdfGeomWriter.SaveState();
                        pdfGeomWriter.TranslateTransform(point.X, point.Y);
                        if (text.Angle != 0)
                        {
                            pdfGeomWriter.RotateTransform(text.Angle);
                        }

                        pdfGeomWriter.DrawString(text.Text, font, brush, textRect, sf, allowHtmlTags);

                        pdfGeomWriter.RestoreState();
                    }
                    else
                    {
                        pdfGeomWriter.DrawString(text.Text, font, brush, RectToRectangleF(text.Location), sf, allowHtmlTags);
                    }
                    #endregion
                }

                if (geom is StiPathGeom)
                {
                    #region Draw path
                    var path = geom as StiPathGeom;
                    pdfGeomWriter.BeginPath();

                    foreach (var geom2 in path.Geoms)
                    {
                        if (geom2 is StiPieSegmentGeom)
                        {
                            var pie = geom2 as StiPieSegmentGeom;
                            pdfGeomWriter.DrawPie(pie.Rect, pie.StartAngle, pie.SweepAngle);
                        }
                        if (geom2 is StiArcSegmentGeom)
                        {
                            var arc = geom2 as StiArcSegmentGeom;
                            pdfGeomWriter.DrawArc(arc.Rect, arc.StartAngle, arc.SweepAngle);
                        }
                        if (geom2 is StiLineSegmentGeom)
                        {
                            var line = geom2 as StiLineSegmentGeom;
                            pdfGeomWriter.DrawLine(new PointF(line.X1, line.Y1), new PointF(line.X2, line.Y2), null);
                        }
                        if (geom2 is StiLinesSegmentGeom)
                        {
                            var lines = geom2 as StiLinesSegmentGeom;
                            pdfGeomWriter.DrawPolyline(lines.Points, null);
                        }
                        if (geom2 is StiCurveSegmentGeom)
                        {
                            var curve = geom2 as StiCurveSegmentGeom;
                            pdfGeomWriter.DrawSpline(curve.Points, curve.Tension, null);
                        }
                        if (geom2 is StiCloseFigureSegmentGeom)
                        {
                            pdfGeomWriter.CloseFigure();
                        }
                    }

                    pdfGeomWriter.CloseFigure();
                    pdfGeomWriter.EndPath();

                    if (path.Background != null)
                    {
                        pdfGeomWriter.FillPath(path.Background);
                    }
                    if (CheckPenGeom(path.Pen))
                    {
                        pdfGeomWriter.StrokePath(path.Pen);
                    }
                    #endregion
                }
            }

        }

        private static PointF GetStartPoint(StiRotationMode rotationMode, RectangleF textRect)
        {
            PointF centerPoint = new PointF(textRect.X + (textRect.Width / 2), textRect.Y + (textRect.Height / 2));

            switch (rotationMode)
            {
                case StiRotationMode.LeftCenter:
                    return new PointF(textRect.X, centerPoint.Y);

                case StiRotationMode.LeftBottom:
                    return new PointF(textRect.X, textRect.Bottom);

                case StiRotationMode.CenterTop:
                    return new PointF(centerPoint.X, textRect.Top);

                case StiRotationMode.CenterCenter:
                    return centerPoint;

                case StiRotationMode.CenterBottom:
                    return new PointF(centerPoint.X, textRect.Bottom);

                case StiRotationMode.RightTop:
                    return new PointF(textRect.Right, textRect.Top);

                case StiRotationMode.RightCenter:
                    return new PointF(textRect.Right, centerPoint.Y);

                case StiRotationMode.RightBottom:
                    return new PointF(textRect.Right, textRect.Bottom);

                default:
                    return textRect.Location;
            }
        }

        private RectangleF RectToRectangleF(object rect)
        {
            if (rect is RectangleF) return (RectangleF)rect;
            if (rect is Rectangle)
            {
                Rectangle rectangle = (Rectangle)rect;
                return new RectangleF((float)rectangle.X, (float)rectangle.Y, (float)rectangle.Width, (float)rectangle.Height);
            }
            if (rect is RectangleD)
            {
                RectangleD rectangle = (RectangleD)rect;
                return new RectangleF((float)rectangle.X, (float)rectangle.Y, (float)rectangle.Width, (float)rectangle.Height);
            }
            //if (rect is PointF)
            //{
            //    PointF point = (PointF)rect;
            //    return new RectangleF(point.X, point.Y, 0, 0);
            //}
            return new RectangleF();
        }

        private StiBrush BrushToStiBrush(object brush)
        {
            if (brush == null) return null;
            if (brush is Color)
            {
                return new StiSolidBrush((Color)brush);
            }
            if (brush is StiBrush)
            {
                return (StiBrush)brush;
            }
            return null;
        }

        private bool CheckPenGeom(StiPenGeom penGeom)
        {
            return !((penGeom == null) || (penGeom.Brush == null) || (penGeom.PenStyle == StiPenStyle.None));
        }
        #endregion

        #region RenderGauge
        private void RenderGauge(StiPdfData pp, bool assemble, int pageNumber)
        {
            var gauge = pp.Component as StiGauge;
            StiGdiGaugeContextPainter painter;

            using (var img = new Bitmap(1, 1))
            {
                using (var g = Graphics.FromImage(img))
                {
                    var storeIsAnimation = gauge.IsAnimation;
                    gauge.IsAnimation = false;

                    float scale = 0.96f;

                    painter = new StiGdiGaugeContextPainter(
                        g,
                        gauge,
                        new RectangleF(0, 0, (float)(report.Unit.ConvertToHInches(pp.Component.Width) * scale), (float)(report.Unit.ConvertToHInches(pp.Component.Height) * scale)),
                        scale);

                    painter.Geoms.Clear();
                    gauge.DrawGauge(painter);
                    gauge.IsAnimation = storeIsAnimation;
                }
            }

            Matrix matrix = new Matrix(1, 0, 0, 1, 0, 0);
            const float gaugeScale = (float)(hiToTwips / 0.96);
            matrix.Translate((float)(pp.X), (float)(pp.Y + pp.Height));
            matrix.Scale(1, -1);
            matrix.Scale(gaugeScale, gaugeScale);

            StiPdfGeomWriter pdfGeomWriter = new StiPdfGeomWriter(pageStream, this, assemble);
            pdfGeomWriter.pageNumber = pageNumber;
            pdfGeomWriter.matrixCache.Push(matrix);

            foreach (var geom in painter.Geoms)
            {
                if (geom is StiPushMatrixGaugeGeom)
                {
                    var matrixGeom = geom as StiPushMatrixGaugeGeom;
                    pdfGeomWriter.SaveState();
                    pdfGeomWriter.RotateTransform(matrixGeom.Angle, matrixGeom.CenterPoint.X, matrixGeom.CenterPoint.Y);
                    pdfGeomWriter.SaveState();
                    pdfGeomWriter.TranslateTransform(-matrixGeom.CenterPoint.X, -matrixGeom.CenterPoint.Y);
                }
                else if (geom is StiPopTranformGaugeGeom)
                {
                    pdfGeomWriter.RestoreState();
                    pdfGeomWriter.RestoreState();
                }
                else if (geom.Type == StiGaugeGeomType.RoundedRectangle)
                {
                    var roundedRectangleGeom = geom as StiRoundedRectangleGaugeGeom;
                    pdfGeomWriter.BeginPath();

                    var offset = 1;
                    var rightTop = roundedRectangleGeom.RightTop;
                    var leftTop = roundedRectangleGeom.LeftTop;
                    var rightBottom = roundedRectangleGeom.RightBottom;
                    var leftBottom = roundedRectangleGeom.LeftBottom;
                    var rect = roundedRectangleGeom.Rect;
                    var right = rect.X + rect.Width - offset;
                    var bottom = rect.Y + rect.Height - offset;

                    pdfGeomWriter.MoveTo(new PointF(rect.X + leftTop, rect.Y));
                    if (rightTop != 0)
                    {
                        pdfGeomWriter.DrawLineTo(new PointF(right - rightTop, rect.Y), null);
                        pdfGeomWriter.DrawArc(new RectangleF(right - rightTop, rect.Y, rightTop * 2, rightTop * 2), 270, 90);
                    }
                    else
                    {
                        pdfGeomWriter.DrawLineTo(new PointF(right, rect.Y), null);
                    }

                    if (rightBottom != 0)
                    {
                        pdfGeomWriter.DrawLineTo(new PointF(right, bottom - rightBottom), null);
                        pdfGeomWriter.DrawArc(new RectangleF(right - rightBottom, bottom - rightBottom, rightBottom * 2, rightBottom * 2), 0, 90);
                    }
                    else
                    {
                        pdfGeomWriter.DrawLineTo(new PointF(right, bottom), null);
                    }

                    if (leftBottom != 0)
                    {
                        pdfGeomWriter.DrawLineTo(new PointF(rect.X + leftBottom, bottom), null);
                        pdfGeomWriter.DrawArc(new RectangleF(rect.X - leftBottom, bottom - leftBottom, leftBottom * 2, leftBottom * 2), 90, 90);
                    }
                    else
                    {
                        pdfGeomWriter.DrawLineTo(new PointF(rect.X, bottom), null);
                    }

                    pdfGeomWriter.CloseFigure();
                    pdfGeomWriter.EndPath();

                    if (!StiBrush.IsTransparent(roundedRectangleGeom.Background))
                    {
                        pdfGeomWriter.FillPath(roundedRectangleGeom.Background);
                    }

                    var penGeom = GetPenGeom(roundedRectangleGeom);
                    if (CheckPenGeom(penGeom))
                    {
                        pdfGeomWriter.StrokePath(penGeom);
                    }
                }
                else if (geom.Type == StiGaugeGeomType.Rectangle)
                {
                    var rectangleGeom = geom as StiRectangleGaugeGeom;
                    if (rectangleGeom.Background != null)
                    {
                        pdfGeomWriter.FillRectangle(RectToRectangleF(rectangleGeom.Rect), rectangleGeom.Background);
                    }
                    var penGeom = GetPenGeom(rectangleGeom);
                    if (CheckPenGeom(penGeom))
                    {
                        pdfGeomWriter.DrawRectangle(RectToRectangleF(rectangleGeom.Rect), penGeom);
                    }
                }
                else if (geom.Type == StiGaugeGeomType.GraphicsPath)
                {
                    var pathGeom = geom as StiGraphicsPathGaugeGeom;
                    pdfGeomWriter.BeginPath();

                    var isClosed = false;

                    foreach (var pointGeom in pathGeom.Geoms)
                    {
                        if (pointGeom.Type == StiGaugeGeomType.GraphicsPathArc)
                        {
                            var arcGeom = pointGeom as StiGraphicsPathArcGaugeGeom;
                            pdfGeomWriter.DrawArc(new RectangleF(arcGeom.x, arcGeom.y, arcGeom.width, arcGeom.height), arcGeom.startAngle, arcGeom.sweepAngle);
                        }
                        else if (pointGeom.Type == StiGaugeGeomType.GraphicsPathLine)
                        {
                            var lineGeom = pointGeom as StiGraphicsPathLineGaugeGeom;
                            pdfGeomWriter.DrawLine(lineGeom.p1, lineGeom.p2, null);
                        }
                        else if (pointGeom.Type == StiGaugeGeomType.GraphicsPathLines)
                        {
                            var linesSegment = pointGeom as StiGraphicsPathLinesGaugeGeom;
                            pdfGeomWriter.DrawPolyline(linesSegment.points, null);
                            pdfGeomWriter.CloseFigure();
                            isClosed = true;
                        }
                        else if (pointGeom.Type == StiGaugeGeomType.GraphicsPathCloseFigure)
                        {
                            pdfGeomWriter.CloseFigure();
                            isClosed = true;
                        }
                    }
                    
                    pdfGeomWriter.EndPath();

                    if (isClosed && pathGeom.Background != null)
                    {
                        pdfGeomWriter.FillPath(pathGeom.Background);
                    }
                    var penGeom = GetPenGeom(pathGeom);
                    if (CheckPenGeom(penGeom))
                    {
                        pdfGeomWriter.StrokePath(penGeom);
                    }
                }
                else if (geom.Type == StiGaugeGeomType.Pie)
                {
                    var pieGeom = geom as StiPieGaugeGeom;
                    pdfGeomWriter.BeginPath();

                    pdfGeomWriter.DrawPie(pieGeom.rect, pieGeom.startAngle, pieGeom.sweepAngle);

                    pdfGeomWriter.CloseFigure();
                    pdfGeomWriter.EndPath();

                    var penGeom = GetPenGeom(pieGeom);
                    if (CheckPenGeom(penGeom))
                    {
                        pdfGeomWriter.StrokePath(penGeom);
                    }
                }
                else if (geom.Type == StiGaugeGeomType.Ellipse)
                {
                    var ellipseGeom = geom as StiEllipseGaugeGeom;
                    if (ellipseGeom.Background != null)
                    {
                        pdfGeomWriter.FillEllipse(RectToRectangleF(ellipseGeom.Rect), ellipseGeom.Background);
                    }

                    var penGeom = GetPenGeom(ellipseGeom);
                    if (CheckPenGeom(penGeom))
                    {
                        pdfGeomWriter.DrawEllipse(RectToRectangleF(ellipseGeom.Rect), penGeom);
                    }
                }
                else if (geom.Type == StiGaugeGeomType.GraphicsArcGeometry)
                {
                    var arcGeom = geom as StiGraphicsArcGeometryGaugeGeom;
                    pdfGeomWriter.BeginPath();

                    pdfGeomWriter.DrawArc(arcGeom.rect, arcGeom.startAngle, arcGeom.sweepAngle);

                    pdfGeomWriter.EndPath();

                    var penGeom = GetPenGeom(arcGeom);
                    if (CheckPenGeom(penGeom))
                    {
                        pdfGeomWriter.StrokePath(penGeom);
                    }
                }
                else if (geom.Type == StiGaugeGeomType.Text)
                {
                    var textGeom = geom as StiTextGaugeGeom;
                    var font = new Font(StiFontCollection.GetFontFamily(textGeom.Font.Name), textGeom.Font.Size, textGeom.Font.Style, textGeom.Font.Unit, textGeom.Font.GdiCharSet, textGeom.Font.GdiVerticalFont);

                    StringFormat sf = new StringFormat();
                    sf.Alignment = textGeom.StringFormat.Alignment;
                    sf.FormatFlags = textGeom.StringFormat.FormatFlags;
                    sf.HotkeyPrefix = textGeom.StringFormat.HotkeyPrefix;
                    sf.LineAlignment = textGeom.StringFormat.LineAlignment;
                    sf.Trimming = textGeom.StringFormat.Trimming;

                    bool allowHtmlTags = StiOptions.Engine.AllowHtmlTagsInChart && StiTextRenderer.CheckTextForHtmlTags(textGeom.Text);

                    pdfGeomWriter.DrawString(textGeom.Text, font, textGeom.Foreground, textGeom.Rect, sf, allowHtmlTags);
                }
                else if (geom.Type == StiGaugeGeomType.RadialRange)
                {
                    var radialRangeGeom = geom as StiRadialRangeGaugeGeom;

                    var centerPoint = radialRangeGeom.centerPoint;
                    var startAngle = radialRangeGeom.startAngle;
                    var sweepAngle = radialRangeGeom.sweepAngle;
                    var radius1 = radialRangeGeom.radius1;
                    var radius2 = radialRangeGeom.radius2;
                    var radius3 = radialRangeGeom.radius3;
                    var radius4 = radialRangeGeom.radius4;
                    var currentStartAngle = startAngle * StiDrawingHelper.PiDiv180;

                    var x1 = centerPoint.X + radius1 * (float)Math.Cos(currentStartAngle);
                    var y1 = centerPoint.Y + radius1 * (float)Math.Sin(currentStartAngle);
                    var firstPoint = new PointF(x1, y1);
                    var steps = Round(Math.Abs(sweepAngle / 90));
                    var stepAngle = sweepAngle / steps;
                    currentStartAngle = startAngle;

                    #region First Arc
                    pdfGeomWriter.BeginPath();
                    pdfGeomWriter.MoveTo(firstPoint);
                    var restRadius = radius1 - radius2;
                    float offsetStep = 1 / (steps);
                    float offset = 0;

                    currentStartAngle = startAngle;
                    for (var indexStep = 0; indexStep < steps; indexStep++)
                    {
                        var startRadius = radius1 - (restRadius * offset);
                        var endRadius = radius1 - (restRadius * (offset + offsetStep));

                        List<PointF> points = StiDrawingHelper.ConvertArcToCubicBezier(centerPoint, startRadius, endRadius, currentStartAngle, stepAngle);
                        pdfGeomWriter.DrawBezierTo(points[1], points[2], points[3], null);

                        currentStartAngle += stepAngle;
                        offset += offsetStep;
                    }
                    #endregion

                    #region Second Arc
                    stepAngle = sweepAngle / steps;
                    restRadius = radius3 - radius4;

                    offsetStep = 1 / (steps);
                    offset = 0;
                    var isFirst = true;

                    List<PointF> points2 = new List<PointF>();
                    for (var indexStep = 0; indexStep < steps; indexStep++)
                    {
                        float endRadius = radius3 - (restRadius * offset);
                        float startRadius = radius3 - (restRadius * (offset + offsetStep));

                        List<PointF> points = StiDrawingHelper.ConvertArcToCubicBezier(centerPoint, startRadius, endRadius, currentStartAngle, -stepAngle);

                        if (isFirst)
                        {
                            pdfGeomWriter.DrawLineTo(points[0], null);
                            isFirst = false;
                        }

                        pdfGeomWriter.DrawBezierTo(points[1], points[2], points[3], null);

                        currentStartAngle -= stepAngle;
                        offset -= offsetStep;
                    }

                    pdfGeomWriter.CloseFigure();
                    pdfGeomWriter.EndPath();
                    #endregion

                    if (!StiBrush.IsTransparent(radialRangeGeom.background))
                    {
                        pdfGeomWriter.FillPath(radialRangeGeom.background);
                    }

                    var penGeom = GetPenGeom(radialRangeGeom);
                    if (CheckPenGeom(penGeom))
                    {
                        pdfGeomWriter.StrokePath(penGeom);
                    }
                }
            }
        }

        private StiPenGeom GetPenGeom(StiBrush brush, float width)
        {
            return new StiPenGeom(brush, width);
        }

        private StiPenGeom GetPenGeom(StiGaugeGeom geom)
        {
            if (geom is StiRectangleGaugeGeom) return GetPenGeom((geom as StiRectangleGaugeGeom).BorderBrush, (geom as StiRectangleGaugeGeom).BorderWidth);
            if (geom is StiRoundedRectangleGaugeGeom) return GetPenGeom((geom as StiRoundedRectangleGaugeGeom).BorderBrush, (geom as StiRoundedRectangleGaugeGeom).BorderWidth);
            if (geom is StiEllipseGaugeGeom) return GetPenGeom((geom as StiEllipseGaugeGeom).BorderBrush, (geom as StiEllipseGaugeGeom).BorderWidth);
            if (geom is StiGraphicsPathGaugeGeom) return GetPenGeom((geom as StiGraphicsPathGaugeGeom).BorderBrush, (geom as StiGraphicsPathGaugeGeom).BorderWidth);
            if (geom is StiPieGaugeGeom) return GetPenGeom((geom as StiPieGaugeGeom).borderBrush, (geom as StiPieGaugeGeom).borderWidth);
            if (geom is StiGraphicsArcGeometryGaugeGeom) return GetPenGeom((geom as StiGraphicsArcGeometryGaugeGeom).borderBrush, (geom as StiGraphicsArcGeometryGaugeGeom).borderWidth);
            if (geom is StiRadialRangeGaugeGeom) return GetPenGeom((geom as StiRadialRangeGaugeGeom).borderBrush, (geom as StiRadialRangeGaugeGeom).borderWidth);
            return null;
        }

        private static float Round(float value)
        {
            int value1 = (int)value;
            float rest = value - value1;
            return (rest > 0) ? (float)(value1 + 1) : (float)(value1);
        }
        #endregion

        #region RenderMap
        private void RenderMap(StiPdfData pp, bool assemble, int pageNumber)
        {
            var map = pp.Component as StiMap;
            float zoom = 1;

            var painter = new StiGdiMapContextPainter(map);
            painter.MapStyle = StiMap.GetMapStyle(map);
            painter.PrepareDataColumns();
            painter.UpdateGroupedData();
            painter.UpdateHeatmapWithGroup();

            var geomContainer = StiMapLoader.GetGeomsObject(map.MapID.ToString());
            bool skipLabels = false;
            if (map.Stretch)
            {
                var rect = map.GetPaintRectangle(true, true);

                zoom = Math.Min(
                    (float) rect.Width / (float)geomContainer.Width, 
                    (float) rect.Height / (float)geomContainer.Height);

                if (zoom == 0)
                    zoom = 1f;

                if (zoom > 1f)
                    zoom = 1f;

                if (zoom < 0.2f)
                    skipLabels = true;
            }

            Matrix matrix = new Matrix(1, 0, 0, 1, 0, 0);
            const float gaugeScale = (float) (hiToTwips / 0.96);
            matrix.Translate((float) (pp.X), (float) (pp.Y + pp.Height));
            matrix.Scale(1, -1);
            matrix.Scale(gaugeScale, gaugeScale);

            var pdfGeomWriter = new StiPdfGeomWriter(pageStream, this, assemble);
            pdfGeomWriter.pageNumber = pageNumber;
            pdfGeomWriter.matrixCache.Push(matrix);

            pdfGeomWriter.SaveState();
            pdfGeomWriter.ScaleTransform(zoom, zoom);

            var hashLabels = new List<string>();

            float individualStepValue = 0.5f / geomContainer.Geoms.Count;
            painter.individualStep = individualStepValue;
            foreach (var geoms in geomContainer.Geoms.OrderBy(x => x.Key))
            {
                var data = painter.MapData.FirstOrDefault(x => x.Key == geoms.Key);
                var pointCount = 0;

                var brush = painter.GetGeomBrush1(data);
                painter.individualStep += individualStepValue;

                foreach (StiMapGeom geom in geoms.Geoms)
                {
                    switch (geom.GeomType)
                    {
                        case StiMapGeomType.MoveTo:
                        {
                            if (pointCount == 0)
                            {
                                pdfGeomWriter.BeginPath();
                            }
                            else if (pointCount > 0)
                            {
                                pointCount = 0;
                                FillMapPath(pdfGeomWriter, painter.IsBorderEmpty, brush, painter.MapStyle.BorderColor, painter.MapStyle.BorderSize);
                            }

                            var moveTo = (StiMoveToMapGeom) geom;
                            pdfGeomWriter.MoveTo(new PointF((float) moveTo.X, (float) moveTo.Y));
                        }
                            break;

                        case StiMapGeomType.Line:
                        {
                            var line = (StiLineMapGeom) geom;

                            var point = new PointF((float) line.X, (float) line.Y);
                            pdfGeomWriter.DrawLineTo(point, null);
                            pointCount++;
                        }
                            break;

                        case StiMapGeomType.Bezier:
                        {
                            var bezier = (StiBezierMapGeom) geom;

                            var point1 = new PointF((float) bezier.X1, (float) bezier.Y1);
                            var point2 = new PointF((float) bezier.X2, (float) bezier.Y2);
                            var point3 = new PointF((float) bezier.X3, (float) bezier.Y3);

                            pdfGeomWriter.DrawBezierTo(point1, point2, point3, null);
                            pointCount++;
                            //pdfGeomWriter.StrokePath(new Pen(Color.Red));
                            //return;
                        }
                            break;

                        case StiMapGeomType.Beziers:
                        {
                            var beziers = (StiBeziersMapGeom) geom;

                            var index = 0;
                            while (beziers.Array.Length > index)
                            {
                                var point1 = new PointF((float) beziers.Array[index++], (float) beziers.Array[index++]);
                                var point2 = new PointF((float) beziers.Array[index++], (float) beziers.Array[index++]);

                                if (index == beziers.Array.Length) index -= 2;
                                var point3 = new PointF((float) beziers.Array[index++], (float) beziers.Array[index++]);

                                pdfGeomWriter.DrawBezierTo(point1, point2, point3, null);
                                pointCount++;
                            }
                        }
                            break;

                        case StiMapGeomType.Close:
                        {
                            pointCount = 0;
                            pdfGeomWriter.CloseFigure();
                            FillMapPath(pdfGeomWriter, painter.IsBorderEmpty, brush, painter.MapStyle.BorderColor, painter.MapStyle.BorderSize);
                        }
                            break;
                    }
                }

                if (pointCount > 0)
                {
                    pointCount = 0;
                    FillMapPath(pdfGeomWriter, painter.IsBorderEmpty, brush, painter.MapStyle.BorderColor, painter.MapStyle.BorderSize);
                }

                if (map.ShowValue || map.DisplayNameType != StiDisplayNameType.None)
                    hashLabels.Add(geoms.Key);
            }

            #region Draw Labels
            if (hashLabels.Count > 0 && !skipLabels)
            {
                var svgContainer = StiMapLoader.LoadResource(map.MapID.ToString());

                var typeface = new Font("Calibri", 18);
                var foregroundDark = new SolidBrush(Color.FromArgb(255, 37, 37, 37));
                var foregroundLight = new SolidBrush(Color.FromArgb(180, 251, 251, 251));

                foreach (var key in hashLabels)
                {
                    var path = svgContainer.HashPaths[key];
                    if (path.SkipText) continue;

                    string text = null;
                    var info = painter.MapData.FirstOrDefault(x => x.Key == key);
                    switch (map.DisplayNameType)
                    {
                        case StiDisplayNameType.Full:
                            {
                                text = (info != null)
                                    ? info.Name
                                    : path.EnglishName;
                            }
                            break;

                        case StiDisplayNameType.Short:
                            {
                                text = Stimulsoft.Report.Maps.Helpers.StiMapHelper.PrepareIsoCode(path.ISOCode);
                            }
                            break;
                    }

                    if (map.ShowValue)
                    {
                        if (info != null && info.Value != null)
                        {
                            if (text == null)
                            {
                                text = info.Value.ToString();
                            }
                            else
                            {
                                text += Environment.NewLine;
                                text += info.Value.ToString();
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(text))
                    {
                        //var bounds = path.Rect;
                        //SizeF textSize = (path.SetMaxWidth)
                        //    ? g.MeasureString(text, typeface, bounds.Width)
                        //    : g.MeasureString(text, typeface, bounds.Width);

                        //#region Calc position

                        //int x = bounds.X + (int)((bounds.Width - textSize.Width) / 2);
                        //int y = bounds.Y + (int)((bounds.Height - textSize.Height) / 2);

                        //if (path.HorAlignment != null)
                        //{
                        //    switch (path.HorAlignment.GetValueOrDefault())
                        //    {
                        //        case StiTextHorAlignment.Left:
                        //            x = bounds.X;
                        //            break;

                        //        case StiTextHorAlignment.Right:
                        //            x = bounds.Right - (int)textSize.Width;
                        //            break;
                        //    }
                        //}

                        //if (path.VertAlignment != null)
                        //{
                        //    switch (path.VertAlignment.GetValueOrDefault())
                        //    {
                        //        case StiVertAlignment.Top:
                        //            y = bounds.Y;
                        //            break;

                        //        case StiVertAlignment.Bottom:
                        //            y = bounds.Bottom - (int)textSize.Height;
                        //            break;
                        //    }
                        //}

                        //#endregion

                        //if (path.SetMaxWidth)
                        //{
                        //    var rectDark = new RectangleF(x, y, textSize.Width + 20, textSize.Height);
                        //    var rectLight = new RectangleF(x + 1, y + 1, textSize.Width + 20, textSize.Height);

                        //    g.DrawString(text, typeface, foregroundLight, rectLight);
                        //    g.DrawString(text, typeface, foregroundDark, rectDark);
                        //}
                        //else
                        //{
                        //    var textPosDark = new Point(x, y);
                        //    var textPosLight = new Point(x + 1, y + 1);

                        //    g.DrawString(text, typeface, foregroundLight, textPosLight);
                        //    g.DrawString(text, typeface, foregroundDark, textPosDark);
                        //}
                    }
                }
            }
            #endregion

            pdfGeomWriter.RestoreState();
        }

        private void FillMapPath(StiPdfGeomWriter pdfGeomWriter, bool isBorderEmpty, StiBrush fillBrush, Color borderColor, double borderSize)
        {
            pdfGeomWriter.FillPath(StiBrush.ToColor(fillBrush));

            if (!isBorderEmpty)
            {
                using (var pen = new Pen(borderColor, (float)borderSize))
                {
                    pdfGeomWriter.StrokePath(pen);
                }
            }
        }

        #endregion

        #region Render Watermark
        private void RenderWatermark(StiPage page, bool behind, double pageWidth, double pageHeight, float imageResolution)
        {
            StiWatermark watermark = page.Watermark;
            if ((watermark != null) && (watermark.Enabled))
            {
                #region watermark image
                if ((watermark.ExistImage() || !string.IsNullOrWhiteSpace(watermark.ImageHyperlink)) && (watermark.ShowImageBehind == behind))
                {
                    StiImageData pd = (StiImageData)imageList[imagesCurrent];

                    double imageWidth = pd.Width * hiToTwips * watermark.ImageMultipleFactor;
                    double imageHeight = pd.Height * hiToTwips * watermark.ImageMultipleFactor;
                    double imageX = 0;
                    double imageY = 0;
                    int dupX = 1;
                    int dupY = 1;
                    ContentAlignment align = watermark.ImageAlignment;
                    bool isImageTiling = watermark.ImageTiling;

                    if (watermark.ImageStretch)
                    {
                        double aspectRatio = imageHeight / imageWidth;
                        imageWidth = pageWidth;
                        imageHeight = pageHeight;
                        isImageTiling = false;
                        if (watermark.AspectRatio)
                        {
                            if (pageHeight / pageWidth > aspectRatio)
                            {
                                imageHeight = imageWidth * aspectRatio;
                            }
                            else
                            {
                                imageWidth = imageHeight / aspectRatio;
                            }
                        }
                    }
                    if ((watermark.ImageStretch) || (watermark.ImageMultipleFactor > 1))
                    {
                        //						pd.Interpolate = true;
                        //						imageList[imagesCurrent] = pd;
                        imageInterpolationTable[(int)imageCache.ImageIndex[imagesCurrent]] = true;
                    }
                    if (isImageTiling)
                    {
                        align = ContentAlignment.TopLeft;
                        dupX = (int)(pageWidth / imageWidth) + 1;
                        dupY = (int)(pageHeight / imageHeight) + 1;
                    }

                    switch (align)
                    {
                        case ContentAlignment.TopCenter:
                        case ContentAlignment.MiddleCenter:
                        case ContentAlignment.BottomCenter:
                            imageX = (pageWidth - imageWidth) / 2f;
                            break;

                        case ContentAlignment.TopRight:
                        case ContentAlignment.MiddleRight:
                        case ContentAlignment.BottomRight:
                            imageX = pageWidth - imageWidth;
                            break;
                    }
                    switch (align)
                    {
                        case ContentAlignment.TopLeft:
                        case ContentAlignment.TopCenter:
                        case ContentAlignment.TopRight:
                            imageY = pageHeight - imageHeight;
                            break;

                        case ContentAlignment.MiddleLeft:
                        case ContentAlignment.MiddleCenter:
                        case ContentAlignment.MiddleRight:
                            imageY = (pageHeight - imageHeight) / 2f;
                            break;
                    }

                    SetNonStrokeColor(Color.Black);

                    for (int indexY = 0; indexY < dupY; indexY++)
                    {
                        for (int indexX = 0; indexX < dupX; indexX++)
                        {
                            pageStream.WriteLine("q");
                            PushColorToStack();
                            pageStream.WriteLine("{0} 0 0 {1} {2} {3} cm",
                                ConvertToString(imageWidth),
                                ConvertToString(imageHeight),
                                ConvertToString(imageX + imageWidth * indexX),
                                ConvertToString(imageY - imageHeight * indexY));
                            pageStream.WriteLine("/{0} Do", pd.Name);
                            pageStream.WriteLine("Q");
                            PopColorFromStack();
                        }
                    }

                    imagesCurrent++;
                }
                #endregion

                #region watermark text
                if ((!string.IsNullOrEmpty(watermark.Text)) && (watermark.ShowBehind == behind))
                {
                    StiPdfData pp = new StiPdfData();
                    pp.X = 0;
                    pp.Y = 0;
                    pp.Width = pageWidth;
                    pp.Height = pageHeight;
                    StiText stt = new StiText(new RectangleD(pp.X, pp.Y, pp.Width, pp.Height));
                    stt.Text = watermark.Text;
                    stt.TextBrush = watermark.TextBrush;
                    stt.Font = watermark.Font;
                    stt.TextOptions = new StiTextOptions();
                    stt.TextOptions.Angle = watermark.Angle;
                    stt.HorAlignment = StiTextHorAlignment.Center;
                    stt.VertAlignment = StiVertAlignment.Center;
                    stt.Page = page;
                    stt.TextQuality = StiTextQuality.Standard;
                    pp.Component = stt;
                    RenderTextFont(pp);
                    RenderText(pp);
                }
                #endregion
            }
        }
        #endregion

        #region Store brushes data
        internal void StoreShadingData1(StiBrush brush, int pageNumber)
        {
            if (brush != null)
            {
                if (brush is StiGradientBrush)
                {
                    StiGradientBrush gbr = brush as StiGradientBrush;
                    StiShadingData ssd = new StiShadingData();
                    ssd.Angle = gbr.Angle;
                    ssd.Color1 = gbr.StartColor;
                    ssd.Color2 = gbr.EndColor;
                    ssd.Page = pageNumber;
                    shadingArray.Add(ssd);
                }
                if (brush is StiGlareBrush)
                {
                    StiGlareBrush gbr = brush as StiGlareBrush;
                    StiShadingData ssd = new StiShadingData();
                    ssd.Angle = gbr.Angle;
                    ssd.Color1 = gbr.StartColor;
                    ssd.Color2 = gbr.EndColor;
                    ssd.IsGlare = true;
                    ssd.Page = pageNumber;
                    shadingArray.Add(ssd);
                }
            }
        }
        internal int StoreShadingData2(double x, double y, double width, double height, StiBrush brush)
        {
            if ((brush != null) && (brush is StiGradientBrush || brush is StiGlareBrush))
            {
                StiShadingData ssd = (StiShadingData)shadingArray[shadingCurrent];
                ssd.X = x;
                ssd.Y = y;
                ssd.Width = width;
                ssd.Height = height;
                shadingArray[shadingCurrent] = ssd;
                shadingCurrent++;
            }
            return shadingCurrent;
        }
        internal void StoreHatchData(StiBrush brush)
        {
            if (brush != null)
            {
                if (brush is StiHatchBrush)
                {
                    GetHatchNumber(brush as StiHatchBrush);
                }
                if (brush is StiGlassBrush)
                {
                    StiGlassBrush glass = brush as StiGlassBrush;
                    if (glass.DrawHatch)
                    {
                        GetHatchNumber(glass.GetTopBrush() as HatchBrush);
                        GetHatchNumber(glass.GetBottomBrush() as HatchBrush);
                    }
                }
            }
        }
        #endregion

        #region RenderPrimitives
        private void RenderPrimitives(StiPdfData pp)
        {
            if (pp.Component is StiRoundedRectanglePrimitive)
            {
                RenderRoundedRectanglePrimitive(pp);
            }
            else
            {
                RenderLinePrimitive(pp);
            }
        }

        private void RenderRoundedRectanglePrimitive(StiPdfData pp)
        {
            IStiBorder mBorder = pp.Component as IStiBorder;
            if (mBorder != null)
            {
                StiRoundedRectanglePrimitive primitive = pp.Component as StiRoundedRectanglePrimitive;
                if (primitive.Style == StiPenStyle.None) return;

                StiBorderSide border = new StiBorderSide(mBorder.Border.Color, mBorder.Border.Size, mBorder.Border.Style);

                bool needPush = (mBorder.Border.Style != StiPenStyle.None) && (mBorder.Border.Style != StiPenStyle.Solid);
                if (needPush)
                {
                    pageStream.WriteLine("q");
                    PushColorToStack();
                }

                double offset = StoreBorderSideData(border);
                double roundOffset = Math.Min((pp.Width < pp.Height ? pp.Width : pp.Height), 100 * pp.Component.Page.Zoom) * primitive.Round;
                double roundOffset2 = roundOffset * (1 - pdfCKT);

                double x1 = pp.X - offset;
                double x2 = pp.X + pp.Width + offset;
                double xc = pp.X + pp.Width / 2d;
                double y1 = pp.Y - offset;
                double y2 = pp.Y + pp.Height + offset;

                #region Draw single lines
                if (primitive.LeftSide)
                {
                    if (primitive.BottomSide)
                    {
                        pageStream.WriteLine("{0} {1} m", ConvertToString((primitive.RightSide ? xc : x2)), ConvertToString(y1));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(x1 + roundOffset), ConvertToString(y1));
                        pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                            ConvertToString(x1 + roundOffset2), ConvertToString(y1),
                            ConvertToString(x1), ConvertToString(y1 + roundOffset2),
                            ConvertToString(x1), ConvertToString(y1 + roundOffset));
                    }
                    else
                    {
                        pageStream.WriteLine("{0} {1} m", ConvertToString(x1), ConvertToString(y1));
                    }
                    if (primitive.TopSide)
                    {
                        pageStream.WriteLine("{0} {1} l", ConvertToString(x1), ConvertToString(y2 - roundOffset));
                        pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                            ConvertToString(x1), ConvertToString(y2 - roundOffset2),
                            ConvertToString(x1 + roundOffset2), ConvertToString(y2),
                            ConvertToString(x1 + roundOffset), ConvertToString(y2));
                        pageStream.WriteLine("{0} {1} l S", ConvertToString((primitive.RightSide ? xc : x2)), ConvertToString(y2));
                    }
                    else
                    {
                        pageStream.WriteLine("{0} {1} l S", ConvertToString(x1), ConvertToString(y2));
                    }
                }
                if (primitive.RightSide)
                {
                    if (primitive.BottomSide)
                    {
                        pageStream.WriteLine("{0} {1} m", ConvertToString((primitive.LeftSide ? xc : x1)), ConvertToString(y1));
                        pageStream.WriteLine("{0} {1} l", ConvertToString(x2 - roundOffset), ConvertToString(y1));
                        pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                            ConvertToString(x2 - roundOffset2), ConvertToString(y1),
                            ConvertToString(x2), ConvertToString(y1 + roundOffset2),
                            ConvertToString(x2), ConvertToString(y1 + roundOffset));
                    }
                    else
                    {
                        pageStream.WriteLine("{0} {1} m", ConvertToString(x2), ConvertToString(y1));
                    }
                    if (primitive.TopSide)
                    {
                        pageStream.WriteLine("{0} {1} l", ConvertToString(x2), ConvertToString(y2 - roundOffset));
                        pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                            ConvertToString(x2), ConvertToString(y2 - roundOffset2),
                            ConvertToString(x2 - roundOffset2), ConvertToString(y2),
                            ConvertToString(x2 - roundOffset), ConvertToString(y2));
                        pageStream.WriteLine("{0} {1} l S", ConvertToString((primitive.LeftSide ? xc : x1)), ConvertToString(y2));
                    }
                    else
                    {
                        pageStream.WriteLine("{0} {1} l S", ConvertToString(x2), ConvertToString(y2));
                    }
                }
                if (primitive.TopSide && !primitive.LeftSide && !primitive.RightSide)
                {
                    pageStream.WriteLine("{0} {1} m", ConvertToString(x1), ConvertToString(y2));
                    pageStream.WriteLine("{0} {1} l S", ConvertToString(x2), ConvertToString(y2));
                }
                if (primitive.BottomSide && !primitive.LeftSide && !primitive.RightSide)
                {
                    pageStream.WriteLine("{0} {1} m", ConvertToString(x1), ConvertToString(y1));
                    pageStream.WriteLine("{0} {1} l S", ConvertToString(x2), ConvertToString(y1));
                }
                #endregion

                if (border.Style == StiPenStyle.Double)
                {
                    #region Draw second lines
                    roundOffset -= offset * 2;
                    roundOffset2 = roundOffset * (1 - pdfCKT);

                    double x3 = pp.X + offset;
                    double x4 = pp.X + pp.Width - offset;
                    double y3 = pp.Y + offset;
                    double y4 = pp.Y + pp.Height - offset;

                    if (primitive.LeftSide)
                    {
                        if (primitive.BottomSide)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString((primitive.RightSide ? xc : x2)), ConvertToString(y3));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(x3 + roundOffset), ConvertToString(y3));
                            pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                                ConvertToString(x3 + roundOffset2), ConvertToString(y3),
                                ConvertToString(x3), ConvertToString(y3 + roundOffset2),
                                ConvertToString(x3), ConvertToString(y3 + roundOffset));
                        }
                        else
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(x3), ConvertToString(y1));
                        }
                        if (primitive.TopSide)
                        {
                            pageStream.WriteLine("{0} {1} l", ConvertToString(x3), ConvertToString(y4 - roundOffset));
                            pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                                ConvertToString(x3), ConvertToString(y4 - roundOffset2),
                                ConvertToString(x3 + roundOffset2), ConvertToString(y4),
                                ConvertToString(x3 + roundOffset), ConvertToString(y4));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString((primitive.RightSide ? xc : x2)), ConvertToString(y4));
                        }
                        else
                        {
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(x3), ConvertToString(y2));
                        }
                    }
                    if (primitive.RightSide)
                    {
                        if (primitive.BottomSide)
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString((primitive.LeftSide ? xc : x1)), ConvertToString(y3));
                            pageStream.WriteLine("{0} {1} l", ConvertToString(x4 - roundOffset), ConvertToString(y3));
                            pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                                ConvertToString(x4 - roundOffset2), ConvertToString(y3),
                                ConvertToString(x4), ConvertToString(y3 + roundOffset2),
                                ConvertToString(x4), ConvertToString(y3 + roundOffset));
                        }
                        else
                        {
                            pageStream.WriteLine("{0} {1} m", ConvertToString(x4), ConvertToString(y1));
                        }
                        if (primitive.TopSide)
                        {
                            pageStream.WriteLine("{0} {1} l", ConvertToString(x4), ConvertToString(y4 - roundOffset));
                            pageStream.WriteLine("{0} {1} {2} {3} {4} {5} c",
                                ConvertToString(x4), ConvertToString(y4 - roundOffset2),
                                ConvertToString(x4 - roundOffset2), ConvertToString(y4),
                                ConvertToString(x4 - roundOffset), ConvertToString(y4));
                            pageStream.WriteLine("{0} {1} l S", ConvertToString((primitive.LeftSide ? xc : x1)), ConvertToString(y4));
                        }
                        else
                        {
                            pageStream.WriteLine("{0} {1} l S", ConvertToString(x4), ConvertToString(y2));
                        }
                    }
                    if (primitive.TopSide && !primitive.LeftSide && !primitive.RightSide)
                    {
                        pageStream.WriteLine("{0} {1} m", ConvertToString(x1), ConvertToString(y4));
                        pageStream.WriteLine("{0} {1} l S", ConvertToString(x2), ConvertToString(y4));
                    }
                    if (primitive.BottomSide && !primitive.LeftSide && !primitive.RightSide)
                    {
                        pageStream.WriteLine("{0} {1} m", ConvertToString(x1), ConvertToString(y3));
                        pageStream.WriteLine("{0} {1} l S", ConvertToString(x2), ConvertToString(y3));
                    }
                    #endregion
                }

                if (needPush)
                {
                    pageStream.WriteLine("Q");
                    PopColorFromStack();
                }
            }
        }

        private void RenderLinePrimitive(StiPdfData pp)
        {
            StiLinePrimitive primitive = pp.Component as StiLinePrimitive;
            StiHorizontalLinePrimitive horizontalLine = pp.Component as StiHorizontalLinePrimitive;
            StiVerticalLinePrimitive verticalLine = pp.Component as StiVerticalLinePrimitive;
            double x1 = pp.X;
            double y1 = pp.Top;
            double x2 = pp.X + (horizontalLine != null ? pp.Width : 0);
            double y2 = pp.Top - (verticalLine != null ? pp.Height : 0);

            #region Render line
            if (primitive.Style != StiPenStyle.None)
            {
                StiBorderSide border = new StiBorderSide(primitive.Color, primitive.Size, primitive.Style);

                bool needPush = (border.Style != StiPenStyle.None) && (border.Style != StiPenStyle.Solid);
                if (needPush)
                {
                    pageStream.WriteLine("q");
                    PushColorToStack();
                }

                double offset = StoreBorderSideData(border);

                if (horizontalLine != null)
                {
                    pageStream.WriteLine("{0} {1} m {2} {1} l S", ConvertToString(x1), ConvertToString(y1 - offset), ConvertToString(x2));
                    if (border.Style == StiPenStyle.Double)
                    {
                        pageStream.WriteLine("{0} {1} m {2} {1} l S", ConvertToString(x1), ConvertToString(y1 + offset), ConvertToString(x2));
                    }
                }
                if (verticalLine != null)
                {
                    pageStream.WriteLine("{0} {1} m {0} {2} l S", ConvertToString(x1 - offset), ConvertToString(y1), ConvertToString(y2));
                    if (border.Style == StiPenStyle.Double)
                    {
                        pageStream.WriteLine("{0} {1} m {0} {2} l S", ConvertToString(x1 + offset), ConvertToString(y1), ConvertToString(y2));
                    }
                }

                if (needPush)
                {
                    pageStream.WriteLine("Q");
                    PopColorFromStack();
                }
            }
            #endregion

            #region Render caps
            double lineSize = primitive.Size * hiToTwips * 0.9;
            if (lineSize < 0.32) lineSize = 0.32;
            pageStream.WriteLine("{0} w", ConvertToString(lineSize));

            if (horizontalLine != null)
            {
                RenderCap(horizontalLine.StartCap, x1, y1, StiAngle.Angle0, lineSize);
                RenderCap(horizontalLine.EndCap, x2, y2, StiAngle.Angle180, lineSize);
            }
            if (verticalLine != null)
            {
                RenderCap(verticalLine.StartCap, x1, y1, StiAngle.Angle270, lineSize);
                RenderCap(verticalLine.EndCap, x2, y2, StiAngle.Angle90, lineSize);
            }
            #endregion
        }

        private void RenderCap(StiCap cap, double x, double y, StiAngle angle, double lineSize)
        {
            if (cap.Style == StiCapStyle.None) return;

            SetStrokeColor(cap.Color);
            if (cap.Fill)
            {
                SetNonStrokeColor(cap.Color);
            }

            double vertOffset = cap.Height / 2f * hiToTwips;
            double horOffset = cap.Width / 2f * hiToTwips;

            switch (cap.Style)
            {
                case StiCapStyle.Arrow:
                    if (angle == StiAngle.Angle0)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l h",
                            ConvertToString(x - lineSize), ConvertToString(y),
                            ConvertToString(x + horOffset * 2), ConvertToString(y + vertOffset),
                            ConvertToString(x + horOffset * 2), ConvertToString(y - vertOffset));
                    }
                    if (angle == StiAngle.Angle90)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l h",
                            ConvertToString(x), ConvertToString(y - lineSize),
                            ConvertToString(x - horOffset), ConvertToString(y + vertOffset * 2),
                            ConvertToString(x + horOffset), ConvertToString(y + vertOffset * 2));
                    }
                    if (angle == StiAngle.Angle180)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l h",
                            ConvertToString(x + lineSize), ConvertToString(y),
                            ConvertToString(x - horOffset * 2), ConvertToString(y - vertOffset),
                            ConvertToString(x - horOffset * 2), ConvertToString(y + vertOffset));
                    }
                    if (angle == StiAngle.Angle270)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l h",
                            ConvertToString(x), ConvertToString(y + lineSize),
                            ConvertToString(x + horOffset), ConvertToString(y - vertOffset * 2),
                            ConvertToString(x - horOffset), ConvertToString(y - vertOffset * 2));
                    }
                    break;

                case StiCapStyle.Open:
                    if (angle == StiAngle.Angle0)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l",
                            ConvertToString(x + horOffset * 2), ConvertToString(y + vertOffset),
                            ConvertToString(x - lineSize), ConvertToString(y),
                            ConvertToString(x + horOffset * 2), ConvertToString(y - vertOffset));
                    }
                    if (angle == StiAngle.Angle90)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l",
                            ConvertToString(x - horOffset), ConvertToString(y + vertOffset * 2),
                            ConvertToString(x), ConvertToString(y - lineSize),
                            ConvertToString(x + horOffset), ConvertToString(y + vertOffset * 2));
                    }
                    if (angle == StiAngle.Angle180)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l",
                            ConvertToString(x - horOffset * 2), ConvertToString(y - vertOffset),
                            ConvertToString(x + lineSize), ConvertToString(y),
                            ConvertToString(x - horOffset * 2), ConvertToString(y + vertOffset));
                    }
                    if (angle == StiAngle.Angle270)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l",
                            ConvertToString(x + horOffset), ConvertToString(y - vertOffset * 2),
                            ConvertToString(x), ConvertToString(y + lineSize),
                            ConvertToString(x - horOffset), ConvertToString(y - vertOffset * 2));
                    }
                    break;

                case StiCapStyle.Stealth:
                    if (angle == StiAngle.Angle0)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l {6} {7} l h",
                            ConvertToString(x + horOffset * 2), ConvertToString(y + vertOffset),
                            ConvertToString(x - lineSize), ConvertToString(y),
                            ConvertToString(x + horOffset * 2), ConvertToString(y - vertOffset),
                            ConvertToString(x + horOffset * 1.3), ConvertToString(y));
                    }
                    if (angle == StiAngle.Angle90)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l {6} {7} l h",
                            ConvertToString(x - horOffset), ConvertToString(y + vertOffset * 2),
                            ConvertToString(x), ConvertToString(y - lineSize),
                            ConvertToString(x + horOffset), ConvertToString(y + vertOffset * 2),
                            ConvertToString(x), ConvertToString(y + vertOffset * 1.3));
                    }
                    if (angle == StiAngle.Angle180)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l {6} {7} l h",
                            ConvertToString(x - horOffset * 2), ConvertToString(y - vertOffset),
                            ConvertToString(x + lineSize), ConvertToString(y),
                            ConvertToString(x - horOffset * 2), ConvertToString(y + vertOffset),
                            ConvertToString(x - horOffset * 1.3), ConvertToString(y));
                    }
                    if (angle == StiAngle.Angle270)
                    {
                        pageStream.Write("{0} {1} m {2} {3} l {4} {5} l {6} {7} l h",
                            ConvertToString(x + horOffset), ConvertToString(y - vertOffset * 2),
                            ConvertToString(x), ConvertToString(y + lineSize),
                            ConvertToString(x - horOffset), ConvertToString(y - vertOffset * 2),
                            ConvertToString(x), ConvertToString(y - vertOffset * 1.3));
                    }
                    break;

                case StiCapStyle.Square:
                    pageStream.Write("{0} {1} {2} {3} re",
                        ConvertToString(x - horOffset), ConvertToString(y - vertOffset),
                        ConvertToString(horOffset * 2), ConvertToString(vertOffset * 2));
                    break;

                case StiCapStyle.Diamond:
                    pageStream.Write("{0} {1} m {2} {3} l {4} {5} l {6} {7} l h",
                        ConvertToString(x), ConvertToString(y + vertOffset),
                        ConvertToString(x + horOffset), ConvertToString(y),
                        ConvertToString(x), ConvertToString(y - vertOffset),
                        ConvertToString(x - horOffset), ConvertToString(y));
                    break;

                case StiCapStyle.Oval:
                    StiPdfGeomWriter pdfGeomWriter = new StiPdfGeomWriter(null, this);
                    pageStream.Write(pdfGeomWriter.GetEllipseString(x - horOffset, y - vertOffset, horOffset * 2, vertOffset * 2));
                    break;

            }

            if (cap.Fill && cap.Style != StiCapStyle.Open)
            {
                pageStream.WriteLine(" B");
            }
            else
            {
                pageStream.WriteLine(" S");
            }
        }
        #endregion

        #region Render Metadata
        private void RenderMetadata(StiReport report)
        {
            #region Make memory stream
            MemoryStream mainStream = new MemoryStream();

            string stXpacket = "<?xpacket begin=\"\xEF\xBB\xBF\" id=\"W5M0MpCehiHzreSzNTczkc9d\"?>\r\n";
            byte[] bufXpacket = new byte[stXpacket.Length];
            for (int indexChar = 0; indexChar < bufXpacket.Length; indexChar++) bufXpacket[indexChar] = (byte)stXpacket[indexChar];
            mainStream.Write(bufXpacket, 0, bufXpacket.Length);

            #region Make XML data
            MemoryStream ms = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(ms, Encoding.UTF8);
            writer.Formatting = Formatting.Indented;

            writer.WriteStartElement("x:xmpmeta");
            writer.WriteAttributeString("xmlns:x", "adobe:ns:meta/");
            writer.WriteAttributeString("x:xmptk", "Adobe XMP Core 4.0-c316 44.253921, Sun Oct 01 2006 17:14:39");
            writer.WriteStartElement("rdf:RDF");
            writer.WriteAttributeString("xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");

            writer.WriteStartElement("rdf:Description");
            writer.WriteAttributeString("rdf:about", "");
            writer.WriteAttributeString("xmlns:xap", "http://ns.adobe.com/xap/1.0/");
            writer.WriteElementString("xap:ModifyDate", currentDateTimeMeta);
            writer.WriteElementString("xap:CreateDate", currentDateTimeMeta);
            writer.WriteElementString("xap:MetadataDate", currentDateTimeMeta);
            writer.WriteElementString("xap:CreatorTool", StiOptions.Export.Pdf.CreatorString);
            writer.WriteFullEndElement();   //rdf:Description

            #region Report info
            writer.WriteStartElement("rdf:Description");
            writer.WriteAttributeString("rdf:about", "");
            writer.WriteAttributeString("xmlns:dc", "http://purl.org/dc/elements/1.1/");
            writer.WriteElementString("dc:format", "application/pdf");

            writer.WriteStartElement("dc:title");
            writer.WriteStartElement("rdf:Alt");
            if (!string.IsNullOrEmpty(report.ReportName))
            {
                writer.WriteStartElement("rdf:li");
                writer.WriteAttributeString("xml:lang", "x-default");
                writer.WriteString(report.ReportName);
                writer.WriteFullEndElement();   //rdf:li
            }
            writer.WriteEndElement();   //rdf:Alt
            writer.WriteFullEndElement();   //dc:title

            writer.WriteStartElement("dc:description");
            writer.WriteStartElement("rdf:Alt");
            if (!string.IsNullOrEmpty(report.ReportAlias))
            {
                writer.WriteStartElement("rdf:li");
                writer.WriteAttributeString("xml:lang", "x-default");
                writer.WriteString(report.ReportAlias);
                writer.WriteFullEndElement();   //rdf:li
            }
            writer.WriteEndElement();   //rdf:Alt
            writer.WriteFullEndElement();   //dc:description

            writer.WriteStartElement("dc:creator");
            writer.WriteStartElement("rdf:Seq");
            if (!string.IsNullOrEmpty(report.ReportAuthor))
            {
                writer.WriteStartElement("rdf:li");
                writer.WriteAttributeString("xml:lang", "x-default");
                writer.WriteString(report.ReportAuthor);
                writer.WriteFullEndElement();   //rdf:li
            }
            writer.WriteEndElement();   //rdf:Seq
            writer.WriteFullEndElement();   //dc:creator

            writer.WriteStartElement("dc:subject");
            writer.WriteStartElement("rdf:Bag");
            if (!string.IsNullOrEmpty(keywords))
            {
                string[] words = keywords.Split(new char[] { ';' });
                foreach (string word in words)
                {
                    writer.WriteStartElement("rdf:li");
                    //writer.WriteAttributeString("xml:lang", "x-default");
                    writer.WriteString(word.Trim());
                    writer.WriteFullEndElement();   //rdf:li
                }
            }
            writer.WriteEndElement();   //rdf:Bag
            writer.WriteFullEndElement();   //dc:subject

            writer.WriteFullEndElement();   //rdf:Description
            #endregion

            writer.WriteStartElement("rdf:Description");
            writer.WriteAttributeString("rdf:about", "");
            writer.WriteAttributeString("xmlns:xapMM", "http://ns.adobe.com/xap/1.0/mm/");
            writer.WriteElementString("xapMM:DocumentID", IDValueStringMeta);
            writer.WriteElementString("xapMM:InstanceID", IDValueStringMeta);
            writer.WriteFullEndElement();   //rdf:Description

            #region Custom metatags
            writer.WriteStartElement("rdf:Description");
            writer.WriteAttributeString("rdf:about", "");
            writer.WriteAttributeString("xmlns:pdf", "http://ns.adobe.com/pdf/1.3/");
            writer.WriteAttributeString("xmlns:pdfx", "http://ns.adobe.com/pdf/1.3/");
            writer.WriteElementString("pdf:Producer", producerName);
            writer.WriteElementString("pdf:Keywords", keywords);

            foreach (StiMetaTag meta in report.MetaTags)
            {
                if (meta.Name.StartsWith("pdf:"))
                {
                    writer.WriteElementString("pdfx:" + meta.Name.Substring(4), meta.Tag);
                }
            }

            writer.WriteFullEndElement();   //rdf:Description
            #endregion

            #region ZUGFeRD
            if (useZUGFeRD)
            {
                #region ZUGFeRD PDFA Extension Schema
                writer.WriteStartElement("rdf:Description");
                writer.WriteAttributeString("xmlns:pdfaExtension", "http://www.aiim.org/pdfa/ns/extension/");
                writer.WriteAttributeString("xmlns:pdfaField", "http://www.aiim.org/pdfa/ns/field#");
                writer.WriteAttributeString("xmlns:pdfaProperty", "http://www.aiim.org/pdfa/ns/property#");
                writer.WriteAttributeString("xmlns:pdfaSchema", "http://www.aiim.org/pdfa/ns/schema#");
                writer.WriteAttributeString("xmlns:pdfaType", "http://www.aiim.org/pdfa/ns/type#");
                writer.WriteAttributeString("rdf:about", "");

                writer.WriteStartElement("pdfaExtension:schemas");

                writer.WriteStartElement("rdf:Bag");

                writer.WriteStartElement("rdf:li");
                writer.WriteAttributeString("rdf:parseType", "Resource");

                writer.WriteElementString("pdfaSchema:schema", "ZUGFeRD PDFA Extension Schema");
                writer.WriteElementString("pdfaSchema:namespaceURI", "urn:ferd:pdfa:CrossIndustryDocument:invoice:1p0#");
                writer.WriteElementString("pdfaSchema:prefix", "zf");

                writer.WriteStartElement("pdfaSchema:property");

                writer.WriteStartElement("rdf:Seq");

                writer.WriteStartElement("rdf:li");
                writer.WriteAttributeString("rdf:parseType", "Resource");
                writer.WriteElementString("pdfaProperty:name", "DocumentFileName");
                writer.WriteElementString("pdfaProperty:valueType", "Text");
                writer.WriteElementString("pdfaProperty:category", "external");
                writer.WriteElementString("pdfaProperty:description", "name of the embedded XML invoice file");
                writer.WriteFullEndElement();   //rdf:li

                writer.WriteStartElement("rdf:li");
                writer.WriteAttributeString("rdf:parseType", "Resource");
                writer.WriteElementString("pdfaProperty:name", "DocumentType");
                writer.WriteElementString("pdfaProperty:valueType", "Text");
                writer.WriteElementString("pdfaProperty:category", "external");
                writer.WriteElementString("pdfaProperty:description", "INVOICE");
                writer.WriteFullEndElement();   //rdf:li

                writer.WriteStartElement("rdf:li");
                writer.WriteAttributeString("rdf:parseType", "Resource");
                writer.WriteElementString("pdfaProperty:name", "Version");
                writer.WriteElementString("pdfaProperty:valueType", "Text");
                writer.WriteElementString("pdfaProperty:category", "external");
                writer.WriteElementString("pdfaProperty:description", "The actual version of the ZUGFeRD data");
                writer.WriteFullEndElement();   //rdf:li

                writer.WriteStartElement("rdf:li");
                writer.WriteAttributeString("rdf:parseType", "Resource");
                writer.WriteElementString("pdfaProperty:name", "ConformanceLevel");
                writer.WriteElementString("pdfaProperty:valueType", "Text");
                writer.WriteElementString("pdfaProperty:category", "external");
                writer.WriteElementString("pdfaProperty:description", "The conformance level of the ZUGFeRD data");
                writer.WriteFullEndElement();   //rdf:li

                writer.WriteFullEndElement();   //rdf:Seq
                writer.WriteFullEndElement();   //pdfaSchema:property
                writer.WriteFullEndElement();   //rdf:li
                writer.WriteFullEndElement();   //rdf:Bag
                writer.WriteFullEndElement();   //pdfaExtension:schemas
                writer.WriteFullEndElement();   //rdf:Description
                #endregion

                writer.WriteStartElement("rdf:Description");
                writer.WriteAttributeString("xmlns:zf", "urn:ferd:pdfa:CrossIndustryDocument:invoice:1p0#");
                writer.WriteAttributeString("zf:ConformanceLevel", "BASIC");
                writer.WriteAttributeString("zf:DocumentFileName", "ZUGFeRD-invoice.xml");
                writer.WriteAttributeString("zf:DocumentType", "INVOICE");
                writer.WriteAttributeString("zf:Version", "1.0");
                writer.WriteAttributeString("rdf:about", "");
                writer.WriteFullEndElement();   //rdf:Description
            }
            #endregion

            #region PDF/A
            if (usePdfA)
            {
                writer.WriteStartElement("rdf:Description");
                writer.WriteAttributeString("rdf:about", "");
                writer.WriteAttributeString("xmlns:pdfaid", "http://www.aiim.org/pdfa/ns/id/");
                string pdfaidPart = "1";
                switch (pdfComplianceMode)
                {
                    case StiPdfComplianceMode.A1: pdfaidPart = "1"; break;
                    case StiPdfComplianceMode.A2: pdfaidPart = "2"; break;
                    case StiPdfComplianceMode.A3: pdfaidPart = "3"; break;
                }
                writer.WriteElementString("pdfaid:part", pdfaidPart);
                writer.WriteElementString("pdfaid:conformance", "A");
                writer.WriteFullEndElement();   //rdf:Description
            }
            #endregion

            writer.WriteFullEndElement();   //rdf:RD
            writer.WriteFullEndElement();   //x:xmpmeta
            writer.WriteRaw("                              \r\n");
            writer.WriteRaw("                              \r\n");
            writer.WriteRaw("                              \r\n");
            writer.WriteRaw("                              \r\n");
            writer.WriteRaw("                              \r\n");
            writer.WriteProcessingInstruction("xpacket", "end=\"w\"");
            #endregion

            writer.Flush();
            byte[] tempBuffer = ms.ToArray();
            ms.Close();
            writer.Close();
            mainStream.Write(tempBuffer, 3, tempBuffer.Length - 3);
            #endregion

            AddXref(info.Metadata.Ref);
            sw.WriteLine("{0} 0 obj", info.Metadata.Ref);
            sw.WriteLine("<<");
            sw.WriteLine("/Type /Metadata");
            sw.WriteLine("/Subtype /XML");

            //sw.WriteLine("/Length {0}", mainStream.Length);
            //sw.WriteLine(">>");
            //sw.WriteLine("stream");
            //StoreMemoryStream(mainStream);
            StoreMemoryStream2(mainStream, "/Length {0}");

            sw.WriteLine("");
            sw.WriteLine("endstream");
            sw.WriteLine("endobj");
            sw.WriteLine("");
        }
        #endregion

        #region Render ColorSpace
        private void RenderColorSpace()
        {
            AddXref(info.DestOutputProfile.Ref);
            sw.WriteLine("{0} 0 obj", info.DestOutputProfile.Ref);
            sw.WriteLine("<<");
            sw.WriteLine("/N 3");
            MemoryStream TmpStream = null;
            string stColorSpace = null;
            if (compressed == true)
            {
                TmpStream = StiExportUtils.MakePdfDeflateStream(sRGBprofile);
                //sw.WriteLine("/Length {0} /Filter [/FlateDecode] /Length1 {1}", TmpStream.Length, sRGBprofile.Length);
                stColorSpace = "/Length {0} /Filter [/FlateDecode] /Length1 " + sRGBprofile.Length.ToString();
            }
            else
            {
                TmpStream = new MemoryStream();
#if TESTPDF
                    byte[] buf = new byte[sRGBprofile.Length * 2];
                    for (int indexb = 0; indexb < sRGBprofile.Length; indexb++)
                    {
                        string st = string.Format("{0:X2}", sRGBprofile[indexb]);
                        buf[indexb * 2 + 0] = (byte)st[0];
                        buf[indexb * 2 + 1] = (byte)st[1];
                    }
                    TmpStream.Write(buf, 0, buf.Length);
                    //sw.WriteLine("/Length {0}", sRGBprofile.Length * 2);
                    //sw.WriteLine("/Filter [/ASCIIHexDecode]");
                    stColorSpace = "/Length {0} /Filter [/ASCIIHexDecode]";
#else
                TmpStream.Write(sRGBprofile, 0, sRGBprofile.Length);
                //sw.WriteLine("/Length {0}", sRGBprofile.Length);
                stColorSpace = "/Length {0}";
#endif
            }
            //sw.WriteLine(">>");
            //sw.WriteLine("stream");
            //StoreMemoryStream(TmpStream);
            StoreMemoryStream2(TmpStream, stColorSpace);

            sw.WriteLine("");
            sw.WriteLine("endstream");
            sw.WriteLine("endobj");
            sw.WriteLine("");


            AddXref(info.OutputIntents.Ref);
            sw.WriteLine("{0} 0 obj", info.OutputIntents.Ref);
            sw.WriteLine("[<<");

            //sw.WriteLine("/Info(sRGB IEC61966-2.1)");
            //sw.WriteLine("/OutputConditionIdentifier(Custom)");
            //sw.WriteLine("/OutputCondition()");
            //sw.WriteLine("/RegistryName()");
            StoreStringLine("/Info", "sRGB IEC61966-2.1");
            StoreStringLine("/OutputConditionIdentifier", "Custom");
            StoreStringLine("/OutputCondition", string.Empty);
            StoreStringLine("/RegistryName", string.Empty);

            sw.WriteLine("/S /GTS_PDFA1");
            sw.WriteLine("/DestOutputProfile {0} 0 R", info.DestOutputProfile.Ref);
            sw.WriteLine("/Type /OutputIntent");
            sw.WriteLine(">>]");
            sw.WriteLine("endobj");
            sw.WriteLine("");
        }
        #endregion

        #region Render AutoPrint
        private void RenderAutoPrint()
        {
            if (autoPrint != StiPdfAutoPrintMode.None)
            {
                AddXref(info.EmbeddedJS.Ref);
                sw.WriteLine("{0} 0 obj", info.EmbeddedJS.Ref);
                sw.WriteLine("<<");
                sw.WriteLine("/Names [(EmbeddedJS) {0} 0 R]", info.EmbeddedJS.Content.Ref);
                sw.WriteLine(">>");
                sw.WriteLine("endobj");
                sw.WriteLine("");

                AddXref(info.EmbeddedJS.Content.Ref);
                sw.WriteLine("{0} 0 obj", info.EmbeddedJS.Content.Ref);
                sw.WriteLine("<<");
                sw.WriteLine("/S /JavaScript");
                sw.WriteLine("/JS (print\\({0}\\);)", autoPrint == StiPdfAutoPrintMode.Dialog ? "true" : "false");
                //if dialog
                //sw.WriteLine("/JS (var pp = getPrintParams\\(\\); pp.printerName = '\\\\\\\\serverName\\\\printerName'; pp.interactive = pp.constants.interactionLevel.full; print\\(pp\\);)");
                //else
                //sw.WriteLine("/JS (var pp = getPrintParams\\(\\); pp.printerName = '\\\\\\\\serverName\\\\printerName'; pp.interactive = pp.constants.interactionLevel.automatic; print\\(pp\\);)");
                sw.WriteLine(">>");
                sw.WriteLine("endobj");
                sw.WriteLine("");
            }
        }
        #endregion

        #region Render EmbeddedFiles
        private void RenderEmbeddedFiles()
        {
            for (int index = 0; index < info.EmbeddedFilesList.Count; index++)
            {
                var file = embeddedFiles[index];

                AddXref(info.EmbeddedFilesList[index].Ref);
                sw.WriteLine("{0} 0 obj", info.EmbeddedFilesList[index].Ref);
                sw.WriteLine("<<");
                StoreStringLine("/F", file.Name);
                sw.WriteLine("/Type /Filespec");
                sw.WriteLine("/EF <<");
                sw.WriteLine("/F {0} 0 R", info.EmbeddedFilesList[index].Content.Ref);
                sw.WriteLine("/UF {0} 0 R", info.EmbeddedFilesList[index].Content.Ref);
                sw.WriteLine(">>");
                if (useZUGFeRD)
                {
                    sw.WriteLine("/AFRelationship /Alternative");
                }
                StoreStringLine("/UF", file.Name);
                StoreStringLine("/Desc", file.Description);
                sw.WriteLine(">>");
                sw.WriteLine("endobj");
                sw.WriteLine("");

                AddXref(info.EmbeddedFilesList[index].Content.Ref);
                sw.WriteLine("{0} 0 obj", info.EmbeddedFilesList[index].Content.Ref);
                sw.WriteLine("<<");
                sw.WriteLine("/Subtype /{0}", file.MIMEType);
                sw.WriteLine("/Type /EmbeddedFile");
                sw.WriteLine("/Params <<");
                StoreStringLine("/ModDate ", "D:" + currentDateTime);
                sw.WriteLine("/Size {0}", file.Data.Length);
                sw.WriteLine(">>");
                if (compressed == true)
                {
                    MemoryStream TmpStream = StiExportUtils.MakePdfDeflateStream(file.Data);
                    StoreMemoryStream2(TmpStream, "/Filter [/FlateDecode] /Length {0}");
                }
                else
                {
                    StoreMemoryStream2(file.Data, "/Length {0}");
                }
                sw.WriteLine("");
                sw.WriteLine("endstream");
                sw.WriteLine("endobj");
                sw.WriteLine("");
            }
        }
        #endregion

        #endregion

        #region StiBookmarkTreeNode
        private class StiBookmarkTreeNode
        {
            public int Parent;
            public int First;
            public int Last;
            public int Prev;
            public int Next;
            public int Count;
            public int Page;
            public double Y;
            public string Title;
            public string Guid;
            public bool Used;
        }
        #endregion

        #region AddBookmarkNode
        private void AddBookmarkNode(StiBookmark bkm, int parentNode)
        {
            StiBookmarkTreeNode tn = new StiBookmarkTreeNode();
            tn.Parent = -1;
            tn.First = -1;
            tn.Last = -1;
            tn.Prev = -1;
            tn.Next = -1;
            tn.Count = -1;
            tn.Page = -1;
            tn.Y = -1;
            tn.Title = string.Empty;
            tn.Guid = string.Empty;
            tn.Used = (parentNode == -1 ? true : false);
            bookmarksTree.Add(tn);
            int currentNode = bookmarksTree.Count - 1;

            tn.Parent = parentNode;
            tn.Title = bkm.Text;
            tn.Guid = bkm.ComponentGuid;
            if (bkm.Bookmarks.Count == 0)
            {
                tn.Count = 0;
            }
            else
            {
                int prevNode = -1;
                for (int tempCount = 0; tempCount <= bkm.Bookmarks.Count - 1; tempCount++)
                {
                    int memNode = bookmarksTree.Count;
                    StiBookmark sbm = bkm.Bookmarks[tempCount];
                    AddBookmarkNode(sbm, currentNode);
                    StiBookmarkTreeNode tempBM = (StiBookmarkTreeNode)bookmarksTree[memNode];
                    if (tempCount < bkm.Bookmarks.Count - 1)
                    {
                        tempBM.Next = bookmarksTree.Count;
                    }
                    if (tempCount > 0)
                    {
                        tempBM.Prev = prevNode;
                    }
                    bookmarksTree[memNode] = tempBM;
                    prevNode = memNode;
                }
                tn.First = currentNode + 1;
                tn.Last = bookmarksTree.Count - 1;
                tn.Count = bookmarksTree.Count - currentNode - 1;
            }
            bookmarksTree[currentNode] = tn;
        }
        #endregion

        #region MakeBookmarkFromTree
        private void MakeBookmarkFromTree(StiBookmark bookmark, StiBookmarkTreeNode treeNode)
        {
            bookmark.Text = treeNode.Title;
            bookmark.Bookmarks = new StiBookmarksCollection();
            if (treeNode.Count > 0)
            {
                int nodePos = treeNode.First;
                while (nodePos != -1)
                {
                    StiBookmarkTreeNode tnChild = (StiBookmarkTreeNode)bookmarksTree[nodePos];
                    if (tnChild.Used)
                    {
                        StiBookmark childBookmark = new StiBookmark();
                        bookmark.Bookmarks.Add(childBookmark);
                        MakeBookmarkFromTree(childBookmark, tnChild);
                    }
                    nodePos = tnChild.Next;
                }
            }
        }
        #endregion

        #region StiLinkObject
        private struct StiLinkObject
        {
            public double X;
            public double Y;
            public double Width;
            public double Height;
            public int Page;
            public int DestPage;
            public double DestY;
            public string Link;
        }
        #endregion

        #region StiEditableObject
        private class StiEditableObject
        {
            public double X;
            public double Y;
            public double Width;
            public double Height;
            public int Page;
            public string Text;
            public byte[] Content;
            public byte[] Content2;
            public bool Multiline;
            public StiTextHorAlignment Alignment;
            public int FontNumber;
            public double FontSize;
            public Color FontColor;
            public StiComponent Component;
        }
        #endregion

        #region Wpf text event
        public static event Events.StiSplitTextIntoLinesEventHandler SplitTextIntoLinesEvent;

        private static object lockSplitTextIntoLinesEventFlag = new object();

        protected virtual string OnSplitTextIntoLines(Stimulsoft.Report.Events.StiSplitTextIntoLinesEventArgs e)
        {
            if (SplitTextIntoLinesEvent != null)
            {
                return SplitTextIntoLinesEvent(this, e);
            }
            return null;
        }

        private void PreparePdfTextHelper()
        {
            isWpf = false;
            if (isWpfException) return;
            try
            {
                if (SplitTextIntoLinesEvent == null)
                {
                    lock (lockSplitTextIntoLinesEventFlag)
                    {
                        object stiPdfTextHelper = StiActivator.CreateObject("Stimulsoft.Report.Wpf.Helpers.StiPdfTextHelper, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo, new object[0]);
                    }
                }
                if (SplitTextIntoLinesEvent != null)
                {
                    isWpf = true;
                }
            }
            catch
            {
                isWpf = false;
                isWpfException = true;
            }
        }
        #endregion

        public void ClearFontsCache()
        {
            PdfFonts.ClearFontsCache();
        }

        #region Main Export Class
        /// <summary>
        /// Exports a document to the pdf file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="fileName">A name of the file for exporting a rendered report.</param>
        public void ExportPdf(StiReport report, string fileName)
        {
            StiFileUtils.ProcessReadOnly(fileName);
            FileStream stream = new FileStream(fileName, FileMode.Create);
            ExportPdf(report, stream);
            stream.Flush();
            stream.Close();
        }


        /// <summary>
        /// Exports a document to the pdf file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        public void ExportPdf(StiReport report, Stream stream)
        {
            StiPdfExportSettings settings = new StiPdfExportSettings();
            ExportPdf(report, stream, settings);
        }


        /// <summary>
        /// Exports a document to the pdf file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        /// <param name="pageRange">Describes range of pages of the document for the export.</param>
        public void ExportPdf(StiReport report, Stream stream, StiPagesRange pageRange)
        {
            StiPdfExportSettings settings = new StiPdfExportSettings();

            settings.PageRange = pageRange;

            ExportPdf(report, stream, settings);
        }


        /// <summary>
        /// Exports a document to the pdf file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        /// <param name="pageRange">Describes range of pages of the document for the export.</param>
        /// <param name="imageQuality">A float value that sets the quality of exporting images. Default value is 1.</param>
        /// <param name="imageResolution">A float value that sets the resolution of exporting images. Default value is 100.</param>
        public void ExportPdf(StiReport report, Stream stream, StiPagesRange pageRange,
            float imageQuality, float imageResolution)
        {
            StiPdfExportSettings settings = new StiPdfExportSettings();

            settings.PageRange = pageRange;
            settings.ImageQuality = imageQuality;
            settings.ImageResolution = imageResolution;

            ExportPdf(report, stream, settings);
        }


        /// <summary>
        /// Exports a document to the pdf file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream for export of a document.</param>
        /// <param name="pageRange">Describes pages range of the document for the export.</param>
        /// <param name="imageQuality">A float value that sets the quality of exporting images. Default value is 1.</param>
        /// <param name="imageResolution">A float value that sets the resolution of exporting images. Default value is 100.</param>
        /// <param name="embeddedFonts">If embeddedFont is true then, when exporting, fonts of the report will be included in the resulting document.</param>
        /// <param name="standardPdfFonts">If standardPdfFont is true then, when exporting, non-standard fonts of the report will be replaced by the standard fonts in resulting document.</param>
        /// <param name="compressed">A parameter which controls a compression of the exported pdf document.</param>
        public void ExportPdf(StiReport report, Stream stream, StiPagesRange pageRange,
            float imageQuality, float imageResolution, bool embeddedFonts, bool standardPdfFonts, bool compressed)
        {
            StiPdfExportSettings settings = new StiPdfExportSettings();

            settings.PageRange = pageRange;
            settings.ImageQuality = imageQuality;
            settings.ImageResolution = imageResolution;
            settings.EmbeddedFonts = embeddedFonts;
            settings.StandardPdfFonts = standardPdfFonts;
            settings.Compressed = compressed;

            ExportPdf(report, stream, settings);
        }


        /// <summary>
        /// Exports a document to the pdf file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream for export of a document.</param>
        /// <param name="pageRange">Describes range of pages of the document for the export.</param>
        /// <param name="imageQuality">A float value that sets the quality of exporting images. Default value is 1.</param>
        /// <param name="imageResolution">A float value that sets the resolution of exporting images. Default value is 100.</param>
        /// <param name="embeddedFonts">If embeddedFont is true then, when exporting, fonts of the report will be included in the resulting document.</param>
        /// <param name="standardPdfFonts">If standardPdfFont is true then, when exporting, non-standard fonts of the report will be replaced by the standard fonts in resulting document.</param>
        /// <param name="compressed">A parameter which controls a compression of the exported pdf document.</param>
        /// <param name="passwordInputUser">An user password for the exported pdf file which enables access to content of the document
        /// in according with the privileges from the userAccesPrivileges parameter.</param>
        /// <param name="passwordInputOwner">An owner password which supplies full control for the content of the exported pdf file.</param>
        /// <param name="userAccessPrivileges">A parameter which controls access privileges for the user.</param>
        public void ExportPdf(StiReport report, Stream stream, StiPagesRange pageRange,
            float imageQuality, float imageResolution, bool embeddedFonts, bool standardPdfFonts,
            bool compressed, string passwordInputUser, string passwordInputOwner,
            StiUserAccessPrivileges userAccessPrivileges)
        {
            StiPdfExportSettings settings = new StiPdfExportSettings();

            settings.PageRange = pageRange;
            settings.ImageQuality = imageQuality;
            settings.ImageResolution = imageResolution;
            settings.EmbeddedFonts = embeddedFonts;
            settings.StandardPdfFonts = standardPdfFonts;
            settings.Compressed = compressed;
            settings.PasswordInputUser = passwordInputUser;
            settings.PasswordInputOwner = passwordInputOwner;
            settings.UserAccessPrivileges = userAccessPrivileges;

            ExportPdf(report, stream, settings);
        }


        /// <summary>
        /// Exports a rendered report to the pdf file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        /// <param name="pageRange">Describes range of pages of the document for the export.</param>
        /// <param name="imageQuality">A float value that sets the quality of exporting images. Default value is 1.</param>
        /// <param name="imageResolution">A float value that sets the resolution of exporting images. Default value is 100.</param>
        /// <param name="embeddedFonts">If the embeddedFont is true then, when exporting, fonts of the report will be included in the resulting document.</param>
        /// <param name="standardPdfFonts">If standardPdfFont is true then, when exporting, non-standard fonts of the report will be replaced by the standard fonts in resulting document.</param>
        /// <param name="compressed">A parameter which controls a compression of the exported pdf document.</param>
        /// <param name="passwordInputUser">An user password for the exported pdf file which enables access to content of the document
        /// in according with the privileges from the userAccesPrivileges parameter.</param>
        /// <param name="passwordInputOwner">An owner password which supplies full control for the content of the exported pdf file.</param>
        /// <param name="userAccessPrivileges">A parameter which controls access privileges for the user.</param>
        /// <param name="keyLength">A parameter for setting an encryption key lenght of the resulting pdf file.</param>
        public void ExportPdf(StiReport report, Stream stream, StiPagesRange pageRange,
            float imageQuality, float imageResolution, bool embeddedFonts, bool standardPdfFonts,
            bool compressed, string passwordInputUser, string passwordInputOwner, StiUserAccessPrivileges userAccessPrivileges,
            StiPdfEncryptionKeyLength keyLength)
        {
            StiPdfExportSettings settings = new StiPdfExportSettings();

            settings.PageRange = pageRange;
            settings.ImageQuality = imageQuality;
            settings.ImageResolution = imageResolution;
            settings.EmbeddedFonts = embeddedFonts;
            settings.StandardPdfFonts = standardPdfFonts;
            settings.Compressed = compressed;
            settings.PasswordInputUser = passwordInputUser;
            settings.PasswordInputOwner = passwordInputOwner;
            settings.UserAccessPrivileges = userAccessPrivileges;
            settings.KeyLength = keyLength;

            ExportPdf(report, stream, settings);
        }


        /// <summary>
        /// Exports a document to the pdf file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        /// <param name="pageRange">Describes range of pages of the document for the export.</param>
        /// <param name="imageQuality">A float value that sets the quality of exporting images. Default value is 1.</param>
        /// <param name="imageResolution">A float value that sets the resolution of exporting images. Default value is 100.</param>
        /// <param name="embeddedFonts">If embeddedFont is true then, when exporting, fonts of the report will be included in the resulting document.</param>
        /// <param name="standardPdfFonts">If standardPdfFont is true then, when exporting, non-standard fonts of the report will be replaced by the standard fonts in resulting document.</param>
        /// <param name="compressed">A parameter which controls a compression of the exported pdf document.</param>
        /// <param name="exportRtfTextAsImage">If true then the rendered report will be exported as one image.</param>
        /// <param name="passwordInputUser">A user password for the exported Pdf file which enables access to content of the document
        /// in according with the privileges from the userAccesPrivileges parameter.</param>
        /// <param name="passwordInputOwner">An owner password which supplies full control for the content of the exported pdf file.</param>
        /// <param name="userAccessPrivileges">A parameter which controls access privileges for the user.</param>
        /// <param name="keyLength">A parameter for setting an encryption key length of the resulting pdf file.</param>
        public void ExportPdf(StiReport report, Stream stream, StiPagesRange pageRange,
            float imageQuality, float imageResolution, bool embeddedFonts, bool standardPdfFonts,
            bool compressed, bool exportRtfTextAsImage,
            string passwordInputUser, string passwordInputOwner, StiUserAccessPrivileges userAccessPrivileges,
            StiPdfEncryptionKeyLength keyLength)
        {
            StiPdfExportSettings settings = new StiPdfExportSettings();

            settings.PageRange = pageRange;
            settings.ImageQuality = imageQuality;
            settings.ImageResolution = imageResolution;
            settings.EmbeddedFonts = embeddedFonts;
            settings.StandardPdfFonts = standardPdfFonts;
            settings.Compressed = compressed;
            settings.ExportRtfTextAsImage = exportRtfTextAsImage;
            settings.PasswordInputUser = passwordInputUser;
            settings.PasswordInputOwner = passwordInputOwner;
            settings.UserAccessPrivileges = userAccessPrivileges;
            settings.KeyLength = keyLength;

            ExportPdf(report, stream, settings);
        }


        public void ExportPdf(StiReport report, Stream stream, StiPagesRange pageRange,
            float imageQuality, float imageResolution, bool embeddedFonts, bool standardPdfFonts,
            bool compressed, bool exportRtfTextAsImage,
            string passwordInputUser, string passwordInputOwner, StiUserAccessPrivileges userAccessPrivileges,
            StiPdfEncryptionKeyLength keyLength, bool useUnicode)
        {
            StiPdfExportSettings settings = new StiPdfExportSettings();

            settings.PageRange = pageRange;
            settings.ImageQuality = imageQuality;
            settings.ImageResolution = imageResolution;
            settings.EmbeddedFonts = embeddedFonts;
            settings.StandardPdfFonts = standardPdfFonts;
            settings.Compressed = compressed;
            settings.ExportRtfTextAsImage = exportRtfTextAsImage;
            settings.PasswordInputUser = passwordInputUser;
            settings.PasswordInputOwner = passwordInputOwner;
            settings.UserAccessPrivileges = userAccessPrivileges;
            settings.KeyLength = keyLength;
            settings.UseUnicode = useUnicode;

            ExportPdf(report, stream, settings);
        }


        public void ExportPdf(StiReport report, Stream stream, StiPdfExportSettings settings)
        {
            try
            {
                //StiExportUtils.DisableFontSmoothing();
                ExportPdf1(report, stream, settings);
            }
            finally
            {
                StiExportUtils.EnableFontSmoothing(report);
            }
        }

        /// <summary>
        /// Exports a rendered report to the pdf file.
        /// </summary>
        /// <param name="report">A report which is to be exported.</param>
        /// <param name="stream">A stream for the export of a document.</param>
        /// <param name="settings">Pdf export settings.</param>
		private void ExportPdf1(StiReport report, Stream stream, StiPdfExportSettings settings)
        {
            StiLogService.Write(this.GetType(), "Export report to Pdf format");
#if NETCORE
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
#endif

            #region Read settings
            if (settings == null)
                throw new ArgumentNullException("The 'settings' argument cannot be equal in null.");

            StiPagesRange pageRange = settings.PageRange;
            float imageQuality = settings.ImageQuality;
            float imageResolution = settings.ImageResolution;
            this.imageResolutionMode = settings.ImageResolutionMode;
            bool embeddedFonts = settings.EmbeddedFonts;
            bool standardPdfFonts = settings.StandardPdfFonts;
            bool compressed = settings.Compressed;
            bool exportRtfTextAsImage = settings.ExportRtfTextAsImage;
            string passwordInputUser = settings.PasswordInputUser;
            string passwordInputOwner = settings.PasswordInputOwner;
            StiUserAccessPrivileges userAccessPrivileges = settings.UserAccessPrivileges;
            this.keyLength = settings.KeyLength;
            this.useUnicodeMode = settings.UseUnicode;
            this.pdfComplianceMode = settings.PdfComplianceMode;
            bool getCertificateFromCryptoUI = settings.GetCertificateFromCryptoUI;
            bool useDigitalSignature = settings.UseDigitalSignature;
            string subjectNameString = settings.SubjectNameString;
            bool useLocalMachineCertificates = settings.UseLocalMachineCertificates;
            this.imageFormat = settings.ImageFormat;
            this.monochromeDitheringType = settings.DitheringType;
            this.autoPrint = settings.AutoPrintMode;
            StiPdfImageCompressionMethod imageCompressionMethod = settings.ImageCompressionMethod;
            byte[] certificateData = settings.CertificateData;
            string certificatePassword = settings.CertificatePassword;
            this.allowEditable = settings.AllowEditable;
            this.digitalSignatureReason = settings.DigitalSignatureReason;
            this.digitalSignatureLocation = settings.DigitalSignatureLocation;
            this.digitalSignatureContactInfo = settings.DigitalSignatureContactInfo;
            this.digitalSignatureSignedBy = settings.DigitalSignatureSignedBy;
            this.embeddedFiles = settings.EmbeddedFiles;
            this.useZUGFeRD = settings.ZUGFeRDCompliance;

            creatorName = StiOptions.Export.Pdf.CreatorString;
            keywords = StiOptions.Export.Pdf.KeywordsString;
            if (!string.IsNullOrEmpty(settings.CreatorString))
            {
                creatorName = settings.CreatorString;
            }
            if (!string.IsNullOrEmpty(settings.KeywordsString))
            {
                keywords = settings.KeywordsString;
            }
            if (string.IsNullOrEmpty(creatorName))
            {
                creatorName = producerName;
            }
            #endregion

            if (useZUGFeRD) pdfComplianceMode = StiPdfComplianceMode.A3;

            this.usePdfA = pdfComplianceMode != StiPdfComplianceMode.None;
            this.useTransparency = pdfComplianceMode != StiPdfComplianceMode.A1;

            this.report = report;

            compressedFonts = true;
            if (usePdfA)
            {
                standardPdfFonts = false;
                embeddedFonts = true;
                this.useUnicodeMode = true;
            }

            bool deleteEmptyBookmarks = (pageRange.RangeType != StiRangeType.All);

            this.reduceFontSize = StiOptions.Export.Pdf.ReduceFontFileSize;
            bool useImageComparer = StiOptions.Export.Pdf.AllowImageComparer;
            bool useImageTransparency = StiOptions.Export.Pdf.AllowImageTransparency && useTransparency;

#pragma warning disable 612, 618
            if (StiOptions.Export.Pdf.AllowEditablePdf)
#pragma warning restore 612, 618
            {
                allowEditable = StiPdfAllowEditable.Yes;
            }

            //if (report.RenderedPages.CacheMode)StatusString = StiLocalization.Get("Export", "ExportingCreatingDocument") + " 1";
            //else StatusString = StiLocalization.Get("Export", "ExportingCreatingDocument");
            StatusString = StiLocalization.Get("Report", "PreparingReport");

            if (embeddedFonts) standardPdfFonts = false;
            bool clipText = true;
            if (imageQuality < 0) imageQuality = 0;
            if (imageQuality > 1) imageQuality = 1;
            if (imageResolution < 10) imageResolution = 10;
            if (useUnicodeMode) standardPdfFonts = false;

            bool fullTrust = true;
            if (!StiOptions.Engine.FullTrust)
            {
                embeddedFonts = false;
                //standardPdfFonts = true;
                useUnicodeMode = false;
                useDigitalSignature = false;
                fullTrust = false;
            }

            if (!StiOptions.Export.Pdf.AllowImportSystemLibraries)
            {
                standardPdfFonts = false;
                useDigitalSignature = false;
                embeddedFonts = false;
                useUnicodeMode = false;
            }

            this.imageQuality = imageQuality;
            this.embeddedFonts = embeddedFonts;
            this.standardPdfFonts = standardPdfFonts;
            this.compressed = compressed;
            this.exportRtfTextAsImage = exportRtfTextAsImage;

            if (imageFormat == StiImageFormat.Monochrome) imageCompressionMethod = StiPdfImageCompressionMethod.Flate;
            this.imageCompressionMethod = imageCompressionMethod;
            imageResolution = imageResolution / 100;
            imageResolutionMain = imageResolution;
            this.haveDigitalSignature = false;

            this.isWpf = false;
            if (report.IsWpf || report.RenderedWith == StiRenderedWith.Wpf)
            {
                PreparePdfTextHelper();
            }

            #region Initialization
            bidi = new StiBidirectionalConvert(StiBidirectionalConvert.Mode.Pdf);
            pdfFont = new PdfFonts();
            pdfFont.standardPdfFonts = standardPdfFonts;
            pdfFont.useUnicode = useUnicodeMode;
            fontGlyphsReduceNotNeed = null;

            precision_digits = StiOptions.Export.Pdf.DefaultCoordinatesPrecision;
            clipLongTextLines = clipText;

            //prepare procedure ConvertToString()
            currentCulture = CultureInfo.CurrentCulture;
            currentNumberFormat = (NumberFormatInfo)currentCulture.NumberFormat.Clone();
            currentNumberFormat.NumberDecimalSeparator = ".";

            //prepare color convert table
            int colorDigits = 3;
            for (int indexColor = 0; indexColor <= 255; indexColor++)
            {
                double doubleColor = Math.Round((double)((double)indexColor / 255), colorDigits);
                string stringColor = doubleColor.ToString("G", currentNumberFormat);
                colorTable[indexColor] = stringColor;
                alphaTable[indexColor] = false;
            }
            lastColorStrokeA = 0xFF;
            lastColorNonStrokeA = 0xFF;
            colorStack = new Stack(32);

            imageList = new ArrayList();
            switch (imageCompressionMethod)
            {
                case StiPdfImageCompressionMethod.Flate:
                    imageCache = new StiImageCache(useImageComparer, ImageFormat.MemoryBmp, imageQuality, useImageTransparency);
                    break;

                default:
                    imageCache = new StiImageCache(useImageComparer, ImageFormat.Jpeg, imageQuality, useImageTransparency);
                    break;
            }
            imageInterpolationTable = new Hashtable();
            imageCacheIndexToList = new Hashtable();
            imageInfoList = new Hashtable();
            imageInfoCounter = 0;

            pdfFont.fontList = new ArrayList();
            xref = new SortedList();
            bookmarksTree = new ArrayList();
            haveBookmarks = false;
            linksArray = new ArrayList();
            annotsArray = new ArrayList();
            annots2Array = new List<StiEditableObject>();
            unsignedSignaturesArray = new List<StiEditableObject>();
            shadingArray = new ArrayList();
            hatchArray = new ArrayList();
            tooltipsArray = new List<StiLinkObject>();

            sw = new StreamWriter(stream, Encoding.GetEncoding(1252));
            stream2 = stream;

            enc = global::System.Text.Encoding.GetEncoding(1252).GetEncoder();

            //init all counters
            imagesCounter = 0;
            fontsCounter = 0;
            bookmarksCounter = 0;
            //patternsCounter = 0;
            linksCounter = 0;
            annotsCounter = 0;
            annots2Counter = 0;
            shadingCounter = 0;
            hatchCounter = 0;
            tooltipsCounter = 0;

            //prepare codepage1252 info
            for (int index = 0; index < 256; index++)
            {
                CodePage1252[index] = index;
            }
            for (int index = 0; index < 32; index++)
            {
                CodePage1252[0x80 + index] = CodePage1252part80AF[index];
            }

            fontGlyphsReduceNotNeed = new bool[256];

            mfRender = new Stimulsoft.Report.Export.Tools.StiPdfMetafileRender(this, useUnicodeMode);

            #endregion

            #region Make ID string
            DateTime dt = DateTime.Now;
            string tempDT = dt.ToString("yyyyMMddHHmmsszzz");
            currentDateTime = tempDT.Substring(0, 17) + "'" + tempDT.Substring(18, 2) + "'";
            currentDateTimeMeta = dt.ToString("yyyy-MM-ddTHH:mm:sszzz");

            StringBuilder IDString = new StringBuilder();
            IDString.Append(dt.ToString("yyyyMMddHHmmssffff"));
            IDString.Append(producerName);
            IDString.Append(creatorName);
            IDString.Append(report.ReportAuthor);
            IDString.Append(report.ReportAlias);
            IDString.Append(report.ReportName);

            byte[] forHash = new byte[IDString.Length];
            for (int index = 0; index < IDString.Length; index++)
            {
                forHash[index] = (byte)IDString[index];
            }

            //IDValue = new byte[16];
            //Random rnd = new Random();
            //rnd.NextBytes(IDValue);
            IDValue = StiMD5Helper.ComputeHash(forHash);

            StringBuilder tempSB = new StringBuilder();
            for (int index = 0; index < IDValue.Length; index++)
            {
                tempSB.Append(IDValue[index].ToString("X2"));
            }
            IDValueString = tempSB.ToString();
            IDValueStringMeta = string.Format("uuid:{0}-{1}-{2}-{3}-{4}",
                IDValueString.Substring(0, 8),
                IDValueString.Substring(8, 4),
                IDValueString.Substring(12, 4),
                IDValueString.Substring(16, 4),
                IDValueString.Substring(20, 12)).ToLowerInvariant();
            #endregion

            pdfSecurity = new Tools.StiPdfSecurity(this);
            if (!usePdfA && fullTrust)
            {
                encrypted = pdfSecurity.ComputingCryptoValues(userAccessPrivileges, passwordInputOwner, passwordInputUser, keyLength, IDValue);
            }

            if (encrypted && useUnicodeMode)
            {
                embeddedFonts = true;
                this.embeddedFonts = embeddedFonts;
            }

            CurrentPassNumber = 0;
            MaximumPassNumber = (StiOptions.Export.Pdf.DivideSegmentPages ? 3 : 2);

            StiPagesCollection pages = pageRange.GetSelectedPages(report.RenderedPages);
            if (StiOptions.Export.Pdf.DivideSegmentPages)
            {
                pages = StiSegmentPagesDivider.Divide(pages, this);
                CurrentPassNumber++;
            }

            #region Make bookmarks table
            if ((report.Bookmark != null) && (report.Bookmark.Bookmarks.Count != 0))
            {
                AddBookmarkNode(report.Bookmark, -1);
                haveBookmarks = true;
                bookmarksCounter = bookmarksTree.Count;
            }
            #endregion

            StatusString = StiLocalization.Get("Export", "ExportingFormatingObjects");

            #region Scan for assemble data
            int tempPageNumber = 0;
            foreach (StiPage page in pages)
            {
                pages.GetPage(page);
                InvokeExporting(tempPageNumber, pages.Count, CurrentPassNumber, MaximumPassNumber);

                StoreShadingData1(page.Brush, tempPageNumber);
                StoreHatchData(page.Brush);

                if ((page.HyperlinkValue != null) && (page.HyperlinkValue.ToString().Trim().Length > 0) && (!page.HyperlinkValue.ToString().Trim().StartsWith("javascript:")) && !usePdfA)
                {
                    StiLinkObject stl = new StiLinkObject();
                    stl.Link = page.HyperlinkValue.ToString();
                    stl.Page = tempPageNumber;
                    linksArray.Add(stl);
                }

                if ((page.Watermark != null) && (page.Watermark.Enabled))
                {
                    if (!string.IsNullOrEmpty(page.Watermark.Text))
                    {
                        if (page.Watermark.Font != null)
                        {
                            int fnt = pdfFont.GetFontNumber(page.Watermark.Font);
                        }
                        StringBuilder sb = new StringBuilder(page.Watermark.Text);
                        sb = bidi.Convert(sb, false);
                        pdfFont.StoreUnicodeSymbolsInMap(sb);

                        StoreShadingData1(page.Watermark.TextBrush, tempPageNumber);
                        StoreHatchData(page.Watermark.TextBrush);
                    }

                    if (page.Watermark.ShowImageBehind == true)
                    {
                        var image = page.Watermark.GetImage(page.Report);
                        if (image != null)
                        {
                            using (var gdiImage = StiImageConverter.BytesToImage(image))
                            {
                                if (gdiImage is Metafile)
                                    StoreWatermarkMetafileData(gdiImage, imageResolution, page);
                                else
                                    StoreImageData(gdiImage, imageResolution, false, false);
                            }
                        }
                    }
                }

                foreach (StiComponent component in page.Components)
                {
                    if (component.Enabled && !(report.IsPrinting && !component.Printable))
                    {
                        imageInfoCounter++;

                        #region render components
                        bool needPaint = (component.Width > 0) && (component.Height > 0);
                        if (needPaint)
                        {
                            IStiBrush mBrush = component as IStiBrush;
                            if (mBrush != null)
                            {
                                StoreShadingData1(mBrush.Brush, tempPageNumber);
                                StoreHatchData(mBrush.Brush);
                            }

                            if (component is StiCheckBox)
                            {
                                StiCheckBox checkBox = component as StiCheckBox;
                                StoreShadingData1(checkBox.TextBrush, tempPageNumber);
                                StoreHatchData(checkBox.TextBrush);

                                if ((allowEditable == StiPdfAllowEditable.Yes) && ((IStiEditable)checkBox).Editable)
                                {
                                    StiEditableObject seo = new StiEditableObject();
                                    seo.Page = tempPageNumber;
                                    seo.Component = component;
                                    annots2Array.Add(seo);
                                }
                                continue;
                            }

                            bool isExportAsImage = component.IsExportAsImage(StiExportFormat.Pdf);

                            StiShape shape = component as StiShape;
                            if ((shape != null) && (CheckShape(shape)))
                            {
                                isExportAsImage = false;
                            }

                            if ((exportRtfTextAsImage || !(component is StiRichText)) && isExportAsImage)
                            {
                                bool flagIsImage = false;
                                StiImage imageTemp = component as StiImage;
                                if ((imageResolutionMode != StiImageResolutionMode.Exactly) && (imageTemp != null) && imageTemp.ExistImageToDraw() && !imageTemp.ImageToDrawIsMetafile())
                                {
                                    using (var gdiImage = imageTemp.TakeGdiImageToDraw(imageResolutionMain))
                                    {
                                        if (gdiImage != null)
                                        {
                                            flagIsImage = true;

                                            if (imageTemp.ImageRotation != StiImageRotation.None)
                                            {
                                                Helpers.StiImageHelper.RotateImage(gdiImage, imageTemp.ImageRotation, true);
                                            }

                                            float rsImageResolution = gdiImage.HorizontalResolution / 100;
                                            if (imageResolutionMode == StiImageResolutionMode.NoMoreThan)
                                            {
                                                if (imageTemp.Stretch)
                                                {
                                                    rsImageResolution = (float) (gdiImage.Width / report.Unit.ConvertToHInches(component.Width));
                                                }
                                                else
                                                {
                                                    rsImageResolution = (float) (1 / imageTemp.MultipleFactor);
                                                }
                                            }
                                            rsImageResolution = StoreImageData(gdiImage, rsImageResolution, true, imageTemp.Smoothing);
                                            imageInfoList[imageInfoCounter] = rsImageResolution;
                                        }
                                    }
                                }
                                if (!flagIsImage)
                                {
                                    IStiExportImageExtended exportImage = component as IStiExportImageExtended;
                                    if (exportImage != null)
                                    {
                                        float rsImageResolution = imageResolution;
                                        using (Image image = exportImage.GetImage(ref rsImageResolution, StiExportFormat.Pdf))
                                        {
                                            if (image != null)
                                            {
                                                //imagesCounter++;
                                                StoreImageData(image, rsImageResolution, false, (imageTemp != null) && imageTemp.Smoothing);
                                                imageInfoList[imageInfoCounter] = rsImageResolution;
                                            }
                                        }
                                    }
                                }
                            }

                            IStiFont mFont = component as IStiFont;
                            if (mFont != null)
                            {
                                int fnt = pdfFont.GetFontNumber(mFont.Font);
                            }

                            //make map of the unicode symbols
                            IStiTextOptions textOpt = component as IStiTextOptions;
                            if (component is StiText && (!isExportAsImage))
                            {
                                StiText text = component as StiText;
                                if (text.CheckAllowHtmlTags() || (StiOptions.Export.Pdf.UseWysiwygRender && text.TextQuality == StiTextQuality.Wysiwyg))
                                {
                                    if (StiOptions.Export.Pdf.UseOldModeAllowHtmlTags)
                                    {
                                        mfRender.AssembleTextToEmf(component, 1f);
                                    }
                                    else
                                    {
                                        StoreWysiwygSymbols(text);
                                    }
                                }
                                else
                                {
                                    bool useRightToLeft =
                                        ((textOpt != null) && (textOpt.TextOptions != null) && (textOpt.TextOptions.RightToLeft));
                                    StringBuilder sb = new StringBuilder(text.Text);
                                    sb = bidi.Convert(sb, useRightToLeft);
                                    pdfFont.StoreUnicodeSymbolsInMap(sb);

                                    IStiTextBrush mTextBrush = component as IStiTextBrush;
                                    if ((text != null) && (mTextBrush != null))
                                    {
                                        StoreShadingData1(mTextBrush.TextBrush, tempPageNumber);
                                        StoreHatchData(mTextBrush.TextBrush);
                                    }
                                }
                                if ((allowEditable == StiPdfAllowEditable.Yes) && ((IStiEditable)text).Editable)
                                {
                                    StiEditableObject seo = new StiEditableObject();
                                    seo.Page = tempPageNumber;
                                    annotsArray.Add(seo);
                                    fontGlyphsReduceNotNeed[pdfFont.CurrentFont] = true;
                                }
                            }
                            if (!exportRtfTextAsImage && (component is StiRichText))
                            {
                                mfRender.AssembleRtf(component);
                            }

                            if (component is StiBarCode && !isExportAsImage)
                            {
                                StiPdfGeomWriter pdfGeomWriter = new StiPdfGeomWriter(pageStream, this, true);
                                StiBarCodeExportPainter barCodePainter = new StiBarCodeExportPainter(pdfGeomWriter);
                                StiBarCode barCode = component as StiBarCode;
                                if (!string.IsNullOrEmpty(barCode.CodeValue) && barCode.Page != null)
                                {
                                    RectangleF rectf = report.Unit.ConvertToHInches(barCode.ClientRectangle).ToRectangleF();
                                    barCode.BarCodeType.Draw(barCodePainter, barCode, rectf, 1);
                                }
                            }

                            else if (component is StiChart && !isExportAsImage)
                            {
                                StiPdfData pp = new StiPdfData();
                                pp.Component = component;
                                RenderChart(pp, true, tempPageNumber);
                            }

                            else if (component is StiGauge && !isExportAsImage)
                            {
                                StiPdfData pp = new StiPdfData();
                                pp.Component = component;
                                RenderGauge(pp, true, tempPageNumber);
                            }

                            else if (component is StiMap && !isExportAsImage)
                            {
                                StiPdfData pp = new StiPdfData();
                                pp.Component = component;
                                RenderMap(pp, true, tempPageNumber);
                            }
                        }

                        if ((component.HyperlinkValue != null) && (component.HyperlinkValue.ToString().Trim().Length > 0) && (!component.HyperlinkValue.ToString().Trim().StartsWith("javascript:")))
                        {
                            StiLinkObject stl = new StiLinkObject();
                            stl.Link = component.HyperlinkValue.ToString();
                            stl.Page = tempPageNumber;
                            linksArray.Add(stl);
                        }

                        if ((component.ToolTipValue != null) && (component.ToolTipValue.ToString().Trim().Length > 0) && !usePdfA)
                        {
                            StiLinkObject stl = new StiLinkObject();
                            //stl.Link = component.ToolTipValue.ToString();
                            stl.Page = tempPageNumber;
                            tooltipsArray.Add(stl);
                        }

                        if ((component.TagValue != null) && (component.TagValue.ToString().ToLower(CultureInfo.InvariantCulture) == "pdfunsignedsignaturefield") && !usePdfA)
                        {
                            var stl = new StiEditableObject();
                            stl.Page = tempPageNumber;
                            unsignedSignaturesArray.Add(stl);
                        }

                        if ((useDigitalSignature) && (!haveDigitalSignature) && (component.TagValue != null) && (component.TagValue.ToString().ToLower(CultureInfo.InvariantCulture) == "pdfdigitalsignature"))
                        {
                            signaturePageNumber = tempPageNumber;
                            haveDigitalSignature = true;
                        }
                        #endregion
                    }
                }
                tempPageNumber++;

                if ((page.Watermark != null) && (page.Watermark.Enabled))
                {
                    if (page.Watermark.ShowImageBehind == false)
                    {
                        var image = page.Watermark.GetImage(page.Report);
                        if (image != null)
                        {
                            using (var gdiImage = StiImageConverter.BytesToImage(image))
                            {
                                if (gdiImage is Metafile)
                                    StoreWatermarkMetafileData(gdiImage, imageResolution, page);
                                else
                                    StoreImageData(gdiImage, imageResolution, false, false);
                            }
                        }
                    }
                }
            }

            //for trimming
            for (int indexFont = 0; indexFont < pdfFont.fontList.Count; indexFont++)
            {
                pdfFont.CurrentFont = indexFont;
                pdfFont.StoreUnicodeSymbolsInMap(new StringBuilder("…"));
            }

            if ((unsignedSignaturesArray.Count > 0) && (annotsArray.Count > 0 || annots2Array.Count > 0))
            {
                for (int indexFont = 0; indexFont < pdfFont.fontList.Count; indexFont++)
                {
                    var font = (PdfFonts.pfontInfo)pdfFont.fontList[indexFont];
                    if (font.Name == "Arial")
                    {
                        fontGlyphsReduceNotNeed[indexFont] = true;
                    }
                }
            }

            //prepare codepage1252 string
            StringBuilder sbb = new StringBuilder();
            for (int indexs = 32; indexs < 256; indexs++)
            {
                sbb.Append((char)CodePage1252[indexs]);
            }
            //add all codepage1252 to symbols
            for (int indexf = 0; indexf < pdfFont.fontList.Count; indexf++)
            {
                if (fontGlyphsReduceNotNeed[indexf])
                {
                    pdfFont.CurrentFont = indexf;
                    pdfFont.StoreUnicodeSymbolsInMap(sbb);
                }
            }

            imagesCurrent = 0;
            annotsCurrent = 0;
            annots2Current = 0;
            shadingCurrent = 0;

            linksCounter = linksArray.Count;
            haveLinks = linksCounter > 0;
            annotsCounter = annotsArray.Count;
            annots2Counter = annots2Array.Count;
            unsignedSignaturesCounter = unsignedSignaturesArray.Count;
            haveAnnots = annotsCounter > 0 || annots2Counter > 0 || unsignedSignaturesCounter > 0;
            shadingCounter = shadingArray.Count;
            hatchCounter = hatchArray.Count;
            tooltipsCounter = tooltipsArray.Count;
            haveTooltips = tooltipsCounter > 0;

            imagesCounter = imageCache.ImageStore.Count;
            int imagesMaskCounter = 0;
            for (int indexMask = 0; indexMask < imageCache.ImageMaskStore.Count; indexMask++)
            {
                if (imageCache.ImageMaskStore[indexMask] != null) imagesMaskCounter++;
            }
            #endregion

            if (useDigitalSignature && (!haveDigitalSignature))
            {
                haveDigitalSignature = true;
                signaturePageNumber = 0;

                StiPage page = pages[0];

                double pageHeight2 = hiToTwips * report.Unit.ConvertToHInches(page.PageHeight * page.SegmentPerHeight);
                double pageWidth2 = hiToTwips * report.Unit.ConvertToHInches(page.PageWidth * page.SegmentPerWidth);
                double mgLeft2 = hiToTwips * report.Unit.ConvertToHInches(page.Margins.Left);
                double mgRight2 = hiToTwips * report.Unit.ConvertToHInches(page.Margins.Right);
                double mgTop2 = hiToTwips * report.Unit.ConvertToHInches(page.Margins.Top);
                double mgBottom2 = hiToTwips * report.Unit.ConvertToHInches(page.Margins.Bottom);

                signaturePlacement = new RectangleD(0, pageHeight2 - mgTop2, mgLeft2, mgBottom2);
            }

            pdfFont.InitFontsData(isWpf);
            fontsCounter = pdfFont.fontList.Count;

            #region Fill structure info
            info = new StiPdfStructure();
            for (int index = 0; index < pages.Count; index++)
            {
                info.PageList.Add(info.CreateContentObject(true));
            }
            for (int index = 0; index < imageCache.ImagePackedStore.Count; index++)
            {
                info.XObjectList.Add(info.CreateXObject(true, imageCache.ImageMaskStore[index] != null));
            }
            for (int index = 0; index < pdfFont.fontList.Count; index++)
            {
                info.FontList.Add(info.CreateFontObject(true, useUnicodeMode, standardPdfFonts, embeddedFonts));
            }
            info.Outlines = info.CreateOutlinesObject(haveBookmarks);
            if (haveBookmarks)
            {
                for (int index = 0; index < bookmarksCounter; index++)
                {
                    info.Outlines.Items.Add(info.CreateObject(true));
                }
            }
            info.Patterns = info.CreatePatternsObject(true);
            for (int index = 0; index < hatchCounter; index++)
            {
                info.Patterns.HatchItems.Add(info.CreateObject(true));
            }
            for (int index = 0; index < shadingCounter; index++)
            {
                info.Patterns.ShadingItems.Add(info.CreateShadingObject(true));
            }
            for (int index = 0; index < linksCounter; index++)
            {
                info.LinkList.Add(info.CreateObject(true));
            }
            info.Encode = info.CreateObject(encrypted);
            info.ExtGState = info.CreateObject(true);

            info.AcroForm = info.CreateAcroFormObject(haveAnnots || haveDigitalSignature || haveTooltips);
            if (haveAnnots)
            {
                for (int index = 0; index < annotsCounter; index++)
                {
                    info.AcroForm.Annots.Add(info.CreateAnnotObject(true, true, 0));
                }
                for (int index = 0; index < fontsCounter; index++)
                {
                    info.AcroForm.AnnotFontItems.Add(info.CreateFontObject(true, false, false, false, true));
                }
                for (int index = 0; index < annots2Counter; index++)
                {
                    info.AcroForm.CheckBoxes.Add(new List<StiPdfStructure.StiPdfAnnotObjInfo>());
                    info.AcroForm.CheckBoxes[index].Add(info.CreateAnnotObject(true, true, 2));
                    info.AcroForm.CheckBoxes[index].Add(info.CreateAnnotObject(true, true, 2));
                    if ((annots2Array[index].Component as StiCheckBox).CheckedValue == null)
                    {
                        info.AcroForm.CheckBoxes[index].Add(info.CreateAnnotObject(true, false, 2));
                    }
                }
                for (int index = 0; index < unsignedSignaturesCounter; index++)
                {
                    info.AcroForm.UnsignedSignatures.Add(info.CreateAnnotObject(true, false, 0));
                }
            }
            info.AcroForm.Signatures.Add(info.CreateAnnotObject(haveDigitalSignature, true, 0));
            for (int index = 0; index < tooltipsCounter; index++)
            {
                info.AcroForm.Tooltips.Add(info.CreateAnnotObject(true, false, 0));
            }

            info.Metadata = info.CreateObject(true);

            info.DestOutputProfile = info.CreateObject(true);
            info.OutputIntents = info.CreateObject(true);

            info.EmbeddedJS = info.CreateContentObject(autoPrint != StiPdfAutoPrintMode.None);

            if (embeddedFiles != null && embeddedFiles.Count > 0)
            {
                foreach (var element in embeddedFiles)
                {
                    info.EmbeddedFilesList.Add(info.CreateContentObject(true));
                }
            }
            #endregion

            RenderStartDoc(report, pages);

            #region Write data
            int pageNumber = 0;
            linksArray.Clear();
            tagsArray = new List<StiLinkObject>();
            tooltipsArray.Clear();
            unsignedSignaturesArray.Clear();
            imageInfoCounter = 0;

            StatusString = StiLocalization.Get("Export", "ExportingCreatingDocument");

            CurrentPassNumber++;

            foreach (StiPage page in pages)
            {
                pages.GetPage(page);
                InvokeExporting(pageNumber, pages.Count, CurrentPassNumber, MaximumPassNumber);
                if (IsStopped) return;

                RenderPageHeader(pageNumber++);

                double pageHeight = hiToTwips * report.Unit.ConvertToHInches(page.PageHeight * page.SegmentPerHeight);
                double pageWidth = hiToTwips * report.Unit.ConvertToHInches(page.PageWidth * page.SegmentPerWidth);
                double mgLeft = hiToTwips * report.Unit.ConvertToHInches(page.Margins.Left);
                double mgRight = hiToTwips * report.Unit.ConvertToHInches(page.Margins.Right);
                double mgTop = hiToTwips * report.Unit.ConvertToHInches(page.Margins.Top);
                double mgBottom = hiToTwips * report.Unit.ConvertToHInches(page.Margins.Bottom);

                if (pageHeight > 14400)
                {
                    pageHeight = 14400;     //Adobe Acrobat limitation of page size: maximum 200" * 200"
                }

                #region bookmarks on pages
                if (report.Bookmark.Engine == StiEngineVersion.EngineV1)
                {
                    if ((haveBookmarks == true) && (page.BookmarkValue != null) && ((string)page.BookmarkValue != ""))
                    {
                        bool bookmarkFinded = false;
                        int tempIndex = 0;
                        while ((bookmarkFinded == false) && (tempIndex < bookmarksCounter))
                        {
                            StiBookmarkTreeNode tn = (StiBookmarkTreeNode)bookmarksTree[tempIndex];
                            if ((tn.Used == false) && (tn.Page == -1) && (tn.Title == (string)page.BookmarkValue))
                            {
                                tn.Page = pageNumber - 1;
                                tn.Y = pageHeight;
                                tn.Used = true;
                                bookmarkFinded = true;
                                bookmarksTree[tempIndex] = tn;
                                break;
                            }
                            tempIndex++;
                        }
                    }
                }
                else
                {
                    if ((haveBookmarks == true) && (!string.IsNullOrEmpty(page.Guid) || ((page.BookmarkValue != null) && ((string)page.BookmarkValue != ""))))
                    {
                        int tempIndex = 0;
                        while (tempIndex < bookmarksCounter)
                        {
                            StiBookmarkTreeNode tn = (StiBookmarkTreeNode)bookmarksTree[tempIndex];
                            if ((tn.Used == false) && (tn.Page == -1))
                            {
                                bool finded = false;
                                if (!string.IsNullOrEmpty(tn.Guid))
                                {
                                    if (!string.IsNullOrEmpty(page.Guid) && (tn.Guid == page.Guid)) finded = true;
                                }
                                else if (!string.IsNullOrEmpty(tn.Title))
                                {
                                    if ((page.BookmarkValue != null) && (((string)page.BookmarkValue).Length > 0) && (tn.Title == (string)page.BookmarkValue)) finded = true;
                                }

                                if (finded)
                                {
                                    tn.Page = pageNumber - 1;
                                    tn.Y = pageHeight;
                                    tn.Used = true;
                                    bookmarksTree[tempIndex] = tn;
                                    break;
                                }
                            }
                            tempIndex++;
                        }
                    }
                }
                #endregion

                #region hyperlinks on page
                if ((page.HyperlinkValue != null) && (page.HyperlinkValue.ToString().Trim().Length > 0) && (!page.HyperlinkValue.ToString().Trim().StartsWith("javascript:")) && !usePdfA)
                {
                    StiLinkObject stl = new StiLinkObject();
                    stl.Link = page.HyperlinkValue.ToString();
                    stl.X = 0;
                    stl.Y = 0;
                    stl.Width = pageWidth;
                    stl.Height = pageHeight;
                    stl.Page = pageNumber;
                    stl.DestPage = -1;
                    stl.DestY = -1;
                    linksArray.Add(stl);
                }
                #endregion

                #region Render page

                if (page.Brush != null)
                {
                    StiPdfData pp = new StiPdfData();
                    pp.X = 0;
                    pp.Y = 0;
                    pp.Width = pageWidth;
                    pp.Height = pageHeight;
                    pp.Component = new StiContainer();
                    (pp.Component as StiContainer).Brush = page.Brush;
                    (pp.Component as StiContainer).Border = null;
                    RenderBorder1(pp);  //fullpage fill
                }

                RenderWatermark(page, true, pageWidth, pageHeight, imageResolution);

                List<StiPdfData> storedBorders = new List<StiPdfData>();

                #region Sorting, DrawTopMost, 
                List<StiComponent> comps1 = new List<StiComponent>();
                List<StiComponent> comps2 = new List<StiComponent>();
                foreach (StiComponent component in page.Components)
                {
                    if ((component.TagValue != null) && (component.TagValue.ToString().ToLowerInvariant() == Stimulsoft.Report.Painters.StiContainerGdiPainter.TopmostToken))
                    {
                        comps2.Add(component);
                    }
                    else
                    {
                        comps1.Add(component);
                    }
                }

                if (StiOptions.Export.Pdf.OrderComponentsByPlacement)
                {
                    #region Sorting by Y, then by X
                    for (int index1 = 0; index1 < comps1.Count - 1; index1++)
                    {
                        var tempComp1 = comps1[index1];
                        for (int index2 = index1 + 1; index2 < comps1.Count; index2++)
                        {
                            var tempComp2 = comps1[index2];
                            if (tempComp1.Top < tempComp2.Top) continue;
                            if ((tempComp1.Top == tempComp2.Top) && (tempComp1.Left <= tempComp2.Left)) continue;
                            comps1[index2] = tempComp1;
                            comps1[index1] = tempComp2;
                            tempComp1 = tempComp2;
                        }
                    }
                    #endregion
                }

                comps1.AddRange(comps2);
                comps2.Clear();
                #endregion

                foreach (StiComponent component in comps1)
                {
                    if (component.Enabled && !(report.IsPrinting && !component.Printable))
                    {
                        imageInfoCounter++;

                        double x1 = hiToTwips * report.Unit.ConvertToHInches(component.Left);
                        double y1 = hiToTwips * report.Unit.ConvertToHInches(component.Top);
                        double x2 = hiToTwips * report.Unit.ConvertToHInches(component.Right);
                        double y2 = hiToTwips * report.Unit.ConvertToHInches(component.Bottom);

                        StiPdfData pp = new StiPdfData();

                        pp.X = x1;
                        pp.Y = y1;
                        pp.Width = x2 - x1;
                        pp.Height = y2 - y1;

                        pp.Y += mgTop;
                        pp.X += mgLeft;
                        pp.Y = pageHeight - (pp.Y + pp.Height);
                        pp.Component = component;

                        bool needPaint = (component.Width > 0) && (component.Height > 0);
                        if (needPaint)
                        {
                            bool isExportAsImage = component.IsExportAsImage(StiExportFormat.Pdf);

                            if (!usePdfA && !component.Printable)
                            {
                                pageStream.WriteLine("/OC /oc1 BDC");
                            }

                            if (!(component is StiShape))
                            {
                                RenderBorder1(pp);
                            }
                            if (component is StiText && !isExportAsImage)
                            {
                                StiText stiText = component as StiText;

                                #region prepare annot
                                int memPos = 0;
                                if (haveAnnots && stiText.Editable)
                                {
                                    pageStream.Flush();
                                    memPos = (int)pageStream.BaseStream.Position;
                                    //some correction - GS is not supported in editable
                                    PushColorToStack();
                                    lastColorStrokeA = 0xFF;
                                    lastColorNonStrokeA = 0xFF;
                                }
                                #endregion

                                if (stiText.CheckAllowHtmlTags() || (StiOptions.Export.Pdf.UseWysiwygRender && stiText.TextQuality == StiTextQuality.Wysiwyg && !isWpf))
                                {
                                    RenderTextFont(pp); //for set seo.font

                                    if (StiOptions.Export.Pdf.UseOldModeAllowHtmlTags)
                                    {
                                        mfRender.RenderTextToEmf(pp, 1f);
                                    }
                                    else
                                    {
                                        RenderText2(pp);
                                    }

                                    #region Store editable info
                                    if (haveAnnots && (pp.Component as StiText).Editable)
                                    {
                                        var seo = (StiEditableObject)annotsArray[annotsCurrent];
                                        seo.Multiline = stiText.WordWrap;
                                        seo.X = pp.X;
                                        seo.Y = pp.Y;
                                        seo.Width = pp.Width;
                                        seo.Height = pp.Height;
                                        seo.Component = pp.Component;

                                        seo.Alignment = StiTextHorAlignment.Left;
                                        var mTextHorAlign = pp.Component as IStiTextHorAlignment;
                                        if (mTextHorAlign != null)
                                        {
                                            StiTextHorAlignment horAlign = mTextHorAlign.HorAlignment;
                                            IStiTextOptions textOpt = pp.Component as IStiTextOptions;
                                            if (textOpt != null && textOpt.TextOptions != null && textOpt.TextOptions.RightToLeft)
                                            {
                                                if (horAlign == StiTextHorAlignment.Left) horAlign = StiTextHorAlignment.Right;
                                                else if (horAlign == StiTextHorAlignment.Right) horAlign = StiTextHorAlignment.Left;
                                            }
                                            seo.Alignment = horAlign;
                                        }

                                        var baseState = new StiTextRenderer.StiHtmlState();
                                        baseState.Text = new StringBuilder();
                                        var listStates = StiTextRenderer.ParseHtmlToStates(stiText.Text.Value, baseState);
                                        var sb = new StringBuilder();
                                        foreach (StiTextRenderer.StiHtmlState state in listStates)
                                        {
                                            sb.Append(state.Text);
                                        }
                                        seo.Text = sb.ToString().Replace("\n", "");
                                    }
                                    #endregion
                                }
                                else
                                {
                                    RenderTextFont(pp);
                                    RenderText(pp);
                                }

                                #region end annot
                                if ((annotsCounter > 0 || annots2Counter > 0) && stiText.Editable)
                                {
                                    pageStream.Flush();
                                    int memPos2 = (int)pageStream.BaseStream.Position;
                                    pageStream.BaseStream.Seek(memPos, SeekOrigin.Begin);
                                    byte[] bufAnnot = new byte[memPos2 - memPos];
                                    pageStream.BaseStream.Read(bufAnnot, 0, memPos2 - memPos);
                                    pageStream.BaseStream.Seek(memPos, SeekOrigin.Begin);
                                    //								memoryPageStream.SetLength(memPos);
                                    StiEditableObject seo = (StiEditableObject)annotsArray[annotsCurrent];
                                    seo.Content = bufAnnot;
                                    annotsCurrent++;
                                    //some correction - GS is not supported in editable
                                    PopColorFromStack();
                                }
                                #endregion
                            }
                            if (component is StiShape) RenderShape(pp, imageResolution);

                            else if (component is StiBarCode && !isExportAsImage)
                            {
                                #region Render barcode
                                StiPdfGeomWriter pdfGeomWriter = new StiPdfGeomWriter(pageStream, this);
                                StiBarCodeExportPainter barCodePainter = new StiBarCodeExportPainter(pdfGeomWriter);
                                StiBarCode barCode = component as StiBarCode;
                                if (!string.IsNullOrEmpty(barCode.CodeValue) && barCode.Page != null)
                                {
                                    pageStream.WriteLine("q");
                                    PushColorToStack();
                                    pageStream.WriteLine("{0} {1} {2} {3} re W n",
                                        ConvertToString(pp.X),
                                        ConvertToString(pp.Y),
                                        ConvertToString(pp.Width),
                                        ConvertToString(pp.Height));
                                    pageStream.WriteLine("1 0 0 1 {0} {1} cm",
                                        ConvertToString(mgLeft),
                                        ConvertToString(pageHeight - mgTop));
                                    pageStream.WriteLine("1 0 0 -1 0 0 cm");
                                    pageStream.WriteLine("{0} 0 0 {0} 0 0 cm", ConvertToString(hiToTwips));

                                    RectangleF rectf = report.Unit.ConvertToHInches(barCode.ClientRectangle).ToRectangleF();
                                    barCode.BarCodeType.Draw(barCodePainter, barCode, rectf, 1);

                                    pageStream.WriteLine("Q");
                                    PopColorFromStack();
                                }
                                #endregion
                            }


                            else if (component is StiChart && !isExportAsImage)
                            {
                                #region Render chart
                                pageStream.WriteLine("q");
                                PushColorToStack();

                                //pageStream.WriteLine("{0} {1} {2} {3} re W n",
                                //    ConvertToString(pp.X),
                                //    ConvertToString(pp.Y),
                                //    ConvertToString(pp.Width),
                                //    ConvertToString(pp.Height));
                                pageStream.WriteLine("1 0 0 1 {0} {1} cm",
                                    ConvertToString(pp.X),
                                    ConvertToString(pp.Y + pp.Height));
                                pageStream.WriteLine("1 0 0 -1 0 0 cm");
                                pageStream.WriteLine("{0} 0 0 {0} 0 0 cm", ConvertToString(hiToTwips / 0.96));

                                RenderChart(pp, false, pageNumber - 1);

                                pageStream.WriteLine("Q");
                                PopColorFromStack();
                                #endregion
                            }
                            else if (component is StiGauge && !isExportAsImage)
                            {
                                #region Render gauge
                                pageStream.WriteLine("q");
                                PushColorToStack();
                                
                                pageStream.WriteLine("1 0 0 1 {0} {1} cm",
                                    ConvertToString(pp.X),
                                    ConvertToString(pp.Y + pp.Height));
                                pageStream.WriteLine("1 0 0 -1 0 0 cm");
                                pageStream.WriteLine("{0} 0 0 {0} 0 0 cm", ConvertToString(hiToTwips / 0.96));

                                RenderGauge(pp, false, pageNumber - 1);

                                pageStream.WriteLine("Q");
                                PopColorFromStack();
                                #endregion
                            }
                            else if (component is StiMap && !isExportAsImage)
                            {
                                #region Render map
                                pageStream.WriteLine("q");
                                PushColorToStack();
                                
                                pageStream.WriteLine("1 0 0 1 {0} {1} cm",
                                    ConvertToString(pp.X),
                                    ConvertToString(pp.Y + pp.Height));
                                pageStream.WriteLine("1 0 0 -1 0 0 cm");
                                pageStream.WriteLine("{0} 0 0 {0} 0 0 cm", ConvertToString(hiToTwips / 0.96));

                                RenderMap(pp, false, pageNumber - 1);

                                pageStream.WriteLine("Q");
                                PopColorFromStack();
                                #endregion
                            }
                            else
                            {
                                if (!exportRtfTextAsImage && (component is StiRichText))
                                {
                                    mfRender.RenderRtf(pp);
                                }
                                else
                                {
                                    if (component is StiCheckBox)
                                    {
                                        #region Render checkbox
                                        StiCheckBox checkbox = component as StiCheckBox;
                                        if (haveAnnots && checkbox.Editable)
                                        {
                                            #region store annots info
                                            StiEditableObject seo = annots2Array[annots2Current];
                                            seo.Multiline = false;
                                            seo.X = pp.X;
                                            seo.Y = pp.Y;
                                            seo.Width = pp.Width;
                                            seo.Height = pp.Height;
                                            seo.Component = pp.Component;
                                            seo.Alignment = StiTextHorAlignment.Center;
                                            seo.Text = "";
                                            #endregion

                                            double storeX = pp.X;
                                            double storeY = pp.Y;
                                            pp.X = 0;
                                            pp.Y = 0;

                                            #region prepare annot
                                            pageStream.Flush();
                                            int memPos = (int)pageStream.BaseStream.Position;
                                            //some correction - GS is not supported in editable
                                            PushColorToStack();
                                            lastColorStrokeA = 0xFF;
                                            lastColorNonStrokeA = 0xFF;
                                            #endregion

                                            RenderCheckbox(pp, true);

                                            #region end annot
                                            pageStream.Flush();
                                            int memPos2 = (int)pageStream.BaseStream.Position;
                                            pageStream.BaseStream.Seek(memPos, SeekOrigin.Begin);
                                            byte[] bufAnnot = new byte[memPos2 - memPos];
                                            pageStream.BaseStream.Read(bufAnnot, 0, memPos2 - memPos);
                                            pageStream.BaseStream.Seek(memPos, SeekOrigin.Begin);
                                            //memoryPageStream.SetLength(memPos);
                                            seo.Content = bufAnnot;
                                            //some correction - GS is not supported in editable
                                            PopColorFromStack();
                                            #endregion

                                            #region prepare annot
                                            pageStream.Flush();
                                            memPos = (int)pageStream.BaseStream.Position;
                                            //some correction - GS is not supported in editable
                                            PushColorToStack();
                                            lastColorStrokeA = 0xFF;
                                            lastColorNonStrokeA = 0xFF;
                                            #endregion

                                            RenderCheckbox(pp, false, false);

                                            #region end annot
                                            pageStream.Flush();
                                            memPos2 = (int)pageStream.BaseStream.Position;
                                            pageStream.BaseStream.Seek(memPos, SeekOrigin.Begin);
                                            bufAnnot = new byte[memPos2 - memPos];
                                            pageStream.BaseStream.Read(bufAnnot, 0, memPos2 - memPos);
                                            pageStream.BaseStream.Seek(memPos, SeekOrigin.Begin);
                                            //memoryPageStream.SetLength(memPos);
                                            seo.Content2 = bufAnnot;
                                            //some correction - GS is not supported in editable
                                            PopColorFromStack();
                                            #endregion

                                            annots2Current++;

                                            pp.X = storeX;
                                            pp.Y = storeY;
                                        }
                                        else
                                        {
                                            bool? checkboxValue = GetCheckBoxValue(checkbox);
                                            if (checkboxValue != null)
                                            {
                                                RenderCheckbox(pp, checkboxValue.Value);
                                            }
                                        }
                                        #endregion
                                    }
                                    else if (isExportAsImage)
                                    {
                                        RenderImage(pp, imageResolution);
                                    }
                                }
                            }
                            if (component is StiLinePrimitive)
                            {
                                RenderPrimitives(pp);
                            }
                            else
                            {
                                IStiBorder mBorder = pp.Component as IStiBorder;
                                if ((mBorder != null) && (mBorder.Border != null) && (mBorder.Border.Topmost))
                                {
                                    storedBorders.Add(pp);
                                }
                                else
                                {
                                    RenderBorder2(pp);
                                }
                            }

                            if (!usePdfA && !component.Printable)
                            {
                                pageStream.WriteLine("EMC");
                            }
                        }

                        #region bookmarks on components
                        if (report.Bookmark.Engine == StiEngineVersion.EngineV1)
                        {
                            if ((haveBookmarks == true) && (component.BookmarkValue != null) && ((string)component.BookmarkValue != ""))
                            {
                                bool bookmarkFinded = false;
                                int tempIndex = 0;
                                while ((bookmarkFinded == false) && (tempIndex < bookmarksCounter))
                                {
                                    StiBookmarkTreeNode tn = (StiBookmarkTreeNode)bookmarksTree[tempIndex];
                                    if ((tn.Used == false) && (tn.Page == -1) && (tn.Title == (string)component.BookmarkValue))
                                    {
                                        tn.Page = pageNumber - 1;
                                        tn.Y = pp.Y + pp.Height;
                                        tn.Used = true;
                                        bookmarkFinded = true;
                                        bookmarksTree[tempIndex] = tn;
                                        break;
                                    }
                                    tempIndex++;
                                }
                            }
                        }
                        else
                        {
                            if ((haveBookmarks == true) && (!string.IsNullOrEmpty(component.Guid) || ((component.BookmarkValue != null) && ((string)component.BookmarkValue != ""))))
                            {
                                int tempIndex = 0;
                                while (tempIndex < bookmarksCounter)
                                {
                                    StiBookmarkTreeNode tn = (StiBookmarkTreeNode)bookmarksTree[tempIndex];
                                    if ((tn.Used == false) && (tn.Page == -1))
                                    {
                                        bool finded = false;
                                        if (!string.IsNullOrEmpty(tn.Guid))
                                        {
                                            if ((!string.IsNullOrEmpty(component.Guid)) && (tn.Guid == component.Guid)) finded = true;
                                        }
                                        else if (!string.IsNullOrEmpty(tn.Title))
                                        {
                                            if ((component.BookmarkValue != null) && (((string)component.BookmarkValue).Length > 0) && (tn.Title == (string)component.BookmarkValue)) finded = true;
                                        }

                                        if (finded)
                                        {
                                            tn.Page = pageNumber - 1;
                                            tn.Y = pp.Y + pp.Height;
                                            tn.Used = true;
                                            bookmarksTree[tempIndex] = tn;
                                            break;
                                        }
                                    }
                                    tempIndex++;
                                }
                            }
                        }
                        #endregion

                        #region hyperlinks on components
                        if ((component.HyperlinkValue != null) && (component.HyperlinkValue.ToString().Trim().Length > 0) && (!component.HyperlinkValue.ToString().Trim().StartsWith("javascript:")))
                        {
                            StiLinkObject stl = new StiLinkObject();
                            stl.Link = component.HyperlinkValue.ToString();
                            stl.X = pp.X;
                            stl.Y = pp.Y;
                            stl.Width = pp.Width;
                            stl.Height = pp.Height;
                            stl.Page = pageNumber;
                            stl.DestPage = -1;
                            stl.DestY = -1;
                            linksArray.Add(stl);
                        }
                        #endregion

                        #region tags on components
                        if ((component.TagValue != null) && (component.TagValue.ToString().Trim().Length > 0))
                        {
                            StiLinkObject stl = new StiLinkObject();
                            stl.Link = component.TagValue.ToString();
                            stl.X = pp.X;
                            stl.Y = pp.Y;
                            stl.Width = pp.Width;
                            stl.Height = pp.Height;
                            stl.Page = pageNumber - 1;
                            stl.DestPage = -1;
                            stl.DestY = -1;
                            tagsArray.Add(stl);

                            if (stl.Link.Trim().ToLower(CultureInfo.InvariantCulture) == "pdfunsignedsignaturefield" && !usePdfA)
                            {
                                var ste = new StiEditableObject();
                                ste.X = stl.X;
                                ste.Y = stl.Y;
                                ste.Width = stl.Width;
                                ste.Height = stl.Height;
                                ste.Page = stl.Page;
                                ste.Component = component;

                                unsignedSignaturesArray.Add(ste);
                            }
                        }
                        #endregion

                        #region tooltips on components
                        if ((component.ToolTipValue != null) && (component.ToolTipValue.ToString().Trim().Length > 0) && !usePdfA)
                        {
                            StiLinkObject stl = new StiLinkObject();
                            stl.Link = component.ToolTipValue.ToString().Trim();
                            stl.X = pp.X;
                            stl.Y = pp.Y;
                            stl.Width = pp.Width;
                            stl.Height = pp.Height;
                            stl.Page = pageNumber - 1;
                            stl.DestPage = -1;
                            stl.DestY = -1;
                            tooltipsArray.Add(stl);
                        }
                        #endregion

                        #region digital signature on component
                        if ((component.TagValue != null) && (component.TagValue.ToString().ToLower(CultureInfo.InvariantCulture) == "pdfdigitalsignature"))
                        {
                            signaturePlacement = new RectangleD(pp.X, pp.Y, pp.Width, pp.Height);
                        }
                        #endregion

                    }
                }

                foreach (StiPdfData ppd in storedBorders)
                {
                    RenderBorder2(ppd);
                }
                storedBorders.Clear();

                if (page.Border != null)
                {
                    StiPdfData pp = new StiPdfData();
                    pp.X = mgLeft;
                    pp.Y = mgBottom;
                    pp.Width = pageWidth - mgLeft - mgRight;
                    pp.Height = pageHeight - mgTop - mgBottom;
                    pp.Component = new StiContainer();
                    (pp.Component as StiContainer).Border = page.Border;
                    RenderBorder2(pp);
                }

                RenderWatermark(page, false, pageWidth, pageHeight, imageResolution);

                #endregion

                RenderPageFooter(pageHeight, pageWidth);
            }
            #endregion

            #region Remake bookmarks table
            bookmarksTreeTemp = null;
            if (haveBookmarks && deleteEmptyBookmarks)
            {
                //mark node path
                for (int indexNode = 0; indexNode < bookmarksTree.Count; indexNode++)
                {
                    StiBookmarkTreeNode tn = (StiBookmarkTreeNode)bookmarksTree[indexNode];
                    if (tn.Used)
                    {
                        while (tn.Parent != -1)
                        {
                            tn = (StiBookmarkTreeNode)bookmarksTree[tn.Parent];
                            tn.Used = true;
                        }
                    }
                }

                //make new Bookmarks
                StiBookmark rootBookmark = new StiBookmark();
                MakeBookmarkFromTree(rootBookmark, (StiBookmarkTreeNode)bookmarksTree[0]);
                bookmarksTreeTemp = bookmarksTree;
                bookmarksTree = new ArrayList();
                AddBookmarkNode(rootBookmark, -1);

                //add empty records
                int numberNodesToAdd = bookmarksCounter - bookmarksTree.Count;
                if (numberNodesToAdd > 0)
                {
                    StiBookmarkTreeNode tn = new StiBookmarkTreeNode();
                    tn.Parent = -1;
                    tn.First = -1;
                    tn.Last = -1;
                    tn.Prev = -1;
                    tn.Next = -1;
                    tn.Count = -1;
                    tn.Page = -1;
                    tn.Y = -1;
                    tn.Title = "";
                    for (int indexNode = 0; indexNode < numberNodesToAdd; indexNode++)
                    {
                        bookmarksTree.Add(tn);
                    }
                }

                //fill new Bookmarks
                Hashtable htNameToNode = new Hashtable();
                for (int indexBookmark = 0; indexBookmark < bookmarksTreeTemp.Count; indexBookmark++)
                {
                    StiBookmarkTreeNode tn = (StiBookmarkTreeNode)bookmarksTreeTemp[indexBookmark];
                    htNameToNode[tn.Title] = tn;
                }
                for (int indexBookmark = 0; indexBookmark < bookmarksTree.Count; indexBookmark++)
                {
                    StiBookmarkTreeNode tn = (StiBookmarkTreeNode)bookmarksTree[indexBookmark];
                    if (!string.IsNullOrEmpty(tn.Title))
                    {
                        StiBookmarkTreeNode tnOld = (StiBookmarkTreeNode)htNameToNode[tn.Title];
                        tn.Page = tnOld.Page;
                        tn.Y = tnOld.Y;
                    }
                }

                bookmarksTreeTemp = null;
            }
            #endregion

            RenderImageTable();
            RenderFontTable();
            RenderBookmarksTable();
            RenderPatternTable();
            RenderLinkTable();
            RenderEncodeRecord();
            RenderExtGStateRecord();
            RenderAnnotTable();
            RenderSignatureTable(report);
            RenderTooltipTable();
            RenderMetadata(report);
            RenderColorSpace();
            RenderAutoPrint();
            RenderEmbeddedFiles();
            RenderEndDoc();

            #region Clear
            imageList.Clear();
            imageList = null;
            imageInterpolationTable.Clear();
            imageInterpolationTable = null;
            imageCacheIndexToList.Clear();
            imageCacheIndexToList = null;
            imageInfoList.Clear();
            imageInfoList = null;
            xref = null;
            bookmarksTree = null;
            linksArray = null;
            tagsArray = null;
            tooltipsArray = null;
            annotsArray = null;
            annots2Array = null;
            unsignedSignaturesArray = null;
            shadingArray = null;
            hatchArray = null;
            colorStack = null;
            enc = null;

            if (graphicsForTextRenderer != null)
            {
                graphicsForTextRenderer.Dispose();
                graphicsForTextRenderer = null;
            }
            if (imageForGraphicsForTextRenderer != null)
            {
                imageForGraphicsForTextRenderer.Dispose();
                imageForGraphicsForTextRenderer = null;
            }

            pdfFont.Clear();
            pdfFont = null;
            bidi.Clear();
            bidi = null;

            imageCache.Clear();

            mfRender.Clear();
            mfRender = null;
            #endregion

            sw.Flush();

            if (haveDigitalSignature)
            {
                #region Sign document
                int fileLen = (int)stream.Length;
                int signDataLen2 = fileLen - (offsetSignatureData + signatureDataLen + 2);

                //write length of second data block to stream
                byte[] buf5 = Encoding.ASCII.GetBytes(signDataLen2.ToString());
                stream.Seek(offsetSignatureLen2, SeekOrigin.Begin);
                stream.Write(buf5, 0, buf5.Length);
                stream.Flush();

                //prepare data for creating signature
                byte[] buf = new byte[offsetSignatureData + signDataLen2];
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(buf, 0, offsetSignatureData);
                stream.Seek(offsetSignatureData + signatureDataLen + 2, SeekOrigin.Begin);
                stream.Read(buf, offsetSignatureData, signDataLen2);

                bool isGost = false;
                string signedBy = digitalSignatureSignedBy;

                byte[] signData = pdfSecurity.CreateSignature(buf, getCertificateFromCryptoUI, subjectNameString, useLocalMachineCertificates, certificateData, certificatePassword, out isGost, ref signedBy, offsetSignatureFilter, offsetSignatureName);
                byte[] buf4 = Encoding.ASCII.GetBytes(BitConverter.ToString(signData).Replace("-", "").ToLower(CultureInfo.InvariantCulture));

                //write signature to stream
                stream.Seek(offsetSignatureData + 1, SeekOrigin.Begin);
                stream.Write(buf4, 0, buf4.Length);

                if (isGost)
                {
                    byte[] buf6 = Encoding.ASCII.GetBytes("CryptoPro#20PDF");
                    stream.Seek(offsetSignatureFilter, SeekOrigin.Begin);
                    stream.Write(buf6, 0, buf6.Length);
                }
                if (string.IsNullOrEmpty(digitalSignatureSignedBy) && !string.IsNullOrEmpty(signedBy))
                {
                    stream.Seek(offsetSignatureName, SeekOrigin.Begin);
                    StoreString(signedBy);
                }

                stream.Seek(fileLen, SeekOrigin.Begin);
                stream.Flush();
                #endregion
            }

        }
        #endregion

    }
}
