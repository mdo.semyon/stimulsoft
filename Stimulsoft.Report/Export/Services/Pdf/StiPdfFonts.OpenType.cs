#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft   							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft 

using System;
using System.Text;
using System.Collections;
using System.IO;

namespace Stimulsoft.Report.Export
{
    internal partial class PdfFonts
    {
        internal class StiOpenTypeHelper
        {

            #region Constants
            private const int TtfHeaderSize = 12;

            //OpenType font flags field
            private const UInt16 ARG_1_AND_2_ARE_WORDS = 0x0001;
            private const UInt16 ARGS_ARE_XY_VALUES = 0x0002;
            private const UInt16 ROUND_XY_TO_GRID = 0x0004;
            private const UInt16 WE_HAVE_A_SCALE = 0x0008;
            private const UInt16 MORE_COMPONENTS = 0x0020;
            private const UInt16 WE_HAVE_AN_X_AND_Y_SCALE = 0x0040;
            private const UInt16 WE_HAVE_A_TWO_BY_TWO = 0x0080;
            private const UInt16 WE_HAVE_INSTRUCTIONS = 0x0100;
            private const UInt16 USE_MY_METRICS = 0x0200;
            private const UInt16 OVERLAP_COMPOUND = 0x0400;
            private const UInt16 SCALED_COMPONENT_OFFSET = 0x0800;
            private const UInt16 UNSCALED_COMPONENT_OFFSET = 0x1000;

            #endregion

            #region Fields
            private byte[] bufGetData = new byte[4];
            private int posBuf = 0;
            private byte[] dataBuf = null;

            private static string[] TablesNames = new string[]
            {
                "head", //+		+ 
                "hhea", //+		+
                "hmtx", //+		+
                "maxp", //+		+
                "cmap", //+		
                "OS/2", //+		
                "post", //+		
                "cvt ", //++	++
                "fpgm", //++	++
                "glyf", //+		+
                "loca", //+		+
                "prep", //++	++
                "name" //this table required for Windows
            };
            #endregion

            #region Class TTF Info
            private class TtfInfo
            {
                public struct TableDirectoryItem
                {
                    public string TagString;
                    public uint Tag;
                    public uint CheckSum;
                    public uint Offset;
                    public uint Length;
                    public uint NewOffset;
                    public uint NewLength;
                    public bool Required;
                    public byte[] NewTable;

                    public override string ToString()
                    {
                        return $"{TagString} {Length} {(Required ? "*" : "")}";
                    }
                }

                public int HeaderOffset;
                public int NumTables;
                public int NumTablesRequired;
                public uint SfntVersion;
                public TableDirectoryItem[] Tables;
                public int HeadCheckSumOffset;
                public uint NumGlyphs;
                public uint IndexToLocFormat;
                public int IndexLocaTable;
                public int IndexGlyfTable;
                public int IndexCmapTable;
                public Hashtable FontName;

                public TtfInfo()
                {
                    HeaderOffset = 0;
                    NumTables = 0;
                    NumTablesRequired = 0;
                    SfntVersion = 0;
                    Tables = null;
                    HeadCheckSumOffset = 0;
                    NumGlyphs = 0;
                    IndexToLocFormat = 0;
                    IndexLocaTable = 0;
                    IndexGlyfTable = 0;
                    FontName = new Hashtable();
                }
            }
            #endregion

            #region Methods

            #region Reduce font size
            public void ReduceFontSize(ref byte[] buff, string fontName, bool remakeGlyphTable, ushort[] GlyphList, ushort[] GlyphRtfList)
            {
                try { 
                TtfInfo ttf = ScanFontFile(buff, fontName);

                if (ttf != null)
                {
                    if (remakeGlyphTable)
                    {
                        #region remake tables

                        #region prepare array
                        bool[] glyphNeeds = new bool[ttf.NumGlyphs];
                        bool[] glyphNeedsScan = new bool[ttf.NumGlyphs];
                        for (int indexGlyph = 0; indexGlyph < GlyphList.Length; indexGlyph ++)
                        {
                            ushort glyphNumber = GlyphList[indexGlyph];
                            if (glyphNumber < ttf.NumGlyphs)
                            {
                                glyphNeeds[glyphNumber] = true;
                                glyphNeedsScan[glyphNumber] = true;
                            }
                        }
                        if (GlyphRtfList != null)
                        {
                            for (int indexGlyph = 0; indexGlyph < GlyphRtfList.Length; indexGlyph ++)
                            {
                                ushort glyphNumber = GlyphRtfList[indexGlyph];
                                if (glyphNumber < ttf.NumGlyphs)
                                {
                                    glyphNeeds[glyphNumber] = true;
                                    glyphNeedsScan[glyphNumber] = true;
                                }
                            }
                        }
                        glyphNeeds[0] = true;
                        glyphNeeds[1] = true;
                        glyphNeeds[2] = true;
                        glyphNeeds[3] = true;
                        glyphNeedsScan[0] = true;
                        glyphNeedsScan[1] = true;
                        glyphNeedsScan[2] = true;
                        glyphNeedsScan[3] = true;
                        #endregion

                        uint offsetLocaTable = ttf.Tables[ttf.IndexLocaTable].Offset;
                        uint offsetGlyfTable = ttf.Tables[ttf.IndexGlyfTable].Offset;

                        #region scan glyf table for composite glyph
                        bool needScan = true;
                        while (needScan)
                        {
                            needScan = false;
                            for (int indexGlyph = 0; indexGlyph < ttf.NumGlyphs; indexGlyph ++)
                            {
                                if (glyphNeedsScan[indexGlyph] == true)
                                {
                                    glyphNeedsScan[indexGlyph] = false;
                                    int locaOffsetScan = (int) (offsetLocaTable + indexGlyph*(ttf.IndexToLocFormat == 1 ? 4 : 2));
                                    int offsetGlyf = 0;
                                    if (ttf.IndexToLocFormat == 1)
                                    {
                                        offsetGlyf = (int) GetUInt32(buff, locaOffsetScan);
                                    }
                                    else
                                    {
                                        offsetGlyf = 2*GetUInt16(buff, locaOffsetScan);
                                    }
                                    offsetGlyf += (int) offsetGlyfTable;
                                    if (GetInt16(buff, offsetGlyf) == -1) //composite glyph
                                    {
                                        needScan = true;

                                        #region scan composite glyph

                                        offsetGlyf += 10; //glyf header length
                                        UInt16 flags = 0;
                                        do
                                        {
                                            flags = GetUInt16(buff, offsetGlyf);
                                            UInt16 glyphIndex = GetUInt16(buff, offsetGlyf + 2);
                                            if (glyphIndex < ttf.NumGlyphs)
                                            {
                                                glyphNeeds[glyphIndex] = true;
                                                glyphNeedsScan[glyphIndex] = true;
                                            }
                                            offsetGlyf += 4;

                                            if ((flags & ARG_1_AND_2_ARE_WORDS) > 0)
                                            {
                                                offsetGlyf += 2; //argument1;
                                                offsetGlyf += 2; //argument2;
                                            }
                                            else
                                            {
                                                offsetGlyf += 2; //argument 1 and 2;
                                            }
                                            if ((flags & WE_HAVE_A_SCALE) > 0)
                                            {
                                                offsetGlyf += 2; // Format 2.14
                                            }
                                            else
                                            {
                                                if ((flags & WE_HAVE_AN_X_AND_Y_SCALE) > 0)
                                                {
                                                    offsetGlyf += 2; // Format 2.14
                                                    offsetGlyf += 2; // Format 2.14
                                                }
                                                else
                                                {
                                                    if ((flags & WE_HAVE_A_TWO_BY_TWO) > 0)
                                                    {
                                                        offsetGlyf += 2; // Format 2.14
                                                        offsetGlyf += 2; // Format 2.14
                                                        offsetGlyf += 2; // Format 2.14
                                                        offsetGlyf += 2; // Format 2.14
                                                    }
                                                }
                                            }
                                        } while ((flags & MORE_COMPONENTS) > 0);

                                        #endregion
                                    }
                                }
                            }
                        }
                        #endregion

                        #region remake glyf table
                        //first pass - calculate new length
                        int newLength = 0;
                        int locaOffset = (int) offsetLocaTable;
                        for (int indexGlyph = 0; indexGlyph < ttf.NumGlyphs; indexGlyph++)
                        {
                            int offsetBegin = 0;
                            int offsetEnd = 0;
                            if (ttf.IndexToLocFormat == 1)
                            {
                                offsetBegin = (int) GetUInt32(buff, locaOffset);
                                offsetEnd = (int) GetUInt32(buff, locaOffset + 4);
                                locaOffset += 4;
                            }
                            else
                            {
                                offsetBegin = 2*GetUInt16(buff, locaOffset);
                                offsetEnd = 2*GetUInt16(buff, locaOffset + 2);
                                locaOffset += 2;
                            }
                            if (glyphNeeds[indexGlyph])
                            {
                                newLength += offsetEnd - offsetBegin;
                            }
                        }

                        //second pass - create new table
                        byte[] newGlyfTable = new byte[newLength + 4];
                        if (offsetGlyfTable + ttf.Tables[ttf.IndexGlyfTable].Length + 4 < buff.Length)
                        {
                            Array.Copy(buff, offsetGlyfTable + ttf.Tables[ttf.IndexGlyfTable].Length, newGlyfTable, newLength, 4);
                        }
                        byte[] newLocaTable = new byte[ttf.Tables[ttf.IndexLocaTable].Length + 4];
                        if (offsetLocaTable + ttf.Tables[ttf.IndexLocaTable].Length + 4 < buff.Length)
                        {
                            Array.Copy(buff, offsetLocaTable + ttf.Tables[ttf.IndexLocaTable].Length, newLocaTable, ttf.Tables[ttf.IndexLocaTable].Length, 4);
                        }
                        uint currentGlyfPos = 0;
                        locaOffset = (int) offsetLocaTable;
                        int locaOffset2 = 0;
                        for (int indexGlyph = 0; indexGlyph < ttf.NumGlyphs; indexGlyph ++)
                        {
                            int offsetBegin = 0;
                            int offsetEnd = 0;
                            if (ttf.IndexToLocFormat == 1)
                            {
                                offsetBegin = (int) GetUInt32(buff, locaOffset);
                                offsetEnd = (int) GetUInt32(buff, locaOffset + 4);
                                locaOffset += 4;
                            }
                            else
                            {
                                offsetBegin = 2*GetUInt16(buff, locaOffset);
                                offsetEnd = 2*GetUInt16(buff, locaOffset + 2);
                                locaOffset += 2;
                            }
                            int lengthGlyf = offsetEnd - offsetBegin;

                            if (glyphNeeds[indexGlyph] == true)
                            {
                                for (int indexPos = 0; indexPos < lengthGlyf; indexPos ++)
                                {
                                    newGlyfTable[currentGlyfPos + indexPos] = buff[offsetGlyfTable + offsetBegin + indexPos];
                                }
                            }
                            else
                            {
                                lengthGlyf = 0;
                            }

                            if (ttf.IndexToLocFormat == 0)
                            {
                                SetUInt16(newLocaTable, locaOffset2, (ushort) (currentGlyfPos/2));
                            }
                            else
                            {
                                SetUInt32(newLocaTable, locaOffset2, currentGlyfPos);
                            }
                            currentGlyfPos += (uint) lengthGlyf;
                            locaOffset2 += (ttf.IndexToLocFormat == 1) ? 4 : 2;
                        }
                        if (ttf.IndexToLocFormat == 0)
                        {
                            SetUInt16(newLocaTable, locaOffset2, (ushort) (currentGlyfPos/2));
                        }
                        else
                        {
                            SetUInt32(newLocaTable, locaOffset2, currentGlyfPos);
                        }
                        ttf.Tables[ttf.IndexGlyfTable].NewLength = currentGlyfPos;
                        ttf.Tables[ttf.IndexGlyfTable].NewTable = newGlyfTable;
                        ttf.Tables[ttf.IndexLocaTable].NewTable = newLocaTable;
                        //for (int indexG = (int)currentGlyfPos; indexG < ttf.Tables[ttf.IndexGlyfTable].Length; indexG ++)
                        //{
                        //    buff[offsetGlyfTable + indexG] = 0;
                        //}
                        #endregion

                        #endregion
                    }

                    byte[] newHeader = new byte[TtfHeaderSize + ttf.NumTablesRequired*16];
                    Array.Copy(buff, ttf.HeaderOffset, newHeader, 0, newHeader.Length);

                    #region make new offset table
                    int maximumPower = 1;
                    while (2 << (maximumPower - 1) <= ttf.NumTablesRequired)
                    {
                        maximumPower ++;
                    }
                    maximumPower --;
                    int searchRange = (2 << (maximumPower - 1))*16;
                    int entrySelector = maximumPower;
                    int rangeShift = ttf.NumTablesRequired*16 - searchRange;
                    SetUInt16(newHeader, 4, (UInt16) ttf.NumTablesRequired);
                    SetUInt16(newHeader, 6, (UInt16) searchRange);
                    SetUInt16(newHeader, 8, (UInt16) entrySelector);
                    SetUInt16(newHeader, 10, (UInt16) rangeShift);
                    #endregion

                    #region make new table info
                    //uint globalCheckSum = 0;
                    uint currentTable = 0;
                    uint currentPos = (uint) (TtfHeaderSize + ttf.NumTablesRequired*16);
                    for (int indexTable = 0; indexTable < ttf.NumTables; indexTable ++)
                    {
                        if (ttf.Tables[indexTable].Required)
                        {
                            ttf.Tables[indexTable].NewOffset = currentPos;
                            currentPos += ttf.Tables[indexTable].NewLength;
                            uint remainder = currentPos%4;
                            if (remainder > 0)
                            {
                                currentPos += 4 - remainder;
                            }

                            //calculate checksum for this table
                            //uint checkSum = 0;
                            //uint lengthTable = (ttf.Tables[indexTable].NewLength + (remainder > 0 ? 4 - remainder : 0)) / 4;
                            //byte[] buff2 = buff;
                            //int offsetCheck = (int)ttf.Tables[indexTable].Offset;
                            //if (ttf.Tables[indexTable].NewTable != null)
                            //{
                            //    buff2 = ttf.Tables[indexTable].NewTable;
                            //    offsetCheck = 0;
                            //}
                            //for (int indexLen = 0; indexLen < lengthTable; indexLen++)
                            //{
                            //    checkSum += GetUInt32(buff2, offsetCheck);
                            //    offsetCheck += 4;
                            //}
                            //ttf.Tables[indexTable].CheckSum = checkSum;
                            //globalCheckSum += checkSum;

                            int pos = (int) (TtfHeaderSize + currentTable*16);
                            SetUInt32(newHeader, pos, ttf.Tables[indexTable].Tag);
                            SetUInt32(newHeader, pos + 4, ttf.Tables[indexTable].CheckSum);
                            SetUInt32(newHeader, pos + 8, ttf.Tables[indexTable].NewOffset);
                            SetUInt32(newHeader, pos + 12, ttf.Tables[indexTable].NewLength);

                            currentTable ++;
                        }
                    }
                    #endregion

                    #region correct global checksum
                    //for (int indexSum = 0; indexSum < (ttf.HeaderOffset + TtfHeaderSize + ttf.NumTablesRequired * 16) / 4; indexSum ++)
                    //{
                    //    globalCheckSum += GetUInt32(buff, (int)(indexSum * 4));
                    //}
                    //globalCheckSum = 0xB1B0AFBA - globalCheckSum;
                    //SetUInt32(newHeader, ttf.HeadCheckSumOffset, globalCheckSum);
                    #endregion

                    #region write new font
                    using (MemoryStream mem = new MemoryStream())
                    {
                        mem.Write(newHeader, 0, newHeader.Length);
                        for (int indexTable = 0; indexTable < ttf.NumTables; indexTable ++)
                        {
                            if (ttf.Tables[indexTable].Required)
                            {
                                if (ttf.Tables[indexTable].NewTable != null)
                                {
                                    mem.Write(ttf.Tables[indexTable].NewTable, 0, (int) ttf.Tables[indexTable].NewLength);
                                }
                                else
                                {
                                    mem.Write(buff, (int) ttf.Tables[indexTable].Offset, (int) ttf.Tables[indexTable].NewLength);
                                }
                                int remainder = (int) ttf.Tables[indexTable].NewLength%4;
                                if (remainder > 0)
                                {
                                    mem.Write(new byte[4 - remainder], 0, 4 - remainder);
                                }
                            }
                        }
                        mem.Flush();
                        buff = mem.ToArray();
                        mem.Close();
                    }
                    #endregion
                }
                } catch { } //nothing to change, return original buffer
            }
            #endregion

            #region ScanFontFile
            private TtfInfo ScanFontFile(byte[] buff, string fontName)
            {
                TtfInfo ttf = null;
                uint tag = GetUInt32(buff, 0);
                if (tag == 0x00010000) //sfnt version 1.0
                {
                    //TTF font
                    ttf = GetTtfInfo(buff, 0);
                }
                else
                {
                    uint ttcVer = GetUInt32(buff, 4);
                    if ((tag == 0x74746366) && ((ttcVer == 0x00010000) || (ttcVer == 0x00020000))) //ttcf version 1.0 or 2.0
                    {
                        #region TTC font
                        uint numFontsTtc = GetUInt32(buff, 8);
                        for (int indexTtc = (int)(numFontsTtc - 1); indexTtc >= 0; indexTtc--)
                        {
                            int offset = (int)GetUInt32(buff, 12 + indexTtc * 4);
                            uint tagTtc = GetUInt32(buff, offset);
                            if (tagTtc == 0x00010000) //sfnt version 1.0
                            {
                                //TTC font
                                ttf = GetTtfInfo(buff, offset);
                                if (ttf.FontName.Contains(fontName))
                                {
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                }
                return ttf;
            }
            #endregion

            #region GetCmapTable
            public ushort[] GetCharToGlyphTable(byte[] buff, string fontName)
            {
                TtfInfo ttf = ScanFontFile(buff, fontName);
                if (ttf != null)
                {
                    return GetCmapTable(buff, ttf);
                }
                return null;
            }

            private ushort[] GetCmapTable(byte[] buff, TtfInfo ttf)
            {
                ushort[] charToGlyph = null;

                int offsetCmapTable = (int)ttf.Tables[ttf.IndexCmapTable].Offset;
                int numCmapTables = GetUInt16(buff, offsetCmapTable + 2);

                for (int indexRecord = 0; indexRecord < numCmapTables; indexRecord++)
                {
                    int offsetRecord = offsetCmapTable + 4 + indexRecord * 8;
                    uint platformID = GetUInt16(buff, offsetRecord);
                    uint encodingID = GetUInt16(buff, offsetRecord + 2);
                    int offsetTable = offsetCmapTable + (int)GetUInt32(buff, offsetRecord + 4);

                    if ((platformID == 3) && (encodingID == 1)) //Unicode BMP (UCS-2)
                    {
                        uint formatNumber = GetUInt16(buff, offsetTable);
                        if (formatNumber == 4)
                        {
                            #region cmap table Format 4
                            charToGlyph = new ushort[65536];
                            for (int index = 0; index < 65536; index++) charToGlyph[index] = 0xffff;

                            int segCountX2 = GetUInt16(buff, offsetTable + 6);
                            //int offsetGlyphIdArray = offsetTable + 14 + segCountX2 * 4 + 2;

                            for (int indexSeg = 0; indexSeg < segCountX2; indexSeg += 2)
                            {
                                int offsetSeg = offsetTable + 14 + indexSeg;
                                int endCode = (int)GetUInt16(buff, offsetSeg);
                                int startCode = (int)GetUInt16(buff, offsetSeg + 2 + segCountX2);
                                int idDelta = GetInt16(buff, offsetSeg + 2 + segCountX2 * 2);
                                int idRangeOffset = (int)GetUInt16(buff, offsetSeg + 2 + segCountX2 * 3);

                                if (startCode != 0xFFFF)
                                {
                                    for (int indexChar = startCode; indexChar <= endCode; indexChar++)
                                    {
                                        int glyphId = 0xFFFF;
                                        if (idRangeOffset == 0)
                                        {
                                            glyphId = idDelta + indexChar;
                                        }
                                        else
                                        {
                                            int glyphIndexAddress = idRangeOffset + 2 * (indexChar - startCode) + (offsetSeg + 2 + segCountX2 * 3);
                                            glyphId = GetUInt16(buff, glyphIndexAddress);
                                            if (glyphId != 0) glyphId += idDelta;
                                        }
                                        charToGlyph[indexChar] = (ushort)glyphId;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
                return charToGlyph;
            }
            #endregion
            
            #region GetTtfInfo
            private TtfInfo GetTtfInfo(byte[] buff, int bufOffset)
            {
                TtfInfo ttf = new TtfInfo();
                ttf.HeaderOffset = bufOffset;
                ttf.SfntVersion = GetUInt32(buff, ttf.HeaderOffset);
                ttf.NumTables = GetUInt16(buff, ttf.HeaderOffset + 4);
                ttf.NumTablesRequired = 0;
                ttf.Tables = new TtfInfo.TableDirectoryItem[ttf.NumTables];

                for (int index = 0; index < ttf.NumTables; index ++)
                {
                    int pos = ttf.HeaderOffset + TtfHeaderSize + index*16;
                    StringBuilder sbTable = new StringBuilder();
                    for (int tempIndex = 0; tempIndex < 4; tempIndex ++)
                    {
                        sbTable.Append((char) GetUInt8(buff, pos + tempIndex));
                    }
                    ttf.Tables[index].Tag = GetUInt32(buff, pos);
                    ttf.Tables[index].TagString = sbTable.ToString();
                    ttf.Tables[index].CheckSum = GetUInt32(buff, pos + 4);
                    ttf.Tables[index].Offset = GetUInt32(buff, pos + 8);
                    ttf.Tables[index].Length = GetUInt32(buff, pos + 12);
                    ttf.Tables[index].NewLength = ttf.Tables[index].Length;
                    for (int tempIndex = 0; tempIndex < TablesNames.Length; tempIndex ++)
                    {
                        if (ttf.Tables[index].TagString == TablesNames[tempIndex])
                        {
                            ttf.Tables[index].Required = true;
                            ttf.NumTablesRequired ++;
                            break;
                        }
                    }

                    switch (ttf.Tables[index].TagString)
                    {
                        case "head":
                            //ttf.HeadCheckSumOffset = (int)ttf.Tables[index].Offset + 8;
                            //SetUInt32(buff, ttf.HeadCheckSumOffset, 0);
                            ttf.IndexToLocFormat = GetUInt16(buff, (int) ttf.Tables[index].Offset + 50);
                            break;
                        case "maxp":
                            ttf.NumGlyphs = GetUInt16(buff, (int) ttf.Tables[index].Offset + 4);
                            break;
                        case "loca":
                            ttf.IndexLocaTable = index;
                            break;
                        case "glyf":
                            ttf.IndexGlyfTable = index;
                            break;
                        case "cmap":
                            ttf.IndexCmapTable = index;
                            break;
                        case "OS/2":
                            GetOtmInfo(buff, (int)ttf.Tables[index].Offset);
                            break;
                        case "name":
                            #region scan for font family name
                            int tableOffset = (int) ttf.Tables[index].Offset;
                            int countNames = GetUInt16(buff, tableOffset + 2);
                            int storageOffset = GetUInt16(buff, tableOffset + 4);
                            for (int indexName = 0; indexName < countNames; indexName ++)
                            {
                                int posName = tableOffset + 6 + indexName*12;
                                int platformID = GetUInt16(buff, posName);
                                int encodingID = GetUInt16(buff, posName + 2);
                                //int languageID = GetUInt16(buff, posName + 4);
                                int nameID = GetUInt16(buff, posName + 6);
                                int lengthName = GetUInt16(buff, posName + 8);
                                int offsetName = GetUInt16(buff, posName + 10);
                                if (((platformID == 3) && (encodingID == 1) && (nameID == 1)) ||
                                    ((platformID == 0) && (encodingID == 3) && (nameID == 1)))
                                {
                                    StringBuilder sbName = new StringBuilder();
                                    for (int indexChar = 0; indexChar < lengthName/2; indexChar ++)
                                    {
                                        sbName.Append((char) GetUInt16(buff, tableOffset + storageOffset + offsetName + indexChar*2));
                                    }
                                    string Name = sbName.ToString();
                                    ttf.FontName[Name] = Name;
                                }
                            }
                            #endregion

                            break;
                    }
                }
                return ttf;
            }
            #endregion

            #region GetOtmInfo
            private void GetOtmInfo(byte[] buff, int offset)
            {
                OUTLINETEXTMETRIC otm = new OUTLINETEXTMETRIC();

                dataBuf = buff;
                posBuf = offset;

                UInt16 version = ReadUInt16();
                Int16 xAvgCharWidth = ReadInt16();
                UInt16 usWeightClass = ReadUInt16();
                UInt16 usWidthClass = ReadUInt16();
                UInt16 fsType = ReadUInt16();
                Int16 ySubscriptXSize = ReadInt16();
                Int16 ySubscriptYSize = ReadInt16();
                Int16 ySubscriptXOffset = ReadInt16();
                Int16 ySubscriptYOffset = ReadInt16();
                Int16 ySuperscriptXSize = ReadInt16();
                Int16 ySuperscriptYSize = ReadInt16();
                Int16 ySuperscriptXOffset = ReadInt16();
                Int16 ySuperscriptYOffset = ReadInt16();
                Int16 yStrikeoutSize = ReadInt16();
                Int16 yStrikeoutPosition = ReadInt16();
                Int16 sFamilyClass = ReadInt16();
                byte[] panose = new byte[10]; ReadUInt32(); ReadUInt32(); ReadUInt16();
                UInt32 ulUnicodeRange1 = ReadUInt32();
                UInt32 ulUnicodeRange2 = ReadUInt32();
                UInt32 ulUnicodeRange3 = ReadUInt32();
                UInt32 ulUnicodeRange4 = ReadUInt32();
                byte[] achVendID = new byte[4]; ReadUInt32();
                UInt16 fsSelection = ReadUInt16();
                UInt16 usFirstCharIndex = ReadUInt16();
                UInt16 usLastCharIndex = ReadUInt16();
                Int16 sTypoAscender = ReadInt16();
                Int16 sTypoDescender = ReadInt16();
                Int16 sTypoLineGap = ReadInt16();
                UInt16 usWinAscent = ReadUInt16();
                UInt16 usWinDescent = ReadUInt16();
                UInt32 ulCodePageRange1 = ReadUInt32();
                UInt32 ulCodePageRange2 = ReadUInt32();
                Int16 sxHeight = ReadInt16();
                Int16 sCapHeight = ReadInt16();
                UInt16 usDefaultChar = ReadUInt16();
                UInt16 usBreakChar = ReadUInt16();
                UInt16 usMaxContext = ReadUInt16();
                //if (version >= 5)
                //{
                //    UInt16 usLowerOpticalPointSize = ReadUInt16();
                //    UInt16 usUpperOpticalPointSize = ReadUInt16();
                //}
                
                dataBuf = null;

                otm.otmAscent = 0;

            }
            #endregion

            #region GetData & SetData
            private byte GetUInt8(byte[] buff, int pos)
            {
                return buff[pos];
            }

            private UInt16 GetUInt16(byte[] buff, int pos)
            {
                bufGetData[0] = buff[pos + 1];
                bufGetData[1] = buff[pos + 0];
                return BitConverter.ToUInt16(bufGetData, 0);
            }

            private UInt32 GetUInt32(byte[] buff, int pos)
            {
                bufGetData[0] = buff[pos + 3];
                bufGetData[1] = buff[pos + 2];
                bufGetData[2] = buff[pos + 1];
                bufGetData[3] = buff[pos + 0];
                return BitConverter.ToUInt32(bufGetData, 0);
            }

            //private sbyte GetInt8(byte[] buff, int pos)
            //{
            //    return (sbyte)buff[pos];
            //}
            private Int16 GetInt16(byte[] buff, int pos)
            {
                return (Int16) GetUInt16(buff, pos);
            }

            //private Int32 GetInt32(byte[] buff, int pos)
            //{
            //    return (Int32)GetUInt32(buff, pos);
            //}

            private void SetUInt16(byte[] buff, int pos, UInt16 value)
            {
                BitConverter.GetBytes(value).CopyTo(bufGetData, 0);
                buff[pos + 0] = bufGetData[1];
                buff[pos + 1] = bufGetData[0];
            }

            private void SetUInt32(byte[] buff, int pos, UInt32 value)
            {
                BitConverter.GetBytes(value).CopyTo(bufGetData, 0);
                buff[pos + 0] = bufGetData[3];
                buff[pos + 1] = bufGetData[2];
                buff[pos + 2] = bufGetData[1];
                buff[pos + 3] = bufGetData[0];
            }


            private byte ReadUInt8()
            {
                return dataBuf[posBuf++];
            }
            private Int16 ReadInt16()
            {
                bufGetData[1] = dataBuf[posBuf++];
                bufGetData[0] = dataBuf[posBuf++];
                return BitConverter.ToInt16(bufGetData, 0);
            }
            private UInt16 ReadUInt16()
            {
                bufGetData[1] = dataBuf[posBuf++];
                bufGetData[0] = dataBuf[posBuf++];
                return BitConverter.ToUInt16(bufGetData, 0);
            }
            private UInt32 ReadUInt32()
            {
                bufGetData[3] = dataBuf[posBuf++];
                bufGetData[2] = dataBuf[posBuf++];
                bufGetData[1] = dataBuf[posBuf++];
                bufGetData[0] = dataBuf[posBuf++];
                return BitConverter.ToUInt32(bufGetData, 0);
            }
            #endregion

            #endregion
        }
    }
}
