﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Gauge;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.PropertyGrid;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Stimulsoft.Report.Components.Gauge.Primitives
{
    public abstract class StiRangeBase : 
        ICloneable,
        IStiPropertyGridObject
    {
        #region IStiJsonReportObject.override

        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();
            
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyStringNullOrEmpty("BorderBrush", StiJsonReportObjectHelper.Serialize.JBrush(BorderBrush));
            jObject.AddPropertyFloat("BorderWidth", BorderWidth, 1f);
            jObject.AddPropertyFloat("StartValue", StartValue, 0f);
            jObject.AddPropertyFloat("EndValue", EndValue, 0f);
            jObject.AddPropertyFloat("StartWidth", StartWidth, 0f);
            jObject.AddPropertyFloat("EndWidth", EndWidth, 0f);
            jObject.AddPropertyEnum("Placement", Placement);
            jObject.AddPropertyFloat("Offset", Offset, 0f);

            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Brush":
                        this.Brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "BorderBrush":
                        this.BorderBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "BorderWidth":
                        this.BorderWidth = property.Value.ToObject<float>();
                        break;

                    case "StartValue":
                        this.StartValue = property.Value.ToObject<float>();
                        break;

                    case "EndValue":
                        this.EndValue = property.Value.ToObject<float>();
                        break;

                    case "StartWidth":
                        this.StartWidth = property.Value.ToObject<float>();
                        break;

                    case "EndWidth":
                        this.EndWidth = property.Value.ToObject<float>();
                        break;

                    case "Placement":
                        this.Placement = (StiPlacement)Enum.Parse(typeof(StiPlacement), property.Value.ToObject<string>());
                        break;

                    case "Offset":
                        this.Offset = property.Value.ToObject<float>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public abstract StiComponentId ComponentId
        {
            get;
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return null;
            }
        }

        public abstract StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level);

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

        #region ICloneable
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            var range = (StiRangeBase)this.MemberwiseClone();

            range.Brush = (StiBrush)this.Brush.Clone();
            range.BorderBrush = (StiBrush)this.BorderBrush.Clone();

            return range;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a brush to fill a component.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [Description("Gets or sets a brush to fill a component.")]
        [StiOrder(StiPropertyOrder.AppearanceBrush)]
        [StiPropertyLevel(StiLevel.Basic)]
        public StiBrush Brush { get; set; } = new StiSolidBrush(Color.White);

        /// <summary>
        /// Gets or sets the border of the component.
        /// </summary>
        [StiSerializable]
        [StiCategory("Appearance")]
        [StiOrder(StiPropertyOrder.AppearanceBorder)]
        [StiPropertyLevel(StiLevel.Basic)]
        [Description("Gets or sets the border of the component.")]
        public StiBrush BorderBrush { get; set; } = new StiEmptyBrush();

        /// <summary>
        /// Gets or sets the border thickness of the component.
        /// </summary>
        [DefaultValue(1f)]
        [StiSerializable]
        [StiCategory("Appearance")]
        [StiOrder(200)]
        [StiPropertyLevel(StiLevel.Basic)]
        [Description("Gets or sets the border thickness of the component.")]
        public float BorderWidth { get; set; } = 1f;

        /// <summary>
        /// Gets or sets start value of the range.
        /// </summary>        
        [DefaultValue(0f)]
        [StiSerializable]
        [StiCategory("Value")]
        [Description("Gets or sets start value of the range.")]
        [StiPropertyLevel(StiLevel.Basic)]
        public float StartValue { get; set; } = 0f;

        /// <summary>
        /// Gets or sets end value of the range.
        /// </summary>        
        [DefaultValue(0f)]
        [StiSerializable]
        [StiCategory("Value")]
        [Description("Gets or sets end value of the range.")]
        [StiPropertyLevel(StiLevel.Basic)]
        public float EndValue { get; set; } = 0f;

        /// <summary>
        /// Gets or sets start width of the range bar.
        /// </summary>        
        [DefaultValue(0f)]
        [StiSerializable]
        [StiCategory("Behavior")]
        [Description("Gets or sets start width of the range bar.")]
        [StiPropertyLevel(StiLevel.Basic)]
        public float StartWidth { get; set; } = 0f;

        /// <summary>
        /// Gets or sets end width of the range bar.
        /// </summary>        
        [DefaultValue(0f)]
        [StiSerializable]
        [StiCategory("Behavior")]
        [Description("Gets or sets end width of the range bar.")]
        [StiPropertyLevel(StiLevel.Basic)]
        public float EndWidth { get; set; } = 0f;

        /// <summary>
        /// Gets or sets the placement of the component.
        /// </summary>
        [DefaultValue(StiPlacement.Overlay)]
        [StiSerializable]
        [StiCategory("Behavior")]
        [TypeConverter(typeof(StiEnumConverter))]
        [StiPropertyLevel(StiLevel.Basic)]
        [Description("Gets or sets the placement of the component.")]
        public StiPlacement Placement { get; set; } = StiPlacement.Overlay;

        /// <summary>
        /// Gets or sets the offset ratio of an item.
        /// </summary>
        [DefaultValue(0f)]
        [StiSerializable]
        [StiCategory("Behavior")]
        [StiPropertyLevel(StiLevel.Basic)]
        [Description("Gets or sets the offset ratio of an item.")]
        public float Offset { get; set; } = 0f;

        internal StiScaleRangeList rangeList;
        [Browsable(false)]
        public StiScaleRangeList RangeList
        {
            get
            {
                return this.rangeList;
            }
        }

        [Browsable(false)]
        public abstract string LocalizeName
        {
            get;
        }
        #endregion

        #region Methods abstract
        protected internal abstract void DrawRange(StiGaugeContextPainter context, StiScaleBase scale);
        #endregion

        #region Methods override

        public virtual StiRangeBase CreateNew()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}