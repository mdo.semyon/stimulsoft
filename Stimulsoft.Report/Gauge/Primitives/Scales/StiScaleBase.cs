﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.ComponentModel;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Design;
using Stimulsoft.Base;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.Gauge.Collections;
using Stimulsoft.Report.Gauge.Helpers;
using Stimulsoft.Report.Gauge.Primitives;
using Stimulsoft.Report.Gauge;
using System.Collections;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.PropertyGrid;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Components.Gauge.Primitives
{
    public abstract class StiScaleBase : 
        StiElementBase,
        IStiPropertyGridObject,
        IStiJsonReportObject
    {
        #region IStiJsonReportObject
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();
            
            jObject.AddPropertyFloat("Left", Left, 0);
            jObject.AddPropertyFloat("Top", Top, 0);
            jObject.AddPropertyFloat("StartWidth", startWidth, 0.1f);
            jObject.AddPropertyFloat("EndWidth", endWidth, 0.1f);
            jObject.AddPropertyFloat("MajorInterval", MajorInterval, 10f);
            jObject.AddPropertyFloat("MinorInterval", MinorInterval, 5f);
            jObject.AddPropertyBool("IsReversed", IsReversed, false);
            jObject.AddPropertyFloat("Minimum", minimum, 0f);
            jObject.AddPropertyFloat("Maximum", maximum, 100f);
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyStringNullOrEmpty("BorderBrush", StiJsonReportObjectHelper.Serialize.JBrush(BorderBrush));
            jObject.AddPropertyJObject("Items", Items.SaveToJsonObject(mode));

            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {

                    case "Left":
                        this.Left = property.Value.ToObject<float>();
                        break;

                    case "Top":
                        this.Top = property.Value.ToObject<float>();
                        break;

                    case "StartWidth":
                        this.StartWidth = property.Value.ToObject<float>();
                        break;

                    case "EndWidth":
                        this.EndWidth = property.Value.ToObject<float>();
                        break;

                    case "MajorInterval":
                        this.MajorInterval = property.Value.ToObject<float>();
                        break;

                    case "MinorInterval":
                        this.MinorInterval = property.Value.ToObject<float>();
                        break;

                    case "IsReversed":
                        this.IsReversed = property.Value.ToObject<bool>();
                        break;

                    case "Minimum":
                        this.Minimum = property.Value.ToObject<float>();
                        break;

                    case "Maximum":
                        this.Maximum = property.Value.ToObject<float>();
                        break;
                        
                    case "Brush":
                        this.Brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "BorderBrush":
                        this.BorderBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "Items":
                        this.Items.LoadFromJsonObject((JObject)property.Value);
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public abstract StiComponentId ComponentId
        {
            get;
        }

        [Browsable(false)]
        public string PropName
        {
            get
            {
                return null;
            }
        }

        public abstract StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level);

        public StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

        #region ICloneable
        public override object Clone()
        {
            var scale = (StiScaleBase)base.Clone();

            scale.Brush = (StiBrush)this.Brush.Clone();
            scale.BorderBrush = (StiBrush)this.BorderBrush.Clone();

            scale.Items = new StiGaugeElementCollection(scale);
            lock (((ICollection)this.Items).SyncRoot)
            {
                foreach (StiGaugeElement element in this.Items) scale.Items.Add((StiGaugeElement)element.Clone());
            }

            if (this is StiLinearScale)
            {
                scale.barGeometry = new StiLinearBarGeometry((StiLinearScale)scale);
            }
            else if(this is StiRadialScale)
            {
                scale.barGeometry = new StiRadialBarGeometry((StiRadialScale)scale);
            }
            
            return scale;
        }
        #endregion

        #region class ScaleHelper
        internal class StiScaleHelper
        {
            public float ActualMinimum = 0f;
            public float ActualMaximum = 100f;
            public float MinWidth = 0.1f;
            public float MaxWidth = 0.1f;

            private float totalLength = 100f;
            public float TotalLength
            {
                get
                {
                    return this.totalLength;
                }
                set
                {
                    if (value == 0)
                        this.totalLength = 1f;
                    else
                        this.totalLength = value;
                }
            }
        }
        #endregion

        #region Fields
        internal IStiScaleBarGeometry barGeometry;
        internal StiScaleHelper ScaleHelper = new StiScaleHelper();
        #endregion

        #region Properties.Internal
        [Browsable(false)]
        internal bool IsUp
        {
            get
            {
                bool isUp = (this.startWidth < this.endWidth);
                if (this.IsReversed) isUp = !isUp;
                return isUp;
            }
        }

        [Browsable(false)]
        [StiSerializable(StiSerializationVisibility.Class, StiSerializeTypes.SerializeToAll)]
        [DefaultValue(null)]
        public StiGauge Gauge { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets X coordinate of the scale relative to height of the scale container.
        /// </summary>
        [DefaultValue(0f)]
        [StiSerializable]
        [StiCategory("Position")]
        [Description("Gets or sets X coordinate of the scale relative to height of the scale container.")]
        [StiPropertyLevel(StiLevel.Basic)]
        public float Left { get; set; } = 0f;

        /// <summary>
        /// Gets or sets Y coordinate of the scale relative to height of the scale container.
        /// </summary>
        [DefaultValue(0f)]
        [StiSerializable]
        [StiCategory("Position")]
        [Description("Gets or sets Y coordinate of the scale relative to height of the scale container.")]
        [StiPropertyLevel(StiLevel.Basic)]
        public float Top { get; set; } = 0f;

        private float startWidth = 0.1f;
        /// <summary>
        /// Gets or sets start width of the scale bar.
        /// </summary>
        [DefaultValue(0.1f)]
        [StiSerializable]
        [StiCategory("Scale")]
        [Description("Gets or sets start width of the scale bar.")]
        [StiOrder(StiPropertyOrder.ScaleStartWidth)]
        [StiPropertyLevel(StiLevel.Basic)]
        public float StartWidth
        {
            get
            {
                return this.startWidth;
            }
            set
            {
                this.startWidth = value;
                CalculateWidthScaleHelper();
            }
        }

        private float endWidth = 0.1f;
        /// <summary>
        /// Gets or sets end width of the scale bar.
        /// </summary>
        [DefaultValue(0.1f)]
        [StiSerializable]
        [StiCategory("Scale")]
        [Description("Gets or sets end width of the scale bar.")]
        [StiOrder(StiPropertyOrder.ScaleEndWidth)]
        [StiPropertyLevel(StiLevel.Basic)]
        public float EndWidth
        {
            get
            {
                return this.endWidth;
            }
            set
            {
                this.endWidth = value;
                CalculateWidthScaleHelper();
            }
        }

        /// <summary>
        /// Gets or sets the major interval.
        /// </summary>
        [DefaultValue(10f)]
        [StiSerializable]
        [StiCategory("Scale")]
        [Description("Gets or sets the major interval.")]
        [StiOrder(StiPropertyOrder.ScaleMajorInterval)]
        [StiPropertyLevel(StiLevel.Basic)]
        public float MajorInterval { get; set; } = 10f;

        /// <summary>
        /// Gets or sets the minor interval.
        /// </summary>
        [DefaultValue(5f)]
        [StiSerializable]
        [StiCategory("Scale")]
        [Description("Gets or sets the minor interval.")]
        [StiOrder(StiPropertyOrder.ScaleMinorInterval)]
        [StiPropertyLevel(StiLevel.Basic)]
        public float MinorInterval { get; set; } = 5f;

        private float minimum = 0f;
        /// <summary>
        /// Gets or sets start value of the scale.
        /// </summary>
        [DefaultValue(0f)]
        [StiSerializable]
        [StiCategory("Scale")]
        [Description("Gets or sets start value of the scale.")]
        [StiOrder(StiPropertyOrder.ScaleMinimum)]
        [StiPropertyLevel(StiLevel.Basic)]
        public float Minimum
        {
            get
            {
                return this.minimum;
            }
            set
            {
                this.minimum = value;
                CalculateMinMaxScaleHelper();
            }
        }

        private float maximum = 100f;
        /// <summary>
        /// Gets or sets end value of the scale.
        /// </summary>
        [DefaultValue(100f)]
        [StiSerializable]
        [StiCategory("Scale")]
        [Description("Gets or sets end value of the scale.")]
        [StiOrder(StiPropertyOrder.ScaleMaximum)]
        [StiPropertyLevel(StiLevel.Basic)]
        public float Maximum
        {
            get
            {
                return this.maximum;
            }
            set
            {
                this.maximum = value;
                CalculateMinMaxScaleHelper();
            }
        }

        /// <summary>
        /// Gets or sets value that indicates whether the scale should be shown in reverse mode.
        /// </summary>
        [DefaultValue(false)]
        [StiSerializable]
        [StiCategory("Scale")]
        [Description("Gets or sets value that indicates whether the scale should be shown in reverse mode.")]
        [StiOrder(StiPropertyOrder.ScaleIsReversed)]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiPropertyLevel(StiLevel.Basic)]
        public bool IsReversed { get; set; }

        /// <summary>
        /// Gets or sets a brush to fill a component.
        /// </summary>
        [StiSerializable]
        [StiPropertyLevel(StiLevel.Basic)]
        [StiCategory("Appearance")]
        [Description("Gets or sets a brush to fill a component.")]
        [StiOrder(StiPropertyOrder.AppearanceBrush)]
        public StiBrush Brush { get; set; } = new StiSolidBrush(Color.FromArgb(50, Color.White));

        /// <summary>
        /// Gets or sets the border of the component.
        /// </summary>
        [StiSerializable]
        [StiPropertyLevel(StiLevel.Basic)]
        [StiCategory("Appearance")]
        [StiOrder(StiPropertyOrder.AppearanceBorder)]
        [Description("Gets or sets the border of the component.")]
        public StiBrush BorderBrush { get; set; } = new StiSolidBrush(Color.FromArgb(150, Color.White));

        [Browsable(false)]
        [StiSerializable(StiSerializationVisibility.List)]
        public StiGaugeElementCollection Items { get; set; }
        #endregion

        #region Properties abstract
        [Browsable(false)]
        public abstract StiGaugeElemenType ScaleType
        {
            get;
        }
        #endregion

        #region Methods
        internal void Prepare(StiGauge gauge)
        {
            foreach (StiGaugeElement element in this.Items)
            {
                element.PrepareGaugeElement();
            }
        }

        internal void CalculateMinMaxScaleHelper()
        {
            if (this.Minimum > this.Maximum)
            {
                this.ScaleHelper.ActualMaximum = this.Maximum;
                this.ScaleHelper.ActualMinimum = this.Minimum;
            }
            else
            {
                this.ScaleHelper.ActualMinimum = this.Minimum;
                this.ScaleHelper.ActualMaximum = this.Maximum;
            }

            this.ScaleHelper.TotalLength = this.ScaleHelper.ActualMaximum - this.ScaleHelper.ActualMinimum;
        }

        internal void CalculateWidthScaleHelper()
        {
            if (this.StartWidth > this.EndWidth)
            {
                this.ScaleHelper.MaxWidth = this.StartWidth;
                this.ScaleHelper.MinWidth = this.EndWidth;
            }
            else
            {
                this.ScaleHelper.MaxWidth = this.EndWidth;
                this.ScaleHelper.MinWidth = this.StartWidth;
            }
        }

        internal float GetPosition(float value)
        {
            return StiMathHelper.Length(this.ScaleHelper.ActualMinimum, value) / this.ScaleHelper.TotalLength;
        }
        #endregion

        #region Methods abstract
        protected abstract void InteractiveClick(MouseEventArgs e);
        #endregion

        #region Methods virtual
        public virtual StiScaleBase CreateNew()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Methods override
        protected internal override void DrawElement(StiGaugeContextPainter context)
        {
            if (this.Gauge != null)
            {
                barGeometry.DrawScaleGeometry(context);

                int index = 0;
                while (index < this.Items.Count)
                {
                    this.Items[index].DrawElement(context);
                    index++;
                }
            }
        }
        #endregion

        public StiScaleBase()
        {
            this.Items = new StiGaugeElementCollection(this);
        }
    }
}