﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft


using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Report.Components.Gauge.Primitives;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.Gauge.Collections;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.PropertyGrid;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Stimulsoft.Report.Gauge
{
    [StiToolbox(true)]
    [StiServiceBitmap(typeof(StiComponent), "Stimulsoft.Report.Images.Components.StiGauge.png")]
    [StiServiceCategoryBitmap(typeof(StiComponent), "Stimulsoft.Report.Images.Components.catInfographics.png")]
    [StiGdiPainter(typeof(StiGaugeGdiPainter))]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiGaugeWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
    [StiDesigner("Stimulsoft.Gauge.Design.StiGaugeDesigner, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfDesigner("Stimulsoft.Report.WpfDesign.StiWpfGaugeDesigner, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
    [StiV2Builder(typeof(StiGaugeV2Builder))]
    [StiContextTool(typeof(IStiComponentDesigner))]
    public class StiGauge : 
        StiComponent,
        IStiExportImageExtended,
        IStiBorder,
        IStiBrush
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // NonSerialized
            jObject.RemoveProperty("ComponentStyle");
            jObject.RemoveProperty("UseParentStyles");
            jObject.RemoveProperty("CanGrow");
            jObject.RemoveProperty("CanShrink");
            jObject.RemoveProperty("GrowToHeight");
            jObject.RemoveProperty("Interaction");
            jObject.RemoveProperty("IsAnimation");

            // StiGauge
            jObject.AddPropertyDecimal("Minimum", Minimum, 0M);
            jObject.AddPropertyDecimal("Maximum", Maximum, 100M);
            jObject.AddPropertyStringNullOrEmpty("Border", StiJsonReportObjectHelper.Serialize.JBorder(Border));
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyEnum("Type", Type, StiGaugeType.FullCircular);
            jObject.AddPropertyEnum("CalculationMode", CalculationMode, StiGaugeCalculationMode.Auto);
            jObject.AddPropertyJObject("Scales", Scales.SaveToJsonObject(mode));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Type":
                        this.Type = (StiGaugeType)Enum.Parse(typeof(StiGaugeType), property.Value.ToObject<string>());
                        break;

                    case "Minimum":
                        this.Minimum = property.Value.ToObject<decimal>();
                        break;

                    case "Maximum":
                        this.Maximum = property.Value.ToObject<decimal>();
                        break;

                    case "CalculationMode":
                        this.CalculationMode = (StiGaugeCalculationMode)Enum.Parse(typeof(StiGaugeCalculationMode), property.Value.ToObject<string>());
                        break;

                    case "Border":
                        this.Border = StiJsonReportObjectHelper.Deserialize.Border(property);
                        break;

                    case "Brush":
                        this.Brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;
                                                                      
                    case "Scales":
                        this.Scales.LoadFromJsonObject((JObject)property.Value);
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        [Browsable(false)]
        public override StiComponentId ComponentId => StiComponentId.StiGauge;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var checkBoxHelper = new StiPropertyCollection();
            StiPropertyObject[] list;

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height(),
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height(),
                    propHelper.MinSize(),
                    propHelper.MaxSize()
                };
            }
            checkBoxHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            list = new[]
            {
                propHelper.Brush(),
                propHelper.Border(),
                propHelper.Conditions()
            };
            checkBoxHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Enabled()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.AnchorMode(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.AnchorMode(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.Printable(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            checkBoxHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Name()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias(),
                    propHelper.Restrictions(),
                    propHelper.Locked(),
                    propHelper.Linked()
                };
            }
            checkBoxHelper.Add(StiPropertyCategories.Design, list);

            return checkBoxHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }

        #endregion

        #region ICloneable override
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public override object Clone(bool cloneProperties)
        {
            var gauge = (StiGauge)base.Clone(cloneProperties);
            
            gauge.Scales = new StiScaleCollection(gauge);

            lock (((ICollection)this.Scales).SyncRoot)
            {
                foreach (StiScaleBase scale in this.Scales) gauge.Scales.Add((StiScaleBase)scale.Clone());
            }

            return gauge;
        }
        #endregion		

        #region IStiExportImageExtended
        public virtual Image GetImage(ref float zoom)
        {
            return GetImage(ref zoom, StiExportFormat.None);
        }

        public virtual Image GetImage(ref float zoom, StiExportFormat format)
        {
            var painter = StiPainter.GetPainter(this.GetType(), StiGuiMode.Gdi);
            return painter.GetImage(this, ref zoom, format);
        }

        [Browsable(false)]
        public override bool IsExportAsImage(StiExportFormat format)
        {
            return format != StiExportFormat.Pdf;
        }
        #endregion

        #region IStiBorder
        /// <summary>
        /// Gets or sets frame of the component.
        /// </summary>
        [StiCategory("Appearance")]
        [StiOrder(StiPropertyOrder.AppearanceBorder)]
        [StiSerializable]
        [Description("Gets or sets frame of the component.")]
        public StiBorder Border { get; set; } = new StiBorder();
        #endregion

        #region IStiBrush
        /// <summary>
        /// Gets or sets a brush to fill a component.
        /// </summary>
        [StiCategory("Appearance")]
        [StiOrder(StiPropertyOrder.AppearanceBrush)]
        [StiSerializable]
        [Description("Gets or sets a brush to fill a component.")]
        public StiBrush Brush { get; set; } = new StiSolidBrush(Color.Transparent);
        #endregion

        #region StiComponent override
        public override int ToolboxPosition => (int)StiComponentToolboxPosition.Gauge;

        /// <summary>
        /// Gets a localized name of the component category.
        /// </summary>
        public override string LocalizedCategory => StiLocalization.Get("Report", "Infographics");

        public override string LocalizedName => StiLocalization.Get("Components", "StiGauge");

        public override RectangleD DefaultClientRectangle => new RectangleD(0, 0, 240, 240);
        #endregion

        #region Browsable(false)
        [StiNonSerialized]
        [Browsable(false)]
        public override string ComponentStyle
        {
            get
            {
                return base.ComponentStyle;
            }
            set
            {
                
            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public override bool UseParentStyles
        {
            get
            {
                return base.UseParentStyles;
            }
            set
            {
                
            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public override bool CanGrow
        {
            get
            {
                return base.CanGrow;
            }
            set
            {
                
            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public override bool CanShrink
        {
            get
            {
                return base.CanShrink;
            }
            set
            {

            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public override bool GrowToHeight
        {
            get
            {
                return base.GrowToHeight;
            }
            set
            {

            }
        }

        [StiNonSerialized]
        [Browsable(false)]
        public override StiInteraction Interaction
        {
            get
            {
                return base.Interaction;
            }
            set
            {
                base.Interaction = value;
            }
        }
        #endregion

        #region Properties

        [StiSerializable]
        [DefaultValue(0d)]
        [StiBrowsable(false)]
        [Browsable(false)]
        public decimal Minimum { get; set; }

        [StiSerializable]
        [DefaultValue(100d)]
        [StiBrowsable(false)]
        [Browsable(false)]
        public decimal Maximum { get; set; } = 100M;

        [StiSerializable]
        [DefaultValue(StiGaugeType.FullCircular)]
        [Browsable(false)]
        [StiBrowsable(false)]
        [TypeConverter(typeof(StiEnumConverter))]
        public StiGaugeType Type { get; set; } = StiGaugeType.FullCircular;

        [StiSerializable]
        [DefaultValue(StiGaugeCalculationMode.Auto)]
        [Browsable(false)]
        [StiBrowsable(false)]
        [TypeConverter(typeof(StiEnumConverter))]
        public StiGaugeCalculationMode CalculationMode { get; set; } = StiGaugeCalculationMode.Auto;

        [Browsable(false)]
        public StiGaugeContextPainter Painter { get; set; }

        private IStiGaugeStyle style = new StiGaugeStyleXF26();
        /// <summary>
        /// Gets or sets style of the chart.
        /// </summary>
        [TypeConverter(typeof(Design.StiGaugeStyleConverter))]
        [StiSerializable(StiSerializationVisibility.Class)]
        [Browsable(false)]
        [Description("Gets or sets style of the chart.")]
        public IStiGaugeStyle Style
        {
            get
            {
                return style;
            }
            set
            {
                if (style == value) return;

                style = value;
                if (value != null)
                    value.Core.Gauge = this;
            }
        }

        [StiSerializable]
        [Browsable(false)]
        public string CustomStyleName { get; set; } = "";

        [Browsable(false)]
        [StiSerializable(StiSerializationVisibility.List)]
        public StiScaleCollection Scales { get; set; }

        [StiNonSerialized]
        [TypeConverter(typeof(StiBoolConverter))]
        [Browsable(false)]
        public bool IsAnimation { get; set; }
        #endregion

        #region Methods override
        public void DrawGauge(StiGaugeContextPainter context)
        {
            this.ApplyStyle(this.Style);

            DrawGaugeInternal(context);
        }

        public void DrawGaugeInternal(StiGaugeContextPainter context)
        {
            var index = -1;
            while (++index < this.Scales.Count)
            {
                var scale = this.Scales[index];
                if (scale != null)
                {
                    scale.barGeometry.CheckRectGeometry(context.Rect);
                    scale.DrawElement(context);
                }
            }
        }

        public override StiComponent CreateNew()
        {
            return new StiGauge();
        }

        public void ApplyStyle(IStiGaugeStyle style)
        {
            this.Brush = style.Core.Brush;
            //this.Border.Color = style.Core.BorderColor;
            //this.Border.Size = style.Core.BorderWidth;

            foreach (StiScaleBase scale in this.Scales)
            {
                scale.ApplyStyle(style);
            }
        }
        #endregion

        /// <summary>
		/// Creates a new StiGauge.
		/// </summary>
		public StiGauge() : this(RectangleD.Empty)
		{
		}

		/// <summary>
		/// Creates a new StiGauge.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiGauge(RectangleD rect) 
            : base(rect)
		{
            Scales = new StiScaleCollection(this);
            PlaceOnToolbox = false;
		}
    }
}