#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components.Gauge;
using Stimulsoft.Report.Components.Gauge.Primitives;
using Stimulsoft.Report.Units;
using System;
using System.Collections;
using System.Drawing;
using System.Globalization;
using System.Threading;

namespace Stimulsoft.Report.Gauge.Helpers
{
    public static class StiGaugeHelper
    {
        #region Fields

        public static TimeSpan GlobalDurationElement = TimeSpan.FromMilliseconds(1800);
        public static TimeSpan GlobalBeginTimeElement = TimeSpan.FromMilliseconds(300);

        #endregion

        #region Methods.Builder.Helper
        public static float GetFloatValueFromObject(object valueObj, StiScaleBase scale)
        {
            float result = scale.Minimum;

            if (valueObj != null)
            {
                if (valueObj is string)
                {
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

                    if (float.TryParse((string)valueObj, out result))
                    {
                        if (result < scale.Minimum)
                            result = scale.Minimum;
                        else if (result > scale.Maximum)
                            result = scale.Maximum;
                    }
                    else
                    {
                        result = scale.Minimum;
                    }

                    Thread.CurrentThread.CurrentCulture = currentCulture;
                }
                else
                {
                    result = Convert.ToSingle(valueObj);
                }
            }

            return result;
        }

        public static float GetFloatValueFromObject(object valueObj, float defaultValue)
        {
            float result = defaultValue;

            if (valueObj != null)
            {
                if (valueObj is string)
                {
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

                    if (!float.TryParse((string)valueObj, out result))
                    {
                        result = defaultValue;
                    }

                    Thread.CurrentThread.CurrentCulture = currentCulture;
                }
                else
                {
                    result = Convert.ToSingle(valueObj);
                }
            }

            return result;
        }

        public static float[] GetFloatArrayValueFromString(object value)
        {
            string valueStr = value as string;
            if (string.IsNullOrEmpty(valueStr))
                return null;

            string[] strs = valueStr.Split(';');
            float[] values = new float[strs.Length];

            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

            int index = -1;
            while(++index < strs.Length)
            {
                float result;
                if (float.TryParse(strs[index], out result))
                {
                    values[index] = result;
                }
                else
                {
                    values = null;
                    break;
                }
            }

            Thread.CurrentThread.CurrentCulture = currentCulture;

            return values;
        }
        #endregion

        #region Init Gauges
        private static void InitializeGauge(StiGauge gauge, double width, double height)
        {
            gauge.Scales.Clear();
            
            if (gauge.Page != null)
            {
                StiUnit unit = gauge.Page.Unit;
                gauge.ClientRectangle = new RectangleD(gauge.Left, gauge.Top, unit.ConvertFromHInches(width), unit.ConvertFromHInches(height));
            }
        }

        private static void InitializeName(StiGauge gauge, StiReport report)
        {
            if (string.IsNullOrEmpty(gauge.Name))
            {
                string name = StiNameCreation.CreateSimpleName(report, StiNameCreation.GenerateName(gauge));
                gauge.Name = name;

                int index = -1;
                while(++index < gauge.Scales.Count)
                {
                    Hashtable tableNames = new Hashtable();

                    StiScaleBase scale = gauge.Scales[index];

                    int index1 = -1;
                    while(++index1 < scale.Items.Count)
                    {
                        StiGaugeElement element = scale.Items[index1];

                        Type type = element.GetType();
                        int count = 0;
                        if (tableNames.ContainsKey(type))
                            count = (int)tableNames[type];
                        count++;
                        tableNames[type] = count;                        
                    }
                }
            }
        }

        public static void CheckGaugeName(StiGauge gauge)
        {
            string name = gauge.Name;

            int index = -1;
            while(++index < gauge.Scales.Count)
            {
                Hashtable tableNames = new Hashtable();

                StiScaleBase scale = gauge.Scales[index];

                int index1 = -1;
                while(++index1 < scale.Items.Count)
                {
                    StiGaugeElement element = scale.Items[index1];

                    Type type = element.GetType();
                    int count = 0;
                    if (tableNames.ContainsKey(type))
                        count = (int)tableNames[type];
                    count++;
                    tableNames[type] = count;                    
                }
            }
        }

        public static void SimpleRadialGauge(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 250, 250);

            StiRadialScale radialScale1 = new StiRadialScale();
            radialScale1.StartAngle = 0f;
            radialScale1.SweepAngle = 340;
            radialScale1.StartWidth = 0.05f;
            radialScale1.EndWidth = 0.05f;
            radialScale1.Minimum = 0f;
            radialScale1.Maximum = 100f;
            radialScale1.MajorInterval = 10;
            radialScale1.MinorInterval = 5;
            radialScale1.Brush = new StiSolidBrush(Color.FromArgb(238, 238, 238));

            StiRadialTickMarkMajor tickMarkMajor1 = new StiRadialTickMarkMajor();
            tickMarkMajor1.Placement = StiPlacement.Overlay;
            tickMarkMajor1.OffsetAngle = -2;
            tickMarkMajor1.RelativeWidth = 0.05f;
            tickMarkMajor1.RelativeHeight = 0.02f;
            tickMarkMajor1.Skin = StiTickMarkSkin.Rectangle;
            tickMarkMajor1.Brush = new StiSolidBrush(Color.Black);

            StiRadialTickMarkMinor tickMarkMinor1 = new StiRadialTickMarkMinor();
            tickMarkMinor1.Placement = StiPlacement.Overlay;
            tickMarkMinor1.RelativeWidth = 0.01f;
            tickMarkMinor1.RelativeHeight = 0.01f;
            tickMarkMinor1.Skin = StiTickMarkSkin.Rectangle;

            StiRadialTickLabelMajor tickLabelMajor1 = new StiRadialTickLabelMajor();
            
            StiNeedle needle1 = new StiNeedle();
            needle1.RelativeHeight = 0.06f;
            needle1.RelativeWidth = 0.45f;
            needle1.StartWidth = 0.1f;
            needle1.EndWidth = 1f;
            needle1.CapBrush = new StiSolidBrush(Color.White);
            needle1.CapBorderBrush = new StiSolidBrush(Color.FromArgb(244, 67, 54));
            needle1.CapBorderWidth = 2;
            needle1.Brush = new StiSolidBrush(Color.FromArgb(244, 67, 54));
            needle1.Value = new StiValueExpression("60");

            radialScale1.Items.Add(tickMarkMajor1);
            radialScale1.Items.Add(tickMarkMinor1);
            radialScale1.Items.Add(tickLabelMajor1);
            radialScale1.Items.Add(needle1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);
        }

        public static void RadialTwoScalesGauge(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 250, 250);

            StiRadialScale radialScale1 = new StiRadialScale();
            radialScale1.StartAngle = 270f;
            radialScale1.SweepAngle = 360;
            radialScale1.StartWidth = 0.05f;
            radialScale1.EndWidth = 0.05f;
            radialScale1.Minimum = 0f;
            radialScale1.Maximum = 60f;
            radialScale1.Radius = 0.5f;
            radialScale1.MajorInterval = 10;
            radialScale1.MinorInterval = 1;
            radialScale1.Brush = new StiSolidBrush(Color.FromArgb(238, 238, 238));

            StiRadialTickMarkMajor tickMarkMajor1 = new StiRadialTickMarkMajor();
            tickMarkMajor1.Placement = StiPlacement.Overlay;
            tickMarkMajor1.RelativeWidth = 0.05f;
            tickMarkMajor1.OffsetAngle = -2f;
            tickMarkMajor1.RelativeHeight = 0.03f;
            tickMarkMajor1.Skin = StiTickMarkSkin.Rectangle;
            tickMarkMajor1.Brush = new StiSolidBrush(Color.Black);

            StiRadialTickLabelMajor tickLabelMajor1 = new StiRadialTickLabelMajor();
            tickLabelMajor1.MinimumValue = 1;
            tickLabelMajor1.Font = new Font("Arial", 7f);

            radialScale1.Items.Add(tickMarkMajor1);
            radialScale1.Items.Add(tickLabelMajor1);
            
            gauge.Scales.Add(radialScale1);

            StiRadialScale radialScale2 = new StiRadialScale();
            radialScale2.StartAngle = 270f;
            radialScale2.SweepAngle = 360;
            radialScale2.StartWidth = 0.05f;
            radialScale2.EndWidth = 0.05f;
            radialScale2.Minimum = 0f;
            radialScale2.Maximum = 12f;
            radialScale2.Radius = 0.75f;
            radialScale2.MajorInterval = 1;
            radialScale2.MinorInterval = 0;
            radialScale2.Brush = new StiSolidBrush(Color.FromArgb(238, 238, 238));

            StiRadialTickMarkMajor tickMarkMajor2 = new StiRadialTickMarkMajor();
            tickMarkMajor2.MinimumValue = 1;
            tickMarkMajor2.Placement = StiPlacement.Overlay;
            tickMarkMajor2.OffsetAngle = -2f;
            tickMarkMajor2.RelativeHeight = 0.03f;
            tickMarkMajor2.RelativeWidth = 0.05f;            
            tickMarkMajor2.Skin = StiTickMarkSkin.Rectangle;
            tickMarkMajor2.Brush = new StiSolidBrush(Color.Black);
            tickMarkMajor2.BorderBrush = new StiSolidBrush(Color.White);
            tickMarkMajor2.BorderWidth = 3;

            StiRadialTickLabelMajor tickLabelMajor2 = new StiRadialTickLabelMajor();
            tickLabelMajor2.MinimumValue = 1;

            StiNeedle needle1 = new StiNeedle();
            needle1.Value = new StiValueExpression("2");
            needle1.OffsetNeedle = 0.15f;
            needle1.StartWidth = 0.05f;
            needle1.EndWidth = 0.5f;
            needle1.RelativeHeight = 0.12f;
            needle1.RelativeWidth = 0.6f;
            needle1.CapBrush = new StiSolidBrush(Color.FromArgb(244, 67, 54));
            needle1.Brush = new StiSolidBrush(Color.FromArgb(244, 67, 54));

            StiNeedle needle2 = new StiNeedle();
            needle2.Value = new StiValueExpression("10");
            needle2.OffsetNeedle = 0.15f;
            needle2.StartWidth = 0.05f;
            needle2.EndWidth = 0.5f;
            needle2.RelativeHeight = 0.08f;
            needle2.RelativeWidth = 0.4f;
            needle2.CapBrush = new StiSolidBrush(Color.FromArgb(3, 169, 244));
            needle2.Brush = new StiSolidBrush(Color.FromArgb(3, 169, 244));

            radialScale2.Items.Add(tickMarkMajor2);
            radialScale2.Items.Add(tickLabelMajor2);
            radialScale2.Items.Add(needle1);
            radialScale2.Items.Add(needle2);

            gauge.Scales.Add(radialScale2);


            InitializeName(gauge, report);
        }

        public static void RadialBarGauge(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 250, 250);

            StiRadialScale radialScale1 = new StiRadialScale()
            {
                StartAngle = 120f,
                SweepAngle = 300,
                StartWidth = 0.05f,
                EndWidth = 0.05f,
                Minimum = 0f,
                Maximum = 180f,
                MajorInterval = 90,
                MinorInterval = 10,
                Brush = new StiSolidBrush(Color.FromArgb(238, 238, 238))
            };

            StiRadialTickMarkMajor tickMarkMajor1 = new StiRadialTickMarkMajor();
            tickMarkMajor1.Placement = StiPlacement.Inside;
            tickMarkMajor1.OffsetAngle = -1;
            tickMarkMajor1.RelativeWidth = 0.05f;
            tickMarkMajor1.RelativeHeight = 0.015f;
            tickMarkMajor1.Skin = StiTickMarkSkin.Rectangle;
            tickMarkMajor1.Brush = new StiSolidBrush(Color.Black);

            StiRadialTickMarkMinor tickMarkMinor1 = new StiRadialTickMarkMinor();
            tickMarkMinor1.Placement = StiPlacement.Inside;
            tickMarkMinor1.RelativeWidth = 0.03f;
            tickMarkMinor1.RelativeHeight = 0.005f;
            tickMarkMinor1.Skin = StiTickMarkSkin.Rectangle;
            tickMarkMinor1.Brush = new StiSolidBrush(Color.Black);

            StiRadialTickLabelMajor tickLabelMajor1 = new StiRadialTickLabelMajor()
            {
                Placement = StiPlacement.Inside,
                MinimumValue = 1,
                Offset = 0.15f
            };
            
            StiRadialBar radialBar = new StiRadialBar()
            {
                StartWidth = 0.1f,
                EndWidth = 0.1f,
                Offset = 0.1f,
                Placement = StiPlacement.Outside,
                Brush = new StiSolidBrush(Color.FromArgb(0, 150, 136)),
                Value = new StiValueExpression("90")
            };

            StiNeedle needle1 = new StiNeedle()
            {
                RelativeHeight = 0.3f,
                RelativeWidth = 0.4f,
                StartWidth = 0.1f,
                EndWidth = 1f,
                CapBrush = new StiEmptyBrush(),
                Brush = new StiEmptyBrush(),
                TextBrush = new StiSolidBrush(Color.FromArgb(0, 150, 136)),
                Font = new Font("Arial", 36f),
                Value = new StiValueExpression("90")
            };

            radialScale1.Items.Add(tickMarkMajor1);
            radialScale1.Items.Add(tickMarkMinor1);
            radialScale1.Items.Add(tickLabelMajor1);
            radialScale1.Items.Add(radialBar);
            radialScale1.Items.Add(needle1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);
        }

        public static void SimpleTwoBarGauge(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 250, 250);

            StiRadialScale radialScale1 = new StiRadialScale()
            {
                Minimum = 0f,
                Maximum = 180f,
                MajorInterval = 20,
                MinorInterval = 10,
                StartWidth = 0.05f,
                EndWidth = 0.05f,
                StartAngle = 120f,
                SweepAngle = 300,

                Brush = new StiSolidBrush(Color.FromArgb(238, 238, 238))
            };

            StiRadialTickMarkMajor tickMarkMajor1 = new StiRadialTickMarkMajor()
            {
                Placement = StiPlacement.Overlay,
                OffsetAngle = -1,
                RelativeWidth = 0.05f,
                RelativeHeight = 0.02f,
                Skin = StiTickMarkSkin.Rectangle,
                Brush = new StiSolidBrush(Color.Black),
                BorderBrush = new StiSolidBrush(Color.White),
                BorderWidth = 1
            };

            StiRadialTickMarkMinor tickMarkMinor1 = new StiRadialTickMarkMinor()
            {
                Placement = StiPlacement.Overlay,
                RelativeWidth = 0.01f,
                RelativeHeight = 0.01f,
                Skin = StiTickMarkSkin.Diamond,
                Brush = new StiSolidBrush(Color.Black),
                BorderBrush = new StiSolidBrush(Color.Gray),
                BorderWidth = 1
            };

            StiRadialTickLabelMajor tickLabelMajor1 = new StiRadialTickLabelMajor()
            {
                Placement = StiPlacement.Inside,
                Offset = 0.15f
            };

            StiRadialBar radialBar1 = new StiRadialBar()
            {
                StartWidth = 0.05f,
                EndWidth = 0.05f,
                Offset = 0f,
                Placement = StiPlacement.Outside,
                Brush = new StiSolidBrush(Color.FromArgb(3, 169, 244)),
                Value = new StiValueExpression("80")
            };

            StiRadialBar radialBar2 = new StiRadialBar()
            {
                StartWidth = 0.05f,
                EndWidth = 0.05f,
                Offset = 0f,
                Placement = StiPlacement.Inside,
                Brush = new StiSolidBrush(Color.FromArgb(255, 235, 59)),
                Value = new StiValueExpression("120")
            };

            StiRadialMarker radialMarker1 = new StiRadialMarker()
            {
                Value = new StiValueExpression("100"),
                Offset = 0.25f,
                RelativeHeight = 0.05f,
                RelativeWidth = 0.05f,
                Skin = StiMarkerSkin.TriangleRight,
                Brush = new StiSolidBrush(Color.FromArgb(76, 175, 80))
            };

            radialScale1.Items.Add(tickMarkMajor1);
            radialScale1.Items.Add(tickMarkMinor1);
            radialScale1.Items.Add(tickLabelMajor1);
            radialScale1.Items.Add(radialBar1);
            radialScale1.Items.Add(radialBar2);
            radialScale1.Items.Add(radialMarker1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);
        }

        public static void DefaultRadialGauge(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 250, 250);

            StiRadialScale radialScale1 = new StiRadialScale();
            radialScale1.StartAngle = 0f;
            radialScale1.SweepAngle = 340;
            radialScale1.StartWidth = 0.05f;
            radialScale1.EndWidth = 0.05f;
            radialScale1.Minimum = 0f;
            radialScale1.Maximum = 100f;
            radialScale1.MajorInterval = 10;
            radialScale1.MinorInterval = 5;

            StiRadialTickMarkMajor tickMarkMajor1 = new StiRadialTickMarkMajor();
            tickMarkMajor1.Placement = StiPlacement.Overlay;
            tickMarkMajor1.OffsetAngle = -2;
            tickMarkMajor1.RelativeWidth = 0.06f;
            tickMarkMajor1.RelativeHeight = 0.03f;
            tickMarkMajor1.Skin = StiTickMarkSkin.TriangleRight;

            StiRadialTickLabelMajor tickLabelMajor1 = new StiRadialTickLabelMajor();

            StiNeedle needle1 = new StiNeedle();
            needle1.RelativeHeight = 0.14f;
            needle1.RelativeWidth = 0.5f;

            radialScale1.Items.Add(tickMarkMajor1);
            radialScale1.Items.Add(tickLabelMajor1);
            radialScale1.Items.Add(needle1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);            
        }

        public static void DefaultLinearGauge(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 120, 240);

            var scale = new StiLinearScale()
            {
                StartWidth = 0.1f,
                EndWidth = 0.1f,
                Maximum = 100f,
                MinorInterval = 5,
                BorderBrush = new StiSolidBrush(Color.FromArgb(158, 158, 158)),
                Brush = new StiSolidBrush(Color.FromArgb(238, 238, 238))
            };

            var tickMarkMajor = new StiLinearTickMarkMajor()
            {
                BorderBrush = new StiEmptyBrush(),
                BorderWidth = 0f,
                RelativeHeight = 0.005f,
                RelativeWidth = 0.05f,
                Brush = new StiSolidBrush(Color.FromArgb(158, 158, 158))
            };

            var tickLabelMajor = new StiLinearTickLabelMajor()
            {
                Placement = StiPlacement.Inside,
                TextBrush = new StiSolidBrush(Color.FromArgb(158, 158, 158))
            };

            var linearBar = new StiLinearBar()
            {
                StartWidth = 0.1f,
                EndWidth = 0.1f,
                Brush = new StiSolidBrush(Color.FromArgb(0, 150, 136)),
                Value = new StiValueExpression("65")
            };
                
            scale.Items.Add(tickMarkMajor);
            scale.Items.Add(tickLabelMajor);
            scale.Items.Add(linearBar);

            gauge.Scales.Add(scale);
            InitializeName(gauge, report);
        }
        
        public static void LinearGaugeRangeList(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 120, 240);

            var scale = new StiLinearScale()
            {
                StartWidth = 0.1f,
                EndWidth = 0.1f,
                Maximum = 100f,
                MinorInterval = 5,
                BorderBrush = new StiSolidBrush(Color.FromArgb(158, 158, 158)),
                Brush = new StiSolidBrush(Color.FromArgb(238, 238, 238))
            };

            var linearRangeList = new StiLinearRangeList();

            var linearRange1 = new StiLinearRange()
            {
                StartValue = 0f,
                EndValue = 50f,
                Brush = new StiGradientBrush(Color.FromArgb(205, 220, 57), Color.FromArgb(139, 195, 74), 90f),
                StartWidth = 0.07f,
                EndWidth = 0.07f
            };

            var linearRange2 = new StiLinearRange()
            {
                StartValue = 45f,
                EndValue = 75f,
                Brush = new StiGradientBrush(Color.FromArgb(255, 193, 7), Color.FromArgb(255, 235, 59), 90f),
                StartWidth = 0.07f,
                EndWidth = 0.07f
            };

            var linearRange3 = new StiLinearRange()
            {
                StartValue = 75f,
                EndValue = 100f,
                Brush = new StiGradientBrush(Color.FromArgb(255, 152, 0), Color.FromArgb(255, 152, 0), 90f),
                StartWidth = 0.07f,
                EndWidth = 0.07f
            };

            linearRangeList.Ranges.Add(linearRange1);
            linearRangeList.Ranges.Add(linearRange2);
            linearRangeList.Ranges.Add(linearRange3);

            var linearTickLabelMajor = new StiLinearTickLabelMajor()
            {
                Placement = StiPlacement.Inside,
                TextBrush = new StiSolidBrush(Color.FromArgb(158, 158, 158))
            };

            var linearTickMarkMajor = new StiLinearTickMarkMajor()
            {
                BorderBrush = new StiEmptyBrush(),
                BorderWidth = 0f,
                RelativeHeight = 0.005f,
                RelativeWidth = 0.05f,
                Brush = new StiSolidBrush(Color.FromArgb(158, 158, 158))
            };

            var linearTickMarkMinor = new StiLinearTickMarkMinor()
            {
                Placement = StiPlacement.Overlay,
                RelativeWidth = 0.08f,
                RelativeHeight = 0.006f,
                BorderBrush = new StiEmptyBrush()
            };

            var linearMarker1 = new StiLinearMarker()
            {
                RelativeWidth = 0.18f,
                RelativeHeight = 0.04f,
                Placement = StiPlacement.Overlay,
                Brush = new StiSolidBrush(Color.FromArgb(205, 220, 57))
            };
            
            scale.Items.Add(linearRangeList);
            scale.Items.Add(linearTickLabelMajor);
            scale.Items.Add(linearTickMarkMajor);
            scale.Items.Add(linearTickMarkMinor);
            scale.Items.Add(linearMarker1);

            gauge.Scales.Add(scale);
            InitializeName(gauge, report);
        }
        
        public static void BulletGraphsGreen(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 300, 100);

            var linearScale1 = new StiLinearScale()
            {
                RelativeHeight = 0.94f,
                StartWidth = 0.01f,
                EndWidth = 0.01f,
                Maximum = 100f,
                MajorInterval = 20f,
                Orientation = System.Windows.Forms.Orientation.Horizontal,
                Brush = new StiEmptyBrush(),
                BorderBrush = new StiEmptyBrush()
            };

            var linearRangeList1 = new StiLinearRangeList();

            float startValue = 0f;
            int index = 0;
            while(++index < 11)
            {
                var linearRange = new StiLinearRange();
                linearRange.StartValue = startValue;
                startValue += 10f;
                linearRange.EndValue = startValue;
                linearRange.StartWidth = 0.5f;
                linearRange.EndWidth = 0.5f;
                linearRange.Placement = StiPlacement.Overlay;
                linearRange.BorderBrush = new StiSolidBrush(Color.FromArgb(150, 139, 138, 135));

                if (index >= 1 && index <= 3)
                    linearRange.Brush = new StiSolidBrush(Color.FromArgb(165, 214, 167));
                else if (index >= 4 && index <= 7)
                    linearRange.Brush = new StiSolidBrush(Color.FromArgb(76, 175, 80));
                else
                    linearRange.Brush = new StiSolidBrush(Color.FromArgb(46, 125, 50));

                linearRangeList1.Ranges.Add(linearRange);
            }

            var linearTickLabelMajor1 = new StiLinearTickLabelMajor()
            {
                Offset = 0.25f
            };

            var linearTickMarkCustom1 = new StiLinearTickMarkCustom()
            {
                Placement = StiPlacement.Overlay,
                RelativeWidth = 0.015f,
                RelativeHeight = 0.3f,
                Brush = new StiGradientBrush(Color.FromArgb(100, 100, 100), Color.FromArgb(10, 10, 10), 90f),
                ValueObj = 80f
            };

            var linearBar1 = new StiLinearBar()
            {
                Brush = new StiSolidBrush(Color.Black),
                StartWidth = 0.1f,
                EndWidth = 0.1f
            };

            linearScale1.Items.Add(linearRangeList1);
            linearScale1.Items.Add(linearTickLabelMajor1);
            linearScale1.Items.Add(linearTickMarkCustom1);
            linearScale1.Items.Add(linearBar1);

            gauge.Scales.Add(linearScale1);
            InitializeName(gauge, report);
        }
        
        public static void HalfDonutsGauge(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 280, 165);

            var radialScale1 = new StiRadialScale()
            {
                StartAngle = 180f,
                SweepAngle = 180f,
                Minimum = 0,
                Maximum = 150f,
                Radius = 0.75f,
                Center = new PointF(0.5f, 0.8f),
                RadiusMode = StiRadiusMode.Width,
                Skin = StiRadialScaleSkin.Empty
            };

            var radialBar1 = new StiRadialBar()
            {
                BorderBrush = new StiEmptyBrush(),
                BorderWidth = 0,
                StartWidth = 0.3f,
                EndWidth = 0.3f,
                EmptyBrush = new StiGradientBrush(Color.FromArgb(221, 221, 221), Color.FromArgb(240, 240, 240), 90f),
                UseRangeColor = true,
                Value = new StiValueExpression("60")
            };

            StiRadialIndicatorRangeInfo range1 = new StiRadialIndicatorRangeInfo();
            range1.Value = 0;
            range1.Brush = new StiSolidBrush(Color.FromArgb(139, 195, 74));
            StiRadialIndicatorRangeInfo range2 = new StiRadialIndicatorRangeInfo();
            range2.Value = 50;
            range2.Brush = new StiSolidBrush(Color.FromArgb(255, 235, 59));
            StiRadialIndicatorRangeInfo range3 = new StiRadialIndicatorRangeInfo();
            range3.Value = 100;
            range3.Brush = new StiSolidBrush(Color.FromArgb(244, 67, 54));

            radialBar1.RangeList.Add(range1);
            radialBar1.RangeList.Add(range2);
            radialBar1.RangeList.Add(range3);

            StiNeedle needle1 = new StiNeedle()
            {
                RelativeHeight = 0.05f,
                RelativeWidth = 0.4f,
                StartWidth = 0.1f,
                EndWidth = 0.2f,
                CapBrush = new StiSolidBrush(Color.White),
                CapBorderBrush = new StiSolidBrush(Color.FromArgb(0, 150, 136)),
                CapBorderWidth = 2f,
                Brush = new StiSolidBrush(Color.FromArgb(0, 150, 136)),
                BorderWidth = 0f,
                TextBrush = new StiSolidBrush(Color.FromArgb(0, 150, 136)),
                Value = new StiValueExpression("60")
            };

            radialScale1.Items.Add(radialBar1);
            radialScale1.Items.Add(needle1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);
        }

        public static void HalfDonutsGauge2(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 280, 165);

            StiRadialScale radialScale1 = new StiRadialScale();
            radialScale1.StartAngle = 180f;
            radialScale1.SweepAngle = 180f;
            radialScale1.Minimum = 0;
            radialScale1.Maximum = 100f;
            radialScale1.Radius = 0.75f;
            radialScale1.Center = new PointF(0.5f, 0.8f);
            radialScale1.RadiusMode = StiRadiusMode.Width;
            radialScale1.Skin = StiRadialScaleSkin.Empty;

            StiRadialBar radialBar1 = new StiRadialBar();
            radialBar1.BorderBrush = new StiEmptyBrush();
            radialBar1.StartWidth = 0.3f;
            radialBar1.EndWidth = 0.3f;
            radialBar1.Brush = new StiSolidBrush(Color.FromArgb(79, 134, 194));
            radialBar1.EmptyBrush = new StiGradientBrush(Color.FromArgb(221, 221, 221), Color.FromArgb(240, 240, 240), 90f);
            radialBar1.EmptyBorderBrush = new StiSolidBrush(Color.FromArgb(79, 134, 194));
            radialBar1.EmptyBorderWidth = 3f;

            radialScale1.Items.Add(radialBar1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);
        }

        public static void RadialGaugeHalfCircleN(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 260, 150);

            StiRadialScale radialScale1 = new StiRadialScale()
            {
                RadiusMode = StiRadiusMode.Width,
                Skin = StiRadialScaleSkin.RadialScaleHalfCircleN,
                StartAngle = 180f,
                SweepAngle = 180f,
                Maximum = 100,
                Center = new PointF(0.5f, 0.85f),
                Radius = 0.75f,
                StartWidth = 0.005f,
                EndWidth = 0.005f,
                MajorInterval = 10,
                MinorInterval = 5,
                Brush = new StiSolidBrush(Color.Gray),
            };

            StiRadialTickMarkMajor radialTickMarkMajor1 = new StiRadialTickMarkMajor()
            {
                Placement = StiPlacement.Overlay,
                Offset = 0.035f,
                RelativeWidth = 0.05f,
                RelativeHeight = 0.007f,
                Brush = new StiSolidBrush(Color.Gray),
                BorderBrush = new StiEmptyBrush()
            };

            StiRadialTickMarkMinor radialTickMarkMinor1 = new StiRadialTickMarkMinor()
            {
                Placement = StiPlacement.Overlay,
                Offset = 0.017f,
                RelativeWidth = 0.03f,
                RelativeHeight = 0.007f,
                Brush = new StiSolidBrush(Color.LightGray),
                BorderBrush = new StiEmptyBrush()
            };

            StiRadialTickLabelMajor radialTickLabelMajor1 = new StiRadialTickLabelMajor()
            {
                LabelRotationMode = StiLabelRotationMode.Automatic,
                TextBrush = new StiSolidBrush(Color.Black)
            };

            StiNeedle needle1 = new StiNeedle()
            {
                Value = new StiValueExpression("45"),
                CapBrush = new StiSolidBrush(Color.FromArgb(3, 169, 244)),
                Brush = new StiSolidBrush(Color.FromArgb(3, 169, 244)),
                StartWidth = 0.1f,
                EndWidth = 0.99f,
                RelativeWidth = 0.5f,
                RelativeHeight = 0.04f,
            };

            radialScale1.Items.Add(radialTickMarkMajor1);
            radialScale1.Items.Add(radialTickMarkMinor1);
            radialScale1.Items.Add(radialTickLabelMajor1);
            radialScale1.Items.Add(needle1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);
        }

        public static void RadialGaugeHalfCircleS(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 260, 150);
            
            StiRadialScale radialScale1 = new StiRadialScale()
            {
                RadiusMode = StiRadiusMode.Width,
                Skin = StiRadialScaleSkin.RadialScaleHalfCircleS,
                StartAngle = 0,
                SweepAngle = 180f,
                Maximum = 100,
                Center = new PointF(0.5f, 0.15f),
                Radius = 0.75f,
                StartWidth = 0.005f,
                EndWidth = 0.005f,
                MajorInterval = 10,
                MinorInterval = 5,
                Brush = new StiSolidBrush(Color.Gray),
            };

            StiRadialTickMarkMajor radialTickMarkMajor1 = new StiRadialTickMarkMajor()
            {
                Placement = StiPlacement.Overlay,
                Offset = 0.035f,
                RelativeWidth = 0.05f,
                RelativeHeight = 0.007f,
                Brush = new StiSolidBrush(Color.Gray),
                BorderBrush = new StiEmptyBrush()
            };

            StiRadialTickMarkMinor radialTickMarkMinor1 = new StiRadialTickMarkMinor()
            {
                Placement = StiPlacement.Overlay,
                Offset = 0.017f,
                RelativeWidth = 0.03f,
                RelativeHeight = 0.007f,
                Brush = new StiSolidBrush(Color.LightGray),
                BorderBrush = new StiEmptyBrush()
            };

            StiRadialTickLabelMajor radialTickLabelMajor1 = new StiRadialTickLabelMajor()
            {
                LabelRotationMode = StiLabelRotationMode.Automatic,
                TextBrush = new StiSolidBrush(Color.Black)
            };

            StiNeedle needle1 = new StiNeedle()
            {
                Value = new StiValueExpression("45"),
                CapBrush = new StiSolidBrush(Color.FromArgb(3, 169, 244)),
                Brush = new StiSolidBrush(Color.FromArgb(3, 169, 244)),
                StartWidth = 0.1f,
                EndWidth = 0.99f,
                RelativeWidth = 0.5f,
                RelativeHeight = 0.04f,
            };

            radialScale1.Items.Add(radialTickMarkMajor1);
            radialScale1.Items.Add(radialTickMarkMinor1);
            radialScale1.Items.Add(radialTickLabelMajor1);
            radialScale1.Items.Add(needle1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);
        }

        public static void RadialGaugeQuarterCircleNW(StiGauge gauge, StiReport report)
        {
            RadialGaugeQuarterCircle(gauge, report, StiRadialScaleSkin.RadialScaleQuarterCircleNW,
                180f, new PointF(0.9f, 0.9f));
        }

        public static void RadialGaugeQuarterCircleNE(StiGauge gauge, StiReport report)
        {
            RadialGaugeQuarterCircle(gauge, report, StiRadialScaleSkin.RadialScaleQuarterCircleNE,
                270f, new PointF(0.1f, 0.9f));
        }

        public static void RadialGaugeQuarterCircleSW(StiGauge gauge, StiReport report)
        {
            RadialGaugeQuarterCircle(gauge, report, StiRadialScaleSkin.RadialScaleQuarterCircleSW,
                90f, new PointF(0.9f, 0.1f));
        }

        public static void RadialGaugeQuarterCircleSE(StiGauge gauge, StiReport report)
        {
            RadialGaugeQuarterCircle(gauge, report, StiRadialScaleSkin.RadialScaleQuarterCircleSE,
                0f, new PointF(0.1f, 0.1f));
        }

        private static void RadialGaugeQuarterCircle(StiGauge gauge, StiReport report, StiRadialScaleSkin scaleSkin, float startAngle, PointF center)
        {
            InitializeGauge(gauge, 250, 250);

            StiRadialScale radialScale1 = new StiRadialScale()
            {
                Skin = scaleSkin,
                StartAngle = startAngle,
                SweepAngle = 90f,
                Maximum = 100f,
                Center = center,
                Radius = 1.5f,
                StartWidth = 0.005f,
                EndWidth = 0.005f,
                MajorInterval = 10f,
                MinorInterval = 5f,
            };
            StiRadialTickMarkMajor radialTickMarkMajor1 = new StiRadialTickMarkMajor()
            {
                Placement = StiPlacement.Overlay,
                Offset = 0.045f,
                RelativeWidth = 0.03f,
                RelativeHeight = 0.004f,
                BorderBrush = new StiEmptyBrush(),
                Brush = new StiSolidBrush(Color.Gray)
            };

            StiRadialTickMarkMinor radialTickMarkMinor1 = new StiRadialTickMarkMinor()
            {
                Placement = StiPlacement.Overlay,
                Offset = 0.04f,
                RelativeWidth = 0.02f,
                RelativeHeight = 0.004f,
                BorderBrush = new StiEmptyBrush(),
                Brush = new StiSolidBrush(Color.Gray)
            };

            StiRadialTickLabelMajor radialTickLabelMajor1 = new StiRadialTickLabelMajor()
            {
                LabelRotationMode = StiLabelRotationMode.Automatic
            };

            StiNeedle needle1 = new StiNeedle()
            {
                Value = new StiValueExpression("45"),
                CapBrush = new StiSolidBrush(Color.FromArgb(244, 67, 54)),
                Brush = new StiSolidBrush(Color.FromArgb(244, 67, 54)),
                StartWidth = 0.1f,
                EndWidth = 0.99f,
                RelativeWidth = 0.5f,
                RelativeHeight = 0.04f,
            };

            radialScale1.Items.Add(radialTickMarkMajor1);
            radialScale1.Items.Add(radialTickMarkMinor1);
            radialScale1.Items.Add(radialTickLabelMajor1);
            radialScale1.Items.Add(needle1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);
        }

        public static void HorizontalThermometer(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 300, 50);

            StiLinearScale linearScale = new StiLinearScale();
            linearScale.Orientation = System.Windows.Forms.Orientation.Horizontal;
            linearScale.RelativeHeight = 0.85f;
            linearScale.Left = 0.03f;
            linearScale.StartWidth = 0.17f;
            linearScale.EndWidth = 0.17f;
            linearScale.Maximum = 80f;
            linearScale.MajorInterval = 20;
            linearScale.MinorInterval = 4;
            
            StiLinearBar linearBar = new StiLinearBar();
            linearBar.Skin = StiLinearBarSkin.HorizontalThermometer;
            linearBar.Placement = StiPlacement.Outside;
            linearBar.StartWidth = 0.2f;
            linearBar.EndWidth = 0.2f;
            linearBar.UseRangeColor = true;

            StiLinearIndicatorRangeInfo indicatorRange1 = new StiLinearIndicatorRangeInfo();
            indicatorRange1.Value = 0;
            indicatorRange1.Brush = new StiGradientBrush(Color.FromArgb(137, 188, 41), Color.FromArgb(111, 163, 14), 90f);
            StiLinearIndicatorRangeInfo indicatorRange2 = new StiLinearIndicatorRangeInfo();
            indicatorRange2.Value = 40;
            indicatorRange2.Brush = new StiGradientBrush(Color.FromArgb(217, 173, 45), Color.FromArgb(222, 166, 0), 90f);
            StiLinearIndicatorRangeInfo indicatorRange3 = new StiLinearIndicatorRangeInfo();
            indicatorRange3.Value = 65;
            indicatorRange3.Brush = new StiGradientBrush(Color.FromArgb(208, 49, 52), Color.FromArgb(186, 6, 10), 90f);

            linearBar.RangeList.Add(indicatorRange1);
            linearBar.RangeList.Add(indicatorRange2);
            linearBar.RangeList.Add(indicatorRange3);

            StiLinearTickMarkMajor tickMarkMajor = new StiLinearTickMarkMajor();
            tickMarkMajor.Placement = StiPlacement.Overlay;
            tickMarkMajor.Brush = new StiSolidBrush(Color.FromArgb(159, 159, 159));
            tickMarkMajor.RelativeWidth = 0.01f;
            tickMarkMajor.RelativeHeight = 0.18f;
            tickMarkMajor.Offset = -0.1f;

            StiLinearTickMarkMinor tickMarkMinor = new StiLinearTickMarkMinor();
            tickMarkMinor.Brush = new StiSolidBrush(Color.FromArgb(159, 159, 159));
            tickMarkMinor.RelativeWidth = 0.005f;
            tickMarkMinor.RelativeHeight = 0.1f;
            tickMarkMinor.Offset = -0.14f;

            StiLinearTickLabelMajor tickLabelMajor = new StiLinearTickLabelMajor();
            tickLabelMajor.Placement = StiPlacement.Inside;
            tickLabelMajor.Offset = 0.11f;
            tickLabelMajor.TextBrush = new StiSolidBrush(Color.FromArgb(83, 85, 86));
            tickLabelMajor.Font = new Font("Arial", 10f);

            StiStateIndicator stateIndicator = new StiStateIndicator();
            stateIndicator.Left = 0.02f;
            stateIndicator.Top = 0.07f;
            stateIndicator.RelativeWidth = 0.083f;
            stateIndicator.RelativeHeight = 0.5f;

            StiStateIndicatorFilter indicatorFilter1 = new StiStateIndicatorFilter();
            indicatorFilter1.StartValue = 0f;
            indicatorFilter1.EndValue = 40f;
            indicatorFilter1.Brush = new StiSolidBrush(Color.FromArgb(112, 156, 28));

            StiStateIndicatorFilter indicatorFilter2 = new StiStateIndicatorFilter();
            indicatorFilter2.StartValue = 40f;
            indicatorFilter2.EndValue = 65f;
            indicatorFilter2.Brush = new StiSolidBrush(Color.FromArgb(225, 174, 25));

            StiStateIndicatorFilter indicatorFilter3 = new StiStateIndicatorFilter();
            indicatorFilter3.StartValue = 65f;
            indicatorFilter3.EndValue = 100f;
            indicatorFilter3.Brush = new StiSolidBrush(Color.FromArgb(194, 45, 48));

            stateIndicator.Filters.Add(indicatorFilter1);
            stateIndicator.Filters.Add(indicatorFilter2);
            stateIndicator.Filters.Add(indicatorFilter3);

            linearScale.Items.Add(linearBar);
            linearScale.Items.Add(tickMarkMajor);
            linearScale.Items.Add(tickMarkMinor);
            linearScale.Items.Add(tickLabelMajor);
            linearScale.Items.Add(stateIndicator);

            gauge.Scales.Add(linearScale);
            InitializeName(gauge, report);
        }

        public static void VerticalThermometer(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 50, 300);

            StiLinearScale linearScale = new StiLinearScale();
            linearScale.Orientation = System.Windows.Forms.Orientation.Vertical;
            linearScale.RelativeHeight = 0.85f;
            linearScale.StartWidth = 0.17f;
            linearScale.EndWidth = 0.17f;
            linearScale.Maximum = 80f;
            linearScale.MajorInterval = 20;
            linearScale.MinorInterval = 4;

            StiLinearBar linearBar = new StiLinearBar();
            linearBar.Skin = StiLinearBarSkin.VerticalThermometer;
            linearBar.Placement = StiPlacement.Outside;
            linearBar.StartWidth = 0.2f;
            linearBar.EndWidth = 0.2f;
            linearBar.UseRangeColor = true;

            StiLinearIndicatorRangeInfo indicatorRange1 = new StiLinearIndicatorRangeInfo();
            indicatorRange1.Value = 0;
            indicatorRange1.Brush = new StiGradientBrush(Color.FromArgb(137, 188, 41), Color.FromArgb(111, 163, 14), 0f);
            StiLinearIndicatorRangeInfo indicatorRange2 = new StiLinearIndicatorRangeInfo();
            indicatorRange2.Value = 40;
            indicatorRange2.Brush = new StiGradientBrush(Color.FromArgb(217, 173, 45), Color.FromArgb(222, 166, 0), 0f);
            StiLinearIndicatorRangeInfo indicatorRange3 = new StiLinearIndicatorRangeInfo();
            indicatorRange3.Value = 65;
            indicatorRange3.Brush = new StiGradientBrush(Color.FromArgb(208, 49, 52), Color.FromArgb(186, 6, 10), 0f);

            linearBar.RangeList.Add(indicatorRange1);
            linearBar.RangeList.Add(indicatorRange2);
            linearBar.RangeList.Add(indicatorRange3);

            StiLinearTickMarkMajor tickMarkMajor = new StiLinearTickMarkMajor();
            tickMarkMajor.Placement = StiPlacement.Overlay;
            tickMarkMajor.Brush = new StiSolidBrush(Color.FromArgb(159, 159, 159));
            tickMarkMajor.RelativeWidth = 0.18f;
            tickMarkMajor.RelativeHeight = 0.01f;
            tickMarkMajor.Offset = -0.1f;

            StiLinearTickMarkMinor tickMarkMinor = new StiLinearTickMarkMinor();
            tickMarkMinor.Brush = new StiSolidBrush(Color.FromArgb(159, 159, 159));
            tickMarkMinor.RelativeWidth = 0.1f;
            tickMarkMinor.RelativeHeight = 0.005f;
            tickMarkMinor.Offset = -0.14f;

            StiLinearTickLabelMajor tickLabelMajor = new StiLinearTickLabelMajor();
            tickLabelMajor.Placement = StiPlacement.Inside;
            tickLabelMajor.Offset = 0.07f;
            tickLabelMajor.TextBrush = new StiSolidBrush(Color.FromArgb(83, 85, 86));
            tickLabelMajor.Font = new Font("Arial", 10f);

            StiStateIndicator stateIndicator = new StiStateIndicator();
            stateIndicator.Left = 0.05f;
            stateIndicator.Top = 0.9f;
            stateIndicator.RelativeWidth = 0.5f;
            stateIndicator.RelativeHeight = 0.083f;

            StiStateIndicatorFilter indicatorFilter1 = new StiStateIndicatorFilter();
            indicatorFilter1.StartValue = 0f;
            indicatorFilter1.EndValue = 40f;
            indicatorFilter1.Brush = new StiSolidBrush(Color.FromArgb(112, 156, 28));

            StiStateIndicatorFilter indicatorFilter2 = new StiStateIndicatorFilter();
            indicatorFilter2.StartValue = 40f;
            indicatorFilter2.EndValue = 65f;
            indicatorFilter2.Brush = new StiSolidBrush(Color.FromArgb(225, 174, 25));

            StiStateIndicatorFilter indicatorFilter3 = new StiStateIndicatorFilter();
            indicatorFilter3.StartValue = 65f;
            indicatorFilter3.EndValue = 100f;
            indicatorFilter3.Brush = new StiSolidBrush(Color.FromArgb(194, 45, 48));

            stateIndicator.Filters.Add(indicatorFilter1);
            stateIndicator.Filters.Add(indicatorFilter2);
            stateIndicator.Filters.Add(indicatorFilter3);

            linearScale.Items.Add(linearBar);
            linearScale.Items.Add(tickMarkMajor);
            linearScale.Items.Add(tickMarkMinor);
            linearScale.Items.Add(tickLabelMajor);
            linearScale.Items.Add(stateIndicator);

            gauge.Scales.Add(linearScale);
            InitializeName(gauge, report);
        }

        public static void LightSpeedometer(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 350, 350);

            StiRadialScale radialScale1 = new StiRadialScale();
            radialScale1.Skin = StiRadialScaleSkin.Empty;
            radialScale1.Radius = 0.65f;
            radialScale1.StartAngle = 115;
            radialScale1.SweepAngle = 310;
            radialScale1.StartWidth = 0.03f;
            radialScale1.EndWidth = 0.03f;
            radialScale1.Brush = new StiSolidBrush(Color.FromArgb(81, 84, 101));
            radialScale1.MajorInterval = 10;
            radialScale1.MinorInterval = 2;
            radialScale1.Maximum = 100;

            StiRadialRangeList radialRangeList1 = new StiRadialRangeList();

            StiRadialRange radialRange1 = new StiRadialRange();
            radialRange1.StartValue = 40;
            radialRange1.EndValue = 101.2f;
            radialRange1.StartWidth = 0.04f;
            radialRange1.EndWidth = 0.04f;
            radialRange1.Placement = StiPlacement.Inside;
            radialRange1.UseValuesFromTheSpecifiedRange = false;
            radialRange1.Brush = new StiGradientBrush(Color.FromArgb(213, 227, 118), Color.FromArgb(118, 71, 24), 90f);
            radialRange1.BorderBrush = new StiSolidBrush(Color.White);

            radialRangeList1.Ranges.Add(radialRange1);

            StiRadialTickMarkMajor radialTickMarkMajor1 = new StiRadialTickMarkMajor();
            radialTickMarkMajor1.SkipValues = new StiSkipValuesExpression("100");
            radialTickMarkMajor1.RelativeHeight = 0.03f;
            radialTickMarkMajor1.RelativeWidth = 0.055f;
            radialTickMarkMajor1.Brush = new StiSolidBrush(Color.FromArgb(81, 84, 101));

            StiRadialTickMarkMinor radialTickMarkMinor1 = new StiRadialTickMarkMinor();
            radialTickMarkMinor1.Offset = 0.04f;
            radialTickMarkMinor1.RelativeHeight = 0.01f;
            radialTickMarkMinor1.RelativeWidth = 0.03f;
            radialTickMarkMinor1.Brush = new StiSolidBrush(Color.FromArgb(81, 84, 101));

            StiRadialTickMarkCustom radialTickMarkCustom1 = new StiRadialTickMarkCustom();
            radialTickMarkCustom1.ValueObj = 100f;
            radialTickMarkCustom1.Offset = -0.057f;
            radialTickMarkCustom1.RelativeWidth = 0.08f;
            radialTickMarkCustom1.RelativeHeight = 0.03f;
            radialTickMarkCustom1.Brush = new StiSolidBrush(Color.FromArgb(81, 84, 101));

            StiRadialTickLabelMajor radialTickLabelMajor1 = new StiRadialTickLabelMajor();
            radialTickLabelMajor1.LabelRotationMode = StiLabelRotationMode.None;
            radialTickLabelMajor1.Offset = 0.14f;
            radialTickLabelMajor1.TextBrush = new StiSolidBrush(Color.Black);
            radialTickLabelMajor1.Font = new Font("Arial", 13f, FontStyle.Bold);

            StiNeedle needle1 = new StiNeedle();
            needle1.Brush = new StiSolidBrush(Color.FromArgb(250, 250, 250));
            needle1.BorderBrush = new StiSolidBrush(Color.FromArgb(163, 163, 163));
            needle1.BorderWidth = 1f;
            needle1.Placement = StiPlacement.Outside;
            needle1.RelativeWidth = 0.63f;
            needle1.RelativeHeight = 0.14f;
            needle1.Skin = StiNeedleSkin.SpeedometerNeedle;

            radialScale1.Items.Add(radialRangeList1);
            radialScale1.Items.Add(radialTickMarkMajor1);
            radialScale1.Items.Add(radialTickMarkMinor1);
            radialScale1.Items.Add(radialTickMarkCustom1);
            radialScale1.Items.Add(radialTickLabelMajor1);
            radialScale1.Items.Add(needle1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);
        }

        public static void DarkSpeedometer(StiGauge gauge, StiReport report)
        {
            InitializeGauge(gauge, 350, 350);

            StiRadialScale radialScale1 = new StiRadialScale();
            radialScale1.Skin = StiRadialScaleSkin.Empty;
            radialScale1.Radius = 0.65f;
            radialScale1.StartAngle = 115;
            radialScale1.SweepAngle = 310;
            radialScale1.StartWidth = 0.03f;
            radialScale1.EndWidth = 0.03f;
            radialScale1.Brush = new StiSolidBrush(Color.FromArgb(81, 84, 101));
            radialScale1.MajorInterval = 10;
            radialScale1.MinorInterval = 2;
            radialScale1.Maximum = 200;

            StiRadialRangeList radialRangeList1 = new StiRadialRangeList();

            StiRadialRange radialRange1 = new StiRadialRange();
            radialRange1.StartValue = 0f;
            radialRange1.EndValue = 60;
            radialRange1.BorderWidth = 1f;
            radialRange1.StartWidth = 0.06f;
            radialRange1.EndWidth = 0.06f;
            radialRange1.Placement = StiPlacement.Overlay;
            radialRange1.Brush = new StiGradientBrush(Color.FromArgb(101, 134, 101), Color.FromArgb(66, 228, 66), 90f);

            StiRadialRange radialRange2 = new StiRadialRange();
            radialRange2.StartValue = 60f;
            radialRange2.EndValue = 100;
            radialRange2.BorderWidth = 1f;
            radialRange2.StartWidth = 0.06f;
            radialRange2.EndWidth = 0.06f;
            radialRange2.Placement = StiPlacement.Overlay;
            radialRange2.Brush = new StiGradientBrush(Color.FromArgb(255, 255, 0), Color.FromArgb(143, 174, 126), 90f);

            StiRadialRange radialRange3 = new StiRadialRange();
            radialRange3.StartValue = 100f;
            radialRange3.EndValue = 140;
            radialRange3.BorderWidth = 1f;
            radialRange3.StartWidth = 0.06f;
            radialRange3.EndWidth = 0.06f;
            radialRange3.Placement = StiPlacement.Overlay;
            radialRange3.Brush = new StiGradientBrush(Color.FromArgb(255, 255, 0), Color.FromArgb(156, 156, 124), 90f);

            StiRadialRange radialRange4 = new StiRadialRange();
            radialRange4.StartValue = 140f;
            radialRange4.EndValue = 200;
            radialRange4.BorderWidth = 1f;
            radialRange4.StartWidth = 0.06f;
            radialRange4.EndWidth = 0.06f;
            radialRange4.Placement = StiPlacement.Overlay;
            radialRange4.Brush = new StiGradientBrush(Color.FromArgb(125, 86, 80), Color.FromArgb(208, 45, 44), 90f);

            radialRangeList1.Ranges.Add(radialRange1);
            radialRangeList1.Ranges.Add(radialRange2);
            radialRangeList1.Ranges.Add(radialRange3);
            radialRangeList1.Ranges.Add(radialRange4);

            StiRadialTickMarkMajor radialTickMarkMajor1 = new StiRadialTickMarkMajor();
            radialTickMarkMajor1.Placement = StiPlacement.Overlay;
            radialTickMarkMajor1.Offset = 0.04f;
            radialTickMarkMajor1.OffsetAngle = -2;
            radialTickMarkMajor1.RelativeWidth = 0.1f;
            radialTickMarkMajor1.RelativeHeight = 0.035f;
            radialTickMarkMajor1.Skin = StiTickMarkSkin.TriangleLeft;

            StiRadialTickMarkMinor radialTickMarkMinor1 = new StiRadialTickMarkMinor();
            radialTickMarkMinor1.Placement = StiPlacement.Overlay;
            radialTickMarkMinor1.SkipMajorValues = false;
            radialTickMarkMinor1.RelativeWidth = 0.04f;
            radialTickMarkMinor1.RelativeHeight = 0.018f;
            radialTickMarkMinor1.Skin = StiTickMarkSkin.Rectangle;
            radialTickMarkMinor1.SkipIndices = new StiSkipIndicesExpression("0;4;8;12;16;20;24;28;32;36;40");

            StiRadialTickLabelMajor radialTickLabelMajor1 = new StiRadialTickLabelMajor();
            radialTickLabelMajor1.LabelRotationMode = StiLabelRotationMode.None;
            radialTickLabelMajor1.Offset = 0.05f;
            radialTickLabelMajor1.Font = new Font("Arial", 11f);

            StiRadialTickMarkCustom radialTickMarkCustom1 = new StiRadialTickMarkCustom();
            radialTickMarkCustom1.Placement = StiPlacement.Inside;
            radialTickMarkCustom1.Offset = 0.3f;
            radialTickMarkCustom1.RelativeHeight = 0.05f;
            radialTickMarkCustom1.RelativeWidth = 0.05f;
            radialTickMarkCustom1.Skin = StiTickMarkSkin.Ellipse;

            StiRadialTickMarkCustomValue custom1 = new StiRadialTickMarkCustomValue(20);
            custom1.Brush = new StiGradientBrush(Color.FromArgb(68, 223, 68), Color.FromArgb(0, 153, 0), 90f);
            StiRadialTickMarkCustomValue custom2 = new StiRadialTickMarkCustomValue(97);
            custom2.Brush = new StiGradientBrush(Color.FromArgb(255, 255, 0), Color.FromArgb(186, 169, 2), 90f);
            StiRadialTickMarkCustomValue custom3 = new StiRadialTickMarkCustomValue(173);
            custom3.Brush = new StiGradientBrush(Color.FromArgb(184, 29, 29), Color.FromArgb(121, 30, 30), 90f);
            radialTickMarkCustom1.Values.Add(custom1);
            radialTickMarkCustom1.Values.Add(custom2);
            radialTickMarkCustom1.Values.Add(custom3);

            StiRadialTickLabelCustom radialTickLabelCustom1 = new StiRadialTickLabelCustom();
            radialTickLabelCustom1.Placement = StiPlacement.Inside;
            radialTickLabelCustom1.LabelRotationMode = StiLabelRotationMode.None;
            radialTickLabelCustom1.Font = new Font("Arial", 10f);

            radialTickLabelCustom1.Values.Add(new StiRadialTickLabelCustomValue(17, "Safe", 0.16f));
            radialTickLabelCustom1.Values.Add(new StiRadialTickLabelCustomValue(102, "Caution", 0.25f));
            radialTickLabelCustom1.Values.Add(new StiRadialTickLabelCustomValue(181, "Danger", 0.06f));

            StiNeedle needle1 = new StiNeedle();
            needle1.BorderBrush = new StiSolidBrush(Color.FromArgb(153, 9, 8));
            needle1.Brush = new StiGradientBrush(Color.FromArgb(255, 198, 172), Color.FromArgb(197, 25, 19), 90f);
            needle1.BorderWidth = 1f;
            needle1.Placement = StiPlacement.Outside;
            needle1.RelativeWidth = 0.57f;
            needle1.RelativeHeight = 0.17f;
            needle1.Skin = StiNeedleSkin.SpeedometerNeedle2;

            radialScale1.Items.Add(radialRangeList1);
            radialScale1.Items.Add(radialTickMarkMajor1);
            radialScale1.Items.Add(radialTickMarkMinor1);
            radialScale1.Items.Add(radialTickLabelMajor1);
            radialScale1.Items.Add(radialTickMarkCustom1);
            radialScale1.Items.Add(radialTickLabelCustom1);
            radialScale1.Items.Add(needle1);

            gauge.Scales.Add(radialScale1);
            InitializeName(gauge, report);
        }
        #endregion
    }
}