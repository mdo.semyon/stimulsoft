﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Gauge
{
    public class StiGaugeStyleCoreXF27 : StiGaugeStyleCoreXF
    {
        #region Properties.Localization
        /// <summary>
        /// Gets a localized name of style.
        /// </summary>
        public override string LocalizedName
        {
            get
            {
                return StiLocalization.Get("Chart", "Style") + "27";
            }
        }
        #endregion

        #region Properties

        public override StiBrush Brush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#FF33475B"));
            }
        }

        public override Color BorderColor
        {
            get
            {
                return Color.Transparent;
            }
        }

        public override float BorderWidth
        {
            get
            {
                return 0;
            }
        }

        #region Scale

        #region TickMarkMajor        
        public override StiBrush TickMarkMajorBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#585257"));
            }
        }

        public override StiBrush TickMarkMajorBorder
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#585257"));
            }
        }
        #endregion

        #region TickMarkMinor
        public override StiBrush TickMarkMinorBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#585257"));
            }
        }

        public override StiBrush TickMarkMinorBorder
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#585257"));
            }
        }
        #endregion

        #region TickLabelMajor
        public override StiBrush TickLabelMajorTextBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ffffff"));
            }
        }

        public override Font TickLabelMajorFont
        {
            get
            {
                return new Font("Arial", 10);
            }
        }
        #endregion

        #region TickLabelMinor
        public override StiBrush TickLabelMinorTextBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ffffff"));
            }
        }

        public override Font TickLabelMinorFont
        {
            get
            {
                return new Font("Arial", 9);
            }
        }
        #endregion
        
        #region Marker        

        public override StiBrush MarkerBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ec334d"));
            }
        }

        public override StiBrush LinearMarkerBorder
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ffffff"));
            }
        }

        #endregion

        #region Linear Scale
        public override StiBrush LinearScaleBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#0bac45"));
            }
        }

        #region Bar
        public override StiBrush LinearBarBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#0bac45"));
            }
        }

        public override StiBrush LinearBarBorderBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override StiBrush LinearBarEmptyBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override StiBrush LinearBarEmptyBorderBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override float LinearBarStartWidth
        {
            get
            {
                return 0.1f;
            }
        }

        public override float LinearBarEndWidth
        {
            get
            {
                return 0.1f;
            }
        }
        #endregion
        #endregion

        #region Radial Scale

        #region Bar
        public override StiBrush RadialBarBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#0bac45"));
            }
        }

        public override StiBrush RadialBarBorderBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override StiBrush RadialBarEmptyBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#FF585257"));
            }
        }

        public override StiBrush RadialBarEmptyBorderBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override float RadialBarStartWidth
        {
            get
            {
                return 0.1f;
            }
        }

        public override float RadialBarEndWidth
        {
            get
            {
                return 0.1f;
            }
        }
        #endregion

        #region Needle
        public override StiBrush NeedleBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ec334d"));
            }
        }

        public override StiBrush NeedleBorderBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override StiBrush NeedleCapBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ffffff"));
            }
        }

        public override StiBrush NeedleCapBorderBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ec334d"));
            }
        }

        public override float NeedleBorderWidth
        {
            get
            {
                return 0;
            }
        }

        public override float NeedleCapBorderWidth
        {
            get
            {
                return 2;
            }
        }

        public override float NeedleStartWidth
        {
            get
            {
                return 0.1f;
            }
        }

        public override float NeedleEndWidth
        {
            get
            {
                return 1;
            }
        }

        public override float NeedleRelativeHeight
        {
            get
            {
                return 0.06f;
            }
        }

        public override float NeedleRelativeWith
        {
            get
            {
                return 0.45f;
            }
        }
        #endregion

        #endregion

        #endregion
        #endregion
    }
}