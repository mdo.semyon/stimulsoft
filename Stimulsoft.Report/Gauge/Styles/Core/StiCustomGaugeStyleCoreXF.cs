﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Report.Gauge
{
    public class StiCustomGaugeStyleCoreXF : StiGaugeStyleCoreXF25
    {
        #region Properties.Localization
        /// <summary>
        /// Gets a service name.
        /// </summary>
        public override string LocalizedName
        {
            get
            {
                return "CustomStyle";
            }
        }
        #endregion

        #region Fields
        public StiGaugeStyle ReportGaugeStyle = null;
        #endregion

        #region Properties

        public StiGaugeStyle ReportStyle
        {
            get
            {
                string styleName = this.ReportStyleName == null ? this.Gauge.CustomStyleName : this.ReportStyleName;

                if (this.Gauge == null || this.Gauge.Report == null || styleName == null || styleName.Length == 0)
                    return null;

                var style = this.Gauge.Report.Styles[styleName] as StiGaugeStyle;
                return style;
            }
        }

        public string ReportStyleName { get; set; }

        private StiCustomGaugeStyle customStyle;
        public StiCustomGaugeStyle CustomStyle
        {
            get
            {
                return customStyle;
            }
        }
        #endregion

        #region Properties.override

        public override StiBrush Brush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.Brush;
                return base.Brush;
            }
        }

        public override Color BorderColor
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.BorderColor;
                return base.BorderColor;
            }
        }

        public override float BorderWidth
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.BorderWidth;
                return base.BorderWidth;
            }
        }

        #region Scale

        #region TickMarkMajor        
        public override StiBrush TickMarkMajorBrush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.TickMarkMajorBrush;
                return base.TickMarkMajorBrush;
            }
        }

        public override StiBrush TickMarkMajorBorder
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.TickMarkMajorBorder;
                return base.TickMarkMajorBorder;
            }
        }
        #endregion

        #region TickMarkMinor
        public override StiBrush TickMarkMinorBrush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.TickMarkMinorBrush;
                return base.TickMarkMajorBorder;
            }
        }

        public override StiBrush TickMarkMinorBorder
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.TickMarkMinorBorder;
                return base.TickMarkMajorBorder;
            }
        }
        #endregion

        #region TickLabelMajor
        public override StiBrush TickLabelMajorTextBrush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.TickLabelMajorTextBrush;
                return base.TickLabelMajorTextBrush;
            }
        }

        public override Font TickLabelMajorFont
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.TickLabelMajorFont;
                return base.TickLabelMajorFont;
            }
        }
        #endregion

        #region TickLabelMinor
        public override StiBrush TickLabelMinorTextBrush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.TickLabelMinorTextBrush;
                return base.TickLabelMinorTextBrush;
            }
        }

        public override Font TickLabelMinorFont
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.TickLabelMinorFont;
                return base.TickLabelMinorFont;
            }
        }
        #endregion

        #region Marker

        public override StiBrush MarkerBrush
        {
            get
            {
                if (ReportStyle != null) return ReportStyle.MarkerBrush;
                return base.MarkerBrush;
            }
        }

        #endregion

        #region Linear Scale

        #region Bar
        public override StiBrush LinearBarBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#4472c4"));
            }
        }

        public override StiBrush LinearBarBorderBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override StiBrush LinearBarEmptyBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override StiBrush LinearBarEmptyBorderBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override float LinearBarStartWidth
        {
            get
            {
                return 0.1f;
            }
        }

        public override float LinearBarEndWidth
        {
            get
            {
                return 0.1f;
            }
        }
        #endregion

        #endregion

        #region Radial Scale

        #region Bar
        public override StiBrush RadialBarBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ffc000"));
            }
        }

        public override StiBrush RadialBarBorderBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override StiBrush RadialBarEmptyBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#43682b"));
            }
        }

        public override StiBrush RadialBarEmptyBorderBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override float RadialBarStartWidth
        {
            get
            {
                return 0.1f;
            }
        }

        public override float RadialBarEndWidth
        {
            get
            {
                return 0.1f;
            }
        }
        #endregion

        #region Needle
        public override StiBrush NeedleBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ffc000"));
            }
        }

        public override StiBrush NeedleBorderBrush
        {
            get
            {
                return new StiEmptyBrush();
            }
        }

        public override StiBrush NeedleCapBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ffc000"));
            }
        }

        public override StiBrush NeedleCapBorderBrush
        {
            get
            {
                return new StiSolidBrush(ColorTranslator.FromHtml("#ffc000"));
            }
        }

        public override float NeedleBorderWidth
        {
            get
            {
                return 0;
            }
        }

        public override float NeedleCapBorderWidth
        {
            get
            {
                return 0;
            }
        }

        public override float NeedleStartWidth
        {
            get
            {
                return 0.1f;
            }
        }

        public override float NeedleEndWidth
        {
            get
            {
                return 1;
            }
        }

        public override float NeedleRelativeHeight
        {
            get
            {
                return 0.08f;
            }
        }

        public override float NeedleRelativeWith
        {
            get
            {
                return 0.55f;
            }
        }
        #endregion

        #endregion

        #endregion
        #endregion

        public StiCustomGaugeStyleCoreXF(StiCustomGaugeStyle customStyle)
        {
            this.customStyle = customStyle;
        }
    }
}
