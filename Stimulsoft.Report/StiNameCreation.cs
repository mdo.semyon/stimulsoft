#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Linq;
using System.Text;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.Components;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Design;
using Stimulsoft.Report.Dictionary;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report
{
	#region StiNamingRule
	public enum StiNamingRule
	{
		Simple,
		Advanced
	}
	#endregion

	/// <summary>
	/// Describes the class that used for report names creation.
	/// </summary>
	public sealed class StiNameCreation
	{
		#region Properties
		public static StiNamingRule NamingRule
		{
			get
			{
				return StiOptions.Engine.NamingRule;
			}
			set
			{
				StiOptions.Engine.NamingRule = value;
			}
		}

		#endregion

		#region Methods
		private static string RemoveSpacesFromName(string baseName, bool removeIncorrectSymbols, StiReport report)
		{
			if (removeIncorrectSymbols)
			{
				StringBuilder sb = new StringBuilder(baseName);
				int charIndex = 0;
				int len = baseName.Length;
			
				for (int pos = 0; pos < len; pos++)
				{
					if (baseName[pos] == ' ')
					{
						sb.Remove(charIndex, 1);
						if (charIndex < sb.Length && Char.IsLetter(sb[charIndex]))
						{
							sb[charIndex] = Char.ToUpper(sb[charIndex]);
						}
					}
					else charIndex++;
				}
				baseName = sb.ToString();
				return Stimulsoft.Report.StiNameValidator.CorrectName(baseName, report);
			}
			return baseName;			
		}

	
		public static string CreateSimpleName(StiReport report, string baseName)
		{
			baseName = RemoveSpacesFromName(baseName, true, report);
			return baseName + (report.IndexName++).ToString();
		}

		/// <summary>
		/// Creates a name from the base name which is correct for the report.
		/// </summary>
		/// <param name="report">Report.</param>
		/// <param name="baseName">The type to form a name.</param>
		public static string CreateName(StiReport report, string baseName)
		{
			return CreateName(report, baseName, true, true);
		}


		public static string CreateName(StiReport report, string baseName, bool addOne, bool removeIncorrectSymbols)
		{
			return CreateName(report, baseName, addOne, removeIncorrectSymbols, false);
		}

		/// <summary>
		/// Creates a name from the base name which is correct for the report.
		/// </summary>
		/// <param name="report">Report.</param>
		/// <param name="baseName">The type to form a name.</param>
		/// <returns></returns>
		public static string CreateName(StiReport report, string baseName, bool addOne, bool removeIncorrectSymbols, bool forceAdvancedNamingRule)
		{			
			baseName = RemoveSpacesFromName(baseName, removeIncorrectSymbols, report);

			if (!forceAdvancedNamingRule)
			{
				if ((!report.IsDesigning) || NamingRule == StiNamingRule.Simple)return baseName + (report.IndexName++).ToString();
			}

			var comps = report.GetComponents();
			int counter = 1;
		
			if (comps.Count == 0 && 
				report.DataSources.Count == 0 &&
				report.Dictionary.DataSources.Count == 0 &&
				report.Dictionary.Relations.Count == 0 &&
				report.Dictionary.Variables.Count == 0)
			{
				if (addOne)return baseName + "1";
				return baseName;
			}

			while (true)
			{
				var testName = baseName + counter;
				string checkName;
				
                if ((!addOne) && counter == 1)checkName = baseName;
				else checkName = testName;

				if (GetObjectWithName(null, report, comps, checkName) == null)
				{
					if ((!addOne) && counter == 1)return baseName;
					return testName;
				}
				
				counter++;
			}
		}

        public static string CreateResourceName(StiReport report, string baseName)
        {
            baseName = RemoveSpacesFromName(baseName, false, report);

            var counter = 1;
            while (true)
            {
                var testName = counter == 1 ? baseName : baseName + counter;

                if (!IsResourceNameExists(report, testName))
                    return testName;

                counter++;
            }
        }

        public static string CreateConnectionName(StiReport report, string baseName)
        {
            baseName = RemoveSpacesFromName(baseName, false, report);

            var counter = 1;
            while (true)
            {
                var testName = counter == 1 ? baseName : baseName + counter;

                if (!IsConnectionNameExists(report, testName))
                    return testName;

                counter++;
            }
        }

        public static bool IsResourceNameExists(StiReport report, string name)
        {
            if (report == null) return false;

            name = name.ToLowerInvariant().Trim();
            return report.Dictionary.Resources.ToList()
                .Any(r => r.Name.ToLowerInvariant().Trim() == name);
        }

        public static bool IsConnectionNameExists(StiReport report, string name)
        {
            if (report == null) return false;

            name = name.ToLowerInvariant().Trim();
            return report.Dictionary.Databases.ToList()
                .Any(r => r.Name.ToLowerInvariant().Trim() == name);
        }

        public static string CreateColumnName(StiDataSource dataSource, string baseName)
        {
            baseName = RemoveSpacesFromName(baseName, false, dataSource?.Dictionary?.Report);

            var counter = 1;
            while (true)
            {
                var testName = counter == 1 ? baseName : baseName + counter;

                if (!IsColumnNameExists(dataSource, testName))
                    return testName;

                counter++;
            }
        }

        public static bool IsColumnNameExists(StiDataSource dataSource, string name)
        {
            name = name.ToLowerInvariant().Trim();

            return dataSource.Columns.ToList()
                .Any(r => r.Name.ToLowerInvariant().Trim() == name);
        }


        /// <summary>
        /// Checks whether the name of the report is correct.
        /// </summary>
        /// <param name="name">Checked name.</param>
        /// <returns>Result of checking.</returns>
        public static bool IsValidName(StiReport report, string name)
		{
			if (string.IsNullOrEmpty(name) || !(Char.IsLetter(name[0]) || name[0] == '_'))
				return false;

			for (int pos = 0; pos < name.Length; pos++)
				if (!(Char.IsLetterOrDigit(name[pos]) || (name[pos] == '_')))return false;
			
			StiComponentsCollection comps = report.GetComponents();

			foreach (StiComponent comp in comps)
				if (name == comp.Name)return false;

			return true;
		}


		public static bool Exists(object checkedObject, StiReport report, string name)
		{
            return GetObjectWithName(checkedObject, report, name) != null;
		}


		public static bool Exists(StiReport report, string name)
		{
			return Exists(null, report, name);
		}

		
		public static bool CheckName(object checkedObject, 
			StiReport report, string name, string messageBoxCaption)
		{
			return CheckName(checkedObject, report, name, messageBoxCaption, true);
		}


		public static bool CheckName(object checkedObject, 
			StiReport report, string name, string messageBoxCaption, bool isValid)
		{
			if (report != null && report.IsDesigning)
			{
				if (StiNameCreation.Exists(checkedObject, report, name))
				{
					MessageBox.Show(string.Format(StiLocalization.Get("Errors", "NameExists"), name), 
						messageBoxCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}
				else if (isValid && (!StiNameCreation.IsValidName(report, name)))
				{
					MessageBox.Show(string.Format(StiLocalization.Get("Errors", "IdentifierIsNotValid"), name), 
						messageBoxCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}
			}
			return true;
		}


		public static object GetObjectWithName(object checkedObject, StiReport report, string name)
		{
			if (report == null)return null;
			return GetObjectWithName(checkedObject, report, report.GetComponents(), name);
		}


		private static object GetObjectWithName(object checkedObject, 
			StiReport report, StiComponentsCollection comps, string name)
		{
			if (report == null)return null;

			#region Check in components names
			for (int index = 0; index < comps.Count; index++)
			{
				StiComponent comp = comps[index] as StiComponent;
				if (name == comp.Name && checkedObject != comp)return comp;
			}
			#endregion				

            #region Check in datasources names
            for (int index = 0; index < report.Dictionary.DataSources.Count; index++)
            {
                StiDataSource dataSource = report.Dictionary.DataSources[index] as StiDataSource;
                if (name == dataSource.Name && checkedObject != dataSource) return dataSource;
            }
            #endregion

			#region Check in datarelations names
			//for (int index = 0; index < report.Dictionary.Relations.Count; index++)
			//{
			//	StiDataRelation dataRelation = report.Dictionary.Relations[index] as StiDataRelation;
			//	if (name == dataRelation.Name && checkedObject != dataRelation)return dataRelation;
			//}
			#endregion

            #region Check in business objects names
            for (int index = 0; index < report.Dictionary.BusinessObjects.Count; index++)
            {
                StiBusinessObject businessObject = report.Dictionary.BusinessObjects[index] as StiBusinessObject;
                if (name == businessObject.Name && checkedObject != businessObject) return businessObject;
            }
            #endregion

			#region Check in variables names
			for (int index = 0; index < report.Dictionary.Variables.Count; index++)
			{
				StiVariable variable = report.Dictionary.Variables[index] as StiVariable;
				if (name == variable.Name && checkedObject != variable)return variable;
			}				
			#endregion

			return null;
		}

		
		/// <summary>
		/// Returns a name of the component.
		/// </summary>
		/// <param name="localizedName">Localized component name.</param>
		/// <param name="name">Did not localized component name.</param>
		/// <returns>Component name.</returns>
		public static string GenerateName(StiReport report, string localizedName, string name)
		{
			if (StiOptions.Engine.ForceGenerationLocalizedName)return localizedName;
			if (!StiOptions.Engine.ForceGenerationNonLocalizedName)
			{
				if (report != null && report.Info.GenerateLocalizedName)return localizedName;
			}

			string componentName = name;
			if (componentName.Length > 1 && componentName.Substring(0, 3) == "Sti")
			{
				componentName = componentName.Substring(3);
            }

            #region DBS
            if (componentName.EndsWith("Element"))
                componentName = componentName.Substring(0, componentName.Length - "Element".Length);
            #endregion

            return componentName;
		}


		/// <summary>
		/// Returns a name of the component.
		/// </summary>
		/// <param name="localizedName">Localized component name.</param>
		/// <param name="type">Type of component.</param>
		/// <returns>Component name.</returns>
		public static string GenerateName(StiReport report, string localizedName, Type type)
		{
			return GenerateName(report, localizedName, type.Name);
		}


		/// <summary>
		/// Returns a name for the component.
		/// </summary>
		/// <param name="component">Component for which a name is created.</param>
		/// <returns>Component name.</returns>
		public static string GenerateName(StiComponent component)
		{
			return GenerateName(component.Report, component.LocalizedName, component.GetType());
		}

        /// <summary>
        /// Returns a name for the relation.
        /// </summary>
        /// <param name="relation">Relation for which a name is created.</param>
        /// <returns>Relation name.</returns>
        public static string GenerateName(StiDataRelation relation)
        {
            return GenerateName(relation.Dictionary.Report, StiLocalization.Get("PropertyMain", "DataRelation"), relation.GetType());
        }

        /// <summary>
        /// Returns a name for the datasource.
        /// </summary>
        /// <param name="dataSource">Datasource for which a name is created.</param>
        /// <returns>Datasource name.</returns>
        public static string GenerateName(StiDataSource dataSource)
        {
            return GenerateName(dataSource.Dictionary.Report, StiLocalization.Get("PropertyMain", "DataSource"), dataSource.GetType());
        }
		#endregion
	}
}
