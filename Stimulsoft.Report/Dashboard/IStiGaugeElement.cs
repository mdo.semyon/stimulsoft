#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Meters;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Gauge;

namespace Stimulsoft.Report.Dashboard
{
	public interface IStiGaugeElement : 
        IStiElement,
        IStiDashboardElementStyle
    {
        #region Value
        void AddValue(StiDataColumn dataColumn);

        void AddValue(IStiMeter meter);

        void RemoveValue();

        IStiMeter GetValue();

        IStiMeter GetValue(IStiMeter meter);

        void CreateNewValue();
        #endregion

        #region Series
        void AddSeries(StiDataColumn dataColumn);

        void AddSeries(IStiMeter meter);

        void RemoveSeries();

        IStiMeter GetSeries();

        IStiMeter GetSeries(IStiMeter meter);

        void CreateNewSeries();
        #endregion

        StiGaugeCalculationMode CalculationMode { get; set; }

        StiGaugeType Type { get; set; }

        decimal Minimum { get; set; }

        decimal Maximum { get; set; }

        StiGauge GetGaugeComponent(SizeD? size = null);
    }
}