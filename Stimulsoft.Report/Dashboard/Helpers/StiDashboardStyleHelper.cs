﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.Chart;
using Stimulsoft.Report.Dashboard.Styles;
using Stimulsoft.Report.Gauge;
using Stimulsoft.Report.Maps;
using System;
using System.Drawing;
using System.Reflection;

namespace Stimulsoft.Report.Dashboard.Helpers
{
    public static class StiDashboardStyleHelper
    {
        #region Fields
        private static FontFamily iconFontFamily;
        #endregion

        #region Methods
        public static StiGaugeStyleXF GetGaugeStyle(IStiGaugeElement element)
        {
            if (element.Style == StiElementStyleIdent.Auto)
            {
                var dashboard = (IStiDashboard)element.Page;
                switch (dashboard.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiGaugeStyleXF24();

                    case StiElementStyleIdent.Green:
                        return new StiGaugeStyleXF25();

                    case StiElementStyleIdent.Turquoise:
                        return new StiGaugeStyleXF26();
                }
            }
            else
            {
                switch (element.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiGaugeStyleXF24();

                    case StiElementStyleIdent.Green:
                        return new StiGaugeStyleXF25();

                    case StiElementStyleIdent.Turquoise:
                        return new StiGaugeStyleXF26();
                }
            }

            return new StiGaugeStyleXF24();
        }

        public static IStiChartStyle GetChartStyle(IStiChartElement element)
        {
            if (element.Style == StiElementStyleIdent.Auto)
            {
                var dashboard = (IStiDashboard)element.Page;
                switch (dashboard.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiStyle24();

                    case StiElementStyleIdent.Green:
                        return new StiStyle25();

                    case StiElementStyleIdent.Turquoise:
                        return new StiStyle26();
                }
            }
            else
            {
                switch (element.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiStyle24();

                    case StiElementStyleIdent.Green:
                        return new StiStyle25();

                    case StiElementStyleIdent.Turquoise:
                        return new StiStyle26();
                }
            }

            return new StiStyle24();
        }

        public static StiMapStyleIdent GetMapStyleIdent(IStiMapElement element)
        {
            if (element.Style == StiElementStyleIdent.Auto)
            {
                var dashboard = (IStiDashboard)element.Page;
                switch (dashboard.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return StiMapStyleIdent.Style24;

                    case StiElementStyleIdent.Green:
                        return StiMapStyleIdent.Style25;

                    case StiElementStyleIdent.Turquoise:
                        return StiMapStyleIdent.Style26;
                }
            }
            else
            {
                switch (element.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return StiMapStyleIdent.Style24;

                    case StiElementStyleIdent.Green:
                        return StiMapStyleIdent.Style25;

                    case StiElementStyleIdent.Turquoise:
                        return StiMapStyleIdent.Style26;
                }
            }

            return StiMapStyleIdent.Style24;
        }

        public static StiMapStyleFX GetMapStyle(StiElementStyleIdent id)
        {
            switch (id)
            {
                case StiElementStyleIdent.Orange:
                    return new StiMap24StyleFX();

                case StiElementStyleIdent.Green:
                    return new StiMap25StyleFX();

                case StiElementStyleIdent.Turquoise:
                    return new StiMap26StyleFX();
            }

            return new StiMap24StyleFX();
        }

        public static StiIndicatorElementStyle GetIndicatorStyle(IStiIndicatorElement element)
        {
            if (element.Style == StiElementStyleIdent.Auto)
            {
                var dashboard = (IStiDashboard)element.Page;
                switch (dashboard.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiOrangeIndicatorElementStyle();

                    case StiElementStyleIdent.Green:
                        return new StiGreenIndicatorElementStyle();

                    case StiElementStyleIdent.Turquoise:
                        return new StiTurquoiseIndicatorElementStyle();
                }
            }
            else
            {
                switch (element.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiOrangeIndicatorElementStyle();

                    case StiElementStyleIdent.Green:
                        return new StiGreenIndicatorElementStyle();

                    case StiElementStyleIdent.Turquoise:
                        return new StiTurquoiseIndicatorElementStyle();
                }
            }

            return new StiOrangeIndicatorElementStyle();
        }

        public static StiProgressElementStyle GetProgressStyle(IStiProgressElement element)
        {
            if (element.Style == StiElementStyleIdent.Auto)
            {
                var dashboard = (IStiDashboard)element.Page;
                switch (dashboard.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiOrangeProgressElementStyle();

                    case StiElementStyleIdent.Green:
                        return new StiGreenProgressElementStyle();

                    case StiElementStyleIdent.Turquoise:
                        return new StiTurquoiseProgressElementStyle();
                }
            }
            else
            {
                switch (element.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiOrangeProgressElementStyle();

                    case StiElementStyleIdent.Green:
                        return new StiGreenProgressElementStyle();

                    case StiElementStyleIdent.Turquoise:
                        return new StiTurquoiseProgressElementStyle();
                }
            }

            return new StiOrangeProgressElementStyle();
        }

        public static StiTableElementStyle GetTableStyle(IStiTableElement element)
        {
            if (element.Style == StiElementStyleIdent.Auto)
            {
                var dashboard = (IStiDashboard)element.Page;
                switch (dashboard.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiOrangeTableElementStyle();

                    case StiElementStyleIdent.Green:
                        return new StiGreenTableElementStyle();

                    case StiElementStyleIdent.Turquoise:
                        return new StiTurquoiseTableElementStyle();
                }
            }
            else
            {
                switch (element.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiOrangeTableElementStyle();

                    case StiElementStyleIdent.Green:
                        return new StiGreenTableElementStyle();

                    case StiElementStyleIdent.Turquoise:
                        return new StiTurquoiseTableElementStyle();
                }
            }

            return new StiOrangeTableElementStyle();
        }

        public static StiPivotElementStyle GetPivotStyle(IStiPivotElement element)
        {
            if (element.Style == StiElementStyleIdent.Auto)
            {
                var dashboard = (IStiDashboard)element.Page;
                switch (dashboard.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiOrangePivotElementStyle();

                    case StiElementStyleIdent.Green:
                        return new StiGreenPivotElementStyle();

                    case StiElementStyleIdent.Turquoise:
                        return new StiTurquoisePivotElementStyle();
                }
            }
            else
            {
                switch (element.Style)
                {
                    case StiElementStyleIdent.Orange:
                        return new StiOrangePivotElementStyle();

                    case StiElementStyleIdent.Green:
                        return new StiGreenPivotElementStyle();

                    case StiElementStyleIdent.Turquoise:
                        return new StiTurquoisePivotElementStyle();
                }
            }

            return new StiOrangePivotElementStyle();
        }

        internal static FontFamily GetIconFontFamily()
        {
            if (iconFontFamily == null)
            {
                var type = Type.GetType("Stimulsoft.Dashboard.Helpers.StiFontIconsHelper, Stimulsoft.Dashboard");
                if (type != null)
                {
                    var mi = type.GetMethod("GetFontFamaliIcons", BindingFlags.NonPublic | BindingFlags.Static);
                    if (mi != null)
                    {
                        iconFontFamily = mi.Invoke(null, null) as FontFamily;
                    }
                }
            }

            if (iconFontFamily == null)
                iconFontFamily = new FontFamily("Arial");

            return iconFontFamily;
        }
        #endregion
    }
}