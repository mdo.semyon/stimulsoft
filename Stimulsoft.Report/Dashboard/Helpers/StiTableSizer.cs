﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Helpers;
using Stimulsoft.Report.Dictionary;
using System.Data;
using System.Drawing;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dashboard.Helpers
{
    public class StiTableSizer
    {
        public static void MakeAutoSizeForDataGridSample(DataGridView grid)
        {
            var dataTable = grid.DataSource as DataTable;
            grid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);

            using (var bmp = new Bitmap(1, 1))
            using (var g = Graphics.FromImage(bmp))
            {
                var columnIndex = 0;
                foreach (DataGridViewColumn column in grid.Columns)
                {
                    column.SortMode = DataGridViewColumnSortMode.NotSortable;
                    if (column.Width > 300)
                        column.Width = 300;

                    if (columnIndex < grid.Columns.Count)
                    {
                        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                        if (dataTable.Columns[columnIndex].DataType.IsDateType() || dataTable.Columns[columnIndex].DataType == typeof(bool))
                            column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                        if (dataTable.Columns[columnIndex].DataType.IsNumericType())
                            column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    }

                    column.MinimumWidth = (int)g.MeasureString(column.HeaderText, grid.Font).Width + 40;

                    columnIndex++;
                }
            }
        }
    }
}
