﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Data.Engine;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Images;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Stimulsoft.Base.Dashboard;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Dashboard.Styles;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dashboard.Helpers
{
    public class StiTablePartDrawer
    {
        #region Methods
        public void DrawCell()
        {
            if (CellArgs.RowIndex != -1)
                DrawDataCell();
            else
                DrawHeaderCell();
        }

        private void DrawHeaderCell()
        {
            var g = CellArgs.Graphics;
            CellArgs.Handled = true;

            CellArgs.Paint(CellBounds,
                DataGridViewPaintParts.Background |
                DataGridViewPaintParts.ContentBackground | DataGridViewPaintParts.Focus |
                DataGridViewPaintParts.SelectionBackground);

            var pos = Grid.PointToClient(Cursor.Position);
            var isMouseOver = CellBounds.Contains(pos);

            Color brushColor;
            if (style != null)
            {
                brushColor = (isMouseOver)
                    ? StiColorUtils.Dark(style.BrushDark, 30)
                    : style.BrushDark;
            }
            else
            {
                brushColor = ColorTranslator.FromHtml(isMouseOver ? "#ffffff" : "#f0f0f0");
            }

            using (var brush = new SolidBrush(brushColor))
            {
                g.FillRectangle(brush, CellBounds);
            }

            DrawBorder();
            DrawHeaderText();
            DrawSortArrow();
            DrawFiltersArrow();
        }

        private void DrawDataCell()
        {
            CellArgs.Handled = true;
            CellArgs.Paint(CellBounds, DataGridViewPaintParts.All);
            DrawBorder();
        }

        private void DrawBorder()
        {
            if (CellArgs.ColumnIndex == 0) return;

            StiDrawing.DrawLine(CellArgs.Graphics, StiElementConsts.Table.BorderColor, 
                CellBounds.Left, CellBounds.Top, CellBounds.Left, CellBounds.Bottom);
        }

        public void DrawHeaderText()
        {
            using (var sf = new StringFormat())
            {
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Center;
                sf.Trimming = StringTrimming.EllipsisCharacter;
                sf.FormatFlags = StringFormatFlags.NoWrap;

                var str = CellArgs.FormattedValue?.ToString();
                if (!string.IsNullOrWhiteSpace(str))
                {
                    using (var brush = new SolidBrush((this.style == null) ? Color.Black : this.style.HeaderForeground))
                    {
                        Graphics.DrawString(str, Grid.Font, brush, CellBounds, sf);
                    }
                }
            }
        }

        public void DrawFiltersArrow()
        {
            if (ColumnsCount <= CellArgs.ColumnIndex) return;

            var isFilterPresent = FilterRules != null && FilterRules.Any(r => r.Key == ColumnKey);
            var isActionPresent = Actions != null && Actions.Any(a => a.Key == ColumnKey);

            if (isFilterPresent || isActionPresent)
            {
                using (var image = (this.style == null)
                    ? ((IsMouseOverCell)
                    ? StiReportImages.Actions.DropDownFilterMouseOver()
                    : StiReportImages.Actions.DropDownFilter())
                    : ((IsMouseOverCell)
                    ? StiReportImages.Actions.DropDownFilterMouseOverWhite()
                    : StiReportImages.Actions.DropDownFilterWhite()))
                {
                    Graphics.DrawImage(image, GetRightImageRect(image));
                }

                return;
            }

            if (IsMouseOverCell)
            {
                using (var image = (this.style == null)
                    ? StiReportImages.Actions.DropDownArrowMouseOver()
                    : StiReportImages.Actions.DropDownArrowMouseOverWhite())
                {
                    Graphics.DrawImage(image, GetRightImageRect(image));
                }
            }
        }

        private Rectangle GetRightImageRect(Image image)
        {
            return new Rectangle(CellBounds.Right - 5 - image.Width, CellBounds.Y + (CellBounds.Height - image.Height) / 2, image.Width, image.Height);
        }

        public void DrawSortArrow()
        {
            if (ColumnsCount <= CellArgs.ColumnIndex) return;

            var direction = StiDataSortRuleHelper.GetSortDirection(SortRules, ColumnKey);
            if (direction == StiDataSortDirection.None) return;

            using (var image = (this.style == null)
                ? ((direction == StiDataSortDirection.Ascending)
                ? (IsMouseOverCell ? StiReportImages.Actions.SortArrowAscMouseOver() : StiReportImages.Actions.SortArrowAsc())
                : (IsMouseOverCell ? StiReportImages.Actions.SortArrowDescMouseOver() : StiReportImages.Actions.SortArrowDesc()))

                : ((direction == StiDataSortDirection.Ascending)
                ? (IsMouseOverCell ? StiReportImages.Actions.SortArrowAscMouseOverWhite() : StiReportImages.Actions.SortArrowAscWhite())
                : (IsMouseOverCell ? StiReportImages.Actions.SortArrowDescMouseOverWhite() : StiReportImages.Actions.SortArrowDescWhite())))
            {
                var imagePoint = new Point(CellBounds.X + 4, CellBounds.Y + (CellBounds.Height - image.Height) / 2);
                Graphics.DrawImage(image, imagePoint.X, imagePoint.Y, image.Width, image.Height);

                DrawSortIndex(imagePoint, image);
            }
        }

        public void DrawSortIndex(Point imagePoint, Image image)
        {
            if (SortRules.Count <= 1) return;
            if (ColumnsCount <= CellArgs.ColumnIndex) return;

            var rule = SortRules.FirstOrDefault(r => string.Equals(r.Key, ColumnKey, StringComparison.InvariantCultureIgnoreCase));
            if (rule == null) return;

            var index = SortRules.IndexOf(rule) + 1;
            if (index == -1 || index > 9) return;

            using (var font = new Font(Grid.Font.Name, 7))
            using (var sf = new StringFormat())
            {
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Near;

                var textRect = new Rectangle(imagePoint.X + image.Width - 1, CellBounds.Y + 1, 20, CellBounds.Height);

                using (var brush = new SolidBrush(
                    (this.style == null)
                    ? IsMouseOverCell ? ColorTranslator.FromHtml("#7a7a7a") : ColorTranslator.FromHtml("#aaaaaa")
                    : this.style.HeaderForeground))
                {
                    Graphics.DrawString(index.ToString(), font, brush, textRect, sf);
                }
            }
        }
        #endregion

        #region Fields
        private StiTableElementStyle style;
        #endregion

        #region Properties
        private bool IsMouseOverCell => CellBounds.Contains(Grid.PointToClient(Cursor.Position));

        private DataGridViewCellPaintingEventArgs CellArgs { get; }

        private Graphics Graphics => CellArgs.Graphics;

        private Rectangle CellBounds => CellArgs.CellBounds;

        private DataGridView Grid { get; }

        private List<StiDataSortRule> SortRules { get; }

        private List<StiDataFilterRule> FilterRules { get; }

        private List<StiDataActionRule> Actions { get; }

        private int ColumnsCount { get; }

        private string ColumnKey { get; }
        #endregion

        public StiTablePartDrawer(DataGridViewCellPaintingEventArgs cellArgs, DataGridView grid, ListBox.ObjectCollection items, 
            List<StiDataSortRule> sortRules, List<StiDataFilterRule> filterRules, List<StiDataActionRule> actions)
        {
            this.CellArgs = cellArgs;
            this.Grid = grid;
            this.SortRules = sortRules;
            this.FilterRules = filterRules;
            this.Actions = actions;

            this.ColumnsCount = items.Count;

            var columnIndex = Math.Min(ColumnsCount - 1, cellArgs.ColumnIndex);
            this.ColumnKey = columnIndex != -1 ? (items[columnIndex] as StiDataColumn)?.Key : null;
        }

        public StiTablePartDrawer(DataGridViewCellPaintingEventArgs cellArgs, DataGridView grid, 
            IStiTableElement element, StiTableElementStyle style = null)
        {
            this.CellArgs = cellArgs;
            this.Grid = grid;
            this.SortRules = element.SortRules;
            this.FilterRules = element.FilterRules;
            this.style = style;

            var meters = element.GetMeters();
            this.ColumnsCount = meters.Count;

            var columnIndex = Math.Min(ColumnsCount - 1, cellArgs.ColumnIndex);
            this.ColumnKey = meters[columnIndex]?.Key;
        }

        public StiTablePartDrawer(DataGridViewCellPaintingEventArgs cellArgs, DataGridView grid)
        {
            this.CellArgs = cellArgs;
            this.Grid = grid;
        }
    }
}
