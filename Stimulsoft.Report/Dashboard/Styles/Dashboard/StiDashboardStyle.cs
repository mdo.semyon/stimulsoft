#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.Drawing.Drawing2D;

namespace Stimulsoft.Report.Dashboard.Styles
{
    public abstract class StiDashboardStyle : StiElementStyle
    {
        #region Properties
        public abstract string LocalizedName { get; }

        public abstract Color Brush { get; }
        #endregion

        #region Methods
        public void DrawStyleForGallery(Graphics g, Rectangle rect)
        {
            g.FillRectangle(Brushes.White, rect);

            var rect1 = new Rectangle(rect.X + 5, rect.Y + 5, rect.Width - 10, rect.Height - 10);
            var rectTop = new Rectangle(rect1.Location, new Size(rect1.Width, rect1.Height - 16));
            var rectBottom = new Rectangle(rectTop.X, rectTop.Bottom, rectTop.Width + 1, 16);

            using (var pen = new Pen(Brush, 1f))
            {
                g.DrawRectangle(pen, rectTop);

                using (var font = new Font("Arial", 15f))
                using (var brush = new SolidBrush(Brush))
                using (var sf = new StringFormat())
                {
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;

                    var state = g.Save();
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.CompositingQuality = CompositingQuality.HighQuality;

                    g.DrawString("Aa", font, brush, rectTop, sf);
                    g.Restore(state);
                }
            }

            using (var brush = new SolidBrush(Brush))
            {
                g.FillRectangle(brush, rectBottom);

                using (var font = new Font("Arial", 8f))
                using (var sf = new StringFormat())
                {
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;

                    var state = g.Save();
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.CompositingQuality = CompositingQuality.HighQuality;

                    g.DrawString(LocalizedName, font, Brushes.White, rectBottom, sf);
                    g.Restore(state);
                }
            }
        }
        #endregion
    }
}