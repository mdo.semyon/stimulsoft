#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.PropertyGrid;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Stimulsoft.Report.Dashboard.Styles
{
    public abstract class StiProgressElementStyle : StiElementStyle
    {
        #region Properties
        public abstract Color BrushLight { get; }

        public abstract Color BrushDark { get; }

        public abstract Color[] BrushDarkColors { get; }

        public abstract Color Foreground { get; }
        #endregion

        #region Methods
        public void DrawStyleForGallery(Graphics g, Rectangle rect, StiProgressElementMode mode)
        {
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.CompositingQuality = CompositingQuality.HighQuality;

            g.FillRectangle(Brushes.White, rect);

            switch (mode)
            {
                #region Circle
                case StiProgressElementMode.Circle:
                    {
                        var size = (float)Math.Min(rect.Width, rect.Height) - 10;
                        var rect1 = new RectangleF(rect.X + (rect.Width - size) / 2, rect.Y + (rect.Height - size) / 2, size, size);
                        using (var pen = new Pen(this.BrushLight, 7))
                        {
                            g.DrawEllipse(pen, rect1);
                        }

                        g.SetClip(new RectangleF(rect1.X + rect1.Width / 2, rect1.Y - 5, rect1.Width / 2 + 10, rect1.Height + 10));
                        using (var pen = new Pen(this.BrushDark, 7))
                        {
                            g.DrawEllipse(pen, rect1);
                        }
                        g.ResetClip();

                        g.SetClip(new RectangleF(rect1.X - 5, rect1.Y + rect1.Height / 2, rect1.Width / 2 + 10, (rect1.Height / 2) + 10));
                        using (var pen = new Pen(this.BrushDark, 7))
                        {
                            g.DrawEllipse(pen, rect1);
                        }
                        g.ResetClip();

                        rect1.Y += 2;
                        using (var textFont = new Font("Arial", 14f, FontStyle.Bold, GraphicsUnit.Pixel))
                        using (var textBrush = new SolidBrush(this.Foreground))
                        using (var sf = new StringFormat())
                        {
                            sf.Alignment = StringAlignment.Center;
                            sf.LineAlignment = StringAlignment.Center;

                            g.DrawString("75%", textFont, textBrush, rect1, sf);
                        }
                    }
                    break;
                #endregion

                #region Pie
                case StiProgressElementMode.Pie:
                    {
                        var size = (float)Math.Min(rect.Width, rect.Height) - 10;
                        var rect1 = new RectangleF(rect.X + (rect.Width - size) / 2, rect.Y + (rect.Height - size) / 2, size, size);
                        using (var brush = new SolidBrush(this.BrushLight))
                        {
                            g.FillEllipse(brush, rect1);
                        }

                        g.SetClip(new RectangleF(rect1.X + rect1.Width / 2, rect1.Y, rect1.Width / 2, rect1.Height));
                        using (var brush = new SolidBrush(this.BrushDark))
                        {
                            g.FillEllipse(brush, rect1);
                        }
                        g.ResetClip();

                        g.SetClip(new RectangleF(rect1.X, rect1.Y + rect1.Height / 2, rect1.Width / 2, rect1.Height / 2));
                        using (var brush = new SolidBrush(this.BrushDark))
                        {
                            g.FillEllipse(brush, rect1);
                        }
                        g.ResetClip();
                    }
                    break;
                #endregion

                #region DataBars
                case StiProgressElementMode.DataBars:
                    {
                        var size = (float)Math.Min(rect.Width, rect.Height) - 10;
                        var rect1 = new RectangleF(rect.X + 5, rect.Y + 5, rect.Width - 10, rect.Height - 10);
                        using (var brush = new SolidBrush(this.BrushLight))
                        {
                            g.FillRectangle(brush, rect1);
                        }

                        using (var textFont = new Font("Arial", 25f, FontStyle.Bold, GraphicsUnit.Pixel))
                        using (var textBrush = new SolidBrush(this.Foreground))
                        using (var sf = new StringFormat())
                        {
                            sf.Alignment = StringAlignment.Center;
                            sf.LineAlignment = StringAlignment.Center;

                            g.DrawString("75%", textFont, textBrush, rect1, sf);
                        }

                        var rect2 = new RectangleF(rect1.X, rect1.Y, rect1.Width * 0.75f, rect1.Height);
                        using (var brush = new SolidBrush(this.BrushDark))
                        {
                            g.FillRectangle(brush, rect2);
                        }


                        g.SetClip(rect2);
                        using (var textFont = new Font("Arial", 25f, FontStyle.Bold, GraphicsUnit.Pixel))
                        using (var textBrush = new SolidBrush(this.Foreground))
                        using (var sf = new StringFormat())
                        {
                            sf.Alignment = StringAlignment.Center;
                            sf.LineAlignment = StringAlignment.Center;

                            g.DrawString("75%", textFont, textBrush, rect1, sf);
                        }
                        g.ResetClip();
                    }
                    break;
                    #endregion
            }
        }
        #endregion
    }
}