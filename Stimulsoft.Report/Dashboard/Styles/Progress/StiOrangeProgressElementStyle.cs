#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;

namespace Stimulsoft.Report.Dashboard.Styles
{
    public class StiOrangeProgressElementStyle : StiProgressElementStyle
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiOrangeDashboardProgressStyle;
        #endregion

        #region Properties.override
        public override StiElementStyleIdent Ident => StiElementStyleIdent.Orange;

        public override Color BrushLight => ColorTranslator.FromHtml("#e6e6e6");

        public override Color BrushDark => ColorTranslator.FromHtml("#ed7d31");

        public override Color[] BrushDarkColors => new []
        {
            ColorTranslator.FromHtml("#ed7d31"),
            ColorTranslator.FromHtml("#ffc000"),
            ColorTranslator.FromHtml("#70ad47"),
            ColorTranslator.FromHtml("#9e480e"),
            ColorTranslator.FromHtml("#997300"),
            ColorTranslator.FromHtml("#43682b")
        };

        public override Color Foreground => ColorTranslator.FromHtml("#8c8c8c");
        #endregion
    }
}