#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;

namespace Stimulsoft.Report.Dashboard.Styles
{
    public class StiTurquoiseProgressElementStyle : StiProgressElementStyle
    {
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiTurquoiseDashboardProgressStyle;
        #endregion

        #region Properties.override
        public override StiElementStyleIdent Ident => StiElementStyleIdent.Turquoise;

        public override Color BrushLight => ColorTranslator.FromHtml("#e6e6e6");

        public override Color BrushDark => ColorTranslator.FromHtml("#2ec6c8");

        public override Color[] BrushDarkColors => new[]
        {
            ColorTranslator.FromHtml("#2ec6c8"),
            ColorTranslator.FromHtml("#b5a1dd"),
            ColorTranslator.FromHtml("#5ab0ee"),
            ColorTranslator.FromHtml("#f4984e"),
            ColorTranslator.FromHtml("#d77a80"),
            ColorTranslator.FromHtml("#d04456")
        };

        public override Color Foreground => ColorTranslator.FromHtml("#8c8c8c");
        #endregion
    }
}