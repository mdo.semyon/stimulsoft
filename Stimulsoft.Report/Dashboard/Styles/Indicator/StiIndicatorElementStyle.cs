#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.Helpers;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Stimulsoft.Report.Dashboard.Styles
{
    public abstract class StiIndicatorElementStyle : StiElementStyle
    {
        #region Properties
        public abstract Color Brush { get; }

        public abstract Color Foreground { get; }
        #endregion

        #region Methods
        public void DrawStyleForGallery(Graphics g, Rectangle rect, StiFontIcons indicatorFontIcons)
        {
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.CompositingQuality = CompositingQuality.HighQuality;

            g.FillRectangle(Brushes.White, rect);

            string icon = StiFontIconsHelper.GetContent(indicatorFontIcons);
            RectangleF rectIcon;
            using (var font = new Font(StiFontIconsHelper.GetFontFamaliIcons(), 20f))
            using (var iconBrush = new SolidBrush(this.Brush))
            {
                var iconSize = g.MeasureString(icon, font);
                rectIcon = new RectangleF(rect.Right - iconSize.Width - 8, rect.Top + (rect.Height - iconSize.Height) / 2, iconSize.Width, iconSize.Height);
                g.DrawString(icon, font, iconBrush, rectIcon);
            }

            var textRect = new RectangleF(rect.Left, rect.Top, rect.Width - rectIcon.Width, rect.Height);
            using (var textFont = new Font("Arial", 20f, FontStyle.Bold, GraphicsUnit.Pixel))
            using (var textBrush = new SolidBrush(this.Foreground))
            using (var sf = new StringFormat())
            {
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;

                g.DrawString("1000", textFont, textBrush, textRect, sf);
            }
        }
        #endregion
    }
}