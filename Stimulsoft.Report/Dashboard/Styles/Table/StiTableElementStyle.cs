#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;

namespace Stimulsoft.Report.Dashboard.Styles
{
    public abstract class StiTableElementStyle : StiElementStyle
    {
        #region Properties
        public abstract Color LineHighlight { get; }

        public abstract Color BrushLight { get; }

        public abstract Color BrushDark { get; }

        public virtual Color HeaderForeground => Color.White;
        #endregion

        #region Methods
        public void DrawStyleForGallery(Graphics g, Rectangle rect)
        {
            var rect1 = new Rectangle(rect.X + 15, rect.Y + 5, rect.Width - 30, rect.Height - 10);
            int cellWidth = rect1.Width / 3;
            int cellHeight = rect1.Height / 5;

            var headerBrush = new SolidBrush(ColorTranslator.FromHtml("#ededed"));
            var row1Brush = new SolidBrush(Color.White);
            var row2Brush = new SolidBrush(BrushLight);

            var lineLightBrush = new SolidBrush(ColorTranslator.FromHtml("#c0c0c0"));

            int x = rect1.X;
            int y = rect1.Y;
            for (int row = 0; row < 5; row++)
            {
                for (int column = 0; column < 3; column++)
                {
                    var rect2 = new Rectangle(x, y, cellWidth, cellHeight);

                    if (row == 0)
                    {
                        g.FillRectangle(headerBrush, rect2);
                        g.FillRectangle(lineLightBrush, new Rectangle(rect2.Right - 1, rect2.Top, 1, rect2.Height));
                    }
                    else
                    {
                        var brush = (row % 2 == 0) ? row2Brush : row1Brush;
                        g.FillRectangle(brush, rect2);
                        g.FillRectangle(lineLightBrush, new Rectangle(rect2.Right - 1, rect2.Top, 1, rect2.Height));
                    }
                    x += cellWidth;
                }

                x = rect1.X;
                y += cellHeight;
            }

            using (var pen = new Pen(lineLightBrush, 1))
            {
                g.DrawRectangle(pen, new Rectangle(rect1.X, rect1.Y, cellWidth * 3 - 1, cellHeight * 5));
            }
        }
        #endregion
    }
}