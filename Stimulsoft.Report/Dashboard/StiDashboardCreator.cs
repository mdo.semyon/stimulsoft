#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Linq;
using Stimulsoft.Report.Components;

namespace Stimulsoft.Report.Dashboard
{
	public static class StiDashboardCreator
	{
        public static IStiDashboard CreateDashboard(StiReport report)
        {
            var comp = StiOptions.Services.Components.FirstOrDefault(c => c is IStiDashboard).Clone() as StiComponent;
            comp.Report = report;
            return comp as IStiDashboard;
        }

        public static IStiChartElement CreateChartElement(StiReport report)
        {
            var comp = StiOptions.Services.Components.FirstOrDefault(c => c is IStiChartElement).Clone() as StiComponent;
            comp.Page = report.GetCurrentPage();
            return comp as IStiChartElement;
        }

        public static IStiGaugeElement CreateGaugeElement(StiReport report)
        {
            var comp = StiOptions.Services.Components.FirstOrDefault(c => c is IStiGaugeElement).Clone() as StiComponent;
            comp.Page = report.GetCurrentPage();
            return comp as IStiGaugeElement;
        }

        public static IStiImageElement CreateImageElement(StiReport report)
        {
            var comp = StiOptions.Services.Components.FirstOrDefault(c => c is IStiImageElement).Clone() as StiComponent;
            comp.Page = report.GetCurrentPage();
            return comp as IStiImageElement;
        }

        public static IStiMapElement CreateMapElement(StiReport report)
        {
            var comp = StiOptions.Services.Components.FirstOrDefault(c => c is IStiMapElement).Clone() as StiComponent;
            comp.Page = report.GetCurrentPage();
            return comp as IStiMapElement;
        }

        public static IStiPivotElement CreatePivotElement(StiReport report)
        {
            var comp = StiOptions.Services.Components.FirstOrDefault(c => c is IStiPivotElement).Clone() as StiComponent;
            comp.Page = report.GetCurrentPage();
            return comp as IStiPivotElement;
        }

        public static IStiTableElement CreateTableElement(StiReport report)
        {
            var comp = StiOptions.Services.Components.FirstOrDefault(c => c is IStiTableElement).Clone() as StiComponent;
            comp.Page = report.GetCurrentPage();
            return comp as IStiTableElement;
        }

        public static IStiTextElement CreateTextElement(StiReport report)
        {
            var comp = StiOptions.Services.Components.FirstOrDefault(c => c is IStiTextElement).Clone() as StiComponent;
            comp.Page = report.GetCurrentPage();
            return comp as IStiTextElement;
        }
    }
}
