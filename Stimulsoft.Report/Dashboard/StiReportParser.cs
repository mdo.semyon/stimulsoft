#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Data.Engine;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Engine;
using System.Collections.Generic;

namespace Stimulsoft.Report.Dashboard
{
    public static class StiReportParser
    {
        #region Fields.Static
        private static object lockObject = new object();
        #endregion

        #region Fields
        private static Dictionary<string, string> expressionCache = new Dictionary<string, string>();
        #endregion

        #region Methods
        internal static void Clear()
        {
            lock (lockObject)
            {
                expressionCache = new Dictionary<string, string>();
            }
        }

        public static string Parse(string expression, StiComponent component)
        {
            //Expression contains '{'. If not than that text doesn't contain expression.
            if (expression != null && !expression.Contains("{"))
                return expression;

            var result = GetFromCache(expression);
            if (result != null)
                return result;

            result = ParseOrDefault(expression, component);

            AddToCache(expression, result);

            return result;
        }

        private static string ParseOrDefault(string expression, StiComponent component)
        {
            string str;

            return !TryParse(expression, out str, component) ? expression : str;
        }

        private static bool TryParse(string expression, out string result, StiComponent component)
        {
            try
            {
                if (expression == null || !expression.Contains("{"))
                {
                    result = expression;
                    return true;
                }

                var value = StiParser.ParseTextValue(expression, component, new StiParserParameters
                {
                    SyntaxCaseSensitive = false
                });

                result = value != null ? value.ToString() : "";
                return true;
            }
            catch
            {
                result = string.Empty;
                return false;
            }
        }

        internal static string GetFromCache(string expression)
        {
            lock (lockObject)
            {
                if (string.IsNullOrEmpty(expression))
                    return string.Empty;

                if (string.IsNullOrWhiteSpace(expression))
                    return expression;

                return expressionCache.ContainsKey(expression) ? expressionCache[expression] : null;
            }
        }

        internal static void AddToCache(string expression, string result)
        {
            lock (lockObject)
            {
                if (result == null)
                    result = string.Empty;

                expressionCache[expression] = result;
            }
        }
        #endregion
    }
}