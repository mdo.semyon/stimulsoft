#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Collections.Generic;
using System.Drawing;

namespace Stimulsoft.Report.Dashboard
{
    public static class StiMapDrawingCache
    {
        #region Fields.Static
        private static Dictionary<string, Image> lastImageCache = new Dictionary<string, Image>();
        #endregion

        #region Methods
        public static void Clear()
        {
            lock (lastImageCache)
            {
                lastImageCache.Clear();
            }
        }

        public static Image GetLastImage(IStiMapElement mapElement)
        {
            lock (lastImageCache)
            {
                if (lastImageCache.ContainsKey(mapElement.Key))
                    return lastImageCache[mapElement.Key];
                else
                    return null;
            }
        }

        public static void StoreLastImage(IStiMapElement mapElement, Image image)
        {
            lock (lastImageCache)
            {
                lastImageCache[mapElement.Key] = image;
            }
        }
        #endregion
    }
}