#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Meters;
using Stimulsoft.Data.Engine;
using Stimulsoft.Report.Dictionary;
using System.Collections.Generic;

namespace Stimulsoft.Report.Dashboard
{
    public interface IStiTableElement : 
        IStiElement,
        IStiDataSortCase,
        IStiDataFilterCase,
        IStiDashboardElementStyle
    {
        void CreateMeters(IStiTableElement tableElement);

	    void CreateMeters(StiDataSource dataSource);

        void CreateMeter(StiDataColumn dataColumn);

        void RemoveMeter(int index);

        void InsertMeter(int index, IStiMeter meter);

        void InsertNewDimension(int index);

        void InsertNewMeasure(int index);

        IStiMeter GetMeasure(StiDataColumn dataColumn);

        IStiMeter GetDimension(StiDataColumn dataColumn);
    }
}
