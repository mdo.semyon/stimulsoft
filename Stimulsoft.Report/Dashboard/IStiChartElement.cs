#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Meters;
using Stimulsoft.Report.Chart;
using Stimulsoft.Report.Dictionary;
using System.Collections.Generic;

namespace Stimulsoft.Report.Dashboard
{
	public interface IStiChartElement : 
        IStiElement,
        IStiDashboardElementStyle
    {
        #region Value
        void AddValue(StiDataColumn dataColumn);

        IStiMeter GetValue(StiDataColumn dataColumn);

        IStiMeter GetValue(IStiMeter meter);

        IStiMeter GetValueByIndex(int index);

        void InsertValue(int index, IStiMeter meter);

        void RemoveValue(int index);

        IStiMeter CreateNewValue();
        #endregion

        #region Argument
        void AddArgument(StiDataColumn dataColumn);

        IStiMeter GetArgument(StiDataColumn dataColumn);

        IStiMeter GetArgument(IStiMeter meter);

        IStiMeter GetArgumentByIndex(int index);

        void InsertArgument(int index, IStiMeter meter);

        void RemoveArgument(int index);

        void CreateNewArgument();
        #endregion

        #region Weight
        void AddWeight(StiDataColumn dataColumn);

        IStiMeter GetWeight(StiDataColumn dataColumn);

        IStiMeter GetWeight(IStiMeter meter);

        IStiMeter GetWeightByIndex(int index);

        void InsertWeight(int index, IStiMeter meter);

        void RemoveWeight(int index);

        void CreateNewWeight();
        #endregion

        #region Series
        void AddSeries(StiDataColumn dataColumn);

        IStiMeter GetSeries(StiDataColumn dataColumn);

        IStiMeter GetSeries(IStiMeter meter);

        IStiMeter GetSeries();

        void InsertSeries(IStiMeter meter);

        void RemoveSeries();

        void CreateNewSeries();
        #endregion

        StiChart GetChartComponent();

        void ConvertToBubble();

        void ConvertFromBubble();

        List<IStiMeter> GetOriginalMeters();

        List<string> GetChartSeriesTypes();
    }
}