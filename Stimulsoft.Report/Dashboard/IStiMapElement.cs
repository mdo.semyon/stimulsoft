#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Meters;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Maps;
using System.Collections.Generic;

namespace Stimulsoft.Report.Dashboard
{
    public interface IStiMapElement : 
        IStiElement,
        IStiDashboardElementStyle
    {
        StiMapID MapID { get; set; }

        StiMapMode MapMode { get; set; }

        StiMapSource DataFrom { get; set; }

        string MapData { get; set; }

        StiMapType MapType { get; set; }

        bool ShowValue { get; set; }

        bool ColorEach { get; set; }

        StiDisplayNameType ShowName { get; set; }

        List<StiMapData> GetMapData();

        #region Key
        void AddKeyMeter(StiDataColumn dataColumn);

        void AddKeyMeter(IStiMeter meter);

        IStiMeter GetKeyMeter();

        void RemoveKeyMeter();

        void CreateNewKeyMeter();
        #endregion

        #region Name
        void AddNameMeter(StiDataColumn dataColumn);

        void AddNameMeter(IStiMeter meter);

        IStiMeter GetNameMeter();

        void RemoveNameMeter();

        void CreateNewNameMeter();
        #endregion

        #region Value
        void AddValueMeter(StiDataColumn dataColumn);

        void AddValueMeter(IStiMeter meter);

        IStiMeter GetValueMeter();

        void RemoveValueMeter();

        void CreateNewValueMeter();
        #endregion

        #region Group
        void AddGroupMeter(StiDataColumn dataColumn);

        void AddGroupMeter(IStiMeter meter);

        IStiMeter GetGroupMeter();

        void RemoveGroupMeter();

        void CreateNewGroupMeter();
        #endregion

        #region Color
        void AddColorMeter(StiDataColumn dataColumn);

        void AddColorMeter(IStiMeter meter);

        IStiMeter GetColorMeter();

        void RemoveColorMeter();

        void CreateNewColorMeter();
        #endregion

        #region Latitude
        void AddLatitudeMeter(StiDataColumn dataColumn);

        void AddLatitudeMeter(IStiMeter meter);

        IStiMeter GetLatitudeMeter();

        void RemoveLatitudeMeter();

        void CreateNewLatitudeMeter();
        #endregion

        #region Longitude
        void AddLongitudeMeter(StiDataColumn dataColumn);

        void AddLongitudeMeter(IStiMeter meter);

        IStiMeter GetLongitudeMeter();

        void RemoveLongitudeMeter();

        void CreateNewLongitudeMeter();
        #endregion
    }
}