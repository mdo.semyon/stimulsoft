#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;

namespace Stimulsoft.Report.Dashboard
{
	public static class StiDashboardHelperCreator
	{
        public static IStiTableElementAutoSizer CreateTableElementAutoSizer(IStiTableElement tableElement)
	    {
	        try
	        {
	            if (StiDashboardDrawingAssembly.Assembly == null) return null;

	            var type = StiDashboardDrawingAssembly.Assembly.GetType("Stimulsoft.Dashboard.Drawing.Helpers.StiTableElementAutoSizer");
	            if (type == null) return null;

	            return StiActivator.CreateObject(type) as IStiTableElementAutoSizer;
	        }
	        catch
	        {
	        }

	        return null;
	    }
	}
}
