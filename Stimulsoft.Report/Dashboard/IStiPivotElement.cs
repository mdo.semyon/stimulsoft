#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Meters;
using Stimulsoft.Report.Dictionary;

namespace Stimulsoft.Report.Dashboard
{
    public interface IStiPivotElement :
        IStiElement,
        IStiDashboardElementStyle
    {
        #region Column
        void CreateNewColumn();

        IStiMeter GetColumn(StiDataColumn dataColumn);

        IStiMeter GetColumn(IStiMeter meter);

        IStiMeter GetColumnByIndex(int index);

        void InsertColumn(int index, IStiMeter meter);

        void RemoveColumn(int index);
        #endregion

        #region Row
        void CreateNewRow();

        IStiMeter GetRow(StiDataColumn dataColumn);

        IStiMeter GetRow(IStiMeter meter);

        IStiMeter GetRowByIndex(int index);

        void InsertRow(int index, IStiMeter meter);

        void RemoveRow(int index);
        #endregion

        #region Summary
        void CreateNewSummary();

        IStiMeter GetSummary(StiDataColumn dataColumn);

        IStiMeter GetSummary(IStiMeter meter);

        IStiMeter GetSummaryByIndex(int index);

        void InsertSummary(int index, IStiMeter meter);

        void RemoveSummary(int index);
        #endregion

        void CreateNextMeter(StiDataColumn dataColumn);
    }
}
