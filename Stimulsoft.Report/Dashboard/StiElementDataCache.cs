#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Helpers;
using Stimulsoft.Data.Engine;
using Stimulsoft.Data.Helpers;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dashboard.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;

namespace Stimulsoft.Report.Dashboard
{
    public static class StiElementDataCache
    {
        #region Fields.Static
        private static object lockObject = new object();
        private static BackgroundWorker worker;
        private static List<IStiElement> elements = new List<IStiElement>();
        private static object lockCacheObject = new object();
        private static Dictionary<int, StiDataTable> hashCache = new Dictionary<int, StiDataTable>();
        private static Dictionary<string, List<StiDataTable>> keyCache = new Dictionary<string, List<StiDataTable>>();
        #endregion

        #region Events
        public static event EventHandler DataInit;
        #endregion

        #region Methods
        public static StiDataTable GetOrCreate(IStiElement element)
        {
            lock (lockObject)
            {
                element.Key = StiKeyHelper.GetOrGeneratedKey(element.Key);
                var hashCode = GetUniqueCode(element);

                var dataTable = GetDataTableFromCache(hashCode);
                if (dataTable != null)
                    return dataTable;

                var dashboard = element.Page as IStiDashboard;

                dataTable = StiDataAnalyzer.Analyse(dashboard, element.GetMeters(), dashboard.GetDataFilterRules());
                AddDataTableToCache(element, hashCode, dataTable);

                return dataTable;
            }
        }

        public static StiDataTable GetOrCreateWithProgress(IStiElement element)
        {
            lock (lockObject)
            {
                element.Key = StiKeyHelper.GetOrGeneratedKey(element.Key);
                var hashCode = GetUniqueCode(element);

                var dataTable = GetDataTableFromCache(hashCode);
                if (dataTable != null)
                    return dataTable;

                if (elements.Contains(element))
                    return null;

                StiComponentProgressHelper.Add(element);
                elements.Add(element);

                InitWorker();

                return null;
            }
        }        
        
        private static void InitWorker()
        {
            if (worker != null) return;

            worker = new BackgroundWorker();
            worker.DoWork += delegate
            {
                while (true)
                {
                    if (elements.Count > 0)
                    {
                        IStiElement element = null;

                        lock (lockObject)
                        {
                            element = elements.FirstOrDefault();
                            elements.Remove(element);
                        }

                        try
                        {
                            var hashCode = GetUniqueCode(element);
                            var dashboard = element.Page as IStiDashboard;

                            var dataTable = StiDataAnalyzer.Analyse(dashboard, element.GetMeters(), dashboard.GetDataFilterRules());

                            AddDataTableToCache(element, hashCode, dataTable);

                            StiComponentProgressHelper.Remove(element);
                            DataInit?.Invoke(element, EventArgs.Empty);
                        }
                        catch
                        {
                            StiComponentProgressHelper.Remove(element, true);
                        }
                    }

                    Thread.Sleep(300);
                }
            };
            worker.RunWorkerAsync();
        }

        private static int GetUniqueCode(IStiElement element)
        {
            var meters = element?.GetMeters();
            if (meters == null || !meters.Any()) return 0;

            var index = 0;
            var keys = meters.Select(m => index++.ToString() + m.GetUniqueCode().ToString());
            return StiHashCodeHelper.GetOrderIndependentHashCode(keys);
        }
        #endregion

        #region Methods
        internal static void ClearCache()
        {
            lock (lockCacheObject)
            {
                hashCache.Clear();
                keyCache.Clear();
            }
        }

        internal static StiDataTable GetDataTableFromCache(int hashCode)
        {
            lock (lockCacheObject)
            {
                return hashCache.ContainsKey(hashCode) ? hashCache[hashCode] : null;
            }
        }

        internal static void AddDataTableToCache(IStiAppComponent element, int hashCode, StiDataTable dataTable)
        {
            lock (lockCacheObject)
            {
                hashCache[hashCode] = dataTable;

                var keys = keyCache.ContainsKey(element.GetKey()) ? keyCache[element.GetKey()] : null;
                if (keys == null)
                {
                    keys = new List<StiDataTable>();
                    keyCache.Add(element.GetKey(), keys);
                }
                else
                    keys.Clear();

                keys.Add(dataTable);
            }
        }
        #endregion
    }
}