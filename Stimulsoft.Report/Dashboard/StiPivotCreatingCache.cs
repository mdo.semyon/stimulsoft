﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.CrossTab;
using System.Collections.Generic;

namespace Stimulsoft.Report.Dashboard
{
    public static class StiPivotCreatingCache
    {
        public static Dictionary<IStiPivotElement, StiCrossTab> CrossTabs { get; set; } = new Dictionary<IStiPivotElement, StiCrossTab>();

        public static Dictionary<IStiPivotElement, IStiPivotGridContainer> Containers { get; set; } = new Dictionary<IStiPivotElement, IStiPivotGridContainer>();

        public static Dictionary<IStiPivotElement, bool> Converted { get; set; } = new Dictionary<IStiPivotElement, bool>();

        public static void Clear()
        {
            CrossTabs = new Dictionary<IStiPivotElement, StiCrossTab>();
            Converted = new Dictionary<IStiPivotElement, bool>();
            Containers = new Dictionary<IStiPivotElement, IStiPivotGridContainer>();
        }
    }
}
