#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Helpers
{
    public static class StiFormHelper
    {
        #region Methods
        public static void LoadStateForm(Form form, string key, ref Point position, ref Size size)
        {
            StiSettings.Load();

            int primaryIndex = GetPrimaryScreenNumber();
            if (string.IsNullOrEmpty(key)) key = form.GetType().Name;

            int screenNumber = StiSettings.GetInt(key, "ScreenNumber", primaryIndex);
            position.X = StiSettings.GetInt(key, "WindowXPos", 0);
            position.Y = StiSettings.GetInt(key, "WindowYPos", 0);

            size.Width = StiSettings.GetInt(key, "WindowWidth", size.Width);
            size.Height = StiSettings.GetInt(key, "WindowHeight", size.Height);

            Screen currentScreen = null;
            if (screenNumber != primaryIndex)
            {
                if (screenNumber + 1 > Screen.AllScreens.Length)
                {
                    currentScreen = Screen.PrimaryScreen;
                    position = currentScreen.Bounds.Location;
                }
                else
                {
                    currentScreen = Screen.AllScreens[screenNumber];
                    if (!currentScreen.Bounds.Contains(new Rectangle(position, size)))
                    {
                        position = currentScreen.Bounds.Location;
                    }
                }
            }
            else
            {
                currentScreen = Screen.PrimaryScreen;

                // ���� ScreenNumber �� �����, ����� ������ ��������� �������
                if (StiSettings.GetInt(key, "ScreenNumber", -1) == -1)
                    position = currentScreen.Bounds.Location;
            }

            if (form.WindowState == FormWindowState.Normal &&
                (StiSettings.GetInt(key, "WindowXPos", int.MaxValue) == int.MaxValue && StiSettings.GetInt(key, "WindowYPos", int.MaxValue) == int.MaxValue))
            {
                Rectangle rect = currentScreen.WorkingArea;
                position = new Point(rect.Left + ((rect.Width - size.Width) / 2), rect.Top + ((rect.Height - size.Height) / 2));
            }
        }

        public static void SaveStateForm(Form form, string key)
        {
            Point p;
            Size formSize;
            int screenNumber = GetScreenNumber(form, out p, out formSize);

            if (string.IsNullOrEmpty(key)) 
                key = form.GetType().Name;

            StiSettings.Load();
            StiSettings.Set(key, "ScreenNumber", screenNumber);
            StiSettings.Set(key, "WindowXPos", p.X);
            StiSettings.Set(key, "WindowYPos", p.Y);
            StiSettings.Set(key, "WindowWidth", formSize.Width);
            StiSettings.Set(key, "WindowHeight", formSize.Height);
            StiSettings.Save();
        }
        #endregion

        #region Methods.Helpers

        private static int GetScreenNumber(Form form, out Point p, out Size formSize)
        {
            if (form.WindowState == FormWindowState.Maximized)
            {
                p = form.RestoreBounds.Location;
                formSize = form.RestoreBounds.Size;
            }
            else
            {
                p = form.Location;
                formSize = form.Size;
            }

            if (Screen.AllScreens.Length > 1)
            {
                for (int index = 0; index < Screen.AllScreens.Length; index++)
                {
                    var screen = Screen.AllScreens[index];
                    if (screen.Bounds.IntersectsWith(new Rectangle(p, formSize)))
                    {
                        return index;
                    }
                }
            }

            return GetPrimaryScreenNumber();
        }

        public static int GetPrimaryScreenNumber()
        {
            int index = 0;
            int primaryIndex = 0;
            while (index < Screen.AllScreens.Length)
            {
                if (Screen.AllScreens[index].Primary)
                {
                    primaryIndex = index;
                    break;
                }
                index++;
            }

            return primaryIndex;
        }
        #endregion
    }
}