﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

namespace Stimulsoft.Report.Helpers
{
    #region StiImageType
    public enum StiImageType
    {
        GdiImage,
        SvgObject
    }
    #endregion

    #region StiFontIconSet
    public enum StiFontIconSet
    {
        Rating,
        Quarter,
        Square,
        Star,
        Latin
    }
    #endregion

    #region StiFontIcons
    public enum StiFontIcons
    {
        Latin4,
        Latin3,
        Latin2,
        Latin1,
        QuarterFull,
        QuarterThreeFourth,
        QuarterHalf,
        QuarterQuarter,
        QuarterNone,
        Rating4,
        Rating3,
        Rating2,
        Rating1,
        Rating0,
        Square0,
        Square1,
        Square2,
        Square3,
        Square4,
        StarFull,
        StarThreeFourth,
        StarHalf,
        StarQuarter,
        StarNone,
        ArrowDown,
        ArrowRight,
        ArrowRightDown,
        ArrowRightUp,
        ArrowUp,
        Check,
        Circle,
        CircleCheck,
        CircleCross,
        CircleExclamation,
        Cross,
        Rhomb,
        Exclamation,
        Flag,
        Minus,
        Triangle,
        TriangleDown,
        TriangleUp,

        Home,
        Cart,
        Phone,
        Mobile,
        Mug,
        Airplane,
        Man,
        Woman,
        UserTie,
        Truck,
        Earth,
        ManWoman,
        Appleinc,
        Android,
        Windows8
    }
    #endregion
}