﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Helpers
{
    public static class StiImageDialogPainter
    {
        #region Methods
        public static void Paint(Control control, Graphics g, bool drawDragDrop, bool drawBorder, bool isImage, bool drawBackground = true)
        {
            Paint(control, g, new byte[0], drawDragDrop, drawBorder, isImage, drawBackground);
        }

        public static void Paint(Control control, Graphics g, Image image, bool drawDragDrop, bool drawBorder, bool isImage, bool drawBackground = true)
        {
            var rect = new Rectangle(0, 0, control.Width, control.Height);

            Paint(rect, g, image, drawDragDrop, drawBorder, isImage, drawBackground);
        }

        public static void Paint(Control control, Graphics g, byte[] image, bool drawDragDrop, bool drawBorder, bool isImage, bool drawBackground = true)
        {
            using (var gdiImage = StiImageConverter.TryBytesToImage(image, control.Width, control.Height))
            {
                Paint(control, g, gdiImage, drawDragDrop, drawBorder, isImage, drawBackground);
            }
        }

        public static void Paint(Rectangle rect, Graphics g, Image image, bool drawDragDrop, bool drawBorder, bool isImage, bool drawBackground = true)
        {
            try
            {
                if (drawBackground)
                    DrawBackground(g, rect);

                if (image == null && drawDragDrop)
                    DrawDragDropLabel(g, rect, isImage);

                if (image != null)
                    DrawImage(g, image, rect);

                if (drawBorder)
                    DrawBorder(g, rect);
            }
            catch
            {
            }
        }

        private static void DrawDragDropLabel(Graphics g, Rectangle rect, bool isImage)
        {
            var str = isImage
                ? StiLocalization.Get("FormDictionaryDesigner", "TextDropImageHere")
                : StiLocalization.Get("FormDictionaryDesigner", "TextDropFileHere");

            rect.Inflate(-10, -10);

            using (var font = new Font("Arial", 8))
            using (var sf = new StringFormat {Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center})
            {
                g.DrawString(str, font, Brushes.DimGray, rect, sf);
            }
        }

        public static void DrawImage(Graphics g, Image image, Rectangle rect)
        {
            var rectOrigin = rect;
            rect.Inflate(-2, -2);
            if (image.Width < rect.Width && image.Height < rect.Height)
            {
                g.DrawImage(image, (rectOrigin.Width - image.Width) / 2, (rectOrigin.Height - image.Height) / 2, image.Width, image.Height);
            }
            else
            {
                var scaleX = (float) rect.Width / (float) image.Width;
                var scaleY = (float) rect.Height / (float) image.Height;

                if (scaleX < scaleY)
                {
                    var imageWidth = image.Width * scaleX;
                    var imageHeight = image.Height * scaleX;
                    g.DrawImage(image, (rectOrigin.Width - imageWidth) / 2, (rectOrigin.Height - imageHeight) / 2, imageWidth, imageHeight);
                }
                else
                {
                    var imageWidth = image.Width * scaleY;
                    var imageHeight = image.Height * scaleY;
                    g.DrawImage(image, (rectOrigin.Width - imageWidth) / 2, (rectOrigin.Height - imageHeight) / 2, imageWidth, imageHeight);
                }
            }
        }

        private static void DrawBackground(Graphics g, Rectangle rect)
        {
            g.Clear(Color.FromArgb(249, 249, 249));
        }

        private static void DrawBorder(Graphics g, Rectangle rect)
        {
            using (var pen = new Pen(SystemColors.ControlDark))
            {
                pen.DashStyle = DashStyle.Dash;
                g.DrawRectangle(pen, 0, 0, rect.Width - 1, rect.Height - 1);
            }
        }
        #endregion
    }
}
