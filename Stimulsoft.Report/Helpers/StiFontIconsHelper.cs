﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.Text;

namespace Stimulsoft.Report.Helpers
{
    public static class StiFontIconsHelper
    {
        #region Fields
        private static PrivateFontCollection pfc = new PrivateFontCollection();
        #endregion

        #region Methods
        internal static FontFamily GetFontFamaliIcons()
        {
            if (pfc.Families.Length == 0)
            {
                using (var fontStream = typeof(StiFontIconsHelper).Assembly.GetManifestResourceStream("Stimulsoft.Report.Resources.Stimulsoft.ttf"))
                {
                    if (null == fontStream)
                        return null;

                    int fontStreamLength = (int)fontStream.Length;

                    var data = Marshal.AllocCoTaskMem(fontStreamLength);

                    var fontData = new byte[fontStreamLength];
                    fontStream.Read(fontData, 0, fontStreamLength);

                    Marshal.Copy(fontData, 0, data, fontStreamLength);

                    pfc.AddMemoryFont(data, fontStreamLength);

                    Marshal.FreeCoTaskMem(data);
                }
            }

            if (pfc.Families.Length == 0)
                return null;

            return pfc.Families[0];
        }

        internal static string GetContent(StiFontIcons fontIcons)
        {
            switch (fontIcons)
            {
                case StiFontIcons.ArrowDown:
                    return '\ue900'.ToString();

                case StiFontIcons.ArrowRight:
                    return '\ue901'.ToString();

                case StiFontIcons.ArrowRightDown:
                    return '\ue902'.ToString();

                case StiFontIcons.ArrowRightUp:
                    return '\ue903'.ToString();

                case StiFontIcons.ArrowUp:
                    return '\ue904'.ToString();

                case StiFontIcons.Check:
                    return '\ue905'.ToString();

                case StiFontIcons.Circle:
                    return '\ue906'.ToString();

                case StiFontIcons.CircleCheck:
                    return '\ue907'.ToString();

                case StiFontIcons.CircleCross:
                    return '\ue908'.ToString();

                case StiFontIcons.CircleExclamation:
                    return '\ue909'.ToString();

                case StiFontIcons.Cross:
                    return '\ue90a'.ToString();

                case StiFontIcons.Exclamation:
                    return '\ue90b'.ToString();

                case StiFontIcons.Flag:
                    return '\ue90c'.ToString();

                case StiFontIcons.Latin1:
                    return '\ue90d'.ToString();

                case StiFontIcons.Latin2:
                    return '\ue90e'.ToString();

                case StiFontIcons.Latin3:
                    return '\ue90f'.ToString();

                case StiFontIcons.Latin4:
                    return '\ue910'.ToString();

                case StiFontIcons.Minus:
                    return '\ue911'.ToString();

                case StiFontIcons.QuarterFull:
                    return '\ue912'.ToString();

                case StiFontIcons.QuarterHalf:
                    return '\ue913'.ToString();

                case StiFontIcons.QuarterNone:
                    return '\ue914'.ToString();

                case StiFontIcons.QuarterQuarter:
                    return '\ue915'.ToString();

                case StiFontIcons.QuarterThreeFourth:
                    return '\ue916'.ToString();

                case StiFontIcons.Rating0:
                    return '\ue917'.ToString();

                case StiFontIcons.Rating1:
                    return '\ue918'.ToString();

                case StiFontIcons.Rating2:
                    return '\ue919'.ToString();

                case StiFontIcons.Rating3:
                    return '\ue91a'.ToString();

                case StiFontIcons.Rating4:
                    return '\ue91b'.ToString();

                case StiFontIcons.Rhomb:
                    return '\ue91c'.ToString();

                case StiFontIcons.Square0:
                    return '\ue91d'.ToString();

                case StiFontIcons.Square1:
                    return '\ue91e'.ToString();

                case StiFontIcons.Square2:
                    return '\ue91f'.ToString();

                case StiFontIcons.Square3:
                    return '\ue920'.ToString();

                case StiFontIcons.Square4:
                    return '\ue921'.ToString();

                case StiFontIcons.StarFull:
                    return '\ue922'.ToString();

                case StiFontIcons.StarHalf:
                    return '\ue923'.ToString();

                case StiFontIcons.StarNone:
                    return '\ue924'.ToString();

                case StiFontIcons.StarQuarter:
                    return '\ue925'.ToString();

                case StiFontIcons.StarThreeFourth:
                    return '\ue926'.ToString();

                case StiFontIcons.Triangle:
                    return '\ue927'.ToString();

                case StiFontIcons.TriangleDown:
                    return '\ue928'.ToString();

                case StiFontIcons.TriangleUp:
                    return '\ue929'.ToString();

                case StiFontIcons.Home:
                    return '\ue92a'.ToString();

                case StiFontIcons.Cart:
                    return '\ue93a'.ToString();

                case StiFontIcons.Phone:
                    return '\ue942'.ToString();

                case StiFontIcons.UserTie:
                    return '\ue976'.ToString();

                case StiFontIcons.Mobile:
                    return '\ue958'.ToString();

                case StiFontIcons.Mug:
                    return '\ue9a2'.ToString();

                case StiFontIcons.Airplane:
                    return '\ue9af'.ToString();

                case StiFontIcons.Truck:
                    return '\ue9b0'.ToString();

                case StiFontIcons.Earth:
                    return '\ue9ca'.ToString();

                case StiFontIcons.Man:
                    return '\ue9dc'.ToString();

                case StiFontIcons.Woman:
                    return '\ue9dd'.ToString();

                case StiFontIcons.ManWoman:
                    return '\ue9de'.ToString();

                case StiFontIcons.Appleinc:
                    return '\ueabe'.ToString();

                case StiFontIcons.Android:
                    return '\ueac0'.ToString();

                case StiFontIcons.Windows8:
                    return '\ueac2'.ToString();

            }

            return null;
        }


        internal static string GetIsonSetContent(StiFontIconSet iconSet)
        {
            var builtString = new StringBuilder();
            foreach (var icon in StiFontIconsHelper.GetFontIcons(iconSet))
            {
                builtString.Append(StiFontIconsHelper.GetContent(icon));
                builtString.Append(' ');
            }

            return builtString.ToString();
        }

        internal static List<StiFontIcons> GetFontIcons(StiFontIconSet iconSet)
        {
            switch (iconSet) {
                case StiFontIconSet.Rating:
                    return new List<StiFontIcons> { StiFontIcons.Rating0, StiFontIcons.Rating1, StiFontIcons.Rating2, StiFontIcons.Rating3, StiFontIcons.Rating4 };
                    
                case StiFontIconSet.Quarter:
                    return new List<StiFontIcons> { StiFontIcons.QuarterNone, StiFontIcons.QuarterQuarter, StiFontIcons.QuarterHalf, StiFontIcons.QuarterThreeFourth, StiFontIcons.QuarterFull };

                case StiFontIconSet.Square:
                    return new List<StiFontIcons> { StiFontIcons.Square4, StiFontIcons.Square3, StiFontIcons.Square2, StiFontIcons.Square1, StiFontIcons.Square0 };

                case StiFontIconSet.Star:
                    return new List<StiFontIcons> { StiFontIcons.StarNone, StiFontIcons.StarQuarter, StiFontIcons.StarHalf, StiFontIcons.StarThreeFourth, StiFontIcons.StarFull };

                case StiFontIconSet.Latin:
                    return new List<StiFontIcons> { StiFontIcons.Latin1, StiFontIcons.Latin2, StiFontIcons.Latin3, StiFontIcons.Latin4 };
            }

            return new List<StiFontIcons> { StiFontIcons.Rating0, StiFontIcons.Rating1, StiFontIcons.Rating2, StiFontIcons.Rating3, StiFontIcons.Rating4 };
        }
        #endregion
    }
}
