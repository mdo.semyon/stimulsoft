#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Collections;
using System.Collections.Generic;
using Stimulsoft.Report.Engine;

namespace Stimulsoft.Report.Components
{	
	public class StiDataBandInfoV2 : StiComponentInfo
	{
		/// <summary>
		/// ��������� �������� ��� ����������� GroupHeaderBand's �������� DataBand. ��� ����������
		/// ��������� ���������� ������� ����� FindGroupHeaders.
		/// </summary>
		public StiComponentsCollection GroupHeaders { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� GroupFooterBand's �������� DataBand. ��� ����������
        /// ��������� ���������� ������� ����� FindGroupFooters.
        /// </summary>
        public StiComponentsCollection GroupFooters { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� DataBand's �������� DataBand. ��� ����������
        /// ��������� ���������� ������� ����� FindDetailDataBands.
        /// </summary>
        public StiComponentsCollection DetailDataBands { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� ���������� �������� DataBand. ��� ����������
        /// ��������� ���������� ������� ����� FindDetails.
        /// </summary>
        public StiComponentsCollection Details { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� SubReport's �������� DataBand. ��� ����������
        /// ��������� ���������� ������� ����� FindSubReports.
        /// </summary>
        public StiComponentsCollection SubReports { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� EmptyBand's �������� DataBand. ��� ����������
        /// ��������� ���������� ������� ����� FindEmptyBands.
        /// </summary>
        public StiComponentsCollection EmptyBands { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� HeaderBand's �������� DataBand. ��� ����������
        /// ��������� ���������� ������� ����� FindHeaders.
        /// </summary>
        public StiComponentsCollection Headers { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� HeaderBand's �������� HierarchicalDataBand. ��� ����������
        /// ��������� ���������� ������� ����� FindHierarchicalHeaders. ����� ����� �������� ����� ������ ������ FindHeaders.
        /// </summary>
        public StiComponentsCollection HierarchicalHeaders { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� Footer's, ������� ��������� �� ���� ���������,
        /// �������� DataBand. ��� ���������� ��������� ���������� ������� ����� FindFootersOnAllPages.
        /// </summary>
        public StiComponentsCollection FootersOnAllPages { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� Footer's, ������� ��������� � ����� ������,
        /// �������� DataBand. ��� ���������� ��������� ���������� ������� ����� FindFootersOnLastPage.
        /// </summary>
        public StiComponentsCollection FootersOnLastPage { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� Footer's, ������� ��������� � ����� ������ �������� HierarchicalDataBand. 
        /// ��� ���������� ��������� ���������� ������� ����� FindHierarchicalFooters. ����� ����� �������� ����� ������ ������ FindFooters.
        /// </summary>
        public StiComponentsCollection HierarchicalFooters { get; set; }

        /// <summary>
        /// ������ ����������� ���������� ����������� ��� GroupHeaderBand's.
        /// </summary>
        public bool[] GroupHeaderResults { get; set; }

        /// <summary>
        /// ������ ����������� ���������� ����������� ��� GroupFooterBand's.
        /// </summary>
        public bool[] GroupFooterResults { get; set; }

        public List<StiReportTitleBand> ReportTitles { get; set; }

        public List<StiReportSummaryBand> ReportSummaries { get; set; }

        /// <summary>
        /// ��������� �������� ��� ����������� DataBand's �������� DataBand, ������� ������� � BusinessObject � ��������� � ���-��������.
        /// ����� �� ���� ��������� �� ������ ��������� ��� details, �.�. ��� ��������� � ������ �����
        /// </summary>
        internal Hashtable DetailDataBandsFromSubReports { get; set; }

        /// <summary>
        /// ���� �������� ��������� ����� ParentBookmark. ����� ������������ ��� ���������� ���� ParentBookmark � ��������� ������, ��������� 
        /// ParentBookmark ����� ���� ������� �������.
        /// </summary>
        public StiBookmark StoredParentBookmark { get; set; }
}
}
