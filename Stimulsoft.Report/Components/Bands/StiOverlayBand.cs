#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Report.QuickButtons;
using Stimulsoft.Base.Json.Linq;
using System;

namespace Stimulsoft.Report.Components
{
    /// <summary>
    /// Desribes the class that realizes the band - Overlay Band.
    /// </summary>
    [StiServiceBitmap(typeof(StiOverlayBand), "Stimulsoft.Report.Images.Components.StiOverlayBand.png")]
    [StiToolbox(true)]
    [StiContextTool(typeof(IStiCanGrow))]
    [StiContextTool(typeof(IStiCanShrink))]
    [StiQuickButton("Stimulsoft.Report.QuickButtons.Design.StiSelectAllQuickButton, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfQuickButton("Stimulsoft.Report.WpfDesign.StiWpfSelectAllQuickButton, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
    public class StiOverlayBand :
        StiStaticBand,
        IStiVertAlignment
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // Old
            jObject.RemoveProperty("CanBreak");
            jObject.RemoveProperty("Bookmark");
            jObject.RemoveProperty("Hyperlink");

            // StiOverlayBand
            jObject.AddPropertyEnum("VertAlignment", VertAlignment, StiVertAlignment.Center);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "VertAlignment":
                        this.VertAlignment = (StiVertAlignment)Enum.Parse(typeof(StiVertAlignment), property.Value.ToObject<string>());
                        break;
                }
            }
        }

        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiOverlayBand;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();
            StiPropertyObject[] list;

            // DataCategory
            if (level != StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.VertAlignment()
                };
                objHelper.Add(StiPropertyCategories.Data, list);
            }

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Height()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Height(),
                    propHelper.MinHeight(),
                    propHelper.MaxHeight()
                };
            }
            objHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Brush(),
                    propHelper.Border(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Brush(),
                    propHelper.Border(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle(),
                    propHelper.UseParentStyles()
                };
            }
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.CanGrow(),
                    propHelper.CanShrink(),
                    propHelper.Enabled()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.CanGrow(),
                    propHelper.CanShrink(),
                    propHelper.Enabled(),
                    propHelper.PrintOn(),
                    propHelper.ResetPageNumber()
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Name()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias(),
                    propHelper.Restrictions(),
                    propHelper.Locked(),
                    propHelper.Linked()
                };
            }
            objHelper.Add(StiPropertyCategories.Design, list);

            return objHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // ValueEventsCategory
            var list = new[] { StiPropertyEventId.GetToolTipEvent, StiPropertyEventId.GetTagEvent };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            // NavigationEventsCategory
            list = new[] { StiPropertyEventId.GetHyperlinkEvent, StiPropertyEventId.GetBookmarkEvent };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[] { StiPropertyEventId.BeforePrintEvent, StiPropertyEventId.AfterPrintEvent };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // MouseEventsCategory
            list = new[] { StiPropertyEventId.GetDrillDownReportEvent, StiPropertyEventId.ClickEvent, StiPropertyEventId.DoubleClickEvent,
                StiPropertyEventId.MouseEnterEvent, StiPropertyEventId.MouseLeaveEvent };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties
        public override string HelpUrl => "user-manual/report_internals_watermarks_overlay_band.htm";
        #endregion

        #region IStiVertAlignment
        /// <summary>
        /// Gets or sets the vertical alignment of an object.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StiVertAlignment.Center)]
        [StiCategory("Data")]
        [StiOrder(StiPropertyOrder.BehaviorVertAlignment)]
        [TypeConverter(typeof(StiEnumConverter))]
        [Description("Gets or sets the vertical alignment of an object.")]
        [StiPropertyLevel(StiLevel.Standard)]
        public virtual StiVertAlignment VertAlignment { get; set; } = StiVertAlignment.Center;
        #endregion

        #region StiBand override
        /// <summary>
        /// Gets header start color.
        /// </summary>
        [Browsable(false)]
        public override Color HeaderStartColor => Color.FromArgb(131, 124, 174);

        /// <summary>
        /// Gets header end color.
        /// </summary>
        [Browsable(false)]
        public override Color HeaderEndColor => Color.FromArgb(131, 124, 174);
        #endregion

        #region StiComponent override
        /// <summary>
        /// Gets the type of processing when printing.
        /// </summary>
        public override StiComponentType ComponentType
        {
            get
            {
                if (Report != null && Report.EngineVersion != Engine.StiEngineVersion.EngineV1)
                    return StiComponentType.Static;

                else
                    return StiComponentType.Detail;
            }
        }

        /// <summary>
        /// Gets value to sort a position in the toolbox.
        /// </summary>
        public override int ToolboxPosition => (int)StiComponentToolboxPosition.OverlayBand;

        public override StiToolboxCategory ToolboxCategory => StiToolboxCategory.Bands;

        /// <summary>
        /// Gets a component priority.
        /// </summary>
        public override int Priority => (int)StiComponentPriority.OverlayBand;

        /// <summary>
        /// Gets a localized component name.
        /// </summary>
        public override string LocalizedName => StiLocalization.Get("Components", "StiOverlayBand");
        #endregion

        #region IStiBreakable Browsable(false)
        /// <summary>
        /// Gets or sets value which indicates whether the component can or cannot break its contents on several pages.
        /// </summary>		
        [StiNonSerialized]
        [Browsable(false)]
        public override bool CanBreak
        {
            get
            {
                return base.CanBreak;
            }
            set
            {
                base.CanBreak = value;
            }
        }
        #endregion

        #region Bookmark Browsable(false)
        [DefaultValue(false)]
        [StiNonSerialized]
        [Browsable(false)]
        public override StiBookmarkExpression Bookmark
        {
            get
            {
                return base.Bookmark;
            }
            set
            {
            }
        }
        #endregion

        #region Hyperlink Browsable(false)
        [DefaultValue(false)]
        [StiNonSerialized]
        [Browsable(false)]
        public override StiHyperlinkExpression Hyperlink
        {
            get
            {
                return base.Hyperlink;
            }
            set
            {
            }
        }
        #endregion

        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiOverlayBand();
        }
        #endregion

        /// <summary>
        /// Creates a new component of the type StiOverlayBand.
        /// </summary>
        public StiOverlayBand() : this(RectangleD.Empty)
        {
        }

        /// <summary>
        /// Creates a new component of the type StiOverlayBand with specified location.
        /// </summary>
        /// <param name="rect">The rectangle describes sizes and position of the component.</param>
        public StiOverlayBand(RectangleD rect) : base(rect)
        {
            PlaceOnToolbox = false;
        }
    }
}