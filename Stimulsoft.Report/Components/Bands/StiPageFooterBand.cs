#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Dialogs;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Report.QuickButtons;
using Stimulsoft.Report.Engine;
using Stimulsoft.Base.Json.Linq;
using System;

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// Describes the class that realizes the band - Page Footer Band.
	/// </summary>
	[StiServiceBitmap(typeof(StiPageFooterBand), "Stimulsoft.Report.Images.Components.StiPageFooterBand.png")]
	[StiToolbox(true)]
	[StiV1Builder(typeof(StiPageFooterBandV1Builder))]
	[StiContextTool(typeof(IStiCanGrow))]
	[StiContextTool(typeof(IStiCanShrink))]
	[StiQuickButton("Stimulsoft.Report.QuickButtons.Design.StiSelectAllQuickButton, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfQuickButton("Stimulsoft.Report.WpfDesign.StiWpfSelectAllQuickButton, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
	public class StiPageFooterBand : 
		StiStaticBand, 
		IStiPrintOnEvenOddPages
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiPageFooterBand
            jObject.AddPropertyEnum("PrintOnEvenOddPages", PrintOnEvenOddPages, StiPrintOnEvenOddPagesType.Ignore);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "PrintOnEvenOddPages":
                        this.PrintOnEvenOddPages = (StiPrintOnEvenOddPagesType)Enum.Parse(typeof(StiPrintOnEvenOddPagesType), property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiPageFooterBand;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();
            StiPropertyObject[] list;

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.Height() 
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.Height(), 
                    propHelper.MinHeight(), 
                    propHelper.MaxHeight()
                };
            }
            objHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.Brush(), 
                    propHelper.Border(), 
                    propHelper.Conditions(), 
                    propHelper.ComponentStyle() 
                };
            }
            else
            {
                list = new[]
                { 
                    propHelper.Brush(), 
                    propHelper.Border(), 
                    propHelper.Conditions(), 
                    propHelper.ComponentStyle(), 
                    propHelper.UseParentStyles() 
                };
            }
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.CanGrow(), 
                    propHelper.CanShrink(), 
                    propHelper.Enabled() 
                };
            }
            else
            {
                list = new[]
                { 
                    propHelper.InteractionEditor(), 
                    propHelper.CanGrow(), 
                    propHelper.CanShrink(), 
                    propHelper.Enabled(), 
                    propHelper.PrintOn(), 
                    propHelper.PrintOnEvenOddPages(), 
                    propHelper.ResetPageNumber() 
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                { 
                    propHelper.Name() 
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                { 
                    propHelper.Name(), 
                    propHelper.Alias() 
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.Name(), 
                    propHelper.Alias(), 
                    propHelper.Restrictions(), 
                    propHelper.Locked(), 
                    propHelper.Linked() 
                };
            }
            objHelper.Add(StiPropertyCategories.Design, list);

            return objHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // ValueEventsCategory
            var list = new[] { StiPropertyEventId.GetToolTipEvent, StiPropertyEventId.GetTagEvent };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            // NavigationEventsCategory
            list = new[] { StiPropertyEventId.GetHyperlinkEvent, StiPropertyEventId.GetBookmarkEvent };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[] { StiPropertyEventId.BeforePrintEvent, StiPropertyEventId.AfterPrintEvent };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // MouseEventsCategory
            list = new[] { StiPropertyEventId.GetDrillDownReportEvent, StiPropertyEventId.ClickEvent, StiPropertyEventId.DoubleClickEvent, 
                StiPropertyEventId.MouseEnterEvent, StiPropertyEventId.MouseLeaveEvent };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties
        public override string HelpUrl => "user-manual/report_internals_page_bands_pagefooter_band.htm";
        #endregion

		#region IStiPrintOnFirstPage Obsolete
		[DefaultValue(true)]
		[StiNonSerialized]
		[Browsable(false)]
		public bool PrintOnFirstPage
		{
			get
			{
				return (PrintOn & StiPrintOnType.ExceptFirstPage) == 0;
			}
			set
			{
				if (value)
				{
					if ((PrintOn & StiPrintOnType.ExceptFirstPage) > 0)
						PrintOn -= StiPrintOnType.ExceptFirstPage;
				}
				else
				{
					PrintOn |= StiPrintOnType.ExceptFirstPage;
				}
			}
		}
		#endregion

		#region IStiPrintOnEvenOddPages
        /// <summary>
		/// Gets or sets value indicates that the component is printed on even-odd pages.
		/// </summary>
		[DefaultValue(StiPrintOnEvenOddPagesType.Ignore)]
		[StiSerializable]
		[StiCategory("Behavior")]
		[StiOrder(StiPropertyOrder.BehaviorPrintOnEvenOddPages)]
		[TypeConverter(typeof(StiEnumConverter))]
		[Description("Gets or sets value indicates that the component is printed on even-odd pages.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual StiPrintOnEvenOddPagesType PrintOnEvenOddPages { get; set; } = StiPrintOnEvenOddPagesType.Ignore;
        #endregion

		#region StiBand override
		/// <summary>
		/// Gets header start color.
		/// </summary>
		[Browsable(false)]
		public override Color HeaderStartColor => Color.FromArgb(206, 207, 206);

        /// <summary>
		/// Gets header end color.
		/// </summary>
		[Browsable(false)]
		public override Color HeaderEndColor => Color.FromArgb(206, 207, 206);
        #endregion

		#region StiComponent override
		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition => (int)StiComponentToolboxPosition.PageFooterBand;

        public override StiToolboxCategory ToolboxCategory => StiToolboxCategory.Bands;

        /// <summary>
		/// Gets a component priority.
		/// </summary>
		public override int Priority
		{
			get
			{
				if (StiOptions.Engine.DockPageFooterToBottom)
					return (int)StiComponentPriority.PageFooterBandBottom;

                else 
				    return (int)StiComponentPriority.PageFooterBandTop;
			}
		}

		/// <summary>
		/// May this container be located in the specified component.
		/// </summary>
		/// <param name="component">Component for checking.</param>
		/// <returns>true, if this container may is located in the specified component.</returns>
		public override bool CanContainIn(StiComponent component)
		{
			if (component is IStiReportControl)return false;
			if (component is StiPage)return true;
			return false;
		}

		/// <summary>
		/// Gets the type of processing when printing.
		/// </summary>
		public override StiComponentType ComponentType
		{
			get
			{
				if (this.Report != null && this.Report.EngineVersion == StiEngineVersion.EngineV2)
					return base.ComponentType;

				return StiComponentType.Simple;
			}
		}
		
		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		[Browsable(false)]
		public override string LocalizedName => StiLocalization.Get("Components", "StiPageFooterBand");
        #endregion		

		#region Position override
		/// <summary>
		/// Gets or sets a rectangle of the component which it fills. Docking occurs in accordance to the area
		/// (Cross - components are docked by ClientRectangle).
		/// </summary>
		public override RectangleD DisplayRectangle
		{
			get
			{
			    if (!StiOptions.Engine.DockPageFooterToBottom)
			        return base.DisplayRectangle;

			    var headerSize = Page.Unit.ConvertFromHInches(this.HeaderSize);
			    var footerSize = Page.Unit.ConvertFromHInches(this.FooterSize);

			    return new RectangleD(Left, Top - headerSize - footerSize, Width, 
			        Height + headerSize + footerSize);
			}
			set
			{
				if (StiOptions.Engine.DockPageFooterToBottom)
				{
					var headerSize = Page.Unit.ConvertFromHInches(this.HeaderSize);
					var footerSize = Page.Unit.ConvertFromHInches(this.FooterSize);

					Left = value.Left;
					Top = value.Top + headerSize + footerSize;
					Width = value.Width;
					Height = value.Height - headerSize - footerSize;					
					
					return;
				}
				base.DisplayRectangle = value;
			}
		}
		#endregion

        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiPageFooterBand();
        }
        #endregion

		#region Methods
		/// <summary>
		/// Returns the component Master of the object.
		/// </summary>
		/// <returns>Master component.</returns>
		public StiComponent GetMaster()
		{
			foreach (StiComponent component in Parent.Components)
			{
				if (component is StiDataBand && (!(component is StiEmptyBand)))
				    return component;
			}
			
			return null;
		}
        #endregion

        /// <summary>
        /// Creates a new component of the type StiPageFooterBand.
        /// </summary>
        public StiPageFooterBand() : this(RectangleD.Empty)
		{
		}

		/// <summary>
		/// Creates a new component of the type StiPageFooterBand with specified location.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiPageFooterBand(RectangleD rect): base(rect)
		{
			if (StiOptions.Engine.DockPageFooterToBottom)
			    DockStyle = StiDockStyle.Bottom;

			PlaceOnToolbox = true;
		}
	}
}