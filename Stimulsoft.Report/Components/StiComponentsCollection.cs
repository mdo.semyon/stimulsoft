#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft


using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base;
using Stimulsoft.Base.Services;
using System.Globalization;

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// Describes a collection component.
	/// </summary>
	public class StiComponentsCollection : 
		CollectionBase, 
		IStiStateSaveRestore,
		ICloneable,
        IStiJsonReportObject
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            if (List.Count == 0)
                return null;

            var jObject = new JObject();

            int index = 0;
            foreach (StiComponent component in List)
            {
                jObject.AddPropertyJObject(index.ToString(), component.SaveToJsonObject(mode));
                index++;
            }

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                var propJObject = (JObject)property.Value;
                var ident = propJObject.Properties().FirstOrDefault(x => x.Name == "Ident").Value.ToObject<string>();

                var service = StiOptions.Services.Components.FirstOrDefault(x => x.GetType().Name == ident);
                
                if (service == null)
                    throw new Exception(string.Format("Type {0} is not found!", ident));

                var component = service.CreateNew();

                Add(component);
                component.LoadFromJsonObject((JObject)property.Value);
            }
        }
        #endregion

        #region ICloneable
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
		{
			var al = new StiComponentsCollection();

            lock (((ICollection)this).SyncRoot)
			foreach (StiComponent comp in this)al.Add(comp.Clone() as StiComponent);

			return al;
		}
		#endregion

		#region Collection
        public List<StiComponent> ToList()
        {
            return this.Cast<object>().Cast<StiComponent>().ToList();
        }

		private void AddCore(StiComponent component)
		{
			if (parent != null)
			{
				component.Parent = parent;
				if (parent.Page != null)
				{
					component.Page = parent.Page;

					if (string.IsNullOrEmpty(component.Name))
					{						
						if (component.Report != null && component.Report.IsDesigning)
						{
							component.Name = StiNameCreation.CreateName(parent.Report, 
								StiNameCreation.GenerateName(component));
						}
						else 
						{
							component.Name = StiNameCreation.CreateSimpleName(parent.Report, 
								StiNameCreation.GenerateName(component));
						}
					}
				}
			}
			
			List.Add(component);
		}


		public void Add(StiComponent component)
		{
			AddCore(component);
			InvokeComponentAdded(component, EventArgs.Empty);
		}


		public void AddRange(StiComponentsCollection components)
		{
            lock (((ICollection)components).SyncRoot)
			foreach (StiComponent comp in components)Add(comp);
		}


		public void AddRange(StiComponent[] components)
		{
            lock (((ICollection)components).SyncRoot)
			foreach (StiComponent comp in components)Add(comp);
		}


		public bool Contains(StiComponent component)
		{
			return List.Contains(component);
		}
		

		public int IndexOf(StiComponent component)
		{
			return List.IndexOf(component);
		}

        public int IndexOf(string name)
        {
            name = name.ToLower(CultureInfo.InvariantCulture);
            int index = 0;
            lock (((ICollection)List).SyncRoot)
            foreach (StiComponent component in List)
            {
                if (component.Name.ToLower(CultureInfo.InvariantCulture) == name) return index;
                index++;
            }
            return -1;
        }


		public void InsertRange(int index, StiComponentsCollection components)
		{
            lock (((ICollection)components).SyncRoot)
			foreach (StiComponent comp in components)Insert(index, comp);
		}


		public void Insert(int index, StiComponent component)
		{
			if (parent != null)
			{
				component.Parent = parent;
				if (parent.Page != null)
				{
					component.Page = parent.Page;
				}
			}
			List.Insert(index, component);
			InvokeComponentAdded(component, EventArgs.Empty);
		}


		public void Remove(StiComponentsCollection components)
		{
            lock (((ICollection)components).SyncRoot)
			foreach (StiComponent component in components)
				Remove(component);
		}

        public void Remove(StiComponent component)
        {
            Remove(component, true);
        }

		public void Remove(StiComponent component, bool clearParent)
		{
			if (clearParent && component.Page != null && component.Report != null)
			{
				component.Parent = null;
			}

			if (List.Contains(component))List.Remove(component);
			InvokeComponentRemoved(component, EventArgs.Empty);
		}
		
		
		public StiComponent this[int index]
		{
			get
			{
                return List[index] as StiComponent;
			}
			set
			{
				List[index] = value;
			}
		}


		public StiComponent this[string name]
		{
			get
			{
				name = name.ToLower(CultureInfo.InvariantCulture);
                lock (((ICollection)List).SyncRoot)
				foreach (StiComponent component in List)
					if (component.Name.ToLower(CultureInfo.InvariantCulture) == name)return component;
				return null;
			}
			set
			{
				name = name.ToLower(CultureInfo.InvariantCulture);
				for (int index = 0; index < List.Count; index++)				
				{
					StiComponent comp = List[index] as StiComponent;
					
					if (comp.Name.ToLower(CultureInfo.InvariantCulture) == name)
					{
						List[index] = value;
						return;
					}
				}
				AddCore(value);
			}
		}


		public void CopyTo(Array array, int index)
		{
			List.CopyTo(array, index);
		}
		

		#region Events
		#region ComponentAdded
		public event EventHandler ComponentAdded;

		protected virtual void OnComponentAdded(EventArgs e)
		{
		}

		public virtual void InvokeComponentAdded(object sender, EventArgs e)
		{
            this.OnComponentAdded(e);
			if (ComponentAdded != null)ComponentAdded(sender, e);
		}
		#endregion

		#region ComponentRemoved
		public event EventHandler ComponentRemoved;

		protected virtual void OnComponentRemoved(EventArgs e)
		{
		}

		public virtual void InvokeComponentRemoved(object sender, EventArgs e)
		{
            this.OnComponentRemoved(e);
			if (ComponentRemoved != null)ComponentRemoved(sender, e);
		}
		#endregion
		#endregion

		#endregion

		#region IStiStateSaveRestore
		/// <summary>
		/// Saves the current state of an object.
		/// </summary>
		/// <param name="stateName">A name of the state being saved.</param>
		public virtual void SaveState(string stateName)
		{
            lock (((ICollection)this).SyncRoot)
			foreach (StiComponent comp in this)comp.SaveState(stateName);
		}

		/// <summary>
		/// Restores the earlier saved object state.
		/// </summary>
		/// <param name="stateName">A name of the state being restored.</param>
		public virtual void RestoreState(string stateName)
		{
            lock (((ICollection)this).SyncRoot)
			foreach (StiComponent comp in this)comp.RestoreState(stateName);
		}


		/// <summary>
		/// Clear all earlier saved object states.
		/// </summary>
		public virtual void ClearAllStates()
		{
            lock (((ICollection)this).SyncRoot)
			foreach (StiComponent comp in this)comp.ClearAllStates();
		}
		#endregion

		#region Sort
		public void SortBySelectionTick()
		{
			if (this.Count > 1)
			{
				int pos = 1;
				while (pos < this.Count)
				{
					if (this[pos - 1].SelectionTick > this[pos].SelectionTick)
					{
						StiComponent swapComp = this[pos - 1];
						this[pos - 1] = this[pos];
						this[pos] = swapComp;
						if (pos != 1)pos --;
					}
					else pos++;
				}


			}
		}


		public void SortByPriority()
		{
			if (this.Count > 1)
			{
				int pos = 1;
				while (pos < this.Count)
				{
					if (this[pos - 1].Priority > this[pos].Priority)
					{
						StiComponent swapComp = this[pos - 1];
						this[pos - 1] = this[pos];
						this[pos] = swapComp;
						if (pos != 1)pos --;
					}
					else pos++;
				}
			}
		}

		public void SortByTopPosition()
		{
			if (this.Count > 1)
			{
                bool needSort = true;
                Hashtable hash = null;
                int thisCount = this.Count;

                if (thisCount > 100)
                {
                    needSort = false;
                    double lastValue = this[0].Top;
                    hash = new Hashtable();
                    for (int index = 0; index < thisCount; index++)
                    {
                        double currentValue = this[index].Top;
                        hash[currentValue] = null;
                        if (currentValue < lastValue) needSort = true;
                        lastValue = currentValue;
                    }
                    int hashKeysCount = hash.Keys.Count;
                    hash.Clear();

                    if (needSort && (hashKeysCount < thisCount / 2))
                    {
                        #region Optimized sorting
                        //
                        //***** sorting V1 *****
                        //
                        //double[] values = new double[this.Count];
                        //Hashtable hash = new Hashtable();
                        //StiComponent[] listComps = new StiComponent[this.Count];
                        //for (int index = 0; index < Count; index++)
                        //{
                        //    StiComponent comp = this[index];
                        //    double currentValue = comp.Top;
                        //    values[index] = currentValue;
                        //    hash[currentValue] = null;
                        //    listComps[index] = comp;
                        //}
                        //List<double> sortedHash = new List<double>();
                        //foreach (DictionaryEntry de in hash)
                        //{
                        //    sortedHash.Add((double)de.Key);
                        //}
                        //sortedHash.Sort();

                        //int counter = 0;
                        //int sortedHashCount = sortedHash.Count;
                        //int thisCount = this.Count;
                        //for (int index2 = 0; index2 < sortedHashCount; index2++)
                        //{
                        //    double currentValue = sortedHash[index2];
                        //    for (int index = 0; index < thisCount; index++)
                        //    {
                        //        if (values[index] == currentValue)
                        //        {
                        //            this[counter] = listComps[index];
                        //            counter++;
                        //        }
                        //    }
                        //}

                        //
                        //***** sorting V2 *****
                        //
                        for (int index = 0; index < Count; index++)
                        {
                            StiComponent comp = this[index];
                            double currentValue = comp.Top;
                            List<StiComponent> comps = (List<StiComponent>)hash[currentValue];
                            if (comps == null)
                            {
                                comps = new List<StiComponent>();
                                hash[currentValue] = comps;
                            }
                            comps.Add(comp);
                        }

                        object[] keys = new object[hash.Keys.Count];
                        hash.Keys.CopyTo(keys, 0);
                        Array.Sort(keys);

                        int counter = 0;
                        foreach (var key in keys)
                        {
                            List<StiComponent> comps = (List<StiComponent>)hash[key];
                            foreach (var comp in comps)
                            {
                                this[counter++] = comp;
                            }
                        }
                        hash.Clear();

                        needSort = false;
                        #endregion
                    }
                }

			    if (needSort)
                {
                    #region Standard sorting
                    int pos = 1;
			        while (pos < thisCount)
			        {
			            if (this[pos - 1].Top > this[pos].Top)
			            {
			                StiComponent swapComp = this[pos - 1];
			                this[pos - 1] = this[pos];
			                this[pos] = swapComp;
			                if (pos != 1) pos--;
			            }
			            else pos++;
                    }
                    #endregion
                }
			}
		}


		public void SortByBottomPosition()
		{
			if (this.Count > 1)
			{
				int pos = 1;
				while (pos < this.Count)
				{
					if (this[pos - 1].Bottom > this[pos].Bottom)
					{
						StiComponent swapComp = this[pos - 1];
						this[pos - 1] = this[pos];
						this[pos] = swapComp;
						if (pos != 1)pos --;
					}
					else pos++;
				}
			}
		}


		public void SortByLeftPosition()
		{
			if (this.Count > 1)
			{
				int pos = 1;
				while (pos < this.Count)
				{
					if (this[pos - 1].Left > this[pos].Left)
					{
						StiComponent swapComp = this[pos - 1];
						this[pos - 1] = this[pos];
						this[pos] = swapComp;
						if (pos != 1)pos --;
					}
					else pos++;
				}
			}
		}


		public void SortByRightPosition()
		{
			if (this.Count > 1)
			{
				int pos = 1;
				while (pos < this.Count)
				{
					if (this[pos - 1].Right > this[pos].Right)
					{
						StiComponent swapComp = this[pos - 1];
						this[pos - 1] = this[pos];
						this[pos] = swapComp;
						if (pos != 1)pos --;
					}
					else pos++;
				}
			}
		}


		public void SortBandsByTopPosition()
		{
			if (this.Count > 1)
			{
				int pos = 1;
				while (pos < this.Count)
				{
					
					if (
						this[pos - 1] is StiBand && 
						this[pos] is StiBand && 
						this[pos - 1].DockStyle == this[pos].DockStyle &&
						((this[pos - 1].DockStyle == StiDockStyle.Top && this[pos - 1].Top > this[pos].Top) ||
						(this[pos - 1].DockStyle == StiDockStyle.Bottom && this[pos - 1].Top < this[pos].Top)))
					{
						StiComponent swapComp = this[pos - 1];
						this[pos - 1] = this[pos];
						this[pos] = swapComp;
						if (pos != 1)pos --;
					}
					else pos++;
				}
			}
		}


		public void SortBandsByLeftPosition()
		{
			if (this.Count > 1)
			{
				int pos = 1;
				while (pos < this.Count)
				{
					if (this[pos - 1] is StiBand && this[pos] is StiBand && this[pos - 1].Left > this[pos].Left)
					{
						StiComponent swapComp = this[pos - 1];
						this[pos - 1] = this[pos];
						this[pos] = swapComp;
						if (pos != 1)pos --;
					}
					else pos++;
				}
			}
		}

		#endregion

		#region this
		internal StiComponent GetComponentByName(string componentName, StiContainer container)
		{
            lock (((ICollection)container.Components).SyncRoot)
			foreach (StiComponent component in container.Components)
			{
				if (component.Name == componentName)return component;

				StiContainer cont = component as StiContainer;
				if (cont != null)
				{
					StiComponent comp = GetComponentByName(componentName, cont);
					if (comp != null)return comp;
				}
			}
			return null;
		}
		

		public StiPage GetPageByAlias(string alias)
		{
            lock (((ICollection)this).SyncRoot)
			foreach (StiPage page in this)
			{
				if (page.Alias == alias)return page;
			}
			return null;
		}

		public void SetParent(StiContainer parent)
		{
			this.parent = parent;
            lock (((ICollection)this).SyncRoot)
			foreach (StiComponent comp in this)
			{
				comp.Parent = parent;
				StiContainer cont = comp as StiContainer;
				if (cont != null)
				{
					cont.Components.SetParent(cont);
				}
			}
		}

		private StiContainer parent;

		public StiComponentsCollection(StiContainer parent)
		{
			this.parent = parent;
		}


		public StiComponentsCollection()
		{
			this.parent = null;
		}
		#endregion
	}
}
