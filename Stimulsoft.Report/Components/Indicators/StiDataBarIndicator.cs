#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.ComponentModel;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Painters;
using Stimulsoft.Base.Json.Linq;
using System;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Components
{
    [StiGdiIndicatorTypePainter(typeof(Stimulsoft.Report.Painters.StiDataBarGdiIndicatorTypePainter))]
    [StiWpfIndicatorTypePainter("Stimulsoft.Report.Painters.StiDataBarWpfIndicatorTypePainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
    public class StiDataBarIndicator : 
        StiIndicator,
        IStiDataBarIndicator
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiDataBarIndicator
            jObject.AddPropertyEnum("BrushType", BrushType, StiBrushType.Gradient);
            jObject.AddPropertyStringNullOrEmpty("PositiveColor", StiJsonReportObjectHelper.Serialize.JColor(PositiveColor, Color.Green));
            jObject.AddPropertyStringNullOrEmpty("NegativeColor", StiJsonReportObjectHelper.Serialize.JColor(NegativeColor, Color.Red));
            jObject.AddPropertyStringNullOrEmpty("PositiveBorderColor", StiJsonReportObjectHelper.Serialize.JColor(PositiveBorderColor, Color.DarkGreen));
            jObject.AddPropertyStringNullOrEmpty("NegativeBorderColor", StiJsonReportObjectHelper.Serialize.JColor(NegativeBorderColor, Color.DarkRed));
            jObject.AddPropertyBool("ShowBorder", ShowBorder);
            jObject.AddPropertyFloat("Value", Value, 0f);
            jObject.AddPropertyFloat("Minimum", Minimum, 0f);
            jObject.AddPropertyFloat("Maximum", Maximum, 100f);
            jObject.AddPropertyEnum("Direction", Direction, StiDataBarDirection.Default);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "BrushType":
                        this.BrushType = (StiBrushType)Enum.Parse(typeof(StiBrushType), property.Value.ToObject<string>());
                        break;

                    case "PositiveColor":
                        this.PositiveColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "NegativeColor":
                        this.NegativeColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "PositiveBorderColor":
                        this.PositiveBorderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "NegativeBorderColor":
                        this.NegativeBorderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "ShowBorder":
                        this.ShowBorder = property.Value.ToObject<bool>();
                        break;

                    case "Value":
                        this.Value = property.Value.ToObject<float>();
                        break;

                    case "Minimum":
                        this.Minimum = property.Value.ToObject<float>();
                        break;

                    case "Maximum":
                        this.Maximum = property.Value.ToObject<float>();
                        break;

                    case "Direction":
                        this.Direction = (StiDataBarDirection)Enum.Parse(typeof(StiDataBarDirection), property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region IStiDataBarIndicator
        /// <summary>
        /// Gets or sets value which indicates which type of brush will be used for Data Bar indicator drawing.
        /// </summary>    
        [StiSerializable]
        [DefaultValue(StiBrushType.Gradient)]
        public StiBrushType BrushType { get; set; } = StiBrushType.Gradient;

        /// <summary>
        /// Gets or sets a color of positive values for data bar indicator.
        /// </summary>
        [StiSerializable]
        public Color PositiveColor { get; set; } = Color.Green;

        /// <summary>
        /// Gets or sets a color of negative values for data bar indicator.
        /// </summary>
        [StiSerializable]
        public Color NegativeColor { get; set; } = Color.Red;

        /// <summary>
        /// Gets or sets a border color of positive values for Data Bar indicator.
        /// </summary>
        [StiSerializable]
        public Color PositiveBorderColor { get; set; } = Color.DarkGreen;

        /// <summary>
        /// Gets or sets a border color of negative values for Data Bar indicator.
        /// </summary>
        [StiSerializable]
        public Color NegativeBorderColor { get; set; } = Color.DarkRed;

        /// <summary>
        /// Gets or sets value which indicates that border is drawing.
        /// </summary>
        [StiSerializable]
        [DefaultValue(false)]
        public bool ShowBorder { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets value from maximum or minimum amount.
        /// </summary>
        [StiSerializable]
        [DefaultValue(0f)]
        public float Value { get; set; }

        /// <summary>
        /// Gets or sets minimum amount.
        /// </summary>
        [StiSerializable]
        [DefaultValue(0f)]
        public float Minimum { get; set; }

        /// <summary>
        /// Gets or sets minimum amount.
        /// </summary>
        [StiSerializable]
        [DefaultValue(100f)]
        public float Maximum { get; set; } = 100f;

        /// <summary>
        /// Gets or sets value which direction data bar will be filled by brush, from left to right or from right to left.
        /// </summary>        
        [StiSerializable]
        [DefaultValue(StiDataBarDirection.Default)]
        public StiDataBarDirection Direction { get; set; } = StiDataBarDirection.Default;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new object of the type StiDataBarIndicator.
		/// </summary>
        public StiDataBarIndicator()
        {
        }

        /// <summary>
        /// Creates a new object of the type StiDataBarIndicator.
		/// </summary>
        public StiDataBarIndicator(StiBrushType brushType, Color positiveColor, Color negativeColor,
            bool showBorder, Color positiveBorderColor, Color negativeBorderColor, 
            StiDataBarDirection direction,
            float value, float minimum, float maximum)
		{
            this.BrushType = brushType;
            this.PositiveColor = positiveColor;
            this.NegativeColor = negativeColor;
            this.ShowBorder = showBorder;
            this.PositiveBorderColor = positiveBorderColor;
            this.NegativeBorderColor = negativeBorderColor;
            this.Direction = direction;
            this.Value = value;
            this.Minimum = minimum;
            this.Maximum = maximum;
        }
        #endregion
    }
}