#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.Engine;
using System.ComponentModel;
using System.Data;

namespace Stimulsoft.Report.Components
{
    public class StiContainerInfoV2 : StiComponentInfo
	{
        /// <summary>
        /// ������� � DataBand �� ������ ��������� ����� ����������.
        /// </summary>
        internal int DataBandPosition { get; set; } = -1;

        /// <summary>
        /// ������ ��������� ������ �� ������ ��������� ����� ����������.
        /// </summary>
        internal DataRow DataSourceRow { get; set; }

        /// <summary>
        /// ������� �������� ������-������� �� ������ ��������� ����� ����������.
        /// </summary>
        internal object BusinessObjectCurrent { get; set; }

        /// <summary>
        /// ���� true, �� ���� ��������� ������������ ������������� (PrintOnAllPages).
        /// </summary>
        internal bool IsAutoRendered { get; set; }

        /// <summary>
        /// ���� �������� ����������� � true, �� �������� ResetPageNumber ��� ���������� ����� ���������������.
        /// </summary>
        internal bool IgnoreResetPageNumber { get; set; }

        /// <summary>
        /// ���� true, �� ���� ��������� ��������� � ��������� �� DataBand.
        /// </summary>
        internal bool IsColumns { get; set; }

        /// <summary>
        /// ������ ������� ������������� ������ ��� �����������, ������� ����� �� �������� ��������. �� ������������ ��� ����������� 
        /// ���������� � ������� ������ ���������� �������� ������. ������ �������� �������� ��������� ������ � �������� ����� �� �������� �������� 
        /// ����� ���������� � ������� ��������� DataBand.
        /// </summary>
        internal int RenderStep { get; set; } = -1;

	    [Browsable(false)]
		public int SetSegmentPerWidth { get; set; } = -1;

	    /// <summary>
	    /// Band, ������� ������������ ���� ���������.
	    /// </summary>
        internal StiBand ParentBand { get; set; }
	}
}
