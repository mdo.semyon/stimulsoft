#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Imaging;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Units;

namespace Stimulsoft.Report.Components
{
    /// <summary>
    /// Summary description for StiWysiwygTextRender.
    /// </summary>
    public static class StiWysiwygTextRender
    {
        public static void DrawString(Graphics g, RectangleD rect, string text, StiText textBox)
        {
            if (!StiOptions.Engine.UseOldWYSIWYGTextQuality)
            {
                StiTextRenderer.StiDpiHelperGraphicsScale = StiDpiHelper.NeedFontScaling ? StiDpiHelper.GraphicsScale : 1;

                StiTextRenderer.DrawText(g, text, textBox.Font, rect, StiBrush.ToColor(textBox.TextBrush),
                    Color.Transparent, textBox.LineSpacing, textBox.HorAlignment, textBox.VertAlignment, textBox.WordWrap,
                    textBox.TextOptions.RightToLeft, textBox.Page.Zoom, textBox.Angle, textBox.TextOptions.Trimming,
                    textBox.TextOptions.LineLimit, textBox.CheckAllowHtmlTags(), textBox.TextOptions);
            }
            else
            {
                #region Old mode
                if (textBox.Page.Zoom < 0.1f)
                    StiStandardTextRenderer.DrawString(g, rect, text, textBox);

                else
                {
                    try
                    {
                        #region Metafile
                        Metafile metafile;

                        using (var bmp = new Bitmap(1, 1))
                        using (var grfx = Graphics.FromImage(bmp))
                        {
                            var ipHdc = grfx.GetHdc();
                            metafile = new Metafile(ipHdc, EmfType.EmfOnly);
                            grfx.ReleaseHdc(ipHdc);
                        }
                        #endregion

                        var zoom = (float) (textBox.Page.Zoom * StiDpiHelper.GraphicsScale);

                        #region Create metafile graphics
                        using (var gMetafile = Graphics.FromImage(metafile))
                        {
                            gMetafile.PageUnit = GraphicsUnit.Pixel;
                            var rect2 = new RectangleD(0, 0, rect.Width / zoom, rect.Height / zoom);
                            StiStandardTextRenderer.DrawString(gMetafile, rect2, text, textBox, 1);
                        }
                        #endregion

                        #region Convert StiTextHorAlignment to StiHorAlignment
                        var align = StiHorAlignment.Left;

                        if (textBox.HorAlignment == StiTextHorAlignment.Center)
                            align = StiHorAlignment.Center;

                        if (textBox.HorAlignment == StiTextHorAlignment.Right)
                            align = StiHorAlignment.Right;
                        #endregion

                        #region Prepare rectangle
                        var unit = GraphicsUnit.Pixel;
                        var size = metafile.GetBounds(ref unit).Size;
                        size.Width += 2;
                        size.Height += 2;

                        var sizeMetafile = new SizeD(size.Width * zoom, size.Height * zoom);
                        var rectMetafile = StiRectangleUtils.AlignSizeInRect(rect, sizeMetafile, align, textBox.VertAlignment);
                        #endregion

                        if (rectMetafile.Width > 1 && rectMetafile.Height > 1)
                        {
                            var rc = rectMetafile.ToRectangleF();
                            rc.Height--;
                            rc.Width--;
                            var clipRect = g.ClipBounds;
                            g.SetClip(rect.ToRectangleF());
                            g.DrawImage(metafile, rc);
                            g.SetClip(clipRect);
                        }

                        metafile.Dispose();
                    }
                    catch
                    {
                    }
                }
                #endregion
            }
        }

        public static SizeD MeasureString(double width, Font font, StiText textBox)
        {
            if (!StiOptions.Engine.UseOldWYSIWYGTextQuality)
            {
                if (textBox.AutoWidth && !textBox.WordWrap)
                    width = 10000000;

                var rect = new RectangleD(0, 0, width, 10000000);

                var size = StiTextRenderer.MeasureText(textBox.GetMeasureGraphics(), textBox.Text, font, rect, textBox.LineSpacing, textBox.WordWrap,
                    textBox.TextOptions.RightToLeft, StiDpiHelper.NeedFontScaling ? StiDpiHelper.GraphicsScale : 1, textBox.Angle, textBox.TextOptions.Trimming,
                    textBox.TextOptions.LineLimit, textBox.CheckAllowHtmlTags(), textBox.TextOptions);

                //correction of the width to prevent rounding error
                if (textBox.Report != null && textBox.Report.ReportUnit != StiReportUnitType.HundredthsOfInch)
                {
                    var unit = textBox.Report.Unit;
                    var fullWidth = size.Width + textBox.Margins.Left + textBox.Margins.Right + textBox.Border.Size;

                    if (unit.ConvertToHInches(Math.Round(unit.ConvertFromHInches(fullWidth), 2)) < fullWidth)
                        size.Width += unit.ConvertToHInches(0.01);
                }

                if (textBox.Angle == 90 || textBox.Angle == 180)
                    size.Width *= StiOptions.Engine.TextDrawingMeasurement.MeasurementFactorWysiwyg;

                else
                    size.Height *= StiOptions.Engine.TextDrawingMeasurement.MeasurementFactorWysiwyg;

                return new SizeD(size.Width, size.Height);
            }
            else
            {
                #region Old mode
                lock (textBox.GetMeasureGraphics())
                {
                    var size = StiTextDrawing.MeasureString(textBox.GetMeasureGraphics(), textBox.Text,
                        font, width, textBox.TextOptions,
                        textBox.HorAlignment, textBox.VertAlignment, false);

                    size.Height *= StiOptions.Engine.TextDrawingMeasurement.MeasurementFactorStandard;
                    return size;
                }
                #endregion
            }
        }
    }
}
