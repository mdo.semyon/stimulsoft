#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using Stimulsoft.Base.Json;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Services;

namespace Stimulsoft.Report.Components.TextFormats
{
	/// <summary>
	/// Describes a service for the text formatting done as customized.
	/// </summary>
	[TypeConverter(typeof(Stimulsoft.Report.Components.TextFormats.Design.StiCustomFormatConverter))]
	[StiFormatEditor("Stimulsoft.Report.Components.TextFormats.Design.StiCustomEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfFormatEditor("Stimulsoft.Report.WpfDesign.StiCustomEditor, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
    [StiServiceBitmap(typeof(StiCurrencyFormatService), "Stimulsoft.Report.Images.TextFormats.FormatCustom.png")]
	public class StiCustomFormatService : StiFormatService
    {
        #region StiFormatService override
        /// <summary>
		/// Gets a service name.
		/// </summary>
        [JsonIgnore]
        public override string ServiceName => Loc.Get("FormFormatEditor", "Custom");

        [JsonIgnore]
        public override int Position => 100;

        /// <summary>
        /// Gets value to show a sample of formatting.
        /// </summary>
        [JsonIgnore]
        public override object Sample => string.Empty;
       
        /// <summary>
        /// Gets or sets string of formatting.
        /// </summary>
        [DefaultValue("")]
		public override string StringFormat
		{
			get
			{
				return base.StringFormat;
			}
			set
			{
				base.StringFormat = value;
			}
        }
        #endregion

        #region Methods.override
        public override bool Equals(object obj)
        {
            var format = obj as StiCustomFormatService;

            return format != null && StringFormat == format.StringFormat;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region Methods.abstract
        public override StiFormatService CreateNew()
        {
            return new StiCustomFormatService();
        }
        #endregion

        /// <summary>
		/// Creates a new format of the type StiCustomFormatService.
		/// </summary>
		public StiCustomFormatService() : this(string.Empty)
		{
			
		}

		/// <summary>
		/// Creates a new format of the type StiCustomFormatService.
		/// </summary>
		/// <param name="stringFormat">String of formatting</param>
		public StiCustomFormatService(string stringFormat)
		{
			this.StringFormat = stringFormat;
		}
	}
}