#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections;
using Stimulsoft.Report.Components;

namespace Stimulsoft.Report.Components
{
    /// <summary>
    /// Describes a collection to store Bookmark.
    /// </summary>
    public class StiBookmarksCollection : CollectionBase
    {
        public void Add(StiBookmark bookmark)
        {
            List.Add(bookmark);
        }

        public void AddRange(StiBookmarksCollection bookmarks)
        {
            foreach (StiBookmark bookmark in bookmarks) Add(bookmark);
        }

        public void AddRange(StiBookmark[] bookmarks)
        {
            foreach (StiBookmark bookmark in bookmarks) Add(bookmark);
        }

        public bool Contains(StiBookmark bookmark)
        {
            return List.Contains(bookmark);
        }

        public int IndexOf(StiBookmark bookmark)
        {
            return List.IndexOf(bookmark);
        }

        public int IndexOf(string name)
        {
            int index = 0;
            foreach (StiBookmark bm in List)
            {
                if (bm.Text == name) return index;
                index++;
            }
            return -1;
        }

        public void Insert(int index, StiBookmark bookmark)
        {
            List.Insert(index, bookmark);
        }

        public void Remove(StiBookmark bookmark)
        {
            List.Remove(bookmark);
        }

        public StiBookmark this[string name]
        {
            get
            {
                StiBookmark bm = null;

                int index = IndexOf(name);
                if (index == -1)
                {
                    bm = new StiBookmark(name, this);
                    Add(bm);
                }
                else bm = List[index] as StiBookmark;

                return bm;
            }
        }

        public StiBookmark this[int index]
        {
            get
            {
                return (StiBookmark)List[index];
            }
            set
            {
                List[index] = value;
            }
        }
    }
}
