﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.PropertyGrid;
using System;
using Stimulsoft.Base.Json.Linq;

namespace Stimulsoft.Report.Components.ShapeTypes
{
    /// <summary>
    /// The class describes the shape type - Chevron.
    /// </summary>
    [StiServiceBitmap(typeof(StiShapeTypeService), "Stimulsoft.Report.Bmp.ShapeTypes.Chevron.png")]
    [TypeConverter(typeof(Stimulsoft.Report.Components.ShapeTypes.Design.StiChevronShapeTypeConverter))]
    [StiGdiShapeTypePainter(typeof(Stimulsoft.Report.Painters.StiChevronGdiShapeTypePainter))]
    [StiWpfShapeTypePainter("Stimulsoft.Report.Painters.StiChevronWpfShapeTypePainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
    public class StiChevronShapeType : StiShapeTypeService
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiChevronShapeType
            jObject.AddPropertyEnum("Direction", Direction, StiShapeDirection.Right);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Direction":
                        this.direction = (StiShapeDirection)Enum.Parse(typeof(StiShapeDirection) ,property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject

        [Browsable(false)]
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiChevronShapeType;
            }
        }


        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[]
            {
                propHelper.Direction()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            return objHelper;
        }

        #endregion

        #region Properties.override
        /// <summary>
		/// Gets a service name.
		/// </summary>
		public override string ServiceName
		{
			get
			{
                return StiLocalization.Get("Shapes", "Chevron");
			}
		}
        #endregion

        #region Properties
        private StiShapeDirection direction = StiShapeDirection.Right;
		/// <summary>
		/// Gets or sets the arrow direction.
		/// </summary>
		[StiCategory("Behavior")]
		[StiOrder(100)]
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		public virtual StiShapeDirection Direction
		{
			get
			{
				return direction;
			}
			set
			{
				direction = value;
			}
		}
        #endregion

        #region Methods.override
        public override StiShapeTypeService CreateNew()
        {
            return new StiChevronShapeType();
        }
        #endregion

        /// <summary>
        /// Creates a new Chevron.
		/// </summary>
        public StiChevronShapeType()
            : this(StiShapeDirection.Right)
		{
		}

		/// <summary>
        /// Creates a new Chevron with specified arguments.
		/// </summary>
		/// <param name="direction">Arrow direction.</param>
        public StiChevronShapeType(StiShapeDirection direction)
		{
			this.direction = direction;
		}
    }
}
