#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Reflection;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Events;

namespace Stimulsoft.Report.Components
{
    /// <summary>
    /// Class contains methods to work with filters.
    /// </summary>
	public sealed class StiFilterHelper
	{
		public static StiFilterCondition ConvertStringToCondition(string condition)
		{
		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionEqualTo"))
		        return StiFilterCondition.EqualTo;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionNotEqualTo"))
		        return StiFilterCondition.NotEqualTo;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionBetween"))
		        return StiFilterCondition.Between;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionNotBetween"))
		        return StiFilterCondition.NotBetween;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionGreaterThan"))
		        return StiFilterCondition.GreaterThan;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionGreaterThanOrEqualTo"))
		        return StiFilterCondition.GreaterThanOrEqualTo;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionLessThan"))
		        return StiFilterCondition.LessThan;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionLessThanOrEqualTo"))
		        return StiFilterCondition.LessThanOrEqualTo;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionContaining"))
		        return StiFilterCondition.Containing;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionNotContaining"))
		        return StiFilterCondition.NotContaining;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionBeginningWith"))
		        return StiFilterCondition.BeginningWith;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionEndingWith"))
		        return StiFilterCondition.EndingWith;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionIsNull"))
		        return StiFilterCondition.IsNull;

		    if (condition == Loc.Get("PropertyEnum", "StiFilterConditionIsNotNull"))
		        return StiFilterCondition.IsNotNull;

		    return StiFilterCondition.EqualTo;
		}
		
		public static string ConvertConditionToString(StiFilterCondition condition)
		{
			switch (condition)
			{
				case StiFilterCondition.EqualTo:
					return Loc.Get("PropertyEnum", "StiFilterConditionEqualTo");

				case StiFilterCondition.NotEqualTo:
					return Loc.Get("PropertyEnum", "StiFilterConditionNotEqualTo");

				case StiFilterCondition.Between:
					return Loc.Get("PropertyEnum", "StiFilterConditionBetween");

				case StiFilterCondition.NotBetween:
					return Loc.Get("PropertyEnum", "StiFilterConditionNotBetween");

				case StiFilterCondition.GreaterThan:
					return Loc.Get("PropertyEnum", "StiFilterConditionGreaterThan");

				case StiFilterCondition.GreaterThanOrEqualTo:
					return Loc.Get("PropertyEnum", "StiFilterConditionGreaterThanOrEqualTo");

				case StiFilterCondition.LessThan:
					return Loc.Get("PropertyEnum", "StiFilterConditionLessThan");

				case StiFilterCondition.LessThanOrEqualTo:
					return Loc.Get("PropertyEnum", "StiFilterConditionLessThanOrEqualTo");

				case StiFilterCondition.Containing:
					return Loc.Get("PropertyEnum", "StiFilterConditionContaining");

				case StiFilterCondition.NotContaining:
					return Loc.Get("PropertyEnum", "StiFilterConditionNotContaining");

				case StiFilterCondition.BeginningWith:
					return Loc.Get("PropertyEnum", "StiFilterConditionBeginningWith");

				case StiFilterCondition.EndingWith:
					return Loc.Get("PropertyEnum", "StiFilterConditionEndingWith");
            
                case StiFilterCondition.IsNull:
                    return Loc.Get("PropertyEnum", "StiFilterConditionIsNull");

                case StiFilterCondition.IsNotNull:
                    return Loc.Get("PropertyEnum", "StiFilterConditionIsNotNull");
            }
			return string.Empty;
		}
		
		public static StiFilterDataType ConvertStringToDataType(string dataType)
		{
		    if (dataType == Loc.Get("PropertyEnum", "StiFilterDataTypeString"))
		        return StiFilterDataType.String;

		    if (dataType == Loc.Get("PropertyEnum", "StiFilterDataTypeNumeric"))
		        return StiFilterDataType.Numeric;

		    if (dataType == Loc.Get("PropertyEnum", "StiFilterDataTypeDateTime"))
		        return StiFilterDataType.DateTime;

		    if (dataType == Loc.Get("PropertyEnum", "StiFilterDataTypeBoolean"))
		        return StiFilterDataType.Boolean;

		    if (dataType == Loc.Get("PropertyEnum", "StiFilterDataTypeExpression"))
		        return StiFilterDataType.Expression;

		    return StiFilterDataType.String;
		}

		public static string ConvertDataTypeToString(StiFilterDataType dataType)
		{
			switch (dataType)
			{
				case StiFilterDataType.String:
					return Loc.Get("PropertyEnum", "StiFilterDataTypeString");

				case StiFilterDataType.Numeric:
					return Loc.Get("PropertyEnum", "StiFilterDataTypeNumeric");

				case StiFilterDataType.DateTime:
					return Loc.Get("PropertyEnum", "StiFilterDataTypeDateTime");

				case StiFilterDataType.Boolean:
					return Loc.Get("PropertyEnum", "StiFilterDataTypeBoolean");

				case StiFilterDataType.Expression:
					return Loc.Get("PropertyEnum", "StiFilterDataTypeExpression");
			}
			return string.Empty;
		}

		public static void SetFilter(StiComponent comp)
		{
			var filter = comp as IStiFilter;			
            var dataSource = comp as IStiDataSource;
            var businessObject = comp as IStiBusinessObject;

		    if (dataSource.IsDataSourceEmpty && businessObject.IsBusinessObjectEmpty) return;
		    if (filter.FilterMethodHandler != null || !filter.FilterOn) return;

		    var correctedDataName = StiNameValidator.CorrectName(comp.Name, comp.Report);

		    var type = comp.Report.GetType();
		    var method = type.GetMethod($"{correctedDataName}__GetFilter", new[]
		    {
		        typeof(object),
		        typeof(StiFilterEventArgs)
		    });

		    if (method == null) return;

		    try
		    {
		        filter.FilterMethodHandler = Delegate.CreateDelegate(
		            typeof(StiFilterEventHandler), comp.Report, correctedDataName + "__GetFilter") 
		            as StiFilterEventHandler;
						
		    }
		    catch (Exception e)
		    {
		        StiLogService.Write(comp.GetType(), "StiFilterEventHandler...ERROR");
		        StiLogService.Write(comp.GetType(), e);
		    }
		}
	}
}
