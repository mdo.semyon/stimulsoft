#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Linq;
using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Engine;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Components
{
	[TypeConverter(typeof(Stimulsoft.Report.Components.Design.StiInteractionConverter))]
	[RefreshProperties(RefreshProperties.All)]
	public class StiInteraction :
		ICloneable,
		IStiSerializeToCodeAsClass,
        IStiInteractionClass,
		IStiReportProperty,
		IStiDefault,
        IStiJsonReportObject
    {
        #region IStiJsonReportObject.override
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyIdent("Ident", this.GetType().Name);

            // StiInteraction
            jObject.AddPropertyBool("SortingEnabled", SortingEnabled, true);
            jObject.AddPropertyStringNullOrEmpty("SortingColumn", SortingColumn);
            jObject.AddPropertyBool("DrillDownEnabled", DrillDownEnabled);
            jObject.AddPropertyStringNullOrEmpty("DrillDownReport", DrillDownReport);
            jObject.AddPropertyEnum("DrillDownMode", DrillDownMode, StiDrillDownMode.MultiPage);
            jObject.AddPropertyStringNullOrEmpty("DrillDownPageGuid", DrillDownPageGuid);
            jObject.AddPropertyJObject("DrillDownParameter5", DrillDownParameter5.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("DrillDownParameter1", DrillDownParameter1.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("DrillDownParameter2", DrillDownParameter2.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("DrillDownParameter3", DrillDownParameter3.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("DrillDownParameter4", DrillDownParameter4.SaveToJsonObject(mode));
            
            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "SortingEnabled":
                        this.SortingEnabled = property.Value.ToObject<bool>();
                        break;

                    case "SortingColumn":
                        this.SortingColumn = property.Value.ToObject<string>();
                        break;

                    case "DrillDownEnabled":
                        this.DrillDownEnabled = property.Value.ToObject<bool>();
                        break;

                    case "DrillDownReport":
                        this.DrillDownReport = property.Value.ToObject<string>();
                        break;

                    case "DrillDownMode":
                        this.DrillDownMode = (StiDrillDownMode)Enum.Parse(typeof(StiDrillDownMode), property.Value.ToObject<string>());
                        break;

                    case "DrillDownPageGuid":
                        this.DrillDownPageGuid = property.Value.ToObject<string>();
                        break;

                    case "DrillDownParameter1":
                        this.DrillDownParameter1.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "DrillDownParameter2":
                        this.DrillDownParameter2.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "DrillDownParameter3":
                        this.DrillDownParameter3.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "DrillDownParameter4":
                        this.DrillDownParameter4.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "DrillDownParameter5":
                        this.DrillDownParameter5.LoadFromJsonObject((JObject)property.Value);
                        break;
                }
            }
        }

        internal static StiInteraction LoadInteractionFromJsonObject(JObject jObject)
        {
            var ident = jObject.Properties().FirstOrDefault(x => x.Name == "Ident").Value.ToObject<string>();

            StiInteraction interaction = null;
            switch (ident)
            {
                case "StiInteraction":
                    interaction = new StiInteraction();
                    break;

                case "StiBandInteraction":
                    interaction = new StiBandInteraction();
                    break;

                case "StiCrossHeaderInteraction":
                    interaction = new StiCrossHeaderInteraction();
                    break;
            }

            interaction.LoadFromJsonObject(jObject);
            return interaction;
        }
        #endregion

        #region IStiReportProperty
        public object GetReport()
        {
            return ParentComponent == null ? null : ParentComponent.Report;
        }
		#endregion

		#region ICloneable override
		public object Clone()
		{
			return (StiInteraction)base.MemberwiseClone();
		}
		#endregion

		#region IStiDefault
		[Browsable(false)]
		public virtual bool IsDefault
		{
			get
			{
				return
                    SortingEnabled &&
					string.IsNullOrEmpty(SortingColumn) &&
					!DrillDownEnabled &&
					string.IsNullOrEmpty(DrillDownReport) &&
					DrillDownPageGuid == null &&
					DrillDownParameter1.IsDefault && 
					DrillDownParameter2.IsDefault &&
					DrillDownParameter3.IsDefault &&
                    DrillDownParameter4.IsDefault &&
                    DrillDownParameter5.IsDefault;
			}
		}
		#endregion

		#region Properties.Sorting
        /// <summary>
		/// Gets or sets value which indicates whether it is allowed or not, using given component, data re-sorting in the report viewer.
		/// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value which indicates whether it is allowed or not, using given component, data re-sorting in the report viewer.")]
		[StiOrder(StiPropertyOrder.InteractionSortingEnabled)]
		public virtual bool SortingEnabled { get; set; } = true;

        /// <summary>
		/// Gets or sets a column by what data should be re-sorted in the report viewer.
		/// </summary>
		[StiSerializable]
		[DefaultValue("")]
		public virtual string SortingColumn { get; set; } = string.Empty;

        [Browsable(false)]
		public virtual int SortingIndex { get; set; } = 0;

        [Browsable(false)]
		public virtual StiInteractionSortDirection SortingDirection { get; set; } = StiInteractionSortDirection.None;
        #endregion

		#region Properties.DrillDown
        /// <summary>
		/// Gets or sets value which indicates whether the Drill-Down operation can be executed.
		/// </summary>
		[StiSerializable]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value which indicates whether the Drill-Down operation can be executed.")]
		[StiOrder(StiPropertyOrder.InteractionDrillDownEnabled)]
		public virtual bool DrillDownEnabled { get; set; }

        /// <summary>
		/// Gets or sets a path to a report for the Drill-Down operation.
		/// </summary>
		[StiSerializable]
		[DefaultValue("")]
		[Description("Gets or sets a path to a report for the Drill-Down operation.")]
		[StiOrder(StiPropertyOrder.InteractionDrillDownReport)]
		public virtual string DrillDownReport { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets a value which indicates opening of sub-reports in the one tab.
        /// </summary>
        [StiSerializable]
        [DefaultValue(StiDrillDownMode.MultiPage)]
        [Description("Gets or sets a value which indicates opening of sub-reports in the one tab.")]
        [StiOrder(StiPropertyOrder.InteractionDrillDownReport)]
        [TypeConverter(typeof(StiEnumConverter))]
        public virtual StiDrillDownMode DrillDownMode { get; set; } = StiDrillDownMode.MultiPage;

        /// <summary>
		/// Gets or sets first Drill-Down parameter.
		/// </summary>
		[StiSerializable(StiSerializationVisibility.Class, 
			StiSerializeTypes.SerializeToDesigner |
			StiSerializeTypes.SerializeToDocument |
			StiSerializeTypes.SerializeToSaveLoad)]
		[Description("Gets or sets first Drill-Down parameter.")]
		[StiOrder(StiPropertyOrder.InteractionDrillDownParameter1)]
		public virtual StiDrillDownParameter DrillDownParameter1 { get; set; }

        /// <summary>
		/// Gets or sets second Drill-Down parameter.
		/// </summary>
		[StiSerializable(StiSerializationVisibility.Class)]
		[Description("Gets or sets second Drill-Down parameter.")]
		[StiOrder(StiPropertyOrder.InteractionDrillDownParameter2)]
		public virtual StiDrillDownParameter DrillDownParameter2 { get; set; }

        /// <summary>
		/// Gets or sets third Drill-Down parameter.
		/// </summary>
		[StiSerializable(StiSerializationVisibility.Class)]
		[Description("Gets or sets third Drill-Down parameter.")]
		[StiOrder(StiPropertyOrder.InteractionDrillDownParameter3)]
		public virtual StiDrillDownParameter DrillDownParameter3 { get; set; }

        /// <summary>
        /// Gets or sets fourth Drill-Down parameter.
        /// </summary>
        [StiSerializable(StiSerializationVisibility.Class)]
        [Description("Gets or sets fourth Drill-Down parameter.")]
        [StiOrder(StiPropertyOrder.InteractionDrillDownParameter4)]
        public virtual StiDrillDownParameter DrillDownParameter4 { get; set; }

        /// <summary>
        /// Gets or sets fifth Drill-Down parameter.
        /// </summary>
        [StiSerializable(StiSerializationVisibility.Class)]
        [Description("Gets or sets fifth Drill-Down parameter.")]
        [StiOrder(StiPropertyOrder.InteractionDrillDownParameter5)]
        public virtual StiDrillDownParameter DrillDownParameter5 { get; set; }

        /// <summary>
		/// Gets or sets a page for the Drill-Down operation.
		/// </summary>
		[StiNonSerialized]
		[Editor("Stimulsoft.Report.Components.Design.StiInteractionDrillDownPageEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[DefaultValue(null)]
		[Description("Gets or sets a page for the Drill-Down operation.")]
		[StiOrder(StiPropertyOrder.InteractionDrillDownPage)]
		public virtual StiPage DrillDownPage
		{
			get
			{
				if (ParentComponent == null || ParentComponent.Report == null) return null;
				foreach (StiPage page in ParentComponent.Report.Pages)
				{
					if (page.Guid == this.DrillDownPageGuid) return page;
				}
				return null;
			}
			set
			{
				if (value == null) this.DrillDownPageGuid = null;
				else
				{
					if (value.Guid == null) value.Guid = StiGuidUtils.NewGuid();
					this.DrillDownPageGuid = value.Guid;
				}
			}
		}

        [Browsable(false)]
		[StiSerializable]
		[DefaultValue(null)]
		public string DrillDownPageGuid { get; set; }
        #endregion

		#region Properties.Expression
		#region Bookmark
		/// <summary>
		/// Gets or sets the expression to fill a component bookmark.
		/// </summary>
		[StiOrder(StiPropertyOrder.InteractionBookmark)]
		[Description("Gets or sets the expression to fill a component bookmark.")]
		public virtual StiBookmarkExpression Bookmark
		{
			get
			{
				return ParentComponent.Bookmark;
			}
			set
			{
				ParentComponent.Bookmark = value;
			}
		}
		#endregion

		#region Hyperlink
		/// <summary>
		/// Gets or sets an expression to fill a component hyperlink.
		/// </summary>
		[StiOrder(StiPropertyOrder.InteractionHyperlink)]
		[Description("Gets or sets an expression to fill a component hyperlink.")]
		public virtual StiHyperlinkExpression Hyperlink
		{
			get
			{
				return ParentComponent.Hyperlink;
			}
			set
			{
				ParentComponent.Hyperlink = value;
			}
		}
		#endregion

		#region Tag
		/// <summary>
		/// Gets or sets the expression to fill a component tag.
		/// </summary>
		[Description("Gets or sets the expression to fill a component tag.")]
		[StiOrder(StiPropertyOrder.InteractionTag)]
		public virtual StiTagExpression Tag
		{
			get
			{
				return ParentComponent.Tag;
			}
			set
			{
				ParentComponent.Tag = value;
			}
		}
		#endregion

		#region ToolTip
		/// <summary>
		/// Gets or sets the expression to fill a component tooltip.
		/// </summary>
		[StiOrder(StiPropertyOrder.InteractionToolTip)]
		[Description("Gets or sets the expression to fill a component tooltip.")]
		public virtual StiToolTipExpression ToolTip
		{
			get
			{
				return ParentComponent.ToolTip;
			}
			set
			{
				ParentComponent.ToolTip = value;
			}
		}
		#endregion
		#endregion

		#region Methods
		public string GetSortDataBandName()
		{
			if (string.IsNullOrEmpty(SortingColumn) || !SortingEnabled)
			    return string.Empty;

            var indexOfDot = SortingColumn.IndexOf(".", StringComparison.InvariantCulture);
		    return indexOfDot != -1 ? SortingColumn.Substring(0, indexOfDot) : string.Empty;
		}

		public string[] GetSortColumns()
		{
			var str = GetSortColumnsString();

		    return str.Length == 0 ? null : str.Split('.');
		}

		public string GetSortColumnsString()
		{
			if (string.IsNullOrEmpty(SortingColumn) || !SortingEnabled) return string.Empty;

            var indexOfDot = this.SortingColumn.IndexOf(".", StringComparison.InvariantCulture);
			return indexOfDot != -1 ? this.SortingColumn.Substring(indexOfDot + 1) : string.Empty;
		}
		#endregion

		#region Fields
		public StiComponent ParentComponent = null;
		#endregion

		public StiInteraction()
		{
			this.DrillDownParameter1 = new StiDrillDownParameter();
			this.DrillDownParameter2 = new StiDrillDownParameter();
			this.DrillDownParameter3 = new StiDrillDownParameter();
            this.DrillDownParameter4 = new StiDrillDownParameter();
            this.DrillDownParameter5 = new StiDrillDownParameter();
		}
	}
}
