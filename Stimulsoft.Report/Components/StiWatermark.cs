#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Helpers;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// The class describes the watermark of the page.
	/// </summary>
	[TypeConverter(typeof(Stimulsoft.Report.Components.Design.StiWatermarkConverter))]
	[RefreshProperties(RefreshProperties.All)]
	public class StiWatermark : 
		ICloneable,
		IStiSerializeToCodeAsClass,
        IStiJsonReportObject
	{
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.Font(Font, "Arial", 100));
            jObject.AddPropertyStringNullOrEmpty("TextBrush", StiJsonReportObjectHelper.Serialize.JBrush(TextBrush));
            jObject.AddPropertyStringNullOrEmpty("Text", Text);
            jObject.AddPropertyStringNullOrEmpty("ImageHyperlink", ImageHyperlink);
            jObject.AddPropertyFloat("Angle", Angle, 45f);
            jObject.AddPropertyBool("Enabled", Enabled, true);
            jObject.AddPropertyBool("ShowImageBehind", ShowImageBehind, true);
            jObject.AddPropertyBool("ShowBehind", ShowBehind);
            jObject.AddPropertyBool("RightToLeft", RightToLeft);
            jObject.AddPropertyDouble("ImageMultipleFactor", ImageMultipleFactor, 1d);
            jObject.AddPropertyInt("ImageTransparency", ImageTransparency, 0);
            jObject.AddPropertyEnum("ImageAlignment", ImageAlignment, ContentAlignment.MiddleCenter);
            jObject.AddPropertyBool("ImageTiling", ImageTiling);
            jObject.AddPropertyBool("ImageStretch", ImageStretch);
            jObject.AddPropertyBool("AspectRatio", AspectRatio);

            if (ExistImage())
                jObject.Add("ImageBytes", global::System.Convert.ToBase64String(TakeImage()));

            if (jObject.Count == 0)
                return null;

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Font":
                        this.Font = StiJsonReportObjectHelper.Deserialize.Font(property, this.Font);
                        break;

                    case "TextBrush":
                        this.TextBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "Text":
                        this.Text = property.Value.ToObject<string>();
                        break;

                    case "Angle":
                        this.Angle = property.Value.ToObject<float>();
                        break;

                    case "Enabled":
                        this.Enabled = property.Value.ToObject<bool>();
                        break;

                    case "ShowImageBehind":
                        this.ShowImageBehind = property.Value.ToObject<bool>();
                        break;

                    case "ShowBehind":
                        this.ShowBehind = property.Value.ToObject<bool>();
                        break;

                    case "RightToLeft":
                        this.RightToLeft = property.Value.ToObject<bool>();
                        break;

                    case "ImageMultipleFactor":
                        this.ImageMultipleFactor = property.Value.ToObject<double>();
                        break;

                    case "ImageTransparency":
                        this.imageTransparency = property.Value.ToObject<int>();
                        break;

                    case "Image":
                        this.PutImage(StiImageConverter.StringToImage(property.Value.ToObject<string>()));
                        break;

                    case "ImageBytes":
                        this.PutImage(global::System.Convert.FromBase64String(property.Value.ToObject<string>()));
                        break;

                    case "ImageHyperlink":
                        this.ImageHyperlink = property.Value.ToObject<string>();
                        break;

                    case "ImageAlignment":
                        this.ImageAlignment = (ContentAlignment)Enum.Parse(typeof(ContentAlignment), property.Value.ToObject<string>());
                        break;

                    case "ImageTiling":
                        this.ImageTiling = property.Value.ToObject<bool>();
                        break;

                    case "ImageStretch":
                        this.ImageStretch = property.Value.ToObject<bool>();
                        break;

                    case "AspectRatio":
                        this.AspectRatio = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

		#region ICloneable override
		public object Clone()
		{
		    var watermark = (StiWatermark)base.MemberwiseClone();

			watermark.Font = this.Font.Clone() as Font;
			watermark.TextBrush = this.TextBrush.Clone() as StiBrush;

			return watermark;
		}
        #endregion

        #region Fields
	    private byte[] cachedImage;
        #endregion

        #region Properties
	    /// <summary>
		/// Gets or sets font of Watermark.
		/// </summary>
		[StiSerializable]
		[Description("Gets or sets font of watermark.")]
		[StiOrder(StiPropertyOrder.WatermarkFont)]
        [StiPropertyLevel(StiLevel.Basic)]
		public Font Font { get; set; } = new Font("Arial", 100);

	    [EditorBrowsable(EditorBrowsableState.Never)]
		internal bool ShouldSerializeFont()
		{
			return !(Font.Size == 100 & Font.Style == FontStyle.Bold & Font.Name == "Arial");
		}

	    /// <summary>
		/// Gets or sets a brush to Watermark.
		/// </summary>
		[StiSerializable]
		[Description("Gets or sets a brush to watermark.")]
		[StiOrder(StiPropertyOrder.WatermarkTextBrush)]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiBrush TextBrush { get; set; } = new StiSolidBrush(Color.FromArgb(50, 0, 0, 0));
        
	    [EditorBrowsable(EditorBrowsableState.Never)]
		internal bool ShouldSerializeTextBrush()
		{
			return !(TextBrush is StiSolidBrush && ((StiSolidBrush)TextBrush).Color == Color.FromArgb(50, 0, 0, 0));
		}

	    /// <summary>
		/// Gets or sets text of Watermark.
		/// </summary>
		[StiSerializable]
		[DefaultValue("")]
		[Description("Gets or sets text of Watermark.")]
		[StiOrder(StiPropertyOrder.WatermarkText)]
		[Editor("Stimulsoft.Report.Components.Design.StiSimpleTextEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual string Text { get; set; } = string.Empty;

	    /// <summary>
		/// Gets or sets angle of Watermark.
		/// </summary>
		[StiSerializable]
		[DefaultValue(45f)]
		[Description("Gets or sets angle of Watermark.")]
		[StiOrder(StiPropertyOrder.WatermarkAngle)]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual float Angle { get; set; } = 45f;

	    /// <summary>
		/// Gets or sets value which indicates where Watermark should be drawing or not.
		/// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value which indicates where Watermark should be drawing or not.")]
		[StiOrder(StiPropertyOrder.WatermarkEnabled)]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual bool Enabled { get; set; } = true;

	    /// <summary>
		/// Gets or sets value which indicates where Watermark's image should be drawing behind or in front of page.
		/// </summary>
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value which indicates where Watermark image should be drawing behind or in front of page.")]
		[StiOrder(StiPropertyOrder.WatermarkShowImageBehind)]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool ShowImageBehind { get; set; } = true;

	    /// <summary>
		/// Gets or sets value which indicates where Watermark should be drawing behind or in front of page.
		/// </summary>
		[StiSerializable]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value which indicates where Watermark should be drawing behind or in front of page.")]
		[StiOrder(StiPropertyOrder.WatermarkShowBehind)]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool ShowBehind { get; set; }

	    /// <summary>
        /// Gets or sets watermark's output direction.
        /// </summary>
        [DefaultValue(false)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets watermark's output direction.")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [StiPropertyLevel(StiLevel.Basic)]
        public bool RightToLeft { get; set; }

	    /// <summary>
		/// Gets or sets value to multiply by it an image size.
		/// </summary>
		[StiSerializable]
		[DefaultValue(1d)]
		[Description("Gets or sets value to multiply by it an image size.")]
		[StiOrder(StiPropertyOrder.WatermarkImageMultipleFactor)]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual double ImageMultipleFactor { get; set; } = 1d;

	    private int imageTransparency;
		/// <summary>
		/// Gets or sets the transparency of the watermark's image.
		/// </summary>
		[StiSerializable]
		[Description("Gets or sets the transparency of the watermark's image.")]
		[DefaultValue(0)]
		[StiOrder(StiPropertyOrder.WatermarkImageTransparency)]
        [StiPropertyLevel(StiLevel.Basic)]
		public int ImageTransparency
		{
			get
			{
				return imageTransparency;
			}
			set
			{
				
				value = Math.Max(0, Math.Min(value, 0xff));
				if (value != imageTransparency)
				{
					imageTransparency = value;
					DisposeCachedImage();
				}
			}
		}

		/// <summary>
		/// Gets or sets value watermark's image.
		/// </summary>
		[StiNonSerialized]
		[DefaultValue(null)]
		[Description("Gets or sets value watermark's image.")]
		[Editor("Stimulsoft.Report.Components.Design.StiSimpleImageEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[TypeConverter(typeof(Stimulsoft.Report.Components.Design.StiSimpeImageConverter))]
		[StiOrder(StiPropertyOrder.WatermarkImage)]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual Image Image
		{
		    get
		    {
		        return TakeGdiImage();
		    }
		    set
		    {
		        PutImage(value);
		    }
		}

	    private byte[] imageBytes;
        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        [Browsable(false)]
        [StiSerializable]
        [DefaultValue(null)]
        [Description("Gets or sets value watermark's image.")]
        [Editor("Stimulsoft.Report.Components.Design.StiSimpleImageEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [TypeConverter(typeof(Stimulsoft.Report.Components.Design.StiSimpeImageConverter))]
        [StiOrder(StiPropertyOrder.WatermarkImage)]
        [StiPropertyLevel(StiLevel.Basic)]
        public byte[] ImageBytes
	    {
	        get
	        {
	            return imageBytes;
	        }
	        set
	        {
	            if (imageBytes != value)
	            {
	                imageBytes = value;
	                DisposeCachedImage();
                    PutImage(value);
	            }
	        }
	    }

        private string imageHyperlink = "";
	    /// <summary>
	    /// Gets or sets value watermark's image hyperlink.
	    /// </summary>
	    [StiSerializable]
	    [DefaultValue("")]
	    [Description("Gets or sets value watermark's image hyperlink.")]
	    [Browsable(false)]
	    public string ImageHyperlink
	    {
	        get
	        {
	            return imageHyperlink; 
	        }
	        set
	        {
	            if (imageHyperlink != value)
	            {
	                imageHyperlink = value;
                    DisposeCachedImage();
	            }
	        }
	    }

	    /// <summary>
		/// Gets or sets the watermark's image alignment.
		/// </summary>
		[StiSerializable]
		[DefaultValue(ContentAlignment.MiddleCenter)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		[Description("Gets or sets the watermark's image alignment.")]
		[StiOrder(StiPropertyOrder.WatermarkImageAlignment)]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual ContentAlignment ImageAlignment { get; set; } = ContentAlignment.MiddleCenter;

	    /// <summary>
		/// Gets or sets the watermark's image should be tiled.
		/// </summary>
		[StiSerializable]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets the watermark's image should be tiled.")]
		[StiOrder(StiPropertyOrder.WatermarkImageTiling)]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool ImageTiling { get; set; }

	    /// <summary>
		/// Gets or sets value, indicates that this watermark's image will stretch on the page.
		/// </summary>
		[StiSerializable]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value, indicates that this watermark's image will stretch on the page.")]
		[StiOrder(StiPropertyOrder.WatermarkImageStretch)]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool ImageStretch { get; set; }

	    /// <summary>
        /// Gets or sets value, indicates that this watermark's image will save its aspect ratio.
        /// </summary>
        [Browsable(true)]
        [DefaultValue(false)]
        [StiSerializable]
        [StiCategory("ImageAdditional")]
        [StiOrder(StiPropertyOrder.ImageAspectRatio)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets value, indicates this watermark's image will save its aspect ratio.")]
        [StiPropertyLevel(StiLevel.Standard)]
        public bool AspectRatio { get; set; }

	    /// <summary>
        /// Gets or sets text of Watermark.
        /// </summary>
        [StiSerializable]
        [Browsable(false)]
        [StiBrowsable(false)]
        [DefaultValue("")]
        [Description("Gets or sets text of EnabledExpression.")]
        [StiOrder(StiPropertyOrder.WatermarkText)]
        [Editor("Stimulsoft.Report.Components.Design.StiSimpleTextEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiPropertyLevel(StiLevel.Basic)]
        public virtual string EnabledExpression { get; set; } = string.Empty;
	    #endregion

        #region Methods
	    /// <summary>
	    /// Internal use only.
	    /// </summary>
	    public byte[] GetImage(StiReport report)
	    {
	        var image = string.IsNullOrWhiteSpace(ImageHyperlink) 
                ? TakeImage() 
                : StiHyperlinkProcessor.GetBytes(report, ImageHyperlink);

	        if (ImageTransparency == 0) return image;
            if (image == null) return null;

            if (cachedImage == null)
	        {
	            var gdiImage = StiImageConverter.BytesToImage(image);
	            var gdiTransparentedImage = StiImageTransparenceHelper.GetTransparentedImage(gdiImage, 1f - this.ImageTransparency / 255f);
                cachedImage = StiImageConverter.ImageToBytes(gdiTransparentedImage);
	        }

	        return cachedImage;
	    }

        private void DisposeCachedImage()
		{
			this.cachedImage = null;
		}

        internal bool ExistImage()
	    {
	        return ImageBytes != null;
	    }

	    internal byte[] TakeImage()
	    {
	        return ImageBytes;
	    }

	    internal Image TakeGdiImage()
	    {
	        return ExistImage()
	            ? StiImageConverter.BytesToImage(ImageBytes)
	            : null;
	    }

	    internal Image TryTakeGdiImage()
	    {
	        return ExistImage()
	            ? StiImageConverter.TryBytesToImage(ImageBytes)
	            : null;
	    }

	    internal void PutImage(Image image)
	    {
	        ImageBytes = StiImageConverter.ImageToBytes(image, true);
	    }

	    internal void PutImage(byte[] image)
	    {
	        ImageBytes = image;
	    }

	    internal void ResetImage()
	    {
	        ImageBytes = null;
	    }

	    internal bool ImageIsMetafile()
	    {
	        return StiImageHelper.IsMetafile(ImageBytes);
	    }
        #endregion

        public StiWatermark()
		{
		    ImageHyperlink = string.Empty;
		}

		public StiWatermark(StiBrush textBrush, string text, float angle, Font font, bool showBehind) : 
			this(textBrush, text, angle, font, showBehind, true, false)
		{
		}

		public StiWatermark(StiBrush textBrush, string text, float angle, 
			Font font, bool showBehind, bool enabled, bool aspectRatio) : 
            this(textBrush, text, angle, font, showBehind, enabled, aspectRatio, false)
		{
		}

	    public StiWatermark(StiBrush textBrush, string text, float angle,
	        Font font, bool showBehind, bool enabled, bool aspectRatio, bool rightToLeft) : 
            this(textBrush, text, angle, font, showBehind, enabled, aspectRatio, rightToLeft, string.Empty)
	    {
	        
	    }

        public StiWatermark(StiBrush textBrush, string text, float angle,
            Font font, bool showBehind, bool enabled, bool aspectRatio, bool rightToLeft, string imageHyperlink)
        {
            this.ImageHyperlink = imageHyperlink;
            this.TextBrush = textBrush;
            this.Text = text;
            this.Angle = angle;
            this.Font = font;
            this.ShowBehind = showBehind;
            this.Enabled = enabled;
            this.AspectRatio = aspectRatio;
            this.RightToLeft = rightToLeft;
        }
	}
}
