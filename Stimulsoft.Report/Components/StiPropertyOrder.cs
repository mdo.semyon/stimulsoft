#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft


namespace Stimulsoft.Report.Components
{
	public class StiPropertyOrder
	{
		#region Appearance
		public const int AppearanceTextBrush =				100;
		public const int AppearanceBrush =					110;
		public const int AppearanceBorder =					120;
		public const int AppearanceConditions =				121;
		public const int AppearanceComponentStyle =			122;
		public const int AppearanceOddStyle =				150;
		public const int AppearanceEvenStyle =				160;
	    public const int AppearanceStyle = 165;
        public const int AppearanceUseParentStyles =		170;
        public const int AppearanceUseStyleOfSummaryInRowTotal = 180;
        public const int AppearanceUseStyleOfSummaryInColumnTotal = 190;
        #endregion

        #region Scale
        public const int ScaleMinimum           = 100;
        public const int ScaleMaximum           = 110;
        public const int ScaleMajorInterval     = 120;
        public const int ScaleMinorInterval     = 130;
        public const int ScaleStartWidth        = 140;
        public const int ScaleEndWidth          = 150;
        public const int ScaleIsReversed        = 160;
        public const int ScaleStartAngle        = 170;
        public const int ScaleSweepAngle        = 180;
        public const int ScaleRadius            = 190;
        public const int ScaleRadiusMode        = 200;
        public const int ScaleSkin              = 210;
        public const int ScaleOrientation       = 220;
        public const int ScaleRelativeHeight    = 230;
        #endregion

        #region BarCode
        public const int BarCodeCode =						100;
		public const int BarCodeBarCodeType =				110;				
		public const int BarCodeHorAlignment =				120;
		public const int BarCodeVertAlignment =				130;		
		#endregion

		#region BarCode Additional
		public const int BarCodeAngle =						100;
		public const int BarCodeAutoScale =					110;
        public const int BarCodeFont =                      115;
        public const int BarCodeForeColor =                 120;
		public const int BarCodeBackColor =					130;		
		public const int BarCodeShowLabelText =				140;
        public const int BarCodeShowQuietZones =            145;
        public const int BarCodeZoom =                      150;
        public const int BarCodeImage =                     160;
        public const int BarCodeImageMultipleFactor =       170;
        #endregion

		#region Behavior
        public const int BehaviorAnchor = 100;		
		public const int BehaviorAutoWidth =				105;
		public const int BehaviorCanGrow =					110;
		public const int BehaviorCanShrink =				120;
		public const int BehaviorGrowToHeight =				130;
		public const int BehaviorCanBreak =					140;
		public const int BehaviorMergeHeaders =				150;
		public const int BehaviorDockStyle =				160;


        public const int BehaviorTitle =                    168;
        public const int BehaviorColorEach =                169;
        public const int BehaviorEnabled =					170;
		public const int BehaviorInteractive =				180;

		public const int BehaviorKeepChildTogether =		190;
		public const int BehaviorKeepCrossTabTogether =		200;
		public const int BehaviorKeepDetailsTogether =		210;
		public const int BehaviorKeepGroupTogether =		220;
		public const int BehaviorKeepFooterTogether =		230;
		public const int BehaviorKeepHeaderTogether =		240;
		public const int BehaviorKeepSubReportTogether =	250;
		
		public const int BehaviorPrintable =				260;
		
		public const int BehaviorPrintAtBottom =			400;
		public const int BehaviorPrintIfEmpty =				410;
		public const int BehaviorPrintIfDetailEmpty =		420;
        public const int BehaviorPrintIfParentDisabled =    425;
        public const int BehaviorPrintOn =                  430;
		public const int BehaviorPrintOnAllPages =			440;
		public const int BehaviorPrintOnEvenOddPages =		450;
		public const int BehaviorPrintOnFirstPage =			460;
		public const int BehaviorPrintOnPreviousPage =		470;
		public const int BehaviorPrintHeadersFootersFromPreviousPage =		480;

		public const int BehaviorResetPageNumber =			500;
		public const int BehaviorShiftMode =				505;		
		public const int BehaviorStartNewPage =				510;
		public const int BehaviorStartNewPageIfLessThan =	520;
		
		public const int BehaviorSizeMode =					600;
		public const int BehaviorVertAlignment =			610;
        public const int BehaviorShowTotal =                620;
        #endregion

        #region Dialog
        public const int DialogCheckedBinding = 100;
        public const int DialogDataBindings = 110;
        public const int DialogItemsBinding = 120;
        public const int DialogKeysBinding = 130;
        public const int DialogMaxDateBinding = 140;
        public const int DialogMaximumBinding = 150;
        public const int DialogMinDateBinding = 160;
        public const int DialogMinimumBinding = 170;
        public const int DialogSelectedIndexBinding = 180;
        public const int DialogSelectedItemBinding = 190;
        public const int DialogSelectedKeyBinding = 200;
        public const int DialogSelectedValueBinding = 210;
        public const int DialogTextBinding = 220;
        public const int DialogValueBinding = 230;
      
        public const int DialogAcceptsReturn = 100;
        public const int DialogAcceptsTab = 110;
        public const int DialogAlignment = 120;
        public const int DialogAlternatingBackColor = 130;
        public const int DialogBackColor = 140;
        public const int DialogBackgroundColor = 150;
        public const int DialogBorderStyle = 160;
        public const int DialogCancel = 170;
        public const int DialogChecked = 180;
        public const int DialogCheckOnClick = 190;
        public const int DialogColumnHeadersVisible = 200;
        public const int DialogColumns = 210;
        public const int DialogComponentStyle = 220;
        public const int DialogCustomFormat = 230;
        public const int DialogDataSource = 235;
        public const int DialogDataTextField = 240;        
        public const int DialogDefault = 250;
        public const int DialogDialogResult = 260;
        public const int DialogDockStyle = 270;
        public const int DialogDropDownAlign = 280;
        public const int DialogDropDownStyle = 290;
        public const int DialogDropDownWidth = 300;
        public const int DialogEnabled = 310;
        public const int DialogFilter = 320;
        public const int DialogFont = 330;
        public const int DialogForeColor = 340;
        public const int DialogFormat = 350;
        public const int DialogGridLineColor = 360;
        public const int DialogGridLineStyle = 370;
        public const int DialogHeaderBackColor = 380;
        public const int DialogHeaderFont = 390;
        public const int DialogHeaderForeColor = 400;
        public const int DialogHeaderText = 410;
        public const int DialogImage = 420;
        public const int DialogImageAlign = 430;
        public const int DialogIncrement = 440;
        public const int DialogItemHeight = 450;
        public const int DialogItems = 460;
        public const int DialogKeys = 470;
        public const int DialogLocation = 480;
        public const int DialogMaxDate = 490;
        public const int DialogMaxDropDownItems = 500;
        public const int DialogMaximum = 510;
        public const int DialogMaxLength = 520;
        public const int DialogMinDate = 530;
        public const int DialogMinimum = 540;
        public const int DialogMultiline = 550;
        public const int DialogNullText = 560;
        public const int DialogPasswordChar = 570;
        public const int DialogPreferredColumnWidth = 580;
        public const int DialogPreferredRowHeight = 590;
        public const int DialogRightToLeft = 600;
        public const int DialogRowHeadersVisible = 610;
        public const int DialogRowHeaderWidth = 620;
        public const int DialogSelectionBackColor = 630;
        public const int DialogSelectionForeColor = 640;
        public const int DialogSelectionMode = 650;
        public const int DialogShowUpDown = 660;
        public const int DialogSize = 670;
        public const int DialogSizeMode = 680;
        public const int DialogSorted = 690;
        public const int DialogStartMode = 700;
        public const int DialogStartPosition = 710;
        public const int DialogTag = 720;
        public const int DialogText = 730;
        public const int DialogTextAlign = 740;
        public const int DialogToday = 750;
        public const int DialogToolTip = 760;
        public const int DialogTransparentColor = 770;
        public const int DialogValue = 780;
        public const int DialogVisible = 790;
        public const int DialogWidth = 800;
        public const int DialogWindowState = 810;
        public const int DialogWordWrap = 820;
        #endregion

        #region Hierarchical
        public const int HierarchicalKeyDataColumn = 100;
        public const int HierarchicalMasterKeyDataColumn = 110;
        public const int HierarchicalParentValue = 120;
        public const int HierarchicalIndent = 130;
        public const int HierarchicalHeaders = 140;
        public const int HierarchicalFooters = 150;
        #endregion

        #region Interaction
        public const int InteractionBookmark = 100;        
		public const int InteractionCollapsingEnabled = 110;
		public const int InteractionCollapsed = 120;
		public const int InteractionCollapseGroupFooter = 130;
		public const int InteractionDrillDownEnabled = 140;
		public const int InteractionDrillDownPage = 150;
		public const int InteractionDrillDownParameters = 153;
		public const int InteractionDrillDownReport = 155;
		public const int InteractionDrillDownParameter1 = 156;
		public const int InteractionDrillDownParameter2 = 157;
		public const int InteractionDrillDownParameter3 = 158;
        public const int InteractionDrillDownParameter4 = 159;
        public const int InteractionDrillDownParameter5 = 160;
		public const int InteractionHyperlink = 170;
        public const int InteractionSelectionEnabled = 175;
		public const int InteractionSortingEnabled = 180;
		public const int InteractionSortingColumn = 190;
		public const int InteractionTag = 200;
		public const int InteractionToolTip = 210;
        #endregion

        #region Page & Column Break
        public const int PageColumnBreakNewPageBefore =		100;
		public const int PageColumnBreakNewPageAfter =		110;
		public const int PageColumnBreakNewColumnBefore =	120;
		public const int PageColumnBreakNewColumnAfter =	130;
		public const int PageColumnBreakBreakIfLessThan =	140;
        public const int PageColumnBreakSkipFirst =         150;
        #endregion

        #region Dashboard
	    public const int DashboardWidth = 101;
	    public const int DashboardHeight = 102;
        public const int DashboardStyleType = 103;
        public const int DashboardStyle = 104;
        #endregion

        #region Gauge Element
        public const int GaugeElementValue = 100;
        public const int GaugeElementSeries = 101;
        public const int GaugeElementCalculationMode = 102;
        public const int GaugeElementMinimum = 103;
        public const int GaugeElementMaximum = 104;                
        public const int GaugeElementType = 105;
	    public const int GaugeElementSeriesTitle = 106;
        #endregion

        #region Map Element
        public const int MapElementLatitude = 100;
	    public const int MapElementLongitude = 101;
        public const int MapElementKeyMeter = 102;
        public const int MapElementNameMeter = 103;
        public const int MapElementValueMeter = 104;
        public const int MapElementGroupMeter = 105;
        public const int MapElementColorMeter = 106;
        #endregion

        #region Indicator Element
        public const int IndicatorElementValue = 100;
	    public const int IndicatorElementTarget = 101;
	    public const int IndicatorElementSeries = 102;
	    public const int IndicatorElementTextFormat = 103;
	    public const int IndicatorElementMode = 104;
	    public const int IndicatorElementSeriesTitle = 105;
        public const int IndicatorElementIcon = 106;
        public const int IndicatorElementIconSet = 107;
        #endregion

        #region Progress Element
        public const int ProgressElementValue = 100;
	    public const int ProgressElementTarget = 101;
	    public const int ProgressElementSeries = 102;
	    public const int ProgressElementTextFormat = 103;
	    public const int ProgressElementMode = 104;
        public const int ProgressElementSeriesTitle = 105;
	    #endregion

        #region Chart
        public const int ChartAllowApplyStyle = 90;
		public const int ChartChartType =					100;		
		public const int ChartArea =						110;
		public const int ChartLegend =						120;        
		public const int ChartSeries =						130;
		public const int ChartSeriesLabels =				140;		
        public const int ChartStyle =						150;
	    public const int ChartTable =                       170;
		#endregion

		#region Chart Additional
		public const int ChartConstantLines =				100;
		public const int ChartProcessAtEnd =				110;
        public const int ChartRotation =                    115;
		public const int ChartHorSpacing =					120;
		public const int ChartVertSpacing =					130;
		public const int ChartStrips =	        			140;
		public const int ChartTitle =						150;
        #endregion

        #region Chart Element
        public const int ChartElementValues = 100;
        public const int ChartElementArguments = 110;
        public const int ChartElementWeights = 120;
        public const int ChartElementSeries = 130;
        #endregion

        #region Check
        public const int CheckChecked =						100;
		public const int CheckCheckStyleForTrue =			110;
		public const int CheckCheckStyleForFalse =			111;
		public const int CheckValues =						112;
		public const int CheckSize =						120;
		public const int CheckContourColor =				130;
		public const int CheckEditable =					140;
		public const int CheckTextBrush =					150;		
		#endregion

		#region Columns
		public const int ColumnsColumns =					100;
		public const int ColumnsColumnWidth =				110;
		public const int ColumnsColumnGaps =				120;
		public const int ColumnsColumnDirection =			130;
		public const int ColumnsMinRowsInColumn =			140;
		public const int ColumnsRightToLeft =				150;
		#endregion

		#region CrossTab
        public const int CrossTabEmptyValue =               100;
        public const int CrossTabHorAlignment =             110;		
		public const int CrossTabPrintIfEmpty =				120;
        public const int CrossTabRightToLeft =              130;		
		public const int CrossTabShowSummarySubHeaders =	140;
		public const int CrossTabWrap =						150;
		public const int CrossTabWrapGap =					160;
        public const int CrossTabPrintTitleOnAllPages =     170;
        #endregion

        #region Map
        public const int MapKeyDataColumn =                 100;
        public const int MapNameDataColumn =                101;
        public const int MapValueDataColumn =               102;
        public const int MapGroupDataColumn =               103;
        public const int MapColorDataColumn =               104;
        #endregion

        #region Data
        public const int DataDataSource =					100;
        public const int DataBusinessObject =               105;
		public const int DataDataRelation =					110;
		public const int DataMasterComponent =				120;
		public const int DataCountData =					130;
		public const int DataResetDataSource =				140;
		public const int DataFilterOn =						150;
		public const int DataFilters =						160;
        public const int DataFilterEngine =                 165;
		public const int DataFilterMode =					170;        
		public const int DataSort =							180;		
		public const int DataCalcInvisible =				200;	
		public const int DataCondition =					210;
		public const int DataSortDirection =				220;
        public const int DataSummarySortDirection =         230;
        public const int DataSummaryExpression =            240;
        public const int DataSummaryType =                  250;
		#endregion

        #region Table
        public const int TableAutoWidth =               100;
        public const int TableAutoWidthType =           110;
        public const int TableColumnCount =             120;
        public const int TableRowCount =                130;
        public const int TableHeaderRowsCount =         140;
        public const int TableFooterRowsCount =         150;
        public const int TableDefaultHeightCell =       160;
        public const int TableDockableTable =           170;
        public const int TableSkipLastColumn =          180;

        #region Table.Header
        public const int TableBandHeaderPrintOn = 100;
        public const int TableBandHeaderCanGrow = 110;
        public const int TableBandHeaderCanShrink = 120;
        public const int TableBandHeaderCanBreak = 130;
        public const int TableBandHeaderPrintAtBottom = 140;
        public const int TableBandHeaderPrintIfEmpty = 150;
        public const int TableBandHeaderPrintOnAllPages = 160;
        public const int TableBandHeaderPrintOnEvenOddPages = 170;
        #endregion

        #region Table.Footer
        public const int TableBandFooterPrintOn = 100;
        public const int TableBandFooterCanGrow = 110;
        public const int TableBandFooterCanShrink = 120;
        public const int TableBandFooterCanBreak = 130;
        public const int TableBandFooterPrintAtBottom = 140;
        public const int TableBandFooterPrintIfEmpty = 150;
        public const int TableBandFooterPrintOnAllPages = 160;
        public const int TableBandFooterPrintOnEvenOddPages = 170;
        #endregion
        #endregion

        #region Table Element
        public const int TableElementColumns = 100;
        #endregion

        #region Title
        public const int TitleText = 100;        
        public const int TitleBrush = 101;
        public const int TitleHorAlignment = 102;
        public const int TitleFont = 103;
        public const int TitleTextBrush = 104;        
        public const int TitleVisible = 105;
        #endregion

        #region Design
        public const int DesignName =						100;
		public const int DesignAlias =						110;
		public const int DesignGlobalizedName =				120;
		public const int DesignRestrictions =				130;
		public const int DesignLocked =						140;
		public const int DesignLinked =						150;
		public const int DesignLargeHeight =				160;
		public const int DesignLargeHeightFactor =			170;
		public const int DesignTag =						180;
		public const int DesignToolTip =					190;
		#endregion		

		#region Export
		public const int ExportExcelValue =					100;
		public const int ExportExportAsImage =				110;
		public const int ExportExcelSheet =					120;		
		#endregion

		#region Image
		public const int ImageImage =						100;		
		public const int ImageDataColumn =					110;
		public const int ImageImageData =					120;
		public const int ImageImageURL =					130;
        public const int ImageImageHyperlink =              130;
        public const int ImageFile =						140;
        #endregion

        #region Image Additional
        public const int ImageAspectRatio =					100;
		public const int ImageMultipleFactor =				110;
        public const int ImageMargins =                     115;
        public const int ImageHorAlignment =                120;
		public const int ImageVertAlignment =				130;
        public const int ImageImageRotation =               140;        
        public const int ImageProcessingDuplicates =        150;
		public const int ImageSmoothing =					160;
		public const int ImageStretch =						170;        
		#endregion	

		#region Sub-Report
		public const int SubReportSubReportPage =			100;
		public const int SubReportUseExternalReport =		110;
        public const int SubReportParameterCollection =     120;
        #endregion	

		#region Navigation
		public const int NavigationHyperlink =				100;
		public const int NavigationBookmark =				110;
		public const int NavigationToolTip =				120;
		public const int NavigationTag =					130;
		#endregion		
		
		#region Primitive
		public const int PrimitiveColor =					100;
		public const int PrimitiveSize =					110;
		public const int PrimitiveStyle =					120;
        public const int PrimitiveStartCap =                130;
        public const int PrimitiveEndCap =                  140;
        public const int PrimitiveRound =                   145;
        public const int PrimitiveTopSide =                 150;
        public const int PrimitiveLeftSide =                160;
        public const int PrimitiveBottomSide =              170;
        public const int PrimitiveRightSide =               180;
		#endregion				

		#region Page
		public const int PagePaperSize =					100;		
		public const int PagePaperSourceOfFirstPage =		110;
		public const int PagePaperSourceOfOtherPages =		120;
		public const int PagePageWidth =					130;
		public const int PagePageHeight =					140;
		public const int PageOrientation =					150;
		public const int PageWatermark =					160;
		public const int PageMargins =						170;
		public const int PageNumberOfCopies =				180;
        #endregion

        #region Page Additional
        public const int PageMirrorMargins =                90;
        public const int PageStretchToPrintArea =           100;
		public const int PageStopBeforePrint =				110;
		public const int PageTitleBeforeHeader =			120;
		public const int PageUnlimitedWidth =				130;
		public const int PageUnlimitedHeight =				140;	
		public const int PageUnlimitedBreakable =			150;
		public const int PageSegmentPerWidth =				160;
		public const int PageSegmentPerHeight =				170;		
		#endregion		

		#region Report
		public const int ReportDescriptionReportName = 100;
		public const int ReportDescriptionReportAlias = 110;
		public const int ReportDescriptionReportAuthor = 120;
		public const int ReportDescriptionReportDescription = 130;
        public const int ReportDescriptionReportImage = 140;

		public const int ReportMainAutoLocalizeReportOnRun = 200;
		public const int ReportMainCacheAllData = 210;
        public const int ReportMainCacheTotals = 212;
        public const int ReportMainCalculationMode = 215;
		public const int ReportMainConvertNulls = 220;
		public const int ReportMainCollate = 230;
        public const int ReportMainCulture = 235;
        public const int ReportMainEngineVersion = 240;
		public const int ReportMainGlobalizationStrings = 250;
		public const int ReportMainNumberOfPass = 260;
		public const int ReportMainPreviewMode = 270;
		public const int ReportMainPreviewSettings = 280;
		public const int ReportMainPrinterSettings = 290;
		public const int ReportMainReferencedAssemblies = 300;
        public const int ReportMainRefreshTime = 305;
        public const int ReportMainReportCacheMode = 310;
		public const int ReportMainReportUnit = 320;
        public const int ReportMainRetrieveOnlyUsedData = 322;
        public const int ReportMainParametersPanelOrientation = 324;
        public const int ReportMainRequestParameters = 325;
		public const int ReportMainScriptLanguage = 330;
		public const int ReportMainStopBeforePage = 340;
        public const int ReportMainStoreImagesInResources = 340;
        public const int ReportMainStyles = 350;
		#endregion		

		#region Shape
		public const int ShapeShapeType =					100;
		public const int ShapeStyle =						110;
		public const int ShapeSize =						120;
		public const int ShapeBorderColor =					130;
		#endregion

		#region Style
		public const int StyleName =						100;
		public const int StyleDescription =					110;
        public const int StyleCollectionName =              111;
        public const int StyleConditions =                  112;        
        public const int StyleColor =                       115;
		public const int StyleBrushType =					116;		
		public const int StyleBrush =						130;
        public const int StyleTextBrush =                   135;
		public const int StyleBorder =						140;        
		public const int StyleFont =						150;
		public const int StyleImage =						155;
		public const int StyleTextOptions =					160;
		public const int StyleHorAlignment =				170;
		public const int StyleVertAlignment =				180;
		#endregion
	
		#region Style Parameters
		public const int StyleAllowUseBorder =				100;
        public const int StyleAllowUseBorderFormatting =    105;
        public const int StyleAllowUseBorderSides =         106;
        public const int StyleAllowUseborderSidesFromLocation = 107;		
		public const int StyleAllowUseBrush =				110;
		public const int StyleAllowUseFont =				120;
		public const int StyleAllowUseImage =				130;
		public const int StyleAllowUseTextBrush =			140;
		public const int StyleAllowUseTextOptions =			150;
		public const int StyleAllowUseHorAlignment =		160;
		public const int StyleAllowUseVertAlignment =		170;
        public const int StyleStyleCode =                   300;
		#endregion

		#region Text
		public const int TextText =							100;
		public const int TextFont =							110;		
		public const int TextHorAlignment =					130;
		public const int TextVertAlignment =				140;		
		public const int TextTextFormat =					150;
		
		public const int TextBackColor =					170;
		public const int TextDataColumn =					180;
	    public const int TextDataUrl =                      185;
        public const int TextCellWidth =					190;
		public const int TextCellHeight =					200;
		public const int TextHorSpacing =					210;
		public const int TextVertSpacing =					220;
		public const int TextMinSize =						230;
		public const int TextMaxSize =						240;		
		public const int TextTextOptions =					260;		

		public const int TextContinuousText =				270;
		#endregion
			
		#region Text Additional
		public const int TextAllowHtmlText =				100;        
		public const int TextAngle =						103;
        public const int TextDetectUrls =                   105;
		public const int TextEditable =						110;
		public const int TextFullConvertExpression =		120;
		public const int TextHideZeros =					130;
		public const int TextLinesOfUnderline =				140;
        public const int TextLineSpacing =                  145;
        public const int TextMargins =						150;
		public const int TextMaxNumberOfLines =				160;
		public const int TextOnlyText =						170;
        public const int TextProcessAt =                    180;
		public const int TextProcessAtEnd =					180;
		public const int TextProcessingDuplicates =			190;
		public const int TextRenderTo =						200;
		public const int TextRightToLeft =					205;
		public const int TextShrinkFontToFit =				210;
		public const int TextShrinkFontToFitMinimumSize =	220;
		public const int TextTextQuality =					230;
        public const int TextTextBrush = 240;   
		public const int TextWordWrap =						300;
        public const int RightToLeft =                      310;
        public const int TextWysiwyg =                      320;
        #endregion		

		#region Watermark
		public const int WatermarkEnabled =					100;

		public const int WatermarkText =					110;
		public const int WatermarkAngle =					120;
		public const int WatermarkFont =					130;
		public const int WatermarkTextBrush =				140;
		public const int WatermarkShowBehind =				150;
		public const int WatermarkShowImageBehind =			160;		
		
		public const int WatermarkImage =					170;
		public const int WatermarkImageAlignment =			180;
		public const int WatermarkImageMultipleFactor =		190;
		public const int WatermarkImageStretch =			200;
		public const int WatermarkImageTiling =				210;
		public const int WatermarkImageTransparency =		220;
		#endregion

		#region WinControl
		public const int WinControlTypeName =				100;
		public const int WinControlText =					110;		
		public const int WinControlFont =					120;
		public const int WinControlForeColor =				130;
		public const int WinControlBackColor =				140;
		#endregion

		#region ZipCode
		public const int ZipCodeCode =						100;
		public const int ZipCodeSize =						110;
        public const int ZipCodeRatio =                     120;
        public const int ZipCodeSpaceRatio =                125;
		public const int ZipCodeForeColor =					130;
		public const int ZipCodeTextBrush =					140;
        public const int ZipCodeUpperMarks =                150;
        #endregion
	}
}
