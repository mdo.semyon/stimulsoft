#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.Engine;

namespace Stimulsoft.Report.Components
{	
	public class StiPageInfoV2 : StiComponentInfo
	{
		internal StiComponentsCollection overlays = null;

		/// <summary>
		/// ������ �������� � ������� �������� ���������� ����������� �� ��������.
		/// </summary>
		public int IndexOfStartRenderedPages = -1;

		/// <summary>
		/// ������������ ��� �������� MasterComponent ��� ���������� SubPage's.
		/// </summary>
		public StiDataBand MasterDataBand = null;

		/// <summary>
		/// ���� ����� true, �� ReportTitleBand's ��� �������� � ������ �� ��� ���� �������� ������� �� �����.
		/// </summary>
		public bool IsReportTitlesRendered = false;

		/// <summary>
		/// ���������� ������������� �������. 
		/// </summary>
		public int RenderedCount = 0;

		/// <summary>
		/// �������� ������� ������� ������ ����� ������ ����������� ������. ������������ 
		/// ��� ����������� ������� ������� ������ CrossTab's, � ����� ��� ������ �����-����������.
		/// </summary>
		public double PositionFromTop = 0;

		/// <summary>
		/// �������� ������� ������ ������ ����� ������ ����������� ������. ������������ 
		/// ��� ����������� ������ ������� ������ CrossTab's, � ����� ��� ������ �����-����������.
		/// </summary>
		public double PositionFromBottom = 0;
	}
}
