#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Report.Components.ShapeTypes;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Report.Units;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// Class describes the component that serves to show shapes.
	/// </summary>
    [StiServiceBitmap(typeof(StiShape), "Stimulsoft.Report.Images.Components.StiShape.png")]
    [StiServiceCategoryBitmap(typeof(StiComponent), "Stimulsoft.Report.Images.Components.StiShape.png")]
    [StiGdiPainter(typeof(Stimulsoft.Report.Painters.StiShapeGdiPainter))]
    [StiDesigner("Stimulsoft.Report.Components.Design.StiShapeDesigner, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiShapeWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
    [StiWpfDesigner("Stimulsoft.Report.WpfDesign.StiWpfShapeDesigner, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
	[StiToolbox(true)]
	[StiContextTool(typeof(IStiShift))]
	[StiContextTool(typeof(IStiGrowToHeight))]
	public class StiShape : 
		StiComponent,
		IStiBrush,
		IStiBorderColor,
		IStiExportImageExtended,
        IStiShape
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // Old
            jObject.RemoveProperty("CanShrink");
            jObject.RemoveProperty("CanGrow");

            // StiShape
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyStringNullOrEmpty("BorderColor", StiJsonReportObjectHelper.Serialize.JColor(BorderColor, Color.Black));
            jObject.AddPropertyEnum("Style", Style, StiPenStyle.Solid);
            jObject.AddPropertyFloat("Size", Size, 1f);
            jObject.AddPropertyJObject("ShapeType", ShapeType.SaveToJsonObject(mode));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Brush":
                        this.brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "BorderColor":
                        this.borderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Style":
                        this.style = (StiPenStyle)Enum.Parse(typeof(StiPenStyle), property.Value.ToObject<string>());
                        break;

                    case "Size":
                        this.size = property.Value.ToObject<float>();
                        break;

                    case "ShapeType":
                        this.shapeType = StiShapeTypeService.CreateFromJsonObject((JObject)property.Value);
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiShape;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[]
            {
                propHelper.ShapeEditor()
            };
            objHelper.Add(StiPropertyCategories.ComponentEditor, list);

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height(),
                    propHelper.MinSize(),
                    propHelper.MaxSize()
                };
            }
            objHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Brush(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Brush(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle(),
                    propHelper.UseParentStyles()
                };
            }
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.GrowToHeight(),
                    propHelper.Enabled()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.GrowToHeight(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.GrowToHeight(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.Printable(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new []
                {
                    propHelper.Name()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias(),
                    propHelper.Restrictions(),
                    propHelper.Locked(),
                    propHelper.Linked()
                };
            }
            objHelper.Add(StiPropertyCategories.Design, list);

            return objHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // ValueEventsCategory
            var list = new[] { StiPropertyEventId.GetToolTipEvent, StiPropertyEventId.GetTagEvent };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            // NavigationEventsCategory
            list = new[] { StiPropertyEventId.GetHyperlinkEvent, StiPropertyEventId.GetBookmarkEvent };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[] { StiPropertyEventId.BeforePrintEvent, StiPropertyEventId.AfterPrintEvent };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // MouseEventsCategory
            list = new[] { StiPropertyEventId.GetDrillDownReportEvent, StiPropertyEventId.ClickEvent, StiPropertyEventId.DoubleClickEvent, 
                StiPropertyEventId.MouseEnterEvent, StiPropertyEventId.MouseLeaveEvent };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties

        public override string HelpUrl
        {
            get
            {
                return "user-manual/report_internals_primitives.htm?zoom_highlightsub=Shape";
            }
        }

        #endregion

		#region IStiCanShrink override
		[Browsable(false)]
		[StiNonSerialized]
		public override bool CanShrink
		{
			get
			{
				return base.CanShrink;
			}
			set
			{
			}
		}
		#endregion

		#region IStiCanGrow override
		[Browsable(false)]
		[StiNonSerialized]
		public override bool CanGrow
		{
			get
			{
				return base.CanGrow;
			}
			set
			{
			}
		}
		#endregion

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public override object Clone(bool cloneProperties)
		{
			StiShape shape =	(StiShape)base.Clone(cloneProperties);
			
			if (this.shapeType != null)shape.shapeType =	(StiShapeTypeService)this.shapeType.Clone();
			else shape.shapeType = null;

			return shape;
		}
		#endregion

        #region IStiUnitConvert
        /// <summary>
        /// Converts a component out of one unit into another.
        /// </summary>
        /// <param name="oldUnit">Old units.</param>
        /// <param name="newUnit">New units.</param>
        public override void Convert(StiUnit oldUnit, StiUnit newUnit, bool isReportSnapshot = false)
        {
            base.Convert(oldUnit, newUnit, isReportSnapshot);

            if (ShapeType is StiOctagonShapeType)
            {
                (ShapeType as StiOctagonShapeType).Bevel = (float)newUnit.ConvertFromHInches(oldUnit.ConvertToHInches((ShapeType as StiOctagonShapeType).Bevel));
            }
        }
        #endregion

		#region IStiExportImageExtended
		public virtual Image GetImage(ref float zoom)
		{
			return GetImage(ref zoom, StiExportFormat.None);
		}

		public virtual Image GetImage(ref float zoom, StiExportFormat format)
		{
            StiPainter painter = StiPainter.GetPainter(this.GetType(), StiGuiMode.Gdi);
            return painter.GetImage(this, ref zoom, format);
		}

		[Browsable(false)]
		public override bool IsExportAsImage(StiExportFormat format)
		{
			return true;
		}
		#endregion

		#region IStiBrush
		private StiBrush brush = new StiSolidBrush();
		/// <summary>
		/// Gets or sets a brush to fill a component.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.AppearanceBrush)]
		[StiSerializable]
		[Description("Gets or sets a brush to fill a component.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual StiBrush Brush
		{
			get 
			{
				return brush;
			}
			set 
			{
				brush = value;
			}
		}
		#endregion

		#region IStiBorderColor
		private Color borderColor = Color.Black;
		/// <summary>
		/// Gets or sets border color.
		/// </summary>
		[StiCategory("Shape")]
		[StiOrder(StiPropertyOrder.ShapeBorderColor)]
		[StiSerializable()]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets border color.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
			}
		}

		#endregion

		#region StiComponent override
		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.Shape;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Shapes;
            }
        }

		/// <summary>
		/// Gets a localized name of the component category.
		/// </summary>
		public override string LocalizedCategory
		{
			get 
			{
                return StiLocalization.Get("Report", "Shapes");
			}
		}

		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiShape");
			}
		}
		#endregion		
        
        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiShape();
        }
        #endregion

		#region this
		/// <summary>
		/// Gets or sets the default client area of a component.
		/// </summary>
		[Browsable(false)]
		public override RectangleD DefaultClientRectangle
		{
			get
			{
				return new RectangleD(0, 0, 64, 64);
			}
		}


		private StiPenStyle style = StiPenStyle.Solid;
		/// <summary>
		/// Gets or sets a pen style.
		/// </summary>
		[Editor("Stimulsoft.Base.Drawing.Design.StiPenStyleEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[StiSerializable]
		[DefaultValue(StiPenStyle.Solid)]
		[StiCategory("Shape")]
		[StiOrder(StiPropertyOrder.ShapeStyle)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		[Description("Gets or sets a pen style.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiPenStyle Style
		{
			get 
			{
				return style;
			}
			set 
			{
				style = value;
			}
		}
		

		private float size = 1f;
		/// <summary>
		/// Gets or sets size of the border.
		/// </summary>
		[StiCategory("Shape")]
		[StiOrder(StiPropertyOrder.ShapeSize)]
		[StiSerializable]
		[DefaultValue(1f)]
        [Description("Gets or sets size of the border.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public float Size
		{
			get 
			{
				return size;
			}
			set 
			{
                if (value != size)
                {
                    if (value < 1) size = 1;
                    else size = value;
                }
			}
		}


		private StiShapeTypeService shapeType = new StiRectangleShapeType();
		/// <summary>
		/// Gets or sets type of the shape.
		/// </summary>
		[StiCategory("Shape")]
		[StiOrder(StiPropertyOrder.ShapeShapeType)]
		[StiSerializable(StiSerializationVisibility.Class)]
		[Editor("Stimulsoft.Report.Components.ShapeTypes.Design.StiShapeTypeServiceEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets type of the shape.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiShapeTypeService ShapeType
		{
			get 
			{
				return shapeType;
			}
			set 
			{
				shapeType = value;
			}
		}


		/// <summary>
		/// Creates a new component of the type StiShape.
		/// </summary>
		public StiShape() : this(RectangleD.Empty)
		{
		}


		/// <summary>
		/// Creates a new component of the type StiShape.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiShape(RectangleD rect) : base(rect)
		{
			PlaceOnToolbox = false;
		}
		#endregion
	}
}