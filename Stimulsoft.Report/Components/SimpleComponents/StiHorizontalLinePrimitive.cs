#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using System;

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// Describes class that realizes component - StiHorizontalLinePrimitive.
	/// </summary>
	[StiToolbox(true)]
    [StiServiceBitmap(typeof(StiHorizontalLinePrimitive), "Stimulsoft.Report.Images.Components.StiHorizontalLinePrimitive.png")]
	[StiDesigner("Stimulsoft.Report.Components.Design.StiHorizontalLinePrimitiveDesigner, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfDesigner("Stimulsoft.Report.WpfDesign.StiWpfHorizontalLinePrimitiveDesigner, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
    [StiGdiPainter(typeof(StiHorizontalLinePrimitiveGdiPainter))]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiHorizontalLinePrimitiveWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
	public class StiHorizontalLinePrimitive : 
		StiLinePrimitive, 
		IStiHideBorderFromDesigner,
		IStiBorder
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiHorizontalLinePrimitive
            jObject.AddPropertyStringNullOrEmpty("StartCap", StiJsonReportObjectHelper.Serialize.JCap(StartCap));
            jObject.AddPropertyStringNullOrEmpty("EndCap", StiJsonReportObjectHelper.Serialize.JCap(EndCap));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "StartCap":
                        this.startCap = StiJsonReportObjectHelper.Deserialize.JCap(property.Value.ToObject<string>());
                        break;

                    case "EndCap":
                        this.endCap = StiJsonReportObjectHelper.Deserialize.JCap(property.Value.ToObject<string>());
                        break;
                }
            }
        }

        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiHorizontalLinePrimitive;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();
            StiPropertyObject[] list;

            // PrimitiveCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Color(),
                    propHelper.SizeFloat(),
                    propHelper.Style()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Color(),
                    propHelper.SizeFloat(),
                    propHelper.Style(),
                    propHelper.StartCap(),
                    propHelper.EndCap()
                };
            }
            objHelper.Add(StiPropertyCategories.Primitive, list);

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.MinSize(),
                    propHelper.MaxSize()
                };
            }
            objHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            list = new[]
            {
                propHelper.ComponentStyle()
            };
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Enabled()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.Enabled(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.Enabled(),
                    propHelper.Printable(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Name()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias(),
                    propHelper.Restrictions(),
                    propHelper.Locked(),
                    propHelper.Linked()
                };
            }
            objHelper.Add(StiPropertyCategories.Design, list);

            return objHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // ValueEventsCategory
            var list = new[] { StiPropertyEventId.GetToolTipEvent, StiPropertyEventId.GetTagEvent };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            // NavigationEventsCategory
            list = new[] { StiPropertyEventId.GetHyperlinkEvent, StiPropertyEventId.GetBookmarkEvent };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[] { StiPropertyEventId.BeforePrintEvent, StiPropertyEventId.AfterPrintEvent };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // MouseEventsCategory
            list = new[] { StiPropertyEventId.GetDrillDownReportEvent, StiPropertyEventId.ClickEvent, StiPropertyEventId.DoubleClickEvent, 
                StiPropertyEventId.MouseEnterEvent, StiPropertyEventId.MouseLeaveEvent };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties

        public override string HelpUrl
        {
            get
            {
                return "user-manual/report_internals_primitives.htm";
            }
        }

        #endregion

        #region ICloneable override
        public override object Clone(bool cloneProperties)
        {
            StiHorizontalLinePrimitive cloneLine = (StiHorizontalLinePrimitive)base.Clone(cloneProperties);

            if (startCap != null) cloneLine.startCap = (StiCap)this.startCap.Clone();
            else cloneLine.startCap = null;
            if (endCap != null) cloneLine.endCap = (StiCap)this.endCap.Clone();
            else cloneLine.endCap = null;

            return cloneLine;
        }
        #endregion

		#region IStiBorder
		private StiBorder border = null;
		/// <summary>
		/// Gets or sets frame of the component.
		/// </summary>
		[Browsable(false)]
		[StiNonSerialized]
		public StiBorder Border
		{
			get 
			{
				if (border == null)
				{
					border = new StiBorder(
						StiBorderSides.Top, this.Color, this.Size, this.Style, false, 0, null);
				}
				return border;
			}
			set 
			{
			}
		}
		#endregion

        #region StiComponent override
        /// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.HorizontalLinePrimitive;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Shapes;
            }
        }

		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiHorizontalLinePrimitive");
			}
		}
		#endregion

        #region Properties
        private StiCap startCap = new StiCap();
        /// <summary>
        /// Gets or sets the start cap settings.
        /// </summary>
        [Browsable(true)]
        [StiCategory("Primitive")]
        [StiSerializable(StiSerializationVisibility.Class)]
        [StiOrder(StiPropertyOrder.PrimitiveStartCap)]
        [Description("Gets or sets the start cap settings.")]
        [RefreshProperties(RefreshProperties.All)]
        [StiPropertyLevel(StiLevel.Standard)]
        public StiCap StartCap
        {
            get
            {
                return startCap;
            }
            set
            {
                startCap = value;
            }
        }

        private StiCap endCap = new StiCap();
        /// <summary>
        /// Gets or sets the end cap settings.
        /// </summary>
        [Browsable(true)]
        [StiCategory("Primitive")]
        [StiSerializable(StiSerializationVisibility.Class)]
        [StiOrder(StiPropertyOrder.PrimitiveEndCap)]
        [Description("Gets or sets the end cap settings.")]
        [RefreshProperties(RefreshProperties.All)]
        [StiPropertyLevel(StiLevel.Standard)]
        public StiCap EndCap
        {
            get
            {
                return endCap;
            }
            set
            {
                endCap = value;
            }
        }
        #endregion

		#region Position
		[Browsable(false)]
		public override double Height
		{
			get 
			{
				if (Page != null && Page.Unit != null)return Page.Unit.ConvertFromHInches(1d);
				return 1;
			}
			set 
			{
			}
		}
		#endregion		
        
        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiHorizontalLinePrimitive();
        }
        #endregion

		#region this
		/// <summary>
		/// Creates a new StiHorizontalLinePrimitive.
		/// </summary>
		public StiHorizontalLinePrimitive() : this(RectangleD.Empty)
		{
		}

		/// <summary>
		/// Creates a new StiHorizontalLinePrimitive.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiHorizontalLinePrimitive(RectangleD rect): base(rect)
		{
            PlaceOnToolbox = false;
		}
		#endregion
	}
}