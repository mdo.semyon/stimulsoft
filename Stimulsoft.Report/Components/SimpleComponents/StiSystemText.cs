#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using System;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// Describes the class that realizes a category - SystemText. 
	/// </summary>
	[StiToolbox(false)]
	public class StiSystemText : StiText
	{
        #region StiComponent.Properties

        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiSystemText;
            }
        }

        #endregion

		#region StiComponent override
		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.SystemText;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Components;
            }
        }

		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiSystemText");
			}
		}
		#endregion

		#region this
		/// <summary>
		/// Creates a new component of the type StiSystemText.
		/// </summary>
		public StiSystemText() : this(RectangleD.Empty, string.Empty)
		{
		}

		/// <summary>
		/// Creates a new component of the type StiSystemText.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiSystemText(RectangleD rect) : this(rect, string.Empty)
		{
		}

		/// <summary>
		/// Creates a new component of the type StiSystemText.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		/// <param name="text">Text expression.</param>
		public StiSystemText(RectangleD rect, string text) : base(rect, text)
		{
			PlaceOnToolbox = true;
		}
		#endregion
	}
}
