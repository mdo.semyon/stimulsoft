#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using System.Globalization;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// Class describes the component - Check Box.
	/// </summary>
    [StiServiceBitmap(typeof(StiCheckBox), "Stimulsoft.Report.Images.Components.StiCheckBox.png")]
    [StiGdiPainter(typeof(Stimulsoft.Report.Painters.StiCheckBoxGdiPainter))]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiCheckBoxWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
	[StiToolbox(true)]
	[StiContextTool(typeof(IStiShift))]
	[StiContextTool(typeof(IStiCanGrow))]
	[StiContextTool(typeof(IStiGrowToHeight))]
	[StiContextTool(typeof(IStiCanShrink))]
	[StiContextTool(typeof(IStiEditable))]
	public class StiCheckBox: 
		StiComponent,
		IStiBorder,
		IStiTextBrush,
		IStiEditable,
		IStiBrush,
        IStiBreakable,
		IStiExportImageExtended
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // Old
            jObject.RemoveProperty("CanShrink");
            jObject.RemoveProperty("CanGrow");

            // StiCheckBox
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyStringNullOrEmpty("Border", StiJsonReportObjectHelper.Serialize.JBorder(Border));
            jObject.AddPropertyStringNullOrEmpty("TextBrush", StiJsonReportObjectHelper.Serialize.JBrush(TextBrush));
            jObject.AddPropertyBool("Editable", Editable);
            jObject.AddPropertyJObject("GetCheckedEvent", GetCheckedEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetExcelValueEvent", GetExcelValueEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Checked", Checked.SaveToJsonObject(mode));
            jObject.AddPropertyStringNullOrEmpty("ExcelDataValue", ExcelDataValue);
            jObject.AddPropertyJObject("ExcelValue", ExcelValue.SaveToJsonObject(mode));
            jObject.AddPropertyStringNullOrEmpty("ContourColor", StiJsonReportObjectHelper.Serialize.JColor(ContourColor, Color.Black));
            jObject.AddPropertyDouble("Size", Size, 1d);
            jObject.AddPropertyString("Values", Values, "true/false");
            jObject.AddPropertyEnum("CheckStyleForTrue", CheckStyleForTrue, StiCheckStyle.Check);
            jObject.AddPropertyEnum("CheckStyleForFalse", CheckStyleForFalse, StiCheckStyle.None);
            if (mode == StiJsonSaveMode.Document && CheckedValue != null)
                jObject.AddPropertyString("CheckedValue", CheckedValue.ToString());

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Brush":
                        this.brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "Border":
                        this.border = StiJsonReportObjectHelper.Deserialize.Border(property);
                        break;

                    case "TextBrush":
                        this.textBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "Editable":
                        this.editable = property.Value.ToObject<bool>();
                        break;

                    case "GetCheckedEvent":
                        {
                            var _event = new StiGetCheckedEvent();
                            _event.LoadFromJsonObject((JObject)property.Value);
                            this.GetCheckedEvent = _event;
                        }
                        break;

                    case "GetExcelValueEvent":
                        {
                            var _event = new StiGetExcelValueEvent();
                            _event.LoadFromJsonObject((JObject)property.Value);
                            this.GetExcelValueEvent = _event;
                        }
                        break;

                    case "Checked":
                        {
                            var _expression = new StiCheckedExpression();
                            _expression.LoadFromJsonObject((JObject)property.Value);
                            this.Checked = _expression;
                        }
                        break;

                    case "ExcelDataValue":
                        this.excelDataValue = property.Value.ToObject<string>();
                        break;

                    case "ExcelValue":
                        {
                            var _expression = new StiExcelValueExpression();
                            _expression.LoadFromJsonObject((JObject)property.Value);
                            this.ExcelValue = _expression;
                        }
                        break;

                    case "ContourColor":
                        this.contourColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Size":
                        this.size = property.Value.ToObject<double>();
                        break;

                    case "Values":
                        this.values = property.Value.ToObject<string>();
                        break;

                    case "CheckStyleForTrue":
                        this.checkStyleForTrue = (StiCheckStyle)Enum.Parse(typeof(StiCheckStyle), property.Value.ToObject<string>());
                        break;

                    case "CheckStyleForFalse":
                        this.checkStyleForFalse = (StiCheckStyle)Enum.Parse(typeof(StiCheckStyle), property.Value.ToObject<string>());
                        break;

                    case "CheckedValue":
                        this.CheckedValue = property.Value.ToObject<string>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiCheckBox;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var checkBoxHelper = new StiPropertyCollection();
            StiPropertyObject[] list;

            // CheckCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Checked(),
                    propHelper.TextBrush()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Checked(),
                    propHelper.CheckStyleForTrue(),
                    propHelper.CheckStyleForFalse(),
                    propHelper.CheckBoxValues(),
                    propHelper.Size(),
                    propHelper.ContourColor(),
                    propHelper.Editable(),
                    propHelper.TextBrush()
                };
            }
            checkBoxHelper.Add(StiPropertyCategories.Check, list);

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height(),
                    propHelper.MinSize(),
                    propHelper.MaxSize()
                };
            }
            checkBoxHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Brush(),
                    propHelper.Border(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Brush(),
                    propHelper.Border(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle(),
                    propHelper.UseParentStyles()
                };
            }
            checkBoxHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.GrowToHeight(),
                    propHelper.Enabled()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.GrowToHeight(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.GrowToHeight(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.Printable(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            checkBoxHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new []
                {
                    propHelper.Name()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias(),
                    propHelper.Restrictions(),
                    propHelper.Locked(),
                    propHelper.Linked()
                };
            }
            checkBoxHelper.Add(StiPropertyCategories.Design, list);

            // ExportCategory
            if (level == StiLevel.Professional)
            {
                list = new []
                {
                    propHelper.ExcelValue()
                };
                checkBoxHelper.Add(StiPropertyCategories.Export, list);
            }

            return checkBoxHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // ValueEventsCategory
            var list = new[]
                {
                    StiPropertyEventId.GetCheckedEvent, 
                    StiPropertyEventId.GetExcelValueEvent, 
                    StiPropertyEventId.GetToolTipEvent, 
                    StiPropertyEventId.GetTagEvent
                };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            // NavigationEventsCategory
            list = new[]
                {
                    StiPropertyEventId.GetHyperlinkEvent, 
                    StiPropertyEventId.GetBookmarkEvent
                };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[]
                {
                    StiPropertyEventId.BeforePrintEvent, 
                    StiPropertyEventId.AfterPrintEvent
                };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // MouseEventsCategory
            list = new[]
                {
                    StiPropertyEventId.GetDrillDownReportEvent,
                    StiPropertyEventId.ClickEvent,
                    StiPropertyEventId.DoubleClickEvent,
                    StiPropertyEventId.MouseEnterEvent,
                    StiPropertyEventId.MouseLeaveEvent
                };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties

        public override string HelpUrl
        {
            get
            {
                return "User-Manual/report_internals_creating_lists_check_box.htm";
            }
        }

        #endregion

		#region IStiExportImageExtended
		public virtual Image GetImage(ref float zoom)
		{
			return GetImage(ref zoom, StiExportFormat.None);
		}


		public virtual Image GetImage(ref float zoom, StiExportFormat format)
		{
            StiPainter painter = StiPainter.GetPainter(this.GetType(), StiGuiMode.Gdi);
            return painter.GetImage(this, ref zoom, format);
		}


		[Browsable(false)]
		public override bool IsExportAsImage(StiExportFormat format)
		{
			if (format == StiExportFormat.Text) return false;
            if ((format == StiExportFormat.Excel ||
                format == StiExportFormat.ExcelXml ||
                format == StiExportFormat.Excel2007 ||
                format == StiExportFormat.Ods) &&
                !string.IsNullOrEmpty(excelDataValue)) return false;
			return true;
		}		
		#endregion

		#region IStiCanShrink override
		[Browsable(false)]
		[StiNonSerialized]
		public override bool CanShrink
		{
			get
			{
				return base.CanShrink;
			}
			set
			{
			}
		}
		#endregion

		#region IStiCanGrow override
		[Browsable(false)]
		[StiNonSerialized]
		public override bool CanGrow
		{
			get
			{
				return base.CanGrow;
			}
			set
			{
			}
		}
		#endregion

		#region IStiBrush
		private StiBrush brush = new StiSolidBrush();
		/// <summary>
		/// Gets or sets a brush to fill a component.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.AppearanceBrush)]
		[StiSerializable]
		[Description("Gets or sets a brush to fill a component.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiBrush Brush
		{
			get 
			{
				return brush;
			}
			set 
			{
				brush = value;
			}
		}
		#endregion

		#region IStiBorder
		private StiBorder border = new StiBorder();
		/// <summary>
		/// Gets or sets border of the component.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.AppearanceBorder)]
		[StiSerializable]
		[Description("Gets or sets border of the component.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiBorder Border
		{
			get 
			{
				return border;
			}
			set 
			{
				border = value;
			}
		}
		#endregion
		
		#region IStiTextBrush
		private StiBrush textBrush = new StiSolidBrush(Color.Black);
		/// <summary>
		/// Gets or sets a brush to draw text.
		/// </summary>
		[StiCategory("Check")]
		[StiOrder(StiPropertyOrder.CheckTextBrush)]
		[StiSerializable]
		[Description("Gets or sets a brush to draw text.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiBrush TextBrush
		{
			get 
			{
				return textBrush;
			}
			set 
			{
				textBrush = value;
			}
		}
		
		#endregion

		#region IStiEditable
		private bool editable = false;
		/// <summary>
		/// Gets or sets value indicates that a component can be edited in the window of viewer.
		/// </summary>
		[StiSerializable]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
		[StiCategory("Check")]
		[StiOrder(StiPropertyOrder.CheckEditable)]
		[Description("Gets or sets value indicates that a component can be edited in the window of viewer.")]
        [StiShowInContextMenu]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool Editable
		{
			get
			{
				return editable;
			}
			set
			{
				editable = value;
			}
		}

		/// <summary>
		/// Saves state of editable value.
		/// </summary>
		string IStiEditable.SaveState()
		{
			return (this.CheckedValue is bool && ((bool)this.CheckedValue) == true) ? "true" : "false";
		}

		/// <summary>
		/// Restores state of editable value.
		/// </summary>
		void IStiEditable.RestoreState(string value)
		{
			this.CheckedValue = value == "true";
		}
		#endregion

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public override object Clone(bool cloneProperties)
		{
			StiCheckBox checkBox = (StiCheckBox)base.Clone(cloneProperties);
			
			if (this.textBrush != null)checkBox.textBrush =		(StiBrush)this.textBrush.Clone();
			else checkBox.textBrush = null;
		
			return checkBox;
		}
		#endregion

        #region IStiBreakable
        private bool canBreak = false;
        [Browsable(false)]
        [StiNonSerialized]
        public virtual bool CanBreak
        {
            get
            {
                return canBreak || (this.GrowToHeight && (this.Page != null) && (this.Height / this.Page.PageHeight > 0.5));
            }
            set
            {
                canBreak = value;
            }
        }


        /// <summary>
        /// Divides content of components in two parts. Returns result of dividing. If true, then component is successful divided.
        /// </summary>
        /// <param name="dividedComponent">Component for store part of content.</param>
        /// <returns>If true, then component is successful divided.</returns>
        public bool Break(StiComponent dividedComponent, double devideFactor, ref double divideLine)
        {
            divideLine = 0;
            bool result = true;

            bool first = (devideFactor > 0.5f) || ((this.Page != null) && (this.Height / this.Page.PageHeight > 0.5));
            if (first)
            {
                ((StiCheckBox)dividedComponent).CheckedValue = null;
            }
            else
            {
                this.CheckedValue = null;
            }

            return result;
        }
        #endregion

		#region StiComponent override
		/// <summary>
		/// Return events collection of this component.
		/// </summary>
		public override StiEventsCollection GetEvents()
		{
			StiEventsCollection events = base.GetEvents();
			if (GetCheckedEvent != null)events.Add(GetCheckedEvent);
			return events;
		}


		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.CheckBox;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Components;
            }
        }

		/// <summary>
		/// Gets a localized name of the component category.
		/// </summary>
		public override string LocalizedCategory
		{
			get 
			{
				return StiLocalization.Get("Report", "Components");
			}
		}

		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiCheckBox");
			}
		}
		#endregion

		#region Events
		/// <summary>
		/// Invokes all events for this components.
		/// </summary>
		public override void InvokeEvents()
		{
			base.InvokeEvents();
			try
			{
				#region GetChecked
                if (Report.CalculationMode == StiCalculationMode.Compilation)
                {
                    if (this.Events[EventGetChecked] != null && checkedValue == null)
                    {
                        StiValueEventArgs e = new StiValueEventArgs();
                        InvokeGetChecked(this, e);
                        this.checkedValue = e.Value;
                    }
                }
                else
                {
                    if (((this.Events[EventGetChecked] != null) || !string.IsNullOrEmpty(this.Checked.Value)) && checkedValue == null)
                    {
                        StiValueEventArgs e = new StiValueEventArgs();
                        InvokeGetChecked(this, e);
                        this.checkedValue = e.Value;
                    }
                }
				#endregion

                #region GetExcelValue
                if (this.Events[EventGetExcelValue] != null && this.ExcelDataValue == null)
                {
                    StiGetExcelValueEventArgs e = new StiGetExcelValueEventArgs();
                    InvokeGetExcelValue(this, e);
                    if (e.Value != null) this.excelDataValue = e.Value.ToString();
                }
                #endregion
			}
			catch (Exception e)
			{
				StiLogService.Write(this.GetType(), "DoEvents...ERROR");
				StiLogService.Write(this.GetType(), e);

                if (Report != null)
                    Report.WriteToReportRenderingMessages(this.Name + " " + e.Message);
			}
		}

		#region GetChecked
        private static readonly object EventGetChecked = new object();

		/// <summary>
		/// Occurs when state is being checked.
		/// </summary>
		public event StiValueEventHandler GetChecked
		{
			add
			{
                base.Events.AddHandler(EventGetChecked, value);
			}
			remove
			{
                base.Events.RemoveHandler(EventGetChecked, value);
			}
		}

		/// <summary>
		/// Raises the GetChecked event.
		/// </summary>
		protected virtual void OnGetChecked(StiValueEventArgs e)
		{
		}
		

		/// <summary>
		/// Raises the GetChecked event.
		/// </summary>
		public void InvokeGetChecked(StiComponent sender, StiValueEventArgs e)
		{
			try
			{
                if (Report.CalculationMode == StiCalculationMode.Compilation)
                {
                    OnGetChecked(e);
                    StiValueEventHandler handler = base.Events[EventGetChecked] as StiValueEventHandler;
                    if (handler != null) handler(sender, e);
                }
                else
                {
                    OnGetChecked(e);

                    if (this.Checked.Value != null && this.Checked.Value.Length > 0)
                    {
                        object parserResult = StiParser.ParseTextValue(this.Checked.Value, this, sender);
                        if (parserResult != null) e.Value = parserResult;
                    }

                    StiValueEventHandler handler = base.Events[EventGetChecked] as StiValueEventHandler;
                    if (handler != null) handler(sender, e);
                }
			}
			catch (Exception ex)
			{
				string str = string.Format("Expression in Checked property of '{0}' can't be evaluated!", this.Name);
				StiLogService.Write(this.GetType(), str);
				StiLogService.Write(this.GetType(), ex.Message);
				Report.WriteToReportRenderingMessages(str);
			}
		}
		

		/// <summary>
		/// Occurs when state is being checked.
		/// </summary>
		[StiSerializable]
		[StiCategory("ValueEvents")]
		[Browsable(false)]
		[Description("Occurs when state is being checked.")]
		public StiGetCheckedEvent GetCheckedEvent
		{
			get
			{				
				return new StiGetCheckedEvent(this);
			}
			set
			{
				if (value != null)value.Set(this, value.Script);
			}
		}
		#endregion

        #region GetExcelValue
        private static readonly object EventGetExcelValue = new object();

        /// <summary>
        /// Occurs when the ExcelValue is calculated.
        /// </summary>
        public event StiGetExcelValueEventHandler GetExcelValue
        {
            add
            {
                base.Events.AddHandler(EventGetExcelValue, value);
            }
            remove
            {
                base.Events.RemoveHandler(EventGetExcelValue, value);
            }
        }


        /// <summary>
        /// Raises the GetExcelValue event.
        /// </summary>
        protected virtual void OnGetExcelValue(StiGetExcelValueEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetExcelValue event.
        /// </summary>
        public virtual void InvokeGetExcelValue(StiComponent sender, StiGetExcelValueEventArgs e)
        {
            try
            {
                OnGetExcelValue(e);

                StiGetExcelValueEventHandler handler = base.Events[EventGetExcelValue] as StiGetExcelValueEventHandler;
                if (handler != null) handler(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in ExcelValue property of '{0}' can't be evaluated!", this.Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                Report.WriteToReportRenderingMessages(str);
            }
        }


        /// <summary>
        /// Occurs when the ExcelValue is calculated.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when the ExcelValue is calculated.")]
        public StiGetExcelValueEvent GetExcelValueEvent
        {
            get
            {
                return new StiGetExcelValueEvent(this);
            }
            set
            {
                if (value != null) value.Set(this, value.Script);
            }
        }

        #endregion
        #endregion

		#region Expression
		#region Checked
		private object checkedValue;
		/// <summary>
		/// Gets or sets checked value.
		/// </summary>
		[Browsable(false)]
		[Description("Gets or sets checked value.")]
		[StiSerializable(StiSerializeTypes.SerializeToDocument)]
		public object CheckedValue
		{
			get
			{
				return checkedValue;
			}
			set
			{
				checkedValue = value;
			}
		}


		/// <summary>
        /// Gets or sets an expression which used to calculate check state.
		/// </summary>
		[StiCategory("Check")]
		[StiOrder(StiPropertyOrder.CheckChecked)]
		[StiSerializable]
		[Description("Gets or sets an expression which used to calculate check state.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiCheckedExpression Checked
		{
			get
			{
				return new StiCheckedExpression(this, "Checked");
			}
			set
			{
				if (value != null)value.Set(this, "Checked", value.Value);
			}
		}
		#endregion

        #region ExcelValue
        private string excelDataValue;
        /// <summary>
        /// Gets or sets excel data value.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets excel data value.")]
        [StiSerializable(StiSerializeTypes.SerializeToDocument)]        
        public string ExcelDataValue
        {
            get
            {
                return excelDataValue;
            }
            set
            {
                excelDataValue = value;
            }
        }


        /// <summary>
        /// Gets or sets an expression used for export data to Excel.
        /// </summary>
        [StiCategory("Export")]
        [StiOrder(StiPropertyOrder.ExportExcelValue)]
        [StiSerializable]
        [Description("Gets or sets an expression used for export data to Excel.")]
        [StiPropertyLevel(StiLevel.Professional)]
        public virtual StiExcelValueExpression ExcelValue
        {
            get
            {
                return new StiExcelValueExpression(this, "ExcelValue");
            }
            set
            {
                if (value != null) value.Set(this, "ExcelValue", value.Value);
            }
        }
        #endregion
		#endregion

        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiCheckBox();
        }
        #endregion

		#region this
		public bool IsChecked()
		{
			string []strs = null;

            if (Values.IndexOf('/') != -1) strs = Values.Split(new char[] { '/' });
            else if (Values.IndexOf(';') != -1) strs = Values.Split(new char[] { ';' });
            else if (Values.IndexOf(',') != -1) strs = Values.Split(new char[] { ',' });

			if (strs.Length == 0)return false;
			
			string value = strs[0].ToLower(CultureInfo.InvariantCulture).Trim();
			if (value.Length == 0)return false;
			if (CheckedValue == null)return false;

			string checkedValueStr = CheckedValue.ToString().ToLower(CultureInfo.InvariantCulture).Trim();
			return checkedValueStr == value;
		}


		public bool IsUnchecked()
		{
			string []strs = null;

            if (Values.IndexOf('/') != -1) strs = Values.Split(new char[] { '/' });
            else if (Values.IndexOf(';') != -1) strs = Values.Split(new char[] { ';' });
            else if (Values.IndexOf(',') != -1) strs = Values.Split(new char[] { ',' });

			if (strs.Length < 2)return false;
			
			string value = strs[1].ToLower(CultureInfo.InvariantCulture).Trim();
			if (value.Length == 0)return false;
			if (CheckedValue == null)return false;

			string checkedValueStr = CheckedValue.ToString().ToLower(CultureInfo.InvariantCulture).Trim();
			return checkedValueStr == value;
		}


        public Metafile GetMetafile()
        {
            Metafile mf = null;
            try
            {
                mf = GetMetafile1();
            }
            catch
            {
                //fix - trying to get a picture again, often it helps
                mf = GetMetafile1();
            }
            return mf;
        }

		private Metafile GetMetafile1()
		{
			using (Bitmap bmp = new Bitmap(1, 1))
			using (Graphics graph = Graphics.FromImage(bmp))
			{
				IntPtr ptrGraph = graph.GetHdc();
					
				Metafile newImage = new Metafile(ptrGraph, EmfType.EmfOnly);

				graph.ReleaseHdc(ptrGraph);

				using (Graphics imageGraph = Graphics.FromImage(newImage))
				{
					//imageGraph.Clear(Color.White);
					RectangleD rect = GetPaintRectangle(true, false);
					rect.X = 0;
					rect.Y = 0;
					StiDrawing.FillRectangle(imageGraph, Brush, rect);
                    StiCheckBoxGdiPainter.PaintCheck(this, imageGraph, rect, 1);
				}
				return newImage;
			}
		}


		private Color contourColor = Color.Black;
		/// <summary>
		/// Gets or sets a contour color.
		/// </summary>
		[StiCategory("Check")]
		[StiOrder(StiPropertyOrder.CheckContourColor)]
		[StiSerializable()]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[Description("Gets or sets a contour color.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public Color ContourColor
		{
			get 
			{
				return contourColor;
			}
			set 
			{
				contourColor = value;
			}
		}


		private double size = 1d;
		/// <summary>
		/// Gets or sets a contour size.
		/// </summary>
		[StiCategory("Check")]
		[StiOrder(StiPropertyOrder.CheckSize)]
		[StiSerializable()]
		[DefaultValue(1d)]
		[Description("Gets or sets a contour size.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public double Size
		{
			get 
			{
				return size;
			}
			set 
			{
				size = value;
			}
		}



		private StiCheckStyle checkStyle = StiCheckStyle.Check;
		/// <summary>
		/// This property is obsoleted. Please use properties CheckStyleForTrue and CheckStyleForFalse.
		/// </summary>
		[StiNonSerialized]
		[Browsable(false)]
		[Editor("Stimulsoft.Report.Components.Design.StiCheckStyleEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[Obsolete("This property is obsoleted. Please use properties CheckStyleForTrue and CheckStyleForFalse.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public StiCheckStyle CheckStyle
		{
			get 
			{
				return CheckStyleForTrue;
			}
			set 
			{
				if (checkStyle == value)
				{
					CheckStyleForTrue = value;
					CheckStyleForFalse = StiCheckStyle.None;
				}
			}
		}

		private string values = "true/false";
		/// <summary>
		/// Gets or sets string which describes true and false values.
		/// </summary>
		[StiCategory("Check")]
		[StiOrder(StiPropertyOrder.CheckValues)]
		[StiSerializable]
		[DefaultValue("true/false")]
		[Description("Gets or sets string which describes true and false values.")]
		[Editor("Stimulsoft.Report.Components.Design.StiCheckValuesEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiPropertyLevel(StiLevel.Standard)]
		public string Values
		{
			get 
			{
				return values;
			}
			set 
			{
				values = value;
			}
		}


		private StiCheckStyle checkStyleForTrue = StiCheckStyle.Check;
		/// <summary>
		/// Gets or sets check style for true value.
		/// </summary>
		[StiCategory("Check")]
		[StiOrder(StiPropertyOrder.CheckCheckStyleForTrue)]
		[StiSerializable]
		[DefaultValue(StiCheckStyle.Check)]
		[Editor("Stimulsoft.Report.Components.Design.StiCheckStyleEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		[Description("Gets or sets check style for true value.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public StiCheckStyle CheckStyleForTrue
		{
			get 
			{
				return checkStyleForTrue;
			}
			set 
			{
				checkStyleForTrue = value;
			}
		}


		private StiCheckStyle checkStyleForFalse = StiCheckStyle.None;
		/// <summary>
		/// Gets or sets check style for false value.
		/// </summary>
		[StiCategory("Check")]
		[StiOrder(StiPropertyOrder.CheckCheckStyleForFalse)]
		[StiSerializable]
		[DefaultValue(StiCheckStyle.None)]
		[Editor("Stimulsoft.Report.Components.Design.StiCheckStyleEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
		[Description("Gets or sets check style for false value.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public StiCheckStyle CheckStyleForFalse
		{
			get 
			{
				return checkStyleForFalse;
			}
			set 
			{
				checkStyleForFalse = value;
			}
		}

		/// <summary>
		/// Creates a new component of the type StiCheckBox.
		/// </summary>
		public StiCheckBox() : this(RectangleD.Empty)
		{
		}


		/// <summary>
		/// Creates a new component of the type StiCheckBox.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiCheckBox(RectangleD rect) : base(rect)
		{			
			PlaceOnToolbox = false;
		}
		#endregion
	}
}