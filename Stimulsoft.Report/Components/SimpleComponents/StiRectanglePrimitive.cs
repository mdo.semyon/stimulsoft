#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Report.Units;
using Stimulsoft.Report.Painters;
using Stimulsoft.Base.Json.Linq;

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// Describes class that realizes component - StiRectanglePrimitive.
	/// </summary>
	[StiToolbox(true)]
    [StiServiceBitmap(typeof(StiRectanglePrimitive), "Stimulsoft.Report.Images.Components.StiRectanglePrimitive.png")]
	[StiDesigner("Stimulsoft.Report.Components.Design.StiRectanglePrimitiveDesigner, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfDesigner("Stimulsoft.Report.WpfDesign.StiWpfRectanglePrimitiveDesigner, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
    [StiGdiPainter(typeof(StiRectanglePrimitiveGdiPainter))]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiRectanglePrimitiveWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
	public class StiRectanglePrimitive : 
		StiCrossLinePrimitive,
		IStiHideBorderFromDesigner,
		IStiBorder
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiRectanglePrimitive
            jObject.AddPropertyBool("TopSide", TopSide, true);
            jObject.AddPropertyBool("LeftSide", LeftSide, true);
            jObject.AddPropertyBool("BottomSide", BottomSide, true);
            jObject.AddPropertyBool("RightSide", RightSide, true);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "TopSide":
                        this.topSide = property.Value.ToObject<bool>();
                        break;

                    case "LeftSide":
                        this.leftSide = property.Value.ToObject<bool>();
                        break;

                    case "BottomSide":
                        this.bottomSide = property.Value.ToObject<bool>();
                        break;

                    case "RightSide":
                        this.rightSide = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiRectanglePrimitive;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();
            StiPropertyObject[] list;

            // PrimitiveCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Color(),
                    propHelper.SizeFloat(),
                    propHelper.Style()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Color(),
                    propHelper.SizeFloat(),
                    propHelper.Style(),
                    propHelper.TopSide(),
                    propHelper.LeftSide(),
                    propHelper.BottomSide(),
                    propHelper.RightSide()
                };
            }
            objHelper.Add(StiPropertyCategories.Primitive, list);

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height(),
                    propHelper.MinSize(),
                    propHelper.MaxSize()
                };
            }
            objHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            list = new[]
            {
                propHelper.ComponentStyle()
            };
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Enabled()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.Enabled(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.Enabled(),
                    propHelper.Printable(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Name()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias(),
                    propHelper.Restrictions(),
                    propHelper.Locked(),
                    propHelper.Linked()
                };
            }
            objHelper.Add(StiPropertyCategories.Design, list);

            return objHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // ValueEventsCategory
            var list = new[] { StiPropertyEventId.GetToolTipEvent, StiPropertyEventId.GetTagEvent };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            // NavigationEventsCategory
            list = new[] { StiPropertyEventId.GetHyperlinkEvent, StiPropertyEventId.GetBookmarkEvent };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[] { StiPropertyEventId.BeforePrintEvent, StiPropertyEventId.AfterPrintEvent };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // MouseEventsCategory
            list = new[] { StiPropertyEventId.GetDrillDownReportEvent, StiPropertyEventId.ClickEvent, StiPropertyEventId.DoubleClickEvent, 
                StiPropertyEventId.MouseEnterEvent, StiPropertyEventId.MouseLeaveEvent };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties

        public override string HelpUrl
        {
            get
            {
                return "user-manual/getting_started_report_with_cross_primitives.htm?zoom_highlightsub=Rectangle%2BPrimitive";
            }
        }

        #endregion

		#region IStiBorder
		private StiBorder border = null;
		/// <summary>
		/// Gets or sets frame of the component.
		/// </summary>
		[Browsable(false)]
		[StiNonSerialized]
		public StiBorder Border
		{
			get 
			{
				if (border == null)
				{
					border = new StiBorder(
						StiBorderSides.All, this.Color, this.Size, this.Style, false, 0, null);
				}
				return border;
			}
			set 
			{
			}
		}
		#endregion

		#region IStiUnitConvert
		/// <summary>
		/// Converts a component out of one unit into another.
		/// </summary>
		/// <param name="oldUnit">Old units.</param>
		/// <param name="newUnit">New units.</param>
        public override void Convert(StiUnit oldUnit, StiUnit newUnit, bool isReportSnapshot = false)
		{
            if (GetStartPoint() == null && GetEndPoint() == null)
            {
                base.Convert(oldUnit, newUnit, isReportSnapshot);
            }
        }
		#endregion

		#region StiComponent override
		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.RectanglePrimitive;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Shapes;
            }
        }


		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiRectanglePrimitive");
			}
		}
		#endregion

		#region Position
		public override double Width
		{
			get 
			{
				StiPointPrimitive startPoint = GetStartPoint();
				StiPointPrimitive endPoint = GetEndPoint();
				if (startPoint == null || endPoint == null)return base.Width;
				else 
				{
					PointD startPos = new PointD(startPoint.Left, startPoint.Top);
					PointD endPos = new PointD(endPoint.Left, endPoint.Top);

					startPos = startPoint.ComponentToPage(startPos);
					endPos = endPoint.ComponentToPage(endPos);

					return Math.Round(endPos.X - startPos.X, 2);
				}
			}
			set 
			{
				base.Width = value;

				StiPointPrimitive startPoint = GetStartPoint();
				StiPointPrimitive endPoint = GetEndPoint();
				if (startPoint != null && endPoint != null)
				{
					PointD startPos = new PointD(startPoint.Left, startPoint.Top);

					startPos = startPoint.ComponentToPage(startPos);
					PointD endPos = new PointD(startPos.X + value, startPos.Y);
					endPos = endPoint.PageToComponent(endPos);
					endPoint.Left = endPos.X;
				}
			}
		}
		#endregion	

		#region Properties
		private bool topSide = true;
		/// <summary>
        /// Gets os sets property which indicates to draw top side of the rectangle or no.
		/// </summary>
        [StiCategory("Primitive")]
        [StiOrder(StiPropertyOrder.PrimitiveTopSide)]
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets os sets property which indicates to draw top side of the rectangle or no.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool TopSide
		{
			get
			{
				return topSide;
			}
			set
			{
				topSide = value;
			}
		}

		private bool leftSide = true;
		/// <summary>
        /// Gets os sets property which indicates to draw left side of the rectangle or no.
		/// </summary>
        [StiCategory("Primitive")]
        [StiOrder(StiPropertyOrder.PrimitiveLeftSide)]
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets os sets property which indicates to draw left side of the rectangle or no.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool LeftSide
		{
			get
			{
				return leftSide;
			}
			set
			{
				leftSide = value;
			}
		}

		private bool bottomSide = true;
		/// <summary>
        /// Gets os sets property which indicates to draw bottom side of the rectangle or no.
		/// </summary>
        [StiCategory("Primitive")]
        [StiOrder(StiPropertyOrder.PrimitiveBottomSide)]
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets os sets property which indicates to draw bottom side of the rectangle or no.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool BottomSide
		{
			get
			{
				return bottomSide;
			}
			set
			{
				bottomSide = value;
			}
		}

		private bool rightSide = true;
		/// <summary>
        /// Gets os sets property which indicates to draw right side of the rectangle or no.
		/// </summary>
        [StiCategory("Primitive")]
        [StiOrder(StiPropertyOrder.PrimitiveRightSide)]
		[StiSerializable]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets os sets property which indicates to draw right side of the rectangle or no.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual bool RightSide
		{
			get
			{
				return rightSide;
			}
			set
			{
				rightSide = value;
			}
		}
        #endregion

        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiRectanglePrimitive();
        }
        #endregion

        #region this
        /// <summary>
		/// Creates a new StiRectanglePrimitive.
		/// </summary>
		public StiRectanglePrimitive() : this(RectangleD.Empty)
		{
		}


		/// <summary>
		/// Creates a new StiRectanglePrimitive.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiRectanglePrimitive(RectangleD rect) : base(rect)
		{
            PlaceOnToolbox = false;
		}
		#endregion
	}
}