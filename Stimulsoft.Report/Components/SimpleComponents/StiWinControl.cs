#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Reflection;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Design;
using System.Drawing.Imaging;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Components
{
    [StiServiceBitmap(typeof(StiWinControl), "Stimulsoft.Report.Images.Components.StiWinControl.png")]
    [StiGdiPainter(typeof(StiWinControlGdiPainter))]
	[StiV1Builder(typeof(StiWinControlV1Builder))]
	[StiV2Builder(typeof(StiWinControlV2Builder))]
	[StiToolbox(true)]
	[StiContextTool(typeof(IStiShift))]
	[StiContextTool(typeof(IStiGrowToHeight))]
    [StiGuiMode(StiGuiMode.Gdi)]
	public class StiWinControl : 
		StiComponent,
		IStiExportImageExtended		
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // Old
            jObject.RemoveProperty("CanShrink");
            jObject.RemoveProperty("CanGrow");

            // StiWinControl
            jObject.AddPropertyStringNullOrEmpty("Border", StiJsonReportObjectHelper.Serialize.JBorder(Border));
            jObject.AddPropertyStringNullOrEmpty("TypeName", TypeName);
            jObject.AddPropertyString("Text", Text);
            jObject.AddPropertyStringNullOrEmpty("ForeColor", StiJsonReportObjectHelper.Serialize.JColor(ForeColor, Color.Black));
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.Font(Font, "Microsoft Sans Serif", 8));
            jObject.AddPropertyStringNullOrEmpty("BackColor", StiJsonReportObjectHelper.Serialize.JColor(BackColor, SystemColors.Control));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Border":
                        this.border = StiJsonReportObjectHelper.Deserialize.Border(property);
                        break;

                    case "TypeName":
                        this.typeName = property.Value.ToObject<string>();
                        break;

                    case "Text":
                        this.text = property.Value.ToObject<string>();
                        break;

                    case "ForeColor":
                        this.foreColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "Font":
                        this.font = StiJsonReportObjectHelper.Deserialize.Font(property, this.font);
                        break;

                    case "BackColor":
                        this.backColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;
                }
            }
        }

        #endregion
        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiWinControl;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            return null;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            return null;
        }
        #endregion

		#region IStiBorder
		private StiBorder border = new StiBorder();
		/// <summary>
		/// Gets or sets frame of the component.
		/// </summary>
		[StiCategory("Appearance")]
		[StiSerializable]
		[Description("Gets or sets frame of the component.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiBorder Border
		{
			get 
			{
				return border;
			}
			set 
			{
				border = value;
			}
		}
		#endregion

		#region IStiExportImageExtended
		public virtual Image GetImage(ref float zoom)
		{
			return GetImage(ref zoom, StiExportFormat.None);
		}

		public virtual Image GetImage(ref float zoom, StiExportFormat format)
		{
            var painter = StiPainter.GetPainter(this.GetType(), StiGuiMode.Gdi);
            return painter.GetImage(this, ref zoom, format);
		}

		[Browsable(false)]
		public override bool IsExportAsImage(StiExportFormat format)
		{
			return true;
		}
		#endregion

		#region IStiCanShrink override
		[Browsable(false)]
		[StiNonSerialized]
		public override bool CanShrink
		{
			get
			{
				return base.CanShrink;
			}
			set
			{
			}
		}
		#endregion

		#region IStiCanGrow override
		[Browsable(false)]
		[StiNonSerialized]
		public override bool CanGrow
		{
			get
			{
				return base.CanGrow;
			}
			set
			{
			}
		}
		#endregion

		#region StiComponent override
		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.WinControl;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Components;
            }
        }

		/// <summary>
		/// Gets a localized name of the component category.
		/// </summary>
		public override string LocalizedCategory
		{
			get 
			{
				return StiLocalization.Get("Report", "Components");
			}
		}

		
		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiWinControl");
			}
		}
		#endregion

		#region Paint
		public Image GetControlImage()
		{
			if (Page != null && Report != null && Control != null)
			{
				RectangleD rect = Report.Unit.ConvertToHInches(ClientRectangle);
				if (rect.Width > 0 && rect.Height > 0)
				{
					Control.Left = 0;
					Control.Top = 0;
					Control.Width = (int)rect.Width;
					Control.Height = (int)rect.Height;
					Control.Text = Text;
					Control.ForeColor = ForeColor;
					Control.BackColor = BackColor;
					Control.Font = Font;

					var stream = new MemoryStream();
					Image mf = null;
					using (var bmp = new Bitmap(1, 1))
					using (var grfx = Graphics.FromImage(bmp))
					{
						IntPtr ipHdc = grfx.GetHdc();
						mf = new Metafile(stream, ipHdc);
						grfx.ReleaseHdc(ipHdc);
					}

					using (Graphics g = Graphics.FromImage(mf))
					{
						g.Clear(Color.White);
						
						Type controlType = this.control.GetType();
				
						MethodInfo getStyleInfo = controlType.GetMethod("GetStyle", BindingFlags.NonPublic | BindingFlags.Instance);
						object[] args = new object[1] {ControlStyles.DoubleBuffer};
						bool doubleBuffer = (bool) getStyleInfo.Invoke(this.control, args);

						MethodInfo setStyleInfo = controlType.GetMethod("SetStyle", BindingFlags.NonPublic | BindingFlags.Instance);
						args = new object[2] {ControlStyles.DoubleBuffer, false};
						setStyleInfo.Invoke(this.control, args);

						IntPtr ptr = g.GetHdc();
						Message message = Message.Create(this.control.Handle, 15, ptr, IntPtr.Zero);
				
						MethodInfo wndProcInfo = controlType.GetMethod("WndProc", BindingFlags.NonPublic | BindingFlags.Instance);
						args = new object[1] {message} ;
						wndProcInfo.Invoke(this.control, args);
						g.ReleaseHdc(ptr);

						args = new object[2] { ControlStyles.DoubleBuffer, doubleBuffer};
						setStyleInfo.Invoke(this.control, args);
					}
					return mf;
				}	
			}
			return null;
		}

		private void UpdateControl()
		{
			if (this.control != null)
			{
				this.control.Dispose();
				this.control = null;
			}

			if (!string.IsNullOrEmpty(TypeName))
			{
				Type type = StiTypeFinder.GetType(this.TypeName);
			
				if ((type != null))
				{
					this.control = StiActivator.CreateObject(type) as Control;

				}
			}
		} 
		#endregion

		#region Properties
		private string typeName = "";
		/// <summary>
		/// Gets or sets type name.
		/// </summary>
		[Browsable(true)]
		[DefaultValue("")]
		[StiSerializable]
		[StiCategory("WinControl")]
		[StiOrder(StiPropertyOrder.WinControlTypeName)]
		[Description("Gets or sets type name.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public string TypeName
		{
			get
			{
				return typeName;
			}
			set
			{
				typeName = value;
				UpdateControl();
			}
		}


		private Control control = null;
		/// <summary>
		/// Gets or sets control.
		/// </summary>
		[Browsable(false)]
		public Control Control
		{
			get 
			{
				return control;
			}
			set
			{
				control = value;
			}
		}


		private string text = "";
		/// <summary>
		/// Gets or sets the text associated with this control.
		/// </summary>
		[StiSerializable]
		[Browsable(true)]
		[Description("Gets or sets the text associated with this control.")]
		[StiCategory("WinControl")]
		[StiOrder(StiPropertyOrder.WinControlText)]
        [StiPropertyLevel(StiLevel.Basic)]
		public string Text
		{
			get
			{
				return text;
			}
			set
			{				
				text = value;
			}
		}


		private Color foreColor = Color.Black;
		/// <summary>
		/// Gets or sets the foreground color of the control.
		/// </summary>
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[StiCategory("WinControl")]
		[StiOrder(StiPropertyOrder.WinControlForeColor)]
		[Description("Gets or sets the foreground color of the control.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual Color ForeColor
		{
			get 
			{
				return foreColor;
			}
			set 
			{
				foreColor = value;
			}
		}


		private Font font = new Font("Microsoft Sans Serif", 8);
		/// <summary>
		/// Gets or sets the font of the text displayed by the control.
		/// </summary>
		[StiSerializable]
		[StiCategory("WinControl")]
		[StiOrder(StiPropertyOrder.WinControlFont)]
		[Description("Gets or sets the font of the text displayed by the control.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual Font Font
		{
			get
			{
				return font;
			}
			set
			{
				font = value;
				UpdateControl();
			}
		}


		private Color backColor = SystemColors.Control;
		/// <summary>
		/// Gets or sets the background color for the control.
		/// </summary>
		[StiSerializable]
		[StiCategory("WinControl")]
		[StiOrder(StiPropertyOrder.WinControlBackColor)]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[Description("Gets or sets the background color for the control.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual Color BackColor
		{
			get
			{
				return backColor;
			}
			set
			{
				backColor = value;
				UpdateControl();
			}
		}
        #endregion

        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiWinControl();
        }
        #endregion

        #region this
        /// <summary>
		/// Creates a new component of the type StiWinControl.
		/// </summary>
		public StiWinControl() : this(RectangleD.Empty)
		{
		}


		/// <summary>
		/// Creates a new component of the type StiWinControl.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiWinControl(RectangleD rect) : base(rect)
		{
			PlaceOnToolbox = false;
		}
		#endregion
	}
}