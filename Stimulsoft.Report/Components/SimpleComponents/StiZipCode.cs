#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// Class describes the component - ZipCode.
	/// </summary>
    [StiServiceBitmap(typeof(StiZipCode), "Stimulsoft.Report.Images.Components.StiZipCode.png")]
    [StiGdiPainter(typeof(StiZipCodeGdiPainter))]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiZipCodeWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
	[StiToolbox(true)]
	[StiContextTool(typeof(IStiShift))]
	[StiContextTool(typeof(IStiCanGrow))]
	[StiContextTool(typeof(IStiGrowToHeight))]
	[StiContextTool(typeof(IStiCanShrink))]
	public class StiZipCode : 
		StiComponent,
		IStiBorder,
		IStiExportImageExtended,
		IStiTextBrush,
		IStiBrush
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // Old
            jObject.RemoveProperty("CanShrink");
            jObject.RemoveProperty("CanGrow");

            // StiZipCode
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyStringNullOrEmpty("Border", StiJsonReportObjectHelper.Serialize.JBorder(Border));
            jObject.AddPropertyStringNullOrEmpty("TextBrush", StiJsonReportObjectHelper.Serialize.JBrush(TextBrush));
            jObject.AddPropertyStringNullOrEmpty("CodeValue", CodeValue);
            jObject.AddPropertyJObject("Code", Code.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetZipCodeEvent", GetZipCodeEvent.SaveToJsonObject(mode));
            jObject.AddPropertyBool("Ratio", Ratio, true);
            jObject.AddPropertyStringNullOrEmpty("ForeColor", StiJsonReportObjectHelper.Serialize.JColor(ForeColor, Color.Black));
            jObject.AddPropertyDouble("SpaceRatio", SpaceRatio, 4d);
            jObject.AddPropertyBool("UpperMarks", UpperMarks);
            jObject.AddPropertyDouble("Size", Size, 1d);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Brush":
                        this.brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "Border":
                        this.border = StiJsonReportObjectHelper.Deserialize.Border(property);
                        break;

                    case "TextBrush":
                        this.textBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "CodeValue":
                        this.codeValue = property.Value.ToObject<string>();
                        break;

                    case "Code":
                        {
                            var _expression = new StiZipCodeExpression();
                            _expression.LoadFromJsonObject((JObject)property.Value);
                            this.Code = _expression;
                        }
                        break;

                    case "GetZipCodeEvent":
                        {
                            var _event = new StiGetZipCodeEvent();
                            _event.LoadFromJsonObject((JObject)property.Value);
                            this.GetZipCodeEvent = _event;
                        }
                        break;

                    case "Ratio":
                        this.ratio = property.Value.ToObject<bool>();
                        break;

                    case "ForeColor":
                        this.foreColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "SpaceRatio":
                        this.spaceRatio = property.Value.ToObject<double>();
                        break;

                    case "UpperMarks":
                        this.upperMarks = property.Value.ToObject<bool>();
                        break;

                    case "Size":
                        this.size = property.Value.ToObject<double>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiZipCode;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            // ColumnsCategory
            var list = new[] 
            { 
                propHelper.Code(), 
                propHelper.Size(), 
                propHelper.Ratio(), 
                propHelper.ForeColor()
            };
            objHelper.Add(StiPropertyCategories.ZipCode, list);

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Width(),
                    propHelper.Height(),
                    propHelper.MinSize(),
                    propHelper.MaxSize()
                };
            }
            objHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Brush(),
                    propHelper.Border(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Brush(),
                    propHelper.Border(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle(),
                    propHelper.UseParentStyles()
                };
            }
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.GrowToHeight(),
                    propHelper.Enabled()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.GrowToHeight(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.GrowToHeight(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.Printable(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Name()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias(),
                    propHelper.Restrictions(),
                    propHelper.Locked(),
                    propHelper.Linked()
                };
            }
            objHelper.Add(StiPropertyCategories.Design, list);

            return objHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // ValueEventsCategory
            var list = new[] { StiPropertyEventId.GetZipCodeEvent, StiPropertyEventId.GetToolTipEvent, StiPropertyEventId.GetTagEvent };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            // NavigationEventsCategory
            list = new[] { StiPropertyEventId.GetHyperlinkEvent, StiPropertyEventId.GetBookmarkEvent };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[] { StiPropertyEventId.BeforePrintEvent, StiPropertyEventId.AfterPrintEvent };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // MouseEventsCategory
            list = new[] { StiPropertyEventId.GetDrillDownReportEvent, StiPropertyEventId.ClickEvent, StiPropertyEventId.DoubleClickEvent, 
                StiPropertyEventId.MouseEnterEvent, StiPropertyEventId.MouseLeaveEvent };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties

        public override string HelpUrl
        {
            get
            {
                return "User-Manual/report_internals_output_text_parameters_zip_code.htm";
            }
        }

        #endregion

		#region IStiExportImageExtended
		public virtual Image GetImage(ref float zoom)
		{
			return GetImage(ref zoom, StiExportFormat.None);
		}

		public virtual Image GetImage(ref float zoom, StiExportFormat format)
		{
            StiPainter painter = StiPainter.GetPainter(this.GetType(), StiGuiMode.Gdi);
            return painter.GetImage(this, ref zoom, format);
		}

		[Browsable(false)]
		public override bool IsExportAsImage(StiExportFormat format)
		{
			return true;
		}
		#endregion

		#region IStiCanShrink override
		[Browsable(false)]
		[StiNonSerialized]
		public override bool CanShrink
		{
			get
			{
				return base.CanShrink;
			}
			set
			{
			}
		}
		#endregion

		#region IStiCanGrow override
		[Browsable(false)]
		[StiNonSerialized]
		public override bool CanGrow
		{
			get
			{
				return base.CanGrow;
			}
			set
			{
			}
		}
		#endregion

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public override object Clone(bool cloneProperties)
		{
			StiZipCode zipCode = (StiZipCode) base.Clone(cloneProperties);
			
			if (this.textBrush != null)zipCode.textBrush = (StiBrush) this.textBrush.Clone();
			else zipCode.textBrush = null;

			return zipCode;
		}
		#endregion

		#region IStiBrush
		private StiBrush brush = new StiSolidBrush();
		/// <summary>
		/// Gets or sets a brush to fill a component.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.AppearanceBrush)]
		[StiSerializable]
		[Description("Gets or sets a brush to fill a component.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiBrush Brush
		{
			get 
			{
				return brush;
			}
			set 
			{
				brush = value;
			}
		}
		#endregion

		#region IStiBorder
		private StiBorder border = new StiBorder();
		/// <summary>
		/// Gets or sets border of the component.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.AppearanceBorder)]
		[StiSerializable]
		[Description("Gets or sets border of the component.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiBorder Border
		{
			get 
			{
				return border;
			}
			set 
			{
				border = value;
			}
		}
		#endregion
		
		#region IStiTextBrush
		private StiBrush textBrush = new StiSolidBrush(Color.Black);
		/// <summary>
		/// Gets or sets a brush to draw text.
		/// </summary>
		[StiCategory("ZipCode")]
		[StiOrder(StiPropertyOrder.ZipCodeTextBrush)]
		[StiSerializable]
		[Description("Gets or sets a brush to draw text.")]
        [StiPropertyLevel(StiLevel.Basic)]
        [Browsable(false)]
		public StiBrush TextBrush
		{
			get 
			{
				return textBrush;
			}
			set 
			{
				textBrush = value;
			}
		}
		
		#endregion		

		#region CalculateRect
		/// <summary>
		/// Calculate Rectangle
		/// </summary>
		public RectangleD CalculateRect(RectangleD rect, int count, int index, out RectangleD markRect)
		{
            double module = 0;
			double width = 0;
			double border = 0;
			double height = 0;
			double x = 0;
			double y = 0;

            if (StiOptions.Engine.UseOldZipCodePaintMode)
            {
                if (!ratio)
                {
                    width = (5 * rect.Width) / (6 * count - 1);
                    border = width / 5;
                    height = rect.Height;

                    x = rect.X + border * index + width * index;
                    y = rect.Y;
                }
                else
                {
                    border = rect.Height / 10;
                    height = rect.Height;
                    width = height / 2;

                    x = rect.X + border * index + width * index;
                    y = rect.Y;
                }
                markRect = new RectangleD(0, 0, 0, 0);
            }
            else
            {
                if (!ratio)
                {
                    module = rect.Height / (12 + (upperMarks ? 3 : 0));
                    double moduleX = rect.Width / ((5 + spaceRatio) * count - spaceRatio + 2);
                    width = moduleX * 5;
                    border = moduleX * spaceRatio;
                    height = module * 10;

                    x = rect.X + moduleX + (width + border) * index;
                    y = rect.Y + module * (upperMarks ? 4 : 1);

                    markRect = new RectangleD(x - moduleX, y - module * 4, moduleX * 7, module * 2);
                }
                else
                {
                    module = rect.Height / (12 + (upperMarks ? 3 : 0));
                    height = module * 10;
                    width = module * 5;
                    border = module * spaceRatio;

                    x = rect.X + module + (width + border) * index;
                    y = rect.Y + module * (upperMarks ? 4 : 1);

                    markRect = new RectangleD(x - module, y - module * 4, module * 7, module * 2);
                }
            }

			RectangleD rectSpace = new RectangleD(x, y, width, height);
			return rectSpace;
		}
		#endregion		

		#region StiComponent override
		/// <summary>
		/// Return events collection of this component.
		/// </summary>
		public override StiEventsCollection GetEvents()
		{
			StiEventsCollection events = base.GetEvents();
			return events;
		}


		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.ZipCode;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Components;
            }
        }


		/// <summary>
		/// Gets a localized name of the component category.
		/// </summary>
		public override string LocalizedCategory
		{
			get 
			{
				return StiLocalization.Get("Report", "Components");
			}
		}

	
		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiZipCode");
			}
		}
		#endregion		

		#region Expressions
		#region ZipCode expression

		private string codeValue = null;
		/// <summary>
		/// Gets or sets the component zip code.
		/// </summary>
		[Browsable(false)]
		[StiSerializable(StiSerializeTypes.SerializeToDocument)]
		[Description("Gets or sets the component zip code.")]
		public string CodeValue
		{
			get
			{
				return codeValue;
			}
			set
			{
				codeValue = value;
			}
		}


		/// <summary>
		/// Gets or sets the expression to fill a code of zip code.
		/// </summary>
		[StiCategory("ZipCode")]
		[StiOrder(StiPropertyOrder.ZipCodeCode)]
		[StiSerializable(
			 StiSerializeTypes.SerializeToCode |
			 StiSerializeTypes.SerializeToDesigner |
			 StiSerializeTypes.SerializeToSaveLoad)]
		[Description("Gets or sets the expression to fill a code of zip code.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual StiZipCodeExpression Code
		{
			get
			{
				return new StiZipCodeExpression(this, "Code");
			}
			set
			{
				if (value != null)value.Set(this, "Code", value.Value);
			}
		}
		#endregion
		#endregion

		#region Events
		/// <summary>
		/// Invokes all events for this components.
		/// </summary>
		public override void InvokeEvents()
		{
			base.InvokeEvents();
			try
			{
				#region Code
                if (this.Events[EventGetZipCode] != null)
                {
                    StiValueEventArgs e = new StiValueEventArgs();
                    InvokeGetZipCode(this, e);
                    if (e.Value != null) this.codeValue = e.Value.ToString();
                }
				#endregion
			}
			catch (Exception e)
			{
				StiLogService.Write(this.GetType(), "DoEvents...ERROR");
				StiLogService.Write(this.GetType(), e);

                if (Report != null)
                    Report.WriteToReportRenderingMessages(this.Name + " " + e.Message);
			}
		}


		#region GetZipCode
		private static readonly object EventGetZipCode = new object();

		/// <summary>
		/// Occurs when getting the code of zipcode.
		/// </summary>
		public event StiValueEventHandler GetZipCode
		{
			add
			{
				base.Events.AddHandler(EventGetZipCode, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventGetZipCode, value);
			}
		}

		/// <summary>
		/// Raises the ZipCode event.
		/// </summary>
		protected virtual void OnGetZipCode(StiValueEventArgs e)
		{
		}
		

		/// <summary>
		/// Raises the GetZipCode event.
		/// </summary>
		public void InvokeGetZipCode(StiComponent sender, StiValueEventArgs e)
		{
			try
			{
				OnGetZipCode(e);
				StiValueEventHandler handler = base.Events[EventGetZipCode] as StiValueEventHandler;
				if (handler != null)handler(sender, e);
			}
			catch (Exception ex)
			{
				string str = string.Format("Expression in ZipCode property of '{0}' can't be evaluated!", this.Name);
				StiLogService.Write(this.GetType(), str);
				StiLogService.Write(this.GetType(), ex.Message);
				Report.WriteToReportRenderingMessages(str);
			}
		}
		

		/// <summary>
		/// Occurs when getting the code of zip code.
		/// </summary>
		[StiSerializable]
		[StiCategory("ValueEvents")]
		[Browsable(false)]
		[Description("Occurs when getting the code of zip code.")]
		public StiGetZipCodeEvent GetZipCodeEvent
		{
			get
			{				
				return new StiGetZipCodeEvent(this);
			}
			set
			{
				if (value != null)value.Set(this, value.Script);
			}
		}
		#endregion
		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets the default client area of a component.
		/// </summary>
		[Browsable(false)]
		public override RectangleD DefaultClientRectangle
		{
			get
			{
				return new RectangleD(0, 0, 200, 32);
			}
		}


		private bool ratio = true;
		/// <summary>
		/// Get or sets value, which indicates width and height ratio.
		/// </summary>
		[StiCategory("ZipCode")]
		[StiOrder(StiPropertyOrder.ZipCodeRatio)]
		[StiSerializable()]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Get or sets value, which indicates width and height ratio.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public bool Ratio
		{
			get
			{
				return ratio;
			}
			set
			{
				ratio = value;
			}
		}


		private Color foreColor = Color.Black;
		/// <summary>
		/// Gets or sets a fore color.
		/// </summary>
		[StiCategory("ZipCode")]
		[StiOrder(StiPropertyOrder.ZipCodeForeColor)]
		[StiSerializable()]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[Description("Gets or sets a fore color.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public Color ForeColor
		{
			get 
			{
				return foreColor;
			}
			set 
			{
				foreColor = value;
			}
		}


		private double spaceRatio = 4d;
		/// <summary>
		/// Gets or sets a space ratio.
		/// </summary>
		[StiCategory("ZipCode")]
		[StiOrder(StiPropertyOrder.ZipCodeSpaceRatio)]
		[StiSerializable]
		[DefaultValue(4d)]
        [Description("Gets or sets a space ratio.")]
        [StiPropertyLevel(StiLevel.Standard)]
        public double SpaceRatio
		{
			get 
			{
                return spaceRatio;
			}
			set 
			{
                spaceRatio = value;
			}
		}


        private bool upperMarks = false;
        /// <summary>
        /// Gets or sets a value which indicates whether it is necessary to draw the upper marks.
        /// </summary>
        [StiCategory("ZipCode")]
        [StiOrder(StiPropertyOrder.ZipCodeUpperMarks)]
        [StiSerializable]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether it is necessary to draw the upper marks.")]
        [StiPropertyLevel(StiLevel.Basic)]
        public bool UpperMarks
        {
            get
            {
                return upperMarks;
            }
            set
            {
                upperMarks = value;
            }
        }


        private double size = 1d;
        /// <summary>
        /// Gets or sets a contour size.
        /// </summary>
        [StiCategory("ZipCode")]
        [StiOrder(StiPropertyOrder.ZipCodeSize)]
        [StiSerializable()]
        [DefaultValue(1d)]
        [Description("Gets or sets a contour size.")]
        [StiPropertyLevel(StiLevel.Basic)]
        public double Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }
        #endregion

        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiZipCode();
        }
        #endregion

        #region this
        /// <summary>
		/// Creates a new component of the type StiZipCode.
		/// </summary>
		public StiZipCode() : this(RectangleD.Empty)
		{
		}


		/// <summary>
		/// Creates a new component of the type StiZipCode.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiZipCode(RectangleD rect) : base(rect)
		{			
			PlaceOnToolbox = false;
			Code.Value = "1234567890";
            if (StiOptions.Engine.UseOldZipCodePaintMode) SpaceRatio = 1;
		}
		#endregion
	}
}