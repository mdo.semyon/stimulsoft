#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.Drawing.Design;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Units;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.QuickButtons;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.Controls;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Helpers;
using System.Collections;

#if NETCORE
using Stimulsoft.System;
using Stimulsoft.System.Drawing.Design;
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// The class describes a component that allows to outtype RichText.
	/// </summary>
    [StiServiceBitmap(typeof(StiRichText), "Stimulsoft.Report.Images.Components.StiRichText.png")]
	[StiToolbox(true)]
	[StiContextTool(typeof(IStiCanGrow))]
	[StiContextTool(typeof(IStiCanShrink))]
	[StiContextTool(typeof(IStiShift))]
	[StiContextTool(typeof(IStiEditable))]
	[StiContextTool(typeof(IStiGrowToHeight))]
	[StiContextTool(typeof(IStiComponentDesigner))]
	[StiContextTool(typeof(IStiBreakable))]
	[StiContextTool(typeof(IStiOnlyText))]
	[StiContextTool(typeof(IStiTextOptions))]
	[StiDesigner("Stimulsoft.Report.Components.Design.StiRichTextDesigner, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfDesigner("Stimulsoft.Report.WpfDesign.StiWpfRichTextDesigner, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
    [StiGdiPainter(typeof(Stimulsoft.Report.Painters.StiRichTextGdiPainter))]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiRichTextWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
	[StiQuickButton("Stimulsoft.Report.QuickButtons.Design.StiRichTextQuickButton, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfQuickButton("Stimulsoft.Report.WpfDesign.StiWpfRichTextQuickButton, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
	public class StiRichText : 
		StiSimpleText,
		IStiExportImageExtended,
		IStiBreakable,
		IStiBorder,
        IStiGlobalizationProvider,
        IStiBackColor
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // Old
            jObject.RemoveProperty("GlobalizedName");
            jObject.RemoveProperty("LinesOfUnderline");
            jObject.RemoveProperty("HideZeros");
            jObject.RemoveProperty("ProcessingDuplicates");
            jObject.RemoveProperty("MaxNumberOfLines");
            

            // StiRichText
            jObject.AddPropertyBool("CanBreak", CanBreak, false);
            jObject.AddPropertyStringNullOrEmpty("Border", StiJsonReportObjectHelper.Serialize.JBorder(Border));
            jObject.AddPropertyJObject("GetDataUrlEvent", GetDataUrlEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("Margins", Margins.SaveToJsonObject(mode, 0, 0, 0, 0));
            jObject.AddPropertyBool("WordWrap", WordWrap, true);
            jObject.AddPropertyBool("DetectUrls", DetectUrls, true);
            jObject.AddPropertyStringNullOrEmpty("BackColor", StiJsonReportObjectHelper.Serialize.JColor(BackColor, Color.White));
            jObject.AddPropertyStringNullOrEmpty("DataColumn", DataColumn);
            jObject.AddPropertyJObject("DataUrl", DataUrl.SaveToJsonObject(mode));
            jObject.AddPropertyBool("FullConvertExpression", FullConvertExpression);
            jObject.AddPropertyBool("Wysiwyg", Wysiwyg);
            jObject.AddPropertyBool("RightToLeft", RightToLeft);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "CanBreak":
                        this.CanBreak = property.Value.ToObject<bool>();
                        break;

                    case "Border":
                        this.Border = StiJsonReportObjectHelper.Deserialize.Border(property);
                        break;

                    case "GetDataUrlEvent":
                        {
                            var _event = new StiGetDataUrlEvent();
                            _event.LoadFromJsonObject((JObject)property.Value);
                            this.GetDataUrlEvent = _event;
                        }
                        break;

                    case "Margins":
                        this.margins.LoadFromJsonObject((JObject)property.Value);
                        break;

                    case "WordWrap":
                        this.wordWrap = property.Value.ToObject<bool>();
                        break;

                    case "DetectUrls":
                        this.detectUrls = property.Value.ToObject<bool>();
                        break;

                    case "BackColor":
                        this.backColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "DataColumn":
                        this.dataColumn = property.Value.ToObject<string>();
                        break;

                    case "DataUrl":
                        {
                            var _expression = new StiDataUrlExpression();
                            _expression.LoadFromJsonObject((JObject)property.Value);
                            this.DataUrl = _expression;
                        }
                        break;

                    case "FullConvertExpression":
                        this.fullConvertExpression = property.Value.ToObject<bool>();
                        break;

                    case "Wysiwyg":
                        this.wysiwyg = property.Value.ToObject<bool>();
                        break;

                    case "RightToLeft":
                        this.rightToLeft = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiRichText;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[]
            {
                propHelper.RichTextDesign()
            };
            objHelper.Add(StiPropertyCategories.ComponentEditor, list);

            // TextAdditionalCategory
            if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.DetectUrls(),
                    propHelper.Editable(),
                    propHelper.Margins(),
                    propHelper.BackColor(),
                    propHelper.OnlyText(),
                    propHelper.WordWrap()
                };
                objHelper.Add(StiPropertyCategories.TextAdditional, list);
            }
            else
            {
                list = new[]
                {
                    propHelper.DetectUrls(),
                    propHelper.Editable(),
                    propHelper.FullConvertExpression(),
                    propHelper.Margins(),
                    propHelper.BackColor(),
                    propHelper.OnlyText(),
                    propHelper.ProcessAt(),
                    propHelper.WordWrap(),
                    propHelper.Wysiwyg(),
                };
                objHelper.Add(StiPropertyCategories.TextAdditional, list);
            }

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.Left(), 
                    propHelper.Top(), 
                    propHelper.Width(), 
                    propHelper.Height() 
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.Left(), 
                    propHelper.Top(), 
                    propHelper.Width(), 
                    propHelper.Height(), 
                    propHelper.MinSize(), 
                    propHelper.MaxSize() 
                };
            }
            objHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            if (level != StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Border(),
                    propHelper.Conditions(),
                    propHelper.ComponentStyle(),
                    propHelper.UseParentStyles()
                };
                objHelper.Add(StiPropertyCategories.Appearance, list);
            }

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.CanGrow(),
                    propHelper.CanShrink(),
                    propHelper.GrowToHeight(),
                    propHelper.CanBreak(),
                    propHelper.Enabled()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.CanGrow(),
                    propHelper.CanShrink(),
                    propHelper.GrowToHeight(),
                    propHelper.CanBreak(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.CanGrow(),
                    propHelper.CanShrink(),
                    propHelper.GrowToHeight(),
                    propHelper.CanBreak(),
                    propHelper.DockStyle(),
                    propHelper.Enabled(),
                    propHelper.Printable(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Name()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias(),
                    propHelper.Restrictions(),
                    propHelper.Locked(),
                    propHelper.Linked()
                };
            }
            objHelper.Add(StiPropertyCategories.Design, list);

            return objHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // MouseEventsCategory
            var list = new[]
            {
                StiPropertyEventId.ClickEvent, StiPropertyEventId.DoubleClickEvent, StiPropertyEventId.GetDrillDownReportEvent,
                StiPropertyEventId.MouseEnterEvent, StiPropertyEventId.MouseLeaveEvent
            };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            // NavigationEventsCategory
            list = new[] { StiPropertyEventId.GetHyperlinkEvent, StiPropertyEventId.GetBookmarkEvent };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[] { StiPropertyEventId.BeforePrintEvent, StiPropertyEventId.AfterPrintEvent };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // ValueEventsCategory
            list = new[] { StiPropertyEventId.GetTagEvent, StiPropertyEventId.GetToolTipEvent, StiPropertyEventId.GetValueEvent };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties

        public override string HelpUrl
        {
            get
            {
                return "user-manual/report_internals_rich_text_output.htm";
            }
        }

        #endregion

		#region IStiBreakable
        protected static object PropertyCanBreak = new object();
		/// <summary>
		/// Gets or sets value which indicates whether the component can or cannot break its contents on several pages.
		/// </summary>
		[DefaultValue(false)]
		[StiSerializable]
		[StiCategory("Behavior")]
		[StiOrder(StiPropertyOrder.BehaviorCanBreak)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value which indicates whether the component can or cannot break its contents on several pages.")]
        [StiShowInContextMenu]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual bool CanBreak
		{
            get
            {
                return Properties.GetBool(PropertyCanBreak, false);
            }
            set
            {
                Properties.SetBool(PropertyCanBreak, value, false);
            }
		}	


		/// <summary>
		/// Divides content of components in two parts. Returns result of dividing. If true, then component is successful divided.
		/// </summary>
		/// <param name="dividedComponent">Component for store part of content.</param>
		/// <returns>If true, then component is successful divided.</returns>
        public bool Break(StiComponent dividedComponent, double devideFactor, ref double divideLine)
		{
            divideLine = 0;

            //if (this.GetTextInternal() == null || this.GetTextInternal().Length == 0) return false;
            if (string.IsNullOrEmpty(this.GetTextInternal())) return true;  //fix

            if (this.Report.IsWpf)
            {
                Type type = Type.GetType("Stimulsoft.Report.Wpf.StiWpfBreakTextHelper, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo);
                if (type == null)
                    throw new Exception("Assembly 'Stimulsoft.Report.Wpf' is not found");

                IStiWpfBreakTextHelper helper = StiActivator.CreateObject(type, new object[0]) as IStiWpfBreakTextHelper;

                AppDomain domain = null;
                if (StiOptions.Engine.RenderRichTextInOtherDomain)
                {
                    if ((dividedComponent.Report != null) && (dividedComponent.Report.WpfRichTextDomain != null))
                    {
                        domain = dividedComponent.Report.WpfRichTextDomain;
                    }
                    if (domain == null)
                    {
                        AppDomainSetup appDomainSetup = new AppDomainSetup();
                        appDomainSetup.ShadowCopyFiles = "false";
                        appDomainSetup.ApplicationBase = AppDomain.CurrentDomain.BaseDirectory;
#if NETCORE
                        domain = AppDomain.CreateDomain(Stimulsoft.Base.StiGuidUtils.NewGuid());
#else
                        domain = AppDomain.CreateDomain(Stimulsoft.Base.StiGuidUtils.NewGuid(), AppDomain.CurrentDomain.Evidence, appDomainSetup);
#endif
                    }
                    if (dividedComponent.Report != null)
                    {
                        dividedComponent.Report.WpfRichTextDomain = domain;
                    }
                }

                return helper.BreakRichText(this, dividedComponent, devideFactor, ref divideLine, domain);
            }

			//RectangleD rect = GetPaintRectangle(true, false);
            RectangleD rect = Report.Unit.ConvertToHInches(this.ClientRectangle);

            if (rect.Width < 0) rect.Width = Math.Abs(rect.Width);  //fix infinity loops in some cases

            rect = ConvertTextMargins(rect, false);

			int charEnd = 0;

			#region	Calculate count of char measured in rectangle
			bool fail = true;
			int failCount = 0;

			while (fail && failCount < 100)
			{
				fail = false;
				try
				{
					using (StiRichTextBox richTextBox = new StiRichTextBox(false))
					{
                        richTextBox.WordWrap = this.WordWrap;
                        richTextBox.DetectUrls = this.DetectUrls;
                        if (this.RightToLeft) richTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;

						GetPreparedText(richTextBox);							
		
						if (richTextBox.TextLength > 0)
						{
                            int rectWidth = (int)rect.Width;
                            if (!this.WordWrap) rectWidth = 100000;
                            if (Wysiwyg || StiDpiHelper.NeedGraphicsRichTextScale)
                            {
                                lock (StiReport.GlobalRichTextMeasureGraphics)
                                {
                                    StiRichTextHelper.FormatRange(StiRtfFormatType.MeasureRtf, richTextBox,
                                        new Rectangle(0, 0, rectWidth, (int)rect.Height),
                                        StiReport.GlobalRichTextMeasureGraphics, 0, -1, out charEnd);
                                }
                            }
                            else
                            {
                                using (Graphics measureGraph = Graphics.FromHwnd(IntPtr.Zero))
                                {
                                    StiRichTextHelper.FormatRange(StiRtfFormatType.MeasureRtf, richTextBox,
                                        new Rectangle(0, 0, rectWidth, (int)rect.Height),
                                        measureGraph, 0, -1, out charEnd);
                                }
                            }
						}
			
					}
				}
				catch (Exception e)
				{
					fail = true;
					failCount ++;

                    if (Report != null)
                        Report.WriteToReportRenderingMessages(this.Name + " " + e.Message);
				}
			}
			#endregion

            if (charEnd == -1) charEnd = 0;

			#region Break richtext
			using (StiRichTextBox rich = new StiRichTextBox(false))
			{
				#region Check height of first line
                if (this.RtfText != null && this.RtfText.TrimStart().ToLowerInvariant().IndexOf("rtf", StringComparison.InvariantCulture) != -1)
                {
                    //rich.Rtf = this.RtfText;
                    rich.Rtf = Export.StiExportUtils.CorrectRichTextForRiched20(this.RtfText);
                }
                else
                {
                    rich.Text = this.GetTextInternal();
                    if (StiOptions.Engine.DefaultRtfFont != null)
                    {
                        rich.SelectAll();
                        rich.SelectionFont = StiOptions.Engine.DefaultRtfFont;
                    }
                    else if (DefaultFont != null)
                    {
                        rich.SelectAll();
                        rich.SelectionFont = DefaultFont;
                    }

                    if (!DefaultColor.IsEmpty)
                    {
                        rich.SelectAll();
                        rich.SelectionColor = DefaultColor;
                    }
                }

				rich.SelectionStart = 0;
				rich.SelectionLength = charEnd;
                rich.WordWrap = this.WordWrap;
                rich.DetectUrls = this.DetectUrls;
                if (this.RightToLeft) rich.RightToLeft = System.Windows.Forms.RightToLeft.Yes;

				fail = true;
				failCount = 0;

				rich.Rtf = rich.SelectedRtf;
				
				while (fail && failCount < 100)
				{
					fail = false;
					try
					{
                        int rectWidth = (int)rect.Width;
                        if (!this.WordWrap) rectWidth = 100000;
                        int heightRtf = 0;
                        if (Wysiwyg || StiDpiHelper.NeedGraphicsRichTextScale)
                        {
                            lock (StiReport.GlobalRichTextMeasureGraphics)
                            {
                                int tempCharEnd = 0;
                                heightRtf = StiRichTextHelper.FormatRange(StiRtfFormatType.MeasureRtf, rich,
                                    new Rectangle(0, 0, rectWidth, (int)rect.Height),
                                    StiReport.GlobalRichTextMeasureGraphics, 0, -1, out tempCharEnd);
                            }
                        }
                        else
                        {
                            using (Graphics measureGraph = Graphics.FromHwnd(IntPtr.Zero))
                            {
                                int tempCharEnd = 0;
                                heightRtf = StiRichTextHelper.FormatRange(StiRtfFormatType.MeasureRtf, rich,
                                    new Rectangle(0, 0, rectWidth, (int)rect.Height),
                                    measureGraph, 0, -1, out tempCharEnd);
                            }
                        }
                        if (heightRtf > (int)rect.Height && (!StiOptions.Engine.ForceRtfBreak))
                        {
                            if (this.GrowToHeight)
                            {
                                this.Text = string.Empty;
                                return true;
                            }
                            else
                                return false;
                        }
					}
					catch (Exception e)
					{
                        if (Report != null)
                            Report.WriteToReportRenderingMessages(this.Name + " " + e.Message);
					}
				}				
				#endregion				

                if (this.RtfText != null && this.RtfText.TrimStart().ToLowerInvariant().IndexOf("rtf", StringComparison.InvariantCulture) != -1)
					//rich.Rtf = this.RtfText;
                    rich.Rtf = Export.StiExportUtils.CorrectRichTextForRiched20(this.RtfText);
				else
				{
					rich.Text = this.GetTextInternal();

					if (StiOptions.Engine.DefaultRtfFont != null)
					{
						rich.SelectAll();
						rich.SelectionFont = StiOptions.Engine.DefaultRtfFont;
					}
					else if (DefaultFont != null)
					{
						rich.SelectAll();
						rich.SelectionFont = DefaultFont;
					}

					if (!DefaultColor.IsEmpty)
					{
						rich.SelectAll();
						rich.SelectionColor = DefaultColor;
					}
				}

				rich.SelectionStart = 0;
				rich.SelectionLength = charEnd;

				if (StiOptions.Engine.RtfCache.Enabled)
				{
					this.NewGuid();
				}
				this.RtfText = rich.SelectedRtf;

                //���� � ����� ������ ���� ������ ������, �� ������ ��������� ����� ������ ����������� ������ ��� ����,
                //����� ������, ��� ���� ����� ���������� � ������������ ���������,
                //����������� ��� �������, � ��� ������������ ���������� ������ BreakContainer ���������� ����������� ������������,
                //������� ��� ������ ���� ����������������
                //
                //if (!this.GrowToHeight)
                //{
                //    if (rich.TextLength - charEnd == 0) return false;
                //}

				rich.SelectionStart = charEnd;
				rich.SelectionLength = Math.Max(rich.TextLength - charEnd, 0);

				if (StiOptions.Engine.RtfCache.Enabled)
				{
					dividedComponent.NewGuid();
				}
				((StiRichText)dividedComponent).RtfText = rich.SelectedRtf;

				return true;					
			}
			#endregion
		}
		#endregion		

		#region ICloneable override
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public override object Clone(bool cloneProperties)
		{
			StiRichText text =	(StiRichText)base.Clone(cloneProperties);
			
			if (this.border != null)text.border = (StiBorder)this.border.Clone();
			else text.border = null;

			return text;
		}
		#endregion

        #region IStiGlobalizationProvider
        /// <summary>
        /// Sets localized string to specified property name.
        /// </summary>
        void IStiGlobalizationProvider.SetString(string propertyName, string value)
        {
            if (propertyName == "Text") SetTextInternal(value);
            else if (propertyName == "ToolTip") this.ToolTip.Value = value;
            else if (propertyName == "Tag") this.Tag.Value = value;
            else if (propertyName == "Hyperlink") this.Hyperlink.Value = value;
            else throw new ArgumentException(string.Format("Property with name {0}", propertyName));
        }

        /// <summary>
        /// Gets localized string from specified property name.
        /// </summary>
        string IStiGlobalizationProvider.GetString(string propertyName)
        {
            if (propertyName == "Text") return GetTextInternal();
            if (propertyName == "Tag") return this.Tag.Value;
            if (propertyName == "ToolTip") return this.ToolTip.Value;
            if (propertyName == "Hyperlink") return this.Hyperlink.Value;
            throw new ArgumentException(string.Format("Property with name {0}", propertyName));
        }

        /// <summary>
        /// Returns array of the property names which can be localized.
        /// </summary>
        string[] IStiGlobalizationProvider.GetAllStrings()
        {
            List<string> strs = new List<string>();
            if (StiOptions.Engine.Globalization.AllowUseText)
                strs.Add("Text");
            if (StiOptions.Engine.Globalization.AllowUseTag)
                strs.Add("Tag");
            if (StiOptions.Engine.Globalization.AllowUseToolTip)
                strs.Add("ToolTip");
            if (StiOptions.Engine.Globalization.AllowUseHyperlink)
                strs.Add("Hyperlink");

            return strs.ToArray();
        }
        #endregion

		#region IStiBorder
		private StiBorder border = new StiBorder();
		/// <summary>
		/// Gets or sets frame of the component.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.AppearanceBorder)]
		[StiSerializable]
		[Description("Gets or sets frame of the component.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public StiBorder Border
		{
			get 
			{
				return border;
			}
			set 
			{
				border = value;
			}
		}
		#endregion

		#region IStiExportImageExtended
		public virtual Image GetImage(ref float zoom)
		{
			return GetImage(ref zoom, StiExportFormat.None);
		}

		public virtual Image GetImage(ref float zoom, StiExportFormat format)
		{
            bool flag = image == null;

            StiGuiMode guiMode;
            if (Report != null)
            {
                if (StiOptions.Configuration.IsXbap)
                    guiMode = StiGuiMode.Xbap;
                else
                {
                    guiMode = (Report.IsWpf || Report.RenderedWith == StiRenderedWith.Wpf ? StiGuiMode.Wpf : StiGuiMode.Gdi);
                }
            }
            else
            {
                if (StiOptions.Configuration.IsXbap)
                    guiMode = StiGuiMode.Xbap;
                else
                {
                    guiMode = (StiOptions.Configuration.IsWPF ? StiGuiMode.Wpf : StiGuiMode.Gdi);
                }
            }

            StiPainter painter = StiPainter.GetPainter(this.GetType(), guiMode);
            Image tempImage = painter.GetImage(this, ref zoom, format);

            if (flag)
            {
                if (image != null) image.Dispose();
                ResetImage();
            }

            return tempImage;
		}

		[Browsable(false)]
		public override bool IsExportAsImage(StiExportFormat format)
		{
            if ((format == StiExportFormat.Text) || 
                (format == StiExportFormat.Rtf) ||
                (format == StiExportFormat.RtfFrame) ||
                (format == StiExportFormat.RtfTable) ||
                (format == StiExportFormat.RtfWinWord)) return false;
			return true;
		}
		#endregion

		#region IStiText browsable(false)
		[Browsable(false)]
		[StiNonSerialized]
		public sealed override string GlobalizedName
		{
			get 
			{
				return base.GlobalizedName;
			}
			set 
			{
				base.GlobalizedName = value;
			}
		}


		[Browsable(false)]
		[StiNonSerialized]
		public override StiPenStyle LinesOfUnderline
		{
			get
			{
				return base.LinesOfUnderline;
			}
			set
			{
			}
		}


		[StiNonSerialized]
		[Browsable(false)]
		public override bool HideZeros
		{
			get
			{
				return false;
			}
			set
			{
			}
		}


		[StiNonSerialized]
		[Browsable(false)]
		public override StiProcessingDuplicatesType ProcessingDuplicates
		{
			get
			{
				return StiProcessingDuplicatesType.None;
			}
			set
			{
			}
		}


		[StiNonSerialized]
		[Browsable(false)]
		public override int MaxNumberOfLines
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}


		[Editor("Stimulsoft.Report.Components.Design.StiRichTextExpressionEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[TypeConverter(typeof(Stimulsoft.Report.Components.Design.StiRichTextExpressionConverter))]
        [StiPropertyLevel(StiLevel.Basic)]
		public override StiExpression Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}
		#endregion
		
		#region StiComponent override
		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.RichText;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Components;
            }
        }

		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiRichText");
			}
		}


		/// <summary>
		/// Gets a localized name of the component category.
		/// </summary>
		public override string LocalizedCategory
		{
			get 
			{
				return StiLocalization.Get("Report", "Components");
			}
		}

		#endregion

		#region Paint
		public RectangleD ConvertTextMargins(RectangleD rect, bool convert)
		{
			if (margins.IsEmpty) return rect;

			double zoom = Page.Zoom;

			double marginsLeft = margins.Left;
			double marginsRight = margins.Right;
			double marginsTop = margins.Top;
			double marginsBottom = margins.Bottom;

			if (!convert) zoom = 1;

			if (marginsLeft != 0)
			{
				rect.X += marginsLeft * zoom;
				rect.Width -= marginsLeft * zoom;
			}
			if (marginsTop != 0)
			{
				rect.Y += marginsTop * zoom;
				rect.Height -= marginsTop * zoom;
			}
			if (marginsRight != 0) rect.Width -= marginsRight * zoom;
			if (marginsBottom != 0) rect.Height -= marginsBottom * zoom;

			return rect;
		}

		private Image image = null;
		[Browsable(false)]
		public Image Image
		{
			get
			{
				return image;
			}
   			set
			{
				image = value;
			}
		}

        public void RenderMetafile()
        {
            bool fail = RenderMetafile(true);

            //check for problems with graphics on some systems
            if (fail && image == null && Wysiwyg)
            {
                lock (StiReport.GlobalRichTextMeasureGraphics)
                {
                    Bitmap bmp = new Bitmap(1, 1);
                    bmp.SetResolution((float)(96 * StiDpiHelper.GraphicsRichTextScale), (float)(96 * StiDpiHelper.GraphicsRichTextScale));
                    Graphics gr = Graphics.FromImage(bmp);
                    gr.PageUnit = GraphicsUnit.Display;
                    gr.PageScale = 1f;
                    StiReport.GlobalRichTextMeasureGraphics = gr;
                }
                RenderMetafile(true);
            }
        }

		public bool RenderMetafile(bool useTransparent)
		{
			if (Page != null && Report != null)
			{
				bool fail = true;
				int failCount = 0;

				while (fail && failCount < 10)
				{
					fail = false;
					try
					{
                        using (StiRichTextBox richTextBox = new StiRichTextBox(useTransparent && ((this.BackColor.A == 0) || Wysiwyg || StiDpiHelper.NeedGraphicsRichTextScale)))
						{
                            string text = GetPreparedText(richTextBox);
                            if (string.IsNullOrWhiteSpace(text)) return false;

							RectangleD rect = GetPaintRectangle(true, false);
							rect = ConvertTextMargins(rect, false);

                            if (BackColor.A > 0) //BackColor != Color.Transparent
                                richTextBox.BackColor = BackColor;

                            richTextBox.WordWrap = this.WordWrap;
                            richTextBox.DetectUrls = this.DetectUrls;
                            if (this.RightToLeft)
                            {
                                var backup = StiClipboardHelper.GetClipboardData();

                                richTextBox.SelectAll();
                                richTextBox.Cut();
                                richTextBox.Text = "Temp";
                                richTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
                                richTextBox.SelectAll();
                                richTextBox.Paste();

                                StiClipboardHelper.SetClipboardData(backup);
                            }

                            if (Wysiwyg || StiDpiHelper.NeedGraphicsRichTextScale)
                            {
                                lock (StiReport.GlobalRichTextMeasureGraphics)
                                {
                                    CreateMetafile(StiReport.GlobalRichTextMeasureGraphics, rect, richTextBox, text);
                                }
                            }
                            else
                            {
                                using (Graphics graph = Graphics.FromHwnd(richTextBox.Handle))
                                {
                                    CreateMetafile(graph, rect, richTextBox, text);
                                }
                            }
						}
					}
					catch (Exception e)
					{
						fail = true;
						failCount ++;

                        if (Report != null)
                            Report.WriteToReportRenderingMessages(this.Name + " " + e.Message);
					}
				}
                if (fail) return true;
			}
            return false;
		}

        private void CreateMetafile(Graphics graph, RectangleD rect, StiRichTextBox richTextBox, string text)
        {
            if (rect.Width < 0) rect.Width = 1;
            if (rect.Height < 0) rect.Height = 1;

            int rectWidth = (int)rect.Width + 1;
            int rectHeight = (int)rect.Height + 1;
            if (!this.WordWrap) rectWidth = 100000;

            float dpiX = graph.DpiX;
            float dpiY = graph.DpiY;

            IntPtr ptrGraph = graph.GetHdc();
            Metafile newImage = null;
            try
            {
                if (Wysiwyg || StiDpiHelper.NeedGraphicsRichTextScale)
                {
                    float rw = (float)(rect.Width * dpiX / 95.7f);
                    float rh = (float)(rect.Height * dpiY / 95.7f);

                    if (rw == 0f) rw = 1f;
                    if (rh == 0f) rh = 1f;

                    newImage = new Metafile(
                        ptrGraph,
                        new RectangleF(0, 0, rw, rh),
                        MetafileFrameUnit.Pixel,
                        EmfType.EmfPlusOnly);
                }
                else
                {
                    newImage = new Metafile(ptrGraph, StiOptions.Engine.RichTextDrawingMetafileType);
                }
            }
            finally
            {
                graph.ReleaseHdc(ptrGraph);
            }

            if (!string.IsNullOrEmpty(text))
            {
                using (Graphics imageGraph = Graphics.FromImage(newImage))
                {
                    int endChar = 0;
                    imageGraph.PageUnit = GraphicsUnit.Pixel;
                    StiRichTextHelper.FormatRange(StiRtfFormatType.DrawRtf, richTextBox,
                        new Rectangle(0, 0, rectWidth, rectHeight), imageGraph, 0, text.Length, out endChar);
                }
            }
            //Border.Draw(graph, rect, 1);

            if (Image != null) Image.Dispose();
            image = newImage;

            //RichText images cache
            if (Report != null)
            {
                if (Report.RichTextImageCache == null)
                {
                    Report.RichTextImageCache = new List<StiRichText>();
                }
                int indexInCache = Report.RichTextImageCache.IndexOf(this);
                if (indexInCache == -1)
                {
                    Report.RichTextImageCache.Add(this);
                }
                int richTextImageCacheSize = Report.ReportCacheMode == StiReportCacheMode.On ? 50 :  StiOptions.Engine.RichTextImageCacheSize;
                if (Report.RichTextImageCache.Count > richTextImageCacheSize)
                {
                    var comp = Report.RichTextImageCache[0];
                    Report.RichTextImageCache.RemoveAt(0);
                    
                    if (comp != null)
                        comp.ResetImage();
                }
            }

        }        
		#endregion

		#region IStiGetActualSize
		public override SizeD GetActualSize()
		{
            var tSize = new SizeD(this.Width, this.Height);
            if (this.CanGrow || this.CanShrink)
			{
                Hashtable hashCheckSize = null;
                if (StiOptions.Engine.AllowCacheForGetActualSize)
                {
                    StiReport report = this.Report;
                    if ((report != null) && (report.Engine != null))
                    {
                        if (report.Engine.HashCheckSize == null) report.Engine.HashCheckSize = new Hashtable();
                        object obj = report.Engine.HashCheckSize[this];
                        if (obj != null)
                            return (SizeD)obj;
                        hashCheckSize = report.Engine.HashCheckSize;
                    }
                }

                if (this.Report.IsWpf)
                {
                    StiUnit unit = this.Page.Unit;
                    RectangleD rect = unit.ConvertToHInches(this.ClientRectangle);
                    SizeD size = StiWpfTextRender.MeasureRtfString(rect.Width, this);
                    tSize = new SizeD(this.Width, unit.ConvertFromHInches(size.Height));
                }
                else
                {
                    if (StiOptions.Engine.FullTrust) tSize = GetActualSizeWin();
                }

                if (hashCheckSize != null) hashCheckSize[this] = tSize;
            }
			return tSize;
		}

        private SizeD GetActualSizeWin()
        {
            bool fail = true;
            int failCount = 0;

            while (fail && failCount < 100)
            {
                fail = false;
                try
                {
                    using (StiRichTextBox richTextBox = new StiRichTextBox(false))
                    {
                        richTextBox.WordWrap = this.WordWrap;
                        richTextBox.DetectUrls = this.DetectUrls;
                        if (this.RightToLeft) richTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;

                        GetPreparedText(richTextBox);
                        if (richTextBox.TextLength == 0) return new SizeD(0, 0);

                        RectangleD rect = GetPaintRectangle(true, false);
                        rect = ConvertTextMargins(rect, false);

                        if (richTextBox.TextLength > 0)
                        {
                            int rectWidth = (int)rect.Width;
                            if (!this.WordWrap) rectWidth = 100000;
                            int rtfHeight = 2;
                            if (Wysiwyg || StiDpiHelper.NeedGraphicsRichTextScale)
                            {
                                lock (StiReport.GlobalRichTextMeasureGraphics)
                                {
                                    int index = 0;
                                    while (index < richTextBox.TextLength)
                                    {
                                        rtfHeight += StiRichTextHelper.FormatRange(StiRtfFormatType.TotalRtfHeight, richTextBox,
                                            new Rectangle(0, 0, rectWidth, (int)rect.Height),
                                            StiReport.GlobalRichTextMeasureGraphics, index, -1, out index);
                                    }
                                }
                            }
                            else
                            {
                                using (Graphics measureGraph = Graphics.FromHwnd(IntPtr.Zero))
                                {
                                    int index = 0;
                                    while (index < richTextBox.TextLength)
                                    {
                                        rtfHeight += StiRichTextHelper.FormatRange(StiRtfFormatType.TotalRtfHeight, richTextBox,
                                            new Rectangle(0, 0, rectWidth, (int)rect.Height),
                                            measureGraph, index, -1, out index);
                                    }
                                }
                            }
                            rtfHeight += (int)(margins.Top + margins.Bottom);
                            double newHeight = Report.Unit.ConvertFromHInches((double)rtfHeight);
                            if (Report.Unit.ConvertToHInches(Math.Round(newHeight, 2)) < rtfHeight)
                                newHeight += 0.01d;
                            if (richTextBox.Rtf.Contains("{\\pict"))
                            {
                                newHeight *= 1.015;
                            }
                            return new SizeD(this.Width, newHeight);
                        }
                    }
                }
                catch (Exception e)
                {
                    fail = true;
                    failCount++;

                    if (Report != null)
                        Report.WriteToReportRenderingMessages(this.Name + " " + e.Message);
                }
            }
            return new SizeD(this.Width, this.Height);
        }
		#endregion

		#region Render override
		internal string GetPreparedText(RichTextBox richTextBox)
		{
		    if (IsDesigning)
		    {
		        var text2 = this.GetRtfFromSource(DataUrl != null ? DataUrl.Value : null, DataColumn);
		        if (text2 != null)
		        {
		            if (StiRtfHelper.IsRtfText(text2))
                        richTextBox.Rtf = Export.StiExportUtils.CorrectRichTextForRiched20(text2);
		            else
		                richTextBox.Text = text2;

		            return text2;
		        }
		    }

            string textInternal = this.GetTextInternal();
		    if (string.IsNullOrEmpty(textInternal))
		    {
		        if (IsDesigning)return null;
                richTextBox.Text = string.Empty;
		    }
		    else
		    {
		        int rtfPos = textInternal.TrimStart().ToLowerInvariant().IndexOf("rtf");
		        if (rtfPos != -1 && rtfPos < 30)
		        {
		            string tempSt = StiRichText.UnpackRtf(XmlConvert.DecodeName(textInternal));
		            richTextBox.Rtf = Export.StiExportUtils.CorrectRichTextForRiched20(tempSt);
		        }
		        else
		        {
		            richTextBox.Text = textInternal;
		            if (StiOptions.Engine.DefaultRtfFont != null)
		            {
		                richTextBox.SelectAll();
		                richTextBox.SelectionFont = StiOptions.Engine.DefaultRtfFont;
		            }
		            else if (DefaultFont != null)
		            {
		                richTextBox.SelectAll();
		                richTextBox.SelectionFont = DefaultFont;
		            }

		            if (!DefaultColor.IsEmpty)
		            {
		                richTextBox.SelectAll();
		                richTextBox.SelectionColor = DefaultColor;
		            }
		        }
		    }

		    richTextBox.WordWrap = true;
				
			string text = richTextBox.Rtf;
			if (richTextBox.Text == string.Empty) richTextBox.Text = " ";
			return text;
		}
		#endregion

		#region Event override
		public override void InvokeGetValue(StiComponent sender, StiGetValueEventArgs e)
		{
			try
			{
                bool isInterpretationMode = (Report != null) && (Report.CalculationMode == StiCalculationMode.Interpretation);

                #region GetDataUrl
                string dataUrlValue = null;
                if (isInterpretationMode)
                {
                    if (this.DataUrl.Value.Length > 0)
                    {
                        object parserResult = Stimulsoft.Report.Engine.StiParser.ParseTextValue(this.DataUrl.Value, this);
                        if (parserResult != null)
                        {
                            dataUrlValue = Report.ToString(parserResult);
                        }
                    }
                }
                if (this.Events[EventGetDataUrl] != null && dataUrlValue == null)
                {
                    StiGetDataUrlEventArgs ee = new StiGetDataUrlEventArgs();
                    InvokeGetDataUrl(this, ee);
                    if (ee.Value != null) dataUrlValue = ee.Value.ToString();
                }
                #endregion

				string rtfData = string.Empty;

				try
				{
                    string rtf = GetRtfFromSource(dataUrlValue, DataColumn);
				    if (!string.IsNullOrEmpty(rtf))
				    {
				        if (StiRtfHelper.IsRtfText(rtf))
				            rtfData = PackRtf(rtf);
				        else
				            rtfData = rtf;
				    }
				}
				catch
				{
					if (!StiOptions.Engine.HideExceptions) throw;
				}

				if (rtfData != null && rtfData.Length > 0)
				{
					e.Value = rtfData;
				}
				else
				{
					base.InvokeGetValue(sender, e);
				}
			}
			catch (Exception ex)
			{
				string str = string.Format("Expression in Text property of '{0}' can't be evaluated!", this.Name);
				StiLogService.Write(this.GetType(), str);
				StiLogService.Write(this.GetType(), ex.Message);
				Report.WriteToReportRenderingMessages(str);
			}
		}


        /// <summary>
        /// Returns the rtf being get as a result of rendering.
        /// </summary>
        public string GetRtfFromSource(string dataUrlValue, string dataColumn)
        {
            string rtf = GetRtfFromUrl(dataUrlValue);
            if (!string.IsNullOrEmpty(rtf)) return rtf;

            rtf = GetRtfFromFile(dataUrlValue);
            if (!string.IsNullOrEmpty(rtf)) return rtf;

            rtf = GetRtfFromDataColumn(dataColumn);
            if (!string.IsNullOrEmpty(rtf)) return rtf;

            return null;
        }

        /// <summary>
        /// Returns the rtf from specified url.
        /// </summary>
        protected string GetRtfFromUrl(string url)
        {
            try
            {
                if (!string.IsNullOrEmpty(url))
                {
                    if (StiHyperlinkProcessor.IsServerHyperlink(url))
                        return StiStimulsoftServerResource.GetRichText(this, StiHyperlinkProcessor.GetServerNameFromHyperlink(url));

                    var resourceName = StiHyperlinkProcessor.GetResourceNameFromHyperlink(url);
                    if (resourceName != null) return StiHyperlinkProcessor.GetString(Report, url);

                    var variableName = StiHyperlinkProcessor.GetVariableNameFromHyperlink(url);
                    if (variableName != null) return StiHyperlinkProcessor.GetString(Report, url);

                    var cookieContainer = Report != null ? Report.CookieContainer : null;
                    return StiRichTextFromURL.LoadRichText(url, cookieContainer);
                }
            }
            catch (Exception ex)
            {
                string str = string.Format("Rtf can't be loaded from URL '{0}' in richtext component {1}!", url, this.Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                Report.WriteToReportRenderingMessages(str);
            }
            return null;
        }

        /// <summary>
        /// Returns the rtf from specified path.
        /// </summary>
        protected string GetRtfFromFile(string hyperlink)
        {
            var file = StiHyperlinkProcessor.GetFileNameFromHyperlink(hyperlink);
            if (file == null) return null;

            if (File.Exists(file))
            {
                try
                {
                    return File.ReadAllText(file, Encoding.Default);
                }
                catch (Exception ex)
                {
                    string str = string.Format("Rtf can't be loaded from file '{0}' in richtext component {1}!", file, this.Name);
                    StiLogService.Write(this.GetType(), str);
                    StiLogService.Write(this.GetType(), ex.Message);
                    Report.WriteToReportRenderingMessages(str);
                }
            }
            else
            {
                string str = string.Format("File '{0}' does not exist in richtext component {1}!", file, this.Name);
                StiLogService.Write(this.GetType(), str);
                Report.WriteToReportRenderingMessages(str);
            }
            return null;
        }

        /// <summary>
        /// Returns the rtf from specified data column.
        /// </summary>
        protected string GetRtfFromDataColumn(string dataColumn)
        {
            try
            {
                var data = StiDataColumn.GetDataFromDataColumn(Report.Dictionary, dataColumn);
                return data is byte[] ? Encoding.UTF8.GetString(data as byte[]) : data as string;
            }
            catch (Exception ex)
            {
                string str = string.Format("Rtf can't be loaded from data column '{0}' in richtext component {1}!", dataColumn, this.Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                Report.WriteToReportRenderingMessages(str);
            }
            return null;
        }
		#endregion		

        #region GetDataUrl
        private static readonly object EventGetDataUrl = new object();

        /// <summary>
        /// Occurs when the DataUrl is calculated.
        /// </summary>
        public event StiGetDataUrlEventHandler GetDataUrl
        {
            add
            {
                base.Events.AddHandler(EventGetDataUrl, value);
            }
            remove
            {
                base.Events.RemoveHandler(EventGetDataUrl, value);
            }
        }


        /// <summary>
        /// Raises the GetDataUrl event.
        /// </summary>
        protected virtual void OnGetDataUrl(StiGetDataUrlEventArgs e)
        {
        }


        /// <summary>
        /// Raises the GetDataUrl event.
        /// </summary>
        public virtual void InvokeGetDataUrl(StiComponent sender, StiGetDataUrlEventArgs e)
        {
            try
            {
                OnGetDataUrl(e);

                StiGetDataUrlEventHandler handler = base.Events[EventGetDataUrl] as StiGetDataUrlEventHandler;
                if (handler != null) handler(sender, e);
            }
            catch (Exception ex)
            {
                string str = string.Format("Expression in DataUrl property of '{0}' can't be evaluated!", this.Name);
                StiLogService.Write(this.GetType(), str);
                StiLogService.Write(this.GetType(), ex.Message);
                Report.WriteToReportRenderingMessages(str);
            }
        }


        /// <summary>
        /// Occurs when the DataUrl is calculated.
        /// </summary>
        [StiSerializable]
        [StiCategory("ValueEvents")]
        [Browsable(false)]
        [Description("Occurs when the DataUrl is calculated.")]
        public virtual StiGetDataUrlEvent GetDataUrlEvent
        {
            get
            {
                return new StiGetDataUrlEvent(this);
            }
            set
            {
                if (value != null) value.Set(this, value.Script);
            }
        }
        #endregion

        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiRichText();
        }
        #endregion

        public override void OnResizeComponent(SizeD oldSize, SizeD newSize)
        {
            ResetImage();
        }

		#region this
		private StiMargins margins = new StiMargins(0, 0, 0, 0);
		/// <summary>
		/// Gets or sets text margins.
		/// </summary>
		[StiSerializable]
		[StiCategory("TextAdditional")]
		[StiOrder(StiPropertyOrder.TextMargins)]
		[Description("Gets or sets text margins.")]
        [StiPropertyLevel(StiLevel.Standard)]
		public virtual StiMargins Margins
		{
			get
			{
				return margins;
			}
			set
			{
				margins = value;
			}
		}

		private Font defaultFont = null;
		/// <summary>
		/// Gets or sets default font.
		/// </summary>
		[Browsable(false)]
		public Font DefaultFont
		{
			get
			{
				return defaultFont;
			}
			set
			{
				defaultFont = value;
			}
		}

		private Color defaultColor = Color.Empty;
		/// <summary>
		/// Gets or sets default Color.
		/// </summary>
		[Browsable(false)]
		public Color DefaultColor
		{
			get
			{
				return defaultColor;
			}
			set
			{
				defaultColor = value;
			}
		}


        private bool wordWrap = true;
        /// <summary>
        /// Gets or sets word wrap.
        /// </summary>
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets word wrap.")]
        [StiOrder(StiPropertyOrder.TextWordWrap)]
        [StiCategory("TextAdditional")]
        [StiShowInContextMenu]
        [StiSerializable]
        [StiPropertyLevel(StiLevel.Standard)]
        public virtual bool WordWrap
        {
            get
            {
                return wordWrap;
            }
            set
            {
                wordWrap = value;
            }
        }

        private bool detectUrls = true;
        /// <summary>
        /// Gets or sets detection of urls.
        /// </summary>
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets detection of urls.")]
        [StiOrder(StiPropertyOrder.TextDetectUrls)]
        [StiCategory("TextAdditional")]
        [StiShowInContextMenu]
        [StiSerializable]
        [StiPropertyLevel(StiLevel.Standard)]
        public virtual bool DetectUrls
        {
            get
            {
                return detectUrls;
            }
            set
            {
                detectUrls = value;
            }
        }

		private Color backColor = Color.White;
		/// <summary>
		/// Gets or sets a back color.
		/// </summary>
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[Description("Gets or sets a back color.")]
		[StiCategory("TextAdditional")]
		[StiOrder(StiPropertyOrder.TextBackColor)]
        [StiPropertyLevel(StiLevel.Standard)]
		public Color BackColor
		{
			get 
			{
				return backColor;
			}
			set 
			{
				backColor = value;
			}
		}


		private bool ShouldSerializeBackColor()
		{
			return backColor != Color.White;
		}


		private string dataColumn = string.Empty;
		/// <summary>
		/// Gets or sets a name of the column that contains the RTF text.
		/// </summary>
		[StiSerializable]
		[Editor("Stimulsoft.Report.Components.Design.StiRichTextExpressionEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiCategory("Text")]
		[StiOrder(StiPropertyOrder.TextDataColumn)]
		[Description("Gets or sets a name of the column that contains the RTF text.")]
        [StiPropertyLevel(StiLevel.Standard)]
        public string DataColumn
		{
			get
			{
				return dataColumn;
			}
			set
			{
			    if (dataColumn != value)
			    {
			        dataColumn = value;
                    ResetImage();
			    }
			}
		}



        /// <summary>
        /// Gets or sets a URL that contains the RTF text.
        /// </summary>
        [StiSerializable]
        [Description("Gets or sets a URL that contains the RTF text.")]
        [DefaultValue(null)]
        [StiCategory("Text")]
        [StiOrder(StiPropertyOrder.TextDataUrl)]
        [StiPropertyLevel(StiLevel.Standard)]
        [Editor("Stimulsoft.Report.Components.Design.StiRichTextExpressionEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        public StiDataUrlExpression DataUrl
        {
            get
            {
                return new StiDataUrlExpression(this, "DataUrl");
            }
            set
            {
                if (value != null)
                {
                    value.Set(this, "DataUrl", value.Value);
                    ResetImage();
                }
            }
        }

 
		private bool fullConvertExpression = false;
		/// <summary>
		/// Gets or sets value which indicates that it is necessary to fully convert the expression to Rtf format. Full convertion of expressions slows down the report rendering.
		/// </summary>
		[StiSerializable]
		[TypeConverter(typeof(StiBoolConverter))]
		[StiCategory("TextAdditional")]
		[StiOrder(StiPropertyOrder.TextFullConvertExpression)]
		[DefaultValue(false)]
		[Description("Gets or sets value which indicates that it is necessary to fully convert the expression to Rtf format. Full convertion of expressions slows down the report rendering.")]
        [StiPropertyLevel(StiLevel.Professional)]
		public bool FullConvertExpression
		{
			get
			{
				return fullConvertExpression;
			}
			set
			{
				fullConvertExpression = value;
			}
		}


        private bool wysiwyg = false;
        /// <summary>
        /// Gets or sets value which indicates that it is necessary to use the Wysiwyg mode of the rendering.
        /// </summary>
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("TextAdditional")]
        [StiOrder(StiPropertyOrder.TextWysiwyg)]
        [DefaultValue(false)]
        [Description("Gets or sets value which indicates that it is necessary to use the Wysiwyg mode of the rendering.")]
        [StiPropertyLevel(StiLevel.Professional)]
        public bool Wysiwyg
        {
            get
            {
                return wysiwyg;
            }
            set
            {
                wysiwyg = value;
            }
        }


        private bool rightToLeft = false;
        /// <summary>
        /// Gets or sets horizontal output direction.
        /// </summary>
        //[Browsable(false)]
        [StiSerializable]
        [TypeConverter(typeof(StiBoolConverter))]
        [StiCategory("TextAdditional")]
        [StiOrder(StiPropertyOrder.TextRightToLeft)]
        [DefaultValue(false)]
        [Description("Gets or sets horizontal output direction.")]
        [StiPropertyLevel(StiLevel.Standard)]
        public bool RightToLeft
        {
            get
            {
                return rightToLeft;
            }
            set
            {
                rightToLeft = value;
            }
        }


		/// <summary>
		/// Pack RTF text for save and compilation.
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string PackRtf(string str)
		{
			StringBuilder sb = new StringBuilder(str);
			StringBuilder ns = new StringBuilder();
			
			for (int index = 0; index < sb.Length; index++)
			{
				char c = sb[index];
				if (c == '{')
				{
					if (index == 0 || sb[index - 1] != '\\')
					{
						ns = ns.Append("__LP__");
						continue;
					}
				}
				if (c == '}')
				{
					if (index == 0 || sb[index - 1] != '\\')
					{
						ns = ns.Append("__RP__");
						continue;
					}
				}
				if ((int)c != 0)ns = ns.Append(c);
			}
			ns = ns.Replace("\\{", "{");
			ns = ns.Replace("\\}", "}");
			return ns.ToString();
		}


		/// <summary>
		/// Unpack RTF text.
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string UnpackRtf(string str)
		{
            //StringBuilder sb = new StringBuilder(str);
            //sb = sb.Replace("{", "\\{");
            //sb = sb.Replace("}", "\\}");
            //sb.Replace("__LP__", "{");
            //sb.Replace("__RP__", "}");
            //return = sb.ToString();

            if (string.IsNullOrEmpty(str)) return string.Empty;

            StringBuilder sbb = new StringBuilder();
            int index = 0;
            int start = 0;
            while (index < str.Length)
            {
                char ch = str[index];
                if (ch == '{')
                {
                    if (index > start)
                    {
                        sbb.Append(str, start, index - start);
                    }
                    sbb.Append("\\{");
                    start = index + 1;
                }
                else if (ch == '}')
                {
                    if (index > start)
                    {
                        sbb.Append(str, start, index - start);
                    }
                    sbb.Append("\\}");
                    start = index + 1;
                }
                else if ((ch == '_') && (index + 5 < str.Length) && (str[index + 1] == '_') && (str[index + 2] == 'L' || str[index + 2] == 'R') && (str[index + 3] == 'P') && (str[index + 4] == '_') && (str[index + 5] == '_'))
                {
                    if (index > start)
                    {
                        sbb.Append(str, start, index - start);
                    }
                    sbb.Append(str[index + 2] == 'L' ? "{" : "}");
                    index += 5;
                    start = index + 1;
                }
                index++;
            }
            if (index > start)
            {
                sbb.Append(str, start, index - start);
            }

            return sbb.ToString();
		}

        /// <summary>
        /// Internal use only.
        /// </summary>
        public override string GetTextInternal()
        {
            if (StiOptions.Engine.RtfCache.Enabled && this.Report != null && (!IsDesigning) && this.Guid != null)
            {
                string path = StiRtfCache.GetRtfCacheName(Report.RtfCachePath, this.Guid);

                if (File.Exists(path))
                {
                    return RemovePageBreakTag(StiRtfCache.LoadRtf(path));
                }
            }
            return RemovePageBreakTag(base.GetTextInternal());
        }

        /// <summary>
        /// Internal use only.
        /// </summary>
        public override void SetTextInternal(string value)
        {
            if (StiOptions.Engine.RtfCache.Enabled && this.Report != null && (!IsDesigning))
            {
                if (string.IsNullOrEmpty(Report.RtfCachePath))
                {
                    Report.RtfCachePath = StiRtfCache.CreateNewCache();
                }

                if (this.Guid == null) this.NewGuid();
                string path = StiRtfCache.GetRtfCacheName(Report.RtfCachePath, this.Guid);

                StiRtfCache.SaveRtf(value, path);
            }
			else base.SetTextInternal(value);
                
        }

        private static string RemovePageBreakTag(string inputString)
        {
            if (string.IsNullOrEmpty(inputString)) return inputString;

            //check packed rtf
            inputString = RemoveToken(inputString, "_x005C_pagebb");
            inputString = RemoveToken(inputString, "_x005C_page");

            //check unpacked rtf
            inputString = RemoveToken(inputString, "\\pagebb");
            inputString = RemoveToken(inputString, "\\page");

            return inputString;
        }

        private static string RemoveToken(string inputString, string token)
        {
            int offset = 0;
            do
            {
                offset = inputString.IndexOf(token, offset, StringComparison.InvariantCulture);
                if (offset == -1) return inputString;

                int offset2 = offset + token.Length;
                if (!char.IsLetterOrDigit(inputString[offset2]))
                {
                    inputString = inputString.Remove(offset, offset2 - offset);
                }
                else
                {
                    offset = offset2;
                }
            }
            while (true);
        }

		public void SetFont(Font font)
		{
			using (StiRichTextBox richTextBox = new StiRichTextBox(false))
			{
				richTextBox.Rtf = this.RtfText;

				richTextBox.SelectAll();
				richTextBox.SelectionFont = font;

				this.RtfText = richTextBox.Rtf;
			}
		}

		public void SetColor(Color color)
		{
			using (StiRichTextBox richTextBox = new StiRichTextBox(false))
			{
				richTextBox.Rtf = this.RtfText;

				richTextBox.SelectAll();
				richTextBox.SelectionColor = color;

				this.RtfText = richTextBox.Rtf;
			}
		}

        public void ResetImage()
        {
            if (!IsDesigning)
                return;

            try
            {
                if (image != null)
                    image.Dispose();

                image = null;
            }
            catch
            {
            }
        }

        internal void ResetSourceProperties()
        {
            ResetImage();

            DataUrl.Value = string.Empty;
            Text.Value = string.Empty;
            DataColumn = string.Empty;
        }
		
		/// <summary>
		/// Gets or sets text in rtf format.
		/// </summary>
		[Browsable(false)]
		public string RtfText
		{
			get
			{
               
				return StiRichText.UnpackRtf(XmlConvert.DecodeName(GetTextInternal()));
			}
			set
			{
                SetTextInternal(XmlConvert.EncodeName(StiRichText.PackRtf(value)));                
			}
		}


		/// <summary>
		/// Creates a new object of the type StiRichText.
		/// </summary>
		public StiRichText() : this(RectangleD.Empty, string.Empty)
		{
		}


		/// <summary>
		/// Creates a new component of the type StiRichText.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiRichText(RectangleD rect) : this(rect, string.Empty)
		{
		}


		/// <summary>
		/// Creates a new component of the type StiRichText.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		/// <param name="text">Text expression</param>
		public StiRichText(RectangleD rect, string text) : base(rect, text)
		{
			PlaceOnToolbox = false;
		}
		#endregion
	}
}