#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.Drawing.Design;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Report.Events;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Helpers;
using Stimulsoft.Report.Painters;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Report.QuickButtons;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Helpers;
using System.IO;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// The class describes the component of printing for image printing - Image.
	/// </summary>
    [StiServiceBitmap(typeof(StiImage), "Stimulsoft.Report.Images.Components.StiImage.png")]
	[StiToolbox(true)]
	[StiContextTool(typeof(IStiShift))]
	[StiContextTool(typeof(IStiCanGrow))]
	[StiContextTool(typeof(IStiCanShrink))]
	[StiContextTool(typeof(IStiGrowToHeight))]
    [StiContextTool(typeof(IStiComponentDesigner))]
    [StiDesigner("Stimulsoft.Report.Components.Design.StiImageDesigner, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfDesigner("Stimulsoft.Report.WpfDesign.StiWpfImageDesigner, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
    [StiGdiPainter(typeof(Stimulsoft.Report.Painters.StiImageGdiPainter))]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiImageWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
    [StiQuickButton("Stimulsoft.Report.QuickButtons.Design.StiImageQuickButton, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfQuickButton("Stimulsoft.Report.WpfDesign.StiWpfImageQuickButton, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
	[StiV1Builder(typeof(Stimulsoft.Report.Engine.StiImageV1Builder))]
    [StiV2Builder(typeof(Stimulsoft.Report.Engine.StiImageV2Builder))]
	[StiContextTool(typeof(IStiBreakable))]
	public class StiImage : 
		StiView,
		IStiBreakable,
		IStiGlobalizedName
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiImage
            jObject.AddPropertyStringNullOrEmpty("GlobalizedName", GlobalizedName);
            jObject.AddPropertyBool("CanBreak", CanBreak);
            jObject.AddPropertyJObject("GetImageURLEvent", GetImageURLEvent.SaveToJsonObject(mode));
            jObject.AddPropertyJObject("GetImageDataEvent", GetImageDataEvent.SaveToJsonObject(mode));
            jObject.AddPropertyEnum("ProcessingDuplicates", ProcessingDuplicates, StiImageProcessingDuplicatesType.None);
            jObject.AddPropertyEnum("ImageRotation", ImageRotation, StiImageRotation.None);
            jObject.AddPropertyStringNullOrEmpty("File", File);
            jObject.AddPropertyStringNullOrEmpty("DataColumn", DataColumn);

            if (mode == StiJsonSaveMode.Document)
            {
                jObject.AddPropertyStringNullOrEmpty("ImageURLValue", ImageURLValue as string);
            }
            else
            {
                jObject.AddPropertyJObject("ImageURL", ImageURL.SaveToJsonObject(mode));
                jObject.AddPropertyJObject("ImageData", ImageData.SaveToJsonObject(mode));

                if (ExistImage())
                    jObject.Add("ImageBytes", global::System.Convert.ToBase64String(TakeImage()));
            }

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "GlobalizedName":
                        this.globalizedName = property.Value.ToObject<string>();
                        break;

                    case "CanBreak":
                        this.CanBreak = property.Value.ToObject<bool>();
                        break;

                    case "ImageURLValue":
                        this.imageURLValue = property.Value.ToObject<string>();
                        break;

                    case "ImageURL":
                        {
                            var _expression = new StiImageURLExpression();
                            _expression.LoadFromJsonObject((JObject)property.Value);
                            this.ImageURL = _expression;
                        }
                        break;

                    case "ImageData":
                        {
                            var _expression = new StiImageDataExpression();
                            _expression.LoadFromJsonObject((JObject)property.Value);
                            this.ImageData = _expression;
                        }
                        break;

                    case "GetImageURLEvent":
                        {
                            var _event = new StiGetImageURLEvent();
                            _event.LoadFromJsonObject((JObject)property.Value);
                            this.GetImageURLEvent = _event;
                        }
                        break;

                    case "GetImageDataEvent":
                        {
                            var _event = new StiGetImageDataEvent();
                            _event.LoadFromJsonObject((JObject)property.Value);
                            this.GetImageDataEvent = _event;
                        }
                        break;

                    case "ProcessingDuplicates":
                        this.ProcessingDuplicates = (StiImageProcessingDuplicatesType)Enum.Parse(typeof(StiImageProcessingDuplicatesType), property.Value.ToObject<string>());
                        break;

                    case "ImageRotation":
                        this.imageRotation = (StiImageRotation)Enum.Parse(typeof(StiImageRotation), property.Value.ToObject<string>());
                        break;

                    case "Image":
                        this.PutImage(StiImageConverter.StringToByteArray(property.Value.ToObject<string>()));
                        break;

                    case "ImageBytes":
                        this.PutImage(StiImageConverter.StringToByteArray(property.Value.ToObject<string>()));
                        break;

                    case "File":
                        this.file = property.Value.ToObject<string>();
                        break;

                    case "DataColumn":
                        this.dataColumn = property.Value.ToObject<string>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiImage;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            // ImageCategory
            var list = new[] 
            { 
                propHelper.ImageEditor()
            };
            objHelper.Add(StiPropertyCategories.ComponentEditor, list);

            // ImageAdditionalCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.AspectRatio(),
                    propHelper.HorAlignment(), 
                    propHelper.VertAlignment(), 
                    propHelper.Stretch() 
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[] 
                { 
                    propHelper.AspectRatio(),
                    propHelper.MultipleFactor(), 
                    propHelper.HorAlignment(), 
                    propHelper.VertAlignment(), 
                    propHelper.ImageRotation(), 
                    propHelper.ProcessingDuplicates(), 
                    propHelper.Stretch() 
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.AspectRatio(),
                    propHelper.MultipleFactor(), 
                    propHelper.HorAlignment(), 
                    propHelper.VertAlignment(), 
                    propHelper.ImageRotation(), 
                    propHelper.ProcessingDuplicates(), 
                    propHelper.Stretch() 
                };
            }
            objHelper.Add(StiPropertyCategories.ImageAdditional, list);

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.Left(), 
                    propHelper.Top(), 
                    propHelper.Width(), 
                    propHelper.Height() 
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.Left(), 
                    propHelper.Top(), 
                    propHelper.Width(), 
                    propHelper.Height(), 
                    propHelper.MinSize(), 
                    propHelper.MaxSize() 
                };
            }
            objHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.Brush(), 
                    propHelper.Border(), 
                    propHelper.Conditions(), 
                    propHelper.ComponentStyle() 
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.Brush(), 
                    propHelper.Border(), 
                    propHelper.Conditions(), 
                    propHelper.ComponentStyle(), 
                    propHelper.UseParentStyles() 
                };
            }
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.CanGrow(), 
                    propHelper.CanShrink(), 
                    propHelper.GrowToHeight(), 
                    propHelper.CanBreak(),
                    propHelper.Enabled() 
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[] 
                { 
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.CanGrow(), 
                    propHelper.CanShrink(), 
                    propHelper.GrowToHeight(), 
                    propHelper.CanBreak(),
                    propHelper.DockStyle(), 
                    propHelper.Enabled(), 
                    propHelper.PrintOn(), 
                    propHelper.ShiftMode() 
                };
            }
            else
            {
                list = new[] 
                { 
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.CanGrow(), 
                    propHelper.CanShrink(), 
                    propHelper.GrowToHeight(), 
                    propHelper.CanBreak(),
                    propHelper.DockStyle(), 
                    propHelper.Enabled(), 
                    propHelper.Printable(), 
                    propHelper.PrintOn(), 
                    propHelper.ShiftMode() 
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[] 
                { 
                    propHelper.Name() 
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[] 
                {
                    propHelper.Name(), 
                    propHelper.Alias() 
                };
            }
            else
            {
                list = new[] 
                {
                    propHelper.Name(), 
                    propHelper.Alias(), 
#if !SERVER
                    propHelper.GlobalizedName(),
#endif
                    propHelper.Restrictions(),
                    propHelper.Locked(), 
                    propHelper.Linked() 
                };
            }
            objHelper.Add(StiPropertyCategories.Design, list);

            return objHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // ValueEventsCategory
            var list = new[] { StiPropertyEventId.GetImageURLEvent, StiPropertyEventId.GetImageDataEvent, StiPropertyEventId.GetToolTipEvent, StiPropertyEventId.GetTagEvent };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            // NavigationEventsCategory
            list = new[] { StiPropertyEventId.GetHyperlinkEvent, StiPropertyEventId.GetBookmarkEvent };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[] { StiPropertyEventId.BeforePrintEvent, StiPropertyEventId.AfterPrintEvent };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // MouseEventsCategory
            list = new[] { StiPropertyEventId.GetDrillDownReportEvent, StiPropertyEventId.ClickEvent, StiPropertyEventId.DoubleClickEvent, 
                StiPropertyEventId.MouseEnterEvent, StiPropertyEventId.MouseLeaveEvent };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties

        public override string HelpUrl
        {
            get
            {
                return "user-manual/report_internals_graphic_information_output.htm";
            }
        }

        #endregion

		#region IStiGlobalizedName
		private string globalizedName = "";
        /// <summary>
        /// Gets or sets special identificator which will be used for report globalization.
        /// </summary>
		[StiCategory("Design")]
		[StiOrder(StiPropertyOrder.DesignGlobalizedName)]
		[StiSerializable]
		[ParenthesizePropertyName(true)]
        [Description("Gets or sets special identificator which will be used for report globalization.")]
		[DefaultValue("")]
		[Editor("Stimulsoft.Report.Design.StiGlobabalizationManagerEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiPropertyLevel(StiLevel.Professional)]
		public virtual string GlobalizedName
		{
			get 
			{
				return globalizedName;
			}
			set 
			{
				globalizedName = value;
			}
		}
		#endregion		

		#region IStiView override
		/// <summary>
		/// Returns the image from the specified path.
		/// </summary>
		protected virtual byte[] GetImageFromFile()
		{
		    if (string.IsNullOrEmpty(file)) return null;

		    var tempFile = StiExpressionHelper.ParseText(Page, File);
		    if (!string.IsNullOrEmpty(StiOptions.Engine.Image.AbsolutePathOfImages))
		        tempFile = Path.Combine(StiOptions.Engine.Image.AbsolutePathOfImages, tempFile);  // 2013.01.25  fix: arguments are reversed

		    if (!global::System.IO.File.Exists(tempFile)) return null;

		    try
		    {
		        return global::System.IO.File.Exists(tempFile) ? global::System.IO.File.ReadAllBytes(tempFile) : null;
		    }
		    catch (Exception ex)
		    {

		        if (!IsDesigning)
		        {
		            var str = string.Format("Image can't be loaded from file '{0}' in image component {1}!", file, Name);
		            StiLogService.Write(this.GetType(), str);
		            StiLogService.Write(this.GetType(), ex.Message);
		            Report.WriteToReportRenderingMessages(str);
		        }
		        else
		            return new byte[0];//We should return empty image in design-time
            }
		    return null;
		}

        /// <summary>
        /// Returns the image from specified url.
        /// </summary>
        protected virtual byte[] GetImageFromUrl()
		{
			var url = IsDesigning ? ImageURL.Value : ImageURLValue as string;

			try
			{				
				if (!string.IsNullOrEmpty(url))
				{
                    if (StiHyperlinkProcessor.IsServerHyperlink(url))
                        return StiStimulsoftServerResource.GetImage(this, StiHyperlinkProcessor.GetServerNameFromHyperlink(url));

                    var resourceName = StiHyperlinkProcessor.GetResourceNameFromHyperlink(url);
                    if (resourceName != null) return StiHyperlinkProcessor.GetBytes(Report, url);

                    var variableName = StiHyperlinkProcessor.GetVariableNameFromHyperlink(url);
                    if (variableName != null) return StiHyperlinkProcessor.GetBytes(Report, url);

				    var cookieContainer = Report != null ? Report.CookieContainer : null;
				    return StiBytesFromURL.Load(url, cookieContainer);
				}
			}
			catch (Exception ex)
			{
			    if (!IsDesigning)
			    {
			        var str = string.Format("Image can't be loaded from URL '{0}' in image component {1}!", url, Name);
			        StiLogService.Write(this.GetType(), str);
			        StiLogService.Write(this.GetType(), ex.Message);
			        Report.WriteToReportRenderingMessages(str);
			    }
                else
                    return new byte[0];
			}
			return null;
		}


		/// <summary>
		/// Returns the image from specified data column.
		/// </summary>
		protected virtual byte[] GetImageFromDataColumn()
		{
			try
			{
				var imageObject = StiDataColumn.GetDataFromDataColumn(this.Report.Dictionary, this.DataColumn);

				return StiImageHelper.GetImageBytesFromObject(imageObject);
			}
			catch (Exception ex)
			{
			    if (!IsDesigning)
			    {
			        var str = string.Format("Image can't be loaded from data column '{0}' in image component {1}!", DataColumn, Name);
			        StiLogService.Write(this.GetType(), str);
			        StiLogService.Write(this.GetType(), ex.Message);
			        Report.WriteToReportRenderingMessages(str);
			    }
                else
			        return new byte[0];//We should return empty image in design-time
            }
			return null;
		}
		
		
		/// <summary>
		/// Returns the image being get as a result of rendering.
		/// </summary>
		public override byte[] GetImageFromSource()
		{
			var image = GetImageFromFile();
			if (image != null) return image;

		    image = GetImageFromUrl();
            if (image != null) return image;

			return GetImageFromDataColumn();
		}
		#endregion

		#region IStiBreakable
        protected static object PropertyCanBreak = new object();
		/// <summary>
		/// Gets or sets value which indicates whether the component can or cannot break its contents on several pages.
		/// </summary>
		[DefaultValue(false)]
		[StiSerializable]
		[StiCategory("Behavior")]
		[StiOrder(StiPropertyOrder.BehaviorCanBreak)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets value which indicates whether the component can or cannot break its contents on several pages.")]
        [StiShowInContextMenu]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual bool CanBreak
		{
			get
			{
                return Properties.GetBool(PropertyCanBreak, false);
			}
			set
			{
                Properties.SetBool(PropertyCanBreak, value, false); 
			}
		}


		/// <summary>
		/// Divides content of components in two parts. Returns result of dividing. If true, then component is successful divided.
		/// </summary>
		/// <param name="dividedComponent">Component for store part of content.</param>
		/// <returns>If true, then component is successful divided.</returns>
        public bool Break(StiComponent dividedComponent, double divideFactor, ref double divideLine)
		{
            divideLine = 0;
			var result = true;
			if (this.ExistImageToDraw())
			{
				var originalImageBytes = TakeImageToDraw();
                this.PutImageToDraw(StiComponentDivider.BreakImage(dividedComponent as StiImage, ref originalImageBytes, divideFactor));

				((StiImage)dividedComponent).PutImageToDraw(originalImageBytes);
			}
			return result;
		}
		#endregion

		#region StiComponent override
		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.Image;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Components;
            }
        }


		/// <summary>
		/// Gets a localized name of the component category.
		/// </summary>
		public override string LocalizedCategory
		{
			get 
			{
				return StiLocalization.Get("Report", "Components");
			}
		}


		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiImage");
			}
		}


		/// <summary>
		/// Gets or sets the default client area of a component.
		/// </summary>
		[Browsable(false)]
		public override RectangleD DefaultClientRectangle
		{
			get
			{
				return new RectangleD(0, 0, 100, 100);
			}
		}
		#endregion		

		#region Expressions
		#region ImageURL
		private object imageURLValue;
		/// <summary>
		/// Gets or sets image URL.
		/// </summary>
		[Browsable(false)]
		[StiSerializable(StiSerializeTypes.SerializeToDocument)]
		[Description("Gets or sets image URL.")]
		public object ImageURLValue
		{
			get
			{
				return imageURLValue;
			}
			set
			{
				if (imageURLValue != value)
				{
					imageURLValue = value;

					if (!ExistImage() && (IsDesigning || (Report != null && Report.IsRendering)))
					{
						if (value != null)
							this.PutImageToDraw(GetImageFromUrl());
					}
				}
			}
		}


		/// <summary>
		/// Gets or sets the expression to fill a component image URL.
		/// </summary>
        [StiCategory("Image")]
		[StiOrder(StiPropertyOrder.ImageImageURL)]
		[StiSerializable(
			 StiSerializeTypes.SerializeToCode |
			 StiSerializeTypes.SerializeToDesigner |
			 StiSerializeTypes.SerializeToSaveLoad)]
		[Description("Gets or sets the expression to fill a component image URL.")]
		[Editor("Stimulsoft.Report.Components.Design.StiImageExpressionEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiPropertyLevel(StiLevel.Basic)]
		public virtual StiImageURLExpression ImageURL
		{
			get
			{
				return new StiImageURLExpression(this, "ImageURL");
			}
			set
			{
				if (value != null)value.Set(this, "ImageURL", value.Value);
			    ResetImageToDraw();
			}
		}
		#endregion

		#region ImageData
		/// <summary>
		/// Gets or sets the expression to fill a component image property.
		/// </summary>
        [StiCategory("Image")]
		[StiOrder(StiPropertyOrder.ImageImageData)]
		[StiSerializable(
			 StiSerializeTypes.SerializeToCode |
			 StiSerializeTypes.SerializeToDesigner |
			 StiSerializeTypes.SerializeToSaveLoad)]
		[Description("Gets or sets the expression to fill a component image property.")]
		[Editor("Stimulsoft.Report.Components.Design.StiImageExpressionEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [StiPropertyLevel(StiLevel.Professional)]
		public virtual StiImageDataExpression ImageData
		{
			get
			{
				return new StiImageDataExpression(this, "ImageData");
			}
			set
			{
				if (value != null)value.Set(this, "ImageData", value.Value);
			    ResetImageToDraw();
			}
		}
		#endregion
		#endregion

		#region Events
		/// <summary>
		/// Invokes all events for this components.
		/// </summary>
		public override void InvokeEvents()
		{
			try
			{
				base.InvokeEvents();
				
				#region GetImageURL
                if (Report.CalculationMode == StiCalculationMode.Compilation)
                {
                    if (this.Events[EventGetImageURL] != null)
                    {
                        if (this.ImageURLValue == null || (this.ImageURLValue is string && ((string)this.ImageURLValue).Length == 0))
                        {
                            var e = new StiValueEventArgs();
                            InvokeGetImageURL(this, e);
                            this.ImageURLValue = e.Value;
                        }
                    }
                }
                else
                {
                    if (this.ImageURLValue == null || (this.ImageURLValue is string && ((string)this.ImageURLValue).Length == 0))
                    {
                        var e = new StiValueEventArgs();
                        InvokeGetImageURL(this, e);
                        this.ImageURLValue = e.Value;
                    }
                }
				#endregion

				#region GetImageData
                if (Report.CalculationMode == StiCalculationMode.Compilation)
                {
                    if (this.Events[EventGetImageData] != null)
                    {
                        var ee = new StiGetImageDataEventArgs();
                        InvokeGetImageData(this, ee);
                        if (ee.Value != null) PutImage(ee.Value);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(this.ImageData.Value))
                    {
                        var ee = new StiGetImageDataEventArgs();
                        InvokeGetImageData(this, ee);
                        if (ee.Value != null) PutImage(ee.Value);
                    }
                }
				#endregion
			}
			catch (Exception er)
			{
				StiLogService.Write(this.GetType(), "DoEvents...ERROR");
				StiLogService.Write(this.GetType(), er);

                if (Report != null)
                    Report.WriteToReportRenderingMessages(this.Name + " " + er.Message);
			}
		}

		#region GetImageURL
		private static readonly object EventGetImageURL = new object();
		/// <summary>
		/// Occurs when getting image url for the component.
		/// </summary>
		public event StiValueEventHandler GetImageURL
		{
			add
			{
				base.Events.AddHandler(EventGetImageURL, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventGetImageURL, value);
			}
		}

		/// <summary>
		/// Raises the GetImageURL event.
		/// </summary>
		protected virtual void OnGetImageURL(StiValueEventArgs e)
		{
		}
		

		/// <summary>
		/// Raises the GetImageURL event.
		/// </summary>
		public void InvokeGetImageURL(object sender, StiValueEventArgs e)
		{
			try
			{
                if (Report.CalculationMode == StiCalculationMode.Compilation)
                {
                    OnGetImageURL(e);
                    StiValueEventHandler handler = base.Events[EventGetImageURL] as StiValueEventHandler;
                    if (handler != null) handler(sender, e);
                }
                else
                {
                    OnGetImageURL(e);

                    object parserResult = StiParser.ParseTextValue(this.ImageURL.Value, this, sender);
                    if (parserResult != null) e.Value = parserResult.ToString();

                    StiValueEventHandler handler = base.Events[EventGetImageURL] as StiValueEventHandler;
                    if (handler != null) handler(sender, e);
                }
			}
			catch (Exception ex)
			{
				string str = string.Format("Expression in ImageURL property of '{0}' can't be evaluated!", this.Name);
				StiLogService.Write(this.GetType(), str);
				StiLogService.Write(this.GetType(), ex.Message);
				Report.WriteToReportRenderingMessages(str);
			}
		}
		

		/// <summary>
		/// Occurs when getting image url for the component.
		/// </summary>
		[StiSerializable]
		[StiCategory("ValueEvents")]
		[Browsable(false)]
		[Description("Occurs when getting image url for the component.")]
		public virtual StiGetImageURLEvent GetImageURLEvent
		{
			get
			{				
				return new StiGetImageURLEvent(this);
			}
			set
			{
				if (value != null)value.Set(this, value.Script);
			}
		}
		#endregion

		#region GetImageData
		private static readonly object EventGetImageData = new object();

		/// <summary>
		/// Occurs when getting image for the component.
		/// </summary>
		public event StiGetImageDataEventHandler GetImageData
		{
			add
			{
				base.Events.AddHandler(EventGetImageData, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventGetImageData, value);
			}
		}


		/// <summary>
		/// Raises the GetImageData event.
		/// </summary>
		protected virtual void OnGetImageData(StiGetImageDataEventArgs e)
		{
		}
		

		/// <summary>
		/// Raises the GetImageData event.
		/// </summary>
		public void InvokeGetImageData(object sender, StiGetImageDataEventArgs e)
		{
			try
			{
                if (Report.CalculationMode == StiCalculationMode.Compilation)
                {
                    OnGetImageData(e);
                    StiGetImageDataEventHandler handler = base.Events[EventGetImageData] as StiGetImageDataEventHandler;
                    if (handler != null) handler(sender, e);
                }
                else
                {
                    OnGetImageData(e);

                    object parserResult = StiParser.ParseTextValue(this.ImageData.Value, this, sender);
                    if (parserResult is Image)
                    {
                        e.Value = parserResult as Image;
                    }
                    
                    StiGetImageDataEventHandler handler = base.Events[EventGetImageData] as StiGetImageDataEventHandler;
                    if (handler != null) handler(sender, e);
                }
			}
			catch (Exception ex)
			{
				string str = string.Format("Expression in ImageData property of '{0}' can't be evaluated!", this.Name);
				StiLogService.Write(this.GetType(), str);
				StiLogService.Write(this.GetType(), ex.Message);
				Report.WriteToReportRenderingMessages(str);
			}
		}
		

		/// <summary>
		/// Occurs when getting image for the component.
		/// </summary>
		[StiSerializable]
		[StiCategory("ValueEvents")]
		[Browsable(false)]
		[Description("Occurs when getting image for the component.")]
		public virtual StiGetImageDataEvent GetImageDataEvent
		{
			get
			{				
				return new StiGetImageDataEvent(this);
			}
			set
			{
				if (value != null)value.Set(this, value.Script);
			}
		}
		#endregion
		#endregion

		#region Properties
        protected static object PropertyProcessingDuplicates = new object();
        /// <summary>
        /// Gets or sets value which indicates how report engine processes duplicated images.
        /// </summary>
        [DefaultValue(StiImageProcessingDuplicatesType.None)]
        [StiSerializable]
        [StiCategory("ImageAdditional")]
        [StiOrder(StiPropertyOrder.ImageProcessingDuplicates)]
        [TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets value which indicates how report engine processes duplicated images.")]
        [StiPropertyLevel(StiLevel.Standard)]
        public virtual StiImageProcessingDuplicatesType ProcessingDuplicates
        {
            get
            {
                return (StiImageProcessingDuplicatesType)Properties.Get(PropertyProcessingDuplicates, StiImageProcessingDuplicatesType.None);
            }
            set
            {
                Properties.Set(PropertyProcessingDuplicates, value, StiImageProcessingDuplicatesType.None);
            }
        }

		private StiImageRotation imageRotation = StiImageRotation.None;
		/// <summary>
		/// Gets or sets value which indicates how to rotate an image before output.
		/// </summary>
		[Browsable(true)]
		[DefaultValue(StiImageRotation.None)]
		[StiSerializable]
		[StiCategory("ImageAdditional")]
		[StiOrder(StiPropertyOrder.ImageImageRotation)]
		[Description("Gets or sets value which indicates how to rotate an image before output.")]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [StiPropertyLevel(StiLevel.Standard)]
		public StiImageRotation ImageRotation
		{
			get
			{
				return imageRotation;
			}
			set
			{
				imageRotation = value;
			}
		}


		/// <summary>
		/// Gets or sets the image.
		/// </summary>
		[Browsable(true)]
		[StiCategory("Image")]
		[StiOrder(StiPropertyOrder.ImageImage)]
		[Description("Gets or sets the image.")]
		[Editor("Stimulsoft.Report.Components.Design.StiImageEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[TypeConverter(typeof(Stimulsoft.Report.Components.Design.StiSimpeImageConverter))]
        [StiPropertyLevel(StiLevel.Basic)]
		public Image Image
		{
			get
			{
			    return TakeGdiImage();
            }
			set
			{
			    if (value == StiNullImage.Null) return;

			    PutImage(value);
            }
		}


        private byte[] imageBytes;
        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        [Browsable(false)]
        [StiSerializable(StiSerializeTypes.SerializeToCode | StiSerializeTypes.SerializeToDesigner | StiSerializeTypes.SerializeToSaveLoad)]
        [StiCategory("Image")]
        [StiOrder(StiPropertyOrder.ImageImage)]
        [Description("Gets or sets the image.")]
        [Editor("Stimulsoft.Report.Components.Design.StiImageEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [TypeConverter(typeof(Stimulsoft.Report.Components.Design.StiSimpeImageConverter))]
        [StiPropertyLevel(StiLevel.Basic)]
        public byte[] ImageBytes
        {
            get
            {
                return imageBytes;
            }
            set
            {
                if (imageBytes != value)
                {
                    imageBytes = value;
                    PutImageToDraw(value);
                }
            }
        }


        protected static object PropertyMargins = new object();
        /// <summary>
        /// Gets or sets image margins.
        /// </summary>
        [StiSerializable]
        [StiCategory("ImageAdditional")]
        [StiOrder(StiPropertyOrder.ImageMargins)]
        [Description("Gets or sets image margins.")]
        [StiPropertyLevel(StiLevel.Standard)]
        public virtual StiMargins Margins
        {
            get
            {
                return (StiMargins)Properties.Get(PropertyMargins, StiMargins.Empty);
            }
            set
            {
                Properties.Set(PropertyMargins, value, StiMargins.Empty);
            }
        }


		private string file = string.Empty;
		/// <summary>
		/// Gets or sets the path to the file that contains the image.
		/// </summary>
        [StiSerializable]
		[Editor("Stimulsoft.Report.Components.Design.StiImageEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[StiCategory("Image")]
		[StiOrder(StiPropertyOrder.ImageFile)]
		[DefaultValue("")]
		[Description("Gets or sets the path to the file that contains the image.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public string File
		{
			get
			{
				return file;
			}
			set
			{
				if (file != value)
				{
					file = value;

                    if (!ExistImage() && (IsDesigning || (Report != null && !Report.IsSerializing)))
                    {
                        this.PutImageToDraw(GetImageFromFile());
                    }
				}
                else
				    ResetImageToDraw();
            }
		}


		private string dataColumn = string.Empty;
		/// <summary>
		/// Gets or sets a name of the column that contains the image.
		/// </summary>
        [StiSerializable]
		[Editor("Stimulsoft.Report.Components.Design.StiImageDataColumnEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
		[StiCategory("Image")]
		[StiOrder(StiPropertyOrder.ImageDataColumn)]
		[DefaultValue("")]
		[Description("Gets or sets a name of the column that contains the image.")]
        [StiPropertyLevel(StiLevel.Basic)]
		public string DataColumn
		{
			get
			{
				return dataColumn;
			}
			set
			{
				dataColumn = value;
			    ResetImageToDraw();
            }
		}
        #endregion

        #region Methods
        public void UpdateImageToDrawInDesigner()
        {
            if (ExistImageToDraw()) return;

            if (Report == null || Report.IsPageDesigner) return;

            if (Report != null && Report.Dictionary != null && IsDesigning && !Report.Info.IsComponentsMoving)
            {
                try
                {
                    if (ImageData != null)
                        this.PutImageToDraw(StiVariableImageProcessor.GetImage(this.Report, ImageData.Value));

                    if (!this.ExistImageToDraw())
                        this.PutImageToDraw(GetImageFromSource());
                }
                catch (Exception e)
                {
                    ResetImageToDraw();

                    if (Report != null)
                        Report.WriteToReportRenderingMessages(this.Name + " " + e.Message);
                }
            }

            if (StiOptions.Designer.UseGlobalizationManager && ExistImage() && (this.Report.GlobalizationManager != null) && !(this.Report.GlobalizationManager is StiNullGlobalizationManager) && !string.IsNullOrWhiteSpace(GlobalizedName))
            {
                try
                {
                    this.PutImageToDraw(Report.GlobalizationManager.GetObject(GlobalizedName) as Image);
                }
                catch
                {
                }
            }
        }

        public RectangleD ConvertImageMargins(RectangleD rect, bool convert)
        {
            StiMargins margins = Margins;   //speed optimization
            if (margins.IsEmpty) return rect;

            double zoom = Page.Zoom;
            if (!convert) zoom = 1;

            double marginsLeft = margins.Left;
            double marginsRight = margins.Right;
            double marginsTop = margins.Top;
            double marginsBottom = margins.Bottom;

            if (marginsLeft != 0)
            {
                rect.X += marginsLeft * zoom;
                rect.Width -= marginsLeft * zoom;
            }
            if (marginsTop != 0)
            {
                rect.Y += marginsTop * zoom;
                rect.Height -= marginsTop * zoom;
            }
            if (marginsRight != 0) rect.Width -= marginsRight * zoom;
            if (marginsBottom != 0) rect.Height -= marginsBottom * zoom;

            return rect;
        }

        public override StiComponent CreateNew()
        {
            return new StiImage();
        }

        internal bool ExistImage()
        {
            return ImageBytes != null;
        }

        internal byte[] TakeImage()
        {
            return ImageBytes;
        }

        internal Image TakeGdiImage()
        {
            if (!ExistImage()) return null;

            if (ImageIsSvg())
            {
                var rect = GetPaintRectangle(true, false);
                return StiImageConverter.BytesToImage(ImageBytes, (int)rect.Width, (int)rect.Height, Stretch, AspectRatio);
            }

            return StiImageConverter.BytesToImage(ImageBytes);
        }

        internal void PutImage(Image image)
        {
            ImageBytes = StiImageConverter.ImageToBytes(image, true);
        }

        internal void PutImage(byte[] image)
        {
            ImageBytes = image;
        }

        internal void ResetImage()
        {
            ImageBytes = null;
        }

        internal bool ImageIsMetafile()
        {
            return StiImageHelper.IsMetafile(ImageBytes);
        }

        internal bool ImageIsSvg()
        {
            return StiSvgHelper.IsSvg(ImageBytes);
        }

        internal void ResetAllImageProperties()
        {
            ResetImage();
            DataColumn = string.Empty;
            ImageURL.Value = string.Empty;
            ImageData.Value = string.Empty;
            File = string.Empty;
        }

        internal void CopyAllImageProperties(StiImage image)
        {
            if (image == null) return;

            ImageBytes = image.ImageBytes;
            DataColumn = image.DataColumn;
            ImageURL.Value = image.ImageURL.Value;
            ImageData.Value = image.ImageData.Value;
            File = image.File;
        }
        #endregion

        #region this
        /// <summary>
		/// Creates a new component of the type StiImage.
		/// </summary>
		public StiImage() : this(RectangleD.Empty)
		{
		}


		/// <summary>
		/// Creates a new component of the type StiImage with specified location.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiImage(RectangleD rect) : base(rect)
		{
			PlaceOnToolbox = true;
		}
		#endregion
	}
}