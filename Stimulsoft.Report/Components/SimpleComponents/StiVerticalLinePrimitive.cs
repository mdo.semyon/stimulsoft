#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Report.Units;
using Stimulsoft.Report.Painters;
using Stimulsoft.Base.Json.Linq;
using System;

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// Describes class that realizes component - StiVerticalLinePrimitive.
	/// </summary>
	[StiToolbox(true)]
    [StiServiceBitmap(typeof(StiVerticalLinePrimitive), "Stimulsoft.Report.Images.Components.StiVerticalLinePrimitive.png")]
	[StiDesigner("Stimulsoft.Report.Components.Design.StiVerticalLinePrimitiveDesigner, Stimulsoft.Report.Design, " + StiVersion.VersionInfo)]
    [StiWpfDesigner("Stimulsoft.Report.WpfDesign.StiWpfVerticalLinePrimitiveDesigner, Stimulsoft.Report.WpfDesign, " + StiVersion.VersionInfo)]
    [StiGdiPainter(typeof(Stimulsoft.Report.Painters.StiVerticalLinePrimitiveGdiPainter))]
    [StiWpfPainter("Stimulsoft.Report.Painters.StiVerticalLinePrimitiveWpfPainter, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo)]
	public class StiVerticalLinePrimitive : 
		StiCrossLinePrimitive, 
		IStiHideBorderFromDesigner,		
		IStiBorder
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiVerticalLinePrimitive
            jObject.AddPropertyStringNullOrEmpty("StartCap", StiJsonReportObjectHelper.Serialize.JCap(startCap));
            jObject.AddPropertyStringNullOrEmpty("EndCap", StiJsonReportObjectHelper.Serialize.JCap(endCap));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "StartCap":
                        this.startCap = StiJsonReportObjectHelper.Deserialize.JCap(property.Value.ToObject<string>());
                        break;

                    case "EndCap":
                        this.endCap = StiJsonReportObjectHelper.Deserialize.JCap(property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId
        {
            get
            {
                return StiComponentId.StiVerticalLinePrimitive;
            }
        }

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();
            StiPropertyObject[] list;

            // PrimitiveCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Color(),
                    propHelper.SizeFloat(),
                    propHelper.Style()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Color(),
                    propHelper.SizeFloat(),
                    propHelper.Style(),
                    propHelper.StartCap(),
                    propHelper.EndCap()
                };
            }
            objHelper.Add(StiPropertyCategories.Primitive, list);

            // PositionCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Height()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Left(),
                    propHelper.Top(),
                    propHelper.Height(),
                    propHelper.MinSize(),
                    propHelper.MaxSize()
                };
            }
            objHelper.Add(StiPropertyCategories.Position, list);

            // AppearanceCategory
            list = new[]
            {
                propHelper.ComponentStyle()
            };
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // BehaviorCategory
            if (level == StiLevel.Basic)
            {
                list = new []
                {
                    propHelper.Enabled()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.Enabled(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.InteractionEditor(),
                    propHelper.AnchorMode(),
                    propHelper.Enabled(),
                    propHelper.Printable(),
                    propHelper.PrintOn(),
                    propHelper.ShiftMode()
                };
            }
            objHelper.Add(StiPropertyCategories.Behavior, list);

            // DesignCategory
            if (level == StiLevel.Basic)
            {
                list = new[]
                {
                    propHelper.Name()
                };
            }
            else if (level == StiLevel.Standard)
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias()
                };
            }
            else
            {
                list = new[]
                {
                    propHelper.Name(),
                    propHelper.Alias(),
                    propHelper.Restrictions(),
                    propHelper.Locked(),
                    propHelper.Linked()
                };
            }
            objHelper.Add(StiPropertyCategories.Design, list);

            return objHelper;
        }

        public override StiEventCollection GetEvents(IStiPropertyGrid propertyGrid)
        {
            var objectHelper = new StiEventCollection();

            // ValueEventsCategory
            var list = new[] { StiPropertyEventId.GetToolTipEvent, StiPropertyEventId.GetTagEvent };
            objectHelper.Add(StiPropertyCategories.ValueEvents, list);

            // NavigationEventsCategory
            list = new[] { StiPropertyEventId.GetHyperlinkEvent, StiPropertyEventId.GetBookmarkEvent };
            objectHelper.Add(StiPropertyCategories.NavigationEvents, list);

            // PrintEventsCategory
            list = new[] { StiPropertyEventId.BeforePrintEvent, StiPropertyEventId.AfterPrintEvent };
            objectHelper.Add(StiPropertyCategories.PrintEvents, list);

            // MouseEventsCategory
            list = new[] { StiPropertyEventId.GetDrillDownReportEvent, StiPropertyEventId.ClickEvent, StiPropertyEventId.DoubleClickEvent, 
                StiPropertyEventId.MouseEnterEvent, StiPropertyEventId.MouseLeaveEvent };
            objectHelper.Add(StiPropertyCategories.MouseEvents, list);

            return objectHelper;
        }
        #endregion

        #region StiComponent.Properties
        
        public override string HelpUrl
        {
            get
            {
                return "user-manual/report_internals_primitives_crossprimitives.htm?zoom_highlightsub=Vertical%2BLine%2BPrimitive";
            }
        }

        #endregion

        #region ICloneable override
        public override object Clone(bool cloneProperties)
        {
            StiVerticalLinePrimitive cloneLine = (StiVerticalLinePrimitive)base.Clone(cloneProperties);

            if (startCap != null) cloneLine.startCap = (StiCap)this.startCap.Clone();
            else cloneLine.startCap = null;
            if (endCap != null) cloneLine.endCap = (StiCap)this.endCap.Clone();
            else cloneLine.endCap = null;

            return cloneLine;
        }
        #endregion

		#region IStiBorder
		private StiBorder border = null;
		/// <summary>
		/// Gets or sets frame of the component.
		/// </summary>
		[Browsable(false)]
		[StiNonSerialized]
		public virtual StiBorder Border
		{
			get 
			{
				if (border == null)
				{
					border = new StiBorder(
						StiBorderSides.Left, this.Color, this.Size, this.Style, false, 0, null);
				}
				return border;
			}
			set 
			{
			}
		}
		#endregion		

		#region IStiUnitConvert
		/// <summary>
		/// Converts a component out of one unit into another.
		/// </summary>
		/// <param name="oldUnit">Old units.</param>
		/// <param name="newUnit">New units.</param>
        public override void Convert(StiUnit oldUnit, StiUnit newUnit, bool isReportSnapshot = false)
        {
            if (GetStartPoint() == null && GetEndPoint() == null)
            {
                base.Convert(oldUnit, newUnit, isReportSnapshot);
            }
        }
		#endregion

		#region StiComponent override
		/// <summary>
		/// Gets value to sort a position in the toolbox.
		/// </summary>
		public override int ToolboxPosition
		{
			get
			{
				return (int)StiComponentToolboxPosition.VerticalLinePrimitive;
			}
		}

        public override StiToolboxCategory ToolboxCategory
        {
            get
            {
                return StiToolboxCategory.Shapes;
            }
        }

		/// <summary>
		/// Gets a localized component name.
		/// </summary>
		public override string LocalizedName
		{
			get 
			{
				return StiLocalization.Get("Components", "StiVerticalLinePrimitive");
			}
		}
		#endregion

        #region Properties
        private StiCap startCap = new StiCap();
        /// <summary>
        /// Gets or sets the end cap settings.
        /// </summary>
        [Browsable(true)]
        [StiCategory("Primitive")]
        [StiSerializable(StiSerializationVisibility.Class)]
        [StiOrder(StiPropertyOrder.PrimitiveStartCap)]
        [Description("Gets or sets the end cap settings.")]
        [RefreshProperties(RefreshProperties.All)]
        [StiPropertyLevel(StiLevel.Standard)]
        public StiCap StartCap
        {
            get
            {
                return startCap;
            }
            set
            {
                startCap = value;
            }
        }

        private StiCap endCap = new StiCap();
        /// <summary>
        /// Gets or sets the end cap settings.
        /// </summary>
        [Browsable(true)]
        [StiCategory("Primitive")]
        [StiSerializable(StiSerializationVisibility.Class)]
        [StiOrder(StiPropertyOrder.PrimitiveEndCap)]
        [Description("Gets or sets the end cap settings.")]
        [RefreshProperties(RefreshProperties.All)]
        [StiPropertyLevel(StiLevel.Standard)]
        public StiCap EndCap
        {
            get
            {
                return endCap;
            }
            set
            {
                endCap = value;
            }
        }
        #endregion

        #region Position
        [Browsable(false)]
		public override double Width
		{
			get 
			{
				if (Page != null)return Page.Unit.ConvertFromHInches(1d);
				return 0;
			}
			set 
			{
				if (IsDesigning)
				{
					base.Width = value;

					StiPointPrimitive startPoint = GetStartPoint();
					StiPointPrimitive endPoint = GetEndPoint();
					if (startPoint != null && endPoint != null)
					{
						PointD startPos = new PointD(startPoint.Left, startPoint.Top);

						startPos = startPoint.ComponentToPage(startPos);
						//PointD endPos = new PointD(startPos.X + value, startPos.Y);
                        PointD endPos = new PointD(startPos.X, startPos.Y);
                        endPos = endPoint.PageToComponent(endPos);
						endPoint.Left = endPos.X;
					}
				}
			}
		}
		#endregion		
        
        #region Methods.override
        public override StiComponent CreateNew()
        {
            return new StiVerticalLinePrimitive();
        }
        #endregion

		#region this
		/// <summary>
		/// Creates a new StiVerticalLinePrimitive.
		/// </summary>
		public StiVerticalLinePrimitive() : this(RectangleD.Empty)
		{
		}

		/// <summary>
		/// Creates a new StiVerticalLinePrimitive.
		/// </summary>
		/// <param name="rect">The rectangle describes size and position of the component.</param>
		public StiVerticalLinePrimitive(RectangleD rect) : base(rect)
		{
            PlaceOnToolbox = false;
		}
		#endregion
	}
}