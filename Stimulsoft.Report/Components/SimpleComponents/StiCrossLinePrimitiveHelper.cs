#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Design;
using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Services;
using Stimulsoft.Report.Components.Design;
using Stimulsoft.Report.Events;

namespace Stimulsoft.Report.Components
{
	public class StiCrossLinePrimitiveHelper
	{
		public static void RemoveIncorrectLinesFromContainer(StiComponentsCollection comps)
		{
			int index = 0;
			while (index < comps.Count)
			{
				StiCrossLinePrimitive crossPrimitive = comps[index] as StiCrossLinePrimitive;
				if (crossPrimitive != null)
				{
					crossPrimitive.StoredStartPoint = null;
					crossPrimitive.StoredEndPoint = null;

					crossPrimitive.GetStartPoint();
					crossPrimitive.GetEndPoint();
							
					if (crossPrimitive.StoredStartPoint == null || crossPrimitive.StoredEndPoint == null)
					{
						comps.RemoveAt(index);
								
						if (crossPrimitive.StoredStartPoint != null && crossPrimitive.StoredStartPoint.Parent != null)
						{
							crossPrimitive.StoredStartPoint.Parent.Components.Remove(crossPrimitive.StoredStartPoint);
						}

						if (crossPrimitive.StoredEndPoint != null && crossPrimitive.StoredEndPoint.Parent != null)
						{
							crossPrimitive.StoredEndPoint.Parent.Components.Remove(crossPrimitive.StoredEndPoint);
						}
						continue;
					}
				}
				index++;
			}
		}
	}
}
