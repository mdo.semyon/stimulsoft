#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Collections;
using System.Linq;
using Stimulsoft.Report.SaveLoad;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.Events;
using System.Threading;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Report.Dialogs;
using System.Globalization;
using Stimulsoft.Report.Dashboard;

namespace Stimulsoft.Report.Components
{
	/// <summary>
	/// The class describes a collection of pages.
	/// </summary>
    [Serializable]
	public partial class StiPagesCollection : 
        CollectionBase, 
        IStiStateSaveRestore,
        IStiJsonReportObject
    {
        #region IStiJsonReportObject.override
        public JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            if (List.Count == 0)
                return null;

            var jObject = new JObject();

            int index = 0;
            foreach(StiPage page in List)
            {
                jObject.AddPropertyJObject(index.ToString(), page.SaveToJsonObject(mode));
                index++;
            }

            return jObject;
        }

        public void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                StiPage page = null;

                var propJObject = (JObject)property.Value;
                var ident = propJObject.Properties().FirstOrDefault(x => x.Name == "Ident").Value.ToObject<string>();

                switch(ident)
                {
                    case "StiDashboard":
                        page = StiDashboardCreator.CreateDashboard(Report) as StiPage;
                        break;

                    case "StiPage":
                        page = StiActivator.CreateObject("Stimulsoft.Report.Components.StiPage") as StiPage;
                        break;

                    case "StiForm":
                        page = new StiForm();
                        break;
                }

                Add(page);
                page.LoadFromJsonObject(propJObject);
            }
        }
        #endregion

        #region NestedClass.PageStore
        private class PageStore
		{
			public bool SavePage;
			public bool IsNotSaved
			{
				get
				{
					return SavePage;
				}
			}

			public StiPage Page;

			public PageStore(StiPage page, bool savePage)
			{
				this.Page = page;
				this.SavePage = savePage;
			}
		}
		#endregion

		#region Collection
	    public List<StiPage> ToList()
	    {
	        return this.InnerList.Cast<StiPage>().ToList();
	    }

        public void Add(StiPage page)
        {
            if (report != null)
            {
                page.DisplayRectangle = page.Unit.ConvertFromHInches(page.DisplayRectangle);
            }

            if ((report != null && report.IsDesigning) && string.IsNullOrEmpty(page.Name))
            {
                page.Name = StiNameCreation.CreateName(report, StiNameCreation.GenerateName(page));
            }

            if (string.IsNullOrEmpty(page.Name))
            {
                if (report != null && report.IsDesigning)
                {
                    page.Name = StiNameCreation.CreateName(report, StiNameCreation.GenerateName(page));
                }
                else
                {
                    page.Name = StiNameCreation.CreateSimpleName(report, StiNameCreation.GenerateName(page));
                }
            }

            AddV2Internal(page);
        }

        internal void AddV2Internal(StiPage page)
		{
			if (page.Report == null)page.Report = report;

			List.Add(page);

            //InvokePageAdded(page, EventArgs.Empty);   //already in OnInsertComplete

			if (report != null && CanUseCacheMode)
			{
				if (CacheMode)
				{
					AddPageToQuickCache(page, true);
				}
				else if (report.ReportCacheMode == StiReportCacheMode.Auto)
				{
					if (Count > StiOptions.Engine.ReportCache.LimitForStartUsingCache)
					{
						this.CacheMode = true;
                        CheckCacheL2();

						int index = this.Count;
                        lock (((ICollection)this).SyncRoot)
						foreach (StiPage pg in this)
						{
                            if (Report != null && Report.EngineVersion == StiEngineVersion.EngineV1)
                            {
                                StiPostProcessProviderV1.PostProcessPage(pg,
                                    page.PageInfoV1.PageNumber == 1,
                                    false);
                                StiPostProcessProviderV1.PostProcessPrimitives(pg);
                            }
						
                            //if (index < StiOptions.Engine.ReportCache.AmountOfQuickAccessPages)
                            //{
								AddPageToQuickCache(pg, true);

                                if (pg.Report != null && pg.Report.EngineVersion == StiEngineVersion.EngineV2)
                                {
                                    StiRenderProviderV2.ProcessPageToCache(pg.Report, pg, false);
                                }

                            //}
                            //else 
                            //{
                            //    if (Report != null && Report.EngineVersion == StiEngineVersion.EngineV2)
                            //    {
                            //        if (Report.ReportPass == StiReportPass.Second)
                            //        {
                            //            int pageIndex = Report.Engine.PageNumbers.GetPageNumber(pg);
                            //            int totalPageCount = Report.Engine.PageNumbers.GetTotalPageCount(pageIndex);
                            //            StiPostProcessProviderV2.PostProcessPage(pg, pageIndex == 1, pageIndex == totalPageCount);
                            //        }
                            //        else
                            //        {
                            //            //StiPostProcessProviderV2.PostProcessPage(pg, index == 0, index == this.Count);
                            //            StiPostProcessProviderV2.PostProcessPage(pg, StiRenderProviderV2.IsFirstPage(Report, pg), StiRenderProviderV2.IsLastPage(Report, pg));
                            //        }
                            //        StiPostProcessProviderV2.PostProcessPrimitives(pg);
                            //    }

                            //    SavePage(pg);
                            //    if (cacheL2 == null) pg.Components.Clear();
                            //}
							index --;
						}
					}
				}
			}
		}

		
		public void AddRange(StiPage[] pages)
		{
            lock (((ICollection)pages).SyncRoot)
			foreach (StiPage page in pages)Add(page);
		}

		
		public void AddRange(StiPagesCollection pages)
		{
            lock (((ICollection)pages).SyncRoot)
			foreach (StiPage page in pages)Add(page);
				
		}

		
		public bool Contains(StiPage page)
		{
			return List.Contains(page);
		}
		
		
		public int IndexOf(StiPage page)
		{
			return List.IndexOf(page);
		}

		
		public void Insert(int index, StiPage page)
		{
			List.Insert(index, page);

            //InvokePageAdded(page, EventArgs.Empty);   //already in OnInsertComplete
		}

		
		public void Remove(StiPage page)
		{
			List.Remove(page);
		}


		/// <summary>
		/// Internal use only.
		/// </summary>
		public void Remove(int startIndex, int endCount)
		{
			while (endCount > 0)
			{
				RemoveAt(Count - 1);
				endCount--;
			}

			while (startIndex > 0)
			{
				RemoveAt(0);
				startIndex--;
			}
		}

        internal StiPage GetPageWithoutCache(int pageIndex)
        {
            return List[pageIndex] as StiPage;
        }
		
		
		public StiPage this[int index]
		{
			get
			{
				StiPage page = List[index] as StiPage;
				GetPage(page);
				return page;
			}
			set
			{
				GetPage(value);
				List[index] = value;
			}
		}

		
		public StiPage this[string name]
		{
			get
			{
				name = name.ToLower(CultureInfo.InvariantCulture);
                lock (((ICollection)List).SyncRoot)
				foreach (StiPage page in List)
				{
					if (page.Name.ToLower(CultureInfo.InvariantCulture) == name)
					{
						GetPage(page);
						return page;
					}
				}
				return null;
			}
			set
			{
				name = name.ToLower(CultureInfo.InvariantCulture);
				for (int index = 0; index < List.Count; index++)				
				{
					StiComponent comp = List[index] as StiComponent;
					
					if (comp.Name.ToLower(CultureInfo.InvariantCulture) == name)
					{
						GetPage(value);
						List[index] = value;
						return;
					}
				}
				Add(value);
			}
		}

		
		public StiPage[] Items
		{
			get
			{
				return (StiPage[])InnerList.ToArray(typeof(StiPage));
			}
		}


		public void SortByPriority()
		{
            lock (((ICollection)this).SyncRoot)
			foreach (StiPage page in this)
			{
				page.Components.SortByPriority();
			}
		}

        protected override void OnClear()
        {
            base.OnClear();
        }

        protected override void OnInsertComplete(int index, object value)
        {
            base.OnInsertComplete(index, value);
            InvokePageAdded(value, EventArgs.Empty);
        }

        protected override void OnRemoveComplete(int index, object value)
        {
            base.OnRemoveComplete(index, value);
            InvokePageRemoved(value, EventArgs.Empty);
        }

        protected override void OnClearComplete()
        {
            base.OnClearComplete();
            InvokePageCleared(this, EventArgs.Empty);
        }

        public StiComponent GetComponentByName(string componentName)
        {
            lock (((ICollection)this).SyncRoot)
                foreach (StiPage page in this)
                {
                    StiComponent comp = page.Components.GetComponentByName(componentName, page);
                    if (comp != null) return comp;
                }

            return null;
        }

        private static void SetParent(StiContainer parent)
        {
            lock (((ICollection)parent.Components).SyncRoot)
                foreach (StiComponent comp in parent.Components)
                {
                    comp.Parent = parent;

                    StiContainer cont = comp as StiContainer;
                    if (cont != null) SetParent(cont);
                }
        }		
		#endregion

		#region IStiStateSaveRestore
		/// <summary>
		/// Saves the current state of an object.
		/// </summary>
		/// <param name="stateName">A name of the state being saved.</param>
		public virtual void SaveState(string stateName)
		{
            lock (((ICollection)this).SyncRoot)
			foreach (StiPage page in this)page.SaveState(stateName);
		}

		/// <summary>
		/// Restores the earlier saved object state.
		/// </summary>
		/// <param name="stateName">A name of the state being restored.</param>
		public virtual void RestoreState(string stateName)
		{
            lock (((ICollection)this).SyncRoot)
			foreach (StiPage page in this)page.RestoreState(stateName);
		}


		/// <summary>
		/// Clear all earlier saved object states.
		/// </summary>
		public virtual void ClearAllStates()
		{
            lock (((ICollection)this).SyncRoot)
			foreach (StiPage page in this)page.ClearAllStates();
		}
		#endregion

		#region Properties
        private bool canUseCacheMode = false;
        public bool CanUseCacheMode
        {
            get
            {
                return canUseCacheMode;
            }
            set
            {
                 canUseCacheMode = value;
            }
        }

		private StiReport report;
		public StiReport Report 
		{
			get
			{
				return report;
			}
			set
			{
				report = value;
			}
		}

		private bool cacheMode = false;
		public bool CacheMode
		{
			get
			{
				return cacheMode;
			}
			set
			{
				cacheMode = value;
			}
		}
		#endregion

		#region Fields.ReportCache
		internal StiSerializing sr = null;
        private List<PageStore> QuickCachedPages = null;
		internal List<StiPage> NotCachedPages = null;
        private int amountOfProcessedPagesForStartGCCollect = 0;
        private CacheL2 cacheL2 = null;
        #endregion

		#region Methods.ReportCache
		internal void AddPageToQuickCache(StiPage page, bool savePage)
		{
            #if DebugCache
            AddMessageToLog(string.Format("AddPageToQuickCache: {0}, save={1}", this.List.IndexOf(page), savePage));
            #endif

            if (QuickCachedPages == null) 
                QuickCachedPages = new List<PageStore>();

			//QuickCachedPages.Add(new PageStore(page, savePage));
            PageStore findedStore = null;
            foreach (var store in QuickCachedPages)
            {
                if (store.Page == page)
                {
                    findedStore = store;
                    break;
                }
            }
            if (findedStore == null)
            {
                QuickCachedPages.Add(new PageStore(page, savePage));
            }
            else
            {
                QuickCachedPages.Remove(findedStore);
                QuickCachedPages.Add(new PageStore(page, savePage || findedStore.SavePage));
            }

            if (QuickCachedPages.Count > StiOptions.Engine.ReportCache.AmountOfQuickAccessPages)
            {
                var prevPage = QuickCachedPages[0];
                bool needClearPage = true;
                if (cacheL2 == null)
                {
                    if (prevPage.SavePage)
                    {
                        SavePage(prevPage.Page);
                    }
                }
                else
                {
                    if (prevPage.SavePage)
                    {
                        cacheL2.AddPageToProcess(prevPage.Page, this.IndexOf(prevPage.Page), true);
                        needClearPage = false;
                    }
                }
                if (needClearPage)
                {
                    //prevPage.Page.Components.Clear();
                    prevPage.Page.DisposeImagesAndClearComponents();
                }
                QuickCachedPages.RemoveAt(0);

                if ((cacheL2 == null) && StiOptions.Engine.ReportCache.AllowGCCollect)
                {
                    int amount = StiOptions.Engine.ReportCache.AmountOfProcessedPagesForStartGCCollect;
                    //if ((SavePageBlockToStorageIncoming != null) || (SavePageToCache != null)) amount *= 10
                    amountOfProcessedPagesForStartGCCollect++;
                    if (amountOfProcessedPagesForStartGCCollect >= amount)
                    {
                        amountOfProcessedPagesForStartGCCollect = 0;
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        GC.Collect();
                    }
                }

            }
		}
		
		/// <summary>
		/// Internal use only.
		/// </summary>
		public bool IsNotSavedPage(StiPage page)
		{
            lock (((ICollection)QuickCachedPages).SyncRoot)
			foreach (var store in QuickCachedPages)
			{
				if (store.Page == page)return store.IsNotSaved;
			}
			return false;
		}

        /// <summary>
        /// Internal use only.
        /// </summary>
        public void MarkPageAsNotSaved(StiPage page)
        {
            lock (((ICollection)QuickCachedPages).SyncRoot)
                foreach (var store in QuickCachedPages)
                {
                    if (store.Page == page) store.SavePage = true;
                }
        }

		/// <summary>
		/// Internal use only.
		/// </summary>
		public void GetPage(StiPage page)
		{
            if (LoadPageFromServer != null)
            {
                if (page.Components.Count == 0)
                    LoadPageFromServer(page, EventArgs.Empty);
                return;
            }

			if (report != null && (!CacheMode || (report.ReportPass == StiReportPass.First)))return;

            if (cacheL2 != null)
            {
                cacheL2.CheckForPageInPagesToSave(this.IndexOf(page));
            }

			if (page.Components.Count != 0) return;

            #region EngineV1
            if (report != null && report.EngineV1 != null && report.EngineV1.ProgressHelper != null && report.EngineV1.ProgressHelper.AllowCachedPagesCache)
            {
                report.InvokeRendering();
                report.EngineV1.ProgressHelper.ProcessInCache(page);
            }
            #endregion

            #region EngineV2
            if (report != null && report.Engine != null && report.Engine.ProgressHelper != null && report.Engine.ProgressHelper.AllowCachedPagesCache)
            {
                report.InvokeRendering();
                report.Engine.ProgressHelper.ProcessInCache(page);
            }
            #endregion

			LoadPage(page);

			AddPageToQuickCache(page, false);
		}

		/// <summary>
		/// Internal use only.
		/// </summary>		
		public void SavePage(StiPage page, bool clearContent = true)
		{
            #if DebugCache
            AddMessageToLog(string.Format("SavePage: {0}, clear={1}", this.List.IndexOf(page), clearContent));
            #endif

            if (page.Report != null && page.Report.EngineVersion == StiEngineVersion.EngineV2)
            {
                StiRenderProviderV2.ProcessPageToCache(page.Report, page, true);
            }
            
            if (page != null && report.RenderedPages.NotCachedPages != null)
			{
				int indexPage = report.RenderedPages.NotCachedPages.IndexOf(page);
				if (indexPage != -1)
				{
					report.RenderedPages.NotCachedPages.RemoveAt(indexPage);
				}
			}

            //optimization - on the first pass, all the pages have been already cleaned
            if ((page.Report != null) && (page.Report.ReportPass == StiReportPass.First)) return;

			if (sr == null) sr = new StiSerializing(new StiReportObjectStringConverter());
                        
			string path = StiReportCache.GetPageCacheName(report.ReportCachePath, page.CacheGuid);
            
            #region Save page
            page.Report = null;

            if (this.SavePageToCache != null)
            {
                SavePageToCache(page, new StiSaveLoadPageEventArgs(page, this.IndexOf(page), path));
            }
            else
            {
                //init ReportCachePath if it not initialized before
                if (string.IsNullOrEmpty(report.ReportCachePath))
                {
                    report.ReportCachePath = StiReportCache.CreateNewCache();
                    StiOptions.Engine.GlobalEvents.InvokeReportCacheCreated(report, EventArgs.Empty);
                    if (cacheL2 != null) cacheL2.ReportCachePath = report.ReportCachePath;

                    path = StiReportCache.GetPageCacheName(report.ReportCachePath, page.CacheGuid);
                }

                if (cacheL2 != null)
                {
                    cacheL2.AddPageToProcess(page, this.IndexOf(page), clearContent);
                }
                else
                {
                    StiFileUtils.ProcessReadOnly(path);
                    using (Stream stream = new FileStream(path, FileMode.Create, FileAccess.Write))
                    {
                        SerializePage(stream, sr, page);
                        stream.Close();
                    }
                }
            }

            page.Report = report;
            #endregion
        }

        internal void SerializePage(Stream stream, StiSerializing sr, StiPage page, bool convertToHInches = false)
        {
            var oldReport = page.Report;
            page.Report = null;
            if (convertToHInches && report.ReportUnit != StiReportUnitType.HundredthsOfInch)
            {                
                page.Convert(report.Unit, Stimulsoft.Report.Units.StiUnit.HundredthsOfInch);
            }

            StiXmlDocumentSLService.RegPropertyNames(sr);

            sr.SortProperties = false;
            sr.CheckSerializable = true;
            sr.IgnoreSerializableForContainer = true;
            sr.Serialize(page, stream, "StiCache", StiSerializeTypes.SerializeToDocument);
            stream.Flush();

            page.Report = oldReport;
        }

		/// <summary>
		/// Internal use only.
		/// </summary>
		public void LoadPage(StiPage page)
		{
            #if DebugCache
            AddMessageToLog(string.Format("LoadPage: {0}", this.List.IndexOf(page)));
            #endif
            
            if (sr == null) sr = new StiSerializing(new StiReportObjectStringConverter(true));
            
			string path = StiReportCache.GetPageCacheName(report.ReportCachePath, page.CacheGuid);

            #region LoadPageFromCache
            if (this.LoadPageFromCache != null)
            {
                LoadPageFromCache(page, new StiSaveLoadPageEventArgs(page, this.IndexOf(page), path));
            }
            else
            {
                if (cacheL2 != null)
                {
                    byte[] pageData = cacheL2.GetPage(this.IndexOf(page));
                    if (pageData != null && pageData.Length != 0)
                    {
                        using (MemoryStream ms = new MemoryStream(pageData))
                        {
                            DeserializePage(ms, sr, page);
                        }
                    }
                }
                else
                {
                    if (File.Exists(path))
                    {
                        using (Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                        {
                            DeserializePage(stream, sr, page);
                        }
                    }
                }
            }
            #endregion

            page.Report = report;

			SetParent(page);
			StiComponentsCollection comps = page.GetComponents();
            lock (((ICollection)comps).SyncRoot)
			foreach (StiComponent comp in comps)
			{
				comp.Page = page;
			}
		}

        private void DeserializePage(Stream stream, StiSerializing sr, StiPage page)
        {
            StiXmlDocumentSLService.RegPropertyNames(sr);
            sr.IgnoreSerializableForContainer = true;
            sr.Deserialize(page, stream, "StiCache");
            stream.Flush();
            stream.Close();
        }

   		/// <summary>
		/// Internal use only.
		/// </summary>
        public void SaveQuickPagesToCache()
        {
            if (QuickCachedPages == null) return;
            lock (((ICollection)QuickCachedPages).SyncRoot)
            {
                foreach (var store in QuickCachedPages)
                {
                    if (store.IsNotSaved)
                    {
                        if (cacheL2 != null)
                        {
                            cacheL2.CheckForPageInPagesToSave(this.List.IndexOf(store.Page));
                        }
                        SavePage(store.Page, false);
                        //store.Page.Components.Clear();
                        //QuickCachedPages.Remove(store.Page);
                        store.SavePage = false;
                    }
                }
            }
        }

        public void Flush(bool final = false)
        {
            #if DebugCache
            AddMessageToLog(string.Format("Flush: {0}", final));
            #endif

            if (cacheL2 != null)
            {
                if (final) SaveQuickPagesToCache();
                cacheL2.Flush(final);
            }
        }
		#endregion

        #region Events
        #region SavePageToCache
        public event StiSaveLoadPageEventHandler SavePageToCache;
        #endregion

        #region LoadPageFromCache
        public event StiSaveLoadPageEventHandler LoadPageFromCache;
        #endregion

        #region LoadPageFromServer
        public event StiLoadPageFromServerEventHandler LoadPageFromServer;
        #endregion

        #region PageAdded
        public event EventHandler PageAdded;

        protected virtual void OnPageAdded(EventArgs e)
        {
        }

        public virtual void InvokePageAdded(object sender, EventArgs e)
        {
            OnPageAdded(e);
            if (PageAdded != null) PageAdded(sender, e);
        }
        #endregion

        #region PageRemoved
        public event EventHandler PageRemoved;

        protected virtual void OnPageRemoved(EventArgs e)
        {
        }

        public virtual void InvokePageRemoved(object sender, EventArgs e)
        {
            OnPageRemoved(e);
            if (PageRemoved != null) PageRemoved(sender, e);
        }
        #endregion

        #region PageCleared
        public event EventHandler PageCleared;

        protected virtual void OnPageCleared(EventArgs e)
        {
        }

        public virtual void InvokePageCleared(object sender, EventArgs e)
        {
            OnPageCleared(e);
            if (PageCleared != null) PageCleared(sender, e);
            if (QuickCachedPages != null) QuickCachedPages.Clear();
            if (cacheL2 != null)
            {
                cacheL2.Clear();
            }
        }
        #endregion

        #region SavePageBlockToStorageIncoming
        public CacheL2.Block.SavePageBlockToStorageDelegate SavePageBlockToStorageIncoming;
        #endregion

        #region LoadPageBlockFromStorageIncoming
        public CacheL2.Block.LoadPageBlockFromStorageDelegate LoadPageBlockFromStorageIncoming;
        #endregion

        internal static void CopyEventsOfPagesCollection(StiPagesCollection sourcePages, StiPagesCollection destinationPages)
        {
            destinationPages.LoadPageFromCache = sourcePages.LoadPageFromCache;
            destinationPages.SavePageToCache = sourcePages.SavePageToCache;
        }
        #endregion

        public StiPagesCollection(StiReport report, StiPagesCollection originalPages)
        {
            this.report = report;
            //if ((StiOptions.Engine.ReportCache.ThreadMode == StiReportCacheThreadMode.On) || (StiOptions.Engine.ReportCache.ThreadMode == StiReportCacheThreadMode.Auto && Environment.ProcessorCount > 1))
            //{
                this.cacheL2 = originalPages.cacheL2;
            //}
            this.LoadPageFromServer = originalPages.LoadPageFromServer;

            StiPagesCollection.CopyEventsOfPagesCollection(originalPages, this);
            QuickCachedPages = originalPages.QuickCachedPages;
        }

        public StiPagesCollection(StiReport report)
        {
            this.report = report;
            if (report != null && report.ReportCacheMode != StiReportCacheMode.Off)
            {
                CheckCacheL2();
            }
        }
	}
}