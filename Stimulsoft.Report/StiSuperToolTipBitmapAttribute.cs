#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Drawing;
using System.Linq;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Report
{
	/// <summary>
	/// Class describes an attribute that is used for assinging SuperToolTip bitmap to custom component.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
	public sealed class StiSuperToolTipBitmapAttribute : Attribute
	{
        #region Methods
        /// <summary>
        /// Returns a path to SuperToolTip image according to its type.
        /// </summary>
        /// <param name="type">Component type.</param>
        /// <returns>Path to Image.</returns>
        public static string GetImagePath(Type type)
        {
            var attributes = (StiSuperToolTipBitmapAttribute[]) 
                type.GetCustomAttributes(typeof(StiSuperToolTipBitmapAttribute), true);

            return attributes.FirstOrDefault()?.BitmapName;
        }

		/// <summary>
        /// Returns a SuperToolTip  image according to component type.
		/// </summary>
		/// <param name="type">Component type.</param>
        /// <returns>SuperToolTip image.</returns>
		public static Bitmap GetImage(Type type)
		{
            var attributes = (StiSuperToolTipBitmapAttribute[])
                type.GetCustomAttributes(typeof(StiSuperToolTipBitmapAttribute), true);

		    return attributes.FirstOrDefault()?.GetImage();
        }

		/// <summary>
        /// Returns an SuperToolTip image of component type.
		/// </summary>
        /// <returns>SuperToolTip image.</returns>
		public Bitmap GetImage()
		{
			return StiImageUtils.GetImage(Type, BitmapName);
		}
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a path to the SuperToolTip bitmap in resources.
        /// </summary>
        public string BitmapName { get; set; }

	    /// <summary>
        /// Gets or sets component type to which a bitmap is assign to.
        /// </summary>
        public Type Type { get; set; }
        #endregion

        /// <summary>
        /// Creates a new attribute of the type StiSuperToolTipBitmapAttribute.
        /// </summary>
        /// <param name="type">Path to the SuperToolTip bitmap in resources.</param>
        /// <param name="bitmapName">Component type to which a bitmap is assign to.</param>
        public StiSuperToolTipBitmapAttribute(Type type, string bitmapName)
		{
			this.Type = type;
			this.BitmapName = bitmapName;
		}
	}
}
