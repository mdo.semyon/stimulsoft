﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Collections;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.Design;

namespace Stimulsoft.Report.Design
{
    public class StiInplaceEditor
    {
        #region Consts
        public static int ButtonSize = 28;
        public static int ButtonMargin = 10;
        #endregion

        #region Properties
        public static bool IsEditorActivated { get; set; }
        #endregion

        #region Fields
        private static Hashtable isSelectedHash = new Hashtable();
        #endregion

        #region Methods
        public static bool IsSelected(StiComponent component)
        {
            component.Guid = StiKeyHelper.GetOrGeneratedKey(component.Guid);
            return isSelectedHash[component.Guid] != null;
        }

        public static void SetSelected(StiComponent component)
        {
            component.Guid = StiKeyHelper.GetOrGeneratedKey(component.Guid);
            isSelectedHash[component.Guid] = component.Guid;
        }

        public static void ResetSelected(StiComponent component)
        {
            component.Guid = StiKeyHelper.GetOrGeneratedKey(component.Guid);
            if (isSelectedHash[component.Guid] != null) isSelectedHash.Remove(component.Guid);
        }

        public static bool IsLeftLocation(StiComponent component)
        {
            return component.Right + ButtonMargin + ButtonSize > component.Page.Width;
        }

        public static RectangleD GetButtonEditorRect(RectangleD rect, StiComponent component)
        {
            return GetButtonEditorRect(rect, IsLeftLocation(component));
        }

        public static RectangleD GetButtonEditorRect(RectangleD rect, bool isLeftLocation)
        {
            if (isLeftLocation)
                return new RectangleD(rect.Left - ButtonMargin - ButtonSize, rect.Top, ButtonSize, ButtonSize);
            else
                return new RectangleD(rect.Right + ButtonMargin, rect.Top, ButtonSize, ButtonSize);
        }

        public static void OnClick(StiComponent component)
        {
            var componentDesigner = StiComponentDesigner.GetComponentDesigner(component.Report.Designer, component.GetType());
            if (componentDesigner != null)
            {
                if (StiRestrictionsHelper.IsAllowChange(component))
                {
                    component.Report.Designer.UndoRedoSave(component.LocalizedName, true);
                    componentDesigner.OnDoubleClick(component);
                }
            }
        }
        #endregion
    }
}