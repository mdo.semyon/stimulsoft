#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.Table;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Dialogs;
using Stimulsoft.Report.Units;
using System;
using System.Collections;
using System.Drawing;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Design
{
    public class StiTableHelper
	{
		#region Methods
		public static bool IsAllowUseInTableMode(StiComponent comp)
		{
			if (comp is StiPage)return false;
			//if (comp is StiBand)return false;
			if (comp.Parent is IStiCrossTab)return false;
			return StiRestrictionsHelper.IsAllowChangePosition(comp);			
		}

		
		public static int GetSelectedCount(StiComponentsCollection comps)
		{
            if (comps == null) return 0;
			int count = 0;

			foreach (StiComponent comp in comps)
			{
				if (IsAllowUseInTableMode(comp))count ++;
			}
			return count;
		}

        public static void GetSelectedLines(IStiDesignerBase designer, ref Hashtable xx, ref Hashtable yy, bool isPaint)
        {
            GetSelectedLines(designer, ref xx, ref yy, isPaint, isPaint);
        }

        public static void GetSelectedLines(IStiDesignerBase designer, ref Hashtable xx, ref Hashtable yy, bool isPaint, bool useZoom)
		{
			RectangleD rect = StiTableHelper.GetSelectedRectangle(designer, isPaint, useZoom);
			
			xx[rect.X] = rect.X;
			yy[rect.Y] = rect.Y;
			xx[rect.Right] = rect.Right;
			yy[rect.Bottom] = rect.Bottom;

			foreach (StiComponent comp in designer.SelectedComponentsOnPage)
			{
				if (IsAllowUseInTableMode(comp))
				{
					RectangleD compRect = comp.GetPaintRectangle(isPaint, useZoom);

					xx[compRect.X] = compRect.X;
					yy[compRect.Y] = compRect.Y;
					xx[compRect.Right] = compRect.Right;
					yy[compRect.Bottom] = compRect.Bottom;
				}
			}
		}
		
		
		public static bool IsTableMode(StiComponent component)
		{
			return IsTableMode(component.Page.Report.Designer);
		}

		
		public static bool IsTableMode(IStiDesignerBase designer)
		{
			if (designer == null)return false;
			
			return GetSelectedCount(designer.SelectedComponentsOnPage) > 1;
		}


        public static RectangleD GetSelectedRectangle(IStiDesignerBase designer)
		{
			return GetSelectedRectangle(designer, false);
		}

        public static RectangleD GetSelectedRectangle(IStiDesignerBase designer, bool isPaintRect)
		{			
			return GetSelectedRectangle(designer, isPaintRect, designer.SelectedComponentsOnPage);
		}

        public static RectangleD GetSelectedRectangle(IStiDesignerBase designer, bool isPaintRect, bool useZoom)
        {
            return GetSelectedRectangle(designer, isPaintRect, useZoom, designer.SelectedComponentsOnPage);
        }

        public static RectangleD GetSelectedRectangle(IStiDesignerBase designer, bool isPaintRect, StiComponentsCollection comps)
        {
            return GetSelectedRectangle(designer, isPaintRect, isPaintRect, comps);
        }

        public static RectangleD GetSelectedRectangle(IStiDesignerBase designer, bool isPaintRect, bool useZoom, StiComponentsCollection comps)
		{
			RectangleD rect = RectangleD.Empty;

			foreach (StiComponent comp in comps)
			{
				if (IsAllowUseInTableMode(comp))
				{
					RectangleD compRect = comp.GetPaintRectangle(isPaintRect, useZoom);
					if (rect.IsEmpty)rect = compRect;
					else rect = rect.FitToRectangle(compRect);
				}
			}
			return rect;
		}


		public static double GetDistForResize(StiComponent comp)
		{
			return comp.Page.Unit.ConvertFromHInches(4d);
		}


		private RectangleD GetRect(StiComponent comp)
		{
			return comp.GetPaintRectangle(false, false, false);
		}


		private void SetRect(StiComponent comp, RectangleD rect)
		{
			comp.SetPaintRectangle(rect);
		}


		private double ConvertValue(double value)
		{
			value = designer.Report.GetCurrentPage().Unit.ConvertFromHInches(
				value / designer.Report.GetCurrentPage().Zoom);

			value = StiAlignValue.AlignToGrid(value, 
				designer.Report.GetCurrentPage().GridSize, 
				designer.Report.Info.AlignToGrid && (!altKeyMode));
			
			return value;
		}

		
		private double Align(double value)
		{
			if (altKeyMode)return value;

			return StiAlignValue.AlignToGrid(value, designer.Report.GetCurrentPage().GridSize, 
				designer.Report.Info.AlignToGrid);
		}


		private decimal Round(double value)
		{
			if (designer.Report.GetCurrentPage() is IStiForm)return Math.Round((decimal)value, 0);
			#region DBS
		    if (designer.Report.GetCurrentPage() is IStiDashboard) return Math.Round((decimal)value, 0);
			#endregion
            StiUnit unit = designer.Report.GetCurrentPage().Unit;

			if (unit is StiCentimetersUnit || unit is StiInchesUnit)return Math.Round((decimal)value, 1);
			return Math.Round((decimal)value, 0);
		}


		private bool IsContain(double value1, double value2, double dist)
		{
			return (value1 > (value2 - dist)) && (value1 < (value2 + dist));
		}

		
		public void ProcessResizeColumns()
		{
            Point currentPos = Cursor.Position;
			double currentX = ConvertValue(currentPos.X);
			double storedX = ConvertValue(storedPosition.X);
            double offset = (currentX - storedX) * SystemWinApi.GetSystemScale();

            StiResizeType resizeType = StiResizeType.Resize;
            object[] objs = designer.Report.GetSelected();
            if (!designer.Report.IsPageDesigner)
            {
                bool contentTableCell = false;
                for (int indexEl = 0; indexEl < objs.Length; indexEl++)
                {
                    if (objs[indexEl] is IStiTableCell)
                    {
                        contentTableCell = true;
                        resizeType = StiResizeType.TwoResize;
                        break;
                    }
                }
                if (!contentTableCell)
                    resizeType = IsControlKeyPressed ? StiResizeType.TwoResize : StiResizeType.Resize;
            }
			
			ResizeColumns(mouseX, offset, resizeType);
		}
		
		
		public void ResizeColumns(double lineX, double offset, StiResizeType resizeType)
		{
			double dist = GetDistForResize(designer.Report.GetCurrentPage());
			RectangleD selectedRect = GetSelectedRectangle(designer);
			
			#region Check range
			foreach (StiComponent component in designer.SelectedComponentsOnPage)
			{
                #region TableCell
                if (!designer.Report.IsPageDesigner)
                {
                    IStiTableCell cell = component as IStiTableCell;
                    if (cell != null)
                    {
                        StiTable table = designer.SelectedComponentsOnPage[0] as StiTable;
                        if (table == null) continue;
                        else
                        {
                            if ((table.Components.Count + 1) != designer.SelectedComponentsOnPage.Count)
                                continue;
                        }
                    }
                }
                #endregion
				if (!IsAllowUseInTableMode(component))continue;

				RectangleD clientRect = component.ClientRectangle;
				RestoreComponentLocation(component);

				try
				{
					RectangleD rect = GetRect(component);

					if (IsContain(rect.Right, lineX, dist))
					{
						if ((component.Width + offset) < 0)offset = -component.Width;
						continue;
					}
					
					if (rect.Left < lineX && rect.Right > lineX && resizeType == StiResizeType.Resize)
					{
						if ((component.Width + offset) < 0)offset = -component.Width;
						continue;
					}

					if (IsContain(rect.Left, lineX, dist) && resizeType == StiResizeType.TwoResize)
					{
						if ((component.Width - offset) < 0)offset = component.Width;
						continue;
					}
				}
				finally
				{
					component.ClientRectangle = clientRect;
				}
			}
			
			#endregion

			RestoreComponentsLocation();

			#region Change Position
			foreach (StiComponent component in designer.SelectedComponentsOnPage)
			{
                #region TableCell
                IStiTableCell cell = component as IStiTableCell;
                if (cell != null)
                {
                    StiTable table = designer.SelectedComponentsOnPage[0] as StiTable;
                    if (table == null) continue;
                    else
                    {
                        if ((table.Components.Count + 1) != designer.SelectedComponentsOnPage.Count)
                            continue;
                    }
                }
                #endregion
				if (!IsAllowUseInTableMode(component))continue;

				RectangleD rect = GetRect(component);

				if (IsContain(rect.Right, lineX, dist))
				{
					component.Width += offset;
					component.Width = Align(component.Width);
					continue;
				}
					
				if (rect.Left >= (lineX - dist) && resizeType == StiResizeType.Resize)
				{
					component.Left += offset;
					component.Left = Align(component.Left);
					continue;
				}

				if (rect.Left < lineX && rect.Right > lineX && resizeType == StiResizeType.Resize)
				{
					component.Width += offset;
					component.Width = Align(component.Width);
					continue;
				}

				if (IsContain(rect.Left, lineX, dist) && resizeType == StiResizeType.TwoResize)
				{
					component.Left += offset;
					component.Width -= offset;
					component.Left = Align(component.Left);
					component.Width = Align(component.Width);
					continue;
				}
			}
			#endregion
		}


		public void ProcessResizeRows()
		{
			Point currentPos = Cursor.Position;
			double currentY = ConvertValue(currentPos.Y);
			double storedY = ConvertValue(storedPosition.Y);
			double offset = (currentY - storedY) * SystemWinApi.GetSystemScale();

			StiResizeType resizeType = IsControlKeyPressed ? StiResizeType.TwoResize : StiResizeType.Resize;
						
			ResizeRows(mouseY, offset, resizeType);
		}
		
		
		public void ResizeRows(double lineY, double offset, StiResizeType resizeType)
		{
			double dist = GetDistForResize(designer.Report.GetCurrentPage());
			RectangleD selectedRect = GetSelectedRectangle(designer);
			
			#region Check range
			foreach (StiComponent component in designer.SelectedComponentsOnPage)
			{
                #region TableCell
                if (!designer.Report.IsPageDesigner)
                {
                    IStiTableCell cell = component as IStiTableCell;
                    if (cell != null)
                    {
                        StiTable table = designer.SelectedComponentsOnPage[0] as StiTable;
                        if (table == null) continue;
                        else
                        {
                            if ((table.Components.Count + 1) != designer.SelectedComponentsOnPage.Count)
                                continue;
                        }
                    }
                }
                #endregion
				if (!IsAllowUseInTableMode(component))continue;

				RectangleD clientRect = component.ClientRectangle;
				RestoreComponentLocation(component);

				try
				{
					RectangleD rect = GetRect(component);

					if (IsContain(rect.Bottom, lineY, dist))
					{
						if ((component.Height + offset) < 0)offset = -component.Height;
						continue;
					}
					
					if (rect.Top < lineY && rect.Bottom > lineY && resizeType == StiResizeType.Resize)
					{
						if ((component.Height + offset) < 0)offset = -component.Height;						
						continue;
					}

					if (IsContain(rect.Top, lineY, dist) && resizeType == StiResizeType.TwoResize)
					{
						if ((component.Height - offset) < 0)offset = component.Height;
						continue;
					}
				}
				finally
				{
					component.ClientRectangle = clientRect;
				}
			}
			
			#endregion

			RestoreComponentsLocation();			

			#region Change position
			foreach (StiComponent component in designer.SelectedComponentsOnPage)
			{
                #region TableCell
                IStiTableCell cell = component as IStiTableCell;
                if (cell != null)
                {
                    StiTable table = designer.SelectedComponentsOnPage[0] as StiTable;
                    if (table == null) continue;
                    else
                    {
                        if ((table.Components.Count + 1) != designer.SelectedComponentsOnPage.Count)
                            continue;
                    }
                }
                #endregion
				if (!IsAllowUseInTableMode(component))continue;

				RectangleD rect = GetRect(component);

				if (IsContain(rect.Bottom, lineY, dist))
				{
					component.Height += offset;					
					component.Height = Align(component.Height);
					continue;
				}
					
				if (rect.Top >= (lineY - dist) && resizeType == StiResizeType.Resize)
				{
					component.Top += offset;
					component.Top = Align(component.Top);
					continue;
				}

				if (rect.Top < lineY && rect.Bottom > lineY && resizeType == StiResizeType.Resize)
				{
					component.Height += offset;
					component.Height = Align(component.Height);
					continue;
				}

				if (IsContain(rect.Top, lineY, dist) && resizeType == StiResizeType.TwoResize)
				{
					component.Top += offset;
					component.Height -= offset;
					component.Top = Align(component.Top);
					component.Height = Align(component.Height);
					continue;
				}
			}
			#endregion
		}


		public void ProcessResizeTable(bool resizeHorizontally, bool resizeVertically)
		{
			RectangleD selectedRect = GetSelectedRectangle(designer);
			Point currentPos = Cursor.Position;

            #region Check inversion
            bool invertX = false;
			bool invertY = false;

			switch (designer.Report.Info.CurrentAction)
			{
				case StiAction.SizeLeft:
				case StiAction.SizeLeftTop:
				case StiAction.SizeLeftBottom:
					invertX = true;
					break;
			}

			switch (designer.Report.Info.CurrentAction)
			{
				case StiAction.SizeLeftTop:
				case StiAction.SizeTop:
				case StiAction.SizeRightTop:
					invertY = true;
					break;
			}
			#endregion

			double currentX = ConvertValue(currentPos.X);
			double currentY = ConvertValue(currentPos.Y);
            double storedX = ConvertValue(storedPosition.X);
			double storedY = ConvertValue(storedPosition.Y);
			
			double offsetX;
			double offsetY;

			double startX;
			double startY;

			if (invertX)
			{
				offsetX = storedX - currentX;
				startX = selectedRect.Right;
			}
			else 
			{
				offsetX = currentX - storedX;
				startX = selectedRect.X;
			}

			if (invertY)
			{
				offsetY = storedY - currentY;
				startY = selectedRect.Bottom;
			}
			else 
			{
				offsetY = currentY - storedY;
				startY = selectedRect.Y;
			}

            var scale = SystemWinApi.GetSystemScale();
            offsetX *= scale;
            offsetY *= scale;

            double dpiX = ((storedSelectedRect.Width + offsetX) / storedSelectedRect.Width);
			double dpiY = ((storedSelectedRect.Height + offsetY) / storedSelectedRect.Height);

			if (dpiX < 0)return;
			if (dpiY < 0)return;

			RestoreComponentsLocation();
			
			ResizeTable(startX, startY, dpiX, dpiY, resizeHorizontally, resizeVertically, invertX, invertY);
		}
		
		
		public void ResizeTable(double startX, double startY, double dpiX, double dpiY, 
			bool resizeHorizontally, bool resizeVertically, bool invertX, bool invertY)
		{
            var x = new Hashtable();
			var y = new Hashtable();

			foreach (StiComponent component in designer.SelectedComponentsOnPage)
            {
                #region TableCell
                IStiTableCell cell = component as IStiTableCell;
                if (cell != null)
                {
                    StiTable table = designer.SelectedComponentsOnPage[0] as StiTable;
                    if (table == null) continue;
                    else
                    {
                        if ((table.Components.Count + 1) != designer.SelectedComponentsOnPage.Count)
                            continue;
                    }
                }
                #endregion

                if (!IsAllowUseInTableMode(component))continue;
				
				RectangleD rect = GetRect(component);
				RectangleD originalRect = rect;

				rect.Width *= dpiX;
				rect.Height *= dpiY;

				if (invertX)rect.X = startX - (startX - rect.X) * dpiX;
				else rect.X = startX + (rect.X - startX) * dpiX;
				
				if (invertY)rect.Y = startY - (startY - rect.Y) * dpiY;
				else rect.Y = startY + (rect.Y - startY) * dpiY;

				rect = rect.Normalize();
				rect = new RectangleD(Align(rect.X), Align(rect.Y), Align(rect.Width), Align(rect.Height));
				
				x[Round(originalRect.Left)] = rect.X;
				y[Round(originalRect.Top)] = rect.Y;
				x[Round(originalRect.Right)] = rect.Right;
				y[Round(originalRect.Bottom)] = rect.Bottom;
			}
			
			foreach (StiComponent component in designer.SelectedComponentsOnPage)
            {
                #region TableCell
                IStiTableCell cell = component as IStiTableCell;
                if (cell != null)
                {
                    StiTable table = designer.SelectedComponentsOnPage[0] as StiTable;
                    if (table == null) continue;
                    else
                    {
                        if ((table.Components.Count + 1) != designer.SelectedComponentsOnPage.Count)
                            continue;
                    }
                }
                #endregion

                if (!IsAllowUseInTableMode(component))continue;

				RectangleD originalRect = GetRect(component);

				object leftObject = x[Round(originalRect.Left)];
				object topObject = y[Round(originalRect.Top)];
				object rightObject = x[Round(originalRect.Right)];
				object bottomObject = y[Round(originalRect.Bottom)];

				if (leftObject != null && topObject != null && rightObject != null && bottomObject != null)
				{
					double left = (double)leftObject;
					double top = (double)topObject;
					double right = (double)rightObject;
					double bottom = (double)bottomObject;

					if (!resizeHorizontally)
					{
						left = originalRect.Left;
						right = originalRect.Right;
					}

					if (!resizeVertically)
					{
						top = originalRect.Top;
						bottom = originalRect.Bottom;
					}

					SetRect(component, new RectangleD(left, top, right - left, bottom - top));
				}
			}
		}

		
		public void SaveSelectedRectangle()
		{
			storedSelectedRect = GetSelectedRectangle(designer);
		}


		public void SaveComponentsLocation()
		{
			if (storedLocations == null)storedLocations = new Hashtable();
			else storedLocations.Clear();

			foreach (StiComponent comp in designer.SelectedComponentsOnPage)
			{
				if (!IsAllowUseInTableMode(comp))continue;

				storedLocations[comp] = comp.ClientRectangle;
			}
		}

		
		public void RestoreComponentsLocation()
		{
			if (storedLocations != null)
			{
				foreach (StiComponent comp in designer.SelectedComponentsOnPage)
				{
					if (!IsAllowUseInTableMode(comp))continue;

					RestoreComponentLocation(comp);
				}
			}
		}


		public void RestoreComponentLocation(StiComponent comp)
		{
			if (storedLocations[comp] is RectangleD)
			{
				RectangleD rect = (RectangleD)storedLocations[comp];
				comp.ClientRectangle = rect;
			}
		}


		public void SaveCursorLocation(int eX, int eY)
		{
			storedPosition = Cursor.Position;

			mouseX = designer.XToPage(eX);
			mouseY = designer.YToPage(eY);
		}

	
		public void Dispose()
		{
			if (storedLocations != null)
			{
				storedLocations.Clear();
				storedLocations = null;
			}
		}
		#endregion

		#region Properties
		public bool IsControlKeyPressed
		{
			get
			{
				return (Control.ModifierKeys & Keys.Control) > 0;
			}
		}
		#endregion

		#region Fields
        private IStiDesignerBase designer = null;
		private bool altKeyMode = false;
		private Hashtable storedLocations = null;
		private Point storedPosition = Point.Empty;
		private RectangleD storedSelectedRect = RectangleD.Empty;
		private double mouseX = 0;
		private double mouseY = 0;
		#endregion

        public StiTableHelper(IStiDesignerBase designer, bool altKeyMode)
		{
			this.designer = designer;
			this.altKeyMode = altKeyMode;
		}
	}
}
