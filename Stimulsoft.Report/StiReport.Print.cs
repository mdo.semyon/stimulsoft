﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Text;
using Stimulsoft.Base;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Viewer;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Print;

namespace Stimulsoft.Report
{
    public partial class StiReport
    {
        #region Methods.WinForms
        /// <summary>
        /// Prints the rendered report without print dialog. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="printerSettings">Specifies information about how a document is printed, including the printer that prints it.</param>
        public void Print(PrinterSettings printerSettings)
        {
            Print(this.PrinterSettings.ShowDialog, printerSettings);
        }

        /// <summary>
        /// Prints the rendered report. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="showPrintDialog">If a parameter is true then the print dialog will be shown.</param>
        /// <param name="printerSettings">Specifies information about how a document is printed, including the printer that prints it.</param>
        public void Print(bool showPrintDialog, PrinterSettings printerSettings)
        {
            Print(showPrintDialog, printerSettings.FromPage, printerSettings.ToPage, printerSettings.Copies, printerSettings);
        }

        /// <summary>
        /// Prints the rendered report. If the report is not rendered then its rendering starts.
        /// </summary>
        public void Print()
        {
            Print(this.PrinterSettings.ShowDialog, -1, -1, (short)PrinterSettings.Copies);
        }

        /// <summary>
        /// Prints the rendered report with the print dialog. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="fromPage">A number of the first page to print.</param>
        /// <param name="toPage">A number of the last page to print.</param>
        /// <param name="copies">A number of copies to print.</param>
		public void Print(int fromPage, int toPage, short copies)
        {
            Print(this.PrinterSettings.ShowDialog, fromPage, toPage, copies);
        }

        /// <summary>
        /// Prints a rendered report. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="showPrintDialog">If a parameter is true then the print dialog will be shown.</param>
        /// <param name="copies">A number of copies to print.</param>
		public void Print(bool showPrintDialog, short copies)
        {
            Print(showPrintDialog, -1, -1, copies);
        }

        /// <summary>
        /// Prints the rendered report. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="showPrintDialog">If a parameter is true then the print dialog will be shown.</param>
		public void Print(bool showPrintDialog)
        {
            Print(showPrintDialog, (short)PrinterSettings.Copies);
        }

        /// <summary>
        /// Prints the rendered report. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="showPrintDialog">If a parameter is true then the print dialog will be shown.</param>
        /// <param name="fromPage">A number of the first page to print.</param>
        /// <param name="toPage">A number of the last page to print.</param>
        /// <param name="copies">A number of copies to print.</param>
		public void Print(bool showPrintDialog, int fromPage, int toPage, short copies)
        {
            Print(showPrintDialog, fromPage, toPage, copies, null);
        }

        /// <summary>
        /// Prints the rendered report. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="showPrintDialog">If a parameter is true then the print dialog will be shown.</param>
        /// <param name="fromPage">A number of the first page to print.</param>
        /// <param name="toPage">A number of the last page to print.</param>
        /// <param name="copies">A number of copies to print.</param>
        /// <param name="printerSettings">Specifies information about how a document is printed, including the printer that prints it.</param>
		public void Print(bool showPrintDialog, int fromPage, int toPage, short copies, PrinterSettings printerSettings)
        {
            InvokePrinting();
            RegReportDataSources();
            if (!IsRendered) this.Render(true);

            StiLogService.Write(this.GetType(), "Printing report");

            try
            {
                var printProvider = new StiPrintProvider();

                var report = NeedsCompiling && CompiledReport != null ? CompiledReport : this;
                printProvider.Print(report, showPrintDialog, fromPage, toPage, copies, printerSettings);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Printing report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            InvokePrinted();
        }

        /// <summary>
        /// Prints the collection of reports. If the report from collection is not rendered then its rendering starts.
        /// </summary>
        /// <param name="reports">Collection of reports to print.</param>
		public static void PrintReports(List<StiReport> reports)
        {
            PrintReports(reports, true, 1);
        }

        /// <summary>
        /// Prints the collection of reports. If the report from collection is not rendered then its rendering starts.
        /// </summary>
        /// <param name="reports">Collection of reports to print.</param>
        /// <param name="showPrintDialog">If a parameter is true then the print dialog will be shown.</param>
        /// <param name="copies">A number of copies to print.</param>
		public static void PrintReports(List<StiReport> reports, bool showPrintDialog, short copies)
        {
            PrintReports(reports, showPrintDialog, copies, null);
        }

        /// <summary>
        /// Prints the collection of reports. If the report from collection is not rendered then its rendering starts.
        /// </summary>
        /// <param name="reports">Collection of reports to print.</param>
        /// <param name="showPrintDialog">If a parameter is true then the print dialog will be shown.</param>
        /// <param name="copies">A number of copies to print.</param>
        /// <param name="printerSettings">Specifies information about how a document is printed, including the printer that prints it.</param>        
		public static void PrintReports(List<StiReport> reports, bool showPrintDialog, short copies, PrinterSettings printerSettings)
        {
            var tempReport = StiActivator.CreateObject(StiOptions.Engine.BaseReportType) as StiReport;
            tempReport.IsRendered = true;
            tempReport.NeedsCompiling = false;
            tempReport.RenderedPages.Clear();
            foreach (StiReport report in reports)
            {
                if (!report.IsRendered) report.Render(true);
                foreach (StiPage page in report.RenderedPages)
                {
                    tempReport.RenderedPages.Add(page);
                }
            }
            tempReport.Print(showPrintDialog, copies, -1, -1, printerSettings);

            foreach (StiReport report in reports)
            {
                foreach (StiPage page in report.RenderedPages)
                {
                    page.Report = report;
                }
            }
        }
        #endregion

        #region Methods.WPF
        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
		public void PrintWithWpf()
        {
            PrintWithWpf(true);
        }

        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="printerName">A name of a printer that will be used for printing.</param>
		public void PrintWithWpf(string printerName)
        {
            PrintWithWpf(true, -1, printerName);
        }

        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="printTicket">Specifies information about how a document is printed.</param>
		public void PrintWithWpf(object printTicket)
        {
            PrintWithWpf(printTicket, true);
        }

        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
		public void PrintWithWpf(bool showPrintDialog)
        {
            PrintWithWpf(null, showPrintDialog);
        }

        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="printTicket">Specifies information about how a document is printed.</param>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
		public void PrintWithWpf(object printTicket, bool showPrintDialog)
        {
            PrintWithWpf(printTicket, showPrintDialog, -1, -1, -1, null);
        }

        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="copies">Number of copies to print.</param>
		public void PrintWithWpf(int copies)
        {
            PrintWithWpf(true, copies);
        }

        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="copies">Number of copies to print.</param>
		public void PrintWithWpf(bool showPrintDialog, int copies)
        {
            PrintWithWpf(showPrintDialog, copies, null);
        }

        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="copies">Number of copies to print.</param>
        /// <param name="printerName">A name of a printer that will be used for printing.</param>
		public void PrintWithWpf(bool showPrintDialog, int copies, string printerName)
        {
            PrintWithWpf(showPrintDialog, -1, -1, copies, printerName);
        }

        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="fromPage">Number of the first page to print. Starts from 1.</param>
        /// <param name="toPage">Number of the last page to print. Starts from 1.</param>
        /// <param name="copies">Number of copies to print.</param>
		public void PrintWithWpf(bool showPrintDialog, int fromPage, int toPage, int copies)
        {
            PrintWithWpf(showPrintDialog, fromPage, toPage, copies, null);
        }
        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="fromPage">Number of the first page to print. Starts from 1.</param>
        /// <param name="toPage">Number of the last page to print. Starts from 1.</param>
        /// <param name="copies">Number of copies to print.</param>
        /// <param name="printerName">A name of a printer that will be used for printing.</param>
		public void PrintWithWpf(bool showPrintDialog, int fromPage, int toPage, int copies, string printerName)
        {
            PrintWithWpf(null, showPrintDialog, fromPage, toPage, copies, printerName);
        }

        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="printTicket">Specifies information about how a document is printed.</param>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="fromPage">Number of the first page to print. Starts from 1.</param>
        /// <param name="toPage">Number of the last page to print. Starts from 1.</param>
        /// <param name="copies">Number of copies to print.</param>
        /// <param name="printerName">A name of a printer that will be used for printing.</param>
		public void PrintWithWpf(object printTicket, bool showPrintDialog, int fromPage, int toPage, int copies, string printerName)
        {
            InvokePrinting();
            RegReportDataSources();
            if (!IsRendered) this.RenderWithWpf(true);

            StiLogService.Write(this.GetType(), "Printing report");
            try
            {
                if (NeedsCompiling)
                    this.CompiledReport.PrintWithWpfInternal(printTicket, showPrintDialog, fromPage, toPage, copies, printerName);
                else
                    this.PrintWithWpfInternal(printTicket, showPrintDialog, fromPage, toPage, copies, printerName);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Printing report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            InvokePrinted();
        }

        /// <summary>
        /// Prints the rendered report with using WPF technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Wpf assembly.
        /// </summary>
        /// <param name="printTicket">Specifies information about how a document is printed.</param>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="fromPage">Number of the first page to print. Starts from 1.</param>
        /// <param name="toPage">Number of the last page to print. Starts from 1.</param>
        /// <param name="copies">Number of copies to print.</param>
        /// <param name="printerName">A name of a printer that will be used for printing.</param>
        private void PrintWithWpfInternal(object printTicket, bool showPrintDialog, int fromPage, int toPage, int copies, string printerName)
        {
            var type = Type.GetType("Stimulsoft.Report.Print.StiWpfPrintProvider, Stimulsoft.Report.Wpf, " + StiVersion.VersionInfo);
            if (type == null)
                throw new Exception("Assembly 'Stimulsoft.Report.Wpf' is not found");

            var wpfPrintProvider = StiActivator.CreateObject(type, new object[] { this }) as IStiWpfPrintProvider;
            wpfPrintProvider.Print(printTicket, showPrintDialog, fromPage, toPage, copies, printerName);
        }
        #endregion

        #region Methods.XBAP
        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        public void PrintWithXbap()
        {
            PrintWithXbap(true);
        }

        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="printerName">A name of a printer that will be used for printing.</param>
        public void PrintWithXbap(string printerName)
        {
            PrintWithXbap(true, -1, printerName);
        }

        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="printTicket">Specifies information about how a document is printed.</param>
        public void PrintWithXbap(object printTicket)
        {
            PrintWithXbap(printTicket, true);
        }

        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        public void PrintWithXbap(bool showPrintDialog)
        {
            PrintWithXbap(null, showPrintDialog);
        }

        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="printTicket">Specifies information about how a document is printed.</param>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        public void PrintWithXbap(object printTicket, bool showPrintDialog)
        {
            PrintWithXbap(printTicket, showPrintDialog, -1, -1, -1, null);
        }

        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="copies">Number of copies to print.</param>
        public void PrintWithXbap(int copies)
        {
            PrintWithXbap(true, copies);
        }

        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="copies">Number of copies to print.</param>
        public void PrintWithXbap(bool showPrintDialog, int copies)
        {
            PrintWithXbap(showPrintDialog, copies, null);
        }

        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="copies">Number of copies to print.</param>
        /// <param name="printerName">A name of a printer that will be used for printing.</param>
        public void PrintWithXbap(bool showPrintDialog, int copies, string printerName)
        {
            PrintWithXbap(showPrintDialog, -1, -1, copies, printerName);
        }

        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="fromPage">Number of the first page to print. Starts from 1.</param>
        /// <param name="toPage">Number of the last page to print. Starts from 1.</param>
        /// <param name="copies">Number of copies to print.</param>
        public void PrintWithXbap(bool showPrintDialog, int fromPage, int toPage, int copies)
        {
            PrintWithXbap(showPrintDialog, fromPage, toPage, copies, null);
        }
        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="fromPage">Number of the first page to print. Starts from 1.</param>
        /// <param name="toPage">Number of the last page to print. Starts from 1.</param>
        /// <param name="copies">Number of copies to print.</param>
        /// <param name="printerName">A name of a printer that will be used for printing.</param>
        public void PrintWithXbap(bool showPrintDialog, int fromPage, int toPage, int copies, string printerName)
        {
            PrintWithXbap(null, showPrintDialog, fromPage, toPage, copies, printerName);
        }

        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="printTicket">Specifies information about how a document is printed.</param>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="fromPage">Number of the first page to print. Starts from 1.</param>
        /// <param name="toPage">Number of the last page to print. Starts from 1.</param>
        /// <param name="copies">Number of copies to print.</param>
        /// <param name="printerName">A name of a printer that will be used for printing.</param>
        public void PrintWithXbap(object printTicket, bool showPrintDialog, int fromPage, int toPage, int copies, string printerName)
        {
            InvokePrinting();
            RegReportDataSources();
            if (!IsRendered) this.RenderWithWpf(true);

            StiLogService.Write(this.GetType(), "Printing report");
            try
            {
                if (NeedsCompiling)
                    this.CompiledReport.PrintWithXbapInternal(printTicket, showPrintDialog, fromPage, toPage, copies, printerName);
                else
                    this.PrintWithXbapInternal(printTicket, showPrintDialog, fromPage, toPage, copies, printerName);
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Printing report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
            InvokePrinted();
        }

        /// <summary>
        /// Prints the rendered report with using Xbap technology. If the report is not rendered then its rendering starts. This method require Stimulsoft.Report.Xbap assembly.
        /// </summary>
        /// <param name="printTicket">Specifies information about how a document is printed.</param>
        /// <param name="showPrintDialog">Show the print dialog or no.</param>
        /// <param name="fromPage">Number of the first page to print. Starts from 1.</param>
        /// <param name="toPage">Number of the last page to print. Starts from 1.</param>
        /// <param name="copies">Number of copies to print.</param>
        /// <param name="printerName">A name of a printer that will be used for printing.</param>
        private void PrintWithXbapInternal(object printTicket, bool showPrintDialog, int fromPage, int toPage, int copies, string printerName)
        {
            var type = Type.GetType("Stimulsoft.Report.Print.StiWpfPrintProvider, Stimulsoft.Report.Xbap, " + StiVersion.VersionInfo);
            if (type == null)
                throw new Exception("Assembly 'Stimulsoft.Report.Xbap' is not found");

            var wpfPrintProvider = StiActivator.CreateObject(type, new object[] { this }) as IStiWpfPrintProvider;
            wpfPrintProvider.Print(printTicket, showPrintDialog, fromPage, toPage, copies, printerName);
        }
        #endregion

        #region Methods.DotMatrixPrinter
        /// <summary>
        /// Prints a report to default dot-matrix printer in the RAW mode in ASCII encoding.
        /// </summary>
        public void PrintToDotMatrixPrinter()
        {
            var settings = new PrinterSettings();
            PrintToDotMatrixPrinter(settings.PrinterName, Encoding.ASCII);
        }

        /// <summary>
        /// Prints a report to dot-matrix printer in the RAW mode in ASCII encoding.
        /// </summary>
        /// <param name="printerName">A name of a printer.</param>
		public void PrintToDotMatrixPrinter(string printerName)
        {
            PrintToDotMatrixPrinter(printerName, Encoding.ASCII);
        }

        /// <summary>
        /// Prints report to dot-matrix printer in the RAW mode.
        /// </summary>
        /// <param name="printerName">A name of a printer.</param>
        /// <param name="encoding">A parameter which sets text encoding.</param>
		public void PrintToDotMatrixPrinter(string printerName, Encoding encoding)
        {
            PrintToDotMatrixPrinter(printerName, encoding, -1, -1);
        }

        /// <summary>
        /// Prints a report to dot-matrix printer in the RAW mode.
        /// </summary>
        /// <param name="printerName">A name of a printer.</param>
        /// <param name="encoding">A parameter which sets text encoding.</param>
        /// <param name="fromPage">A number of the first page to print.</param>
        /// <param name="toPage">A number of the last page to print.</param>
		public void PrintToDotMatrixPrinter(string printerName, Encoding encoding, int fromPage, int toPage)
        {
            StiDotMatrixPrintProvider.PrintToDotMatrixPrinter(
                                this, printerName, encoding,
                                fromPage, toPage);
        }

        /// <summary>
        /// Prints a report to dot-matrix printer in RAW mode.
        /// </summary>
        /// <param name="printerName">A name of the printer.</param>
        /// <param name="encoding">A parameter which sets text encoding.</param>
        /// <param name="drawBorder">If true then borders are exported to a text.</param>
        /// <param name="borderType">Type of borders (StiTxtBorderType).</param>
        /// <param name="putFeedPageCode">If true then the EOF character will be added to the end of each page.</param>
        /// <param name="cutLongLines">If true then all long lines will be cut.</param>
        /// <param name="fromPage">A number of the first page to print.</param>
        /// <param name="toPage">A number of the last page to print.</param>
		public void PrintToDotMatrixPrinter(string printerName, Encoding encoding,
			bool drawBorder, StiTxtBorderType borderType, 
			bool putFeedPageCode, bool cutLongLines, int fromPage, int toPage)
        {
            StiDotMatrixPrintProvider.PrintToDotMatrixPrinter(
                                this, printerName, encoding,
                                drawBorder, borderType, putFeedPageCode, cutLongLines,
                                fromPage, toPage);
        }

        /// <summary>
        /// Prints report to dot-matrix printer in the RAW mode.
        /// </summary>
        /// <param name="printerName">A name of the printer.</param>
        /// <param name="encoding">A parameter which sets text encoding.</param>
        /// <param name="drawBorder">If true then borders are exported to the text.</param>
        /// <param name="borderType">Type of borders (StiTxtBorderType).</param>
        /// <param name="killSpaceLines">If true then empty lines will be removed from the result text.</param>
        /// <param name="killSpaceGraphLines">If true then empty lines with vertical borders will be removed from the result text.</param>
        /// <param name="putFeedPageCode">If true then the EOF character will be added to the end of each page.</param>
        /// <param name="cutLongLines">If true then all long lines will be cut.</param>
        /// <param name="zoomX">Horizontal zoom factor by X axis. By default a value is 1.0f what is equal 100% in export settings window.</param>
        /// <param name="zoomY">Vertical zoom factor by Y axis. By default a value is 1.0f what is equal 100% in export settings window.</param>
        /// <param name="fromPage">A number of the first page to print.</param>
        /// <param name="toPage">A number of the last page to print.</param>
		public void PrintToDotMatrixPrinter(string printerName, Encoding encoding,
			bool drawBorder, StiTxtBorderType borderType, bool killSpaceLines, bool killSpaceGraphLines,
			bool putFeedPageCode, bool cutLongLines, float zoomX, float zoomY, int fromPage, int toPage,
            bool useEscapeCodes, string escapeCodesCollectionName)
       {
            StiDotMatrixPrintProvider.PrintToDotMatrixPrinter(
                                this, printerName, encoding,
                                drawBorder, borderType, killSpaceLines,
                                killSpaceGraphLines, putFeedPageCode, cutLongLines,
                                zoomX, zoomY, fromPage, toPage, useEscapeCodes, escapeCodesCollectionName);
        }
        #endregion
    }
}