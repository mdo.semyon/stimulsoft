﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using Stimulsoft.Report.Design;
using Stimulsoft.Report.Viewer;

namespace Stimulsoft.Report.Events
{
    public delegate void StiWCFExportEventHandlers(object sender, StiWCFExportEventArgs e);

    #region StiWCFExportEventArgs
    public class StiWCFExportEventArgs : EventArgs
    {
        public bool Handled = false;
        public readonly string Xml;
        public readonly string Filter;
        public readonly IStiDesignerBase Designer;
        public readonly IStiViewerControl Viewer;

        public StiWCFExportEventArgs(string xml, string filter, StiReport report)
        {
            this.Xml = xml;
            this.Filter = filter;

            this.Viewer = report.ViewerControl;
            this.Designer = report.Designer;
        }
    }
    #endregion
}
