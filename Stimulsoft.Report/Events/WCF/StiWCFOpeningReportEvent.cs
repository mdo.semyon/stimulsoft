﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using Stimulsoft.Report.Design;

namespace Stimulsoft.Report.Events
{
    public delegate void StiWCFOpeningReportEventEventHandlers(object sender, StiWCFOpeningReportEventArgs e);

    #region StiWCFBuildObjectsEventArgs
    public class StiWCFOpeningReportEventArgs : EventArgs
    {
        public StiWCFOpeningReportEventArgs(IStiDesignerBase designer)
        {
            this.Designer = designer;
        }

        public bool Handled = false;
        public readonly IStiDesignerBase Designer;
    }
    #endregion
}
