﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using Stimulsoft.Report.Viewer;

namespace Stimulsoft.Report.Events
{
    public delegate void StiWCFRenderingInteractionsEventHandlers(object viewer, StiWCFRenderingInteractionsEventArgs e);

    #region StiWCFInteractionRenderReportEventArgs
    public class StiWCFRenderingInteractionsEventArgs : EventArgs
    {
        public bool Handled = false;
        public readonly string Xml;
        public readonly StiInteractionType InteractionType;
        public readonly IStiViewerControl Viewer;

        public StiWCFRenderingInteractionsEventArgs(string xml, StiInteractionType interactionType, IStiViewerControl viewer)
        {
            this.Xml = xml;
            this.InteractionType = interactionType;
            this.Viewer = viewer;
        }
    }
    #endregion
}
