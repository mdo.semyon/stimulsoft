﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using Stimulsoft.Report.Design;
using Stimulsoft.Report.Viewer;
using Stimulsoft.Report.WCFService;

namespace Stimulsoft.Report.Events
{
    #region StiWCFEventArgs
    public delegate void StiWCFEventHandlers(object sender, StiWCFEventArgs e);

    public class StiWCFEventArgs : EventArgs
    {
        public bool Handled = false;
        public readonly string Xml;
        public readonly IStiDesignerBase Designer;
        public readonly IStiViewerControl Viewer;

        public StiWCFEventArgs(string xml, IStiViewerControl viewer)
        {
            this.Xml = xml;
            this.Viewer = viewer;
        }

        public StiWCFEventArgs(string xml, IStiDesignerBase designer)
        {
            this.Xml = xml;
            this.Designer = designer;
        }
    }
    #endregion

    #region StiWCFReportCheckEventArgs
    public delegate void StiWCFReportCheckEventHandlers(IStiCheckStatusControl sender, StiWCFReportCheckEventArgs e);

    public class StiWCFReportCheckEventArgs : EventArgs
    {
        public bool Handled = false;
        public readonly string Xml;
        public readonly IStiDesignerBase Designer;

        public StiWCFReportCheckEventArgs(string xml, IStiDesignerBase designer)
        {
            this.Xml = xml;
            this.Designer = designer;
        }
    }
    #endregion

    #region StiWCFEventArgs
    public delegate void StiWCFFindDatabaseTypeEventHandlers(object sender, StiWCFFindDatabaseTypeEventArgs e);

    public class StiWCFFindDatabaseTypeEventArgs : EventArgs
    {
        #region Fields

        public bool Handled = false;
        public readonly string DatabaseType;
        public readonly IStiDesignerBase Designer;
        public string UserDatabaseType;

        #endregion

        public StiWCFFindDatabaseTypeEventArgs(string databaseType, IStiDesignerBase designer)
        {
            this.DatabaseType = databaseType;
            this.Designer = designer;
        }
    }
    #endregion
}
