#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;

namespace Stimulsoft.Report.Events
{
    /// <summary>
    /// Represents the method that handles the events which work with image.
    /// </summary>
    public delegate void StiGetImageDataEventHandler(object sender, StiGetImageDataEventArgs e);

    /// <summary>
    /// Describes an argument for the event GetImageData.
    /// </summary>
    public class StiGetImageDataEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the image for the event.
        /// </summary>
        public virtual Image Value { get; set; }

        /// <summary>
        /// Creates a new object of the type StiGetImageDataEventArgs.
        /// </summary>
        public StiGetImageDataEventArgs() : this(null)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiGetImageDataEventArgs.
        /// </summary>
        /// <param name="image">Image for event.</param>
        public StiGetImageDataEventArgs(Image image)
        {
            this.Value = image;
        }
    }
}