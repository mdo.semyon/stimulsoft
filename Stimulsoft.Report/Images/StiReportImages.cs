#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Report.Images
{
    public static class StiReportImages
    {
        #region class Actions
        public static class Actions
        {
            public static Image DropDownArrowMouseOver() => GetImage("Actions.DropDownArrowMouseOver");

            public static Image DropDownFilter() => GetImage("Actions.DropDownFilter");

            public static Image DropDownFilterMouseOver() => GetImage("Actions.DropDownFilterMouseOver");

            public static Image DropDownArrowMouseOverWhite() => GetImage("Actions.DropDownArrowMouseOver-white");

            public static Image DropDownFilterWhite() => GetImage("Actions.DropDownFilter-white");

            public static Image DropDownFilterMouseOverWhite() => GetImage("Actions.DropDownFilterMouseOver-white");

            public static Image SortArrowAsc() => GetImage("Actions.SortArrowAsc");

            public static Image SortArrowAscMouseOver() => GetImage("Actions.SortArrowAscMouseOver");

            public static Image SortArrowDesc() => GetImage("Actions.SortArrowDesc");

            public static Image SortArrowDescMouseOver() => GetImage("Actions.SortArrowDescMouseOver");

            public static Image SortArrowAscWhite() => GetImage("Actions.SortArrowAsc-white");

            public static Image SortArrowAscMouseOverWhite() => GetImage("Actions.SortArrowAscMouseOver-white");

            public static Image SortArrowDescWhite() => GetImage("Actions.SortArrowDesc-white");

            public static Image SortArrowDescMouseOverWhite() => GetImage("Actions.SortArrowDescMouseOver-white");

            public static Image SortAsc() => GetImage("Actions.SortAsc");

            public static Image SortDesc() => GetImage("Actions.SortDesc");
        }
        #endregion

        #region Consts
        private const string RootPath = "Stimulsoft.Report.Images";
        #endregion

        #region Methods
        internal static Image GetImage(string imagePath)
        {
            return StiImageUtils.GetImage(typeof(StiReportImages).Assembly, $"{RootPath}.{imagePath}.png", false);
        }
        #endregion
    }
}