﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Localization;
using Stimulsoft.Report.BarCodes;
using Stimulsoft.Report.Chart;
using Stimulsoft.Report.Gauge;
using Stimulsoft.Report.Gauge.Helpers;
using Stimulsoft.Report.Toolbox;
using System.Collections.Generic;

namespace Stimulsoft.Report.Maps
{
    public static class StiToolboxHelper
    {
        #region Methods
        public static StiInfographicsToolboxInfo[] GetMapToolboxItems()
        {
            return new StiInfographicsToolboxInfo[]
            {
                new StiInfographicsToolboxInfo(null, "World", "World")
                {
                    SkipCategory = true,
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.World, "World", "World")
                    }
                },
                new StiInfographicsToolboxInfo(null, "Europe", "EU")
                {
                    BeginGroup = true,
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.EU, "EU", "EU"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.France, "France", "France")
                        {
                            BeginGroup = true,
                        },
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Germany, "Germany", "Germany"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Italy, "Italy", "Italy"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Netherlands, "Netherlands", "Netherlands"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Russia, "Russia", "Russia"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Switzerland, "Switzerland", "Switzerland"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.UK, "UK", "UK"),
                        new StiInfographicsToolboxInfo(null, "ShowMore", "ShowMore")
                        {
                            BeginGroup = true,
                            IsShowMore = true,
                            Infos = new List<StiInfographicsToolboxInfo>()
                            {
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Albania, "Albania", "Albania"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Andorra, "Andorra", "Andorra"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Austria, "Austria", "Austria"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Belarus, "Belarus", "Belarus"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Belgium, "Belgium", "Belgium"),

                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.BosniaAndHerzegovina, "Bosnia and Herzegovina", "BosniaAndHerzegovina"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Bulgaria, "Bulgaria", "Bulgaria"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Croatia, "Croatia", "Croatia"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Cyprus, "Cyprus", "Cyprus"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.CzechRepublic, "CzechRepublic", "CzechRepublic"),

                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Denmark, "Denmark", "Denmark"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Estonia, "Estonia", "Estonia"),
                                //new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Europe, "Europe", "Europe"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Finland, "Finland", "Finland"),

                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Georgia, "Georgia", "Georgia"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Greece, "Greece", "Greece"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Hungary, "Hungary", "Hungary"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Iceland, "Iceland", "Iceland"),

                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Ireland, "Ireland", "Ireland"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Latvia, "Latvia", "Latvia"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Liechtenstein, "Liechtenstein", "Liechtenstein"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Lithuania, "Lithuania", "Lithuania"),

                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Luxembourg, "Luxembourg", "Luxembourg"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Macedonia, "Macedonia", "Macedonia"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Malta, "Malta", "Malta"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Moldova, "Moldova", "Moldova"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Monaco, "Monaco", "Monaco"),

                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Montenegro, "Montenegro", "Montenegro"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Norway, "Norway", "Norway"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Poland, "Poland", "Poland"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Portugal, "Portugal", "Portugal"),

                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Romania, "Romania", "Romania"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.SanMarino, "San Marino", "SanMarino"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Serbia, "Serbia", "Serbia"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Slovakia, "Slovakia", "Slovakia"),

                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Slovenia, "Slovenia", "Slovenia"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Spain, "Spain", "Spain"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Sweden, "Sweden", "Sweden"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Turkey, "Turkey", "Turkey"),

                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Ukraine, "Ukraine", "Ukraine"),
                                new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Vatican, "Vatican", "Vatican"),
                            }
                        }
                    }
                },
                new StiInfographicsToolboxInfo(null, "North America", "NorthAmerica")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.USA, "USA", "USA"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Canada, "Canada", "Canada"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Mexico, "Mexico", "Mexico")
                    }
                },
                new StiInfographicsToolboxInfo(null, "South America", "SouthAmerica")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Argentina, "Argentina", "Argentina"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Bolivia, "Bolivia", "Bolivia"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Brazil, "Brazil", "Brazil"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Chile, "Chile", "Chile"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Colombia, "Colombia", "Colombia"),

                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Ecuador, "Ecuador", "Ecuador"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.FalklandIslands, "Falkland Islands", "FalklandIslands"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Guyana, "Guyana", "Guyana"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Paraguay, "Paraguay", "Paraguay"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Peru, "Peru", "Peru"),

                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Suriname, "Suriname", "Suriname"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Uruguay, "Uruguay", "Uruguay"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Venezuela, "Venezuela", "Venezuela"),
                    }
                },
                new StiInfographicsToolboxInfo(null, "Asia", "Asia")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Armenia, "Armenia", "Armenia"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Azerbaijan, "Azerbaijan", "Azerbaijan"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.China, "China", "China"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.India, "India", "India"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Israel, "Israel", "Israel"),

                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Japan, "Japan", "Japan"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Kazakhstan, "Kazakhstan", "Kazakhstan"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Malaysia, "Malaysia", "Malaysia"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Philippines, "Philippines", "Philippines"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.SaudiArabia, "Saudi Arabia", "SaudiArabia"),

                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.SouthKorea, "South Korea", "SouthKorea"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Thailand, "Thailand", "Thailand"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Vietnam, "Vietnam", "Vietnam"),
                    }
                },
                new StiInfographicsToolboxInfo(null, "Oceania", "Oceania")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Australia, "Australia", "Australia"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.Indonesia, "Indonesia", "Indonesia"),
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.NewZealand, "New Zealand", "NewZealand")
                    }
                },
                new StiInfographicsToolboxInfo(null, "Africa", "Africa")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(StiInfographicsItemType.Map, StiMapID.SouthAfrica, "South Africa", "SouthAfrica"),
                    }
                }
            };
        }

        public static StiInfographicsToolboxInfo[] GetBarCodesToolboxItems()
        {
            return new StiInfographicsToolboxInfo[]
            {
                new StiInfographicsToolboxInfo(
                    new List<StiBarCodeTypeService>
                    {
                            new StiQRCodeBarCodeType(),
                            new StiDataMatrixBarCodeType(),
                            new StiMaxicodeBarCodeType(),
                            new StiPdf417BarCodeType(),
                    }
                    , StiLocalization.Get("BarCode", "TwoDimensional"), "StiQRCodeBarCodeType"),
                new StiInfographicsToolboxInfo(
                    new List<StiBarCodeTypeService>
                    {
                            new StiEAN128aBarCodeType(),
                            new StiEAN128bBarCodeType(),
                            new StiEAN128cBarCodeType(),
                            new StiEAN128AutoBarCodeType(),
                            new StiEAN13BarCodeType(),
                            new StiEAN8BarCodeType(),
                            new StiUpcABarCodeType(),
                            new StiUpcEBarCodeType(),
                            new StiUpcSup2BarCodeType(),
                            new StiUpcSup5BarCodeType(),
                            new StiJan13BarCodeType(),
                            new StiJan8BarCodeType(),
                    }
                    , "EAN\\UPC", "StiPharmacodeBarCodeType"),
                new StiInfographicsToolboxInfo(
                    new List<StiBarCodeTypeService>
                    {
                            new StiGS1_128BarCodeType(),
                            new StiSSCC18BarCodeType(),
                            new StiITF14BarCodeType(),
                    }
                    , "GS1", "StiPharmacodeBarCodeType"),

                new StiInfographicsToolboxInfo(
                    new List<StiBarCodeTypeService>
                    {
                            new StiAustraliaPost4StateBarCodeType(),
                            new StiPostnetBarCodeType(),
                            new StiDutchKIXBarCodeType(),
                            new StiRoyalMail4StateBarCodeType(),
                            new StiFIMBarCodeType()
                    }
                    , StiLocalization.Get("BarCode", "Post"), "StiAustraliaPost4StateBarCodeType"),
                new StiInfographicsToolboxInfo(
                    new List<StiBarCodeTypeService>
                    {
                            new StiPharmacodeBarCodeType(),
                            new StiCode11BarCodeType(),
                            new StiCode128aBarCodeType(),
                            new StiCode128bBarCodeType(),
                            new StiCode128cBarCodeType(),
                            new StiCode128AutoBarCodeType(),
                            new StiCode39BarCodeType(),
                            new StiCode39ExtBarCodeType(),
                            new StiCode93BarCodeType(),
                            new StiCode93ExtBarCodeType(),
                            new StiCodabarBarCodeType(),
                            new StiIsbn10BarCodeType(),
                            new StiIsbn13BarCodeType(),
                            new StiMsiBarCodeType(),
                            new StiPlesseyBarCodeType(),
                            new StiInterleaved2of5BarCodeType(),
                            new StiStandard2of5BarCodeType()
                    }
                    , StiLocalization.Get("FormDesigner", "Others"), "StiGS1_128BarCodeType"),
            };
        }

        public static StiInfographicsToolboxInfo[] GetGaugeToolboxItems(bool allowGaugeV2 = false)
        {
            if (allowGaugeV2 && !StiGaugeV2InitHelper.AllowOldEditor)
            {
                return new StiInfographicsToolboxInfo[]
                {
                    new StiInfographicsToolboxInfo(StiInfographicsItemType.Gauge, StiGaugeType.FullCircular, StiLocalization.Get("PropertyEnum", "StiGaugeTypeFullCircular"), "FullCircular"),
                    new StiInfographicsToolboxInfo(StiInfographicsItemType.Gauge, StiGaugeType.HalfCircular, StiLocalization.Get("PropertyEnum", "StiGaugeTypeHalfCircular"), "HalfCircular"),
                    new StiInfographicsToolboxInfo(StiInfographicsItemType.Gauge, StiGaugeType.Linear, StiLocalization.Get("PropertyEnum", "StiGaugeTypeLinear"), "Linear"),
                };
            }
            else
            {
                return new StiInfographicsToolboxInfo[]
                {
                    new StiInfographicsToolboxInfo(StiInfographicsItemType.Gauge, StiGaugeElemenType.RadialElement, StiLocalization.Get("Gauge", "RadialScale"), "RadialScale"),
                    new StiInfographicsToolboxInfo(StiInfographicsItemType.Gauge, StiGaugeElemenType.LinearElement, StiLocalization.Get("Gauge", "LinearScale"), "LinearScale"),
                };
            }
        }

        public static StiInfographicsToolboxInfo[] GetChartToolboxItems()
        {
            return new StiInfographicsToolboxInfo[]
            {
                new StiInfographicsToolboxInfo(null, StiLocalization.Get("Chart", "ClusteredColumn"), "ClusteredColumn")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiClusteredColumnSeries), StiLocalization.Get("Chart", "ClusteredColumn"), "ClusteredColumn"),
                        new StiInfographicsToolboxInfo(typeof(StiStackedColumnSeries), StiLocalization.Get("Chart", "StackedColumn"), "StackedColumn"),
                        new StiInfographicsToolboxInfo(typeof(StiFullStackedColumnSeries), StiLocalization.Get("Chart", "FullStackedColumn"), "FullStackedColumn"),
                    }
                },
                new StiInfographicsToolboxInfo(null, StiLocalization.Get("Chart", "Line"), "Line")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiLineSeries), StiLocalization.Get("Chart", "Line"), "Line"),
                        new StiInfographicsToolboxInfo(typeof(StiStackedLineSeries), StiLocalization.Get("Chart", "StackedLine"), "StackedLine"),
                        new StiInfographicsToolboxInfo(typeof(StiFullStackedLineSeries), StiLocalization.Get("Chart", "FullStackedLine"), "FullStackedLine"),

                        new StiInfographicsToolboxInfo(typeof(StiSplineSeries), StiLocalization.Get("Chart", "Spline"), "Spline", true),
                        new StiInfographicsToolboxInfo(typeof(StiStackedSplineSeries), StiLocalization.Get("Chart", "StackedSpline"), "StackedSpline"),
                        new StiInfographicsToolboxInfo(typeof(StiFullStackedSplineSeries), StiLocalization.Get("Chart", "FullStackedSpline"), "FullStackedSpline"),

                        new StiInfographicsToolboxInfo(typeof(StiSteppedLineSeries), StiLocalization.Get("Chart", "SteppedLine"), "SteppedLine", true)
                    }
                },
                new StiInfographicsToolboxInfo(null, StiLocalization.Get("Chart", "Area"), "Area")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiAreaSeries), StiLocalization.Get("Chart", "Area"), "Area"),
                        new StiInfographicsToolboxInfo(typeof(StiStackedAreaSeries), StiLocalization.Get("Chart", "StackedLine"), "StackedArea"),
                        new StiInfographicsToolboxInfo(typeof(StiFullStackedAreaSeries), StiLocalization.Get("Chart", "FullStackedLine"), "FullStackedArea"),

                        new StiInfographicsToolboxInfo(typeof(StiSplineAreaSeries), StiLocalization.Get("Chart", "SplineArea"), "SplineArea", true),
                        new StiInfographicsToolboxInfo(typeof(StiStackedSplineAreaSeries), StiLocalization.Get("Chart", "StackedSplineArea"), "StackedSplineArea"),
                        new StiInfographicsToolboxInfo(typeof(StiFullStackedSplineAreaSeries), StiLocalization.Get("Chart", "FullStackedSplineArea"), "FullStackedSplineArea"),

                        new StiInfographicsToolboxInfo(typeof(StiSteppedAreaSeries), StiLocalization.Get("Chart", "SteppedArea"), "SteppedArea", true)
                    }
                },
                new StiInfographicsToolboxInfo(typeof(StiRangeSeries), StiLocalization.Get("Chart", "Range"), "Range")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiRangeSeries), StiLocalization.Get("Chart", "Range"), "Range"),
                        new StiInfographicsToolboxInfo(typeof(StiSplineRangeSeries), StiLocalization.Get("Chart", "SplineRange"), "SplineRange"),
                        new StiInfographicsToolboxInfo(typeof(StiSteppedRangeSeries), StiLocalization.Get("Chart", "SteppedRange"), "SteppedRange"),
                        new StiInfographicsToolboxInfo(typeof(StiRangeBarSeries), StiLocalization.Get("Chart", "RangeBar"), "RangeBar"),
                    }
                },
                new StiInfographicsToolboxInfo(null, StiLocalization.Get("Chart", "ClusteredBar"), "ClusteredBar")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiClusteredBarSeries), StiLocalization.Get("Chart", "ClusteredBar"), "ClusteredBar"),
                        new StiInfographicsToolboxInfo(typeof(StiStackedBarSeries), StiLocalization.Get("Chart", "StackedBar"), "StackedBar"),
                        new StiInfographicsToolboxInfo(typeof(StiFullStackedBarSeries), StiLocalization.Get("Chart", "FullStackedBar"), "FullStackedBar")
                    }
                },
                new StiInfographicsToolboxInfo(null, StiLocalization.Get("Chart", "Scatter"), "Scatter")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiScatterSeries), StiLocalization.Get("Chart", "Scatter"), "Scatter"),
                        new StiInfographicsToolboxInfo(typeof(StiScatterLineSeries), StiLocalization.Get("Chart", "ScatterLine"), "ScatterLine"),
                        new StiInfographicsToolboxInfo(typeof(StiScatterSplineSeries), StiLocalization.Get("Chart", "ScatterSpline"), "ScatterSpline")
                    }
                },
                new StiInfographicsToolboxInfo(null, StiLocalization.Get("Chart", "Pie"), "categoryPie")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiPieSeries), StiLocalization.Get("Chart", "Pie"), "categoryPie")
                    }
                },
                new StiInfographicsToolboxInfo(null, StiLocalization.Get("Chart", "RadarArea"), "RadarArea")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiRadarPointSeries), StiLocalization.Get("Chart", "RadarPoint"), "RadarPoint"),
                        new StiInfographicsToolboxInfo(typeof(StiRadarLineSeries), StiLocalization.Get("Chart", "RadarLine"), "RadarLine"),
                        new StiInfographicsToolboxInfo(typeof(StiRadarAreaSeries), StiLocalization.Get("Chart", "RadarArea"), "RadarArea")
                    }
                },
                new StiInfographicsToolboxInfo(null, StiLocalization.Get("Chart", "Funnel"), "Funnel")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiFunnelSeries), StiLocalization.Get("Chart", "Funnel"), "Funnel"),
                        new StiInfographicsToolboxInfo(typeof(StiFunnelWeightedSlicesSeries), StiLocalization.Get("Chart", "FunnelWeightedSlices"), "FunnelWeightedSlices")
                    }
                },
                new StiInfographicsToolboxInfo(null, StiLocalization.Get("Chart", "Financial"), "categoryFinancial")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiCandlestickSeries), StiLocalization.Get("Chart", "Candlestick"), "Candlestick"),
                        new StiInfographicsToolboxInfo(typeof(StiStockSeries), StiLocalization.Get("Chart", "Stock"), "Stock")
                    }
                },
                new StiInfographicsToolboxInfo(null, StiLocalization.Get("FormDesigner", "Others"), "categoryOther")
                {
                    Infos = new List<StiInfographicsToolboxInfo>()
                    {
                        new StiInfographicsToolboxInfo(typeof(StiTreemapSeries), StiLocalization.Get("Chart", "Treemap"), "Treemap"),
                        new StiInfographicsToolboxInfo(typeof(StiGanttSeries), StiLocalization.Get("Chart", "Gantt"), "Gantt"),
                        new StiInfographicsToolboxInfo(typeof(StiDoughnutSeries), StiLocalization.Get("Chart", "Doughnut"), "categoryOther"),
                        new StiInfographicsToolboxInfo(typeof(StiBubbleSeries), StiLocalization.Get("Chart", "Bubble"), "Bubble"),
                        new StiInfographicsToolboxInfo(typeof(StiPictorialSeries), StiLocalization.Get("Chart", "Pictorial"), "Pictorial")
                    }
                }
            };
        }
        #endregion
    }
}