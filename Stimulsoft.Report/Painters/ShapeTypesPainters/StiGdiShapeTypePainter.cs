﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.Drawing.Drawing2D;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;

namespace Stimulsoft.Report.Painters
{
    public abstract class StiGdiShapeTypePainter : StiShapeTypePainter
    {
        #region Draw
        public static void DrawShape(Graphics g, StiShape shape, Point[] points, RectangleF rect)
        {
            using (var path = new GraphicsPath())
            {
                path.AddLines(points);

                using (var brush = StiBrush.GetBrush(shape.Brush, rect))
                {
                    g.FillPath(brush, path);
                }
                if (shape.Style != StiPenStyle.None)
                {
                    using (var pen = new Pen(shape.BorderColor, shape.Size))
                    {
                        pen.DashStyle = StiPenUtils.GetPenStyle(shape.Style);
                        g.DrawLines(pen, points);
                    }
                }
            }
        }

        public static void DrawShape(Graphics g, StiShape shape, GraphicsPath path, RectangleF rect)
        {
            using (var brush = StiBrush.GetBrush(shape.Brush, rect))
            {
                g.FillPath(brush, path);
            }
            if (shape.Style != StiPenStyle.None)
            {
                using (var pen = new Pen(shape.BorderColor, shape.Size))
                {
                    pen.DashStyle = StiPenUtils.GetPenStyle(shape.Style);
                    g.DrawPath(pen, path);
                }
            }
        }
        #endregion
    }
}
