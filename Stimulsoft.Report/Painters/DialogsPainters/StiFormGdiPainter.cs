﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dialogs;
using Stimulsoft.Report.Design;
using Stimulsoft.Report.Events;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Painters
{
    public class StiFormGdiPainter : StiPageGdiPainter
    {
        #region Methods
        private Rectangle GetOffsetRect(StiForm form, Rectangle rect, int minWidth, int minHeight)
        {
            if (form.IsSelected)
            {
                rect.Width = Math.Max(rect.Width + (int)form.OffsetRectangle.Width, minWidth);
                rect.Height = Math.Max(rect.Height + (int)form.OffsetRectangle.Height, minHeight);
            }
            else
            {
                rect.Width = Math.Max(rect.Width, minWidth);
                rect.Height = Math.Max(rect.Height, minHeight);
            }
            return rect;
        }
        #endregion

        #region Methods.Painter
        public override void Paint(StiComponent component, StiPaintEventArgs e)
        {
            if (!(e.Context is Graphics))
                throw new Exception("StiGdiPainter can work only with System.Drawing.Graphics context!");

            StiForm form = component as StiForm;
            Graphics g = e.Graphics;

            int mgLeft = (int)form.Margins.Left;
            int mgTop = (int)form.Margins.Top;
            int mgRight = (int)form.Margins.Right;
            int mgBottom = (int)form.Margins.Bottom;

            int pgWidth = (int)form.DisplayRectangle.Width;
            int pgHeight = (int)form.DisplayRectangle.Height;

            if (!e.ClipRectangle.IsEmpty)
            {
                g.ResetClip();
                g.SetClip(e.ClipRectangle.ToRectangleF());
            }

            Rectangle rect = GetOffsetRect(form, new Rectangle(0, 0, pgWidth, pgHeight), 112, 32);

            ControlPaint.DrawBorder3D(g, rect, Border3DStyle.Raised, Border3DSide.All);

            #region Draw Title
            double titleHeight = 20;
            Rectangle titleRect = new Rectangle(2, 2, pgWidth - 4, (int)titleHeight - 2);

            if (form.IsSelected)
                titleRect.Width = Math.Max(titleRect.Width + (int)form.OffsetRectangle.Width, 108);

            g.FillRectangle(
                new LinearGradientBrush(titleRect, StiColors.ActiveCaptionEnd, StiColors.ActiveCaptionStart, 0f),
                titleRect);

            titleRect.X += 2;
            using (Font font = new Font("Arial", 8, FontStyle.Bold))
            using (SolidBrush brush = new SolidBrush(SystemColors.ActiveCaptionText))
            using (StringFormat sf = new StringFormat())
            {
                sf.LineAlignment = StringAlignment.Center;
                sf.FormatFlags = StringFormatFlags.NoWrap;
                sf.Trimming = StringTrimming.EllipsisCharacter;
                if (form.RightToLeft == RightToLeft.Yes)
                    sf.FormatFlags |= StringFormatFlags.DirectionRightToLeft;

                g.DrawString(form.Text, font, brush, titleRect, sf);
            }
            #endregion

            Rectangle clipRect = GetOffsetRect(form, form.ClientRectangle.ToRectangle(), 104, 8);
            clipRect.Width++;
            clipRect.Height++;
            clipRect.Y += 20;
            clipRect.X += 4;
            g.SetClip(clipRect);

            #region Fill Content
            if (form.Report.Info.ShowGrid)
            {
                int grid = (int)form.GridSize;
                Bitmap bmp = new Bitmap(grid, grid);
                using (Graphics gg = Graphics.FromImage(bmp))
                {
                    using (Brush brush = new SolidBrush(form.BackColor))
                    {
                        gg.FillRectangle(brush, 0, 0, grid, grid);
                    }
                }
                bmp.SetPixel(0, 0, SystemColors.ControlDarkDark);

                Rectangle textureRect = clipRect;
                g.TranslateTransform(textureRect.X, textureRect.Y);

                textureRect.X = 0;
                textureRect.Y = 0;

                TextureBrush textureBrush = new TextureBrush(bmp);
                g.FillRectangle(textureBrush, textureRect);
                g.ResetTransform();
            }
            #endregion

            g.TranslateTransform(mgLeft, mgTop);

            PaintTableLines(form, g);

            Bitmap buffer = new Bitmap(clipRect.Width, clipRect.Height);
            using (Graphics gBuffer = Graphics.FromImage(buffer))
            {
                PaintComponents(form, new StiPaintEventArgs(gBuffer, e.ClipRectangle));
                gBuffer.ResetClip();

                if (form.Report.Info.ShowOrder && form.IsDesigning)
                {
                    PaintOrderAndQuickInfo(form, gBuffer, "");
                }
            }

            g.DrawImage(buffer, 0, 0, clipRect.Width, clipRect.Height);
            g.ResetClip();

            #region Draw Dimension Lines
            if (form.Report.Info.ShowDimensionLines && form.IsDesigning && form.Report.Info.CurrentAction != StiAction.None)
            {
                StiDimensionLinesHelper.DrawDimensionLines(g, form);
            }
            #endregion

            #region Draw Selected Rectangle
            if (!form.SelectedRectangle.IsEmpty)
            {
                using (Brush brush = new SolidBrush(Color.FromArgb(75, Color.Blue)))
                using (Pen pen = new Pen(Color.DimGray))
                {
                    pen.DashStyle = DashStyle.Dash;

                    RectangleF rectSelect = form.SelectedRectangle.ToRectangleF();
                    g.FillRectangle(brush, rectSelect);
                    g.DrawRectangle(pen, rectSelect.X, rectSelect.Y, rectSelect.Width, rectSelect.Height);
                }
            }
            #endregion

            PaintSelection(form, e);
        }
        #endregion
    }
}
