﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Maps.Geoms;
using Stimulsoft.Data.Engine;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Maps;
using Stimulsoft.Report.Maps.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

namespace Stimulsoft.Report.Painters
{
    public class StiGdiMapContextPainter
    {
        #region Fields
        internal float individualStep = 0;
        private StiMap map;
        private Brush DefaultBrush;
        private StiBrush DefaultBrush1;
        private HeatmapInfo heatmapInfo;
        private HeatmapWithGroupInfo heatmapWithGroupInfo;
        private NoneInfo noneInfo;
        private Dictionary<string, StiMapGroup> hashGroup = new Dictionary<string, StiMapGroup>();
        private StiStyleColorsContainer colorsContainer = new StiStyleColorsContainer();
        #endregion

        #region Properties
        internal List<StiMapData> MapData { get; private set; }

        internal bool IsBorderEmpty { get; set; }

        internal StiMapStyle MapStyle { get; set; }

        public StiDataTable DataTable { get; set; }

        public List<string> CheckedMapKeys { get; set; }

        public string MouseOverKey { get; set; }
        #endregion

        #region Additional Classes

        #region class StiMapGroup
        private class StiMapGroup
        {
            public double? MinValue;
            public double? MaxValue;
            public Brush Fill;
            public StiBrush Fill1;
        }
        #endregion

        #region class StiStyleColorsContainer
        private class StiStyleColorsContainer
        {
            #region Fields

            private List<Color> stackColors = new List<Color>();
            private int index = 0;

            #endregion

            #region Methods
            public Brush GetColor()
            {
                var brush = new SolidBrush(stackColors[index]);

                index++;
                if (index >= stackColors.Count)
                    index = 0;

                return brush;
            }

            public StiBrush GetColor1()
            {
                var brush = new StiSolidBrush(stackColors[index]);

                index++;
                if (index >= stackColors.Count)
                    index = 0;

                return brush;
            }

            public void Init(StiMap map)
            {
                this.stackColors.Clear();
                var colors = map.GetCurrentStyleColors();
                index = 0;

                foreach (var color in colors)
                {
                    this.stackColors.Add(color);
                }
            }
            #endregion
        }
        #endregion

        #region class HeatmapInfo
        private class HeatmapInfo
        {
            #region Fields
            private StiGdiMapContextPainter painter;
            private double min;
            private double max;
            private Color[] colors;
            #endregion

            #region Methods
            public Brush GetBrush(StiMapData data)
            {
                if (min == max)
                    return new SolidBrush(colors[0]);

                double value = 0;
                if (data.Value == null || !double.TryParse(data.Value, out value))
                    return this.painter.DefaultBrush;

                return new SolidBrush(StiColorUtils.ChangeLightness(colors[0], (float)(0.85 * (max - value) / (max - min))));
            }

            public StiBrush GetBrush1(StiMapData data)
            {
                if (min == max)
                    return new StiSolidBrush(colors[0]);

                double value = 0;
                if (data.Value == null || !double.TryParse(data.Value, out value))
                    return this.painter.DefaultBrush1;

                return new StiSolidBrush(StiColorUtils.ChangeLightness(colors[0], (float)(0.85 * (max - value) / (max - min))));
            }
            #endregion

            public HeatmapInfo(StiGdiMapContextPainter painter, StiMap map, List<StiMapData> mapData)
            {
                this.painter = painter;
                bool isFirst = true;
                foreach (var data in mapData)
                {
                    if (data.Value == null) continue;

                    double value = 0;
                    if (data.Value == null || !double.TryParse(data.Value, out value)) continue;

                    if (isFirst)
                    {
                        isFirst = false;

                        min = value;
                        max = value;
                    }
                    else
                    {
                        if (value < min)
                            min = value;
                        else if (value > max)
                            max = value;
                    }
                }

                var mapStyle = StiMap.GetMapStyle(map);
                this.colors = mapStyle.HeatmapColors;
            }
        }
        #endregion

        #region class HeatmapWithGroupInfo
        private class HeatmapWithGroupInfo
        {
            #region Fields
            private StiGdiMapContextPainter painter;
            private Dictionary<string, double[]> hash = new Dictionary<string, double[]>();
            private Dictionary<string, Color> hashColors = new Dictionary<string, Color>();
            #endregion

            #region Methods
            public Brush GetBrush(StiMapData data)
            {
                if (data.Group == null) return null;

                if (!hash.ContainsKey(data.Group))
                    return this.painter.DefaultBrush;

                var values = hash[data.Group];
                var color = hashColors[data.Group];

                if (values[0] == values[1])
                    return new SolidBrush(color);

                double value = 0;
                if (data.Value == null || !double.TryParse(data.Value, out value))
                    return this.painter.DefaultBrush;

                return new SolidBrush(StiColorUtils.ChangeLightness(color, (float)(0.85f * (values[1] - value) / (values[1] - values[0]))));
            }

            public StiBrush GetBrush1(StiMapData data)
            {
                if (data.Group == null) return null;

                if (!hash.ContainsKey(data.Group))
                    return this.painter.DefaultBrush1;

                var values = hash[data.Group];
                var color = hashColors[data.Group];

                if (values[0] == values[1])
                    return new StiSolidBrush(color);

                double value = 0;
                if (data.Value == null || !double.TryParse(data.Value, out value))
                    return this.painter.DefaultBrush1;

                return new StiSolidBrush(StiColorUtils.ChangeLightness(color, (float)(0.85f * (values[1] - value) / (values[1] - values[0]))));
            }
            #endregion

            public HeatmapWithGroupInfo(StiGdiMapContextPainter painter, StiMap map, List<StiMapData> mapData)
            {
                this.painter = painter;

                var colors = StiMap.GetMapStyle(map).HeatmapColors;
                int index = 0;

                foreach (var data in mapData)
                {
                    var key = data.Group;
                    if (key == null) continue;
                    if (data.Value == null) continue;

                    double value = 0;
                    if (data.Value == null || !double.TryParse(data.Value, out value)) continue;

                    if (!hash.ContainsKey(key))
                    {
                        var values = new double[2] { value, value };
                        hash.Add(key, values);
                    }
                    else
                    {
                        var values = hash[key];
                        if (value < values[0])
                            values[0] = value;
                        else if (value > values[1])
                            values[1] = value;
                    }

                    if (!hashColors.ContainsKey(key))
                    {
                        var color = colors[index];

                        index++;
                        if (index >= colors.Length)
                            index = 0;

                        hashColors.Add(key, color);
                    }
                }
            }
        }
        #endregion

        #region class NoneInfo
        private class NoneInfo
        {
            public NoneInfo()
            {
                this.Colors = StiMapHelper.GetColors();
            }

            #region Fields

            private Color[] Colors;
            private int index = 0;

            #endregion

            #region Methods

            public StiSolidBrush GetBrush()
            {
                var color = this.Colors[index];

                index++;
                if (index >= this.Colors.Length)
                    index = 0;

                return new StiSolidBrush(color);
            }

            #endregion
        }
        #endregion

        #endregion

        #region Methods.Render
        public void Render(Graphics graphics, bool useZoom)
        {
            var rect = map.GetPaintRectangle(true, true);
            Render(graphics, useZoom, rect);
        }

        public Brush GetGeomBrush(StiMapData data)
        {
            if (map.MapType == StiMapType.Individual)
            {
                if (this.map.ColorEach)
                {
                    if (data == null)
                        return new SolidBrush(this.MapStyle.DefaultColor);

                    var brush = ParseHexColor(data.Color);
                    if (brush != null) return brush;

                    return new SolidBrush(this.noneInfo.GetBrush().Color);
                }
                else
                {
                    return new SolidBrush(StiColorUtils.ChangeLightness(this.MapStyle.IndividualColor, this.individualStep));
                }
            }

            switch (map.MapType)
            {
                case StiMapType.Individual:
                    return DefaultBrush;

                case StiMapType.Heatmap:
                    {
                        if (data == null || data.Value == null)
                            return DefaultBrush;
                        else
                            return heatmapInfo.GetBrush(data);
                    }

                case StiMapType.Group:
                    {
                        return (data != null && data.Group != null && hashGroup.ContainsKey(data.Group))
                            ? hashGroup[data.Group].Fill
                            : new SolidBrush(this.MapStyle.DefaultColor);
                    }

                case StiMapType.HeatmapWithGroup:
                    {
                        if (data == null || data.Group == null)
                            return new SolidBrush(this.MapStyle.DefaultColor);
                        else
                            return heatmapWithGroupInfo.GetBrush(data);
                    }
            }

            return DefaultBrush;
        }

        public StiBrush GetGeomBrush1(StiMapData data)
        {
            if (map.MapType == StiMapType.Individual)
            {
                if (this.map.ColorEach)
                {
                    if (data == null)
                        return new StiSolidBrush(this.MapStyle.DefaultColor);

                    var brush = ParseHexColor1(data.Color);
                    if (brush != null) return brush;

                    return new StiSolidBrush(this.noneInfo.GetBrush().Color);
                }
                else
                {
                    return new StiSolidBrush(StiColorUtils.ChangeLightness(this.MapStyle.IndividualColor, this.individualStep));
                }
            }

            switch (map.MapType)
            {
                case StiMapType.Individual:
                    return DefaultBrush1;

                case StiMapType.Heatmap:
                    {
                        if (data.Value == null)
                            return DefaultBrush1;
                        else
                            return heatmapInfo.GetBrush1(data);
                    }

                case StiMapType.Group:
                    {
                        return (data.Group != null && hashGroup.ContainsKey(data.Group))
                            ? hashGroup[data.Group].Fill1
                            : new StiSolidBrush(this.MapStyle.DefaultColor);
                    }

                case StiMapType.HeatmapWithGroup:
                    {
                        if (data.Group == null)
                            return new StiSolidBrush(this.MapStyle.DefaultColor);
                        else
                            return heatmapWithGroupInfo.GetBrush1(data);
                    }
            }

            return DefaultBrush1;
        }

        private List<object> GetDataTableValues(int column)
        {
            var list = new List<object>();
            foreach (var row in this.DataTable.Rows)
            {
                list.Add(row[column]);
            }

            return list;
        }

        internal void PrepareDataColumns()
        {
            if (map.DataFrom == StiMapSource.Manual)
            {
                this.MapData = this.map.GetMapData();
                return;
            }

            this.MapData = StiMap.GetDefaultMapData(this.map.MapID);

            List<object> keyValues = null;
            List<object> nameValues = null;
            List<object> valueValues = null;
            List<object> groupValues = null;
            List<object> colorValues = null;

            if (this.DataTable != null)
            {
                int index = 0;

                if (!string.IsNullOrEmpty(map.KeyDataColumn))
                {
                    keyValues = GetDataTableValues(index);
                    index++;
                }
                if (!string.IsNullOrEmpty(map.NameDataColumn))
                {
                    nameValues = GetDataTableValues(index);
                    index++;
                }
                if (!string.IsNullOrEmpty(map.ValueDataColumn))
                {
                    valueValues = GetDataTableValues(index);
                    index++;
                }
                if (!string.IsNullOrEmpty(map.GroupDataColumn))
                {
                    groupValues = GetDataTableValues(index);
                    index++;
                }
                if (!string.IsNullOrEmpty(map.ColorDataColumn))
                {
                    colorValues = GetDataTableValues(index);
                    index++;
                }
            }
            else
            {
                try
                {
                    if (!map.Report.IsDesigning)
                        map.Report.Dictionary.Connect();

                    keyValues = !string.IsNullOrEmpty(map.KeyDataColumn) ? StiDataColumn.GetDataListFromDataColumn(map.Report.Dictionary, map.KeyDataColumn) : null;
                    nameValues = !string.IsNullOrEmpty(map.NameDataColumn) ? StiDataColumn.GetDataListFromDataColumn(map.Report.Dictionary, map.NameDataColumn) : null;
                    valueValues = !string.IsNullOrEmpty(map.ValueDataColumn) ? StiDataColumn.GetDataListFromDataColumn(map.Report.Dictionary, map.ValueDataColumn) : null;
                    groupValues = !string.IsNullOrEmpty(map.GroupDataColumn) ? StiDataColumn.GetDataListFromDataColumn(map.Report.Dictionary, map.GroupDataColumn) : null;
                    colorValues = !string.IsNullOrEmpty(map.ColorDataColumn) ? StiDataColumn.GetDataListFromDataColumn(map.Report.Dictionary, map.ColorDataColumn) : null;

                    if (!map.Report.IsDesigning)
                        map.Report.Dictionary.Disconnect();
                }
                catch { }
            }

            if (keyValues == null) return;

            try
            {
                #region find min
                int min = 0;
                if (keyValues != null)
                {
                    if (min == 0)
                    {
                        min = keyValues.Count;
                    }
                    else
                    {
                        if (keyValues.Count < min)
                            min = keyValues.Count;
                    }
                }
                if (nameValues != null)
                {
                    if (min == 0)
                    {
                        min = nameValues.Count;
                    }
                    else
                    {
                        if (nameValues.Count < min)
                            min = nameValues.Count;
                    }
                }
                if (valueValues != null)
                {
                    if (min == 0)
                    {
                        min = valueValues.Count;
                    }
                    else
                    {
                        if (valueValues.Count < min)
                            min = valueValues.Count;
                    }
                }
                if (groupValues != null)
                {
                    if (min == 0)
                    {
                        min = groupValues.Count;
                    }
                    else
                    {
                        if (groupValues.Count < min)
                            min = groupValues.Count;
                    }
                }
                if (colorValues != null)
                {
                    if (min == 0)
                    {
                        min = colorValues.Count;
                    }
                    else
                    {
                        if (colorValues.Count < min)
                            min = colorValues.Count;
                    }
                }
                #endregion

                #region Key
                if (keyValues != null)
                {
                    foreach (var data in this.MapData)
                    {
                        data.Group = null;
                        data.Value = null;
                        if (colorValues != null)
                            data.Color = null;
                    }

                    for (int index = 0; index < min; index++)
                    {
                        string key = null;
                        if (keyValues[index] == null) continue;

                        key = keyValues[index].ToString();
                        if (string.IsNullOrEmpty(key)) continue;
                        key = key.ToLowerInvariant();

                        var data = this.MapData.FirstOrDefault(x => x.Key.ToLowerInvariant() == key);
                        if (data == null) continue;

                        if (nameValues != null && nameValues[index] != null)
                        {
                            var name = nameValues[index].ToString();
                            data.Name = name;
                        }

                        if (valueValues != null && valueValues[index] != null)
                        {
                            var value = valueValues[index].ToString();
                            data.Value = value;
                        }

                        if (groupValues != null && groupValues[index] != null)
                        {
                            var group = groupValues[index].ToString();
                            data.Group = group;
                        }

                        if (colorValues != null && colorValues[index] != null)
                        {
                            var color = colorValues[index].ToString();
                            data.Color = color;
                        }
                    }
                }
                #endregion
            }
            catch { }
        }

        public static StiMapInteractionContainer RenderInteractionContainer(StiMapID mapID)
        {
            var geomContainer = StiMapLoader.GetGeomsObject(mapID.ToString());
            var container = new StiMapInteractionContainer(geomContainer.Width, geomContainer.Height);

            foreach (var geoms in geomContainer.Geoms.OrderBy(x => x.Key))
            {
                var listPaths = new List<GraphicsPath>();

                var path = new GraphicsPath();
                var lastPoint = PointF.Empty;

                foreach (var geom in geoms.Geoms)
                {
                    switch (geom.GeomType)
                    {
                        case StiMapGeomType.MoveTo:
                            {
                                if (path.PointCount > 0)
                                {
                                    path.CloseFigure();

                                    listPaths.Add(path);
                                    path = new GraphicsPath();
                                }

                                var moveTo = (StiMoveToMapGeom)geom;
                                lastPoint = new PointF((float)moveTo.X, (float)moveTo.Y);
                            }
                            break;

                        case StiMapGeomType.Line:
                            {
                                var line = (StiLineMapGeom)geom;

                                var point = new PointF((float)line.X, (float)line.Y);
                                path.AddLine(lastPoint, point);
                                lastPoint = point;
                            }
                            break;

                        case StiMapGeomType.Bezier:
                            {
                                var bezier = (StiBezierMapGeom)geom;

                                var point1 = new PointF((float)bezier.X1, (float)bezier.Y1);
                                var point2 = new PointF((float)bezier.X2, (float)bezier.Y2);
                                var point3 = new PointF((float)bezier.X3, (float)bezier.Y3);
                                path.AddBezier(lastPoint, point1, point2, point3);

                                lastPoint = point3;
                            }
                            break;

                        case StiMapGeomType.Beziers:
                            {
                                var beziers = (StiBeziersMapGeom)geom;

                                var list = new List<PointF>()
                                {
                                    lastPoint
                                };
                                var p = PointF.Empty;
                                for (var index = 0; index < beziers.Array.Length; index += 2)
                                {
                                    p = new PointF((float)beziers.Array[index], (float)beziers.Array[index + 1]);
                                    list.Add(p);
                                }

                                list.Add(p);
                                lastPoint = p;

                                if (list.Count == 9)
                                {
                                    list.RemoveAt(8);
                                    list.RemoveAt(7);
                                }

                                path.AddBeziers(list.ToArray());
                            }
                            break;

                        case StiMapGeomType.Close:
                            {
                                path.CloseFigure();
                                listPaths.Add(path);
                                path = new GraphicsPath();
                            }
                            break;
                    }
                }

                if (path.PointCount > 0)
                {
                    path.CloseFigure();
                    listPaths.Add(path);
                }

                container.Add(geoms.Key, listPaths);
            }

            return container;
        }

        private void DrawState(Graphics g, GraphicsPath path, string key)
        {
            if (this.CheckedMapKeys != null && this.CheckedMapKeys.Contains(key))
            {
                using (var fill = new HatchBrush(HatchStyle.WideUpwardDiagonal, Color.FromArgb(100, Color.White), Color.Transparent))
                {
                    g.FillPath(fill, path);
                }
            }

            if (this.MouseOverKey == key)
            {
                using (var fill = new SolidBrush(Color.FromArgb(100, Color.White)))
                {
                    g.FillPath(fill, path);
                }
            }
        }

        public void Render(Graphics g, bool useZoom, RectangleD rect, bool center = true)
        {
            this.MapStyle = StiMap.GetMapStyle(map);

            PrepareDataColumns();
            UpdateGroupedData();

            this.heatmapInfo = new HeatmapInfo(this, map, MapData);
            this.noneInfo = new NoneInfo();

            UpdateHeatmapWithGroup();

            var geomContainer = StiMapLoader.GetGeomsObject(map.MapID.ToString());
            bool skipLabels = false;
            if (useZoom && map.Stretch)
            {
                var zoom = Math.Min(
                    (float)rect.Width / (float)geomContainer.Width, 
                    (float)rect.Height / (float)geomContainer.Height);

                if (zoom == 0)
                    zoom = 1f;

                if (zoom > 1f)
                    zoom = 1f;

                if (zoom < 0.2f)
                    skipLabels = true;

                g.ScaleTransform(zoom, zoom);

                if (center)
                {
                    var destWidth = rect.Width / zoom;
                    var destHeight = rect.Height / zoom;

                    g.TranslateTransform(
                        (float)((destWidth - geomContainer.Width) / 2),
                        (float)((destHeight - geomContainer.Height) / 2));
                }
            }

            this.DefaultBrush = new SolidBrush(this.MapStyle.DefaultColor);
            this.DefaultBrush1 = new StiSolidBrush(this.MapStyle.DefaultColor);

            var hashLabels = new List<string>();

            float individualStepValue = 0.5f / geomContainer.Geoms.Count;
            this.individualStep = individualStepValue;
            foreach (var geoms in geomContainer.Geoms.OrderBy(x => x.Key))
            {
                var path = new GraphicsPath();
                var lastPoint = PointF.Empty;

                var data = this.MapData.FirstOrDefault(x => x.Key == geoms.Key);
                Brush fill = GetGeomBrush(data);
                this.individualStep += individualStepValue;

                foreach (var geom in geoms.Geoms)
                {
                    switch (geom.GeomType)
                    {
                        case StiMapGeomType.MoveTo:
                            {
                                if (path.PointCount > 0)
                                {
                                    path.CloseFigure();
                                    g.FillPath(fill, path);

                                    DrawState(g, path, geoms.Key);

                                    if (!IsBorderEmpty)
                                    {
                                        using (var pen = new Pen(MapStyle.BorderColor, (float)MapStyle.BorderSize))
                                        {
                                            g.DrawPath(pen, path);
                                        }
                                    }

                                    path.Dispose();
                                    path = new GraphicsPath();
                                }

                                var moveTo = (StiMoveToMapGeom)geom;
                                lastPoint = new PointF((float)moveTo.X, (float)moveTo.Y);
                            }
                            break;

                        case StiMapGeomType.Line:
                            {
                                var line = (StiLineMapGeom)geom;

                                var point = new PointF((float)line.X, (float)line.Y);
                                path.AddLine(lastPoint, point);
                                lastPoint = point;
                            }
                            break;

                        case StiMapGeomType.Bezier:
                            {
                                var bezier = (StiBezierMapGeom)geom;

                                var point1 = new PointF((float)bezier.X1, (float)bezier.Y1);
                                var point2 = new PointF((float)bezier.X2, (float)bezier.Y2);
                                var point3 = new PointF((float)bezier.X3, (float)bezier.Y3);
                                path.AddBezier(lastPoint, point1, point2, point3);

                                lastPoint = point3;
                            }
                            break;

                        case StiMapGeomType.Beziers:
                            {
                                var beziers = (StiBeziersMapGeom)geom;

                                var list = new List<PointF>()
                                {
                                    lastPoint
                                };
                                var p = PointF.Empty;
                                for (var index = 0; index < beziers.Array.Length; index += 2)
                                {
                                    p = new PointF((float)beziers.Array[index], (float)beziers.Array[index + 1]);
                                    list.Add(p);
                                }

                                list.Add(p);
                                lastPoint = p;

                                if (list.Count == 9)
                                {
                                    list.RemoveAt(8);
                                    list.RemoveAt(7);
                                }

                                path.AddBeziers(list.ToArray());
                            }
                            break;

                        case StiMapGeomType.Close:
                            {
                                path.CloseFigure();
                                g.FillPath(fill, path);

                                DrawState(g, path, geoms.Key);

                                if (!IsBorderEmpty)
                                {
                                    using (var pen = new Pen(MapStyle.BorderColor, (float)MapStyle.BorderSize))
                                    {
                                        g.DrawPath(pen, path);
                                    }
                                }

                                path.Dispose();
                                path = new GraphicsPath();
                            }
                            break;
                    }
                }

                if (path.PointCount > 0)
                {
                    path.CloseFigure();
                    g.FillPath(fill, path);

                    DrawState(g, path, geoms.Key);

                    if (!IsBorderEmpty)
                    {
                        using (var pen = new Pen(MapStyle.BorderColor, (float)MapStyle.BorderSize))
                        {
                            g.DrawPath(pen, path);
                        }
                    }
                }

                path.Dispose();

                if (map.ShowValue || map.DisplayNameType != StiDisplayNameType.None)
                    hashLabels.Add(geoms.Key);
            }

            if (hashLabels.Count > 0 && !skipLabels)
            {
                var compositingQualityState = g.CompositingQuality;
                var interpolationModeState = g.InterpolationMode;
                var smoothingModeState = g.SmoothingMode;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.High;
                g.SmoothingMode = SmoothingMode.HighQuality;

                var svgContainer = StiMapLoader.LoadResource(map.MapID.ToString());

                var typeface = new Font("Calibri", 18);
                var foregroundDark = new SolidBrush(Color.FromArgb(255, 37, 37, 37));
                var foregroundLight = new SolidBrush(Color.FromArgb(180, 251, 251, 251));

                foreach (var key in hashLabels)
                {
                    var path = svgContainer.HashPaths[key];
                    if (path.SkipText) continue;

                    string text = null;
                    var info = this.MapData.FirstOrDefault(x => x.Key == key);
                    switch (map.DisplayNameType)
                    {
                        case StiDisplayNameType.Full:
                            {
                                text = (info != null)
                                    ? info.Name
                                    : path.EnglishName;
                            }
                            break;

                        case StiDisplayNameType.Short:
                            {
                                text = StiMapHelper.PrepareIsoCode(path.ISOCode);
                            }
                            break;
                    }

                    if (map.ShowValue)
                    {
                        if (info != null && info.Value != null)
                        {
                            if (text == null)
                            {
                                text = info.Value.ToString();
                            }
                            else
                            {
                                text += Environment.NewLine;
                                text += info.Value.ToString();
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(text))
                    {
                        var bounds = path.Rect;
                        SizeF textSize = (path.SetMaxWidth)
                            ? g.MeasureString(text, typeface, bounds.Width)
                            : g.MeasureString(text, typeface, bounds.Width);

                        #region Calc position

                        int x = bounds.X + (int)((bounds.Width - textSize.Width) / 2);
                        int y = bounds.Y + (int)((bounds.Height - textSize.Height) / 2);

                        if (path.HorAlignment != null)
                        {
                            switch (path.HorAlignment.GetValueOrDefault())
                            {
                                case StiTextHorAlignment.Left:
                                    x = bounds.X;
                                    break;

                                case StiTextHorAlignment.Right:
                                    x = bounds.Right - (int)textSize.Width;
                                    break;
                            }
                        }

                        if (path.VertAlignment != null)
                        {
                            switch (path.VertAlignment.GetValueOrDefault())
                            {
                                case StiVertAlignment.Top:
                                    y = bounds.Y;
                                    break;

                                case StiVertAlignment.Bottom:
                                    y = bounds.Bottom - (int)textSize.Height;
                                    break;
                            }
                        }

                        #endregion

                        if (path.SetMaxWidth)
                        {
                            var rectDark = new RectangleF(x, y, textSize.Width + 20, textSize.Height);
                            var rectLight = new RectangleF(x + 1, y + 1, textSize.Width + 20, textSize.Height);

                            g.DrawString(text, typeface, foregroundLight, rectLight);
                            g.DrawString(text, typeface, foregroundDark, rectDark);
                        }
                        else
                        {
                            var textPosDark = new Point(x, y);
                            var textPosLight = new Point(x + 1, y + 1);

                            g.DrawString(text, typeface, foregroundLight, textPosLight);
                            g.DrawString(text, typeface, foregroundDark, textPosDark);
                        }
                    }
                }

                g.CompositingQuality = compositingQualityState;
                g.InterpolationMode = interpolationModeState;
                g.SmoothingMode = smoothingModeState;
            }
        }
        #endregion

        #region Methods
        private void FillGroupColors()
        {
            colorsContainer.Init(this.map);

            foreach (string key in hashGroup.Keys.OrderBy(x => x))
            {
                var value = hashGroup[key];
                value.Fill = colorsContainer.GetColor();
                value.Fill1 = colorsContainer.GetColor1();
            }
        }

        public void UpdateHeatmapWithGroup()
        {
            if (map.MapType == StiMapType.HeatmapWithGroup)
                this.heatmapWithGroupInfo = new HeatmapWithGroupInfo(this, map, MapData);
        }

        public void UpdateGroupedData()
        {
            if (this.MapData != null)
            {
                foreach (var data in this.MapData)
                {
                    if (!string.IsNullOrEmpty(data.Group))
                    {
                        double? value = null;
                        double result = 0;
                        if (data.Value != null && double.TryParse(data.Value, out result))
                            value = null;

                        StiMapGroup group = null;
                        if (!hashGroup.ContainsKey(data.Group))
                        {
                            group = new StiMapGroup
                            {
                                MinValue = value,
                                MaxValue = value
                            };

                            hashGroup.Add(data.Group, group);
                        }
                        else
                        {
                            group = hashGroup[data.Group];

                            if (value != null)
                            {
                                if (group.MinValue == null || group.MaxValue == null)
                                {
                                    group.MinValue = 0d;
                                    group.MaxValue = 0d;
                                }

                                if (group.MinValue > value)
                                    group.MinValue = value;
                                else if (group.MaxValue < value)
                                    group.MaxValue = value;
                            }
                        }
                    }
                }

                FillGroupColors();
            }
        }

        private Brush ParseHexColor(string color)
        {
            try
            {
                if (!string.IsNullOrEmpty(color))
                {
                    if (color.StartsWithInvariant("#"))
                        return new SolidBrush(ColorTranslator.FromHtml(color));
                    else
                        return new SolidBrush(Color.FromName(color));
                }
            }
            catch { }

            return null;
        }

        private StiBrush ParseHexColor1(string color)
        {
            try
            {
                if (!string.IsNullOrEmpty(color) && color.StartsWithInvariant("#"))
                    return new StiSolidBrush(ColorTranslator.FromHtml(color));
            }
            catch { }

            return null;
        }
        #endregion

        public StiGdiMapContextPainter(StiMap map)
        {
            this.map = map;
        }
    }
}