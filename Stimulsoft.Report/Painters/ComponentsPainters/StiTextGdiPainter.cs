﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Imaging;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Events;

namespace Stimulsoft.Report.Painters
{
    public class StiTextGdiPainter : StiComponentGdiPainter
    {
        #region Methods
        public virtual RectangleD PaintIndicator(StiText textComp, Graphics g, RectangleD rect)
        {
            if (textComp.Indicator == null) return rect;

            var painter = StiIndicatorTypePainter.GetPainter(textComp.Indicator.GetType(), StiGuiMode.Gdi);
            return painter.Paint(g, textComp, rect);
        }

        public virtual void PaintText(StiText textComp, Graphics g, RectangleD rect)
        {
            var text = textComp.GetTextForPaint();
            if (textComp.IsDesigning && !textComp.Report.Info.QuickInfoOverlay && textComp.Report.Info.QuickInfoType != StiQuickInfoType.None)
                text = textComp.GetQuickInfo();

            if (string.IsNullOrEmpty(text)) return;

            //speed optimization
            var fontSize = textComp.Font.Size * textComp.Page.Zoom * SystemWinApi.GetWindowsScale().Scale;
            if (fontSize < 1 && !textComp.IsPrinting && StiOptions.Engine.TinyTextOptimization)
            {
                var brush = new StiSolidBrush(Color.FromArgb(128, StiBrush.ToColor(textComp.TextBrush)));
                StiDrawing.FillRectangle(g, brush, rect);
                return;
            }

            if (textComp.TextQuality == StiTextQuality.Wysiwyg)
                StiWysiwygTextRender.DrawString(g, rect, text, textComp);

            else if (textComp.TextQuality == StiTextQuality.Typographic)
                StiTypographicTextRender.DrawString(g, rect, text, textComp);

            else
                StiStandardTextRenderer.DrawString(g, rect, text, textComp);
        }

        public virtual void PaintBackground(StiText text, Graphics g, RectangleD rect)
        {
            if (text.Brush is StiSolidBrush &&
                ((StiSolidBrush)text.Brush).Color.A == 0 &&
                text.Report.Info.FillComponent &&
                text.IsDesigning)
            {
                var color = Color.FromArgb(150, Color.White);

                StiDrawing.FillRectangle(g, color, rect.Left, rect.Top, rect.Width, rect.Height);
            }
            else
                StiDrawing.FillRectangle(g, text.Brush, rect);
        }

        public virtual void PaintBorder(StiText text, Graphics g, RectangleD rect, bool drawBorderFormatting, bool drawTopmostBorderSides)
        {
            if (text.HighlightState == StiHighlightState.Hide)
                base.PaintBorder(text, g, rect, text.Page.Zoom * SystemWinApi.GetWindowsScale().Scale, drawBorderFormatting, drawTopmostBorderSides);
        }


        public virtual void PaintLinesOfUnderlining(StiText text, Graphics g, RectangleD rect)
        {
            if (text.LinesOfUnderline == StiPenStyle.None) return;

            var heightFont = text.Font.GetHeight() * text.Page.Zoom * SystemWinApi.GetWindowsScale().Scale;
            using (var pen = new Pen(text.Border.Color))
            {
                pen.DashStyle = StiPenUtils.GetPenStyle(text.LinesOfUnderline);
                pen.Width = (int)(text.Border.Size * text.Page.Zoom * SystemWinApi.GetWindowsScale().Scale);

                if (text.VertAlignment == StiVertAlignment.Top)
                {
                    var posy = heightFont + rect.Top;
                    while (posy < rect.Bottom)
                    {
                        PaintOneLineOfUnderline(text, g, pen, rect.X, rect.Right, posy);
                        posy += heightFont;
                    }
                }
                else if (text.VertAlignment == StiVertAlignment.Center)
                {
                    var posy = rect.Top + rect.Height / 2 + heightFont * 0.5 - heightFont * 0.1;
                    while (posy > rect.Top && posy < rect.Bottom)
                    {
                        PaintOneLineOfUnderline(text, g, pen, rect.X, rect.Right, posy);
                        posy -= heightFont;
                    }

                    posy = rect.Top + rect.Height / 2 + heightFont * 0.5 - heightFont * 0.1;
                    while (posy > rect.Top && posy < rect.Bottom)
                    {
                        PaintOneLineOfUnderline(text, g, pen, rect.X, rect.Right, posy);
                        posy += heightFont;
                    }
                }
                else if (text.VertAlignment == StiVertAlignment.Bottom)
                {
                    var posy = rect.Bottom - heightFont - heightFont * 0.1;
                    while (posy > rect.Top)
                    {
                        PaintOneLineOfUnderline(text, g, pen, rect.X, rect.Right, posy);
                        posy -= heightFont;
                    }
                }
            }
        }

        public virtual void PaintOneLineOfUnderline(StiText text, Graphics g, Pen pen, double x1, double x2, double y)
        {
            if (text.LinesOfUnderline == StiPenStyle.Double)
            {
                g.DrawLine(pen,
                    (float)x1,
                    (float)(y - 1),
                    (float)x2,
                    (float)(y - 1));

                g.DrawLine(pen,
                    (float)x1,
                    (float)(y + 1),
                    (float)x2,
                    (float)(y + 1));
            }
            else
            {
                g.DrawLine(pen,
                    (float)x1,
                    (float)y,
                    (float)x2,
                    (float)y);
            }
        }

        private void PaintMarkerFields(StiText text, Graphics g, RectangleD rect)
        {
            if (!text.GetMarkerFieldResult()) return;

            var zoom = text.Report.Info.Zoom;
            var rectMarker = new RectangleD(
                rect.Right - 8 * zoom,
                rect.Bottom - 8 * zoom,
                6 * zoom,
                6 * zoom);

            g.FillRectangle(Brushes.Red, rectMarker.ToRectangleF());
        }
        #endregion

        #region Methods.Painter
        public override Image GetImage(StiComponent component, ref float zoom, StiExportFormat format)
        {
            var text = (StiText)component;
            if (text.Indicator != null)
                zoom *= 2;

            var resZoom = text.Report.Info.Zoom;
            text.Report.Info.Zoom = zoom;
            var rect = text.GetPaintRectangle();
            rect.X = 0;
            rect.Y = 0;

            //fix for border width in html
            if ((format == StiExportFormat.HtmlTable || format == StiExportFormat.ImagePng) && StiOptions.Export.Html.PrintLayoutOptimization &&
                (text.Border != null) && (text.Border.Style != StiPenStyle.None) && (text.Border.Side != StiBorderSides.None))
            {
                rect.Height -= text.Border.Size;
                rect.Width -= text.Border.Size;
                if (rect.Height < 0) rect.Height = 0;
                if (rect.Width < 0) rect.Width = 0;
            }

            var imageWidth = (int)rect.Width;
            var imageHeight = (int)rect.Height;

            //Bitmap bmp = new Bitmap(imageWidth, imageHeight);
            Image bmp;
            if (format == StiExportFormat.Pdf && text.CheckAllowHtmlTags() && !text.IsExportAsImage(format) && StiOptions.Engine.FullTrust)
                bmp = CreateMetafileImage();
            else
                bmp = new Bitmap(imageWidth, imageHeight);

            if (text.Brush != null && text.Brush is StiSolidBrush && (text.Brush as StiSolidBrush).Color == Color.Transparent)
                Stimulsoft.Report.Export.StiExportUtils.DisableFontSmoothing(component.Report);

            using (var g = Graphics.FromImage(bmp))
            {
                g.PageUnit = GraphicsUnit.Pixel;
                if ((format == StiExportFormat.Pdf) || (format == StiExportFormat.ImagePng))
                {
                    if ((text.Brush != null) && (text.Brush is StiSolidBrush) && ((text.Brush as StiSolidBrush).Color.A == 0))
                    {
                        g.FillRectangle(new SolidBrush(Color.FromArgb(1, 255, 255, 255)), new Rectangle(0, 0, imageWidth, imageHeight));
                    }
                }
                else
                {
                    g.FillRectangle(Brushes.White, new Rectangle(0, 0, imageWidth, imageHeight));
                }

                StiDrawing.FillRectangle(g, text.Brush, rect);
                rect = PaintIndicator(text, g, rect);

                rect = text.ConvertTextMargins(rect, true);
                rect = text.ConvertTextBorders(rect, true);

                PaintText(text, g, rect);
                PaintLinesOfUnderlining(text, g, rect);
            }
            text.Report.Info.Zoom = resZoom;
            return bmp;
        }

        private static Image CreateMetafileImage()
        {
            Image bmp = null; 
            using (var bmpTemp = new Bitmap(1, 1))
            using (var grfx = Graphics.FromImage(bmpTemp))
            {
                var ipHdc = grfx.GetHdc();
                bmp = new Metafile(ipHdc, EmfType.EmfOnly);
                grfx.ReleaseHdc(ipHdc);
            }
            return bmp;
        }

        public override void Paint(StiComponent component, StiPaintEventArgs e)
        {
            var text = (StiText)component;

            if ((!e.DrawBorderFormatting) && e.DrawTopmostBorderSides && (!text.Border.Topmost))
                return;

            if (!(e.Context is Graphics))
                throw new Exception("StiGdiPainter can work only with System.Drawing.Graphics context!");

            if (e.DrawBorderFormatting)
                component.InvokePainting(component, e);

            if (!e.Cancel && (!(component.Enabled == false && component.IsDesigning == false)))
            {
                var g = e.Graphics;

                var rect = component.GetPaintRectangle();
                
                if (rect.Width > 0 && rect.Height > 0 && (e.ClipRectangle.IsEmpty || rect.IntersectsWith(e.ClipRectangle)))
                {
                    //if (component.Report.IsExporting && (text.Brush != null) && (text.Brush is StiSolidBrush) && ((text.Brush as StiSolidBrush).Color == Color.Transparent))
                    //if (component.Report.IsExporting && (text.Brush != null) && (text.Brush is StiSolidBrush) && ((text.Brush as StiSolidBrush).Color == Color.Transparent) && (text.TextQuality == StiTextQuality.Wysiwyg))
                    if (component.Report.IsExporting && (text.TextQuality == StiTextQuality.Wysiwyg))
                    {
                        Stimulsoft.Report.Export.StiExportUtils.DisableFontSmoothing(component.Report);
                    }

                    #region Fill rectangle
                    if (e.DrawBorderFormatting)
                        PaintBackground(text, g, rect);
                    #endregion

                    #region Markers
                    if (e.DrawBorderFormatting)
                    {
                        if (text.HighlightState == StiHighlightState.Hide && text.Border.Side != StiBorderSides.All)
                            PaintMarkers(text, g, rect);
                    }
                    #endregion
                                        
                    var borderRect = rect;

                    rect = PaintIndicator(text, g, rect);

                    #region Text margins
                    rect = text.ConvertTextMargins(rect, true);
                    rect = text.ConvertTextBorders(rect, true);
                    #endregion

                    #region Paint text
                    if (e.DrawBorderFormatting)
                        PaintText(text, g, rect);
                    #endregion

                    #region Lines of underlining
                    if (e.DrawBorderFormatting)
                        PaintLinesOfUnderlining(text, g, rect);
                    #endregion

                    #region Border
                    PaintBorder(text, g, borderRect, e.DrawBorderFormatting, e.DrawTopmostBorderSides || (!text.Border.Topmost));
                    #endregion

                    if (e.DrawBorderFormatting)
                        PaintMarkerFields(text, e.Graphics, rect);
                }

                if (e.DrawBorderFormatting)
                {
                    PaintEvents(component, e.Graphics, rect);
                    PaintConditions(component, e.Graphics, rect);
                    PaintQuickButtons(component, e.Graphics);
                    PaintInteraction(component, e.Graphics);
                }
            }
            e.Cancel = false;
            
            if (e.DrawBorderFormatting)
                component.InvokePainted(component, e);

        }
        #endregion
    }
}
