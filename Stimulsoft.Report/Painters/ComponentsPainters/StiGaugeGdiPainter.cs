﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Drawing;
using Stimulsoft.Gauge.Painters;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.Gauge;
using Stimulsoft.Report.Gauge.Helpers;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Stimulsoft.Report.Painters
{
    public class StiGaugeGdiPainter : StiComponentGdiPainter
    {
        #region Methods.Painter
        public override void Paint(StiComponent component, StiPaintEventArgs e)
        {
            var gauge = component as StiGauge;

            if (!(e.Context is Graphics))
                throw new Exception("StiGdiPainter can work only with System.Drawing.Graphics context!");

            if (!e.Cancel && (!(component.Enabled == false && component.IsDesigning == false)))
            {
                var g = e.Graphics;

                var rect = component.GetPaintRectangle();
                
                if (rect.Width > 0 && rect.Height > 0 && (e.ClipRectangle.IsEmpty || rect.IntersectsWith(e.ClipRectangle)))
                {
                    DrawGauge(g, gauge, rect.ToRectangleF());
                }
            }
        }

        public void DrawGauge(Graphics g, StiGauge gauge, RectangleF rect)
        {
            this.DrawGauge(g, gauge, rect, gauge.Page != null ? (float)gauge.Page.Zoom : 1f);
        }

        public void DrawGauge(Graphics g, StiGauge gauge, RectangleF rect, float zoom)
        {
            this.DrawGauge(g, gauge, rect, gauge.Page != null ? (float)gauge.Page.Zoom : 1f, true);
        }

        public void DrawGauge(Graphics g, StiGauge gauge, RectangleF rect, float zoom, bool isClip)
        {
            #region Fill rectangle
            var fillComponent = gauge.Report?.Info?.FillComponent ?? false;
            if (gauge.Brush is StiSolidBrush && ((StiSolidBrush)gauge.Brush).Color == Color.Transparent && fillComponent && gauge.IsDesigning)
            {
                var color = Color.FromArgb(150, Color.White);
                StiDrawing.FillRectangle(g, color, rect.Left, rect.Top, rect.Width, rect.Height);
            }
            else
            {
                StiDrawing.FillRectangle(g, gauge.Brush, rect);
            }
            #endregion

            if (isClip)
                g.SetClip(rect);

            if (gauge.IsDesigning)
            {
                StiGaugeV2InitHelper.Prepare(gauge);
            }

            var context = new StiGdiGaugeContextPainter(g, gauge, rect, zoom);
            gauge.DrawGauge(context);
            context.Render();

            if (isClip)
                g.ResetClip();

            #region Draw Border
            if (gauge.Border.Side == StiBorderSides.None && gauge.IsDesigning)
            {
                using (var pen = new Pen(Color.Gray))
                {
                    pen.DashStyle = DashStyle.Dash;
                    StiDrawing.DrawRectangle(g, pen, RectangleD.CreateFromRectangle(rect));
                }
            }
            if (gauge.HighlightState == StiHighlightState.Hide)
            {
                PaintBorder(gauge, g, RectangleD.CreateFromRectangle(rect), true, true);
            }
            #endregion
        }

        public override Image GetImage(StiComponent component, ref float zoom, StiExportFormat format)
        {
            var gauge = component as StiGauge;

            double resZoom = gauge.Report.Info.Zoom;
            if (format != StiExportFormat.HtmlSpan &&
                format != StiExportFormat.HtmlDiv &&
                format != StiExportFormat.HtmlTable) zoom *= 2;
            gauge.Report.Info.Zoom = zoom;

            var rect = gauge.GetPaintRectangle();
            rect.X = 0;
            rect.Y = 0;

            int imageWidth = (int)rect.Width + 2;
            int imageHeight = (int)rect.Height + 2;

            var bmp = new Bitmap(imageWidth, imageHeight);
            using (var g = Graphics.FromImage(bmp))
            {
                g.PageUnit = GraphicsUnit.Pixel;

                if (((format == StiExportFormat.Pdf) || (format == StiExportFormat.ImagePng)) && (gauge.Brush != null) &&
                    (((gauge.Brush is StiSolidBrush) && ((gauge.Brush as StiSolidBrush).Color.A == 0)) || (gauge.Brush is StiEmptyBrush)))
                {
                    g.Clear(Color.FromArgb(1, 255, 255, 255));
                }
                else
                {
                    if (((format == StiExportFormat.Excel) || (format == StiExportFormat.Excel2007) || (format == StiExportFormat.Word2007) || (format == StiExportFormat.Rtf) ||
                        (format == StiExportFormat.RtfFrame) || (format == StiExportFormat.RtfTable) || (format == StiExportFormat.RtfWinWord)) &&
                        (gauge.Brush != null) && (((gauge.Brush is StiSolidBrush) && ((gauge.Brush as StiSolidBrush).Color.A < 16)) || (gauge.Brush is StiEmptyBrush)))
                    {
                        g.Clear(Color.White);
                    }
                    else
                    {
                        g.Clear(StiBrush.ToColor(gauge.Brush));
                    }
                }

                rect.X = 0;
                rect.Y = 0;

                var rectF = rect.ToRectangleF();
                var context = new StiGdiGaugeContextPainter(g, gauge, rectF, zoom);
                gauge.DrawGauge(context);
                context.Render();
            }
            gauge.Report.Info.Zoom = resZoom;

            return bmp;
        }
        #endregion
    }
}