﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.Engine;

namespace Stimulsoft.Report.Painters
{
    public class StiBandGdiPainter : StiContainerGdiPainter
    {
        #region Methods
        /// <summary>
        /// Paints the columns.
        /// </summary>
        /// <param name="g">The Graphics to paint on.</param>
        public override void PaintColumns(StiContainer container, Graphics g)
        {
            var dataBand = container as StiDataBand;
            if (dataBand == null)
            {
                if (container is StiColumnHeaderBand) dataBand = StiHeaderBandV1Builder.GetMaster(container as StiColumnHeaderBand) as StiDataBand;
                if (container is StiColumnFooterBand) dataBand = StiFooterBandV1Builder.GetMaster(container as StiColumnFooterBand) as StiDataBand;
            }
            if (container.IsDesigning && dataBand != null && dataBand.Columns >= 2)
            {
                double zoom = dataBand.Page.Zoom;

                var rect = dataBand.GetPaintRectangle();

                double columnWidth = dataBand.Page.Unit.ConvertToHInches(dataBand.GetColumnWidth()) * zoom;
                double columnGaps = dataBand.Page.Unit.ConvertToHInches(dataBand.ColumnGaps) * zoom;

                rect.X += columnWidth + columnGaps;
                var comps = dataBand.GetComponents();

                foreach (StiComponent comp in comps)
                {
                    var compRect = comp.GetPaintRectangle();
                    if (compRect.Width > 0 && compRect.Height > 0)
                    {
                        using (var pen = new Pen(Color.Blue, 1))
                        {
                            pen.DashStyle = DashStyle.Dash;

                            for (int index = 0; index < dataBand.Columns - 1; index++)
                            {
                                compRect.X += columnWidth + columnGaps;

                                StiDrawing.FillRectangle(g, Color.FromArgb(20, Color.Blue), compRect);
                                StiDrawing.DrawRectangle(g, pen, compRect);
                            }
                        }
                    }
                }
            }
        }


        public override void PaintSelection(StiComponent component, StiPaintEventArgs e)
        {
            var g = e.Graphics;
            if (component.IsDesigning && component.IsSelected && (!component.Report.Info.IsComponentsMoving))
            {
                var rect = component.GetPaintRectangle().ToRectangleF();

                int size = 2;
                if (component.Linked) size = 3;

                DrawSelectedPoint(g, size, rect.Left, rect.Top);
                DrawSelectedPoint(g, size, rect.Right, rect.Top);
                DrawSelectedPoint(g, size, rect.Left, rect.Bottom);
                DrawSelectedPoint(g, size, rect.Right, rect.Bottom);

                if (component.IsCross)
                {
                    var image = StiStaticResources.ResizeHorizontally;
                    var imageRect = new Rectangle(
                        (int)Math.Round((rect.Right - image.Width / 2)),
                        (int)Math.Round(rect.Top + rect.Height / 2 - image.Height / 2), image.Width, image.Height);

                    g.DrawImage(image, imageRect);
                }
                else
                {
                    var image = StiStaticResources.ResizeVertically;
                    var imageRect = Rectangle.Empty;

                    if (StiOptions.Engine.DockPageFooterToBottom && component is StiPageFooterBand)
                    {
                        imageRect = new Rectangle(
                            (int)Math.Round((rect.X + rect.Width / 2 - image.Width / 2)),
                            (int)Math.Round(rect.Top - image.Height / 2), image.Width, image.Height);
                    }
                    else
                    {
                        imageRect = new Rectangle(
                            (int)Math.Round((rect.X + rect.Width / 2 - image.Width / 2)),
                            (int)Math.Round(rect.Bottom - image.Height / 2), image.Width, image.Height);
                    }
                    g.DrawImage(image, imageRect);
                }
            }
        }


        private void DrawSelectedPoint(Graphics g, float size, float x, float y)
        {
            var rect = new RectangleF(x - size, y - size, size * 2 + 1, size * 2 + 1);
            g.FillRectangle(Brushes.White, rect);
            g.DrawRectangle(Pens.DimGray, rect.X, rect.Y, rect.Width, rect.Height);
        }
        #endregion

        #region Methods.Painter
        public override void Paint(StiComponent component, StiPaintEventArgs e)
        {
            var band = component as StiBand;

            if ((!e.DrawBorderFormatting) && e.DrawTopmostBorderSides && (!band.Border.Topmost))
                return;

            if (!(e.Context is Graphics))
                throw new Exception("StiGdiPainter can work only with System.Drawing.Graphics context!");

            if (e.DrawBorderFormatting)
                band.InvokePainting(band, e);

            var g = e.Graphics;
            double zoom = band.Page.Zoom;

            if (!e.Cancel)
            {
                if (band.IsDesigning)
                {
                    var rect = band.GetPaintRectangle();

                    var headerRect = new RectangleD(rect.Left, rect.Top - band.HeaderSize * zoom, rect.Width, band.HeaderSize * zoom);
                    var fullRect = headerRect;
                    fullRect.Height += rect.Height;

                    if (fullRect.Width > 0 && fullRect.Height > 0 && (e.ClipRectangle.IsEmpty || fullRect.IntersectsWith(e.ClipRectangle)))
                    {
                        #region Fill bands
                        if (e.DrawBorderFormatting)
                        {
                            if (band.Brush is StiSolidBrush && ((StiSolidBrush)band.Brush).Color == Color.Transparent &&
                                band.Report.Info.FillBands && band.IsDesigning)
                            {
                                var color = band.Report.Info.GetFillColor(band.HeaderEndColor);
                                StiDrawing.FillRectangle(g, color, rect.Left, rect.Top, rect.Width, rect.Height);
                            }
                            else StiDrawing.FillRectangle(g, band.Brush, rect);
                        }
                        #endregion

                        #region Headers
                        if (band.Report.Info.ShowHeaders)
                        {
                            if (e.DrawBorderFormatting)
                            {
                                var headerColor = Color.FromArgb(220, band.HeaderStartColor);
                                using (var brush = new SolidBrush(headerColor))
                                {
                                    g.FillRectangle(brush, headerRect.ToRectangleF());
                                }

                                #region Band name
                                using (var stringFormat = new StringFormat())
                                using (var font = new Font("Segoe UI", (float)(9 * zoom)))
                                {
                                    stringFormat.LineAlignment = StringAlignment.Center;
                                    stringFormat.Alignment = StringAlignment.Near;
                                    stringFormat.Trimming = StringTrimming.EllipsisCharacter;
                                    stringFormat.FormatFlags = StringFormatFlags.NoWrap;

                                    StiTextDrawing.DrawString(g, band.GetHeaderText(), font, Brushes.Black, headerRect, stringFormat);

                                    #region StiDataBand
                                    var dataBand = band as StiDataBand;
                                    if (dataBand != null)
                                    {
                                        float width = g.MeasureString(band.GetHeaderText(), font, new SizeF((float)rect.Width, (float)rect.Height)).Width;
                                        if (dataBand.MasterComponent != null && width > 0)
                                        {
                                            stringFormat.LineAlignment = StringAlignment.Center;
                                            stringFormat.Alignment = StringAlignment.Far;
                                            stringFormat.Trimming = StringTrimming.EllipsisCharacter;
                                            stringFormat.FormatFlags = StringFormatFlags.NoWrap;

                                            var rectMaster = new RectangleD(headerRect.Left + width, headerRect.Top, headerRect.Width - width, headerRect.Height);
                                            StiTextDrawing.DrawString(g, StiLocalization.Get("PropertyMain", "MasterComponent") + ": " + dataBand.MasterComponent.Name, font, Brushes.Black, rectMaster, stringFormat);

                                        }
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                        }

                        #region Border
                        if (band.Border.Side == StiBorderSides.None && band.IsDesigning)
                        {
                            if (e.DrawBorderFormatting)
                            {
                                var headerColor = Color.FromArgb(220, band.HeaderStartColor);
                                using (var pen = new Pen(headerColor))
                                {
                                    pen.DashStyle = DashStyle.Dash;
                                    StiDrawing.DrawRectangle(g, pen, rect.Left, rect.Top, rect.Width, rect.Height);
                                }
                            }
                        }
                        else
                        {
                            if (band.HighlightState == StiHighlightState.Hide)
                            {
                                PaintBorder(band, g, rect, zoom, e.DrawBorderFormatting, e.DrawTopmostBorderSides || (!band.Border.Topmost));
                            }
                        }
                        #endregion
                        #endregion

                        if (band.IsDesigning)
                        {
                            if (e.DrawBorderFormatting)
                            {
                                PaintColumns(band, g);
                                var dataBand = band as StiDataBand;
                                if (dataBand == null)
                                {
                                    if (band is StiColumnHeaderBand)
                                    {
                                        dataBand = StiHeaderBandV1Builder.GetMaster(band as StiHeaderBand) as StiDataBand;
                                    }
                                    if (band is StiColumnFooterBand)
                                    {
                                        dataBand = StiFooterBandV1Builder.GetMaster(band as StiFooterBand) as StiDataBand;
                                    }
                                }

                                if (dataBand != null && dataBand.Columns > 1)
                                {
                                    #region Paint columns
                                    double columnWidth = band.Page.Unit.ConvertToHInches(dataBand.GetColumnWidth()) * zoom;
                                    double columnGaps = band.Page.Unit.ConvertToHInches(dataBand.ColumnGaps) * zoom;

                                    double pos = columnWidth + rect.Left;
                                    using (var pen = new Pen(Color.Red))
                                    {
                                        pen.DashStyle = DashStyle.Dash;

                                        for (int index = 1; index < dataBand.Columns; index++)
                                        {
                                            g.DrawLine(pen,
                                                (float)pos, (float)rect.Top,
                                                (float)pos,
                                                (float)(rect.Top + rect.Height));

                                            g.DrawLine(pen,
                                                (float)(pos + columnGaps), (float)rect.Top,
                                                (float)(pos + columnGaps),
                                                (float)(rect.Top + rect.Height));

                                            pos += columnWidth + columnGaps;
                                        }

                                        g.DrawLine(pen,
                                            (float)pos, (float)rect.Top,
                                            (float)pos,
                                            (float)(rect.Top + rect.Height));
                                    }
                                    #endregion
                                }

                                #region Special Dock Service
                                if (band.RectangleMoveComponent != null)
                                {
                                    var r = component.Page.Unit.ConvertToHInches((RectangleD)band.RectangleMoveComponent).Multiply(component.Page.Zoom);
                                    r.X += fullRect.Left;
                                    r.Y += fullRect.Top;
                                    if (band.Report.Info.ShowHeaders)
                                    {
                                        r.Y += headerRect.Height;
                                    }

                                    using (var brush = new SolidBrush(Color.FromArgb(100, Color.Red)))
                                    {
                                        StiDrawing.FillRectangle(g, brush, r);
                                    }
                                }
                                #endregion
                            }
                        }

                        if (e.DrawBorderFormatting)
                        {
                            PaintEvents(band, g, rect);
                            PaintConditions(band, g, rect);
                        }
                    }
                }
            }
            e.Cancel = false;
            
            if (e.DrawBorderFormatting)
            {
                band.InvokePainted(band, e);

                PaintComponents(band, e);
                PaintQuickButtons(band, e.Graphics);
            }
        }
        #endregion
    }
}