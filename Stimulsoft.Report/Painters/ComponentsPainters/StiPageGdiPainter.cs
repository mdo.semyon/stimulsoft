﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Security.Cryptography;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Licenses;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dialogs;
using Stimulsoft.Report.Design;

namespace Stimulsoft.Report.Painters
{
    public class StiPageGdiPainter : StiContainerGdiPainter, IStiPagePainter
    {
        #region Methods
        /// <summary>
        /// Draws page numbers.
        /// </summary>
        public virtual void DrawPageNumber(StiPage page, Graphics g, int pageNumber, RectangleD pageRect)
        {
            var scale = SystemWinApi.GetWindowsScale().Scale;
            
            if (page.Zoom * scale > .1d * scale)
            {
                using (var sf = new StringFormat())
                using (var font = new Font("Arial", 14))
                using (var brush = new SolidBrush(Color.FromArgb(100, Color.Blue)))
                {
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    sf.FormatFlags = StringFormatFlags.NoWrap;
                    sf.Trimming = StringTrimming.EllipsisPath;

                    g.DrawString(StiLocalization.Get("Components", "StiPage") + pageNumber.ToString(), font,
                        brush, pageRect.ToRectangleF(), sf);
                }
            }
        }

        public virtual void PaintTableLines(StiPage page, Graphics g)
        {
            if (page.Report.Info.IsComponentsMoving && page.Report.Info.CurrentAction == StiAction.Move) return;

            if (StiTableHelper.IsTableMode(page.Report.Designer))
            {
                var xx = new Hashtable();
                var yy = new Hashtable();

                StiTableHelper.GetSelectedLines(page.Report.Designer, ref xx, ref yy, true);
                var rect = StiTableHelper.GetSelectedRectangle(page.Report.Designer, true);

                Color penColor = Color.Blue;
                if (page is StiForm) penColor = Color.DimGray;

                using (var pen = new Pen(penColor))
                {
                    pen.DashStyle = DashStyle.Dash;
                    foreach (double value in xx.Keys)
                    {
                        if (!double.IsNaN(value))
                            g.DrawLine(pen, (float)value, (float)rect.Y, (float)value, (float)rect.Bottom);
                    }

                    foreach (double value in yy.Keys)
                    {
                        if (!double.IsNaN(value))
                            g.DrawLine(pen, (float)rect.X, (float)value, (float)rect.Right, (float)value);
                    }
                }
            }
        }
        #endregion

        #region Methods.Watermark
        public virtual void PaintText(StiWatermark watermark, Graphics g, RectangleD rect, double zoom, bool isPrinting)
        {
            if (watermark.Enabled && !string.IsNullOrEmpty(watermark.Text))
            {
                float fontSize = (float)watermark.Font.Size * (float)zoom;

                if (isPrinting)
                {
                    if ((watermark.Font.Unit != GraphicsUnit.Pixel) && (watermark.Font.Unit != GraphicsUnit.World))
                    {
                        fontSize *= 96f / 100f;
                    }
                }

                using (var brush = StiBrush.GetBrush(watermark.TextBrush, rect.ToRectangleF()))
                using (var font = new Font(watermark.Font.FontFamily, fontSize,
                           watermark.Font.Style, watermark.Font.Unit,
                           watermark.Font.GdiCharSet, watermark.Font.GdiVerticalFont))
                {
                    var defaultHint = g.TextRenderingHint;
                    g.TextRenderingHint = TextRenderingHint.AntiAlias;

                    var textOptions = new StiTextOptions
                    {
                        Angle = watermark.Angle,
                        RightToLeft = watermark.RightToLeft
                    };

                    StiTextDrawing.DrawString(g, watermark.Text, font, brush, rect,
                        textOptions, StiTextHorAlignment.Center, StiVertAlignment.Center, true, watermark.Angle);

                    g.TextRenderingHint = defaultHint;
                }
            }
        }

        public virtual void PaintImage(StiReport report, StiWatermark watermark, Graphics g, RectangleD rect, double zoom)
        {
            if (!watermark.Enabled) return;

            var image = watermark.GetImage(report);
            if (image != null)
            {
                
                {
                    var state = g.Save();
                    g.SetClip(rect.ToRectangleF());

                    if (watermark.ImageStretch)
                    {
                        var destRect = rect.ToRectangleF();

                        using (var gdiImage = StiImageConverter.BytesToImage(image, (int)destRect.Width, (int)destRect.Height))
                        {
                            #region AspectRatio
                            if (watermark.AspectRatio)
                            {
                                float xRatio = destRect.Width / gdiImage.Width;
                                float yRatio = destRect.Height / gdiImage.Height;

                                if (xRatio > yRatio)
                                {
                                    destRect.X = (destRect.Width - gdiImage.Width * yRatio) / 2;
                                    destRect.Width = gdiImage.Width * yRatio;
                                }
                                else
                                {
                                    destRect.Y = (destRect.Height - gdiImage.Height * xRatio) / 2;
                                    destRect.Height = gdiImage.Height * xRatio;
                                }
                            }
                            #endregion

                            g.DrawImage(gdiImage, destRect);
                        }
                    }
                    else
                    {
                        using (var gdiImage = StiImageConverter.BytesToImage(image))
                        {
                            zoom *= watermark.ImageMultipleFactor;
                            var imageSize = new SizeD(gdiImage.Size.Width * zoom, gdiImage.Height * zoom);
                            if (watermark.ImageTiling)
                            {
                                PaintTileImage(gdiImage, g, rect, imageSize);
                            }
                            else
                            {
                                var imageRect = StiRectangleUtils.AlignSizeInRect(rect, imageSize, watermark.ImageAlignment);
                                g.DrawImage(gdiImage, imageRect.ToRectangleF());
                            }
                        }
                    }
                    g.Restore(state);
                }
            }
        }

        public virtual void PaintTileImage(Image watermarkImage, Graphics g, RectangleD rect, SizeD imageSize)
        {
            float y = (float)rect.Y;

            while (y < rect.Bottom)
            {
                float x = (float)rect.X;
                while (x < rect.Right)
                {
                    g.DrawImage(watermarkImage, new RectangleF(x, y, (float)imageSize.Width, (float)imageSize.Height));
                    x += (float)imageSize.Width;
                }
                y += (float)imageSize.Height;
            }
        }

        public Bitmap GetWatermarkImage(StiPage page, double zoom, bool useMargins)
        {
            int imgWidth = (int)Math.Round(page.Unit.ConvertToHInches(page.Width + (useMargins ? page.Margins.Left + page.Margins.Right : 0)) * zoom);
            int imgHeight = (int)Math.Round(page.Unit.ConvertToHInches(page.Height + (useMargins ? page.Margins.Top + page.Margins.Bottom : 0)) * zoom);

            var image = new Bitmap(imgWidth, imgHeight);
            using (var g = Graphics.FromImage(image))
            using (var brush = StiBrush.GetBrush(page.Brush, new RectangleD(0, 0, 0, 0)))
            {
                var selectedBrush = brush;
                if (StiBrush.ToColor(page.Brush) == Color.Transparent) selectedBrush = Brushes.White;
                g.FillRectangle(selectedBrush, 0, 0, (float)imgWidth, (float)imgHeight);

                PaintImage(page.Report, page.Watermark, g, new RectangleD(0, 0, imgWidth, imgHeight), zoom);
                PaintText(page.Watermark, g, new RectangleD(0, 0, imgWidth, imgHeight), zoom, false);
            }

            return image;
        }

        private void PaintWatermark(StiPage page, Graphics g, bool isBehind)
        {
            double scale = SystemWinApi.GetWindowsScale().Scale;
            double pgWidth = page.Unit.ConvertToHInches(page.DisplayRectangle.Width);
            double pgHeight = page.Unit.ConvertToHInches(page.DisplayRectangle.Height);

            var rect = new RectangleD(0, 0, pgWidth * page.Zoom * scale, pgHeight * page.Zoom * scale);

            if (page.Watermark.ShowImageBehind == isBehind) PaintImage(page.Report, page.Watermark, g, rect, page.Zoom * scale);
            if (page.Watermark.ShowBehind == isBehind) PaintText(page.Watermark, g, rect, page.Zoom * scale, page.IsPrinting);
        }
        #endregion

        #region Methods.Painter
        public override void Paint(StiComponent comp, StiPaintEventArgs e)
        {
            var isPaintWinFormsViewer = e.IsPaintWinFormsViewer;

            if (!(e.Context is Graphics))
                throw new Exception("StiGdiPainter can work only with System.Drawing.Graphics context!");

            var page = (StiPage)comp;
            if (page.Report == null) return;
            if (page.IsDesigning) page.SelectedComponents.Clear();

            var g = e.Graphics;
            page.InvokePainting(page, e);

            var unit = page.Unit;
            double scale = SystemWinApi.GetWindowsScale().Scale;

            double mgLeft = unit.ConvertToHInches(page.Margins.Left);
            double mgTop = unit.ConvertToHInches(page.Margins.Top);
            double mgRight = unit.ConvertToHInches(page.Margins.Right);
            double mgBottom = unit.ConvertToHInches(page.Margins.Bottom);

            double pgWidth = unit.ConvertToHInches(page.DisplayRectangle.Width);
            double pgHeight = unit.ConvertToHInches(page.DisplayRectangle.Height);

            double wd = unit.ConvertToHInches(page.Width);
            double ht = unit.ConvertToHInches(page.Height);

            var printerMargins = RectangleF.Empty;
            if (page.IsPrinting)
            {
                printerMargins = page.GetPrinterMargins(g);

                #region Page alignment
                double pgw = unit.ConvertToHInches(page.PageWidth);
                double pgh = unit.ConvertToHInches(page.PageHeight);
                if (printerMargins.Width > pgw * 1.005)
                {
                    if (StiOptions.Print.PageHorAlignment == StiHorAlignment.Center)
                    {
                        printerMargins.X -= (float)(printerMargins.Width - pgw) / 2f;
                    }
                    if (StiOptions.Print.PageHorAlignment == StiHorAlignment.Right)
                    {
                        printerMargins.X -= (float)(printerMargins.Width - pgw);
                    }
                }
                if (printerMargins.Height > pgh * 1.005)
                {
                    if (StiOptions.Print.PageVertAlignment == StiVertAlignment.Center)
                    {
                        printerMargins.Y -= (float)(printerMargins.Height - pgh) / 2f;
                    }
                    if (StiOptions.Print.PageVertAlignment == StiVertAlignment.Bottom)
                    {
                        printerMargins.Y -= (float)(printerMargins.Height - pgh);
                    }
                }
                #endregion
            }

            if (!e.Cancel)
            {
                if (!e.ClipRectangle.IsEmpty)
                {
                    g.ResetClip();
                    g.SetClip(e.ClipRectangle.ToRectangleF(), CombineMode.Replace);
                }

                #region !IsPrinting
                if (!page.IsPrinting)
                {
                    #region Paint page
                    if (page.Brush is StiSolidBrush && ((StiSolidBrush)page.Brush).Color == Color.Transparent)
                    {
                        if (page.IsDesigning) StiDrawing.FillRectangle(g, Brushes.White, e.ClipRectangle);
                        else StiDrawing.FillRectangle(g, Brushes.White, 0, 0, pgWidth * page.Zoom * scale, pgHeight * page.Zoom * scale);
                    }
                    else
                    {
                        if (page.IsDesigning) StiDrawing.FillRectangle(g, page.Brush, e.ClipRectangle);
                        else StiDrawing.FillRectangle(g, page.Brush, 0, 0, pgWidth * page.Zoom * scale, pgHeight * page.Zoom * scale);
                    }
                    #endregion

                    #region Watermark Behind
                    PaintWatermark(page, g, true);
                    #endregion

                    g.TranslateTransform((int)(mgLeft * page.Zoom * scale), (int)(mgTop * page.Zoom * scale));

                    #region Paint grid lines or dotes
                    if (page.IsDesigning && page.Report.Info.ShowGrid)
                    {
                        double gdSize = unit.ConvertToHInches(page.GridSize);

                        #region Paint lines
                        if (page.Report.Info.GridMode == StiGridMode.Lines)
                        {
                            using (var pen = new Pen(StiColorUtils.Dark(Color.Gainsboro, 30)))
                            {
                                double posx = 0;
                                double wdMax = wd * page.Zoom * scale;
                                double st = gdSize * page.Zoom * scale;
                                int a = 0;
                                for (posx = 0; posx < wdMax; posx += st)
                                {
                                    if ((a & 1) == 0)
                                        StiDrawing.DrawLine(g, pen, posx, 0, posx, ht * page.Zoom * scale);
                                    else
                                        StiDrawing.DrawLine(g, Pens.Gainsboro, posx, 0, posx, ht * page.Zoom * scale);

                                    a++;
                                }

                                double posy = 0;
                                double htMax = ht * page.Zoom * scale;
                                st = gdSize * page.Zoom * scale;
                                a = 0;
                                for (posy = 0; posy < htMax; posy += st)
                                {
                                    if ((a & 1) == 0)
                                        StiDrawing.DrawLine(g, pen, 0, posy, wd * page.Zoom * scale, posy);
                                    else
                                        StiDrawing.DrawLine(g, Pens.Gainsboro, 0, posy, wd * page.Zoom * scale, posy);

                                    a++;
                                }
                            }
                        }
                        #endregion

                        #region Paint dotes
                        if (page.Report.Info.GridMode == StiGridMode.Dots)
                        {
                            using (var bmp = new Bitmap((int)(wd * page.Zoom * scale) + 1, 1))
                            {
                                int sizeBmp = bmp.Width;
                                double wdMax = wd * page.Zoom * scale;
                                double st = gdSize * page.Zoom * scale;

                                for (double posx = 0; posx < wdMax; posx += st)
                                {
                                    int pixelPos = (int)Math.Round(posx);
                                    if (pixelPos < sizeBmp)
                                    {
                                        bmp.SetPixel(pixelPos, 0, StiColorUtils.Dark(Color.Gainsboro, 30));
                                    }
                                }

                                double htMax = ht * page.Zoom * scale;

                                for (double posy = 0; posy < htMax; posy += st)
                                {
                                    g.DrawImage(bmp, 0, (int)posy);
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion

                    //#region Draw LargeHeightFactor linee
                    ////if (page.LargeHeight && page.IsDesigning)
                    //if ((page.LargeHeightAutoFactor > 1) && page.IsDesigning)
                    //{
                    //    float largeHeightValue = (float)(ht / page.LargeHeightAutoFactor * page.Zoom * scale);
                    //    using (var pen = new Pen(Color.Blue, (float)(2.5 * page.Zoom * scale)))
                    //    {
                    //        g.DrawLine(pen, 0, largeHeightValue, (float)(wd * page.Zoom * scale), largeHeightValue);
                    //    }
                    //}
                    //#endregion

                    #region IsDesigning, Border of content, Columns
                    if (page.IsDesigning)
                    {
                        #region Paint the border of contents of the page
                        using (var pen = new Pen(StiColorUtils.Dark(Color.Gainsboro, 100)))
                        {
                            StiDrawing.DrawRectangle(g, pen, 0, 0, wd * page.Zoom * scale, ht * page.Zoom * scale);
                        }
                        #endregion

                        #region Paint columns
                        if (page.Columns > 1)
                        {
                            #region Paint columns
                            double columnWidth = page.Unit.ConvertToHInches(page.GetColumnWidth()) * page.Zoom * scale;
                            double columnGaps = page.Unit.ConvertToHInches(page.ColumnGaps) * page.Zoom * scale;

                            double pos = columnWidth;
                            using (var pen = new Pen(Color.Red))
                            {
                                pen.DashStyle = DashStyle.Dash;

                                for (int index = 1; index < page.Columns; index++)
                                {
                                    g.DrawLine(pen,
                                        (float)pos, 0,
                                        (float)pos,
                                        (float)(ht * page.Zoom * scale));

                                    g.DrawLine(pen,
                                        (float)(pos + columnGaps), 0,
                                        (float)(pos + columnGaps),
                                        (float)(ht * page.Zoom * scale));

                                    pos += columnWidth + columnGaps;
                                }

                                g.DrawLine(pen,
                                    (float)pos, 0,
                                    (float)pos,
                                    (float)(ht * page.Zoom * scale));
                            }
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion

                #region IsPrinting
                else
                {
                    g.ResetTransform();
                    g.TranslateTransform(-printerMargins.Left, -printerMargins.Top);
                    PaintWatermark(page, g, true);

                    g.PageUnit = GraphicsUnit.Inch;
                    g.ResetTransform();

                    #region Paint segment pages
                    if (page.SegmentPerHeight > 1 || page.SegmentPerWidth > 1)
                    {
                        g.PageScale = .01f;

                        g.TranslateTransform(
                            -printerMargins.Left + (float)mgLeft,
                            -printerMargins.Top + (float)mgTop);

                        PaintWatermark(page, g, true);

                        double pgWd = page.PageWidth - page.Margins.Left - page.Margins.Right;
                        double pgHt = page.PageHeight - page.Margins.Top - page.Margins.Bottom;

                        float scLeft = (float)unit.ConvertToHInches(page.CurrentWidthSegment * pgWd);
                        float scTop = (float)unit.ConvertToHInches(page.CurrentHeightSegment * pgHt);

                        g.SetClip(new RectangleF(0, 0,
                            (float)unit.ConvertToHInches(pgWd),
                            (float)unit.ConvertToHInches(pgHt)), CombineMode.Replace);

                        g.TranslateTransform(-scLeft, -scTop);

                    }
                    #endregion

                    #region Paint simple pages
                    else
                    {
                        if (page.StretchToPrintArea)
                        {
                            g.PageScale = .01f * (float)(printerMargins.Width / (pgWidth - mgLeft - mgRight));
                        }
                        else
                        {
                            g.PageScale = .01f;
                            g.TranslateTransform(
                                -printerMargins.Left + (float)mgLeft,
                                -printerMargins.Top + (float)mgTop);
                        }
                    }
                    #endregion
                }
                #endregion
            }

            #region Paint copyright before
            var ppe = new StiPagePaintEventArgs(
                e.Graphics,
                e.ClipRectangle.ToRectangle(),
                new Rectangle((int)(mgLeft * page.Zoom * scale), (int)(mgTop * page.Zoom * scale), (int)(wd * page.Zoom * scale), (int)(ht * page.Zoom * scale)),
                new Rectangle(0, 0, (int)(pgWidth * page.Zoom * scale), (int)(pgHeight * page.Zoom * scale)),
                page.Report.IsPrinting, page.Report.IsDesigning);

            g.TranslateTransform(-(int)(mgLeft * page.Zoom * scale), -(int)(mgTop * page.Zoom * scale));
            StiPage.InvokePagePainting(page, ppe);
            g.TranslateTransform((int)(mgLeft * page.Zoom * scale), (int)(mgTop * page.Zoom * scale));
            #endregion

            e.Cancel = false;
            page.InvokePainted(page, e);

            #region Set ClipRectangle comparatively begin pages
            if (!e.ClipRectangle.IsEmpty)
            {
                e = new StiPaintEventArgs(g,
                    new RectangleD(e.ClipRectangle.X - mgLeft * page.Zoom * scale, e.ClipRectangle.Y - mgTop * page.Zoom * scale,
                    e.ClipRectangle.Width, e.ClipRectangle.Height));
            }
            #endregion

            var state = g.Save();

            var compClipRect = new RectangleF(
                (float)(-mgLeft * page.Zoom * scale), (float)(-mgTop * page.Zoom * scale),
                (float)(pgWidth * page.Zoom * scale), (float)(pgHeight * page.Zoom * scale));

            g.SetClip(compClipRect, CombineMode.Intersect);

            PaintTableLines(page, g);

            base.PaintComponents(page, e);

            #region Draw Selected Components
            if (page.IsDesigning)
            {
                int index = 0;
                while (index < page.SelectedComponents.Count)
                {
                    var component = page.SelectedComponents[index] as StiComponent;
                    if (component is StiBand) component.Paint(e);
                    index++;
                }

                index = 0;
                while (index < page.SelectedComponents.Count)
                {
                    var component = page.SelectedComponents[index] as StiComponent;
                    if (!(component is StiBand)) component.Paint(e);
                    index++;
                }

                var comps = page.GetSelectedComponents();
                if (comps.Count > 1)
                {
                    index = 0;
                    while (index < comps.Count)
                    {
                        var component = comps[index];
                        if (component.IsSelected)
                        {
                            var rect = component.GetPaintRectangle();

                            double centerX = rect.X + rect.Width / 2;
                            double centerY = rect.Y + rect.Height / 2;

                            // LeftTop
                            StiDrawing.FillRectangle(g, Brushes.Gray, rect.X - 1, rect.Y - 1, 3, 3);
                            // LeftCenter
                            StiDrawing.FillRectangle(g, Brushes.Gray, rect.X - 1, centerY - 1, 3, 3);
                            // TopCenter
                            StiDrawing.FillRectangle(g, Brushes.Gray, centerX - 1, rect.Y - 1, 3, 3);
                            // TopRight
                            StiDrawing.FillRectangle(g, Brushes.Gray, rect.Right - 1, rect.Y - 1, 3, 3);
                            // RightCenter
                            StiDrawing.FillRectangle(g, Brushes.Gray, rect.Right - 1, centerY - 1, 3, 3);
                            // BottomRight
                            StiDrawing.FillRectangle(g, Brushes.Gray, rect.Right - 1, rect.Bottom - 1, 3, 3);
                            // BottomCenter
                            StiDrawing.FillRectangle(g, Brushes.Gray, centerX - 1, rect.Bottom - 1, 3, 3);
                            // BottomLeft
                            StiDrawing.FillRectangle(g, Brushes.Gray, rect.X - 1, rect.Bottom - 1, 3, 3);
                        }
                        index++;
                    }
                }
            }
            #endregion

            g.Restore(state);

            #region Paint Border
            PaintBorder(page, g, new RectangleD(0, 0, wd * page.Zoom * scale, ht * page.Zoom * scale), page.Zoom * scale, true, true);
            #endregion

            #region Paint Watermark at top
            if (page.IsPrinting)
            {
                if (page.StretchToPrintArea)
                    g.PageScale = .01f;

                g.ResetTransform();
                g.TranslateTransform(-printerMargins.Left, -printerMargins.Top);
                PaintWatermark(page, g, false);//At top
                g.ResetTransform();

                if (page.StretchToPrintArea)
                    g.PageScale = .01f * (float)(printerMargins.Width / (pgWidth - mgLeft - mgRight));
            }
            else
            {
                g.TranslateTransform(-(int)(mgLeft * page.Zoom * scale), -(int)(mgTop * page.Zoom * scale));
                PaintWatermark(page, g, false);
                g.TranslateTransform((int)(mgLeft * page.Zoom * scale), (int)(mgTop * page.Zoom * scale));
            }

            #endregion

            #region Paint page breakes
            if (!page.DenyDrawSegmentMode && StiOptions.Viewer.AllowDrawSegmentMode)
            {
                if ((!page.IsPrinting) && (page.SegmentPerWidth > 1 || page.SegmentPerHeight > 1)
                    || (page.Report.Info.ViewMode == StiViewMode.PageBreakPreview && page.IsDesigning))
                {
                    #region Segment page
                    if (page.SegmentPerWidth > 1 || page.SegmentPerHeight > 1)
                    {
                        double widthStep = page.Unit.ConvertToHInches(page.Width / page.SegmentPerWidth);
                        //double heightStep = page.Unit.ConvertToHInches(page.PageHeight / page.SegmentPerHeight);
                        double heightStep = page.Unit.ConvertToHInches(page.PageHeight - page.Margins.Top - page.Margins.Bottom);

                        using (var pen = new Pen(Color.Blue))
                        {
                            pen.Width = 2;
                            pen.DashStyle = DashStyle.Dash;

                            double py = 0;
                            int startPageNumber = 1;

                            if (!page.IsDesigning && page.Report.RenderedPages.Count > 0)
                            {
                                startPageNumber = page.Report.GetPageNumber(page.Report.RenderedPages.IndexOf(page));
                            }

                            for (int dy = 0; dy < page.SegmentPerHeight; dy++)
                            {
                                double px = 0;
                                py += heightStep;

                                for (int dx = 0; dx < page.SegmentPerWidth; dx++)
                                {
                                    px += widthStep;
                                    if (dx < page.SegmentPerWidth - 1)
                                    {
                                        g.DrawLine(pen, (float)(px * page.Zoom * scale), 0,
                                            (float)(px * page.Zoom * scale), (float)(page.Unit.ConvertToHInches(page.Height) * page.Zoom * scale));
                                    }

                                    var pageRect = new RectangleD(
                                        (px - widthStep) * page.Zoom * scale, (py - heightStep) * page.Zoom * scale,
                                        widthStep * page.Zoom * scale, heightStep * page.Zoom * scale);

                                    if (StiOptions.Viewer.IsRightToLeft)
                                    {
                                        pageRect.X = (page.SegmentPerWidth * widthStep - px) * page.Zoom * scale;
                                    }

                                    DrawPageNumber(page, g, startPageNumber, pageRect);

                                    startPageNumber++;

                                }

                                if (dy < page.SegmentPerHeight - 1)
                                {
                                    g.DrawLine(pen, 0, (float)(py * page.Zoom * scale),
                                        (float)(page.Unit.ConvertToHInches(page.Width) * page.Zoom * scale), (float)(py * page.Zoom * scale));
                                }
                            }
                        }
                    }
                    #endregion

                    else
                    {
                        DrawPageNumber(page, g, 1, new RectangleD(0, 0, wd * page.Zoom * scale, ht * page.Zoom * scale));
                    }

                    using (var pen = new Pen(Color.Blue))
                    {
                        pen.Width = 2;
                        g.DrawRectangle(pen, 0, 0, (float)(wd * page.Zoom * scale), (float)(ht * page.Zoom * scale));
                    }
                }
            }
            #endregion

            if (!page.IsPrinting)
            {
                #region Draw LargeHeightFactor line
                if ((page.LargeHeightAutoFactor > 1) && page.IsDesigning)
                {
                    float largeHeightValue = (float)(unit.ConvertToHInches(page.PageHeight - page.Margins.Top - page.Margins.Bottom) * page.SegmentPerHeight * page.Zoom * scale);
                    using (var pen = new Pen(Color.Red, (float)(2 * page.Zoom * scale)))
                    {
                        pen.DashStyle = DashStyle.Dash;
                        g.DrawLine(pen, 0, largeHeightValue, (float)(wd * page.Zoom * scale), largeHeightValue);
                        g.DrawLine(pen, -(float)(mgLeft * page.Zoom * scale), (float)(largeHeightValue + mgBottom * page.Zoom * scale), (float)(pgWidth * page.Zoom * scale), (float)(largeHeightValue + mgBottom * page.Zoom * scale));
                    }
                }
                #endregion
            }

            #region Paint Highlight
            if ((!page.LockHighlight) && (!page.IsPrinting))
            {
                PaintHighlight(page, e);
            }
            #endregion

            #region Paint Order & QuickInfo
            if ((page.Report.Info.ShowOrder ||
                (page.Report.Info.QuickInfoOverlay && page.Report.Info.QuickInfoType != StiQuickInfoType.None)) &&
                page.IsDesigning)
            {
                PaintOrderAndQuickInfo(page, g, string.Empty);
            }
            #endregion

            #region Paint Inherited Image
            if (page.IsDesigning && page.Report != null)
            {
                PaintInheritedImage(page, g);
            }
            #endregion

            #region Trial
#if CLOUD
            var isTrial = false; //StiCloudPlan.IsTrialPlan(page.Report != null ? page.Report.ReportGuid : null); //- server used only, for small thumbnails 
#elif SERVER
            var isTrial = StiVersionX.IsSvr;
#else
            var key = StiLicenseKeyValidator.GetLicenseKey();
            var isTrial = !(isPaintWinFormsViewer 
                ? StiLicenseKeyValidator.IsValidInDesignerOrOnSpecifiedPlatform(StiProductIdent.Net, key)
                : StiLicenseKeyValidator.IsValidOnNetFramework(key));

            if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key))isTrial = true;

            #region IsValidLicenseKey
            if (!isTrial)
            {
                try
                {
                    using (var rsa = new RSACryptoServiceProvider(512))
                    using (var sha = new SHA1CryptoServiceProvider())
                    {
                        rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                        isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
                    }
                }
                catch (Exception)
                {
                    isTrial = true;
                }
            }
            #endregion

#endif
            if (isTrial)
            {
                using (var sf = new StringFormat())
                {
                    sf.LineAlignment = StringAlignment.Center;
                    sf.Alignment = StringAlignment.Center;

                    Color color = Color.FromArgb(30, 100, 100, 100);
                    if (page.IsPrinting) color = Color.LightGray;
                    using (var brush = new SolidBrush(color))
                    {
                        var rect = new RectangleD(-mgLeft * page.Zoom * scale, -mgTop * page.Zoom * scale, pgWidth * page.Zoom * scale, pgHeight * page.Zoom * scale);
                        using (var font = new Font("Arial", (float)(150 * page.Zoom * scale), FontStyle.Bold))
                        {
                            StiTextDrawing.DrawString(g, "Trial", font, brush, rect, sf, 45);
                        }
                    }
                }
            }
            #endregion

            #region Paint Selected Rectangle
            if (!page.SelectedRectangle.IsEmpty)
            {
                using (var brush = new SolidBrush(Color.FromArgb(75, Color.Blue)))
                using (var pen = new Pen(Color.DimGray))
                {
                    pen.DashStyle = DashStyle.Dash;

                    var rect = page.SelectedRectangle;
                    rect.X *= page.Zoom * scale;
                    rect.Y *= page.Zoom * scale;
                    rect.Width *= page.Zoom * scale;
                    rect.Height *= page.Zoom * scale;
                    rect = page.Page.Unit.ConvertToHInches(rect);
                    var rectSelect = rect.ToRectangleF();
                    g.FillRectangle(brush, rectSelect);
                    g.DrawRectangle(pen, rectSelect.X, rectSelect.Y, rectSelect.Width, rectSelect.Height);
                }
            }
            #endregion

            #region Paint Dimension Lines
			#region DBS
            if (page.Report.Info.ShowDimensionLines && page.IsDesigning && page.Report.Info.CurrentAction != StiAction.None && (!page.IsDashboard))
            {
                StiDimensionLinesHelper.DrawDimensionLines(g, page);
            }
			#endregion
            #endregion

            #region Paint copyright after
            g.TranslateTransform(-(int)(mgLeft * page.Zoom * scale), -(int)(mgTop * page.Zoom * scale));
            StiPage.InvokePagePainted(page, ppe);
            g.TranslateTransform((int)(mgLeft * page.Zoom * scale), (int)(mgTop * page.Zoom * scale));
            #endregion
        }
        #endregion
    }
}
