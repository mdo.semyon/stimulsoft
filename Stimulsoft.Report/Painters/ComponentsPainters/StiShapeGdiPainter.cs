﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft


using System;
using System.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Events;

namespace Stimulsoft.Report.Painters
{
    public class StiShapeGdiPainter : StiComponentGdiPainter
    {
        #region Methods.Painter
        public override Image GetImage(StiComponent component, ref float zoom, StiExportFormat format)
        {
            var shape = (StiShape)component;

            double resZoom = shape.Report.Info.Zoom;
            shape.Report.Info.Zoom = zoom;
            int shift = (int)Math.Round(shape.Size * zoom / 2);
            var rect = shape.GetPaintRectangle();
            rect.X = 0;
            rect.Y = 0;
            //shape.Report.Info.Zoom = resZoom;

            int imageWidth = (int)(rect.Width + 0.5) + 1 + (int)(shape.Size * zoom);
            int imageHeight = (int)(rect.Height + 0.5) + 1 + (int)(shape.Size * zoom);

            var bmp = new Bitmap(imageWidth, imageHeight);
            using (var g = Graphics.FromImage(bmp))
            {
                g.PageUnit = GraphicsUnit.Pixel;
                if (format == StiExportFormat.ImagePng)
                {
                    g.Clear(Color.Transparent);
                }
                else
                {
                    g.Clear(Color.White);
                }
                g.TranslateTransform(shift, shift);
                shape.ShapeType.Paint(g, shape, rect.ToRectangleF(), (float)zoom);
            }

            shape.Report.Info.Zoom = resZoom;

            return bmp;
        }

        public override void Paint(StiComponent component, StiPaintEventArgs e)
        {
            if (!(e.Context is Graphics))
                throw new Exception("StiGdiPainter can work only with System.Drawing.Graphics context!");

            if ((!e.DrawBorderFormatting) && e.DrawTopmostBorderSides) return;

            var shape = (StiShape)component;

            shape.InvokePainting(shape, e);

            if (!e.Cancel && (!(shape.Enabled == false && shape.IsDesigning == false)))
            {
                var g = e.Graphics;

                var rect = shape.GetPaintRectangle();
                if (rect.Width > 0 && rect.Height > 0 && (e.ClipRectangle.IsEmpty || rect.IntersectsWith(e.ClipRectangle)))
                {
                    shape.ShapeType.Paint(g, shape, rect.ToRectangleF(), (float)shape.Page.Zoom);

                    #region Markers
                    PaintMarkers(shape, g, rect);
                    #endregion

                    PaintEvents(shape, e.Graphics, rect);
                    PaintConditions(shape, e.Graphics, rect);
                }
            }
            e.Cancel = false;
            shape.InvokePainted(shape, e);

        }
        #endregion
    }
}
