﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;

namespace Stimulsoft.Base.Context.Animation
{
    public class StiTranslationAnimation : StiAnimation
    {
        public StiTranslationAnimation(TimeSpan duration, TimeSpan? beginTime) : this(PointF.Empty, PointF.Empty, duration, beginTime)
        {
        }

        public StiTranslationAnimation(PointF startPoint, PointF endPoint, TimeSpan duration, TimeSpan? beginTime) : base(duration, beginTime)
        {
            this.StartPoint = startPoint;
            this.EndPoint = endPoint;
        }

        #region Fields
        public PointF StartPoint;
        public PointF EndPoint;
        #endregion

        public override StiAnimationType Type
        {
            get
            {
                return StiAnimationType.Translation;
            }
        }
    }
}
