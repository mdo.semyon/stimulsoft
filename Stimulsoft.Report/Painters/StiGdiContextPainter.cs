﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Context;

namespace Stimulsoft.Report.Painters
{
    public class StiGdiContextPainter : StiContextPainter
    {
        private bool isClone = false;

        public override StiContextPainter Clone()
        {
            isClone = true;

            var globalMeasureGraphics = Graphics.FromImage(new Bitmap(1, 1));
            globalMeasureGraphics.PageUnit = GraphicsUnit.Pixel;
            globalMeasureGraphics.PageScale = 1f;
            var painter = new StiGdiContextPainter(globalMeasureGraphics);

            return painter;
        }

        public override void Dispose()
        {
            if (isClone)
            {
                this.graphics.Dispose();
            }
        }

        #region Methods.String Format
        public override StiStringFormatGeom GetDefaultStringFormat()
        {
            using (StringFormat sf = new StringFormat())
            {
                return new StiStringFormatGeom(sf);
            }
        }

        public override StiStringFormatGeom GetGenericStringFormat()
        {
            using (StringFormat sf = StringFormat.GenericDefault.Clone() as StringFormat)
            {
                StiStringFormatGeom sfGeom = new StiStringFormatGeom(sf);
                sfGeom.IsGeneric = true;
                return sfGeom;
            }
        }
        #endregion

        #region Methods.Shadow
        public override StiContext CreateShadowGraphics(bool isPrinting, float zoom)
        {
            return new StiContext(new StiGdiContextPainter(this.graphics), true, false, isPrinting, zoom);
        }
        #endregion

        #region Methods.Path
        public override RectangleF GetPathBounds(List<StiSegmentGeom> geoms)
        {
            using (GraphicsPath path = GetPath(geoms))
            {
                return path.GetBounds();
            }
        }
        #endregion

        #region Methods.Measure String
        public override SizeF MeasureString(string text, StiFontGeom font)
        {
            using (Font fontGdi = GetFont(font))
            {
                if (NeedHtmlTags(text))
                {
                    return MeasureHtmlTags(graphics, text, new RectangleD(0, 0, int.MaxValue, int.MaxValue), fontGdi, null, 0);
                }
                else
                {
                    return graphics.MeasureString(text, fontGdi);
                }
            }
        }

        public override SizeF MeasureString(string text, StiFontGeom font, int width, StiStringFormatGeom sf)
        {
            using (Font fontGdi = GetFont(font))
            using (StringFormat sfGdi = GetStringFormat(sf))
            {
                if (NeedHtmlTags(text))
                {
                    return MeasureHtmlTags(graphics, text, new RectangleD(0, 0, width, int.MaxValue), fontGdi, sf, 0);
                }
                else
                {
                    return graphics.MeasureString(text, fontGdi, width, sfGdi);
                }
            }
        }

        public override RectangleF MeasureRotatedString(string text, StiFontGeom font, RectangleF rect, StiStringFormatGeom sf, float angle)
        {
            using (Font fontGdi = GetFont(font))
            using (StringFormat sfGdi = GetStringFormat(sf))
            {
                return StiRotatedTextDrawing.MeasureString(graphics, text, fontGdi, rect, sfGdi, StiRotationMode.CenterCenter, angle);
            }
        }

        public override RectangleF MeasureRotatedString(string text, StiFontGeom font, RectangleF rect, StiStringFormatGeom sf, StiRotationMode mode, float angle)
        {
            using (Font fontGdi = GetFont(font))
            using (StringFormat sfGdi = GetStringFormat(sf))
            {
                return StiRotatedTextDrawing.MeasureString(graphics, text, fontGdi, rect, sfGdi, mode, angle);
            }
        }

        public override RectangleF MeasureRotatedString(string text, StiFontGeom font, PointF point, StiStringFormatGeom sf, StiRotationMode mode, float angle, int maximalWidth)
        {
            using (Font fontGdi = GetFont(font))
            using (StringFormat sfGdi = GetStringFormat(sf))
            {
                return StiRotatedTextDrawing.MeasureString(graphics, text, fontGdi, point, sfGdi, mode, angle, maximalWidth);
            }
        }

        public override RectangleF MeasureRotatedString(string text, StiFontGeom font, PointF point, StiStringFormatGeom sf, StiRotationMode mode, float angle)
        {
            using (Font fontGdi = GetFont(font))
            using (StringFormat sfGdi = GetStringFormat(sf))
            {
                return StiRotatedTextDrawing.MeasureString(graphics, text, fontGdi, point, sfGdi, mode, angle);
            }
        }
        #endregion

        #region Methods.Render
        public override void Render(RectangleF rect, List<StiGeom> geoms)
        {
            foreach (StiGeom geom in geoms)
            {
                try
                {
                    switch (geom.Type)
                    {
                        #region Border
                        case StiGeomType.Border:
                            DrawBorderGeom(geom as StiBorderGeom);
                            break;
                        #endregion

                        #region Line
                        case StiGeomType.Line:
                            DrawLineGeom(geom as StiLineGeom);
                            break;
                        #endregion

                        #region Lines
                        case StiGeomType.Lines:
                            DrawLinesGeom(geom as StiLinesGeom);
                            break;
                        #endregion

                        #region Curve
                        case StiGeomType.Curve:
                            DrawCurveGeom(geom as StiCurveGeom);
                            break;
                        #endregion

                        #region Ellipse
                        case StiGeomType.Ellipse:
                            DrawEllipseGeom(geom as StiEllipseGeom);
                            break;
                        #endregion

                        #region CachedShadow
                        case StiGeomType.CachedShadow:
                            DrawCachedShadowGeom(geom as StiCachedShadowGeom);
                            break;
                        #endregion

                        #region Shadow
                        case StiGeomType.Shadow:
                            DrawShadowGeom(geom as StiShadowGeom);
                            break;
                        #endregion

                        #region Path
                        case StiGeomType.Path:
                            DrawPathGeom(geom as StiPathGeom);
                            break;
                        #endregion

                        #region Text
                        case StiGeomType.Text:
                            DrawTextGeom(geom as StiTextGeom);
                            break;
                        #endregion

                        #region PushClip
                        case StiGeomType.PushClip:
                            PushClipGeom(geom as StiPushClipGeom);
                            break;
                        #endregion

                        #region PushTranslateTransform
                        case StiGeomType.PushTranslateTransform:
                            PushTranslateTransformGeom(geom as StiPushTranslateTransformGeom);
                            break;
                        #endregion

                        #region PushRotateTransform
                        case StiGeomType.PushRotateTransform:
                            PushRotateTransformGeom(geom as StiPushRotateTransformGeom);
                            break;
                        #endregion

                        #region PushSmothingModeToAntiAlias
                        case StiGeomType.PushSmothingModeToAntiAlias:
                            PushSmothingModeToAntiAliasGeom(geom as StiPushSmothingModeToAntiAliasGeom);
                            break;
                        #endregion

                        #region PushTextRenderingHintToAntiAlias
                        case StiGeomType.PushTextRenderingHintToAntiAlias:
                            PushTextRenderingHintToAntiAliasGeom(geom as StiPushTextRenderingHintToAntiAliasGeom);
                            break;
                        #endregion

                        #region PopSmothingMode
                        case StiGeomType.PopSmothingMode:
                            PopSmothingModeGeom(geom as StiPopSmothingModeGeom);
                            break;
                        #endregion

                        #region PopTextRenderingHint
                        case StiGeomType.PopTextRenderingHint:
                            PopTextRenderingHintGeom(geom as StiPopTextRenderingHintGeom);
                            break;
                        #endregion

                        #region PopTransform
                        case StiGeomType.PopTransform:
                            PopTransformGeom(geom as StiPopTransformGeom);
                            break;
                        #endregion

                        #region PopClip
                        case StiGeomType.PopClip:
                            PopClipGeom(geom as StiPopClipGeom);
                            break;
                            #endregion
                    }
                }
                catch
                {
                }
            }
        }

        private void DrawBorderGeom(StiBorderGeom borderGeom)
        {
            object rect = GetRect(borderGeom.Rect);

            Brush backgroundBrush = GetBrush(borderGeom.Background, rect);
            Pen borderPen = GetPen(borderGeom.BorderPen);

            if (rect is Rectangle)
            {
                if (backgroundBrush != null)
                    graphics.FillRectangle(backgroundBrush, (Rectangle)rect);
                if (borderPen != null)
                    graphics.DrawRectangle(borderPen, (Rectangle)rect);
            }
            else if (rect is RectangleF)
            {
                RectangleF rectF = (RectangleF)rect;
                if (backgroundBrush != null)
                    graphics.FillRectangle(backgroundBrush, rectF);
                if (borderPen != null)
                    graphics.DrawRectangle(borderPen, rectF.X, rectF.Y, rectF.Width, rectF.Height);
            }
            if (backgroundBrush != null)
                backgroundBrush.Dispose();
            if (borderPen != null)
                borderPen.Dispose();

        }

        private void DrawLineGeom(StiLineGeom lineGeom)
        {
            Pen linePen = GetPen(lineGeom.Pen);
            if (linePen == null)
                return;

            graphics.DrawLine(linePen, lineGeom.X1, lineGeom.Y1, lineGeom.X2, lineGeom.Y2);

            if (linePen != null)
                linePen.Dispose();
        }

        private void DrawLinesGeom(StiLinesGeom linesGeom)
        {
            Pen linesPen = GetPen(linesGeom.Pen);
            if (linesPen == null)
                return;

            graphics.DrawLines(linesPen, linesGeom.Points);

            if (linesPen != null)
                linesPen.Dispose();
        }

        private void DrawCurveGeom(StiCurveGeom curveGeom)
        {
            Pen curvePen = GetPen(curveGeom.Pen);
            if (curvePen == null)
                return;

            graphics.DrawCurve(curvePen, curveGeom.Points, curveGeom.Tension);

            if (curvePen != null)
                curvePen.Dispose();
        }

        private void DrawEllipseGeom(StiEllipseGeom ellipseGeom)
        {
            object rect = GetRect(ellipseGeom.Rect);

            Brush backgroundBrush = GetBrush(ellipseGeom.Background, rect);
            Pen borderPen = GetPen(ellipseGeom.BorderPen);

            if (rect is Rectangle)
            {
                if (backgroundBrush != null)
                    graphics.FillEllipse(backgroundBrush, (Rectangle)rect);
                if (borderPen != null)
                    graphics.DrawEllipse(borderPen, (Rectangle)rect);
            }
            else if (rect is RectangleF)
            {
                RectangleF rectF = (RectangleF)rect;
                if (backgroundBrush != null)
                    graphics.FillEllipse(backgroundBrush, rectF);
                if (borderPen != null)
                    graphics.DrawEllipse(borderPen, rectF.X, rectF.Y, rectF.Width, rectF.Height);
            }
            if (backgroundBrush != null)
                backgroundBrush.Dispose();
            if (borderPen != null)
                borderPen.Dispose();
        }

        private void DrawCachedShadowGeom(StiCachedShadowGeom cachedShadowGeom)
        {
            object rectObj = GetRect(cachedShadowGeom.Rect);

            if (rectObj is Rectangle)
            {
                Rectangle rect = (Rectangle)rectObj;
                StiShadow.DrawCachedShadow(graphics, rect, cachedShadowGeom.Sides, cachedShadowGeom.IsPrinting);
            }
            else if (rectObj is RectangleF)
            {
                RectangleF rectF = (RectangleF)rectObj;
                StiShadow.DrawCachedShadow(graphics, rectF, cachedShadowGeom.Sides, cachedShadowGeom.IsPrinting);
            }
        }

        private void DrawShadowGeom(StiShadowGeom shadowGeom)
        {
            using (StiShadowGraphics shadowGraphics = new StiShadowGraphics(shadowGeom.Rect))
            {
                ((StiGdiContextPainter)shadowGeom.ShadowContext.ContextPainter).graphics = shadowGraphics.Graphics;
                shadowGeom.ShadowContext.Render(shadowGeom.Rect);
                shadowGraphics.DrawShadow(graphics, shadowGeom.Rect, shadowGeom.Radius);
            }
        }

        private void DrawPathGeom(StiPathGeom pathGeom)
        {
            using (GraphicsPath path = GetPath(pathGeom.Geoms))
            {
                object rect = (pathGeom.Rect == StiPathGeom.GetBoundsState) ? path.GetBounds() : GetRect(pathGeom.Rect);

                Brush backgroundBrush = GetBrush(pathGeom.Background, rect);
                Pen borderPen = GetPen(pathGeom.Pen);

                if (backgroundBrush != null)
                    graphics.FillPath(backgroundBrush, path);
                if (borderPen != null)
                    graphics.DrawPath(borderPen, path);

                if (backgroundBrush != null)
                    backgroundBrush.Dispose();
                if (borderPen != null)
                    borderPen.Dispose();
            }

        }

        private void DrawTextGeom(StiTextGeom textGeom)
        {
            TextRenderingHint textHint = graphics.TextRenderingHint;

            if (textGeom.Antialiasing) graphics.TextRenderingHint = TextRenderingHint.AntiAlias;

            if (!textGeom.IsRotatedText)
            {
                object rectObj = GetRect(textGeom.Location);
                if (rectObj is Rectangle)
                {
                    Rectangle rect = (Rectangle)rectObj;

                    using (Brush br = GetBrush(textGeom.Brush, rect))
                    using (Font fontGdi = GetFont(textGeom.Font))
                    using (StringFormat sfGdi = GetStringFormat(textGeom.StringFormat))
                    {
                        if (NeedHtmlTags(textGeom.Text))
                        {
                            DrawHtmlTags(graphics, textGeom, RectangleD.CreateFromRectangle(rect), fontGdi);
                        }
                        else
                        {
                            graphics.DrawString(textGeom.Text, fontGdi, br, rect, sfGdi);
                        }
                    }
                }
                else if (rectObj is RectangleF)
                {
                    RectangleF rectF = (RectangleF)rectObj;

                    using (Brush br = GetBrush(textGeom.Brush, rectF))
                    using (Font fontGdi = GetFont(textGeom.Font))
                    using (StringFormat sfGdi = GetStringFormat(textGeom.StringFormat))
                    {
                        if (NeedHtmlTags(textGeom.Text))
                        {
                            DrawHtmlTags(graphics, textGeom, RectangleD.CreateFromRectangle(rectF), fontGdi);
                        }
                        else
                        {
                            graphics.DrawString(textGeom.Text, fontGdi, br, rectF, sfGdi);
                        }
                    }
                }
            }
            else
            {
                object rectObj = GetRect(textGeom.Location);
                if ((rectObj is RectangleF || rectObj is Rectangle) && textGeom.IsRounded)
                {
                    Rectangle rect = Rectangle.Empty;

                    if (rectObj is Rectangle)
                        rect = (Rectangle)rectObj;
                    else
                        rect = Rectangle.Round(((RectangleF)rectObj));

                    using (Brush br = GetBrush(textGeom.Brush, rect))
                    using (Font fontGdi = GetFont(textGeom.Font))
                    using (StringFormat sfGdi = GetStringFormat(textGeom.StringFormat))
                    {
                        if (NeedHtmlTags(textGeom.Text))
                        {
                            DrawHtmlTags(graphics, textGeom, RectangleD.CreateFromRectangle(rect), fontGdi);
                        }
                        else
                        {
                            StiRotatedTextDrawing.DrawString(graphics, textGeom.Text, fontGdi, br, rect, sfGdi, textGeom.RotationMode.GetValueOrDefault(), textGeom.Angle, textGeom.Antialiasing);
                        }
                    }
                }
                else if (rectObj is RectangleF || rectObj is Rectangle)
                {
                    RectangleF rectF;

                    if (rectObj is RectangleF)
                        rectF = (RectangleF)rectObj;
                    else
                        rectF = (Rectangle)rectObj;

                    using (Brush br = GetBrush(textGeom.Brush, rectF))
                    using (Font fontGdi = GetFont(textGeom.Font))
                    using (StringFormat sfGdi = GetStringFormat(textGeom.StringFormat))
                    {
                        if (NeedHtmlTags(textGeom.Text))
                        {
                            DrawHtmlTags(graphics, textGeom, RectangleD.CreateFromRectangle(rectF), fontGdi);
                        }
                        else
                        {
                            if (textGeom.MaximalWidth != null)
                                StiRotatedTextDrawing.DrawString(graphics, textGeom.Text, fontGdi, br, rectF, sfGdi, textGeom.RotationMode.GetValueOrDefault(), textGeom.Angle, textGeom.Antialiasing, textGeom.MaximalWidth.GetValueOrDefault());
                            else
                                StiRotatedTextDrawing.DrawString(graphics, textGeom.Text, fontGdi, br, rectF, sfGdi, textGeom.Angle, textGeom.Antialiasing);
                        }
                    }
                }
                else if (textGeom.Location is PointF)
                {
                    PointF pointF = (PointF)textGeom.Location;

                    using (Brush br = GetBrush(textGeom.Brush, RectangleF.Empty))
                    using (Font fontGdi = GetFont(textGeom.Font))
                    using (StringFormat sfGdi = GetStringFormat(textGeom.StringFormat))
                    {
                        if (textGeom.MaximalWidth != null)
                            StiRotatedTextDrawing.DrawString(graphics, textGeom.Text, fontGdi, br, pointF, sfGdi, textGeom.RotationMode.GetValueOrDefault(), textGeom.Angle, textGeom.Antialiasing, textGeom.MaximalWidth.GetValueOrDefault());
                        else
                            StiRotatedTextDrawing.DrawString(graphics, textGeom.Text, fontGdi, br, pointF, sfGdi, textGeom.RotationMode.GetValueOrDefault(), textGeom.Angle, textGeom.Antialiasing);
                    }
                }
            }
            if (textGeom.Antialiasing) graphics.TextRenderingHint = textHint;
        }

        private void PushTranslateTransformGeom(StiPushTranslateTransformGeom translate)
        {
            PushState();
            graphics.TranslateTransform(translate.X, translate.Y);
        }

        private void PushClipGeom(StiPushClipGeom clip)
        {
            PushState();
            graphics.SetClip(clip.ClipRectangle, CombineMode.Intersect);
        }

        private void PushRotateTransformGeom(StiPushRotateTransformGeom rotate)
        {
            PushState();
            graphics.RotateTransform(rotate.Angle);
        }

        private void PopClipGeom(StiPopClipGeom clip)
        {
            PopState();
        }

        private void PopTransformGeom(StiPopTransformGeom translate)
        {
            PopState();
        }

        private void PushSmothingModeToAntiAliasGeom(StiPushSmothingModeToAntiAliasGeom smothing)
        {
            PushState();
            smothingModes.Add(graphics.SmoothingMode);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
        }

        private void PopSmothingModeGeom(StiPopSmothingModeGeom smothing)
        {
            graphics.SmoothingMode = smothingModes[smothingModes.Count - 1];
            smothingModes.RemoveAt(smothingModes.Count - 1);
            PopState();
        }

        private void PushTextRenderingHintToAntiAliasGeom(StiPushTextRenderingHintToAntiAliasGeom text)
        {
            PushState();
            textRenderingHints.Add(graphics.TextRenderingHint);
            graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
        }

        private void PopTextRenderingHintGeom(StiPopTextRenderingHintGeom text)
        {
            graphics.TextRenderingHint = textRenderingHints[textRenderingHints.Count - 1];
            textRenderingHints.RemoveAt(textRenderingHints.Count - 1);
            PopState();
        }

        private void PushState()
        {
            states.Add(graphics.Save());
        }

        private void PopState()
        {
            graphics.Restore(states[states.Count - 1]);
            states.RemoveAt(states.Count - 1);
        }
        #endregion

        #region Methods.Helper
        private PenAlignment GetPenAlignment(StiPenAlignment alignment)
        {
            switch (alignment)
            {
                case StiPenAlignment.Inset:
                    return PenAlignment.Inset;

                case StiPenAlignment.Left:
                    return PenAlignment.Left;

                case StiPenAlignment.Outset:
                    return PenAlignment.Outset;

                case StiPenAlignment.Right:
                    return PenAlignment.Right;

                case StiPenAlignment.Center:
                    return PenAlignment.Center;

                default:
                    return PenAlignment.Center;
            }
        }

        private object GetRect(object rect)
        {
            if (rect is Rectangle)
                return (Rectangle)rect;
            if (rect is RectangleF)
                return (RectangleF)rect;
            if (rect is RectangleD)
                return ((RectangleD)rect).ToRectangleF();
            return null;
        }

        private GraphicsPath GetPath(List<StiSegmentGeom> geoms)
        {
            GraphicsPath path = new GraphicsPath();
            foreach (StiSegmentGeom geom in geoms)
            {
                if (geom is StiArcSegmentGeom)
                {
                    StiArcSegmentGeom arcSegment = geom as StiArcSegmentGeom;
                    path.AddArc(arcSegment.Rect, arcSegment.StartAngle, arcSegment.SweepAngle);
                }
                else if (geom is StiCloseFigureSegmentGeom)
                {
                    path.CloseAllFigures();
                }
                else if (geom is StiCurveSegmentGeom)
                {
                    StiCurveSegmentGeom curveSegment = geom as StiCurveSegmentGeom;
                    path.AddCurve(curveSegment.Points, curveSegment.Tension);
                }
                else if (geom is StiLineSegmentGeom)
                {
                    StiLineSegmentGeom lineSegment = geom as StiLineSegmentGeom;
                    path.AddLine(lineSegment.X1, lineSegment.Y1, lineSegment.X2, lineSegment.Y2);
                }
                else if (geom is StiLinesSegmentGeom)
                {
                    StiLinesSegmentGeom linesSegment = geom as StiLinesSegmentGeom;
                    path.AddLines(linesSegment.Points);
                }
                else if (geom is StiPieSegmentGeom)
                {
                    StiPieSegmentGeom pieSegment = geom as StiPieSegmentGeom;
                    path.AddPie(pieSegment.Rect.X, pieSegment.Rect.Y, pieSegment.Rect.Width, pieSegment.Rect.Height, pieSegment.StartAngle, pieSegment.SweepAngle);
                }
            }
            return path;
        }

        private Brush GetBrush(object brush, object rect)
        {
            if (brush == null)
                return null;
            if (brush is Color)
                return new SolidBrush((Color)brush);

            if (brush is StiBrush)
            {
                if (rect is Rectangle)
                    return StiBrush.GetBrush((StiBrush)brush, (Rectangle)rect);
                if (rect is RectangleF)
                    return StiBrush.GetBrush((StiBrush)brush, (RectangleF)rect);
            }

            return brush as Brush;
        }

        private Pen GetPen(StiPenGeom penGeom)
        {
            if (penGeom == null)
                return null;

            if (penGeom.Brush == null)
                return null;

            if (penGeom.PenStyle == StiPenStyle.None)
                return null;

            if (penGeom.Brush is Color)
            {
                Pen pen = new Pen((Color)penGeom.Brush, penGeom.Thickness);
                pen.Alignment = GetPenAlignment(penGeom.Alignment);
                pen.DashStyle = StiPenUtils.GetPenStyle(penGeom.PenStyle);
                pen.StartCap = StiPenUtils.GetLineCap(penGeom.StartCap);
                pen.EndCap = StiPenUtils.GetLineCap(penGeom.EndCap);
                return pen;
            }

            return null;
        }

        private Font GetFont(StiFontGeom fontGeom)
        {
            if (fontGeom.FontFamily != null)
                return new Font(fontGeom.FontFamily, (float)(fontGeom.FontSize * StiDpiHelper.DeviceCapsScale));

            return new Font(StiFontCollection.GetFontFamily(fontGeom.FontName), (float)(fontGeom.FontSize * StiDpiHelper.DeviceCapsScale), fontGeom.FontStyle, fontGeom.Unit, fontGeom.GdiCharSet, fontGeom.GdiVerticalFont);
        }

        private StringFormat GetStringFormat(StiStringFormatGeom sfGeom)
        {
            StringFormat sf = sfGeom.IsGeneric ? StringFormat.GenericDefault.Clone() as StringFormat : new StringFormat();
            sf.Alignment = sfGeom.Alignment;
            sf.FormatFlags = sfGeom.FormatFlags;
            sf.HotkeyPrefix = sfGeom.HotkeyPrefix;
            sf.LineAlignment = sfGeom.LineAlignment;
            sf.Trimming = sfGeom.Trimming;

            return sf;
        }

        private bool NeedHtmlTags(string text)
        {
            return StiOptions.Engine.AllowHtmlTagsInChart && StiTextRenderer.CheckTextForHtmlTags(text);
        }

        private void DrawHtmlTags(Graphics graphics, StiTextGeom textGeom, RectangleD rect, Font fontGdi)
        {
            StiTextRenderer.DrawText(
                graphics,
                textGeom.Text,
                fontGdi,
                rect,
                StiBrush.ToColor(textGeom.Brush as StiBrush),
                Color.Transparent,
                1f,
                (StiTextHorAlignment)textGeom.StringFormat.Alignment,
                (StiVertAlignment)textGeom.StringFormat.LineAlignment,
                !((textGeom.StringFormat.FormatFlags & StringFormatFlags.NoWrap) > 0),
                (textGeom.StringFormat.FormatFlags & StringFormatFlags.DirectionRightToLeft) > 0,
                1f,
                textGeom.Angle,
                textGeom.StringFormat.Trimming,
                false,
                true);
        }

        private SizeF MeasureHtmlTags(Graphics graphics, string text, RectangleD rect, Font fontGdi, StiStringFormatGeom sf, float angle)
        {
            return StiTextRenderer.MeasureText(
                graphics,
                text,
                fontGdi,
                rect,
                1f,
                sf != null ? !((sf.FormatFlags & StringFormatFlags.NoWrap) > 0) : false,
                sf != null ? (sf.FormatFlags & StringFormatFlags.DirectionRightToLeft) > 0 : false,
                1f,
                angle,
                sf != null ? sf.Trimming : StringTrimming.None,
                false,
                true, new StiTextOptions()).ToSizeF();
        }
        #endregion

        #region Fields
        private Graphics graphics;
        private List<GraphicsState> states = new List<GraphicsState>();
        private List<SmoothingMode> smothingModes = new List<SmoothingMode>();
        private List<TextRenderingHint> textRenderingHints = new List<TextRenderingHint>();
        #endregion
        
        public StiGdiContextPainter(Graphics graphics)
        {
            this.graphics = graphics;
        }
    }
}
