﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Base.Json;
using Stimulsoft.Report.Maps;

namespace Stimulsoft.Report.Painters
{
    public abstract class StiMapContextPainter
    {
        #region Properties

        public readonly float Zoom;
        public readonly RectangleF Rect;
        public readonly StiMap Map;

        #endregion

        #region Methods
        public static Font ChangeFontSize(Font font, float zoom)
        {
            float newFontSize = font.Size * zoom;
            if (newFontSize < 1) newFontSize = 1;
            return new Font(
                font.FontFamily.Name,
                newFontSize,
                font.Style,
                font.Unit,
                font.GdiCharSet,
                font.GdiVerticalFont);
        }
        #endregion

        #region Methods abstract
        public abstract SizeF MeasureString(string text, Font font);
        #endregion

        #region Methods.Render

        public abstract void Render();

        #endregion

        public StiMapContextPainter(StiMap map, RectangleF rect, float zoom)
        {
            this.Map = map;
            this.Rect = rect;
            this.Zoom = zoom;
        }
    }
}