#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report
{
	
	/// <summary>
	/// Class store information about form parameters.
	/// </summary>
	public class StiFormSettings
	{
		public static void Save(Form form)
		{
			Save(form.Name, form);
		}

		public static void Save(string name, Form form)
		{
		    if (StiOptions.Designer.DontSaveFormsSettings) return;

		    StiSettings.Set(name, "left", form.Left);
		    StiSettings.Set(name, "top", form.Top);
		    StiSettings.Set(name, "width", form.Width);
		    StiSettings.Set(name, "height", form.Height);
		}

		public static void Load(Form form, bool skipLocation = false)
		{
			Load(form.Name, form, skipLocation);
		}

		public static void Load(string name, Form form, bool skipLocation = false)
		{
		    if (StiOptions.Designer.DontSaveFormsSettings) return;

		    var left = StiSettings.GetInt(name, "left");
		    var top = StiSettings.GetInt(name, "top");
		    var width = StiSettings.GetInt(name, "width");
		    var height = StiSettings.GetInt(name, "height");

		    if (left == -1 || top == -1 || width == -1 || height == -1)return;

		    width = Math.Min(width, Screen.PrimaryScreen.Bounds.Width);
		    height = Math.Min(height, Screen.PrimaryScreen.Bounds.Height);

		    form.AutoScaleMode = AutoScaleMode.None;
		    form.StartPosition = FormStartPosition.Manual;

		    if (skipLocation)
		        form.SetBounds(left, top, width, height, BoundsSpecified.Width | BoundsSpecified.Height);
		    else 
		        form.SetBounds(left, top, width, height);
		}

	    public static Size LoadSize(Form form)
	    {
	        if (StiOptions.Designer.DontSaveFormsSettings) return form.Size;

            var width = StiSettings.GetInt(form.Name, "width");
	        var height = StiSettings.GetInt(form.Name, "height");

	        if (width == -1 || height == -1) return form.Size;

	        width = Math.Min(width, Screen.PrimaryScreen.Bounds.Width);
	        height = Math.Min(height, Screen.PrimaryScreen.Bounds.Height);

	        return new Size(width, height);
	    }
	}
}
