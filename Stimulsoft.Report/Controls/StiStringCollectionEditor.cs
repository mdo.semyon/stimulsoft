#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
using Stimulsoft.System.Windows.Forms;
using Stimulsoft.System.Windows.Forms.Design;
#else
using System.Windows.Forms;
using System.Windows.Forms.Design;
#endif

namespace Stimulsoft.Report.Controls
{
	public class StiStringCollectionEditor : System.Drawing.Design.UITypeEditor
	{
        #region Fields
		private IWindowsFormsEditorService edSvc = null;
		//private bool cancel = false;
		#endregion

		#region Methods
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			if (context != null && context.Instance != null)
			{
				return UITypeEditorEditStyle.Modal;
			}
			return base.GetEditStyle(context);
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
            edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

            var strs = value as StiStringCollection;

            var list = value as Stimulsoft.Report.Dialogs.StiArrayList;
            if (list != null)
            {
                strs = new StiStringCollection();
                foreach (object item in list)
                {
                    strs.Add(item.ToString());
                }
            }

            using (var form = new StringCollectionForm(strs))
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    if (list != null)
                    {
                        list.Clear();
                        foreach (string str in form.items)
                        {
                            list.Add(str);                            
                        }
                        return list;
                    }
                    return form.items;
                }
            }
            return value;
		}
			
		#endregion
		
		#region Constructors
		private class StringCollectionForm : Form
		{
            public StringCollectionForm(StiStringCollection items)
			{
				this.instruction = new Label();
				this.textEntry = new TextBox();
				this.btOk = new Button();
				this.btCancel = new Button();
				this.InitializeComponent();
				this.textEntry.Focus();

                var strs = items;
                string text = string.Empty;

                foreach (string str in strs)
                {
                    text += str + (Char)13 + (Char)10;
                }

                this.textEntry.Text = text;
			}


			private void InitializeComponent()
			{
				this.instruction.Location = new Point(4, 7);
				this.instruction.Size = new Size(422, 14);
				this.instruction.TabIndex = 0;
				this.instruction.TabStop = false;
				this.instruction.Text = "Enter the strings in the collection (one per line):";

				this.textEntry.Location = new Point(4, 22);
				this.textEntry.Size = new Size(422, 244);
				this.textEntry.TabIndex = 0;
				this.textEntry.Text = "";
				this.textEntry.AcceptsTab = true;
				this.textEntry.AcceptsReturn = true;
				this.textEntry.AutoSize = false;
				this.textEntry.Multiline = true;
				this.textEntry.WordWrap = true;

				this.btOk.Location = new Point(260, 274);
				this.btOk.Size = new Size(75, 23);
				this.btOk.TabIndex = 1;
				this.btOk.Text = "Ok";
				this.btOk.DialogResult = DialogResult.OK;
				this.btOk.Click += this.btOk_click;
				
				this.btCancel.Location = new Point(339, 274);
				this.btCancel.Size = new Size(75, 23);
				this.btCancel.TabIndex = 2;
				this.btCancel.Text = "Cancel";
				this.btCancel.DialogResult = DialogResult.Cancel;

				base.Location = new Point(7, 7);
				base.AcceptButton = this.btOk;
				//base.AutoScaleBaseSize = new Size(5, 13);
				base.CancelButton = this.btCancel;
				base.ClientSize = new Size(429, 307);
				base.MaximizeBox = false;
				base.MinimizeBox = false;
				base.ShowInTaskbar = false;
				base.FormBorderStyle = FormBorderStyle.FixedDialog;
				base.StartPosition = FormStartPosition.CenterScreen;
				base.MinimumSize = new Size(300, 200);
				base.Text = "String Collection Editor";
				base.Controls.Clear();
				
				base.Controls.Add(textEntry);
				base.Controls.Add(btOk);
				base.Controls.Add(btCancel);
				base.Controls.Add(instruction);
 			}


			private void btOk_click(object sender, EventArgs e)
			{
                string[] strs = textEntry.Text.Split(new char[] { (Char)13 });

                var items2 = new List<string>();
                items = new StiStringCollection();
                foreach (string str in strs)
                {
                    string value = str.Trim(new char[] { (Char)10 });
                    if (value.Trim().Length == 0) continue;
                    items.Add(value);
                }
                
			}

		
			//private StiStringCollectionEditor editor;
			private Label instruction;
			private Button btOk;
			private Button btCancel;
			private TextBox textEntry;
            public StiStringCollection items = null;
		}
		#endregion 
	}
}
