﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Report.Viewer;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report
{
    public partial class StiReport
    {
        #region Properties
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [StiBrowsable(false)]
        public IStiViewerControl ViewerControl { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [StiBrowsable(false)]
        public Type ViewerForm { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [StiBrowsable(false)]
        [Obsolete("Please use property 'ViewerControl' instead 'PreviewControl' property.")]
        public IStiViewerControl PreviewControl
        {
            get
            {
                return this.ViewerControl;
            }
            set
            {
                this.ViewerControl = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [StiBrowsable(false)]
        [Obsolete("Please use property 'ViewerForm' instead 'PreviewForm' property.")]
        public Type PreviewForm
        {
            get
            {
                return ViewerForm;
            }
            set
            {
                ViewerForm = value;
            }
        }
        #endregion

        #region Mehtods.WinForms
        /// <summary>
        /// Shows a rendered report. If the report is not rendered then its rendering starts.
        /// </summary>
        public void Show()
        {
            Show(null, false);
        }

        /// <summary>
        /// Shows the rendered report as a dialog form or not. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="dialogForm">If this parameter is true then the report will be rendered as a dialog form.</param>
        public void Show(bool dialogForm)
        {
            Show(null, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report as MDI child window. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        public void Show(Form parentForm)
        {
            Show(parentForm, false);
        }

        /// <summary>
        /// Shows the rendered report as MDI child window. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="owner">Provides an interface to expose parent Win32 HWND handle.</param>
        public void Show(IWin32Window owner)
        {
            Show(null, owner, true);
        }

        /// <summary>
        /// Shows the rendered report as MDI child window. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="owner">Provides an interface to expose parent Win32 HWND handle.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        public void Show(IWin32Window owner, bool dialogForm)
        {
            Show(null, owner, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report as MDI child window as a dialog form or not.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        private void Show(Form parentForm, bool dialogForm)
        {
            Show(parentForm, null, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report as MDI child window as a dialog form or not.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        /// <param name="win32Window">Provides an interface to expose parent Win32 HWND handle.</param>
        private void Show(Form parentForm, IWin32Window win32Window, bool dialogForm)
        {
            if (!StiGuiOptions.AllowWinViewer)
            {
                if (StiGuiOptions.AllowWpfViewer)
                {
                    ShowWithWpf(parentForm, dialogForm);
                    return;
                }
            }

            if (this.PreviewMode == StiPreviewMode.DotMatrix)
                this.ShowDotMatrix(parentForm, win32Window, dialogForm);

            else
            {
                RegReportDataSources();
                if (!IsRendered) this.Render(true);

                StiLogService.Write(this.GetType(), "Showing report");

                if (this.NeedsCompiling && !this.IsCompiled)
                    throw new Exception("Can't showing report because report require compilation.");

                try
                {
                    if (!this.IsStopped)
                    {
                        var report = NeedsCompiling ? this.CompiledReport : this;

                        if (ViewerControl != null)
                            ViewerControl.Report = report;

                        else
                        {
                            IStiViewerForm viewerForm;
                            if (ViewerForm == null)
                            {
                                viewerForm = StiGuiOptions.GetViewerForm(report, StiGuiMode.Gdi);
                                viewerForm.Report = report;
                            }
                            else
                            {
                                viewerForm = StiActivator.CreateObject(ViewerForm, new object[] { null }) as IStiViewerForm;
                                viewerForm.Report = report;
                            }

                            if (parentForm != null && (parentForm.IsMdiContainer || parentForm.MdiParent != null))
                            {
                                viewerForm.TopLevel = false;
                                viewerForm.ShowInTaskbar = false;
                                viewerForm.ViewerOwner = viewerForm.GetOwner(parentForm) ?? parentForm;

                                viewerForm.ShowViewer();
                            }
                            else
                            {
                                if (dialogForm)
                                {
                                    viewerForm.ShowInTaskbar = StiOptions.Viewer.Windows.ShowInTaskbar;
                                    if (win32Window == null) viewerForm.ShowDialogViewer();
                                    else viewerForm.ShowDialogViewer(win32Window);

                                    if (viewerForm is IDisposable)
                                        ((IDisposable) viewerForm).Dispose();
                                }
                                else
                                {
                                    if (win32Window == null)
                                        viewerForm.ShowViewer(parentForm);
                                    else
                                        viewerForm.ShowViewer(win32Window);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    StiLogService.Write(this.GetType(), "Showing report...ERROR");
                    StiLogService.Write(this.GetType(), e);

                    if (!StiOptions.Engine.HideExceptions) throw;
                }
            }
        }
        #endregion

        #region Methods.WinForms.RibbonGUI
        /// <summary>
        /// Shows a rendered report in viewer with Ribbon GUI. If the report is not rendered then its rendering starts.
        /// </summary>
        public void ShowWithRibbonGUI()
        {
            ShowWithRibbonGUI(null, false);
        }

        /// <summary>
        /// Shows the rendered report as a dialog form or not with Ribbon GUI. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="dialogForm">If this parameter is true then the report will be rendered as a dialog form.</param>
        public void ShowWithRibbonGUI(bool dialogForm)
        {
            ShowWithRibbonGUI(null, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report as MDI child window with Ribbon GUI. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        public void ShowWithRibbonGUI(Form parentForm)
        {
            ShowWithRibbonGUI(parentForm, false);
        }

        /// <summary>
        /// Shows the rendered report as MDI child window with Ribbon GUI. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="owner">Provides an interface to expose parent Win32 HWND handle.</param>
        public void ShowWithRibbonGUI(IWin32Window owner)
        {
            ShowWithRibbonGUI(null, owner, true);
        }

        /// <summary>
        /// Shows the rendered report as MDI child window with Ribbon GUI. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="owner">Provides an interface to expose parent Win32 HWND handle.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        public void ShowWithRibbonGUI(IWin32Window owner, bool dialogForm)
        {
            ShowWithRibbonGUI(null, owner, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report as MDI child window as a dialog form or not with Ribbon GUI.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        private void ShowWithRibbonGUI(Form parentForm, bool dialogForm)
        {
            ShowWithRibbonGUI(parentForm, null, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report as MDI child window as a dialog form or not with Ribbon GUI.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        /// <param name="win32Window">Provides an interface to expose parent Win32 HWND handle.</param>
        private void ShowWithRibbonGUI(Form parentForm, IWin32Window win32Window, bool dialogForm)
        {
            if (!StiGuiOptions.AllowWinViewer && StiGuiOptions.AllowWpfViewer)
            {
                ShowWithWpfRibbonGUI(parentForm, dialogForm);
                return;
            }

            if (this.PreviewMode == StiPreviewMode.DotMatrix)
                this.ShowDotMatrix(parentForm, win32Window, dialogForm);

            else
            {
                RegReportDataSources();
                if (!IsRendered) this.Render(true);

                StiLogService.Write(this.GetType(), "Showing report");

                if (NeedsCompiling && !IsCompiled)
                    throw new Exception("Can't showing report because report require compilation.");

                try
                {
                    if (!this.IsStopped)
                    {
                        var report = NeedsCompiling ? this.CompiledReport : this;

                        if (ViewerControl != null)
                            ViewerControl.Report = report;

                        else
                        {
                            IStiViewerForm viewerForm;
                            if (ViewerForm == null)
                            {
                                viewerForm = StiGuiOptions.GetViewerFormWithRibbonGUI(report, StiGuiMode.Gdi);
                                viewerForm.Report = report;
                            }
                            else
                            {
                                var typeRibbonViewerForm = Type.GetType("Stimulsoft.Report.Viewer.StiRibbonViewerForm, Stimulsoft.Report.Win, " + StiVersion.VersionInfo);
                                if (typeRibbonViewerForm == null)
                                    throw new Exception("Assembly 'Stimulsoft.Report.Win' is not found");

                                viewerForm = StiActivator.CreateObject(typeRibbonViewerForm, new object[] { null }) as IStiViewerForm;
                                viewerForm.Report = report;
                            }

                            if (parentForm != null && (parentForm.IsMdiContainer || parentForm.MdiParent != null))
                            {
                                viewerForm.TopLevel = false;
                                viewerForm.ShowInTaskbar = false;
                                viewerForm.ViewerOwner = viewerForm.GetOwner(parentForm) ?? parentForm;

                                viewerForm.ShowViewer();
                            }
                            else
                            {
                                if (dialogForm)
                                {
                                    viewerForm.ShowInTaskbar = StiOptions.Viewer.Windows.ShowInTaskbar;
                                    if (win32Window == null) viewerForm.ShowDialogViewer();
                                    else viewerForm.ShowDialogViewer(win32Window);

                                    if (viewerForm is IDisposable)
                                        ((IDisposable) viewerForm).Dispose();
                                }
                                else
                                {
                                    if (win32Window == null)
                                        viewerForm.ShowViewer(parentForm);
                                    else
                                        viewerForm.ShowViewer(win32Window);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    StiLogService.Write(this.GetType(), "Showing report...ERROR");
                    StiLogService.Write(this.GetType(), e);

                    if (!StiOptions.Engine.HideExceptions) throw;
                }
            }
        }
        #endregion

        #region Methods.WPF
        /// <summary>
        /// Shows the rendered report with using WPF technology. 
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        public void ShowWithWpf()
        {
            ShowWithWpf(false);
        }

        /// <summary>
        /// Shows the rendered report with using WPF technology.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="dialogWindow">If this parameter is true then the report will be shown as a dialog window.</param>
        public void ShowWithWpf(bool dialogWindow)
        {
            ShowWithWpf(null, dialogWindow);
        }

        /// <summary>
        /// Shows the rendered report with using WPF technology.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="ownerWindow">A parent form.</param>
        public void ShowWithWpf(object ownerWindow)
        {
            ShowWithWpf(ownerWindow, false);
        }

        /// <summary>
        /// Shows the rendered report with using WPF technology.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="ownerWindow">A parent form.</param>
        /// <param name="dialogWindow">If this parameter is true then the report will be shown as a dialog window.</param>
        public void ShowWithWpf(object ownerWindow, bool dialogWindow)
        {
            StiOptions.Configuration.IsWPF = true;

            if (this.PreviewMode == StiPreviewMode.DotMatrix)
                this.ShowDotMatrixWithWpf(dialogWindow);

            else
            {
                RegReportDataSources();
                if (!IsRendered) this.RenderWithWpf(true);

                StiLogService.Write(this.GetType(), "Showing report");

                if (NeedsCompiling && !IsCompiled)
                    throw new Exception("Can't showing report because report require compilation.");

                try
                {
                    if (!this.IsStopped)
                    {
                        var report = NeedsCompiling ? this.CompiledReport : this;

                        if (ViewerControl != null)
                            ViewerControl.Report = report;

                        else
                        {
                            IStiViewerForm viewerForm;
                            if (ViewerForm == null)
                            {
                                viewerForm = StiGuiOptions.GetViewerForm(null, StiGuiMode.Wpf);
                                viewerForm.ViewerOwner = ownerWindow;
                                viewerForm.Report = report;
                            }
                            else
                            {
                                viewerForm = StiActivator.CreateObject(ViewerForm, new object[] { null }) as IStiViewerForm;
                                viewerForm.ViewerOwner = ownerWindow;
                                viewerForm.Report = report;
                            }

                            if (dialogWindow)
                            {
                                viewerForm.ShowInTaskbar = StiOptions.Viewer.Windows.ShowInTaskbar;
                                viewerForm.ShowDialogViewer(ownerWindow);
                            }
                            else
                                viewerForm.ShowViewer(ownerWindow);
                        }
                    }
                }
                catch (Exception e)
                {
                    StiLogService.Write(this.GetType(), "Showing report...ERROR");
                    StiLogService.Write(this.GetType(), e);

                    if (!StiOptions.Engine.HideExceptions) throw;
                }
            }
        }
        #endregion

        #region Methods.WPF.RibbonGUI
        /// <summary>
        /// Shows the rendered report with using WPF Ribbon GUI technology. 
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        public void ShowWithWpfRibbonGUI()
        {
            ShowWithWpfRibbonGUI(false);
        }

        /// <summary>
        /// Shows the rendered report with using WPF Ribbon GUI technology.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="dialogWindow">If this parameter is true then the report will be shown as a dialog window.</param>
        public void ShowWithWpfRibbonGUI(bool dialogWindow)
        {
            ShowWithWpfRibbonGUI(null, dialogWindow);
        }

        /// <summary>
        /// Shows the rendered report with using WPF Ribbon GUI technology.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="ownerWindow">A parent form.</param>
        public void ShowWithWpfRibbonGUI(object ownerWindow)
        {
            ShowWithWpfRibbonGUI(ownerWindow, false);
        }

        /// <summary>
        /// Shows the rendered report with using WPF Ribbon GUI technology.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="ownerWindow">A parent form.</param>
        /// <param name="dialogWindow">If this parameter is true then the report will be shown as a dialog window.</param>
        public void ShowWithWpfRibbonGUI(object ownerWindow, bool dialogWindow)
        {
            if (this.PreviewMode == StiPreviewMode.DotMatrix) this.ShowDotMatrixWithWpf(dialogWindow);
            else
            {
                RegReportDataSources();
                if (!IsRendered) this.RenderWithWpf(true);

                StiLogService.Write(this.GetType(), "Showing report");

                if (NeedsCompiling && !IsCompiled)
                    throw new Exception("Can't showing report because report require compilation.");

                try
                {
                    if (!this.IsStopped)
                    {
                        var report = NeedsCompiling ? this.CompiledReport : this;

                        if (ViewerControl != null)
                            ViewerControl.Report = report;

                        else
                        {
                            IStiViewerForm viewerForm;
                            if (ViewerForm == null)
                            {
                                viewerForm = StiGuiOptions.GetViewerFormWithRibbonGUI(null, StiGuiMode.Wpf);
                                viewerForm.ViewerOwner = ownerWindow;
                                viewerForm.Report = report;
                            }
                            else
                            {
                                viewerForm = StiActivator.CreateObject(ViewerForm, new object[] { null }) as IStiViewerForm;
                                viewerForm.ViewerOwner = ownerWindow;
                                viewerForm.Report = report;
                            }

                            if (dialogWindow)
                            {
                                viewerForm.ShowInTaskbar = StiOptions.Viewer.Windows.ShowInTaskbar;
                                viewerForm.ShowDialogViewer(ownerWindow);
                            }
                            else
                            {
                                viewerForm.ShowViewer(ownerWindow);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    StiLogService.Write(this.GetType(), "Showing report...ERROR");
                    StiLogService.Write(this.GetType(), e);

                    if (!StiOptions.Engine.HideExceptions) throw;
                }
            }
        }
        #endregion

        #region Methods.DotMatrix
        /// <summary>
        /// Shows a rendered report in DotMatrix mode. If the report is not rendered then its rendering starts.
        /// </summary>
        public void ShowDotMatrix()
        {
            ShowDotMatrix(null, false);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as a dialog form or not. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="dialogForm">If this parameter is true then the report will be rendered as a dialog form.</param>
        public void ShowDotMatrix(bool dialogForm)
        {
            ShowDotMatrix(null, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as MDI child window. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        public void ShowDotMatrix(Form parentForm)
        {
            ShowDotMatrix(parentForm, false);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as MDI child window. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="owner">Provides an interface to expose parent Win32 HWND handle.</param>
        public void ShowDotMatrix(IWin32Window owner)
        {
            ShowDotMatrix(null, owner, true);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as MDI child window. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="owner">Provides an interface to expose parent Win32 HWND handle.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        public void ShowDotMatrix(IWin32Window owner, bool dialogForm)
        {
            ShowDotMatrix(null, owner, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as MDI child window as a dialog form or not.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        private void ShowDotMatrix(Form parentForm, bool dialogForm)
        {
            ShowDotMatrix(parentForm, null, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as MDI child window as a dialog form or not.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        /// <param name="win32Window">Provides an interface to expose parent Win32 HWND handle.</param>
        private void ShowDotMatrix(Form parentForm, IWin32Window win32Window, bool dialogForm)
        {
            if (!StiGuiOptions.AllowWinViewer && StiGuiOptions.AllowWpfViewer)
            {
                ShowDotMatrixWithWpf(dialogForm);
                return;
            }

            RegReportDataSources();
            if (!IsRendered) this.Render(true);

            StiLogService.Write(this.GetType(), "Showing report in DotMatrixMode");

            if (NeedsCompiling && !IsCompiled)
                throw new Exception("Can't showing report in DotMatrix mode because report require compilation.");

            try
            {
                if (!this.IsStopped)
                {
                    var report = NeedsCompiling ? this.CompiledReport : this;
                    if (ViewerControl != null)
                        ViewerControl.Report = report;

                    else
                    {
                        IStiViewerForm viewerForm;
                        if (ViewerForm == null)
                            viewerForm = StiGuiOptions.GetDotMatrixViewerForm(report, StiGuiMode.Gdi);
                        
                        else
                            viewerForm = StiActivator.CreateObject(ViewerForm, new object[] { report }) as IStiViewerForm;

                        if (parentForm != null && (parentForm.IsMdiContainer || parentForm.MdiParent != null))
                        {
                            viewerForm.TopLevel = false;
                            viewerForm.ShowInTaskbar = false;

                            viewerForm.ViewerOwner = viewerForm.GetOwner(parentForm) ?? parentForm;

                            viewerForm.ShowViewer();
                        }
                        else
                        {
                            if (dialogForm)
                            {
                                viewerForm.ShowInTaskbar = StiOptions.Viewer.Windows.ShowInTaskbar;
                                if (win32Window == null)
                                    viewerForm.ShowDialogViewer();
                                else
                                    viewerForm.ShowDialogViewer(win32Window);
                            }
                            else
                            {
                                if (win32Window == null)
                                    viewerForm.ShowViewer();
                                else
                                    viewerForm.ShowViewer(win32Window);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Showing report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
        }
        #endregion

        #region Methods.DotMatrix.RibbonGUI
        /// <summary>
        /// Shows a rendered report in DotMatrix mode with Ribbon GUI. If the report is not rendered then its rendering starts.
        /// </summary>
        public void ShowDotMatrixWithRibbonGUI()
        {
            ShowDotMatrixWithRibbonGUI(null, false);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as a dialog form or not with Ribbon GUI. If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="dialogForm">If this parameter is true then the report will be rendered as a dialog form.</param>
        public void ShowDotMatrixWithRibbonGUI(bool dialogForm)
        {
            ShowDotMatrixWithRibbonGUI(null, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as MDI child window with Ribbon GUI. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        public void ShowDotMatrixWithRibbonGUI(Form parentForm)
        {
            ShowDotMatrixWithRibbonGUI(parentForm, false);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as MDI child window with Ribbon GUI. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="owner">Provides an interface to expose parent Win32 HWND handle.</param>
        public void ShowDotMatrixWithRibbonGUI(IWin32Window owner)
        {
            ShowDotMatrixWithRibbonGUI(null, owner, true);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as MDI child window with Ribbon GUI. If the report is not rendered then its rendering starts. 
        /// </summary>
        /// <param name="owner">Provides an interface to expose parent Win32 HWND handle.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        public void ShowDotMatrixWithRibbonGUI(IWin32Window owner, bool dialogForm)
        {
            ShowDotMatrixWithRibbonGUI(null, owner, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as MDI child window as a dialog form or not with Ribbon GUI.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        private void ShowDotMatrixWithRibbonGUI(Form parentForm, bool dialogForm)
        {
            ShowDotMatrixWithRibbonGUI(parentForm, null, dialogForm);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as MDI child window as a dialog form or not with Ribbon GUI.
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="parentForm">A parent form in the MDI application.</param>
        /// <param name="dialogForm">If this parameter is true then the report will be shown as a dialog form.</param>
        /// <param name="win32Window">Provides an interface to expose parent Win32 HWND handle.</param>
        private void ShowDotMatrixWithRibbonGUI(Form parentForm, IWin32Window win32Window, bool dialogForm)
        {
            RegReportDataSources();
            if (!IsRendered) this.Render(true);

            StiLogService.Write(this.GetType(), "Showing report in DotMatrixMode");

            if (NeedsCompiling && !IsCompiled)
                throw new Exception("Can't showing report in DotMatrix mode because report require compilation.");

            try
            {
                if (!this.IsStopped)
                {
                    StiReport report = NeedsCompiling ? this.CompiledReport : this;

                    if (ViewerControl != null)
                        ViewerControl.Report = report;

                    else
                    {
                        IStiViewerForm viewerForm;
                        if (ViewerForm == null)
                        {
                            viewerForm = StiGuiOptions.GetDotMatrixViewerFormWithRibbonGUI(report, StiGuiMode.Gdi);
                        }
                        else
                        {
                            var typeRibbonDotMatrix = Type.GetType("Stimulsoft.Report.Viewer.StiRibbonDotMatrixViewerForm, Stimulsoft.Report.Win, " + StiVersion.VersionInfo);
                            if (typeRibbonDotMatrix == null)
                                throw new Exception("Assembly 'Stimulsoft.Report.Win' is not found");

                            viewerForm = StiActivator.CreateObject(typeRibbonDotMatrix, new object[] { report }) as IStiViewerForm;
                        }

                        if (parentForm != null && (parentForm.IsMdiContainer || parentForm.MdiParent != null))
                        {
                            viewerForm.TopLevel = false;
                            viewerForm.ShowInTaskbar = false;

                            viewerForm.ViewerOwner = viewerForm.GetOwner(parentForm) ?? parentForm;

                            viewerForm.ShowViewer();
                        }
                        else
                        {
                            if (dialogForm)
                            {
                                viewerForm.ShowInTaskbar = StiOptions.Viewer.Windows.ShowInTaskbar;
                                if (win32Window == null)
                                    viewerForm.ShowDialogViewer();
                                else
                                    viewerForm.ShowDialogViewer(win32Window);
                            }
                            else
                            {
                                if (win32Window == null)
                                    viewerForm.ShowViewer();
                                else
                                    viewerForm.ShowViewer(win32Window);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Showing report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
        }
        #endregion

        #region Methods.DotMatrix.WPF
        /// <summary>
        /// Shows a rendered report in DotMatrix mode with using WPF technology. If the report is not rendered then its rendering starts.
        /// </summary>
        public void ShowDotMatrixWithWpf()
        {
            ShowDotMatrixWithWpf(false);
        }

        /// <summary>
        /// Shows the rendered report in DotMatrix mode as a dialog form or not with using WPF technology. 
        /// If the report is not rendered then its rendering starts.
        /// </summary>
        /// <param name="dialogWindow">If this parameter is true then the report will be rendered as a dialog window.</param>
        public void ShowDotMatrixWithWpf(bool dialogWindow)
        {
            RegReportDataSources();
            if (!IsRendered) this.RenderWithWpf(true);

            StiLogService.Write(this.GetType(), "Showing report in DotMatrixMode");

            if (NeedsCompiling && !IsCompiled)
                throw new Exception("Can't showing report in DotMatrix mode because report require compilation.");

            try
            {
                if (!this.IsStopped)
                {
                    var report = NeedsCompiling ? this.CompiledReport : this;

                    if (ViewerControl != null)
                        ViewerControl.Report = report;

                    else
                    {
                        IStiViewerForm viewerForm;
                        if (ViewerForm == null)
                            viewerForm = StiGuiOptions.GetDotMatrixViewerForm(report, StiGuiMode.Wpf);

                        else
                            viewerForm = StiActivator.CreateObject(ViewerForm, new object[] { report }) as IStiViewerForm;

                        if (dialogWindow)
                        {
                            viewerForm.ShowInTaskbar = StiOptions.Viewer.Windows.ShowInTaskbar;
                            viewerForm.ShowDialogViewer();
                        }
                        else
                            viewerForm.ShowViewer();
                    }
                }
            }
            catch (Exception e)
            {
                StiLogService.Write(this.GetType(), "Showing report...ERROR");
                StiLogService.Write(this.GetType(), e);

                if (!StiOptions.Engine.HideExceptions) throw;
            }
        }
        #endregion
    }
}