#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft


using System;
using System.ComponentModel;
using System.Drawing.Printing;
using System.Drawing;
using System.Collections;
using System.Data;
using System.Reflection;
using Stimulsoft.Report;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using Stimulsoft.Base.Services;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.Controls;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Dialogs
{
	/// <summary>
	/// This class provide forms rendering.
	/// </summary>
	public class StiWinDialogsProvider : StiDialogsProvider
	{
		#region Fields
		private Hashtable ControlToReport;
		private Hashtable ReportToControl;
		private Hashtable ManagerToGridControl;
		
		private StiForm formControl = null;
		private ToolTip toolTip = null;
        private bool lockReportControlUpdate = false;
		#endregion

		#region Properties
        protected override StiGuiMode GuiMode
        {
            get
            {
                return StiGuiMode.Gdi;
            }
        }

		private Form form = null;
		public Form Form
		{
			get
			{
				return form;
			}
		}


		private StiReport report = null;
		public override StiReport Report
		{
			get
			{
				return report;
			}
			set
			{
				report = value;
			}
		}
		#endregion		

		#region Handlers
		private void OnPositionChanged(object sender, EventArgs e)
		{
			StiGridControl control = ManagerToGridControl[sender] as StiGridControl;
			if (control != null)
			{
				control.InvokePositionChanged(e);
			}
			InvokeEventFired(sender, e);
		}

		private void OnClick(object sender, EventArgs e)
		{
			if (ControlToReport != null)
			{
				StiReportControl control = ControlToReport[sender] as StiReportControl;
                StiForm formControl = ControlToReport[sender] as StiForm;
				if (control != null)
				{
					#region Set Focus to Button
					Control selectedControl = null;
					Control winControl = sender as Control;
					Form form = winControl.FindForm();
					if (form != null)
					{
						selectedControl = form.ActiveControl;
						form.ActiveControl = winControl;
					}
					#endregion

					control.InvokeClick(control, e);
					if (sender is Button)InvokeButtonClick(sender, e);

					#region Set Focus to Original Control
					if (form != null && form.Visible)
					{
						form.ActiveControl = selectedControl;
					}
					#endregion
				}

				if (formControl != null)
				{
					formControl.InvokeClick(control, e);
				}
			}
		}

		private void OnDoubleClick(object sender, EventArgs e)
		{
			if (ControlToReport != null)
			{
				StiReportControl control = ControlToReport[sender] as StiReportControl;
				if (control != null)
				{
					control.InvokeDoubleClick(control, e);
				}

				StiForm form = ControlToReport[sender] as StiForm;
				if (form != null)
				{
					form.InvokeDoubleClick(control, e);
				}
			}
		}

		private void OnEnter(object sender, EventArgs e)
		{
			if (ControlToReport != null)
			{
				StiReportControl control = ControlToReport[sender] as StiReportControl;
				if (control != null)
				{
					control.InvokeEnter(e);
				}
			}
		}

		private void OnLeave(object sender, EventArgs e)
		{
			lockReportControlUpdate = true;
			FormToReportControl(formControl);
			lockReportControlUpdate = false;			

			if (ControlToReport != null)
			{
				StiReportControl control = ControlToReport[sender] as StiReportControl;
				if (control != null)
				{
					control.InvokeLeave(e);
				}
			}
		}

		private void OnMouseDown(object sender, MouseEventArgs e)
		{
			if (ControlToReport != null)
			{
				StiReportControl control = ControlToReport[sender] as StiReportControl;
				if (control != null)
				{
					control.InvokeMouseDown(e);
				}
			}
		}

		private void OnMouseUp(object sender, MouseEventArgs e)
		{
			if (ControlToReport != null)
			{
				StiReportControl control = ControlToReport[sender] as StiReportControl;
				if (control != null)
				{
					control.InvokeMouseUp(e);
				}
			}
		}

		private void OnMouseMove(object sender, MouseEventArgs e)
		{
			if (ControlToReport != null)
			{
				StiReportControl control = ControlToReport[sender] as StiReportControl;
				if (control != null)
				{
					control.InvokeMouseMove(e);
				}
			}
		}

		private void OnClosedForm(object sender, EventArgs e)
		{
			if (ControlToReport != null)
			{
				StiForm formControl = ControlToReport[sender] as StiForm;
				if (formControl != null)
				{
					formControl.InvokeClosedForm(e);
				}
			}
		}

		private void OnClosingForm(object sender, CancelEventArgs e)
		{
			if (ControlToReport != null)
			{
				StiForm formControl = ControlToReport[sender] as StiForm;
				if (formControl != null)
				{
					formControl.InvokeClosingForm(e);
				}
			}
		}

		private void OnLoadForm(object sender, EventArgs e)
		{
			if (ControlToReport != null)
			{
				StiForm formControl = ControlToReport[sender] as StiForm;
				if (formControl != null)
				{
					formControl.InvokeLoadForm(e);
				}
			}
		}

		private void OnCheckedChanged(object sender, EventArgs e)
		{
			if (ControlToReport != null)
			{
				lockReportControlUpdate = true;
				FormToReportControl(formControl);
				lockReportControlUpdate = false;

				StiCheckBoxControl control = ControlToReport[sender] as StiCheckBoxControl;
				if (control != null)
				{
					control.InvokeCheckedChanged(e);				
				}

				StiRadioButtonControl radioButton = ControlToReport[sender] as StiRadioButtonControl;
				if (radioButton != null)
				{
					radioButton.InvokeCheckedChanged(e);				
				}
				InvokeEventFired(sender, e);
			}
		}

		private void OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (ControlToReport != null)
			{
				lockReportControlUpdate = true;
				FormToReportControl(formControl);
				lockReportControlUpdate = false;

				StiListBoxControl control = ControlToReport[sender] as StiListBoxControl;
				if (control != null)
				{
					control.InvokeSelectedIndexChanged(e);
				}

				StiCheckedListBoxControl checkedListBox = ControlToReport[sender] as StiCheckedListBoxControl;
				if (checkedListBox != null)
				{
					checkedListBox.InvokeSelectedIndexChanged(e);
				}

				StiListViewControl listViewControl = ControlToReport[sender] as StiListViewControl;
				if (listViewControl != null)
				{
					listViewControl.InvokeSelectedIndexChanged(e);
				}

				//Also for lookup
				StiComboBoxControl comboBox = ControlToReport[sender] as StiComboBoxControl;
				if (comboBox != null)
				{
					comboBox.InvokeSelectedIndexChanged(e);
				}
				InvokeEventFired(sender, e);
			}
		}

		private void OnAfterSelect(object sender, TreeViewEventArgs e)
		{
			if (ControlToReport != null)
			{
				lockReportControlUpdate = true;
				FormToReportControl(formControl);
				lockReportControlUpdate = false;

				StiTreeViewControl control = ControlToReport[sender] as StiTreeViewControl;
				if (control != null)
				{
					control.InvokeAfterSelect(e);
				}
				InvokeEventFired(sender, e);
			}
		}

		private void OnValueChanged(object sender, EventArgs e)
		{
			if (ControlToReport != null)
			{
				lockReportControlUpdate = true;
				FormToReportControl(formControl);
				lockReportControlUpdate = false;

				StiDateTimePickerControl control = ControlToReport[sender] as StiDateTimePickerControl;
				if (control != null)
				{
					control.InvokeValueChanged(e);
				}

				StiNumericUpDownControl numeric = ControlToReport[sender] as StiNumericUpDownControl;
				if (numeric != null)
				{
					numeric.InvokeValueChanged(e);				
				}
				InvokeEventFired(sender, e);
			}
		}

		#endregion

		#region Methods.ConvertReportControlToControl
		protected virtual void ConvertReportControlToControl(Control control, StiReportControl reportControl)
		{
			if (reportControl.TagValueBinding.Length > 0)
			{
				reportControl.TagValue = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, reportControl.TagValueBinding);
			}

			control.Dock =			StiComponent.ConvertDockStyle(reportControl.DockStyle);
			control.Location =		reportControl.Location;
			control.Size =			reportControl.Size;
            if (control.BackColor != reportControl.BackColor)
			    control.BackColor =		reportControl.BackColor;
			control.Font =			reportControl.Font;
			control.RightToLeft =	reportControl.RightToLeft;
			control.Tag =			reportControl.TagValue;
            if (control.ForeColor != reportControl.ForeColor)
			    control.ForeColor =		reportControl.ForeColor;
			control.Enabled =		reportControl.Enabled;
			control.Visible =		reportControl.Visible;

			reportControl.InvokeEvents();
			toolTip.SetToolTip(control, reportControl.ToolTipValue as string);
			
			control.Click +=		new EventHandler(OnClick);
			control.DoubleClick +=	new EventHandler(OnDoubleClick);
			control.Enter +=		new EventHandler(OnEnter);
			control.Leave +=		new EventHandler(OnLeave);
			control.MouseDown +=	new MouseEventHandler(OnMouseDown);
			control.MouseUp +=		new MouseEventHandler(OnMouseUp);
			control.MouseMove +=	new MouseEventHandler(OnMouseMove);
		}

		protected virtual Control ConvertReportControlToPanel(StiPanelControl panelControl)
		{
			Stimulsoft.Report.Controls.StiPanel panel = new Stimulsoft.Report.Controls.StiPanel();
			panelControl.Control = panel;
			ConvertReportControlToControl(panel, panelControl);
			panel.BorderStyle =	panelControl.BorderStyle;

			return panel;
		}

		protected virtual Control ConvertReportControlToTextBox(StiTextBoxControl textBoxControl)
		{
			TextBox textBox = new TextBox();
			textBoxControl.Control = textBox;
			ConvertReportControlToControl(textBox, textBoxControl);

			#region DataBinding
			if (textBoxControl.TextBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, textBoxControl.TextBinding);
				
				if (value != null)
				{
					textBoxControl.Text = value.ToString();
				}
			}
			#endregion

			textBox.MaxLength =		textBoxControl.MaxLength;
			textBox.PasswordChar =	textBoxControl.PasswordChar;
			textBox.Text =			textBoxControl.Text;
			textBox.Multiline =		textBoxControl.Multiline;
			textBox.WordWrap =		textBoxControl.WordWrap;
			textBox.AcceptsReturn =	textBoxControl.AcceptsReturn;
			textBox.AcceptsTab =	textBoxControl.AcceptsTab;

			return textBox;
		}

		protected virtual Control ConvertReportControlToRichTextBox(StiRichTextBoxControl textBoxControl)
		{
            StiRichTextBox textBox = new StiRichTextBox(textBoxControl.BackColor == Color.Transparent);
			textBoxControl.Control = textBox;
			ConvertReportControlToControl(textBox, textBoxControl);

			textBox.Rtf =			textBoxControl.RtfText;

			return textBox;
		}

		protected virtual Control ConvertReportControlToButton(StiButtonControl buttonControl)
		{
			Button button = new Button();
			button.TextAlign =		buttonControl.TextAlign;
			button.ImageAlign =		buttonControl.ImageAlign;
			button.Image =			buttonControl.Image;

			buttonControl.Control = button;
			ConvertReportControlToControl(button, buttonControl);
		    button.DialogResult =	buttonControl.DialogResult;
			button.Text =			buttonControl.Text;
			
			if (buttonControl.Default)form.AcceptButton = button;
            if (buttonControl.Cancel) form.CancelButton = button;

			return button;
		}

		protected virtual Control ConvertReportControlToCheckBox(StiCheckBoxControl checkBoxControl)
		{
			CheckBox checkBox = new CheckBox();
			checkBoxControl.Control = checkBox;
			ConvertReportControlToControl(checkBox, checkBoxControl);	
		
			#region DataBinding
			if (checkBoxControl.CheckedBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, checkBoxControl.CheckedBinding);
				if (value is bool)checkBoxControl.Checked = (bool)value;
			}

			if (checkBoxControl.TextBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, checkBoxControl.TextBinding);
				if (value != null)checkBoxControl.Text = value.ToString();
			}
			#endregion

			checkBox.Checked =		checkBoxControl.Checked;
			checkBox.Text =			checkBoxControl.Text;

			checkBox.CheckedChanged += new EventHandler(OnCheckedChanged);

			return checkBox;
		}

		protected virtual Control ConvertReportControlToRadioButton(StiRadioButtonControl radioButtonControl)
		{
			RadioButton radioButton = new RadioButton();
			radioButtonControl.Control = radioButton;
			ConvertReportControlToControl(radioButton, radioButtonControl);	
			
			#region DataBinding
			if (radioButtonControl.CheckedBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, radioButtonControl.CheckedBinding);
				if (value is bool)radioButtonControl.Checked = (bool)value;
			}

			if (radioButtonControl.TextBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, radioButtonControl.TextBinding);
				if (value != null)radioButtonControl.Text = value.ToString();
			}
			#endregion

			radioButton.Checked =		radioButtonControl.Checked;
			radioButton.Text =			radioButtonControl.Text;		
	
			radioButton.CheckedChanged += new EventHandler(OnCheckedChanged);

			return radioButton;
		}

		protected virtual Control ConvertReportControlToLabel(StiLabelControl labelControl)
		{
			Label label = new Label();
			labelControl.Control = label;
			ConvertReportControlToControl(label, labelControl);	
		
			#region DataBinding
			if (labelControl.TextBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, labelControl.TextBinding);
				if (value != null)labelControl.Text = value.ToString();
			}
			#endregion

			label.TextAlign =		labelControl.TextAlign;
			label.Text =			labelControl.Text;

			return label;
		}

		protected virtual Control ConvertReportControlToGrid(StiGridControl gridControl)
		{
			DataGrid grid = new DataGrid();
			gridControl.Control = grid;
			ConvertReportControlToControl(grid, gridControl);

			grid.ColumnHeadersVisible =	gridControl.ColumnHeadersVisible;
			grid.RowHeadersVisible =	gridControl.RowHeadersVisible;
			grid.GridLineStyle =		gridControl.GridLineStyle;
			grid.HeaderFont =			gridControl.HeaderFont;
			grid.PreferredColumnWidth =	gridControl.PreferredColumnWidth;
			grid.PreferredRowHeight =	gridControl.PreferredRowHeight;
			grid.RowHeaderWidth =		gridControl.RowHeaderWidth;

			if (gridControl.DataSource != null)
			{
				StiDataSource dataSource = gridControl.DataSource;

				#region Create Table
				DataGridTableStyle tableStyle = new DataGridTableStyle();
				if (gridControl.Columns.Count == 0)
				{
					grid.AlternatingBackColor = gridControl.AlternatingBackColor;
					grid.BackgroundColor =		gridControl.BackgroundColor;
					grid.GridLineColor =		gridControl.GridLineColor;
					grid.ForeColor =			gridControl.ForeColor;
					grid.BackColor =			gridControl.BackColor;
					grid.HeaderBackColor =		gridControl.HeaderBackColor;
					grid.HeaderForeColor =		gridControl.HeaderForeColor;
					grid.SelectionBackColor =	gridControl.SelectionBackColor;
					grid.SelectionForeColor =	gridControl.SelectionForeColor;
				}
				
				DataSet dataSet = new DataSet();
				DataTable table = new DataTable(dataSource.Name);
				dataSet.Tables.Add(table);
								
				#region Create columns from gridControl.Columns
				if (gridControl.Columns.Count > 0)
				{					
					foreach (StiGridColumn column in gridControl.Columns)
					{
						if (column.Visible && column.DataTextField.Length > 0)
						{
							StiDataColumn dtColumn = StiDataColumn.GetDataColumnFromColumnName(
								dataSource.Dictionary, column.DataTextField);
						
							if (dtColumn != null)
							{
								string columnInRow = StiDataColumn.GetColumnNameFromDataColumn(dataSource.Dictionary, 
									column.DataTextField);

								DataColumn dataColumn = new DataColumn(columnInRow, dtColumn.Type);
								dataColumn.Caption = dtColumn.Alias;
								dataColumn.AllowDBNull = true;
								table.Columns.Add(dataColumn);

								DataGridTextBoxColumn columnStyle = new DataGridTextBoxColumn();
								columnStyle.MappingName = columnInRow;
								if (column.Width != 0)columnStyle.Width = column.Width;
								columnStyle.Alignment = column.Alignment;
								columnStyle.NullText = column.NullText;
								if (column.HeaderText.Length == 0)columnStyle.HeaderText = dtColumn.Alias;
								else columnStyle.HeaderText = column.HeaderText;
								tableStyle.GridColumnStyles.Add(columnStyle);
							}
						}
					}
				}
					#endregion

				#region Auto Refresh Columns
				else
				{
					foreach (StiDataColumn column in dataSource.Columns)
					{
						DataColumn dataColumn = new DataColumn(column.Name, column.Type);
						dataColumn.Caption = column.Alias;
						dataColumn.AllowDBNull = true;
						table.Columns.Add(dataColumn);
					}
				}
				#endregion
				#endregion

				#region Fill data
				while (!dataSource.IsEof)
				{
					DataRow row = table.NewRow();

					if (gridControl.Columns.Count > 0)
					{
						foreach (StiGridColumn column in gridControl.Columns)
						{
							if (column.Visible && column.DataTextField.Length > 0)
							{
								string columnInRow = StiDataColumn.GetColumnNameFromDataColumn(dataSource.Dictionary, 
									column.DataTextField);

								row[columnInRow] = StiDataColumn.GetDataFromDataColumn(
									dataSource.Dictionary, column.DataTextField);
							}
						}
					}
					else
					{
						foreach (StiDataColumn column in dataSource.Columns)
						{
							row[column.Name] = dataSource[column.Name];
						}
					}

					table.Rows.Add(row);

					dataSource.Next();
				}
				dataSource.First();
				#endregion

				DataView view = new DataView(table);
				view.RowFilter = gridControl.Filter;

				grid.DataSource = view;				
				grid.ReadOnly = true;
				grid.AllowNavigation = false;
				grid.CaptionVisible = false;
				
				#region Set colors
				if (gridControl.Columns.Count > 0)
				{
					grid.BackgroundColor =				gridControl.BackgroundColor;

					tableStyle.AlternatingBackColor =	gridControl.AlternatingBackColor;					
					tableStyle.GridLineColor =			gridControl.GridLineColor;					
					tableStyle.ForeColor =				gridControl.ForeColor;
					tableStyle.BackColor =				gridControl.BackColor;
					tableStyle.HeaderBackColor =		gridControl.HeaderBackColor;
					tableStyle.HeaderForeColor =		gridControl.HeaderForeColor;
					tableStyle.SelectionBackColor =		gridControl.SelectionBackColor;
					tableStyle.SelectionForeColor =		gridControl.SelectionForeColor;

					tableStyle.MappingName = dataSource.Name;
					grid.TableStyles.Add(tableStyle);
				}
				#endregion
				
				BindingManagerBase manager = form.BindingContext[grid.DataSource, grid.DataMember];
				ManagerToGridControl[manager] = gridControl;
				manager.PositionChanged += new EventHandler(OnPositionChanged);				
				
			}

			return grid;
		}

		protected virtual Control ConvertReportControlToPictureBox(StiPictureBoxControl pictureBoxControl)
		{
			PictureBox pictureBox = new PictureBox();
			pictureBoxControl.Control = pictureBox;
			ConvertReportControlToControl(pictureBox, pictureBoxControl);
			pictureBox.SizeMode =			pictureBoxControl.SizeMode;
			pictureBox.BorderStyle =		pictureBoxControl.BorderStyle;
			pictureBox.Image =				pictureBoxControl.Image;

			return pictureBox;
		}

		protected virtual Control ConvertReportControlToGroupBox(StiGroupBoxControl groupBoxControl)
		{
			GroupBox groupBox = new GroupBox();
			groupBoxControl.Control = groupBox;
			ConvertReportControlToControl(groupBox, groupBoxControl);	
	
			#region DataBinding
			if (groupBoxControl.TextBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, groupBoxControl.TextBinding);
				if (value != null)groupBoxControl.Text = value.ToString();
			}
			#endregion

			groupBox.Text =			groupBoxControl.Text;

			return groupBox;
		}

		protected virtual Control ConvertReportControlToListBox(StiListBoxControl listBoxControl)
		{
			ListBox listBox = new ListBox();
			listBoxControl.Control = listBox;
			ConvertReportControlToControl(listBox, listBoxControl);	
		
			#region ItemsBinding
			if (listBoxControl.ItemsBinding.Length > 0)
			{		
				object [] values = 
					StiDataColumn.GetDatasFromDataColumn(report.Dictionary, listBoxControl.ItemsBinding);
				if (listBoxControl.Sorted)Array.Sort(values);

				listBoxControl.Items.ClearCore();
				listBoxControl.Items.AddRangeCore(values);
								
			}
			listBox.Items.AddRange(listBoxControl.Items.ToArray());

			if (listBox.Items.Count > 0)
			{
				listBox.SelectedItem = listBoxControl.SelectedItem = listBox.Items[0];
			}
			#endregion	

			#region SelectedItemBinding
			if (listBoxControl.SelectedItemBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, listBoxControl.SelectedItemBinding);

				listBox.SelectedItem = listBoxControl.SelectedItem = value;
			}
			#endregion

			#region SelectedValueBinding
			if (listBoxControl.SelectedValueBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, listBoxControl.SelectedValueBinding);
				listBox.SelectedValue = listBoxControl.SelectedValue = value;
			}
			#endregion

			listBox.SelectionMode =	listBoxControl.SelectionMode;
			listBox.Sorted =		listBoxControl.Sorted;
			listBox.ItemHeight =	listBoxControl.ItemHeight;
            listBox.HorizontalScrollbar = true;

			listBox.SelectedIndexChanged += new EventHandler(OnSelectedIndexChanged);
			
			return listBox;
		}

		protected virtual Control ConvertReportControlToTreeView(StiTreeViewControl treeViewControl)
		{
			TreeView treeView = new TreeView();
            treeView.Scrollable = true;
			treeViewControl.Control = treeView;
			ConvertReportControlToControl(treeView, treeViewControl);

			treeView.AfterSelect += new TreeViewEventHandler(OnAfterSelect);
			
			return treeView;
		}

		protected virtual Control ConvertReportControlToListView(StiListViewControl listViewControl)
		{
			ListView listView = new ListView();
            listView.Scrollable = true;
			listViewControl.Control = listView;
			ConvertReportControlToControl(listView, listViewControl);

			listView.SelectedIndexChanged += new EventHandler(OnSelectedIndexChanged);
			
			return listView;
		}

		protected virtual Control ConvertReportControlToCheckedListBox(StiCheckedListBoxControl checkedListBoxControl)
		{
			CheckedListBox checkedListBox = new CheckedListBox();
			checkedListBoxControl.Control = checkedListBox;
			ConvertReportControlToControl(checkedListBox, checkedListBoxControl);
			
			checkedListBox.SelectionMode =	checkedListBoxControl.SelectionMode;
			checkedListBox.Sorted =			checkedListBoxControl.Sorted;
			checkedListBox.ItemHeight =		checkedListBoxControl.ItemHeight;
			checkedListBox.CheckOnClick =	checkedListBoxControl.CheckOnClick;
            checkedListBox.HorizontalScrollbar = true;
		
			#region ItemsBinding
			if (checkedListBoxControl.ItemsBinding.Length > 0)
			{		
				object [] values = 
					StiDataColumn.GetDatasFromDataColumn(report.Dictionary, checkedListBoxControl.ItemsBinding);
				if (checkedListBoxControl.Sorted)Array.Sort(values);

				checkedListBoxControl.Items.ClearCore();
				checkedListBoxControl.Items.AddRangeCore(values);
								
			}
			checkedListBox.Items.AddRange(checkedListBoxControl.Items.ToArray());

			if (checkedListBox.Items.Count > 0)
			{
				checkedListBox.SelectedItem = checkedListBoxControl.SelectedItem = checkedListBox.Items[0];
			}
			#endregion	

			#region SelectedItemBinding
			if (checkedListBoxControl.SelectedItemBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, checkedListBoxControl.SelectedItemBinding);

				checkedListBox.SelectedItem = checkedListBoxControl.SelectedItem = value;
			}
			#endregion

			#region SelectedValueBinding
			if (checkedListBoxControl.SelectedValueBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, checkedListBoxControl.SelectedValueBinding);
				checkedListBox.SelectedValue = checkedListBoxControl.SelectedValue = value;
			}
			#endregion			
			
			checkedListBox.SelectedIndexChanged += new EventHandler(OnSelectedIndexChanged);			
	
			return checkedListBox;
		}

		protected virtual Control ConvertReportControlToComboBox(StiComboBoxControl comboBoxControl)
		{
			ComboBox comboBox = new ComboBox();
			comboBoxControl.Control = comboBox;
			ConvertReportControlToControl(comboBox, comboBoxControl);

			#region ItemsBinding
			if (comboBoxControl.ItemsBinding.Length > 0)
			{		
				object [] values = 
					StiDataColumn.GetDatasFromDataColumn(report.Dictionary, comboBoxControl.ItemsBinding);
				if (comboBoxControl.Sorted)Array.Sort(values);

				comboBoxControl.Items.ClearCore();
				comboBoxControl.Items.AddRangeCore(values);
								
			}
			comboBox.Items.AddRange(comboBoxControl.Items.ToArray());

			if (comboBox.Items.Count > 0)
			{
				comboBox.SelectedItem = comboBoxControl.SelectedItem = comboBox.Items[0];
			}			
			#endregion
		
			#region TextBinding
			if (comboBoxControl.TextBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, comboBoxControl.TextBinding);
				if (value != null)comboBoxControl.Text = value.ToString();
			}
			#endregion			

			#region SelectedItemBinding
			if (comboBoxControl.SelectedItemBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, comboBoxControl.SelectedItemBinding);

				comboBox.SelectedItem = comboBoxControl.SelectedItem = value;
			}
			#endregion

			#region SelectedValueBinding
			if (comboBoxControl.SelectedValueBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, comboBoxControl.SelectedValueBinding);
				comboBox.SelectedValue = comboBoxControl.SelectedValue = value;
			}
			#endregion

			comboBox.DropDownStyle =		comboBoxControl.DropDownStyle;
			comboBox.MaxLength =			comboBoxControl.MaxLength;
			comboBox.Sorted =				comboBoxControl.Sorted;
			comboBox.ItemHeight =			comboBoxControl.ItemHeight;
			comboBox.MaxDropDownItems =		comboBoxControl.MaxDropDownItems;
			comboBox.DropDownWidth =		comboBoxControl.DropDownWidth;

			comboBox.SelectedIndexChanged += new EventHandler(OnSelectedIndexChanged);

			return comboBox;
		}

		protected virtual Control ConvertReportControlToLookUpBox(StiLookUpBoxControl lookUpBoxControl)
		{
			StiLookUpBox lookUpBox = new StiLookUpBox();
			lookUpBoxControl.Control = lookUpBox;
			ConvertReportControlToControl(lookUpBox, lookUpBoxControl);
		
			#region DataBinding
			if (lookUpBoxControl.TextBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, lookUpBoxControl.TextBinding);
				if (value != null)lookUpBoxControl.Text = value.ToString();
			}
	
			lookUpBox.Items.Clear();
			if (lookUpBoxControl.ItemsBinding.Length > 0)
			{
				lookUpBox.Items.AddRange(StiDataColumn.GetDatasFromDataColumn(
					report.Dictionary, lookUpBoxControl.ItemsBinding));
				
				if (lookUpBox.Items.Count > 0)
				{
					lookUpBox.SelectedItem = lookUpBoxControl.SelectedItem = lookUpBox.Items[0];
				}
			}
			else lookUpBox.Items.AddRange(lookUpBoxControl.Items.ToArray());

			lookUpBox.Keys.Clear();
			if (lookUpBoxControl.KeysBinding.Length > 0)
			{
				lookUpBox.Keys.AddRange(StiDataColumn.GetDatasFromDataColumn(
					report.Dictionary, lookUpBoxControl.KeysBinding));
				
			}
			else lookUpBox.Keys.AddRange(lookUpBoxControl.Keys.ToArray());

			if (lookUpBoxControl.SelectedItemBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, lookUpBoxControl.SelectedItemBinding);

				lookUpBox.SelectedItem = lookUpBoxControl.SelectedItem = value;
			}

			if (lookUpBoxControl.SelectedKeyBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, lookUpBoxControl.SelectedKeyBinding);

				lookUpBox.SelectedKey = lookUpBoxControl.SelectedKey = value;
			}

			if (lookUpBoxControl.SelectedValueBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, lookUpBoxControl.SelectedValueBinding);
				lookUpBox.SelectedValue = lookUpBoxControl.SelectedValue = value;
			}
			#endregion

			lookUpBox.DropDownStyle =		lookUpBoxControl.DropDownStyle;
			lookUpBox.MaxLength =			lookUpBoxControl.MaxLength;
			lookUpBox.Sorted =				lookUpBoxControl.Sorted;
			lookUpBox.ItemHeight =			lookUpBoxControl.ItemHeight;
			lookUpBox.MaxDropDownItems =	lookUpBoxControl.MaxDropDownItems;
			lookUpBox.DropDownWidth =		lookUpBoxControl.DropDownWidth;
			lookUpBox.Flat = false;

			lookUpBox.SelectedIndexChanged += new EventHandler(OnSelectedIndexChanged);

			return lookUpBox;
		}

		protected virtual Control ConvertReportControlToNumericUpDown(StiNumericUpDownControl numericUpDownControl)
		{
			NumericUpDown numericUpDown = new NumericUpDown();
			numericUpDownControl.Control = numericUpDown;
			ConvertReportControlToControl(numericUpDown, numericUpDownControl);		
	
			#region DataBinding
			if (numericUpDownControl.ValueBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, numericUpDownControl.ValueBinding);
				if (value is int)numericUpDownControl.Value = (int)value;
			}

			if (numericUpDownControl.MaximumBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, numericUpDownControl.MaximumBinding);
				if (value is int)numericUpDownControl.Maximum = (int)value;
			}

			if (numericUpDownControl.MinimumBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, numericUpDownControl.MinimumBinding);
				if (value is int)numericUpDownControl.Minimum = (int)value;
			}
			#endregion

			numericUpDown.Minimum =		numericUpDownControl.Minimum;
			numericUpDown.Maximum =		numericUpDownControl.Maximum;
			numericUpDown.Increment =	numericUpDownControl.Increment;
			numericUpDown.Value =		numericUpDownControl.Value;	
		
			numericUpDown.ValueChanged += new EventHandler(OnValueChanged);

			return numericUpDown;
		}

		protected virtual Control ConvertReportControlToDateTimePicker(StiDateTimePickerControl dateTimePickerControl)
		{
			DateTimePicker dateTimePicker = new DateTimePicker();
			dateTimePickerControl.Control = dateTimePicker;
			ConvertReportControlToControl(dateTimePicker, dateTimePickerControl);

			#region DataBinding
			if (dateTimePickerControl.TagValueBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, dateTimePickerControl.TagValueBinding);
				if (value is DateTime)dateTimePickerControl.Value = (DateTime)value;
			}

			if (dateTimePickerControl.MaxDateBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, dateTimePickerControl.MaxDateBinding);
				if (value is DateTime)dateTimePickerControl.MaxDate = (DateTime)value;
			}

			if (dateTimePickerControl.MinDateBinding.Length > 0)
			{
				object value = StiDataColumn.GetDataFromDataColumn(
					report.Dictionary, dateTimePickerControl.MinDateBinding);
				if (value is DateTime)dateTimePickerControl.MinDate = (DateTime)value;
			}
			#endregion

			if (dateTimePickerControl.Today)dateTimePickerControl.Value = DateTime.Now;

			dateTimePicker.CalendarFont =		dateTimePickerControl.Font;
			dateTimePicker.CalendarForeColor =	dateTimePickerControl.ForeColor;
			dateTimePicker.CustomFormat =		dateTimePickerControl.CustomFormat;
			dateTimePicker.DropDownAlign =		dateTimePickerControl.DropDownAlign;
			dateTimePicker.ShowUpDown =			dateTimePickerControl.ShowUpDown;
			dateTimePicker.MaxDate =			dateTimePickerControl.MaxDate;
			dateTimePicker.MinDate =			dateTimePickerControl.MinDate;
			dateTimePicker.Value =				dateTimePickerControl.Value;
			dateTimePicker.Format =				dateTimePickerControl.Format;			

			dateTimePicker.ValueChanged += new EventHandler(OnValueChanged);

			return dateTimePicker;
		}

        protected virtual Control ConvertReportControlToCustomControl(StiCustomControl customControl)
        {
            Control control = customControl.Control as Control;
            control.Location = customControl.Location;
            control.Size = customControl.Size;

            control.Click += new EventHandler(OnClick);
            control.DoubleClick += new EventHandler(OnDoubleClick);
            control.Enter += new EventHandler(OnEnter);
            control.Leave += new EventHandler(OnLeave);
            control.MouseDown += new MouseEventHandler(OnMouseDown);
            control.MouseUp += new MouseEventHandler(OnMouseUp);
            control.MouseMove += new MouseEventHandler(OnMouseMove);

            return control;
        }

		protected virtual void ConvertDialogsToControls(Control parentControl, StiReportControl control, int tabIndex)
		{
			Control createdControl = null;
			
			if (control is StiPanelControl)
				createdControl = ConvertReportControlToPanel(control as StiPanelControl);
			
			if (control is StiTextBoxControl)
				createdControl = ConvertReportControlToTextBox(control as StiTextBoxControl);

			if (control is StiRichTextBoxControl)
				createdControl = ConvertReportControlToRichTextBox(control as StiRichTextBoxControl);
			
			if (control is StiButtonControl)
				createdControl = ConvertReportControlToButton(control as StiButtonControl);

			if (control is StiCheckBoxControl)
				createdControl = ConvertReportControlToCheckBox(control as StiCheckBoxControl);

			if (control is StiRadioButtonControl)
				createdControl = ConvertReportControlToRadioButton(control as StiRadioButtonControl);

			if (control is StiLabelControl)
				createdControl = ConvertReportControlToLabel(control as StiLabelControl);

			if (control is StiGridControl)
				createdControl = ConvertReportControlToGrid(control as StiGridControl);

			if (control is StiPictureBoxControl)
				createdControl = ConvertReportControlToPictureBox(control as StiPictureBoxControl);

			if (control is StiGroupBoxControl)
				createdControl = ConvertReportControlToGroupBox(control as StiGroupBoxControl);

			if (control is StiListBoxControl)
				createdControl = ConvertReportControlToListBox(control as StiListBoxControl);

			if (control is StiTreeViewControl)
				createdControl = ConvertReportControlToTreeView(control as StiTreeViewControl);

			if (control is StiListViewControl)
				createdControl = ConvertReportControlToListView(control as StiListViewControl);

			if (control is StiCheckedListBoxControl)
				createdControl = ConvertReportControlToCheckedListBox(control as StiCheckedListBoxControl);

			if (control is StiLookUpBoxControl)
				createdControl = ConvertReportControlToLookUpBox(control as StiLookUpBoxControl);

			else if (control is StiComboBoxControl)
				createdControl = ConvertReportControlToComboBox(control as StiComboBoxControl);

			if (control is StiNumericUpDownControl)
				createdControl = ConvertReportControlToNumericUpDown(control as StiNumericUpDownControl);

			if (control is StiDateTimePickerControl)
				createdControl = ConvertReportControlToDateTimePicker(control as StiDateTimePickerControl);

            if (control is StiCustomControl)
            {
                if (((StiCustomControl)control).Control != null)
                {
                    createdControl = ConvertReportControlToCustomControl(control as StiCustomControl);
                }
                else
                {
                    return;
                }
            }

			createdControl.TabIndex = tabIndex;
			parentControl.Controls.Add(createdControl);
			

			ReportToControl[control] = createdControl;
			ControlToReport[createdControl] = control;

			if (control.IsReportContainer)
			{
				for (int index = control.Components.Count - 1; index >= 0; index--)
				{
					StiReportControl component = control.Components[index] as StiReportControl;
					ConvertDialogsToControls(createdControl, component, index);
				}
			}
		}

		protected virtual void ConvertReportControlToForm(StiForm formControl)
		{
			form = new Form();
            if (Form.ActiveForm != null)
                form.Owner = Form.ActiveForm;
			formControl.Control = form;
			form.Handle.ToString();
			form.FormBorderStyle =	FormBorderStyle.FixedDialog;
			form.MaximizeBox =		false;
			form.MinimizeBox =		false;
			form.ShowInTaskbar =	false;

			form.Location =			formControl.Location;
			form.Size =				new Size(formControl.Size.Width,  
                                             formControl.Size.Height - 7 + SystemInformation.CaptionHeight);
			form.WindowState =		formControl.WindowState;
			form.StartPosition =	formControl.StartPosition;
			form.BackColor =		formControl.BackColor;
			form.Font =				formControl.Font;
			form.Text =				formControl.Text;
			form.RightToLeft =		formControl.RightToLeft;
			form.DialogResult =		formControl.DialogResult;

			form.Closed += new EventHandler(OnClosedForm);
			form.Closing += new CancelEventHandler(OnClosingForm);
			form.Click += new EventHandler(OnClick);
			form.DoubleClick += new EventHandler(OnDoubleClick);

			ReportToControl[formControl] = form;
			ControlToReport[form] = formControl;

			for (int index = formControl.Components.Count - 1; index >= 0; index--) 
			{
				StiReportControl component = formControl.Components[index] as StiReportControl;
                if (component != null)
				    ConvertDialogsToControls(form, component, index);
			}
		}
		#endregion

		#region Methods.ConvertControlToReportControl
		protected virtual void ConvertControlToReportControl(StiReportControl reportControl, Control control)
		{
			reportControl.DockStyle = StiComponent.ConvertDock(control.Dock);
			reportControl.Location =	control.Location;
			reportControl.Size =		control.Size;
			reportControl.BackColor =	control.BackColor;
			reportControl.ForeColor =	control.ForeColor;
			reportControl.Font =		control.Font;
			reportControl.RightToLeft =	control.RightToLeft;
			reportControl.Enabled =		control.Enabled;
			//reportControl.Visible =		control.Visible;
			reportControl.TagValue =	control.Tag;
		}

		protected virtual void ConvertPanelToReportControl(StiPanelControl panelControl, Control panel)
		{
			ConvertControlToReportControl(panelControl, panel);
			panelControl.BorderStyle =	((Stimulsoft.Report.Controls.StiPanel)panel).BorderStyle;
		}

		protected virtual void ConvertTextBoxToReportControl(StiTextBoxControl textBoxControl, Control textBox)
		{
			ConvertControlToReportControl(textBoxControl, textBox);
			textBoxControl.MaxLength =		((TextBox)textBox).MaxLength;
			textBoxControl.PasswordChar =	((TextBox)textBox).PasswordChar;
			textBoxControl.Text =			((TextBox)textBox).Text;
			textBoxControl.Multiline =		((TextBox)textBox).Multiline;
			textBoxControl.WordWrap =		((TextBox)textBox).WordWrap;
			textBoxControl.AcceptsReturn =	((TextBox)textBox).AcceptsReturn;
			textBoxControl.AcceptsTab =		((TextBox)textBox).AcceptsTab;
		}

		protected virtual void ConvertRichTextBoxToReportControl(StiRichTextBoxControl textBoxControl, Control textBox)
		{
			ConvertControlToReportControl(textBoxControl, textBox);
			textBoxControl.RtfText =		((RichTextBox)textBox).Rtf;
		}

		protected virtual void ConvertButtonToReportControl(StiButtonControl buttonControl, Control button)
		{
			ConvertControlToReportControl(buttonControl, button);
			buttonControl.DialogResult =	((Button)button).DialogResult;
			buttonControl.Text =			button.Text;
		}

		protected virtual void ConvertCheckBoxToReportControl(StiCheckBoxControl checkBoxControl, Control checkBox)
		{
			ConvertControlToReportControl(checkBoxControl, checkBox);
			checkBoxControl.Checked =		((CheckBox)checkBox).Checked;
			checkBoxControl.Text =		checkBox.Text;
		}

		protected virtual void ConvertRadioButtonToReportControl(StiRadioButtonControl radioButtonControl, Control radioButton)
		{
			ConvertControlToReportControl(radioButtonControl, radioButton);
			radioButtonControl.Checked =	((RadioButton)radioButton).Checked;
			radioButtonControl.Text =		radioButton.Text;
		}

		protected virtual void ConvertLabelToReportControl(StiLabelControl labelControl, Control label)
		{
			ConvertControlToReportControl(labelControl, label);
			labelControl.TextAlign =	((Label)label).TextAlign;
			labelControl.Text =		label.Text;
		}

		protected virtual void ConvertGridToReportControl(StiGridControl gridControl, Control grid)
		{
			ConvertControlToReportControl(gridControl, grid);

			DataGrid gd = grid as DataGrid;

			gridControl.ColumnHeadersVisible =	gd.ColumnHeadersVisible;
			gridControl.RowHeadersVisible =		gd.RowHeadersVisible;
			gridControl.GridLineStyle =			gd.GridLineStyle;
			gridControl.HeaderFont =			gd.HeaderFont;
			gridControl.PreferredColumnWidth =	gd.PreferredColumnWidth;
			gridControl.PreferredRowHeight =	gd.PreferredRowHeight;
			gridControl.RowHeaderWidth =		gd.RowHeaderWidth;

			gridControl.AlternatingBackColor =	gd.AlternatingBackColor;
			gridControl.BackgroundColor =		gd.BackgroundColor;
			gridControl.GridLineColor =			gd.GridLineColor;
			gridControl.ForeColor =				gd.ForeColor;
			gridControl.BackColor =				gd.BackColor;
			gridControl.HeaderBackColor =		gd.HeaderBackColor;
			gridControl.HeaderForeColor =		gd.HeaderForeColor;
			gridControl.SelectionBackColor =	gd.SelectionBackColor;
			gridControl.SelectionForeColor =	gd.SelectionForeColor;
		}

		protected virtual void ConvertPictureBoxToReportControl(StiPictureBoxControl pictureBoxControl, Control pictureBox)
		{
			ConvertControlToReportControl(pictureBoxControl, pictureBox);
			pictureBoxControl.SizeMode =			((PictureBox)pictureBox).SizeMode;
			//pictureBoxControl.TransparentColor =	((PictureBox)pictureBox).TransparentColor;
			pictureBoxControl.BorderStyle =			((PictureBox)pictureBox).BorderStyle;
			pictureBoxControl.Image =				((PictureBox)pictureBox).Image;
		}

		protected virtual void ConvertGroupBoxToReportControl(StiGroupBoxControl groupBoxControl, Control groupBox)
		{
			ConvertControlToReportControl(groupBoxControl, groupBox);
			groupBoxControl.Text =		groupBox.Text;
		}

		protected virtual void ConvertListBoxToReportControl(StiListBoxControl listBoxControl, ListBox listBox)
		{
			ConvertControlToReportControl(listBoxControl, listBox);
			listBoxControl.SelectionMode =	listBox.SelectionMode;
			listBoxControl.Sorted =			listBox.Sorted;
			listBoxControl.SelectedIndex =	listBox.SelectedIndex;
			listBoxControl.SelectedItem =	listBox.SelectedItem;
			listBoxControl.SelectedValue =	listBox.SelectedValue;
			listBoxControl.ItemHeight =		listBox.ItemHeight;
			
			listBoxControl.Items.ClearCore();
			foreach (object obj in listBox.Items)listBoxControl.Items.AddCore(obj);
		}

		protected virtual void ConvertTreeViewToReportControl(StiTreeViewControl treeViewControl, TreeView treeView)
		{
			ConvertControlToReportControl(treeViewControl, treeView);
		}

		protected virtual void ConvertListViewToReportControl(StiListViewControl listViewControl, ListView listView)
		{
			ConvertControlToReportControl(listViewControl, listView);
		}

		protected virtual void ConvertCheckedListBoxToReportControl(StiCheckedListBoxControl checkedListBoxControl, CheckedListBox checkedListBox)
		{
			ConvertControlToReportControl(checkedListBoxControl, checkedListBox);
			checkedListBoxControl.SelectionMode =	checkedListBox.SelectionMode;
			checkedListBoxControl.Sorted =			checkedListBox.Sorted;
			checkedListBoxControl.SelectedIndex =	checkedListBox.SelectedIndex;
			checkedListBoxControl.SelectedItem =	checkedListBox.SelectedItem;
			checkedListBoxControl.SelectedValue =	checkedListBox.SelectedValue;
			checkedListBoxControl.ItemHeight =		checkedListBox.ItemHeight;
			checkedListBoxControl.CheckOnClick =	checkedListBox.CheckOnClick;
			
			#region Checked Items
			object[] items = new object[checkedListBox.CheckedItems.Count];
			int index = 0;
			foreach (object obj in checkedListBox.CheckedItems)
			{
				items[index++] = obj;
			}
			checkedListBoxControl.CheckedItems = items;
			#endregion
			
			checkedListBoxControl.Items.ClearCore();
			foreach (object obj in checkedListBox.Items)checkedListBoxControl.Items.AddCore(obj);
		}

		protected virtual void ConvertComboBoxToReportControl(StiComboBoxControl comboBoxControl, ComboBox comboBox)
		{
			ConvertControlToReportControl(comboBoxControl, comboBox);
			comboBoxControl.DropDownStyle =		comboBox.DropDownStyle;
			comboBoxControl.MaxLength =			comboBox.MaxLength;
			comboBoxControl.Sorted =			comboBox.Sorted;
			comboBoxControl.SelectedIndex =		comboBox.SelectedIndex;
			comboBoxControl.SelectedItem =		comboBox.SelectedItem;
			comboBoxControl.SelectedValue =		comboBox.SelectedValue;
			comboBoxControl.ItemHeight =		comboBox.ItemHeight;
			comboBoxControl.MaxDropDownItems =	comboBox.MaxDropDownItems;
			comboBoxControl.DropDownWidth =		comboBox.DropDownWidth;
            comboBoxControl.Text =              comboBox.Text;
			
			comboBoxControl.Items.ClearCore();
			foreach (object obj in comboBox.Items)comboBoxControl.Items.AddCore(obj);			
		}

		protected virtual void ConvertLookUpBoxToReportControl(StiLookUpBoxControl lookUpBoxControl, StiLookUpBox lookUpBox)
		{
			ConvertControlToReportControl(lookUpBoxControl, lookUpBox);
			lookUpBoxControl.DropDownStyle =		lookUpBox.DropDownStyle;
			lookUpBoxControl.MaxLength =			lookUpBox.MaxLength;
			lookUpBoxControl.Sorted =				lookUpBox.Sorted;
			lookUpBoxControl.SelectedIndex =		lookUpBox.SelectedIndex;
			lookUpBoxControl.SelectedItem =			lookUpBox.SelectedItem;
			lookUpBoxControl.SelectedValue =		lookUpBox.SelectedValue;
			lookUpBoxControl.SelectedKey =			lookUpBox.SelectedKey;
			lookUpBoxControl.ItemHeight =			lookUpBox.ItemHeight;
			lookUpBoxControl.MaxDropDownItems =		lookUpBox.MaxDropDownItems;
			lookUpBoxControl.DropDownWidth =		lookUpBox.DropDownWidth;
            lookUpBoxControl.Text =                 lookUpBox.Text;
			
			lookUpBoxControl.Items.Clear();
			foreach (object obj in lookUpBox.Items)lookUpBoxControl.Items.Add(obj);

			lookUpBoxControl.Keys.Clear();
			foreach (object obj in lookUpBox.Keys)lookUpBoxControl.Keys.Add(obj);
		}

		protected virtual void ConvertNumericUpDownToReportControl(StiNumericUpDownControl numericUpDownControl, NumericUpDown numericUpDown)
		{
			ConvertControlToReportControl(numericUpDownControl, numericUpDown);
			numericUpDownControl.Minimum =		(int)numericUpDown.Minimum;
			numericUpDownControl.Maximum =		(int)numericUpDown.Maximum;
			numericUpDownControl.Increment =	(int)numericUpDown.Increment;
			numericUpDownControl.Value =		(int)numericUpDown.Value;	
		}

		protected virtual void ConvertDateTimePickerToReportControl(StiDateTimePickerControl dateTimePickerControl, DateTimePicker dateTimePicker)
		{
			ConvertControlToReportControl(dateTimePickerControl, dateTimePicker);
			dateTimePickerControl.CustomFormat =	dateTimePicker.CustomFormat;
			dateTimePickerControl.DropDownAlign =	dateTimePicker.DropDownAlign;
			dateTimePickerControl.ShowUpDown =		dateTimePicker.ShowUpDown;
			dateTimePickerControl.MaxDate =			dateTimePicker.MaxDate;
			dateTimePickerControl.MinDate =			dateTimePicker.MinDate;
			dateTimePickerControl.Value =			dateTimePicker.Value;
			dateTimePickerControl.Format =			dateTimePicker.Format;
		}

		protected virtual void ConvertControlsToDialogs(Control parentControl, StiReportControl control)
		{
			Control createdControl = ReportToControl[control] as Control;

			if (createdControl != null)
			{			
				if (control is StiPanelControl)
					ConvertPanelToReportControl(control as StiPanelControl, createdControl);

				if (control is StiTextBoxControl)
					ConvertTextBoxToReportControl(control as StiTextBoxControl, createdControl);

				if (control is StiRichTextBoxControl)
					ConvertRichTextBoxToReportControl(control as StiRichTextBoxControl, createdControl);
			
				if (control is StiButtonControl)
					ConvertButtonToReportControl(control as StiButtonControl, createdControl);

				if (control is StiCheckBoxControl)
					ConvertCheckBoxToReportControl(control as StiCheckBoxControl, createdControl);

				if (control is StiRadioButtonControl)
					ConvertRadioButtonToReportControl(control as StiRadioButtonControl, createdControl);

				if (control is StiLabelControl)
					ConvertLabelToReportControl(control as StiLabelControl, createdControl);

				if (control is StiGridControl)
					ConvertGridToReportControl(control as StiGridControl, createdControl);

				if (control is StiPictureBoxControl)
					ConvertPictureBoxToReportControl(control as StiPictureBoxControl, createdControl);

				if (control is StiGroupBoxControl)
					ConvertGroupBoxToReportControl(control as StiGroupBoxControl, createdControl);

				if (control is StiListBoxControl)
					ConvertListBoxToReportControl(control as StiListBoxControl, (ListBox)createdControl);

				if (control is StiTreeViewControl)
					ConvertTreeViewToReportControl(control as StiTreeViewControl, (TreeView)createdControl);

				if (control is StiListViewControl)
					ConvertListViewToReportControl(control as StiListViewControl, (ListView)createdControl);

				if (control is StiCheckedListBoxControl)
					ConvertCheckedListBoxToReportControl(control as StiCheckedListBoxControl, (CheckedListBox)createdControl);

				if (control is StiLookUpBoxControl)
					ConvertLookUpBoxToReportControl(control as StiLookUpBoxControl, (StiLookUpBox)createdControl);

				else if (control is StiComboBoxControl)
					ConvertComboBoxToReportControl(control as StiComboBoxControl, (ComboBox)createdControl);

				if (control is StiNumericUpDownControl)
					ConvertNumericUpDownToReportControl(control as StiNumericUpDownControl, (NumericUpDown)createdControl);

				if (control is StiDateTimePickerControl)
					ConvertDateTimePickerToReportControl(control as StiDateTimePickerControl, (DateTimePicker)createdControl);


				if (control.IsReportContainer)
				{
					foreach (StiReportControl component in control.Components)
					{
						ConvertControlsToDialogs(createdControl, component);
					}
				}
			}
		}

		protected virtual void FormToReportControl(StiForm formControl)
		{
			if (formControl != null)
			{
				formControl.Location =			form.Location;
				formControl.Size =				form.Size;
				formControl.WindowState =		form.WindowState;
				formControl.StartPosition =		form.StartPosition;
				formControl.BackColor =			form.BackColor;
				formControl.Font =				form.Font;
				formControl.Text =				form.Text;
				formControl.RightToLeft =		form.RightToLeft;
				formControl.DialogResult =		form.DialogResult;

				foreach (StiComponent component in formControl.Components)
				{
                    StiReportControl control = component as StiReportControl;
                    if (control != null)
					    ConvertControlsToDialogs(form, control);
				}
			}
		}
		#endregion
		
		#region Methods
		private void OnFormClose(object sender, EventArgs e)
		{
			form.Close();
		}


		private void OnReportControlUpdate(object sender, StiReportControlUpdateEventArgs e)
		{
			if (!lockReportControlUpdate)
			{
				string propertyName = e.PropertyName;
				Control control = ReportToControl[sender] as Control;
				StiComponent reportControl = sender as StiComponent;

				PropertyInfo reportInfo = null;

				//Special checking for RightToLeft property because StiForm contain two RightToLeft property
				if (propertyName == "RightToLeft")reportInfo = reportControl.GetType().GetProperty(propertyName, typeof(RightToLeft));
                else if (propertyName == "BorderStyle") reportInfo = reportControl.GetType().GetProperty(propertyName, typeof(System.Windows.Forms.BorderStyle));
				else reportInfo = reportControl.GetType().GetProperty(propertyName);

				PropertyInfo controlInfo = null;
				
				try
				{
                    if (propertyName == "BorderStyle") controlInfo = control.GetType().GetProperty(propertyName, typeof(System.Windows.Forms.BorderStyle));
                    else if (propertyName == "Value" && reportControl is StiDateTimePickerControl) controlInfo = control.GetType().GetProperty(propertyName, typeof(DateTime));
                    else if (propertyName == "Value") controlInfo = control.GetType().GetProperty(propertyName, typeof(decimal));
                    else if (propertyName == "Filter")
                    {
                        DataGrid grid = control as DataGrid;
                        if (grid != null)
                        {
                            object value3 = reportInfo.GetValue(reportControl, null);

                            DataView view = grid.DataSource as DataView;
                            if (view != null) view.RowFilter = value3 as string;
                        }
                    }
                    else
                    {
                        PropertyInfo[] properties = control.GetType().GetProperties();

                        foreach (PropertyInfo propertyInfo in properties)
                        {
                            if (propertyName == propertyInfo.Name && (propertyInfo.PropertyType == reportInfo.PropertyType || propertyName == "Items" || propertyName == "Keys"))
                            {
                                controlInfo = propertyInfo;
                                break;
                            }
                        }
                    }                    
				}
				catch
				{
				}

				if (reportInfo == null) return;
				if (controlInfo == null) return;
				
				object value = reportInfo.GetValue(reportControl, null);
				if (value is StiArrayList && propertyName == "Items" && control is ListBox)
				{
					var listBox = control as ListBox;
					listBox.Items.Clear();
					if (reportControl is StiCheckedListBoxControl)
						listBox.Items.AddRange(((StiCheckedListBoxControl)reportControl).Items.ToArray());
					else
						listBox.Items.AddRange(((StiListBoxControl)reportControl).Items.ToArray());

				}
				else if (value is StiArrayList && propertyName == "Items" && control is ComboBox)
				{
					var comboBox = control as ComboBox;
					comboBox.Items.Clear();
					comboBox.Items.AddRange(((StiComboBoxControl)reportControl).Items.ToArray());
				}
				else if (value is StiArrayList && propertyName == "Keys" && control is StiLookUpBox)
				{
					var lookUpBox = control as StiLookUpBox;
					lookUpBox.Keys.Clear();
					lookUpBox.Keys.AddRange(((StiLookUpBoxControl)reportControl).Keys.ToArray());
				}
				else if (value is StiArrayList && propertyName == "Items" && control is CheckedListBox)
				{
					var checkedListBox = control as CheckedListBox;
					checkedListBox.Items.Clear();
					checkedListBox.Items.AddRange(((StiCheckedListBoxControl)reportControl).Items.ToArray());
				}
				else if (propertyName == "Filter")
				{
					var grid = control as DataGrid;
					if (grid != null)
					{
						var view = grid.DataSource as DataView;
						if (view != null)view.RowFilter = value as string;
					}
				}
				else 
				{
                    try
                    {
                        if (value is IConvertible)
                        {
                            value = Convert.ChangeType(value, controlInfo.PropertyType);
                            controlInfo.SetValue(control, value, null);
                        }
                    }
                    catch
                    {
                    }
				}
			}
		} 
		

		public override void PrepareForm()
		{
			ControlToReport = new Hashtable();
			ReportToControl = new Hashtable();
			ManagerToGridControl = new Hashtable();
			toolTip = new ToolTip();
		}


		public override void DisposeForm()
		{
			if (ControlToReport != null) ControlToReport.Clear();
			if (ReportToControl != null) ReportToControl.Clear();
			if (ManagerToGridControl != null) ManagerToGridControl.Clear();
			ControlToReport = null;
			ReportToControl = null;
			ManagerToGridControl = null;

			if (form != null) form.Dispose();
			form = null;

			if (toolTip != null) toolTip.Dispose();
			toolTip = null;

			formControl = null;
		}


		public override void LoadForm(IStiForm formControl)
		{
			this.formControl = formControl as StiForm;

			ConvertReportControlToForm(formControl as StiForm);			
			this.formControl.ReportControlUpdate += OnReportControlUpdate;
			this.formControl.FormClose += OnFormClose;
			this.formControl.InvokeLoadForm(EventArgs.Empty);
		}


		public override void CloseForm()
		{
			if (formControl != null && form != null)
			{
				this.formControl.ReportControlUpdate -= OnReportControlUpdate;
				this.formControl.FormClose -= OnFormClose;
				FormToReportControl(formControl);
			}
		}


		/// <summary>
		/// Render form.
		/// </summary>
		public override bool RenderForm(IStiForm formControl)
		{
			try
			{
				this.formControl = formControl as StiForm;

				PrepareForm();

				if (formControl.Visible)
				{
					if (report.Progress != null && report.Progress.IsVisible)report.Progress.Hide();
					try
					{
						LoadForm(formControl);
					
						if (formControl.Visible)
						{
							var result = form.ShowDialog();
							CloseForm();
							if (result == DialogResult.Cancel)return false;
						}
					}
					catch (Exception ee)
					{
                        StiExceptionProvider.Show(ee);
						return false;
					}
				}
			}
			finally
			{
				CloseForm();
			}
			return true;
		}


		/// <summary>
		/// Render all forms in report.
		/// </summary>
		public override bool Render(StiReport report, StiFormStartMode startMode)
		{
			try
			{
				this.report = report;
				foreach (StiPage page in report.Pages)
				{
					StiForm formControl = page as StiForm;
					if (formControl != null && formControl.StartMode == startMode)
					{
						if (!this.RenderForm(formControl))return false;
					}
				}
				return true;
			}
			finally
			{
				this.report = null;
			}
		}

		public override IStiForm CreateForm(StiReport report)
		{
			return new StiForm(report);
		}

		public override IStiTextBoxControl CreateTextBoxControl()
		{
			return new StiTextBoxControl();
		}

		public override IStiLabelControl CreateLabelControl()
		{
			return new StiLabelControl();
		}

		public override IStiCheckBoxControl CreateCheckBoxControl()
		{
			return new StiCheckBoxControl();
		}

		public override IStiPictureBoxControl CreatePictureBoxControl()
		{
			return new StiPictureBoxControl();
		}
		#endregion

        public StiWinDialogsProvider(StiReport report)
        {
            this.report = report;
        }

        public StiWinDialogsProvider() : this(null)
        {
        }
	}
}
