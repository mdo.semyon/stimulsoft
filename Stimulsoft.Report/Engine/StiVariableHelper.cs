#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dictionary;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Stimulsoft.Report.Engine
{
    public class StiVariableHelper
    {
        public static void FillItemsOfVariables(StiReport compiledReport)
        {
            if (compiledReport.IsSLPreviewDialogs) return;

            foreach (StiVariable variable in compiledReport.Dictionary.Variables)
            {
                if (FillItemsOfVariable(variable, compiledReport)) return;
            }
        }

        public static bool FillItemsOfVariable(StiVariable variable, StiReport compiledReport)
        {

#if SERVER
            //If this variable initialized from the server side then we need skip variable initialization
            if (compiledReport.ServerParameters != null && compiledReport.ServerParameters[variable.Name] != null) return false;
#endif

            if (variable.DialogInfo == null) return false;

            #region StiItemsInitializationType.Items
            if (variable.DialogInfo.ItemsInitializationType == StiItemsInitializationType.Items)
            {
                if (compiledReport.IsPreviewDialogs) return false;

                #region Fill List
                var items = variable.DialogInfo.GetDialogInfoItems(variable.Type);
                if (items != null && items.Count > 0)
                {
                    var list = compiledReport[variable.Name] as IStiList;
                    if (list == null) return false;
                    if (list.Count > 0)return true;

                    list.Clear();

                    foreach (var item in items)
                    {
                        try
                        {
                            list.AddElement(item.KeyObject);
                        }
                        catch
                        {
                        }
                    }
                }
                #endregion
            }
            #endregion

            #region StiItemsInitializationType.Columns
            else
            {
                if ((variable.DialogInfo.Keys != null && variable.DialogInfo.Keys.Length > 0) ||
                    (variable.DialogInfo.Values != null && variable.DialogInfo.Values.Length > 0))
                    return false;

                var keys = !string.IsNullOrEmpty(variable.DialogInfo.KeysColumn) ?
                    StiDataColumn.GetDatasFromDataColumn(compiledReport.Dictionary, variable.DialogInfo.KeysColumn) : null;

                #region Fill List
                if (StiTypeFinder.FindInterface(variable.Type, typeof(IStiList)))
                {
                    if (keys != null && keys.Length > 0)
                    {
                        var list = compiledReport[variable.Name] as IStiList;
                        if (list == null)
                        {
                            var typeList = StiType.GetTypeFromTypeMode(variable.Type, StiTypeMode.List);
                            list = StiActivator.CreateObject(typeList) as IStiList;
                        }

                        list.Clear();

                        foreach (var key in keys)
                        {
                            try
                            {
                                list.AddElement(key);
                            }
                            catch
                            {
                            }
                        }
                    }
                }
                #endregion

                var values = !string.IsNullOrEmpty(variable.DialogInfo.ValuesColumn) ?
                    StiDataColumn.GetDatasFromDataColumn(compiledReport.Dictionary, variable.DialogInfo.ValuesColumn) : null;

                var valuesBinding = !string.IsNullOrEmpty(variable.DialogInfo.BindingValuesColumn) ?
                    StiDataColumn.GetDatasFromDataColumn(compiledReport.Dictionary, variable.DialogInfo.BindingValuesColumn) : null;

                if (keys == null) keys = new object[0];
                if (values == null) values = new object[0];
                if (valuesBinding == null) valuesBinding = new object[0];

                var length = Math.Max(keys.Length, values.Length);

                var items = new List<StiDialogInfoItem>();

                #region Create List<StiDialogInfoItem>
                var type = variable.Type;

                for (var index = 0; index < length; index++)
                {
                    var key = keys.Length > index ? keys[index] : null;
                    var value = values.Length > index ? values[index].ToString() : string.Empty;
                    var valueBinding = valuesBinding.Length > index ? valuesBinding[index] : null;

                    StiDialogInfoItem item = null;

                    #region StiLongDialogInfoItem
                    if (type == typeof(ByteList) ||
                        type == typeof(ShortList) ||
                        type == typeof(IntList) ||
                        type == typeof(LongList) ||
                        type == typeof(byte) ||
                        type == typeof(short) ||
                        type == typeof(int) ||
                        type == typeof(long) ||
                        type == typeof(byte?) ||
                        type == typeof(short?) ||
                        type == typeof(int?) ||
                        type == typeof(long?))
                    {
                        item = new StiLongDialogInfoItem();
                    }
                    #endregion

                    #region StiStringDialogInfoItem
                    else if (type == typeof(StringList) || type == typeof(String))
                    {
                        item = new StiStringDialogInfoItem();
                    }
                    #endregion

                    #region StiDoubleDialogInfoItem
                    else if (
                        type == typeof(DoubleList) ||
                        type == typeof(FloatList) ||
                        type == typeof(double) ||
                        type == typeof(float) ||
                        type == typeof(double?) ||
                        type == typeof(float?))
                    {
                        item = new StiDoubleDialogInfoItem();
                    }
                    #endregion

                    #region StiDecimalDialogInfoItem
                    else if (type == typeof(DecimalList) || type == typeof(Decimal) || type == typeof(Decimal?))
                    {
                        item = new StiDecimalDialogInfoItem();
                    }
                    #endregion

                    #region StiDateTimeDialogInfoItem
                    else if (type == typeof(DateTimeList) || type == typeof(DateTime) || type == typeof(DateTime?))
                    {
                        item = new StiDateTimeDialogInfoItem();
                    }
                    #endregion

                    #region StiTimeSpanDialogInfoItem
                    else if (type == typeof(TimeSpanList) || type == typeof(TimeSpan) || type == typeof(TimeSpan?))
                    {
                        item = new StiTimeSpanDialogInfoItem();
                    }
                    #endregion

                    #region StiBoolDialogInfoItem
                    else if (type == typeof(BoolList) || type == typeof(bool) || type == typeof(bool?))
                    {
                        item = new StiBoolDialogInfoItem();
                    }
                    #endregion

                    #region StiCharDialogInfoItem
                    else if (type == typeof(CharList) || type == typeof(char) || type == typeof(char?))
                    {
                        item = new StiCharDialogInfoItem();
                    }
                    #endregion

                    #region StiGuidDialogInfoItem
                    else if (type == typeof(GuidList) || type == typeof(Guid))
                    {
                        item = new StiGuidDialogInfoItem();
                    }
                    #endregion

                    if (key != null)
                        item.KeyObject = key;

                    item.Value = value;
                    item.ValueBinding = valueBinding;

                    items.Add(item);
                }
                #endregion

                var itemsFiltered = new List<StiDialogInfoItem>();
                var hash = new Hashtable();
                foreach (var item in items)
                {
                    if (hash[item.KeyObject] == null || item is StiRangeDialogInfoItem)
                    {
                        hash[item.KeyObject] = item.KeyObject;
                        itemsFiltered.Add(item);
                    }
                }
                //fix Artem (17.09.2013) (RequestFromUser Do not use filtering for dependent variables because this may cause lost of data for further calculations.)
                var resultItem = variable.DialogInfo.BindingValue ? items : itemsFiltered;
                variable.DialogInfo.SetDialogInfoItems(resultItem, variable.Type);

            }
            #endregion

            return false;
        }

        public static void SetDefaultValueForRequestFromUserVariables(StiReport compiledReport)
        {
            var tempText = new StiText { Name = "**VariableRequestFromUser**" };
            if (compiledReport.Pages.Count > 0)
                tempText.Page = compiledReport.Pages[0];

            var haveVars = false;

            #region Calculate variables
            foreach (StiVariable variable in compiledReport.Dictionary.Variables)
            {
                if (variable.RequestFromUser)
                {
                    haveVars = true;

                    if (!compiledReport.ModifiedVariables.ContainsKey(variable.Name))
                    {
                        try
                        {
                            if (variable.Selection == StiSelectionMode.FromVariable && variable.InitBy == StiVariableInitBy.Expression)
                                compiledReport[variable.Name] = StiParser.ParseTextValue("{" + variable.Value + "}", tempText);

                            if (variable.Selection == StiSelectionMode.First && variable.DialogInfo.Keys != null && variable.DialogInfo.Keys.Length > 0)
                                compiledReport[variable.Name] = StiReport.ChangeType(variable.DialogInfo.Keys[0], variable.Type);
                        }
                        catch
                        {
                        }
                    }
                }
            }
            #endregion

            if (!haveVars) return;

            #region Reconnect datasources with RequestFromUser variables in the SqlCommand
            var dataSourcesToReconnect = compiledReport.Dictionary.ReconnectListForRequestFromUserVariables;
            if (dataSourcesToReconnect == null)
                dataSourcesToReconnect = GetDataSourcesWithRequestFromUserVariablesInCommand(compiledReport).ToArray();

            if (dataSourcesToReconnect.Length <= 0) return;

            foreach (var dsName in dataSourcesToReconnect)
            {
                var ds = compiledReport.Dictionary.DataSources[dsName];
                if (ds != null)
                {
                    ds.Disconnect();
                    ds.Connect();
                }
            }
            compiledReport.Dictionary.RegRelations();
            compiledReport.Dictionary.RegRelations(true);
            #endregion
        }

        internal static List<string> GetDataSourcesWithRequestFromUserVariablesInCommand(StiReport report)
        {
            var list = new List<string>();
            var vars = new Hashtable();

            var tempText = new StiText
            {
                Name = "**VariableRequestFromUser**",
                Page = report.Pages[0]
            };

            foreach (StiVariable variable in report.Dictionary.Variables)
            {
                if (variable.RequestFromUser)
                    vars[variable.Name] = null;
            }

            foreach (StiDataSource ds in report.Dictionary.DataSources)
            {
                var sql = ds as StiSqlSource;
                if (sql == null) continue;

                try
                {
                    string sqlCommand = null;
                    if ((report.CalculationMode == StiCalculationMode.Interpretation) && (report.Variables != null))
                    {
                        object baseSqlCommand = report.Variables["**StoredDataSourceSqlCommandForInterpretationMode**" + ds.Name];
                        if ((baseSqlCommand != null) && (baseSqlCommand is string))
                        {
                            sqlCommand = baseSqlCommand as string;
                        }
                    }
                    if (string.IsNullOrWhiteSpace(sqlCommand)) sqlCommand = sql.SqlCommand;

                    var found = CheckExpressionForVariables(sqlCommand, tempText, vars);
                    if (!found)
                    {
                        foreach (StiDataParameter parameter in sql.Parameters)
                        {
                            if (!string.IsNullOrWhiteSpace(parameter.Expression) && CheckExpressionForVariables(parameter.Expression, tempText, vars))
                            {
                                found = true;
                                break;
                            }
                        }
                    }

                    if (found)
                        list.Add(ds.Name);
                }
                catch
                {
                }
            }
            return list;
        }

        private static bool CheckExpressionForVariables(string expression, StiComponent component, Hashtable vars)
        {
            try
            {
                var storeToPrint = false;
                var result = StiParser.ParseTextValue(expression, component, ref storeToPrint, false, true);
                if (result != null && result is List<StiParser.StiAsmCommand>)
                {
                    foreach (var asmCommand in result as List<StiParser.StiAsmCommand>)
                    {
                        if (asmCommand.Type == StiParser.StiAsmCommandType.PushVariable)
                        {
                            var varName = asmCommand.Parameter1.ToString();
                            if (vars.ContainsKey(varName)) return true;
                        }
                    }
                }
            }
            catch
            {
            }
            return false;
        }


        #region RequestFromUser related variables
        public static bool ApplyVariableValue(StiReport report, StiVariable var, object variableValue)
        {
            if (!StiOptions.Engine.ReconnectDataSourcesIfRequestFromUserVariableChanged) return false;

            StiComponent comp = null;
            var dataSourcesToReconnect = GetDataSourcesToReconnectList(report, var, ref comp);
            if (dataSourcesToReconnect.Count == 0) return false;

            #region Set variable value
            var field = report.GetType().GetField(var.Name);
            if (field != null)
                field.SetValue(report, variableValue);

            else
                report[var.Name] = variableValue;
            #endregion

            //reconnect dataSources
            report.Dictionary.ConnectToDatabases(false);
            foreach (var ds in dataSourcesToReconnect)
            {
                ds.Disconnect();
                ds.Connect();
            }
            report.Dictionary.DataStore.ClearReportDatabase();
            return true;
        }

        public static void RefreshDialogInfo(StiReport report, StiVariable var)
        {
            if (!StiOptions.Engine.ReconnectDataSourcesIfRequestFromUserVariableChanged) return;

            var.DialogInfo.Keys = null;
            var.DialogInfo.Values = null;
            FillItemsOfVariable(var, report);
        }

        public static List<string> GetRelatedVariablesList(StiReport report, StiVariable var)
        {
            var relatedVariables = new List<string>();

            var dataSourceNames = new Hashtable();
            StiComponent comp = null;
            var dataSourcesToReconnect = GetDataSourcesToReconnectList(report, var, ref comp);

            //find the variables that are initialized from these reconnected dataSources
            foreach (StiVariable vr in report.Dictionary.Variables)
            {
                if (vr.RequestFromUser && (vr.DialogInfo != null))
                {
                    dataSourceNames.Clear();

                    if (vr.DialogInfo.ItemsInitializationType == StiItemsInitializationType.Columns)
                        StiDataSourceHelper.CheckExpression("{" + vr.DialogInfo.KeysColumn + "}", comp, dataSourceNames);

                    if (vr.InitBy == StiVariableInitBy.Expression)
                    {
                        StiDataSourceHelper.CheckExpression("{" + vr.Value + "}", comp, dataSourceNames);
                        if (vr.Type == typeof(Range))
                        {
                            StiDataSourceHelper.CheckExpression("{" + vr.InitByExpressionFrom + "}", comp, dataSourceNames);
                            StiDataSourceHelper.CheckExpression("{" + vr.InitByExpressionTo + "}", comp, dataSourceNames);
                        }
                    }

                    foreach (var ds in dataSourcesToReconnect)
                    {
                        if (dataSourceNames.ContainsKey(ds.Name))
                        {
                            relatedVariables.Add(vr.Name);
                            break;
                        }
                    }
                }
            }

            return relatedVariables;
        }

        private static List<StiDataSource> GetDataSourcesToReconnectList(StiReport report, StiVariable var, ref StiComponent comp)
        {
            var vars = new Hashtable();
            vars[var.Name] = null;

            var dataSourcesToReconnect = new List<StiDataSource>();

            comp = new StiText
            {
                Name = "**DataSourceParameter**",
                Page = report.Pages[0]
            };

            //get list of all datasources, which used in the RequestFromUsers variables
            var dataSourceNames = StiDataSourceHelper.GetDataSourcesUsedInRequestFromUsersVariables(report);

            //leave dataSources with selected variable in parameters
            foreach (string dsName in dataSourceNames.Keys)
            {
                var ds = report.Dictionary.DataSources[dsName];
                foreach (StiDataParameter dp in ds.Parameters)
                {
                    if (CheckExpressionForVariables("{" + dp.Expression + "}", comp, vars))
                    {
                        dataSourcesToReconnect.Add(ds);
                        break;
                    }
                }
            }

            return dataSourcesToReconnect;
        }
        #endregion

    }
}
