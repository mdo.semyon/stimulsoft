#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.PropertyGrid;

namespace Stimulsoft.Report
{
    /// <summary>
    /// Describes the class that contains a style for Map components.
    /// </summary>	
    public class StiTableStyle : StiBaseStyle
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            jObject.AddPropertyStringNullOrEmpty("HeaderColor", StiJsonReportObjectHelper.Serialize.JColor(HeaderColor, Color.White));
            jObject.AddPropertyStringNullOrEmpty("HeaderForeground", StiJsonReportObjectHelper.Serialize.JColor(HeaderForeground, Color.Black));
            jObject.AddPropertyStringNullOrEmpty("FooterForeground", StiJsonReportObjectHelper.Serialize.JColor(FooterForeground, Color.Black));
            jObject.AddPropertyStringNullOrEmpty("DataColor", StiJsonReportObjectHelper.Serialize.JColor(HeaderColor, Color.White));
            jObject.AddPropertyStringNullOrEmpty("DataForeground", StiJsonReportObjectHelper.Serialize.JColor(HeaderColor, Color.Black));
            jObject.AddPropertyStringNullOrEmpty("GridColor", StiJsonReportObjectHelper.Serialize.JColor(HeaderColor, Color.Black));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "HeaderColor":
                        this.HeaderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "HeaderForeground":
                        this.HeaderForeground = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "FooterForeground":
                        this.FooterForeground = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "DataColor":
                        this.DataColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "DataForeground":
                        this.DataForeground = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "GridColor":
                        this.GridColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiTableStyle;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[]
            {
                propHelper.StyleName(),
                propHelper.Description(),
                propHelper.StyleCollectionName(),
                propHelper.StyleConditions()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            list = new[]
            {
                propHelper.HeaderColor(),
                propHelper.HeaderForeground(),
                propHelper.FooterForeground(),
                propHelper.DataColor(),
                propHelper.DataForeground(),
                propHelper.GridColor()
            };
            objHelper.Add(StiPropertyCategories.Appearance, list);

            return objHelper;
        }
        #endregion

        #region Properties
        [StiSerializable]
        [StiCategory("Appearance")]
        public virtual Color HeaderColor { get; set; } = Color.White;

        [StiSerializable]
        [StiCategory("Appearance")]
        public virtual Color HeaderForeground { get; set; } = Color.Black;

        [StiSerializable]
        [StiCategory("Appearance")]
        public virtual Color FooterColor { get; set; } = Color.White;

        [StiSerializable]
        [StiCategory("Appearance")]
        public virtual Color FooterForeground { get; set; } = Color.Black;

        [StiSerializable]
        [StiCategory("Appearance")]
        public virtual Color DataColor { get; set; } = Color.White;

        [StiSerializable]
        [StiCategory("Appearance")]
        public virtual Color DataForeground { get; set; } = Color.Black;

        [StiSerializable]
        [StiCategory("Appearance")]
        public virtual Color GridColor { get; set; } = Color.Black;
        #endregion

        #region Methods.Style
        private Color GetColor(Color color)
        {
            return color == Color.White
                ? Color.FromArgb(180, 255, 255, 255)
                : color;
        }

        public void DrawStyleForGallery(Graphics g, Rectangle rect)
        {
            var rectElement = new Rectangle(rect.X + 5, rect.Y + 5, rect.Width - 10, rect.Height - 10);

            int cellSize = rectElement.Height / 5;
            if (cellSize * 5 > rectElement.Height)
                cellSize--;
            rectElement.Height = cellSize * 5;

            int cols = rectElement.Width / cellSize;
            rectElement.Width = cellSize * cols;

            using (var brush = new SolidBrush(GetColor(this.DataColor)))
            {
                g.FillRectangle(brush, rectElement);
            }

            using (var pen = new Pen(GetColor(GridColor), 1))
            {
                for (int index = 0; index < 6; index++)
                {
                    g.DrawLine(pen, rectElement.X, rectElement.Y + cellSize * index, rectElement.Right, rectElement.Y + cellSize * index);
                }
                for (int index = 0; index <= cols; index++)
                {
                    g.DrawLine(pen, rectElement.X + cellSize * index, rectElement.Y, rectElement.X + cellSize * index, rectElement.Bottom);
                }
            }

            using (var brush = new SolidBrush(GetColor(this.HeaderColor)))
                g.FillRectangle(brush, new Rectangle(rectElement.X, rectElement.Y, rectElement.Width, cellSize));

            using (var brush = new SolidBrush(GetColor(this.FooterColor)))
                g.FillRectangle(brush, new Rectangle(rectElement.X, rectElement.Bottom - cellSize, rectElement.Width, cellSize));
        }

        public override void DrawStyle(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            Rectangle rectElement = rect;
            if (rect.Width == 80 && rect.Height == 80)
                rectElement = new Rectangle(rect.X + 9, rect.Y + 10, 61, 60);

            //else
            //    rectElement = new Rectangle(rect.X + 5, rect.Y + 5, 41, 30);

            int textXOffset = 0;

            if (paintImage)
            {
                int widthStep = 0;
                int heightStep = 0;
                int footerWidth = 0;
                int x = rectElement.X;
                int y = rectElement.Y;

                if (rect.Width == 18 && rect.Height == 10)
                {
                    widthStep = 6;
                    heightStep = 3;

                    footerWidth = widthStep * 3;
                    textXOffset = footerWidth;
                }
                else if (rectElement.Height == 14)
                {
                    x += 2;
                    y += 2;
                    widthStep = 8;
                    heightStep = 3;

                    footerWidth = widthStep * 3;
                    textXOffset = footerWidth;
                }
                else
                {
                    x += 5;
                    y += 5;
                    widthStep = 13;
                    heightStep = 7;

                    footerWidth = widthStep * 3 + 1;
                    textXOffset = footerWidth + 4;
                }

                using (var brush = new SolidBrush(this.HeaderColor))
                {
                    g.FillRectangle(brush, x, y, footerWidth, heightStep);
                }
                using (var brush = new SolidBrush(this.FooterColor))
                {
                    g.FillRectangle(brush, x, y + (heightStep * 3), footerWidth, heightStep);
                }

                using (var brush = new SolidBrush(this.DataColor))
                using (var pen = new Pen(this.GridColor))
                {
                    for (int index = 0; index < 3; index++)
                    {
                        g.FillRectangle(brush, x + (widthStep * index), y + heightStep, widthStep, heightStep);
                        g.DrawRectangle(pen, x + (widthStep * index), y + heightStep, widthStep, heightStep);
                    }

                    for (int index = 0; index < 3; index++)
                    {
                        g.FillRectangle(brush, x + (widthStep * index), y + heightStep * 2, widthStep, heightStep);
                        g.DrawRectangle(pen, x + (widthStep * index), y + heightStep * 2, widthStep, heightStep);
                    }
                }
            }

            #region Draw Text
            if (!paintValue)
            {
                rect.X += textXOffset + 4;
                rect.Width -= textXOffset + 4;

                using (var br = new SolidBrush(Color.Black))
                using (var font = new Font("Arial", 10))
                using (var sf = new StringFormat())
                {
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Center;

                    var textRect = rect;
                    textRect.Inflate(-2, 0);
                    var textSize = g.MeasureString(this.Name, font);
                    if (textRect.Width < textSize.Width)
                        sf.Alignment = StringAlignment.Near;

                    g.DrawString(this.Name, font, br, rect, sf);
                }
            }
            #endregion
        }

        public override void DrawBox(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            rect.X++;
            rect.Y++;
            rect.Width -= 2;
            rect.Height -= 3;

            this.DrawStyle(g, rect, paintValue, paintImage);

            using (Pen pen = new Pen(Color.FromArgb(150, Color.Gray)))
            {
                pen.DashStyle = DashStyle.Dash;
                g.DrawRectangle(pen, rect);
            }
        }

        /// <summary>
        /// Gets a style from the component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void GetStyleFromComponent(StiComponent component, StiStyleElements styleElements)
        {
        }

        /// <summary>
        /// Sets style to a component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void SetStyleToComponent(StiComponent component)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Saves the style in the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the style.</param>
        public override void Save(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Serialize(this, stream, "StiTableStyle");
        }

        /// <summary>
        /// Loads the style from the stream.
        /// </summary>
        /// <param name="stream">Stream for loading the style.</param>
        public override void Load(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Deserialize(this, stream, "StiTableStyle");
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new object of the type StiTableStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        /// <param name="description">Style description.</param>
        internal StiTableStyle(string name, string description, StiReport report)
            : base(name, description, report)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiTableStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        /// <param name="description">Style description.</param>
        public StiTableStyle(string name, string description)
            : this(name, description, null)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiTableStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        public StiTableStyle(string name)
            : this(name, "")
        {
        }

        /// <summary>
        /// Creates a new object of the type StiTableStyle.
        /// </summary>
        public StiTableStyle()
            : this("")
        {
        }
        #endregion
    }
}
