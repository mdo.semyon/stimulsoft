#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.PropertyGrid;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.IO;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report
{
    /// <summary>
    /// Describes the class that contains a style for components.
    /// </summary>	
    public class StiStyle : StiBaseStyle
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiStyle
            jObject.AddPropertyEnum("HorAlignment", HorAlignment, StiTextHorAlignment.Left);
            jObject.AddPropertyEnum("VertAlignment", VertAlignment, StiVertAlignment.Top);
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.FontDefault(Font));

            jObject.AddPropertyStringNullOrEmpty("Border", StiJsonReportObjectHelper.Serialize.JBorder(Border));
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyStringNullOrEmpty("TextBrush", StiJsonReportObjectHelper.Serialize.JBrush(TextBrush));

            jObject.AddPropertyBool("AllowUseHorAlignment", AllowUseHorAlignment);
            jObject.AddPropertyBool("AllowUseVertAlignment", AllowUseVertAlignment);
            jObject.AddPropertyBool("AllowUseImage", AllowUseImage);
            jObject.AddPropertyBool("AllowUseFont", AllowUseFont, true);
            jObject.AddPropertyBool("AllowUseBorderFormatting", AllowUseBorderFormatting, true);
            jObject.AddPropertyBool("AllowUseBorderSides", AllowUseBorderSides, true);
            jObject.AddPropertyBool("AllowUseBorderSidesFromLocation", AllowUseBorderSidesFromLocation);
            jObject.AddPropertyBool("AllowUseBrush", AllowUseBrush, true);
            jObject.AddPropertyBool("AllowUseTextBrush", AllowUseTextBrush, true);
            jObject.AddPropertyStringNullOrEmpty("Image", StiImageConverter.ImageToString(Image));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch(property.Name)
                {
                    case "HorAlignment":
                        this.HorAlignment = (StiTextHorAlignment)Enum.Parse(typeof(StiTextHorAlignment), property.Value.ToObject<string>());
                        break;

                    case "VertAlignment":
                        this.VertAlignment = (StiVertAlignment)Enum.Parse(typeof(StiVertAlignment), property.Value.ToObject<string>());
                        break;

                    case "Font":
                        this.Font = StiJsonReportObjectHelper.Deserialize.Font(property, this.Font);
                        break;

                    case "Border":
                        this.Border = StiJsonReportObjectHelper.Deserialize.Border(property);
                        break;

                    case "Brush":
                        this.Brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "TextBrush":
                        this.TextBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "AllowUseHorAlignment":
                        this.AllowUseHorAlignment = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseVertAlignment":
                        this.AllowUseVertAlignment = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseImage":
                        this.AllowUseImage = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseFont":
                        this.AllowUseFont = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseBorderFormatting":
                        this.AllowUseBorderFormatting = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseBorderSides":
                        this.AllowUseBorderSides = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseBorderSidesFromLocation":
                        this.AllowUseBorderSidesFromLocation = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseBrush":
                        this.AllowUseBrush = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseTextBrush":
                        this.AllowUseTextBrush = property.Value.ToObject<bool>();
                        break;

                    case "Image":
                        this.Image = StiImageConverter.StringToImage(property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiStyle;

	    public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            // MainCategory
            var list = new[] { 
                propHelper.StyleName(), 
                propHelper.Description(),
                propHelper.StylesCollectionName(),
                propHelper.StyleConditions()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            // AppearanceCategory
            list = new[] {
                propHelper.Brush(),
                propHelper.TextBrush(),
                propHelper.Border(),
                propHelper.Font(),
                propHelper.SimpleImage(),
                propHelper.HorAlignment(),
                propHelper.VertAlignment()
            };
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // ParametersCategory
            list = new[] { 
                propHelper.AllowUseBorderFormatting(), 
                propHelper.AllowUseBorderSides(), 
                propHelper.AllowUseBorderSidesFromLocation(),
                propHelper.AllowUseBrush(), 
                propHelper.AllowUseFont(), 
                propHelper.AllowUseImage(), 
                propHelper.AllowUseTextBrush(), 
                propHelper.AllowUseTextOptions(), 
                propHelper.AllowUseHorAlignment(), 
                propHelper.AllowUseVertAlignment()
            };
            objHelper.Add(StiPropertyCategories.Parameters, list);

            return objHelper;
        }
        #endregion

		#region ICloneable
		public override object Clone()
		{
			var style = base.Clone() as StiStyle;
			style.Border = this.Border.Clone() as StiBorder;
			style.Brush = this.Brush.Clone() as StiBrush;
			style.Font = this.Font.Clone() as Font;
			style.TextBrush = this.TextBrush.Clone() as StiBrush;   

			return style;
		}
		#endregion

		#region IStiTextHorAlignment
	    /// <summary>
        /// Gets or sets a horizontal alignment of the style.
		/// </summary>
		[StiSerializable]
		[DefaultValue(StiTextHorAlignment.Left)]
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.StyleHorAlignment)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets a horizontal alignment of the style.")]
		public virtual StiTextHorAlignment HorAlignment { get; set; } = StiTextHorAlignment.Left;
	    #endregion

		#region IStiVertAlignment
	    /// <summary>
		/// Gets or sets a vertical alignment of the style.
		/// </summary>
		[StiSerializable]
		[DefaultValue(StiVertAlignment.Top)]
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.StyleVertAlignment)]
		[TypeConverter(typeof(Stimulsoft.Base.Localization.StiEnumConverter))]
        [Description("Gets or sets a vertical alignment of the style.")]
		public virtual StiVertAlignment VertAlignment { get; set; } = StiVertAlignment.Top;
	    #endregion

		#region IStiFont
	    /// <summary>
        /// Gets or sets a font for drawing this style.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.StyleFont)]
		[StiSerializable]
        [Description("Gets or sets a font for drawing this style.")]
		public Font Font { get; set; } = new Font("Arial",8);
	    #endregion

		#region IStiBorder
	    /// <summary>
		/// Gets or sets a border of the component.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.StyleBorder)]
		[StiSerializable]
        [Description("Gets or sets a border of the component.")]
		public StiBorder Border { get; set; } = new StiBorder();
	    #endregion

		#region IStiBrush
	    /// <summary>
        /// Gets or sets a brush to fill the style.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.StyleBrush)]
		[StiSerializable]
        [Description("Gets or sets a brush to fill the style.")]
		public StiBrush Brush { get; set; } = new StiSolidBrush(Color.Transparent);
	    #endregion

		#region IStiTextBrush
	    /// <summary>
        /// Gets or sets a brush to draw the text.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.StyleTextBrush)]
		[StiSerializable]
        [Description("Gets or sets a brush to draw the text.")]
		public StiBrush TextBrush { get; set; } = new StiSolidBrush(Color.Black);
	    #endregion

		#region Properties.Allow
	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use HorAlignment formatting or not. 
		/// </summary>
		[StiSerializable]
		[StiCategory("Parameters")]
		[StiOrder(StiPropertyOrder.StyleAllowUseHorAlignment)]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use HorAlignment formatting or not.")]
		public bool AllowUseHorAlignment { get; set; }

	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use VertAlignment formatting or not. 
		/// </summary>
		[StiSerializable]
		[StiCategory("Parameters")]
		[StiOrder(StiPropertyOrder.StyleAllowUseVertAlignment)]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use VertAlignment formatting or not.")]
		public bool AllowUseVertAlignment { get; set; }

	    /// <summary>
		/// Gets or sets a value which indicates whether a report engine can use Image formatting or not. 
		/// </summary>
		[StiSerializable]
		[StiCategory("Parameters")]
		[StiOrder(StiPropertyOrder.StyleAllowUseImage)]
		[DefaultValue(false)]
		[TypeConverter(typeof(StiBoolConverter))]
		[Description("Gets or sets a value which indicates whether a report engine can use Image formatting or not.")]
		public bool AllowUseImage { get; set; }

	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use Font formatting or not. 
		/// </summary>
		[StiSerializable]
		[StiCategory("Parameters")]
		[StiOrder(StiPropertyOrder.StyleAllowUseFont)]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use Font formatting or not.")]
		public bool AllowUseFont { get; set; } = true;


	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use Border formatting or not. 
		/// </summary>
		[StiNonSerialized]
		[StiCategory("Parameters")]
		[StiOrder(StiPropertyOrder.StyleAllowUseBorder)]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use Border formatting or not.")]
        [Browsable(false)]
        [Obsolete("AllowUserBorder property is obsolete. Please use AllowUseBorderFormatting and AllowUseBorderSides properties instead it.")]
		public bool AllowUseBorder
		{
			get
			{
				return AllowUseBorderFormatting & AllowUseBorderSides;
			}
			set
			{
				this.AllowUseBorderFormatting = value;
                this.AllowUseBorderSides = value;
			}
		}

	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use Border formatting or not. 
        /// </summary>
        [StiSerializable]
        [StiCategory("Parameters")]
        [StiOrder(StiPropertyOrder.StyleAllowUseBorder)]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use Border formatting or not.")]
        public bool AllowUseBorderFormatting { get; set; } = true;

	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use Border Sides or not. 
        /// </summary>
        [StiSerializable]
        [StiCategory("Parameters")]
        [StiOrder(StiPropertyOrder.StyleAllowUseBorderSides)]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use Border Sides or not.")]
        public bool AllowUseBorderSides { get; set; } = true;
        
	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can set border sides of a component depending on the component location.
        /// </summary>
        [StiSerializable]
        [StiCategory("Parameters")]
        [StiOrder(StiPropertyOrder.StyleAllowUseborderSidesFromLocation)]
        [DefaultValue(false)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can set border sides of a component depending on the component location.")]
        public bool AllowUseBorderSidesFromLocation { get; set; }
        
	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use Brush formatting or not. 
		/// </summary>
		[StiSerializable]
		[StiCategory("Parameters")]
		[StiOrder(StiPropertyOrder.StyleAllowUseBrush)]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use Brush formatting or not.")]
		public bool AllowUseBrush { get; set; } = true;
        
	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use TextBrush formatting or not. 
		/// </summary>
		[StiSerializable]
		[StiCategory("Parameters")]
		[StiOrder(StiPropertyOrder.StyleAllowUseTextBrush)]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use TextBrush formatting or not.")]
		public bool AllowUseTextBrush { get; set; } = true;
	    #endregion

        #region Methods.Style
		public override void DrawStyle(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            var borderRect = rect;
            if (this.AllowUseBrush)
            {
                using (Brush brush = StiBrush.GetBrush(this.Brush, rect))
                {
                    g.FillRectangle(brush, rect);
                }
            }
            else
            {
                rect.Width++;
                rect.Height++;
                g.FillRectangle(Brushes.White, rect);
                rect.Width--;
                rect.Height--;
            }

            if (!paintValue)
            {
                #region Draw Image
                if (paintImage)
                {
                    DrawStyleImage(g, rect);
                    rect.Width -= 50;
                    rect.X += 50;
                }
                #endregion

                #region Draw Name of Style
                using (var sf = new StringFormat())
                using (var textBrush = StiBrush.GetBrush(this.TextBrush, rect))
                {
                    sf.Trimming = StringTrimming.EllipsisCharacter;
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Center;
                    sf.FormatFlags = StringFormatFlags.NoWrap;
                    sf.Trimming = StringTrimming.None;

                    #region AllowUseHorAlignment
                    if (this.AllowUseHorAlignment)
                    {
                        switch (this.HorAlignment)
                        {
                            case StiTextHorAlignment.Left:
                                sf.Alignment = StringAlignment.Near;
                                break;

                            case StiTextHorAlignment.Right:
                                sf.Alignment = StringAlignment.Far;
                                break;

                            default:
                                sf.Alignment = StringAlignment.Center;
                                break;
                        }
                    }
                    #endregion

                    #region AllowUseVertAlignment
                    if (this.AllowUseVertAlignment)
                    {
                        switch (this.VertAlignment)
                        {
                            case StiVertAlignment.Top:
                                sf.LineAlignment = StringAlignment.Near;
                                break;

                            case StiVertAlignment.Bottom:
                                sf.LineAlignment = StringAlignment.Far;
                                break;

                            default:
                                sf.LineAlignment = StringAlignment.Center;
                                break;
                        }
                    }
                    #endregion

                    var br = textBrush;
                    if (!this.AllowUseTextBrush)
                        br = Brushes.Black;

					var oldMode = g.SmoothingMode;
					g.SmoothingMode = SmoothingMode.AntiAlias;
                    
                    string str = this.Name;
                    if ((!string.IsNullOrEmpty(this.Description)) && this.Description != this.Name)
                        str = this.Description;

                    var scale = WindowsScaleInternal == null ? 1 : WindowsScaleInternal.GetValueOrDefault();

                    if (this.AllowUseFont)
                    {
                        using (var font = new Font(this.Font.Name, (float)(this.Font.Size * scale), this.Font.Style))
                        {
                            var textRect = rect;
                            textRect.Inflate(-2, 0);
                            var textSize = g.MeasureString(str, font);
                            if (textRect.Width < textSize.Width)
                            {
                                sf.Alignment = StringAlignment.Near;
                            }

                            g.DrawString(str, font, br, textRect, sf);
                        }
                    }
                    else
                    {
                        using (Font font = new Font("Arial", (float)(10 * scale)))
                        {
                            g.DrawString(str, font, br, rect, sf);
                        }
                    }
					g.SmoothingMode = oldMode;
                    	
                }
                #endregion
            }

            if (this.Border != null && this.AllowUseBorderFormatting || this.AllowUseBorderSides)
                this.Border.Draw(g, borderRect, 1, Color.White, this.AllowUseBorderFormatting, this.AllowUseBorderSides);
        }

        public override void DrawStyleImage(Graphics g, Rectangle rect)
        {
            Rectangle imageRect;
            Image image = null;

            if (rect.Height < 32)
            {
                image = global::Stimulsoft.Report.Images.StylesResource.Style2013;
                imageRect = new Rectangle(rect.X + 10, rect.Y + (rect.Height - 16) / 2, 16, 16);
            }
            else
            {
                image = global::Stimulsoft.Report.Images.StylesResource.Style2013_32;
                imageRect = new Rectangle(rect.X + 10, rect.Y + (rect.Height - 32) / 2, 32, 32);
            }

            g.DrawImage(image, new Point(imageRect.X, imageRect.Y));
        }

        public override void DrawBox(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            rect.X++;
            rect.Y++;
            rect.Width -= 2;
            rect.Height -= 3;

            this.DrawStyle(g, rect, paintValue, paintImage);

            if (this.Border != null && this.Border.Side == StiBorderSides.None && (!paintValue))
            {
                using (Pen pen = new Pen(Color.FromArgb(150, Color.Gray)))
                {
                    pen.DashStyle = DashStyle.Dash;
                    g.DrawRectangle(pen, rect);
                }
            }

            if (rect.Width < 20)
            {
                rect.X += 4;
                rect.Y += 3;
                rect.Width -= 7;
                rect.Height -= 5;

                using (Brush brush = StiBrush.GetBrush(this.TextBrush, rect))
                {
                    g.FillRectangle(brush, rect);
                }
            }
        }

        /// <summary>
        /// Gets a style from the component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void GetStyleFromComponent(StiComponent component, StiStyleElements styleElements)
        {
            GetStyleFromComponent(component, styleElements, null);
        }

        /// <summary>
        /// Gets a style from the component.
        /// </summary>
        /// <param name="component">Component</param>
        /// <param name="styleElements">Elements of style</param>
        /// <param name="componentStyle">Odd/Even/Component style of component, if present</param>
        internal void GetStyleFromComponent(StiComponent component, StiStyleElements styleElements, StiBaseStyle componentStyle)
        {
            var compStyle = componentStyle as StiStyle;
            this.AllowUseBorderFormatting = false;
            this.AllowUseBorderSides = false;
            this.AllowUseBorderSidesFromLocation = false;
            this.AllowUseBrush = false;
            this.AllowUseTextBrush = false;
            this.AllowUseFont = false;
            this.AllowUseHorAlignment = false;
            this.AllowUseVertAlignment = false;

            bool useComponentStyleProperty = !StiOptions.Engine.UseParentStylesOldMode;
            if (compStyle == null) useComponentStyleProperty = false;

            #region IStiFont
            if ((styleElements & StiStyleElements.Font) > 0)
            {
                if (component is IStiFont)
                {
                    var cmp = component as IStiFont;
                    this.Font = cmp.Font.Clone() as Font;

                    this.AllowUseFont = true;
                }
                else if (useComponentStyleProperty && compStyle.AllowUseFont)
                {
                    this.Font = compStyle.Font.Clone() as Font;
                    this.AllowUseFont = true;
                }
            }
            #endregion

            #region IStiBorder
            if ((styleElements & StiStyleElements.Border) > 0)
            {
                if (component is IStiBorder)
                {
                    var cmp = component as IStiBorder;
                    this.Border = cmp.Border.Clone() as StiBorder;

                    this.AllowUseBorderFormatting = true;
                    this.AllowUseBorderSides = true;
                }
                else if (useComponentStyleProperty && (compStyle.AllowUseBorderSides || compStyle.AllowUseBorderFormatting))
                {
                    this.Border = compStyle.Border.Clone() as StiBorder;
                    this.AllowUseBorderFormatting = true;
                    this.AllowUseBorderSides = true;
                }
            }
            #endregion

            #region IStiBrush
            if ((styleElements & StiStyleElements.Brush) > 0)
            {
                if (component is IStiBrush)
                {
                    var cmp = component as IStiBrush;
                    this.Brush = cmp.Brush.Clone() as StiBrush;

                    this.AllowUseBrush = true;
                }
                else if (useComponentStyleProperty && compStyle.AllowUseBrush)
                {
                    this.Brush = compStyle.Brush.Clone() as StiBrush;
                    this.AllowUseBrush = true;
                }
            }
            #endregion

            #region IStiTextBrush
            if ((styleElements & StiStyleElements.TextBrush) > 0)
            {
                if (component is IStiTextBrush)
                {
                    var cmp = component as IStiTextBrush;
                    this.TextBrush = cmp.TextBrush.Clone() as StiBrush;

                    this.AllowUseTextBrush = true;
                }
                else if (useComponentStyleProperty && compStyle.AllowUseTextBrush)
                {
                    this.TextBrush = compStyle.TextBrush.Clone() as StiBrush;
                    this.AllowUseTextBrush = true;
                }
            }
            #endregion

            #region IStiBackColor
            if (component is IStiBackColor && ((styleElements & StiStyleElements.Brush) > 0))
            {
                var cmp = component as IStiBackColor;
                this.Brush = new StiSolidBrush(cmp.BackColor);

                this.AllowUseBrush = true;
            }
            #endregion

            #region IStiForeColor
            if (component is IStiForeColor && ((styleElements & StiStyleElements.TextBrush) > 0))
            {
                var cmp = component as IStiForeColor;
                this.TextBrush = new StiSolidBrush(cmp.ForeColor);

                this.AllowUseTextBrush = true;
            }
            #endregion

            #region IStiTextHorAlignment
            if ((styleElements & StiStyleElements.HorAlignment) > 0)
            {
                if (component is IStiTextHorAlignment)
                {
                    var cmp = component as IStiTextHorAlignment;
                    this.HorAlignment = cmp.HorAlignment;

                    this.AllowUseHorAlignment = true;
                }
                else if (useComponentStyleProperty && compStyle.AllowUseHorAlignment)
                {
                    this.HorAlignment = compStyle.HorAlignment;
                    this.AllowUseHorAlignment = true;
                }
            }
            #endregion

            #region IStiHorAlignment
            if (component is IStiHorAlignment && ((styleElements & StiStyleElements.HorAlignment) > 0))
            {
                var cmp = component as IStiHorAlignment;
                switch (cmp.HorAlignment)
                {
                    case StiHorAlignment.Center:
                        this.HorAlignment = StiTextHorAlignment.Center;
                        break;

                    case StiHorAlignment.Left:
                        this.HorAlignment = StiTextHorAlignment.Left;
                        break;

                    case StiHorAlignment.Right:
                        this.HorAlignment = StiTextHorAlignment.Right;
                        break;
                }

                this.AllowUseHorAlignment = true;
            }
            #endregion

            #region IStiVertAlignment
            if ((styleElements & StiStyleElements.VertAlignment) > 0)
            {
                if (component is IStiVertAlignment)
                {
                    var cmp = component as IStiVertAlignment;
                    this.VertAlignment = cmp.VertAlignment;
                    this.AllowUseVertAlignment = true;
                }
                else if (useComponentStyleProperty && compStyle.AllowUseVertAlignment)
                {
                    this.VertAlignment = compStyle.VertAlignment;
                    this.AllowUseVertAlignment = true;
                }
            }
            #endregion

            #region Primitives
            var primitive = component as StiLinePrimitive;
            if (primitive != null)
            {
                this.Border = new StiBorder(this.Border.Side, primitive.Color, primitive.Size, primitive.Style);

                this.AllowUseBorderFormatting = true;
                this.AllowUseBorderSides = true;
            }
            #endregion
        }

        /// <summary>
        /// Sets style to a component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void SetStyleToComponent(StiComponent component)
        {
            if (component is Stimulsoft.Report.Chart.StiChart) return;
            if (component is Stimulsoft.Report.CrossTab.StiCrossTab) return;
            
            var primitive = component as StiLinePrimitive;
            var shape = component as StiShape;
            #region IStiFont
            if (component is IStiFont && this.AllowUseFont)
            {
                var cmp = component as IStiFont;
                cmp.Font = this.Font.Clone() as Font;
            }
            #endregion

            #region IStiBorder
            if (component is IStiBorder && (this.AllowUseBorderFormatting || this.AllowUseBorderSides) && (primitive == null))
            {
                var cmp = component as IStiBorder;

                var sides = cmp.Border.Side;

                if (this.AllowUseBorderFormatting)
                {
                    cmp.Border = this.Border.Clone() as StiBorder;
                    cmp.Border.Side = sides;
                }

                if (this.AllowUseBorderSides)
                {
                    if (this.AllowUseBorderSidesFromLocation && component.Parent != null)
                        cmp.Border.Side = StiStylesHelper.GetBorderSidesFromLocation(component);
                    else
                        cmp.Border.Side = this.Border.Side;
                }
            }
            #endregion

            #region IStiBrush
            if (component is IStiBrush && this.AllowUseBrush)
            {
                var cmp = component as IStiBrush;
                cmp.Brush = this.Brush.Clone() as StiBrush;
            }
            #endregion

            #region IStiTextBrush
            if (component is IStiTextBrush && this.AllowUseTextBrush)
            {
                var cmp = component as IStiTextBrush;
                cmp.TextBrush = this.TextBrush.Clone() as StiBrush;
            }
            #endregion

            #region IStiBackColor
            if (component is IStiBackColor && this.AllowUseBrush)
            {
                var cmp = component as IStiBackColor;
                cmp.BackColor = StiBrush.ToColor(this.Brush);
            }
            #endregion

            #region IStiForeColor
            if (component is IStiForeColor && this.AllowUseTextBrush)
            {
                var cmp = component as IStiForeColor;
                cmp.ForeColor = StiBrush.ToColor(this.TextBrush);
            }
            #endregion

            #region IStiTextHorAlignment
            if (component is IStiTextHorAlignment && this.AllowUseHorAlignment)
            {
                var cmp = component as IStiTextHorAlignment;
                cmp.HorAlignment = this.HorAlignment;
            }
            #endregion

            #region IStiHorAlignment
            if (component is IStiHorAlignment && this.AllowUseHorAlignment)
            {
                var cmp = component as IStiHorAlignment;
                switch (this.HorAlignment)
                {
                    case StiTextHorAlignment.Center:
                        cmp.HorAlignment = StiHorAlignment.Center;
                        break;

                    case StiTextHorAlignment.Left:
                        cmp.HorAlignment = StiHorAlignment.Left;
                        break;

                    case StiTextHorAlignment.Right:
                        cmp.HorAlignment = StiHorAlignment.Right;
                        break;
                }
            }
            #endregion

            #region IStiVertAlignment
            if (component is IStiVertAlignment && this.AllowUseVertAlignment)
            {
                var cmp = component as IStiVertAlignment;
                cmp.VertAlignment = this.VertAlignment;
            }
            #endregion

			#region Image
			if (component is StiImage && this.AllowUseImage)
			{
				var cmp = component as StiImage;
				cmp.PutImage(this.Image);
			}
			#endregion

            #region Primitives
            if (primitive != null && this.AllowUseBorderFormatting)
            {
                primitive.Color = this.Border.Color;
                primitive.Size = (float)this.Border.Size;
                primitive.Style = this.Border.Style;
            }
            #endregion

            #region Shape
            if (shape != null && this.AllowUseBorderFormatting)
            {
                shape.BorderColor = this.Border.Color;
                shape.Size = (float)this.Border.Size;
                shape.Style = this.Border.Style;
            }
            #endregion
        }
        #endregion

        #region Methods
        /// <summary>
        /// Saves the style in the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the style.</param>
        public override void Save(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Serialize(this, stream, "StiStyle");
        }

        /// <summary>
        /// Loads the style from the stream.
        /// </summary>
        /// <param name="stream">Stream for loading the style.</param>
        public override void Load(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Deserialize(this, stream, "StiStyle");
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use TextOptions formatting or not. 
        /// </summary>
        [Obsolete("Please do not use this property")]
        [Browsable(false)]
        [StiBrowsable(false)]
        [StiNonSerialized]
        public bool AllowUseTextOptions
        {
            get
            {
                return false;
            }
            set
            {
                
            }
        }

	    /// <summary>
        /// Gets or sets a value which indicates on which band in a report this style can be apply when applying style automatically. StyleCode property is obsolete. Do not use it!
        /// </summary>
        [DefaultValue(StiStyleCode.None)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates on which band in a report this style can be apply when applying style automatically. StyleCode property is obsolete. Do not use it!")]
        [Browsable(false)]
        [Obsolete("StyleCode property is obsolete. Do not use it!")]
        public StiStyleCode StyleCode { get; set; } = StiStyleCode.None;

	    /// <summary>
        /// Gets or sets an image to fill the Image property of the Image component.
        /// </summary>
        [Browsable(true)]
        [StiSerializable(StiSerializeTypes.SerializeToCode | StiSerializeTypes.SerializeToDesigner | StiSerializeTypes.SerializeToSaveLoad)]
        [StiCategory("Appearance")]
        [StiOrder(StiPropertyOrder.StyleImage)]
        [Description("Gets or sets an image to fill the Image property of the Image component.")]
        [Editor("Stimulsoft.Report.Components.Design.StiSimpleImageEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [TypeConverter(typeof(Stimulsoft.Report.Components.Design.StiSimpeImageConverter))]
        public Image Image { get; set; }
	    #endregion

        #region Constructors
        /// <summary>
		/// Creates a new object of the type StiStyle.
		/// </summary>
		/// <param name="name">Style name.</param>
		/// <param name="description">Style description.</param>
		internal StiStyle(string name, string description, StiReport report) : base(name, description, report)
		{
		}

		/// <summary>
		/// Creates a new object of the type StiStyle.
		/// </summary>
		/// <param name="name">Style name.</param>
		/// <param name="description">Style description.</param>
		public StiStyle(string name, string description) : this(name, description, null)
		{
		}

		/// <summary>
		/// Creates a new object of the type StiStyle.
		/// </summary>
		/// <param name="name">Style name.</param>
		public StiStyle(string name) : this(name, "")
		{
		}

		/// <summary>
		/// Creates a new object of the type StiStyle.
		/// </summary>
		public StiStyle() : this("")
		{
		}
		#endregion		
	}
}