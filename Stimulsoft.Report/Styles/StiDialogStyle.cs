#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Drawing.Design;
using System.IO;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dialogs;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report
{
	/// <summary>
    /// Describes the class that contains a style for dialog controls.
	/// </summary>	
	public class StiDialogStyle : StiBaseStyle
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiDialogStyle
            jObject.AddPropertyStringNullOrEmpty("Font", StiJsonReportObjectHelper.Serialize.FontDefault(Font));
            jObject.AddPropertyStringNullOrEmpty("ForeColor", StiJsonReportObjectHelper.Serialize.JColor(ForeColor, Color.Black));
            jObject.AddPropertyStringNullOrEmpty("BackColor", StiJsonReportObjectHelper.Serialize.JColor(BackColor, Color.White));
            jObject.AddPropertyBool("AllowUseFont", AllowUseFont, true);
            jObject.AddPropertyBool("AllowUseBackColor", AllowUseBackColor, true);
            jObject.AddPropertyBool("AllowUseForeColor", AllowUseForeColor, true);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Font":
                        this.Font = StiJsonReportObjectHelper.Deserialize.Font(property, this.Font);
                        break;

                    case "ForeColor":
                        this.ForeColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "BackColor":
                        this.BackColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "AllowUseFont":
                        this.AllowUseFont = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseBackColor":
                        this.AllowUseBackColor = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseForeColor":
                        this.AllowUseForeColor = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiDialogStyle;

	    public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            // MainCategory
            var list = new[] 
            { 
                propHelper.StyleName(), 
                propHelper.Description(),
                propHelper.StylesCollectionName(),
                propHelper.StyleConditions() 
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            // AppearanceCategory
            list = new[] 
            { 
                propHelper.Font() 
            };
            objHelper.Add(StiPropertyCategories.Appearance, list);

            // ParametersCategory
            list = new[] 
            { 
                propHelper.BackColor(),
                propHelper.ForeColor(),
                propHelper.AllowUseBackColor(), 
                propHelper.AllowUseFont(), 
                propHelper.AllowUseForeColor() 
            };
            objHelper.Add(StiPropertyCategories.Parameters, list);

            return objHelper;
        }
        #endregion

		#region ICloneable
		public override object Clone()
		{
            var style = base.Clone() as StiDialogStyle;
			style.Font = this.Font.Clone() as Font;
			return style;
		}
		#endregion

		#region IStiFont
	    /// <summary>
        /// Gets or sets the font for drawing this style.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.StyleFont)]
		[StiSerializable]
        [Description("Gets or sets the font for drawing this style.")]
		public Font Font { get; set; } = new Font("Arial",8);
	    #endregion

        #region Properties
	    /// <summary>
        /// Gets or sets a foreground color for drawing this style. 
		/// </summary>
		[StiCategory("Parameters")]
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets a foreground color for drawing this style.")]
        public Color ForeColor { get; set; } = Color.Black;

	    /// <summary>
        /// Gets or sets a background color for drawing this style. 
		/// </summary>
		[StiCategory("Parameters")]
		[StiSerializable]
		[TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
		[Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets a background color for drawing this style.")]
        public Color BackColor { get; set; } = Color.White;
	    #endregion

		#region Properties.Allow
	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use Font for dialog controls. 
		/// </summary>
		[StiSerializable]
		[StiCategory("Parameters")]
		[StiOrder(StiPropertyOrder.StyleAllowUseFont)]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use Font for dialog controls.")]
		public bool AllowUseFont { get; set; } = true;

	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use BackColor for dialog controls. 
		/// </summary>
		[StiSerializable]
		[StiCategory("Parameters")]
		[StiOrder(StiPropertyOrder.StyleAllowUseBrush)]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use BackColor for dialog controls.")]
        public bool AllowUseBackColor { get; set; } = true;

	    /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use ForeColor for dialog controls.
		/// </summary>
		[StiSerializable]
		[StiCategory("Parameters")]
		[StiOrder(StiPropertyOrder.StyleAllowUseTextBrush)]
		[DefaultValue(true)]
		[TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use ForeColor for dialog controls.")]
        public bool AllowUseForeColor { get; set; } = true;
	    #endregion

        #region Methods.Style
        public override void DrawStyle(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {

            var rectElement = new Rectangle(rect.X + 5, rect.Y + 5, 40, 30);
            var rectButton = new Rectangle(
                    rectElement.X,
                    rectElement.Y + rectElement.Height / 4,
                    rectElement.Width,
                    rectElement.Height / 2);

            if (this.AllowUseBackColor && this.BackColor != Color.Transparent)
            {
                using (var brush = new SolidBrush(this.BackColor))
                {
                    g.FillRectangle(brush, rectButton);
                }
            }

            g.DrawRectangle(Pens.LightGray, rectButton);

            using (var br = new SolidBrush(this.ForeColor))
            using (var font = new Font("Arial", 8))
            using (var sf = new StringFormat())
            {
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;                

                g.DrawString("OK", font, br, rectButton, sf);
            }

            #region Draw Text
            rect.X += rectElement.Right + 5;
            rect.Width -= rectElement.Width - 10;

            using (var br = new SolidBrush(Color.Black))
            using (var font = new Font("Arial", 10))
            using (var sf = new StringFormat())
            {
                sf.Alignment = StringAlignment.Near;
                sf.LineAlignment = StringAlignment.Center;

                var textRect = rect;
                textRect.Inflate(-2, 0);
                var textSize = g.MeasureString(this.Name, font);

                if (textRect.Width < textSize.Width)
                    sf.Alignment = StringAlignment.Near;

                g.DrawString(this.Name, font, br, rect, sf);
            }
            #endregion            
        }

		public override void DrawBox(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            rect.X++;
            rect.Y++;
            rect.Width -= 2;
            rect.Height -= 3;

            this.DrawStyle(g, rect, paintValue, paintImage);

            using (Pen pen = new Pen(Color.Gray))
            {
                pen.DashStyle = DashStyle.Dash;
                g.DrawRectangle(pen, rect);
            }

            if (rect.Width < 20)
            {
                rect.X += 4;
                rect.Y += 3;
                rect.Width -= 7;
                rect.Height -= 5;

                using (Brush brush = new SolidBrush(this.ForeColor))
                {
                    g.FillRectangle(brush, rect);
                }
            }
        }

        /// <summary>
        /// Gets a style from the component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void GetStyleFromComponent(StiComponent component, StiStyleElements styleElements)
        {
            GetStyleFromComponent(component, styleElements, null);
        }

        /// <summary>
        /// Gets a style from the component.
        /// </summary>
        /// <param name="component">Component</param>
        /// <param name="styleElements">Elements of style</param>
        /// <param name="componentStyle">Odd/Even/Component style of component, if present</param>
        internal void GetStyleFromComponent(StiComponent component, StiStyleElements styleElements, StiBaseStyle componentStyle)
        {
            if (!(component is Stimulsoft.Report.Dialogs.StiReportControl || component is Stimulsoft.Report.Dialogs.StiForm)) return;

            var compStyle = componentStyle as StiDialogStyle;
            if (compStyle != null) return;

            var form = component as StiForm;
            var control = component as StiReportControl;

            #region IStiFont
            if ((styleElements & StiStyleElements.Font) > 0)
            {
                if (form != null)
                    this.Font = form.Font.Clone() as Font;
                else
                    this.Font = control.Font.Clone() as Font;

                this.AllowUseFont = true;
            }
            #endregion

            #region IStiBackColor
            if ((styleElements & StiStyleElements.Brush) > 0)
            {
                this.BackColor = form != null ? form.BackColor : control.BackColor;
                this.AllowUseBackColor = true;
            }
            #endregion

            #region IStiForeColor
            if ((styleElements & StiStyleElements.TextBrush) > 0)
                this.AllowUseForeColor = true;
            #endregion

        }

        /// <summary>
        /// Sets style to a component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void SetStyleToComponent(StiComponent component)
        {
            var control = component as StiReportControl;
            if (control == null)return;

            if (!StiStyleConditionHelper.IsAllowStyle(component, this))
                return;

            #region IStiFont
            if (component is IStiFont && this.AllowUseFont)
                control.Font = this.Font.Clone() as Font;
            #endregion

            #region IStiBackColor
            if (this.AllowUseBackColor)
                control.BackColor = this.BackColor;
            #endregion

            #region IStiForeColor
            if (this.AllowUseForeColor)
                control.ForeColor = this.ForeColor;
            #endregion
        }
        #endregion

        #region Methods
        /// <summary>
        /// Saves the style in the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the style.</param>
        public override void Save(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Serialize(this, stream, "StiDialogStyle");
        }

        /// <summary>
        /// Loads the style from the stream.
        /// </summary>
        /// <param name="stream">Stream for loading the style.</param>
        public override void Load(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Deserialize(this, stream, "StiDialogStyle");
        }
        #endregion

        #region Constructors
        /// <summary>
		/// Creates a new object of the type StiStyle.
		/// </summary>
		/// <param name="name">Style name.</param>
		/// <param name="description">Style description.</param>
		internal StiDialogStyle(string name, string description, StiReport report) : base(name, description, report)
		{
		}

		/// <summary>
		/// Creates a new object of the type StiStyle.
		/// </summary>
		/// <param name="name">Style name.</param>
		/// <param name="description">Style description.</param>
		public StiDialogStyle(string name, string description) : this(name, description, null)
		{
		}

		/// <summary>
		/// Creates a new object of the type StiStyle.
		/// </summary>
		/// <param name="name">Style name.</param>
		public StiDialogStyle(string name) : this(name, "")
		{
		}

		/// <summary>
		/// Creates a new object of the type StiStyle.
		/// </summary>
        public StiDialogStyle()
            : this("")
		{
		}
		#endregion		
	}
}
