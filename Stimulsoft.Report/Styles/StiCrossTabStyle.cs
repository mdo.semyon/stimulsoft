#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Drawing.Design;
using System.IO;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report
{
	/// <summary>
	/// Describes the class that contains a style for CrossTab components.
	/// </summary>	
	public class StiCrossTabStyle : StiBaseStyle
	{
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiCrossTabStyle
            jObject.AddPropertyStringNullOrEmpty("Color", StiJsonReportObjectHelper.Serialize.JColor(Color, Color.White));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Color":
                        this.Color = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiCrossTabStyle;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[]
            {
                propHelper.StyleName(),
                propHelper.Description(),
                propHelper.StyleCollectionName(),
                propHelper.StyleConditions()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            list = new[]
            {
                propHelper.Color(),
            };
            objHelper.Add(StiPropertyCategories.Appearance, list);

            return objHelper;
        }
        #endregion

        #region Properties
	    /// <summary>
		/// Gets or sets a color of style.
		/// </summary>
		[StiCategory("Appearance")]
		[StiOrder(StiPropertyOrder.StyleColor)]
		[StiSerializable]
        [Description("Gets or sets a color of style.")]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        public Color Color { get; set; } = Color.White;
	    #endregion

        #region Methods.Style
        public void DrawStyleForGallery(Graphics g, Rectangle rect)
        {
            var rectElement = new Rectangle(rect.X + 5, rect.Y + 5, rect.Width - 10, rect.Height - 10);

            int cellSize = rectElement.Height / 5;
            if (cellSize * 5 > rectElement.Height)
                cellSize--;
            rectElement.Height = cellSize * 5;

            int cols = rectElement.Width / cellSize;
            rectElement.Width = cellSize * cols;

            g.FillRectangle(Brushes.White, new Rectangle(rectElement.X + cellSize, rectElement.Y + cellSize, rectElement.Width - cellSize, rectElement.Height - cellSize));
            using (var brush = new SolidBrush((this.Color == Color.White) ? Color.FromArgb(255, 236, 236, 236) : this.Color))
            {
                g.FillRectangle(brush, new Rectangle(rectElement.X + cellSize, rectElement.Y, rectElement.Width - cellSize, cellSize));
                g.FillRectangle(brush, new Rectangle(rectElement.X, rectElement.Y + cellSize, cellSize, rectElement.Height - cellSize));
            }

            for (int index = 0; index < 6; index++)
            {
                if (index == 0)
                    g.DrawLine(Pens.LightGray, rectElement.X + cellSize, rectElement.Y + cellSize * index, rectElement.Right, rectElement.Y + cellSize * index);

                else
                    g.DrawLine(Pens.LightGray, rectElement.X, rectElement.Y + cellSize * index, rectElement.Right, rectElement.Y + cellSize * index);
            }
            for (int index = 0; index <= cols; index++)
            {
                if (index == 0)
                    g.DrawLine(Pens.LightGray, rectElement.X + cellSize * index, rectElement.Y + cellSize, rectElement.X + cellSize * index, rectElement.Bottom);

                else
                    g.DrawLine(Pens.LightGray, rectElement.X + cellSize * index, rectElement.Y, rectElement.X + cellSize * index, rectElement.Bottom);
            }
        }

        public override void DrawStyle(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            Rectangle rectElement;
            if (rect.Width == 80 && rect.Height == 80)
                rectElement = new Rectangle(rect.X + 9, rect.Y + 10, 61, 60);

            else if (rect.Width == 18 && rect.Height == 10)
                rectElement = rect;

            else
                rectElement = new Rectangle(rect.X + 5, rect.Y + 5, 41, 30);

            if (paintImage)
            {
                int widthStep = rectElement.Width / 6;
                int heightStep = rectElement.Height / 6;

                g.FillRectangle(Brushes.White, new Rectangle(rectElement.X + widthStep, rectElement.Y + heightStep, rectElement.Width - widthStep, rectElement.Height - heightStep));
                using (var brush = new SolidBrush((this.Color == Color.White) ? Color.FromArgb(255, 236, 236, 236) : this.Color))
                {
                    g.FillRectangle(brush, new Rectangle(rectElement.X + widthStep, rectElement.Y, rectElement.Width - widthStep, heightStep));
                    g.FillRectangle(new SolidBrush(this.Color), new Rectangle(rectElement.X, rectElement.Y + heightStep, widthStep, rectElement.Height - heightStep));
                }

                for (int index = 0; index < 7; index++)
                {
                    if (index == 0)
                        g.DrawLine(Pens.LightGray, rectElement.X + widthStep * index, rectElement.Y + widthStep, rectElement.X + widthStep * index, rectElement.Y + rectElement.Height);

                    else if (index == 6)
                        g.DrawLine(Pens.LightGray, rectElement.X + widthStep * index + 1, rectElement.Y, rectElement.X + widthStep * index + 1, rectElement.Y + rectElement.Height);

                    else
                        g.DrawLine(Pens.LightGray, rectElement.X + widthStep * index, rectElement.Y, rectElement.X + widthStep * index, rectElement.Y + rectElement.Height);
                }

                for (int index = 0; index < 7; index++)
                {
                    if (index == 0)
                        g.DrawLine(Pens.LightGray, rectElement.X + heightStep, rectElement.Y + heightStep * index, rectElement.X + rectElement.Width, rectElement.Y + heightStep * index);

                    else
                        g.DrawLine(Pens.LightGray, rectElement.X, rectElement.Y + heightStep * index, rectElement.X + rectElement.Width, rectElement.Y + heightStep * index);
                }

                g.DrawLine(Pens.LightGray, rectElement.Right, rectElement.Y, rectElement.Right, rectElement.Y + rectElement.Height);
            }

            #region Draw Text
            if (!paintValue)
            {
                rect.X += rectElement.Right + 14;
                rect.Width -= rectElement.Width - 10;

                using (var br = new SolidBrush(Color.Black))
                using (var font = new Font("Arial", 10))
                using (var sf = new StringFormat())
                {
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Center;

                    var textRect = rect;
                    textRect.Inflate(-2, 0);
                    var textSize = g.MeasureString(this.Name, font);
                    if (textRect.Width < textSize.Width)
                        sf.Alignment = StringAlignment.Near;

                    g.DrawString(this.Name, font, br, rect, sf);
                }
            }
            #endregion
        }

        public override void DrawBox(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            rect.X++;
            rect.Y++;
            rect.Width -= 2;
            rect.Height -= 3;

            this.DrawStyle(g, rect, paintValue, paintImage);

            using (Pen pen = new Pen(Color.FromArgb(50, Color.Gray)))
            {
                pen.DashStyle = DashStyle.Dash;
                g.DrawRectangle(pen, rect);
            }
        }

        /// <summary>
        /// Gets a style from the component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void GetStyleFromComponent(StiComponent component, StiStyleElements styleElements)
        {
            if (styleElements != StiStyleElements.All)throw new Exception("StiCrossTabStyle support only StiStyleElements.All.");

            var crossTab = component as Stimulsoft.Report.CrossTab.StiCrossTab;
            if (crossTab == null) return;

            string name = crossTab.CrossTabStyle;
            if (!string.IsNullOrEmpty(name) && crossTab.Report != null && crossTab.Report.Styles[name] is StiCrossTabStyle)
            {
                this.Color = ((StiCrossTabStyle)crossTab.Report.Styles[name]).Color;
            }   
            else if (crossTab.CrossTabStyleIndex < StiOptions.Designer.CrossTab.StyleColors.Length)
            {
                if (crossTab.CrossTabStyleIndex >= 0 && crossTab.CrossTabStyleIndex < StiOptions.Designer.CrossTab.StyleColors.Length - 1)
                    this.Color = StiOptions.Designer.CrossTab.StyleColors[crossTab.CrossTabStyleIndex];
            }
        }

        /// <summary>
        /// Sets style to a component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void SetStyleToComponent(StiComponent component)
        {
            var crossTab = component as Stimulsoft.Report.CrossTab.StiCrossTab;
            if (crossTab != null)
            {
                if (!StiStyleConditionHelper.IsAllowStyle(component, this))
                    return;

                #region Color
                crossTab.CrossTabStyleColor = this.Color;
                crossTab.UpdateStyles();
                #endregion
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Saves the style in the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the style.</param>
        public override void Save(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Serialize(this, stream, "StiCrossTabStyle");
        }

        /// <summary>
        /// Loads the style from the stream.
        /// </summary>
        /// <param name="stream">Stream for loading the style.</param>
        public override void Load(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Deserialize(this, stream, "StiCrossTabStyle");
        }
        #endregion

        #region Constructors
        /// <summary>
		/// Creates a new object of the type StiCrossTabStyle.
		/// </summary>
		/// <param name="name">Style name.</param>
		/// <param name="description">Style description.</param>
		internal StiCrossTabStyle(string name, string description, StiReport report) : base(name, description, report)
		{
		}

		/// <summary>
		/// Creates a new object of the type StiCrossTabStyle.
		/// </summary>
		/// <param name="name">Style name.</param>
		/// <param name="description">Style description.</param>
		public StiCrossTabStyle(string name, string description) : this(name, description, null)
		{
		}

		/// <summary>
		/// Creates a new object of the type StiCrossTabStyle.
		/// </summary>
		/// <param name="name">Style name.</param>
		public StiCrossTabStyle(string name) : this(name, "")
		{
		}

		/// <summary>
        /// Creates a new object of the type StiCrossTabStyle.
		/// </summary>
        public StiCrossTabStyle()
            : this("")
		{
		}
		#endregion
	}
}
