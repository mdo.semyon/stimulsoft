﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.PropertyGrid;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace Stimulsoft.Report
{
    /// <summary>
    /// Describes the class that contains a style for Gauge components.
    /// </summary>	
    public class StiGaugeStyle : StiBaseStyle
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyStringNullOrEmpty("BorderColor", StiJsonReportObjectHelper.Serialize.JColor(BorderColor, Color.Transparent));
            jObject.AddPropertyFloat("BorderWidth", BorderWidth, 0f);

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Brush":
                        this.Brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "BorderColor":
                        this.BorderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "BorderWidth":
                        this.BorderWidth = property.Value.ToObject<float>();
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiGaugeStyle;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[]
            {
                propHelper.StyleName(),
                propHelper.Description(),
                propHelper.StyleCollectionName(),
                propHelper.StyleConditions()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            list = new[]
            {
                propHelper.MarkerBrush()
            };
            objHelper.Add(StiPropertyCategories.Market, list);

            list = new[]
            {
                propHelper.BorderWidth(),
                propHelper.BorderColor(),
                propHelper.Brush()
            };
            objHelper.Add(StiPropertyCategories.Misc, list);

            list = new[]
            {
                propHelper.TickMarkMajorBorder(),
                propHelper.TickMarkMajorBrush()
            };
            objHelper.Add(StiPropertyCategories.TickMarkMajor, list);

            list = new[]
            {
                propHelper.TickMarkMinorBorder(),
                propHelper.TickMarkMinorBrush()
            };
            objHelper.Add(StiPropertyCategories.TickMarkMinor, list);

            list = new[]
            {
                propHelper.TickLabelMajorTextBrush(),
                propHelper.TickLabelMajorFont()
            };
            objHelper.Add(StiPropertyCategories.TickLabelMajor, list);

            list = new[]
            {
                propHelper.TickLabelMinorTextBrush(),
                propHelper.TickLabelMinorFont()
            };
            objHelper.Add(StiPropertyCategories.TickLabelMinor, list);

            list = new[]
            {
                propHelper.LinearBarBorderBrush(),
                propHelper.LinearBarBrush(),
                propHelper.LinearBarEmptyBorderBrush(),
                propHelper.LinearBarEmptyBrush()
            };
            objHelper.Add(StiPropertyCategories.LinearScaleBar, list);

            list = new[]
            {
                propHelper.RadialBarBorderBrush(),
                propHelper.RadialBarBrush(),
                propHelper.RadialBarEmptyBorderBrush(),
                propHelper.RadialBarEmptyBrush()
            };
            objHelper.Add(StiPropertyCategories.RadialScaleBar, list);

            list = new[]
            {
                propHelper.NeedleBorderBrush(),
                propHelper.NeedleBrush(),
                propHelper.NeedleCapBorderBrush(),
                propHelper.NeedleCapBrush()
            };
            objHelper.Add(StiPropertyCategories.Needle, list);

            return objHelper;
        }
        #endregion

        #region Properties
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush Brush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#ffffff"));

        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color BorderColor { get; set; } = Color.Transparent;

        [StiSerializable]
        public float BorderWidth { get; set; }
        #endregion

        #region Scale

        #region TickMarkMajor
        [StiCategory("TickMarkMajor")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush TickMarkMajorBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#990000"));

        [StiCategory("TickMarkMajor")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush TickMarkMajorBorder { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#0bac45"));
        #endregion

        #region TickMarkMinor
        [StiCategory("TickMarkMinor")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush TickMarkMinorBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#4472c4"));

        [StiCategory("TickMarkMinor")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush TickMarkMinorBorder { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#4472c4"));
        #endregion

        #region TickLabelMajor
        [StiCategory("TickLabelMajor")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush TickLabelMajorTextBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#FF33475B"));

        [StiCategory("TickLabelMajor")]
        [StiOrder(StiPropertyOrder.StyleFont)]
        [StiSerializable]
        public Font TickLabelMajorFont { get; set; } = new Font("Arial", 10);
        #endregion

        #region TickLabelMinor
        [StiCategory("TickLabelMinor")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush TickLabelMinorTextBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#FF33475B"));

        [StiCategory("TickLabelMinor")]
        [StiOrder(StiPropertyOrder.StyleFont)]
        [StiSerializable]
        public Font TickLabelMinorFont { get; set; } = new Font("Arial", 10);
        #endregion

        #region Marker
        [StiCategory("Marker")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush MarkerBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#70ad47"));
        #endregion

        #region Linear Scale

        #region Bar
        [StiCategory("LinearScaleBar")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush LinearBarBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#4472c4"));

        [StiCategory("LinearScaleBar")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush LinearBarBorderBrush { get; set; } = new StiEmptyBrush();

        [StiCategory("LinearScaleBar")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush LinearBarEmptyBrush { get; set; } = new StiEmptyBrush();

        [StiCategory("LinearScaleBar")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush LinearBarEmptyBorderBrush { get; set; } = new StiEmptyBrush();
        #endregion

        #endregion

        #region Radial Scale

        #region Bar
        [StiCategory("RadialScaleBar")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush RadialBarBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#ffc000"));

        [StiCategory("RadialScaleBar")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush RadialBarBorderBrush { get; set; } = new StiEmptyBrush();

        [StiCategory("RadialScaleBar")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush RadialBarEmptyBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#43682b"));

        [StiCategory("RadialScaleBar")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush RadialBarEmptyBorderBrush { get; set; } = new StiEmptyBrush();
        #endregion

        #region Needle
        [StiCategory("Needle")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush NeedleBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#ffc000"));

        [StiCategory("Needle")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush NeedleBorderBrush { get; set; } = new StiEmptyBrush();

        [StiCategory("Needle")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush NeedleCapBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#ffc000"));

        [StiCategory("Needle")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush NeedleCapBorderBrush { get; set; } = new StiSolidBrush(ColorTranslator.FromHtml("#ffc000"));
        #endregion

        #endregion

        #endregion

        #region Methods.Style
        public override void DrawStyle(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            var rectElement = new Rectangle(rect.X + 5, rect.Y + 5, 40, 30);
            var rectGauge = new Rectangle(
                    rectElement.X + rectElement.Width / 2 - rectElement.Height / 2,
                    rectElement.Y,
                    rectElement.Height,
                    rectElement.Height);

            #region Draw Body Gauge
            using(var brush = StiBrush.GetBrush(this.Brush, rectElement))
                g.FillEllipse(brush, rectGauge);

            g.DrawEllipse(Pens.LightGray, rectGauge);

            var rectCenter = new Rectangle(
                    rectElement.X + rectElement.Width / 2 - 2,
                    rectElement.Y + rectElement.Height / 2 - 2,
                    4, 4);
            #endregion

            #region Draw Needle
            using (var brush = StiBrush.GetBrush(this.NeedleCapBrush, rectCenter))
            using (var pen = new Pen(brush))
                g.DrawEllipse(pen, rectCenter);
            
            g.DrawLine(new Pen(StiBrush.ToColor(this.NeedleBrush)),
                rectGauge.X + rectGauge.Width / 2 + 2,
                rectGauge.Y + rectGauge.Height / 2,
                rectGauge.X + rectGauge.Width + 2,
                rectGauge.Y + rectGauge.Height / 2);
            #endregion

            #region Draw Text
            rect.X += rectElement.Right + 5;
            rect.Width -= rectElement.Width - 10;

            using (var br = new SolidBrush(Color.Black))
            using (var font = new Font("Arial", 10))
            using (var sf = new StringFormat())
            {
                sf.Alignment = StringAlignment.Near;
                sf.LineAlignment = StringAlignment.Center;

                var textRect = rect;
                textRect.Inflate(-2, 0);
                var textSize = g.MeasureString(this.Name, font);
                if (textRect.Width < textSize.Width)
                    sf.Alignment = StringAlignment.Near;

                g.DrawString(this.Name, font, br, rect, sf);
            }
            #endregion
        }

        public override void DrawBox(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            rect.X++;
            rect.Y++;
            rect.Width -= 2;
            rect.Height -= 3;

            this.DrawStyle(g, rect, paintValue, paintImage);

            using (var pen = new Pen(Color.FromArgb(150, Color.Gray)))
            {
                pen.DashStyle = DashStyle.Dash;
                g.DrawRectangle(pen, rect);
            }
        }

        /// <summary>
        /// Gets a style from the component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void GetStyleFromComponent(StiComponent component, StiStyleElements styleElements)
        {
            if (styleElements != StiStyleElements.All)
                throw new Exception("StiGaugeStyle support only StiStyleElements.All.");

            var gauge = component as Gauge.StiGauge;
            if (gauge != null)
            {
                #region IStiBrush
                if ((styleElements & StiStyleElements.Brush) > 0)
                {
                    var cmp = component as IStiBrush;
                    this.Brush = cmp.Brush.Clone() as StiBrush;
                }
                #endregion
            }
        }

        /// <summary>
        /// Sets style to a component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void SetStyleToComponent(StiComponent component)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Saves the style in the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the style.</param>
        public override void Save(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Serialize(this, stream, "StiGaugeStyle");
        }

        /// <summary>
        /// Loads the style from the stream.
        /// </summary>
        /// <param name="stream">Stream for loading the style.</param>
        public override void Load(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Deserialize(this, stream, "StiGaugeStyle");
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new object of the type StiGaugeStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        /// <param name="description">Style description.</param>
        internal StiGaugeStyle(string name, string description, StiReport report)
            : base(name, description, report)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiGaugeStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        /// <param name="description">Style description.</param>
        public StiGaugeStyle(string name, string description)
            : this(name, description, null)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiGaugeStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        public StiGaugeStyle(string name)
            : this(name, "")
        {
        }

        /// <summary>
        /// Creates a new object of the type StiGaugeStyle.
        /// </summary>
        public StiGaugeStyle()
            : this("")
        {
        }
        #endregion
    }
}
