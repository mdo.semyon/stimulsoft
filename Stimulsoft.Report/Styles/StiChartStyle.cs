#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Drawing.Design;
using System.IO;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report
{
    /// <summary>
    /// Describes the class that contains a style for Chart components.
    /// </summary>	
    public class StiChartStyle : StiBaseStyle
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            // StiChartStyle
            jObject.AddPropertyStringNullOrEmpty("Border", StiJsonReportObjectHelper.Serialize.JBorder(Border));
            jObject.AddPropertyStringNullOrEmpty("Brush", StiJsonReportObjectHelper.Serialize.JBrush(Brush));
            jObject.AddPropertyEnum("BrushType", BrushType, StiBrushType.Glare);
            jObject.AddPropertyJObject("StyleColors", StiJsonReportObjectHelper.Serialize.ColorArray(StyleColors));
            jObject.AddPropertyStringNullOrEmpty("BasicStyleColor", StiJsonReportObjectHelper.Serialize.JColor(BasicStyleColor, Color.WhiteSmoke));
            jObject.AddPropertyBool("AllowUseBorderFormatting", AllowUseBorderFormatting, true);
            jObject.AddPropertyBool("AllowUseBorderSides", AllowUseBorderSides, true);
            jObject.AddPropertyBool("AllowUseBrush", AllowUseBrush, true);

            jObject.AddPropertyStringNullOrEmpty("ChartAreaBrush", StiJsonReportObjectHelper.Serialize.JBrush(ChartAreaBrush));
            jObject.AddPropertyStringNullOrEmpty("ChartAreaBorderColor", StiJsonReportObjectHelper.Serialize.JColor(ChartAreaBorderColor, Color.FromArgb(171, 172, 173)));

            jObject.AddPropertyStringNullOrEmpty("SeriesLabelsBrush", StiJsonReportObjectHelper.Serialize.JBrush(SeriesLabelsBrush));
            jObject.AddPropertyStringNullOrEmpty("SeriesLabelsColor", StiJsonReportObjectHelper.Serialize.JColor(SeriesLabelsColor, Color.FromArgb(90, 90, 90)));
            jObject.AddPropertyStringNullOrEmpty("SeriesLabelsBorderColor", StiJsonReportObjectHelper.Serialize.JColor(SeriesLabelsBorderColor, Color.FromArgb(90, 90, 90)));

            jObject.AddPropertyStringNullOrEmpty("LegendBrush", StiJsonReportObjectHelper.Serialize.JBrush(LegendBrush));
            jObject.AddPropertyStringNullOrEmpty("LegendLabelsColor", StiJsonReportObjectHelper.Serialize.JColor(LegendLabelsColor, Color.FromArgb(140, 140, 140)));
            jObject.AddPropertyStringNullOrEmpty("LegendBorderColor", StiJsonReportObjectHelper.Serialize.JColor(LegendBorderColor, Color.FromArgb(105, 105, 105)));
            jObject.AddPropertyStringNullOrEmpty("LegendTitleColor", StiJsonReportObjectHelper.Serialize.JColor(LegendTitleColor, Color.FromArgb(105, 105, 105)));

            jObject.AddPropertyStringNullOrEmpty("AxisTitleColor", StiJsonReportObjectHelper.Serialize.JColor(LegendLabelsColor, Color.FromArgb(140, 140, 140)));
            jObject.AddPropertyStringNullOrEmpty("AxisLineColor", StiJsonReportObjectHelper.Serialize.JColor(LegendLabelsColor, Color.FromArgb(140, 140, 140)));
            jObject.AddPropertyStringNullOrEmpty("AxisLabelsColor", StiJsonReportObjectHelper.Serialize.JColor(LegendLabelsColor, Color.FromArgb(140, 140, 140)));

            jObject.AddPropertyStringNullOrEmpty("InterlacingHorBrush", StiJsonReportObjectHelper.Serialize.JBrush(InterlacingHorBrush));
            jObject.AddPropertyStringNullOrEmpty("InterlacingVertBrush", StiJsonReportObjectHelper.Serialize.JBrush(InterlacingVertBrush));

            jObject.AddPropertyStringNullOrEmpty("GridLinesHorColor", StiJsonReportObjectHelper.Serialize.JColor(GridLinesHorColor, Color.FromArgb(105, 105, 105)));
            jObject.AddPropertyStringNullOrEmpty("GridLinesVertColor", StiJsonReportObjectHelper.Serialize.JColor(GridLinesVertColor, Color.FromArgb(105, 105, 105)));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Border":
                        this.Border = StiJsonReportObjectHelper.Deserialize.Border(property);
                        break;

                    case "Brush":
                        this.Brush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "BrushType":
                        this.BrushType = (StiBrushType) Enum.Parse(typeof(StiBrushType), property.Value.ToObject<string>());
                        break;

                    case "StyleColors":
                        this.StyleColors = StiJsonReportObjectHelper.Deserialize.ColorArray((JObject) property.Value);
                        break;

                    case "BasicStyleColor":
                        this.BasicStyleColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "AllowUseBorderFormatting":
                        this.AllowUseBorderFormatting = property.Value.ToObject<bool>();
                        break;

                    case "AllowUseBorderSides":
                        this.AllowUseBorderSides = property.Value.ToObject<bool>();
                        break;

                    case "ChartAreaBrush":
                        this.ChartAreaBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "ChartAreaBorderColor":
                        this.ChartAreaBorderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "SeriesLabelsBrush":
                        this.SeriesLabelsBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "SeriesLabelsColor":
                        this.SeriesLabelsColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "SeriesLabelsBorderColor":
                        this.SeriesLabelsBorderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "LegendBrush":
                        this.LegendBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "LegendLabelsColor":
                        this.LegendLabelsColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "LegendBorderColor":
                        this.LegendBorderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "LegendTitleColor":
                        this.LegendTitleColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "AxisTitleColor":
                        this.AxisTitleColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "AxisLineColor":
                        this.AxisLineColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "AxisLabelsColor":
                        this.AxisLabelsColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "InterlacingHorBrush":
                        this.InterlacingHorBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "InterlacingVertBrush":
                        this.InterlacingVertBrush = StiJsonReportObjectHelper.Deserialize.Brush(property);
                        break;

                    case "GridLinesHorColor":
                        this.GridLinesHorColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "GridLinesVertColor":
                        this.GridLinesVertColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiChartStyle;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[]
            {
                propHelper.StyleName(),
                propHelper.Description(),
                propHelper.StyleCollectionName(),
                propHelper.StyleConditions()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            list = new[]
            {
                propHelper.Brush(),
                propHelper.Border()
            };
            objHelper.Add(StiPropertyCategories.Appearance, list);

            list = new[]
            {
                propHelper.BasicStyleColor(),
                propHelper.StyleColors(),
                propHelper.AllowUseBorderFormatting(),
                propHelper.AllowUseBorderSides(),
                propHelper.AllowUseBrush(),
                propHelper.BrushType()
            };
            objHelper.Add(StiPropertyCategories.Parameters, list);

            list = new[]
            {
                propHelper.ChartAreaShowShadow(),
                propHelper.ChartAreaBorderColor(),
                propHelper.ChartAreaBrush(),
            };
            objHelper.Add(StiPropertyCategories.Area, list);

            list = new[]
            {
                propHelper.SeriesLighting(),
                propHelper.SeriesShowShadow()
            };
            objHelper.Add(StiPropertyCategories.Series, list);

            list = new[]
            {
                propHelper.SeriesLabelsBorderColor(),
                propHelper.SeriesLabelsColor(),
                propHelper.SeriesLabelsBrush()
            };
            objHelper.Add(StiPropertyCategories.SeriesLabels, list);

            list = new[]
            {
                propHelper.TrendLineShowShadow(),
                propHelper.TrendLineColor(),
            };
            objHelper.Add(StiPropertyCategories.TrendLine, list);

            list = new[]
            {
                propHelper.LegendBorderColor(),
                propHelper.LegendLabelsColor(),
                propHelper.LegendTitleColor(),
                propHelper.LegendBrush(),
            };
            objHelper.Add(StiPropertyCategories.Legend, list);

            list = new[]
            {
                propHelper.AxisLabelsColor(),
                propHelper.AxisLineColor(),
                propHelper.AxisTitleColor(),
            };
            objHelper.Add(StiPropertyCategories.Axis, list);

            list = new[]
            {
                propHelper.InterlacingHorBrush(),
                propHelper.InterlacingVertBrush(),
            };
            objHelper.Add(StiPropertyCategories.Interlacing, list);

            list = new[]
            {
                propHelper.GridLinesHorColor(),
                propHelper.GridLinesVertColor(),
            };
            objHelper.Add(StiPropertyCategories.GridLines, list);

            return objHelper;
        }
        #endregion

        #region IStiBorder
        /// <summary>
        /// Gets or sets a border of the component.
        /// </summary>
        [StiCategory("Appearance")]
        [StiOrder(StiPropertyOrder.StyleBorder)]
        [StiSerializable]
        [Description("Gets or sets a border of the component.")]
        public StiBorder Border { get; set; } = new StiBorder();
        #endregion

        #region IStiBrush
        /// <summary>
        /// Gets or sets a brush to fill the style.
        /// </summary>
        [StiCategory("Appearance")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        [Description("Gets or sets a brush to fill the style.")]
        public StiBrush Brush { get; set; } = new StiSolidBrush(Color.Transparent);
        #endregion

        #region Area
        [StiCategory("Area")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush ChartAreaBrush { get; set; } = new StiSolidBrush(Color.White);

        [StiCategory("Area")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color ChartAreaBorderColor { get; set; } = Color.FromArgb(171, 172, 173);

        [StiCategory("Area")]
        [StiSerializable]
        public bool ChartAreaShowShadow { get; set; }
        #endregion

        #region Series
        [StiCategory("Series")]
        [StiSerializable]
        public bool SeriesLighting { get; set; }

        [StiCategory("Series")]
        [StiSerializable]
        public bool SeriesShowShadow { get; set; }
        #endregion

        #region SeriesLabels
        [StiCategory("SeriesLabels")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush SeriesLabelsBrush { get; set; } = new StiSolidBrush(Color.White);

        [StiCategory("SeriesLabels")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color SeriesLabelsColor { get; set; } = Color.FromArgb(90, 90, 90);

        [StiCategory("SeriesLabels")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color SeriesLabelsBorderColor { get; set; } = Color.FromArgb(140, 140, 140);
        #endregion

        #region Trend Line
        [StiCategory("TrendLine")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color TrendLineColor { get; set; } = Color.FromArgb(140, 140, 140);

        [StiCategory("TrendLine")]
        [StiSerializable]
        public bool TrendLineShowShadow { get; set; }
        #endregion

        #region Legend
        [StiCategory("Legend")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush LegendBrush { get; set; } = new StiSolidBrush(Color.White);

        [StiCategory("Legend")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color LegendLabelsColor { get; set; } = Color.FromArgb(140, 140, 140);

        [StiCategory("Legend")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color LegendBorderColor { get; set; } = Color.FromArgb(105, 105, 105);

        [StiCategory("Legend")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color LegendTitleColor { get; set; } = Color.FromArgb(105, 105, 105);
        #endregion

        #region Axis
        [StiCategory("Axis")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color AxisTitleColor { get; set; } = Color.FromArgb(140, 140, 140);

        [StiCategory("Axis")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color AxisLineColor { get; set; } = Color.FromArgb(140, 140, 140);

        [StiCategory("Axis")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color AxisLabelsColor { get; set; } = Color.FromArgb(140, 140, 140);
        #endregion

        #region Marker
        [StiCategory("Marker")]
        [StiSerializable]
        public bool MarkerVisible { get; set; } = true;
        #endregion

        #region Interlacing
        [StiCategory("Interlacing")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush InterlacingHorBrush { get; set; } = new StiSolidBrush(Color.FromArgb(10, 155, 155, 155));

        [StiCategory("Interlacing")]
        [StiOrder(StiPropertyOrder.StyleBrush)]
        [StiSerializable]
        public StiBrush InterlacingVertBrush { get; set; } = new StiSolidBrush(Color.FromArgb(10, 155, 155, 155));
        #endregion

        #region GridLines
        [StiCategory("GridLines")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color GridLinesHorColor { get; set; } = Color.FromArgb(100, 105, 105, 105);

        [StiCategory("GridLines")]
        [StiOrder(StiPropertyOrder.StyleColor)]
        [StiSerializable]
        public Color GridLinesVertColor { get; set; } = Color.FromArgb(100, 105, 105, 105);
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value which indicates which type of brush report engine will be used to draw this style.
        /// </summary>
        [StiSerializable]
        [StiCategory("Parameters")]
        [StiOrder(StiPropertyOrder.StyleBrushType)]
        [DefaultValue(StiBrushType.Glare)]
        [TypeConverter(typeof(StiEnumConverter))]
        [Description("Gets or sets a value which indicates which type of brush report engine will be used to draw this style.")]
        public StiBrushType BrushType { get; set; } = StiBrushType.Solid;

        /// <summary>
        /// Gets or sets a list of colors which will be used for drawing chart series.
        /// </summary>
        [StiCategory("Parameters")]
        [StiSerializable]
        [Description("Gets or sets a list of colors which will be used for drawing chart series.")]
        [TypeConverter(typeof(Stimulsoft.Report.Design.StiStyleColorsConverter))]
        public Color[] StyleColors { get; set; } = 
        {
            Color.FromArgb(112, 173, 71),
            Color.FromArgb(68, 114, 196),
            Color.FromArgb(255, 192, 0),
            Color.FromArgb(67, 104, 43),
            Color.FromArgb(38, 68, 120),
            Color.FromArgb(153, 115, 0)
        };


        /// <summary>
        /// Gets or sets a base color for drawing this style. 
        /// </summary>
        [StiCategory("Parameters")]
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        [Description("Gets or sets a base color for drawing this style.")]
        public Color BasicStyleColor { get; set; } = Color.WhiteSmoke;

        /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use Border formatting or not. 
        /// </summary>
        [StiSerializable]
        [StiCategory("Parameters")]
        [StiOrder(StiPropertyOrder.StyleAllowUseBorder)]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use Border formatting or not.")]
        public bool AllowUseBorderFormatting { get; set; } = true;

        /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use Border Sides or not. 
        /// </summary>
        [StiSerializable]
        [StiCategory("Parameters")]
        [StiOrder(StiPropertyOrder.StyleAllowUseBorderSides)]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use Border Sides or not.")]
        public bool AllowUseBorderSides { get; set; } = true;

        /// <summary>
        /// Gets or sets a value which indicates whether a report engine can use Brush formatting or not. 
        /// </summary>
        [StiSerializable]
        [StiCategory("Parameters")]
        [StiOrder(StiPropertyOrder.StyleAllowUseBrush)]
        [DefaultValue(true)]
        [TypeConverter(typeof(StiBoolConverter))]
        [Description("Gets or sets a value which indicates whether a report engine can use Brush formatting or not.")]
        public bool AllowUseBrush { get; set; } = true;
        #endregion

        #region Methods.Style
        public override void DrawStyle(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            var rectChart = new Rectangle(rect.X + 5, rect.Y + 5, 40, 30);

            using (var brush = StiBrush.GetBrush(this.ChartAreaBrush, rectChart))
                g.FillRectangle(brush, rectChart);

            using (var brush = StiBrush.GetBrush(this.ChartAreaBrush, rectChart))
            using (var pen = new Pen(brush))
                g.DrawRectangle(pen, rectChart);

            var points = new[]
            {
                new Point(rectChart.X, rectChart.Y),
                new Point(rectChart.X, rectChart.Y + rectChart.Height),
                new Point(rectChart.X + rectChart.Width, rectChart.Y + rectChart.Height)
            };

            g.DrawLines(new Pen(this.ChartAreaBorderColor), points);

            var widthStep = rectChart.Width / 5;
            var widthColumn = rectChart.Width / 5 - 2;
            var values = new[] { 2, 4, 3, 4, 5 };

            for (int indexColumn = 0; indexColumn < 5; indexColumn++)
            {
                var heightColumn = rectChart.Height / 7 * values[indexColumn] + rectChart.Height / 7;

                var color = this.StyleColors.Length > indexColumn ? this.StyleColors[indexColumn] : Color.LightGray;

                using (var brush = new SolidBrush(color))
                {
                    g.FillRectangle(brush,
                        new Rectangle(rectChart.X + rectChart.Width / 20 + widthStep * indexColumn,
                            rectChart.Y + rectChart.Height - heightColumn,
                            widthColumn, heightColumn));
                }
            }

            #region Draw Text
            rect.X += rectChart.Right + 5;
            rect.Width -= rectChart.Width - 10;

            using (var br = new SolidBrush(Color.Black))
            using (var font = new Font("Arial", 10))
            using (var sf = new StringFormat())
            {
                sf.Alignment = StringAlignment.Near;
                sf.LineAlignment = StringAlignment.Center;

                var textRect = rect;
                textRect.Inflate(-2, 0);

                var textSize = g.MeasureString(this.Name, font);
                if (textRect.Width < textSize.Width)
                    sf.Alignment = StringAlignment.Near;

                g.DrawString(this.Name, font, br, rect, sf);
            }
            #endregion
        }

        public override void DrawBox(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            rect.X++;
            rect.Y++;
            rect.Width -= 2;
            rect.Height -= 3;

            this.DrawStyle(g, rect, paintValue, paintImage);

            using (var pen = new Pen(Color.FromArgb(150, Color.Gray)))
            {
                pen.DashStyle = DashStyle.Dash;
                g.DrawRectangle(pen, rect);
            }
        }

        /// <summary>
        /// Gets a style from the component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void GetStyleFromComponent(StiComponent component, StiStyleElements styleElements)
        {
            if (styleElements != StiStyleElements.All)
                throw new Exception("StiCrossTabStyle support only StiStyleElements.All.");

            var chart = component as Stimulsoft.Report.Chart.StiChart;
            if (chart != null)
            {
                #region Color
                if (chart.Style is Stimulsoft.Report.Chart.StiCustomStyle)
                {
                    string name = chart.CustomStyleName;
                    if (!string.IsNullOrEmpty(name) && chart.Report != null && chart.Report.Styles[name] is StiChartStyle)
                    {
                        this.BasicStyleColor = ((StiChartStyle) chart.Report.Styles[name]).BasicStyleColor;
                        this.StyleColors = ((StiChartStyle) chart.Report.Styles[name]).StyleColors;
                    }
                }
                else
                {
                    this.BasicStyleColor = chart.Style.Core.BasicStyleColor;
                    this.StyleColors = chart.Style.Core.StyleColors;
                }
                #endregion

                #region IStiBorder
                if ((styleElements & StiStyleElements.Border) > 0)
                {
                    var cmp = component as IStiBorder;
                    this.Border = cmp.Border.Clone() as StiBorder;

                    this.AllowUseBorderFormatting = true;
                    this.AllowUseBorderSides = true;
                }
                #endregion

                #region IStiBrush
                if ((styleElements & StiStyleElements.Brush) > 0)
                {
                    var cmp = component as IStiBrush;
                    this.Brush = cmp.Brush.Clone() as StiBrush;

                    this.AllowUseBrush = true;
                }
                #endregion

                #region IStiBackColor
                if (component is IStiBackColor && ((styleElements & StiStyleElements.Brush) > 0))
                {
                    var cmp = component as IStiBackColor;
                    this.Brush = new StiSolidBrush(cmp.BackColor);

                    this.AllowUseBrush = true;
                }
                #endregion
            }
        }

        /// <summary>
        /// Sets style to a component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void SetStyleToComponent(StiComponent component)
        {
            var chart = component as Stimulsoft.Report.Chart.StiChart;
            if (chart != null)
            {
                if (!StiStyleConditionHelper.IsAllowStyle(component, this))
                    return;

                #region Color
                chart.CustomStyleName = this.Name;
                chart.Style = new Stimulsoft.Report.Chart.StiCustomStyle(this.Name);
                #endregion

                #region IStiBorder
                if ((this.AllowUseBorderFormatting || this.AllowUseBorderSides))
                {
                    var cmp = component as IStiBorder;

                    var sides = cmp.Border.Side;

                    if (this.AllowUseBorderFormatting)
                    {
                        cmp.Border = this.Border.Clone() as StiBorder;
                        cmp.Border.Side = sides;
                    }

                    if (this.AllowUseBorderSides)
                        cmp.Border.Side = this.Border.Side;
                }
                #endregion

                #region IStiBrush
                if (this.AllowUseBrush)
                {
                    var cmp = component as IStiBrush;
                    cmp.Brush = this.Brush.Clone() as StiBrush;
                }
                #endregion

                #region IStiBackColor
                if (component is IStiBackColor && this.AllowUseBrush)
                {
                    var cmp = component as IStiBackColor;
                    cmp.BackColor = StiBrush.ToColor(this.Brush);
                }
                #endregion
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Saves the style in the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the style.</param>
        public override void Save(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Serialize(this, stream, "StiChartStyle");
        }

        /// <summary>
        /// Loads the style from the stream.
        /// </summary>
        /// <param name="stream">Stream for loading the style.</param>
        public override void Load(Stream stream)
        {
            var ser = new StiSerializing(new StiReportObjectStringConverter());
            ser.Deserialize(this, stream, "StiChartStyle");
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new object of the type StiChartStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        /// <param name="description">Style description.</param>
        internal StiChartStyle(string name, string description, StiReport report)
            : base(name, description, report)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiChartStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        /// <param name="description">Style description.</param>
        public StiChartStyle(string name, string description)
            : this(name, description, null)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiChartStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        public StiChartStyle(string name)
            : this(name, "")
        {
        }

        /// <summary>
        /// Creates a new object of the type StiChartStyle.
        /// </summary>
        public StiChartStyle()
            : this("")
        {
        }
        #endregion
    }
}