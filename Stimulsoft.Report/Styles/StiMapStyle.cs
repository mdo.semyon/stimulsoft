#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.IO;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Maps;
using Stimulsoft.Report.PropertyGrid;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Localization;
using Stimulsoft.Base.Json.Linq;

#if NETCORE
using Stimulsoft.System.Drawing.Design;
#endif

namespace Stimulsoft.Report
{
    /// <summary>
    /// Describes the class that contains a style for Map components.
    /// </summary>	
    public class StiMapStyle : StiBaseStyle
    {
        #region IStiJsonReportObject.override
        public override JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = base.SaveToJsonObject(mode);

            jObject.AddPropertyJObject("Colors", StiJsonReportObjectHelper.Serialize.ColorArray(Colors));
            jObject.AddPropertyJObject("HeatmapColors", StiJsonReportObjectHelper.Serialize.ColorArray(HeatmapColors));
            jObject.AddPropertyStringNullOrEmpty("DefaultColor", StiJsonReportObjectHelper.Serialize.JColor(DefaultColor, ColorTranslator.FromHtml("#4472c4")));
            jObject.AddPropertyStringNullOrEmpty("BackColor", StiJsonReportObjectHelper.Serialize.JColor(DefaultColor, Color.White));
            jObject.AddPropertyStringNullOrEmpty("ForeColor", StiJsonReportObjectHelper.Serialize.JColor(ForeColor, Color.White));
            jObject.AddPropertyDouble("BorderSize", BorderSize, 0.5);
            jObject.AddPropertyStringNullOrEmpty("BorderColor", StiJsonReportObjectHelper.Serialize.JColor(BorderColor, Color.White));

            return jObject;
        }

        public override void LoadFromJsonObject(JObject jObject)
        {
            base.LoadFromJsonObject(jObject);

            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Colors":
                        this.Colors = StiJsonReportObjectHelper.Deserialize.ColorArray((JObject)property.Value);
                        break;

                    case "HeatmapColors":
                        this.HeatmapColors = StiJsonReportObjectHelper.Deserialize.ColorArray((JObject)property.Value);
                        break;

                    case "DefaultColor":
                        this.DefaultColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "BackColor":
                        this.BackColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "ForeColor":
                        this.ForeColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;

                    case "BorderSize":
                        this.BorderSize = property.Value.ToObject<double>();
                        break;

                    case "BorderColor":
                        this.BorderColor = StiJsonReportObjectHelper.Deserialize.Color(property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region IStiPropertyGridObject
        public override StiComponentId ComponentId => StiComponentId.StiMapStyle;

        public override StiPropertyCollection GetProperties(IStiPropertyGrid propertyGrid, StiLevel level)
        {
            var propHelper = propertyGrid.PropertiesHelper;
            var objHelper = new StiPropertyCollection();

            var list = new[]
            {
                propHelper.StyleName(),
                propHelper.Description(),
                propHelper.StyleCollectionName(),
                propHelper.StyleConditions()
            };
            objHelper.Add(StiPropertyCategories.Main, list);

            list = new[]
            {
                propHelper.BackColor(),
                propHelper.StyleColors(),
                propHelper.DefaultColor(),
                propHelper.ForeColor(),
                propHelper.HeatmapColors(),
            };
            objHelper.Add(StiPropertyCategories.Appearance, list);

            list = new[]
            {
                propHelper.BorderColor(),
                propHelper.BorderSize(),
            };
            objHelper.Add(StiPropertyCategories.Map, list);

            return objHelper;
        }
        #endregion

        #region Properties
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        public virtual Color IndividualColor { get; set; } = ColorTranslator.FromHtml("#70ad47");

        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Report.Design.StiStyleColorsConverter))]
        public virtual Color[] Colors { get; set; } = 
        {
            ColorTranslator.FromHtml("#70ad47"),
            ColorTranslator.FromHtml("#4472c4"),
            ColorTranslator.FromHtml("#ffc000"),
            ColorTranslator.FromHtml("#43682b"),
            ColorTranslator.FromHtml("#fd6a37"),
            ColorTranslator.FromHtml("#997300"),
        };

        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Report.Design.StiStyleColorsConverter))]
        public virtual Color[] HeatmapColors { get; set; } =
        {
            ColorTranslator.FromHtml("#70ad47"),
            ColorTranslator.FromHtml("#ffc000"),
        };

        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        public virtual Color DefaultColor { get; set; } = ColorTranslator.FromHtml("#4472c4");
        
        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        public virtual Color BackColor { get; set; } = ColorTranslator.FromHtml("#ffffff");

        [StiSerializable]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [Editor("Stimulsoft.Base.Drawing.Design.StiColorEditor, Stimulsoft.Report.Design, " + StiVersion.VersionInfo, typeof(UITypeEditor))]
        public virtual Color ForeColor { get; set; } = ColorTranslator.FromHtml("#ffffff");

        [StiSerializable]
        [StiCategory("Map")]
        [DefaultValue(0.5d)]
        [StiPropertyLevel(StiLevel.Basic)]
        public double BorderSize { get; set; } = 0.5;

        [StiSerializable]
        [StiCategory("Map")]
        [TypeConverter(typeof(Stimulsoft.Base.Drawing.Design.StiColorConverter))]
        [StiPropertyLevel(StiLevel.Basic)]
        public virtual Color BorderColor { get; set; } = Color.White;
        #endregion

        #region Methods.Style
        public override void DrawStyle(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            g.DrawImage(Maps.Helpers.StiMapHelper.MapIcon, rect.X + 4, rect.Y + ((rect.Height - 32) / 2), 32, 32);

            var colorCount = Colors.Length;
            if (colorCount > 10)
                colorCount = 10;

            var rectElement = new Rectangle(rect.X + 44, rect.Y + 6, rect.Width - 10, rect.Height - 10);
            var xPos = rectElement.Left;
            for (int index = 0; index < colorCount; index++)
            {
                using (var brush = new SolidBrush(this.Colors[index]))
                    g.FillRectangle(brush, xPos, rectElement.Top, 10, rectElement.Height);

                xPos += 10;
            }

            #region Draw Text
            rectElement.X = xPos + 5;
            rectElement.Width = rectElement.Right - rectElement.X;

            using (var br = new SolidBrush(Color.Black))
            using (var font = new Font("Arial", 10))
            using (var sf = new StringFormat())
            {
                sf.Alignment = StringAlignment.Near;
                sf.LineAlignment = StringAlignment.Center;

                g.DrawString(this.Name, font, br, rectElement, sf);
            }
            #endregion   
        }

        public override void DrawBox(Graphics g, Rectangle rect, bool paintValue, bool paintImage)
        {
            rect.X++;
            rect.Y++;
            rect.Width -= 2;
            rect.Height -= 3;

            this.DrawStyle(g, rect, paintValue, paintImage);

            using (var pen = new Pen(Color.FromArgb(150, Color.Gray)))
            {
                pen.DashStyle = DashStyle.Dash;
                g.DrawRectangle(pen, rect);
            }
        }

        /// <summary>
        /// Gets a style from the component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void GetStyleFromComponent(StiComponent component, StiStyleElements styleElements)
        {
        }

        /// <summary>
        /// Sets style to a component.
        /// </summary>
        /// <param name="component">Component.</param>
        public override void SetStyleToComponent(StiComponent component)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Saves the style in the stream.
        /// </summary>
        /// <param name="stream">Stream for saving the style.</param>
        public override void Save(Stream stream)
        {
            var serializing = new StiSerializing(new StiReportObjectStringConverter());
            serializing.Serialize(this, stream, "StiMapStyle");
        }

        /// <summary>
        /// Loads the style from the stream.
        /// </summary>
        /// <param name="stream">Stream for loading the style.</param>
        public override void Load(Stream stream)
        {
            var serializing = new StiSerializing(new StiReportObjectStringConverter());
            serializing.Deserialize(this, stream, "StiMapStyle");
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new object of the type StiMapStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        /// <param name="description">Style description.</param>
        internal StiMapStyle(string name, string description, StiReport report)
            : base(name, description, report)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiMapStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        /// <param name="description">Style description.</param>
        public StiMapStyle(string name, string description)
            : this(name, description, null)
        {
        }

        /// <summary>
        /// Creates a new object of the type StiMapStyle.
        /// </summary>
        /// <param name="name">Style name.</param>
        public StiMapStyle(string name)
            : this(name, "")
        {
        }

        /// <summary>
        /// Creates a new object of the type StiMapStyle.
        /// </summary>
        public StiMapStyle()
            : this("")
        {
        }
        #endregion
    }
}
