#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;

namespace Stimulsoft.Report
{
    /// <summary>
    /// This class contains method which helps convert component location to its border sides.
    /// </summary>
    internal static class StiStylesHelper
    {
        internal static StiBorderSides GetBorderSidesFromLocation(StiComponent component)
        {
            var sides = StiBorderSides.None;

            var compLeft = Math.Round((decimal)component.Page.Unit.ConvertToHInches(component.Left) / 10, 0);
            var compTop = Math.Round((decimal)component.Page.Unit.ConvertToHInches(component.Top) / 10, 0);
            
            var parentWidth = Math.Round((decimal)component.Page.Unit.ConvertToHInches(component.Parent.Width) / 10, 0);
            var parentHeight = Math.Round((decimal)component.Page.Unit.ConvertToHInches(component.Parent.Height) / 10, 0);
            var compRight = Math.Round((decimal)component.Page.Unit.ConvertToHInches(component.Left + component.Width) / 10, 0);
            var compBottom = Math.Round((decimal)component.Page.Unit.ConvertToHInches(component.Top + component.Height) / 10, 0);
                        
            if (compLeft <= 0) sides |= StiBorderSides.Left;
            if (compTop <= 0) sides |= StiBorderSides.Top;

            if (compRight >= parentWidth) sides |= StiBorderSides.Right;
            if (compBottom >= parentHeight) sides |= StiBorderSides.Bottom;

            return sides;
        }
    }
}
