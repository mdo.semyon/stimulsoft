#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Helpers;

namespace Stimulsoft.Report.Help
{
    public partial class StiHelpViewerForm : Form, IStiThreadForm
    {
        private StiHelpViewerForm()
        {
            InitializeComponent();

            currentForm = this;

            this.Closing += ThisClosing;
            this.MouseDown += panel1Click;
            this.buttonPin.PressedState = StiSettings.GetBool("HelpViewerForm", "IsPin", false);
            this.TopMost = this.buttonPin.PressedState;

            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);

            this.StartPosition = FormStartPosition.Manual;
            LoadPosition();

            this.HeaderText = StiLocalization.Get("HelpDesigner", "StimulsoftHelp");
        }

        #region Methods.DllImport
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion

        #region Fields
        private System.Windows.Forms.Timer timerUrl;
        private static StiHelpViewerForm currentForm;
        private bool isCustomHelp;
        #endregion

        #region Properties
        internal string HeaderText { get; set; }

        internal static string Url { get; set; }
        #endregion

        #region Methods
        public void InvokeCustomHelp()
        {
            isCustomHelp = StiOptions.Engine.GlobalEvents.InvokeOpenCustomHelp(this);
        }

        private void LoadPosition()
        {
            StiSettings.Load();

            Point position;
            var size = new Size(800, 700);
            int screenNumber = StiSettings.GetInt("HelpViewerForm", "ScreenNumber", -999);

            #region ���� ������ � �������� ����� ��� �� �����������
            if (screenNumber == -999)
            {
                var screenRect = Rectangle.Empty;
                if (Screen.AllScreens.Length == 1 || ActiveForm == null)
                {
                    screenRect = Screen.PrimaryScreen.Bounds;
                }
                else
                {
                    #region ���� ������ ������� �������� �����

                    Point p;
                    Size formSize;
                    var activeForm = ActiveForm;

                    if (activeForm.WindowState == FormWindowState.Maximized)
                    {
                        p = activeForm.RestoreBounds.Location;
                        formSize = activeForm.RestoreBounds.Size;
                    }
                    else
                    {
                        p = activeForm.Location;
                        formSize = activeForm.Size;
                    }

                    #endregion

                    bool isFind = false;
                    for (int index = 0; index < Screen.AllScreens.Length; index++)
                    {
                        var screen = Screen.AllScreens[index];
                        if (screen.Bounds.IntersectsWith(new Rectangle(p, formSize)))
                        {
                            screenRect = screen.Bounds;
                            isFind = true;
                            break;
                        }
                    }

                    if (!isFind)
                        screenRect = Screen.PrimaryScreen.Bounds;
                }

                position = new Point(screenRect.Right - size.Width, screenRect.Top + (screenRect.Height - size.Height) / 2);

                base.SetBoundsCore(position.X, position.Y, size.Width, size.Height, BoundsSpecified.All);
                return;
            }
            #endregion

            int primaryIndex = StiFormHelper.GetPrimaryScreenNumber();
            position = new Point(StiSettings.GetInt("HelpViewerForm", "WindowXPos", 0), StiSettings.GetInt("HelpViewerForm", "WindowYPos", 0));
            size.Width = StiSettings.GetInt("HelpViewerForm", "WindowWidth", size.Width);
            size.Height = StiSettings.GetInt("HelpViewerForm", "WindowHeight", size.Height);

            Screen currentScreen;
            if (screenNumber != primaryIndex)
            {
                if (screenNumber + 1 > Screen.AllScreens.Length)
                {
                    currentScreen = Screen.PrimaryScreen;
                    position = currentScreen.Bounds.Location;
                }
                else
                {
                    currentScreen = Screen.AllScreens[screenNumber];
                    if (!currentScreen.Bounds.Contains(new Rectangle(position, size)))
                    {
                        position = currentScreen.Bounds.Location;
                    }
                }
            }
            else
            {
                currentScreen = Screen.PrimaryScreen;

                // ���� ScreenNumber �� �����, ����� ������ ��������� �������
                if (StiSettings.GetInt("HelpViewerForm", "ScreenNumber", -1) == -1)
                {
                    var screenRect = currentScreen.Bounds;
                    position = new Point(screenRect.Right - size.Width, screenRect.Top + screenRect.Height - size.Height);
                }
            }

            base.SetBoundsCore(position.X, position.Y, size.Width, size.Height, BoundsSpecified.All);
        }

        private void SetUrl(string url)
        {
            if (isCustomHelp)
            {
                currentForm.webBrowser.Url = new Uri(url); 
                return;
            }

            string file = $"{StiOptions.Configuration.ApplicationDirectory}\\Help\\{StiLocalization.CultureName}\\{url}";
            if (File.Exists(file))
                currentForm.webBrowser.Url = new Uri(file, UriKind.Absolute);

            else
            {
                string language;
                switch (StiLocalization.CultureName)
                {
                    case "ru":
                        language = "ru";
                        break;

                    default:
                        language = "en";
                        break;
                }

                url = $"https://www.stimulsoft.com/{language}/documentation/online/{url}";
                currentForm.webBrowser.Url = new Uri(url);
            }
        }
        #endregion

        #region Methods.override
        protected override void WndProc(ref Message m)
        {
            #region WM_NCHITTEST
            if (m.Msg == 0x0084)
            {
                int x = m.LParam.ToInt32() & 0x0000FFFF;
                int y = (int)((m.LParam.ToInt32() & 0xFFFF0000) >> 16);

                Point pos = this.PointToClient(new Point(x, y));
                const int borderWidth = 10;

                #region HTTOPLEFT
                if (pos.X < borderWidth && pos.Y < borderWidth)
                {
                    m.Result = new IntPtr(13);
                    return;
                }
                #endregion

                #region HTTOPRIGHT
                if (pos.X > this.Width - borderWidth && pos.Y < borderWidth)
                {
                    m.Result = new IntPtr(14);
                    return;
                }
                #endregion

                #region HTBOTTOMLEFT
                if (pos.X < borderWidth && pos.Y > this.Height - borderWidth)
                {
                    m.Result = new IntPtr(16);
                    return;
                }
                #endregion

                #region HTBOTTOMRIGHT
                if (pos.X > this.Width - borderWidth && pos.Y > this.Height - borderWidth)
                {
                    m.Result = new IntPtr(17);
                    return;
                }
                #endregion

                #region HTLEFT
                if (pos.X < borderWidth)
                {
                    m.Result = new IntPtr(10);
                    return;
                }
                #endregion

                #region HTRIGHT
                if (pos.X > this.Width - borderWidth)
                {
                    m.Result = new IntPtr(11);
                    return;
                }
                #endregion

                #region HTTOP
                if (pos.Y < borderWidth)
                {
                    m.Result = new IntPtr(12);
                    return;
                }
                #endregion

                #region HTBOTTOM
                if (pos.Y > this.Height - borderWidth)
                {
                    m.Result = new IntPtr(15);
                    return;
                }
                #endregion
            }
            #endregion

            base.WndProc(ref m);
        }
        #endregion

        #region Methods.static
        public static void Show(string url)
        {
            if (currentForm == null)
            {
                var thread = new Thread(RunHelp);
                thread.TrySetApartmentState(ApartmentState.STA);
                thread.Name = "StiHelp";
                thread.Start();
            }

            Url = url;
        }

        private static void RunHelp()
        {
            Application.Run(new StiHelpViewerForm());
        }
        #endregion

        #region Handlers
        private void ThisClosing(object sender, CancelEventArgs e)
        {
            this.Closing -= ThisClosing;
            StiFormHelper.SaveStateForm(this, "HelpViewerForm");
            currentForm = null;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);
            var g = e.Graphics;

            var oldSmoothingMode = g.SmoothingMode;
            var oldCompositingQuality = g.CompositingQuality;
            var oldTextRenderingHint = g.TextRenderingHint;
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.TextRenderingHint = TextRenderingHint.AntiAlias;

            using (var pen = new Pen(Color.FromArgb(44, 107, 156)))
            {
                var rect = new Rectangle(0, 0, this.Width - 1, this.Height - 1);
                g.FillRectangle(Brushes.White, rect);
                g.DrawRectangle(pen, rect);
            }

            using (Brush brush = new SolidBrush(Color.FromArgb(43, 87, 154)))
            {
                using (var font = new Font("Calibri", 17))
                {
                    g.DrawString(this.HeaderText, font, brush, 12, 32);
                }
            }

            g.TextRenderingHint = oldTextRenderingHint;
            g.CompositingQuality = oldCompositingQuality;
            g.SmoothingMode = oldSmoothingMode;
        }

        private void panel1Click(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, 0xA1, 0x2, 0);
            }
        }

        private void buttonCloseClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonMinimizeClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void buttonMaximizeClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
                buttonMaximize.PressedState = true;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                buttonMaximize.PressedState = false;
            }

            buttonMaximize.Invalidate();
        }

        private void buttonPinClick(object sender, EventArgs e)
        {
            this.buttonPin.PressedState = !this.buttonPin.PressedState;
            StiSettings.Set("HelpViewerForm", "IsPin", this.buttonPin.PressedState);
            this.TopMost = this.buttonPin.PressedState;

            this.buttonPin.Invalidate();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            InvokeCustomHelp();

            if (Url != null)
            {
                string url = Url;
                Url = null;

                this.SetUrl(url);
            }

            timerUrl = new System.Windows.Forms.Timer
            {
                Interval = 1500
            };
            timerUrl.Tick += timerUrlTick;
            timerUrl.Start();
        }

        private void timerUrlTick(object sender, EventArgs e)
        {
            timerUrl.Stop();

            if (Url != null)
            {
                string url = Url;
                Url = null;

                this.SetUrl(url);
                this.Activate();
            }

            timerUrl.Start();
        }
        #endregion
    }
}