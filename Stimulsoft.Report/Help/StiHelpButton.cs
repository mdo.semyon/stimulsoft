#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Help
{
    [ToolboxItem(false)]
    internal sealed class StiHelpButton : Button
    {
        #region enum StiHelpButtonType
        public enum StiHelpButtonType
        {
            Close,
            Minimize,
            Maximize
        }
        #endregion

        #region Properties
        public bool PressedState { get; set; }

        public StiHelpButtonType ButtonType { get; set; } = StiHelpButtonType.Close;
        #endregion

        #region Fields
        private bool isEnter;
        private bool isPressed;
        #endregion

        #region Handlers
        protected override void OnPaint(PaintEventArgs pevent)
        {
            var g = pevent.Graphics;

            Color c1;
            if (isPressed)
            {
                using (Brush brush = new SolidBrush(Color.FromArgb(163, 189, 227)))
                {
                    g.FillRectangle(brush, this.ClientRectangle);
                }

                c1 = Color.FromArgb(25, 71, 138);
            }
            else
            {
                if (isEnter)
                {
                    using (Brush brush = new SolidBrush(Color.FromArgb(213, 225, 242)))
                    {
                        g.FillRectangle(brush, this.ClientRectangle);
                    }
                    c1 = Color.FromArgb(25, 71, 138);
                }
                else
                {
                    g.FillRectangle(Brushes.White, this.ClientRectangle);
                    c1 = Color.FromArgb(119, 119, 119);
                }
            }

            using (Brush brush = new SolidBrush(c1))
            {
                if (ButtonType == StiHelpButtonType.Close)
                {
                    using (var pen = new Pen(c1, 2f))
                    {
                        g.DrawLine(pen, 10, 6, 19, 15);
                        g.DrawLine(pen, 10, 15, 19, 6);
                    }

                    g.FillRectangle(brush, 14, 9, 3, 4);
                }
                else if (ButtonType == StiHelpButtonType.Maximize)
                {
                    using (var pen = new Pen(c1, 1f))
                    {
                        if (PressedState)
                        {
                            g.DrawLine(pen, 19, 13, 20, 13);
                            g.DrawLine(pen, 20, 8, 20, 13);
                            g.DrawLine(pen, 12, 8, 12, 9);
                            g.FillRectangle(brush, 12, 6, 9, 2);

                            g.DrawRectangle(pen, 10, 9, 8, 7);
                            g.FillRectangle(brush, 10, 9, 8, 2);
                        }
                        else
                        {
                            g.DrawRectangle(pen, 10, 6, 10, 10);
                            g.FillRectangle(brush, 10, 6, 10, 3);
                        }
                    }
                }
                else
                {
                    g.FillRectangle(brush, 10, 12, 9, 2);
                }
            }
        }

        private void ThisMouseUp(object sender, MouseEventArgs e)
        {
            if (isPressed)
            {
                isPressed = false;
                this.Invalidate();
            }
        }

        private void ThisMouseDown(object sender, MouseEventArgs e)
        {
            isPressed = true;
            this.Invalidate();
        }

        private void ThisMouseLeave(object sender, EventArgs e)
        {
            isEnter = false;
            isPressed = false;
            this.Invalidate();
        }

        private void ThisMouseEnter(object sender, EventArgs e)
        {
            isEnter = true;
            this.Invalidate();
        }
        #endregion

        public StiHelpButton()
        {
            this.MouseEnter += ThisMouseEnter;
            this.MouseLeave += ThisMouseLeave;
            this.MouseDown += ThisMouseDown;
            this.MouseUp += ThisMouseUp;
        }
    }
}