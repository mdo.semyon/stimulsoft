﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Help
{
    public static class StiHelpProvider
    {
        public static void ShowHelpViewer(string url)
        {
            try
            {
                if (!string.IsNullOrEmpty(url) && !url.EndsWithInvariant("?toc=0"))
                    url += "?toc=0";

                var hyperlink = url ?? "user-manual/";
#if !NETCORE
                StiHelpViewerForm.Show(hyperlink);
#endif
            }
            catch (Exception ee)
            {
                StiExceptionProvider.Show(ee);
            }
        }
    }
}