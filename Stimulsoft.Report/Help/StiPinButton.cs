#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Report.Help
{
    [ToolboxItem(false)]
    internal sealed class StiPinButton : Button
    {
        #region Properties
        public bool PressedState { get; set; }
        #endregion

        #region Fields
        private bool isEnter;
        private bool isPressed;
        #endregion

        #region Handlers
        protected override void OnPaint(PaintEventArgs pevent)
        {
            var g = pevent.Graphics;

            Color c1;
            if (isPressed)
            {
                using (Brush brush = new SolidBrush(Color.FromArgb(163, 189, 227)))
                {
                    g.FillRectangle(brush, this.ClientRectangle);
                }

                c1 = Color.FromArgb(25, 71, 138);
            }
            else
            {
                if (isEnter)
                {
                    using (Brush brush = new SolidBrush(Color.FromArgb(213, 225, 242)))
                    {
                        g.FillRectangle(brush, this.ClientRectangle);
                    }
                    c1 = Color.FromArgb(25, 71, 138);
                }
                else
                {
                    g.FillRectangle(Brushes.White, this.ClientRectangle);
                    c1 = Color.FromArgb(119, 119, 119);
                }
            }

            using (Brush brush = new SolidBrush(c1))
            {
                if (PressedState)
                {
                    g.FillRectangle(brush, 12, 7, 4, 3);
                    g.FillRectangle(brush, 13, 12, 2, 4);

                    using (var pen = new Pen(c1, 1f))
                    {
                        g.DrawLine(pen, 11, 6, 16, 6);
                        g.DrawLine(pen, 11, 10, 16, 10);
                        g.DrawLine(pen, 10, 11, 17, 11);
                    }
                }
                else
                {
                    g.FillRectangle(brush, 9, 10, 4, 2);
                    g.FillRectangle(brush, 15, 9, 3, 4);

                    using (var pen = new Pen(c1, 1f))
                    {
                        g.DrawLine(pen, 13, 7, 13, 14);
                        g.DrawLine(pen, 14, 8, 14, 13);
                        g.DrawLine(pen, 18, 8, 18, 13);
                    }
                }
            }
        }

        private void ThisMouseUp(object sender, MouseEventArgs e)
        {
            if (isPressed)
            {
                isPressed = false;
                this.Invalidate();
            }
        }

        private void ThisMouseDown(object sender, MouseEventArgs e)
        {
            isPressed = true;
            this.Invalidate();
        }

        private void ThisMouseLeave(object sender, EventArgs e)
        {
            isEnter = false;
            isPressed = false;
            this.Invalidate();
        }

        private void ThisMouseEnter(object sender, EventArgs e)
        {
            isEnter = true;
            this.Invalidate();
        }
        #endregion

        public StiPinButton()
        {
            this.MouseEnter += ThisMouseEnter;
            this.MouseLeave += ThisMouseLeave;
            this.MouseDown += ThisMouseDown;
            this.MouseUp += ThisMouseUp;
        }
    }
}