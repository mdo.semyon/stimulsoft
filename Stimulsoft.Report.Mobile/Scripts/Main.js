﻿
function StiMobileViewer(parameters) {
    this.options = parameters;
    //Fields
    this.options.currentMenu = null,
    this.options.currentDropDownListMenu = null,
    this.options.currentDatePicker = null,
    this.options.menuAnimDuration = 150,
    this.options.formAnimDuration = 200,
    this.options.scrollDuration = 350,    
    this.options.buttonsTimer = null,
    this.options.formInDrag = false,   
    this.options.css = null,
    this.options.head = null,
    this.options.newWindow = null,
    this.options.bookmarkAnchor = null,
    this.options.firstZoomDistance = 0,
    this.options.secondZoomDistance = 0,
    this.options.zoomStep = 0,
    this.options.parametersValues = {},    
    this.options.fingerIsMoved = false,        
    this.options.cacheReportGuid = null,
    this.options.pagesWidth = 0,
    this.options.pagesHeight = 0,
    this.options.pagesArrayForPrint = null,
    this.options.formInDrag = false;
    this.callbackFunctions = {};    

    //Pressed Events    
    this.options.buttonPressed = false,
    this.options.menuPressed = false,
    this.options.datePickerPressed = false,
    this.options.formPressed = false,
    this.options.dropDownListMenuPressed = false,
    this.options.disabledPanelPressed = false,
    
    this.options.mobileViewer = document.getElementById(parameters.mobileViewerId);
    this.options.head = document.getElementsByTagName("head")[0];
    this.options.mainPanel = document.getElementById(parameters.mobileViewerId + "_MainPanel");
    this.options.isTouchDevice = parameters.interfaceType == "Auto" ? this.IsTouchDevice() : parameters.interfaceType == "Touch";
    this.options.buttons = {};
    this.options.controls = {};
    this.options.radioButtons = {};
    this.options.menus = {};
    this.options.forms = {};
    this.options.parameterRowHeight = this.options.isTouchDevice ? 35 : 30;
    this.options.toolbarFontColor = (this.options.toolbarFontColor != "Empty") ? this.options.toolbarFontColor : "#444444";
    this.options.defaultExportSettings = {};
    this.options.loc = {};
    this.options.fontNames = {};
    this.options.dateRanges = {};
    if (this.options.loc) this.loc = this.options.loc.Localization || this.options.loc;
    delete this.options.loc;
    this.options.images = {};
    this.options.cachedReports = {};

    this.options.encodingDataCollection = {};
    this.options.reportPagesLoadedStatus = {};
    this.options.notAnimatedPages = {};
    this.options.monthesCollection = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    this.options.dayOfWeekCollection = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];    
    this.options.findHelper = { findLabels : [] };
    //this.options.reportsOnlineUrl = "http://localhost:17764/Main.aspx";
    this.options.reportsOnlineUrl = "https://reports.stimulsoft.com";

    //Initialize Controls    
    if (this.options.theme.indexOf("Material") == 0) {
        this.initializeMaterial();
    }
    this.InitializeMobileViewer();
    this.InitializeToolBar();
    this.InitializeFindPanel();
    this.InitializeDisabledPanels();
    this.InitializeProcessImage();
    this.InitializeReportPanel();

    if (this.options.cloudMode || this.options.designerMode) {
        this.InitializeNavigatePanel();
    }

    if (this.options.cloudMode) {
        this.InitializeBigProgressBarForm();
        this.InitializeSmallProgressBar();
    }
   
    this.InitializeToolTip();
    this.InitializeErrorMessageForm();

    if (parameters.scrollbiParameters && parameters.scrollbiParameters.errorMessage) {
        this.options.forms.errorMessageForm.show(parameters.scrollbiParameters.errorMessage);
    }

    if (this.options.showToolBar && this.options.showSave) {
        if (this.options.cloudMode)
            this.InitializeCloudSaveMenu(this.options.toolbar.controls.Save);
        else
            this.InitializeSaveMenu();
    }

    if (this.options.showToolBar && this.options.showPrintButton) this.InitializePrintMenu();
    if (this.options.showToolBar && this.options.showZoom) this.InitializeZoomMenu();
    if (this.options.showToolBar && this.options.showViewMode) this.InitializeViewModeMenu();
        
    this.InitializeBookmarksPanel();
    this.InitializeParametersPanel();    
        
    //add pages to report panel    
    if (this.options.pagesArray && !this.options.isMaterial) this.options.reportPanel.addPages();

    //change toolbar
    if (this.options.toolbar) this.options.toolbar.changeToolBarState();
    if (this.options.materialPanel) this.options.materialPanel.changeToolBarState();

    if (parameters["errorMessage"]) this.options.forms.errorMessageForm.show(parameters["errorMessage"]);

    var jsObject = this;

    this.addEvent(document, 'mouseup', function (event) {
        jsObject.DocumentMouseUp(event);
    });

    this.addEvent(document, 'mousemove', function (event) {
        jsObject.DocumentMouseMove(event);
    });

    if (this.options.isEditableReport && this.options.buttons.Editor) {
        this.options.buttons.Editor.style.display = "";
    }

    if (this.options.cloudMode && this.options.cloudParameters) {
        this.SetWindowIcon(document, this.options.cloudParameters.favIcon);

        document.title = this.options.cloudParameters.reportName
            ? Base64.decode(this.options.cloudParameters.reportName) + " - " + this.loc.FormViewer.title
            : this.loc.FormViewer.title;

        if (this.options.cloudParameters.dVers === true || this.options.cloudParameters.dVers == "true") {
            document.title += " " + this.getT();
        }

        // if sharing, hide viewer panels
        if (this.options.cloudParameters.shareKey) {
            jsObject.options.toolbar.style.display = "none";
            jsObject.options.navigatePanel.style.display = "none";
        }

        jsObject.InitializeController();
        jsObject.InitializeCloudReportFirstPageSignal();
        jsObject.InitializeCloudBookmarksSignals();
        jsObject.InitializeCloudExportSignals();
        jsObject.LoadReportOnStart();        
    }
    
    jsObject.InitializeFolderReportsPanel();

    if (this.options.theme.indexOf("Material") == 0 && this.options.pagesArray) {
        var sizes = this.options.pagesArray[0].sizes.split(";");
        jsObject.options.materialZoom = Math.round(screen.width / (parseFloat(sizes[0]) + 25) * 100);
        setTimeout(function () {
            jsObject.actionEvent("Zoom" + jsObject.options.materialZoom);
        }, 100);
        this.options.pagesArray = [];
        if (this.options.materialPanel) this.options.materialPanel.show();
    }
}

StiMobileViewer.prototype.initializeMaterial = function () {
    var viewPortTag = document.createElement('meta');
    viewPortTag.id = "viewport";
    viewPortTag.name = "viewport";
    viewPortTag.content = "initial-scale=1.0,width=device-width,user-scalable=0, minimal-ui;user-scalable=no";
    //viewPortTag.content = "width=device-width, initial-scale=1.0";
    document.getElementsByTagName('head')[0].appendChild(viewPortTag);

    var this_ = this;
    this.options.isMaterial = true;
    this.options.animations = [];
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                                window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
    var div = document.createElement("div");
    div.style.width = "1in";
    var body = document.getElementsByTagName("body")[0];
    body.appendChild(div);
    var ppi = document.defaultView.getComputedStyle(div, null).getPropertyValue('width');
    body.removeChild(div);
    this.options.ppi = parseFloat(ppi);
    //alert("PPI: " + ppi);

    this.InitializeMaterialPanel();
    this.InitializeMainMaterialMenu();
    this.InitializeCenterText();
    this.options.optionShowToolBar = this.options.showToolBar;
    this.options.showToolBar = false;
    //this.options.paramsBookmarks = null;//TODO
}

StiMobileViewer.prototype.actionEvent = function (action, bookmarkPage, bookmarkAnchor, componentGuid) {
    switch (action) {
        case "Print":
            {
                if (this.options.printDestination == "Pdf")
                    this.printToPdf();
                else if (this.options.printDestination == "Direct")
                    this.sendMainParameters("PrintDirect");
                else if (this.options.printDestination == "WithPreview") {
                    this.options.newWindow = window.open("");
                    this.sendMainParameters("PrintWithPreview");
                }
                else {
                    this.options.menus.printMenu.changeVisibleState(!this.options.menus.printMenu.visible);
                }
                return;
            }
        case "Save":
            {
                var saveMenu = this.options.cloudMode ? this.options.menus.cloudSaveMenu : this.options.menus.saveMenu;
                saveMenu.changeVisibleState(!saveMenu.visible);
                return;
            }
        case "Zoom": this.options.menus.zoomMenu.changeVisibleState(!this.options.menus.zoomMenu.visible); return;
        case "ViewMode": this.options.menus.viewModeMenu.changeVisibleState(!this.options.menus.viewModeMenu.visible); return;
        case "FirstPage":
        case "PrevPage":
        case "NextPage":
        case "LastPage":
            {
                if (action == "NextPage" && this.options.pageNumber < this.options.pagesCount - 1) this.options.pageNumber++;
                if (action == "PrevPage" && this.options.pageNumber > 0) this.options.pageNumber--;
                if (action == "FirstPage") this.options.pageNumber = 0;
                if (action == "LastPage") this.options.pageNumber = this.options.pagesCount - 1;
                if (this.options.cloudMode) {
                    this.scrollToPage(this.options.pageNumber);
                    if (this.options.toolbar) this.options.toolbar.changeToolBarState();
                    if (this.options.materialPanel) this.options.materialPanel.changeToolBarState();
                    return;
                }
                if (this.options.isMaterial) {
                    this.options.materialPanel.showOnPage = true;
                    this.options.materialPanel.show();
                }
                break;
            }
        case "Zoom25": this.options.zoom = 25; break;
        case "Zoom50": this.options.zoom = 50; break;
        case "Zoom75": this.options.zoom = 75; break;
        case "Zoom100": this.options.zoom = 100; break;
        case "Zoom150": this.options.zoom = 150; break;
        case "Zoom200": this.options.zoom = 200; break;
        case "ZoomOnePage":
        case "ZoomPageWidth":
            {
                if (this.options.cloudMode || this.options.designerMode) {
                    this.options.toolbar.controls.ZoomOnePage.setSelected(action == "ZoomOnePage");
                    this.options.toolbar.controls.ZoomPageWidth.setSelected(action == "ZoomPageWidth");
                }
                this.options.zoom = action == "ZoomPageWidth" ? parseInt(this.options.reportPanel.getZoomByPageWidth()) : parseInt(this.options.reportPanel.getZoomByPageHeight());
                break;
            }
        case "OnePage": this.options.menuViewMode = "OnePage"; break;
        case "WholeReport": this.options.menuViewMode = "WholeReport"; break;
        case "GoToPage":
            {
                if (!this.options.isMaterial) {
                    this.options.pageNumber = this.options.toolbar.controls.PageControl.textBox.getCorrectValue() - 1;
                } else {
                    this.options.pageNumber = this.options.bottomPanel.controls.PageControl.textBox.getCorrectValue() - 1;
                }
                
                if (this.options.cloudMode) {
                    this.scrollToPage(this.options.pageNumber);
                    if (this.options.toolbar) this.options.toolbar.changeToolBarState();
                    if (this.options.materialPanel) this.options.materialPanel.changeToolBarState();
                    return;
                }
                break;
            }
        case "Bookmarks": this.options.bookmarksPanel.changeVisibleState(!this.options.toolbar.controls.Bookmarks.isSelected); return;
        case "Parameters": this.options.parametersPanel.changeVisibleState(!this.options.toolbar.controls.Parameters.isSelected); return;
        case "Find":
            {
                this.options.findPanel.changeVisibleState(!this.options.toolbar.controls.Find.isSelected); return;
            }
        case "About":
            {
                var aboutPanel = this.options.aboutPanel || this.InitializeAboutPanel();
                aboutPanel.changeVisibleState(!this.options.toolbar.controls.About.isSelected); return;
            }
        case "Design": this.exportEvent("Design"); if (this.options.demoMode) { this.options.processImage.show(); } return;
        case "Submit":
            {
                this.options.editableParameters = null;
                !this.options.cloudMode ? this.sendMainParameters("SubmitVariables") : this.LoadReportTemplateItemToViewer();
                return;
            }
        case "Reset":
            this.options.parameters = {};
            this.options.parametersPanel.clearParameters();
            this.options.parametersPanel.addParameters();
            return;
        case "BookmarkAction":            
            if (bookmarkPage == -1 && this.options.cloudMode) {
                this.findReportBookmark(bookmarkAnchor, componentGuid);
                return;
            }
            if (this.options.pageNumber == bookmarkPage) {
                this.scrollToAnchor(bookmarkAnchor, componentGuid);
                return;
            } else /*if (bookmarkPage != -1)*/ {                
                this.options.pageNumber = bookmarkPage;
                this.options.bookmarkAnchor = bookmarkAnchor;
                this.options.componentGuid = componentGuid;
                if (this.options.cloudMode && this.options.cloudParameters.pagination && !this.options.reportPagesLoadedStatus[bookmarkPage]) {
                    if (this.options.toolbar) this.options.toolbar.changeToolBarState();
                    this.scrollToPage(bookmarkPage);
                    return;
                }
            }
            break;
        case "PrintPdf":
            {
                if (this.options.cloudMode) {
                    this.exportReport("StiPdfExportSettings", null, null, true, true);
                }
                else {
                    this.printToPdf();
                }
                return;
            }
        case "PrintWithPreview": this.options.newWindow = window.open(""); this.sendMainParameters("PrintWithPreview"); return;
        case "PrintWithoutPreview": this.sendMainParameters("PrintDirect"); return;
        case "Editor": this.SetEditableMode(!this.options.editableMode); return;
        case "Close": window.close(); return;
    }

    if (action.indexOf("Zoom") == 0 && action != "ZoomOnePage" && action != "ZoomPageWidth") {
        this.options.zoom = parseFloat(action.substring(4));
    }

    this.sendMainParameters("NavigationReport");
}

String.prototype.stiReplaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};