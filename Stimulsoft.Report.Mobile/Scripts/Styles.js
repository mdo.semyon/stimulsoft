﻿
StiMobileViewer.prototype.Styles = {    
    "FormButton":
         {
             "default": "stiMobileViewerFormButton",
             "over": "stiMobileViewerFormButtonOver",
             "selected": "stiMobileViewerFormButtonSelected",
             "disabled": "stiMobileViewerFormButtonDisabled"
         },
    "StandartSmallButton":
         {
             "default": "stiMobileViewerStandartSmallButton",
             "over": "stiMobileViewerStandartSmallButtonOver",
             "selected": "stiMobileViewerStandartSmallButtonSelected",
             "disabled": "stiMobileViewerStandartSmallButtonDisabled"
         },
    "NavigateButton":
         {
             "default": "stiMobileViewerNavigateButton",
             "over": "stiMobileViewerNavigateButtonOver",
             "selected": "stiMobileViewerNavigateButtonSelected",
             "disabled": "stiMobileViewerNavigateButtonDisabled"
         },
    "ParametersButton":
        {
            "default": "stiMobileViewerParametersButton",
            "over": "stiMobileViewerParametersButtonOver",
            "selected": "stiMobileViewerParametersButtonSelected",
            "disabled": "stiMobileViewerParametersButtonDisabled"
        },
    "DropDownListButton":
         {
             "default": "stiMobileViewerDropDownListButton",
             "over": "stiMobileViewerDropDownListButtonOver",
             "selected": "stiMobileViewerDropDownListButtonSelected",
             "disabled": "stiMobileViewerDropDownListButtonDisabled"
         },
    "MenuStandartItem":
         {
             "default": "stiMobileViewerMenuStandartItem",
             "over": "stiMobileViewerMenuStandartItemOver",
             "selected": "stiMobileViewerMenuStandartItemSelected",
             "disabled": "stiMobileViewerMenuStandartItemDisabled"
         },
    "DatePickerDayButton":
        {
            "default": "stiMobileViewerDatePickerDayButton",
            "over": "stiMobileViewerDatePickerDayButtonOver",
            "selected": "stiMobileViewerDatePickerDayButtonSelected",
            "disabled": "stiMobileViewerDatePickerDayButtonDisabled"
        },
    "HyperlinkButton":
        {
            "default": "stiMobileViewerHyperlinkButton",
            "over": "stiMobileViewerHyperlinkButtonOver",
            "selected": "stiMobileViewerHyperlinkButtonSelected",
            "disabled": "stiMobileViewerHyperlinkButtonDisabled"
        },
    "LoginButton":
        {
            "default": "stiMobileViewerLoginButton",
            "over": "stiMobileViewerLoginButtonOver",
            "selected": "stiMobileViewerLoginButtonSelected",
            "disabled": "stiMobileViewerLoginButtonDisabled"
        },
    "BrightButton":
        {
            "default": "stiMobileViewerBrightButton",
            "over": "stiMobileViewerBrightButtonOver",
            "selected": "stiMobileViewerBrightButtonSelected",
            "disabled": "stiMobileViewerBrightButtonDisabled"
        }
}

StiMobileViewer.prototype.GetStyles = function (name) {
    return this.Styles[name];
}