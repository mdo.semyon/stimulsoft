﻿
StiMobileViewer.prototype.TextArea = function (name, width, height, hintText) {
    var textArea = document.createElement("textarea");
    if (hintText) textArea.setAttribute("placeholder", hintText);

    if (width) {
        textArea.style.width = width + "px";
        textArea.style.minWidth = width + "px";
    }
    if (height) {
        textArea.style.height = height + "px";
        textArea.style.minHeight = height + "px";
    }

    textArea.jsObject = this;
    this.options.controls[name] = textArea;
    textArea.name = name;
    textArea.isEnabled = true;
    textArea.isSelected = false;
    textArea.isOver = false;
    textArea.className = "stiMobileViewerTextArea";

    textArea.setEnabled = function (state) {
        this.isEnabled = state;
        this.disabled = !state;
        this.className = (state ? "stiMobileViewerTextArea" : "stiMobileViewerTextArea stiMobileViewerTextAreaDisabled");
    }

    textArea.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    textArea.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        if (!this.isSelected) this.className = "stiMobileViewerTextArea stiMobileViewerTextAreaOver";
    }
    
    textArea.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.isOver = false;
        if (!this.isSelected) this.className = "stiMobileViewerTextArea";
    }

    textArea.setSelected = function (state) {
        this.isSelected = state;
        this.className = "stiMobileViewerTextArea " +
            (state ? "stiMobileViewerTextAreaOver" : (this.isEnabled ? (this.isOver ? "stiMobileViewerTextAreaOver" : "") : "stiMobileViewerTextAreaDisabled"));
    }

    textArea.onblur = function () {
        this.action();
    }

    textArea.setReadOnly = function (state) {
        this.style.cursor = state ? "default" : "";
        this.readOnly = state;
        try {
            this.setAttribute("unselectable", state ? "on" : "off");
            this.setAttribute("onselectstart", state ? "return false" : "");
        }
        catch (e) { };
    }

    textArea.action = function () { }; 

    return textArea;
}