﻿
StiMobileViewer.prototype.DropDownList = function (name, width, toolTip, items, readOnly, showImage) {
    var dropDownList = this.CreateHTMLTable();
    dropDownList.style.fontFamily = this.options.toolbarFontFamily;
    dropDownList.style.color = this.options.toolbarFontColor;
    dropDownList.jsObject = this;    
    if (name != null) this.options.controls[name] = dropDownList;
    dropDownList.name = name != null ? name : this.generateKey();
    dropDownList.key = null;
    dropDownList.imageCell = null;
    dropDownList.readOnly = readOnly;
    dropDownList.items = (items == null) ? {} : items;
    dropDownList.isEnabled = true;
    dropDownList.isSelected = false;
    dropDownList.isOver = false;
    dropDownList.isFocused = false;
    dropDownList.fullWidth = width + 3;
    if (toolTip) dropDownList.setAttribute("title", toolTip);
    var textBoxWidth = width - (this.options.isTouchDevice ? 23 : 15) - (showImage ? 38 : 0);
    dropDownList.className = "stiMobileViewerDropDownList";

    //Image
    if (showImage) {
        dropDownList.image = document.createElement("div");
        dropDownList.image.dropDownList = dropDownList;
        dropDownList.image.jsObject = this;
        dropDownList.image.className = "stiMobileViewerDropDownListImage";
        dropDownList.imageCell = dropDownList.addCell(dropDownList.image);
        if (readOnly) {
            dropDownList.image.onclick = function () {
                if (!this.isTouchEndFlag && !this.jsObject.options.isTouchClick) 
                    this.dropDownList.button.onclick(); 
            }            
            dropDownList.image.ontouchend = function () {
                var this_ = this;
                this.isTouchEndFlag = true;
                clearTimeout(this.isTouchEndTimer);
                this.dropDownList.button.ontouchend();
                this.isTouchEndTimer = setTimeout(function () {
                    this_.isTouchEndFlag = false;
                }, 1000);
            }
        }
    }
    
    //TextBox
    dropDownList.textBox = document.createElement("input");
    dropDownList.textBox.jsObject = this;
    dropDownList.addCell(dropDownList.textBox);
    dropDownList.textBox.style.width = textBoxWidth + "px";
    dropDownList.textBox.dropDownList = dropDownList;
    dropDownList.textBox.readOnly = readOnly;
    dropDownList.textBox.style.cursor = readOnly ? "default" : "text";
    dropDownList.textBox.style.fontFamily = this.options.toolbarFontFamily;
    dropDownList.textBox.style.color = this.options.toolbarFontColor;
    dropDownList.textBox.style.height = this.options.isTouchDevice ? "23px" : "18px";
    dropDownList.textBox.className = "stiMobileViewerDropDownList_TextBox";
    if (readOnly) {
        dropDownList.textBox.onclick = function () {
            if (!this.isTouchEndFlag && !this.jsObject.options.isTouchClick) 
                this.dropDownList.button.onclick();
        }
        dropDownList.textBox.ontouchend = function () {
            var this_ = this;
            this.isTouchEndFlag = true;
            clearTimeout(this.isTouchEndTimer);
            this.dropDownList.button.ontouchend();
            this.isTouchEndTimer = setTimeout(function () {
                this_.isTouchEndFlag = false;
            }, 1000);
        }
    }
    dropDownList.textBox.action = function () { if (!this.dropDownList.readOnly) { this.dropDownList.setKey(this.value); this.dropDownList.action(); } }
    dropDownList.textBox.onfocus = function () { this.isFocused = true; this.dropDownList.isFocused = true; this.dropDownList.setSelected(true); }
    dropDownList.textBox.onblur = function () { this.isFocused = false; this.dropDownList.isFocused = false; this.dropDownList.setSelected(false); this.action(); }
    dropDownList.textBox.onkeypress = function (event) {
        if (this.dropDownList.readOnly) return false;
        if (event && event.keyCode == 13) {
            this.action();
            return false;
        }
    }

    //DropDownButton
    dropDownList.button = this.SmallButton(null, null, null, "ButtonArrowDown.png", null, null, this.GetStyles("DropDownListButton"));
    dropDownList.button.style.height = this.options.isTouchDevice ? "26px" : "21px";
    dropDownList.addCell(dropDownList.button);
    dropDownList.button.dropDownList = dropDownList;    
    dropDownList.button.action = function () {
        if (!this.dropDownList.menu.visible) {
            if (this.dropDownList.menu.isDinamic) this.dropDownList.menu.addItems(this.dropDownList.items);
            this.dropDownList.menu.changeVisibleState(true);
        }
        else
            this.dropDownList.menu.changeVisibleState(false);
    }


    //Menu
    dropDownList.menu = this.DropDownListMenu(dropDownList);
    this.options.mainPanel.appendChild(dropDownList.menu);
    dropDownList.menu.isDinamic = (items == null);
    if (items != null) dropDownList.menu.addItems(items);

    dropDownList.onmouseenter = function () { 
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        if (!this.isSelected && !this.isFocused) this.className = "stiMobileViewerDropDownListOver";
    }

    dropDownList.onmouseleave = function () { 
        if (!this.isEnabled) return;
        this.isOver = false;
        if (!this.isSelected && !this.isFocused) this.className = "stiMobileViewerDropDownList";
    }

    dropDownList.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    dropDownList.setEnabled = function (state) {
        this.isEnabled = state;
        this.button.setEnabled(state);
        this.textBox.disabled = !state;
        this.textBox.style.visibility = state ? "visible" : "hidden";
        this.className = state ? "stiMobileViewerDropDownList" : "stiMobileViewerDropDownListDisabled";
        if (this.imageCell) this.image.style.visibility = state ? "visible" : "hidden";
    }
    
    dropDownList.setSelected = function (state) {        
        this.isSelected = state;
        this.className = state ? "stiMobileViewerDropDownListOver" :
            (this.isEnabled ? (this.isOver ? "stiMobileViewerDropDownListOver" : "stiMobileViewerDropDownList") : "stiMobileViewerDropDownListDisabled");
    }
    
    dropDownList.setKey = function (key) {        
        this.key = key;
        for (var itemName in this.items)
            if (key == this.items[itemName].key) {
                this.textBox.value = this.items[itemName].caption;
                if (this.image) this.image.style.background = "url(" + this.jsObject.options.images[this.items[itemName].imageName] + ")";
                return;                
            }            
        this.textBox.value = key.toString();
    }
    
    dropDownList.haveKey = function (key) {
        for (var num in this.items)
            if (this.items[num].key == key) return true;
        return false;
    }
    
    dropDownList.action = function () {} 
            
    return dropDownList;
}

StiMobileViewer.prototype.DropDownListMenu = function (dropDownList) {
    var menu = this.VerticalMenu(dropDownList.name, dropDownList.button, "Down", dropDownList.items, this.GetStyles("MenuStandartItem"));
    menu.dropDownList = dropDownList;
    menu.innerContent.style.minWidth = dropDownList.fullWidth + "px";

    menu.changeVisibleState = function (state) {
        var mainClassName = "stiMobileViewerMainPanel";
        if (state) {
            this.onshow();
            this.style.display = "";        
            this.visible = true;
            this.style.overflow = "hidden";
            this.parentButton.dropDownList.setSelected(true);
            this.parentButton.setSelected(true);
            this.jsObject.options.currentDropDownListMenu = this;
            this.style.width = this.innerContent.offsetWidth + "px";
            this.style.height = this.innerContent.offsetHeight + "px";
            this.style.left = (this.jsObject.FindPosX(this.parentButton.dropDownList, mainClassName)) + "px";
            this.style.top = (this.jsObject.FindPosY(this.parentButton.dropDownList, mainClassName) + this.parentButton.offsetHeight + 3) + "px";
            this.innerContent.style.top = -this.innerContent.offsetHeight + "px";

            d = new Date();
            var endTime = d.getTime() + this.jsObject.options.menuAnimDuration;
            this.jsObject.ShowAnimationVerticalMenu(this, 0, endTime);
        }
        else {
            clearTimeout(this.innerContent.animationTimer);
            this.visible = false;
            this.parentButton.dropDownList.setSelected(false);
            this.parentButton.setSelected(false);
            this.style.display = "none";
            if (this.jsObject.options.currentDropDownListMenu == this) this.jsObject.options.currentDropDownListMenu = null;
        }
    }

    menu.onmousedown = function () {
        if (!this.isTouchStartFlag) this.ontouchstart(true);
    }

    menu.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.dropDownListMenuPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }    
    
    menu.action = function(menuItem) {        
        this.changeVisibleState(false);
        this.dropDownList.key = menuItem.key;
        this.dropDownList.textBox.value = menuItem.caption.innerHTML;
        if (this.dropDownList.image) this.dropDownList.image.style.background = "url(" + this.jsObject.options.images[menuItem.imageName] + ")";
        this.dropDownList.action();
    }
    
    menu.onshow = function() {        
        if (this.dropDownList.key == null) return;        
        for (var itemName in this.items) { 
            if (this.dropDownList.key == this.items[itemName].key) {            
                this.items[itemName].setSelected(true);
                return;
            }
            else
                if (!this.items[itemName].isSeparator) this.items[itemName].setSelected(false); 
        }
    }
    
    return menu; 
}