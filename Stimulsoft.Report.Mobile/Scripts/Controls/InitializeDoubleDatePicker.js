﻿
StiMobileViewer.prototype.InitializeDoubleDatePicker = function (params) {
    if (this.options.doubleDatePicker) {
        this.options.mainPanel.removeChild(this.options.doubleDatePicker);
    }

    var datePicker = this.BaseMenu(null, params.secondParentButton, "Down");
    datePicker.style.fontFamily = this.options.toolbar.fontFamily;
    if (this.options.toolbar.fontColor != "") datePicker.style.color = this.options.toolbar.fontColor;
    datePicker.style.zIndex = "36";
    datePicker.dayButtons = [];
    datePicker.showTime = false;
    datePicker.key = new Date();
    this.options.doubleDatePicker = datePicker;
    this.options.mainPanel.appendChild(datePicker);

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "4px";
    innerTable.style.border = "1px dotted #c6c6c6";
    datePicker.innerContent.appendChild(innerTable);

    //First DatePicker
    var firstDatePicker = this.InitializeDatePicker(datePicker);
    firstDatePicker.ownerValue = params.firstOwnerValue;
    firstDatePicker.showTime = params.showTime;
    firstDatePicker.parentDataControl = params.firstParentDataControl;
    firstDatePicker.parentButton = params.firstParentButton;

    //Second DatePicker
    var secondDatePicker = this.InitializeDatePicker(datePicker);
    secondDatePicker.ownerValue = params.secondOwnerValue;
    secondDatePicker.showTime = params.showTime;
    secondDatePicker.parentDataControl = params.secondParentDataControl;
    secondDatePicker.parentButton = params.secondParentButton;

    //Add Pickers to Double Picker Panel
    firstDatePicker.innerContent.className = "";
    secondDatePicker.innerContent.className = "";
    firstDatePicker.innerContent.style.margin = "4px";
    secondDatePicker.innerContent.style.margin = "4px";
    innerTable.addCell(firstDatePicker.innerContent);
    innerTable.addCell(secondDatePicker.innerContent).style.borderLeft = "1px dotted #c6c6c6";

    var container = document.createElement("div");
    innerTable.addCell(container).style.borderLeft = "1px dotted #c6c6c6";

    container.jsObject = this;
    container.style.width = "150px";
    container.style.height = "200px";
    container.style.overflow = "auto";
    container.style.margin = "4px";

    var locNames = {
        CurrentMonth: "Current Month",
        CurrentQuarter: "Current Quarter",
        CurrentWeek: "Current Week",
        CurrentYear: "Current Year",
        NextMonth: "Next Month",
        NextQuarter: "Next Quarter",
        NextWeek: "Next Week",
        NextYear: "Next Year",
        PreviousMonth: "Previous Month",
        PreviousQuarter: "Previous Quarter",
        PreviousWeek: "Previous Week",
        PreviousYear: "Previous Year",
        FirstQuarter: "First Quarter",
        SecondQuarter: "Second Quarter",
        ThirdQuarter: "Third Quarter",
        FourthQuarter: "Fourth Quarter",
        MonthToDate: "Month To Date",
        QuarterToDate: "Quarter To Date",
        WeekToDate: "Week To Date",
        YearToDate: "Year To Date",
        Today: "Today",
        Tomorrow: "Tomorrow",
        Yesterday: "Yesterday"
    };

    for (var i = 0; i < this.options.dateRanges.length; i++) {
        var dateRangeName = this.options.dateRanges[i];
        var item = this.StandartSmallButton(null, null, this.loc.DatePickerRanges ? this.loc.DatePickerRanges[dateRangeName] : locNames[dateRangeName]);
        item.name = dateRangeName;
        container.appendChild(item);

        item.action = function () {
            var values = datePicker.getValuesByDateRangeName(this.name);
            if (values) datePicker.setValuesToDatePickers(values[0], values[1]);
        }
    }

    datePicker.onshow = function () {
        firstDatePicker.onshow();
        secondDatePicker.onshow();
    }

    datePicker.setValuesToDatePickers = function (value1, value2) {
        firstDatePicker.key = value1;
        secondDatePicker.key = value2;
        firstDatePicker.fill();
        secondDatePicker.fill();
        firstDatePicker.action();
        secondDatePicker.action();
    }

    datePicker.getValuesByDateRangeName = function (dateRangeName) {
        var now = new Date();

        var setTimeInterval = function (firstDate, secondDate) {
            firstDate.setHours(0);
            firstDate.setMinutes(0);
            firstDate.setSeconds(0);
            secondDate.setHours(23);
            secondDate.setMinutes(59);
            secondDate.setSeconds(59);
        }

        var getWeekInterval = function (date) {
            var startDay = datePicker.jsObject.GetFirstDayOfWeek();
            var dayWeek = startDay == 0 ? now.getDay() : now.getDay() - 1;
            if (dayWeek < 0) dayWeek = 6;
            var values = [new Date(now.valueOf() - dayWeek * 86400000)];
            values.push(new Date(values[0].valueOf() + 6 * 86400000));
            setTimeInterval(values[0], values[1]);

            return values;
        }

        var firstDate = new Date();
        var secondDate = new Date();
        setTimeInterval(firstDate, secondDate);

        var values = [firstDate, secondDate];

        switch (dateRangeName) {
            case "CurrentMonth":
                {
                    values[0].setDate(1);
                    values[1].setDate(this.jsObject.GetCountDaysOfMonth(now.getFullYear(), now.getMonth()));
                    break;
                }
            case "CurrentQuarter":
                {
                    var firstMonth = parseInt(now.getMonth() / 3) * 3;
                    values[0].setMonth(firstMonth);
                    values[1].setMonth(firstMonth + 2);
                    values[0].setDate(1);
                    values[1].setDate(this.jsObject.GetCountDaysOfMonth(now.getFullYear(), firstMonth + 2));
                    break;
                }
            case "CurrentWeek":
                {
                    values = getWeekInterval(now);
                    break;
                }
            case "CurrentYear":
                {
                    values[0].setMonth(0);
                    values[0].setDate(1);
                    values[1].setMonth(11);
                    values[1].setDate(31);
                    break;
                }
            case "NextMonth":
                {
                    var month = now.getMonth() + 1;
                    var year = now.getFullYear();
                    if (month > 11) {
                        month = 0;
                        year++;
                    }
                    values[0].setYear(year);
                    values[0].setMonth(month);
                    values[0].setDate(1);
                    values[1].setYear(year);
                    values[1].setMonth(month);
                    values[1].setDate(this.jsObject.GetCountDaysOfMonth(year, month));
                    break;
                }
            case "NextQuarter":
                {
                    var year = now.getFullYear();
                    var firstMonth = parseInt(now.getMonth() / 3) * 3 + 3;
                    if (firstMonth > 11) {
                        firstMonth = 0;
                        year++;
                    }
                    values[0].setYear(year);
                    values[1].setYear(year);
                    values[0].setMonth(firstMonth);
                    values[1].setMonth(firstMonth + 2);
                    values[0].setDate(1);
                    values[1].setDate(this.jsObject.GetCountDaysOfMonth(year, firstMonth + 2));
                    break;
                }
            case "NextWeek":
                {
                    values = getWeekInterval(now);
                    values[0] = new Date(values[0].valueOf() + 7 * 86400000);
                    values[1] = new Date(values[1].valueOf() + 7 * 86400000);
                    break;
                }
            case "NextYear":
                {
                    values[0].setYear(now.getFullYear() + 1);
                    values[1].setYear(now.getFullYear() + 1);
                    values[0].setMonth(0);
                    values[1].setMonth(11);
                    values[0].setDate(1);
                    values[1].setDate(31);
                    break;
                }
            case "PreviousMonth":
                {
                    var month = now.getMonth() - 1;
                    var year = now.getFullYear();
                    if (month < 0) {
                        month = 11;
                        year--;
                    }
                    values[0].setYear(year);
                    values[0].setMonth(month);
                    values[0].setDate(1);
                    values[1].setYear(year);
                    values[1].setMonth(month);
                    values[1].setDate(this.jsObject.GetCountDaysOfMonth(year, month));
                    break;
                }
            case "PreviousQuarter":
                {
                    var year = now.getFullYear();
                    var firstMonth = parseInt(now.getMonth() / 3) * 3 - 3;
                    if (firstMonth < 0) {
                        firstMonth = 9;
                        year--;
                    }
                    values[0].setYear(year);
                    values[1].setYear(year);
                    values[0].setMonth(firstMonth);
                    values[1].setMonth(firstMonth + 2);
                    values[0].setDate(1);
                    values[1].setDate(this.jsObject.GetCountDaysOfMonth(year, firstMonth + 2));
                    break;
                }
            case "PreviousWeek":
                {
                    values = getWeekInterval(now);
                    values[0] = new Date(values[0].valueOf() - 7 * 86400000);
                    values[1] = new Date(values[1].valueOf() - 7 * 86400000);
                    break;
                }
            case "PreviousYear":
                {
                    values[0].setYear(now.getFullYear() - 1);
                    values[1].setYear(now.getFullYear() - 1);
                    values[0].setMonth(0);
                    values[1].setMonth(11);
                    values[0].setDate(1);
                    values[1].setDate(31);
                    break;
                }
            case "FirstQuarter":
                {
                    values[0].setMonth(0);
                    values[1].setMonth(2);
                    values[0].setDate(1);
                    values[1].setDate(this.jsObject.GetCountDaysOfMonth(now.getFullYear(), 2));
                    break;
                }
            case "SecondQuarter":
                {
                    values[0].setMonth(3);
                    values[1].setMonth(5);
                    values[0].setDate(1);
                    values[1].setDate(this.jsObject.GetCountDaysOfMonth(now.getFullYear(), 5));
                    break;
                }
            case "ThirdQuarter":
                {
                    values[0].setMonth(6);
                    values[1].setMonth(8);
                    values[0].setDate(1);
                    values[1].setDate(this.jsObject.GetCountDaysOfMonth(now.getFullYear(), 8));
                    break;
                }
            case "FourthQuarter":
                {
                    values[0].setMonth(9);
                    values[1].setMonth(11);
                    values[0].setDate(1);
                    values[1].setDate(this.jsObject.GetCountDaysOfMonth(now.getFullYear(), 11));
                    break;
                }
            case "MonthToDate":
                {
                    values[0].setDate(1);
                    break;
                }
            case "QuarterToDate":
                {
                    var firstMonth = parseInt(now.getMonth() / 3) * 3;
                    values[0].setMonth(firstMonth);
                    values[0].setDate(1);
                    break;
                }
            case "WeekToDate":
                {
                    var weekValues = getWeekInterval(now);
                    values[0] = weekValues[0];
                    break;
                }
            case "YearToDate":
                {
                    values[0].setMonth(0);
                    values[0].setDate(1);
                    break;
                }
            case "Today":
                {
                    break;
                }
            case "Tomorrow":
                {
                    values[0] = new Date(values[0].valueOf() + 86400000);
                    values[1] = new Date(values[1].valueOf() + 86400000);
                    break;
                }
            case "Yesterday":
                {
                    values[0] = new Date(values[0].valueOf() - 86400000);
                    values[1] = new Date(values[1].valueOf() - 86400000);
                    break;
                }
        }

        return values;
    }

    return datePicker;
}