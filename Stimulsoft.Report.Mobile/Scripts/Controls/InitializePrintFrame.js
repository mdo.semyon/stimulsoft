
StiMobileViewer.prototype.InitializePrintFrame = function () {
    var printFrame = document.createElement("iframe");
    printFrame.jsObject = this;
    printFrame.style.width = "1px";
    printFrame.style.height = "1px";
    printFrame.style.border = "none";
    printFrame.setAttribute("name", this.options.mobileViewer.id + "printBuffer");
    this.options.printFrame = printFrame;
    this.options.mainPanel.appendChild(printFrame);

    printFrame.addPages = function (doc) {
        if (this.jsObject.options.pagesArrayForPrint == null) return;
        doc.body.innerHTML = "";
        var count = this.jsObject.getCountObjects(this.jsObject.options.pagesArrayForPrint);

        //add pages styles
        var css = doc.createElement("STYLE");
        css.setAttribute('type', 'text/css');
        this.head = doc.getElementsByTagName("head")[0];
        this.head.appendChild(css);
        if (css.styleSheet) css.styleSheet.cssText = this.jsObject.options.pagesArrayForPrint[count - 2];
        else css.innerHTML = this.jsObject.options.pagesArrayForPrint[count - 2];

        //add pages attributes
        for (num = 0; num < count - 2; num++) {
            var page = doc.createElement("DIV");
            doc.body.appendChild(page);
            var pageAttributes = this.jsObject.options.pagesArrayForPrint[num];
            page.style.display = "table";
            page.style.padding = pageAttributes["margins"];
            page.style.background = pageAttributes["background"];
            page.innerHTML = pageAttributes["content"];

            //Correct Watermark
            for (var i = 0; i < page.childNodes.length; i++) {
                if (page.childNodes[i].style && page.childNodes[i].style.backgroundImage) {
                    page.style.backgroundImage = page.childNodes[i].style.backgroundImage;
                    page.childNodes[i].style.backgroundImage = "";
                    if (this.jsObject.options.reportDisplayMode == "Div" || this.jsObject.options.reportDisplayMode == "Span") {
                        page.childNodes[i].style.backgroundColor = "transparent";
                    }
                    break;
                }
            }
        }
    }

    printFrame.printDirect = function () {
        var doc = this.contentWindow.document;
        this.addPages(doc);
        var printFrameId = this.jsObject.options.mobileViewer.id + "printBuffer";

        if (window.frames[printFrameId]) {
            window.frames[printFrameId].focus();
            window.frames[printFrameId].print();
        }
        else {
            document[printFrameId].focus();
            document[printFrameId].print();
        }
    }

    printFrame.printWithPreview = function () {
        if (this.jsObject.options.newWindow) setTimeout(function () {
            printFrame.addPages(printFrame.jsObject.options.newWindow.document)
        }, 1000);
    }

    return printFrame;
}