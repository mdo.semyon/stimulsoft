﻿
StiMobileViewer.prototype.BigMaterialButton = function (name, groupName, caption, imageName, toolTip, needUnderline, needIconUnderline) {
    var button = document.createElement("div");
    button.jsObject = this;
    button.name = name != null ? name : this.generateKey();
    button.id = button.name;
    if (name != null) this.options.buttons[name] = button;
    button.groupName = groupName;
    button.isEnabled = true;
    button.isSelected = false;
    button.isOver = false;
    button.arrow = null;
    button.style="-moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;-o-user-select:none;" 
    button.unselectable="on"
    button.onselectstart = function () { return false; };
    button.onmousedown = function () { return false; };
    button.className = "bigMaterialButton";
    button.toolTip = toolTip;
    if (toolTip && typeof (toolTip) != "object") { button.setAttribute("title", toolTip); }

    var innerTable = this.CreateHTMLTable();
    button.innerTable = innerTable;
    innerTable.style.height = "100%";
    innerTable.style.width = "100%";
    button.appendChild(innerTable);
    //var pRatio = window.devicePixelRatio || 1;
    var ratio = Math.min(screen.width, screen.height);// / (4 - pRatio);


    if (imageName) {
        button.image = document.createElement("img");
        var srcset = imageName.substr(0, imageName.indexOf("."));
        var x2 = this.options.images[srcset + "2x.png"] ? ", " + this.options.images[srcset + "2x.png"] + " 2x" : "";
        var x3 = this.options.images[srcset + "3x.png"] ? ", " + this.options.images[srcset + "3x.png"] + " 3x" : "";
        button.image.srcset = this.options.images[imageName] + " 1x " + x2 + x3;
        //if (imageName != true) button.image.src = this.options.images[imageName];
        button.image.style.width = "24px";//0.07 * ratio + "px";
        button.image.style.height = "24px";//0.07 * ratio + "px";
        button.cellImage = innerTable.addCell(button.image);
        button.cellImage.style.width = "1px";
        button.cellImage.style.padding = 0.04 * ratio + "px";
    }

    button.caption = innerTable.addCell();
    button.caption.style.lineHeight = "1";
    button.caption.style.fontSize = 0.04 * ratio + "px";
    button.caption.style.fontColor = "gray";
    button.caption.innerHTML = caption;
    if (caption) {
        button.caption.style.paddingRight = "10px";
        button.caption.style.fontFamily = this.options.toolbarFontFamily;
        button.caption.style.color = this.options.toolbarFontColor;
    }
    if (needUnderline) {
        if (needIconUnderline) {
            innerTable.addCellInNextRow().style.borderTop = "1px solid #EEEEEE";;
        } else {
            innerTable.addCellInNextRow();
        }        
        innerTable.addCellInLastRow().style.borderTop = "1px solid #EEEEEE";
    }
    var ontouchstart = function (this_, e) {
        if (!this_.isEnabled) return;
        this_.touchPosition = { x: e.changedTouches[0].clientX, y: e.changedTouches[0].clientY };
        //this_.className = this_.className + " touched";
        this_.touched = true;
    }

    var ontouchend = function (this_, e) {        
        if (this_.touched) {
            this_.touched = false;
            if (!this_.isEnabled) return;
            if (!this_.isSelected) {
                //this_.className = this_.className.stiReplaceAll(" touched", "");
            }
            if (e.currentTarget == this_) {
                this_.action();
            }
        }
    }

    button.ontouchstart = function (e) { ontouchstart(this, e) };
    button.ontouchenter = function (e) { ontouchstart(this, e) };
    button.ontouchmove = function (e) {
        if (!this.isEnabled) return;
        if (this.touched && (Math.abs(e.changedTouches[0].clientX - this.touchPosition.x) > 10 || Math.abs(e.changedTouches[0].clientY - this.touchPosition.y) > 10)) {
            this.touched = false;
            //this.className = this.className.stiReplaceAll(" touched", "");
        }
    }
    button.ontouchend = function (e) { ontouchend(this, e); }
    button.ontouchleave = function (e) { ontouchend(this, e); }
    button.ontouchcancel = function (e) { ontouchend(this, e); }

    var this_ = this;
    button.show = function () {
        if (!button.visible) {
            this_.animate(button, { duration: 150, animations: [{ style: "opacity", start: parseFloat(button.style.opacity), end: 1, postfix: "" }] });
            button.visible = true;
        }
    }

    button.hide = function (duration) {
        if (button.visible) {
            this_.animate(button, { duration: duration | 150, animations: [{ style: "opacity", start: parseFloat(button.style.opacity), end: 0, postfix: "" }] });
            button.visible = false;
        }
    }

    button.action = function () { };
    button.setEnabled = function (state, notVisible) {
        this.isEnabled = state;
        if (notVisible && !state) return;
        if (this.image) this.image.style.opacity = state ? "1" : "0.3";
        if (!state && !this.isOver) this.isOver = false;
    }
    button.setSelected = function (value) {
        if (button.isSelected == value);
        button.isSelected = value;
        if (value) {
            button.className = button.className + " bmbselected";
        } else {
            button.className = button.className.stiReplaceAll(" bmbselected", "");
        }
    }


    return button;
}
