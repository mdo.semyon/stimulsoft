﻿
StiMobileViewer.prototype.FormButton = function (name, caption, imageName, minWidth) {
    var button = this.SmallButton(name, null, caption || "", imageName, null, null, this.GetStyles("FormButton"));
    button.style.height = this.options.isTouchDevice ? "29px" : "23px";
    button.innerTable.style.width = "100%";
    button.style.minWidth = (minWidth || 80) + "px";
    button.caption.style.textAlign = "center";
    
    return button;
}