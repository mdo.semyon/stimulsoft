﻿
StiMobileViewer.prototype.FontList = function (name, width, height, cutMenu, toolTip) {
    var items = [];
    for (var num in this.options.fontNames)
        items.push(this.Item("fontItem" + num, this.options.fontNames[num].value, null, this.options.fontNames[num].value));
    var fontList = this.DropDownList(name, width, toolTip ? toolTip : this.loc.HelpDesigner.FontName, items, true, false, height, cutMenu);
    
    return fontList;
}