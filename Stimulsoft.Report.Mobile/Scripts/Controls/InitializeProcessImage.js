﻿var GlobalProcessImage;

StiMobileViewer.prototype.InitializeProcessImage = function () {
    var processImage = this.Progress();
    processImage.jsObject = this;
    processImage.style.display = "none";
    this.options.processImage = processImage;
    this.options.mainPanel.appendChild(processImage);

    processImage.show = function () {
        this.style.display = "";
        if (this.jsObject.options.cloudMode || this.jsObject.options.designerMode) {
            this.style.left = "calc(50% - 40px)";
            this.style.top = "calc(50% - 100px)";
        }
        else
            this.jsObject.SetObjectToCenter(this);

    }

    processImage.hide = function () {
        this.style.display = "none";
    }

    if (this.options.demoMode) {
        processImage.show();
        GlobalProcessImage = processImage;
        window.onload = function () { if (GlobalProcessImage) GlobalProcessImage.hide(); }
    }

    return processImage;
}

StiMobileViewer.prototype.InitializeCenterText = function () {
    var progressContainer = document.createElement("div");
    progressContainer.style.position = "absolute";
    progressContainer.style.zIndex = "1000";
    var progress = document.createElement("div");
    progressContainer.appendChild(progress);
    progressContainer.text = progress;
    progressContainer.style.fontFamily = this.options.toolbarFontFamily;
    progressContainer.style.color = this.options.toolbarFontColor;
    progressContainer.style.textShadow = "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000";
    progressContainer.style.fontSize = "100px";

    var processImage = progressContainer;
    processImage.jsObject = this;
    processImage.style.display = "none";
    this.options.centerText = processImage;
    this.options.mainPanel.appendChild(processImage);

    processImage.show = function () {
        this.style.display = "";
        if (this.jsObject.options.cloudMode || this.jsObject.options.designerMode) {
            this.jsObject.SetObjectToCenterReportPanel(this);
        }
        else
            this.jsObject.SetObjectToCenter(this);

    }

    processImage.hide = function () {
        this.style.display = "none";
    }

    processImage.setText = function (text) {
        processImage.text.innerHTML = text;
        processImage.show();
    }

    return processImage;
}