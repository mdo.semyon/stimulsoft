﻿
StiMobileViewer.prototype.CreateParameter = function (params) {
    var parameter = this.CreateHTMLTable();
    this.options.parameters[params.name] = parameter;
    parameter.params = params;
    parameter.controls = {};
    parameter.jsObject = this;
    parameter.params.isNull = false;
    parameter.menu = null;

    parameter.addCell = function (control) {
        var cell = document.createElement("td");
        cell.style.height = parameter.jsObject.options.parameterRowHeight + "px";
        cell.style.padding = "0px 2px 0 2px";
        this.tr[this.tr.length - 1].appendChild(cell);
        if (control) cell.appendChild(control);

        return cell;
    }

    parameter.oldAddCellInNextRow = parameter.addCellInNextRow;
    parameter.addCellInNextRow = function (control) {
        var cell = this.oldAddCellInNextRow(control);
        cell.style.height = parameter.jsObject.options.parameterRowHeight + "px";
        cell.style.padding = "0px 2px 0 2px";

        return cell;
    }

    parameter.oldAddCellInLastRow = parameter.addCellInLastRow;
    parameter.addCellInLastRow = function (control) {
        var cell = this.oldAddCellInLastRow(control);
        cell.style.height = parameter.jsObject.options.parameterRowHeight + "px";
        cell.style.padding = "0px 2px 0 2px";

        return cell;
    }

    var showParameterInTwoRows = parameter.params.basicType == "Range" && this.options.designerMode;

    //boolCheckBox
    if (parameter.params.type == "Bool" && (parameter.params.basicType == "Value" || parameter.params.basicType == "NullableValue"))
        parameter.addCell(this.CreateBoolCheckBox(parameter));
    //labelFrom
    if (parameter.params.basicType == "Range") parameter.addCell().innerHTML = this.loc.PropertyMain.RangeFrom;
    //firstTextBox
    if (parameter.params.type != "Bool" || parameter.params.basicType == "List") parameter.addCell(this.CreateFirstTextBox(parameter));
    //firstDateTimeButton    
    if (parameter.params.type == "DateTime" && parameter.params.allowUserValues && parameter.params.basicType != "List" && parameter.params.basicType != "Range")
        parameter.addCell(this.CreateFirstDateTimeButton(parameter));
    //firstGuidButton
    if (parameter.params.type == "Guid" && parameter.params.allowUserValues && parameter.params.basicType != "List") parameter.addCell(this.CreateFirstGuidButton(parameter));
    if (this.options.isMaterial && parameter.params.basicType == "Range") {
        parameter.addRow();
    }

    //labelTo
    if (parameter.params.basicType == "Range") {
        var cellRangeTo = showParameterInTwoRows ? parameter.addCellInNextRow() : parameter.addCell();
        cellRangeTo.innerHTML = this.loc.PropertyMain.RangeTo;
    }
    //secondTextBox
    if (parameter.params.basicType == "Range") parameter.addCellInLastRow(this.CreateSecondTextBox(parameter));
    //secondDateTimeButton
    if (parameter.params.basicType == "Range" && parameter.params.type == "DateTime" && parameter.params.allowUserValues) parameter.addCellInLastRow(this.CreateSecondDateTimeButton(parameter));
    //secondGuidButton
    if (parameter.params.basicType == "Range" && parameter.params.type == "Guid" && parameter.params.allowUserValues) parameter.addCellInLastRow(this.CreateSecondGuidButton(parameter));
    //dropDownButton
    if (parameter.params.items != null || (parameter.params.basicType == "List" && parameter.params.allowUserValues)) parameter.addCellInLastRow(this.CreateDropDownButton(parameter));
    //nullableCheckBox
    if (parameter.params.basicType == "NullableValue" && parameter.params.allowUserValues) parameter.addCellInLastRow(this.CreateNullableCheckBox(parameter));
    //nullableText
    if (parameter.params.basicType == "NullableValue" && parameter.params.allowUserValues) {
        var nullableCell = parameter.addCellInLastRow();
        nullableCell.innerHTML = "Null";
        nullableCell.style.padding = "0px";
    }

    parameter.setEnabled = function (state) {
        this.params.isNull = !state;
        for (var controlName in this.controls) { this.controls[controlName].setEnabled(state); }
    }

    parameter.changeVisibleStateMenu = function (state) {
        if (state) {
            var menu = null;
            switch (this.params.basicType) {
                case "Value":
                case "NullableValue": { menu = this.jsObject.parameterMenuForValue(this); break; }
                case "Range": { menu = this.jsObject.parameterMenuForRange(this); break; }
                case "List": { menu = (this.params.allowUserValues) ? this.jsObject.parameterMenuForEditList(this) : this.jsObject.parameterMenuForNotEditList(this); break; }
            }

            if (menu != null) {
                menu.changeVisibleState(true);
            }
        }
        else {
            if (parameter.menu != null) {
                if (parameter.params.allowUserValues && parameter.params.basicType == "List") parameter.menu.updateItems();
                parameter.menu.changeVisibleState(false);
            }
        }
    }

    parameter.getStringDateTime = function (object) {
        return object.month + "/" + object.day + "/" + object.year + " " +
            (object.hours > 12 ? object.hours - 12 : object.hours) + ":" + object.minutes + ":" + object.seconds + " " +
            (object.hours < 12 ? "AM" : "PM");
    }

    parameter.getValue = function () {
        var value = null;
        if (parameter.params.isNull) return null;

        if (parameter.params.basicType == "Value" || parameter.params.basicType == "NullableValue") {
            if (parameter.params.type == "Bool") { return parameter.controls.boolCheckBox.isChecked; }
            if (parameter.params.type == "DateTime") { return this.getStringDateTime(parameter.params.key); }
            value = parameter.params.allowUserValues ? parameter.controls.firstTextBox.value : parameter.params.key;
        }

        if (parameter.params.basicType == "Range") {
            value = {};
            value.from = (parameter.params.type == "DateTime") ? this.getStringDateTime(parameter.params.key) : parameter.controls.firstTextBox.value;
            value.to = (parameter.params.type == "DateTime") ? this.getStringDateTime(parameter.params.keyTo) : parameter.controls.secondTextBox.value;
        }

        if (parameter.params.basicType == "List") {
            value = []
            if (parameter.params.allowUserValues)
                for (var index in parameter.params.items) value[index] =
                    (parameter.params.type == "DateTime")
                        ? this.getStringDateTime(parameter.params.items[index].key)
                        : parameter.params.items[index].key;
            else {
                num = 0;
                for (var index in parameter.params.items)
                    if (parameter.params.items[index].isChecked) {
                        value[num] = (parameter.params.type == "DateTime")
                            ? this.getStringDateTime(parameter.params.items[index].key)
                            : parameter.params.items[index].key;
                        num++;
                    }
            }
        }

        return value;
    };

    //Methods For Stimulsoft Reports.Server

    parameter.getDateTimeForReportServer = function (value) {
        var date = new Date(value.year, value.month - 1, value.day, value.hours, value.minutes, value.seconds);
        date.setUTCFullYear(value.year);
        date.setUTCMonth(value.month - 1);
        date.setUTCDate(value.day);
        date.setUTCHours(value.hours, value.minutes, value.seconds);

        return (parameter.jsObject.options.const_dateTime1970InTicks + date * 10000).toString();
    }

    parameter.getTimeSpanForReportServer = function (value) {
        var jsObject = parameter.jsObject;

        var timeArray = value.split(":");
        var daysHoursArray = timeArray[0].split(".");
        var days = (daysHoursArray.length > 1) ? jsObject.StrToInt(daysHoursArray[0]) : 0;
        var hours = jsObject.StrToInt((daysHoursArray.length > 1) ? daysHoursArray[1] : daysHoursArray[0]);
        var minutes = (timeArray.length > 1) ? jsObject.StrToInt(timeArray[1]) : 0;
        var seconds = (timeArray.length > 2) ? jsObject.StrToInt(timeArray[2]) : 0;

        return ((days * 86400000 + hours * 3600000 + minutes * 60000 + seconds * 1000) * 10000).toString();
    }

    parameter.getSingleValueForReportServer = function () {
        var value = null;
        if (parameter.params.isNull) return null;

        if (parameter.params.basicType == "Value" || parameter.params.basicType == "NullableValue") {
            if (parameter.params.type == "Bool") { return parameter.controls.boolCheckBox.isChecked ? "True" : "False" }
            if (parameter.params.type == "DateTime") { return parameter.getDateTimeForReportServer(parameter.params.key); }
            value = parameter.params.allowUserValues ? parameter.controls.firstTextBox.value : parameter.params.key;
            if (parameter.params.type == "TimeSpan") { value = parameter.getTimeSpanForReportServer(value); }
        }

        return value;
    };

    parameter.getRangeValuesForReportServer = function () {
        var values = {};
        values.from = (parameter.params.type == "DateTime")
            ? parameter.getDateTimeForReportServer(parameter.params.key)
            : (parameter.params.type == "TimeSpan") ? parameter.getTimeSpanForReportServer(parameter.controls.firstTextBox.value) : parameter.controls.firstTextBox.value;

        values.to = (parameter.params.type == "DateTime")
            ? parameter.getDateTimeForReportServer(parameter.params.keyTo)
            : (parameter.params.type == "TimeSpan") ? parameter.getTimeSpanForReportServer(parameter.controls.secondTextBox.value) : parameter.controls.secondTextBox.value;

        return values;
    };

    parameter.getListValuesForReportServer = function () {
        var values = [];
        var num = 0;

        for (var index in parameter.params.items) {
            var valuesItem = {};
            valuesItem.Ident = "Single";

            if (parameter.params.allowUserValues || (!parameter.params.allowUserValues && parameter.params.items[index].isChecked)) {
                valuesItem.Value = (parameter.params.type == "DateTime")
                    ? parameter.getDateTimeForReportServer(parameter.params.items[index].key)
                    : (parameter.params.type == "TimeSpan")
                        ? parameter.getTimeSpanForReportServer(parameter.params.items[index].key)
                        : parameter.params.items[index].key;
                valuesItem.Type = (valuesItem.Value == null) ? null : parameter.getSingleType();
                values.push(valuesItem);
            }
        }

        return values;
    };

    parameter.getParameterObjectForReportServer = function () {
        var parameterObject = {}
        parameterObject.Ident = parameter.params.basicType.indexOf("Value") != -1 ? "Single" : parameter.params.basicType;
        parameterObject.Name = parameter.params.name;

        switch (parameterObject.Ident) {
            case "Single": 
                {
                    parameterObject.Value = parameter.getSingleValueForReportServer();
                    parameterObject.Type = (parameterObject.Value == null) ? null : parameter.getSingleType();
                    break;
                }
            case "Range": 
                {
                    var values = parameter.getRangeValuesForReportServer();
                    parameterObject.FromValue = values.from;
                    parameterObject.ToValue = values.to;
                    parameterObject.RangeType = parameter.params.type + "Range";
                    parameterObject.FromType = (parameterObject.FromValue == null) ? null : parameter.getSingleType();
                    parameterObject.ToType = (parameterObject.ToValue == null) ? null : parameter.getSingleType();
                    break;
                }
            case "List": 
                {
                    parameterObject.ListType = parameter.params.type + "List";
                    parameterObject.Values = parameter.getListValuesForReportServer();
                    break;
                }
        }

        return parameterObject;
    };

    parameter.getSingleType = function () {
        var type = parameter.params.type;
        if (type != "DateTime" && type != "TimeSpan" && type != "Guid" && type != "Decimal") return type.toLowerCase();

        return type;
    }

    return parameter;
}

// ---------------------  Controls   ----------------------------

//boolCheckBox
StiMobileViewer.prototype.CreateBoolCheckBox = function (parameter) {
    var checkBox = this.ParameterCheckBox(parameter);
    parameter.controls.boolCheckBox = checkBox;
    checkBox.setChecked(parameter.params.value == "true" || parameter.params.value == "True");
    checkBox.setEnabled(parameter.params.allowUserValues);

    return checkBox;
}

//firstTextBox
StiMobileViewer.prototype.CreateFirstTextBox = function (parameter) {
    var textBox = this.ParameterTextBox(parameter);
    parameter.controls.firstTextBox = textBox;
    textBox.setReadOnly(parameter.params.basicType == "List" || !parameter.params.allowUserValues)

    //Value
    if (parameter.params.basicType == "Value" || parameter.params.basicType == "NullableValue") {
        if (parameter.params.type == "DateTime" && !parameter.params.value) {            
            parameter.params.key = this.getDateTimeObject(new Date(0, 0, 1, 0, 0, 0, 0));
        }

        textBox.value =
            (parameter.params.type == "DateTime")
            ? this.getStringKey(parameter.params.key, parameter)
            : parameter.params.value;
    }

    //Range
    if (parameter.params.basicType == "Range") {
        if (parameter.params.type == "DateTime" && parameter.params.key && parameter.params.key.isNull) {
            parameter.params.key = this.getDateTimeObject(new Date(0, 0, 1, 0, 0, 0, 0));
        }

        textBox.value = this.getStringKey(parameter.params.key, parameter);
    }
        
    //List
    if (parameter.params.basicType == "List") {
        for (var index in parameter.params.items) {
            var isChecked = true;
            if (parameter.params.value instanceof Array && !parameter.params.value.contains(parameter.params.items[index].value)) isChecked = false;
                        
            parameter.params.items[index].isChecked = isChecked;
            if (isChecked) {
                if (textBox.value != "") textBox.value += ";";

                if (parameter.params.allowUserValues)
                    textBox.value += this.getStringKey(parameter.params.items[index].key, parameter);
                else
                    textBox.value += parameter.params.items[index].value != "" ? parameter.params.items[index].value : this.getStringKey(parameter.params.items[index].key, parameter);
            }
        }
    }

    return textBox;
}

//firstDateTimeButton
StiMobileViewer.prototype.CreateFirstDateTimeButton = function (parameter) {
    var dateTimeButton = this.ParameterButton("DateTimeButton", parameter);
    parameter.controls.firstDateTimeButton = dateTimeButton;
    dateTimeButton.action = function () {
        var datePicker = dateTimeButton.jsObject.options.datePicker || dateTimeButton.jsObject.InitializeDatePicker();
        datePicker.ownerValue = this.parameter.params.key;
        datePicker.parentDataControl = this.parameter.controls.firstTextBox;
        datePicker.parentButton = this;
        datePicker.changeVisibleState(!datePicker.visible);
    }

    return dateTimeButton;
}

//firstGuidButton
StiMobileViewer.prototype.CreateFirstGuidButton = function (parameter) {
    var guidButton = this.ParameterButton("GuidButton", parameter);
    parameter.controls.firstGuidButton = guidButton;
    guidButton.action = function () { this.parameter.controls.firstTextBox.value = this.parameter.jsObject.newGuid(); }

    return guidButton;
}

//secondTextBox
StiMobileViewer.prototype.CreateSecondTextBox = function (parameter) {
    var textBox = this.ParameterTextBox(parameter);
    parameter.controls.secondTextBox = textBox;
    textBox.setReadOnly(!parameter.params.allowUserValues);

    if (parameter.params.type == "DateTime" && parameter.params.keyTo && parameter.params.keyTo.isNull) {
        parameter.params.keyTo = this.getDateTimeObject(new Date);
    }
    
    textBox.value = this.getStringKey(parameter.params.keyTo, parameter);

    return textBox;
}

//secondDateTimeButton
StiMobileViewer.prototype.CreateSecondDateTimeButton = function (parameter) {
    var dateTimeButton = this.ParameterButton("DateTimeButton", parameter);
    parameter.controls.secondDateTimeButton = dateTimeButton;

    dateTimeButton.action = function () {
        var datePickerParams = {
            showTime: this.parameter.params.dateTimeType != "Date",
            firstParentDataControl: this.parameter.controls.firstTextBox,
            firstParentButton: this.parameter.controls.firstDateTimeButton,
            firstOwnerValue: this.parameter.params.key,
            secondParentDataControl: this.parameter.controls.secondTextBox,
            secondParentButton: this,
            secondOwnerValue: this.parameter.params.keyTo
        }

        var datePicker = dateTimeButton.jsObject.InitializeDoubleDatePicker(datePickerParams);
        datePicker.changeVisibleState(!datePicker.visible, null, this.jsObject.options.designerMode ? false : true, this.jsObject.options.designerMode ? 245 : 0);
    }

    return dateTimeButton;
}

//secondGuidButton
StiMobileViewer.prototype.CreateSecondGuidButton = function (parameter) {
    var guidButton = this.ParameterButton("GuidButton", parameter);
    parameter.controls.secondGuidButton = guidButton;
    guidButton.action = function () { this.parameter.controls.secondTextBox.value = this.parameter.jsObject.newGuid(); }

    return guidButton;
}

//dropDownButton
StiMobileViewer.prototype.CreateDropDownButton = function (parameter) {
    var dropDownButton = this.ParameterButton("DropDownButton", parameter);
    parameter.controls.dropDownButton = dropDownButton;
    dropDownButton.action = function () { this.parameter.changeVisibleStateMenu(this.parameter.menu == null); }

    return dropDownButton;
}

//nullableCheckBox
StiMobileViewer.prototype.CreateNullableCheckBox = function (parameter) {
    var checkBox = this.ParameterCheckBox(parameter);

    checkBox.onChecked = function () {
        this.parameter.setEnabled(!this.isChecked);
        var textColor = !this.isChecked ? this.jsObject.options.toolbarFontColor : "transparent";

        if (this.parameter.controls.firstTextBox) this.parameter.controls.firstTextBox.style.color = textColor;
        if (this.parameter.controls.secondTextBox) this.parameter.controls.secondTextBox.style.color = textColor;
    }

    return checkBox;
}