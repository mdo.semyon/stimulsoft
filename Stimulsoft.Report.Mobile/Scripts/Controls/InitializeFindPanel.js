﻿
StiMobileViewer.prototype.InitializeFindPanel = function () {
    var findPanel = document.createElement("div");
    
    findPanel.controls = {};
    findPanel.visible = false;
    this.options.findPanel = findPanel;
    this.options.mainPanel.appendChild(findPanel);
    findPanel.jsObject = this;
    findPanel.className = "stiMobileViewerToolBar";
    if (this.options.cloudMode || this.options.designerMode) { findPanel.className += " stiMobileViewerToolBarCloudMode"; }

    var findPanelInnerContent = document.createElement("div");
    findPanel.innerContent = findPanelInnerContent;
    findPanel.appendChild(findPanelInnerContent);
    

    var findPanelBlock = document.createElement("div");
    findPanelInnerContent.appendChild(findPanelBlock);
    findPanelBlock.className = "stiMobileViewerToolBarTable";
    if (!this.options.isMaterial) {
        findPanelInnerContent.style.padding = this.options.cloudMode || this.options.designerMode ? "2px" : "0 3px 3px 3px";
        findPanelBlock.style.width = "auto";
        findPanel.style.display = "none";        
    } 
    if (this.options.cloudMode || this.options.designerMode) { findPanelBlock.style.border = "0px"; }

    if (this.options.demoMode && !this.options.isMaterial) {
        findPanelBlock.style.border = "0px";
        findPanel.style.borderBottom = "1px solid #c6c6c6";
        findPanelInnerContent.style.padding = "4px";
    }

    var controlsTable = this.CreateHTMLTable();
    findPanelBlock.appendChild(controlsTable);

    var controlProps = this.options.isMaterial ?
        [
        ["findTextBox", this.TextBox(null, 170), "2px"],
        ["findPreviows", this.ToolButton(null, null, null, "ArrowUp.png"), "2px"],
        ["findNext", this.ToolButton(null, null, null, "ArrowDown.png"), "2px"],
        ["matchCase", this.ToolButton(null, null, null, "MatchWholeWord.png"), "2px"],
        ["matchWholeWord", this.ToolButton(null, null, null, "MatchCase.png"), "2px"],
        ["close", this.ToolButton(null, null, null, "Close.png"), "2px"]
        ]:
        [
        ["close", this.ToolButton(null, null, null, "Close.png"), "2px"],
        ["text", this.TextBlock(this.loc.FormViewerFind.FindWhat), "2px"],
        ["findTextBox", this.TextBox(null, 170), "2px"],
        ["findPreviows", this.ToolButton(null, null, this.loc.FormViewerFind.FindPrevious, "ArrowUp.png"), "2px"],
        ["findNext", this.ToolButton(null, null, this.loc.FormViewerFind.FindNext, "ArrowDown.png"), "2px"],
        ["matchCase", this.ToolButton(null, null, this.loc.Editor.MatchCase.replace("&", ""), null), "2px"],
        ["matchWholeWord", this.ToolButton(null, null, this.loc.Editor.MatchWholeWord.replace("&", ""), null), "2px"]
    ];

    for (var i = 0; i < controlProps.length; i++) {
        findPanel.controls[controlProps[i][0]] = controlProps[i][1];
        var cell = controlsTable.addCell(controlProps[i][1]);
        controlProps[i][1].style.margin = controlProps[i][2];
        if (this.options.isMaterial) {
            if (i == 0) {
                cell.style.width = "100%";
                cell.style.paddingRight = "8px";
                controlProps[i][1].style.width = "100%";
                controlProps[i][1].style.height = "40px";
            } else {
                controlProps[i][1].imageCell.style.pading = "0px 13px";
            }
        }
    }

    var find = function (direction) {
        if (findPanel.controls.findTextBox.value == "") {
            findPanel.jsObject.hideFindLabels();
            return;
        }
        if (findPanel.jsObject.options.findHelper.lastFindText != findPanel.controls.findTextBox.value || findPanel.jsObject.options.changeFind)
            findPanel.jsObject.showFindLabels(findPanel.controls.findTextBox.value);
        else
            findPanel.jsObject.selectFindLabel(direction);
    }

    findPanel.controls.close.action = function () { findPanel.changeVisibleState(false); }
    findPanel.controls.findTextBox.onkeyup = function (e) { if (e && e.keyCode == 13) find("Next"); }
    /*findPanel.controls.findTextBox.ontouchstart = function (e) {
        $(e.target).focus();
       
    }
    $(findPanel.controls.findTextBox).on('touchstart', function () {
        this.focus();
    });*/
    findPanel.controls.matchCase.action = function () {
        this.setSelected(!this.isSelected);
        this.jsObject.options.changeFind = true;
    }
    findPanel.controls.matchWholeWord.action = function () {
        this.setSelected(!this.isSelected);
        this.jsObject.options.changeFind = true;
    }
    findPanel.controls.findPreviows.action = function () { find("Previows"); }
    findPanel.controls.findNext.action = function () { find("Next"); }
    if (this.options.isMaterial) {
        findPanel.style.top = -findPanel.offsetHeight + "px";
    }

    var duration = 250;
    findPanel.changeVisibleState = function (state) {
        var options = this.jsObject.options;
        if (!state) this.jsObject.hideFindLabels();
        if (!options.isMaterial) {
            this.style.display = state ? "" : "none";
            if (state) {
                if (findPanel.jsObject.options.findHelper)
                    findPanel.jsObject.options.findHelper.lastFindText = "";
                findPanel.controls.findTextBox.value = "";
                findPanel.controls.findTextBox.focus();
            }
            if (options.showFindButton) {
                options.toolbar.controls.Find.setSelected(state);
            }
            if (options.parametersPanel) {
                options.parametersPanel.style.top = (options.showToolBar ? options.toolbar.offsetHeight : 0) + (options.findPanel ? options.findPanel.offsetHeight : 0) + "px";
            }
            if (options.bookmarksPanel) {
                options.bookmarksPanel.style.top = (options.showToolBar ? options.toolbar.offsetHeight : 0) + (options.findPanel ? options.findPanel.offsetHeight : 0) +
                    (options.parametersPanel && !options.cloudMode && !options.designerMode ? options.parametersPanel.offsetHeight : 0) + "px";
            }
            options.reportPanel.style.marginTop = (options.parametersPanel && !options.cloudMode && !options.designerMode ? options.parametersPanel.offsetHeight : 0) +
                    (options.reportPanel.style.position == "absolute" && options.findPanel ? options.findPanel.offsetHeight : 0) + "px";
        } else {
            
            if (state) {
                setTimeout(function () {
                    findPanel.controls.findTextBox.focus();
                    /*var e = {};
                    try {
                        e = document.createEvent('TouchEvent')
                        e.initTouchEvent("touchstart", true, true)
                    }
                    catch (err) {
                        try {
                            e = document.createEvent('UIEvent')
                            e.initUIEvent("touchstart", true, true)
                        } catch (err2) {
                            e = document.createEvent('Event')
                            e.initEvent("touchstart", true, true)
                        }
                    }
                    e.targetTouches = { pageX: 10, pageY: 10 };
                    findPanel.controls.findTextBox.dispatchEvent(e);*/

                    /*x = 5; y = 5;
                    var touch = document.createTouch(window, window, 1, x, y, x, y);
                    var touches = document.createTouchList(touch);
                    var targetTouches = document.createTouchList(touch);
                    var changedTouches = document.createTouchList(touch);
                    var event = document.createEvent("TouchEvent");
                    event.initTouchEvent('touchstart', true, true, window, null, x, y, x, y, false, false, false, false, touches, targetTouches, changedTouches, 1, 0);
                    findPanel.controls.findTextBox.dispatchEvent(event);
                    */
                    //$(findPanel.controls.findTextBox).trigger('touchstart')

                    //findPanel.controls.findTextBox.dispatchEvent(document.createEvent('TouchEvent'));
                }, 300);
                findPanel.controls.findTextBox.value = "";
                this.jsObject.animate(this, {
                    duration: duration, animations: [{ style: "opacity", start: this.style.opacity || 0, end: 1, postfix: "" },
                                                     { style: "top", start: parseFloat(this.style.top), end: 0, postfix: "px" }]
                });
                this.jsObject.animate(options.reportPanel, {
                    duration: duration, animations: [{
                        style: "marginTop", start: options.reportPanel.style.marginTop || 0,
                        end: this.offsetHeight, postfix: "px", property: "margin-top"
                    }]
                });
                this.visible = true;
            } else {
                this.jsObject.animate(this, {
                    duration: duration, animations: [{ style: "opacity", start: this.style.opacity, end: 0, postfix: "" },
                                                     { style: "top", start: parseFloat(this.style.top), end: -this.offsetHeight, postfix: "px" }]
                });
                this.jsObject.animate(options.reportPanel, {
                    duration: duration, animations: [{
                        style: "marginTop", start: options.reportPanel.style.marginTop || 0, end: 0,
                        postfix: "px", property: "margin-top"
                    }]
                });
                this.visible = false;
                if (this.jsObject.options.materialPanel.visible) {
                    this.jsObject.options.materialPanel.hide();
                }
            }
        }
    }

    return findPanel;
}

