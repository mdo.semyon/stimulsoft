
var GlobalKeyPressObject = null;

StiMobileViewer.prototype.InitializeMobileViewer = function () {
    this.options.mobileViewer.jsObject = this;

    this.options.mobileViewer.pressedDown = function () {
        var options = this.jsObject.options;

        this.jsObject.removeBookmarksLabel();

        //Close Current Menu
        if (options.currentMenu != null)
            if (options.menuPressed != options.currentMenu && options.currentMenu.parentButton != options.buttonPressed && !options.datePickerPressed && !options.dropDownListMenuPressed)
                options.currentMenu.changeVisibleState(false);

        //Close Current DropDownList
        if (options.currentDropDownListMenu != null)
            if (options.dropDownListMenuPressed != options.currentDropDownListMenu && options.currentDropDownListMenu.parentButton != options.buttonPressed)
                options.currentDropDownListMenu.changeVisibleState(false);

        //Close Current DatePicker
        if (options.currentDatePicker != null)
            if (options.datePickerPressed != options.currentDatePicker && options.currentDatePicker.parentButton != options.buttonPressed)
                options.currentDatePicker.changeVisibleState(false);

        options.buttonPressed = false;
        options.menuPressed = false;
        options.formPressed = false;
        options.dropDownListMenuPressed = false;
        options.disabledPanelPressed = false;
        options.datePickerPressed = false;
        options.fingerIsMoved = false;        
    }

    this.options.mobileViewer.ontouchstart = function (e) {
        var this_ = this;
        this.isTouchStartFlag = true;
        
        clearTimeout(this.isTouchStartTimer);
        if (this.jsObject.options.buttonsTimer) {
            clearTimeout(this.jsObject.options.buttonsTimer[2]);
            this.jsObject.options.buttonsTimer[0].className = this.jsObject.options.buttonsTimer[1];
            this.jsObject.options.buttonsTimer = null;
        }
        this.jsObject.options.isTouchClick = true;
        this.pressedDown();
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    this.options.mobileViewer.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.jsObject.options.isTouchClick = false;
        this.pressedDown();
    }    
    if (!this.options.isMaterial) {
        this.options.mobileViewer.onmouseup = function () {
            if (!this.isTouchEndFlag) this.ontouchend(true);
        }
    }
    this.options.mobileViewer.ontouchend = function (mouseProcess) {
        var this_ = this;
        this.isTouchEndFlag = mouseProcess ? false : true;
        this.touchDown = false;
        clearTimeout(this.isTouchEndTimer);
        this.jsObject.options.fingerIsMoved = false;
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    this.options.mobileViewer.ontouchmove = function () {
        this.jsObject.options.fingerIsMoved = true;        
    }

    GlobalKeyPressObject = this;
    var jsObject = this;

    this.addEvent(window, 'keydown', function (e) {
        if (e) {
            if (e.keyCode == 27) {
                if (jsObject.options.currentForm && jsObject.options.currentForm.visible) {
                    jsObject.options.currentForm.changeVisibleState(false);
                }
            }
        }
    });

    var jsObject = this;

    if (this.options.cloudMode) {
        window.onbeforeunload = function (event) {
            if (jsObject.options.reportRenderComplete) return;

            var msg = "Are you sure you want to close?";
            if (typeof event == "undefined") event = window.event;
            if (event) event.returnValue = msg;

            // For Safari
            return msg;
        }
    }
}
