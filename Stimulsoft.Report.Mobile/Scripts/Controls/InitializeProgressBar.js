﻿
StiMobileViewer.prototype.ProgressBar = function (style, isIndeterminate) {
    var progress = $("<div class='stiProgressbar'" + (style ? "style='overflow:hidden;" + style + "' " : "") + "><div style='height:100%'></div></div>")[0];

    progress.animateProgress = function (val, duration, completeFunction) {
        if (this.currentValue != null && this.currentValue > val) return; //progress not animate to back;
        this.currentValue = val;
        $(progress.firstChild).animate({ "width": parseInt(100 * val) + "%" }, { duration: duration || 500, complete: completeFunction });
    }

    progress.progress = function (val) {
        this.currentValue = val;
        $(progress.firstChild).css("width", parseInt(100 * val) + "%");
    }

    progress.setIndeterminate = function (ind) {        
        if (ind) {
            $(progress.firstChild).css("width", "25%");
            $(progress.firstChild).css("margin-left", "-25%");
            var val = -25;
            progress.iId = setInterval(function () {
                val += 3;
                if (val > 100) {
                    val = -25;
                }
                $(progress.firstChild).css("margin-left", val + "%");
            }, 40);
        } else if (progress.ind === true) {
            clearInterval(progress.iId);
            $(progress.firstChild).css("width", "0%");
            $(progress.firstChild).css("margin-left", "0%");
        }
        progress.ind = ind;
    }
    if (isIndeterminate) {
        progress.setIndeterminate(true);
    }

    return progress;
}

StiMobileViewer.prototype.InitializeBigProgressBar = function () {
    var bigProgressBar = document.createElement("div");
    bigProgressBar.jsObject = this;       

    var captionProgress = document.createElement("div");
    bigProgressBar.appendChild(captionProgress);
    bigProgressBar.caption = captionProgress;
    captionProgress.className = "stiMobileViewerBigProgressbarCaption";

    var innerProgress = this.ProgressBar();
    bigProgressBar.innerProgress = innerProgress;
    innerProgress.className = "stiMobileViewerBigProgressbarInnerProgress";
    bigProgressBar.appendChild(innerProgress);

    bigProgressBar.progress = function (val) {
        return innerProgress.progress(val);
    }
    
    bigProgressBar.animateProgress = function (val, duration, completeFunction) {
        return innerProgress.animateProgress(val, duration, completeFunction);
    }

    return bigProgressBar;
}

StiMobileViewer.prototype.InitializeSmallProgressBar = function () {
    var smallProgressBar = document.createElement("div");
    smallProgressBar.style.textAlign = "center";
    smallProgressBar.jsObject = this;
    smallProgressBar.style.display = "none";
    smallProgressBar.style.height = "60px";
    smallProgressBar.className = "stiMobileViewerSmallProgressbar";
    this.options.mainPanel.appendChild(smallProgressBar);
    this.options.smallProgressBar = smallProgressBar;

    var progressContainer = document.createElement("div");   
    progressContainer.style.marginTop = "40px";
    progressContainer.className = "stiMobileViewerSmallProgressbarContainer";
    smallProgressBar.appendChild(progressContainer);

    var innerProgress = this.ProgressBar();
    innerProgress.style.width = "200px";
    innerProgress.style.height = "10px";
    innerProgress.style.border = "0px";
    progressContainer.appendChild(innerProgress);
        
    var cancelButton = this.FormButton(null, this.loc.Gui.barname_cancel, null);    
    cancelButton.style.margin = "8px";
    cancelButton.style.opacity = 0;
    cancelButton.style.display = "inline-block";
    smallProgressBar.appendChild(cancelButton);
    
    cancelButton.action = function () {
        this.jsObject.StopRenderReportTask();
    }

    smallProgressBar.show = function () {
        this.progress(0);
        this.style.display = "";
    }

    smallProgressBar.hide = function (completeFunction) {
        if (completeFunction) {
            this.animateProgress(1, 150, function () {
                smallProgressBar.style.display = "none";
                completeFunction();
            });
        }
        else {
            this.style.display = "none";
        }
    }

    smallProgressBar.progress = function (val) {
        return innerProgress.progress(val);
    }

    smallProgressBar.animateProgress = function (val, duration, completeFunction) {
        return innerProgress.animateProgress(val, duration, completeFunction);
    }

    smallProgressBar.onmouseenter = function () {       
        clearTimeout(this.hideTimer);
        $(progressContainer).stop();
        $(cancelButton).stop();
        $(progressContainer).animate({ marginTop: 10 }, { duration: 400 });
        $(cancelButton).animate({ opacity: 1 }, { duration: 400 });
    }

    smallProgressBar.onmouseleave = function () {
        this.hideTimer = setTimeout(function () {
            cancelButton.animating = true;
            $(cancelButton).animate({ opacity: 0 }, {
                duration: 400, complete: function () {
                    $(progressContainer).animate({ marginTop: 40 }, {
                        duration: 500, complete: function () {
                            cancelButton.animating = false;
                        }
                    });
                }
            });
        }, 500);
    }

    return smallProgressBar;
}


StiMobileViewer.prototype.InitializeBigProgressBarForm = function () {
    var bigProgressBarForm = this.InitializeBigProgressBar();
    bigProgressBarForm.className = "stiMobileViewerBigProgressbarForm";
    bigProgressBarForm.style.display = "none";

    this.options.mainPanel.appendChild(bigProgressBarForm);
    this.options.bigProgressBar = bigProgressBarForm;

    bigProgressBarForm.footerPanel = document.createElement("div");
    bigProgressBarForm.footerPanel.style.display = "none";
    bigProgressBarForm.appendChild(bigProgressBarForm.footerPanel);
    bigProgressBarForm.footerPanel.className = "stiMobileViewerBigProgressbarFormFooter";
    bigProgressBarForm.buttonCancel = this.FormButton(null, this.loc.Gui.barname_cancel, null);
    bigProgressBarForm.buttonCancel.style.display = "inline-block";
    bigProgressBarForm.footerPanel.appendChild(bigProgressBarForm.buttonCancel);   

    bigProgressBarForm.innerProgress.style.width = "300px";
    bigProgressBarForm.innerProgress.style.margin = "0 40px 10px 40px";

    bigProgressBarForm.show = function (text, taskKey, cancelFunc) {
        bigProgressBarForm.footerPanel.style.display = taskKey ? "" : "none";
        bigProgressBarForm.innerProgress.style.margin = taskKey ? "0 40px 15px 40px" : "0 40px 35px 40px";
        bigProgressBarForm.taskKey = taskKey;
        bigProgressBarForm.cancelFunc = cancelFunc;
        bigProgressBarForm.caption.innerHTML = text;
        bigProgressBarForm.progress(0);
        bigProgressBarForm.style.display = "";
        bigProgressBarForm.jsObject.SetObjectToCenter(bigProgressBarForm);
        bigProgressBarForm.jsObject.options.disabledPanels[3].changeVisibleState(true);
    }

    bigProgressBarForm.buttonCancel.action = function () {        
        if (bigProgressBarForm.taskKey && !bigProgressBarForm.jsObject.options.cloudParameters.shareKey) bigProgressBarForm.jsObject.SendCommand("TaskStop", {
            TaskKey: bigProgressBarForm.taskKey,
            ResultSuccess: true
        });
        bigProgressBarForm.jsObject.options.reportStoppedByUser = true;
        bigProgressBarForm.hide();
        if (bigProgressBarForm.cancelFunc) bigProgressBarForm.cancelFunc();
    }

    bigProgressBarForm.hide = function (completeFunction) {        
        if (completeFunction) {
            bigProgressBarForm.animateProgress(1, 150, function () {
                bigProgressBarForm.style.display = "none";
                bigProgressBarForm.jsObject.options.disabledPanels[3].changeVisibleState(false);
                if (completeFunction) completeFunction();
            });
        }
        else {
            bigProgressBarForm.style.display = "none";
            bigProgressBarForm.jsObject.options.disabledPanels[3].changeVisibleState(false);
        }
    }

    return bigProgressBarForm;
}

StiMobileViewer.prototype.InitializeWaitingPanel = function (text, cancelFunc) {
    var waitingPanel = document.createElement("div");
    waitingPanel.className = "stiMobileViewerBigProgressbarForm";
    waitingPanel.jsObject = this;    
    waitingPanel.style.zIndex = "100";

    if (text) {
        var caption = document.createElement("div");
        waitingPanel.appendChild(caption);
        caption.className = "stiMobileViewerBigProgressbarCaption";
        caption.innerHTML = text;
    }
    
    var progress = this.ProgressBar("height:10px; margin:0px 15px 10px", true);
    progress.style.width = "300px";
    progress.style.margin = "0 40px 10px 40px";
    waitingPanel.appendChild(progress);
    waitingPanel.progress = progress;

    footerPanel = document.createElement("div");
    waitingPanel.appendChild(footerPanel);
    footerPanel.className = "stiMobileViewerBigProgressbarFormFooter";

    var buttonCancel = this.FormButton(null, this.loc.Common.ButtonCancel, null);
    buttonCancel.style.display = "inline-block";
    footerPanel.appendChild(buttonCancel); 

    waitingPanel.hide = function () {
        this.jsObject.options.mainPanel.removeChild(waitingPanel);
        waitingPanel.jsObject.options.disabledPanels[1].style.display = "none";
    }

    buttonCancel.action = function () {
        waitingPanel.hide();
        if (cancelFunc) cancelFunc();
    }

    this.options.disabledPanels[1].style.display = "";
    this.options.mainPanel.appendChild(waitingPanel);    
    this.SetObjectToCenter(waitingPanel);

    return waitingPanel;
}