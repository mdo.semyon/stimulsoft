﻿
StiMobileViewer.prototype.InitializeFolderReportsPanel = function () {
    var folderKey = this.options.cloudParameters ? this.options.cloudParameters.folderKey : null;
    if (!folderKey) return;
    var productName = "STIMULSOFT";
    var currItemKey = this.options.cloudParameters ? this.options.cloudParameters.reportSnapshotItemKey || this.options.cloudParameters.reportTemplateItemKey : null;
    var restUrl = this.options.cloudParameters ? this.options.cloudParameters.restUrl : null;

    var jsObject = this;
    var panel = document.createElement("div");
    this.options.folderReportsPanel = panel;
    panel.className = "stiMobileViewerFolderReportsPanel";    
    panel.selectedButton = null;

    //Logo
    var logo = document.createElement("div");
    logo.onclick = function () { window.open("https://www.stimulsoft.com/en");};
    logo.className = "stiMobileViewerFolderReportsPanelLogo";
    var innerTable = this.CreateHTMLTable();
    innerTable.style.width = "100%";
    innerTable.style.height = "100%";
    innerTable.addTextCell(productName).style.textAlign = "center";
    logo.appendChild(innerTable);
    panel.appendChild(logo);
    
    //Resize Item
    var resizeItem = document.createElement("div");
    resizeItem.className = "stiMobileViewerFolderReportsPanelItem";
    var resizeImg = document.createElement("img");
    resizeImg.style.margin = "12px 0 0 14px";
    resizeImg.src = jsObject.options.images["MainItem.png"];
    resizeItem.appendChild(resizeImg);
    panel.appendChild(resizeItem);
    resizeItem.onclick = function () {
        panel.resize();
    }

    //Separator
    panel.appendChild(jsObject.ReportsPanelSeparator());

    //Container
    var reportsContainer = document.createElement("div");
    reportsContainer.className = "stiMobileViewerFolderReportsPanelContainer";
    panel.appendChild(reportsContainer);
    
    panel.clear = function () {
        while (reportsContainer.childNodes[0]) reportsContainer.removeChild(reportsContainer.childNodes[0]);
    }

    panel.buildReports = function (rootItemKey, completeFunc) {
        var optionsObject = {
            Options: {
                SortType: "Name",
                SortDirection: "Ascending",
                ViewMode: "All",
                FilterIdent: "ReportTemplateItem"
            }
        }
        if (rootItemKey) optionsObject.ItemKey = rootItemKey;

        jsObject.SendCommand("ItemFetchAll", optionsObject,
            function (data) {
                if (data.ResultItems) {
                    completeFunc(data.ResultItems);
                }
            });
    }

    panel.addToViewer = function () {
        if (jsObject.options.mainPanel.parentElement) {
            jsObject.options.mainPanel.parentElement.insertBefore(panel, jsObject.options.mainPanel);
            jsObject.options.mainPanel.style.marginLeft = panel.offsetWidth + "px";
            jsObject.options.mainPanel.style.width = "calc(100% - " + panel.offsetWidth + "px)";

            var minimize = jsObject.GetCookie("StimulsoftWebDemoMainMenuMinimize");
            panel.resize(minimize == null ? false : minimize == "true", true);
        }
    }

    panel.buildReports(folderKey != "root" ? folderKey : null, function (resultItems) {
        var reportItems = [];
        for (var i = 0; i < resultItems.length; i++) {
            if (resultItems[i].Ident == "ReportTemplateItem") {
                reportItems.push(resultItems[i]);
            }
        }

        if (reportItems.length > 0) {
            panel.addToViewer();
            panel.clear();
            for (var i = 0; i < reportItems.length; i++) {
                var button = jsObject.ReportsPanelButton(reportItems[i]);
                if (restUrl) button.image.src = restUrl + "service/smallthumbnail/" + reportItems[i].Key;
                reportsContainer.appendChild(button);
                panel.selectButtonByItemKey(currItemKey);

                button.action = function () {
                    if (this.isSelected) return;
                    var cloudParameters = jsObject.options.cloudParameters;
                    jsObject.options.processImage.show();

                    cloudParameters.versionKey = null;
                    cloudParameters.reportSnapshotItemKey = this.itemObject.Ident == "ReportSnapshotItem" ? this.itemObject.Key : null;
                    cloudParameters.reportTemplateItemKey = this.itemObject.Ident == "ReportTemplateItem" ? this.itemObject.Key : null;
                    cloudParameters.reportName = Base64.encode(this.itemObject.Name);
                    cloudParameters.pagination = null;

                    document.title = this.itemObject.Name + " - " + jsObject.loc.FormViewer.title;
                    if (jsObject.options.cloudParameters.dVers === true || jsObject.options.cloudParameters.dVers == "true") {
                        document.title += " " + jsObject.getT();
                    }

                    jsObject.options.pagesCount = 0;
                    jsObject.options.pageNumber = 0;
                    jsObject.options.reportRenderComplete = null;
                    jsObject.options.firstPageComplete = null;
                    jsObject.options.notLoadAllPages = null;
                    jsObject.options.cacheReportGuid = null;
                    jsObject.options.reportPagesLoadedStatus = {};
                    jsObject.options.notAnimatedPages = {};
                    jsObject.options.findHelper = { findLabels: [] };
                    jsObject.HideParametersAndBookmarksPanels();
                    jsObject.options.reportPanel.clear();
                    
                    if (cloudParameters.reportSnapshotItemKey) {
                        jsObject.options.reportRenderComplete = true;
                        jsObject.LoadReportSnapshotItemToViewer(cloudParameters.reportSnapshotItemKey, cloudParameters.versionKey);
                    }
                    else if (cloudParameters.reportTemplateItemKey) {
                        if (jsObject.options.cachedReports[cloudParameters.reportTemplateItemKey]) {
                            jsObject.options.reportRenderComplete = true;
                            jsObject.LoadReportSnapshotItemToViewer(jsObject.options.cachedReports[cloudParameters.reportTemplateItemKey], cloudParameters.versionKey);
                        }
                        else {
                            jsObject.LoadReportTemplateItemToViewer(cloudParameters.reportTemplateItemKey, cloudParameters.versionKey);
                        }
                    }

                    panel.selectButtonByItemKey(this.itemObject.Key);
                }
            }
        }
    });

    panel.selectButtonByItemKey = function (itemKey) {
        if (panel.selectedButton) panel.selectedButton.setSelected(false);
        for (var i = 0; i < reportsContainer.childNodes.length; i++) {
            if (reportsContainer.childNodes[i].itemObject && reportsContainer.childNodes[i].itemObject.Key == itemKey) {
                reportsContainer.childNodes[i].setSelected(true);
                panel.selectedButton = reportsContainer.childNodes[i];
            }
        }
    }

    panel.resize = function (minimize, noAnimation) {
        var mainPanel = jsObject.options.mainPanel;
        if (minimize == null) minimize = !panel.isMinimize;

        var finishWidth = minimize ? 44 : 170;
        var durationAnimation = noAnimation ? 0 : 150;
        panel.isMinimize = minimize;
        jsObject.SetCookie("StimulsoftWebDemoMainMenuMinimize", minimize ? "true" : "false");

        if (noAnimation) {
            panel.style.width = finishWidth + "px";
            mainPanel.style.marginLeft = panel.offsetWidth + "px";
            mainPanel.style.width = "calc(100% - " + panel.offsetWidth + "px)";
        }
        else {
            resizeImg.className = "stiMobileViewerFolderReportsPanelItemImageRotate";
            $(panel).animate({ width: finishWidth }, {
                duration: durationAnimation,
                step: function () {
                    mainPanel.style.marginLeft = panel.offsetWidth + "px";
                    mainPanel.style.width = "calc(100% - " + panel.offsetWidth + "px)";
                },
                complete: function () {
                    panel.style.width = finishWidth + "px";
                    mainPanel.style.marginLeft = panel.offsetWidth + "px";
                    mainPanel.style.width = "calc(100% - " + panel.offsetWidth + "px)";
                    resizeImg.className = "";
                }
            });
        }

        var finishOpacity = minimize ? 0 : 1;
        $(reportsContainer).animate({ opacity: finishOpacity }, { duration: durationAnimation });
        $(logo).animate({ opacity: finishOpacity }, { duration: durationAnimation });
    }

    return panel;
}

StiMobileViewer.prototype.ReportsPanelButton = function (itemObject) {
    var button = this.CreateHTMLTable();;
    button.itemObject = itemObject;
    button.className = "stiMobileViewerReportButton";

    var image = document.createElement("img");
    image.className = "stiMobileViewerReportButtonImage";
    button.addCell(image).style.textAlign = "center";
    button.image = image;

    var caption = document.createElement("div");
    caption.className = "stiMobileViewerReportButtonCaption";
    button.addCellInNextRow(caption).style.textAlign = "center";
    caption.innerHTML = itemObject.Name;

    button.setSelected = function(state){
        this.isSelected = state;
        this.className = state ? "stiMobileViewerReportButton stiMobileViewerReportButtonSelected" : "stiMobileViewerReportButton";
    }

    button.onclick = function () {
        this.action();
    }

    button.action = function () { };

    return button;
}

StiMobileViewer.prototype.ReportsPanelSeparator = function () {
    var sep = document.createElement("div");
    sep.className = "stiMobileViewerFolderReportsPanelSeparator";

    return sep;
}