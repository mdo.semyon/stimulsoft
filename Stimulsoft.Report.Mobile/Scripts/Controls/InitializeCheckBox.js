﻿
StiMobileViewer.prototype.CheckBox = function (name, caption, toolTip) {
    var checkBox = this.CreateHTMLTable();
    checkBox.style.fontFamily = this.options.toolbarFontFamily;
    checkBox.jsObject = this;
    if (name != null) this.options.controls[name] = checkBox;
    checkBox.name = name != null ? name : this.generateKey();
    checkBox.id = checkBox.name;
    checkBox.isEnabled = true;
    checkBox.isChecked = false;
    checkBox.className = "stiMobileViewerCheckBox";
    checkBox.captionText = caption;

    if (toolTip) checkBox.setAttribute("title", toolTip);

    //Image
    checkBox.imageBlock = document.createElement("div");
    checkBox.imageBlock.className = "stiMobileViewerCheckBoxImageBlock";
    checkBox.imageBlock.style.width = checkBox.imageBlock.style.height = this.options.isTouchDevice ? "16px" : "13px";
    var imageBlockCell = checkBox.addCell(checkBox.imageBlock);
    if (this.options.isTouchDevice) imageBlockCell.style.padding = "1px 3px 1px 1px";

    checkBox.image = document.createElement("img");
    checkBox.image.src = this.options.images["CheckBox.png"];
    checkBox.image.style.visibility = "hidden";
    var imgTable = this.CreateHTMLTable();
    imgTable.style.width = "100%";
    imgTable.style.height = "100%";
    checkBox.imageBlock.appendChild(imgTable);
    imgTable.addCell(checkBox.image).style.textAlign = "center";

    //Caption
    if (caption != null || typeof (caption) == "undefined") {
        checkBox.captionCell = checkBox.addCell();
        if (!this.options.isTouchDevice) checkBox.captionCell.style.padding = "1px 0 0 4px";
        checkBox.captionCell.style.whiteSpace = "nowrap";
        if (caption) checkBox.captionCell.innerHTML = caption;
    }

    checkBox.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    checkBox.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.imageBlock.className = "stiMobileViewerCheckBoxImageBlockOver";
    }

    checkBox.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.imageBlock.className = "stiMobileViewerCheckBoxImageBlock";
    }

    checkBox.onclick = function () {
        if (this.isTouchEndFlag || !this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.setChecked(!this.isChecked);
        this.action();
    }

    checkBox.ontouchend = function () {
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        this.imageBlock.className = "stiMobileViewerCheckBoxImageBlockOver";
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        setTimeout(function () {
            this_.imageBlock.className = "stiMobileViewerCheckBoxImageBlock";
            this_.setChecked(!this_.isChecked);
            this_.action();
        }, 150);
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    checkBox.ontouchstart = function () {
        this.jsObject.options.fingerIsMoved = false;
    }

    checkBox.setEnabled = function (state) {
        this.image.style.opacity = state ? "1" : "0.5";
        this.isEnabled = state;
        this.className = state ? "stiMobileViewerCheckBox" : "stiMobileViewerCheckBox stiMobileViewerCheckBoxDisabled";
        this.imageBlock.className = state ? "stiMobileViewerCheckBoxImageBlock" : "stiMobileViewerCheckBoxImageBlockDisabled";
    }

    checkBox.setChecked = function (state, ignoreOnChecked) {
        this.image.style.visibility = (state) ? "visible" : "hidden";
        this.isChecked = state;
        if (!ignoreOnChecked) this.onChecked();
    }

    checkBox.onChecked = function () { }
    checkBox.action = function () { }

    return checkBox;
}