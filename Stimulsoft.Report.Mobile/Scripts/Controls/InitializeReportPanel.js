﻿
StiMobileViewer.prototype.InitializeReportPanel = function () {
    var reportPanel = document.createElement("div");
    reportPanel.id = this.options.mobileViewer.id + "ReportPanel";
    reportPanel.jsObject = this;
    this.options.reportPanel = reportPanel;
    this.options.mainPanel.appendChild(reportPanel);
    reportPanel.style.textAlign = this.options.contentAlignment == "default" ? "center" : this.options.contentAlignment;
    reportPanel.style.overflow = this.options.scrollbarsMode ? "auto" : "hidden";
    reportPanel.className = "stiMobileViewerReportPanel";
    reportPanel.style.bottom = this.options.cloudMode || this.options.designerMode ? "35px" : "0px";
    reportPanel.pages = [];

    if (this.options.cloudMode || this.options.designerMode) {
        reportPanel.style.position = "absolute";
        reportPanel.style.top = this.options.toolbar.offsetHeight + "px";
    }
    else {
        if (this.options.fullScreen) {
            reportPanel.style.position = "absolute";
            reportPanel.style.top = this.options.showToolBar ? this.options.toolbar.offsetHeight + "px" : "0px";
        }
        else {
            reportPanel.style.position = this.options.viewerHeightType != "Percentage" || this.options.scrollbarsMode ? "absolute" : "relative";
            reportPanel.style.top = this.options.showToolBar
            ? (this.options.viewerHeightType != "Percentage" || this.options.scrollbarsMode
                ? this.options.toolbar.offsetHeight + "px"
                : "0px")
            : "0px";
        }
    }

    reportPanel.addPage = function (pageAttributes) {
        var page = document.createElement("DIV");
        reportPanel.appendChild(page);
        reportPanel.pages.push(page);

        page.jsObject = reportPanel.jsObject;
        page.pageNumber = reportPanel.pages.length - 1;
        page.className = this.jsObject.options.showPageShadow ? "stiMobileViewerPageShadow" : "stiMobileViewerPage";
        page.style.overflow = "hidden";
        page.style.margin = "10px";
        page.style.display = "inline-block";
        page.style.verticalAlign = "top";
        page.style.color = "#000000";
        if (this.jsObject.options.showPageBorders) page.style.border = this.jsObject.options.pageBorderSize + "px solid " + this.jsObject.options.pageBorderColor;

        //if cloud mode new page on new row
        if (this.jsObject.options.cloudMode) {
            var br = document.createElement("br");
            reportPanel.appendChild(br);
        }

        //page sizes
        var pageSizes = pageAttributes["sizes"].split(";");
        page.pageWidth = parseInt(pageSizes[0]);
        page.pageHeight = parseInt(pageSizes[1]);
        
        //page margins
        page.style.padding = pageAttributes["margins"];
        var marginsPx = pageAttributes["margins"].split(" ");
        page.margins = [];
        for (var i = 0; i < marginsPx.length; i++) {
            page.margins.push(parseInt(marginsPx[i].replace("px", "")));
        }

        //background       
        page.style.background = pageAttributes["background"];

        //content
        page.innerHTML = pageAttributes["content"];

        //Correct Watermark
        for (var i = 0; i < page.childNodes.length; i++) {
            if (page.childNodes[i].style && page.childNodes[i].style.backgroundImage) {
                page.style.backgroundImage = page.childNodes[i].style.backgroundImage;
                page.style.backgroundRepeat = "no-repeat";
                page.style.backgroundSize = "contain";

                page.childNodes[i].style.backgroundImage = "";
                page.childNodes[i].style.backgroundColor = "";
                break;
            }
        }

        if (this.jsObject.options.reportDisplayMode == "Div" || this.jsObject.options.reportDisplayMode == "Span") {
            var childs = page.getElementsByClassName("StiPageContainer");
            if (childs && childs.length > 0) {
                var pageContainer = childs[0];
                pageContainer.style.position = "relative";
                page.style.width = (page.pageWidth - page.margins[1] - page.margins[3]) + "px";
                page.style.height = (page.pageHeight - page.margins[0] - page.margins[2]) + "px";
            }
        }

        this.jsObject.options.pagesWidth = page.pageWidth;
        this.jsObject.options.pagesHeight = page.pageHeight;

        //find max height of pages
        if (!this.jsObject.options.cloudMode) {
            var currentPageHeight = page.offsetHeight - page.margins[0] - page.margins[2];
            if (reportPanel.maxHeights[pageSizes[1]] == null || currentPageHeight > reportPanel.maxHeights[pageSizes[1]])
                reportPanel.maxHeights[pageSizes[1]] = currentPageHeight;

            this.jsObject.options.pagesWidth = page.offsetWidth;
            this.jsObject.options.pagesHeight = page.offsetHeight;
        }

        //add page methods
        page.loadContent = function (pageContent) {
            var pageAttributes = pageContent[0];
            page.style.background = pageAttributes["background"];
            page.innerHTML = pageAttributes["content"];
            page.style.padding = pageAttributes["margins"];

            var marginsPx = pageAttributes["margins"].split(" ");
            page.margins = [];
            for (var i = 0; i < marginsPx.length; i++) {
                page.margins.push(parseInt(marginsPx[i].replace("px", "")));
            }

            if (pageAttributes["content"]) {
                page.style.width = "";
                page.style.height = "";
            }

            if (pageAttributes["sizes"]) {
                var pageSizes = pageAttributes["sizes"].split(";");
                this.jsObject.options.pagesWidth = parseInt(pageSizes[0]);
                this.jsObject.options.pagesHeight = parseInt(pageSizes[1]);
            }

            //add style
            if (pageContent[pageContent.length - 2]) {
                //add pages styles           
                if (page.cssStyles != null && page.cssStyles.parentElement) {
                    page.cssStyles.parentElement.removeChild(page.cssStyles);
                }

                page.cssStyles = document.createElement("STYLE");
                page.cssStyles.setAttribute('type', 'text/css');
                page.appendChild(page.cssStyles);

                if (page.cssStyles.styleSheet) page.cssStyles.styleSheet.cssText = pageContent[pageContent.length - 2];
                else page.cssStyles.innerHTML = pageContent[pageContent.length - 2];

            }

            //add chart scripts
            if (pageContent[pageContent.length - 1] && this.jsObject.options.cloudParameters && this.jsObject.options.cloudParameters.pagination) {
                var currChartScripts = document.getElementById("chartScriptMobileViewer");
                if (currChartScripts) this.jsObject.options.mainPanel.removeChild(currChartScripts);

                var chartScripts = document.createElement("Script");
                chartScripts.setAttribute('type', 'text/javascript');
                chartScripts.id = "chartScriptMobileViewer";
                chartScripts.textContent = pageContent[pageContent.length - 1];
                this.jsObject.options.mainPanel.appendChild(chartScripts);
                this.jsObject.options.notAnimatedPages[page.pageNumber.toString()] = true;
            }
        }

        page.setSelected = function () {
            if (reportPanel.currentPage && this != reportPanel.currentPage) {
                reportPanel.currentPage.style.border = this.jsObject.options.pageBorderSize + "px solid " + this.jsObject.options.pageBorderColor;
                reportPanel.currentPage.isSelected = false;
            }
            reportPanel.currentPage = this;
            this.style.border = "2px solid " + this.jsObject.GetThemeColorValue();
            this.isSelected = true;
        }

        page.onclick = function () {
            if (!this.isSelected) {
                if (this.jsObject.options.menuViewMode == "WholeReport") this.jsObject.options.pageNumber = this.pageNumber;
                if (this.jsObject.options.toolbar) this.jsObject.options.toolbar.changeToolBarState();
                if (this.jsObject.options.materialPanel) this.jsObject.options.materialPanel.changeToolBarState();
            }
        }

        if (this.jsObject.options.isMaterial && this.jsObject.options.menuViewMode == "OnePage") {
            page.startX = 0;
            page.touchesLength = 0;
            page.multitouch = false;
            page.lastTouches = [{ x: 0, y: 0, time: 0 }, { x: 0, y: 0, time: 0}];

            var touchStart = function (this_, e) {
                page.startX = e.changedTouches[0].clientX;
                if (page.touchesLength == 0) {
                    page.multitouch = false;
                } else if (page.touchesLength > 0) {
                    page.multitouch = true;
                }
                page.touchesLength++;
                //this_.canPrevPage = this_.parentElement.scrollLeft == 0;
                //this_.canNextPage = this_.parentElement.scrollLeft + this_.parentElement.offsetWidth - this_.parentElement.scrollWidth > -3;
            }

            var touchEnd = function (this_, e) {
                page.touchesLength--;
                if (this_.multitouch || this_.jsObject.options.zoom > this_.jsObject.options.materialZoom) return;
                var dX = this_.lastTouches[1].x - this_.lastTouches[0].x;
                this_.style.transitionDuration = "300ms";
                if ((dX <= 0 && this_.jsObject.options.pageNumber >= this_.jsObject.options.pagesCount - 1) ||
                    (dX >= 0 && this_.jsObject.options.pageNumber <= 0)) {
                    this_.style.transform = "translate3d(0, 0, 0)";
                } else if (dX != 0 && new Date().getTime() - this_.lastTouches[1].time <= 14) {
                    if (dX < -5 && this_.lastTouches[1].x < this_.startX) {
                        this_.jsObject.actionEvent("NextPage");
                        this_.style.transform = "translate3d(-" + this_.pageWidth + "px, 0, 0)";
                    } else if (dX > 5 && this_.lastTouches[1].x > this_.startX) {
                        this_.jsObject.actionEvent("PrevPage");
                        this_.style.transform = "translate3d(" + this_.pageWidth + "px, 0, 0)";
                    } else {
                        this_.style.transform = "translate3d(0, 0, 0)";
                    }
                } else {
                    if (dX < 0 && this_.posX < -this_.pageWidth / 2) {
                        this_.jsObject.actionEvent("NextPage");
                        this_.style.transform = "translate3d(-" + this_.pageWidth + "px, 0, 0)";
                    } else if (dX > 0 && this_.posX > this_.pageWidth / 2) {
                        this_.jsObject.actionEvent("PrevPage");
                        this_.style.transform = "translate3d(" + this_.pageWidth + "px, 0, 0)";
                    } else {
                        this_.style.transform = "translate3d(0, 0, 0)";
                    }
                }
                setTimeout(function () {
                    this_.style.transitionDuration = "";
                }, 300);
            }
            page.ontouchstart = function (e) { touchStart(this, e); }
            page.ontouchenter = function (e) { touchStart(this, e); }
            page.ontouchmove = function (e) {
                if (this.multitouch || this.jsObject.options.zoom > this.jsObject.options.materialZoom) return;
                var posX = parseInt(e.changedTouches[0].clientX);
                var moveX = posX - this.startX;
                /*if ((moveX <= 0 && page.jsObject.options.pageNumber >= page.jsObject.options.pagesCount - 1) ||
                (moveX >= 0 && page.jsObject.options.pageNumber <= 0)) {
                moveX = 0;
                this.startX = posX;
                }*/
                this.style.transform = "translate3d(" + moveX + "px" + ", 0, 0)";
                this.posX = moveX;
                page.lastTouches.shift();
                page.lastTouches.push({ x: posX, y: parseInt(e.changedTouches[0].clientY), time: new Date().getTime() });
            }
            page.ontouchend = function (e) { touchEnd(this, e); }
            page.ontouchleave = function (e) { touchEnd(this, e); }
            page.ontouchcancel = function (e) { touchEnd(this, e); }
        }       

        //this.jsObject.InitializeInteractions(page);

        return page;
    }

    reportPanel.getZoomByPageWidth = function () {
        if (this.jsObject.options.pagesWidth == 0) return 100;
        var newZoom = ((this.offsetWidth - 35) * this.jsObject.options.zoom) / (this.jsObject.options.pagesWidth);
        return newZoom;
    }

    reportPanel.getZoomByPageHeight = function () {
        if (this.jsObject.options.pagesHeight == 0) return 100;
        var newPagesHeight = this.jsObject.options.scrollbarsMode ? Math.min(this.jsObject.options.mobileViewer.offsetHeight, window.innerHeight) : window.innerHeight;
        if (this.jsObject.options.toolbar) newPagesHeight -= this.jsObject.options.toolbar.offsetHeight;
        if (this.jsObject.options.parametersPanel && !this.jsObject.options.cloudMode && !this.jsObject.options.designerMode)
            newPagesHeight -= this.jsObject.options.parametersPanel.offsetHeight;
        var newZoom = ((newPagesHeight - 25) * this.jsObject.options.zoom) / (this.jsObject.options.pagesHeight);
        return newZoom;
    }

    reportPanel.addPages = function () {
        if (this.jsObject.options.pagesArray == null) return;
        if (this.jsObject.options.isMaterial && this.jsObject.options.materialPanel.showOnPage) {
            if (this.jsObject.options.materialPanel.hideTimeout) {
                this.jsObject.options.materialPanel.hideTimeout = null;
                //this.jsObject.options.materialPanel.showOnPage = false;
                clearTimeout(this.jsObject.options.materialPanel.hideTimeout);
            }
            var this_ = this;
            this.jsObject.options.materialPanel.hideTimeout = setTimeout(function (e) {
                this_.jsObject.options.materialPanel.hide(500);
                this_.jsObject.options.materialPanel.showOnPage = false;
                this.jsObject.options.materialPanel.hideTimeout = null;
            }, 700);
        }
        var countPages = this.jsObject.options.pagesArray.length - 2;
        this.maxHeights = {};

        if (!this.jsObject.options.notLoadAllPages) this.clear();

        //add pages styles
        if (this.jsObject.options.css == null) {
            this.jsObject.options.css = document.createElement("STYLE");
            this.jsObject.options.css.setAttribute('type', 'text/css');
            this.jsObject.options.mainPanel.appendChild(this.jsObject.options.css);
        }
        if (this.jsObject.options.css.styleSheet) this.jsObject.options.css.styleSheet.cssText = this.jsObject.options.pagesArray[countPages];
        else this.jsObject.options.css.innerHTML = this.jsObject.options.pagesArray[countPages];

        //add chart scripts        
        var currChartScripts = document.getElementById(this.jsObject.options.mobileViewerId + "chartScriptMobileViewer");
        if (currChartScripts) this.jsObject.options.mainPanel.removeChild(currChartScripts);

        if (this.jsObject.options.pagesArray[countPages + 1] && !(this.jsObject.options.cloudParameters && this.jsObject.options.cloudParameters.pagination)) { 
            var chartScripts = document.createElement("Script");
            chartScripts.setAttribute('type', 'text/javascript');
            chartScripts.id = this.jsObject.options.mobileViewerId + "chartScriptMobileViewer";
            chartScripts.textContent = this.jsObject.options.pagesArray[countPages + 1];
            this.jsObject.options.mainPanel.appendChild(chartScripts);
        }

        //add page contents
        for (num = 0; num < countPages; num++) {
            if (!this.jsObject.options.notLoadAllPages)
                this.addPage(this.jsObject.options.pagesArray[num]);
            else {
                this.pages[num].loadContent([this.jsObject.options.pagesArray[num], "", ""]);
            }
        }

        if (this.jsObject.options.notLoadAllPages) {
            for (var i = countPages; i < this.pages.length; i++) {
                this.pages[i].style.width = parseInt(this.pages[i].pageWidth * (this.jsObject.options.zoom / 100) * 0.72) + "pt";
                this.pages[i].style.height = parseInt(this.pages[i].pageHeight * (this.jsObject.options.zoom / 100) * 0.72) + "pt";
            }
        }

        //go to current page after zoom
        if (this.jsObject.options.cloudMode)
            reportPanel.jsObject.scrollToPage(reportPanel.jsObject.options.pageNumber, true);

        if (reportPanel.jsObject.options.cloudParameters && reportPanel.jsObject.options.cloudParameters.pagination) {
            reportPanel.loadVisiblePagesContent();
        }
        else {
            reportPanel.correctHeights();
        }

        if (reportPanel.jsObject.options.editableMode) reportPanel.jsObject.ShowAllEditableFields();
        reportPanel.jsObject.UpdateAllHyperLinks();
    }

    reportPanel.onscroll = function () {
        if (reportPanel.jsObject.options.cloudMode) {            
            clearTimeout(reportPanel.scrollTimer);

            var this_ = this;
            reportPanel.scrollTimer = setTimeout(function () {
                this_.loadVisiblePagesContent()
            }, 300);
        }
    }

    reportPanel.loadVisiblePagesContent = function () {
        var options = reportPanel.jsObject.options;
        var pageNumbers = [];
        var commonPagesHeight = 0;
        var i = 0;

        for (i = 0; i < reportPanel.pages.length; i++) {
            commonPagesHeight += reportPanel.pages[i].offsetHeight + 20;
            if (commonPagesHeight > reportPanel.scrollTop) break;
        }

        if (options.bookmarkAnchor) i = options.pageNumber;

        if (i < options.pagesCount && i >= 0 && i != options.pageNumber) {
            var currentCommonPagesHeight = 0;
            for (var k = 0; k <= options.pageNumber; k++) currentCommonPagesHeight += reportPanel.pages[k].offsetHeight + 20;
            if (currentCommonPagesHeight < reportPanel.scrollTop ||
                currentCommonPagesHeight - reportPanel.pages[options.pageNumber].offsetHeight - 20 > reportPanel.scrollTop + reportPanel.offsetHeight) {
                options.pageNumber = i;
                if (options.toolbar) options.toolbar.changeToolBarState();
                if (options.materialPanel) options.materialPanel.changeToolBarState();
            }
        }

        if (options.cloudParameters && options.cloudParameters.pagination) {
            //load current visible page
            if (options.reportPagesLoadedStatus[i] == null) {
                pageNumbers.push(i);
                options.reportPagesLoadedStatus[i] = false;
            }
            //and load next page if visible
            if (i < reportPanel.pages.length - 1 && options.reportPagesLoadedStatus[i + 1] == null &&
                commonPagesHeight < reportPanel.scrollTop + reportPanel.offsetHeight) {
                pageNumbers.push(i + 1);
                options.reportPagesLoadedStatus[i + 1] = false;
            }
            //and load next-next page if visible
            if (i + 1 < reportPanel.pages.length - 1 && options.reportPagesLoadedStatus[i + 2] == null &&
                commonPagesHeight + reportPanel.pages[i + 1].offsetHeight + 20 < reportPanel.scrollTop + reportPanel.offsetHeight) {
                pageNumbers.push(i + 2);
                options.reportPagesLoadedStatus[i + 2] = false;
            }
            reportPanel.loadPageContent(pageNumbers);
        }
    }

    reportPanel.loadPageContent = function (pageIndexes) {
        if (!pageIndexes || pageIndexes.length == 0) return;
        var options = reportPanel.jsObject.options;
        var param = {};
        param.cacheReportGuid = options.cacheReportGuid;
        param.pageIndexes = pageIndexes;
        param.versionKey = options.cloudParameters.versionKey;
        param.reportSnapshotItemKey = options.cloudParameters.reportSnapshotItemKey;
        param.taskKey = options.reportRenderTaskKey;
        param.sessionKey = options.cloudParameters.sessionKey;
        param.zoom = options.zoom;
        param.notAnimatedPages = options.notAnimatedPages;
        
        if (this.jsObject.options.reportStoppedByUser) return;
        
        reportPanel.jsObject.sendToServer("LoadPageFromData", param, function (data) { 
            for (var i = 0; i < data["pagesContent"].length; i++) {
                var pageParams = data["pagesContent"][i];
                var pageIndex = pageParams["pageIndex"];
                var pageContent = pageParams["pageContent"];
                var page = reportPanel.jsObject.options.reportPanel.getPageByIndex(pageIndex);
                if (page && pageContent) {                   
                    page.loadContent(pageContent);
                    reportPanel.jsObject.options.reportPagesLoadedStatus[pageIndex] = true;
                }
            }
            reportPanel.jsObject.options.processImage.hide();

            if (reportPanel.jsObject.options.bookmarkAnchor != null) {
                reportPanel.jsObject.scrollToAnchor(reportPanel.jsObject.options.bookmarkAnchor, reportPanel.jsObject.options.componentGuid);
                reportPanel.jsObject.options.bookmarkAnchor = null;
                reportPanel.jsObject.options.componentGuid = null;
            }
        });
    }

    reportPanel.getPageByIndex = function (pageIndex) {
        var page = null;
        if (reportPanel.jsObject.options.menuViewMode == "OnePage") {
            if (reportPanel.pages.length > 0) { page = reportPanel.pages[0]; }
        }
        else {
            if (reportPanel.pages.length > pageIndex) { page = reportPanel.pages[pageIndex]; }
        }
        return page;
    }

    reportPanel.clear = function () {
        while (this.childNodes[0]) {
            this.removeChild(this.childNodes[0]);
        }
        reportPanel.pages = [];
    }

    reportPanel.correctHeights = function () {
        for (var i in this.childNodes) {
            if (this.childNodes[i].pageHeight != null) {
                var height = reportPanel.maxHeights[this.childNodes[i].pageHeight.toString()];
                if (height) this.childNodes[i].style.height = height + "px";
            }
        }
    }

    reportPanel.ontouchstart = function (e) {
        this.touchDown = true;
        this.jsObject.options.touchMove = false;
        if (this.jsObject.options.allowTouchZoom) {
            this.jsObject.options.firstZoomDistance = 0;
            this.jsObject.options.secondZoomDistance = 0;
            this.jsObject.options.zoomStep = 0;
        }
        var this_ = this;
        if (this.jsObject.options.isMaterial) {
            if (e.touches.length == 2) {
                this.jsObject.options.pinchStart = Math.sqrt(Math.pow(e.touches[0].pageX - e.touches[1].pageX, 2) + Math.pow(e.touches[0].pageY - e.touches[1].pageY, 2));
            } else {
                this.jsObject.options.pinchStart = null;
            }
            if (this.jsObject.options.bookmarksPanel && this.jsObject.options.bookmarksPanel.visible) {
                this.jsObject.options.bookmarksPanel.changeVisibleState(false);
            }
            clearTimeout(this.jsObject.materialPanelTimeout);
            this.jsObject.materialPanelTimeout = setTimeout(function () {
                if (!this_.touchDown && !this_.jsObject.options.mainMaterialMenu.visible && !this_.jsObject.options.touchMove
                    && !this_.jsObject.options.materialPanel.visible && (!this_.jsObject.options.parametersPanel || !this_.jsObject.options.parametersPanel.visible) &&
                    e.srcElement.tagName.toLowerCase() != "a") {
                    this_.jsObject.options.materialPanel.show();
                    this_.jsObject.options.materialPanel.showOnPage = false;
                } else {
                    if (!this_.jsObject.options.materialPanel.showOnPage) {
                        this_.jsObject.options.mainMaterialMenu.changeVisibleState(false);
                        this_.jsObject.options.materialPanel.hide();
                    }
                }
            }, 200);
        }
    }

    reportPanel.ontouchend = function () {
        this.touchDown = false;
        if (this.jsObject.options.centerText) {
            this.jsObject.options.centerText.hide();
        }
        if (this.jsObject.options.newMaterialZoom) {
            this.jsObject.actionEvent("Zoom" + this.jsObject.options.newMaterialZoom);
            this.jsObject.options.newMaterialZoom = null;
        }
    }

    reportPanel.ontouchmove = function (event) {
        this.jsObject.options.touchMove = true;
        if (typeof event !== "undefined" && event.touches.length > 1 && this.jsObject.options.allowTouchZoom) {
            if (!this.jsObject.options.isMaterial) {
                if ('preventDefault' in event) event.preventDefault();
                this.jsObject.options.zoomStep++;
                if (this.jsObject.options.firstZoomDistance == 0)
                    this.jsObject.options.firstZoomDistance = Math.sqrt(Math.pow(event.touches[0].pageX - event.touches[1].pageX, 2) + Math.pow(event.touches[0].pageY - event.touches[1].pageY, 2));

                if (this.jsObject.options.zoomStep > 2 && this.jsObject.options.secondZoomDistance == 0) {
                    this.jsObject.options.secondZoomDistance = Math.sqrt(Math.pow(event.touches[0].pageX - event.touches[1].pageX, 2) + Math.pow(event.touches[0].pageY - event.touches[1].pageY, 2));

                    this.jsObject.SetZoom(this.jsObject.options.secondZoomDistance > this.jsObject.options.firstZoomDistance);
                }
            } else {
                if (event.touches.length == 2 && this.jsObject.options.pinchStart !== null) {
                    var newPinch = Math.sqrt(Math.pow(event.touches[0].pageX - event.touches[1].pageX, 2) + Math.pow(event.touches[0].pageY - event.touches[1].pageY, 2));
                    var cs = 100 * this.jsObject.options.zoom / this.jsObject.options.materialZoom + Math.round(newPinch - this.jsObject.options.pinchStart) / 3;
                    cs = Math.round(cs / 5) * 5;
                    cs = Math.max(20, Math.min(200, cs));
                    this.jsObject.options.centerText.setText(cs + "%");
                    this.jsObject.options.newMaterialZoom = cs * this.jsObject.options.materialZoom / 100;
                } else {
                    this.jsObject.options.centerText.hide();
                    this.jsObject.options.newMaterialZoom = null;
                }

            }
        }
    }
}