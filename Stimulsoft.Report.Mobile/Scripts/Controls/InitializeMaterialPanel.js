﻿
StiMobileViewer.prototype.InitializeMaterialPanel = function () {
    var materialPanel = document.createElement("div");
    var this_ = this;
    materialPanel.controls = {};
    this.options.materialPanel = materialPanel;
    this.options.mainPanel.appendChild(materialPanel);
    materialPanel.jsObject = this;
    materialPanel.visible = false;
    materialPanel.className = "stiMobileViewerToolBar";
    if (this.options.cloudMode || this.options.designerMode) { materialPanel.className += " stiMobileViewerToolBarCloudMode"; }
    var findPanelInnerContent = document.createElement("div");
    materialPanel.innerContent = findPanelInnerContent;
    materialPanel.appendChild(findPanelInnerContent);
    var topPanelBlock = document.createElement("div");
    findPanelInnerContent.appendChild(topPanelBlock);
    topPanelBlock.className = "stiMobileViewerToolBarTable";    
    if (this.options.cloudMode || this.options.designerMode) { topPanelBlock.style.border = "0px"; }
    var controlsTable = this.CreateHTMLTable();
    topPanelBlock.appendChild(controlsTable);
    materialPanel.style.zIndex = 888;

    var controlProps = [["materialMain", this.BigMaterialButton("MaterialMain", null, "Menu", "MainButton.png", null), "2px"]];
    if (this.options.showBookmarksButton && this.options.paramsBookmarks != null) {
        controlProps.push(["Bookmarks", this.BigMaterialButton("Bookmarks", null, null, "Bookmarks.png"), "2px"]);
    }
    if (this.options.showParametersButton && this.options.paramsVariables != null) {
        controlProps.push(["Parameters", this.BigMaterialButton("Parameters", null, null, "Parameters.png"), "2px"]);
    }

    if (this.options.showDesignButton ) {
        controlProps.push(["Designer", this.BigMaterialButton("Designer", null, null, "Designer.png"), "2px"]);
    }

    

    for (var i = 0; i < controlProps.length; i++) {
        materialPanel.controls[controlProps[i][0]] = controlProps[i][1];
        var cell = controlsTable.addCell(controlProps[i][1]);
        controlProps[i][1].style.margin = controlProps[i][2];
        if (i == 0) {
            controlsTable.addCell().style.width = "100%";
        }
    }

    materialPanel.controls.materialMain.action = function () {
        if (materialPanel.visible) {
            this_.options.mainMaterialMenu.changeVisibleState(true);
            materialPanel.hide();
        } else {
            materialPanel.show();
        }
    }
    if (materialPanel.controls.Parameters) {
        materialPanel.controls.Parameters.action = function () {
            materialPanel.hide(50);
            this_.actionEvent("Parameters");
        }
    }
    if (materialPanel.controls.Bookmarks) {
        materialPanel.controls.Bookmarks.action = function () {
            materialPanel.hide(50);
            this_.actionEvent("Bookmarks");
        }
    }

    if (materialPanel.controls.Designer) {
        materialPanel.controls.Designer.action = function () {
            this_.actionEvent("Design");
        }
    }

    materialPanel.style.top = -materialPanel.offsetHeight + "px";

    //=========================================BOTTOM============================================

    var pagePanel = document.createElement("div");
    pagePanel.controls = {};
    this.options.bottomPanel = pagePanel;
    this.options.mainPanel.appendChild(pagePanel);
    pagePanel.jsObject = this;
    pagePanel.visible = false;
    pagePanel.style.display = "none";
    pagePanel.className = "stiMobileViewerToolBarBottomTable";
    if (this.options.cloudMode || this.options.designerMode) { pagePanel.className += " stiMobileViewerToolBarCloudMode"; }
    var pagePanelInnerContent = document.createElement("div");
    pagePanel.innerContent = pagePanelInnerContent;
    pagePanel.appendChild(pagePanelInnerContent);
    var topPagePanelBlock = document.createElement("div");
    pagePanelInnerContent.appendChild(topPagePanelBlock);
    topPagePanelBlock.className = "stiMobileViewerToolBarTable";
    if (this.options.cloudMode || this.options.designerMode) { topPagePanelBlock.style.border = "0px"; }
    controlsTable = this.CreateHTMLTable();
    topPagePanelBlock.appendChild(controlsTable);
    controlsTable.style.margin = "0 auto";
    pagePanel.style.zIndex = 888;
    var setPos = function (onlyWidth) {
        if (pagePanel.visible) {
            if (!onlyWidth) pagePanel.style.top = (window.innerHeight - pagePanel.clientHeight) + "px";
            pagePanel.controls.FirstPage.parentElement.style.width = (window.innerWidth - 6 - pagePanel.controls.PageControl.offsetWidth) / 2 + "px";
            pagePanel.controls.LastPage.parentElement.style.width = pagePanel.controls.FirstPage.parentElement.style.width;
        } else {
            if (!onlyWidth) pagePanel.style.top = (window.innerHeight + 1) + "px";
        }        
    }
    
    window.addEventListener("resize", function () {
        setPos();
    });

    window.addEventListener("orientationchange", function () {
        setPos();
    });

    controlProps = [];
    if (this.options.showFirstButton) { controlProps.push(["FirstPage", this.BigMaterialButton("Parameters", null, null, "FirstPage.png"), "2px"])}; 
    if (this.options.showCurrentPage) { controlProps.push(["PageControl", this.PageControl(), "2px"]);}
    if (this.options.showLastButton) { controlProps.push(["LastPage", this.BigMaterialButton("Parameters", null, null, "LastPage.png"), "2px"])}; 

    for (var i = 0; i < controlProps.length; i++) {
        pagePanel.controls[controlProps[i][0]] = controlProps[i][1];
        var cell = controlsTable.addCell(controlProps[i][1]);
        controlProps[i][1].style.margin = controlProps[i][2];
    }
    pagePanel.controls.FirstPage.cellImage.style.width = "100%";
    pagePanel.controls.FirstPage.cellImage.style.position = "relative";
    pagePanel.controls.FirstPage.cellImage.style.textAlign = "center";
    pagePanel.controls.LastPage.cellImage.style.width = "100%";
    pagePanel.controls.LastPage.cellImage.style.position = "relative";
    pagePanel.controls.LastPage.cellImage.style.textAlign = "center";
    

    pagePanel.controls.FirstPage.action = function () {
        this_.actionEvent("FirstPage");
    }
    pagePanel.controls.LastPage.action = function () {
        this_.actionEvent("LastPage");
    }

    setPos();

    materialPanel.show = function (duration) {
        if (!this_.options.optionShowToolBar) return;
        if (!materialPanel.visible) {
            materialPanel.visible = true;
            this_.animate(this, {
                duration: duration || 200, animations: [{ style: "opacity", start: this.style.opacity || 0, end: 1, postfix: "" },
                                                 {
                                                     style: "top", start: parseFloat(this.style.top), end: (this_.options.findPanel && this_.options.findPanel.visible)
                                                       ? this_.options.findPanel.offsetHeight : 0, postfix: "px"
                                                 }]
            });
            if (this_.options.menuViewMode == "OnePage") {
                pagePanel.style.display = "";
                pagePanel.visible = true;
                setPos(true);
                this_.animate(pagePanel, {
                    duration: duration || 200, animations: [{ style: "opacity", start: pagePanel.style.opacity || 0, end: 1, postfix: "" },
                                                     {
                                                         style: "top", start: parseFloat(pagePanel.style.top), end: window.innerHeight - pagePanel.clientHeight, postfix: "px",
                                                         finish: function () { setPos();}
                                                     }]
                });                
            } 
        }
    }

    materialPanel.hide = function (duration) {
        if (materialPanel.visible) {
            materialPanel.visible = false;
            this_.animate(this, {
                duration: duration || 200, animations: [{ style: "opacity", start: this.style.opacity, end: 0, postfix: "" },
                                                { style: "top", start: parseFloat(this.style.top), end: -this.offsetHeight, postfix: "px" }]
            });
            if (this_.options.menuViewMode == "OnePage") {
                pagePanel.visible = false;
                this_.animate(pagePanel, {
                    duration: duration || 200, animations: [{ style: "opacity", start: pagePanel.style.opacity || 0, end: 0, postfix: "" },
                                                     {
                                                         style: "top", start: parseFloat(pagePanel.style.top), end: window.innerHeight + 1, postfix: "px",
                                                         finish: function () { if (!pagePanel.visible){setPos(); pagePanel.style.display = "none"}}
                                                     }]
                });                
            }
            
        }
    }

    materialPanel.changeToolBarState = function () {
        var options = pagePanel.jsObject.options;
        var controls = pagePanel.controls;
        if (pagePanel.controls["PageControl"]) {
            pagePanel.controls["PageControl"].countLabel.innerHTML = options.pagesCount;
            pagePanel.controls["PageControl"].textBox.value = options.pageNumber + 1;
            pagePanel.controls["PageControl"].textBox.setEnabled(options.pagesCount > 1 && (options.menuViewMode == "OnePage" || options.cloudMode || options.designerMode));
        }
        if (controls["FirstPage"]) controls["FirstPage"].setEnabled(options.pageNumber > 0 && (options.menuViewMode == "OnePage" || options.cloudMode || options.designerMode));
        if (controls["LastPage"]) controls["LastPage"].setEnabled(options.pageNumber < options.pagesCount - 1 && (options.menuViewMode == "OnePage" || options.cloudMode || options.designerMode));
    }
}
