﻿
StiMobileViewer.prototype.SmallButton = function (name, groupName, caption, imageName, toolTip, arrowType, styles) {
    var button = document.createElement("div");
    button.style.fontFamily = this.options.toolbarFontFamily;
    button.jsObject = this;
    button.name = name != null ? name : this.generateKey();
    button.id = button.name;
    if (name != null) this.options.buttons[name] = button;
    button.groupName = groupName;
    button.styles = styles;
    button.isEnabled = true;
    button.isSelected = false;
    button.isOver = false;
    button.arrowType = arrowType;
    button.className = styles["default"];
    button.style.height = this.options.isTouchDevice ? "28px" : "23px";
    button.toolTip = toolTip;

    var innerTable = this.CreateHTMLTable();
    button.innerTable = innerTable;
    innerTable.style.height = "100%";
    button.appendChild(innerTable);

    if (imageName != null) {
        button.image = document.createElement("img");
        if (imageName != true) {
            var srcset = imageName.substr(0, imageName.indexOf("."));
            var x2 = this.options.images[srcset + "2x.png"] ? ", " + this.options.images[srcset + "2x.png"] + " 2x" : "";
            var x3 = this.options.images[srcset + "3x.png"] ? ", " + this.options.images[srcset + "3x.png"] + " 3x" : "";

            if (!button.jsObject.options.images) {
                var timerId = setInterval(function () {
                    if (button.jsObject.options.images[imageName]) {
                        if (!button.jsObject.options.isMaterial) {
                            button.image.src = button.jsObject.options.images[imageName];
                        } else {
                            button.image.srcset = button.jsObject.options.images[imageName] + " 1x, " + x2 + x3;
                        }
                        clearInterval(timerId);
                    }
                }, 50);
            }
            if (!button.jsObject.options.isMaterial) {
                button.image.src = this.options.images[imageName];
            } else {
                button.image.srcset = this.options.images[imageName] + " 1x, " + x2 + x3;
            }
        }
        button.imageCell = innerTable.addCell(button.image);
        button.imageCell.style.padding = (this.options.isTouchDevice && caption == null) ? "0 7px 0 7px" : "0 3px 0 3px";
        button.imageCell.style.lineHeight = "0";
    }

    if (caption != null || typeof (caption) == "undefined") {
        button.caption = innerTable.addCell();
        button.caption.style.padding = (arrowType ? "0px 0 " : "1px 5px ") + (imageName ? "0 0" : "0 5px");
        button.caption.style.whiteSpace = "nowrap";
        button.caption.style.textAlign = "left";
        if (caption) button.caption.innerHTML = caption;
    }

    if (arrowType != null) {
        button.arrow = document.createElement("img");
        button.arrow.src = this.options.images["ButtonArrow" + arrowType + ".png"];
        var arrowCell = innerTable.addCell(button.arrow);
        arrowCell.style.padding = caption ? "0 5px 0 5px" : (this.options.isTouchDevice ? "0 7px 0 0" : "0 5px 0 2px");
        arrowCell.style.lineHeight = "0";
    }

    if (toolTip && typeof (toolTip) != "object") { button.setAttribute("title", toolTip); }

    button.onmouseenter = function () {
        if (!this.isEnabled || ((this["haveMenu"] || this.jsObject.options.cloudMode || this.jsObject.options.designerMode) && this.isSelected) || this.jsObject.options.isTouchClick) return;
        this.className = styles["default"] + " " + this.styles["over"];
        this.isOver = true;
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object") {
            this.jsObject.options.toolTip.showWithDelay(
                this.toolTip[0],
                this.toolTip[1],
                (this.toolTip.length == 3 && this.toolTip[2].left) ? this.toolTip[2].left : this.jsObject.FindPosX(this, "stiMobileViewerMainPanel"),
                (this.toolTip.length == 3 && this.toolTip[2].top) ? this.toolTip[2].top : this.jsObject.options.toolbar.offsetHeight + 2,
                (this.toolTip.length == 3 && this.toolTip[2].rightToLeft) ? this.offsetWidth : null
            );
        }
    }

    button.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    button.onmouseleave = function () {
        this.isOver = false;
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.className = styles["default"] + " " + (this.isSelected ? this.styles["selected"] : "");
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object") this.jsObject.options.toolTip.hideWithDelay();
    }

    button.onmousedown = function () {
        if (this.isTouchStartFlag || !this.isEnabled) return;
        this.jsObject.options.buttonPressed = this;
    }

    button.onclick = function () {
        if (this.isTouchEndFlag || !this.isEnabled || this.jsObject.options.isTouchClick) return;
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object") this.jsObject.options.toolTip.hide();
        this.action();
    }

    button.ontouchend = function () {
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        this.jsObject.options.buttonsTimer = [this, this.className, setTimeout(function () {
            this_.jsObject.options.buttonsTimer = null;
            this_.className = this_.styles["default"];
            this_.action();
        }, 150)];
        this.className = styles["default"] + " " + this.styles["over"];
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    button.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.fingerIsMoved = false;
        this.jsObject.options.buttonPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    button.setEnabled = function (state) {
        if (this.image) this.image.style.opacity = state ? "1" : "0.5";
        if (this.arrow) this.arrow.style.opacity = state ? "1" : "0.5";
        this.isEnabled = state;
        if (!state && !this.isOver) this.isOver = false;
        this.className = styles["default"] + " " + (state ? (this.isOver ? this.styles["over"] : "") : this.styles["disabled"]);
    }

    button.setSelected = function (state) {
        if (this.groupName && state)
            for (var name in this.jsObject.options.buttons) {
                if (this.groupName == this.jsObject.options.buttons[name].groupName)
                    this.jsObject.options.buttons[name].setSelected(false);
            }

        this.isSelected = state;
        this.className = styles["default"] + " " +
            (state ? this.styles["selected"] : (this.isEnabled ? (this.isOver ? this.styles["over"] : "") : this.styles["disabled"]));
    }

    button.action = function () { this.jsObject.actionEvent(this.name); }

    return button;
}