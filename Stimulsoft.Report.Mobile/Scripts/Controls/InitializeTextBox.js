﻿
StiMobileViewer.prototype.TextBox = function (name, width, toolTip, actionLostFocus) {
    var textBox = document.createElement("input");
    textBox.style.fontFamily = this.options.toolbarFontFamily;
    textBox.style.color = this.options.toolbarFontColor;
    if (width) textBox.style.width = width + "px";
    textBox.jsObject = this;
    if (name != null) this.options.controls[name] = textBox;
    textBox.name = name != null ? name : this.generateKey();
    textBox.isEnabled = true;
    textBox.isSelected = false;
    textBox.isFocused = false;
    textBox.isOver = false;
    textBox.actionLostFocus = actionLostFocus;
    if (toolTip) {
        try { textBox.setAttribute("title", toolTip); } catch (e) { }
    }
    textBox.className = "stiMobileViewerTextBox";
    textBox.style.height = this.options.isTouchDevice ? "26px" : "21px";

    textBox.setEnabled = function (state) {
        this.isEnabled = state;
        this.disabled = !state;
        this.className = state ? "stiMobileViewerTextBox" : "stiMobileViewerTextBox stiMobileViewerTextBoxDisabled";
    }

    textBox.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    textBox.onmouseenter = function () {
        if (!this.isEnabled || this.readOnly || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        if (!this.isSelected && !this.isFocused) this.className = "stiMobileViewerTextBox stiMobileViewerTextBoxOver";
    }

    textBox.onmouseleave = function () {
        if (!this.isEnabled || this.readOnly) return;
        this.isOver = false;
        if (!this.isSelected && !this.isFocused) this.className = "stiMobileViewerTextBox";
    }

    textBox.setSelected = function (state) {
        this.isSelected = state;
        this.className = "stiMobileViewerTextBox " +
            (state ? "stiMobileViewerTextBoxOver" : (this.isEnabled ? (this.isOver ? "stiMobileViewerTextBoxOver" : "") : "stiMobileViewerTextBoxDisabled"));
    }

    textBox.setReadOnly = function (state) {
        this.style.cursor = state ? "default" : "";
        this.readOnly = state;
        try {
            this.setAttribute("unselectable", state ? "on" : "off");
            this.setAttribute("onselectstart", state ? "return false" : "");
        }
        catch (e) { };
    }

    textBox.onfocus = function () { this.isFocused = true; this.setSelected(true); this.oldValue = this.value; }
    textBox.onblur = function () { this.isFocused = false; this.setSelected(false); this.action(); }
    textBox.onkeypress = function (event) {
        if (this.readOnly) return false;
        if (event && event.keyCode == 13) {
            if ("blur" in this && this.actionLostFocus)
                this.blur();
            else
                this.action();
            return false;
        }
    }

    textBox.action = function () { };

    return textBox;
}