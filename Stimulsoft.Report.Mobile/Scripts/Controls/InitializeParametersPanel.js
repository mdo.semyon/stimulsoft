﻿
StiMobileViewer.prototype.InitializeParametersPanel = function () {
    if (this.options.parametersPanel) {
        this.options.parametersPanel.changeVisibleState(false);
        this.options.mainPanel.removeChild(this.options.parametersPanel);
        delete this.options.parametersPanel;
    }
    if (this.options.showToolBar && this.options.showParametersButton) {
        this.options.toolbar.controls.Parameters.setEnabled(this.options.paramsVariables != null && this.options.showPanelParameters);
    }
    if (this.options.paramsVariables == null || !this.options.showPanelParameters) return;

    var parametersPanel = document.createElement("div");
    parametersPanel.menus = {};
    this.options.parametersPanel = parametersPanel;
    this.options.mainPanel.appendChild(parametersPanel);
    parametersPanel.className = "stiMobileViewerParametersPanel stiMobileViewerParametersPanel" + (this.options.cloudMode || this.options.designerMode ? "CloudMode" : "DefaultMode");
    parametersPanel.id = this.options.mobileViewer.id + "_ParametersPanel";
    if (!this.options.isMaterial) {
        parametersPanel.style.display = "none";
    }
    parametersPanel.visible = false;
    parametersPanel.style.fontFamily = this.options.toolbarFontFamily;
    parametersPanel.style.color = (this.options.theme == "Purple" && this.options.toolbarFontColor == "#444444") ? "#ffffff" : this.options.toolbarFontColor;
    parametersPanel.jsObject = this;
    parametersPanel.currentOpeningParameter = null;
    parametersPanel.dropDownButtonWasClicked = false;
    parametersPanel.dateTimeButtonWasClicked = false;

    var innerPanel = document.createElement("div");
    parametersPanel.innerPanel = innerPanel;
    parametersPanel.appendChild(innerPanel);
    parametersPanel.style.top = (this.options.cloudMode || this.options.designerMode)
        ? this.options.toolbar.offsetHeight + "px"
        : ((this.options.showToolBar ? this.options.toolbar.offsetHeight : 0) + (this.options.findPanel ? this.options.findPanel.offsetHeight : 0) + "px");
    if (!this.options.cloudMode && !this.options.designerMode && !this.options.isMaterial) { innerPanel.style.padding = "0 3px 3px 3px"; }

    //Container
    parametersPanel.container = document.createElement("div");
    innerPanel.appendChild(parametersPanel.container);
    parametersPanel.container.className = "stiMobileViewerInnerContainerParametersPanel stiMobileViewerInnerContainerParametersPanel"
        + (this.options.cloudMode || this.options.designerMode ? "CloudMode" : "DefaultMode");
    parametersPanel.container.id = parametersPanel.id + "Container";
    if (!this.options.cloudMode && !this.options.designerMode && !this.options.isMaterial) {
        parametersPanel.container.style.maxHeight = this.options.maxHeightParametersPanel + "px";
    }
    parametersPanel.container.jsObject = this;

    if (this.options.demoMode && !this.options.isMaterial) {
        parametersPanel.container.style.border = "0px";
        parametersPanel.container.style.borderBottom = "1px solid #c6c6c6";
        parametersPanel.innerPanel.style.padding = "0px";
    }

    //Buttons
    var mainButtons = this.CreateHTMLTable();
    parametersPanel.mainButtons = mainButtons;
    mainButtons.setAttribute("align", "right");
    if (!this.options.isMaterial) {
        mainButtons.style.margin = "5px 0 10px 0";
    } else {
        parametersPanel.style.backgroundColor = "white";
        mainButtons.style.margin = "5px 0 0px 0";
        parametersPanel.style.width = "100%";
        parametersPanel.style.height = "100%";
        parametersPanel.resizePanel = function () {
            parametersPanel.container.style.height = window.innerHeight - parametersPanel.mainButtons.offsetHeight - 20 + "px";
            parametersPanel.style.left = parametersPanel.visible ? 0 : (-parseInt(parametersPanel.offsetWidth) - 10) + "px";
        }

        window.addEventListener("resize", function () {
            parametersPanel.resizePanel();
        });

        window.addEventListener("orientationchange", function () {
            parametersPanel.resizePanel();
        });
    }

    mainButtons.ID = parametersPanel.id + "MainButtons";

    if (!this.options.isMaterial) {
        parametersPanel.mainButtons.reset = this.FormButton("Reset", this.loc.Gui.cust_pm_reset, null, 80);
        parametersPanel.mainButtons.submit = this.FormButton("Submit", this.loc.Buttons.Submit, null, 80);
        mainButtons.addCell(parametersPanel.mainButtons.reset);
        mainButtons.addCell(parametersPanel.mainButtons.submit).style.paddingLeft = "10px";
    } else {
        parametersPanel.mainButtons.reset = this.BigMaterialButton("Reset", null, null, "Restore.png");
        parametersPanel.mainButtons.submit = this.BigMaterialButton("Submit", null, null, "Submit.png");
        mainButtons.addCell(parametersPanel.mainButtons.reset);
        mainButtons.addCell(parametersPanel.mainButtons.submit);

        parametersPanel.mainButtons.cancel = this.BigMaterialButton("Cancel", null, null, "Cancel.png");
        mainButtons.addCell(parametersPanel.mainButtons.cancel);
        parametersPanel.mainButtons.reset.action = function () {
            parametersPanel.jsObject.actionEvent("Reset");
        }

        parametersPanel.lastTouches = [{ x: 0, y: 0, time: 0 }, { x: 0, y: 0, time: 0}];

        var touchStart = function (this_, e) {
            this_.startX = parseInt(e.changedTouches[0].clientX);
            this_.startY = parseInt(e.changedTouches[0].clientY);
        }

        var touchEnd = function (this_, e) {
            if (!parametersPanel.btnTouched) {
                var dX = this_.lastTouches[1].x - this_.lastTouches[0].x;
                var dY = Math.abs(this_.lastTouches[1].y - this_.lastTouches[0].y);
                if (dX < 0 && new Date().getTime() - this_.lastTouches[1].time <= 9 && dY < 15) {
                    parametersPanel.changeVisibleState(false, 100, true);
                } else {
                    parametersPanel.changeVisibleState(-parseInt(this_.style.left) < parseInt(this_.style.width) / 2, 80, true);
                }
            }
            parametersPanel.btnTouched = false;
            this_.startX = null;
        }

        parametersPanel.ontouchstart = function (e) { touchStart(this, e); }
        parametersPanel.ontouchenter = function (e) { touchStart(this, e); }
        parametersPanel.ontouchmove = function (e) {
            var posX = parseInt(e.changedTouches[0].clientX);
            if (!this.startX || posX - this.startX > 0) {
                this.startX = posX;
            }
            this.style.left = Math.min(0, posX - this.startX) + "px";
            this.lastTouches.shift();
            this.lastTouches.push({ x: posX, y: parseInt(e.changedTouches[0].clientY), time: new Date().getTime() });
            //console.log(posX + "<=>" + e.changedTouches[0].clientY);
        }
        parametersPanel.ontouchend = function (e) { touchEnd(this, e); }
        parametersPanel.ontouchleave = function (e) { touchEnd(this, e); }
        parametersPanel.ontouchcancel = function (e) { touchEnd(this, e); }
    }

    if (parametersPanel.mainButtons.cancel) {
        parametersPanel.mainButtons.cancel.action = function () {
            parametersPanel.changeVisibleState(false);
        }
    }
    parametersPanel.mainButtons.submit.action = function () {
        if (this.jsObject.options.isMaterial) {
            parametersPanel.changeVisibleState(false);
        }
        this.jsObject.actionEvent(this.name);
    }


    if (!this.options.isTouchDevice) {
        if (this.options.cloudMode || this.options.designerMode) {
            parametersPanel.onscroll = function () { parametersPanel.hideAllMenus(); }
        }
        else {
            parametersPanel.container.onscroll = function () { parametersPanel.hideAllMenus(); }
        }
    }

    var duration = 250;
    parametersPanel.changeVisibleState = function (state) {
        var options = parametersPanel.jsObject.options;
        if (!options.isMaterial) {
            parametersPanel.visible = state;
            parametersPanel.style.display = state ? "" : "none";
            if (options.showToolBar && options.showParametersButton) options.toolbar.controls.Parameters.setSelected(state);
            if (options.cloudMode || options.designerMode) {
                parametersPanel.style.left = (options.bookmarksPanel && options.bookmarksPanel.visible) ? options.bookmarksTreeWidth + "px" : "0px";
                var bookmarksWidth = (options.bookmarksPanel && options.bookmarksPanel.visible) ? options.bookmarksTreeWidth : 0;
                options.reportPanel.style.marginLeft = state
                ? (bookmarksWidth + parametersPanel.offsetWidth) + "px"
                : bookmarksWidth + "px";
            }
            else {
                options.reportPanel.style.marginTop = parametersPanel.offsetHeight + (options.findPanel ? options.findPanel.offsetHeight : 0) + "px";
                if (options.bookmarksPanel != null)
                    options.bookmarksPanel.style.top = ((options.showToolBar ? options.toolbar.offsetHeight : 0) +
                        (options.findPanel ? options.findPanel.offsetHeight : 0) + parametersPanel.offsetHeight) + "px";
            }
        } else {
            if (state) {
                if (this.jsObject.options.findPanel && this.jsObject.options.findPanel.visible) {
                    this.jsObject.options.findPanel.changeVisibleState(false, 10);
                }
                this.jsObject.animate(this, { duration: duration, animations: [{ style: "left", start: this.style.left, end: 0, postfix: "px"}] });
                parametersPanel.visible = state;
            } else {
                this.jsObject.animate(this, { duration: duration, animations: [{ style: "left", start: this.style.left, end: -parseInt(this.offsetWidth) - 10, postfix: "px"}] });
                this.visible = state;
            }
        }

    }

    parametersPanel.addParameters = function () {
        var paramsVariables = this.jsObject.copyObject(parametersPanel.jsObject.options.paramsVariables);
        countParameters = this.jsObject.getCountObjects(paramsVariables);
        var countColumns = 1;
        if (!this.jsObject.options.isMaterial) {
            countColumns = (countParameters <= 5) ? 1 : parametersPanel.jsObject.options.countColumnsParameters;
        }
        countInColumn = parseInt(countParameters / countColumns);
        if (countInColumn * countColumns < countParameters) countInColumn++;

        var table = document.createElement("Table");
        table.cellPadding = 0;
        table.cellSpacing = 0;
        table.style.border = "0px";
        if (this.jsObject.options.isMaterial) {
            table.style.width = "100%";
            this.itemsTable = table;
        }
        tbody = document.createElement("TBODY");
        table.appendChild(tbody);
        this.container.appendChild(table);

        cellsVar = {};
        for (indexRow = 0; indexRow < countInColumn + 1; indexRow++) {
            row = document.createElement("TR");
            tbody.appendChild(row);

            for (indexColumn = 0; indexColumn < countColumns; indexColumn++) {
                cellForName = document.createElement("TD");
                cellForName.style.padding = "0px 10px 0px " + ((indexColumn > 0) ? "30px" : "0px");
                row.appendChild(cellForName);
                if (this.jsObject.options.isMaterial) {
                    cellForName.style.padding = "0px 0px 0px " + ((indexColumn > 0) ? "30px" : "0px");
                    row = document.createElement("TR");
                    tbody.appendChild(row);
                }

                cellForControls = document.createElement("TD");
                cellForControls.style.padding = "0px";
                row.appendChild(cellForControls);

                cellsVar[indexRow + ";" + indexColumn + "name"] = cellForName;
                cellsVar[indexRow + ";" + indexColumn + "controls"] = cellForControls;
            }
        }

        indexColumn = 0;
        indexRow = 0;

        for (var index = 0; index < countParameters; index++) {
            var nameCell = cellsVar[indexRow + ";" + indexColumn + "name"];
            nameCell.style.whiteSpace = "nowrap";
            nameCell.innerHTML = paramsVariables[index].alias;
            if (paramsVariables[index].basicType == "Range" && parametersPanel.jsObject.options.designerMode) {
                nameCell.style.verticalAlign = "top";
                nameCell.style.paddingTop = parametersPanel.jsObject.options.isTouchDevice ? "11px" : "9px";
            }
            cellsVar[indexRow + ";" + indexColumn + "controls"].appendChild(parametersPanel.jsObject.CreateParameter(paramsVariables[index]));
            indexRow++;
            if (index == countParameters - 1) cellsVar[indexRow + ";" + indexColumn + "controls"].appendChild(parametersPanel.mainButtons);
            if (indexRow == countInColumn) { indexRow = 0; indexColumn++; }
        }

        if (this.jsObject.options.isMaterial) {
            this.innerPanel.appendChild(parametersPanel.mainButtons);
            parametersPanel.resizePanel();
        }
    }

    parametersPanel.clearParameters = function () {
        while (parametersPanel.container.childNodes[0]) {
            parametersPanel.container.removeChild(parametersPanel.container.childNodes[0]);
        }
    }

    parametersPanel.getParametersValues = function () {
        parametersValues = {};

        for (var name in parametersPanel.jsObject.options.parameters) {
            var parameter = parametersPanel.jsObject.options.parameters[name];
            parametersValues[name] = parameter.getValue();
        }

        return parametersValues;
    }

    parametersPanel.getParametersValuesForReportServer = function () {
        parametersValues = [];

        for (var name in parametersPanel.jsObject.options.parameters) {
            var parameter = parametersPanel.jsObject.options.parameters[name];
            parametersValues.push(parameter.getParameterObjectForReportServer());
        }

        return parametersValues;
    }

    parametersPanel.hideAllMenus = function () {
        if (parametersPanel.jsObject.options.currentMenu) parametersPanel.jsObject.options.currentMenu.changeVisibleState(false);
        if (parametersPanel.jsObject.options.currentDatePicker) parametersPanel.jsObject.options.currentDatePicker.changeVisibleState(false);
    }

    this.options.parameters = {};
    parametersPanel.addParameters();
    if (!this.options.isMaterial) {
        parametersPanel.changeVisibleState(true);
    } else {
        parametersPanel.style.top = 0;
        parametersPanel.style.left = -parametersPanel.offsetWidth + "px";
    }
}

//Button
StiMobileViewer.prototype.ParameterButton = function (buttonType, parameter) {
    var button = this.SmallButton(null, null, null, buttonType + ".png", null, null, this.GetStyles("ParametersButton"));
    button.style.width = button.style.height = this.options.isTouchDevice ? "26px" : "21px";
    button.innerTable.style.width = "100%";
    button.imageCell.style.padding = "0px";
    button.imageCell.style.textAlign = "center";
    button.parameter = parameter;
    button.buttonType = buttonType;

    return button;
}

//TextBox
StiMobileViewer.prototype.ParameterTextBox = function (parameter) {
    var textBox = this.TextBox(null, null, null, true);
    textBox.parameter = parameter;
    if (parameter.params.type == "Char") textBox.maxLength = 1;

    var width = "210px";
    if (parameter.params.basicType == "Range") {
        width = "140px";
        if (parameter.params.type == "Guid" || parameter.params.type == "String") width = "190px";
        if (parameter.params.type == "DateTime") width = "235px";
        if (parameter.params.type == "Char") width = "60px";
    }
    else {
        if (parameter.params.type == "Guid" && !this.options.isMaterial) width = "265px"; 
        else width = (parameter.params.basicType == "NullableValue" && this.options.isMaterial) ? "180px" : "210px";
    }
    textBox.style.width = width;

    if (parameter.params.type == "DateTime") {
        textBox.action = function () {
            if (this.oldValue == this.value) return;
            try {
                var timeString = new Date().toLocaleTimeString();
                var isAmericanFormat = timeString.toLowerCase().indexOf("am") >= 0 || timeString.toLowerCase().indexOf("pm") >= 0;
                var formatDate = isAmericanFormat ? "MM/dd/yyyy" : "dd.MM.yyyy";                
                var format = formatDate + (isAmericanFormat ? " h:mm:ss tt" : " hh:mm:ss"); 
                if (textBox.parameter.params.dateTimeType == "Date") format = formatDate;
                if (textBox.parameter.params.dateTimeType == "Time") format = "hh:mm:ss";
                var date = textBox.jsObject.GetDateTimeFromString(this.value, format);
                var dateTimeObject = textBox.jsObject.getDateTimeObject(date);                
                textBox.parameter.params[textBox.parameter.controls.secondTextBox == textBox ? "keyTo" : "key"] = dateTimeObject;
                textBox.value = textBox.jsObject.dateTimeObjectToString(dateTimeObject, textBox.parameter.params.dateTimeType);
            }
            catch (e) {
                alert(e);
            }
        }
    }

    return textBox;
}

//CheckBox
StiMobileViewer.prototype.ParameterCheckBox = function (parameter, caption) {
    var checkBox = this.CheckBox(null, caption);
    checkBox.parameter = parameter;

    return checkBox;
}

//Menu
StiMobileViewer.prototype.ParameterMenu = function (parameter) {
    var menu = this.BaseMenu(null, parameter.controls.dropDownButton, "Down");
    menu.parameter = parameter;

    var cumulativeOffset = function (element) {
        var top = 0, left = 0;
        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);

        return {
            top: top,
            left: left
        };
    }

    menu.changeVisibleState = function (state, parentButton) {
        var mainClassName = "stiMobileViewerMainPanel";
        if (parentButton) {
            this.parentButton = parentButton;
            parentButton.haveMenu = true;
        }
        if (state) {
            this.onshow();
            this.style.display = "";
            this.visible = true;
            this.style.overflow = "hidden";
            this.parentButton.setSelected(true);
            this.jsObject.options.currentMenu = this;
            this.style.width = this.innerContent.offsetWidth + "px";
            this.style.height = this.innerContent.offsetHeight + "px";
            this.style.left = (this.jsObject.FindPosX(parameter, mainClassName)) + "px";
            if (this.jsObject.options.isMaterial) {
                var offset = cumulativeOffset(this.parameter);
                if (offset.top - this.jsObject.options.parametersPanel.itemsTable.parentElement.scrollTop + this.offsetHeight + 20 > window.innerHeight) {
                    this.animationDirection = "Up";
                } else {
                    this.animationDirection = "Down";
                }
            }
            this.style.top = (this.animationDirection == "Down")
                ? (this.jsObject.FindPosY(this.parentButton, mainClassName) + this.parentButton.offsetHeight + 2) + "px"
                : (this.jsObject.FindPosY(this.parentButton, mainClassName) - this.offsetHeight) + "px";
            this.innerContent.style.top = ((this.animationDirection == "Down" ? -1 : 1) * this.innerContent.offsetHeight) + "px";
            parameter.menu = this;

            d = new Date();
            var endTime = d.getTime() + this.jsObject.options.menuAnimDuration;
            this.jsObject.ShowAnimationVerticalMenu(this, (this.animationDirection == "Down" ? 0 : -1), endTime);
        }
        else {
            this.onHide();
            clearTimeout(this.innerContent.animationTimer);
            this.visible = false;
            this.parentButton.setSelected(false);
            this.style.display = "none";
            this.jsObject.options.mainPanel.removeChild(this);
            parameter.menu = null;
            if (this.jsObject.options.currentMenu == this) this.jsObject.options.currentMenu = null;
        }
    }

    var table = this.CreateHTMLTable();
    table.style.fontFamily = this.options.toolbarFontFamily;
    table.style.color = this.options.toolbarFontColor;
    table.style.fontSize = "12px";
    table.style.width = (parameter.offsetWidth - 5) + "px";
    table.className = "stiMobileViewerParametersMenuInnerTable";
    menu.innerContent.appendChild(table);
    menu.innerTable = table;

    return menu;
}

//MenuItem
StiMobileViewer.prototype.parameterMenuItem = function (parameter) {
    var menuItem = document.createElement("Div");
    menuItem.jsObject = this;
    menuItem.parameter = parameter;
    menuItem.isOver = false;
    menuItem.className = "stiMobileViewerParametersMenuItem";
    menuItem.style.height = this.options.isTouchDevice ? "30px" : "24px";

    var table = this.CreateHTMLTable();
    table.className = "stiMobileViewerInnerTable";
    menuItem.appendChild(table);

    menuItem.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    menuItem.onmouseenter = function () {
        if (this.jsObject.options.isTouchClick) return;
        this.className = "stiMobileViewerParametersMenuItemOver";
        this.isOver = true;
    }

    menuItem.onmouseleave = function () {
        this.className = "stiMobileViewerParametersMenuItem";
        this.isOver = false;
    }

    menuItem.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.className = "stiMobileViewerParametersMenuItemPressed";
    }

    menuItem.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        this.parameter.jsObject.options.fingerIsMoved = false;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    menuItem.onmouseup = function () {
        if (this.isTouchEndFlag) return;
        this.parameter.jsObject.TouchEndMenuItem(this, false);
    }

    menuItem.ontouchend = function () {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        this.parameter.jsObject.TouchEndMenuItem(this, true);
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    menuItem.innerContainer = table.addCell();
    menuItem.innerContainer.style.padding = "0 5px 0 5px";

    return menuItem;
}

StiMobileViewer.prototype.TouchEndMenuItem = function (menuItem, flag) {
    if (!menuItem || menuItem.parameter.jsObject.options.fingerIsMoved) return;

    if (flag) {
        menuItem.className = "stiMobileViewerParametersMenuItemPressed";
        if (typeof event !== "undefined" && ('preventDefault' in event)) event.preventDefault();
        var this_ = this;
        setTimeout(function () {
            this_.TouchEndMenuItem(menuItem, false);
        }, 200);
        return;
    }

    menuItem.className = menuItem.isOver ? "stiMobileViewerParametersMenuItemOver" : "stiMobileViewerParametersMenuItem";
    if (menuItem.action != null) menuItem.action();
}

//MenuSeparator
StiMobileViewer.prototype.parameterMenuSeparator = function () {
    var separator = document.createElement("Div");
    separator.className = "stiMobileViewerParametersMenuSeparator";

    return separator;
}

//Menu For Value
StiMobileViewer.prototype.parameterMenuForValue = function (parameter) {
    var menuParent = this.ParameterMenu(parameter);
    for (var index in parameter.params.items) {
        var cell = menuParent.innerTable.addCellInNextRow();
        var menuItem = this.parameterMenuItem(parameter);
        cell.appendChild(menuItem);

        menuItem.id = parameter.jsObject.options.mobileViewer.id + parameter.params.name + "Item" + index;
        menuItem.parameter = parameter;
        menuItem.key = parameter.params.items[index].key;
        menuItem.value = parameter.params.items[index].value;
        menuItem.innerContainer.innerHTML =
            (menuItem.value != "" && parameter.params.type != "DateTime" && parameter.params.type != "TimeSpan" && parameter.params.type != "Bool")
                ? menuItem.value
                : this.getStringKey(menuItem.key, menuItem.parameter);

        menuItem.action = function () {
            this.parameter.params.key = this.key;
            if (this.parameter.params.type != "Bool")
                this.parameter.controls.firstTextBox.value = (this.parameter.params.type == "DateTime" || this.parameter.params.type == "TimeSpan")
                    ? this.parameter.jsObject.getStringKey(this.key, this.parameter)
                    : (this.parameter.params.allowUserValues ? this.key : (this.value != "" ? this.value : this.key));
            else
                this.parameter.controls.boolCheckBox.setChecked(this.key == "True");
            this.parameter.changeVisibleStateMenu(false);

            if (this.parameter.params.binding) {
                var jsObject = this.jsObject;
                jsObject.sendToServer("RefreshVariables", { variables: jsObject.options.parametersPanel.getParametersValues() }, function (data) {
                    if (data.paramsVariables) {
                        jsObject.options.paramsVariables = data.paramsVariables;
                        jsObject.InitializeParametersPanel();
                        jsObject.options.processImage.hide();
                    }
                });
            }
        }
    }

    return menuParent;
}

//Menu For Range
StiMobileViewer.prototype.parameterMenuForRange = function (parameter) {
    var menuParent = this.ParameterMenu(parameter);

    for (var index in parameter.params.items) {
        var cell = menuParent.innerTable.addCellInNextRow();
        var menuItem = this.parameterMenuItem(parameter);
        cell.appendChild(menuItem);

        menuItem.id = parameter.jsObject.options.mobileViewer.id + parameter.params.name + "Item" + index;
        menuItem.parameter = parameter;
        menuItem.value = parameter.params.items[index].value;
        menuItem.key = parameter.params.items[index].key;
        menuItem.keyTo = parameter.params.items[index].keyTo;
        menuItem.innerContainer.innerHTML = menuItem.value + " [" + this.getStringKey(menuItem.key, menuItem.parameter) +
            " - " + this.getStringKey(menuItem.keyTo, menuItem.parameter) + "]";

        menuItem.action = function () {
            this.parameter.params.key = this.key;
            this.parameter.params.keyTo = this.keyTo;
            this.parameter.controls.firstTextBox.value = this.parameter.jsObject.getStringKey(this.key, this.parameter);
            this.parameter.controls.secondTextBox.value = this.parameter.jsObject.getStringKey(this.keyTo, this.parameter);
            this.parameter.changeVisibleStateMenu(false);
        }
    }

    return menuParent;
}

//Menu For ListNotEdit
StiMobileViewer.prototype.parameterMenuForNotEditList = function (parameter) {
    var menuParent = this.ParameterMenu(parameter);
    menuParent.menuItems = {};
    var selectedAll = true;

    var checkBoxSelectAll = this.CheckBox(null, this.loc.MainMenu.menuEditSelectAll.replace("&", ""));
    menuParent.checkBoxSelectAll = checkBoxSelectAll;
    checkBoxSelectAll.style.margin = "8px 7px 8px 7px";
    menuParent.innerTable.addCellInNextRow(checkBoxSelectAll);
    menuParent.innerTable.addCellInNextRow(this.parameterMenuSeparator());        
    checkBoxSelectAll.setChecked(selectedAll);
    checkBoxSelectAll.action = function () {
        var selectAll = this.isChecked;
        for (var index in parameter.params.items) {
           menuParent.menuItems[index].checkBox.setChecked(selectAll, true);
        }
        menuParent.updateItems();
    }

    menuParent.updateItems = function () {
        parameter.params.items = {};
        parameter.controls.firstTextBox.value = "";
        var selectAll = true;

        for (var index in this.menuItems) {
            parameter.params.items[index] = {};
            parameter.params.items[index].key = this.menuItems[index].key;
            parameter.params.items[index].value = this.menuItems[index].value;
            parameter.params.items[index].isChecked = this.menuItems[index].checkBox.isChecked;

            if (selectAll && !this.menuItems[index].checkBox.isChecked) {
                selectAll = false;
            }

            if (parameter.params.items[index].isChecked) {
                if (parameter.controls.firstTextBox.value != "") parameter.controls.firstTextBox.value += ";";
                parameter.controls.firstTextBox.value += this.menuItems[index].value != ""
                    ? this.menuItems[index].value : parameter.jsObject.getStringKey(this.menuItems[index].key, parameter);
            }
        }

        this.checkBoxSelectAll.setChecked(selectAll);
    }

    for (var index in parameter.params.items) {
        var cell = menuParent.innerTable.addCellInNextRow();
        menuItem = this.parameterMenuItem(parameter);
        cell.appendChild(menuItem);

        menuItem.action = null;
        menuItem.id = parameter.jsObject.options.mobileViewer.id + parameter.params.name + "Item" + index;
        menuItem.parameter = parameter;
        menuItem.value = parameter.params.items[index].value;
        menuItem.key = parameter.params.items[index].key;
        menuParent.menuItems[index] = menuItem;

        var innerTable = this.CreateHTMLTable();        
        innerTable.style.width = "100%";
        menuItem.innerContainer.appendChild(innerTable);
        var cellCheck = innerTable.addCell();

        var checkBox = this.ParameterCheckBox(parameter, menuItem.value != "" ? menuItem.value : this.getStringKey(menuItem.key, menuItem.parameter));
        checkBox.style.marginRight = "5px";
        checkBox.style.width = "100%";
        checkBox.imageBlock.parentElement.style.width = "1px";
        cellCheck.appendChild(checkBox);
        checkBox.menuParent = menuParent;
        checkBox.setChecked(parameter.params.items[index].isChecked);
        menuItem.checkBox = checkBox;
        if (!checkBox.isChecked) selectedAll = false;

        checkBox.onChecked = function () {
            menuParent.updateItems();
        }
    }

    var closeButton = this.parameterMenuItem(parameter);
    closeButton.id = parameter.jsObject.options.mobileViewer.id + parameter.params.name + "ItemClose";
    closeButton.innerContainer.innerHTML = this.loc.Buttons.Close;
    closeButton.innerContainer.style.paddingLeft = "13px";
    closeButton.action = function () { this.parameter.changeVisibleStateMenu(false); }
    menuParent.innerTable.addCellInNextRow(this.parameterMenuSeparator());
    menuParent.innerTable.addCellInNextRow(closeButton);

    return menuParent;
}

//Menu For ListEdit
StiMobileViewer.prototype.parameterMenuForEditList = function (parameter) {
    var menuParent = this.ParameterMenu(parameter);

    //New Item Method
    menuParent.newItem = function (item, parameter) {
        var menuItem = parameter.jsObject.parameterMenuItem(parameter);
        //cell.appendChild(menuItem);
        menuItem.id = parameter.jsObject.options.mobileViewer.id + parameter.params.name + "Item" + parameter.jsObject.newGuid().replace(/-/g, '');
        menuItem.onmouseover = null;
        menuItem.onmousedown = null;
        menuItem.ontouchend = null;
        menuItem.action = null;
        menuItem.parameter = parameter;
        menuItem.value = item.value;
        menuItem.key = item.key;

        var innerTable = menuItem.jsObject.CreateHTMLTable();
        menuItem.innerContainer.appendChild(innerTable);

        //Text Box        
        var textBox = parameter.jsObject.ParameterTextBox(parameter);
        menuItem.textBox = textBox;
        //textBox.setReadOnly(parameter.params.type == "DateTime");        
        textBox.value = parameter.jsObject.getStringKey(menuItem.key, menuItem.parameter);
        textBox.thisMenu = menuParent;
        innerTable.addCell(textBox).style.padding = "0 1px 0 0";

        //DateTime Button
        if (parameter.params.type == "DateTime") {
            var dateTimeButton = parameter.jsObject.ParameterButton("DateTimeButton", parameter);
            dateTimeButton.id = menuItem.id + "DateTimeButton";
            dateTimeButton.parameter = parameter;
            dateTimeButton.thisItem = menuItem;
            innerTable.addCell(dateTimeButton).style.padding = "0 1px 0 1px";

            dateTimeButton.action = function () {
                var datePicker = dateTimeButton.jsObject.options.datePicker || dateTimeButton.jsObject.InitializeDatePicker();
                datePicker.ownerValue = this.thisItem.key;
                datePicker.parentDataControl = this.thisItem.textBox;
                datePicker.parentButton = this;
                datePicker.changeVisibleState(!datePicker.visible);
            }
        }

        //Guid Button
        if (parameter.params.type == "Guid") {
            var guidButton = parameter.jsObject.ParameterButton("GuidButton", parameter);
            guidButton.id = menuItem.id + "GuidButton";
            guidButton.thisItem = menuItem;
            guidButton.thisMenu = menuParent;
            innerTable.addCell(guidButton).style.padding = "0 1px 0 1px";

            guidButton.action = function () {
                this.thisItem.textBox.value = this.parameter.jsObject.newGuid();
                this.thisMenu.updateItems();
            }
        }

        //Remove Button                        
        var removeButton = parameter.jsObject.ParameterButton("RemoveItemButton", parameter);
        removeButton.id = menuItem.id + "RemoveButton";
        removeButton.itemsContainer = this.itemsContainer;
        removeButton.thisItem = menuItem;
        removeButton.thisMenu = menuParent;
        innerTable.addCell(removeButton).style.padding = "0 1px 0 1px";
        removeButton.action = function () {
            this.itemsContainer.removeChild(this.thisItem);
            this.thisMenu.updateItems();
        }

        return menuItem;
    }

    //Update Items
    menuParent.updateItems = function () {
        this.parameter.params.items = {};
        this.parameter.controls.firstTextBox.value = "";
        for (index = 0; index < this.itemsContainer.childNodes.length; index++) {
            itemMenu = this.itemsContainer.childNodes[index];
            this.parameter.params.items[index] = {};
            this.parameter.params.items[index].key =
                (this.parameter.params.type == "DateTime")
                ? itemMenu.key
                : itemMenu.textBox.value;
            this.parameter.params.items[index].value = itemMenu.value;
            if (this.parameter.controls.firstTextBox.value != "") this.parameter.controls.firstTextBox.value += ";";
            this.parameter.controls.firstTextBox.value += this.parameter.jsObject.getStringKey(this.parameter.params.items[index].key, this.parameter);
        }

        if (this.parameter.menu.innerTable.offsetHeight > 400) this.parameter.menu.style.height = "350px;"
        else this.parameter.menu.style.height = this.parameter.menu.innerTable.offsetHeight + "px";
    }

    //New Item Button
    var newItemButton = this.parameterMenuItem(parameter);
    menuParent.innerTable.addCell(newItemButton);
    newItemButton.id = parameter.jsObject.options.mobileViewer.id + parameter.params.name + "ItemNew";
    newItemButton.innerContainer.innerHTML = this.loc.FormDictionaryDesigner.NewItem;
    newItemButton.thisMenu = menuParent;
    newItemButton.action = function () {
        var item_ = {};
        if (this.parameter.params.type == "DateTime") {
            item_.key = this.parameter.jsObject.getDateTimeObject();
            item_.value = this.parameter.jsObject.dateTimeObjectToString(item_.key, this.parameter);
        }
        else if (this.parameter.params.type == "TimeSpan") {
            item_.key = "00:00:00";
            item_.value = "00:00:00";
        }
        else if (this.parameter.params.type == "Bool") {
            item_.key = "False";
            item_.value = "False";
        }
        else {
            item_.key = "";
            item_.value = "";
        }
        var newItem = this.thisMenu.newItem(item_, this.parameter);
        this.thisMenu.itemsContainer.appendChild(newItem);
        if ("textBox" in newItem) newItem.textBox.focus();
        this.thisMenu.updateItems();
    }

    //Add Items
    var cellItems = menuParent.innerTable.addCellInNextRow();
    menuParent.itemsContainer = cellItems;

    for (var index in parameter.params.items) {
        cellItems.appendChild(menuParent.newItem(parameter.params.items[index], parameter));
    }

    var cellDown = menuParent.innerTable.addCellInNextRow();

    //Remove All Button
    var removeAllButton = this.parameterMenuItem(parameter);
    cellDown.appendChild(removeAllButton);
    removeAllButton.id = parameter.jsObject.options.mobileViewer.id + parameter.params.name + "ItemRemoveAll";
    removeAllButton.innerContainer.innerHTML = this.loc.Buttons.RemoveAll;
    removeAllButton.thisMenu = menuParent;
    removeAllButton.action = function () {
        while (this.thisMenu.itemsContainer.childNodes[0]) {
            this.thisMenu.itemsContainer.removeChild(this.thisMenu.itemsContainer.childNodes[0]);
        }
        this.thisMenu.updateItems();
    }

    //Close Button
    cellDown.appendChild(this.parameterMenuSeparator());
    var closeButton = this.parameterMenuItem(parameter);
    cellDown.appendChild(closeButton);
    closeButton.id = parameter.jsObject.options.mobileViewer.id + parameter.params.name + "ItemClose";
    closeButton.innerContainer.innerHTML = this.loc.Buttons.Close;
    closeButton.action = function () { this.parameter.changeVisibleStateMenu(false); }

    menuParent.onHide = function () {
        this.updateItems();
    }

    return menuParent;
}

StiMobileViewer.prototype.ReplaceMonths = function (value) {
    for (var i = 1; i <= 12; i++) {
        var enName = "";
        var locName = "";
        switch (i) {
            case 1:
                enName = "January";
                locName = this.loc.A_WebViewer.MonthJanuary;
                break;

            case 2:
                enName = "February";
                locName = this.loc.A_WebViewer.MonthFebruary;
                break;

            case 3:
                enName = "March";
                locName = this.loc.A_WebViewer.MonthMarch;
                break;

            case 4:
                enName = "April";
                locName = this.loc.A_WebViewer.MonthApril;
                break;

            case 5:
                enName = "May";
                locName = this.loc.A_WebViewer.MonthMay;
                break;

            case 6:
                enName = "June";
                locName = this.loc.A_WebViewer.MonthJune;
                break;

            case 7:
                enName = "July";
                locName = this.loc.A_WebViewer.MonthJuly;
                break;

            case 8:
                enName = "August";
                locName = this.loc.A_WebViewer.MonthAugust;
                break;

            case 9:
                enName = "September";
                locName = this.loc.A_WebViewer.MonthSeptember;
                break;

            case 10:
                enName = "October";
                locName = this.loc.A_WebViewer.MonthOctober;
                break;

            case 11:
                enName = "November";
                locName = this.loc.A_WebViewer.MonthNovember;
                break;

            case 12:
                enName = "December";
                locName = this.loc.A_WebViewer.MonthDecember;
                break;
        }

        var enShortName = enName.substring(0, 3);
        var locShortName = locName.substring(0, 3);
        value = value.replace(enName, i).replace(enName.toLowerCase(), i).replace(enShortName, i).replace(enShortName.toLowerCase(), i);
        value = value.replace(locName, i).replace(locName.toLowerCase(), i).replace(locShortName, i).replace(locShortName.toLowerCase(), i);
    }

    return value;
}

StiMobileViewer.prototype.GetDateTimeFromString = function (value, format) {
    var charIsDigit = function (char) {
        return ("0123456789".indexOf(char) >= 0);
    }

    if (!value) return new Date();
    value = this.ReplaceMonths(value);

    var dateTime = new Date();

    // If the date format is not specified, then deserializator for getting date and time is applied
    if (format == null) format = "dd.MM.yyyy hh:mm:ss";
    // Otherwise the format is parsed. Now only numeric date and time formats are supported

    var year = 1970;
    var month = 1;
    var day = 1;
    var hour = 0;
    var minute = 0;
    var second = 0;
    var millisecond = 0;

    var char = "";
    var pos = 0;
    var values = [];

    // Parse date and time into separate numeric values
    while (pos < value.length) {
        char = value.charAt(pos);
        if (charIsDigit(char)) {
            values.push(char);
            pos++;

            while (pos < value.length && charIsDigit(value.charAt(pos))) {
                values[values.length - 1] += value.charAt(pos);
                pos++;
            }

            values[values.length - 1] = this.StrToInt(values[values.length - 1]);
        }

        pos++;
    }

    pos = 0;
    var charCount = 0;
    var index = -1;
    var is12hour = false;

    // Parsing format and replacement of appropriate values of date and time
    while (pos < format.length) {
        char = format.charAt(pos);
        charCount = 0;

        if (char == "Y" || char == "y" || char == "M" || char == "d" || char == "h" || char == "H" ||
						char == "m" || char == "s" || char == "f" || char == "F" || char == "t" || char == "z") {
            index++;

            while (pos < format.length && format.charAt(pos) == char) {
                pos++;
                charCount++;
            }
        }

        switch (char) {
            case "Y": // full year
                year = values[index];
                break;

            case "y": // year
                if (values[index] < 1000) year = 2000 + values[index];
                else year = values[index];
                break;

            case "M": // month
                month = values[index];
                break;

            case "d": // day
                day = values[index];
                break;

            case "h": // (hour 12)
                is12hour = true;

            case "H": // (hour 24)
                hour = values[index];
                break;

            case "m": // minute
                minute = values[index];
                break;

            case "s": // second
                second = values[index];
                break;

            case "f": // second fraction
            case "F": // second fraction, trailing zeroes are trimmed
                millisecond = values[index];
                break;

            case "t": // PM or AM
                if (value.toLowerCase().indexOf("am") >= 0 && hour == 12) hour = 0;
                if (value.toLowerCase().indexOf("pm") >= 0 && hour < 12) hour += 12;
                break;

            default:
                pos++;
                break;
        }
    }

    dateTime = new Date(year, month - 1, day, hour, minute, second, millisecond);

    return dateTime;
}