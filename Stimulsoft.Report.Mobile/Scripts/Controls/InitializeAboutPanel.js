﻿
StiMobileViewer.prototype.InitializeAboutPanel = function () {
    var aboutPanel = document.createElement("div");
    this.options.aboutPanel = aboutPanel;
    this.options.mainPanel.appendChild(aboutPanel);
    aboutPanel.jsObject = this;
    aboutPanel.className = "stiMobileViewerAboutPanel";
    aboutPanel.style.background = "url(" + this.options.images["AboutInfo.png"] + ")";
    aboutPanel.style.display = "none";

    var header = document.createElement("div");
    header.innerHTML = "Stimulsoft Reports";
    header.className = "stiMobileViewerAboutPanelHeader";
    aboutPanel.appendChild(header);

    var copyRight = document.createElement("div");
    copyRight.innerHTML = "Copyright 2003-" + new Date().getFullYear() + " Stimulsoft";
    copyRight.className = "stiMobileViewerAboutPanelCopyright";
    aboutPanel.appendChild(copyRight);

    var version = document.createElement("div");
    version.innerHTML = "Version " + this.options.productVersion;
    if (!this.options.jsMode) version.innerHTML += ", ASP.NET";
    version.innerHTML += ", JS";
    version.className = "stiMobileViewerAboutPanelVersion";
    aboutPanel.appendChild(version);

    var allRight = document.createElement("div");
    allRight.innerHTML = "All rights reserved";
    allRight.className = "stiMobileViewerAboutPanelVersion";
    aboutPanel.appendChild(allRight);

    var stiLink = document.createElement("div");
    stiLink.innerHTML = "www.stimulsoft.com";
    stiLink.className = "stiMobileViewerAboutPanelStiLink";
    aboutPanel.appendChild(stiLink);

    stiLink.onclick = function (event) {
        if (event) {
            event.stopPropagation();
            event.preventDefault();
        }
        window.open("https://www.stimulsoft.com");
    };

    aboutPanel.ontouchend = function () { this.changeVisibleState(false); }
    if (!this.options.isMaterial) {
        aboutPanel.onclick = function () { this.changeVisibleState(false); }
    }

    aboutPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        this.jsObject.SetObjectToCenter(this);
        this.jsObject.options.disabledPanels[2].changeVisibleState(state);
        this.visible = state;
        this.jsObject.options.currentForm = state ? this : null;
    }

    return aboutPanel;
}