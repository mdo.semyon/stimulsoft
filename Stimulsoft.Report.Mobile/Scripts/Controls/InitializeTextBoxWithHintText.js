﻿
StiMobileViewer.prototype.TextBoxWithHintText = function (name, width, hintText, toolTip, imageName) {

    var table = this.CreateHTMLTable();
    var textBox = this.TextBox(name, width, toolTip);
    textBox.setAttribute("placeholder", hintText);
    textBox.style.backgroundColor = "#f2f1f1";
    textBox.style.border = "none";
    textBox.style.width = "280px";
    textBox.style.height = "30px";

    var img = document.createElement("img");
    img.src = this.options.images[imageName];
    img.style.verticalAlign = "middle";
    img.style.opacity = "0.8";

    var iim = table.addCell(img);
    iim.style.lineHeight = "0";
    iim.style.backgroundColor = "#f2f1f1";
    iim.fileName = img;
    iim.style.width = "22px";
    iim.style.textAlign = "right";
    table.addCell(textBox);
    textBox.table = table;

    //Override
    textBox.onblur = function () { this.isFocused = false; this.setSelected(false); }

    return textBox;
}