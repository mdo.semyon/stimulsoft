﻿
StiMobileViewer.prototype.InitializeToolBar = function () {
    var toolbar = document.createElement("div");
    toolbar.controls = {};
    toolbar.shortType = false;
    toolbar.minWidth = 0;
    this.options.toolbar = toolbar;
    this.options.mainPanel.appendChild(toolbar);
    toolbar.jsObject = this;
    toolbar.className = "stiMobileViewerToolBar";
    if (this.options.cloudMode || this.options.designerMode) { toolbar.className += " stiMobileViewerToolBarCloudMode"; }
    if (!this.options.showToolBar) {
        toolbar.style.height = "0px";
        toolbar.style.width = "0px";
    }
    var toolbarInnerContent = document.createElement("div");
    toolbar.innerContent = toolbarInnerContent;
    toolbar.appendChild(toolbarInnerContent);
    if (!this.options.isMaterial) {
        toolbarInnerContent.style.padding = this.options.cloudMode || this.options.designerMode ? "1px" : "3px";
    }
    var toolbarTable = this.CreateHTMLTable();
    toolbarInnerContent.appendChild(toolbarTable);
    toolbarTable.className = "stiMobileViewerToolBarTable";
    if (this.options.cloudMode || this.options.designerMode || this.options.demoMode) { toolbarTable.style.border = "0px" };

    if (this.options.toolBarBackColor != "Empty") {
        toolbarTable.style.background = this.options.toolBarBackColor;
        toolbarTable.style.border = "1px solid " + this.options.toolBarBackColor;
    }

    toolbarTable.style.color = (this.options.theme == "Purple" && this.options.toolbarFontColor == "#444444") ? "#ffffff" : this.options.toolbarFontColor;
    toolbarTable.style.fontFamily = this.options.toolbarFontFamily;

    var cell1 = toolbarTable.addCell();
    var cell2 = toolbarTable.addCell();
    var mainCell = (!this.options.rightToLeft) ? cell1 : cell2;
    var dopCell = (!this.options.rightToLeft) ? cell2 : cell1;
    mainCell.style.width = "100%";
    var mainTable = this.CreateHTMLTable();
    var dopTable = this.CreateHTMLTable();
    mainCell.appendChild(mainTable);
    dopCell.appendChild(dopTable);
    mainTable.setAttribute("align", this.options.rightToLeft ? "right" : (this.options.toolbarAlignment == "default" ? "left" : this.options.toolbarAlignment));
    mainTable.style.margin = "1px";
    dopTable.style.margin = "1px";

    //debugger;
    //Add Controls
    //1 - name, 2 - caption, 3 - image, 4 - showToolTip;
    var isFirst = true;
    var controlProps = []
    if (this.options.showAboutButton) controlProps.push(["About", null, "About.png", false]);
    if (this.options.showAboutButton && this.options.showDesignButton) controlProps.push(["Separator1"]);
    if (this.options.showDesignButton) controlProps.push(["Design", this.loc.Buttons.Design, "Design.png", false]);
    if (this.options.showPrintButton) {
        controlProps.push(["Print", this.loc.A_WebViewer.PrintReport, this.options.cloudMode ? "CloudIcons.ReportPrint20.png" : "Print.png", this.loc.HelpViewer.Print]); isFirst = false;
    }
    if (this.options.showSave) {
        controlProps.push(["Save", this.loc.A_WebViewer.SaveReport, this.options.cloudMode ? "CloudIcons.Save20.png" : "Save.png", this.loc.HelpViewer.Save]);
        isFirst = false;
    }
    if (this.options.showBookmarksButton || this.options.showParametersButton) {
        if (!isFirst) controlProps.push(["Separator2"]);
        isFirst = false;
    }
    if (this.options.showBookmarksButton) {
        controlProps.push(["Bookmarks", this.options.cloudMode || this.options.designerMode ? this.loc.FormViewer.Bookmarks : null,
            this.options.cloudMode || this.options.designerMode ? "Bookmarks20.png" : "Bookmarks.png", this.loc.HelpViewer.Bookmarks]);
        isFirst = false;
    }
    if (this.options.showParametersButton) {
        controlProps.push(["Parameters", this.options.cloudMode || this.options.designerMode ? this.loc.FormViewer.Parameters : null,
            this.options.cloudMode || this.options.designerMode ? "Parameters20.png" : "Parameters.png", this.loc.HelpViewer.Parameters]);
        isFirst = false;
    }
    if (this.options.showFindButton || this.options.showEditorButton) {
        if (!isFirst) controlProps.push(["Separator2_1"]);
        isFirst = false;
    }
    if (this.options.showFindButton) {
        controlProps.push(["Find", null, "Find.png", this.loc.HelpViewer.Find]);
        isFirst = false;
    }
    if (this.options.showEditorButton) {
        controlProps.push(["Editor", null, "Editor.png", this.loc.FormViewer.Editor]);
        isFirst = false;
    }
    if (!this.options.cloudMode && !this.options.designerMode) {
        if (this.options.showFirstButton || this.options.showPrevButton || this.options.showNextButton || this.options.showLastButton || this.options.showCurrentPage) {
            if (!isFirst) controlProps.push(["Separator3"]);
            isFirst = false;
        }
        if (this.options.showFirstButton) { controlProps.push(["FirstPage", null, this.options.rightToLeft ? "LastPage.png" : "FirstPage.png", this.loc.HelpViewer.PageFirst]); isFirst = false; }
        if (this.options.showPrevButton) { controlProps.push(["PrevPage", null, this.options.rightToLeft ? "NextPage.png" : "PrevPage.png", this.loc.HelpViewer.PagePrevious]); isFirst = false; }
        if (this.options.showCurrentPage) { controlProps.push(["PageControl"]); isFirst = false; }
        if (this.options.showNextButton) { controlProps.push(["NextPage", null, this.options.rightToLeft ? "PrevPage.png" : "NextPage.png", this.loc.HelpViewer.PageNext]); isFirst = false; }
        if (this.options.showLastButton) { controlProps.push(["LastPage", null, this.options.rightToLeft ? "FirstPage.png" : "LastPage.png", this.loc.HelpViewer.PageLast]); isFirst = false; }

        if (this.options.showViewMode || this.options.showZoom) {
            if (!isFirst) controlProps.push(["Separator4"]);
            isFirst = false;
        }
        if (this.options.showZoom) { controlProps.push(["Zoom", "100%", "Zoom.png", this.loc.FormViewer.Zoom]); isFirst = false; }
        if (this.options.showViewMode) { controlProps.push(["ViewMode", this.loc.A_WebViewer.OnePage, "OnePage.png", this.loc.FormViewer.ViewMode]); isFirst = false; }

        if (!this.options.rightToLeft && this.options.toolbarAlignment == "right" && (this.options.showAboutButton || this.options.showDesignButton)) { controlProps.push(["Separator5"]); }
    }

    if (this.options.designerMode) {
        controlProps.push(["Separator4"]);
        controlProps.push(["ViewMode", this.loc.A_WebViewer.OnePage, "ViewMode.png", this.loc.FormViewer.ViewMode]);
    }

    if (this.options.cloudMode) {
        controlProps.push(["Close", this.loc.FormViewer.Close, "RemoveItemButton.png", null]);
    }

    for (var i = 0; i < controlProps.length; i++) {
        var index = this.options.rightToLeft ? controlProps.length - 1 - i : i;
        var name = controlProps[index][0];
        var table = (name == "About" || name == "Design" || name == "Separator1" || name == "Close") ? dopTable : mainTable;

        if (name.indexOf("Separator") == 0) {
            table.addCell(this.ToolBarSeparator());
            continue;
        }

        var buttonArrow = (name == "Print" || name == "Save" || name == "Zoom" || name == "ViewMode") ? "Down" : null;
        var control = (name != "PageControl")
            ? this.ToolButton(name, null, controlProps[index][1], controlProps[index][2],
                 (controlProps[index][3] ? [controlProps[index][3], this.HelpLinks["Toolbar"]] : null), buttonArrow)
            : this.PageControl();

        if ((this.options.cloudMode || this.options.designerMode) && name != "PageControl") {
            control.style.height = "28px";
            if (name == "Find" || name == "Editor") {
                control.style.width = "28px";
                control.innerTable.style.width = "100%";
                control.imageCell.style.textAlign = "center";
            }
        }

        if (this.options.theme == "Purple" && control.arrow != null)
            control.arrow.src = this.options.images["ButtonArrowWhite.png"];

        control.style.margin = (name == "Design") ? "1px 8px 1px 8px" : "1px";
        if (name == "Editor") control.style.display = "none";
        toolbar.controls[name] = control;
        table.addCell(control);
    }

    if (this.options.demoMode && !this.options.isMaterial) {
        toolbar.style.borderTop = toolbar.style.borderBottom = "1px solid #c6c6c6";

        var newButton = this.ToolButton("newButton", null, this.loc.MainMenu.menuFileNew.replace("&", ""), "NewButtonWhite.png", null, null, this.GetStyles("BrightButton"));
        newButton.style.margin = "0 3px 0 3px";
        newButton.caption.style.padding = "1px 15px 0px 0px";
        newButton.imageCell.style.padding = "0px 4px 0px 15px";
        newButton.style.color = "#ffffff";
        dopTable.addCell(newButton);

        var editButton = this.ToolButton("editButton", null, this.loc.MainMenu.menuEditEdit, "Edit.png");
        editButton.style.margin = "0 3px 0 3px";
        editButton.caption.style.padding = "1px 15px 0px 0px";
        editButton.imageCell.style.padding = "0px 4px 0px 15px";
        dopTable.addCell(editButton);

        var designerHref = "https://designer.stimulsoft.com/";
        //var designerHref = "http://localhost:20455/";

        newButton.action = function () {
            //var win = window.open("Designer.aspx?themename=" + this.jsObject.options.theme + "&localization=" + this.jsObject.options.demolocalization);

            var url = designerHref + "?params=" + Base64.encode(
                "demomode;" +
                this.jsObject.options.demolocalization + ";" +
                this.jsObject.options.theme + ";"
            );

            window.open(url);
        }

        editButton.action = function () {
            //window.open("Designer.aspx?reportname=" + (this.jsObject.options.reportName || "Metrics") +
            //    "&themename=" + this.jsObject.options.theme + "&localization=" + this.jsObject.options.demolocalization);

            var url = designerHref + "?params=" + Base64.encode(
                "demomode;" +
                this.jsObject.options.demolocalization + ";" +
                this.jsObject.options.theme + ";" +
                (this.jsObject.options.reportName || "")
            );

            window.open(url);
        }
    }

    toolbar.haveScroll = function () {
        return (toolbar.scrollWidth > toolbar.offsetWidth)
    }

    toolbar.getMinWidth = function () {
        a = mainCell.offsetWidth;
        b = mainTable.offsetWidth
        c = toolbarTable.offsetWidth;

        return c - (a - b) + 50;
    }

    toolbar.minWidth = toolbar.getMinWidth();

    toolbar.changeToolBarState = function () {
        var options = toolbar.jsObject.options;
        var controls = toolbar.controls;

        if (controls["FirstPage"]) controls["FirstPage"].setEnabled(options.pageNumber > 0 && (options.menuViewMode == "OnePage" || options.cloudMode || options.designerMode));
        if (controls["PrevPage"]) controls["PrevPage"].setEnabled(options.pageNumber > 0 && (options.menuViewMode == "OnePage" || options.cloudMode || options.designerMode));
        if (controls["NextPage"]) controls["NextPage"].setEnabled(options.pageNumber < options.pagesCount - 1 && (options.menuViewMode == "OnePage" || options.cloudMode || options.designerMode));
        if (controls["LastPage"]) controls["LastPage"].setEnabled(options.pageNumber < options.pagesCount - 1 && (options.menuViewMode == "OnePage" || options.cloudMode || options.designerMode));
        if (controls["ViewMode"]) {
            controls["ViewMode"].caption.innerHTML = toolbar.jsObject.loc.A_WebViewer[options.menuViewMode];
            controls["ViewMode"].image.src = options.images[options.menuViewMode + ".png"];
        }
        if (controls["Zoom"]) controls["Zoom"].caption.innerHTML = options.zoom + "%";
        if (controls["PageControl"]) {
            controls["PageControl"].countLabel.innerHTML = options.pagesCount;
            controls["PageControl"].textBox.value = options.pageNumber + 1;
            controls["PageControl"].textBox.setEnabled(options.pagesCount > 1 && (options.menuViewMode == "OnePage" || options.cloudMode || options.designerMode));
        }

        if (options.menus["zoomMenu"]) {
            var zoomItems = options.menus["zoomMenu"].items;
            for (var i in zoomItems) {
                if (zoomItems[i]["image"] == null) continue;
                if (zoomItems[i].name != "ZoomOnePage" && zoomItems[i].name != "ZoomPageWidth") {
                    zoomItems[i].setSelected(zoomItems[i].name == "Zoom" + options.zoom);
                    zoomItems[i].image.style.visibility = "hidden";
                }
            }
        }

        if (options.menuViewMode == "WholeReport") {
            var page = options.reportPanel.pages[options.pageNumber];
            if (page && !page.isSelected) page.setSelected();
        }
    }

    toolbar.setEnabledMainButtons = function (state) {
        var controls = toolbar.controls;
        if (controls["Zoom"]) controls["Zoom"].setEnabled(state);
        if (controls["Save"]) controls["Save"].setEnabled(state);
        if (controls["Print"]) controls["Print"].setEnabled(state);
        if (controls["ZoomOnePage"]) controls["ZoomOnePage"].setEnabled(state);
        if (controls["ZoomPageWidth"]) controls["ZoomPageWidth"].setEnabled(state);
        if (this.jsObject.options.parametersPanel) this.jsObject.options.parametersPanel.mainButtons.submit.setEnabled(state);
    }

    toolbar.changeShortType = function () {
        if (toolbar.shortType && toolbar.jsObject.options.mobileViewer.offsetWidth < toolbar.minWidth) return;
        toolbar.shortType = toolbar.jsObject.options.mobileViewer.offsetWidth < toolbar.minWidth;
        shortButtons = ["Print", "Save", "Zoom", "ViewMode", "Design"];
        for (var index in shortButtons) {
            button = toolbar.controls[shortButtons[index]];
            if (button && button.caption) {
                button.caption.style.display = toolbar.shortType ? "none" : "";
            }
        }
    }

    if (!this.options.cloudMode && !this.options.designerMode) {
        window.onresize = function () {
            toolbar.changeShortType();
        }

        toolbar.changeShortType();
    }

    if (this.options.cloudMode && toolbar.controls.Close) {
        toolbar.controls.Close.style.display = this.isInFullScreen() ? "" : "none";

        this.addEvent(window, 'keyup', function (e) {
            if (e && e.keyCode == 122) {
                toolbar.controls.Close.style.display = toolbar.jsObject.isInFullScreen() ? "" : "none";
            }
        });
    }
}

//ToolButton
StiMobileViewer.prototype.ToolButton = function (name, groupName, caption, imageName, toolTip, arrowType, styles) {
    var toolButton = this.SmallButton(name, groupName, caption, imageName, toolTip, arrowType, styles || this.GetStyles("StandartSmallButton"));

    if (this.options.demoMode) {
        toolButton.style.height = "28px";
        toolButton.innerTable.style.width = "100%";
        if (!caption) toolButton.style.minWidth = "28px";
        if (toolButton.imageCell) {
            toolButton.imageCell.style.padding = "0 4px 0 4px";
            toolButton.imageCell.style.textAlign = "center";
        }
    }

    return toolButton;
}

//Separator
StiMobileViewer.prototype.ToolBarSeparator = function () {
    var separator = document.createElement("div");
    separator.style.width = "1px";
    separator.style.height = this.options.isTouchDevice ? "26px" : "21px";
    separator.className = "stiMobileViewerToolBarSeparator";

    return separator;
}

//PageControl
StiMobileViewer.prototype.PageControl = function () {
    var pageControl = this.CreateHTMLTable();
    var text1 = pageControl.addCell();
    text1.style.padding = "0 4px 0 4px";
    text1.innerHTML = this.loc.A_WebViewer.Page;
    text1.style.fontFamily = this.options.toolbarFontFamily;
    if (!this.options.cloudMode && !this.options.designerMode) {
        text1.style.color = (this.options.theme == "Purple" && this.options.toolbarFontColor == "#444444") ? "#ffffff" : this.options.toolbarFontColor;
    }

    var textBox = this.TextBox("PageControl", 45);
    pageControl.addCell(textBox);
    pageControl.textBox = textBox;
    textBox.action = function () {
        if (textBox.jsObject.options.pageNumber != textBox.getCorrectValue() - 1)
            textBox.jsObject.actionEvent("GoToPage"); 
    }

    textBox.getCorrectValue = function () {
        value = parseInt(this.value);
        if (value < 1 || !value) value = 1;
        if (value > textBox.jsObject.options.pagesCount) value = textBox.jsObject.options.pagesCount;
        return value;
    }

    var text2 = pageControl.addCell();
    text2.style.padding = "0 4px 0 4px";
    text2.innerHTML = this.loc.A_WebViewer.PageOf;
    text2.style.fontFamily = this.options.toolbarFontFamily;
    if (!this.options.cloudMode && !this.options.designerMode) {
        text2.style.color = (this.options.theme == "Purple" && this.options.toolbarFontColor == "#444444") ? "#ffffff" : this.options.toolbarFontColor;
    }

    var countLabel = pageControl.addCell();
    pageControl.countLabel = countLabel;
    countLabel.style.padding = "0 4px 0 0";
    countLabel.innerHTML = "?";

    return pageControl;
}

StiMobileViewer.prototype.isInFullScreen = function () {

    if(window['fullScreen'] !== undefined) {
        return !!window.fullScreen;
    }

    return screen.width == window.innerWidth &&
        Math.abs(screen.height - window.innerHeight) < 5;
}