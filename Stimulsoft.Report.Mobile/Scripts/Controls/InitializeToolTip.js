﻿
StiMobileViewer.prototype.InitializeToolTip = function () {
    var toolTip = document.createElement("div");
    toolTip.id = this.options.mobileViewer.id + "ToolTip";
    toolTip.jsObject = this;
    this.options.toolTip = toolTip;
    this.options.mainPanel.appendChild(toolTip);
    toolTip.className = "stiMobileViewerToolTip";
    toolTip.style.display = "none";
    toolTip.showTimer = null;
    toolTip.hideTimer = null;
    toolTip.visible = false;

    toolTip.innerTable = this.CreateHTMLTable();
    toolTip.appendChild(toolTip.innerTable);

    toolTip.textCell = toolTip.innerTable.addCell();
    toolTip.textCell.className = "stiMobileViewerToolTipTextCell";

    toolTip.helpButton = this.SmallButton(null, null, this.loc.HelpDesigner.TellMeMore, "HelpIcon.png", null, null, this.GetStyles("HyperlinkButton"), true);
    toolTip.innerTable.addCellInNextRow(toolTip.helpButton);
    toolTip.helpButton.style.margin = "4px 8px 4px 8px";
    toolTip.helpButton.style.display = this.options.showTooltipsHelp ? "" : "none";
    if (!this.options.showTooltipsHelp) toolTip.textCell.style.borderBottom = "0px";

    toolTip.show = function (text, helpUrl, leftPos, topPos, controlWidthForRightToLeft) {
        if (this.visible && text == this.textCell.innerHTML) return;
        this.hide();
        this.textCell.innerHTML = text;
        this.helpButton.helpUrl = helpUrl;
        this.helpButton.action = function () { this.jsObject.ShowHelpWindow(this.helpUrl); }        
        var d = new Date();
        var endTime = d.getTime() + 300;
        this.style.opacity = 1 / 100;
        this.style.display = "";
        this.style.left = (controlWidthForRightToLeft != null ?
            leftPos - this.offsetWidth + controlWidthForRightToLeft : leftPos) + "px";
        this.style.top = (topPos == "isNavigatePanelTooltip" ?
            this.jsObject.options.toolbar.offsetHeight + this.jsObject.options.reportPanel.offsetHeight +
            (this.jsObject.options.findPanel ? this.jsObject.options.findPanel.offsetHeight : 0) - this.offsetHeight - 2 : topPos) + "px";
        this.visible = true;
        this.jsObject.ShowAnimationForm(this, endTime);
    }

    toolTip.showWithDelay = function (text, helpUrl, leftPos, topPos, controlWidthForRightToLeft) {
        clearTimeout(this.showTimer);
        clearTimeout(this.hideTimer);
        this.showTimer = setTimeout(function () { toolTip.show(text, helpUrl, leftPos, topPos, controlWidthForRightToLeft) }, 300);
    }

    toolTip.hide = function () {
        this.visible = false;
        clearTimeout(this.showTimer);
        this.style.display = "none";
    }

    toolTip.hideWithDelay = function () {
        clearTimeout(this.showTimer);
        clearTimeout(this.hideTimer);
        this.hideTimer = setTimeout(function () { toolTip.hide() }, 500);
    }

    toolTip.onmouseover = function () {
        clearTimeout(this.showTimer);
        clearTimeout(this.hideTimer);
    }

    toolTip.onmouseout = function () {
        this.hideWithDelay();
    }
}