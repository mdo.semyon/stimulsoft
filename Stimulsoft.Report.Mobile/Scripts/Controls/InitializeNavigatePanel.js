﻿
StiMobileViewer.prototype.InitializeNavigatePanel = function () {
    var navigatePanel = document.createElement("div");
    navigatePanel.id = this.options.mobileViewer.id + "NavigatePanel";
    navigatePanel.jsObject = this;
    this.options.navigatePanel = navigatePanel;
    this.options.mainPanel.appendChild(navigatePanel);
    navigatePanel.className = "stiMobileViewerNavigatePanel";

    var controlsTable = this.CreateHTMLTable();
    navigatePanel.appendChild(controlsTable);
    
    var controlProps = [
        ["FirstPage", null, "PageFirst20.png", this.loc.HelpViewer.PageFirst, null],
        ["PrevPage", null, "PagePrevious20.png", this.loc.HelpViewer.PagePrevious, null],
        ["Separator"],
        ["PageControl"],
        ["Separator"],
        ["NextPage", null, "PageNext20.png", this.loc.HelpViewer.PageNext, null],
        ["LastPage", null, "PageLast20.png", this.loc.HelpViewer.PageLast, null],
        ["Space"],
        ["ZoomPageWidth", null, "ZoomPageWidth20.png", this.loc.HelpViewer.ZoomPageWidth, null],
        ["ZoomOnePage", null, "ZoomOnePage20.png", this.loc.HelpViewer.ZoomOnePage, null],
        ["Separator"],
        ["Zoom", "100%", null, this.loc.FormViewer.Zoom, "Up"]
    ]

    for (var index = 0; index < controlProps.length; index++) {
        var name = controlProps[index][0];
        
        if (name.indexOf("Space") == 0) {
            controlsTable.addCell().style.width = "100%";
            continue;
        }

        if (name.indexOf("Separator") == 0) {
            controlsTable.addCell(this.NavigatePanelSeparator());
            continue;
        }

        var control = (name != "PageControl")
            ? this.NavigateButton(name, null, controlProps[index][1], controlProps[index][2],
                (controlProps[index][3] ? [controlProps[index][3], this.HelpLinks["Toolbar"]] : null), controlProps[index][4])
            : this.PageControl();

        if (name != "PageControl") {            
            if (control.caption == null) {
                control.imageCell.style.textAlign = "center";
                control.innerTable.style.width = "100%";
                control.style.width = "35px";
            }
            if (control.toolTip) {
                var positions = { top: "isNavigatePanelTooltip" }
                if (name == "Zoom" || name == "ZoomPageWidth" || name == "ZoomOnePage") {
                    positions.rightToLeft = true;
                }
                control.toolTip.push(positions);
            }
        }
        else {
            control.textBox.style.border = "0px";
        }
        if (control.arrow) control.arrow.src = this.options.images["ButtonArrowUpWhite.png"];
        if (name == "FirstPage") control.style.margin = "0 1px 0 3px";
        else if (name == "Zoom") control.style.margin = "0 3px 0 1px";
        else control.style.margin = "0px 1px 0 1px";

        this.options.toolbar.controls[name] = control;
        controlsTable.addCell(control);
    }

    var disabledPanel = document.createElement("div");    
    navigatePanel.disabledPanel = disabledPanel;
    disabledPanel.className = "stiMobileViewerNavigatePanelDisabledPanel";
    navigatePanel.appendChild(disabledPanel);

    navigatePanel.setEnabled = function (state) {
        disabledPanel.style.display = state ? "none" : "";
    }

    navigatePanel.setEnabled(true);
}

//Separator
StiMobileViewer.prototype.NavigatePanelSeparator = function () {
    var separator = document.createElement("div");
    separator.style.height = "35px";
    separator.className = "stiMobileViewerNavigatePanelSeparator";

    return separator;
}

//Navigate Button
StiMobileViewer.prototype.NavigateButton = function (name, groupName, caption, imageName, toolTip, arrowType) {
    var button = this.SmallButton(name, groupName, caption, imageName, toolTip, arrowType, this.GetStyles("NavigateButton"));
    button.style.height = "35px";

    return button;
}