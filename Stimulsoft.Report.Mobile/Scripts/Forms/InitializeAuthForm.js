﻿
StiMobileViewer.prototype.InitializeAuthForm = function () {

    var authForm = this.BaseForm("authForm", this.loc.Cloud.WindowTitleLogin, 2);
    authForm.controls = {};
    authForm.style.width = "1000px";
    authForm.style.height = "700px";
    authForm.style.backgroundColor = "white";
    authForm.buttonsPanel.style.display = "none";
    authForm.buttonsSeparator.style.display = "none";
    authForm.showCaptions = this.GetNavigatorName() == "MSIE";
    authForm.caption.className = "stiLoginFormCaption";
    authForm.buttonClose.parentElement.style.display = "none";

    var loginImg = document.createElement("img");
    loginImg.src = this.options.images["LogIn.CloudBigIcon.png"];
    loginImg.style.marginBottom = "-100px";
    loginImg.style.float = "right";
    loginImg.style.paddingRight = "20px";
    authForm.imgLogin.appendChild(loginImg);
    authForm.imgLogin.style.width = "320px";
    authForm.imgLogin.className = "stiLoginFormCaption";
    authForm.container.style.paddingTop = "80px";

    var loginPanel = document.createElement("div");
    authForm.container.appendChild(loginPanel);
    var mTable = this.CreateHTMLTable();
    var controlsTable = this.CreateHTMLTable();
    controlsTable.style.margin = "0 auto 0 auto";
    loginPanel.appendChild(mTable);
    mTable.style.width = "100%";
    var ct = mTable.addCell(controlsTable);
    ct.style.paddingRight = "35px"
    ct.style.width = "50%";

    controlsTable.style.marginTop = "16px";
    var rDiv = document.createElement("div");
    rDiv.style.width = "710px";
    rDiv.style.margin = "0 auto";
    rDiv.style.borderTop = "1px solid #f2f1f1";
    rDiv.style.marginTop = "30px";

    var ds = mTable.addCellInNextRow(rDiv);
    ds.style.textAlign = "center";
    ds.colSpan = 2;
    ds.style.paddingLeft = "35px";

    var loginText = this.loc.Cloud.labelUserName ? this.loc.Cloud.labelUserName.replace(":", "") : "User Name";
    var passwordText = this.loc.Cloud.labelPassword ? this.loc.Cloud.labelPassword.replace(":", "") : "Password";

    //UserName
    if (authForm.showCaptions) {
        authForm.loginCaption = controlsTable.addCell();
        authForm.loginCaption.className = "stiAuthFormTextCell";
        authForm.loginCaption.innerHTML = loginText;
    }

    var loginControl = this.TextBoxWithHintText(null, 230, authForm.showCaptions ? "" : loginText, loginText, "LogIn." + this.GetThemeColor() + ".login.png");
    authForm.controls.loginUserName = loginControl;
    controlsTable.addCellInNextRow(loginControl.table);
    loginControl.action = function () { authForm.action(); };
    loginControl.value = this.GetCookie("loginLogin") || "";

    //Password
    if (authForm.showCaptions) {
        authForm.passwordCaption = controlsTable.addCellInNextRow();
        authForm.passwordCaption.style.paddingTop = "17px";
        authForm.passwordCaption.className = "stiAuthFormTextCell";
        authForm.passwordCaption.innerHTML = passwordText;
    }

    var passwordControl = this.TextBoxWithHintText(null, 230, authForm.showCaptions ? "" : passwordText, passwordText, "LogIn." + this.GetThemeColor() + ".password.png");
    passwordControl.setAttribute("type", "password");
    authForm.controls.loginPassword = passwordControl;
    var passwControlCell = controlsTable.addCellInNextRow(passwordControl.table);
    if (!authForm.showCaptions) passwControlCell.style.paddingTop = "17px";
    passwordControl.action = function () { authForm.action(); };
    passwordControl.value = this.GetCookie("loginPassword") || "";

    //Remember Me
    var loginRememberMe = this.GetCookie("loginRememberMe") === 'true';
    var rememberMeLabel = document.createElement("label");
    var rememberMeControl = document.createElement("input");
    rememberMeControl.style.marginTop = "15px";
    authForm.controls.rememberMe = rememberMeControl;
    rememberMeControl.setAttribute("type", "checkbox");
    controlsTable.addCellInNextRow(rememberMeLabel);
    rememberMeLabel.appendChild(rememberMeControl);
    var rememberMeText = document.createElement("span");
    rememberMeText.style.margin = "0 0 3px 4px";
    rememberMeLabel.appendChild(rememberMeText);
    rememberMeText.className = "stiAuthFormTextCell";
    rememberMeText.innerHTML = "Remember Me"; //this.loc.Authorization.CheckBoxRememberMe;
    rememberMeControl.checked = loginRememberMe;

    //Button Login
    var buttonLogin = this.LoginButton(null, this.loc.Cloud.Login, null);
    authForm.controls.buttonLogin = buttonLogin;
    buttonLogin.style.margin = "8px 0 8px 0";
    buttonLogin.style.display = "inline-block";
    controlsTable.addCellInNextRow(buttonLogin).style.paddingTop = "10px";
    buttonLogin.action = function () { authForm.action(); }

    authForm.onshow = function () {
        loginControl.focus();
    }

    authForm.getShareInfo = function () {
        var params = { ItemKey: authForm.jsObject.options.cloudParameters.shareKey };        
        authForm.jsObject.SendCommand("ItemGetShareInfo", params, authForm.getShareInfoComplete, authForm.getShareInfoError);
    }

    authForm.getShareInfoComplete = function (data) {
        if (data.ResultShareLevel == "Public" || (data.ResultShareLevel == "Registered" && authForm.jsObject.options.cloudParameters.sessionKey)) {
            var params = {
                ItemKey: authForm.jsObject.options.cloudParameters.shareKey
            };
            if (authForm.jsObject.options.cloudParameters.sessionKey) params.SessionKey = authForm.jsObject.options.cloudParameters.sessionKey;
            authForm.jsObject.SendCommand("ItemGet", params, authForm.getShareItemComplete, authForm.getShareItemError);
        }
        else if (data.ResultShareLevel == "Registered") {
            authForm.changeVisibleState(true);
        }
    }

    authForm.getShareInfoError = function (data) {
        // TODO: error message for get share info
    }

    authForm.getShareItemComplete = function (data) {
        if (data.ResultItem) {
            if (data.ResultItem.Ident == "ReportTemplateItem" || data.ResultItem.Ident == "ReportSnapshotItem") {
                document.title = data.ResultItem.Name + " - " + authForm.jsObject.loc.FormViewer.title;
                if (authForm.jsObject.options.cloudParameters) {
                    authForm.jsObject.options.cloudParameters.reportName = Base64.encode(data.ResultItem.Name);
                    if (authForm.jsObject.options.cloudParameters.dVers === true || authForm.jsObject.options.cloudParameters.dVers == "true") {
                        document.title += " " + authForm.jsObject.getT();
                    }
                }
            }

            if (data.ResultItem.Ident == "ReportTemplateItem")
                authForm.jsObject.options.cloudParameters.sessionKey
                    ? authForm.jsObject.LoadReportTemplateItemToViewer(data.ResultItem.Key, data.ResultLastVersionKey)
                    : authForm.startAnonymousLogin(data, function () {
                        authForm.jsObject.LoadReportTemplateItemToViewer(data.ResultItem.Key, data.ResultLastVersionKey)
                    });
            else if (data.ResultItem.Ident == "ReportSnapshotItem") {
                authForm.jsObject.options.reportRenderComplete = true;
                authForm.jsObject.options.cloudParameters.sessionKey
                    ? authForm.jsObject.LoadReportSnapshotItemToViewer(data.ResultItem.Key, data.ResultLastVersionKey)
                    : authForm.startAnonymousLogin(data, function () {
                        authForm.jsObject.LoadReportSnapshotItemToViewer(data.ResultItem.Key, data.ResultLastVersionKey)
                    });
            }
            else authForm.jsObject.postForm({ SessionKey: authForm.jsObject.options.cloudParameters.sessionKey }, document, window.location.href);
        }
    }

    authForm.getShareItemError = function (data) {
        // TODO: error message for get item
    }

    authForm.startAnonymousLogin = function (shareData, completeFuntion) {
        var this_ = this;
        var result = {
            Ident: "UserLogin",
            AuthType: "Anonymous",
            Device: { Ident: "WebDevice" },
            Localization: this_.jsObject.options.defaultLocalization,
            Module: "Reports"
        };
        this_.jsObject.options.controller.exec("service/login", result,
            function (data) {
                document.title = shareData.ResultItem.Name + " - " + authForm.jsObject.loc.FormViewer.title;

                if (authForm.jsObject.options.cloudParameters.dVers === true || authForm.jsObject.options.cloudParameters.dVers == "true") {
                    document.title += " " + authForm.jsObject.getT();
                }

                authForm.jsObject.options.cloudParameters.sessionKey = data.ResultSessionKey;
                authForm.jsObject.options.cloudParameters.userKey = data.ResultUserKey;

                //Initialize controller with new sessionKey
                authForm.jsObject.options.controller.updateSignals();
                if (completeFuntion) completeFuntion();
            },
            function (data, msg) {
                authForm.jsObject.options.forms.errorMessageForm.show(msg);
            }
        );
    }

    authForm.action = function () {
        var result = {};
        result.Ident = "UserLogin";
        result.UserName = authForm.controls.loginUserName.value;
        result.Password = authForm.controls.loginPassword.value;
        result.Device = { Ident: "WebDevice" };
        this.jsObject.options.controller.exec("service/login", result,
            function (data) {
                authForm.jsObject.options.cloudParameters.sessionKey = data.ResultSessionKey;
                authForm.jsObject.options.cloudParameters.userKey = data.ResultUserKey;
                authForm.changeVisibleState(false);                

                //Initialize controller with new sessionKey
                authForm.jsObject.options.controller.updateSignals();
                authForm.getShareInfo();
            },
            function (data, msg) {
                authForm.jsObject.options.forms.errorMessageForm.show(msg);
            });
    }

    return authForm;
}

StiMobileViewer.prototype.LoginButton = function (name, caption, imageName, minWidth, tooltip) {
    var button = this.SmallButton(name, null, caption || "", imageName, tooltip, null, this.GetStyles("LoginButton"));
    button.style.height = "40px";
    button.innerTable.style.width = "100%";
    button.style.minWidth = (minWidth || 80) + "px";
    button.caption.style.textAlign = "center";
    button.style.cursor = "pointer";

    return button;
}