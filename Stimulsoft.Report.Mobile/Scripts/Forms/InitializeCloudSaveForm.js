﻿
StiMobileViewer.prototype.InitializeCloudSaveForm = function (sendEmail) {

    var cloudSaveForm = this.BaseForm("cloudSaveForm", sendEmail ? this.loc.A_WebViewer.ButtonSend : this.loc.A_WebViewer.SaveReport, 1);
    cloudSaveForm.header.className = "stiMobileViewerCloudFormHeader";
    cloudSaveForm.caption.style.padding = "0 0 0 15px"
    cloudSaveForm.container.style.padding = "0px";
    cloudSaveForm.container.style.background = "#ffffff";
    cloudSaveForm.style.background = "#ffffff";
    cloudSaveForm.container.style.overflow = "auto";
    cloudSaveForm.header.appendChild(this.FormSeparator());
    cloudSaveForm.buttonOk.caption.innerHTML = sendEmail ? this.loc.A_WebViewer.ButtonSend : this.loc.A_WebViewer.SaveReport;

    var viewerExportForm = this.options.forms.exportForm || this.InitializeExportForm("exportForm");
    viewerExportForm.style.background = "#ffffff";
    var emailControl;
    var messageControl;
        
    if (sendEmail) {
        var emailBlock = document.createElement("div");
        cloudSaveForm.container.appendChild(emailBlock);

        var emailTable = this.CreateHTMLTable();
        emailBlock.appendChild(emailTable);
        emailTable.addTextCell(this.loc.A_WebViewer.LabelTo).className = "stiMobileViewerTextBeforeControl2";
        emailControl = this.TextBox(null, 350);
        emailControl.setAttribute("placeholder", this.loc.A_WebViewer.Email.replace(":", ""));
        emailControl.style.border = "0px";
        emailControl.style.height = "35px";
        emailControl.style.marginLeft = "4px";
        emailTable.addCell(emailControl);

        emailBlock.appendChild(this.FormSeparator());

        messageControl = this.TextArea(null, 450, 100, this.loc.A_WebViewer.Message.replace(":", ""));
        messageControl.style.margin = "8px";
        messageControl.style.padding = "0px";
        messageControl.style.border = "0px";
        emailBlock.appendChild(messageControl);
    }
    else {    
        var textCell = document.createElement("div");
        textCell.innerHTML = this.loc.A_WebViewer.LabelSelectExportFormat;
        textCell.className = "stiMobileViewerTextBeforeControl";
        textCell.style.padding = "15px 0 15px 8px";
        cloudSaveForm.container.appendChild(textCell);
    }
        
    var exportFormatButton = this.SmallButton(null, null, this.GetFullFileNameByExportFormat(this.GetCurrentReportName(), "StiPdfExportSettings"),
        "CloudIcons.FileTypes.BigExportPdf.png", null, null, this.GetStyles("FormButton"));
    exportFormatButton.style.height = "40px";
    exportFormatButton.style.width = "450px";
    exportFormatButton.innerTable.style.width = "100%";
    exportFormatButton.style.margin = "0 8px 8px 8px";
    cloudSaveForm.container.appendChild(exportFormatButton);

    var settingsButton = this.StandartSmallButton(null, null, null, "CloudIcons.Settings.png");
    settingsButton.style.width = "24px";
    settingsButton.style.height = "100%";
    settingsButton.innerTable.style.width = "100%";
    settingsButton.imageCell.style.padding = "0px";
    settingsButton.imageCell.style.textAlign = "center";
    exportFormatButton.innerTable.addCell().style.width = "100%";
    exportFormatButton.innerTable.addCell(settingsButton);

    //DropDown Button
    var dropDownButton = this.StandartSmallButton(null, null, null, "CloudIcons.BigDropDownButton.png");
    dropDownButton.style.height = "100%";    
    dropDownButton.style.width = "24px";
    dropDownButton.innerTable.style.width = "100%";
    dropDownButton.imageCell.style.padding = "0px";
    dropDownButton.imageCell.style.textAlign = "center";
    exportFormatButton.innerTable.addCell(dropDownButton);

    //Override events
    exportFormatButton.onmouseenter = function () {
        if (!this.isEnabled || ((this["haveMenu"] || this.jsObject.options.cloudMode || this.jsObject.options.designerMode) && this.isSelected) || this.jsObject.options.isTouchClick) return;
        this.className = this.styles["default"] + " " + this.styles["over"];
        this.isOver = true;
        this.style.background = "transparent";
    }

    exportFormatButton.onmouseleave = function () {
        this.isOver = false;
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.className = this.styles["default"] + " " + (this.isSelected ? this.styles["selected"] : "");
        this.style.background = "";
    }

    exportFormatButton.setSelected = function (state) {
        this.isSelected = state;
        this.className = this.styles["default"] + " " +
            (state ? this.styles["selected"] : (this.isEnabled ? (this.isOver ? this.styles["over"] : "") : this.styles["disabled"]));
        dropDownButton.setSelected(state);
        this.style.background = "";
    }
        
    var items = [];
    items.push(this.Item("StiPdfExportSettings", this.loc.Export.ExportTypePdfFile.replace("...", ""), "CloudIcons.FileTypes.BigExportPdf.png", "StiPdfExportSettings"));
    items.push(this.Item("StiXpsExportSettings", this.loc.Export.ExportTypeXpsFile.replace("...", ""), "CloudIcons.FileTypes.BigExportXps.png", "StiXpsExportSettings"));
    items.push(this.Item("StiPpt2007ExportSettings", this.loc.Export.ExportTypePpt2007File.replace("...", ""), "CloudIcons.FileTypes.BigExportPpt2007.png", "StiPpt2007ExportSettings"));
    items.push(this.Item("StiHtmlExportSettings", this.loc.Export.ExportTypeHtmlFile.replace("...", ""), "CloudIcons.FileTypes.BigExportHtml.png", "StiHtmlExportSettings"));
    items.push(this.Item("StiTxtExportSettings", this.loc.Export.ExportTypeTxtFile.replace("...", ""), "CloudIcons.FileTypes.BigExportTxt.png", "StiTxtExportSettings"));
    items.push(this.Item("StiRtfExportSettings", this.loc.Export.ExportTypeRtfFile.replace("...", ""), "CloudIcons.FileTypes.BigExportRtf.png", "StiRtfExportSettings"));
    items.push(this.Item("StiWord2007ExportSettings", this.loc.Export.ExportTypeWord2007File.replace("...", ""), "CloudIcons.FileTypes.BigExportWord2007.png", "StiWord2007ExportSettings"));
    items.push(this.Item("StiOdtExportSettings", this.loc.Export.ExportTypeWriterFile.replace("...", ""), "CloudIcons.FileTypes.BigExportOdt.png", "StiOdtExportSettings"));
    items.push(this.Item("StiExcel2007ExportSettings", this.loc.Export.ExportTypeExcelFile.replace("...", ""), "CloudIcons.FileTypes.BigExportExcel.png", "StiExcel2007ExportSettings"));
    items.push(this.Item("StiOdsExportSettings", this.loc.Export.ExportTypeCalcFile.replace("...", ""), "CloudIcons.FileTypes.BigExportOds.png", "StiOdsExportSettings"));
    items.push(this.Item("StiCsvExportSettings", this.loc.Export.ExportTypeDataFile.replace("...", ""), "CloudIcons.FileTypes.BigExportData.png", "StiCsvExportSettings"));
    items.push(this.Item("StiBmpExportSettings", this.loc.Export.ExportTypeImageFile.replace("...", ""), "CloudIcons.FileTypes.BigExportImage.png", "StiBmpExportSettings"));

    var exportTypesMenu = this.VerticalMenu("exportTypesMenu", exportFormatButton, "Down", items, this.GetStyles("MenuStandartItem"))
    exportTypesMenu.innerContent.style.width = "450px";
    exportTypesMenu.innerContent.style.maxHeight = "400px";
    for (var name in exportTypesMenu.items) {
        exportTypesMenu.items[name].style.height = "40px";
    }

    var settingsButtonWasClicked = false;
    settingsButton.action = function () {
        exportTypesMenu.changeVisibleState(false);
        settingsButtonWasClicked = true;
        exportTypesMenu.changeVisibleState(false);
        viewerExportForm.controls.SavingReportGroup.changeOpeningState(true);
        viewerExportForm.controls.PageRangeGroup.changeOpeningState(true);
        viewerExportForm.controls.SettingsGroup.changeOpeningState(false);
        viewerExportForm.changeVisibleState(true);
        viewerExportForm.controls.OpenAfterExport.style.display = "none";
    }

    exportFormatButton.action = function () {
        if (settingsButtonWasClicked) {
            settingsButtonWasClicked = false;
            return;
        };
        exportTypesMenu.changeVisibleState(!exportTypesMenu.visible);        
    }

    exportTypesMenu.action = function (menuItem) {
        cloudSaveForm.fillExportSettingsForm(menuItem.key);
        settingsButton.style.display = menuItem.key != "SaveDocument" ? "" : "none";
        this.changeVisibleState(false);
    }
    
    cloudSaveForm.fillExportSettingsForm = function (exportFormat) {
        viewerExportForm.showControlsByExportFormat(exportFormat);
        var exportSettingsStr = this.jsObject.GetCookie(exportFormat);
        var incomingExportSettings = (exportSettingsStr) ? JSON.parse(exportSettingsStr) : null;
        if (incomingExportSettings) viewerExportForm.setControlsValue(incomingExportSettings, true);
        exportFormatButton.caption.innerHTML = this.jsObject.GetFullFileNameByExportFormat(this.jsObject.GetCurrentReportName(), exportFormat);
        exportFormatButton.image.src = this.jsObject.options.images["CloudIcons.FileTypes." + this.jsObject.GetExportTypeImage(exportFormat)];
    }

    cloudSaveForm.show = function () {
        var exportFormat = cloudSaveForm.jsObject.GetCookie("StimulsoftLastViewerExportType") || "StiPdfExportSettings";
        cloudSaveForm.fillExportSettingsForm(exportFormat);
        settingsButton.style.display = exportFormat != "SaveDocument" ? "" : "none";
        cloudSaveForm.changeVisibleState(true);
        cloudSaveForm.style.top = "20%";
        if (sendEmail) {
            emailControl.focus();
            var lastSendEmail = this.jsObject.GetCookie("StimulsoftLastSendEmail");
            if (lastSendEmail) emailControl.value = lastSendEmail;
        }
    }

    cloudSaveForm.action = function () {
        var emailParameters = null;
        this.jsObject.SetCookie("StimulsoftLastViewerExportType", viewerExportForm.exportFormat);
        
        if (sendEmail) {
            this.jsObject.SetCookie("StimulsoftLastSendEmail", emailControl.value);
            var emails = emailControl.value.trim().split(",");
            for (var i = 0; i < emails.length; i++) {
                emails[i] = emails[i].trim();
            }

            emailParameters = {
                AttachedItemKeys: [],
                To: emails,
                Subject: this.jsObject.GetCurrentReportName(),
                Message: messageControl.value,
                SessionKey: cloudSaveForm.jsObject.options.cloudParameters.sessionKey
            }
        }

        cloudSaveForm.jsObject.exportReport(viewerExportForm.exportFormat, viewerExportForm.getExportSettingsObject(), emailParameters);
        cloudSaveForm.changeVisibleState(false);
    }

    viewerExportForm.action = function () {
        exportForm.changeVisibleState(false);
        this.jsObject.SetCookie(this.exportFormat, JSON.stringify(this.getExportSettingsObject()));
        exportFormatButton.caption.innerHTML = this.jsObject.GetFullFileNameByExportFormat(this.jsObject.GetCurrentReportName(), this.exportFormat);
        exportFormatButton.image.src = this.jsObject.options.images["CloudIcons.FileTypes." + this.jsObject.GetExportTypeImage(this.exportFormat)];
    }

    return cloudSaveForm;
}

StiMobileViewer.prototype.GetExportTypeImage = function (exportFormat) {
    switch (exportFormat) {
        case "StiHtmlExportSettings":
        case "StiHtml5ExportSettings":
        case "StiMhtExportSettings": return "BigExportHtml.png";
        case "StiExcelExportSettings":
        case "StiExcelXmlExportSettings":
        case "StiExcel2007ExportSettings": return "BigExportExcel.png";
        case "StiCsvExportSettings":
        case "StiDbfExportSettings":
        case "StiSylkExportSettings":
        case "StiXmlExportSettings":
        case "StiDifExportSettings": return "BigExportData.png";
        case "StiBmpExportSettings":
        case "StiGifExportSettings":
        case "StiJpegExportSettings":
        case "StiPcxExportSettings":
        case "StiPngExportSettings":
        case "StiTiffExportSettings":
        case "StiEmfExportSettings":
        case "StiSvgExportSettings":
        case "StiSvgzExportSettings": return "BigExportImage.png";
        case "StiOdsExportSettings": return "BigExportOds.png";
        case "StiOdtExportSettings": return "BigExportOdt.png";
        case "StiPdfExportSettings": return "BigExportPdf.png";
        case "StiPpt2007ExportSettings": return "BigExportPpt2007.png";
        case "StiRtfExportSettings": return "BigExportRtf.png";
        case "StiTxtExportSettings": return "BigExportTxt.png";
        case "StiWord2007ExportSettings": return "BigExportWord2007.png";
        case "StiXpsExportSettings": return "BigExportXps.png";
        default: return "BigExportData.png";
    }
}