﻿
StiMobileViewer.prototype.InitializeErrorMessageForm = function () {
    var form = this.BaseForm("errorMessageForm", this.loc.Errors.Error, 3);
    form.buttonCancel.style.display = "none";
    form.header.style.fontSize = "14px";
    form.header.style.fontFamily = "Arial";
    form.caption.style.textAlign = "center";
    form.style.background = "#ffffff";

    var table = this.CreateHTMLTable();
    form.container.appendChild(table);

    form.image = document.createElement("img");
    form.image.style.padding = "15px";
    form.image.src = this.options.images["MsgFormError.png"];
    table.addCellInLastRow(form.image);

    form.description = table.addCellInLastRow();
    form.description.className = "stiMobileViewerMessagesFormDescription";
    form.description.style.maxWidth = "600px";

    form.show = function (messageText) {
        this.changeVisibleState(true);
        this.description.innerHTML = messageText;
    }

    form.action = function () {
        this.changeVisibleState(false);
    }
}