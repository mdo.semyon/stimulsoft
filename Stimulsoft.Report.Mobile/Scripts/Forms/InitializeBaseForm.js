﻿
StiMobileViewer.prototype.BaseForm = function (name, caption, level) {
    var form = document.createElement("div");
    form.name = name != null ? name : this.generateKey();
    form.id = form.name;
    if (this.options.forms[name] != null) {
        this.options.mainPanel.removeChild(this.options.forms[name]);
    }    
    if (name != null) this.options.forms[name] = form;
    this.options.mainPanel.appendChild(form);
    form.className = "stiMobileViewerForm";
    form.jsObject = this;
    form.name = name != null ? name : this.generateKey();
    form.level = level;
    form.caption = null;
    form.visible = false;
    form.style.display = "none";
    if (level == null) level = 1;
    form.style.zIndex = (level * 10) + 1;

    //Header
    form.header = document.createElement("div");
    form.header.thisForm = form;
    form.appendChild(form.header);
    form.header.className = "stiMobileViewerFormHeader";
    var headerTable = this.CreateHTMLTable();
    headerTable.style.width = "100%";
    form.header.appendChild(headerTable);

    form.imgLogin = headerTable.addCell();
    form.caption = headerTable.addCell();
    if (caption != null || typeof (caption) == "undefined") {
        if (caption) form.caption.innerHTML = caption;
        form.caption.style.textAlign = "left";
        form.caption.style.padding = "10px 10px 3px 15px";
    }

    form.buttonClose = this.StandartSmallButton(null, null, null, "CloseForm.png", null, null);
    form.buttonClose.style.display = "inline-block";
    form.buttonClose.form = form;
    form.buttonClose.action = function () { this.form.changeVisibleState(false); };
    var closeButtonCell = headerTable.addCell(form.buttonClose);
    closeButtonCell.style.verticalAlign = "top";
    closeButtonCell.style.width = "30px";
    closeButtonCell.style.textAlign = "right";
    closeButtonCell.style.padding = "2px 1px 1px 1px";

    //Container
    form.container = document.createElement("div");
    form.appendChild(form.container);
    form.container.className = "stiMobileViewerFormContainer";

    //Separator
    form.buttonsSeparator = this.FormSeparator();
    form.appendChild(form.buttonsSeparator);

    //Buttons
    form.floatPanel = document.createElement("div");
    form.buttonsPanel = document.createElement("div");
    form.floatPanel.appendChild(form.buttonsPanel);
    form.appendChild(form.floatPanel);
    form.buttonsPanel.className = "stiMobileViewerFormButtonsPanel";
    var buttonsTable = this.CreateHTMLTable();
    form.buttonsPanel.appendChild(buttonsTable);
    form.buttonsTable = buttonsTable;
    
    form.buttonOk = this.FormButton(null, this.loc.Gui.barname_ok, null);
    form.buttonOk.action = function () { form.action(); };
    buttonsTable.addCell(form.buttonOk).style.padding = "8px";

    form.buttonCancel = this.FormButton(null, this.loc.Gui.barname_cancel, null);
    form.buttonCancel.action = function () { form.changeVisibleState(false); };
    buttonsTable.addCell(form.buttonCancel).style.padding = "8px 8px 8px 0";

    form.changeVisibleState = function (state) {
        if (state) {
            this.style.display = "";
            this.onshow();
            this.jsObject.SetObjectToCenter(this);
            this.jsObject.options.disabledPanels[this.level].changeVisibleState(true);
            this.visible = true;
            this.jsObject.options.currentForm = this;
            d = new Date();
            var endTime = d.getTime() + this.jsObject.options.formAnimDuration;
            this.flag = false;
            this.jsObject.ShowAnimationForm(this, endTime);
        }
        else {
            clearTimeout(this.animationTimer);
            this.visible = false;
            this.jsObject.options.currentForm = null;
            this.style.display = "none";
            this.onhide();
            this.jsObject.options.disabledPanels[this.level].changeVisibleState(false);
        }
    }

    form.action = function () { };
    form.onshow = function () { };
    form.onhide = function () { };

    form.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.ontouchstart(true);
    }

    form.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.formPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    //Mouse Events
    form.header.onmousedown = function (event) {
        if (!event || this.isTouchStartFlag) return;
        var mouseStartX = event.clientX;
        var mouseStartY = event.clientY;
        var formStartX = this.thisForm.jsObject.FindPosX(this.thisForm, "stiMobileViewerMainPanel");
        var formStartY = this.thisForm.jsObject.FindPosY(this.thisForm, "stiMobileViewerMainPanel");
        this.thisForm.jsObject.options.formInDrag = [mouseStartX, mouseStartY, formStartX, formStartY, this.thisForm];
    }

    //Touch Events
    form.header.ontouchstart = function (event) {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        var fingerStartX = event.touches[0].pageX;
        var fingerStartY = event.touches[0].pageY;
        var formStartX = this.thisForm.jsObject.FindPosX(this.thisForm, "stiMobileViewerMainPanel");
        var formStartY = this.thisForm.jsObject.FindPosY(this.thisForm, "stiMobileViewerMainPanel");
        this.thisForm.jsObject.options.formInDrag = [fingerStartX, fingerStartY, formStartX, formStartY, this.thisForm];
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    form.header.ontouchmove = function (event) {
        event.preventDefault();

        if (this.thisForm.jsObject.options.formInDrag) {
            var formInDrag = this.thisForm.jsObject.options.formInDrag;
            var formStartX = formInDrag[2];
            var formStartY = formInDrag[3];
            var fingerCurrentXPos = event.touches[0].pageX;
            var fingerCurrentYPos = event.touches[0].pageY;
            var deltaX = formInDrag[0] - fingerCurrentXPos;
            var deltaY = formInDrag[1] - fingerCurrentYPos;
            var newPosX = formStartX - deltaX;
            var newPosY = formStartY - deltaY;
            formInDrag[4].style.left = newPosX + "px";
            formInDrag[4].style.top = newPosY + "px";
        }
    }

    form.header.ontouchend = function () {
        event.preventDefault();
        this.thisForm.jsObject.options.formInDrag = false;
    }

    //Form Move
    form.move = function (evnt) {
        var leftPos = this.jsObject.options.formInDrag[2] + (evnt.clientX - this.jsObject.options.formInDrag[0]);
        var topPos = this.jsObject.options.formInDrag[3] + (evnt.clientY - this.jsObject.options.formInDrag[1]);

        this.style.left = leftPos > 0 ? leftPos + "px" : 0;
        this.style.top = topPos > 0 ? topPos + "px" : 0;
    }

    return form;
}

//Separator
StiMobileViewer.prototype.FormSeparator = function () {
    var separator = document.createElement("div");
    separator.className = "stiMobileViewerFormSeparator";

    return separator;
}