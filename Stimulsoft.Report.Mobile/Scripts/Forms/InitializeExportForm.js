﻿
StiMobileViewer.prototype.InitializeExportForm = function (formName) {

    var exportForm = this.BaseForm(formName, this.loc.Export.title, 2);
    exportForm.style.fontFamily = this.options.toolbarFontFamily;
    exportForm.style.color = (this.options.theme == "Purple" && this.options.toolbarFontColor == "#444444") ? "#ffffff" : this.options.toolbarFontColor;
    exportForm.style.fontSize = "12px";
    exportForm.controls = {};
    exportForm.labels = {};
    exportForm.container.style.padding = "3px";

    exportForm.addControlToParentControl = function (label, control, parentControl, name) {
        if (parentControl.innerTable == null) {
            parentControl.innerTable = exportForm.jsObject.CreateHTMLTable();
            parentControl.innerTable.style.width = "100%";
            parentControl.appendChild(parentControl.innerTable);
        }
        control.parentRow = parentControl.innerTable.addRow();
        var cellForLabel = parentControl.innerTable.addCellInLastRow();
        var cellForControl = (label != null || typeof (label) == "undefined") ? parentControl.innerTable.addCellInLastRow() : cellForLabel;
        if (label != null || typeof (label) == "undefined") {
            cellForLabel.style.padding = "0 8px 0 8px";
            if (!exportForm.jsObject.options.isMaterial) {
                cellForLabel.style.minWidth = "150px";
            } else {
                cellForLabel.style.maxWidth = "90px";
            }
            if (label) cellForLabel.innerHTML = label;
            exportForm.labels[name] = cellForLabel;
            var tooltip = control.getAttribute("title");
            if (tooltip != null) cellForLabel.setAttribute("title", tooltip);
        }
        else {
            cellForControl.setAttribute("colspan", "2");
        }
        cellForControl.appendChild(control);
    }

    var mrgn = "8px";

    //0-name, 1-label, 2-control, 3-parentControlName, 4-margin
    var controlProps = [
        ["SavingReportGroup", null, this.GroupPanel(this.loc.DesignerFx.SavingReport, true, 390, "4px 0 4px 0"), null, "4px"],
        ["SaveReportMdc", null, this.RadioButton(exportForm.name + "SaveReportMdc", exportForm.name + "SavingReportGroup", this.loc.FormViewer.DocumentFile.replace("...", "") + " (.mdc)", null), "SavingReportGroup.container", "6px " + mrgn + " 3px " + mrgn],
        ["SaveReportMdz", null, this.RadioButton(exportForm.name + "SaveReportMdz", exportForm.name + "SavingReportGroup", this.loc.FormViewer.CompressedDocumentFile + " (.mdz)", null), "SavingReportGroup.container", "3px " + mrgn + " 3px " + mrgn],
        ["SaveReportMdx", null, this.RadioButton(exportForm.name + "SaveReportMdx", exportForm.name + "SavingReportGroup", this.loc.FormViewer.EncryptedDocumentFile + " (.mdx)", null), "SavingReportGroup.container", "3px " + mrgn + " 0px " + mrgn],
        ["SaveReportPassword", this.loc.Report.LabelPassword, this.TextBox(null, 140, this.loc.HelpViewer.UserPassword), "SavingReportGroup.container", "4px " + mrgn + " 0px " + mrgn],
        ["PageRangeGroup", null, this.GroupPanel(this.loc.Report.RangePage, true, 390, "4px 0 4px 0"), null, "4px"],
        ["PageRangeAll", null, this.RadioButton(exportForm.name + "PagesRangeAll", exportForm.name + "PageRangeGroup", this.loc.Report.RangeAll, this.loc.HelpViewer.PageAll), "PageRangeGroup.container", "6px " + mrgn + " 6px " + mrgn],
        ["PageRangeCurrentPage", null, this.RadioButton(exportForm.name + "PagesRangeCurrentPage", exportForm.name + "PageRangeGroup", this.loc.Report.RangeCurrentPage, this.loc.HelpViewer.CurrentPage), "PageRangeGroup.container", "0px " + mrgn + " 4px " + mrgn],
        ["PageRangePages", null, this.RadioButton(exportForm.name + "PagesRangePages", exportForm.name + "PageRangeGroup", this.loc.Report.RangePages, this.loc.HelpViewer.RangePages), "PageRangeGroup.container", "0px " + mrgn + " 0px " + mrgn],
        ["PageRangePagesText", null, this.TextBox(null, 130, this.loc.HelpViewer.RangePages), "PageRangePages.lastCell"/*, true*/, "0 0 0 30px"],
        ["SettingsGroup", null, this.GroupPanel(this.loc.Export.Settings, false, 390, "4px 0 4px 0"), null, "4px"],
        ["ImageType", this.loc.PropertyMain.Type + ":", this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.TypeExport, this.GetImageTypesItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["DataType", this.loc.PropertyMain.Type + ":", this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.TypeExport, this.GetDataTypesItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ExcelType", this.loc.PropertyMain.Type + ":", this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.TypeExport, this.GetExcelTypesItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["HtmlType", this.loc.PropertyMain.Type + ":", this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.TypeExport, this.GetHtmlTypesItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["Zoom", this.loc.Export.Scale, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ScaleHtml, this.GetZoomItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ImageFormatForHtml", this.loc.Export.ImageFormat, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ImageFormat, this.GetImageFormatForHtmlItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ExportMode", this.loc.Export.ExportMode, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ExportMode, this.GetExportModeItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],        
        ["UseEmbeddedImages", null, this.CheckBox(null, this.loc.Export.EmbeddedImageData, this.loc.HelpViewer.EmbeddedImageData), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["AddPageBreaks", null, this.CheckBox(null, this.loc.Export.AddPageBreaks, this.loc.HelpViewer.AddPageBreaks), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["ImageResolution", this.loc.Export.ImageResolution, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ImageResolution, this.GetImageResolutionItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ImageResolutionMode", this.loc.Export.ImageResolutionMode, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ImageResolutionMode, this.GetImageResolutionModeItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ImageCompressionMethod", this.loc.Export.ImageCompressionMethod, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ImageCompressionMethod, this.GetImageCompressionMethodItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["AllowEditable", this.loc.Export.AllowEditable, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.AllowEditable, this.GetAllowEditableItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ImageQuality", this.loc.Export.ImageQuality, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ImageQuality, this.GetImageQualityItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["RestrictEditing", this.loc.Export.RestrictEditing, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.RestrictEditing, this.GetRestrictEditingItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ContinuousPages", null, this.CheckBox(null, this.loc.Export.ContinuousPages, this.loc.HelpViewer.ContinuousPages), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["CompressToArchive", null, this.CheckBox(null, this.loc.Export.CompressToArchive, this.loc.HelpViewer.CompressToArchive), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["StandardPdfFonts", null, this.CheckBox(null, this.loc.Export.StandardPDFFonts, this.loc.HelpViewer.StandardPdfFonts), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["EmbeddedFonts", null, this.CheckBox(null, this.loc.Export.EmbeddedFonts, this.loc.HelpViewer.EmbeddedFonts), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["UseUnicode", null, this.CheckBox(null, this.loc.Export.UseUnicode, this.loc.HelpViewer.UseUnicode), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["Compressed", null, this.CheckBox(null, this.loc.Export.Compressed, this.loc.HelpViewer.Compressed), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["ExportRtfTextAsImage", null, this.CheckBox(null, this.loc.Export.ExportRtfTextAsImage, this.loc.HelpViewer.ExportRtfTextAsImage), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["PdfACompliance", null, this.CheckBox(null, this.loc.Export.PdfACompliance, this.loc.HelpViewer.PdfACompliance), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["KillSpaceLines", null, this.CheckBox(null, this.loc.Export.TxtKillSpaceLines, this.loc.HelpViewer.KillSpaceLines), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["PutFeedPageCode", null, this.CheckBox(null, this.loc.Export.TxtPutFeedPageCode, this.loc.HelpViewer.PutFeedPageCode), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["DrawBorder", null, this.CheckBox(null, this.loc.Export.TxtDrawBorder, this.loc.HelpViewer.DrawBorder), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["CutLongLines", null, this.CheckBox(null, this.loc.Export.TxtCutLongLines, this.loc.HelpViewer.CutLongLines), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["BorderType", this.loc.Export.TxtBorderType + ":", this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.BorderType, this.GetBorderTypeItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ZoomX", this.loc.Export.Zoom ? this.loc.Export.Zoom.replace(":", "") + " X: " : "", this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ZoomTxt, this.GetZoomItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ZoomY", this.loc.Export.Zoom ? this.loc.Export.Zoom.replace(":", "") + " Y: " : "", this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ZoomTxt, this.GetZoomItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["EncodingTextOrCsvFile", this.loc.Export.Encoding, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.EncodingData, this.GetEncodingDataItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ImageFormat", this.loc.Export.ImageType + ":", this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ImageType, this.GetImageFormatItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["DitheringType", this.loc.Export.MonochromeDitheringType, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.DitheringType, this.GetMonochromeDitheringTypeItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["TiffCompressionScheme", this.loc.Export.TiffCompressionScheme, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.TiffCompressionScheme, this.GetTiffCompressionSchemeItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["CutEdges", null, this.CheckBox(null, this.loc.Export.ImageCutEdges, this.loc.HelpViewer.CutEdges), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["MultipleFiles", null, this.CheckBox(null, this.loc.Export.MultipleFiles, this.loc.HelpViewer.MultipleFiles), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["ExportDataOnly", null, this.CheckBox(null, this.loc.Export.ExportDataOnly, this.loc.HelpViewer.ExportDataOnly), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["UseDefaultSystemEncoding", null, this.CheckBox(null, this.loc.Export.UseDefaultSystemEncoding, this.loc.HelpViewer.UseDefaultSystemEncoding), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["EncodingDifFile", this.loc.Export.Encoding, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.EncodingData, this.GetEncodingDifFileItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["ExportModeRtf", this.loc.Export.ExportMode, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ExportModeRtf, this.GetExportModeRtfItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["UsePageHeadersAndFooters", null, this.CheckBox(null, this.loc.Export.UsePageHeadersAndFooters, this.loc.HelpViewer.UsePageHeadersAndFooters), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["RemoveEmptySpaceAtBottom", null, this.CheckBox(null, this.loc.Export.RemoveEmptySpaceAtBottom, this.loc.HelpViewer.RemoveEmptySpaceAtBottom), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["Separator", this.loc.Export.Separator, this.TextBox(null, 160, this.loc.HelpViewer.Separator), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["DataExportMode", this.loc.Export.BandsFilter, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.ExportMode, this.GetDataExportModeItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["SkipColumnHeaders", null, this.CheckBox(null, this.loc.Export.SkipColumnHeaders, this.loc.HelpViewer.SkipColumnHeaders), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["ExportObjectFormatting", null, this.CheckBox(null, this.loc.Export.ExportObjectFormatting, this.loc.HelpViewer.ExportObjectFormatting), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["UseOnePageHeaderAndFooter", null, this.CheckBox(null, this.loc.Export.UseOnePageHeaderAndFooter, this.loc.HelpViewer.UseOnePageHeaderAndFooter), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["ExportEachPageToSheet", null, this.CheckBox(null, this.loc.Export.ExportEachPageToSheet, this.loc.HelpViewer.ExportEachPageToSheet), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["ExportPageBreaks", null, this.CheckBox(null, this.loc.Export.ExportPageBreaks, this.loc.HelpViewer.ExportPageBreaks), "SettingsGroup.container", "4px " + mrgn + " 4px " + mrgn],
        ["EncodingDbfFile", this.loc.Export.Encoding, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.EncodingData, this.GetEncodingDbfFileItems(), true), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["DocumentSecurityButton", null, this.SmallButton(null, null, this.loc.Export.DocumentSecurity, null, null, "Down", this.GetStyles("FormButton")), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["DigitalSignatureButton", null, this.SmallButton(null, null, this.loc.Export.DigitalSignature, null, null, "Down", this.GetStyles("FormButton")), "SettingsGroup.container", "2px " + mrgn + " 2px " + mrgn],
        ["OpenAfterExport", null, this.CheckBox(null, this.loc.Export.OpenAfterExport, this.loc.HelpViewer.OpenAfterExport), null, "4px " + mrgn + " 4px " + mrgn],
        ["DocumentSecurityMenu", null, this.BaseMenu(exportForm.name + "DocumentSecurityMenu", null, "Down"), null, null],
        ["PasswordInputUser", this.loc.Export.labelUserPassword, this.TextBox(null, 160, this.loc.HelpViewer.UserPassword), "DocumentSecurityMenu.innerContent", "8px " + mrgn + " 2px " + mrgn],
        ["PasswordInputOwner", this.loc.Export.labelOwnerPassword, this.TextBox(null, 160, this.loc.HelpViewer.OwnerPassword), "DocumentSecurityMenu.innerContent", "2px " + mrgn + " 2px " + mrgn],
        ["PrintDocument", null, this.CheckBox(null, this.loc.Export.AllowPrintDocument, this.loc.HelpViewer.AllowPrintDocument), "DocumentSecurityMenu.innerContent", "4px " + mrgn + " 4px " + mrgn],
        ["ModifyContents", null, this.CheckBox(null, this.loc.Export.AllowModifyContents, this.loc.HelpViewer.AllowModifyContents), "DocumentSecurityMenu.innerContent", "4px " + mrgn + " 4px " + mrgn],
        ["CopyTextAndGraphics", null, this.CheckBox(null, this.loc.Export.AllowCopyTextAndGraphics, this.loc.HelpViewer.AllowCopyTextAndGraphics), "DocumentSecurityMenu.innerContent", "4px " + mrgn + " 4px " + mrgn],
        ["AddOrModifyTextAnnotations", null, this.CheckBox(null, this.loc.Export.AllowAddOrModifyTextAnnotations, this.loc.HelpViewer.AllowAddOrModifyTextAnnotations), "DocumentSecurityMenu.innerContent", "4px " + mrgn + " 4px " + mrgn],
        ["KeyLength", this.loc.Export.labelEncryptionKeyLength, this.DropDownListForExportForm(null, 160, this.loc.HelpViewer.EncryptionKeyLength, this.GetEncryptionKeyLengthItems(), true), "DocumentSecurityMenu.innerContent", "2px " + mrgn + " 8px " + mrgn],
        ["DigitalSignatureMenu", null, this.BaseMenu(exportForm.name + "DigitalSignatureMenu", null, "Down"), null, null],
        ["UseDigitalSignature", null, this.CheckBox(null, this.loc.Export.UseDigitalSignature, this.loc.HelpViewer.DigitalSignature), "DigitalSignatureMenu.innerContent", "8px " + mrgn + " 4px " + mrgn],
        ["GetCertificateFromCryptoUI", null, this.CheckBox(null, this.loc.Export.GetCertificateFromCryptoUI, this.loc.HelpViewer.GetCertificateFromCryptoUI), "DigitalSignatureMenu.innerContent", "4px " + mrgn + " 4px " + mrgn],
        ["SubjectNameString", this.loc.Export.labelSubjectNameString, this.TextBox(null, 160, this.loc.HelpViewer.SubjectNameString), "DigitalSignatureMenu.innerContent", "8px " + mrgn + " 8px " + mrgn]
    ]

    //Add Controls To Form
    for (var i = 0; i < controlProps.length; i++) {
        var name = controlProps[i][0];
        var label = controlProps[i][1];
        var control = controlProps[i][2];
        var parentControlName = controlProps[i][3];
        exportForm.controls[name] = control;
        if (controlProps[i][4]) control.style.margin = controlProps[i][4];
        if (control.className == "stiMobileViewerGroupPanel") {
            control.container.style.paddingBottom = "6px";
            if (exportForm.jsObject.options.isMaterial) {
                control.container.style.maxHeight = window.innerHeight - 285 + "px";
                control.container.style.overflowY = "scroll";
            }
        }
        if (name == "DocumentSecurityMenu" || name == "DigitalSignatureMenu") continue;

        if (parentControlName != null) {
            var controlNamesArray = parentControlName.split(".");
            var parentControl = exportForm.controls[controlNamesArray[0]];
            if (controlNamesArray.length > 1) {
                for (var k = 1; k < controlNamesArray.length; k++) {
                    if (parentControl) parentControl = parentControl[controlNamesArray[k]]
                }
            }
            if (parentControl) exportForm.addControlToParentControl(label, control, parentControl, name);
            continue;
        }
        exportForm.addControlToParentControl(label, control, exportForm.container, name);
    }

    exportForm.controls.PageRangePages.lastCell.style.paddingLeft = !exportForm.jsObject.isMaterial ? "60px" : "30px";
    
    if (this.options.theme == "Purple") exportForm.controls.OpenAfterExport.style.color = "#ffffff";

    try {
        exportForm.controls.PasswordInputUser.setAttribute("type", "password");
        exportForm.controls.PasswordInputOwner.setAttribute("type", "password");
        exportForm.controls.SaveReportPassword.setAttribute("type", "password");
    } catch (e) { }

    exportForm.controls.DocumentSecurityMenu.parentButton = exportForm.controls.DocumentSecurityButton;
    exportForm.controls.DigitalSignatureMenu.parentButton = exportForm.controls.DigitalSignatureButton;
    var buttonNames = ["DocumentSecurityButton", "DigitalSignatureButton"];
    for (var i = 0; i < buttonNames.length; i++) {
        var button = exportForm.controls[buttonNames[i]];
        button.innerTable.style.width = "100%";
        button.style.minWidth = "220px";
        button.caption.style.textAlign = "center";
        button.caption.style.width = "100%";
        button.style.display = "inline-block";
    }

    //Add Action Methods To Controls
    //Types Controls
    var controlNames = ["ImageType", "DataType", "ExcelType", "HtmlType"];
    for (var i = 0; i < controlNames.length; i++) {
        exportForm.controls[controlNames[i]].action = function () {
            exportForm.showControlsByExportFormat(this.key == "ExcelBinary" ? "StiExcelExportSettings" : "Sti" + this.key + "ExportSettings", true);
        }
    }
    //Saving Report
    var controlNames = ["SaveReportMdc", "SaveReportMdz", "SaveReportMdx"];
    for (var i = 0; i < controlNames.length; i++) {
        exportForm.controls[controlNames[i]].controlName = controlNames[i];
        exportForm.controls[controlNames[i]].onChecked = function () {
            if (this.isChecked) { exportForm.controls.SaveReportPassword.setEnabled(this.controlName == "SaveReportMdx"); }
        }
    }
    //PdfACompliance
    exportForm.controls.PdfACompliance.onChecked = function () {
        var controlNames = ["StandardPdfFonts", "EmbeddedFonts", "UseUnicode"];
        for (var i = 0; i < controlNames.length; i++) {
            exportForm.controls[controlNames[i]].setEnabled(!this.isChecked);
        }
    }
    //EmbeddedFonts, UseUnicode
    var controlNames = ["EmbeddedFonts", "UseUnicode"];
    for (var i = 0; i < controlNames.length; i++) {
        exportForm.controls[controlNames[i]].onChecked = function () { if (this.isChecked) exportForm.controls.StandardPdfFonts.setChecked(false); };
    }
    //StandardPdfFonts
    exportForm.controls.StandardPdfFonts.onChecked = function () {
        if (!this.isChecked) return;
        var controlNames = ["EmbeddedFonts", "UseUnicode"];
        for (var i = 0; i < controlNames.length; i++) {
            exportForm.controls[controlNames[i]].setChecked(false);
        }
    }
    //ImageCompressionMethod
    exportForm.controls.ImageCompressionMethod.onChange = function () {
        exportForm.controls.ImageQuality.setEnabled(this.key == "Jpeg");
    }
    //ExportDataOnly
    exportForm.controls.ExportDataOnly.onChecked = function () {
        exportForm.controls.ExportObjectFormatting.setEnabled(this.isChecked);
        exportForm.controls.UseOnePageHeaderAndFooter.setEnabled(!this.isChecked);
    }
    //UseDefaultSystemEncoding
    exportForm.controls.UseDefaultSystemEncoding.onChecked = function () {
        exportForm.controls.EncodingDifFile.setEnabled(!this.isChecked);
    }
    //ImageType
    exportForm.controls.ImageType.onChange = function () {
        exportForm.controls.TiffCompressionScheme.setEnabled(this.key == "Tiff");
        var items = exportForm.jsObject.GetImageFormatItems(this.key == "Emf");
        exportForm.controls.ImageFormat.menu.addItems(items);
    }
    //ImageFormat
    exportForm.controls.ImageFormat.onChange = function () {
        exportForm.controls.DitheringType.setEnabled(this.key == "Monochrome");
    }
    //DocumentSecurityButton
    exportForm.controls.DocumentSecurityButton.action = function () {
        exportForm.jsObject.options.menus[exportForm.name + "DocumentSecurityMenu"].changeVisibleState(!this.isSelected);
    }
    //DigitalSignatureButton
    exportForm.controls.DigitalSignatureButton.action = function () {
        exportForm.jsObject.options.menus[exportForm.name + "DigitalSignatureMenu"].changeVisibleState(!this.isSelected);
    }
    //UseDigitalSignature
    exportForm.controls.UseDigitalSignature.onChecked = function () {
        //exportForm.controls.GetCertificateFromCryptoUI.setEnabled(this.isChecked);
        exportForm.controls.SubjectNameString.setEnabled(this.isChecked && !exportForm.controls.GetCertificateFromCryptoUI.isChecked);
    }
    //GetCertificateFromCryptoUI
    exportForm.controls.GetCertificateFromCryptoUI.onChecked = function () {
        exportForm.controls.SubjectNameString.setEnabled(!this.isChecked && exportForm.controls.UseDigitalSignature.isChecked);
    }

    //Form Methods
    exportForm.setControlsValue = function (exportSettings, ignoreTypeControls) {
        var defaultExportSettings = exportSettings || exportForm.jsObject.options.defaultExportSettings[exportForm.exportFormat];
        var exportControlNames = exportForm.jsObject.ExportControlNames[exportForm.exportFormat];
        if (!defaultExportSettings) return;

        //Reset Enabled States for All Controls
        for (var i in exportForm.controls) {
            if (exportForm.controls[i]["setEnabled"] != null) exportForm.controls[i].setEnabled(true);
        }

        if (exportForm.jsObject.options.cloudMode) {
            exportForm.controls.DigitalSignatureButton.setEnabled(false);
        }

        //PageRange       
        var pageRangeAllIsDisabled = exportForm.jsObject.isContainted(exportControlNames, "ImageType") && exportForm.exportFormat != "StiTiffExportSettings";
        exportForm.controls[!pageRangeAllIsDisabled ? "PageRangeAll" : "PageRangeCurrentPage"].setChecked(true);
        exportForm.controls.PageRangeAll.setEnabled(!pageRangeAllIsDisabled);

        for (var propertyName in defaultExportSettings) {
            if (exportForm.jsObject.isContainted(exportControlNames, propertyName)) {
                if (propertyName == "ImageType" || propertyName == "DataType" || propertyName == "ExcelType" || propertyName == "HtmlType") {
                    if (ignoreTypeControls) continue;

                    switch (propertyName) {
                        case "ImageType":
                            if (!exportForm.jsObject.options.showExportToBmp && defaultExportSettings[propertyName] == "Bmp") defaultExportSettings[propertyName] = "Gif";
                            if (!exportForm.jsObject.options.showExportToGif && defaultExportSettings[propertyName] == "Gif") defaultExportSettings[propertyName] = "Jpeg";
                            if (!exportForm.jsObject.options.showExportToJpeg && defaultExportSettings[propertyName] == "Jpeg") defaultExportSettings[propertyName] = "Pcx";
                            if (!exportForm.jsObject.options.showExportToPcx && defaultExportSettings[propertyName] == "Pcx") defaultExportSettings[propertyName] = "Png";
                            if (!exportForm.jsObject.options.showExportToPng && defaultExportSettings[propertyName] == "Png") defaultExportSettings[propertyName] = "Tiff";
                            if (!exportForm.jsObject.options.showExportToTiff && defaultExportSettings[propertyName] == "Tiff") defaultExportSettings[propertyName] = "Emf";
                            if (!exportForm.jsObject.options.showExportToMetafile && defaultExportSettings[propertyName] == "Emf") defaultExportSettings[propertyName] = "Svg";
                            if (!exportForm.jsObject.options.showExportToSvg && defaultExportSettings[propertyName] == "Svg") defaultExportSettings[propertyName] = "Svgz";
                            if (!exportForm.jsObject.options.showExportToSvgz && defaultExportSettings[propertyName] == "Svgz") defaultExportSettings[propertyName] = "Bmp";
                            break;

                        case "DataType":
                            if (!exportForm.jsObject.options.showExportToCsv && defaultExportSettings[propertyName] == "Csv") defaultExportSettings[propertyName] = "Dbf";
                            if (!exportForm.jsObject.options.showExportToDbf && defaultExportSettings[propertyName] == "Dbf") defaultExportSettings[propertyName] = "Xml";
                            if (!exportForm.jsObject.options.showExportToXml && defaultExportSettings[propertyName] == "Xml") defaultExportSettings[propertyName] = "Dif";
                            if (!exportForm.jsObject.options.showExportToDif && defaultExportSettings[propertyName] == "Dif") defaultExportSettings[propertyName] = "Sylk";
                            if (!exportForm.jsObject.options.showExportToSylk && defaultExportSettings[propertyName] == "Sylk") defaultExportSettings[propertyName] = "Csv";
                            break;

                        case "ExcelType":
                            if (!exportForm.jsObject.options.showExportToExcel2007 && defaultExportSettings[propertyName] == "Excel2007") defaultExportSettings[propertyName] = "ExcelBinary";
                            if (!exportForm.jsObject.options.showExportToExcel && defaultExportSettings[propertyName] == "ExcelBinary") defaultExportSettings[propertyName] = "ExcelXml";
                            if (!exportForm.jsObject.options.showExportToExcelXml && defaultExportSettings[propertyName] == "ExcelXml") defaultExportSettings[propertyName] = "Excel2007";
                            break;

                        case "HtmlType":
                            if (!exportForm.jsObject.options.showExportToHtml && defaultExportSettings[propertyName] == "Html") defaultExportSettings[propertyName] = "Html5";
                            if (!exportForm.jsObject.options.showExportToHtml5 && defaultExportSettings[propertyName] == "Html5") defaultExportSettings[propertyName] = "Mht";
                            if (!exportForm.jsObject.options.showExportToMht && defaultExportSettings[propertyName] == "Mht") defaultExportSettings[propertyName] = "Html";
                            break;
                    }
                }

                var control = exportForm.controls[propertyName];
                exportForm.setDefaultValueToControl(control, defaultExportSettings[propertyName]);
            }
        }

        //Exceptions
        if (exportForm.exportFormat == "SaveDocument") exportForm.controls.SaveReportMdc.setChecked(true);
        if (exportForm.exportFormat == "StiPdfExportSettings" && defaultExportSettings.StandardPdfFonts) exportForm.controls.StandardPdfFonts.setChecked(true);
        if (exportForm.jsObject.isContainted(exportControlNames, "HtmlType") && defaultExportSettings.ImageFormat) exportForm.controls.ImageFormatForHtml.setKey(defaultExportSettings.ImageFormat);
        if (exportForm.exportFormat == "StiRtfExportSettings" && defaultExportSettings.ExportMode) exportForm.controls.ExportModeRtf.setKey(defaultExportSettings.ExportMode);
        if (exportForm.jsObject.isContainted(exportControlNames, "ImageType") && defaultExportSettings.ImageZoom) exportForm.controls.Zoom.setKey(defaultExportSettings.ImageZoom.toString());
        if (exportForm.exportFormat == "StiPdfExportSettings") {
            var userAccessPrivileges = defaultExportSettings.UserAccessPrivileges;
            exportForm.controls.PrintDocument.setChecked(userAccessPrivileges.indexOf("PrintDocument") != -1 || userAccessPrivileges == "All");
            exportForm.controls.ModifyContents.setChecked(userAccessPrivileges.indexOf("ModifyContents") != -1 || userAccessPrivileges == "All");
            exportForm.controls.CopyTextAndGraphics.setChecked(userAccessPrivileges.indexOf("CopyTextAndGraphics") != -1 || userAccessPrivileges == "All");
            exportForm.controls.AddOrModifyTextAnnotations.setChecked(userAccessPrivileges.indexOf("AddOrModifyTextAnnotations") != -1 || userAccessPrivileges == "All");
        }
        //Encodings
        if (exportForm.exportFormat == "StiDifExportSettings" || exportForm.exportFormat == "StiSylkExportSettings") exportForm.controls.EncodingDifFile.setKey("437");
        if (exportForm.exportFormat == "StiDbfExportSettings" && defaultExportSettings.CodePage) exportForm.controls.EncodingDbfFile.setKey(defaultExportSettings.CodePage);
        if ((exportForm.exportFormat == "StiTxtExportSettings" || exportForm.exportFormat == "StiCsvExportSettings") && defaultExportSettings.Encoding)
            exportForm.controls.EncodingTextOrCsvFile.setKey(defaultExportSettings.Encoding);
    }

    exportForm.onhide = function () {
        exportForm.jsObject.SetCookie("StimulsoftMobileViewerExportSettingsOpeningGroups", JSON.stringify({
            SavingReportGroup: exportForm.controls.SavingReportGroup.isOpened,
            PageRangeGroup: exportForm.controls.PageRangeGroup.isOpened,
            SettingsGroup: exportForm.controls.SettingsGroup.isOpened
        }));
    }

    exportForm.show = function (exportFormat) {
        exportForm.exportFormat = exportFormat || "StiPdfExportSettings";
        exportForm.showControlsByExportFormat(exportForm.exportFormat);

        if (exportForm.jsObject.options.storeExportSettings) {
            var exportSettingsStr = exportForm.jsObject.GetCookie("StimulsoftMobileViewerExportSettings" + exportForm.jsObject.GetCommonExportFormat(exportForm.exportFormat));
            if (exportSettingsStr) {
                var exportSettings = JSON.parse(exportSettingsStr);
                var exportFormat = exportSettings.ImageType || exportSettings.DataType || exportSettings.ExcelType || exportSettings.HtmlType;
                if (exportFormat) {
                    exportFormat = exportFormat == "ExcelBinary" ? "StiExcelExportSettings" : "Sti" + exportFormat + "ExportSettings";
                    exportForm.showControlsByExportFormat(exportFormat);
                }
                exportForm.setControlsValue(exportSettings);
            }
        }

        var openingGroupsStr = exportForm.jsObject.GetCookie("StimulsoftMobileViewerExportSettingsOpeningGroups");
        var openingGroups = openingGroupsStr ? JSON.parse(openingGroupsStr) : null;

        exportForm.controls.GetCertificateFromCryptoUI.style.display = "none";
        exportForm.controls.GetCertificateFromCryptoUI.setChecked(false);
        exportForm.controls.SavingReportGroup.changeOpeningState(openingGroups ? openingGroups.SavingReportGroup : true);
        exportForm.controls.PageRangeGroup.changeOpeningState(openingGroups ? openingGroups.PageRangeGroup : true);
        exportForm.controls.SettingsGroup.changeOpeningState(openingGroups ? openingGroups.SettingsGroup : false);

        exportForm.changeVisibleState(true);
    }

    exportForm.action = function () {
        var exportSettingsObject = exportForm.getExportSettingsObject();
        exportForm.changeVisibleState(false);

        if (exportForm.jsObject.options.storeExportSettings) {
            exportForm.jsObject.SetCookie("StimulsoftMobileViewerExportSettings" + exportForm.jsObject.GetCommonExportFormat(exportForm.exportFormat),
                JSON.stringify(exportSettingsObject));
        }

        exportForm.jsObject.exportEvent(exportForm.exportFormat, exportSettingsObject);
    }

    exportForm.showControlsByExportFormat = function (exportFormat, ignoreTypeControls) {
        exportForm.exportFormat = exportFormat;
        for (var controlName in exportForm.controls) {
            var control = exportForm.controls[controlName];
            var exportControlNames = exportForm.jsObject.ExportControlNames[exportFormat];
            if (control.parentRow) {
                control.parentRow.style.display = exportForm.jsObject.isContainted(exportControlNames, controlName) ? "" : "none";
            }
        }
        exportForm.setControlsValue(null, ignoreTypeControls);
    }


    exportForm.setDefaultValueToControl = function (control, value) {
        if (control["setKey"] != null) control.setKey(value.toString());
        else if (control["setChecked"] != null) control.setChecked(value);
        else if (control["value"] != null) control.value = value;
    }

    exportForm.getValueFromControl = function (control) {
        if (control["isEnabled"] == false) return control["setChecked"] != null ? false : null;
        else if (control["setKey"] != null) return control.key;
        else if (control["setChecked"] != null) return control.isChecked;
        else if (control["value"] != null) return control.value;

        return null;
    }

    exportForm.getExportSettingsObject = function () {
        var exportSettings = {};
        var exportControlNames = exportForm.jsObject.ExportControlNames[exportForm.exportFormat];

        for (var i = 0; i < exportControlNames.length; i++) {
            var controls = exportForm.controls;
            var controlName = exportControlNames[i];
            var control = controls[controlName];
            if (control.groupName == exportForm.name + "SavingReportGroup" || control.groupName == exportForm.name + "PageRangeGroup" ||
                controlName == "PageRangePagesText") {
                continue;
            }
            else if (controlName == "SavingReportGroup") {
                exportSettings.Format = controls.SaveReportMdc.isChecked ? "Mdc" : (controls.SaveReportMdz.isChecked ? "Mdz" : "Mdx");
                if (exportSettings.Format == "Mdx") exportSettings.Password = controls.SaveReportPassword.value;
            }
            else if (controlName == "PageRangeGroup") {
                exportSettings.PageRange = controls.PageRangeAll.isChecked ? "All" :
                    (controls.PageRangeCurrentPage.isChecked ? (exportForm.jsObject.options.pageNumber + 1).toString() : controls.PageRangePagesText.value);
            }
            else {
                var value = exportForm.getValueFromControl(control);
                if (value != null) exportSettings[controlName] = value;
            }
        }

        //Exceptions
        if (exportForm.exportFormat == "StiPdfExportSettings") {
            exportSettings.UserAccessPrivileges = "";
            var controlNames = ["PrintDocument", "ModifyContents", "CopyTextAndGraphics", "AddOrModifyTextAnnotations"];
            for (var i = 0; i < controlNames.length; i++) {
                if (exportSettings[controlNames[i]]) {
                    if (exportSettings.UserAccessPrivileges != "") exportSettings.UserAccessPrivileges += ", ";
                    exportSettings.UserAccessPrivileges += controlNames[i];
                    delete exportSettings[controlNames[i]];
                }
            }
        }

        if (exportForm.jsObject.isContainted(exportControlNames, "ImageType")) {
            exportSettings.ImageZoom = exportSettings.Zoom;
            delete exportSettings.Zoom;
        }
        var controlNames = [
                ["ImageFormatForHtml", "ImageFormat"],
                ["EncodingTextOrCsvFile", "Encoding"],
                ["ExportModeRtf", "ExportMode"],
                ["EncodingDifFile", "Encoding"],
                ["EncodingDbfFile", "CodePage"]
            ]
        for (var i = 0; i < controlNames.length; i++) {
            if (exportSettings[controlNames[i][0]] != null) {
                exportSettings[controlNames[i][1]] = exportSettings[controlNames[i][0]];
                delete exportSettings[controlNames[i][0]];
            }
        }

        return exportSettings;
    }

    return exportForm;
}

StiMobileViewer.prototype.ExportControlNames = {
    //Save Document
    SaveDocument: ["SavingReportGroup", "SaveReportMdc", "SaveReportMdz", "SaveReportMdx", "SaveReportPassword"],
    //Pdf
    StiPdfExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageResolution", "ImageResolutionMode", "ImageCompressionMethod",
        "ImageQuality", /*"StandardPdfFonts",*/ "EmbeddedFonts", /*"UseUnicode", "Compressed",*/ "ExportRtfTextAsImage", "PdfACompliance", "DocumentSecurityButton", "DigitalSignatureButton",
        "OpenAfterExport", "AllowEditable", "PasswordInputUser", "PasswordInputOwner", "PrintDocument", "ModifyContents", "CopyTextAndGraphics",
        "AddOrModifyTextAnnotations", "KeyLength", "UseDigitalSignature", "GetCertificateFromCryptoUI", "SubjectNameString"],
    //Xps
    StiXpsExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageResolution", "ImageQuality", "OpenAfterExport",
        "ExportRtfTextAsImage"],
    //Power Point 2007
    StiPpt2007ExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageResolution", "ImageQuality"],
     //HTML
    StiHtmlExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "HtmlType", "Zoom", "ImageFormatForHtml",
        "ExportMode", "UseEmbeddedImages", "AddPageBreaks", "CompressToArchive", "OpenAfterExport"],
    //HTML5
    StiHtml5ExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "HtmlType", "ImageFormatForHtml", "ImageResolution",
        "ImageQuality", "ContinuousPages", "CompressToArchive", "OpenAfterExport"],
    //Mht
    StiMhtExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "HtmlType", "Zoom", "ImageFormatForHtml",
        "ExportMode", "AddPageBreaks", "CompressToArchive"],
    //Txt
    StiTxtExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "KillSpaceLines",
        "PutFeedPageCode", "DrawBorder", "CutLongLines", "BorderType", "ZoomX", "ZoomY", "EncodingTextOrCsvFile"],
    //Rtf
    StiRtfExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageResolution",
        "ImageQuality", "ExportModeRtf", "UsePageHeadersAndFooters", "RemoveEmptySpaceAtBottom"],
    //Word 2007
    StiWord2007ExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageResolution",
         "ImageQuality", "UsePageHeadersAndFooters", "RemoveEmptySpaceAtBottom", "RestrictEditing"],
    //Odt
    StiOdtExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageResolution",
        "ImageQuality", "RemoveEmptySpaceAtBottom"],
    //Excel
    StiExcelExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ExcelType", "ImageResolution",
        "ImageQuality", "ExportDataOnly", "ExportObjectFormatting", "UseOnePageHeaderAndFooter", "ExportEachPageToSheet", "ExportPageBreaks"],
    //ExcelXml
    StiExcelXmlExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ExcelType"],
    //Excel2007
    StiExcel2007ExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ExcelType", "ImageResolution",
        "ImageQuality", "ExportDataOnly", "ExportObjectFormatting", "UseOnePageHeaderAndFooter", "ExportEachPageToSheet", "ExportPageBreaks"],
    //Ods
    StiOdsExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageResolution",
        "ImageQuality"],
    //Bmp
    StiBmpExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageType", "Zoom", "ImageResolution",
        "ImageFormat", "DitheringType", "TiffCompressionScheme", "CutEdges"],
    //Gif
    StiGifExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageType", "Zoom", "ImageResolution",
        "ImageFormat", "DitheringType", "TiffCompressionScheme", "CutEdges"],
    //Jpeg
    StiJpegExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageType", "Zoom", "ImageResolution",
        "ImageFormat", "DitheringType", "TiffCompressionScheme", "CutEdges"],
    //Pcx
    StiPcxExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageType", "Zoom", "ImageResolution",
        "ImageFormat", "DitheringType", "TiffCompressionScheme", "CutEdges"],
    //Png
    StiPngExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageType", "Zoom", "ImageResolution",
        "ImageFormat", "DitheringType", "TiffCompressionScheme", "CutEdges"],
    //Tiff
    StiTiffExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageType", "Zoom", "ImageResolution",
        "ImageFormat", "DitheringType", "TiffCompressionScheme", "CutEdges"],    
    //Emf
    StiEmfExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageType", "Zoom", "ImageResolution",
        "ImageFormat", "DitheringType", "TiffCompressionScheme", "CutEdges"],
    //Svg
    StiSvgExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageType", "Zoom", "ImageResolution",
        "ImageFormat", "DitheringType", "TiffCompressionScheme", "CutEdges"],
    //Svgz
    StiSvgzExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "ImageType", "Zoom", "ImageResolution",
        "ImageFormat", "DitheringType", "TiffCompressionScheme", "CutEdges"],
    //Csv
    StiCsvExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "DataType", "EncodingTextOrCsvFile",
        "Separator", "SkipColumnHeaders", "DataExportMode"],
    //Dbf
    StiDbfExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "DataType", "EncodingDbfFile"],
    //Dif
    StiDifExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "DataType", "ExportDataOnly",
        "UseDefaultSystemEncoding", "EncodingDifFile"],
    //Sylk
    StiSylkExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "DataType", "ExportDataOnly",
        "UseDefaultSystemEncoding", "EncodingDifFile"],
    //Xml
    StiXmlExportSettings: ["PageRangeGroup", "PageRangeAll", "PageRangeCurrentPage", "PageRangePages", "PageRangePagesText", "SettingsGroup", "DataType", "DataExportMode"]
}

StiMobileViewer.prototype.GetCommonExportFormat = function (format) {
    if (format == "StiHtmlExportSettings" || format == "StiHtml5ExportSettings" || format == "StiMhtExportSettings") return "Html";
    if (format == "StiExcelExportSettings" || format == "StiExcel2007ExportSettings" || format == "StiExcelXmlExportSettings") return "Excel";
    if (format == "StiCsvExportSettings" || format == "StiDbfExportSettings" || format == "StiXmlExportSettings" ||
        format == "StiDifExportSettings" || format == "StiSylkExportSettings") return "Data";
    if (format == "StiBmpExportSettings" || format == "StiGifExportSettings" || format == "StiJpegExportSettings" ||
        format == "StiPcxExportSettings" || format == "StiPngExportSettings" || format == "StiTiffExportSettings" ||
        format == "StiEmfExportSettings" || format == "StiSvgExportSettings" || format == "StiSvgzExportSettings") return "Image";

    return format
}

StiMobileViewer.prototype.DropDownListForExportForm = function (name, width, toolTip, items, readOnly, showImage) {
    var dropDownList = this.DropDownList(name, width, toolTip, items, readOnly, showImage);

    dropDownList.onChange = function () { };

    dropDownList.setKey = function (key) {
        dropDownList.key = key;
        dropDownList.onChange();
        for (var itemName in dropDownList.items)
            if (key == dropDownList.items[itemName].key) {
                this.textBox.value = dropDownList.items[itemName].caption;
                if (dropDownList.image) dropDownList.image.style.background = "url(" + dropDownList.jsObject.options.images[dropDownList.items[itemName].imageName] + ")";
                return;
            }
        dropDownList.textBox.value = key.toString();
    }
    if (dropDownList.menu) {
        dropDownList.menu.action = function (menuItem) {
            this.changeVisibleState(false);
            this.dropDownList.key = menuItem.key;
            this.dropDownList.textBox.value = menuItem.caption.innerHTML;
            if (this.dropDownList.image) this.dropDownList.image.style.background = "url(" + this.jsObject.options.images[menuItem.imageName] + ")";
            this.dropDownList.onChange();
            this.dropDownList.action();
        }
    }

    return dropDownList;
}
