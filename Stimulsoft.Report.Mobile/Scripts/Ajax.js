﻿
//Error
StiMobileViewer.prototype.errorFromServer = function (args, context) {
}

//Receive
StiMobileViewer.prototype.receveFromServer = function (args, context) {
    var params;
    try {
        params = JSON.parse(args);
    }
    catch (e) {
        return;
    }

    var jsObject = eval("js" + params["mobileViewerId"]);

    //Error Message
    if (params["errorMessage"] != null) {
        if (jsObject.options.bigProgressBar) jsObject.options.bigProgressBar.hide();
        jsObject.options.processImage.hide();
        jsObject.options.forms.errorMessageForm.show(params["errorMessage"]);
    }

    if (params["cacheReportGuid"] != null) jsObject.options.cacheReportGuid = params["cacheReportGuid"];

    if (params["command"] == "CustomCallBack") {
        jsObject.options.processImage.hide();
    }

    //Callback Function
    if (params["callbackFunctionGuid"] != null) {
        jsObject.callbackFunctions[params["callbackFunctionGuid"]](params);
        delete jsObject.callbackFunctions[params["callbackFunctionGuid"]];
        return;
    }

    //Get Export Settings For Server
    if (params["command"] == "GetExportSettingsForServer") {
        jsObject.options.processImage.hide();
        if (jsObject.helperObjectForReportsServer) {
            jsObject.helperObjectForReportsServer.exportButton.exportSettings = params["exportSettings"] ? JSON.parse(params["exportSettings"]) : null;
            if (jsObject.helperObjectForReportsServer.exportSettingsForm) {
                jsObject.helperObjectForReportsServer.exportSettingsForm.container.progressMarker.changeVisibleState(false);
                jsObject.helperObjectForReportsServer.exportSettingsForm.changeVisibleState(false);
            }
        }
        return;
    }

    //Print
    if (params["command"] == "PrintDirect" || params["command"] == "PrintWithPreview") {
        jsObject.options.pagesArrayForPrint = params["pagesArray"];
        jsObject.options.processImage.hide();
        var printFrame = jsObject.options.printFrame || jsObject.InitializePrintFrame();
        if (params["command"] == "PrintWithPreview")
            printFrame.printWithPreview();
        else
            printFrame.printDirect();
        return;
    }

    //Get Report From File or Metric
    if (params["command"] == "LoadReportFromFile" ||
        params["command"] == "LoadReportSnapshotFromMetric" ||
        params["command"] == "LoadReportFromCache" ||
        params["command"] == "LoadReportToViewer")
    {
        //Clear pages loaded status and reload visible pages        
        jsObject.options.reportPagesLoadedStatus = {};

        //Set Pagination
        if (jsObject.options.cloudParameters)
            jsObject.options.cloudParameters.pagination = params["pagination"];

        if (params["pagination"])
            jsObject.options.reportPanel.loadVisiblePagesContent();

        //Initialize bookmarks
        jsObject.options.paramsBookmarks = params["paramsBookmarks"];
        jsObject.InitializeBookmarksPanel();

        //Initialize variables
        if (!jsObject.options.cloudMode) {
            jsObject.options.paramsVariables = params["paramsVariables"];
            jsObject.InitializeParametersPanel();
        }

        //Find panel
        if (jsObject.options.findPanel)
            jsObject.options.findPanel.changeVisibleState(false);

        //Editable fields
        if (jsObject.options.buttons.Editor)
            jsObject.options.buttons.Editor.style.display = params["isEditableReport"] ? "" : "none";

        //Toolbar
        if (jsObject.options.toolbar)
            jsObject.options.toolbar.setEnabledMainButtons(true);

        if (params["notLoadAllPages"]) {
            jsObject.options.notLoadAllPages = true;
            return;
        }
    }

    //Apply navigation parameters    
    if (params["pageNumber"] != null) jsObject.options.pageNumber = params["pageNumber"];
    if (params["pagesCount"] != null && !jsObject.options.notLoadAllPages) jsObject.options.pagesCount = params["pagesCount"];
    if (params["zoom"] != null) jsObject.options.zoom = params["zoom"];
    if (params["pagesArray"]) jsObject.options.pagesArray = params["pagesArray"];
    if (params["viewMode"] != null) jsObject.options.menuViewMode = params["viewMode"];

    //Add pages to report panel
    if (params["pagesArray"]) {
        jsObject.options.reportPanel.addPages();
    }

    //Change toolbar
    if (jsObject.options.toolbar) jsObject.options.toolbar.changeToolBarState();
    if (jsObject.options.isMaterial && jsObject.options.materialPanel) jsObject.options.materialPanel.changeToolBarState();    

    //Go to the bookmark, if it present
    if (jsObject.options.bookmarkAnchor != null) {
        if (jsObject.options.cloudMode && !jsObject.options.reportPagesLoadedStatus[jsObject.options.pageNumber]) return;
        jsObject.scrollToAnchor(jsObject.options.bookmarkAnchor, jsObject.options.componentGuid);
        jsObject.options.bookmarkAnchor = null;
        jsObject.options.componentGuid = null;
    }

    //Show find labels if findMode enabled
    if (jsObject.options.findMode && jsObject.options.findPanel && params["pagesArray"]) {
        jsObject.showFindLabels(jsObject.options.findPanel.controls.findTextBox.value);
    }

    //Hide load progress
    jsObject.options.processImage.hide();    
}

StiMobileViewer.prototype.sendToServer = function (command, aditionalParams, callbackFunction, notProgress) {
    //Show load progress
    if (!notProgress) this.options.processImage.show();

    //Send parameters
    var params = {};
    params.command = command;
    params.cacheReportGuid = this.options.cacheReportGuid;

    //Callback Function
    if (callbackFunction) {
        params.callbackFunctionGuid = this.newGuid();
        this.callbackFunctions[params.callbackFunctionGuid] = callbackFunction;
    }

    //Editable Params
    if (this.options.editableParameters)
        params.editableParameters = this.options.editableParameters;

    //Aditional Params
    for (var key in aditionalParams) {
        params[key] = aditionalParams[key];
    }

    //Execute ajax function
    var actionFunction = this.options.callbackFunction.replace("callbackParams", Base64.encode(JSON.stringify(params)));
    eval(actionFunction);
    document.getElementById("__EVENTARGUMENT").value = "";
}

StiMobileViewer.prototype.sendMainParameters = function (command) {
    if (this.options.pagesArray == null && !this.options.cloudMode) return;
        
    var params = {};
    params.cacheReportGuid = this.options.cacheReportGuid;
    params.pageNumber = this.options.pageNumber;
    params.zoom = this.options.zoom;
    params.viewmode = this.options.menuViewMode;
    params.variables = (command == "SubmitVariables") ? this.options.parametersPanel.getParametersValues() : null;
    if (this.options.editableParameters) params.editableParameters = this.options.editableParameters;


    this.sendToServer(command, params);
}

StiMobileViewer.prototype.exportEvent = function (command, exportSettings) {
    if (this.options.pagesArray == null) return;

    var attributes = {};
    attributes.command = command;
    if (exportSettings) attributes.exportSettings = exportSettings;
    attributes.cacheReportGuid = this.options.cacheReportGuid;
    if (this.options.editableParameters) attributes.editableParameters = this.options.editableParameters;

    if (this.options.designerMode && this.options.jsDesigner)
        this.options.jsDesigner.options.ignoreBeforeUnload = true;

    var actionFunction = this.options.callbackFunctionForExport.replace("callbackParams", JSON.stringify(attributes));
    eval(actionFunction);
    document.getElementById("__EVENTARGUMENT").value = "";

    if (this.options.designerMode && this.options.jsDesigner)
        this.options.jsDesigner.options.ignoreBeforeUnload = false;
}

StiMobileViewer.prototype.printToPdf = function () {
    var win = window.open();
    var doc = (win) ? (win.document) : document;
    var params = { command: "PrintToPdf" };
    if (this.options.cacheReportGuid) {
        params.cacheReportGuid = this.options.cacheReportGuid;
    }
    if (this.options.editableParameters) {
        params.editableParameters = JSON.stringify(this.options.editableParameters);
    }

    if (this.options.designerMode && this.options.jsDesigner)
        this.options.jsDesigner.options.ignoreBeforeUnload = true;

    this.postForm(params, doc, this.options.requestUrl);

    if (this.options.designerMode && this.options.jsDesigner)
        this.options.jsDesigner.options.ignoreBeforeUnload = false;
}