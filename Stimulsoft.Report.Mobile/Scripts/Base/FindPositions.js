
StiMobileViewer.prototype.FindPosX = function (obj, mainClassName, noScroll) {
    var curleft = noScroll ? 0 : this.GetScrollXOffset(obj, mainClassName);
    if (obj.offsetParent) {
        while (obj.className != mainClassName) {            
            curleft += obj.offsetLeft;
            if (!obj.offsetParent) {
                break;
            }
            obj = obj.offsetParent;
            
        }
    } else if (obj.x) {
        curleft += obj.x;
    }
    return curleft;
}

StiMobileViewer.prototype.FindPosY = function (obj, mainClassName, noScroll) {
    var curtop = noScroll ? 0 : this.GetScrollYOffset(obj, mainClassName);
    if (obj.offsetParent) {
        while (obj.className != mainClassName) {
            curtop += obj.offsetTop;
            if (!obj.offsetParent) {
                break;
            }
            obj = obj.offsetParent;
        }
    } else if (obj.y) {
        curtop += obj.y;
    }
    return curtop;
}

StiMobileViewer.prototype.GetScrollXOffset = function (obj, mainClassName) {
    var scrollleft = 0;
    if (obj.parentElement) {
        while (obj.className != mainClassName) {
            if ("scrollLeft" in obj) { scrollleft -= obj.scrollLeft }
            if (!obj.parentElement) {
                break;
            }
            obj = obj.parentElement;
        }
    }
    
    return scrollleft;
}

StiMobileViewer.prototype.GetScrollYOffset = function (obj, mainClassName) {
    var scrolltop = 0;
    if (obj.parentElement) {
        while (obj.className != mainClassName) {
            if ("scrollTop" in obj) { scrolltop -= obj.scrollTop }
            if (!obj.parentElement) {
                break;
            }
            obj = obj.parentElement;
        }
    }
    
    return scrolltop;
}