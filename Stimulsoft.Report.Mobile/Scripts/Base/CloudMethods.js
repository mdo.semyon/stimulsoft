﻿
StiMobileViewer.prototype.LoadReportOnStart = function () {
    var jsObject = this;
    
    window.onload = function () {
        var cloudParameters = jsObject.options.cloudParameters;
        if (!cloudParameters.shareKey) jsObject.options.processImage.show();

        if (cloudParameters.reportSnapshotItemKey) {
            jsObject.options.reportRenderComplete = true;
            jsObject.LoadReportSnapshotItemToViewer(cloudParameters.reportSnapshotItemKey, cloudParameters.versionKey);
        }
        else if (cloudParameters.reportTemplateItemKey) {
            jsObject.LoadReportTemplateItemToViewer(cloudParameters.reportTemplateItemKey, cloudParameters.versionKey);
        }
        else if (cloudParameters.shareKey) {
            var authForm = jsObject.InitializeAuthForm();
            authForm.getShareInfo();
        }
    }
}

StiMobileViewer.prototype.HideParametersAndBookmarksPanels = function () {
    this.options.paramsVariables = null;
    this.InitializeParametersPanel();
    this.options.paramsBookmarks = null;
    this.InitializeBookmarksPanel();
}

//Load Report Snapshot Item To Viewer
StiMobileViewer.prototype.LoadReportSnapshotItemToViewer = function (itemKey, versionKey, notProgress) {
    var jsObject = this;

    // for sharing
    jsObject.options.toolbar.style.display = "";
    jsObject.options.navigatePanel.style.display = "";

    jsObject.options.cloudParameters.versionKey = versionKey;
    jsObject.options.cloudParameters.reportSnapshotItemKey = itemKey;
    
    jsObject.sendToServer("LoadReportSnapshotFromMetric", jsObject.options.cloudParameters, null, notProgress);
}

//Load Report Template Item To Viewer
StiMobileViewer.prototype.LoadReportTemplateItemToViewer = function (itemKey, versionKey) {
    var jsObject = this;
    
    // for sharing
    jsObject.options.toolbar.style.display = "";
    jsObject.options.navigatePanel.style.display = "";
    jsObject.options.notAnimatedPages = {};

    //write report params
    jsObject.options.cloudParameters.reportSnapshotItemKey = jsObject.generateKey();
    jsObject.options.cloudParameters.versionKey = versionKey;
    jsObject.options.reportRenderTaskKey = jsObject.generateKey();    
    jsObject.options.reportRenderComplete = false;
    jsObject.options.firstPageComplete = false;
    if (jsObject.options.toolbar) jsObject.options.toolbar.setEnabledMainButtons(false);

    var funcGetSignalReportProgress = function (data) { 
        if (jsObject.options.reportRenderTaskKey == data.TaskKey) {
            jsObject.options.smallProgressBar.animateProgress(data.Progress);
            jsObject.options.cloudParameters.pagination = true;
            
            if (data.Metrics && data.Metrics.length > 0) {
                setTimeout(function () {
                    for (var i = 0; i < data.Metrics.length; i++) {
                        var startIndex = data.Metrics[i].StartIndex || 0;

                        var pageAttributes = {
                            sizes: data.Metrics[i].Width + ";" + data.Metrics[i].Height,
                            margins: "0px 0px 0px 0px",
                            background: "#ffffff",
                            content: ""
                        }
                        for (var pageIndex = startIndex; pageIndex <= data.Metrics[i].EndIndex; pageIndex++) {
                            var page = jsObject.options.reportPanel.addPage(pageAttributes);
                            page.style.width = parseInt(data.Metrics[i].Width * 0.72) + "pt";
                            page.style.height = parseInt(data.Metrics[i].Height * 0.72) + "pt";
                        }
                    }

                    jsObject.options.pagesCount = data.TotalPageCount <= 0 ? 1 : data.TotalPageCount;
                    if (jsObject.options.toolbar) jsObject.options.toolbar.changeToolBarState();
                    if (jsObject.options.materialPanel) jsObject.options.materialPanel.changeToolBarState();
                    jsObject.options.reportPanel.loadVisiblePagesContent();
                }, 50);
            }
        }
    }        

    var funcGetSignalReportComplete = function (data) {
        if (jsObject.options.reportRenderTaskKey == data.TaskKey) {
            jsObject.options.reportRenderComplete = true;
            jsObject.options.smallProgressBar.hide(function () {
                if (!data.ResultNotice)
                    jsObject.LoadReportSnapshotItemToViewer(jsObject.options.cloudParameters.reportSnapshotItemKey, null, true);
           });
        }
    }

    var completeFuncReportView = function (data) {
        if (data.ResultCachedSnapshotKey) {
            jsObject.options.reportRenderComplete = true;
            jsObject.LoadReportSnapshotItemToViewer(data.ResultCachedSnapshotKey);
            return;
        }

        jsObject.options.smallProgressBar.show();

        if (data.ResultFirstPage && !jsObject.options.reportRenderComplete && !jsObject.options.firstPageComplete) {
            var params = {
                reportSnapshotItemKey: jsObject.options.cloudParameters.reportSnapshotItemKey,
                sessionKey: jsObject.options.cloudParameters.sessionKey,
                taskKey: jsObject.options.reportRenderTaskKey,
                resultFirstPage: data.ResultFirstPage
            }
            jsObject.options.firstPageComplete = true;
            jsObject.LoadFirstPage(params);
        }
    }

    if (!this.options.reportSignalsAdded) {
        this.options.controller.addListener(funcGetSignalReportProgress, "ReportRunProgress");
        this.options.controller.addListener(funcGetSignalReportComplete, "ReportRunComplete");
        this.options.reportSignalsAdded = true;
    }


    if (!itemKey) {        
        var params = jsObject.options.paramsReportView;
        params.TaskKey = jsObject.options.reportRenderTaskKey;
        params.ReportSnapshotItemKey = jsObject.options.cloudParameters.reportSnapshotItemKey;

        params.Parameters = [];
        if (jsObject.options.parametersPanel) {
            params.Parameters = jsObject.options.parametersPanel.getParametersValuesForReportServer();
            var query = jsObject.GetUrlQueryString();
            for (var param in query) params.Parameters.push({ Ident: "Single", Type: "string", Name: param, Value: query[param] });
        }
        
        jsObject.SendCommand("ReportView", params, completeFuncReportView);
    }
    else {
        //Get Variables
        var paramsGetVariables = {};
        paramsGetVariables.reportTemplateItemKey = itemKey;
        paramsGetVariables.sessionKey = jsObject.options.cloudParameters.sessionKey;

        jsObject.sendToServer("GetVariables", paramsGetVariables, function (data) {
            var paramsReportView = {};
            paramsReportView.ReportSnapshotItemKey = jsObject.options.cloudParameters.reportSnapshotItemKey;
            paramsReportView.ReportTemplateItemKey = itemKey;
            paramsReportView.TaskKey = jsObject.options.reportRenderTaskKey;
            paramsReportView.UserKey = jsObject.options.cloudParameters.userKey;
            paramsReportView.SessionKey = jsObject.options.cloudParameters.sessionKey;
            jsObject.options.paramsReportView = paramsReportView;
            jsObject.options.cachedReports[itemKey] = paramsReportView.ReportSnapshotItemKey;

            if (data["paramsVariables"]) {
                //Contains request form user variables
                jsObject.options.processImage.hide();
                jsObject.options.paramsVariables = data["paramsVariables"];
                jsObject.InitializeParametersPanel();
                jsObject.options.reportRenderComplete = true;

                if (data["resultRequestParameters"] === false) {
                    var params = jsObject.options.paramsReportView;
                    params.TaskKey = jsObject.options.reportRenderTaskKey;
                    params.ReportSnapshotItemKey = jsObject.options.cloudParameters.reportSnapshotItemKey;
                    params.Parameters = [];
                    jsObject.SendCommand("ReportView", params, completeFuncReportView);
                }
            }
            else {
                //Does not contain request form user variables 
                var params = [];
                var query = jsObject.GetUrlQueryString();
                for (var param in query) params.push({ Ident: "Single", Type: "string", Name: param, Value: query[param] });
                if (jsObject.options.cloudParameters.postParams) {
                    for (var param in jsObject.options.cloudParameters.postParams)
                        params.push({ Ident: "Single", Type: "string", Name: param, Value: jsObject.options.cloudParameters.postParams[param] });
                }
                if (params.length > 0) paramsReportView.Parameters = params;
                if (versionKey) paramsReportView.VersionKey = versionKey;
                jsObject.SendCommand("ReportView", paramsReportView, completeFuncReportView);
            }
        });
    }
}

//First Page Signal
StiMobileViewer.prototype.InitializeCloudReportFirstPageSignal = function () {
    var jsObject = this;

    var funcSignalFirstPageReady = function (data) {
        if (data && data.UserKey == data.OwnerUserKey && jsObject.options.reportRenderTaskKey == data.TaskKey &&
            !jsObject.options.reportRenderComplete && !jsObject.options.firstPageComplete) {

            var params = {
                reportSnapshotItemKey: data.ReportSnapshotItemKey,
                sessionKey: jsObject.options.cloudParameters.sessionKey,
                versionKey: jsObject.options.cloudParameters.versionKey,
                taskKey: jsObject.options.reportRenderTaskKey
            }
            jsObject.options.firstPageComplete = true;
            jsObject.LoadFirstPage(params);
        }
    }
    this.options.controller.addListener(funcSignalFirstPageReady, "ReportFirstPageReady");
}

StiMobileViewer.prototype.LoadFirstPage = function (params) {
    var jsObject = this;
    jsObject.options.reportPagesLoadedStatus[0] = true;
    
    jsObject.sendToServer("LoadFirstPage", params, function (data) {        
        var firstPageContent = data["firstPageContent"];
        var firstPage = jsObject.options.reportPanel.getPageByIndex(0);
        if (!firstPage) firstPage = jsObject.options.reportPanel.addPage(firstPageContent[0]);
        firstPage.setSelected();        
        if (firstPageContent) {
            firstPage.loadContent(firstPageContent);
        }
        jsObject.options.processImage.hide();
    });
}

StiMobileViewer.prototype.InitializeCloudBookmarksSignals = function () {
    var taskKey;
    var jsObject = this;
    var bookmarkAnchor_;
    var componentGuid_;

    this.findReportBookmark = function (bookmarkAnchor, componentGuid) {
        taskKey = jsObject.generateKey();
        bookmarkAnchor_ = bookmarkAnchor;
        componentGuid_ = componentGuid;

        var param = {
            VersionKey: jsObject.options.cloudParameters.versionKey,
            ReportSnapshotItemKey: jsObject.options.cloudParameters.reportSnapshotItemKey,
            TaskKey: taskKey,
            ResultSuccess: true,
            Options: {
                Text: bookmarkAnchor,
                ComponentGuid: componentGuid
            }
        };

        jsObject.SendCommand("ReportFindBookmark", param, function (data) {
            jsObject.options.bigProgressBar.show(jsObject.loc.PropertyCategory.NavigationCategory, taskKey);
        },
        function (data) {
            jsObject.options.bigProgressBar.hide();
            var msg = jsObject.options.controller.formatResultMsg(data);
            jsObject.options.processImage.hide();
            jsObject.options.forms.errorMessageForm.show(msg);
        });
    }

    //Bookmark Progress
    var funcSignalFindBookmarkProgress = function (data) {
        if (data && taskKey == data.TaskKey) {
            jsObject.options.bigProgressBar.animateProgress(data.Progress, 50);
        }
    }

    //Bookmark Complete
    var funcSignalFindBookmarkComplete = function (data) {
        if (data && taskKey == data.TaskKey) {
            jsObject.options.bigProgressBar.hide(function () {
                if (data.ResultPageIndex) {
                    jsObject.actionEvent("BookmarkAction", data.ResultPageIndex, bookmarkAnchor_, componentGuid_);
                }
            });
        }
    }

    this.options.controller.addListener(funcSignalFindBookmarkProgress, "ReportFindBookmarkProgress");
    this.options.controller.addListener(funcSignalFindBookmarkComplete, "ReportFindBookmarkComplete");
}

//Export Report
StiMobileViewer.prototype.InitializeCloudExportSignals = function () {
    var taskKey;
    var fileItemKey;
    var jsObject = this;
    var currentExportFormat;
    var emailParameters;

    this.exportReport = function (exportFormat, exportSettings, emailParameters_, showAfterExportInNewWindow, printAfterExport) {
        taskKey = jsObject.generateKey();
        fileItemKey = jsObject.generateKey();
        currentExportFormat = exportFormat;
        emailParameters = emailParameters_;
        jsObject.options.showAfterExportInNewWindow = showAfterExportInNewWindow;

        var params = {
            versionKey: jsObject.options.cloudParameters.versionKey,
            reportSnapshotItemKey: jsObject.options.cloudParameters.reportSnapshotItemKey,
            taskKey: taskKey,
            userKey: jsObject.options.cloudParameters.userKey,
            sessionKey: jsObject.options.cloudParameters.sessionKey,
            exportFormat: exportFormat,
            exportSettings: exportSettings,
            fileItemKey: fileItemKey,
            printAfterExport: printAfterExport
        };
        
        jsObject.options.bigProgressBar.show(jsObject.loc.Report.PreparingReport, taskKey);
        jsObject.sendToServer("ReportExport", params);
        jsObject.options.processImage.hide();
    }

    var funcSignalExportProgress = function (data) {
        if (data && taskKey == data.TaskKey) {
            jsObject.options.bigProgressBar.animateProgress(data.Progress, 50);
        }
    }

    var funcSignalExportComplete = function (data) {
        if (data && taskKey == data.TaskKey) {
            var resultSuccess = data.ResultSuccess;

            //Set public share for big files which sending by email
            if (emailParameters) {
                jsObject.SendCommand("ItemSetShareInfo", {
                    ItemKeys: [jsObject.options.cloudParameters.reportSnapshotItemKey, fileItemKey],
                    ShareLevel: "Public",
                    AllowSignalsReturn: true,
                    ShareExpires: jsObject.DateToJSONDateFormat(new Date(new Date().getTime() + (7 * 24 * 3600 * 1000)))
                });
            }

            jsObject.options.bigProgressBar.hide(function () {
                if (resultSuccess) {
                    if (emailParameters) {
                        emailParameters.AttachedItemKeys.push(fileItemKey);
                        jsObject.SendCommand("ItemSendByEmail", emailParameters,
                        function (data) { },
                        function (data) {
                            var msg = jsObject.options.controller.formatResultMsg(data);
                            jsObject.options.processImage.hide();
                            jsObject.options.forms.errorMessageForm.show(msg);
                        });
                    }
                    else {
                        var command = {
                            Ident: "ItemResourceGet",
                            ItemKey: fileItemKey,
                            SessionKey: jsObject.options.cloudParameters.sessionKey
                        }

                        var param = JSON.stringify(command);
                        var data = { fileName: jsObject.GetFullFileNameByExportFormat(jsObject.GetCurrentReportName(), currentExportFormat), param: param };

                        if (jsObject.options.showAfterExportInNewWindow) {
                            var win = window.open("about:blank", '_blank');
                            var doc = (win) ? (win.document) : document;
                            data.fileType = "Pdf";

                            jsObject.options.controller.exec("service/file/testdownload", command, function () {
                                jsObject.postForm(data, doc, jsObject.options.cloudParameters.restUrl + "service/file/view/" + jsObject.generateKey());
                            });
                        }
                        else {
                            jsObject.options.controller.exec("service/file/testdownload", command, function () {
                                jsObject.postForm(data, doc, jsObject.options.cloudParameters.restUrl + "service/file/download/" + jsObject.generateKey());
                            });
                        }
                    }
                }
            });
        }
    }

    this.options.controller.addListener(funcSignalExportProgress, "ReportExportProgress");
    this.options.controller.addListener(funcSignalExportComplete, "ReportExportComplete");
}

StiMobileViewer.prototype.GetCurrentReportName = function () {
    var name = this.options.cloudParameters.reportName ? Base64.decode(this.options.cloudParameters.reportName) : "file";
    if (name.toLowerCase().endsWith(".mrt") ||
        name.toLowerCase().endsWith(".mrz") ||
        name.toLowerCase().endsWith(".mrx") ||
        name.toLowerCase().endsWith(".mdc") ||
        name.toLowerCase().endsWith(".mdz") ||
        name.toLowerCase().endsWith(".mdx")) {
        name = name.substring(0, name.length - 4);
    }

    return name;
}

StiMobileViewer.prototype.GetUrlQueryString = function () {
    var a = window.location.search.substr(1).split('&');
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i) {
        var p = a[i].split('=', 2);
        if (p.length == 1)
            b[p[0]] = "";
        else
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
}

StiMobileViewer.prototype.StopRenderReportTask = function () {
    var jsObject = this;
    if (jsObject.options.reportRenderTaskKey) {
        jsObject.options.reportStoppedByUser = true;

        jsObject.SendCommand("TaskStop", {
            TaskKey: jsObject.options.reportRenderTaskKey,
            ResultSuccess: true
        }, function (data) {
            jsObject.options.smallProgressBar.style.display = "none";
        });
    }
}