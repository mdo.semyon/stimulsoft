﻿
StiMobileViewer.prototype.InitializeController = function () {
    var controller = new Object();
    this.options.controller = controller;
    controller.queue = [];
    controller.uploadQueue = [];
    controller.isExecuting = false;
    controller.jsObject = this;
    controller.listeners = {};
    controller.signals = {};
    var WAIT_INTERVAL = 700;
    var MAX_INTERVAL = 3000;
    controller.delayTime = 0;
    controller.shortDelayTime = 10;
    controller.longDelayTime = 50;
    controller.switchToLongDelayTime = 600;
    controller.time = 0;
    controller.isResetting = false;
    controller.isLongTime = false;
    var this_ = this;
    
    controller.resetSingals = function () {
        controller.isResetting = true;
        controller.time = 0;
        controller.isLongTime = false;
    }
    
    controller.processSignals = function (data) {
        for (var i in data.ResultSignals) {
            var signal = data.ResultSignals[i];
            if (controller.listeners[signal.Ident]) {
                if (signal.UserKey == signal.OwnerUserKey && signal.UserKey && signal.UserKey != "") {
                    controller.resetSingals();
                }
                for (var j in controller.listeners[signal.Ident]) {
                    controller.listeners[signal.Ident][j](signal);
                }
            }
        }
        controller.updateSignals();
    }

    controller.updateSignals = function () {
        setTimeout(function () {
            if (controller.isResetting || (controller.isLongTime ? controller.delayTime >= controller.longDelayTime : controller.delayTime >= controller.shortDelayTime)) {
                controller.isResetting = false;
                controller.delayTime = 0;
                if (this_.options.cloudParameters.sessionKey) {                    
                    controller.exec("1/runcommand", { Ident: "SignalFetchAll" },
                        controller.processSignals, function (data, msg) {
                            console.error(msg);
                            controller.updateSignals();
                        });
                }
            } else {
                controller.delayTime++;
                controller.time++;
                if (controller.time > controller.switchToLongDelayTime) {
                    controller.isLongTime = true;
                }
                controller.updateSignals();
            }
        }, 100);
    }

    controller.exec = function (command, param, success, failure, dataType, allowSignal) {
        if (param && param.Ident == "SignalFetchAll" && this.queue.length > 0 && this.queue[this.queue.length - 1].param == param) {
            return;
        }
        this.queue[this.queue.length] = {
            command: command,
            param: param,
            success: success,
            failure: failure,
            allowSignal: allowSignal,
            dataType: dataType ? dataType : "json"
        };
        if (!this.isExecuting) this.run();
    };

    controller.run = function () {
        if (this.queue.length > 0) {
            this.isExecuting = true;
            this.selfExec(this.queue[0], this.next);
        }
    };

    controller.selfExec = function (qData, next) {
        var data = qData.param;
        if (this.jsObject.options.cloudParameters.sessionKey) data.SessionKey = this.jsObject.options.cloudParameters.sessionKey;
        if (this.jsObject.options.cloudParameters.userKey) data.UserKey = this.jsObject.options.cloudParameters.userKey;
        var success = qData.success;
        var failure = qData.failure;
        var userName = data.UserName;
        var progress = qData.progress;
        var allowSignal = qData.allowSignal;
        var self = qData.self;
        var cId = qData.id;
        var obj = qData.obj;
        var this_ = this;
             
        controller.xhr = $.ajax({
            type: "POST",
            //TODO: timeout?
            url: this.jsObject.options.cloudParameters.restUrl + qData.command + "/" + this.jsObject.generateKey(),
            data: JSON.stringify(data, null, 2),
            contentType: qData.dataType == "text" ? "text/plain; charset=utf-8" : "application/json; charset=utf-8",
            dataType: qData.dataType ? qData.dataType : "json",
            success: function (data, textStatus, jqXHR) {

                //debugger;
                try {
                    if (qData.dataType == "text") {
                        success(data, self, cId);
                    }
                    else if (data.ResultSuccess) {                        
                        if (data.ResultSignals) {
                            for (var i = 0; i < data.ResultSignals.length; i++) {
                                if (data.ResultSignals[i].ResultNotice && data.ResultSignals[i].ResultNotice.CustomMessage) {
                                    this_.jsObject.options.processImage.hide();
                                    if (!this_.jsObject.options.reportStoppedByUser)
                                        this_.jsObject.options.forms.errorMessageForm.show(data.ResultSignals[i].ResultNotice.CustomMessage);
                                }
                            }
                        }
                        success(data, self, cId);
                        if (allowSignal) {
                            controller.resetSingals();
                        }
                    } else {
                        var msg = controller.formatResultMsg(data);                        
                        if (failure) {
                            failure(data, msg, obj);
                        } else {
                            msg = msg || "Unknown error";
                            this_.jsObject.options.processImage.hide();
                            this_.jsObject.options.forms.errorMessageForm.show(msg);
                        }
                    }
                } finally {
                    next(this_);
                }
            },
            error: function (errMsg) {
                if (!controller.aborted) {
                    var responseText = errMsg.responseText ? "Error: " + errMsg.responseText : "Unknown error";
                    this_.jsObject.options.processImage.hide();
                    this_.jsObject.options.forms.errorMessageForm.show(responseText);
                    next(this_);
                } else {
                    controller.aborted = false;
                }
            },
            progress: function (e) {
                if (e.lengthComputable && progress) {
                    progress(e, self, cId);
                }
            }
        });
    }
    
    controller.formatResultMsg = function (data) {
        var msg = "";
        if (data.ResultNotice) {
            var arg = data.ResultNotice.Argument ? data.ResultNotice.Argument : "";
            if (data.ResultNotice.Arguments) {
                for (var i in data.ResultNotice.Arguments) {
                    arg += (arg != "" ? ", " : "") + data.ResultNotice.Arguments[i];
                }
            }
            if (controller.jsObject.loc.Notices && controller.jsObject.loc.Notices[data.ResultNotice.Ident]) {
                return controller.jsObject.formatString(controller.jsObject.loc.Notices[data.ResultNotice.Ident], data.ResultNotice.Arguments ? data.ResultNotice.Arguments : arg);
            }
            if (!data.ResultNotice.Arguments && arg == "") {
                msg = data.ResultNotice[data.ResultNotice.Ident];
                if (!msg) {
                    msg = data.ResultNotice.Ident;
                    if (data.ResultNotice.CustomMessage) msg += ". " + data.ResultNotice.CustomMessage;
                }
            } else {
                msg = controller.jsObject.formatString(data.ResultNotice.Ident, data.ResultNotice.Arguments ? data.ResultNotice.Arguments : arg);
            }
        }
        return msg;
    }
    
    controller.next = function (this_) {
        this_.queue.splice(0, 1);
        if (this_.queue.length > 0) {
            this_.run();
        } else {
            this_.isExecuting = false;
        }
    }

    controller.addListener = function (func, idents) {
        for (var i = 1; i < arguments.length; i++) {
            var ident = arguments[i];
            if (!controller.listeners[ident]) {
                controller.listeners[ident] = [];
            }
            controller.listeners[ident][controller.listeners[ident].length] = func;
        }
    }

    controller.updateSignals();
}

StiMobileViewer.prototype.SendCommand = function (commandName, param, completeFunc, errorFunc) {
    param.Ident = commandName;

    this.options.controller.exec("1/runcommand", param,
        //Success
        function (data) {
            if (completeFunc) completeFunc(data);
        },
        errorFunc, null, true);
}