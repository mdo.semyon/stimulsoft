
StiMobileViewer.prototype.GetImageTypesItems = function () {
    var items = [];
    if (this.options.showExportToBmp) items.push(this.Item("StiBmpExportSettings", "Bmp", null, "Bmp"));
    if (this.options.showExportToGif) items.push(this.Item("StiGifExportSettings", "Gif", null, "Gif"));
    if (this.options.showExportToJpeg) items.push(this.Item("StiJpegExportSettings", "Jpeg", null, "Jpeg"));
    if (this.options.showExportToPcx) items.push(this.Item("StiPcxExportSettings", "Pcx", null, "Pcx"));
    if (this.options.showExportToPng) items.push(this.Item("StiPngExportSettings", "Png", null, "Png"));
    if (this.options.showExportToTiff) items.push(this.Item("StiTiffExportSettings", "Tiff", null, "Tiff"));
    if (this.options.showExportToMetafile) items.push(this.Item("StiEmfExportSettings", "Emf", null, "Emf"));
    if (this.options.showExportToSvg) items.push(this.Item("StiSvgExportSettings", "Svg", null, "Svg"));
    if (this.options.showExportToSvgz) items.push(this.Item("StiSvgzExportSettings", "Svgz", null, "Svgz"));
    
    return items;
}

StiMobileViewer.prototype.GetDataTypesItems = function () {
    var items = [];
    if (this.options.showExportToCsv) items.push(this.Item("StiCsvExportSettings", "Csv", null, "Csv"));
    if (this.options.showExportToDbf) items.push(this.Item("StiDbfExportSettings", "Dbf", null, "Dbf"));
    if (this.options.showExportToXml) items.push(this.Item("StiXmlExportSettings", "Xml", null, "Xml"));
    if (this.options.showExportToDif) items.push(this.Item("StiDifExportSettings", "Dif", null, "Dif"));
    if (this.options.showExportToSylk) items.push(this.Item("StiSylkExportSettings", "Sylk", null, "Sylk"));

    return items;
}

StiMobileViewer.prototype.GetExcelTypesItems = function () {
    var items = [];
    if (this.options.showExportToExcel) items.push(this.Item("StiExcelExportSettings", "Excel 97-2003", null, "ExcelBinary"));
    if (this.options.showExportToExcel2007) items.push(this.Item("StiExcel2007ExportSettings", "Excel", null, "Excel2007"));
    if (this.options.showExportToExcelXml) items.push(this.Item("StiExcelXmlExportSettings", "Excel Xml 2003", null, "ExcelXml"));

    return items;
}

StiMobileViewer.prototype.GetHtmlTypesItems = function () {
    var items = [];
    if (this.options.showExportToHtml) items.push(this.Item("StiHtmlExportSettings", "Html", null, "Html"));
    if (this.options.showExportToHtml5) items.push(this.Item("StiHtml5ExportSettings", "Html5", null, "Html5"));
    if (this.options.showExportToMht) items.push(this.Item("StiMhtExportSettings", "Mht", null, "Mht"));

    return items;
}

StiMobileViewer.prototype.GetZoomItems = function () {
    var items = [];
    var values = [0.25, 0.5, 0.75, 1, 1.25, 1.5, 2];    
    for (var i = 0; i < values.length; i++)
        items.push(this.Item("item" + i, (values[i] * 100) + "%", null, values[i].toString()));

    return items;
}

StiMobileViewer.prototype.GetImageFormatForHtmlItems = function () {
    var items = [];
    items.push(this.Item("item0", "Jpeg", null, "Jpeg"));
    items.push(this.Item("item1", "Gif", null, "Gif"));
    items.push(this.Item("item2", "Bmp", null, "Bmp"));
    items.push(this.Item("item3", "Png", null, "Png"));

    return items;
}

StiMobileViewer.prototype.GetExportModeItems = function () {
    var items = [];
    items.push(this.Item("item0", "Table", null, "Table"));
    items.push(this.Item("item1", "Span", null, "Span"));
    items.push(this.Item("item2", "Div", null, "Div"));

    return items;
}

StiMobileViewer.prototype.GetImageResolutionModeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.Export.Exactly, null, "Exactly"));
    items.push(this.Item("item1", this.loc.Export.NoMoreThan, null, "NoMoreThan"));
    items.push(this.Item("item2", this.loc.Export.Auto, null, "Auto"));

    return items;
}

StiMobileViewer.prototype.GetImageResolutionItems = function () {
    var items = [];
    var values = ["10", "25", "50", "75", "100", "200", "300", "400", "500"];
    for (var i = 0; i < values.length; i++)
        items.push(this.Item("item" + i, values[i], null, values[i]));

    return items;
}

StiMobileViewer.prototype.GetImageCompressionMethodItems = function () {
    var items = [];
    items.push(this.Item("item0", "Jpeg", null, "Jpeg"));
    items.push(this.Item("item1", "Flate", null, "Flate"));

    return items;
}

StiMobileViewer.prototype.GetImageQualityItems = function () {
    var items = [];
    var values = [0.25, 0.5, 0.75, 0.85, 0.9, 0.95, 1];
    for (var i = 0; i < values.length; i++)
        items.push(this.Item("item" + i, (values[i] * 100) + "%", null, values[i].toString()));

    return items;
}

StiMobileViewer.prototype.GetRestrictEditingItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.FormFormatEditor.nameNo, null, "No"));
    items.push(this.Item("item1", this.loc.Export.ExceptEditableFields, null, "ExceptEditableFields"));
    items.push(this.Item("item1", this.loc.FormFormatEditor.nameYes, null, "Yes"));

    return items;
}

StiMobileViewer.prototype.GetBorderTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.Export.TxtBorderTypeSimple, null, "Simple"));
    items.push(this.Item("item1", this.loc.Export.TxtBorderTypeSingle, null, "UnicodeSingle"));
    items.push(this.Item("item2", this.loc.Export.TxtBorderTypeDouble, null, "UnicodeDouble"));

    return items;
}

StiMobileViewer.prototype.GetEncodingDataItems = function () {
    var items = [];
    for (var i = 0; i < this.options.encodingDataCollection.length; i++) {
        var item = this.options.encodingDataCollection[i];
        items.push(this.Item("item" + i, item.value, null, item.key));
    }

    return items;
}

StiMobileViewer.prototype.GetImageFormatItems = function (withoutMonochrome) {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyMain.Color, null, "Color"));
    items.push(this.Item("item1", this.loc.Export.ImageGrayscale, null, "Grayscale"));
    if (!withoutMonochrome) items.push(this.Item("item2", this.loc.Export.ImageMonochrome, null, "Monochrome"));

    return items;
}

StiMobileViewer.prototype.GetMonochromeDitheringTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", "None", null, "None"));
    items.push(this.Item("item1", "FloydSteinberg", null, "FloydSteinberg"));
    items.push(this.Item("item2", "Ordered", null, "Ordered"));

    return items;
}

StiMobileViewer.prototype.GetTiffCompressionSchemeItems = function () {
    var items = [];
    items.push(this.Item("item0", "Default", null, "Default"));
    items.push(this.Item("item1", "CCITT3", null, "CCITT3"));
    items.push(this.Item("item2", "CCITT4", null, "CCITT4"));
    items.push(this.Item("item3", "LZW", null, "LZW"));
    items.push(this.Item("item4", "None", null, "None"));
    items.push(this.Item("item5", "Rle", null, "Rle"));

    return items;
}

StiMobileViewer.prototype.GetEncodingDifFileItems = function () {
    var items = [];
    items.push(this.Item("item0", "437", null, "437"));
    items.push(this.Item("item1", "850", null, "850"));
    items.push(this.Item("item2", "852", null, "852"));
    items.push(this.Item("item3", "857", null, "857"));
    items.push(this.Item("item4", "860", null, "860"));
    items.push(this.Item("item5", "861", null, "861"));
    items.push(this.Item("item6", "862", null, "862"));
    items.push(this.Item("item7", "863", null, "863"));
    items.push(this.Item("item8", "865", null, "865"));
    items.push(this.Item("item9", "866", null, "866"));
    items.push(this.Item("item10", "869", null, "869"));

    return items;
}

StiMobileViewer.prototype.GetExportModeRtfItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.Export.ExportModeTable, null, "Table"));
    items.push(this.Item("item1", this.loc.Export.ExportModeFrame, null, "Frame"));

    return items;
}

StiMobileViewer.prototype.GetEncodingDbfFileItems = function () {
    var items = [];
    items.push(this.Item("item0", "Default", null, "Default"));
    items.push(this.Item("item1", "437 U.S. MS-DOS", null, "USDOS"));
    items.push(this.Item("item2", "620 Mazovia(Polish) MS-DOS", null, "MazoviaDOS"));
    items.push(this.Item("item3", "737 Greek MS-DOS(437G)", null, "GreekDOS"));
    items.push(this.Item("item4", "850 International MS-DOS", null, "InternationalDOS"));
    items.push(this.Item("item5", "852 Eastern European MS-DOS", null, "EasternEuropeanDOS"));
    items.push(this.Item("item6", "857 Turkish MS-DOS", null, "TurkishDOS"));
    items.push(this.Item("item7", "861 Icelandic MS-DOS", null, "IcelandicDOS"));
    items.push(this.Item("item8", "865 Nordic MS-DOS", null, "NordicDOS"));
    items.push(this.Item("item9", "866 Russian MS-DOS", null, "RussianDOS"));
    items.push(this.Item("item10", "895 Kamenicky(Czech) MS-DOS", null, "KamenickyDOS"));
    items.push(this.Item("item11", "1250 Eastern European Windows", null, "EasternEuropeanWindows"));
    items.push(this.Item("item12", "1251 Russian Windows", null, "RussianWindows"));
    items.push(this.Item("item13", "1252 WindowsANSI", null, "WindowsANSI"));
    items.push(this.Item("item14", "1253 GreekWindows", null, "GreekWindows"));
    items.push(this.Item("item15", "1254 TurkishWindows", null, "TurkishWindows"));
    items.push(this.Item("item16", "10000 StandardMacintosh", null, "StandardMacintosh"));
    items.push(this.Item("item17", "10006 GreekMacintosh", null, "GreekMacintosh"));
    items.push(this.Item("item18", "10007 RussianMacintosh", null, "RussianMacintosh"));
    items.push(this.Item("item19", "10029 EasternEuropeanMacintosh", null, "EasternEuropeanMacintosh"));
    
    return items;
}

StiMobileViewer.prototype.GetAllowEditableItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.FormFormatEditor.nameYes, null, "Yes"));
    items.push(this.Item("item1", this.loc.FormFormatEditor.nameNo, null, "No"));

    return items;
}

StiMobileViewer.prototype.GetEncryptionKeyLengthItems = function () {
    var items = [];
    items.push(this.Item("item0", "40 bit RC4 (Acrobat 3)", null, "Bit40"));
    items.push(this.Item("item1", "128 bit RC4 (Acrobat 5)", null, "Bit128"));
    items.push(this.Item("item2", "128 bit AES (Acrobat 7)", null, "Bit128_r4"));
    items.push(this.Item("item3", "256 bit AES (Acrobat 9)", null, "Bit256_r5"));
    items.push(this.Item("item4", "256 bit AES (Acrobat X)", null, "Bit256_r6"));

    return items;
}

/* Export Formats */
StiMobileViewer.prototype.GetExportFormatsItems = function () {
    var items = [];
    items.push(this.Item("StiPdfExportSettings", this.loc.Export.ExportTypePdfFile, "SavePdf.png", "StiPdfExportSettings"));
    items.push(this.Item("StiXpsExportSettings", this.loc.Export.ExportTypeXpsFile, "SaveXps.png", "StiXpsExportSettings"));
    items.push(this.Item("StiPpt2007ExportSettings", this.loc.Export.ExportTypePpt2007File, "SavePpt2007.png", "StiPpt2007ExportSettings"));
    items.push("separator");
    items.push(this.Item("StiHtmlExportSettings", this.loc.Export.ExportTypeHtmlFile, "SaveHtml.png", "StiHtmlExportSettings"));
    items.push("separator");

    items.push(this.Item("StiTxtExportSettings", this.loc.Export.ExportTypeTxtFile, "SaveText.png", "StiTxtExportSettings"));
    items.push(this.Item("StiRtfExportSettings", this.loc.Export.ExportTypeRtfFile, "SaveRtf.png", "StiRtfExportSettings"));
    items.push(this.Item("StiWord2007ExportSettings", this.loc.Export.ExportTypeWord2007File, "SaveWord2007.png", "StiWord2007ExportSettings"));
    items.push(this.Item("StiOdtExportSettings", this.loc.Export.ExportTypeWriterFile, "SaveOdt.png", "StiOdtExportSettings"));
    items.push("separator");
    items.push(this.Item("StiExcelExportSettings", this.loc.Export.ExportTypeExcelFile, "SaveExcel.png", "StiExcelExportSettings"));
    items.push(this.Item("StiOdsExportSettings", this.loc.Export.ExportTypeCalcFile, "SaveOds.png", "StiOdsExportSettings"));
    items.push("separator");
    items.push(this.Item("StiCsvExportSettings", this.loc.Export.ExportTypeDataFile, "SaveData.png", "StiCsvExportSettings"));
    items.push("separator");
    items.push(this.Item("StiBmpExportSettings", this.loc.Export.ExportTypeImageFile, "SaveImage.png", "StiBmpExportSettings"));

    return items;
}

StiMobileViewer.prototype.GetDataExportModeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.Export.AllBands, null, "AllBands"));
    items.push(this.Item("item1", this.loc.Export.DataOnly, null, "Data"));
    items.push(this.Item("item2", this.loc.Export.DataAndHeadersFooters, null, "DataAndHeadersFooters"));

    return items;
}