﻿
StiMobileViewer.prototype.InitializeInteractions = function (page) {

    page.paintSortingArrow = function (component, sort) {
        var arrowImg = document.createElement("img");
        arrowImg.src = sort == "asc" ? this.jsObject.options.images["ArrowDown.png"] : this.jsObject.options.images["ArrowUp.png"];
        var arrowWidth = (this.jsObject.options.zoom / 100) * 9;
        var arrowHeight = (this.jsObject.options.zoom / 100) * 5;        
        arrowImg.style.position = "absolute";
        arrowImg.style.width = arrowWidth + "px";
        arrowImg.style.height = arrowHeight + "px";
        component.appendChild(arrowImg);

        var oldPosition = component.style.position;
        var oldClassName = component.className;
        component.style.position = "relative";
        if (!oldClassName) component.className = "stiSortingParentElement";

        var arrowLeftPos = this.jsObject.FindPosX(arrowImg, component.className);
        var arrowTopPos = this.jsObject.FindPosY(arrowImg, component.className);
        
        arrowImg.style.marginLeft = (component.offsetWidth - arrowLeftPos - arrowWidth - ((this.jsObject.options.zoom / 100) * 3)) + "px";
        arrowImg.style.marginTop = (component.offsetHeight / 2 - arrowHeight / 2 - arrowTopPos) + "px";
        component.style.position = oldPosition;
        component.className = oldClassName;
    }

    var elems = page.getElementsByTagName('TD');
    var collapsedHash = [];
    for (var i = 0; i < elems.length; i++) {
        if (elems[i].getAttribute("interaction") && (
                elems[i].getAttribute("pageguid") ||
                elems[i].getAttribute("collapsed") ||
                elems[i].getAttribute("databandsort"))) {

            elems[i].style.cursor = "pointer";
            elems[i].jsObject = this;

            var sort = elems[i].getAttribute("sort");
            if (sort) {
                page.paintSortingArrow(elems[i], sort);
            }

            var collapsed = elems[i].getAttribute("collapsed");
            if (collapsed) {
                var compId = elems[i].getAttribute("compindex") + "|" + elems[i].getAttribute("interaction");
                if (collapsedHash.indexOf(compId) < 0) {
                    page.paintCollapsingIcon(elems[i], collapsed == "true");
                    collapsedHash.push(compId);
                }
            }

            elems[i].onclick = function (e) {
                if (this.getAttribute("pageguid")) page.postInteractionDrillDown(this);
                else if (this.getAttribute("collapsed")) page.postInteractionCollapsing(this);
                else page.postInteractionSorting(this, e.ctrlKey);
            }
        }
    }
}