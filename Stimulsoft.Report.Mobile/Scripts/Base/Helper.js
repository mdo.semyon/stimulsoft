
StiMobileViewer.prototype.scrollToAnchor = function (anchor, componentGuid) {
    var anchorElement = null;
    if (componentGuid) {
        for (var i = 0; i < document.anchors.length; i++) {
            if (document.anchors[i].getAttribute("guid") == componentGuid) {
                anchorElement = document.anchors[i];
                break;
            }
        }
    }
    if (!anchorElement) {
        for (var i = 0; i < document.anchors.length; i++) {
            if (document.anchors[i].name && anchor &&
                (document.anchors[i].name == anchor || document.anchors[i].name.replace(/'/g, '') == anchor.replace(/'/g, '')))
            {
                anchorElement = document.anchors[i];
                break;
            }
        }
    }
    if (!anchorElement) return;
    var anchorParent = anchorElement.parentElement || anchorElement;
    var targetTop = this.FindPosY(anchorElement, this.options.scrollbarsMode ? "stiMobileViewerReportPanel" : null, true) - anchorElement.offsetHeight - 50;
    var d = new Date();
    var endTime = d.getTime() + this.options.scrollDuration;
    var jsObject = this;
    this.ShowAnimationForScroll(this.options.reportPanel, targetTop, endTime,
    function () {
        if (jsObject.options.cloudMode && jsObject.options.cloudParameters.pagination) {
            clearTimeout(jsObject.options.reportPanel.scrollTimer);
        }

        var page = jsObject.getPageFromAnchorElement(anchorElement);
        var anchorParentTopPos = jsObject.FindPosY(anchorParent, "stiMobileViewerReportPanel", true);
        var pageTopPos = page ? jsObject.FindPosY(page, "stiMobileViewerReportPanel", true) : anchorParentTopPos;

        jsObject.removeBookmarksLabel();

        var label = document.createElement("div");
        jsObject.options.bookmarksLabel = label;
        label.className = "stiMobileViewerBookmarksLabel";
        var labelMargin = 20 * (jsObject.options.zoom / 100);
        var labelWidth = page ? page.offsetWidth - labelMargin - 6 : anchorParent.offsetWidth;
        var labelHeight = anchorParent.offsetHeight - 3;
        label.style.width = labelWidth + "px";
        label.style.height = labelHeight + "px";

        var pageLeftMargin = page.margins ? jsObject.StrToInt(page.margins[3]) : 0;
        label.style.marginLeft = (labelMargin / 2 - pageLeftMargin) + "px";
        var pageTopMargin = page.margins ? jsObject.StrToInt(page.margins[0]) : 0;
        label.style.marginTop = (anchorParentTopPos - pageTopPos - pageTopMargin - (jsObject.options.zoom / 100)) + "px";

        page.insertBefore(label, page.childNodes[0]);
    });
}

StiMobileViewer.prototype.isWholeWord = function (str, word) {
    var symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    var index = str.indexOf(word);
    var preSymbol = str.substring(index - 1, index);
    var nextSymbol = str.substring(index + word.length, index + word.length + 1);

    return ((preSymbol == "" || symbols.indexOf(preSymbol) == -1) && (nextSymbol == "" || symbols.indexOf(nextSymbol) == -1));
}

StiMobileViewer.prototype.getT = function (withoutBrackets) {
    var t = String.fromCharCode(84) + "r" + String.fromCharCode(105) + "a" + String.fromCharCode(108);
    if (withoutBrackets) return t;
    return (String.fromCharCode(91) + t + String.fromCharCode(93));
}

StiMobileViewer.prototype.goToFindedElement = function (findLabel) {
    if (findLabel && findLabel.ownerElement) {
        var targetTop = this.FindPosY(findLabel.ownerElement, this.options.scrollbarsMode ? "stiMobileViewerReportPanel" : null, true) - findLabel.ownerElement.offsetHeight - 50;
        var d = new Date();
        var endTime = d.getTime() + this.options.scrollDuration;
        this.ShowAnimationForScroll(this.options.reportPanel, targetTop, endTime);
    }
}

StiMobileViewer.prototype.hideFindLabels = function () {
    for (var i = 0; i < this.options.findHelper.findLabels.length; i++) {
        this.options.findHelper.findLabels[i].parentElement.removeChild(this.options.findHelper.findLabels[i]);
    }
    this.options.findHelper.findLabels = [];
    this.options.findMode = false;
}

StiMobileViewer.prototype.showFindLabels = function (text) {    
    this.hideFindLabels();
    this.options.findMode = true;
    this.options.changeFind = false;
    this.options.findHelper.lastFindText = text;
    var matchCase = this.options.findPanel && this.options.findPanel.controls.matchCase.isSelected;
    var matchWholeWord = this.options.findPanel && this.options.findPanel.controls.matchWholeWord.isSelected;
    var pages = this.options.reportPanel.pages;

    for (var i = 0; i < pages.length; i++) {
        var page = pages[i];
        var pageElements = page.getElementsByTagName('*');
        for (k = 0; k < pageElements.length; k++) {
            var innerText = pageElements[k].innerHTML;
            if (innerText && pageElements[k].childNodes.length == 1 && pageElements[k].childNodes[0].nodeName == "#text") {
                if (!matchCase) {
                    innerText = innerText.toLowerCase();
                    text = text.toLowerCase();
                }
                if (innerText.indexOf(text) >= 0) {
                    if (matchWholeWord && !this.isWholeWord(innerText, text)) {
                        continue;
                    }
                    var label = document.createElement("div");
                    label.ownerElement = pageElements[k];
                    label.className = "stiMobileViewerFindLabel";
                    label.style.width = (pageElements[k].offsetWidth - 4) + "px";
                    var labelHeight = pageElements[k].offsetHeight - 4;
                    label.style.height = labelHeight + "px";
                    label.style.marginTop = (-4 * (this.options.zoom / 100)) + "px";
                    pageElements[k].insertBefore(label, pageElements[k].childNodes[0]);

                    label.setSelected = function (state) {
                        this.isSelected = state;
                        this.style.border = "2px solid " + (state ? "red" : "#8a8a8a");
                    }

                    if (this.options.findHelper.findLabels.length == 0) label.setSelected(true);
                    this.options.findHelper.findLabels.push(label);
                }
            }
        }
    }

    if (this.options.findHelper.findLabels.length > 0) this.goToFindedElement(this.options.findHelper.findLabels[0]);
}

StiMobileViewer.prototype.selectFindLabel = function (direction) {
    var labels = this.options.findHelper.findLabels;
    if (labels.length == 0) return;
    var selectedIndex = 0;
    for (var i = 0; i < labels.length; i++) {
        if (labels[i].isSelected) {
            labels[i].setSelected(false);
            selectedIndex = i;
            break;
        }
    }
    if (direction == "Next") {
        selectedIndex++;
        if (selectedIndex > labels.length - 1) selectedIndex = 0;
    }
    else {
        selectedIndex--;
        if (selectedIndex < 0) selectedIndex = labels.length - 1;
    }
    labels[selectedIndex].setSelected(true);
    this.goToFindedElement(labels[selectedIndex]);
}

StiMobileViewer.prototype.scrollToPage = function (pageNumber, noAnimation) {
    var commonPagesHeight = 0;
    for (i = 0; i < pageNumber; i++) {
        commonPagesHeight += this.options.reportPanel.pages[i].offsetHeight + 20;
    }
    if (!this.options.scrollbarsMode) commonPagesHeight += this.FindPosY(this.options.reportPanel, null, true);

    var d = new Date();
    var endTime = d.getTime() + this.options.scrollDuration;
    this.ShowAnimationForScroll(this.options.reportPanel, commonPagesHeight, noAnimation ? 0 : endTime);
}

StiMobileViewer.prototype.removeBookmarksLabel = function () {
    if (this.options.bookmarksLabel) {
        this.options.bookmarksLabel.parentElement.removeChild(this.options.bookmarksLabel);
        this.options.bookmarksLabel = null;
    }
}

StiMobileViewer.prototype.getPageFromAnchorElement = function (anchorElement) {
    var obj = anchorElement;
    while (obj.parentElement) {
        if (obj.className && obj.className.indexOf("stiMobileViewerPage") == 0) {
            return obj;
        }
        obj = obj.parentElement;
    }
    return obj;
}

StiMobileViewer.prototype.isContainted = function (array, item) {
    for (var index in array)
        if (item == array[index]) return true;

    return false;
}

StiMobileViewer.prototype.IsTouchDevice = function () {
    return ('ontouchstart' in document.documentElement) && this.isMobileDevice.any() != null;
}

StiMobileViewer.prototype.isMobileDevice = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
    }
};

StiMobileViewer.prototype.SetZoom = function (zoomIn) {
    zoomValues = ["25", "50", "75", "100", "150", "200"];

    var i = 0;
    for (i = 0; i < zoomValues.length; i++)
        if (zoomValues[i] == this.options.zoom) break;

    if (zoomIn && i < zoomValues.length - 1) this.actionEvent("Zoom" + zoomValues[i + 1]);
    if (!zoomIn && i > 0) this.actionEvent("Zoom" + zoomValues[i - 1]);
}

StiMobileViewer.prototype.getCssParameter = function (css) {
    if (css.indexOf(".gif]") > 0 || css.indexOf(".png]") > 0) return css.substr(css.indexOf("["), css.indexOf("]") - css.indexOf("[") + 1);
    return null;
}

StiMobileViewer.prototype.newGuid = (function () {
    var CHARS = '0123456789abcdefghijklmnopqrstuvwxyz'.split('');
    return function (len, radix) {
        var chars = CHARS, uuid = [], rnd = Math.random;
        radix = radix || chars.length;

        if (len) {
            for (var i = 0; i < len; i++) uuid[i] = chars[0 | rnd() * radix];
        } else {
            var r;
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4';

            for (var i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    r = 0 | rnd() * 16;
                    uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
                }
            }
        }

        return uuid.join('');
    };
})();

StiMobileViewer.prototype.generateKey = function () {
    return this.newGuid().replace(/-/g, '');
}

StiMobileViewer.prototype.Item = function (name, caption, imageName, key) {
    var item = {
        "name": name,
        "caption": caption,
        "imageName": imageName,
        "key": key
    }

    return item;
}

StiMobileViewer.prototype.dateTimeObjectToString = function (dateTimeObject, typeDateTimeObject) {
    var date = new Date(dateTimeObject.year, dateTimeObject.month - 1, dateTimeObject.day, dateTimeObject.hours, dateTimeObject.minutes, dateTimeObject.seconds);
    
    return this.DateToLocaleString(date, typeDateTimeObject);
}

StiMobileViewer.prototype.getStringKey = function (key, parameter) {
    stringKey = (parameter.params.type == "DateTime")
        ? this.dateTimeObjectToString(key, parameter.params.dateTimeType)
        : key;

    return stringKey;
}

StiMobileViewer.prototype.getCountObjects = function (objectArray) {
    count = 0;
    if (objectArray)
        for (var singleObject in objectArray) { count++ };
    return count;
}

StiMobileViewer.prototype.getDateTimeObject = function (date) {
    if (!date) date = new Date();
    dateTimeObject = {};
    dateTimeObject.year = date.getFullYear();
    dateTimeObject.month = date.getMonth() + 1;
    dateTimeObject.day = date.getDate();
    dateTimeObject.hours = date.getHours();
    dateTimeObject.minutes = date.getMinutes();
    dateTimeObject.seconds = date.getSeconds();

    return dateTimeObject;
}

StiMobileViewer.prototype.getNowTimeSpanObject = function () {
    date = new Date();
    timeSpanObject = {};
    timeSpanObject.hours = date.getHours();
    timeSpanObject.minutes = date.getMinutes();
    timeSpanObject.seconds = date.getSeconds();

    return timeSpanObject;
}

StiMobileViewer.prototype.copyObject = function (o) {
    if (!o || "object" !== typeof o) {
        return o;
    }
    var c = "function" === typeof o.pop ? [] : {};
    var p, v;
    for (p in o) {
        if (o.hasOwnProperty(p)) {
            v = o[p];
            if (v && "object" === typeof v) {
                c[p] = this.copyObject(v);
            }
            else c[p] = v;
        }
    }
    return c;
}

StiMobileViewer.prototype.GetIEVersion = function () {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    // If IE, return version number.
    if (Idx > 0)
        return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));

    // If IE 11 then look for Updated user agent string.
    else if (!!navigator.userAgent.match(/Trident\/7\./))
        return 11;

    else
        return 0; //It is not IE
}

StiMobileViewer.prototype.GetNavigatorName = function () {
    var useragent = navigator.userAgent;
    var navigatorname = "Unknown";
    if (this.GetIEVersion() > 0) {
        navigatorname = "MSIE";
    }
    else if (useragent.indexOf('Gecko') != -1) {
        if (useragent.indexOf('Chrome') != -1)
            navigatorname = "Google Chrome";
        else navigatorname = "Mozilla";
    }
    else if (useragent.indexOf('Mozilla') != -1) {
        navigatorname = "old Netscape or Mozilla";
    }
    else if (useragent.indexOf('Opera') != -1) {
        navigatorname = "Opera";
    }

    return navigatorname;
}

StiMobileViewer.prototype.ShowHelpWindow = function (url) {
    var lang = this.options.helpLanguage == "ru" ? "ru" : "en";
    window.open("https://www.stimulsoft.com/" + lang + "/documentation/online/" + url);
}

StiMobileViewer.prototype.SetObjectToCenter = function (object) {
    var leftPos = (this.options.mobileViewer.offsetWidth / 2 - object.offsetWidth / 2);
    var topPos = (this.options.mobileViewer.offsetHeight / 2 - object.offsetHeight / 2);
    object.style.left = leftPos > 0 ? leftPos + "px" : 0;
    object.style.top = this.options.fullScreen ? (topPos > 0 ? topPos + "px" : 0) : "200px";
}

StiMobileViewer.prototype.SetObjectToCenterReportPanel = function (object) {
    object.style.left = (this.FindPosX(this.options.reportPanel, "stiMobileViewerMainPanel") + this.options.reportPanel.offsetWidth / 2 - object.offsetWidth / 2) + "px";
    object.style.top = (this.FindPosY(this.options.reportPanel, "stiMobileViewerMainPanel", true) + this.options.reportPanel.offsetHeight / 2 - object.offsetHeight / 2) + "px";
}

StiMobileViewer.prototype.StrToInt = function (value) {
    var result = parseInt(value);
    if (result)
        return result;
    else
        return 0;
}

StiMobileViewer.prototype.StrToCorrectPositiveInt = function (value) {
    var result = this.StrToInt(value);
    if (result >= 0)
        return result;
    else
        return 0;
}

StiMobileViewer.prototype.HelpLinks = {
    "Toolbar": "user-manual/index.html?viewing_reports_basic_toolbar_of_report_viewer.htm"
}

StiMobileViewer.prototype.setEnabledViewer = function (state, notVisible) {
    var names = ["Print", "Save", "Zoom", "ViewMode"];
    for (var i = 0; i < names.length; i++) {
        var control = this.options.toolbar.controls[names[i]];
        if (control) control.setEnabled(state, notVisible);
    }
    this.options.navigatePanel.disabledPanel.style.display = state ? "none" : "";
}

StiMobileViewer.prototype.formatString = function (format, args) {
    if (format) {
        var result = format;
        for (var i = 1; i < arguments.length; i++) {
            result = result.replace('{' + (i - 1) + '}', arguments[i]);
        }
        return arguments.length > 1 ? result : format.replace('{0}', "");
    } else {
        return "";
    }
}

StiMobileViewer.prototype.AddBigProgressMarkerToControl = function (control) {
    var progressMarker = document.createElement("div");
    progressMarker.className = "stiMobileViewerBigProgressMarker";
    progressMarker.style.display = "none";
    var jsObject = this;

    progressMarker.changeVisibleState = function (state, left, top) {
        progressMarker.style.display = state ? "" : "none";
        if (state) {
            progressMarker.style.background = "url(" + jsObject.options.images["BigProgressMarker" + jsObject.GetThemeColor() + ".gif"] + ")";
            progressMarker.style.left = (left || control.offsetWidth / 2 - 32) + "px";
            progressMarker.style.top = (top || control.offsetHeight / 2 - 32) + "px";
        }
    }

    control.appendChild(progressMarker);
    control.progressMarker = progressMarker;
}

StiMobileViewer.prototype.GetThemeColor = function () {
    var themeColor = this.options.theme;
    if (themeColor == "Purple") return "Violet";
    if (themeColor == "Blue" || themeColor == "Office2013") return "Blue";
    try{
        themeColor = themeColor.replace("Office2013White", "");
        themeColor = themeColor.replace("Office2013LightGray", "");
        themeColor = themeColor.replace("Office2013VeryDarkGray", "");
        themeColor = themeColor.replace("Office2013DarkGray", "");
    } catch (e) {
        return "Blue";
    }

    return themeColor;
}

StiMobileViewer.prototype.GetThemeColorValue = function () {
    switch (this.GetThemeColor()) {
        case "Blue": return "#19478a";
        case "Carmine": return "#912c2f";
        case "Green": return "#0b6433";
        case "Orange": return "#b73a1c";
        case "Purple": return "#8653a5";
        case "Teal": return "#23645c";
        case "Violet": return "#6d3069";
    }

    return "#19478a";
}

StiMobileViewer.prototype.SetCookie = function (name, value, path, domain, secure) {
    var pathName = location.pathname;
    var expDate = new Date();
    expDate.setTime(expDate.getTime() + (365 * 24 * 3600 * 1000));//TODO Setup period
    document.cookie = name + "=" + escape(value) +
      "; expires=" + expDate.toGMTString() +
      ((path) ? "; path=" + path : "") +
      ((domain) ? "; domain=" + pathName.substring(0, pathName.lastIndexOf('/')) + '/' : "") +
      ((secure) ? "; secure" : "");
}

StiMobileViewer.prototype.GetCookie = function (name) {
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset)
            if (end == -1) {
                end = cookie.length;
            }
            setStr = unescape(cookie.substring(offset, end));
        }
    }
    return (setStr);
}

StiMobileViewer.prototype.GetFullFileNameByExportFormat = function (fileName, exportFormat) {
    if (exportFormat == "StiPdfExportSettings") return fileName + ".pdf";
    else if (exportFormat == "StiXpsExportSettings") return fileName + ".xps";
    else if (exportFormat == "StiPpt2007ExportSettings") return fileName + ".ppt";
    else if (exportFormat == "StiHtmlExportSettings") return fileName + ".html";
    else if (exportFormat == "StiHtml5ExportSettings") return fileName + ".html";
    else if (exportFormat == "StiMhtExportSettings") return fileName + ".mht";
    else if (exportFormat == "StiTxtExportSettings") return fileName + ".txt";
    else if (exportFormat == "StiRtfExportSettings") return fileName + ".rtf";
    else if (exportFormat == "StiWord2007ExportSettings") return fileName + ".docx";
    else if (exportFormat == "StiOdtExportSettings") return fileName + ".odt";
    else if (exportFormat == "StiExcelExportSettings") return fileName + ".xls";
    else if (exportFormat == "StiExcelXmlExportSettings") return fileName + ".xls";
    else if (exportFormat == "StiExcel2007ExportSettings") return fileName + ".xlsx";
    else if (exportFormat == "StiCsvExportSettings") return fileName + ".csv";
    else if (exportFormat == "StiDbfExportSettings") return fileName + ".dbf";
    else if (exportFormat == "StiXmlExportSettings") return fileName + ".xml";
    else if (exportFormat == "StiDifExportSettings") return fileName + ".dif";
    else if (exportFormat == "StiSylkExportSettings") return fileName + ".sylk";
    else if (exportFormat == "StiBmpExportSettings") return fileName + ".bmp";
    else if (exportFormat == "StiGifExportSettings") return fileName + ".gif";
    else if (exportFormat == "StiJpegExportSettings") return fileName + ".jpg";
    else if (exportFormat == "StiPcxExportSettings") return fileName + ".pcx";
    else if (exportFormat == "StiPngExportSettings") return fileName + ".png";
    else if (exportFormat == "StiTiffExportSettings") return fileName + ".tiff";
    else if (exportFormat == "StiEmfExportSettings") return fileName + ".emf";
    else if (exportFormat == "StiSvgExportSettings") return fileName + ".svg";
    else if (exportFormat == "StiSvgzExportSettings") return fileName + ".svgz";

    return fileName;
}

StiMobileViewer.prototype.StrToCorrectByte = function (value) {
    var result = parseInt(value);
    if (result) {
        if (result > 255) return 255;
        if (result < 0) return 0;
        return result;
    }
    else
        return 0;
}

StiMobileViewer.prototype.rgb = function (r, g, b) {
    return "#" + this.dec2hex(r) + this.dec2hex(g) + this.dec2hex(b)
};

StiMobileViewer.prototype.dec2hex = function (d) {
    if (d > 15) {
        return d.toString(16)
    } else {
        return "0" + d.toString(16)
    }
}

StiMobileViewer.prototype.Progress = function () {
    var progressContainer = document.createElement("div");
    progressContainer.style.position = "absolute";
    progressContainer.style.zIndex = "1000";

    var progress = document.createElement("div");
    progressContainer.appendChild(progress);
    progress.className = "mobile_viewer_loader";

    return progressContainer;
}

StiMobileViewer.prototype.DateToLocaleString = function (date, dateTimeType) {
    var timeString = date.toLocaleTimeString();
    var isAmericanFormat = timeString.toLowerCase().indexOf("am") >= 0 || timeString.toLowerCase().indexOf("pm") >= 0;      
    var formatDate = isAmericanFormat ? "MM/dd/yyyy" : "dd.MM.yyyy";
            
    var yyyy = date.getFullYear();
    var yy = yyyy.toString().substring(2);
    var M = date.getMonth() + 1;
    var MM = M < 10 ? "0" + M : M;
    var d = date.getDate();
    var dd = d < 10 ? "0" + d : d;

    formatDate = formatDate.replace(/yyyy/i, yyyy);
    formatDate = formatDate.replace(/yy/i, yy);
    formatDate = formatDate.replace(/MM/i, MM);
    formatDate = formatDate.replace(/M/i, M);
    formatDate = formatDate.replace(/dd/i, dd);
    formatDate = formatDate.replace(/d/i, d);

    if (dateTimeType == "Time") return timeString;
    if (dateTimeType == "Date") return formatDate;
    return formatDate + " " + timeString;
}

StiMobileViewer.prototype.addEvent = function (element, eventName, fn) {
    if (element.addEventListener) element.addEventListener(eventName, fn, false);
    else if (element.attachEvent) element.attachEvent('on' + eventName, fn);
}

StiMobileViewer.prototype.UpdateAllHyperLinks = function () {
    if (this.options.menuViewMode == "WholeReport") return;    
    var aHyperlinks = this.options.reportPanel.getElementsByTagName("a");

    if (this.options.bookmarksPanel) {
        var aBookmarks = this.options.bookmarksPanel.getElementsByTagName("a");

        for (var i = 0; i < aHyperlinks.length; i++) {
            if (aHyperlinks[i].getAttribute("href")) {
                aHyperlinks[i].anchorName = aHyperlinks[i].getAttribute("href").replace("#", "");

                aHyperlinks[i].onclick = function () {
                    for (var k = 0; k < aBookmarks.length; k++) {
                        var clickFunc = aBookmarks[k].getAttribute("onclick");
                        if (clickFunc && clickFunc.indexOf("'" + this.anchorName + "'") > 0) {
                            try {
                                eval(clickFunc);
                                return false;
                            }
                            catch (e) { }
                        }
                    }
                }
            }
        }
    }
}

StiMobileViewer.prototype.GetCountObjects = function (objectArray) {
    count = 0;
    if (objectArray)
        for (var singleObject in objectArray) { count++ };
    return count;
}

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

StiMobileViewer.prototype.DateToJSONDateFormat = function (date) {
    var offset = date.getTimezoneOffset() * -1;
    hoursOffset = Math.abs(parseInt(offset / 60));
    minutesOffset = Math.abs(offset % 60);
    if (hoursOffset.toString().length == 1) hoursOffset = "0" + hoursOffset;
    if (minutesOffset.toString().length == 1) minutesOffset = "0" + minutesOffset;
    return "/Date(" + Date.parse(date).toString() + ")/";
}

StiMobileViewer.prototype.ClearStyles = function (object) {
    object.className = "stiDesignerClearAllStyles";
}

StiMobileViewer.prototype.SetWindowIcon = function (doc, imageSrc) {
    var head = doc.head || doc.getElementsByTagName("head")[0];
    var link = doc.createElement("link"),
    oldLink = doc.getElementById("window-icon");
    link.id = "window-icon";
    link.rel = "icon";
    link.href = imageSrc;
    if (oldLink) {
        head.removeChild(oldLink);
    }
    head.appendChild(link);
}

StiMobileViewer.prototype.postForm = function (postData, doc, url) {
    if (!doc) doc = document;
    var form = doc.createElement("FORM");
    form.setAttribute("method", "POST");
    form.setAttribute("action", url || this.options.requestUrl);

    for (var key in postData) {
        var hiddenField = doc.createElement("INPUT");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", postData[key]);

        form.appendChild(hiddenField);
    }

    doc.body.appendChild(form);
    form.submit();
    doc.body.removeChild(form);
}