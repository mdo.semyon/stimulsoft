﻿
StiMobileViewer.prototype.BaseMenu = function (name, parentButton, animationDirection, rightToLeft, material) {
    var parentMenu = document.createElement("div");
    this.options.mainPanel.appendChild(parentMenu);
    parentMenu.className = "stiMobileViewerParentMenu";
    if (this.options.isMaterial) {
        parentMenu.style.position = "absolute";
        parentMenu.style.zIndex = "35";
    }
    parentMenu.jsObject = this;
    parentMenu.id = this.generateKey();
    parentMenu.name = name != null ? name : parentMenu.id;
    parentMenu.items = {};
    parentMenu.parentButton = parentButton;
    parentMenu.type = null;
    if (parentButton) parentButton.haveMenu = true;
    parentMenu.animationDirection = animationDirection;
    parentMenu.rightToLeft = rightToLeft || this.options.rightToLeft;
    parentMenu.visible = false;    
    if (name != null) this.options.menus[name] = parentMenu;

    var menu = document.createElement("div");
    menu.style.overflowY = "auto";
    menu.style.borderWidth = "1px";
    if (!material) {
        parentMenu.style.display = "none";
        menu.style.overflowX = "hidden";
        menu.style.maxHeight = "500px";
    } else {
        menu.style.overflowX = "hidden";
        menu.style.height = window.innerHeight + "px";
        menu.style.width = window.innerWidth * 0.7 + "px";
        menu.style.left = -window.innerWidth * 0.7 + "px";
        menu.style.top = 0;
    }
    menu.style.fontFamily = this.options.toolbarFontFamily;
    menu.style.color = this.options.toolbarFontColor;
    parentMenu.appendChild(menu);
    parentMenu.innerContent = menu;
    menu.className = "stiMobileViewerMenu";

    parentMenu.changeVisibleState = function (state, parentButton, rightAlign, leftOffset) {
        var mainClassName = "stiMobileViewerMainPanel";
        if (parentButton) {
            this.parentButton = parentButton;
            parentButton.haveMenu = true;
        }
        if (state) {
            this.onshow();
            this.style.display = "";
            this.visible = true;
            this.style.overflow = "hidden";
            this.parentButton.setSelected(true);
            this.jsObject.options[this.type == null ? "currentMenu" : "current" + this.type] = this;
            this.style.width = this.innerContent.offsetWidth + "px";
            this.style.height = this.innerContent.offsetHeight + "px";
            this.style.left = this.rightToLeft || rightAlign
                    ? (this.jsObject.FindPosX(this.parentButton, mainClassName) - this.innerContent.offsetWidth + this.parentButton.offsetWidth) - (leftOffset || 0) + "px"
                    : this.jsObject.FindPosX(this.parentButton, mainClassName) - (leftOffset || 0) + "px";
            this.style.top = (this.animationDirection == "Down")
                ? (this.jsObject.FindPosY(this.parentButton, mainClassName) + this.parentButton.offsetHeight + 2) + "px"
                : (this.jsObject.FindPosY(this.parentButton, mainClassName) - this.offsetHeight) + "px";
            this.innerContent.style.top = ((this.animationDirection == "Down" ? -1 : 1) * this.innerContent.offsetHeight) + "px";

            d = new Date();
            var endTime = d.getTime() + this.jsObject.options.menuAnimDuration;
            this.jsObject.ShowAnimationVerticalMenu(this, (this.animationDirection == "Down" ? 0 : -1), endTime);
        }
        else {
            this.onHide();
            clearTimeout(this.innerContent.animationTimer);
            this.visible = false;
            this.parentButton.setSelected(false);
            this.style.display = "none";
            if (this.jsObject.options[this.type == null ? "currentMenu" : "current" + this.type] == this)
                this.jsObject.options[this.type == null ? "currentMenu" : "current" + this.type] = null;
        }
    }

    parentMenu.action = function (menuItem) {
        return menuItem;
    }

    parentMenu.onmousedown = function () {
        if (!this.isTouchStartFlag) this.ontouchstart(true);
    }

    parentMenu.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.menuPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000); 
    }
    parentMenu.onshow = function () { };
    parentMenu.onHide = function () { };

    return parentMenu;
}

StiMobileViewer.prototype.Separator = function () {
    var separator = document.createElement("div");    
    separator.className = "stiMobileViewerFormSeparator";
    
    return separator;
}