﻿
StiMobileViewer.prototype.InitializeSaveMenu = function () {
    var isFirst = true;
    var items = [];
    if (this.options.showExportToDocument) {
        items.push(this.Item("SaveDocument", this.loc.FormViewer.DocumentFile, "SaveDocument.png", "SaveDocument"));
        isFirst = false;
    }
    if (this.options.showExportToPdf || this.options.showExportToXps || this.options.showExportToPowerPoint) {
        if (!isFirst) items.push("separator1");
        isFirst = false;
    }
    if (this.options.showExportToPdf) items.push(this.Item("StiPdfExportSettings", this.loc.Export.ExportTypePdfFile, "SavePdf.png", "StiPdfExportSettings"));
    if (this.options.showExportToXps) items.push(this.Item("StiXpsExportSettings", this.loc.Export.ExportTypeXpsFile, "SaveXps.png", "StiXpsExportSettings"));
    if (this.options.showExportToPowerPoint) items.push(this.Item("StiPpt2007ExportSettings", this.loc.Export.ExportTypePpt2007File, "SavePpt2007.png", "StiPpt2007ExportSettings"));

    if (this.options.showExportToHtml || this.options.showExportToHtml5 || this.options.showExportToMht) {
        if (!isFirst) items.push("separator2");
        isFirst = false;
        items.push(this.Item("StiHtmlExportSettings", this.loc.Export.ExportTypeHtmlFile, "SaveHtml.png", "StiHtmlExportSettings"));
    }
    if (this.options.showExportToText || this.options.showExportToRtf || this.options.showExportToWord2007 || this.options.showExportToOdt) {
        if (!isFirst) items.push("separator3");
        isFirst = false;
    }
    if (this.options.showExportToText) items.push(this.Item("StiTxtExportSettings", this.loc.Export.ExportTypeTxtFile, "SaveText.png", "StiTxtExportSettings"));
    if (this.options.showExportToRtf) items.push(this.Item("StiRtfExportSettings", this.loc.Export.ExportTypeRtfFile, "SaveRtf.png", "StiRtfExportSettings"));
    if (this.options.showExportToWord2007) items.push(this.Item("StiWord2007ExportSettings", this.loc.Export.ExportTypeWord2007File, "SaveWord2007.png", "StiWord2007ExportSettings"));
    if (this.options.showExportToOdt) items.push(this.Item("StiOdtExportSettings", this.loc.Export.ExportTypeWriterFile, "SaveOdt.png", "StiOdtExportSettings"));
    if (this.options.showExportToExcel || this.options.showExportToExcel2007 || this.options.showExportToExcelXml || this.options.showExportToOds) {
        if (!isFirst) items.push("separator4");
        isFirst = false;
    }
    if (this.options.showExportToExcel || this.options.showExportToExcelXml || this.options.showExportToExcel2007) {
        items.push(this.Item("StiExcelExportSettings", this.loc.Export.ExportTypeExcelFile, "SaveExcel.png", "StiExcelExportSettings"));
    }
    if (this.options.showExportToOds) {
        items.push(this.Item("StiOdsExportSettings", this.loc.Export.ExportTypeCalcFile, "SaveOds.png", "StiOdsExportSettings"));
    }
    if (this.options.showExportToCsv || this.options.showExportToDbf || this.options.showExportToXml || this.options.showExportToDif || this.options.showExportToSylk) {
        if (!isFirst) items.push("separator5");
        isFirst = false;
        items.push(this.Item("StiCsvExportSettings", this.loc.Export.ExportTypeDataFile, "SaveData.png", "StiCsvExportSettings"));
    }
    if (this.options.showExportToBmp || this.options.showExportToGif || this.options.showExportToJpeg || this.options.showExportToPcx ||
        this.options.showExportToPng || this.options.showExportToTiff || this.options.showExportToMetafile || this.options.showExportToSvg || this.options.showExportToSvgz) {
        if (!isFirst) items.push("separator6");
        isFirst = false;
        items.push(this.Item("StiBmpExportSettings", this.loc.Export.ExportTypeImageFile, "SaveImage.png", "StiBmpExportSettings"));
    }

    var saveMenu = this.VerticalMenu("saveMenu", this.options.toolbar.controls["Save"], "Down", items, this.GetStyles("MenuStandartItem"));

    saveMenu.action = function (menuItem) {
        saveMenu.changeVisibleState(false);
        if (saveMenu.jsObject.options.showExportDialog) {
            var viewerExportForm = saveMenu.jsObject.options.forms.exportForm || saveMenu.jsObject.InitializeExportForm("exportForm");
            viewerExportForm.show(menuItem.key);
        }
        else
            saveMenu.jsObject.exportEvent(menuItem.key);
    }
}