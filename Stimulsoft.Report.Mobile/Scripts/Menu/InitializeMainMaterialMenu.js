﻿
StiMobileViewer.prototype.InitializeMainMaterialMenu = function () {
    var isFirst = true;
    var items = [];
    if (this.options.showPrintButton) {
        items.push(["Print", this.loc.A_WebViewer.PrintReport, this.options.cloudMode ? "CloudIcons.ReportPrint20.png" : "Print.png", this.loc.HelpViewer.Print,
                  [["PrintPdf", this.loc.A_WebViewer.PrintToPdf, "PrintPdf.png", "PrintPdf"],
                  ["PrintWithPreview", this.loc.A_WebViewer.PrintWithPreview, "PrintWithPreview.png", "PrintWithPreview"],
                  ["PrintWithoutPreview", this.loc.A_WebViewer.PrintWithoutPreview, "PrintWithoutPreview.png", "PrintWithoutPreview"]]]);
        isFirst = false;
    }
    var saveSubMenu = [];
    if (this.options.showExportToDocument) {
        saveSubMenu.push(["SaveDocument", this.loc.FormViewer.DocumentFile, "SaveDocument.png", "SaveDocument"]);
        isFirst = false;
    }
    //if (this.options.showExportToPdf || this.options.showExportToXps || this.options.showExportToPowerPoint) {
    //    if (!isFirst) items.push("separator1");
    //    isFirst = false;
    //}
    if (this.options.showExportToPdf) saveSubMenu.push(["StiPdfExportSettings", this.loc.Export.ExportTypePdfFile, "SavePdf.png", "StiPdfExportSettings"]);
    if (this.options.showExportToXps) saveSubMenu.push(["StiXpsExportSettings", this.loc.Export.ExportTypeXpsFile, "SaveXps.png", "StiXpsExportSettings"]);
    if (this.options.showExportToPowerPoint) saveSubMenu.push(["StiPpt2007ExportSettings", this.loc.Export.ExportTypePpt2007File, "SavePpt2007.png", "StiPpt2007ExportSettings"]);

    if (this.options.showExportToHtml || this.options.showExportToHtml5 || this.options.showExportToMht) {
        //if (!isFirst) items.push("separator2");
        //isFirst = false;
        saveSubMenu.push(["StiHtmlExportSettings", this.loc.Export.ExportTypeHtmlFile, "SaveHtml.png", "StiHtmlExportSettings"]);
    }
    if (this.options.showExportToText || this.options.showExportToRtf || this.options.showExportToWord2007 || this.options.showExportToOdt) {
        //if (!isFirst) items.push("separator3");
        isFirst = false;
    }
    if (this.options.showExportToText) saveSubMenu.push(["StiTxtExportSettings", this.loc.Export.ExportTypeTxtFile, "SaveText.png", "StiTxtExportSettings"]);
    if (this.options.showExportToRtf) saveSubMenu.push(["StiRtfExportSettings", this.loc.Export.ExportTypeRtfFile, "SaveRtf.png", "StiRtfExportSettings"]);
    if (this.options.showExportToWord2007) saveSubMenu.push(["StiWord2007ExportSettings", this.loc.Export.ExportTypeWord2007File, "SaveWord2007.png", "StiWord2007ExportSettings"]);
    if (this.options.showExportToOdt) saveSubMenu.push(["StiOdtExportSettings", this.loc.Export.ExportTypeWriterFile, "SaveOdt.png", "StiOdtExportSettings"]);
    if (this.options.showExportToExcel || this.options.showExportToExcel2007 || this.options.showExportToExcelXml || this.options.showExportToOds) {
        //if (!isFirst) items.push("separator4");
        isFirst = false;
    }
    if (this.options.showExportToExcel || this.options.showExportToExcelXml || this.options.showExportToExcel2007) {
        saveSubMenu.push(["StiExcelExportSettings", this.loc.Export.ExportTypeExcelFile, "SaveExcel.png", "StiExcelExportSettings"]);
    }
    if (this.options.showExportToOds) {
        saveSubMenu.push(["StiOdsExportSettings", this.loc.Export.ExportTypeCalcFile, "SaveOds.png", "StiOdsExportSettings"]);
    }
    if (this.options.showExportToCsv || this.options.showExportToDbf || this.options.showExportToXml || this.options.showExportToDif || this.options.showExportToSylk) {
        //if (!isFirst) items.push("separator5");
        //isFirst = false;
        saveSubMenu.push(["StiCsvExportSettings", this.loc.Export.ExportTypeDataFile, "SaveData.png", "StiCsvExportSettings"]);
    }
    if (this.options.showExportToBmp || this.options.showExportToGif || this.options.showExportToJpeg || this.options.showExportToPcx ||
        this.options.showExportToPng || this.options.showExportToTiff || this.options.showExportToMetafile || this.options.showExportToSvg || this.options.showExportToSvgz) {
        //if (!isFirst) items.push("separator6");
        //isFirst = false;
        saveSubMenu.push(["StiBmpExportSettings", this.loc.Export.ExportTypeImageFile, "SaveImage.png", "StiBmpExportSettings"]);
    }

    if (this.options.showSave) {
        items.push(["Save", this.loc.A_WebViewer.SaveReport, this.options.cloudMode ? "CloudIcons.Save20.png" : "Save.png", this.loc.HelpViewer.Save, saveSubMenu ]);
        isFirst = false;
    }
    if (this.options.showFindButton) {
        items.push(["Find", this.loc.FormViewer.Find, "Find.png", this.loc.HelpViewer.Find]);
        isFirst = false;
    }
    if (this.options.showEditorButton) {
        items.push(["Editor", this.loc.FormViewer.Editor, "Editor.png", this.loc.FormViewer.Editor]);
        isFirst = false;
    }


    if (this.options.showViewMode) {
        items.push(["OnePageWholeReport", this.loc.A_WebViewer.OnePage, "OnePage.png", this.loc.A_WebViewer.OnePage]);          
    }

    //if (this.options.showAboutButton) items.push(["About", this.loc.MainMenu.menuHelpAboutProgramm.replace("...", "").replace("&", ""), "About.png", false]);
    var mainMaterialMenu = this.BaseMaterialMenu("mainMaterialMenu", this.options.materialPanel, "Down", items);
    this.options.mainMaterialMenu = mainMaterialMenu;
}