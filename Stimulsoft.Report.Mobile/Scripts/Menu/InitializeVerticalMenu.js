﻿
StiMobileViewer.prototype.VerticalMenu = function (name, parentButton, animDirection, items, itemsStyles, rightToLeft, material) {
    var menu = this.BaseMenu(name, parentButton, animDirection, rightToLeft, material);
    menu.itemsStyles = itemsStyles;

    menu.addItems = function (items) {
        while (this.innerContent.childNodes[0]) {
            this.innerContent.removeChild(this.innerContent.childNodes[0]);
        }
        for (var index in items) {
            if (typeof (items[index]) != "string")
                this.innerContent.appendChild(this.jsObject.VerticalMenuItem(this, items[index].name, items[index].caption, items[index].imageName, items[index].key, this.itemsStyles));
            else
                this.innerContent.appendChild(this.jsObject.VerticalMenuSeparator(this, items[index]));
        }
    }

    menu.addItems(items);
    
    return menu; 
}

StiMobileViewer.prototype.VerticalMenuItem = function (menu, itemName, caption, imageName, key, styles) {
    var menuItem = document.createElement("div");
    menuItem.jsObject = this;
    menuItem.menu = menu;
    menuItem.name = itemName;
    menuItem.key = key;
    menuItem.caption_ = caption;
    menuItem.imageName = imageName;
    menuItem.styles = styles;
    menuItem.id = this.generateKey();
    menuItem.className = styles["default"];
    menuItem.style.height = this.options.isTouchDevice ? "30px" : "24px";
    menu.items[itemName] = menuItem;
    menuItem.isEnabled = true;
    menuItem.isSelected = false;
    menuItem.selectedItem = null;

    var innerTable = this.CreateHTMLTable();
    menuItem.appendChild(innerTable);
    innerTable.style.height = "100%";
    innerTable.style.width = "100%";

    if (imageName != null) {
        menuItem.cellImage = innerTable.addCell();
        menuItem.cellImage.style.width = "1px";
        menuItem.cellImage.style.padding = "0 5px 0 5px";
        menuItem.cellImage.style.textAlign = "left";
        if (this.options.images[imageName]) {
            var img = document.createElement("img");
            menuItem.image = img;
            menuItem.cellImage.appendChild(img);
            img.src = this.options.images[imageName];
        }
        else {
            menuItem.cellImage.style.width = "16px";
        }
    }

    if (caption != null || typeof (caption) == "undefined") {
        var captionCell = innerTable.addCell();
        menuItem.caption = captionCell;
        captionCell.style.padding = "0 20px 0 5px";
        captionCell.style.textAlign = "left";
        captionCell.style.whiteSpace = "nowrap";
        if (caption) captionCell.innerHTML = caption;
        if (itemName.indexOf("fontItem") == 0) {
            captionCell.style.fontSize = "16px";
            captionCell.style.fontFamily = caption;
            captionCell.style.lineHeight = "1";
            menuItem.setAttribute("title", caption);
        }
    }

    menuItem.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    menuItem.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.className =  styles["default"] + " " + this.styles["over"];
    }

    menuItem.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.className =  styles["default"] + " " + (this.isSelected ? this.styles["selected"] : "");
    }

    menuItem.onclick = function (event) {
        if (this.isTouchEndFlag || !this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.action();
    }

    menuItem.ontouchstart = function () {
        this.jsObject.options.fingerIsMoved = false;
    }

    menuItem.ontouchend = function () {
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        this.className = styles["default"] + " " + this.styles["over"];
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        setTimeout(function () {
            this_.className = this_.styles["default"];
            this_.action();
        }, 150);
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    menuItem.action = function () {
        this.menu.action(this);
    }

    menuItem.setEnabled = function (state) {
        this.isEnabled = state;
        this.className = styles["default"] + " " + (!state ? this.styles["disabled"] : "");
    }

    menuItem.setSelected = function (state) {
        if (!state) {
            this.isSelected = false;
            this.className = this.styles["default"];
            return;
        }
        if (this.menu.selectedItem != null) {
            this.menu.selectedItem.className = this.styles["default"];
            this.menu.selectedItem.isSelected = false;
        }
        this.className = styles["default"] + " " + this.styles["selected"];
        this.menu.selectedItem = this;
        this.isSelected = true;
    }

    return menuItem;
}

StiMobileViewer.prototype.VerticalMenuSeparator = function (menu, name) {
    var menuSeparator = document.createElement("div");
    menuSeparator.isSeparator = true;
    menuSeparator.className = "stiMobileViewerVerticalMenuSeparator";
    menu.items[name] = menuSeparator;

    return menuSeparator;
}