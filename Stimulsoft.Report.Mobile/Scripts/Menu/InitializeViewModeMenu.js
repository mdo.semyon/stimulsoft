﻿
StiMobileViewer.prototype.InitializeViewModeMenu = function () {
    var items = [];
    items.push(this.Item("OnePage", this.loc.A_WebViewer.OnePage, "OnePage.png", "OnePage"));
    items.push(this.Item("WholeReport", this.loc.A_WebViewer.WholeReport, "WholeReport.png", "WholeReport"));

    var viewModeMenu = this.VerticalMenu("viewModeMenu", this.options.toolbar.controls["ViewMode"],
        this.options.cloudMode ? "Up" : "Down", items, this.GetStyles("MenuStandartItem"), this.options.cloudMode);

    viewModeMenu.action = function (menuItem) {
        viewModeMenu.changeVisibleState(false);
        viewModeMenu.jsObject.actionEvent(menuItem.key);
    }
}