﻿
StiMobileViewer.prototype.BaseMaterialMenu = function (name, parentButton, animationDirection, items, rightToLeft) {
    var this_ = this;
    var parentMenu = document.createElement("div");
    this.options.mainPanel.appendChild(parentMenu);
    //parentMenu.className = "stiMobileViewerMenu";
    parentMenu.jsObject = this;
    parentMenu.id = this.generateKey();
    parentMenu.name = name != null ? name : parentMenu.id;
    parentMenu.items = {};
    parentMenu.parentButton = parentButton;
    parentMenu.type = null;
    if (parentButton) parentButton.haveMenu = true;
    parentMenu.animationDirection = animationDirection;
    parentMenu.rightToLeft = rightToLeft || this.options.rightToLeft;
    parentMenu.visible = false;
    parentMenu.style.zIndex = 1111;
    parentMenu.style.position = "absolute";
    parentMenu.style.overflowX = "hidden";
    parentMenu.style.top = "0";
    if (name != null) this.options.menus[name] = parentMenu;

    var createMenu = function () {
        var menu = document.createElement("div");
        menu.style.overflowY = "auto";
        menu.style.overflowX = "hidden";
        menu.style.height = window.innerHeight + "px";
        menu.style.left = 0;
        menu.style.top = 0;
        menu.style.fontFamily = this_.options.toolbarFontFamily;
        menu.style.color = this_.options.toolbarFontColor;
        parentMenu.appendChild(menu);        
        menu.className = "stiMobileViewerMenu";
        return menu;
    }

    var resizeMenu = function(){
        for (var i in parentMenu.childNodes) {
            if (parentMenu.childNodes[i].style) {
                parentMenu.childNodes[i].style.height = window.innerHeight + "px";
            }
        }
        parentMenu.style.height = window.innerHeight + "px";
    }

    window.addEventListener("resize", function () {
        resizeMenu();
    });

    window.addEventListener("orientationchange", function () {
        resizeMenu();
    });

    var showSubMenu = function (subMenu, show) {
        var dur = 250;        
        if (show) {
            menu.subMenu = subMenu;
            this_.animate(subMenu, { duration: dur, animations: [{ style: "left", start: subMenu.style.left, end: 0, postfix: "px" }] });
            this_.animate(menu, { duration: dur, animations: [{ style: "left", start: 0, end: parseInt(subMenu.offsetWidth), postfix: "px" }] });
            this_.animate(parentMenu, { duration: dur, animations: [{ style: "width", start: parentMenu.style.width, end: parseInt(subMenu.offsetWidth), postfix: "px" }] });
        } else {
            subMenu = menu.subMenu;
            menu.subMenu = null;
            this_.animate(subMenu, { duration: dur, animations: [{ style: "left", start: subMenu.style.left, end: -parseInt(subMenu.offsetWidth), postfix: "px" }] });
            this_.animate(menu, { duration: dur, animations: [{ style: "left", start: menu.style.left, end: 0, postfix: "px" }] });
            this_.animate(parentMenu, { duration: dur, animations: [{ style: "width", start: parentMenu.style.width, end: parseInt(menu.offsetWidth), postfix: "px" }] });
        }
    }
    
    var menu = createMenu();
    parentMenu.innerContent = menu;
    parentMenu.style.height = window.innerHeight + "px";
    parentMenu.style.left = -window.innerWidth + "px";
    parentMenu.style.width = window.innerWidth + "px";
    parentMenu.lastTouches = [{x:0, y:0, time:0}, {x:0, y:0, time:0}];

    parentMenu.changeVisibleState = function (state, duration, forceAnimate) {
        duration = duration || 200;
        var dPanel = this.jsObject.options.disabledPanels[5];
        if (state && (!parentMenu.visible || forceAnimate)) {
            if (!menu.subMenu) {
                this.style.width = this.innerContent.offsetWidth + "px";                
            } else {
                this.style.width = menu.subMenu.offsetWidth + "px";
            }
            dPanel.style.display = "";
            var mmenu = this;
            setTimeout(function () {
                this_.animate(dPanel, { duration: duration, animations: [{ style: "opacity", start: dPanel.style.opacity || 0, end: 0.5, postfix: "" }] });
                this_.animate(mmenu, { duration: duration, animations: [{ style: "left", start: mmenu.style.left, end: 0, postfix: "px" }] });
            }, 0);
            parentMenu.visible = true;
        } else if (!state && (parentMenu.visible || forceAnimate)) {
            var endd = menu.subMenu ? -menu.offsetWidth : -parseInt(this.offsetWidth);
            this.jsObject.animate(this, { duration: duration, animations: [{ style: "left", start: this.style.left, end: endd - 10, postfix: "px" }] });
            this.jsObject.animate(dPanel, { duration: duration, animations: [{ style: "opacity", start: dPanel.style.opacity || 0, end: 0, postfix: "", finish:
                function () { dPanel.style.display = "none";}}] });
            if (menu.subMenu) {
                menu.subMenu.style.left = -menu.subMenu.offsetWidth + "px";
                menu.style.left = 0;
                parentMenu.style.width =menu.offsetWidth + "px";
                menu.subMenu = null;
            }
            parentMenu.visible = false;
            this.jsObject.options.disabledPanels[1].changeVisibleState(false);
        }
    }

    
    var touchStart = function (this_, e) {
        this_.startX = parseInt(e.changedTouches[0].clientX);
        this_.startY = parseInt(e.changedTouches[0].clientY);
    }

    var touchEnd = function (this_, e) {
        //console.log(e.changedTouches[0].clientX + "<========>" + e.changedTouches[0].clientY);
        //console.log("time:" + (new Date().getTime() - this_.lastTouches[1].time));
        if (!menu.btnTouched) {
            var dX = this_.lastTouches[1].x - this_.lastTouches[0].x;
            var dY = Math.abs(this_.lastTouches[1].y - this_.lastTouches[0].y);
            if (dX < 0 && new Date().getTime() - this_.lastTouches[1].time <= 9 && dY < 15) {
                parentMenu.changeVisibleState(false, 100, true);
            } else {
                parentMenu.changeVisibleState(-parseInt(this_.style.left) < parseInt(this_.style.width) / 2, 80, true);
            }
        }
        menu.btnTouched = false;
        this_.startX = null;
    }

    parentMenu.ontouchstart = function (e) {touchStart(this, e);}
    parentMenu.ontouchenter = function (e) { touchStart(this, e); }
    parentMenu.ontouchmove = function (e) {
        var posX = parseInt(e.changedTouches[0].clientX);
        if (!this.startX || posX - this.startX > 0) {
            this.startX = posX;
        }
        this.style.left = Math.min(0, posX - this.startX) + "px";
        this_.options.disabledPanels[5].style.opacity = 0.5 + Math.min(0, posX - this.startX) / this.offsetWidth * 0.5;
        this.lastTouches.shift();
        this.lastTouches.push({ x: posX, y: parseInt(e.changedTouches[0].clientY), time: new Date().getTime() });
        //console.log(posX + "<=>" + e.changedTouches[0].clientY);
    }
    parentMenu.ontouchend = function (e) { touchEnd(this, e); }
    parentMenu.ontouchleave = function (e) { touchEnd(this, e); }
    parentMenu.ontouchcancel = function (e) { touchEnd(this, e); }    

    var t = document.createElement("div");
    t.style.height = "10px";
    menu.appendChild(t);
    for (var i in items) {
        var item = items[i];
        var button;
        if (item[0] == "OnePageWholeReport") {
            button = this.BigMaterialButton("OnePage", null, this.loc.A_WebViewer.OnePage, "OnePage.png", null, true);
            button.wholeReport = this.BigMaterialButton("WholeReport", null, this.loc.A_WebViewer.WholeReport, "WholeReport.png", null, true, true);
            var table = this.CreateHTMLTable();
            table.style.width = "100%";
            table.addCell(button).style.width = "50%";
            table.addCell(button.wholeReport).style.width = "50%";
            if (this.options.menuViewMode == "OnePage") {
                button.setSelected(true);
                button.setEnabled(false);
            } else {
                button.wholeReport.setSelected(true);
                button.wholeReport.setEnabled(false);
            }
            menu.wholeReport = button.wholeReport;
            menu.onePage = button;
            menu.appendChild(table);
        } else {
            button = this.BigMaterialButton(item[0], null, item[1], item[2], null, true);
            menu.appendChild(button);
        }
        if (item[0] == "Editor") {
            button.style.display = "none";
            this_.options.buttons.Editor = button;
        }
        if (item.length == 5) {
            var subMenu = createMenu();
            button.subMenu = subMenu;
            for (var j in item[4]) {
                var subItem = item[4][j];
                var btn = this.BigMaterialButton(subItem[0], null, subItem[1], subItem[2], null, true);
                subMenu.appendChild(btn);
                btn.action = function () {
                    menu.btnTouched = true;
                    this_.options.mainMaterialMenu.changeVisibleState(false, 50, true);
                    this_.options.materialPanel.hide(50);
                    if (this.name.indexOf("ExportSettings") > 0) {
                        if (this_.options.showExportDialog) {
                            var viewerExportForm = this_.options.forms.exportForm || this_.InitializeExportForm("exportForm");
                            viewerExportForm.show(this.name);
                        }
                        else {
                            this_.exportEvent(menuItem.key);
                        }
                    } else {
                        this_.actionEvent(this.name);
                    }
                }
            }
            var backBtn = this.BigMaterialButton("back" + item[0], null, "", "Back.png", null, true);
            subMenu.appendChild(backBtn);
            backBtn.action = function () {
                showSubMenu(null, false);
            }

            subMenu.style.left = -window.innerWidth + "px";
            subMenu.style.width = subMenu.offsetWidth + "px";
            button.action = function () {
                showSubMenu(this.subMenu, true);
            }
        } else {
            button.action = function () {
                menu.btnTouched = true;
                this_.options.mainMaterialMenu.changeVisibleState(false, 50, true);
                this_.options.materialPanel.hide(50);
                this_.actionEvent(this.name);
                if (this.name == "OnePage") {
                    menu.wholeReport.setSelected(false);
                    menu.wholeReport.setEnabled(true);
                    this.setSelected(true);
                    this.setEnabled(false);
                } 
            }
            if (button.wholeReport) {
                button.wholeReport.action = function () {
                    menu.btnTouched = true;
                    this_.options.mainMaterialMenu.changeVisibleState(false, 50, true);
                    this_.options.materialPanel.hide(50);
                    this_.actionEvent(this.name);
                    menu.onePage.setSelected(false);
                    menu.onePage.setEnabled(true);
                    this.setSelected(true);
                    this.setEnabled(false);
                }
            }
        }
    }
    menu.style.width = menu.offsetWidth + "px";

    return parentMenu;
}

StiMobileViewer.prototype.Separator = function () {
    var separator = document.createElement("div");    
    separator.className = "stiMobileViewerFormSeparator";
    
    return separator;
}