﻿
StiMobileViewer.prototype.InitializePrintMenu = function () {
    var items = [];
    items.push(this.Item("PrintPdf", this.loc.A_WebViewer.PrintToPdf, "PrintPdf.png", "PrintPdf"));
    items.push(this.Item("PrintWithPreview", this.loc.A_WebViewer.PrintWithPreview, "PrintWithPreview.png", "PrintWithPreview"));
    items.push(this.Item("PrintWithoutPreview", this.loc.A_WebViewer.PrintWithoutPreview, "PrintWithoutPreview.png", "PrintWithoutPreview"));

    var printMenu = this.VerticalMenu("printMenu", this.options.toolbar.controls["Print"], "Down", items, this.GetStyles("MenuStandartItem"));

    printMenu.action = function (menuItem) {
        printMenu.changeVisibleState(false);
        printMenu.jsObject.actionEvent(menuItem.key);
    }
}