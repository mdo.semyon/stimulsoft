﻿
StiMobileViewer.prototype.InitializeZoomMenu = function () {
    var items = [];
    var zoomItems = ["25", "50", "75", "100", "150", "200"];
    for (var i = 0; i < zoomItems.length; i++) {
        items.push(this.Item("Zoom" + zoomItems[i], zoomItems[i] + "%", "SelectedItem.png", "Zoom" + zoomItems[i]));
    }

    if (!this.options.cloudMode && !this.options.designerMode) {
        items.push("separator1");
        items.push(this.Item("ZoomOnePage", this.loc.Zoom.PageHeight, "ZoomOnePage.png", "ZoomOnePage"));
        items.push(this.Item("ZoomPageWidth", this.loc.FormViewer.ZoomPageWidth, "ZoomPageWidth.png", "ZoomPageWidth"));
    }

    var zoomMenu = this.VerticalMenu("zoomMenu", this.options.toolbar.controls["Zoom"],
        this.options.cloudMode || this.options.designerMode ? "Up" : "Down", items, this.GetStyles("MenuStandartItem"), this.options.cloudMode || this.options.designerMode);

    zoomMenu.action = function (menuItem) {
        zoomMenu.changeVisibleState(false);
        if (this.jsObject.options.cloudMode || this.jsObject.options.designerMode) {
            this.jsObject.options.toolbar.controls.ZoomOnePage.setSelected(false);
            this.jsObject.options.toolbar.controls.ZoomPageWidth.setSelected(false);
        }
        zoomMenu.jsObject.actionEvent(menuItem.key);
    }
}