﻿
StiMobileViewer.prototype.InitializeCloudSaveMenu = function (parentButton) {
    var items = [];
    items.push(this.Item("reportFromFile", this.loc.PropertyMain.File, "CloudIcons.BigReportFromFile.png", "ReportFromFile"));
    items.push(this.Item("sendEmail", this.loc.FormViewer.SendEMail.replace("...", ""), "CloudIcons.BigSendEmail.png", "SendEmail"));
    
    var menu = this.VerticalMenu("cloudSaveMenu", parentButton, "Down", items, this.GetStyles("MenuStandartItem"))
    var jsObject = this;

    menu.items.reportFromFile.style.height = "40px";
    menu.items.sendEmail.style.height = "40px";

    menu.action = function(menuItem) {        
        this.changeVisibleState(false);
        jsObject.InitializeCloudSaveForm(menuItem.key == "SendEmail").show();
    }
    
    return menu; 
}