﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Export;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Report.Mobile
{
    public class StiWebMatrix
    {
        #region Properties
        private StiCell[,] cells;
        public StiCell[,] Cells
        {
            get
            {
                return cells;
            }
        }

        private StiCell[,] cellsMap;
        public StiCell[,] CellsMap
        {
            get
            {
                return cellsMap;
            }
        }


        private double totalHeight = 0;
        public double TotalHeight
        {
            get
            {
                return totalHeight;
            }
        }


        private double totalWidth = 0;
        public double TotalWidth
        {
            get
            {
                return totalWidth;
            }
        }


        private ArrayList styles = new ArrayList();
        public ArrayList Styles
        {
            get
            {
                return styles;
            }
        }


        private SortedList coordX = new SortedList();
        public SortedList CoordX
        {
            get
            {
                return this.coordX;
            }
        }


        private SortedList coordY = new SortedList();
        public SortedList CoordY
        {
            get
            {
                return this.coordY;
            }
        }

        #endregion

        #region Methods
        private double Round(double value)
        {
            return (int)value;
        }

        private void AddCoord(RectangleD rect)
        {
            AddCoord(rect.Left, rect.Top + totalHeight);
            AddCoord(rect.Right, rect.Bottom + totalHeight);
        }

        private void AddCoord(double x, double y)
        {
            x = Round(x);
            y = Round(y);
            coordX[x] = x;
            coordY[y] = y;
        }

        public void PrepareTable()
        {
            for (int rowIndex = 1; rowIndex < coordY.Count; rowIndex++)
            {
                double rowHeight = (double)coordY.GetByIndex(rowIndex) - (double)coordY.GetByIndex(rowIndex - 1);
                rowHeight = Round(rowHeight * 1.25F);

                if (rowHeight > 300f)
                {
                    double endHeight = (double)coordY.GetByIndex(rowIndex - 1) + 300f;
                    AddCoord(0f, endHeight);
                }
            }
        }

        public Rectangle GetRange(RectangleD rect)
        {
            double ll = Round(rect.Left);
            double tt = Round(rect.Top + TotalHeight);
            double rr = Round(rect.Right);
            double bb = Round(rect.Bottom + TotalHeight);

            int left = coordX.IndexOfValue(ll);
            int top = coordY.IndexOfValue(tt);
            int right = coordX.IndexOfValue(rr);
            int bottom = coordY.IndexOfValue(bb);

            return new Rectangle(left, top, right - left, bottom - top);
        }

        private ArrayList createdCells = new ArrayList();

        private void RenderComponent(StiComponent component)
        {
            Rectangle rect = GetRange(component.Page.Unit.ConvertToHInches(component.DisplayRectangle));

            if (rect.Left != -1)
            {
                StiCell cell = new StiCell();
                createdCells.Add(cell);

                cell.Component = component;
                Cells[rect.Top, rect.Left] = cell;
                cell.Left = rect.Left;
                cell.Top = rect.Top;

                cell.Width = rect.Width - 1;
                cell.Height = rect.Height - 1;

                for (int indexX = rect.X; indexX < rect.Right; indexX++)
                {
                    for (int indexY = rect.Y; indexY < rect.Bottom; indexY++)
                    {
                        CellsMap[indexY, indexX] = cell;
                    }
                }
            }
        }

        private Rectangle GetCellRectangle(int startX, int startY, StiCell cell)
        {
            int rectLeft = startX;
            int rectTop = startY;
            int rectRight = startX;
            int rectBottom = startY;

            #region Detect left corner
            while (rectLeft <= cell.Width && CellsMap[rectTop + cell.Top, rectLeft + cell.Left] != cell)
            {
                rectLeft++;
            }

            if (rectLeft > cell.Width) return Rectangle.Empty;
            #endregion

            rectRight = rectLeft;

            #region Detect right corner
            while (rectRight <= cell.Width && CellsMap[rectTop + cell.Top, rectRight + cell.Left] == cell)
            {
                rectRight++;
            }
            if (rectLeft == rectRight) return Rectangle.Empty;
            #endregion

            #region Detect left bottom and right bottom corners
            bool fail = false;
            rectBottom = rectTop + 1;
            while (rectBottom <= cell.Height && fail == false)
            {
                if (rectLeft > 0 && CellsMap[cell.Top + rectBottom, cell.Left + rectLeft - 1] == cell)
                {
                    fail = true;
                    break;
                }

                if (rectRight <= cell.Width && CellsMap[rectBottom + cell.Top, cell.Left + rectRight] == cell)
                {
                    fail = true;
                    break;
                }

                for (int index = rectLeft; index < rectRight; index++)
                {
                    if (CellsMap[rectBottom + cell.Top, index + cell.Left] != cell)
                    {
                        fail = true;
                        break;
                    }
                }
                if (!fail) rectBottom++;
            }
            #endregion

            return new Rectangle(rectLeft, rectTop, rectRight - rectLeft, rectBottom - rectTop);
        }

        private void CutRectangleFromCellsMap(Rectangle cellRect, StiCell cell)
        {
            for (int mapX = cellRect.Left; mapX < cellRect.Right; mapX++)
            {
                for (int mapY = cellRect.Top; mapY < cellRect.Bottom; mapY++)
                {
                    CellsMap[cell.Top + mapY, cell.Left + mapX] = null;
                }
            }
        }
        #endregion

        public StiWebMatrix(StiContainer cont)
        {
            StiPage page = cont.Page;
            RectangleD rect2 = new RectangleD(0, 0, cont.ClientRectangle.Width, cont.ClientRectangle.Height);
            AddCoord(page.Unit.ConvertToHInches(rect2));

            foreach (StiComponent component in cont.Components)
            {
                RectangleD rect = page.Unit.ConvertToHInches(component.DisplayRectangle);
                AddCoord(rect);
            }
            totalHeight += Math.Round(page.Unit.ConvertToHInches(cont.Height), 0);
            totalWidth = Math.Max(totalWidth, page.Unit.ConvertToHInches(cont.Width));

            cells = new StiCell[coordY.Count, coordX.Count];
            cellsMap = new StiCell[coordY.Count, coordX.Count];

            totalHeight = 0;

            foreach (StiComponent component in cont.Components)
            {
                RenderComponent(component);
            }
            totalHeight += Math.Round((page.Unit.ConvertToHInches(cont.Height)), 0);


            #region Process interseced cells
            IList listX = coordX.GetValueList();
            IList listY = coordY.GetValueList();

            foreach (StiCell cell in createdCells)
            {
                if (cell.Width != 0 || cell.Height != 0)
                {
                    int startX = 0;
                    int startY = 0;

                    Rectangle cellRect = GetCellRectangle(startX, startY, cell);

                    if (cellRect.Width == (cell.Width + 1) && cellRect.Height == (cell.Height + 1))
                    {
                        CutRectangleFromCellsMap(cellRect, cell);
                        continue;
                    }

                    ArrayList newCells = new ArrayList();


                    for (int y = 0; y <= cell.Height; y++)
                    {
                        for (int x = 0; x <= cell.Width; )
                        {
                            cellRect = GetCellRectangle(x, y, cell);

                            if (cellRect.Width == 0)
                            {
                                x = cell.Width + 1;
                                continue;
                            }

                            CutRectangleFromCellsMap(cellRect, cell);

                            #region Create new cell
                            StiCell newCell = cell.Clone() as StiCell;

                            newCell.Left = cell.Left + cellRect.X;
                            newCell.Top = cell.Top + cellRect.Y;
                            newCell.Width = cellRect.Width - 1;
                            newCell.Height = cellRect.Height - 1;

                            Cells[newCell.Top, newCell.Left] = newCell;

                            newCells.Add(newCell);
                            #endregion

                            x += cellRect.Width;
                        }
                    }
                }
            }
            #endregion
        }
    }
}
