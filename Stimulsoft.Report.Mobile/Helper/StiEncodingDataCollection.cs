﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Collections;
using System.Text;

namespace Stimulsoft.Report.Mobile
{
    internal class StiEncodingDataCollection
    {        
        private static Hashtable Item(string value, string key)
        {
            Hashtable item = new Hashtable();
            item["key"] = key;
            item["value"] = value;

            return item;
        }

        public static ArrayList GetItems()
        {
            ArrayList items = new ArrayList();            
            items.Add(Item(Encoding.Default.EncodingName, Encoding.Default.CodePage.ToString()));
            items.Add(Item(Encoding.ASCII.EncodingName, Encoding.ASCII.CodePage.ToString()));
            items.Add(Item(Encoding.BigEndianUnicode.EncodingName, Encoding.BigEndianUnicode.CodePage.ToString()));
            items.Add(Item(Encoding.Unicode.EncodingName, Encoding.Unicode.CodePage.ToString()));
            items.Add(Item(Encoding.UTF7.EncodingName, Encoding.UTF7.CodePage.ToString()));
            items.Add(Item(Encoding.UTF8.EncodingName, Encoding.UTF8.CodePage.ToString()));
            items.Add(Item(Encoding.GetEncoding(1250).EncodingName, "1250"));
            items.Add(Item(Encoding.GetEncoding(1251).EncodingName, "1251"));
            items.Add(Item(Encoding.GetEncoding(1252).EncodingName, "1252"));
            items.Add(Item(Encoding.GetEncoding(1253).EncodingName, "1253"));
            items.Add(Item(Encoding.GetEncoding(1254).EncodingName, "1254"));
            items.Add(Item(Encoding.GetEncoding(1255).EncodingName, "1255"));
            items.Add(Item(Encoding.GetEncoding(1256).EncodingName, "1256"));
          
            return items;
        }
    }
}