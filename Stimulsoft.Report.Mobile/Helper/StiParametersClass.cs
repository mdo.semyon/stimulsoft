﻿
//#define SERVER
#define MobileViewer

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using System.Globalization;
using Stimulsoft.Report.Engine;
using System.Xml;
using System.Linq;

#if SERVER
using Stimulsoft.Server;
using Stimulsoft.Server.Objects;
using Stimulsoft.Report.Design;
using System.ComponentModel;
#endif

namespace Stimulsoft.Report.Mobile
{
    public class StiParametersClass
    {
        #region Fields
        private static CultureInfo en_us_culture = CultureInfo.CreateSpecificCulture("en-US");
        #endregion

        /// <summary>
        /// Convert XML object to Hashtable
        /// </summary>
        public static Hashtable ToHashtable(XmlNode xml, bool decodeName)
        {
            Hashtable obj = new Hashtable();
            if (xml == null) return obj;
            foreach (XmlNode node in xml)
            {
                if (node.FirstChild == null || node.FirstChild.NodeType == XmlNodeType.Text)
                {
                    if (node.Attributes["isRange"] != null)
                    {
                        Hashtable range = new Hashtable();
                        string[] values = node.InnerText.Split(',');
                        range["from"] = XmlConvert.DecodeName(values[1]);
                        range["to"] = XmlConvert.DecodeName(values[2]);
                        obj[node.Name] = range;
                    }
                    else if (node.Attributes["isNull"] != null) obj[node.Name] = null;
                    else if (decodeName) obj[node.Name] = XmlConvert.DecodeName(node.InnerText);
                    else obj[node.Name] = node.InnerText;
                }
                else if (node.Attributes["isList"] != null)
                {
                    ArrayList list = new ArrayList();
                    foreach (XmlNode listNode in node) list.Add(XmlConvert.DecodeName(listNode.InnerText));
                    obj[node.Name] = list;
                }
                else
                    obj[node.Name] = ToHashtable(node, decodeName);
            }

            return obj;
        }

        /// <summary>
        /// Filling the list of variables initialized from the database columns
        /// </summary>
        public static void FillDialogInfoItems(StiReport report)
        {
            bool isColumnsInitializationTypeItems = false;
            foreach (StiVariable variable in report.Dictionary.Variables)
            {
                if (variable.RequestFromUser && variable.DialogInfo.ItemsInitializationType == StiItemsInitializationType.Columns &&
                        (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0 ||
                         variable.DialogInfo.Values == null || variable.DialogInfo.Values.Length == 0))
                {
                    isColumnsInitializationTypeItems = true;
                    break;
                }
            }

            if (isColumnsInitializationTypeItems)
            {
                report.Dictionary.Connect();
                StiVariableHelper.FillItemsOfVariables(report);
                report.Dictionary.Disconnect();
            }
        }

        private static string GetVariableAlias(StiVariable variable)
        {
            if (variable.Name == variable.Alias)
            {
                return variable.Name;
            }
            else
            {
                if (!string.IsNullOrEmpty(variable.Alias))
                {
                    return variable.Alias;
                }
                else
                {
                    return variable.Name;
                }
            }
        }

        private static ArrayList GetItems(StiVariable variable)
        {
            ArrayList items = new ArrayList();
            var valueBinding = variable.DialogInfo.BindingVariable != null ? variable.DialogInfo.BindingVariable.Value : null;
            var index = 0;

            if (variable.DialogInfo.Keys != null && variable.DialogInfo.Keys.Length != 0)
            {
                List<StiDialogInfoItem> itemsVariable = variable.DialogInfo.GetDialogInfoItems(variable.Type);
                Hashtable bindingValues = new Hashtable();

                foreach (StiDialogInfoItem itemVariable in itemsVariable)
                {

                    if (string.IsNullOrEmpty(valueBinding) || valueBinding == Convert.ToString(itemVariable.ValueBinding))
                    {
                        Hashtable item = new Hashtable();
                        item["value"] = itemVariable.Value;
                        item["key"] = (itemVariable.KeyObject != null) ? itemVariable.KeyObject.ToString() : null;
                        item["keyTo"] = (itemVariable.KeyObjectTo != null) ? itemVariable.KeyObjectTo.ToString() : null;

                        if (variable.Type == typeof(DateTime) || variable.Type == typeof(DateTime?) ||
                            variable.Type == typeof(DateTimeRange) || variable.Type == typeof(DateTimeList))
                        {
                            if (itemVariable.KeyObject != null) item["key"] = GetDateTimeObject(itemVariable.KeyObject);
                            if (itemVariable.KeyObjectTo != null) item["keyTo"] = GetDateTimeObject(itemVariable.KeyObjectTo);
                        }
                        
                        if (string.IsNullOrEmpty(valueBinding) || item["key"] == null || bindingValues[item["key"]] == null)
                        {
                            items.Add(item);
                        }

                        if (!string.IsNullOrEmpty(valueBinding))
                        {
                            bindingValues[item["key"]] = true;
                        }
                    }

                    index++;
                }
            }

            return index > 0 ? items : null;
        }

        private static Hashtable GetDateTimeObject(object value)
        {
            DateTime dateValue = DateTime.Now;
            if ((value is DateTime || value is DateTime?) && value != null)
                dateValue = (DateTime)value;
            if (value is string && value as string != string.Empty)
            {
                if (!DateTime.TryParse(value as string, en_us_culture, DateTimeStyles.None, out dateValue))
                    DateTime.TryParse(value as string, CultureInfo.CurrentCulture, DateTimeStyles.None, out dateValue);
            }

            Hashtable dateTime = new Hashtable();
            dateTime["year"] = dateValue.Year;
            dateTime["month"] = dateValue.Month;
            dateTime["day"] = dateValue.Day;
            dateTime["hours"] = dateValue.Hour;
            dateTime["minutes"] = dateValue.Minute;
            dateTime["seconds"] = dateValue.Second;
            if (value == null) dateTime["isNull"] = true;

            return dateTime;
        }

        private static string GetTimeSpanString(string value)
        {
            TimeSpan time;
            if (TimeSpan.TryParse(value, out time))
            {
                string hours = time.Hours < 10 ? "0" + time.Hours : time.Hours.ToString();
                string minutes = time.Minutes < 10 ? "0" + time.Minutes : time.Minutes.ToString();
                string seconds = time.Seconds < 10 ? "0" + time.Seconds : time.Seconds.ToString();

                return (time.Days != 0)
                    ? string.Format("{0}.{1}:{2}:{3}", time.Days, hours, minutes, seconds)
                    : string.Format("{0}:{1}:{1}", hours, minutes, seconds);
            }

            return value;
        }

        private static string GetBasicType(Type variableType)
        {
            if (StiTypeFinder.FindType(variableType, typeof(Range))) return "Range";
            if (StiTypeFinder.FindInterface(variableType, typeof(IStiList))) return "List";
            if (variableType.Name.StartsWith("Nullable")) return "NullableValue";

            return "Value";
        }

        private static string GetBasicType(StiVariable variable)
        {
            return GetBasicType(variable.Type);
        }

        private static string GetType(Type variableType)
        {
            if (variableType == typeof(string) || variableType == typeof(StringList) || variableType == typeof(StringRange)) return "String";
            if (variableType == typeof(char) || variableType == typeof(char?) || variableType == typeof(CharRange) || variableType == typeof(CharList)) return "Char";
            if (variableType == typeof(bool) || variableType == typeof(bool?) || variableType == typeof(BoolList)) return "Bool";
            if (variableType == typeof(DateTime) || variableType == typeof(DateTime?) || variableType == typeof(DateTimeList) || variableType == typeof(DateTimeRange)) return "DateTime";
            if (variableType == typeof(TimeSpan) || variableType == typeof(TimeSpan?) || variableType == typeof(TimeSpanList) || variableType == typeof(TimeSpanRange)) return "TimeSpan";
            if (variableType == typeof(Guid) || variableType == typeof(Guid?) || variableType == typeof(GuidList) || variableType == typeof(GuidRange)) return "Guid";
            if (variableType == typeof(System.Drawing.Image) || variableType == typeof(System.Drawing.Bitmap)) return "Image";
            if (variableType == typeof(float) || variableType == typeof(float?) || variableType == typeof(FloatList) || variableType == typeof(FloatRange)) return "Float";
            if (variableType == typeof(double) || variableType == typeof(double?) || variableType == typeof(DoubleList) || variableType == typeof(DoubleRange)) return "Double";
            if (variableType == typeof(Decimal) || variableType == typeof(Decimal?) || variableType == typeof(DecimalList) || variableType == typeof(DecimalRange)) return "Decimal";
            if (variableType == typeof(int) || variableType == typeof(int?) || variableType == typeof(IntList) || variableType == typeof(IntRange)) return "Int";
            if (variableType == typeof(uint) || variableType == typeof(uint?)) return "Uint";
            if (variableType == typeof(short) || variableType == typeof(short?) || variableType == typeof(ShortList) || variableType == typeof(ShortRange)) return "Short";
            if (variableType == typeof(ushort) || variableType == typeof(ushort?)) return "Ushort";
            if (variableType == typeof(long) || variableType == typeof(long?) || variableType == typeof(LongList) || variableType == typeof(LongRange)) return "Long";
            if (variableType == typeof(ulong) || variableType == typeof(ulong?)) return "Ulong";
            if (variableType == typeof(byte) || variableType == typeof(byte?) || variableType == typeof(ByteList) || variableType == typeof(ByteRange)) return "Byte";
            if (variableType == typeof(sbyte) || variableType == typeof(sbyte?)) return "Sbyte";

            return string.Empty;
        }

        private static string GetType(StiVariable variable)
        {
            return GetType(variable.Type);
        }

        /// <summary>
        /// Assigning sent by the client parameters to the report
        /// </summary>
        public static void ApplyReportParameters(StiReport report, XmlNode parameters)
        {
            Hashtable parametersObject = ToHashtable(parameters, true);
            ApplyReportParameters(report, parametersObject);
        }

        /// <summary>
        /// Assigning sent by the client parameters to the report
        /// </summary>
        public static void ApplyReportParameters(StiReport report, Hashtable parameters)
        {
            if (report.CalculationMode == StiCalculationMode.Interpretation)
            {
                foreach (StiVariable variable in report.Dictionary.Variables)
                {
                    report[variable.Name] = variable.ValueObject;
                }
            }
            
            foreach (string key in parameters.Keys)
            {
                StiVariable variable = report.Dictionary.Variables[key];
                if (variable != null) SetVariableValue(report, key, parameters[key], variable);
            }

            report.IsRendered = false;
        }

        /// <summary>
        /// Assigning sent by the client parameters to the report
        /// </summary>
        public static void ApplyReportBindingVariables(StiReport report, Hashtable values)
        {
            foreach (string key in values.Keys)
            {
                foreach (StiVariable variable in report.Dictionary.Variables)
                {
                    if (variable.Name == key) variable.Value = Convert.ToString(values[key]);
                    if (variable.DialogInfo.BindingVariable != null && variable.DialogInfo.BindingVariable.Name == key) variable.DialogInfo.BindingVariable.Value = Convert.ToString(values[key]);
                }
            }
        }

        /// <summary>
        /// Assigning the specified parameter to the report
        /// </summary>
        private static void SetVariableValue(StiReport report, string paramName, object paramValue, StiVariable variable)
        {
            string decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            #region Restore values

            string stringValue = null;
            Hashtable values = null;
            ArrayList array = null;
            
            if (paramValue != null)
            {
                if (paramValue is Hashtable) values = (Hashtable)paramValue;
                else if (paramValue is ArrayList) array = (ArrayList)paramValue;
                else stringValue = paramValue.ToString();
            }
            #endregion

            #region System types

            if (variable.Type == typeof(string))
            {
                report[paramName] = paramValue;
            }
            else if (variable.Type == typeof(float) || variable.Type == typeof(float?))
            {
                float value = 0;
                float.TryParse(stringValue.Replace(".", ",").Replace(",", decimalSeparator), out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(double) || variable.Type == typeof(double?))
            {
                double value = 0;
                double.TryParse(stringValue.Replace(".", ",").Replace(",", decimalSeparator), out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(decimal) || variable.Type == typeof(decimal?))
            {
                decimal value = 0;
                decimal.TryParse(stringValue.Replace(".", ",").Replace(",", decimalSeparator), out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(int) || variable.Type == typeof(int?))
            {
                int value = 0;
                int.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(uint) || variable.Type == typeof(uint?))
            {
                uint value = 0;
                uint.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(short) || variable.Type == typeof(short?))
            {
                short value = 0;
                short.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(ushort) || variable.Type == typeof(ushort?))
            {
                ushort value = 0;
                ushort.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(long) || variable.Type == typeof(long?))
            {
                long value = 0;
                long.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(ulong) || variable.Type == typeof(ulong?))
            {
                ulong value = 0;
                ulong.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(byte) || variable.Type == typeof(byte?))
            {
                byte value = 0;
                byte.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(sbyte) || variable.Type == typeof(sbyte?))
            {
                sbyte value = 0;
                sbyte.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(char) || variable.Type == typeof(char?))
            {
                char value = ' ';
                char.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(bool) || variable.Type == typeof(bool?))
            {
                bool value = false;
                bool.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(DateTime) || variable.Type == typeof(DateTime?))
            {
                DateTime value = DateTime.Now;
                DateTime.TryParse(stringValue, en_us_culture, DateTimeStyles.None, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(TimeSpan) || variable.Type == typeof(TimeSpan?))
            {
                TimeSpan value = TimeSpan.Zero;
                TimeSpan.TryParse(stringValue, out value);
                report[paramName] = value;
            }
            else if (variable.Type == typeof(Guid) || variable.Type == typeof(Guid?))
            {
                Guid variableGuid;
                try
                {
                    variableGuid = new Guid(stringValue);
                }
                catch
                {
                    variableGuid = Guid.Empty;
                }
                report[paramName] = variableGuid;
            }

            #endregion

            #region Ranges

            else if (variable.Type == typeof(StringRange))
            {
                report[paramName] = new StringRange((string)values["from"], (string)values["to"]);
            }
            else if (variable.Type == typeof(FloatRange))
            {
                float valueFrom = 0;
                float valueTo = 0;
                float.TryParse(((string)values["from"]).Replace(",", decimalSeparator), out valueFrom);
                float.TryParse(((string)values["to"]).Replace(".", ",").Replace(",", decimalSeparator), out valueTo);
                report[paramName] = new FloatRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(DoubleRange))
            {
                double valueFrom = 0;
                double valueTo = 0;
                double.TryParse(((string)values["from"]).Replace(".", ",").Replace(",", decimalSeparator), out valueFrom);
                double.TryParse(((string)values["to"]).Replace(".", ",").Replace(",", decimalSeparator), out valueTo);
                report[paramName] = new DoubleRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(DecimalRange))
            {
                decimal valueFrom = 0;
                decimal valueTo = 0;
                decimal.TryParse(((string)values["from"]).Replace(".", ",").Replace(",", decimalSeparator), out valueFrom);
                decimal.TryParse(((string)values["to"]).Replace(".", ",").Replace(",", decimalSeparator), out valueTo);
                report[paramName] = new DecimalRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(IntRange))
            {
                int valueFrom = 0;
                int valueTo = 0;
                int.TryParse(((string)values["from"]), out valueFrom);
                int.TryParse(((string)values["to"]), out valueTo);
                report[paramName] = new IntRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(ShortRange))
            {
                short valueFrom = 0;
                short valueTo = 0;
                short.TryParse(((string)values["from"]), out valueFrom);
                short.TryParse(((string)values["to"]), out valueTo);
                report[paramName] = new ShortRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(LongRange))
            {
                long valueFrom = 0;
                long valueTo = 0;
                long.TryParse(((string)values["from"]), out valueFrom);
                long.TryParse(((string)values["to"]), out valueTo);
                report[paramName] = new LongRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(ByteRange))
            {
                byte valueFrom = 0;
                byte valueTo = 0;
                byte.TryParse(((string)values["from"]), out valueFrom);
                byte.TryParse(((string)values["to"]), out valueTo);
                report[paramName] = new ByteRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(CharRange))
            {
                char valueFrom = ' ';
                char valueTo = ' ';
                char.TryParse(((string)values["from"]), out valueFrom);
                char.TryParse(((string)values["to"]), out valueTo);
                report[paramName] = new CharRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(DateTimeRange))
            {
                DateTime valueFrom = DateTime.Now;
                DateTime valueTo = DateTime.Now;
                DateTime.TryParse(((string)values["from"]), en_us_culture, DateTimeStyles.None, out valueFrom);
                DateTime.TryParse(((string)values["to"]), en_us_culture, DateTimeStyles.None, out valueTo);
                report[paramName] = new DateTimeRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(TimeSpanRange))
            {
                TimeSpan valueFrom = TimeSpan.Zero;
                TimeSpan valueTo = TimeSpan.Zero;
                TimeSpan.TryParse(((string)values["from"]), out valueFrom);
                TimeSpan.TryParse(((string)values["to"]), out valueTo);
                report[paramName] = new TimeSpanRange(valueFrom, valueTo);
            }
            else if (variable.Type == typeof(GuidRange))
            {
                Guid valueFrom = Guid.Empty;
                Guid valueTo = Guid.Empty;
                try
                {
                    valueFrom = new Guid(((string)values["from"]));
                    valueTo = new Guid(((string)values["to"]));
                }
                catch
                {
                }
                report[paramName] = new GuidRange(valueFrom, valueTo);
            }

            #endregion

            #region Lists

            else if (variable.Type == typeof(StringList))
            {
                StringList list = new StringList();
                foreach (object value in array) list.Add((string)value);
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray();
            }
            else if (variable.Type == typeof(FloatList))
            {
                FloatList list = new FloatList();
                foreach (object value in array)
                {
                    float listValue = 0;
                    float.TryParse(((string)value).Replace(".", ",").Replace(",", decimalSeparator), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(DoubleList))
            {
                DoubleList list = new DoubleList();
                foreach (object value in array)
                {
                    double listValue = 0;
                    double.TryParse(((string)value).Replace(".", ",").Replace(",", decimalSeparator), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(DecimalList))
            {
                DecimalList list = new DecimalList();
                foreach (object value in array)
                {
                    decimal listValue = 0;
                    decimal.TryParse(((string)value).Replace(".", ",").Replace(",", decimalSeparator), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(ByteList))
            {
                ByteList list = new ByteList();
                foreach (object value in array)
                {
                    byte listValue = 0;
                    byte.TryParse(((string)value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(ShortList))
            {
                ShortList list = new ShortList();
                foreach (object value in array)
                {
                    short listValue = 0;
                    short.TryParse(((string)value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(IntList))
            {
                IntList list = new IntList();
                foreach (object value in array)
                {
                    int listValue = 0;
                    int.TryParse(((string)value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(LongList))
            {
                LongList list = new LongList();
                foreach (object value in array)
                {
                    long listValue = 0;
                    long.TryParse(((string)value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(CharList))
            {
                CharList list = new CharList();
                foreach (object value in array)
                {
                    char listValue = ' ';
                    char.TryParse(((string)value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(DateTimeList))
            {
                DateTimeList list = new DateTimeList();
                foreach (object value in array)
                {
                    DateTime listValue;
                    DateTime.TryParse(((string)value), en_us_culture, DateTimeStyles.None, out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(TimeSpanList))
            {
                TimeSpanList list = new TimeSpanList();
                foreach (object value in array)
                {
                    TimeSpan listValue = TimeSpan.Zero;
                    TimeSpan.TryParse(((string)value), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(BoolList))
            {
                BoolList list = new BoolList();
                foreach (object value in array)
                {
                    bool listValue;
                    bool.TryParse(value.ToString(), out listValue);
                    list.Add(listValue);
                }
                report[paramName] = list;
                if (variable.DialogInfo.Keys == null || variable.DialogInfo.Keys.Length == 0) variable.DialogInfo.Keys = list.ToArray().Select(i => Convert.ToString(i)).ToArray();
            }
            else if (variable.Type == typeof(GuidList))
            {
                GuidList list = new GuidList();
                foreach (object value in array)
                {
                    Guid listValue;
                    try
                    {
                        listValue = new Guid(((string)value));
                    }
                    catch
                    {
                        listValue = Guid.Empty;
                    }

                    list.Add(listValue);
                }

                report[paramName] = list;
            }

            #endregion
        }

        public static Hashtable GetVariables(StiReport report)
        {
            FillDialogInfoItems(report);

            var variables = new Hashtable();
            var binding = new Hashtable();
            var index = 0;

            foreach (StiVariable variable in report.Dictionary.Variables)
            {
                if (variable.RequestFromUser)
                {
                    if (variable.DialogInfo.BindingVariable != null) binding[variable.DialogInfo.BindingVariable.Name] = true;

                    Hashtable var = new Hashtable();
                    var["name"] = variable.Name;
                    var["alias"] = GetVariableAlias(variable);
                    var["basicType"] = GetBasicType(variable);
                    var["type"] = GetType(variable);
                    var["allowUserValues"] = variable.DialogInfo.AllowUserValues;
                    var["dateTimeType"] = variable.DialogInfo.DateTimeType.ToString();

                    var items = GetItems(variable);
                    var["items"] = items;
                                        
                    if (variable.Selection == StiSelectionMode.Nothing) var["value"] = string.Empty;
                    else if (variable.Selection == StiSelectionMode.First) var["value"] = (items != null && items.Count > 0) ? ((Hashtable)items[0])["key"] : string.Empty;
                    else var["value"] = (variable.InitBy == StiVariableInitBy.Value) ? variable.Value : report[variable.Name];
                    var["key"] = (variable.InitBy == StiVariableInitBy.Value) ? variable.ValueObject : report[variable.Name];
                    var["keyTo"] = string.Empty;

                    // Find selected item by key
                    Hashtable selectedItem = null;
                    if (items != null && items.Count > 0)
                    {
                        selectedItem = new Hashtable();
                        selectedItem["value"] = "";
                        selectedItem["key"] = variable.Type == typeof(DateTime) ? GetDateTimeObject(DateTime.Now) : null;
                        selectedItem["keyTo"] = variable.Type == typeof(DateTime) ? GetDateTimeObject(DateTime.Now) : null;

                        if (variable.Selection == StiSelectionMode.First) selectedItem = (Hashtable)items[0];

                        string stringValue = Convert.ToString(var["value"]);
                        foreach (Hashtable item in items)
                        {
                            if (Convert.ToString(item["key"]) == stringValue)
                            {
                                selectedItem = item;
                                break;
                            }
                        }
                    }

                    // Value
                    if ((string)var["basicType"] == "Value" || (string)var["basicType"] == "NullableValue")
                    {
                        if (selectedItem != null)
                        {
                            var["key"] = selectedItem["key"];
                            var["value"] = selectedItem["value"];
                            if (variable.DialogInfo.AllowUserValues || var["value"] == null || (var["value"] is string && (string)var["value"] == "")) var["value"] = var["key"];

                            // Update binding variables for selected value
                            foreach (StiVariable bindingVariable in report.Dictionary.Variables)
                            {
                                if (bindingVariable.DialogInfo.BindingVariable != null && bindingVariable.DialogInfo.BindingVariable.Name == variable.Name)
                                {
                                    bindingVariable.DialogInfo.BindingVariable.ValueObject = var["key"];
                                }
                            }
                        }

                        if ((string)var["type"] == "DateTime") var["key"] = GetDateTimeObject(var["key"]);
                    }

                    // Range
                    if ((string)var["basicType"] == "Range")
                    {
                        if ((string)var["type"] == "DateTime")
                            var["key"] = GetDateTimeObject(variable.InitBy == StiVariableInitBy.Value ? ((Range)variable.ValueObject).FromObject : ((Range)report[variable.Name]).FromObject);
                        else
                            var["key"] = variable.InitBy == StiVariableInitBy.Value ? ((Range)variable.ValueObject).FromObject.ToString() : ((Range)report[variable.Name]).FromObject.ToString();

                        if ((string)var["type"] == "DateTime")
                            var["keyTo"] = GetDateTimeObject(variable.InitBy == StiVariableInitBy.Value ? ((Range)variable.ValueObject).ToObject : ((Range)report[variable.Name]).ToObject);
                        else
                            var["keyTo"] = variable.InitBy == StiVariableInitBy.Value ? ((Range)variable.ValueObject).ToObject.ToString() : ((Range)report[variable.Name]).ToObject.ToString();
                    }

                    variables[index.ToString()] = var;
                    index++;
                }
            }

            if (variables.Count > 0)
            {
                foreach (string name in binding.Keys)
                    foreach (Hashtable var in variables.Values)
                        if ((string)var["name"] == name) var["binding"] = true;

                return variables;
            }

            return null;
        }

#if SERVER  
        public static Hashtable GetVariablesFromInfo(List<StiReportParameterInfo> variablesInfo, StiReport report = null)
        {
            Hashtable variables = new Hashtable();
            int index = 0;

            foreach (var variableInfo in variablesInfo)
            {
                Hashtable var = new Hashtable();
                var["name"] = variableInfo.Name;
                var["alias"] = variableInfo.Alias;
                var["basicType"] = GetBasicType(variableInfo.Type);
                var["type"] = GetType(variableInfo.Type);
                var["allowUserValues"] = variableInfo.AllowUserValues;
                var["dateTimeType"] = variableInfo.DateTimeType.ToString();

                var items = GetItemsFromInfo(variableInfo);
                var["items"] = items;

                if (variableInfo.Selection == StiSelectionMode.Nothing) var["value"] = string.Empty;
                else if (variableInfo.Selection == StiSelectionMode.First) var["value"] = (items != null && items.Count > 0) ? ((Hashtable)items[0])["key"] : string.Empty;
                else var["value"] = (variableInfo.InitBy == StiVariableInitBy.Value) ? variableInfo.Value : (report != null ? report[variableInfo.Name] : variableInfo.Value);
                var["key"] = (variableInfo.InitBy == StiVariableInitBy.Value) ? var["value"] : report[variableInfo.Name];
                var["keyTo"] = string.Empty;

                // Find selected item by key
                Hashtable selectedItem = null;
                if (items != null && items.Count > 0)
                {
                    selectedItem = new Hashtable();
                    selectedItem["value"] = "";
                    selectedItem["key"] = variableInfo.Type == typeof(DateTime) ? GetDateTimeObject(DateTime.Now) : null;
                    selectedItem["keyTo"] = variableInfo.Type == typeof(DateTime) ? GetDateTimeObject(DateTime.Now) : null;

                    if (variableInfo.Selection == StiSelectionMode.First) selectedItem = (Hashtable)items[0];

                    string stringValue = Convert.ToString(var["value"]);
                    foreach (Hashtable item in items)
                    {
                        if (Convert.ToString(item["key"]) == stringValue)
                        {
                            selectedItem = item;
                            break;
                        }
                    }
                }

                // Value
                if ((string)var["basicType"] == "Value" || (string)var["basicType"] == "NullableValue")
                {
                    if (selectedItem != null)
                    {
                        var["key"] = selectedItem["key"];
                        var["value"] = selectedItem["value"];
                        if (variableInfo.AllowUserValues || var["value"] == null || (var["value"] is string && (string)var["value"] == "")) var["value"] = var["key"];
                        if ((string)var["type"] == "DateTime") var["key"] = GetDateTimeObject(var["key"]);
                    }
                    else
                    {
                        var["value"] = (variableInfo.Count > 0 && variableInfo.KeyList != null && variableInfo.ValueList != null)
                            ? (variableInfo.AllowUserValues
                                ? (variableInfo.Value != ""
                                    ? ((string)var["type"] == "TimeSpan"
                                        ? GetTimeSpanString(variableInfo.Value)
                                        : variableInfo.Value)
                                    : variableInfo.KeyList[0])
                                : (variableInfo.KeyList[0] != ""
                                    ? variableInfo.KeyList[0]
                                    : variableInfo.ValueList[0]))
                             : (string)var["type"] == "TimeSpan"
                                ? GetTimeSpanString(variableInfo.Value)
                                : (variableInfo.InitBy == StiVariableInitBy.Value) ? variableInfo.Value : (report != null ? report[variableInfo.Name] : variableInfo.Value);

                        if ((string)var["type"] == "DateTime")
                            var["key"] = GetDateTimeObject((variableInfo.InitBy == StiVariableInitBy.Value) ? variableInfo.Value : (report != null ? report[variableInfo.Name] : variableInfo.Value));
                        else
                            var["key"] = (variableInfo.KeyList != null && variableInfo.Count > 0) 
                                ? variableInfo.KeyList[0].ToString() 
                                : (variableInfo.InitBy == StiVariableInitBy.Value) ? variableInfo.Value : (report != null ? report[variableInfo.Name] : variableInfo.Value);
                    }
                }

                // Range
                if ((string)var["basicType"] == "Range")
                {
                    Range rangeValue = RangeConverter.StringToRange(variableInfo.Value);

                    if ((string)var["type"] == "DateTime") 
                        var["key"] = GetDateTimeObject(rangeValue.FromObject);
                    else 
                        var["key"] = rangeValue.FromObject.ToString();
                    
                    if ((string)var["type"] == "DateTime") 
                        var["keyTo"] = GetDateTimeObject(rangeValue.ToObject);
                    else 
                        var["keyTo"] = rangeValue.ToObject.ToString();
                }                                

                variables[index.ToString()] = var;
                index++;               
            }

            return variables.Count == 0 ? null : variables;
        }

        private static ArrayList GetItemsFromInfo(StiReportParameterInfo variableInfo)
        {
            ArrayList items = new ArrayList();
            int index = 0;

            if (variableInfo.KeyList != null && variableInfo.ValueList != null && variableInfo.Count != 0)
            {
                for (var i = 0; i < variableInfo.ValueList.Count; i++)
                {
                    Hashtable item = new Hashtable();
                    item["value"] = variableInfo.ValueList[i];
                    item["key"] = variableInfo.KeyList[i];
                    item["keyTo"] = variableInfo.KeyToList[i];
                    
                    if (variableInfo.Type == typeof(DateTime) || variableInfo.Type == typeof(DateTime?) ||
                        variableInfo.Type == typeof(DateTimeRange) || variableInfo.Type == typeof(DateTimeList))
                    {
                        if (variableInfo.KeyList[i] != null) item["key"] = GetDateTimeObject(variableInfo.KeyList[i]);
                        if (variableInfo.KeyToList[i] != null) item["keyTo"] = GetDateTimeObject(variableInfo.KeyToList[i]);
                    }

                    items.Add(item);
                    index++;
                }
            }
            return index != 0 ? items : null;
        }
#endif
    }
}
