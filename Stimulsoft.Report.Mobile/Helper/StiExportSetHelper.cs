﻿
//#define SERVER
#define MobileViewer

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using System.Globalization;
using Stimulsoft.Report.Engine;
using System.Xml;
using Stimulsoft.Report.Export;

#if SERVER
using Stimulsoft.Server.Objects;
using Stimulsoft.Server;
using Stimulsoft.Report.Design;
using System.ComponentModel;
#endif

namespace Stimulsoft.Report.Mobile
{
    internal class StiExportSetHelper
    {
#if SERVER  
        public static StiExportSet ConvertToCloudExportSettings(StiExportSettings settings)
        {
            if (settings == null) return null;

            if (settings is StiPdfExportSettings)
                return new StiPdfExportSet(settings as StiPdfExportSettings);

            if (settings is StiXpsExportSettings)
                return new StiXpsExportSet(settings as StiXpsExportSettings);

            if (settings is StiPpt2007ExportSettings)
                return new StiPowerPointExportSet(settings as StiPpt2007ExportSettings);

        #region Html

            if (settings is StiHtmlExportSettings)
                return new StiHtmlExportSet(settings as StiHtmlExportSettings);

            #endregion

        #region Word

            if (settings is StiTxtExportSettings)
                return new StiTextExportSet(settings as StiTxtExportSettings);

            if (settings is StiRtfExportSettings)
                return new StiRichTextExportSet(settings as StiRtfExportSettings);

            if (settings is StiWord2007ExportSettings)
                return new StiWordExportSet(settings as StiWord2007ExportSettings);

            if (settings is StiOdtExportSettings)
                return new StiOpenDocumentWriterExportSet(settings as StiOdtExportSettings);

            #endregion

        #region Excel

            if (settings is StiExcelExportSettings)
                return new StiExcelExportSet(settings as StiExcelExportSettings);

            if (settings is StiOdsExportSettings)
                return new StiOpenDocumentCalcExportSet(settings as StiOdsExportSettings);

            #endregion

        #region Data

            if (settings is StiDataExportSettings)
                return new StiDataExportSet(settings as StiDataExportSettings);

            #endregion

        #region Images

            if (settings is StiImageExportSettings)
                return new StiImageExportSet(settings as StiImageExportSettings);

            #endregion

            return null;
        }   
#endif
    }
}
