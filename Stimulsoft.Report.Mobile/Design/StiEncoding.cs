#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections;
using System.Text;

namespace Stimulsoft.Report.Web.Design
{
    public struct StiEncoding
    {
        private int codePage;
        public int CodePage
        {
            get
            {
                return codePage;
            }
            set
            {
                codePage = value;
            }
        }


        private string displayName;
        public string DisplayName
        {
            get
            {
                return displayName;
            }
            set
            {
                displayName = value;
            }
        }

        private static StiEncoding[] encodings = new StiEncoding[]
            {
                new StiEncoding("Arabic (ISO)", 28596),
                new StiEncoding("Arabic (Windows)", 1256),
                new StiEncoding("Baltic (ISO)", 28594),
                new StiEncoding("Baltic (Windows)", 1257),
                new StiEncoding("Central European (ISO)", 28592),
                new StiEncoding("Central European (Windows)", 1250),
                new StiEncoding("Cyrillic (ISO)", 28595),
                new StiEncoding("Cyrillic (KOI8-R)", 20866),
                new StiEncoding("Cyrillic (Windows)", 1251),
                new StiEncoding("Greek (ISO)", 28597),
                new StiEncoding("Greek (Windows)", 1253),
                new StiEncoding("Hebrew (Windows)", 1255),
                new StiEncoding("Latin 9 (ISO)", 28605),
                new StiEncoding("Turkish (ISO)", 28599),
                new StiEncoding("Turkish (Windows)", 1254),
                new StiEncoding("Unicode (Big-Endian)", 1201),
                new StiEncoding("Unicode (UTF-7)", 65000),
                new StiEncoding("Unicode (UTF-8)", 65001),
                new StiEncoding("Unicode", 1200),
                new StiEncoding("US-ASCII", 20127),
                new StiEncoding("Western European (ISO)", 28591),
                new StiEncoding("Western European (Windows)", 1252)
            };


        public static StiEncoding[] GetEncodings()
        {
            return encodings;
        }

        public StiEncoding(string displayName, int codePage)
        {
            this.displayName = displayName;
            this.codePage = codePage;
        }
    }
}
