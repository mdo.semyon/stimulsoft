#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Windows.Forms;
using System.Collections;
using System.Text;
using System.Web.UI.WebControls;
using System.Windows.Forms.Design;
using System.Drawing.Design;

using Stimulsoft.Report;
using Stimulsoft.Report.Units;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;



namespace Stimulsoft.Report.Web.Design
{
	public class StiHtmlToEditor : System.Drawing.Design.UITypeEditor
	{
		private IWindowsFormsEditorService edSvc = null;

		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			if (context != null && context.Instance != null)
			{
				return UITypeEditorEditStyle.DropDown;
			}
			return base.GetEditStyle(context);
			
		}

		internal void OnDoubleClick(object sender, EventArgs e)
		{
			edSvc.CloseDropDown();
		}
        
        
		public override object EditValue(ITypeDescriptorContext context, 
			IServiceProvider provider, object value)
		{
			if (context != null &&
				context.Instance != null &&
				provider != null) 
			{
				string codePage = value as string;

				edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

				System.Windows.Forms.ListBox lb = new System.Windows.Forms.ListBox();
				lb.Width = 300;
				lb.BorderStyle = System.Windows.Forms.BorderStyle.None;

				StiEncoding[] encodings = StiEncoding.GetEncodings();
				int selectedIndex = 0;

				foreach (StiEncoding encoding in encodings)
				{
					lb.Items.Add(encoding.DisplayName);
				}
				lb.Handle.ToString();
				lb.Sorted = true;

				int index = 0;
				foreach (string name in lb.Items)
				{
					if (codePage == name)
					{
						selectedIndex = index;
					}
					index++;
				}
				lb.SelectedIndex = selectedIndex;
				
				lb.DoubleClick += new EventHandler(OnDoubleClick);
				lb.Click += new EventHandler(OnDoubleClick);

				lb.Height = lb.ItemHeight * lb.Items.Count + 10;
				if (lb.Height > 300)lb.Height = 300;

				edSvc.DropDownControl(lb);

				return lb.SelectedItem as string;
			}
			return null;
		}
	}
}
