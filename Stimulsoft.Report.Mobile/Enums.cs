#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

namespace Stimulsoft.Report.Mobile
{
    #region StiMobileViewerTheme
    public enum StiMobileViewerTheme
    {
        Purple,
        Blue,
        Office2013,
        Office2013DarkGrayBlue,
        Office2013DarkGrayCarmine,
        Office2013DarkGrayGreen,
        Office2013DarkGrayOrange,
        Office2013DarkGrayPurple,
        Office2013DarkGrayTeal,
        Office2013DarkGrayViolet,
        Office2013LightGrayBlue,
        Office2013LightGrayCarmine,
        Office2013LightGrayGreen,
        Office2013LightGrayOrange,
        Office2013LightGrayPurple,
        Office2013LightGrayTeal,
        Office2013LightGrayViolet,
        Office2013WhiteBlue,
        Office2013WhiteCarmine,
        Office2013WhiteGreen,
        Office2013WhiteOrange,
        Office2013WhitePurple,
        Office2013WhiteTeal,
        Office2013WhiteViolet,
        Office2013VeryDarkGrayBlue,
        Office2013VeryDarkGrayCarmine,
        Office2013VeryDarkGrayGreen,
        Office2013VeryDarkGrayOrange,
        Office2013VeryDarkGrayPurple,
        Office2013VeryDarkGrayTeal,
        Office2013VeryDarkGrayViolet,
        Material,
        MaterialDarkGrayBlue,
        MaterialDarkGrayCarmine,
        MaterialDarkGrayGreen,
        MaterialDarkGrayOrange,
        MaterialDarkGrayPurple,
        MaterialDarkGrayTeal,
        MaterialDarkGrayViolet,
        MaterialLightGrayBlue,
        MaterialLightGrayCarmine,
        MaterialLightGrayGreen,
        MaterialLightGrayOrange,
        MaterialLightGrayPurple,
        MaterialLightGrayTeal,
        MaterialLightGrayViolet,
        MaterialWhiteBlue,
        MaterialWhiteCarmine,
        MaterialWhiteGreen,
        MaterialWhiteOrange,
        MaterialWhitePurple,
        MaterialWhiteTeal,
        MaterialWhiteViolet,
        MaterialVeryDarkGrayBlue,
        MaterialVeryDarkGrayCarmine,
        MaterialVeryDarkGrayGreen,
        MaterialVeryDarkGrayOrange,
        MaterialVeryDarkGrayPurple,
        MaterialVeryDarkGrayTeal,
        MaterialVeryDarkGrayViolet
    }
    #endregion
	
    #region StiWebViewMode
	public enum StiWebViewMode
	{
		OnePage,
		WholeReport
	}
	#endregion
    
    #region StiPrintMode
    public enum StiPrintMode
    {
        CurrentPage,
        WholeReport,
    }
    #endregion

    #region StiCacheMode
    public enum StiCacheMode
    {
        Page,
        Session
    }
    #endregion

    #region StiRenderMode
	public enum StiRenderMode
	{
        UseCache,
		RenderOnlyCurrentPage,
        Ajax,
        AjaxWithCache
	}
    #endregion	
	
    #region StiContentAlignment
	public enum StiContentAlignment
	{
		Left,
		Center,
		Right,
        Default
	}
    #endregion

    #region StiPrintDestination
    public enum StiPrintDestination
    {
        Pdf,
        Direct,
        WithPreview,
        PopupMenu
    }
    #endregion

    #region StiInterfaceType
    public enum StiInterfaceType
    {
        Auto,
        Mouse,
        Touch
    }
    #endregion

    #region StiTypeRenderChart
    public enum StiTypeRenderChart
    {
        Image,
        Vector,
        AnimatedVector
    }
    #endregion

    #region StiReportDisplayMode
    public enum StiReportDisplayMode
    {
        Table,
        Div,
        Span
    }
    #endregion

    #region StiFirstDayOfWeek
    public enum StiFirstDayOfWeek
    {
        Monday,
        Sunday
    }
    #endregion
}
