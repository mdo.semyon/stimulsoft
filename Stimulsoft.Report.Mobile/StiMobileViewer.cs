#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing.Design;
using System.Reflection;
using System.Collections;
using System.IO;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Caching;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Web.Design;
using Stimulsoft.Report.Dialogs;
using Stimulsoft.Report.Engine;
using System.Xml;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using System.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using Stimulsoft.Base.Serializing;
using System.Text.RegularExpressions;
using Stimulsoft.Base.Licenses;
using Stimulsoft.Base.Cloud;
using Stimulsoft.Report.Dictionary;

#if SERVER
using Stimulsoft.Client;
using Stimulsoft.Server;
using Stimulsoft.Server.Objects;
using Stimulsoft.Server.Connect;
#endif

namespace Stimulsoft.Report.Mobile
{
#if !SERVER
    [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiWebViewer component instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
#endif
    public class StiMobileViewer :
        WebControl,
        INamingContainer,
        ICallbackEventHandler
    {
        #region Cache Helper
        private static class DataSerializer
        {
            public static byte[] Serialize(object o)
            {
                if (o == null)
                {
                    return null;
                }

                BinaryFormatter binaryFormatter = new BinaryFormatter();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    binaryFormatter.Serialize(memoryStream, o);
                    byte[] objectDataAsStream = memoryStream.ToArray();
                    return objectDataAsStream;
                }
            }

            public static T Deserialize<T>(byte[] stream)
            {
                if (stream == null)
                {
                    return default(T);
                }

                BinaryFormatter binaryFormatter = new BinaryFormatter();
                using (MemoryStream memoryStream = new MemoryStream(stream))
                {
                    T result = (T)binaryFormatter.Deserialize(memoryStream);
                    return result;
                }
            }
        }

        abstract public class StiCacheHelper
        {
            abstract public void SaveObjectToCache(object obj, string guid);

            abstract public object GetObjectFromCache(string guid);

            public static object GetObjectFromCacheData(byte[] fileData)
            {
                object data = DataSerializer.Deserialize<object>(fileData);
                try
                {
                    if (data is string)
                    {
                        return data;
                    }
                    else if (data is byte[])
                    {
                        StiReport report = new StiReport();
                        string reportStr = Encoding.UTF8.GetString((byte[])data, 0, ((byte[])data).Length);
                        if (reportStr.IndexOf("application=\"StiDocument\"") > 0)
                            report.LoadDocument((byte[])data);
                        else
                            report.Load((byte[])data);
                        return report;
                    }
                    else if (data is ArrayList)
                    {
                        ArrayList array = (ArrayList)data;
                        for (int i = 0; i < array.Count; i++)
                        {
                            if (array[i] != null)
                            {
                                string str = (string)array[i];
                                string typeStyle = str.Substring(0, str.IndexOf(";"));
                                string strStyle = str.Substring(str.IndexOf(";") + 1);

                                Assembly assembly = typeof(StiReport).Assembly;
                                object style = assembly.CreateInstance("Stimulsoft.Report.Export." + typeStyle) as StiCellStyle;
                                //if (style == null) style = assembly.CreateInstance("Stimulsoft.Report.Chart." + typeStyle) as StiComponent;
                                //else if (style == null) style = assembly.CreateInstance("Stimulsoft.Report.CrossTab." + typeStyle) as StiComponent;

                                if (style != null)
                                {
                                    using (var stringReader = new StringReader(strStyle))
                                    {
                                        var sr = new StiSerializing(new StiReportObjectStringConverter());
                                        sr.Deserialize(style, stringReader, "StiStyle");
                                        stringReader.Close();
                                    }
                                }
                                array[i] = style;
                            }

                        }
                        return array;
                    }
                }
                finally
                {
                }

                return null;
            }

            public static byte[] GetCacheDataFromObject(object obj)
            {
                byte[] data = null;

                if (obj is string)
                {
                    data = DataSerializer.Serialize((string)obj);
                }
                else if (obj is StiReport)
                {
                    byte[] reportValue = ((StiReport)obj).IsDocument ? ((StiReport)obj).SaveDocumentToByteArray() : ((StiReport)obj).SaveToByteArray();
                    data = DataSerializer.Serialize(reportValue);
                }
                else if (obj is ArrayList)
                {
                    ArrayList array = new ArrayList();
                    for (int i = 0; i < ((ArrayList)obj).Count; i++)
                    {
                        if (((ArrayList)obj)[i] != null)
                        {
                            var sr = new StiSerializing(new StiReportObjectStringConverter());
                            var sb = new StringBuilder();
                            using (var stringWriter = new StringWriter(sb))
                            {
                                sr.Serialize(((ArrayList)obj)[i], stringWriter, "StiStyle", StiSerializeTypes.SerializeToAll);
                                stringWriter.Close();
                                array.Add(((ArrayList)obj)[i].GetType().Name + ";" + sb.ToString());
                            }
                        }
                    }
                    data = DataSerializer.Serialize(array);
                }

                return data;
            }
        }
        #endregion

        #region Constants
        internal static string STYLES_PATH = "Stimulsoft.Report.Mobile.Styles";
        internal static string SCRIPTS_PATH = "Stimulsoft.Report.Mobile.Scripts";
        #endregion

        #region StiWebReportStatus
        public enum StiWebReportStatus
        {
            FirstPass,
            SecondPass
        }
        #endregion

        #region Const
        private const string ReportNotAssigned =
            "'Report' property is not assigned. You need to rebuild the" +
            " report on each page refreshing or need set property RenderMode of the StiMobileViewer to 'UseCache' or 'AjaxWithCache'.";
        #endregion

        #region Stimulsoft Reports.Server Client Methods
#if SERVER
        private StiCommand RunCommand(StiCommand command)
        {
            return StiCommandToServer.RunCommand(command);
        }
#endif
        #endregion

        #region ICallbackEventHandler
        public void RaiseCallbackEvent(string receivingDataJsonText)
        {
            try
            {
                //parse receiving data
                Hashtable receivingData = (Hashtable)JSON.Decode(StiEncodingHelper.DecodeString(receivingDataJsonText));
                string command = (string)receivingData["command"];

                //prepare answer data
                Hashtable answer = new Hashtable();
                answer["mobileViewerId"] = this.ClientID;
                answer["command"] = command;
                if (receivingData["callbackFunctionGuid"] != null) answer["callbackFunctionGuid"] = receivingData["callbackFunctionGuid"];

                #region Custom CallBack
                if (command == "CustomCallBack")
                {
                    InvokeCustomCallBack(receivingData);
                    answer["parameters"] = receivingData;
                    callbackResult = JSON.Encode(answer);
                    return;
                }
                #endregion

                #region Report Export
                if (command == "ReportExport")
                {
#if SERVER
                    var getReportExportCommand = new StiReportCommands.Export()
                    {
                        AllowSignals = true,
                        ReportSnapshotItemKey = (string)receivingData["reportSnapshotItemKey"],
                        FileItemKey = (string)receivingData["fileItemKey"],
                        SessionKey = (string)receivingData["sessionKey"],
                        TaskKey = (string)receivingData["taskKey"],
                        UserKey = (string)receivingData["userKey"],
                        VersionKey = (string)receivingData["versionKey"],
                        ItemVisibility = false,
                        Expires = DateTime.Now + StiClientConfiguration.Reports.ReportExportedLifeTime
                    };

                    StiExportSettings resultExportSettings = receivingData["exportSettings"] != null
                        ? GetExportSettings((string)receivingData["exportFormat"], (Hashtable)receivingData["exportSettings"]) : null;

                    if (receivingData["printAfterExport"] != null && receivingData["exportFormat"] as string == "StiPdfExportSettings")
                    {
                        resultExportSettings = new StiPdfExportSettings();
                        ((StiPdfExportSettings)resultExportSettings).AutoPrintMode = StiPdfAutoPrintMode.Dialog;
                    }

                    if (resultExportSettings != null)
                    {
                        getReportExportCommand.FileType = GetFileTypeByExportSettings(resultExportSettings);
                        getReportExportCommand.ExportSet = StiExportSetHelper.ConvertToCloudExportSettings(resultExportSettings);
                    }

                    var resultReportExportCommand = RunCommand(getReportExportCommand) as StiReportCommands.Export;

                    if (!resultReportExportCommand.ResultSuccess && resultReportExportCommand.ResultNotice != null)
                        answer["errorMessage"] = StiNoticeConverter.Convert(resultReportExportCommand.ResultNotice);
#endif
                    callbackResult = JSON.Encode(answer);
                    return;
                }
                #endregion

                #region Get Variables
                if (command == "GetVariables")
                {
#if SERVER
                    var getParameterInfosCommand = new StiReportCommands.GetParameterInfos()
                    {
                        ReportTemplateItemKey = (string)receivingData["reportTemplateItemKey"],
                        SessionKey = (string)receivingData["sessionKey"]
                    };

                    var resultParameterInfosCommand = RunCommand(getParameterInfosCommand) as StiReportCommands.GetParameterInfos;

                    if (resultParameterInfosCommand.ResultSuccess && resultParameterInfosCommand.ResultParameters != null)
                    {
                        StiReport report = null;
                        bool haveExpressionVariable = false;

                        foreach (StiReportParameterInfo parameterInfo in resultParameterInfosCommand.ResultParameters)
                        {
                            if (parameterInfo.InitBy == StiVariableInitBy.Expression)
                            {
                                haveExpressionVariable = true;
                                break;
                            }
                        }

                        if (haveExpressionVariable)
                        {
                            var commandResourceGet = new StiItemResourceCommands.Get
                            {
                                ItemKey = receivingData["reportTemplateItemKey"] as string,
                                SessionKey = receivingData["sessionKey"] as string
                            };

                            commandResourceGet = RunCommand(commandResourceGet) as StiItemResourceCommands.Get;

                            if (commandResourceGet.ResultResource != null && commandResourceGet.ResultResource.Length > 0)
                            {
                                try
                                {
                                    report = new StiReport();
                                    report.Load(commandResourceGet.ResultResource);
                                    if (!report.IsCompiled) report.Compile();
                                }
                                catch { }
                            }
                        }

                        answer["paramsVariables"] = StiParametersClass.GetVariablesFromInfo(resultParameterInfosCommand.ResultParameters, report);
                        answer["resultRequestParameters"] = resultParameterInfosCommand.ResultRequestParameters;
                    }
                    else
                    {
                        if (resultParameterInfosCommand.ResultNotice != null)
                            answer["errorMessage"] = StiNoticeConverter.Convert(resultParameterInfosCommand.ResultNotice);
                    }
#endif
                    callbackResult = JSON.Encode(answer);
                    return;
                }
                #endregion

                #region Get Export Settings For Stimulsoft Reports.Server
                if (command == "GetExportSettingsForServer")
                {
                    StiExportSettings resultExportSettings = GetExportSettings((string)receivingData["exportFormat"], (Hashtable)receivingData["exportSettings"]);
#if SERVER
                    if (resultExportSettings != null)
                    {
                        answer["exportSettings"] = StiJsonHelper.SaveToJsonString(StiExportSetHelper.ConvertToCloudExportSettings(resultExportSettings));
                    }
#endif
                    callbackResult = JSON.Encode(answer);
                    return;
                }
                #endregion

                #region Get Report From Cache by ReportGuid
                lock (lockReport)
                {
                    if (receivingData["cacheReportGuid"] != null)
                    {
                        CacheReportGuid = (string)receivingData["cacheReportGuid"];
                        ResetReport();
                        Report = CacheReport;
                    }
                }
                #endregion

                #region Load First Page
                if (command == "LoadFirstPage")
                {
#if SERVER
                    StiPage page = null;

                    if (receivingData["resultFirstPage"] != null)
                    {
                        page = new StiPage();
                        StiReportPageHelper.Load(page, Convert.FromBase64String((string)receivingData["resultFirstPage"]));
                    }
                    else
                    {
                        var getPageCommand = new StiReportCommands.GetPage
                        {
                            ReportSnapshotItemKey = (string)receivingData["reportSnapshotItemKey"],
                            TaskKey = (string)receivingData["taskKey"],
                            SessionKey = (string)receivingData["sessionKey"]
                        };

                        var resultCommand = RunCommand(getPageCommand) as StiReportCommands.GetPage;
                        if (resultCommand.ResultSuccess && resultCommand.ResultPage != null && resultCommand.ResultPage.Content != null)
                        {
                            page = resultCommand.ResultPage.GetPage();
                        }
                    }

                    if (page != null)
                    {
                        lock (lockReport)
                        {
                            StiReport newReport = AddPageToReportRenderedPagesCollection(Report, page, 0, null);

                            answer["firstPageContent"] = GetPagesArray(newReport, 0, false);

                            CacheReport = newReport;
                            answer["cacheReportGuid"] = CacheReportGuid;
                        }
                    }
#endif
                    callbackResult = JSON.Encode(answer);
                    return;
                }
                #endregion

                #region Load Report From Metric ||Load Report From File
                if (command == "LoadReportFromFile" ||
                    command == "LoadReportSnapshotFromMetric" ||
                    command == "LoadReportFromCache" ||
                    command == "LoadReportToViewer")
                {
                    StiReport newReport = new StiReport();
                    if (command == "LoadReportFromCache")
                    {
                        #region LoadReportFromCache
                        newReport = GetReportForPreviewFromCache((string)receivingData["previewReportGuid"]);
                        if (newReport == null)
                        {
                            answer["errorMessage"] = "Cannot get report from cache!";
                            callbackResult = JSON.Encode(answer);
                            return;
                        }
                        answer["isEditableReport"] = (newReport != null) ? CheckEditableReport(newReport) : false;
                        #endregion
                    }
                    else if (command == "LoadReportFromFile")
                    {
                        #region LoadReportFromFile
                        string appDirectory = HttpContext.Current.Server.MapPath(string.Empty);
                        newReport.Load(string.Format("{0}\\Reports\\{1}.mrt", appDirectory, (string)receivingData["reportName"]));

                        DataSet dataJson = StiJsonToDataSetConverter.GetDataSetFromFile(appDirectory + "\\Data\\countries.json");
                        newReport.RegData(dataJson);

                        DataSet data = new DataSet();
                        data.ReadXml(appDirectory + "\\Data\\Demo.xml");
                        newReport.RegData(data);

                        newReport.Render(false);
                        answer["isEditableReport"] = (newReport != null) ? CheckEditableReport(newReport) : false;
                        #endregion
                    }
                    else if (command == "LoadReportToViewer")
                    {
                        #region LoadReportToViewer

                        #region LoadReportFromFileData
                        if (receivingData["reportFileData"] != null)
                        {
                            string reportData = (string)receivingData["reportFileData"];
                            if (reportData.IndexOf("base64,") > 0)
                            {
                                string content = reportData.Substring(reportData.IndexOf("base64,") + "base64,".Length);
                                byte[] contentBytes = System.Convert.FromBase64String(content);
                                if (receivingData["isDocument"] != null && (bool)receivingData["isDocument"])
                                {
                                    newReport.LoadDocument(contentBytes);
                                }
                                else
                                {
                                    newReport.Load(contentBytes);
                                }
                            }
                            else
                            {
                                if (receivingData["isDocument"] != null && (bool)receivingData["isDocument"])
                                {
                                    newReport.LoadDocumentFromString((string)receivingData["reportFileData"]);
                                }
                                else
                                {
                                    newReport.LoadFromString((string)receivingData["reportFileData"]);
                                }

                            }
                        }
                        #endregion

                        #region LoadReportFromFilePath
                        if (receivingData["reportFilePath"] != null)
                        {
                            if (receivingData["isDocument"] != null && (bool)receivingData["isDocument"])
                            {
                                newReport.LoadDocument((string)receivingData["reportFilePath"]);
                            }
                            else
                            {
                                newReport.Load((string)receivingData["reportFilePath"]);
                            }
                        }
                        #endregion

                        if (!newReport.IsDocument && !newReport.IsRendered)
                        {
                            InvokeGetReportData(newReport);
                            newReport.Render(false);
                        }

                        answer["isEditableReport"] = (newReport != null) ? CheckEditableReport(newReport) : false;
                        #endregion
                    }
                    else if (command == "LoadReportSnapshotFromMetric")
                    {
                        #region LoadReportSnapshotFromMetric
#if SERVER
                        var getInfoCommand = new StiReportCommands.GetInfo()
                        {
                            ReportSnapshotItemKey = (string)receivingData["reportSnapshotItemKey"],
                            SessionKey = (string)receivingData["sessionKey"],
                            VersionKey = (string)receivingData["versionKey"]
                        };

                        var resultInfoCommand = RunCommand(getInfoCommand) as StiReportCommands.GetInfo;

                        if (resultInfoCommand.ResultSuccess && resultInfoCommand.ResultMetric != null && resultInfoCommand.ResultManifest != null)
                        {
                            answer["pagination"] = resultInfoCommand.ResultManifest.Pagination;

                            if (resultInfoCommand.ResultManifest.Pagination == true)
                            {
                                #region Pagination == true
                                try
                                {
                                    if (Report != null)
                                    {
                                        newReport = Report;
                                        answer["notLoadAllPages"] = true;
                                    }
                                    else
                                    {
                                        newReport = CreateReportSnapshotFromMetric(resultInfoCommand.ResultMetric.PageMetrics);
                                    }
                                }
                                catch (Exception e) { answer["errorMessage"] = e.Message; };

                                #region Get Bookmark
                                var getBookmarkCommand = new StiReportCommands.GetBookmark()
                                {
                                    ReportSnapshotItemKey = (string)receivingData["reportSnapshotItemKey"],
                                    SessionKey = (string)receivingData["sessionKey"],
                                    VersionKey = (string)receivingData["versionKey"]
                                };

                                var resultBookmarkCommand = RunCommand(getBookmarkCommand) as StiReportCommands.GetBookmark;

                                if (resultBookmarkCommand.ResultSuccess && resultBookmarkCommand.ResultBookmark != null)
                                {
                                    StiBookmark bookmark = StiBookmarkHelper.Load(resultBookmarkCommand.ResultBookmark.Content);
                                    newReport.Bookmark = bookmark;
                                }
                                else
                                {
                                    if (resultBookmarkCommand.ResultNotice != null)
                                        answer["errorMessage"] = StiNoticeConverter.Convert(resultBookmarkCommand.ResultNotice);
                                }
                                #endregion
                                #endregion
                            }
                            else
                            {
                                #region Pagination == false
                                var itemResourceGetCommand = new StiItemResourceCommands.Get()
                                {
                                    ItemKey = (string)receivingData["reportSnapshotItemKey"],
                                    SessionKey = (string)receivingData["sessionKey"],
                                    VersionKey = (string)receivingData["versionKey"]
                                };
                                var resultCommand = RunCommand(itemResourceGetCommand) as StiItemResourceCommands.Get;

                                if (resultCommand.ResultSuccess && resultCommand.ResultResource != null)
                                {

                                    try
                                    {
                                        newReport.LoadDocument(new MemoryStream(resultCommand.ResultResource));
                                        //answer["isEditableReport"] = CheckEditableReport(newReport);
                                    }
                                    catch (Exception e) { answer["errorMessage"] = e.Message; };
                                }
                                else
                                {
                                    if (resultCommand.ResultNotice != null)
                                        answer["errorMessage"] = StiNoticeConverter.Convert(resultCommand.ResultNotice);
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            if (resultInfoCommand.ResultNotice != null)
                                answer["errorMessage"] = StiNoticeConverter.Convert(resultInfoCommand.ResultNotice);
                        }
#endif
                        #endregion
                    }

                    answer["paramsVariables"] = (newReport != null) ? (!newReport.IsDocument ? StiParametersClass.GetVariables(newReport) : null) : null;
                    answer["paramsBookmarks"] = (AllowBookmarks && newReport != null && newReport.Bookmark != null && newReport.Bookmark.Bookmarks.Count > 0)
                        ? RenderBookmarks(newReport, this.ClientID, ViewMode == StiWebViewMode.OnePage ? CurrentPage : -1)
                        : null;

                    CacheReport = newReport;

                    if (answer["notLoadAllPages"] != null)
                    {
                        callbackResult = JSON.Encode(answer);
                        return;
                    }

                    ResetReport();
                    Report = newReport;
                }
                #endregion

                #region Load Page From Data
                if (command == "LoadPageFromData")
                {
#if SERVER

                    if (receivingData["zoom"] != null) ZoomPercent = (float)((double)receivingData["zoom"]);
                    Hashtable notAnimatedPages = new Hashtable();
                    if (receivingData["notAnimatedPages"] != null) notAnimatedPages = receivingData["notAnimatedPages"] as Hashtable;
                    ArrayList pageIndexes = (ArrayList)receivingData["pageIndexes"];
                    ArrayList pagesContent = new ArrayList();
                    StiReport resultReport = Report;

                    for (int i = 0; i < pageIndexes.Count; i++)
                    {
                        int pageIndex = Convert.ToInt32(pageIndexes[i]);
                        var getPageCommand = new StiReportCommands.GetPage();
                        getPageCommand.ReportSnapshotItemKey = (string)receivingData["reportSnapshotItemKey"];
                        getPageCommand.SessionKey = (string)receivingData["sessionKey"];
                        getPageCommand.TaskKey = (string)receivingData["taskKey"];
                        getPageCommand.VersionKey = (string)receivingData["versionKey"];
                        getPageCommand.PageIndex = pageIndex;

                        var resultCommand = RunCommand(getPageCommand) as StiReportCommands.GetPage;

                        if (resultCommand.ResultSuccess && resultCommand.ResultPage != null && resultCommand.ResultPage.Content != null)
                        {
                            lock (lockReport)
                            {
                                resultReport = AddPageToReportRenderedPagesCollection(Report, null, pageIndex, resultCommand.ResultPage.Content);

                                Hashtable pageParams = new Hashtable();
                                pageParams["pageIndex"] = pageIndex;
                                pageParams["pageContent"] = GetPagesArray(resultReport, pageIndex, notAnimatedPages[pageIndex.ToString()] != null ? true : false);
                                pagesContent.Add(pageParams);
                            }
                        }
                        else
                        {
                            if (resultCommand.ResultNotice != null)
                                answer["errorMessage"] = StiNoticeConverter.Convert(resultCommand.ResultNotice);
                        }
                    }

                    if (resultReport != null)
                    {
                        CacheReport = resultReport;
                        answer["cacheReportGuid"] = CacheReportGuid;
                    }
                    answer["pagesContent"] = pagesContent;
                    callbackResult = JSON.Encode(answer);
                    return;
#endif
                }
                #endregion

                #region Apply Variables
                if (receivingData["variables"] != null)
                {
                    InvokeGetReportData(SourceReport);

                    StiReport compiledReport = SourceReport != null && SourceReport.CompiledReport != null ? SourceReport.CompiledReport : (SourceReport != null ? SourceReport : report);
                    if (compiledReport.NeedsCompiling && !compiledReport.IsCompiled && StiOptions.Engine.FullTrust) compiledReport.Compile();
                    if (SourceReport != null && SourceReport.CompiledReport == null && !SourceReport.IsDocument && StiOptions.Engine.FullTrust && SourceReport.NeedsCompiling) SourceReport.Compile();
                    StiReport currentReport = SourceReport != null && SourceReport.CompiledReport != null ? SourceReport.CompiledReport : (SourceReport != null ? SourceReport : report);

                    Hashtable parametersObject = (Hashtable)receivingData["variables"];
                    if (currentReport != null) StiParametersClass.ApplyReportParameters(currentReport, parametersObject);
                    currentReport.IsPreviewDialogs = true;
                    if (currentReport != null && !currentReport.IsDocument) currentReport.Render(false);
                    currentReport.IsPreviewDialogs = false;

                    prevServerReportName = ServerReportName;
                    ServerReportName = null;
                    if (SourceReport != null && currentReport != null && !currentReport.IsDocument) Report = currentReport;
                    CacheReport = Report;
                }
                #endregion

                #region Apply Navigation
                bool printCommand = (command == "PrintDirect" || command == "PrintWithPreview");
                if (receivingData["pageNumber"] != null)
                    SetPage((int)((double)receivingData["pageNumber"]));
                if (receivingData["zoom"] != null)
                    ZoomPercent = printCommand ? 100 : (float)((double)receivingData["zoom"]);
                if (receivingData["viewmode"] != null)
                    ViewMode = printCommand ? StiWebViewMode.WholeReport : (((string)receivingData["viewmode"]) == "OnePage") ? StiWebViewMode.OnePage : StiWebViewMode.WholeReport;
                #endregion

                #region Data Refresh
                if (command == "dataRefresh")
                {
                    StiReport compiledReport = SourceReport != null && SourceReport.CompiledReport != null ? SourceReport.CompiledReport : report;
                    if (compiledReport.NeedsCompiling && !compiledReport.IsCompiled && StiOptions.Engine.FullTrust) compiledReport.Compile();
                    if (SourceReport != null && SourceReport.CompiledReport == null && StiOptions.Engine.FullTrust) SourceReport.Compile();
                    StiReport currentReport = SourceReport != null && SourceReport.CompiledReport != null ? SourceReport.CompiledReport : (SourceReport != null ? SourceReport : report);

                    InvokeGetReportData((SourceReport != null) ? SourceReport : currentReport);
                    currentReport.IsPreviewDialogs = true;
                    if (currentReport != null && !currentReport.IsDocument) currentReport.Render(false);
                    currentReport.IsPreviewDialogs = false;

                    prevServerReportName = ServerReportName;
                    ServerReportName = null;
                    if (SourceReport != null) Report = SourceReport;
                }
                #endregion

                #region Refresh Variables
                if (command == "RefreshVariables")
                {
                    if (Report != null)
                    {
                        StiParametersClass.ApplyReportBindingVariables(Report, receivingData["variables"] as Hashtable);
                        answer["paramsVariables"] = StiParametersClass.GetVariables(Report);
                    }
                }
                #endregion

                //Send answer
                ApplyEditableFieldsToReport(Report, receivingData["editableParameters"]);

                answer["pagesArray"] = GetPagesArray(Report, command == "PrintDirect" || command == "PrintWithPreview" ||
                    command == "LoadReportSnapshotFromMetric" && answer["pagination"] != null && (bool)answer["pagination"]);
                answer["pagesCount"] = PagesCount;
                answer["pageNumber"] = CurrentPage;
                answer["viewMode"] = ViewMode;
                answer["zoom"] = ZoomPercent;
                answer["cacheReportGuid"] = CacheReport != null ? CacheReportGuid : null;

                callbackResult = JSON.Encode(answer);
            }
            catch (Exception e)
            {
                Hashtable answer = new Hashtable();
                answer["mobileViewerId"] = this.ClientID;
                answer["errorMessage"] = e.Message;
                callbackResult = JSON.Encode(answer);
            }
        }

        public string GetCallbackResult()
        {
            if (!IsAjax) return string.Empty;

            return callbackResult;
        }
        #endregion

        #region Handlers
        protected override void OnInit(EventArgs e)
        {
            if (GetRequestParam("demolocalization") != null)
            {
                this.GlobalizationFile = "Localization/" + GetRequestParam("demolocalization") as string + ".xml";
            }

            if (!String.IsNullOrEmpty(AutoRedirectMobilePage) && isMobile())
            {
                Page.Response.Redirect(AutoRedirectMobilePage);
                return;
            }

            if (!IsDesignMode)
            {
#if SERVER
                if (this.ProcessShareRequest()) return;
#endif
                if (IsAjax && this.ProcessCssRequest()) return;
                if (this.ProcessScriptRequest()) return;
                if (this.ProcessImageRequest()) return;
                if (this.ProcessPrintHtml()) return;
            }
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!IsDesignMode && !Page.IsPostBack) EnsureChildControls();
            base.OnLoad(e);

            if (!IsDesignMode)
            {
                if (IsAjax)
                {
                    ProcessReport();

                    if (Page != null)
                    {
                        #region PrintToPdf
                        if (Page.Request.Params["command"] == "PrintToPdf")
                        {
                            if (Page.Request.Params["cacheReportGuid"] != null)
                            {
                                string cacheReportGuid = (string)Page.Request.Params["cacheReportGuid"];
                                this.CacheReportGuid = cacheReportGuid;
                                this.ResetReport();
                                this.Report = this.CacheReport;
                            }
                            if (Page.Request.Params["editableParameters"] != null)
                            {
                                Hashtable editableParameters = (Hashtable)JSON.Decode(Page.Request.Params["editableParameters"] as string);
                                ApplyEditableFieldsToReport(this.Report, editableParameters);
                            }
                            PrintToPdf();
                            return;
                        }
                        #endregion

                        string actionString = null;
                        object target = Page.Request.Form["__EVENTTARGET"];
                        object action = Page.Request.Form["__EVENTARGUMENT"];
                        Hashtable exportSettings = null;

                        if (!String.IsNullOrEmpty((string)action))
                        {
                            Hashtable actions = (action != null) ? (Hashtable)JSON.Decode(action.ToString()) : null;
                            action = actions["command"];
                            if (actions["exportSettings"] != null) exportSettings = (Hashtable)actions["exportSettings"];
                            if (actions["cacheReportGuid"] != null)
                            {
                                string cacheReportGuid = (string)actions["cacheReportGuid"];
                                this.CacheReportGuid = cacheReportGuid;
                                this.ResetReport();
                                this.Report = this.CacheReport;
                            }
                            if (actions["editableParameters"] != null) ApplyEditableFieldsToReport(this.Report, actions["editableParameters"]);
                        }

                        if (target != null && action != null && target.ToString() == this.UniqueID)
                        {
                            actionString = action.ToString();
                            if (actionString == "Design") InvokeReportDesign();
                        }

                        string keyValue = Page.Request.QueryString.Get(string.Format("sti_{0}_export", this.ID));
                        if (keyValue != null) actionString = keyValue;
                        keyValue = Page.Request.QueryString.Get(string.Format("sti_{0}_guid", this.ID));
                        if (actionString != null)
                        {
                            if (keyValue != null)
                            {
                                ServerReportName = keyValue;
                                ProcessReport();
                            }

                            // Process Preview or Save                            
                            if (actionString == "PrintPdf")
                            {
                                PrintToPdf();
                            }

                            else if (actionString == "SaveDocument") ProcessDocumentRequest(true, exportSettings);
                            else if (actionString == "StiPdfExportSettings") ProcessPdfRequest(true, exportSettings);
                            else if (actionString == "StiXpsExportSettings") ProcessXpsRequest(true, exportSettings);
                            else if (actionString == "StiPpt2007ExportSettings") ProcessPowerPointRequest(true, exportSettings);
                            else if (actionString == "StiHtmlExportSettings") ProcessHtmlRequest(true, HtmlShowDialog, exportSettings);
                            else if (actionString == "StiHtml5ExportSettings") ProcessHtml5Request(true, HtmlShowDialog, exportSettings);
                            else if (actionString == "StiMhtExportSettings") ProcessMhtRequest(true, exportSettings);
                            else if (actionString == "StiTxtExportSettings") ProcessTextRequest(true, exportSettings);
                            else if (actionString == "StiRtfExportSettings") ProcessRtfRequest(true, exportSettings);
                            else if (actionString == "StiWord2007ExportSettings") ProcessWord2007Request(true, exportSettings);
                            else if (actionString == "StiOdtExportSettings") ProcessOdtRequest(true, exportSettings);
                            else if (actionString == "StiExcelExportSettings") ProcessXlsRequest(true, exportSettings);
                            else if (actionString == "StiExcelXmlExportSettings") ProcessXlsXmlRequest(true, exportSettings);
                            else if (actionString == "StiExcel2007ExportSettings") ProcessExcel2007Request(true, exportSettings);
                            else if (actionString == "StiOdsExportSettings") ProcessOdsRequest(true, exportSettings);
                            else if (actionString == "StiCsvExportSettings") ProcessCsvRequest(true, exportSettings);
                            else if (actionString == "StiDbfExportSettings") ProcessDbfRequest(true, exportSettings);
                            else if (actionString == "StiXmlExportSettings") ProcessXmlRequest(true, exportSettings);
                            else if (actionString == "StiDifExportSettings") ProcessDifRequest(true, exportSettings);
                            else if (actionString == "StiSylkExportSettings") ProcessSylkRequest(true, exportSettings);
                            else if (actionString == "StiBmpExportSettings") ProcessBmpRequest(true, exportSettings);
                            else if (actionString == "StiGifExportSettings") ProcessGifRequest(true, exportSettings);
                            else if (actionString == "StiJpegExportSettings") ProcessJpegRequest(true, exportSettings);
                            else if (actionString == "StiPcxExportSettings") ProcessPcxRequest(true, exportSettings);
                            else if (actionString == "StiPngExportSettings") ProcessPngRequest(true, exportSettings);
                            else if (actionString == "StiTiffExportSettings") ProcessTiffRequest(true, exportSettings);
                            else if (actionString == "StiEmfExportSettings") ProcessMetafileRequest(true, exportSettings);
                            else if (actionString == "StiSvgExportSettings") ProcessSvgRequest(true, exportSettings);
                            else if (actionString == "StiSvgzExportSettings") ProcessSvgzRequest(true, exportSettings);
                        }
                    }
                }
                else ProcessReport();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsDesignMode)
            {
                this.EnsureChildControls();
                if (!this.DesignerMode)
                {
                    RenderCssStyles();
                    RenderScripts();
                }
            }

            base.OnPreRender(e);
            if (!IsDesignMode) ProcessReport();
        }
        #endregion

        #region Render Resources
        /// <summary>
        /// Get the Stimulsoft.Report.dll file version.
        /// </summary>        
        private static string GetProductVersion(bool isShort, string reportGuid)
        {
            string[] values = StiVersion.Version.Split('.');
            string versionString = string.Format("{0}.{1}.{2}", values[0], values[1], values[2]);
            if (isShort) return values[3] != "0" ? string.Format("{0}.{1}", versionString, values[3]) : versionString;
            versionString += " from " + StiVersion.CreationDate;

            #region Trial
#if CLOUD
            var isTrial = StiCloudPlan.IsTrialPlan(reportGuid);
#else
            var key = StiLicenseKeyValidator.GetLicenseKey();
            var isTrial = !StiLicenseKeyValidator.IsValid(StiProductIdent.Web, key);
            if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key))isTrial = true;

            #region IsValidLicenseKey
            if (!isTrial)
            {
                try
                {
                    using (var rsa = new RSACryptoServiceProvider(512))
                    using (var sha = new SHA1CryptoServiceProvider())
                    {
                        rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                        isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
                    }
                }
                catch (Exception)
                {
                    isTrial = true;
                }
            }
            #endregion
#endif
            if (!isTrial) versionString += " ";
            #endregion

            return versionString;
        }

        private ArrayList GetResourceNamesBySubstring(string subString)
        {
            ArrayList resultResurceNames = new ArrayList();
            Assembly assembly = typeof(StiMobileViewer).Assembly;

            string[] resourceNames = assembly.GetManifestResourceNames();
            foreach (string resourceName in resourceNames)
                if (resourceName.IndexOf(subString) != -1) resultResurceNames.Add(resourceName);

            return resultResurceNames;
        }

        private void RenderScripts()
        {
            var url = GetRequestUrl("mobileviewer_script", "ViewerScripts", PassQueryParametersForResources);
            url += url.IndexOf("?") > 0 ? "&" : "?";
            url += "mobileviewer_version=" + GetProductVersion(true, this.Report != null ? this.Report.ReportGuid : null);
            var script = string.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", url);
            RegisterClientScriptBlockIntoHeader(url, script);
        }

        internal string GetCssStyle(string cssName)
        {
            Assembly a = typeof(StiMobileViewer).Assembly;
            Stream stream = a.GetManifestResourceStream("Stimulsoft.Report.Mobile.Styles." + this.Theme.ToString() + "." + cssName);
            if (stream != null)
            {
                string css;
                using (StreamReader reader = new StreamReader(stream))
                {
                    css = reader.ReadToEnd();
                }
                return "<style type=\"text/css\">\r\n" + css + "</style>\r\n";
            }
            return null;
        }

        internal Bitmap GetImage(string imageName)
        {
            Assembly a = typeof(StiMobileViewer).Assembly;
            Stream stream = a.GetManifestResourceStream("Stimulsoft.Report.Mobile.Images." +
                (this.Theme.ToString().StartsWith("Office2013") ? "Office2013" : (this.Theme.ToString().StartsWith("Material")) ? "Material" : this.Theme.ToString()) + "." + imageName);
            if (stream != null) return new Bitmap(stream);
            return null;
        }

        private void RegisterClientScriptBlockIntoHeader(string key, string script)
        {
            if (HttpContext.Current.Items.Contains("scriptblock_" + key)) return;

            HttpContext.Current.Items.Add("scriptblock_" + key, string.Empty);
            int? index = HttpContext.Current.Items["__ScriptResourceIndex"] as int?;
            if (index == null) index = 0;
            Page.Header.Controls.Add(new LiteralControl(script));
            HttpContext.Current.Items["__ScriptResourceIndex"] = index;
        }

        private StiEmptyObject emptyObject = new StiEmptyObject();

        private string GetWebResourceUrl(string resourceName)
        {
            return Page.ClientScript.GetWebResourceUrl(emptyObject.GetType(), resourceName);
        }
        #endregion

        #region Methods.Process Request
        public bool ProcessDocumentRequest()
        {
            return ProcessDocumentRequest(false, null);
        }

        public bool ProcessDocumentRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                InvokeReportExport(ReportForRendering, null);

                string formatReport = null;
                string password = null;

                if (exportSettings != null)
                {
                    formatReport = (string)exportSettings["Format"];
                    password = (string)exportSettings["Password"];
                }

                if (ExportResponse) StiReportResponse.ResponseAsDocument(this.Page, ReportForRendering, formatReport, password);

                return true;
            }
            return false;
        }


        public bool ProcessPdfRequest()
        {
            return ProcessPdfRequest(false, null);
        }

        private StiPdfExportSettings GetPdfExportSettings(Hashtable exportSettings)
        {
            StiPdfExportSettings settings = new StiPdfExportSettings();
            settings.ImageCompressionMethod = PdfImageCompressionMethod;
            settings.ImageQuality = ImageQuality;
            settings.ImageResolution = ImageResolution;
            settings.EmbeddedFonts = PdfEmbeddedFonts;
            settings.StandardPdfFonts = PdfStandardFonts;
            settings.UseUnicode = PdfUseUnicode;

            if (exportSettings != null)
            {
                if (exportSettings["AutoPrintMode"] != null)
                    settings.AutoPrintMode = (StiPdfAutoPrintMode)Enum.Parse(typeof(StiPdfAutoPrintMode), (string)exportSettings["AutoPrintMode"]);
                settings.ImageResolutionMode = (StiImageResolutionMode)Enum.Parse(typeof(StiImageResolutionMode), (string)exportSettings["ImageResolutionMode"]);
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageResolution = (float)StrToDouble((string)exportSettings["ImageResolution"]);
                settings.ImageCompressionMethod = (StiPdfImageCompressionMethod)Enum.Parse(typeof(StiPdfImageCompressionMethod), (string)exportSettings["ImageCompressionMethod"]);
                if (exportSettings["ImageQuality"] != null) settings.ImageQuality = (float)StrToDouble((string)exportSettings["ImageQuality"]);
                //settings.StandardPdfFonts = (bool)exportSettings["StandardPdfFonts"];
                settings.EmbeddedFonts = (bool)exportSettings["EmbeddedFonts"];
                //settings.UseUnicode = (bool)exportSettings["UseUnicode"];
                //settings.Compressed = (bool)exportSettings["Compressed"];
                settings.ExportRtfTextAsImage = (bool)exportSettings["ExportRtfTextAsImage"];
                settings.PdfACompliance = (bool)exportSettings["PdfACompliance"];
                settings.AllowEditable = (string)exportSettings["AllowEditable"] == "Yes" ? StiPdfAllowEditable.Yes : StiPdfAllowEditable.No;
                settings.PasswordInputUser = (string)exportSettings["PasswordInputUser"];
                settings.PasswordInputOwner = (string)exportSettings["PasswordInputOwner"];
                settings.KeyLength = (StiPdfEncryptionKeyLength)Enum.Parse(typeof(StiPdfEncryptionKeyLength), (string)exportSettings["KeyLength"]);
                settings.UseDigitalSignature = (bool)exportSettings["UseDigitalSignature"];
                settings.GetCertificateFromCryptoUI = (bool)exportSettings["GetCertificateFromCryptoUI"];
                if (exportSettings["SubjectNameString"] != null) settings.SubjectNameString = (string)exportSettings["SubjectNameString"];
                settings.UserAccessPrivileges = StiUserAccessPrivileges.None;
                if (((string)exportSettings["UserAccessPrivileges"]) == "All")
                    settings.UserAccessPrivileges = StiUserAccessPrivileges.All;
                else
                {
                    if (((string)exportSettings["UserAccessPrivileges"]).IndexOf("PrintDocument") != -1) settings.UserAccessPrivileges |= StiUserAccessPrivileges.PrintDocument;
                    if (((string)exportSettings["UserAccessPrivileges"]).IndexOf("ModifyContents") != -1) settings.UserAccessPrivileges |= StiUserAccessPrivileges.ModifyContents;
                    if (((string)exportSettings["UserAccessPrivileges"]).IndexOf("CopyTextAndGraphics") != -1) settings.UserAccessPrivileges |= StiUserAccessPrivileges.CopyTextAndGraphics;
                    if (((string)exportSettings["UserAccessPrivileges"]).IndexOf("AddOrModifyTextAnnotations") != -1) settings.UserAccessPrivileges |= StiUserAccessPrivileges.AddOrModifyTextAnnotations;
                }

                PdfShowDialog = !(bool)exportSettings["OpenAfterExport"];
            }

            return settings;
        }

        public bool ProcessPdfRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiPdfExportSettings settings = GetPdfExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsPdf(this.Page, ReportForRendering, PdfShowDialog, settings);

                return true;
            }
            return false;
        }


        public bool ProcessXpsRequest()
        {
            return ProcessXpsRequest(false, null);
        }

        private StiXpsExportSettings GetXpsExportSettings(Hashtable exportSettings)
        {
            StiXpsExportSettings settings = new StiXpsExportSettings();
            settings.ImageQuality = ImageQuality;
            settings.ImageResolution = ImageResolution;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageResolution = (float)StrToDouble((string)exportSettings["ImageResolution"]);
                settings.ImageQuality = (float)StrToDouble((string)exportSettings["ImageQuality"]);
                settings.ExportRtfTextAsImage = (bool)exportSettings["ExportRtfTextAsImage"];
                XpsShowDialog = !(bool)exportSettings["OpenAfterExport"];
            }

            return settings;
        }

        public bool ProcessXpsRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiXpsExportSettings settings = GetXpsExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsXps(this.Page, ReportForRendering, XpsShowDialog, settings);

                return true;
            }
            return false;
        }


        public bool ProcessPowerPointRequest()
        {
            return ProcessPowerPointRequest(false, null);
        }

        private StiPpt2007ExportSettings GetPowerPointExportSettings(Hashtable exportSettings)
        {
            StiPpt2007ExportSettings settings = new StiPpt2007ExportSettings();
            settings.ImageQuality = ImageQuality;
            settings.ImageResolution = ImageResolution;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageResolution = (float)StrToDouble((string)exportSettings["ImageResolution"]);
                settings.ImageQuality = (float)StrToDouble((string)exportSettings["ImageQuality"]);
            }

            return settings;
        }

        public bool ProcessPowerPointRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiPpt2007ExportSettings settings = GetPowerPointExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsPpt(this.Page, ReportForRendering, true, settings);

                return true;
            }
            return false;
        }


        public bool ProcessHtmlRequest()
        {
            return ProcessHtmlRequest(false);
        }

        public bool ProcessHtmlRequest(bool saveFromAjaxMenu)
        {
            return ProcessHtmlRequest(saveFromAjaxMenu, HtmlShowDialog, null);
        }

        private StiHtmlExportSettings GetHtmlExportSettings(Hashtable exportSettings)
        {
            StiHtmlExportSettings settings = new StiHtmlExportSettings();
            settings.ImageFormat = ImageFormat;
            settings.Encoding = HtmlEncoding;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.Zoom = (float)StrToDouble((string)exportSettings["Zoom"]);
                switch ((string)exportSettings["ImageFormat"])
                {
                    case "Jpeg": this.ImageFormat = ImageFormat.Jpeg; break;
                    case "Gif": this.ImageFormat = ImageFormat.Gif; break;
                    case "Bmp": this.ImageFormat = ImageFormat.Bmp; break;
                    case "Png": this.ImageFormat = ImageFormat.Png; break;
                }
                settings.ImageFormat = this.ImageFormat;
                settings.ExportMode = (StiHtmlExportMode)Enum.Parse(typeof(StiHtmlExportMode), (string)exportSettings["ExportMode"]);
                settings.UseEmbeddedImages = (bool)exportSettings["UseEmbeddedImages"];
                settings.AddPageBreaks = (bool)exportSettings["AddPageBreaks"];
                settings.CompressToArchive = (bool)exportSettings["CompressToArchive"];
            }

            return settings;
        }

        public bool ProcessHtmlRequest(bool saveFromAjaxMenu, bool showDialog, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiHtmlExportSettings settings = GetHtmlExportSettings(exportSettings);

                if (exportSettings != null)
                    showDialog = !(bool)exportSettings["OpenAfterExport"];

                InvokeReportExport(ReportForRendering, settings);

                //if (ExportResponse) StiReportResponse.ResponseAsHtml(this.Page, ReportForRendering, showDialog, settings, WebImageHost);
                if (ExportResponse) StiReportResponse.ResponseAsHtml(this.Page, ReportForRendering, showDialog, settings, WebImageHost);
                return true;
            }
            return false;
        }


        public bool ProcessHtml5Request()
        {
            return ProcessHtml5Request(false);
        }

        public bool ProcessHtml5Request(bool saveFromAjaxMenu)
        {
            return ProcessHtml5Request(saveFromAjaxMenu, HtmlShowDialog, null);
        }

        private StiHtml5ExportSettings GetHtml5ExportSettings(Hashtable exportSettings)
        {
            StiHtml5ExportSettings settings = new StiHtml5ExportSettings();
            settings.ImageFormat = ImageFormat;
            settings.Encoding = HtmlEncoding;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                switch ((string)exportSettings["ImageFormat"])
                {
                    case "Jpeg": this.ImageFormat = ImageFormat.Jpeg; break;
                    case "Gif": this.ImageFormat = ImageFormat.Gif; break;
                    case "Bmp": this.ImageFormat = ImageFormat.Bmp; break;
                    case "Png": this.ImageFormat = ImageFormat.Png; break;
                }
                settings.ImageFormat = this.ImageFormat;
                settings.ImageResolution = (float)StrToDouble((string)exportSettings["ImageResolution"]);
                settings.ImageQuality = (float)StrToDouble((string)exportSettings["ImageQuality"]);
                settings.ContinuousPages = (bool)exportSettings["ContinuousPages"];
                settings.CompressToArchive = (bool)exportSettings["CompressToArchive"];
            }

            return settings;
        }

        public bool ProcessHtml5Request(bool saveFromAjaxMenu, bool showDialog, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiHtml5ExportSettings settings = GetHtml5ExportSettings(exportSettings);

                if (exportSettings != null)
                    showDialog = !(bool)exportSettings["OpenAfterExport"];

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsHtml(this.Page, ReportForRendering, showDialog, settings, WebImageHost);
                return true;
            }
            return false;
        }


        public bool ProcessMhtRequest()
        {
            return ProcessMhtRequest(false, null);
        }

        private StiMhtExportSettings GetMhtExportSettings(Hashtable exportSettings)
        {
            StiMhtExportSettings settings = new StiMhtExportSettings();
            settings.ImageFormat = ImageFormat;
            settings.Encoding = HtmlEncoding;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.Zoom = (float)StrToDouble((string)exportSettings["Zoom"]);
                switch ((string)exportSettings["ImageFormat"])
                {
                    case "Jpeg": this.ImageFormat = ImageFormat.Jpeg; break;
                    case "Gif": this.ImageFormat = ImageFormat.Gif; break;
                    case "Bmp": this.ImageFormat = ImageFormat.Bmp; break;
                    case "Png": this.ImageFormat = ImageFormat.Png; break;
                }
                settings.ImageFormat = this.ImageFormat;
                settings.ExportMode = (StiHtmlExportMode)Enum.Parse(typeof(StiHtmlExportMode), (string)exportSettings["ExportMode"]);
                settings.AddPageBreaks = (bool)exportSettings["AddPageBreaks"];
                settings.CompressToArchive = (bool)exportSettings["CompressToArchive"];
            }

            return settings;
        }

        public bool ProcessMhtRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiMhtExportSettings settings = GetMhtExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsMht(this.Page, ReportForRendering, true, settings);
                return true;
            }
            return false;
        }


        public bool ProcessTextRequest()
        {
            return ProcessTextRequest(false, null);
        }

        private StiTxtExportSettings GetTextExportSettings(Hashtable exportSettings)
        {
            StiTxtExportSettings settings = new StiTxtExportSettings();
            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.KillSpaceLines = (bool)exportSettings["KillSpaceLines"];
                settings.PutFeedPageCode = (bool)exportSettings["PutFeedPageCode"];
                settings.DrawBorder = (bool)exportSettings["DrawBorder"];
                settings.CutLongLines = (bool)exportSettings["CutLongLines"];
                settings.BorderType = (StiTxtBorderType)Enum.Parse(typeof(StiTxtBorderType), (string)exportSettings["BorderType"]);
                settings.ZoomX = (float)StrToDouble((string)exportSettings["ZoomX"]);
                settings.ZoomY = (float)StrToDouble((string)exportSettings["ZoomY"]);
                int codePage = 0;
                int.TryParse((string)exportSettings["Encoding"], out codePage);
                try { settings.Encoding = Encoding.GetEncoding(codePage); }
                catch { settings.Encoding = Encoding.UTF8; };
            }

            return settings;
        }

        public bool ProcessTextRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiTxtExportSettings settings = GetTextExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsText(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessRtfRequest()
        {
            return ProcessRtfRequest(false, null);
        }

        private StiRtfExportSettings GetRtfExportSettings(Hashtable exportSettings)
        {
            StiRtfExportSettings settings = new StiRtfExportSettings();
            settings.ExportMode = RtfExportMode;
            settings.ImageQuality = ImageQuality;
            settings.ImageResolution = ImageResolution;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageResolution = (float)StrToDouble((string)exportSettings["ImageResolution"]);
                settings.ImageQuality = (float)StrToDouble((string)exportSettings["ImageQuality"]);
                settings.ExportMode = (StiRtfExportMode)Enum.Parse(typeof(StiRtfExportMode), (string)exportSettings["ExportMode"]);
                settings.UsePageHeadersAndFooters = (bool)exportSettings["UsePageHeadersAndFooters"];
                settings.RemoveEmptySpaceAtBottom = (bool)exportSettings["RemoveEmptySpaceAtBottom"];
            }

            return settings;
        }

        public bool ProcessRtfRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiRtfExportSettings settings = GetRtfExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsRtf(this.Page, ReportForRendering, true, settings);

                return true;

            }
            return false;
        }


        public bool ProcessWord2007Request()
        {
            return ProcessWord2007Request(false, null);
        }

        private StiWord2007ExportSettings GetWord2007ExportSettings(Hashtable exportSettings)
        {
            StiWord2007ExportSettings settings = new StiWord2007ExportSettings();
            settings.ImageQuality = ImageQuality;
            settings.ImageResolution = ImageResolution;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageResolution = (float)StrToDouble((string)exportSettings["ImageResolution"]);
                settings.ImageQuality = (float)StrToDouble((string)exportSettings["ImageQuality"]);
                settings.UsePageHeadersAndFooters = (bool)exportSettings["UsePageHeadersAndFooters"];
                settings.RemoveEmptySpaceAtBottom = (bool)exportSettings["RemoveEmptySpaceAtBottom"];
                settings.RestrictEditing = (StiWord2007RestrictEditing)Enum.Parse(typeof(StiWord2007RestrictEditing), (string)exportSettings["RestrictEditing"]);
            }

            return settings;
        }

        public bool ProcessWord2007Request(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiWord2007ExportSettings settings = GetWord2007ExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsWord2007(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessOdtRequest()
        {
            return ProcessOdtRequest(false, null);
        }

        private StiOdtExportSettings GetOdtExportSettings(Hashtable exportSettings)
        {
            StiOdtExportSettings settings = new StiOdtExportSettings();
            settings.ImageQuality = ImageQuality;
            settings.ImageResolution = ImageResolution;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageResolution = (float)StrToDouble((string)exportSettings["ImageResolution"]);
                settings.ImageQuality = (float)StrToDouble((string)exportSettings["ImageQuality"]);
                settings.RemoveEmptySpaceAtBottom = (bool)exportSettings["RemoveEmptySpaceAtBottom"];
            }

            return settings;
        }

        public bool ProcessOdtRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiOdtExportSettings settings = GetOdtExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsOdt(this.Page, ReportForRendering);
                return true;
            }
            return false;
        }


        public bool ProcessXlsRequest()
        {
            return ProcessXlsRequest(false, null);
        }

        private StiExcelExportSettings GetXlsExportSettings(Hashtable exportSettings)
        {
            StiExcelExportSettings settings = new StiExcelExportSettings();
            settings.ImageQuality = ImageQuality;
            settings.ImageResolution = ImageResolution;
            settings.UseOnePageHeaderAndFooter = this.ExcelUseOnePageHeaderAndFooter;
            settings.ExportPageBreaks = this.ExcelPageBreaks;
            settings.ExportDataOnly = this.ExportDataOnly;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageResolution = (float)StrToDouble((string)exportSettings["ImageResolution"]);
                settings.ImageQuality = (float)StrToDouble((string)exportSettings["ImageQuality"]);
                settings.ExportDataOnly = (bool)exportSettings["ExportDataOnly"];
                settings.ExportObjectFormatting = (bool)exportSettings["ExportObjectFormatting"];
                settings.UseOnePageHeaderAndFooter = (bool)exportSettings["UseOnePageHeaderAndFooter"];
                settings.ExportEachPageToSheet = (bool)exportSettings["ExportEachPageToSheet"];
                settings.ExportPageBreaks = (bool)exportSettings["ExportPageBreaks"];
            }

            return settings;
        }

        public bool ProcessXlsRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiExcelExportSettings settings = GetXlsExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);
                if (ExportResponse) StiReportResponse.ResponseAsXls(this.Page, ReportForRendering, settings);

                return true;
            }
            return false;
        }


        public bool ProcessXlsXmlRequest()
        {
            return ProcessXlsXmlRequest(false, null);
        }

        private StiExcelXmlExportSettings GetXlsXmlExportSettings(Hashtable exportSettings)
        {
            StiExcelXmlExportSettings settings = new StiExcelXmlExportSettings();
            if (exportSettings != null)
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);

            return settings;
        }

        public bool ProcessXlsXmlRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiExcelXmlExportSettings settings = GetXlsXmlExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsXlsXml(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessExcel2007Request()
        {
            return ProcessExcel2007Request(false, null);
        }

        private StiExcel2007ExportSettings GetExcel2007ExportSettings(Hashtable exportSettings)
        {
            StiExcel2007ExportSettings settings = new StiExcel2007ExportSettings();
            settings.ImageQuality = ImageQuality;
            settings.ImageResolution = ImageResolution;
            settings.ExportDataOnly = ExportDataOnly;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageResolution = (float)StrToDouble((string)exportSettings["ImageResolution"]);
                settings.ImageQuality = (float)StrToDouble((string)exportSettings["ImageQuality"]);
                settings.ExportDataOnly = (bool)exportSettings["ExportDataOnly"];
                settings.ExportObjectFormatting = (bool)exportSettings["ExportObjectFormatting"];
                settings.UseOnePageHeaderAndFooter = (bool)exportSettings["UseOnePageHeaderAndFooter"];
                settings.ExportEachPageToSheet = (bool)exportSettings["ExportEachPageToSheet"];
                settings.ExportPageBreaks = (bool)exportSettings["ExportPageBreaks"];
            }

            return settings;
        }

        public bool ProcessExcel2007Request(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiExcel2007ExportSettings settings = GetExcel2007ExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsExcel2007(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessOdsRequest()
        {
            return ProcessOdsRequest(false, null);
        }

        private StiOdsExportSettings GetOdsExportSettings(Hashtable exportSettings)
        {
            StiOdsExportSettings settings = new StiOdsExportSettings();
            settings.ImageQuality = ImageQuality;
            settings.ImageResolution = ImageResolution;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageResolution = (float)StrToDouble((string)exportSettings["ImageResolution"]);
                settings.ImageQuality = (float)StrToDouble((string)exportSettings["ImageQuality"]);
            }

            return settings;
        }

        public bool ProcessOdsRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiOdsExportSettings settings = GetOdsExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsOds(this.Page, ReportForRendering);
                return true;
            }
            return false;
        }


        public bool ProcessCsvRequest()
        {
            return ProcessCsvRequest(false, null);
        }

        private StiCsvExportSettings GetCsvExportSettings(Hashtable exportSettings)
        {
            StiCsvExportSettings settings = new StiCsvExportSettings();
            settings.Separator = CsvSeparator.Length == 0 ? ";" : CsvSeparator;
            settings.Encoding = this.CsvEncoding;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.Separator = (string)exportSettings["Separator"];
                settings.SkipColumnHeaders = (bool)exportSettings["SkipColumnHeaders"];
                settings.DataExportMode = (StiDataExportMode)Enum.Parse(typeof(StiDataExportMode), (string)exportSettings["DataExportMode"]);
                int codePage = 0;
                int.TryParse((string)exportSettings["Encoding"], out codePage);
                try { settings.Encoding = Encoding.GetEncoding(codePage); }
                catch { settings.Encoding = Encoding.UTF8; };
            }

            return settings;
        }

        public bool ProcessCsvRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiCsvExportSettings settings = GetCsvExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsCsv(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessDbfRequest()
        {
            return ProcessDbfRequest(false, null);
        }

        private StiDbfExportSettings GetDbfExportSettings(Hashtable exportSettings)
        {
            StiDbfExportSettings settings = new StiDbfExportSettings();
            settings.CodePage = DbfCodePages;

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.CodePage = (StiDbfCodePages)Enum.Parse(typeof(StiDbfCodePages), (string)exportSettings["CodePage"]);
            }

            return settings;
        }

        public bool ProcessDbfRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiDbfExportSettings settings = GetDbfExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsDbf(this.Page, ReportForRendering, true, settings);
                return true;
            }
            return false;
        }


        public bool ProcessXmlRequest()
        {
            return ProcessXmlRequest(false, null);
        }

        private StiXmlExportSettings GetXmlExportSettings(Hashtable exportSettings)
        {
            StiXmlExportSettings settings = new StiXmlExportSettings();
            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.DataExportMode = (StiDataExportMode)Enum.Parse(typeof(StiDataExportMode), (string)exportSettings["DataExportMode"]);
            }

            return settings;
        }

        public bool ProcessXmlRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiXmlExportSettings settings = GetXmlExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsXml(this.Page, ReportForRendering);
                return true;
            }
            return false;
        }


        public bool ProcessDifRequest()
        {
            return ProcessDifRequest(false, null);
        }

        private StiDifExportSettings GetDifExportSettings(Hashtable exportSettings)
        {
            StiDifExportSettings settings = new StiDifExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ExportDataOnly = (bool)exportSettings["ExportDataOnly"];
                settings.UseDefaultSystemEncoding = (bool)exportSettings["UseDefaultSystemEncoding"];
                if (exportSettings["Encoding"] != null) settings.Encoding = Encoding.GetEncoding(StrToInt((string)exportSettings["Encoding"]));
            }

            return settings;
        }

        public bool ProcessDifRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiDifExportSettings settings = GetDifExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsDif(this.Page, ReportForRendering, true, settings);
                return true;
            }
            return false;
        }


        public bool ProcessSylkRequest()
        {
            return ProcessSylkRequest(false, null);
        }

        private StiSylkExportSettings GetSylkExportSettings(Hashtable exportSettings)
        {
            StiSylkExportSettings settings = new StiSylkExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ExportDataOnly = (bool)exportSettings["ExportDataOnly"];
                settings.UseDefaultSystemEncoding = (bool)exportSettings["UseDefaultSystemEncoding"];
                if (exportSettings["Encoding"] != null) settings.Encoding = Encoding.GetEncoding(StrToInt((string)exportSettings["Encoding"]));
            }

            return settings;
        }

        public bool ProcessSylkRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiSylkExportSettings settings = GetSylkExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsSylk(this.Page, ReportForRendering, true, settings);
                return true;
            }
            return false;
        }


        public bool ProcessBmpRequest()
        {
            return ProcessBmpRequest(false, null);
        }

        private StiBmpExportSettings GetBmpExportSettings(Hashtable exportSettings)
        {
            StiBmpExportSettings settings = new StiBmpExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageZoom = StrToDouble((string)exportSettings["ImageZoom"]);
                settings.ImageResolution = StrToInt((string)exportSettings["ImageResolution"]);
                settings.ImageFormat = (StiImageFormat)Enum.Parse(typeof(StiImageFormat), (string)exportSettings["ImageFormat"]);
                if (exportSettings["DitheringType"] != null) settings.DitheringType = (StiMonochromeDitheringType)Enum.Parse(typeof(StiMonochromeDitheringType), (string)exportSettings["DitheringType"]);
                if (exportSettings["TiffCompressionScheme"] != null) settings.TiffCompressionScheme = (StiTiffCompressionScheme)Enum.Parse(typeof(StiTiffCompressionScheme), (string)exportSettings["TiffCompressionScheme"]);
                settings.CutEdges = (bool)exportSettings["CutEdges"];

            }

            return settings;
        }

        public bool ProcessBmpRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiBmpExportSettings settings = GetBmpExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsBmp(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessGifRequest()
        {
            return ProcessGifRequest(false, null);
        }

        private StiGifExportSettings GetGifExportSettings(Hashtable exportSettings)
        {
            StiGifExportSettings settings = new StiGifExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageZoom = StrToDouble((string)exportSettings["ImageZoom"]);
                settings.ImageResolution = StrToInt((string)exportSettings["ImageResolution"]);
                settings.ImageFormat = (StiImageFormat)Enum.Parse(typeof(StiImageFormat), (string)exportSettings["ImageFormat"]);
                if (exportSettings["DitheringType"] != null) settings.DitheringType = (StiMonochromeDitheringType)Enum.Parse(typeof(StiMonochromeDitheringType), (string)exportSettings["DitheringType"]);
                if (exportSettings["TiffCompressionScheme"] != null) settings.TiffCompressionScheme = (StiTiffCompressionScheme)Enum.Parse(typeof(StiTiffCompressionScheme), (string)exportSettings["TiffCompressionScheme"]);
                settings.CutEdges = (bool)exportSettings["CutEdges"];
            }

            return settings;
        }

        public bool ProcessGifRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiGifExportSettings settings = GetGifExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsGif(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessJpegRequest()
        {
            return ProcessJpegRequest(false, null);
        }

        private StiJpegExportSettings GetJpegExportSettings(Hashtable exportSettings)
        {
            StiJpegExportSettings settings = new StiJpegExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageZoom = StrToDouble((string)exportSettings["ImageZoom"]);
                settings.ImageResolution = StrToInt((string)exportSettings["ImageResolution"]);
                settings.ImageFormat = (StiImageFormat)Enum.Parse(typeof(StiImageFormat), (string)exportSettings["ImageFormat"]);
                if (exportSettings["DitheringType"] != null) settings.DitheringType = (StiMonochromeDitheringType)Enum.Parse(typeof(StiMonochromeDitheringType), (string)exportSettings["DitheringType"]);
                if (exportSettings["TiffCompressionScheme"] != null) settings.TiffCompressionScheme = (StiTiffCompressionScheme)Enum.Parse(typeof(StiTiffCompressionScheme), (string)exportSettings["TiffCompressionScheme"]);
                settings.CutEdges = (bool)exportSettings["CutEdges"];
            }

            return settings;
        }

        public bool ProcessJpegRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiJpegExportSettings settings = GetJpegExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsJpeg(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessPcxRequest()
        {
            return ProcessPcxRequest(false, null);
        }

        private StiPcxExportSettings GetPcxExportSettings(Hashtable exportSettings)
        {
            StiPcxExportSettings settings = new StiPcxExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageZoom = StrToDouble((string)exportSettings["ImageZoom"]);
                settings.ImageResolution = StrToInt((string)exportSettings["ImageResolution"]);
                settings.ImageFormat = (StiImageFormat)Enum.Parse(typeof(StiImageFormat), (string)exportSettings["ImageFormat"]);
                if (exportSettings["DitheringType"] != null) settings.DitheringType = (StiMonochromeDitheringType)Enum.Parse(typeof(StiMonochromeDitheringType), (string)exportSettings["DitheringType"]);
                if (exportSettings["TiffCompressionScheme"] != null) settings.TiffCompressionScheme = (StiTiffCompressionScheme)Enum.Parse(typeof(StiTiffCompressionScheme), (string)exportSettings["TiffCompressionScheme"]);
                settings.CutEdges = (bool)exportSettings["CutEdges"];
            }

            return settings;
        }

        public bool ProcessPcxRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiPcxExportSettings settings = GetPcxExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsPcx(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessPngRequest()
        {
            return ProcessPngRequest(false, null);
        }

        private StiPngExportSettings GetPngExportSettings(Hashtable exportSettings)
        {
            StiPngExportSettings settings = new StiPngExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageZoom = StrToDouble((string)exportSettings["ImageZoom"]);
                settings.ImageResolution = StrToInt((string)exportSettings["ImageResolution"]);
                settings.ImageFormat = (StiImageFormat)Enum.Parse(typeof(StiImageFormat), (string)exportSettings["ImageFormat"]);
                if (exportSettings["DitheringType"] != null) settings.DitheringType = (StiMonochromeDitheringType)Enum.Parse(typeof(StiMonochromeDitheringType), (string)exportSettings["DitheringType"]);
                if (exportSettings["TiffCompressionScheme"] != null) settings.TiffCompressionScheme = (StiTiffCompressionScheme)Enum.Parse(typeof(StiTiffCompressionScheme), (string)exportSettings["TiffCompressionScheme"]);
                settings.CutEdges = (bool)exportSettings["CutEdges"];
            }

            return settings;
        }

        public bool ProcessPngRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiPngExportSettings settings = GetPngExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsPng(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessTiffRequest()
        {
            return ProcessTiffRequest(false, null);
        }

        private StiTiffExportSettings GetTiffExportSettings(Hashtable exportSettings)
        {
            StiTiffExportSettings settings = new StiTiffExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageZoom = StrToDouble((string)exportSettings["ImageZoom"]);
                settings.ImageResolution = StrToInt((string)exportSettings["ImageResolution"]);
                settings.ImageFormat = (StiImageFormat)Enum.Parse(typeof(StiImageFormat), (string)exportSettings["ImageFormat"]);
                if (exportSettings["DitheringType"] != null) settings.DitheringType = (StiMonochromeDitheringType)Enum.Parse(typeof(StiMonochromeDitheringType), (string)exportSettings["DitheringType"]);
                if (exportSettings["TiffCompressionScheme"] != null) settings.TiffCompressionScheme = (StiTiffCompressionScheme)Enum.Parse(typeof(StiTiffCompressionScheme), (string)exportSettings["TiffCompressionScheme"]);
                settings.CutEdges = (bool)exportSettings["CutEdges"];
            }

            return settings;
        }

        public bool ProcessTiffRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiTiffExportSettings settings = GetTiffExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsTiff(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessMetafileRequest()
        {
            return ProcessMetafileRequest(false, null);
        }

        private StiEmfExportSettings GetMetafileExportSettings(Hashtable exportSettings)
        {
            StiEmfExportSettings settings = new StiEmfExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageZoom = StrToDouble((string)exportSettings["ImageZoom"]);
                settings.ImageResolution = StrToInt((string)exportSettings["ImageResolution"]);
                settings.ImageFormat = (StiImageFormat)Enum.Parse(typeof(StiImageFormat), (string)exportSettings["ImageFormat"]);
                if (exportSettings["DitheringType"] != null) settings.DitheringType = (StiMonochromeDitheringType)Enum.Parse(typeof(StiMonochromeDitheringType), (string)exportSettings["DitheringType"]);
                if (exportSettings["TiffCompressionScheme"] != null) settings.TiffCompressionScheme = (StiTiffCompressionScheme)Enum.Parse(typeof(StiTiffCompressionScheme), (string)exportSettings["TiffCompressionScheme"]);
                settings.CutEdges = (bool)exportSettings["CutEdges"];
            }

            return settings;
        }

        public bool ProcessMetafileRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiEmfExportSettings settings = GetMetafileExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsMetafile(this.Page, ReportForRendering, settings);
                return true;
            }
            return false;
        }


        public bool ProcessSvgRequest()
        {
            return ProcessSvgRequest(false, null);
        }

        private StiSvgExportSettings GetSvgExportSettings(Hashtable exportSettings)
        {
            StiSvgExportSettings settings = new StiSvgExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageZoom = StrToDouble((string)exportSettings["ImageZoom"]);
                settings.ImageResolution = StrToInt((string)exportSettings["ImageResolution"]);
                settings.ImageFormat = (StiImageFormat)Enum.Parse(typeof(StiImageFormat), (string)exportSettings["ImageFormat"]);
                if (exportSettings["DitheringType"] != null) settings.DitheringType = (StiMonochromeDitheringType)Enum.Parse(typeof(StiMonochromeDitheringType), (string)exportSettings["DitheringType"]);
                if (exportSettings["TiffCompressionScheme"] != null) settings.TiffCompressionScheme = (StiTiffCompressionScheme)Enum.Parse(typeof(StiTiffCompressionScheme), (string)exportSettings["TiffCompressionScheme"]);
                settings.CutEdges = (bool)exportSettings["CutEdges"];
            }

            return settings;
        }

        public bool ProcessSvgRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiSvgExportSettings settings = GetSvgExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsSvg(this.Page, ReportForRendering, true, settings);
                return true;
            }
            return false;
        }


        public bool ProcessSvgzRequest()
        {
            return ProcessSvgzRequest(false, null);
        }

        private StiSvgzExportSettings GetSvgzExportSettings(Hashtable exportSettings)
        {
            StiSvgzExportSettings settings = new StiSvgzExportSettings();

            if (ViewMode == StiWebViewMode.OnePage) settings.PageRange = new StiPagesRange(this.CurrentPage);
            else settings.PageRange = new StiPagesRange(0);

            if (exportSettings != null)
            {
                settings.PageRange = new StiPagesRange((string)exportSettings["PageRange"]);
                settings.ImageZoom = StrToDouble((string)exportSettings["ImageZoom"]);
                settings.ImageResolution = StrToInt((string)exportSettings["ImageResolution"]);
                settings.ImageFormat = (StiImageFormat)Enum.Parse(typeof(StiImageFormat), (string)exportSettings["ImageFormat"]);
                if (exportSettings["DitheringType"] != null) settings.DitheringType = (StiMonochromeDitheringType)Enum.Parse(typeof(StiMonochromeDitheringType), (string)exportSettings["DitheringType"]);
                if (exportSettings["TiffCompressionScheme"] != null) settings.TiffCompressionScheme = (StiTiffCompressionScheme)Enum.Parse(typeof(StiTiffCompressionScheme), (string)exportSettings["TiffCompressionScheme"]);
                settings.CutEdges = (bool)exportSettings["CutEdges"];
            }

            return settings;
        }

        public bool ProcessSvgzRequest(bool saveFromAjaxMenu, Hashtable exportSettings)
        {
            if (saveFromAjaxMenu)
            {
                if (Report == null) throw new ArgumentNullException(ReportNotAssigned);

                StiSvgzExportSettings settings = GetSvgzExportSettings(exportSettings);

                InvokeReportExport(ReportForRendering, settings);

                if (ExportResponse) StiReportResponse.ResponseAsSvgz(this.Page, ReportForRendering, true, settings);
                return true;
            }
            return false;
        }

        private string GetRequestParam(string key)
        {
            var value = Page.Request.QueryString[key];
            return value ?? Page.Request.Params[key];
        }

        private void ResponseString(string message)
        {
            var buffer = Encoding.UTF8.GetBytes(message);
            ResponseBuffer(buffer, false, "text/plain");
        }

        //Use only for resources, browser cache enabled
        private void ResponseString(string message, string contentType)
        {
            var buffer = Encoding.UTF8.GetBytes(message);
            ResponseBuffer(buffer, true, contentType);
        }

        private void ResponseBuffer(byte[] buffer, bool enableBrowserCache = false, string contentType = null)
        {
            ResponseBuffer(buffer, null, enableBrowserCache, contentType);
        }

        private void ResponseBuffer(byte[] buffer, string fileName, string contentType = null)
        {
            ResponseBuffer(buffer, fileName, false, contentType);
        }

        private void ResponseBuffer(byte[] buffer, string fileName, bool enableBrowserCache = false, string contentType = null)
        {
            Page.Response.Buffer = true;
            Page.Response.ClearContent();
            Page.Response.ClearHeaders();

            Page.Response.AddHeader("Content-Length", buffer.Length.ToString());
            Page.Response.ContentType = contentType ?? "application/octet-stream";
            Page.Response.ContentEncoding = Encoding.UTF8;

            if (fileName != null)
            {
                Page.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=\"{0}\"", fileName));
            }
            else
            {
                Page.Response.AddHeader("Cache-Control", enableBrowserCache ? "public, max-age=31536000" : "no-cache, no-store");
                if (enableBrowserCache)
                {
                    Page.Response.Cache.SetExpires(DateTime.Now.AddYears(1));
                    Page.Response.Cache.SetCacheability(HttpCacheability.Public);
                }
                else
                {
                    Page.Response.Cache.SetAllowResponseInBrowserHistory(false);
                    Page.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Page.Response.Cache.SetNoStore();
                    Page.Response.Cache.SetExpires(DateTime.Now);
                }
            }

            Page.Response.BinaryWrite(buffer);

            try
            {
                if (StiOptions.Web.AllowUseResponseFlush) Page.Response.Flush();
                Page.Response.End();
            }
            catch
            {

            }
        }

        private void RenderCssStyles()
        {
            var url = GetRequestUrl("stimobileviewer_css", Theme.ToString(), PassQueryParametersForResources);
            url += url.IndexOf("?") > 0 ? "&" : "?";
            url += "mobileviewer_version=" + GetProductVersion(true, this.Report != null ? this.Report.ReportGuid : null);
            var css = string.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />", string.IsNullOrEmpty(CustomCss) ? url : CustomCss);
            RegisterClientScriptBlockIntoHeader(url, css);
        }

        private string GetRequestUrl(string parameter, string value)
        {
            return GetRequestUrl(parameter, value, true);
        }

        private string GetRequestUrl(string parameter, string value, bool passQueryParams)
        {
            string url = this.Page.Request.Url.AbsolutePath;
            if (this.UseRelativeUrls) url = this.Page.Response.ApplyAppPathModifier(url);

            if (parameter != null && value != null)
            {
                string query = passQueryParams ? this.Page.Request.Url.Query : string.Empty;
                if (!string.IsNullOrEmpty(query)) url = string.Format("{0}{1}&{2}={3}", url, query, parameter, value);
                else url = string.Format("{0}?{1}={2}", url, parameter, value);

                if (CloudMode && parameter == "mobileviewer_script")
                {
                    if (this.Page.Request.Params["localizationName"] != null) url += "&localizationName=" + this.Page.Request.Params["localizationName"];
                    if (this.Page.Request.Params["sessionKey"] != null) url += "&sessionKey=" + this.Page.Request.Params["sessionKey"];
                }

                url = url.Replace("'", "\\'").Replace("\"", "&quot;");
            }

            return url;
        }

        private Hashtable GetCssConstants(string cssText)
        {
            Hashtable constants = new Hashtable();
            int startIndex = cssText.IndexOf('@');
            var constantsStr = cssText.Substring(startIndex, cssText.LastIndexOf(';') - startIndex);
            string[] constantsArray = constantsStr.Split(';');
            for (int i = 0; i < constantsArray.Length; i++)
            {
                string[] tmpArray = constantsArray[i].Split('=');
                if (tmpArray.Length == 2)
                {
                    constants[tmpArray[0].Trim()] = tmpArray[1];
                }
            }

            return constants;
        }

        public bool ProcessCssRequest()
        {
            var themeName = GetRequestParam("stimobileviewer_css");
            if (themeName == null) return false;

            Assembly a = typeof(StiMobileViewer).Assembly;
            var names = a.GetManifestResourceNames();
            var css = string.Empty;
            var path = string.Format("{0}.{1}.", STYLES_PATH, themeName.StartsWith("Office2013") ? "Office2013" : themeName.StartsWith("Material") ? "Material" : themeName);
            Hashtable constants = null;

            foreach (var name in names)
            {
                if (name.IndexOf(path) == 0 && name.EndsWith(".css"))
                {
                    Stream stream = a.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string cssText = reader.ReadToEnd();
                        if (name.EndsWith(themeName + ".Constants.css")) constants = GetCssConstants(cssText);
                        else if (!name.EndsWith(".Constants.css")) css += cssText + "\r\n";
                    }
                }
            }

            if (constants != null)
            {
                foreach (DictionaryEntry constant in constants)
                {
                    css = css.Replace((string)constant.Key, (string)constant.Value);
                }
            }

            ResponseString(css, "text/css");
            return true;
        }

        public bool ProcessImageRequest()
        {
            string imageGuid = GetRequest("stimulsoft_mobile_image") as string;
            if (imageGuid == null) return false;

            byte[] imageBuffer = null;
            if (imageGuid.EndsWith(".gif") || imageGuid.EndsWith(".png"))
            {
                System.Drawing.Image image = GetImage(imageGuid);
                if (image != null) imageBuffer = StiImageConverter.ImageToBytes(image);
            }
            else
            {
                if (CacheHelper != null)
                {
                    imageBuffer = CacheHelper.GetObjectFromCache(imageGuid) as byte[];
                }
                else
                {
                    if (CacheMode == StiCacheMode.Page) imageBuffer = (byte[])this.Page.Cache[imageGuid];
                    else imageBuffer = (byte[])this.Page.Session[imageGuid];
                }
            }

            this.Page.Response.ClearContent();
            this.Page.Response.ContentType = "image/" + this.ImageFormat.ToString();
            if (imageBuffer != null) this.Page.Response.BinaryWrite(imageBuffer);
            this.Page.Response.End();

            return true;
        }

#if SERVER
        private bool ResponseSharedResourceNotFoundError(string message)
        {
            Assembly a = typeof(StiMobileViewer).Assembly;
            string imageSrc = string.Empty;
            Stream stream = a.GetManifestResourceStream("Stimulsoft.Report.Mobile.Images.Office2013.MsgFormError.png");
            if (stream != null)
            {
                MemoryStream ms = new MemoryStream();
                stream.CopyTo(ms);
                byte[] buff = ms.ToArray();
                ms.Dispose();
                stream.Dispose();

                imageSrc = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(buff));
            }

            string htmlText = "<div style='width: 380px; margin-left: -175px; margin-top: -20px; top: 50%; left: 50%; position: absolute; ";
            htmlText += "border: 1px solid #c6c6c6; -moz-box-shadow: 0px 0px 7px rgba(0,0,0,0.6); -webkit-box-shadow: 0px 0px 7px rgba(0,0,0,0.6); ";
            htmlText += "box-shadow: 0 0 7px rgba(0,0,0,0.3); padding: 20px; 50px 20px; 50px' >";
            htmlText += "<table><tr><td><img src=\"" + imageSrc + "\" /></td><td style='font-family: Arial; font-size: 14px; padding-left: 10px;'>An error occurred while loading the shared resource. ";
            htmlText += message + "</td></tr></table>";
            htmlText += "</div>";

            var buffer = Encoding.Default.GetBytes(htmlText);
            ResponseBuffer(buffer, null, "text/html");

            return true;
        }

        private bool ProcessShareRequest()
        {
            var shareKey = GetRequestParam("shareKey");
            var resource = GetRequestParam("stimobileviewer_css") + GetRequestParam("mobileviewer_script") + GetRequestParam("stimulsoft_mobile_image") + GetRequestParam("sr_print");
            if (string.IsNullOrEmpty(shareKey) || !string.IsNullOrEmpty(resource)) return false;

            var parts = string.IsNullOrEmpty(shareKey) ? new string[] { } : shareKey.Split('/');
            if (parts.Length == 0) return ResponseSharedResourceNotFoundError("The share key is empty.");

            var itemKey = parts[0];
            if (!string.IsNullOrEmpty(shareKey) && shareKey.Length < 32)
            {
                try
                {
                    itemKey = TinyKey.GetFullKeyFromShortKey(parts[0]);
                }
                catch (Exception e)
                {
                    return ResponseSharedResourceNotFoundError(e.Message);
                }

                if (StiKeyHelper.IsEmptyKey(itemKey))
                    return ResponseSharedResourceNotFoundError("The item key is empty.");
            }

            string shareType = parts.Length > 1 ? parts[1] : "show";
            var sessionKey = GetRequestParam("SessionKey");

            var getShareInfo = new StiItemCommands.GetShareInfo { ItemKey = itemKey };
            getShareInfo = (StiItemCommands.GetShareInfo)StiCommandToServer.RunCommand(getShareInfo);

            if (getShareInfo.ResultShareLevel == null)
            {
                return ResponseSharedResourceNotFoundError("The specified item is not using sharing.");
            }
            else if (getShareInfo.ResultShareLevel == StiShareLevel.Private)
            {
                return ResponseSharedResourceNotFoundError("The specified item is not shared.");
            }

            if (getShareInfo.ResultShareLevel == StiShareLevel.Public || (getShareInfo.ResultShareLevel == StiShareLevel.Registered && !string.IsNullOrEmpty(sessionKey)))
            {
                var getItem = new StiItemCommands.Get();
                getItem.ItemKey = itemKey;
                getItem.SessionKey = sessionKey;
                getItem = (StiItemCommands.Get)StiCommandToServer.RunCommand(getItem);

                if (!(getItem.ResultItem is StiFileItem || getItem.ResultItem is StiReportTemplateItem || getItem.ResultItem is StiReportSnapshotItem))
                {
                    return ResponseSharedResourceNotFoundError("The specified item is not intended for sharing.");
                }

                var contentType = "text/xml";
                var fileItem = getItem.ResultItem as StiFileItem;
                if (fileItem != null)
                {
                    switch (fileItem.FileType)
                    {
                        case StiFileType.Csv: contentType = "text/csv"; break;
                        case StiFileType.Excel: contentType = "application/excel"; break;
                        case StiFileType.Html: contentType = "text/html"; break;
                        case StiFileType.Image: contentType = "image/jpeg"; break;
                        case StiFileType.Pdf: contentType = "application/pdf"; break;
                        case StiFileType.PowerPoint: contentType = "application/powerpoint"; break;
                        case StiFileType.RichText: contentType = "text/richtext"; break;
                        case StiFileType.Text: contentType = "text/plain"; break;
                        case StiFileType.Word: contentType = "application/msword"; break;
                        case StiFileType.Json: contentType = "application/json"; break;
                        case StiFileType.Xml: contentType = "text/xml"; break;
                        case StiFileType.Xps: contentType = "application/oxps"; break;
                        case StiFileType.Xsd: contentType = "text/xml"; break;

                        default: contentType = "application/octet-stream"; break;
                    }
                }

                // Show report snapshot or report template in viewer
                if (shareType != "download" && getItem.ResultItem != null && (getItem.ResultItem is StiReportTemplateItem || getItem.ResultItem is StiReportSnapshotItem))
                    return false;

                var getResource = new StiItemResourceCommands.Get();
                getResource.ItemKey = itemKey;
                getResource.SessionKey = sessionKey;
                getResource = (StiItemResourceCommands.Get)StiCommandToServer.RunCommand(getResource);

                if (getResource.ResultResource == null)
                {
                    return ResponseSharedResourceNotFoundError("The specified resource was not found.");
                }

                // Show resource in browser window
                if (shareType != "download")
                {
                    ResponseBuffer(getResource.ResultResource, null, contentType);
                    return true;
                }

                // Download shared resource
                var fileName = getItem.ResultItem.Name;
                if (getItem.ResultItem is StiReportTemplateItem) fileName += ".mrt";
                if (getItem.ResultItem is StiReportSnapshotItem) fileName += ".mdc";
                ResponseBuffer(getResource.ResultResource, fileName, contentType);
                return true;
            }

            return false;
        }
#endif

        #endregion

        #region Methods.Print
        public void PrintToPdf()
        {
            StiPdfExportSettings settings = new StiPdfExportSettings();
            settings.PageRange = GetPrintPagesRange();
            settings.ImageQuality = ImageQuality;
            settings.ImageResolution = ImageResolution;
            settings.EmbeddedFonts = PdfEmbeddedFonts;
            settings.StandardPdfFonts = PdfStandardFonts;
            settings.AutoPrintMode = StiPdfAutoPrintMode.Dialog;

            StiReportResponse.ResponseAsPdf(this.Page, ReportForRendering, false, settings);
        }
        #endregion

        #region Methods.Navigation

        public void SetPage(int pageNumber)
        {
            if (ReportForRendering != null)
            {
                CurrentPage = Math.Max(Math.Min(pageNumber, ReportForRendering.RenderedPages.Count - 1), 0);
            }
        }
        #endregion

        #region Image Resources
        private Hashtable GetImagesArray()
        {
            Hashtable images = new Hashtable();

            Assembly a = typeof(StiMobileViewer).Assembly;
            var names = a.GetManifestResourceNames();
            var js = string.Empty;

            string imagesTheme = this.Theme.ToString().StartsWith("Office2013") ? "Office2013" : this.Theme.ToString().StartsWith("Material") ? "Material" : this.Theme.ToString();
            var path = "Stimulsoft.Report.Mobile.Images." + imagesTheme + ".";

            foreach (var name in names)
            {
                if (name.IndexOf(path) == 0 && (name.EndsWith(".png") || name.EndsWith(".gif")))
                {
                    if (!CloudMode && (name.IndexOf(".CloudIcons.") >= 0 || name.IndexOf(".LogIn.") >= 0)) continue;

                    Stream stream = a.GetManifestResourceStream(name);
                    if (stream == null) return null;

                    MemoryStream ms = new MemoryStream();
                    stream.CopyTo(ms);
                    byte[] buffer = ms.ToArray();
                    ms.Dispose();
                    stream.Dispose();

                    string imageName = name.Replace(path, "");
                    images[imageName] = string.Format("data:image/{0};base64,{1}", imageName.Substring(imageName.Length - 3), Convert.ToBase64String(buffer));
                }
            }

            return images;
        }
        #endregion

        #region Methods.Render

        private string GetJSParameters()
        {
            if (!IsDesignMode && !string.IsNullOrEmpty(GlobalizationFile))
            {
                try
                {
                    if (GlobalizationFile.ToLower() == "default" || GlobalizationFile == "en" || GlobalizationFile == "en.xml") StiLocalization.LoadDefaultLocalization();
                    else StiLocalization.Load(Page.Request.PhysicalApplicationPath + GlobalizationFile);
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
            }

            Hashtable parameters = new Hashtable();

            #region Cloud Mode
            Hashtable viewerParams = null;
#if SERVER
            if (CloudMode)
            {
                viewerParams = GetViewerParametersFromCloud();
                if (viewerParams != null && viewerParams.Contains("themeName"))
                {
                    ZoomPercent = 100;
                    Theme = (StiMobileViewerTheme)Enum.Parse(typeof(StiMobileViewerTheme), (string)viewerParams["themeName"]);
                    parameters["cloudParameters"] = viewerParams;

                    try
                    {
                        #region New Session
                        var newSessionCommand = new StiUserCommands.NewSession()
                        {
                            SessionKey = (string)viewerParams["sessionKey"]
                        };

                        var resultNewSessionCommand = RunCommand(newSessionCommand) as StiUserCommands.NewSession;
                        if (resultNewSessionCommand.ResultSuccess && resultNewSessionCommand.ResultSessionKey != null)
                        {
                            viewerParams["sessionKey"] = resultNewSessionCommand.ResultSessionKey;
                        }
                        else
                        {
                            if (resultNewSessionCommand.ResultNotice != null)
                                parameters["errorMessage"] = StiNoticeConverter.Convert(resultNewSessionCommand.ResultNotice);
                        }
                        #endregion
                    }
                    catch (Exception e)
                    {
                        parameters["errorMessage"] = e.Message;
                    }
                }
                else
                {
                    var shareKey = GetRequestParam("shareKey");
                    var request = HttpContext.Current.Request;
                    var appUrl = HttpRuntime.AppDomainAppVirtualPath;

                    if (!string.IsNullOrEmpty(shareKey))
                    {
                        var parts = shareKey.Split('/');

                        viewerParams = new Hashtable();
                        viewerParams["shareKey"] = TinyKey.GetFullKeyFromShortKey(parts[0]);
                        viewerParams["themeName"] = this.Theme.ToString();
                        viewerParams["restUrl"] = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);
                        viewerParams["dVers"] = StiVersionX.IsSvr;
                        viewerParams["postParams"] = GetViewerParametersFromCloud();

                        parameters["cloudParameters"] = viewerParams;
                    }
                    else if (!string.IsNullOrEmpty(request.Url.AbsolutePath))
                    {
                        string urlParams = StiEncodingHelper.DecodeString(request.Url.AbsolutePath.Substring(request.Url.AbsolutePath.LastIndexOf("/") + 1));
                        var paramsArray = urlParams.Split(';');
                        if (paramsArray.Length == 2)
                        {
                            string itemKey = TinyKey.GetFullKeyFromShortKey(paramsArray[0]);
                            string sessionKey = paramsArray[1];

                            var getItem = new StiItemCommands.Get()
                            {
                                ItemKey = itemKey
                            };

                            getItem = (StiItemCommands.Get)StiCommandToServer.RunCommand(getItem);

                            if (getItem.ResultItem != null && (getItem.ResultItem is StiReportTemplateItem || getItem.ResultItem is StiReportSnapshotItem))
                            {
                                //var userLogin = new StiUserCommands.Login()
                                //{
                                //    AuthType = StiServerAuthType.Anonymous
                                //};

                                //userLogin = (StiUserCommands.Login)StiCommandToServer.RunCommand(userLogin);
                                //if (userLogin.ResultSuccess)
                                //{
                                var newSessionCommand = new StiUserCommands.NewSession()
                                {
                                    SessionKey = sessionKey
                                };

                                var resultNewSessionCommand = RunCommand(newSessionCommand) as StiUserCommands.NewSession;
                                if (resultNewSessionCommand.ResultSuccess && resultNewSessionCommand.ResultSessionKey != null)
                                {
                                    viewerParams = new Hashtable();
                                    viewerParams["userName"] = "";
                                    viewerParams["sessionKey"] = resultNewSessionCommand.ResultSessionKey;//userLogin.ResultSessionKey;
                                    viewerParams["localizationName"] = "en";
                                    viewerParams["themeName"] = "Office2013WhiteTeal";
                                    viewerParams["restUrl"] = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);
                                    byte[] utfBytes = Encoding.UTF8.GetBytes(getItem.ResultItem.Name);
                                    viewerParams["reportName"] = Encoding.UTF8.GetString(utfBytes, 0, utfBytes.Length);
                                    viewerParams["favIcon"] = "favicon_.ico";
                                    viewerParams["folderKey"] = getItem.ResultItem.FolderKey ?? "root";
                                    viewerParams["report" + (getItem.ResultItem is StiReportTemplateItem ? "Template" : "Snapshot") + "ItemKey"] = itemKey;
                                    viewerParams["dVers"] = StiVersionX.IsSvr;

                                    parameters["cloudParameters"] = viewerParams;
                                }
                                //}
                            }
                        }
                    }
                }
            }

#endif
            #endregion

            if (Report != null && Report.NeedsCompiling && !Report.IsCompiled) Report.Compile();
            var rep = Report != null ? Report.CompiledReport != null ? Report.CompiledReport : Report : null;

            if (ScrollbiMode) parameters["scrollbiParameters"] = ScrollbiParameters;
            if (DemoMode)
            {
                parameters["demolocalization"] = GetRequestParam("demolocalization");
                parameters["reportName"] = GetRequestParam("reportname");
                if (GetRequestParam("themename") != null)
                {
                    Theme = (StiMobileViewerTheme)Enum.Parse(typeof(StiMobileViewerTheme), (string)GetRequestParam("themename"));
                }
            }

            parameters["mobileViewerId"] = this.ClientID;
            parameters["pagesArray"] = (rep != null) ? GetPagesArray(rep, Theme.ToString().StartsWith("Material")) : null;
            parameters["imagesPath"] = this.GetImageUrl();
            parameters["pageNumber"] = CurrentPage;
            parameters["pagesCount"] = PagesCount;
            parameters["zoom"] = ZoomPercent;
            parameters["menuViewMode"] = ViewMode.ToString();
            parameters["countColumnsParameters"] = CountColumnsParameters;
            parameters["maxHeightParametersPanel"] = MaxHeightParametersPanel;
            parameters["bookmarksTreeWidth"] = BookmarksTreeWidth;
            parameters["bookmarksTreeHeight"] = BookmarksTreeHeight;
            parameters["scrollbarsMode"] = FullScreen ? true : ScrollbarsMode;
            parameters["callbackFunctionForExport"] = this.Page.ClientScript.GetPostBackEventReference(this, "callbackParams");
            parameters["callbackFunction"] = this.Page.ClientScript.GetCallbackEventReference(this, "'callbackParams'", "js" + this.ClientID + ".receveFromServer", "null", "js" + this.ClientID + ".errorFromServer", true);
            parameters["paramsVariables"] = (rep != null) ? StiParametersClass.GetVariables(rep) : null;
            parameters["showPanelParameters"] = ShowPanelParameters;
            parameters["showPageShadow"] = ShowPageShadow;
            parameters["pageBorderColor"] = GetHtmlColor(this.PageBorderColor);
            parameters["pageBorderSize"] = PageBorderSize;
            parameters["showPageBorders"] = ShowPageBorders;
            parameters["paramsBookmarks"] = (AllowBookmarks && ReportForRendering != null && ReportForRendering.Bookmark != null && ReportForRendering.Bookmark.Bookmarks.Count > 0)
                ? RenderBookmarks(rep, this.ClientID, ViewMode == StiWebViewMode.OnePage ? CurrentPage : -1)
                : null;
            parameters["theme"] = CloudMode && viewerParams != null ? viewerParams["themeName"] : this.Theme.ToString();
            parameters["allowTouchZoom"] = AllowTouchZoom;
            parameters["printDestination"] = PrintDestination.ToString();
            parameters["cloudMode"] = CloudMode;
            parameters["designerMode"] = DesignerMode;
            parameters["demoMode"] = DemoMode;
            parameters["helpLanguage"] = CloudMode && viewerParams != null ? viewerParams["localizationName"] : GetCurrentHelpLanguage();
            parameters["showTooltips"] = ShowTooltips;
            parameters["showTooltipsHelp"] = ShowTooltipsHelp;
            parameters["productVersion"] = GetProductVersion(false, this.Report != null ? this.Report.ReportGuid : null);
            parameters["fullScreen"] = FullScreen;
            parameters["contentAlignment"] = ContentAlignment.ToString().ToLower();
            parameters["viewerHeightType"] = this.Height.Type.ToString();
            parameters["interfaceType"] = InterfaceType.ToString();
            parameters["const_dateTime1970InTicks"] = (new DateTime(1970, 1, 1)).Ticks;
            parameters["isEditableReport"] = (rep != null) ? CheckEditableReport(rep) : false;
            parameters["reportDisplayMode"] = ReportDisplayMode.ToString();
            parameters["datePickerFirstDayOfWeek"] = DatePickerFirstDayOfWeek.ToString();
            parameters["storeExportSettings"] = StoreExportSettings;
            parameters["requestUrl"] = GetApplicationUrl(false, true);

            //ToolBar
            parameters["rightToLeft"] = RightToLeft;
            parameters["showToolBar"] = ShowToolBar;
            parameters["toolbarAlignment"] = ToolbarAlignment.ToString().ToLower();
            parameters["toolBarBackColor"] = ToolBarBackColor == Color.Empty ? "Empty" : GetHtmlColor(ToolBarBackColor);
            parameters["toolbarFontColor"] = ToolbarFontColor == Color.Empty ? "Empty" : GetHtmlColor(ToolbarFontColor);
            parameters["toolbarFontFamily"] = ToolbarFontFamily;

            parameters["showPrintButton"] = ShowPrintButton;
            parameters["showSave"] = ShowSave;
            parameters["showBookmarksButton"] = ShowBookmarksButton;
            parameters["showParametersButton"] = ShowParametersButton;
            parameters["showFindButton"] = ShowFindButton;
            parameters["showEditorButton"] = ShowEditorButton;
            parameters["showFirstButton"] = ShowFirstButton;
            parameters["showPrevButton"] = ShowPrevButton;
            parameters["showCurrentPage"] = ShowCurrentPage;
            parameters["showNextButton"] = ShowNextButton;
            parameters["showLastButton"] = ShowLastButton;
            parameters["showViewMode"] = ShowViewMode;
            parameters["showZoom"] = ShowZoom;
            parameters["showDesignButton"] = ShowDesignButton;
            parameters["showAboutButton"] = ShowAboutButton;

            //Exports
            parameters["showExportDialog"] = ShowExportDialog;
            parameters["showExportToBmp"] = ShowExportToBmp;
            parameters["showExportToCsv"] = ShowExportToCsv;
            parameters["showExportToDbf"] = ShowExportToDbf;
            parameters["showExportToDif"] = ShowExportToDif;
            parameters["showExportToDocument"] = ShowExportToDocument;
            parameters["showExportToExcel"] = ShowExportToExcel;
            parameters["showExportToExcel2007"] = ShowExportToExcel2007;
            parameters["showExportToExcelXml"] = ShowExportToExcelXml;
            parameters["showExportToGif"] = ShowExportToGif;
            parameters["showExportToHtml"] = ShowExportToHtml;
            parameters["showExportToHtml5"] = ShowExportToHtml5;
            parameters["showExportToJpeg"] = ShowExportToJpeg;
            parameters["showExportToMetafile"] = ShowExportToMetafile;
            parameters["showExportToMht"] = ShowExportToMht;
            parameters["showExportToOds"] = ShowExportToOds;
            parameters["showExportToOdt"] = ShowExportToOdt;
            parameters["showExportToPcx"] = ShowExportToPcx;
            parameters["showExportToPdf"] = ShowExportToPdf;
            parameters["showExportToPng"] = ShowExportToPng;
            parameters["showExportToPowerPoint"] = ShowExportToPowerPoint;
            parameters["showExportToRtf"] = ShowExportToRtf;
            parameters["showExportToSvg"] = ShowExportToSvg;
            parameters["showExportToSvgz"] = ShowExportToSvgz;
            parameters["showExportToSylk"] = ShowExportToSylk;
            parameters["showExportToText"] = ShowExportToText;
            parameters["showExportToTiff"] = ShowExportToTiff;
            parameters["showExportToWord2007"] = ShowExportToWord2007;
            parameters["showExportToXml"] = ShowExportToXml;
            parameters["showExportToXps"] = ShowExportToXps;

            return JSON.Encode(parameters);
        }

        internal static string GetHtmlColor(Color color)
        {
            return "#" + color.R.ToString("x2") + color.G.ToString("x2") + color.B.ToString("x2");
        }

        private void CheckAutoSwitchToMobileTheme()
        {
            if (AutoSwitchToMobileTheme && Theme.ToString().StartsWith("Office2013") && isMobile())
            {
                Theme = (StiMobileViewerTheme)Enum.Parse(typeof(StiMobileViewerTheme), Theme.ToString().Replace("Office2013", "Material"));
            }
            else if (Theme.ToString().StartsWith("Material") && !isMobile())
            {
                Theme = (StiMobileViewerTheme)Enum.Parse(typeof(StiMobileViewerTheme), Theme.ToString().Replace("Material", "Office2013"));
            }
        }

        private bool isMobile()
        {
            string u = Page.Request.ServerVariables["HTTP_USER_AGENT"];
            Regex b = new Regex(@"(ipad|iphone|android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if ((b.IsMatch(u) || v.IsMatch(u.Substring(0, 4)))) return true;
            else return false;
        }

        protected override void CreateChildControls()
        {
            this.Controls.Clear();
            CheckAutoSwitchToMobileTheme();
            if (!IsDesignMode)
            {
                if (!DesignerMode) jsParameters = GetJSParameters();
                ProcessReport();
            }

            if (FullScreen)
            {
                this.Style.Add("position", "absolute");
                this.Style.Add("top", "0px");
                this.Style.Add("left", "0px");
                this.Style.Add("right", "0px");
                this.Style.Add("bottom", "0px");
            }

            Panel mainPanel = new Panel();
            mainPanel.CssClass = "stiMobileViewerMainPanel";
            mainPanel.ID = "MainPanel";
            this.Controls.Add(mainPanel);

            base.CreateChildControls();
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            #region Design Mode
            if (IsDesignMode)
            {
                Panel panel = new Panel();
                panel.BackColor = this.BackColor.IsEmpty ? Color.WhiteSmoke : this.BackColor;
                panel.BorderColor = this.BorderColor.IsEmpty ? Color.DarkGray : this.BorderColor;
                panel.BorderWidth = this.BorderWidth.IsEmpty ? 2 : this.BorderWidth;
                panel.BorderStyle = this.BorderStyle == BorderStyle.NotSet ? BorderStyle.Solid : this.BorderStyle;
                panel.Width = this.Width;
                panel.Height = this.Height;
                panel.Style.Add("overflow", "hidden");

                #region ToolBar
                if (this.ShowToolBar)
                {
                    Table tableToolBar = new Table();
                    tableToolBar.Style.Add("width", "100%");
                    tableToolBar.Style.Add("height", "29px");
                    tableToolBar.CellPadding = 0;
                    tableToolBar.CellSpacing = 0;
                    TableRow rowToolBar = new TableRow();
                    TableCell cellToolBarLeft = new TableCell();
                    TableCell cellToolBarMiddle = new TableCell();
                    TableCell cellToolBarRight = new TableCell();
                    tableToolBar.Rows.Add(rowToolBar);
                    rowToolBar.Cells.Add(cellToolBarLeft);
                    rowToolBar.Cells.Add(cellToolBarMiddle);
                    rowToolBar.Cells.Add(cellToolBarRight);

                    Panel leftPanel = new Panel();
                    leftPanel.Style.Add("width", "559px");
                    leftPanel.Style.Add("height", "29px");
                    cellToolBarLeft.Controls.Add(leftPanel);

                    Panel rightPanel = new Panel();
                    rightPanel.Style.Add("width", "93px");
                    rightPanel.Style.Add("height", "29px");
                    cellToolBarRight.Controls.Add(rightPanel);

                    if (ToolBarBackColor != Color.Empty)
                    {
                        tableToolBar.BackColor = ToolBarBackColor;
                    }
                    else
                    {
                        leftPanel.Style.Add("background", "url(" + GetImageUrl("ToolBarLeftHalfMobileViewer.gif", false) + ")");
                        cellToolBarMiddle.Style.Add("background", "url(" + GetImageUrl("ToolBarMiddleHalfMobileViewer.gif", false) + ")");
                        rightPanel.Style.Add("background", "url(" + GetImageUrl("ToolBarRightHalfMobileViewer.gif", false) + ")");
                        cellToolBarMiddle.Style.Add("height", "29px");
                        cellToolBarMiddle.Style.Add("width", "100%");
                    }

                    panel.Controls.Add(tableToolBar);
                }
                #endregion

                #region Center
                Table tableCenter = new Table();
                tableCenter.Style.Add("height", "100%");
                tableCenter.Style.Add("width", "100%");
                TableRow rowCenter = new TableRow();
                TableCell cellCenter = new TableCell();
                tableCenter.Rows.Add(rowCenter);
                rowCenter.Cells.Add(cellCenter);

                cellCenter.Text = "Mobile Viewer";
                cellCenter.HorizontalAlign = HorizontalAlign.Center;
                cellCenter.VerticalAlign = VerticalAlign.Middle;

                panel.Controls.Add(tableCenter);
                #endregion

                panel.RenderControl(output);
            }
            #endregion

            #region Runtime Mode
            else
            {
                base.RenderContents(output);
            }
            #endregion
        }

        #region Page

        private string RenderReportPage(StiHtmlExportService service, StiReport report, int pageIndex, double zoom, bool notAnimate)
        {
            StiHtmlExportSettings settings = new StiHtmlExportSettings();
            settings.PageRange = new StiPagesRange(pageIndex);
            settings.Zoom = zoom;
            settings.ImageFormat = ImageFormat.Png;
            settings.ExportQuality = StiHtmlExportQuality.High;
            settings.ExportBookmarksMode = StiHtmlExportBookmarksMode.ReportOnly;
            settings.RemoveEmptySpaceAtBottom = false;
            settings.UseWatermarkMargins = true;

            switch (ReportDisplayMode)
            {
                case StiReportDisplayMode.Table: settings.ExportMode = StiHtmlExportMode.Table; break;
                case StiReportDisplayMode.Div: settings.ExportMode = StiHtmlExportMode.Div; break;
                case StiReportDisplayMode.Span: settings.ExportMode = StiHtmlExportMode.Span; break;
            }

            if (notAnimate)
            {
                settings.ChartType = StiHtmlChartType.Vector;
            }
            else
            {
                switch (TypeRenderChart)
                {
                    case StiTypeRenderChart.AnimatedVector: settings.ChartType = StiHtmlChartType.AnimatedVector; break;
                    case StiTypeRenderChart.Image: settings.ChartType = StiHtmlChartType.Image; break;
                    case StiTypeRenderChart.Vector: settings.ChartType = StiHtmlChartType.Vector; break;
                }
            }

            MemoryStream stream = new MemoryStream();
            service.ExportHtml(report, stream, settings);
            string htmlText = Encoding.UTF8.GetString(stream.ToArray()).Substring(1);
            stream.Close();

            return htmlText;
        }

        private void RenderPageParameters(StiReport report, int pageNumber, double zoom, ref string pageMargins, ref string pageSizes, ref string pageBackgrounds)
        {
            StiPage page = report.RenderedPages[pageNumber];

            pageMargins = string.Format("{0}px {1}px {2}px {3}px",
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Top) * zoom),
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Right) * zoom),
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Bottom) * zoom),
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Left) * zoom));

            pageSizes = string.Format("{0};{1}",
                    Math.Round(report.Unit.ConvertToHInches(page.PageWidth) * zoom),
                    Math.Round(report.Unit.ConvertToHInches(page.PageHeight) * zoom));

            pageBackgrounds = GetHtmlColor(StiBrush.ToColor(page.Brush));
        }

        private Hashtable GetPageAttributes(StiReport report, StiHtmlExportService service, int pageIndex, bool notAnimate, bool notExport = false)
        {
            string htmlText = string.Empty;
            string pageMargins = string.Empty;
            string pageSizes = string.Empty;
            string pageBackgrounds = string.Empty;

            Hashtable pageAttr = new Hashtable();
            RenderPageParameters(report, pageIndex, ZoomPercent / 100, ref pageMargins, ref pageSizes, ref pageBackgrounds);
            pageAttr["content"] = notExport ? "{}" : RenderReportPage(service, report, pageIndex, ZoomPercent / 100, notAnimate);
            pageAttr["margins"] = pageMargins;
            pageAttr["sizes"] = pageSizes;
            pageAttr["background"] = pageBackgrounds;

            return pageAttr;
        }

        private ArrayList GetPagesArray(StiReport report, bool notAnimate, bool notExport = false)
        {
            return GetPagesArray(report, -1, notAnimate, notExport);
        }

        private ArrayList GetPagesArray(StiReport report, int pageIndex, bool notAnimate, bool notExport = false)
        {
            StiOptions.Export.Html.UseComponentStyleName = false;
            StiHtmlExportService service = new StiHtmlExportService();
            service.RenderAsDocument = false;
            service.Styles = !CloudMode ? new ArrayList() : (CacheReportStyles != null ? CacheReportStyles : new ArrayList());
            service.ClearOnFinish = false;
            service.RenderStyles = false;

            string htmlText = string.Empty;
            string pageMargins = string.Empty;
            string pageSizes = string.Empty;
            string pageBackgrounds = string.Empty;

            ArrayList pages = new ArrayList();

            if (pageIndex != -1)
            {
                pages.Add(GetPageAttributes(report, service, pageIndex, notAnimate, notExport));
            }
            else if (ViewMode == StiWebViewMode.OnePage)
            {
                pages.Add(GetPageAttributes(report, service, CurrentPage, notAnimate, notExport));
            }
            else
            {
                for (int id = 0; id < report.RenderedPages.Count; id++)
                {
                    pages.Add(GetPageAttributes(report, service, id, notAnimate, notExport));
                }
            }

            if (CloudMode) CacheReportStyles = service.Styles;

            // Add Styles
            StringWriter writer = new StringWriter();
            StiHtmlTextWriter htmlWriter = new StiHtmlTextWriter(writer);
            service.HtmlWriter = htmlWriter;
            if (service.TableRender != null) service.TableRender.RenderStylesTable(true, false, false);
            htmlWriter.Flush();
            writer.Flush();
            string htmlTextStyles = writer.GetStringBuilder().ToString();
            writer.Close();

            pages.Add(htmlTextStyles);

            //Add Chart Scripts
            pages.Add(notAnimate || TypeRenderChart != StiTypeRenderChart.AnimatedVector ? String.Empty : service.GetChartScript());
            service.Clear();

            return pages;
        }

        #endregion

        #region Bookmarks

        private class StiBookmarkTreeNode
        {
            public int Parent;
            public string Title;
            public string Url;
            public string ComponentGuid;
            public bool Used;
        }

        private void AddBookmarkNode(StiBookmark bkm, int parentNode, ArrayList bookmarksTree, Hashtable bookmarksPageIndex)
        {
            StiBookmarkTreeNode tn = new StiBookmarkTreeNode();
            tn.Parent = parentNode;
            string st = bkm.Text.Replace("'", "");
            tn.Title = st;
            tn.Url = "#" + st;
            tn.Used = true;
            tn.ComponentGuid = bkm.ComponentGuid;
            if (bkm.ComponentGuid != null) bookmarksPageIndex.Add(bkm.ComponentGuid, -1);

            bookmarksTree.Add(tn);
            int currentNode = bookmarksTree.Count - 1;
            if (bkm.Bookmarks.Count != 0)
            {
                for (int tempCount = 0; tempCount < bkm.Bookmarks.Count; tempCount++)
                {
                    AddBookmarkNode(bkm.Bookmarks[tempCount], currentNode, bookmarksTree, bookmarksPageIndex);
                }
            }
        }

        private string RenderBookmarks(StiReport report, string mobileViewerId, int currentPageNumber)
        {
            Hashtable bookmarksPageIndex = new Hashtable();
            ArrayList bookmarksTree = new ArrayList();
            AddBookmarkNode(report.Bookmark, -1, bookmarksTree, bookmarksPageIndex);

            #region Prepare bookmarksPageIndex
            int tempPageNumber = 0;
            foreach (StiPage page in report.RenderedPages)
            {
                report.RenderedPages.GetPage(page);
                StiComponentsCollection components = page.GetComponents();
                foreach (StiComponent comp in components)
                {
                    if (comp.Enabled)
                    {
                        string bookmarkValue = comp.BookmarkValue as string;
                        string componentGuid = comp.Guid as string;
                        if (componentGuid != null && bookmarksPageIndex[componentGuid] != null) bookmarksPageIndex[componentGuid] = tempPageNumber;
                        if (bookmarkValue == null) bookmarkValue = string.Empty;
                        bookmarkValue = bookmarkValue.Replace("'", "");
                        if (bookmarkValue != string.Empty)
                        {
                            if (!bookmarksPageIndex.ContainsKey(bookmarkValue)) bookmarksPageIndex.Add(bookmarkValue, tempPageNumber);
                        }
                    }
                }

                tempPageNumber++;
            }
            #endregion

            string html = string.Empty;
            html += string.Format("bookmarks = new stiTree('bookmarks','{0}',{1}, imagesForBookmarks);", mobileViewerId, currentPageNumber);
            for (int index = 0; index < bookmarksTree.Count; index++)
            {
                StiBookmarkTreeNode node = (StiBookmarkTreeNode)bookmarksTree[index];
                int pageIndex = -1;
                if (node.ComponentGuid != null && bookmarksPageIndex.ContainsKey(node.ComponentGuid)) pageIndex = (int)bookmarksPageIndex[node.ComponentGuid];
                else if (bookmarksPageIndex.ContainsKey(node.Title)) pageIndex = (int)bookmarksPageIndex[node.Title];
                html += string.Format("bookmarks.add({0},{1},'{2}','{3}',{4},'{5}');", index, node.Parent, node.Title, node.Url, pageIndex, node.ComponentGuid);
            }

            return html;
        }

        #endregion

        protected override void Render(HtmlTextWriter output)
        {
            if (IsAjax)
            {
                if (IsDesignMode) base.Render(output);
                else
                {
                    base.Render(output);

                    if (DesignerMode)
                        output.Write(string.Format("<script language=\"javascript\" type=\"text/javascript\">var js{0}Parameters = {1};</script>", this.ClientID, GetJSParameters()));
                    else
                        output.Write(string.Format("<script language=\"javascript\" type=\"text/javascript\">var js{0} = new StiMobileViewer({1});</script>", this.ClientID, jsParameters));
                }
            }
            else
            {
                if (IsDesignMode) base.Render(output);
            }
        }
        #endregion

        #region Methods
        public static Hashtable GetRequestParams(Page page)
        {
            if (page.Request.Params["__CALLBACKPARAM"] != null)
            {
                string text = StiEncodingHelper.DecodeString(page.Request.Params["__CALLBACKPARAM"]);
                if (String.IsNullOrEmpty(text))
                    text = page.Request.Params["__CALLBACKPARAM"] as string;
                if (!String.IsNullOrEmpty(text))
                    return JSON.Decode(text) as Hashtable;
            }
            return null;
        }

        private StiReport GetReportForPreviewFromCache(string guid)
        {
            StiReport reportForPreview = null;

            if (CacheHelper != null)
            {
                reportForPreview = CacheHelper.GetObjectFromCache(guid) as StiReport;
            }
            else
            {
                if (CacheMode == StiCacheMode.Page)
                {
                    reportForPreview = Page.Cache[guid] as StiReport;
                    Page.Cache.Remove(guid);
                }
                else
                {
                    reportForPreview = Page.Session[guid] as StiReport;
                    Page.Session.Remove(guid);
                }
            }

            return reportForPreview;
        }

        private Hashtable GetViewerParametersFromCloud()
        {
            if (!CloudMode || HttpContext.Current.Request.Form.AllKeys == null || HttpContext.Current.Request.Form.AllKeys.Length == 0) return null;
            string[] keys = HttpContext.Current.Request.Form.AllKeys;
            Hashtable parameters = new Hashtable();
            for (int i = 0; i < keys.Length; i++)
            {
                parameters[keys[i]] = HttpContext.Current.Request.Form[keys[i]];
            }
            return parameters;
        }

        private StiExportSettings GetExportSettings(string exportFormat, Hashtable incomingExportSettings)
        {
            StiExportSettings resultExportSettings = null;

            if (exportFormat == "StiPdfExportSettings") resultExportSettings = GetPdfExportSettings(incomingExportSettings);
            else if (exportFormat == "StiXpsExportSettings") resultExportSettings = GetXpsExportSettings(incomingExportSettings);
            else if (exportFormat == "StiPpt2007ExportSettings") resultExportSettings = GetPowerPointExportSettings(incomingExportSettings);
            else if (exportFormat == "StiHtmlExportSettings") resultExportSettings = GetHtmlExportSettings(incomingExportSettings);
            else if (exportFormat == "StiHtml5ExportSettings") resultExportSettings = GetHtml5ExportSettings(incomingExportSettings);
            else if (exportFormat == "StiMhtExportSettings") resultExportSettings = GetMhtExportSettings(incomingExportSettings);
            else if (exportFormat == "StiTxtExportSettings") resultExportSettings = GetTextExportSettings(incomingExportSettings);
            else if (exportFormat == "StiRtfExportSettings") resultExportSettings = GetRtfExportSettings(incomingExportSettings);
            else if (exportFormat == "StiWord2007ExportSettings") resultExportSettings = GetWord2007ExportSettings(incomingExportSettings);
            else if (exportFormat == "StiOdtExportSettings") resultExportSettings = GetOdtExportSettings(incomingExportSettings);
            else if (exportFormat == "StiExcelExportSettings") resultExportSettings = GetXlsExportSettings(incomingExportSettings);
            else if (exportFormat == "StiExcelXmlExportSettings") resultExportSettings = GetXlsXmlExportSettings(incomingExportSettings);
            else if (exportFormat == "StiExcel2007ExportSettings") resultExportSettings = GetExcel2007ExportSettings(incomingExportSettings);
            else if (exportFormat == "StiOdsExportSettings") resultExportSettings = GetOdsExportSettings(incomingExportSettings);
            else if (exportFormat == "StiCsvExportSettings") resultExportSettings = GetCsvExportSettings(incomingExportSettings);
            else if (exportFormat == "StiDbfExportSettings") resultExportSettings = GetDbfExportSettings(incomingExportSettings);
            else if (exportFormat == "StiXmlExportSettings") resultExportSettings = GetXmlExportSettings(incomingExportSettings);
            else if (exportFormat == "StiDifExportSettings") resultExportSettings = GetDifExportSettings(incomingExportSettings);
            else if (exportFormat == "StiSylkExportSettings") resultExportSettings = GetSylkExportSettings(incomingExportSettings);
            else if (exportFormat == "StiBmpExportSettings") resultExportSettings = GetBmpExportSettings(incomingExportSettings);
            else if (exportFormat == "StiGifExportSettings") resultExportSettings = GetGifExportSettings(incomingExportSettings);
            else if (exportFormat == "StiJpegExportSettings") resultExportSettings = GetJpegExportSettings(incomingExportSettings);
            else if (exportFormat == "StiPcxExportSettings") resultExportSettings = GetPcxExportSettings(incomingExportSettings);
            else if (exportFormat == "StiPngExportSettings") resultExportSettings = GetPngExportSettings(incomingExportSettings);
            else if (exportFormat == "StiTiffExportSettings") resultExportSettings = GetTiffExportSettings(incomingExportSettings);
            else if (exportFormat == "StiEmfExportSettings") resultExportSettings = GetMetafileExportSettings(incomingExportSettings);
            else if (exportFormat == "StiSvgExportSettings") resultExportSettings = GetSvgExportSettings(incomingExportSettings);
            else if (exportFormat == "StiSvgzExportSettings") resultExportSettings = GetSvgzExportSettings(incomingExportSettings);

            return resultExportSettings;
        }

#if SERVER
        private StiFileType GetFileTypeByExportSettings(StiExportSettings exportSettings)
        {
            if (exportSettings is StiPdfExportSettings) return StiFileType.Pdf;
            if (exportSettings is StiXpsExportSettings) return StiFileType.Xps;
            if (exportSettings is StiPpt2007ExportSettings) return StiFileType.PowerPoint;
            if (exportSettings is StiHtmlExportSettings || exportSettings is StiHtml5ExportSettings || exportSettings is StiMhtExportSettings) return StiFileType.Html;
            if (exportSettings is StiTxtExportSettings) return StiFileType.Text;
            if (exportSettings is StiRtfExportSettings) return StiFileType.RichText;
            if (exportSettings is StiWord2007ExportSettings) return StiFileType.Word;
            if (exportSettings is StiOdtExportSettings) return StiFileType.OpenDocumentWriter;
            if (exportSettings is StiExcelExportSettings || exportSettings is StiExcelXmlExportSettings || exportSettings is StiExcel2007ExportSettings) return StiFileType.Excel;
            if (exportSettings is StiOdsExportSettings) return StiFileType.OpenDocumentCalc;
            if (exportSettings is StiCsvExportSettings) return StiFileType.Csv;
            if (exportSettings is StiDbfExportSettings) return StiFileType.Dbf;
            if (exportSettings is StiXmlExportSettings) return StiFileType.Xml;
            if (exportSettings is StiDifExportSettings) return StiFileType.Dif;
            if (exportSettings is StiSylkExportSettings) return StiFileType.Sylk;
            if (exportSettings is StiBmpExportSettings || exportSettings is StiGifExportSettings || exportSettings is StiJpegExportSettings ||
                exportSettings is StiPcxExportSettings || exportSettings is StiPngExportSettings || exportSettings is StiTiffExportSettings ||
                exportSettings is StiEmfExportSettings || exportSettings is StiSvgExportSettings || exportSettings is StiSvgzExportSettings)
                return StiFileType.Image;

            return StiFileType.Unknown;
        }
#endif

        private double StrToDouble(string value)
        {
            System.Globalization.CultureInfo currentCulture = CultureInfo.CurrentCulture;
            string numSep = currentCulture.NumberFormat.NumberDecimalSeparator;
            value = value.Replace(",", ".").Replace(".", numSep);

            double result = 0;
            double.TryParse(value, out result);

            return result;
        }

        private string DoubleToStr(double value)
        {
            System.Globalization.CultureInfo currentCulture = CultureInfo.CurrentCulture;
            string numSep = currentCulture.NumberFormat.NumberDecimalSeparator;
            string str = value.ToString();
            str = str.Replace(numSep, ".").Replace(",", ".");

            return str;
        }

        private int StrToInt(string value)
        {
            int result = 0;
            int.TryParse(value, out result);

            return result;
        }

        private void SetParent(StiContainer parent, StiPage page)
        {
            foreach (StiComponent comp in parent.Components)
            {
                if (comp == null) continue;

                comp.Parent = parent;
                comp.Page = page;

                var cont = comp as StiContainer;
                if (cont != null) SetParent(cont, page);
            }
        }

        private bool CheckEditableReport(StiReport report)
        {
            StiComponentsCollection components = report.GetComponents();
            foreach (StiComponent component in components)
            {
                if (component is StiText && ((StiText)component).Editable) return true;
                else if (component is StiCheckBox && ((StiCheckBox)component).Editable) return true;
                else if (component is StiRichText && ((StiRichText)component).Editable) return true;
            }

            return false;
        }

        private void ApplyEditableFieldsToReport(StiReport report, object parameters)
        {
            if (parameters == null) return;
            try
            {
                Hashtable allPagesParams = (Hashtable)parameters;
                foreach (DictionaryEntry pageParams in allPagesParams)
                {
                    int pageIndex = Convert.ToInt32(pageParams.Key);
                    Hashtable allComponetsParams = (Hashtable)pageParams.Value;

                    foreach (DictionaryEntry compParamsObject in allComponetsParams)
                    {
                        int compIndex = Convert.ToInt32(compParamsObject.Key);
                        Hashtable compParams = (Hashtable)compParamsObject.Value;

                        if (pageIndex < report.RenderedPages.Count)
                        {
                            StiPage page = report.RenderedPages[pageIndex];
                            if (compIndex < page.Components.Count)
                            {
                                StiComponent component = page.Components[compIndex];
                                if ((string)compParams["type"] == "CheckBox" && component is StiCheckBox)
                                {
                                    ((StiCheckBox)component).CheckedValue = (bool)compParams["checked"] ? "true" : "false";
                                }
                                else if ((string)compParams["type"] == "Text" && component is StiText)
                                {
                                    ((StiText)component).Text.Value = (string)compParams["text"];
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
        }

        private static object lockReport = new object();

#if SERVER
        private StiReport AddPageToReportRenderedPagesCollection(StiReport report, StiPage page, int pageIndex, byte[] pageContent)
        {
            {
                //Create new report if no exist current report            
                if (report == null)
                {
                    report = new StiReport
                    {
                        ReportUnit = StiReportUnitType.HundredthsOfInch,
                        NeedsCompiling = false,
                        IsRendered = true
                    };

                    report.RenderedPages.Clear();
                }

                //Create new page if no exist current page             
                if (page == null)
                {
                    page = new StiPage();
                }

                //Load page content if exist
                if (pageContent != null)
                {
                    StiReportPageHelper.Load(page, pageContent);
                }

                //Add empty pages
                if (pageIndex >= report.RenderedPages.Count)
                {
                    for (int i = report.RenderedPages.Count; i <= pageIndex; i++)
                    {
                        StiPage newPage = new StiPage();
                        report.RenderedPages.Add(newPage);
                    }
                }

                //Add current page
                report.RenderedPages[pageIndex] = page;
                page.Report = report;
                SetParent(page, page);
                page.ClearPage();
            }

            return report;
        }

        private StiReport CreateReportSnapshotFromMetric(List<StiReportPageMetric> pageMetrics)
        {
            var report = new StiReport
            {
                ReportUnit = StiReportUnitType.HundredthsOfInch
            };

            #region Create Empty Pages based on Report Metrics
            report.RenderedPages.Clear();

            if (pageMetrics != null)
            {
                foreach (var metric in pageMetrics)
                {
                    var count = metric.PageCount;

                    while (count > 0)
                    {
                        var page = new StiPage(report)
                        {
                            Margins = new StiMargins(metric.MarginLeft, metric.MarginRight, metric.MarginTop, metric.MarginBottom),
                            PageWidth = metric.PageWidth,
                            PageHeight = metric.PageHeight,
                            SegmentPerWidth = metric.SegmentPerWidth,
                            SegmentPerHeight = metric.SegmentPerHeight,
                            Guid = metric.Guid,
                            BookmarkValue = metric.Bookmark
                        };

                        report.RenderedPages.Add(page);
                        count--;
                    }
                }
            }
            #endregion

            report.IsRendered = true;
            report.NeedsCompiling = false;

            return report;
        }
#endif

        private void SetDefaultValuesExportSettings(Hashtable DefaultSettings)
        {
            #region Pdf
            StiPdfExportSettings PdfSettings = (StiPdfExportSettings)DefaultSettings["StiPdfExportSettings"];
            PdfSettings.ImageCompressionMethod = PdfImageCompressionMethod;
            PdfSettings.ImageQuality = ImageQuality;
            PdfSettings.ImageResolution = ImageResolution;
            PdfSettings.EmbeddedFonts = PdfEmbeddedFonts;
            PdfSettings.StandardPdfFonts = PdfStandardFonts;
            PdfSettings.UseUnicode = PdfUseUnicode;
            #endregion

            #region Xps
            StiXpsExportSettings XpsSettings = (StiXpsExportSettings)DefaultSettings["StiXpsExportSettings"];
            XpsSettings.ImageQuality = ImageQuality;
            XpsSettings.ImageResolution = ImageResolution;
            #endregion

            #region Ppt2007
            StiPpt2007ExportSettings Ppt2007Settings = (StiPpt2007ExportSettings)DefaultSettings["StiPpt2007ExportSettings"];
            Ppt2007Settings.ImageQuality = ImageQuality;
            Ppt2007Settings.ImageResolution = ImageResolution;
            #endregion

            #region Html
            StiHtmlExportSettings HtmlSettings = (StiHtmlExportSettings)DefaultSettings["StiHtmlExportSettings"];
            HtmlSettings.ImageFormat = ImageFormat;
            #endregion

            #region Html5
            StiHtmlExportSettings Html5Settings = (StiHtml5ExportSettings)DefaultSettings["StiHtml5ExportSettings"];
            HtmlSettings.ImageFormat = ImageFormat;
            #endregion

            #region Mht
            StiMhtExportSettings MhtSettings = (StiMhtExportSettings)DefaultSettings["StiMhtExportSettings"];
            MhtSettings.ImageFormat = ImageFormat;
            #endregion

            #region Rtf
            StiRtfExportSettings RtfSettings = (StiRtfExportSettings)DefaultSettings["StiRtfExportSettings"];
            RtfSettings.ImageQuality = ImageQuality;
            RtfSettings.ImageResolution = ImageResolution;
            RtfSettings.ExportMode = RtfExportMode;
            #endregion

            #region Word2007
            StiWord2007ExportSettings Word2007Settings = (StiWord2007ExportSettings)DefaultSettings["StiWord2007ExportSettings"];
            Word2007Settings.ImageQuality = ImageQuality;
            Word2007Settings.ImageResolution = ImageResolution;
            #endregion

            #region Odt
            StiOdtExportSettings OdtSettings = (StiOdtExportSettings)DefaultSettings["StiOdtExportSettings"];
            OdtSettings.ImageQuality = ImageQuality;
            OdtSettings.ImageResolution = ImageResolution;
            #endregion

            #region Excel
            StiExcelExportSettings ExcelSettings = (StiExcelExportSettings)DefaultSettings["StiExcelExportSettings"];
            ExcelSettings.ImageQuality = ImageQuality;
            ExcelSettings.ImageResolution = ImageResolution;
            ExcelSettings.UseOnePageHeaderAndFooter = ExcelUseOnePageHeaderAndFooter;
            ExcelSettings.ExportPageBreaks = ExcelPageBreaks;
            ExcelSettings.ExportDataOnly = ExportDataOnly;
            #endregion

            #region Excel2007
            StiExcel2007ExportSettings Excel2007Settings = (StiExcel2007ExportSettings)DefaultSettings["StiExcel2007ExportSettings"];
            Excel2007Settings.ImageQuality = ImageQuality;
            Excel2007Settings.ImageResolution = ImageResolution;
            Excel2007Settings.UseOnePageHeaderAndFooter = ExcelUseOnePageHeaderAndFooter;
            Excel2007Settings.ExportPageBreaks = ExcelPageBreaks;
            Excel2007Settings.ExportDataOnly = ExportDataOnly;
            #endregion

            #region Ods
            StiOdsExportSettings OdsSettings = (StiOdsExportSettings)DefaultSettings["StiOdsExportSettings"];
            OdsSettings.ImageQuality = ImageQuality;
            OdsSettings.ImageResolution = ImageResolution;
            #endregion

            #region Csv
            StiCsvExportSettings CsvSettings = (StiCsvExportSettings)DefaultSettings["StiCsvExportSettings"];
            CsvSettings.Separator = CsvSeparator.Length == 0 ? ";" : CsvSeparator;
            CsvSettings.Encoding = CsvEncoding;
            #endregion

            #region Dbf
            StiDbfExportSettings DbfSettings = (StiDbfExportSettings)DefaultSettings["StiDbfExportSettings"];
            DbfSettings.CodePage = DbfCodePages;
            #endregion
        }

        private Hashtable GetDefaultExportSettings()
        {
            Hashtable DefaultSettings = new Hashtable();
            DefaultSettings["StiPdfExportSettings"] = new StiPdfExportSettings();
            DefaultSettings["StiPpt2007ExportSettings"] = new StiPpt2007ExportSettings();
            DefaultSettings["StiXpsExportSettings"] = new StiXpsExportSettings();

            StiHtmlExportSettings htmlSettings = new StiHtmlExportSettings();
            htmlSettings.UseEmbeddedImages = true;
            DefaultSettings["StiHtmlExportSettings"] = htmlSettings;

            StiHtml5ExportSettings html5Settings = new StiHtml5ExportSettings();
            html5Settings.UseEmbeddedImages = true;
            DefaultSettings["StiHtml5ExportSettings"] = html5Settings;

            DefaultSettings["StiTxtExportSettings"] = new StiTxtExportSettings();
            DefaultSettings["StiRtfExportSettings"] = new StiRtfExportSettings();
            DefaultSettings["StiWord2007ExportSettings"] = new StiWord2007ExportSettings();
            DefaultSettings["StiMhtExportSettings"] = new StiMhtExportSettings();
            DefaultSettings["StiOdtExportSettings"] = new StiOdtExportSettings();
            DefaultSettings["StiExcelExportSettings"] = new StiExcelExportSettings();
            DefaultSettings["StiExcelXmlExportSettings"] = new StiExcelXmlExportSettings();
            DefaultSettings["StiExcel2007ExportSettings"] = new StiExcel2007ExportSettings();
            DefaultSettings["StiOdsExportSettings"] = new StiOdsExportSettings();
            DefaultSettings["StiBmpExportSettings"] = new StiBmpExportSettings();
            DefaultSettings["StiGifExportSettings"] = new StiGifExportSettings();
            DefaultSettings["StiJpegExportSettings"] = new StiJpegExportSettings();
            DefaultSettings["StiPcxExportSettings"] = new StiPcxExportSettings();
            DefaultSettings["StiPngExportSettings"] = new StiPngExportSettings();
            DefaultSettings["StiTiffExportSettings"] = new StiTiffExportSettings();
            DefaultSettings["StiSvgExportSettings"] = new StiSvgExportSettings();
            DefaultSettings["StiSvgzExportSettings"] = new StiSvgzExportSettings();
            DefaultSettings["StiEmfExportSettings"] = new StiEmfExportSettings();
            DefaultSettings["StiCsvExportSettings"] = new StiCsvExportSettings();
            DefaultSettings["StiDbfExportSettings"] = new StiDbfExportSettings();
            DefaultSettings["StiDifExportSettings"] = new StiDifExportSettings();
            DefaultSettings["StiSylkExportSettings"] = new StiSylkExportSettings();
            DefaultSettings["StiXmlExportSettings"] = new StiXmlExportSettings();
            DefaultSettings["StiDifExportSettings"] = new StiDifExportSettings();

            SetDefaultValuesExportSettings(DefaultSettings);

            Hashtable defaultSettings = new Hashtable();
            foreach (DictionaryEntry exportSettings in DefaultSettings)
            {
                StiExportSettings settings = (StiExportSettings)exportSettings.Value;
                Hashtable values = new Hashtable();
                Type type = settings.GetType();
                PropertyInfo[] properties = type.GetProperties();

                foreach (PropertyInfo property in properties)
                {
                    if (property.Name != "CompanyString" && property.Name != "LastModifiedString")
                    {
                        object value = property.GetValue(settings, null);
                        if (value is StiPagesRange) value =
                            ((StiPagesRange)value).RangeType == StiRangeType.CurrentPage
                                ? ((StiPagesRange)value).CurrentPage.ToString()
                                : ((StiPagesRange)value).RangeType == StiRangeType.Pages
                                    ? ((StiPagesRange)value).PageRanges
                                    : ((StiPagesRange)value).RangeType.ToString();
                        if (value is Encoding) value = ((Encoding)value).CodePage.ToString();
                        if (value is ImageFormat) value = ((ImageFormat)value).ToString();
                        if (value is int || value is float) value = value.ToString();
                        if (value is List<StiPdfEmbeddedFileData>)
                        {
                            ArrayList fileNames = new ArrayList();
                            foreach (StiPdfEmbeddedFileData fileData in (List<StiPdfEmbeddedFileData>)value)
                            {
                                fileNames.Add(fileData.Name);
                            }
                            value = fileNames;
                        }

                        values[property.Name] = value;
                    }
                }

                defaultSettings[type.Name] = values;
            }

            Hashtable saveDocumentProps = new Hashtable();
            defaultSettings["SaveDocument"] = saveDocumentProps;

            return defaultSettings;
        }

        private string GetProductVersion(string reportGuid)
        {
            Assembly assembly = System.Reflection.Assembly.GetAssembly(typeof(StiReport));
            Version version = assembly.GetName().Version;
            string versionString = string.Format("{0}.{1}{2} from {3:D}",
                version.Major.ToString(), version.Minor.ToString(), version.Build == 0 ? "" : "." + version.Build.ToString(), StiVersion.CreationDate);

            #region Trial
#if CLOUD
            var isTrial = StiCloudPlan.IsTrialPlan(reportGuid);
#else
            var key = StiLicenseKeyValidator.GetLicenseKey();
            var isTrial = !StiLicenseKeyValidator.IsValid(StiProductIdent.Web, key);
            if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key)) isTrial = true;

            #region IsValidLicenseKey
            if (!isTrial)
            {
                try
                {
                    using (var rsa = new RSACryptoServiceProvider(512))
                    using (var sha = new SHA1CryptoServiceProvider())
                    {
                        rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                        isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
                    }
                }
                catch (Exception)
                {
                    isTrial = true;
                }
            }
            #endregion
#endif
            if (!isTrial) versionString += " ";
            #endregion

            return versionString;
        }

        private string GetCurrentHelpLanguage()
        {
            string language;
            switch (StiLocalization.CultureName)
            {
                case "ru":
                    language = "ru";
                    break;

                //case "de":
                //    language = "de";
                //    break;

                default:
                    language = "en";
                    break;
            }

            return language;
        }

        private object GetRequest(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null) return HttpContext.Current.Request.Params[key];
            if (!IsDesignMode) return this.Page.Request.Params[key];

            return null;
        }

        public void SetDefaultSaveType(StiExportFormat format)
        {
            defaultSaveType = format;
        }

        private void AddPrintHtmlToPageCache(string guid)
        {
            if (Report != null)
            {
                if (CacheHelper != null)
                {
                    CacheHelper.SaveObjectToCache(GetPrintHtml(), guid);
                }
                else
                {
                    if (CacheMode == StiCacheMode.Page)
                    {
                        this.Page.Cache.Add(guid, GetPrintHtml(), null, Cache.NoAbsoluteExpiration,
                            ServerTimeOut, System.Web.Caching.CacheItemPriority.Low, null);
                    }
                    else
                    {
                        this.Page.Session.Add(guid, GetPrintHtml());
                    }
                }
            }
        }

        public void ResetReport()
        {
            CacheGuid = null;
            ServerReportName = null;
            prevCacheGuid = null;
            prevServerReportName = null;
            this.ViewState[this.ID + "_RequestFromUserDialog"] = null;
            report = null;
        }

        public void ResetCurrentPage()
        {
            CurrentPage = 0;
        }

        private Hashtable GetInteractionComponents()
        {
            Hashtable componentsList = null;
            if (!string.IsNullOrEmpty(InteractionComponents))
            {
                componentsList = new Hashtable();

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(InteractionComponents);

                foreach (XmlElement element in xml.FirstChild.ChildNodes)
                {
                    componentsList.Add(
                        XmlConvert.DecodeName(element.Attributes["name"].Value),
                        (StiInteractionSortDirection)Enum.Parse(typeof(StiInteractionSortDirection), element.Attributes["sort"].Value)
                    );
                }
            }

            return componentsList;
        }

        private void ApplyInteractionSorting(StiReport report, string componentName, bool ctrlKeyPressed)
        {
            if (report != null)
            {
                StiComponent comp = report.GetComponentByName(componentName);
                if (comp != null)
                {
                    string dataBandName = comp.Interaction.GetSortDataBandName();
                    string[] dataBandColumns = comp.Interaction.GetSortColumns();
                    string dataBandColumnString = comp.Interaction.GetSortColumnsString();

                    StiDataBand dataBand = report.GetComponentByName(dataBandName) as StiDataBand;
                    if (dataBand != null)
                    {
                        if (dataBand.Sort == null || dataBand.Sort.Length == 0)
                        {
                            dataBand.Sort = StiSortHelper.AddColumnToSorting(dataBand.Sort, dataBandColumnString, true);
                        }
                        else
                        {
                            int sortIndex = StiSortHelper.GetColumnIndexInSorting(dataBand.Sort, dataBandColumnString);
                            if (ctrlKeyPressed)
                            {
                                if (sortIndex == -1) dataBand.Sort = StiSortHelper.AddColumnToSorting(dataBand.Sort, dataBandColumnString, true);
                                else dataBand.Sort = StiSortHelper.ChangeColumnSortDirection(dataBand.Sort, dataBandColumnString);
                            }
                            else
                            {
                                if (sortIndex != -1)
                                {
                                    StiInteractionSortDirection direction = StiSortHelper.GetColumnSortDirection(dataBand.Sort, dataBandColumnString);

                                    if (direction == StiInteractionSortDirection.Ascending) direction = StiInteractionSortDirection.Descending;
                                    else direction = StiInteractionSortDirection.Ascending;

                                    dataBand.Sort = StiSortHelper.AddColumnToSorting(new string[0], dataBandColumnString, direction == StiInteractionSortDirection.Ascending);
                                }
                                else
                                {
                                    dataBand.Sort = StiSortHelper.AddColumnToSorting(new string[0], dataBandColumnString, true);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void ProcessReport()
        {
            StiReport reportFromCache = null;
            if (UseCache && ServerReportName.Length > 0 && ServerReportName != null)
            {
                #region Read IsWebReport status from cache
                bool isWebReport = false;
                if (CacheHelper != null)
                {
                    isWebReport = CacheHelper.GetObjectFromCache(this.ServerReportName + "IsWebReport") != null;
                }
                else
                {
                    if (CacheMode == StiCacheMode.Page) isWebReport = this.Page.Cache[this.ServerReportName + "IsWebReport"] != null;
                    else isWebReport = this.Page.Session[this.ServerReportName + "IsWebReport"] != null;
                }
                #endregion

                #region If WebReport then read report from cache as StiReport object
                if (isWebReport)
                {
                    if (CacheHelper != null)
                    {
                        this.report = CacheHelper.GetObjectFromCache(this.ServerReportName) as StiReport;
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Page) this.report = this.Page.Cache[this.ServerReportName] as StiReport;
                        else this.report = this.Page.Session[this.ServerReportName] as StiReport;
                    }
                }
                #endregion

                #region Is report stored in cache as string
                else
                {
                    this.report = new StiReport();
                    string reportStr = null;
                    string reportCacheGuids = null;

                    if (CacheHelper != null)
                    {
                        reportStr = CacheHelper.GetObjectFromCache(this.ServerReportName) as string;
                        reportCacheGuids = CacheHelper.GetObjectFromCache(this.ServerReportName + "CacheGuids") as string;
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Page)
                        {
                            reportStr = this.Page.Cache[this.ServerReportName] as string;
                            reportCacheGuids = this.Page.Cache[this.ServerReportName + "CacheGuids"] as string;
                        }
                        else
                        {
                            reportStr = this.Page.Session[this.ServerReportName] as string;
                            reportCacheGuids = this.Page.Session[this.ServerReportName + "CacheGuids"] as string;
                        }
                    }
                    try
                    {
                        if (reportStr != null && reportStr.Length > 0)
                        {
                            report.LoadPackedDocumentFromString(reportStr);
                            if (reportCacheGuids != null)
                            {
                                string[] cacheGuids = reportCacheGuids.Split(';');
                                report.RenderedPages.CacheMode = true;
                                report.ReportCachePath = cacheGuids[cacheGuids.Length - 1];
                                for (int id = 0; id < cacheGuids.Length; id++) report.RenderedPages[id].CacheGuid = cacheGuids[id];
                            }
                        }
                    }
                    catch
                    {
                    }
                    reportFromCache = this.report;
                }
                #endregion

            }

            if (((this.ReportForRendering == null) && (this.Report != null)) ||
                (this.Report != null && (!this.Report.NeedsCompiling) && (!this.Report.IsRendered)) ||
                (reportFromCache == null && this.Report != null))
            {
                if ((!report.IsRendered) && (!report.IsDocument) && (!IsWebFormReport)) RenderReport(report);

                if (UseCache)
                {
                    if (prevServerReportName != null)
                    {
                        ServerReportName = prevServerReportName;
                        prevServerReportName = null;
                    }
                    else
                    {
                        Guid reportGuid = Guid.NewGuid();
                        ServerReportName = reportGuid.ToString();
                    }

                    object reportItem = null;
                    object isWebReport = null;

                    if (IsWebFormReport)
                    {
                        reportItem = this.Report;
                        isWebReport = "true";
                    }
                    else
                    {
                        reportItem = Report.SavePackedDocumentToString();
                    }

                    if (CacheMode == StiCacheMode.Page)
                    {
                        // ���� �������� ����������� ������������ ������, �� ��������� Guid ���� ������� � ���� � ���� ��� ����������� �� ��������������
                        if (report.RenderedPages.CacheMode && report.RenderedPages.Count > 50)
                        {
                            string cacheGuids = string.Empty;
                            foreach (StiPage page in report.RenderedPages) cacheGuids += page.CacheGuid + ";";
                            cacheGuids += this.Report.ReportCachePath;

                            if (CacheHelper != null)
                            {
                                CacheHelper.SaveObjectToCache(cacheGuids, ServerReportName + "CacheGuids");
                            }
                            else
                            {
                                this.Page.Cache.Remove(ServerReportName + "CacheGuids");
                                this.Page.Cache.Add(ServerReportName + "CacheGuids", cacheGuids,
                                    null, Cache.NoAbsoluteExpiration, ServerTimeOut, System.Web.Caching.CacheItemPriority.Low, null);
                            }
                        }

                        if (CacheHelper != null)
                        {
                            CacheHelper.SaveObjectToCache(reportItem, ServerReportName);
                        }
                        else
                        {
                            this.Page.Cache.Remove(ServerReportName);
                            this.Page.Cache.Add(ServerReportName, reportItem,
                                null, Cache.NoAbsoluteExpiration, ServerTimeOut, System.Web.Caching.CacheItemPriority.Low, null);
                        }

                        if (isWebReport != null)
                        {
                            if (CacheHelper != null)
                            {
                                CacheHelper.SaveObjectToCache(isWebReport, ServerReportName + "IsWebReport");
                            }
                            else
                            {
                                this.Page.Cache.Remove(ServerReportName + "IsWebReport");
                                this.Page.Cache.Add(ServerReportName + "IsWebReport", isWebReport,
                                    null, Cache.NoAbsoluteExpiration, ServerTimeOut, System.Web.Caching.CacheItemPriority.Low, null);
                            }
                        }
                    }
                    else
                    {
                        if (CacheHelper != null)
                        {
                            if (this.Report.ReportCacheMode != StiReportCacheMode.Off) CacheHelper.SaveObjectToCache(this.Report.ReportCachePath, ServerReportName + "CachePath");
                            CacheHelper.SaveObjectToCache(reportItem, ServerReportName);
                            if (isWebReport != null) CacheHelper.SaveObjectToCache(isWebReport, ServerReportName + "IsWebReport");
                        }
                        else
                        {
                            if (this.Report.ReportCacheMode != StiReportCacheMode.Off) this.Page.Session.Add(ServerReportName + "CachePath", this.Report.ReportCachePath);
                            this.Page.Session.Add(ServerReportName, reportItem);
                            if (isWebReport != null) this.Page.Session.Add(ServerReportName + "IsWebReport", isWebReport);
                        }
                    }
                }
            }
        }

        private void RenderReport(StiReport report)
        {
            #region Correct View Mode
            StiWebViewMode currentViewMode = this.ViewMode;
            if (NewViewModeList != currentViewMode)
            {
                currentViewMode = NewViewModeList;
            }
            #endregion

            if (currentViewMode == StiWebViewMode.WholeReport ||
                RenderMode != StiRenderMode.RenderOnlyCurrentPage ||
                UseCache ||
                IsPrinting ||
                IsSaving)
            {
                InvokeGetReportData(report);
                report.Render(false);
            }
            else
            {
                if (currentPageFromViewer == -1) currentPageFromViewer = CurrentPage;
                else currentPageFromViewer--;

                if (IsLastButtonClicking)
                {
                    InvokeGetReportData(report);
                    report.Render(false);
                    return;
                }
                int fromPage = currentPageFromViewer;
                int toPage = currentPageFromViewer + 1;

                if (IsFirstButtonClicking)
                {
                    fromPage = 0;
                    toPage = 1;
                }
                if (IsPrevButtonClicking)
                {
                    fromPage--;
                    toPage--;
                }

                InvokeGetReportData(report);
                StiRenderState state = new StiRenderState(fromPage, toPage, false, false, true);
                report.Render(state);
            }
        }

        private StiPagesRange GetPrintPagesRange()
        {
            if (PrintMode == StiPrintMode.WholeReport || ViewMode == StiWebViewMode.WholeReport)
            {
                return StiPagesRange.All;
            }
            else
            {
                return new StiPagesRange(CurrentPage);
            }
        }

        private string GetPrintHtml()
        {
            string reportStr = string.Empty;

            using (MemoryStream ms = new MemoryStream())
            using (StreamReader sr = new StreamReader(ms))
            {
                StiHtmlExportService export = new StiHtmlExportService();
                StiHtmlExportSettings settings = new StiHtmlExportSettings();
                settings.AddPageBreaks = true;
                settings.PageRange = GetPrintPagesRange();
                settings.ImageFormat = ImageFormat;
                settings.UseWatermarkMargins = true;
                if (!AllowPrintBookmarks) settings.ExportBookmarksMode = StiHtmlExportBookmarksMode.ReportOnly;
                else settings.ExportBookmarksMode = StiHtmlExportBookmarksMode.All;

                switch (ReportDisplayMode)
                {
                    case StiReportDisplayMode.Table: settings.ExportMode = StiHtmlExportMode.Table; break;
                    case StiReportDisplayMode.Div: settings.ExportMode = StiHtmlExportMode.Div; break;
                    case StiReportDisplayMode.Span: settings.ExportMode = StiHtmlExportMode.Span; break;
                }

                export.HtmlImageHost = WebImageHost;
                export.ExportHtml(report, ms, settings);

                ms.Flush();
                ms.Seek(0, SeekOrigin.Begin);
                reportStr = "<!DOCTYPE html PUBLIC \" -//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<meta HTTP-EQUIV='Content-Type' CONTENT='text/html; charset=utf-8'>\n" + sr.ReadToEnd();

                sr.Close();
                ms.Close();
            }

            return reportStr;
        }

        private static byte[] ConvertStringToByteArray(string str)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            using (StreamWriter writer = new StreamWriter(memoryStream))
            {
                writer.Write(str);
                writer.Flush();
                memoryStream.Flush();
                memoryStream.Close();
                return memoryStream.ToArray();
            }
        }

        private bool ProcessPrintHtml()
        {
            string guid = GetRequest("sr_print") as string;
            if (guid == null) return false;

            string reportStr = null;
            if (CacheHelper != null)
            {
                reportStr = CacheHelper.GetObjectFromCache(guid) as string;
            }
            else
            {
                if (CacheMode == StiCacheMode.Page) reportStr = this.Page.Cache[guid] as string;
                else reportStr = this.Page.Session[guid] as string;
            }
            if (reportStr == null)
            {
                //If reportStr is empty then assign null table
                reportStr = "<table cellpadding=\"0\" cellspacing=\"0\" id=\"webReportTable_" + this.ClientID + "\"></table>";
            }
            else
            {
                if (CacheMode == StiCacheMode.Page) this.Page.Cache.Remove(guid);
                else this.Page.Session.Remove(guid);
            }

            Page.Response.Buffer = true;
            Page.Response.ClearContent();
            Page.Response.ClearHeaders();
            Page.Response.ContentEncoding = System.Text.Encoding.UTF8;

            byte[] bytes = ConvertStringToByteArray(reportStr);

            Page.Response.AddHeader("content-length", bytes.Length.ToString());
            Page.Response.Cache.SetExpires(DateTime.Now.AddSeconds(60));
            Page.Response.Cache.SetCacheability(HttpCacheability.Private);

            Page.Response.BinaryWrite(bytes);

            if (StiOptions.Web.AllowUseResponseFlush)
            {
                try
                {
                    Page.Response.Flush();
                }
                catch
                {
                }
            }

            Page.Response.End();

            return true;
        }

        private string GetHtmlCharSetString()
        {
            return string.Format("<meta HTTP-EQUIV='Content-Type' CONTENT='text/html; charset={0}'>", this.HtmlEncoding.WebName);
        }

        private StiForm GetWebForm(StiReport report)
        {
            if (report == null) return null;
            foreach (StiPage page in report.Pages)
            {
                if (page is StiForm && page.Enabled && ((StiForm)page).Visible) return page as StiForm;
            }
            return null;
        }

        protected string GetSessionId()
        {
            return Page != null ? GetApplicationUrl() + "." + this.ID + "." : this.ID + ".";
        }

        #endregion

        #region Fields

        internal StiExportFormat defaultSaveType = StiExportFormat.None;
        private int currentPageFromViewer = -1;
        private string callbackResult = string.Empty;
        private string prevServerReportName = null;
        private string prevCacheGuid = null;
        private string jsParameters = string.Empty;
        #endregion

        #region Properties.States
        private bool isPrinting = false;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Description("Internal use only.")]
        public bool IsPrinting
        {
            get
            {
                return isPrinting;
            }
        }


        private bool isSaving = false;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Description("Internal use only.")]
        public bool IsSaving
        {
            get
            {
                return isSaving;
            }
        }


        private bool isFirstButtonClicking = false;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Description("Internal use only.")]
        public bool IsFirstButtonClicking
        {
            get
            {
                return isFirstButtonClicking;
            }
        }


        private bool isPrevButtonClicking = false;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Description("Internal use only.")]
        public bool IsPrevButtonClicking
        {
            get
            {
                return isPrevButtonClicking;
            }
        }


        private bool isNextButtonClicking = false;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Description("Internal use only.")]
        public bool IsNextButtonClicking
        {
            get
            {
                return isNextButtonClicking;
            }
        }


        private bool isLastButtonClicking = false;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Description("Internal use only.")]
        public bool IsLastButtonClicking
        {
            get
            {
                return isLastButtonClicking;
            }
        }


        private StiWebViewMode newViewModeList = StiWebViewMode.OnePage;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Description("Internal use only.")]
        public StiWebViewMode NewViewModeList
        {
            get
            {
                return newViewModeList;
            }
        }

        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool IsWebFormReport
        {
            get
            {
                return GetWebForm(report) != null;
            }
        }

        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool IsImageRequest
        {
            get
            {
                if (this.Page.Request.Params["stimulreport_btnimage"] != null) return true;
                if (this.Page.Request.Params["stimulreport_image"] != null) return true;
                return false;
            }
        }

        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        private bool IsExportingReport
        {
            get
            {
                return this.Page != null && this.Page.ClientQueryString != null &&
                       this.Page.ClientQueryString.IndexOf(string.Format("sti_{0}_export=", this.ID)) >= 0;
            }
        }
        #endregion

        #region Properties
        #region CustomCss
        /// <summary>
        /// Gets or sets a path to the custom css file for the mobile viewer.
        /// </summary>        
        [Category("Settings")]
        [DefaultValue("")]
        [Description("Gets or sets a path to the custom css file for the mobile viewer.")]
        public string CustomCss
        {
            get
            {
                object customCss = this.ViewState["CustomCss"];
                return customCss is string ? (string)customCss : string.Empty;
            }
            set
            {
                this.ViewState["CustomCss"] = value;
            }
        }
        #endregion

        #region ScrollbiMode
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool ScrollbiMode
        {
            get
            {
                object scrollbiMode = this.ViewState["ScrollbiMode"];
                return scrollbiMode is bool ? (bool)scrollbiMode : false;
            }
            set
            {
                this.ViewState["ScrollbiMode"] = value;
            }
        }
        #endregion

        #region ScrollbiParameters
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        private Hashtable scrollbiParameters = null;
        public Hashtable ScrollbiParameters
        {
            get
            {
                return scrollbiParameters;
            }
            set
            {
                scrollbiParameters = value;
            }
        }
        #endregion

        #region Cache Helper
        private StiCacheHelper cacheHelper = null;
        /// <summary>
        /// This property to override methods of caching. 
        /// </summary>
        public StiCacheHelper CacheHelper
        {
            get
            {
                return cacheHelper;
            }
            set
            {
                cacheHelper = value;
            }
        }
        #endregion

        #region CacheReportGuid
        private string cacheReportGuid = string.Empty;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public string CacheReportGuid
        {
            get
            {
                if (cacheReportGuid == string.Empty) cacheReportGuid = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);
                return cacheReportGuid;
            }
            set
            {
                cacheReportGuid = value;
            }
        }
        #endregion

        #region CacheReport
        private StiReport cacheReport = null;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public StiReport CacheReport
        {
            get
            {
                if (cacheReport == null)
                {
                    if (CacheHelper != null)
                    {
                        cacheReport = CacheHelper.GetObjectFromCache(CacheReportGuid) as StiReport;
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Page) cacheReport = Page.Cache[CacheReportGuid] as StiReport;
                        else cacheReport = Page.Session[CacheReportGuid] as StiReport;
                    }
                }

                return cacheReport;
            }
            set
            {
                cacheReport = value;
                if (!DesignMode)
                {
                    if (CacheHelper != null)
                    {
                        CacheHelper.SaveObjectToCache(cacheReport, CacheReportGuid);
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Session)
                        {
                            Page.Session.Remove(CacheReportGuid);
                            Page.Session.Add(CacheReportGuid, cacheReport);
                        }
                        else
                        {
                            Page.Cache.Remove(CacheReportGuid);
                            Page.Cache.Add(CacheReportGuid, cacheReport, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 20, 0), CacheItemPriority.Low, null);
                        }
                    }
                }
            }
        }
        #endregion

        #region CacheReportStyles
        private ArrayList cacheReportStyles = null;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public ArrayList CacheReportStyles
        {
            get
            {
                if (cacheReportStyles == null)
                {
                    /*if (CacheHelper != null)
                    {
                        cacheReportStyles = CacheHelper.GetObjectFromCache(CacheReportGuid + "Styles") as ArrayList;
                    }
                    else*/
                    {
                        if (CacheMode == StiCacheMode.Page) cacheReportStyles = Page.Cache[CacheReportGuid + "Styles"] as ArrayList;
                        else cacheReportStyles = Page.Session[CacheReportGuid + "Styles"] as ArrayList;
                    }
                }

                return cacheReportStyles;
            }
            set
            {
                cacheReportStyles = value;
                if (!DesignMode)
                {
                    /*if (CacheHelper != null)
                    {
                        CacheHelper.SaveObjectToCache(cacheReportStyles, CacheReportGuid + "Styles");
                    }
                    else*/
                    {
                        if (CacheMode == StiCacheMode.Session)
                        {
                            Page.Session.Remove(CacheReportGuid + "Styles");
                            Page.Session.Add(CacheReportGuid + "Styles", cacheReportStyles);
                        }
                        else
                        {
                            Page.Cache.Remove(CacheReportGuid + "Styles");
                            Page.Cache.Add(CacheReportGuid + "Styles", cacheReportStyles, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 20, 0), CacheItemPriority.Low, null);
                        }
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Gets or sets a css style of scroll bars in the mobile viewer.
        /// </summary>
        [Browsable(true)]
        [Category("Settings")]
        [Description("Gets or sets a css style of scroll bars in the mobile viewer.")]
        private string ScrollBarCssStyle
        {
            get
            {
                object scrollBarCssStyle = this.ViewState["ScrollBarCssStyle"];
                return scrollBarCssStyle is string ? scrollBarCssStyle as string : "";
            }
            set
            {
                this.ViewState["ScrollBarCssStyle"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates which pass of the report rendering is now active - first or second.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets a value which indicates which pass of the report rendering is now active - first or second.")]
        public StiWebReportStatus WebReportStatus
        {
            get
            {
                object webReportStatus = this.ViewState["WebReportStatus"];
                return webReportStatus is StiWebReportStatus ? (StiWebReportStatus)webReportStatus : StiWebReportStatus.FirstPass;
            }
            set
            {
                this.ViewState["WebReportStatus"] = value;
            }
        }


        /// <summary>
        /// Internal use only.
        /// </summary>
        [Description("Internal use only.")]
        internal int PagesCount
        {
            get
            {
                return ReportForRendering != null ? ReportForRendering.RenderedPages.Count : 1;
            }
        }


        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        internal bool RequestPanelVisible
        {
            get
            {
                object requestPanelVisible = this.ViewState["RequestPanelVisible"];
                return requestPanelVisible is bool ? (bool)requestPanelVisible : true;
            }
            set
            {
                this.ViewState["RequestPanelVisible"] = value;
            }
        }


        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        internal string CacheGuid
        {
            get
            {
                string cacheGuid = this.ViewState["CacheGuid"] as string;
                return cacheGuid == null ? null : cacheGuid;
            }
            set
            {
                this.ViewState["CacheGuid"] = value;
            }
        }


        internal StiReport sourceReport = null;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        internal StiReport SourceReport
        {
            get
            {
                if (sourceReport == null && !string.IsNullOrEmpty(CacheGuid))
                {
                    string reportString;
                    if (CacheHelper != null)
                    {
                        reportString = CacheHelper.GetObjectFromCache(CacheGuid + "_SourceReport") as string;
                    }
                    else
                    {
                        if (this.CacheMode == StiCacheMode.Page) reportString = this.Page.Cache[CacheGuid + "_SourceReport"] as string;
                        else reportString = this.Page.Session[CacheGuid + "_SourceReport"] as string;
                    }
                    if (!string.IsNullOrEmpty(reportString))
                    {
                        sourceReport = new StiReport();
                        sourceReport.LoadPackedReportFromString(reportString);
                    }
                }

                return sourceReport;
            }
        }


        internal string interactionComponents = string.Empty;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        internal string InteractionComponents
        {
            get
            {
                if (string.IsNullOrEmpty(interactionComponents) && !string.IsNullOrEmpty(CacheGuid))
                {
                    if (CacheHelper != null)
                    {
                        interactionComponents = CacheHelper.GetObjectFromCache(CacheGuid + "_InteractionComponents") as string;
                    }
                    else
                    {
                        if (this.CacheMode == StiCacheMode.Page) interactionComponents = this.Page.Cache[CacheGuid + "_InteractionComponents"] as string;
                        else interactionComponents = this.Page.Session[CacheGuid + "_InteractionComponents"] as string;
                    }
                }

                return interactionComponents;
            }
        }


        private StiReport report = null;
        /// <summary>
        /// Gets or sets a report object which is shown in the mobile viewer.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets a report object which is shown in the mobile viewer.")]
        public StiReport Report
        {
            get
            {
                return report;
            }
            set
            {
                #region Saving the additional report parameters in the cache, if needed
                if (value != null && string.IsNullOrEmpty(CacheGuid))
                {
                    interactionComponents = string.Empty;
                    sourceReport = null;

                    #region Stores component names and sort direction, if they contain interactions
                    StiComponentsCollection comps = value.GetComponents();
                    foreach (StiComponent comp in comps)
                    {
                        if (comp.Interaction != null && !comp.Interaction.IsDefault)
                        {
                            if (interactionComponents.Length == 0) interactionComponents = "<InteractionComponents>";
                            interactionComponents += string.Format("<Component name=\"{0}\" ", XmlConvert.EncodeName(comp.Name));

                            StiInteractionSortDirection sortDirection = StiInteractionSortDirection.None;
                            string dataBandName = comp.Interaction.GetSortDataBandName();
                            StiDataBand dataBand = value.GetComponentByName(dataBandName) as StiDataBand;
                            if (dataBand != null)
                            {
                                string sortStr = string.Empty;
                                int index = 0;
                                bool isAsc = false;

                                foreach (string str in dataBand.Sort)
                                {
                                    if (str != "ASC" && str != "DESC")
                                    {
                                        if (sortStr.Length == 0) sortStr = str;
                                        else sortStr += "." + str;
                                    }

                                    if (str == "ASC" || str == "DESC" || index == dataBand.Sort.Length - 1)
                                    {
                                        if (sortStr.Length > 0 && sortStr == comp.Interaction.GetSortColumnsString())
                                        {
                                            if (isAsc) sortDirection = StiInteractionSortDirection.Ascending;
                                            else sortDirection = StiInteractionSortDirection.Descending;
                                        }

                                        sortStr = "";
                                        isAsc = str == "ASC";
                                    }

                                    index++;
                                }
                            }

                            interactionComponents += string.Format("sort=\"{0}\" />", sortDirection.ToString());
                        }
                    }

                    if (interactionComponents.Length > 0) interactionComponents += "</InteractionComponents>";
                    #endregion

                    #region Stores source report, if request from user variables present
                    if (value.Dictionary.IsRequestFromUserVariablesPresent || !string.IsNullOrEmpty(interactionComponents) || EnableRefreshData)
                    {
                        sourceReport = value;
                    }
                    #endregion

                    #region Stores values to server cache or session
                    if (UseCache)
                    {
                        if (!string.IsNullOrEmpty(prevCacheGuid)) CacheGuid = prevCacheGuid;
                        else CacheGuid = Guid.NewGuid().ToString();

                        if (sourceReport != null)
                        {
                            if (CacheHelper != null)
                            {
                                CacheHelper.SaveObjectToCache(sourceReport.SavePackedReportToString(), CacheGuid + "_SourceReport");
                            }
                            else
                            {
                                if (CacheMode == StiCacheMode.Page)
                                {
                                    this.Page.Cache.Remove(CacheGuid + "_SourceReport");
                                    this.Page.Cache.Add(CacheGuid + "_SourceReport", sourceReport.SavePackedReportToString(),
                                        null, Cache.NoAbsoluteExpiration, ServerTimeOut, System.Web.Caching.CacheItemPriority.Low, null);
                                }
                                else
                                {
                                    this.Page.Session.Remove(CacheGuid + "_SourceReport");
                                    this.Page.Session.Add(CacheGuid + "_SourceReport", sourceReport.SavePackedReportToString());
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(interactionComponents))
                        {
                            if (CacheHelper != null)
                            {
                                CacheHelper.SaveObjectToCache(interactionComponents, CacheGuid + "_InteractionComponents");
                            }
                            else
                            {
                                if (CacheMode == StiCacheMode.Page)
                                {
                                    this.Page.Cache.Remove(CacheGuid + "_InteractionComponents");
                                    this.Page.Cache.Add(CacheGuid + "_InteractionComponents", interactionComponents,
                                        null, Cache.NoAbsoluteExpiration, ServerTimeOut, System.Web.Caching.CacheItemPriority.Low, null);
                                }
                                else
                                {
                                    this.Page.Session.Remove(CacheGuid + "_InteractionComponents");
                                    this.Page.Session.Add(CacheGuid + "_InteractionComponents", interactionComponents);
                                }
                            }
                        }
                    }
                    #endregion
                }
                #endregion

                #region Crear current report page number and bookmarks tree settings
                if (report == null && UseCache && ResetPageNumberOnAssignNewReport && !IsExportingReport)
                {
                    CurrentPage = 0;

                    HttpCookie cookie;

                    cookie = Page.Request.Cookies.Get("cobmrk");
                    if (cookie != null)
                    {
                        cookie.Value = string.Empty;
                        cookie.Path = Page.Request.ApplicationPath + "/";
                        Page.Response.Cookies.Add(cookie);
                    }

                    cookie = Page.Request.Cookies.Get("cdbmrk");
                    if (cookie != null)
                    {
                        cookie.Value = string.Empty;
                        cookie.Path = Page.Request.ApplicationPath + "/";
                        Page.Response.Cookies.Add(cookie);
                    }
                }
                #endregion

                #region Reset the report, if needed
                //if (!IsDesignMode && value != null && !value.Dictionary.isRequestFromUserVariablesPresent) ResetReport();
                #endregion

                report = value;

                #region Process the report, if needed
                if (!IsDesignMode && (report != null || (ServerReportName.Length > 0 && ServerReportName != null))) this.ProcessReport();
                WebReportStatus = StiWebReportStatus.FirstPass;
                #endregion
            }
        }

        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool DemoMode
        {
            get
            {
                object demoMode = this.ViewState["DemoMode"];
                return demoMode is bool ? (bool)demoMode : false;
            }
            set
            {
                this.ViewState["DemoMode"] = value;
            }
        }

        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool CloudMode
        {
            get
            {
                object cloudMode = this.ViewState["CloudMode"];
                return cloudMode is bool ? (bool)cloudMode : false;
            }
            set
            {
                this.ViewState["CloudMode"] = value;
            }
        }

        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool DesignerMode
        {
            get
            {
                object designerMode = this.ViewState["DesignerMode"];
                return designerMode is bool ? (bool)designerMode : false;
            }
            set
            {
                this.ViewState["DesignerMode"] = value;
            }
        }

        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public string ServerReportName
        {
            get
            {
                string reportName = this.ViewState["ServerReportName"] as string;
                return reportName == null ? "" : reportName;
            }
            set
            {
                this.ViewState["ServerReportName"] = value;
            }
        }


        /// <summary>
        /// Gets or sets an index of the current page which is shown now in the mobile viewer.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets an index of the current page which is shown now in the mobile viewer.")]
        public int CurrentPage
        {
            get
            {
                object currentPage = null;
                /*if (IsAjax)
                {
                    if (!IsDesignMode)
                    {
                        currentPage = this.Page.Session[GetSessionId() + "CurrentPage"]; //TO DO Tolyan
                        if (currentPage is int) return (int)currentPage;
                    }
                }*/
                currentPage = this.ViewState["CurrentPage"];
                return currentPage is int ? (int)currentPage : 0;
            }
            set
            {
                this.ViewState["CurrentPage"] = value;

                /*if (IsAjax)
                {
                    if (!IsDesignMode && Page != null) this.Page.Session[GetSessionId() + "CurrentPage"] = value;
                }*/
            }
        }


        /// <summary>
        /// Gets or sets a value which controls of full screen mode.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(false)]
        [Description("Gets or sets a value which controls of full screen mode.")]
        public bool FullScreen
        {
            get
            {
                object fullScreen = this.ViewState["FullScreen"];
                return fullScreen is bool ? (bool)fullScreen : false;
            }
            set
            {
                this.ViewState["FullScreen"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the web viewer will show the report with scrollbars.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(false)]
        [Description("Gets or sets a value which indicates that the web viewer will show the report with scrollbars.")]
        public bool ScrollbarsMode
        {
            get
            {
                object scrollbarsMode = this.ViewState["ScrollbarsMode"];
                return scrollbarsMode is bool ? (bool)scrollbarsMode : false;
            }
            set
            {
                this.ViewState["ScrollbarsMode"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value which indicates that show or hide tooltips.
        /// </summary>
        [DefaultValue(true)]
        [Category("Settings")]
        [Description("Gets or sets a value which indicates that show or hide tooltips.")]
        public bool ShowTooltips
        {
            get
            {
                object showTooltips = this.ViewState["ShowTooltips"];
                return showTooltips is bool ? (bool)showTooltips : true;
            }
            set
            {
                this.ViewState["ShowTooltips"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value which indicates that show or hide tooltips help icon.
        /// </summary>
        [DefaultValue(true)]
        [Category("Settings")]
        [Description("Gets or sets a value which indicates that show or hide tooltips help icon.")]
        public bool ShowTooltipsHelp
        {
            get
            {
                object showTooltipsHelp = this.ViewState["ShowTooltipsHelp"];
                return showTooltipsHelp is bool ? (bool)showTooltipsHelp : true;
            }
            set
            {
                this.ViewState["ShowTooltipsHelp"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value which indicates how much time do the report rendering result will be stored in the server cache.
        /// </summary>
        [Browsable(true)]
        [Category("Settings")]
        [Description("Gets or sets a value which indicates how much time do the report rendering result will be stored in the server cache.")]
        public TimeSpan ServerTimeOut
        {
            get
            {
                object serverTimeOut = this.ViewState["ServerTimeOut"];
                return serverTimeOut is TimeSpan ? (TimeSpan)serverTimeOut : new TimeSpan(0, 10, 0);
            }
            set
            {
                this.ViewState["ServerTimeOut"] = value;
            }
        }


        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        internal StiReport ReportForRendering
        {
            get
            {
                if (Report == null) return null;
                return Report.NeedsCompiling ? Report.CompiledReport : Report;
            }
        }

        /// <summary>
        /// Gets or sets the current visual theme which is used for drawing visual elements of the mobile viewer.
        /// </summary>        
        [Category("Settings")]
        [DefaultValue(StiMobileViewerTheme.Office2013)]
        [Description("Gets or sets the current visual theme which is used for drawing visual elements of the mobile viewer.")]
        public StiMobileViewerTheme Theme
        {
            get
            {
                object theme = this.ViewState["Theme"];
                return theme is StiMobileViewerTheme ? (StiMobileViewerTheme)theme : StiMobileViewerTheme.Office2013WhiteBlue;
            }
            set
            {
                if (value == StiMobileViewerTheme.Office2013) value = StiMobileViewerTheme.Office2013WhiteBlue;
                this.ViewState["Theme"] = value;
            }
        }

        /// <summary>
        /// If theme is Office & Mobile browser detected - switch to material theme.
        /// </summary>        
        [Category("Settings")]
        [DefaultValue(false)]
        [Description("If theme is Office & Mobile browser detected - switch to material theme")]
        public Boolean AutoSwitchToMobileTheme
        {
            get
            {
                object autoSwitchToMobileTheme = this.ViewState["AutoSwitchToMobileTheme"];
                return autoSwitchToMobileTheme is Boolean ? (Boolean)autoSwitchToMobileTheme : false;
            }
            set
            {
                this.ViewState["AutoSwitchToMobileTheme"] = value;
            }
        }

        /// <summary>
        /// If theme is Office & Mobile browser detected - redirect to mobile page
        /// </summary>        
        [Category("Settings")]
        [DefaultValue(false)]
        [Description("If theme is Office & Mobile browser detected - redirect to mobile page")]
        public String AutoRedirectMobilePage
        {
            get
            {
                object autoRedirectMobilePage = this.ViewState["AutoRedirectMobilePage"];
                return autoRedirectMobilePage is String ? (String)autoRedirectMobilePage : "";
            }
            set
            {
                this.ViewState["AutoRedirectMobilePage"] = value;
            }
        }


        /// <summary>
        /// Gets or sets alignment of the report content in the mobile viewer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(StiContentAlignment.Center)]
        [Description("Gets or sets alignment of the report content in the mobile viewer.")]
        public StiContentAlignment ContentAlignment
        {
            get
            {
                object contentAlignment = this.ViewState["ContentAlignment"];
                return contentAlignment is StiContentAlignment ? (StiContentAlignment)contentAlignment : StiContentAlignment.Left;
            }
            set
            {
                this.ViewState["ContentAlignment"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the mode of showing a report in the mobile viewer - one page or the whole report.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(StiWebViewMode.OnePage)]
        [Description("Gets or sets the mode of showing a report in the mobile viewer - one page or the whole report.")]
        public StiWebViewMode ViewMode
        {
            get
            {
                object viewMode = null;

                /*if (IsAjax)
                {
                    if (!IsDesignMode)
                    {
                        viewMode = this.Page.Session[GetSessionId() + "ViewMode"]; //TO DO Tolyan
                        if (viewMode is StiWebViewMode) return (StiWebViewMode)viewMode;
                    }
                }*/

                viewMode = this.ViewState["ViewMode"];
                return viewMode is StiWebViewMode ? (StiWebViewMode)viewMode : StiWebViewMode.OnePage;
            }
            set
            {
                this.ViewState["ViewMode"] = value;
                //if (!IsDesignMode && Page != null) this.Page.Session[GetSessionId() + "ViewMode"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the mode of report printing from the mobile viewer - the current page or the whole report.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(StiPrintMode.WholeReport)]
        [Description("Gets or sets the mode of report printing from the mobile viewer - the current page or the whole report.")]
        public StiPrintMode PrintMode
        {
            get
            {
                object printMode = this.ViewState["PrintMode"];
                return printMode is StiPrintMode ? (StiPrintMode)printMode : StiPrintMode.WholeReport;
            }
            set
            {
                this.ViewState["PrintMode"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which allows printing report bookmarks.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows printing report bookmarks.")]
        public bool AllowPrintBookmarks
        {
            get
            {
                object allowPrintBookmarks = this.ViewState["AllowPrintBookmarks"];
                return allowPrintBookmarks is bool ? (bool)allowPrintBookmarks : true;
            }
            set
            {
                this.ViewState["AllowPrintBookmarks"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the mode of the report caching.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(StiCacheMode.Page)]
        [Description("Gets or sets the mode of the report caching.")]
        public StiCacheMode CacheMode
        {
            get
            {
                object cacheMode = this.ViewState["CacheMode"];
                return cacheMode is StiCacheMode ? (StiCacheMode)cacheMode : StiCacheMode.Page;
            }
            set
            {
                this.ViewState["CacheMode"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the report showing zoom. The default value is 100.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(100f)]
        [Description("Gets or sets the report showing zoom. The default value is 100.")]
        public float ZoomPercent
        {
            get
            {
                object zoomPercent = null;

                /*if (IsAjax)
                {
                    if (!IsDesignMode)
                    {
                        zoomPercent = this.Page.Session[GetSessionId() + "Zoom"]; //TO DO Tolyan
                        if (zoomPercent is float) return (float)zoomPercent;
                    }
                }*/

                zoomPercent = this.ViewState["Zoom"];
                return zoomPercent is float ? (float)zoomPercent : 100f;
            }
            set
            {
                if (value > 0)
                {
                    this.ViewState["Zoom"] = value;
                    //if (!IsDesignMode && Page != null) this.Page.Session[GetSessionId() + "Zoom"] = value;
                }
            }
        }


        /// <summary>
        /// Gets or sets the report scale factor. The default value is 1.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(1f)]
        [Description("Gets or sets the report scale factor. The default value is 1.")]
        public float ReportScale
        {
            get
            {
                object reportScale = this.ViewState["ReportScale"];
                return reportScale is float ? (float)reportScale : 1f;
            }
            set
            {
                if (value > 0) this.ViewState["ReportScale"] = value;
            }
        }


        /// <summary>
        ///Gets or sets a value which indicates that the mobile viewer will use cache.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the mobile viewer will use cache.")]
        public bool UseCache
        {
            get
            {
                return RenderMode == StiRenderMode.AjaxWithCache;
            }
            set
            {
                RenderMode = (value) ? StiRenderMode.AjaxWithCache : StiRenderMode.Ajax;
            }
        }


        /// <summary>
        /// Returns true if the mobile viewer uses one of ajax modes of the report rendering.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(false)]
        [Browsable(false)]
        [Description("Returns true if the mobile viewer uses one of ajax modes of the report rendering.")]
        public bool IsAjax
        {
            get
            {
                return RenderMode == StiRenderMode.Ajax || RenderMode == StiRenderMode.AjaxWithCache;
            }
        }


        /// <summary>
        /// Gets or sets a method how the mobile viewer will show and render a report.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(StiRenderMode.Ajax)]
        [Description("Gets or sets a method how the mobile viewer will show and render a report.")]
        private StiRenderMode RenderMode
        {
            get
            {
                object renderMode = this.ViewState["RenderMode"];
                return renderMode is StiRenderMode ? (StiRenderMode)renderMode : StiRenderMode.AjaxWithCache;
            }
            set
            {
                this.ViewState["RenderMode"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a method how the mobile viewer will show a report.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(StiReportDisplayMode.Table)]
        [Description("Gets or sets a method how the mobile viewer will show a report.")]
        public StiReportDisplayMode ReportDisplayMode
        {
            get
            {
                object reportDisplayMode = this.ViewState["ReportDisplayMode"];
                return reportDisplayMode is StiReportDisplayMode ? (StiReportDisplayMode)reportDisplayMode : StiReportDisplayMode.Table;
            }
            set
            {
                this.ViewState["ReportDisplayMode"] = value;
            }
        }


        /// <summary>
        /// This property is obsolete. Please use 'ImagesPath' instead of it.
        /// </summary>
        [Browsable(false)]
        [Obsolete("This property is obsolete. Please use 'ImagesPath' instead it.")]
        [Description("")]
        public string ButtonImagesPath
        {
            get
            {
                return ImagesPath;
            }
            set
            {
                ImagesPath = value;
            }
        }


        /// <summary>
        /// Gets or sets a path to the folder with images which is used for showing the mobile viewer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue("")]
        [Description("Gets or sets a path to the folder with images which is used for showing the mobile viewer.")]
        public string ImagesPath
        {
            get
            {
                object imagesPath = this.ViewState["ImagesPath"];
                return imagesPath is string ? (string)imagesPath : string.Empty;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string separator = "/";
                    if (value.IndexOf("\\") >= 0) separator = "\\";
                    if (!value.EndsWith(separator)) value += separator;
                }
                this.ViewState["ImagesPath"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that parameters panel will be shown in the mobile viewer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that parameters panel will be shown in the mobile viewer.")]
        public bool ShowPanelParameters
        {
            get
            {
                object showPanelParameters = this.ViewState["ShowPanelParameters"];
                return showPanelParameters is bool ? (bool)showPanelParameters : true;
            }
            set
            {
                this.ViewState["ShowPanelParameters"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that report bookmarks will be shown in the mobile viewer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that report bookmarks will be shown in the mobile viewer.")]
        public bool AllowBookmarks
        {
            get
            {
                object allowBookmarks = this.ViewState["AllowBookmarks"];
                return allowBookmarks is bool ? (bool)allowBookmarks : true;
            }
            set
            {
                this.ViewState["AllowBookmarks"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a width of the bookmarks tree in the mobile viewer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(180)]
        [Description("Gets or sets a width of the bookmarks tree in the mobile viewer.")]
        public int BookmarksTreeWidth
        {
            get
            {
                object bookmarksTreeWidth = this.ViewState["BookmarksTreeWidth"];
                return bookmarksTreeWidth is int ? (int)bookmarksTreeWidth : 180;
            }
            set
            {
                this.ViewState["BookmarksTreeWidth"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a height of the bookmarks tree in the mobile viewer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(0)]
        [Description("Gets or sets a height of the bookmarks tree in the mobile viewer.")]
        public int BookmarksTreeHeight
        {
            get
            {
                object bookmarksTreeHeight = this.ViewState["BookmarksTreeHeight"];
                return bookmarksTreeHeight is int ? (int)bookmarksTreeHeight : 0;
            }
            set
            {
                this.ViewState["BookmarksTreeHeight"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a count columns in parameters Panel.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(2)]
        [Description("Gets or sets a count columns in parameters Panel.")]
        public int CountColumnsParameters
        {
            get
            {
                object countColumnsParameters = this.ViewState["CountColumnsParameters"];
                return countColumnsParameters is int ? (int)countColumnsParameters : 2;
            }
            set
            {
                this.ViewState["CountColumnsParameters"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a max height parameters Panel.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(300)]
        [Description("Gets or sets a max height parameters Panel.")]
        /// <summary>
        /// Gets or sets a max height parameters Panel.
        /// </summary>
        public int MaxHeightParametersPanel
        {
            get
            {
                object maxHeightParametersPanel = this.ViewState["MaxHeightParametersPanel"];
                return maxHeightParametersPanel is int ? (int)maxHeightParametersPanel : 300;
            }
            set
            {
                this.ViewState["MaxHeightParametersPanel"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a path to the globalization file for the mobile viewer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue("")]
        [Description("Gets or sets a path to the globalization file for the mobile viewer.")]
        public string GlobalizationFile
        {
            get
            {
                object globalizationFile = this.ViewState["GlobalizationFile"];
                return globalizationFile is string ? (string)globalizationFile : string.Empty;
            }
            set
            {
                this.ViewState["GlobalizationFile"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value which indicates that hyperlinks from the report will be opened in the new browser window.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that hyperlinks from the report will be opened in the new browser window.")]
        public bool OpenLinksInNewWindow
        {
            get
            {
                object openLinksInNewWindow = this.ViewState["OpenLinksInNewWindow"];
                return openLinksInNewWindow is bool ? (bool)openLinksInNewWindow : true;
            }
            set
            {
                this.ViewState["OpenLinksInNewWindow"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the mobile viewer will use relative or absolute URLs for getting images.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the mobile viewer will use relative or absolute URLs for getting images.")]
        public bool UseRelativeUrls
        {
            get
            {
                object useRelativeUrls = this.ViewState["UseRelativeUrls"];
                return useRelativeUrls is bool ? (bool)useRelativeUrls : true;
            }
            set
            {
                this.ViewState["UseRelativeUrls"] = value;
            }
        }


        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool ResetPageNumberOnAssignNewReport
        {
            get
            {
                object resetPageNumberOnAssignNewReport = this.ViewState["ResetPageNumberOnAssignNewReport"];
                return resetPageNumberOnAssignNewReport is bool ? (bool)resetPageNumberOnAssignNewReport : true;
            }
            set
            {
                this.ViewState["ResetPageNumberOnAssignNewReport"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a name of the frame which will be used for opening hyperlinks from the report.
        /// </summary>
        [Category("Settings")]
        [DefaultValue("")]
        [Description("Gets or sets a name of the frame which will be used for opening hyperlinks from the report.")]
        public string OpenLinksInFrame
        {
            get
            {
                object openLinksInFrame = this.ViewState["OpenLinksInFrame"];
                return openLinksInFrame is string ? (string)openLinksInFrame : string.Empty;
            }
            set
            {
                this.ViewState["OpenLinksInFrame"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value which allows touch zoom in the mobile viewer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows touch zoom in the mobile viewer.")]
        public bool AllowTouchZoom
        {
            get
            {
                object allowTouchZoom = this.ViewState["AllowTouchZoom"];
                return allowTouchZoom is bool ? (bool)allowTouchZoom : true;
            }
            set
            {
                this.ViewState["AllowTouchZoom"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value which allows touch zoom in the mobile viewer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(false)]
        [Description("Gets or sets a value which allows refresh data event.")]
        public bool EnableRefreshData
        {
            get
            {
                object enableRefreshData = this.ViewState["EnableRefreshData"];
                return enableRefreshData is bool ? (bool)enableRefreshData : true;
            }
            set
            {
                this.ViewState["EnableRefreshData"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the default mode of the report print destination.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(StiPrintDestination.PopupMenu)]
        [Description("Gets or sets the default mode of the report print destination.")]
        public StiPrintDestination PrintDestination
        {
            get
            {
                object printDestination = this.ViewState["PrintDestination"];
                return printDestination is StiPrintDestination ? (StiPrintDestination)printDestination : StiPrintDestination.PopupMenu;
            }
            set
            {
                this.ViewState["PrintDestination"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a color of the toolbar background.
        /// </summary>
        [Category("Toolbar")]
        [Description("Gets or sets a color of the toolbar background.")]
        public Color ToolBarBackColor
        {
            get
            {
                object toolBarBackColor = this.ViewState["ToolBarBackColor"];
                return toolBarBackColor is Color ? (Color)toolBarBackColor : Color.White;
            }
            set
            {
                this.ViewState["ToolBarBackColor"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a dark color of the page border.
        /// </summary>
        [Category("Page")]
        [Description("Gets or sets a color of the page border.")]
        public Color PageBorderColor
        {
            get
            {
                object pageBorderColor = this.ViewState["PageBorderColor"];
                return pageBorderColor is Color ? (Color)pageBorderColor : Color.Gray;
            }
            set
            {
                this.ViewState["PageBorderColor"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the shadow of the page will be displayed in the mobile viewer.
        /// </summary>
        [Category("Page")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the shadow of the page will be displayed in the mobile viewer.")]
        public bool ShowPageShadow
        {
            get
            {
                object ShowPageShadow = this.ViewState["ShowPageShadow"];
                return ShowPageShadow is bool ? (bool)ShowPageShadow : true;
            }
            set
            {
                this.ViewState["ShowPageShadow"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value which indicates that the mobile viewer will show report pages with/without space between them.
        /// </summary>
        [Category("Page")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the mobile viewer will show report pages with/without space between them.")]
        public bool ShowPageBorders
        {
            get
            {
                object showPageBorders = this.ViewState["ShowPageBorders"];
                return showPageBorders is bool ? (bool)showPageBorders : true;
            }
            set
            {
                this.ViewState["ShowPageBorders"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a width of the page border. The default value is 1.
        /// </summary>
        [Category("Page")]
        [DefaultValue(1)]
        [Description("Gets or sets a width of the page border. The default value is 1.")]
        public int PageBorderSize
        {
            get
            {
                object pageBorderSize = this.ViewState["PageBorderSize"];
                return pageBorderSize is int ? (int)pageBorderSize : 1;
            }
            set
            {
                this.ViewState["PageBorderSize"] = value;
            }
        }


        /// <summary>
        /// This property is no longer used.
        /// </summary>
        [Browsable(false)]
        [Obsolete("This property is no longer used.")]
        [Description("This property is no longer used.")]
        public bool ExportsNewWindow
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        private StiWebImageHost webImageHost = null;
        /// <summary>
        /// Gets or sets the StiWebImageHost object which converts images to urls.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets the StiWebImageHost object which converts images to urls.")]
        public StiWebImageHost WebImageHost
        {
            get
            {
                return webImageHost;
            }
            set
            {
                webImageHost = value;
            }
        }


        /// <summary>
        /// Gets or sets the type of rendering charts.
        /// </summary>        
        [Category("Settings")]
        [DefaultValue(StiTypeRenderChart.AnimatedVector)]
        [Description("Gets or sets the type of rendering charts.")]
        public StiTypeRenderChart TypeRenderChart
        {
            get
            {
                object typeChartRender = this.ViewState["TypeRenderChart"];
                return typeChartRender is StiTypeRenderChart ? (StiTypeRenderChart)typeChartRender : StiTypeRenderChart.AnimatedVector;
            }
            set
            {
                this.ViewState["TypeRenderChart"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the interface type of the mobile viewer.
        /// </summary>        
        [Category("Settings")]
        [DefaultValue(StiInterfaceType.Auto)]
        [Description("Gets or sets the interface type of the mobile viewer.")]
        public StiInterfaceType InterfaceType
        {
            get
            {
                object interfaceType = this.ViewState["InterfaceType"];
                return interfaceType is StiInterfaceType ? (StiInterfaceType)interfaceType : StiInterfaceType.Auto;
            }
            set
            {
                this.ViewState["InterfaceType"] = value;
            }
        }

        private bool cacheClientSide = false;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool CacheClientSide
        {
            get
            {
                return cacheClientSide;
            }
            set
            {
                cacheClientSide = value;
            }
        }

        private bool passQueryParametersForResources = true;
        /// <summary>
        /// Gets or sets a value which enables or disables the transfer of URL parameters when requesting the scripts and styles of the mobile viewer.
        /// </summary>  
        [Category("Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets the interface type of the mobile viewer.")]
        public bool PassQueryParametersForResources
        {
            get
            {
                return passQueryParametersForResources;
            }
            set
            {
                passQueryParametersForResources = value;
            }
        }

        /// <summary>
        /// Gets or sets the first day of week in the date picker.
        /// </summary>        
        [Category("Settings")]
        [DefaultValue(StiFirstDayOfWeek.Monday)]
        [Description("Gets or sets the first day of week in the date picker.")]
        public StiFirstDayOfWeek DatePickerFirstDayOfWeek
        {
            get
            {
                object datePickerFirstDayOfWeek = this.ViewState["DatePickerFirstDayOfWeek"];
                return datePickerFirstDayOfWeek is StiFirstDayOfWeek ? (StiFirstDayOfWeek)datePickerFirstDayOfWeek : StiFirstDayOfWeek.Monday;
            }
            set
            {
                this.ViewState["DatePickerFirstDayOfWeek"] = value;
            }
        }
        #endregion

        #region Properties.Export.Settings
        private bool storeExportSettings = true;
        /// <summary>
        /// Gets or sets a value which allows store the export settings in the cookies.
        /// </summary>
        [Browsable(true)]
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows store the export settings in the cookies.")]
        public bool StoreExportSettings
        {
            get
            {
                return storeExportSettings;
            }
            set
            {
                storeExportSettings = value;
            }
        }

        /// <summary>
        /// Gets or sets a value which indicates that the export dialog will be displayed when the mobile viewer starts exporting to the XPS format.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the export dialog will be displayed when the mobile viewer starts exporting to the XPS format.")]
        public bool XpsShowDialog
        {
            get
            {
                object xpsShowDialog = this.ViewState["XpsShowDialog"];
                return xpsShowDialog is bool ? (bool)xpsShowDialog : true;
            }
            set
            {
                this.ViewState["XpsShowDialog"] = value;
            }
        }


        /// <summary>
        /// Gets or sets mode of the image compression in the result PDF file.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(StiPdfImageCompressionMethod.Jpeg)]
        [Description("Gets or sets mode of the image compression in the result PDF file.")]
        public StiPdfImageCompressionMethod PdfImageCompressionMethod
        {
            get
            {
                object pdfImageCompressionMethod = this.ViewState["PdfImageCompressionMethod"];
                return pdfImageCompressionMethod is StiPdfImageCompressionMethod ? (StiPdfImageCompressionMethod)pdfImageCompressionMethod : StiPdfImageCompressionMethod.Jpeg;
            }
            set
            {
                this.ViewState["PdfImageCompressionMethod"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the result PDF file will include fonts which are used in the report.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the result PDF file will include fonts which are used in the report.")]
        public bool PdfEmbeddedFonts
        {
            get
            {
                object pdfEmbeddedFonts = this.ViewState["PdfEmbeddedFonts"];
                return pdfEmbeddedFonts is bool ? (bool)pdfEmbeddedFonts : true;
            }
            set
            {
                this.ViewState["PdfEmbeddedFonts"] = value;
            }
        }


        /// <summary>
        /// PdfEmbeddedFont property is obsolete. Please use PdfEmbeddedFonts property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Description("")]
        [Obsolete("PdfEmbeddedFont property is obsolete. Please use PdfEmbeddedFonts property.")]
        public bool PdfEmbeddedFont
        {
            get
            {
                return PdfEmbeddedFonts;
            }
            set
            {
                PdfEmbeddedFonts = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the result PDF file will use Unicode encoding for the text.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the result PDF file will use Unicode encoding for the text.")]
        public bool PdfUseUnicode
        {
            get
            {
                object pdfUseUnicode = this.ViewState["PdfUseUnicode"];
                return pdfUseUnicode is bool ? (bool)pdfUseUnicode : true;
            }
            set
            {
                this.ViewState["PdfUseUnicode"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the export dialog will be displayed when the mobile viewer starts exporting to the PDF format.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the export dialog will be displayed when the mobile viewer starts exporting to the PDF format.")]
        public bool PdfShowDialog
        {
            get
            {
                object pdfShowDialog = this.ViewState["PdfShowDialog"];
                return pdfShowDialog is bool ? (bool)pdfShowDialog : true;
            }
            set
            {
                this.ViewState["PdfShowDialog"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the export dialog will be displayed when the mobile viewer starts exporting to the HTML format.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the export dialog will be displayed when the mobile viewer starts exporting to the HTML format.")]
        public bool HtmlShowDialog
        {
            get
            {
                object htmlShowDialog = this.ViewState["HtmlShowDialog"];
                return htmlShowDialog is bool ? (bool)htmlShowDialog : true;
            }
            set
            {
                this.ViewState["HtmlShowDialog"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the result PDF file will use standard fonts.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(false)]
        [Description("Gets or sets a value which indicates that the result PDF file will use standard fonts.")]
        public bool PdfStandardFonts
        {
            get
            {
                object pdfStandardFonts = this.ViewState["PdfStandardFonts"];
                return pdfStandardFonts is bool ? (bool)pdfStandardFonts : false;
            }
            set
            {
                this.ViewState["PdfStandardFonts"] = value;
            }
        }


        /// <summary>
        /// Please use ImageQuality property instead PdfImageQuality property.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(1f)]
        [Obsolete("Please use ImageQuality property instead PdfImageQuality property.")]
        [Description("")]
        public float PdfImageQuality
        {
            get
            {
                return ImageQuality;
            }
            set
            {
                ImageQuality = value;
            }
        }


        /// <summary>
        /// Please use ImageResolution property instead PdfImageResolution property.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(100)]
        [Obsolete("Please use ImageResolution property instead PdfImageResolution property.")]
        [Description("")]
        public int PdfImageResolution
        {
            get
            {
                return ImageResolution;
            }
            set
            {
                ImageResolution = value;
            }
        }


        /// <summary>
        /// Gets or sets an image format which will be used in the HTML export.
        /// </summary>
        [Category("Export Settings")]
        [Description("Gets or sets an image format which will be used in the HTML export.")]
        public ImageFormat ImageFormat
        {
            get
            {
                object imageFormat = this.ViewState["ImageFormat"];
                return imageFormat is ImageFormat ? (ImageFormat)imageFormat : ImageFormat.Png;
            }
            set
            {
                this.ViewState["ImageFormat"] = value;
            }
        }


        internal bool ShouldSerializeImageFormat()
        {
            if (ImageFormat != ImageFormat.Png) return true;
            return false;
        }


        /// <summary>
        /// Gets or sets the encoding name used when exporting report to HTML.
        /// </summary>
        [Category("Export Settings")]
        [Description("Gets or sets the encoding name used when exporting report to HTML.")]
        [Editor(typeof(StiHtmlToEditor), typeof(UITypeEditor))]
        [DefaultValue("Unicode (UTF-8)")]
        public string HtmlCharSet
        {
            get
            {
                object encoding = this.ViewState["HtmlCharSet"];
                return encoding is string ? encoding as string : "";// 65001;
            }
            set
            {
                this.ViewState["HtmlCharSet"] = value;
            }
        }


        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public Encoding HtmlEncoding
        {
            get
            {
                string name = HtmlCharSet;
                StiEncoding[] encodings = StiEncoding.GetEncodings();
                foreach (StiEncoding encoding in encodings)
                {
                    if (encoding.DisplayName == name)
                        return Encoding.GetEncoding(encoding.CodePage);
                }
                return null;
            }
            set
            {
                HtmlCharSet = value.EncodingName;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates quality of the generated HTML file.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(StiHtmlExportQuality.High)]
        [Browsable(false)]
        [Description("")]
        public StiHtmlExportQuality HtmlQuality
        {
            get
            {
                object htmlQuality = this.ViewState["HtmlQuality"];
                return htmlQuality is StiHtmlExportQuality ? (StiHtmlExportQuality)htmlQuality : StiHtmlExportQuality.High;
            }
            set
            {
                this.ViewState["HtmlQuality"] = value;
            }
        }


        /// <summary>
        /// Gets or sets value which indicates the export mode to HTML format.
        /// </summary>
        [Browsable(false)]
        [Category("Export Settings")]
        [DefaultValue(StiHtmlExportMode.Table)]
        [Description("Gets or sets value which indicates the export mode to HTML format.")]
        public StiHtmlExportMode HtmlExportMode
        {
            get
            {
                object htmlExportMode = this.ViewState["HtmlExportMode"];
                return htmlExportMode is StiHtmlExportMode ? (StiHtmlExportMode)htmlExportMode : StiHtmlExportMode.Table;
            }
            set
            {
                this.ViewState["HtmlExportMode"] = value;
            }
        }


        /// <summary>
        /// Gets or sets value which indicates the export mode to RTF format.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(StiRtfExportMode.Table)]
        [Description("Gets or sets value which indicates the export mode to RTF format.")]
        public StiRtfExportMode RtfExportMode
        {
            get
            {
                object rtfExportMode = this.ViewState["RtfExportMode"];
                return rtfExportMode is StiRtfExportMode ? (StiRtfExportMode)rtfExportMode : StiRtfExportMode.Table;
            }
            set
            {
                this.ViewState["RtfExportMode"] = value;
            }
        }


        /// <summary>
        /// This property is obsolete.
        /// </summary>
        [Browsable(false)]
        [Obsolete]
        [Description("This property is obsolete.")]
        public int RtfCodePage
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }


        /// <summary>
        /// Gets or sets a string separator for the export to CSV format.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue("")]
        [Description("Gets or sets the string separator for the export to CSV format.")]
        public string CsvSeparator
        {
            get
            {
                object csvSeparator = this.ViewState["CsvSeparator"];
                return csvSeparator is string ? (string)csvSeparator : string.Empty;
            }
            set
            {
                this.ViewState["CsvSeparator"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a name of the CharSet which used for exporting to the CSV format.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue("Unicode (UTF-8)")]
        [Editor(typeof(StiHtmlToEditor), typeof(UITypeEditor))]
        [Description("Gets or sets a name of the CharSet which used for exporting to the CSV format.")]
        public string CsvCharSet
        {
            get
            {
                object csvCharSet = this.ViewState["CsvCharSet"];
                return csvCharSet is string ? (string)csvCharSet : string.Empty;
            }
            set
            {
                this.ViewState["CsvCharSet"] = value;
            }
        }


        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public Encoding CsvEncoding
        {
            get
            {
                string name = CsvCharSet;
                StiEncoding[] encodings = StiEncoding.GetEncodings();
                foreach (StiEncoding encoding in encodings)
                {
                    if (encoding.DisplayName == name)
                        return Encoding.GetEncoding(encoding.CodePage);
                }
                return null;
            }
            set
            {
                CsvCharSet = value.EncodingName;
            }
        }


        /// <summary>
        /// Gets or sets a CodePage for the export to the DBF format.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(StiDbfCodePages.Default)]
        [Description("Gets or sets a CodePage for the export to the DBF format.")]
        public StiDbfCodePages DbfCodePages
        {
            get
            {
                object dbfCodePages = this.ViewState["DbfCodePages"];
                return dbfCodePages is StiDbfCodePages ? (StiDbfCodePages)dbfCodePages : StiDbfCodePages.Default;
            }
            set
            {
                this.ViewState["DbfCodePages"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that only one page header and page footer will be exported to the result Excel file. The property has affect on Excel 2007-2010 export.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(false)]
        [Description("Gets or sets a value which indicates that only one page header and page footer will be exported to the result Excel file. The property has affect on Excel 2007-2010 export.")]
        public bool ExcelUseOnePageHeaderAndFooter
        {
            get
            {
                object value = this.ViewState["ExcelUseOnePageHeaderAndFooter"];
                return value is bool ? (bool)value : false;
            }
            set
            {
                this.ViewState["ExcelUseOnePageHeaderAndFooter"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that page breaks will be exported to the Excel file. The property has affect on Excel 2007-2010 export.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(false)]
        [Description("Gets or sets a value which indicates that page breaks will be exported to the Excel file. The property has affect on Excel 2007-2010 export.")]
        public bool ExcelPageBreaks
        {
            get
            {
                object value = this.ViewState["ExcelPageBreaks"];
                return value is bool ? (bool)value : false;
            }
            set
            {
                this.ViewState["ExcelPageBreaks"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that only data will be exported into file. The property has affect on Excel and Excel 2007-2010 exports.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(false)]
        [Description("Gets or sets a value which indicates that only data will be exported into file. The property has affect on Excel and Excel 2007-2010 exports.")]
        public bool ExportDataOnly
        {
            get
            {
                object value = this.ViewState["ExportDataOnly"];
                return value is bool ? (bool)value : false;
            }
            set
            {
                this.ViewState["ExportDataOnly"] = value;
            }
        }


        /// <summary>
        /// Gets or sets an image quality value which is used in the export of the reports from the mobile viewer. The default value is 1.
        /// The greater the value the better quality is.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(0.75f)]
        [Description("Gets or sets an image quality value which is used in the export of the reports from the mobile viewer. The default value is 0.75. The greater the value the better quality is.")]
        public float ImageQuality
        {
            get
            {
                object imageQuality = this.ViewState["ImageQuality"];
                return imageQuality is float ? (float)imageQuality : 0.75f;
            }
            set
            {
                if (value > 0) this.ViewState["ImageQuality"] = value;
            }
        }


        /// <summary>
        /// Gets or sets an image resolution value in percent which is used in the export of the reports from the mobile viewer. The default value is 100.
        /// The greater the value the better quality is.
        /// </summary>
        [Category("Export Settings")]
        [DefaultValue(100)]
        [Description("Gets or sets an image resolution value in percent which is used in the export of the reports from the mobile viewer. The default value is 100. The greater the value the better quality is.")]
        public int ImageResolution
        {
            get
            {
                object imageResolution = this.ViewState["ImageResolution"];
                return imageResolution is int ? (int)imageResolution : 100;
            }
            set
            {
                if (value > 0) this.ViewState["ImageResolution"] = value;
            }
        }
        #endregion

        #region Properties.Toolbar

        /// <summary>
        /// Gets or sets a color of the toolbar texts.
        /// </summary>
        [Category("Toolbar")]
        [Description("Gets or sets a color of the toolbar texts.")]
        public Color ToolbarFontColor
        {
            get
            {
                object toolbarFontColor = this.ViewState["ToolbarFontColor"];
                return toolbarFontColor is Color ? (Color)toolbarFontColor : Color.White;
            }
            set
            {
                this.ViewState["ToolbarFontColor"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates which font family will be used for drawing texts in the viewer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue("Arial")]
        [Description("Gets or sets a value which indicates which font family will be used for drawing texts in the viewer.")]
        public string ToolbarFontFamily
        {
            get
            {
                object toolbarFontFamily = this.ViewState["ToolbarFontFamily"];
                return toolbarFontFamily is string ? (string)toolbarFontFamily : "Arial";
            }
            set
            {
                this.ViewState["ToolbarFontFamily"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which controls of output objects in the right to left mode.
        /// </summary>
        [DefaultValue(false)]
        [Category("Toolbar")]
        [Description("Gets or sets a value which controls of output objects in the right to left mode.")]
        public bool RightToLeft
        {
            get
            {
                object rightToLeft = this.ViewState["RightToLeft"];
                return rightToLeft is bool ? (bool)rightToLeft : false;
            }
            set
            {
                this.ViewState["RightToLeft"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the alignment of the mobile viewer toolbar.
        /// </summary>
        [DefaultValue(StiContentAlignment.Default)]
        [Category("Toolbar")]
        [Description("Gets or sets the alignment of the mobile viewer toolbar.")]
        public StiContentAlignment ToolbarAlignment
        {
            get
            {
                object toolbarAlignment = this.ViewState["ToolbarAlignment"];
                return toolbarAlignment is StiContentAlignment ? (StiContentAlignment)toolbarAlignment : StiContentAlignment.Default;
            }
            set
            {
                this.ViewState["ToolbarAlignment"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a visibility of the current page control in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the current page control in the toolbar of the mobile viewer.")]
        public bool ShowCurrentPage
        {
            get
            {
                object showCurrentPage = this.ViewState["ShowCurrentPage"];
                return showCurrentPage is bool ? (bool)showCurrentPage : true;
            }
            set
            {
                this.ViewState["ShowCurrentPage"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a visibility of the zoom control in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the zoom control in the toolbar of the mobile viewer.")]
        public bool ShowZoom
        {
            get
            {
                object showZoom = this.ViewState["ShowZoom"];
                return showZoom is bool ? (bool)showZoom : true;
            }
            set
            {
                this.ViewState["ShowZoom"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a visibility of the Save button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the Save button in the toolbar of the mobile viewer.")]
        public bool ShowSave
        {
            get
            {
                object showSave = this.ViewState["ShowSave"];
                return showSave is bool ? (bool)showSave : true;
            }
            set
            {
                this.ViewState["ShowSave"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a visibility of the ViewMode button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the ViewMode button in the toolbar of the mobile viewer.")]
        public bool ShowViewMode
        {
            get
            {
                object showViewMode = this.ViewState["ShowViewMode"];
                return showViewMode is bool ? (bool)showViewMode : true;
            }
            set
            {
                this.ViewState["ShowViewMode"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a visibility of the Parameters button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the Parameters button in the toolbar of the mobile viewer.")]
        public bool ShowParametersButton
        {
            get
            {
                object showParametersButton = this.ViewState["ShowParametersButton"];
                return showParametersButton is bool ? (bool)showParametersButton : true;
            }
            set
            {
                this.ViewState["ShowParametersButton"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a visibility of the Find button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the Find button in the toolbar of the mobile viewer.")]
        public bool ShowFindButton
        {
            get
            {
                object showFindButton = this.ViewState["ShowFindButton"];
                return showFindButton is bool ? (bool)showFindButton : true;
            }
            set
            {
                this.ViewState["ShowFindButton"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a visibility of the Editor button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the Editor button in the toolbar of the mobile viewer.")]
        public bool ShowEditorButton
        {
            get
            {
                object showEditorButton = this.ViewState["ShowEditorButton"];
                return showEditorButton is bool ? (bool)showEditorButton : true;
            }
            set
            {
                this.ViewState["ShowEditorButton"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a visibility of the Bookmarks button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the Bookmarks button in the toolbar of the mobile viewer.")]
        public bool ShowBookmarksButton
        {
            get
            {
                object showBookmarksButton = this.ViewState["ShowBookmarksButton"];
                return showBookmarksButton is bool ? (bool)showBookmarksButton : true;
            }
            set
            {
                this.ViewState["ShowBookmarksButton"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a visibility of the About button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the About button in the toolbar of the mobile viewer.")]
        public bool ShowAboutButton
        {
            get
            {
                object showAboutButton = this.ViewState["ShowAboutButton"];
                return showAboutButton is bool ? (bool)showAboutButton : true;
            }
            set
            {
                this.ViewState["ShowAboutButton"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a visibility of the Print button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the Print button in the toolbar of the mobile viewer.")]
        public bool ShowPrintButton
        {
            get
            {
                object showPrintButton = this.ViewState["ShowPrintButton"];
                return showPrintButton is bool ? (bool)showPrintButton : true;
            }
            set
            {
                this.ViewState["ShowPrintButton"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a visibility of the First button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the First button in the toolbar of the mobile viewer.")]
        public bool ShowFirstButton
        {
            get
            {
                object showFirstButton = this.ViewState["ShowFirstButton"];
                return showFirstButton is bool ? (bool)showFirstButton : true;
            }
            set
            {
                this.ViewState["ShowFirstButton"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a visibility of the Previous button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the Previous button in the toolbar of the mobile viewer.")]
        public bool ShowPrevButton
        {
            get
            {
                object showPrevButton = this.ViewState["ShowPrevButton"];
                return showPrevButton is bool ? (bool)showPrevButton : true;
            }
            set
            {
                this.ViewState["ShowPrevButton"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a visibility of the Next button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the Next button in the toolbar of the mobile viewer.")]
        public bool ShowNextButton
        {
            get
            {
                object showNextButton = this.ViewState["ShowNextButton"];
                return showNextButton is bool ? (bool)showNextButton : true;
            }
            set
            {
                this.ViewState["ShowNextButton"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a visibility of the Last button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the Last button in the toolbar of the mobile viewer.")]
        public bool ShowLastButton
        {
            get
            {
                object showLastButton = this.ViewState["ShowLastButton"];
                return showLastButton is bool ? (bool)showLastButton : true;
            }
            set
            {
                this.ViewState["ShowLastButton"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a visibility of the ToolBar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the ToolBar of the mobile viewer.")]
        public bool ShowToolBar
        {
            get
            {
                object showToolBar = this.ViewState["ShowToolBar"];
                return showToolBar is bool ? (bool)showToolBar : true;
            }
            set
            {
                this.ViewState["ShowToolBar"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a visibility of the Design button in the toolbar of the mobile viewer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the Design button in the toolbar of the mobile viewer.")]

        public bool ShowDesignButton
        {
            get
            {
                object showDesignButton = this.ViewState["ShowDesignButton"];
                return showDesignButton is bool ? (bool)showDesignButton : true;
            }
            set
            {
                this.ViewState["ShowDesignButton"] = value;
            }
        }
        #endregion

        #region Properties.Export
        /// <summary>
        /// Gets or sets a value which allows to display the export dialog, or to export with the default settings.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which allows to display the export dialog, or to export with the default settings.")]
        public bool ShowExportDialog
        {
            get
            {
                object showExportDialog = this.ViewState["ShowExportDialog"];
                return showExportDialog is bool ? (bool)showExportDialog : true;
            }
            set
            {
                this.ViewState["ShowExportDialog"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the report document file.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the report document file.")]
        public bool ShowExportToDocument
        {
            get
            {
                object showExportToDocument = this.ViewState["ShowExportToDocument"];
                return showExportToDocument is bool ? (bool)showExportToDocument : true;
            }
            set
            {
                this.ViewState["ShowExportToDocument"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the PDF format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the PDF format.")]
        public bool ShowExportToPdf
        {
            get
            {
                object showExportToPdf = this.ViewState["ShowExportToPdf"];
                return showExportToPdf is bool ? (bool)showExportToPdf : true;
            }
            set
            {
                this.ViewState["ShowExportToPdf"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the XPS format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the XPS format.")]
        public bool ShowExportToXps
        {
            get
            {
                object showExportToXps = this.ViewState["ShowExportToXps"];
                return showExportToXps is bool ? (bool)showExportToXps : true;
            }
            set
            {
                this.ViewState["ShowExportToXps"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Power Point 2007-2010 format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Power Point 2007-2010 format.")]
        public bool ShowExportToPowerPoint
        {
            get
            {
                object showExportToPowerPoint = this.ViewState["ShowExportToPowerPoint"];
                return showExportToPowerPoint is bool ? (bool)showExportToPowerPoint : true;
            }
            set
            {
                this.ViewState["ShowExportToPowerPoint"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the HTML format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the HTML format.")]
        public bool ShowExportToHtml
        {
            get
            {
                object showExportToHtml = this.ViewState["ShowExportToHtml"];
                return showExportToHtml is bool ? (bool)showExportToHtml : true;
            }
            set
            {
                this.ViewState["ShowExportToHtml"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the HTML5 format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the HTML5 format.")]
        public bool ShowExportToHtml5
        {
            get
            {
                object showExportToHtml5 = this.ViewState["ShowExportToHtml5"];
                return showExportToHtml5 is bool ? (bool)showExportToHtml5 : true;
            }
            set
            {
                this.ViewState["ShowExportToHtml5"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the MHT (Web Archive) format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the MHT (Web Archive) format.")]
        public bool ShowExportToMht
        {
            get
            {
                object showExportToMht = this.ViewState["ShowExportToMht"];
                return showExportToMht is bool ? (bool)showExportToMht : true;
            }
            set
            {
                this.ViewState["ShowExportToMht"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the TEXT format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the TEXT format.")]
        public bool ShowExportToText
        {
            get
            {
                object showExportToText = this.ViewState["ShowExportToText"];
                return showExportToText is bool ? (bool)showExportToText : true;
            }
            set
            {
                this.ViewState["ShowExportToText"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the RTF format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the RTF format.")]
        public bool ShowExportToRtf
        {
            get
            {
                object showExportToRtf = this.ViewState["ShowExportToRtf"];
                return showExportToRtf is bool ? (bool)showExportToRtf : true;
            }
            set
            {
                this.ViewState["ShowExportToRtf"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Word 2007-2010 format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Word 2007-2010 format.")]
        public bool ShowExportToWord2007
        {
            get
            {
                object showExportToWord2007 = this.ViewState["ShowExportToWord2007"];
                return showExportToWord2007 is bool ? (bool)showExportToWord2007 : true;
            }
            set
            {
                this.ViewState["ShowExportToWord2007"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Excel BIFF format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Excel BIFF format.")]
        public bool ShowExportToExcel
        {
            get
            {
                object showExportToExcel = this.ViewState["ShowExportToExcel"];
                return showExportToExcel is bool ? (bool)showExportToExcel : true;
            }
            set
            {
                this.ViewState["ShowExportToExcel"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the ExcelXML format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the ExcelXML format.")]
        public bool ShowExportToExcelXml
        {
            get
            {
                object showExportToExcelXml = this.ViewState["ShowExportToExcelXml"];
                return showExportToExcelXml is bool ? (bool)showExportToExcelXml : true;
            }
            set
            {
                this.ViewState["ShowExportToExcelXml"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Excel2007-2010 format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Excel2007-2010 format.")]
        public bool ShowExportToExcel2007
        {
            get
            {
                object showExportToExcel2007 = this.ViewState["ShowExportToExcel2007"];
                return showExportToExcel2007 is bool ? (bool)showExportToExcel2007 : true;
            }
            set
            {
                this.ViewState["ShowExportToExcel2007"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the CSV format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the CSV format.")]
        public bool ShowExportToCsv
        {
            get
            {
                object showExportToCsv = this.ViewState["ShowExportToCsv"];
                return showExportToCsv is bool ? (bool)showExportToCsv : true;
            }
            set
            {
                this.ViewState["ShowExportToCsv"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the DBF format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the DBF format.")]
        public bool ShowExportToDbf
        {
            get
            {
                object showExportToDbf = this.ViewState["ShowExportToDbf"];
                return showExportToDbf is bool ? (bool)showExportToDbf : true;
            }
            set
            {
                this.ViewState["ShowExportToDbf"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the XML format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the XML format.")]
        public bool ShowExportToXml
        {
            get
            {
                object showExportToXml = this.ViewState["ShowExportToXml"];
                return showExportToXml is bool ? (bool)showExportToXml : true;
            }
            set
            {
                this.ViewState["ShowExportToXml"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Open Document Calc format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Open Document Calc format.")]
        public bool ShowExportToOds
        {
            get
            {
                object showExportToOds = this.ViewState["ShowExportToOds"];
                return showExportToOds is bool ? (bool)showExportToOds : true;
            }
            set
            {
                this.ViewState["ShowExportToOds"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Open Document Text format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Open Document Text format.")]
        public bool ShowExportToOdt
        {
            get
            {
                object showExportToOdt = this.ViewState["ShowExportToOdt"];
                return showExportToOdt is bool ? (bool)showExportToOdt : true;
            }
            set
            {
                this.ViewState["ShowExportToOdt"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the DIF format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the DIF format.")]
        public bool ShowExportToDif
        {
            get
            {
                object showExportToDif = this.ViewState["ShowExportToDif"];
                return showExportToDif is bool ? (bool)showExportToDif : true;
            }
            set
            {
                this.ViewState["ShowExportToDif"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Sylk format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Sylk format.")]
        public bool ShowExportToSylk
        {
            get
            {
                object showExportToSylk = this.ViewState["ShowExportToSylk"];
                return showExportToSylk is bool ? (bool)showExportToSylk : true;
            }
            set
            {
                this.ViewState["ShowExportToSylk"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the BMP image format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the BMP image format.")]
        public bool ShowExportToBmp
        {
            get
            {
                object showExportToBmp = this.ViewState["ShowExportToBmp"];
                return showExportToBmp is bool ? (bool)showExportToBmp : true;
            }
            set
            {
                this.ViewState["ShowExportToBmp"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the GIF image format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the GIF image format.")]
        public bool ShowExportToGif
        {
            get
            {
                object showExportToGif = this.ViewState["ShowExportToGif"];
                return showExportToGif is bool ? (bool)showExportToGif : true;
            }
            set
            {
                this.ViewState["ShowExportToGif"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the JPEG image format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the JPEG image format.")]
        public bool ShowExportToJpeg
        {
            get
            {
                object showExportToJpeg = this.ViewState["ShowExportToJpeg"];
                return showExportToJpeg is bool ? (bool)showExportToJpeg : true;
            }
            set
            {
                this.ViewState["ShowExportToJpeg"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the PCX image format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the PCX image format.")]
        public bool ShowExportToPcx
        {
            get
            {
                object showExportToPcx = this.ViewState["ShowExportToPcx"];
                return showExportToPcx is bool ? (bool)showExportToPcx : true;
            }
            set
            {
                this.ViewState["ShowExportToPcx"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the PNG image format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the PNG image format.")]
        public bool ShowExportToPng
        {
            get
            {
                object showExportToPng = this.ViewState["ShowExportToPng"];
                return showExportToPng is bool ? (bool)showExportToPng : true;
            }
            set
            {
                this.ViewState["ShowExportToPng"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the TIFF image format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the TIFF image format.")]
        public bool ShowExportToTiff
        {
            get
            {
                object showExportToTiff = this.ViewState["ShowExportToTiff"];
                return showExportToTiff is bool ? (bool)showExportToTiff : true;
            }
            set
            {
                this.ViewState["ShowExportToTiff"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Metafile image format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the Metafile image format.")]
        public bool ShowExportToMetafile
        {
            get
            {
                object showExportToMetafile = this.ViewState["ShowExportToMetafile"];
                return showExportToMetafile is bool ? (bool)showExportToMetafile : true;
            }
            set
            {
                this.ViewState["ShowExportToMetafile"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the SVG image format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the SVG image format.")]
        public bool ShowExportToSvg
        {
            get
            {
                object showExportToSvg = this.ViewState["ShowExportToSvg"];
                return showExportToSvg is bool ? (bool)showExportToSvg : true;
            }
            set
            {
                this.ViewState["ShowExportToSvg"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the mobile viewer to the SVGZ image format.
        /// </summary>
        [DefaultValue(true)]
        [Category("Export")]
        [Description("Gets or sets a value which indicates that the user can save the report from the mobile viewer to the SVGZ image format.")]
        public bool ShowExportToSvgz
        {
            get
            {
                object showExportToSvgz = this.ViewState["ShowExportToSvgz"];
                return showExportToSvgz is bool ? (bool)showExportToSvgz : true;
            }
            set
            {
                this.ViewState["ShowExportToSvgz"] = value;
            }
        }


        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool ExportResponse
        {
            get
            {
                object exportResponse = this.ViewState["ExportResponse"];
                return exportResponse is bool ? (bool)exportResponse : true;
            }
            set
            {
                this.ViewState["ExportResponse"] = value;
            }
        }
        #endregion

        #region Events
        #region ReportConnect
        /// <summary>
        /// [8:26:01] stimulsoft: Occurs before creation of the web form in the report. This event is used to connect the report to the specified data.
        /// </summary>
        [Category("WebForms")]
        [Description("Occurs before creation of the web form in the report. This event is used to connect the report to the specified data.")]
        public event StiReportDataEventHandler ReportConnect;

        private void InvokeReportConnect(StiReport report)
        {
            OnReportConnect(new StiReportDataEventArgs(report));
            report.Dictionary.Connect();
            report.Dictionary.ConnectVirtualDataSources();
        }

        protected virtual void OnReportConnect(StiReportDataEventArgs e)
        {
            if (ReportConnect != null) ReportConnect(this, e);
        }
        #endregion

        #region ReportDisconnect
        /// <summary>
        /// Occurs after running of the web form. This event is used for disconnecting data from report.
        /// </summary>
        [Category("WebForms")]
        [Description("Occurs after running of the web form. This event is used for disconnecting data from the report.")]
        public event StiReportDataEventHandler ReportDisconnect;

        private void InvokeReportDisconnect(StiReport report)
        {
            report.Dictionary.Disconnect();
            OnReportDisconnect(new StiReportDataEventArgs(report));
            report.Dictionary.DataStore.Clear();
        }

        protected virtual void OnReportDisconnect(StiReportDataEventArgs e)
        {
            if (ReportDisconnect != null) ReportDisconnect(this, e);
        }
        #endregion

        #region ReportDialogResult
        /// <summary>
        /// Occurs when a user clicks a button with the specified dialog result on the web form.
        /// </summary>
        [Category("WebForms")]
        [Description("Occurs when a user clicks a button with the specified dialog result on the web form.")]
        public event StiReportDialogResultEventHandler ReportDialogResult;

        private void InvokeReportDialogResult(System.Windows.Forms.DialogResult dialogResult)
        {
            OnReportDialogResult(new StiReportDialogResultEventArgs(dialogResult));
        }

        protected virtual void OnReportDialogResult(StiReportDialogResultEventArgs e)
        {
            if (ReportDialogResult != null) ReportDialogResult(this, e);
        }
        #endregion

        #region ReportAfterDialogResult
        /// <summary>
        /// Occurs when a user clicks the button with the specified dialog result on the web form, but after invoking the ReportConnect event.
        /// </summary>
        [Category("WebForms")]
        [Description("Occurs when a user clicks the button with the specified dialog result on the web form, but after invoking the ReportConnect event.")]
        public event StiReportDialogResultEventHandler ReportAfterDialogResult;

        private void InvokeReportAfterDialogResult(System.Windows.Forms.DialogResult dialogResult)
        {
            OnReportAfterDialogResult(new StiReportDialogResultEventArgs(dialogResult));
        }

        protected virtual void OnReportAfterDialogResult(StiReportDialogResultEventArgs e)
        {
            if (ReportAfterDialogResult != null) ReportAfterDialogResult(this, e);
        }
        #endregion

        #region GetReportData
        /// <summary>
        /// Occurs for getting data for the report.
        /// </summary>
        [Category("WebForms")]
        [Description("Occurs for getting data for the report.")]
        public event StiReportDataEventHandler GetReportData;

        private void InvokeGetReportData(StiReport report)
        {
            OnGetReportData(new StiReportDataEventArgs(report));
        }

        protected virtual void OnGetReportData(StiReportDataEventArgs e)
        {
            if (GetReportData != null) GetReportData(this, e);
        }
        #endregion

        #region ReportExport
        /// <summary>
        /// Occurs when a user starts exporting the report from the mobile viewer.
        /// </summary>
        [Category("WebForms")]
        [Description("Occurs when a user starts exporting the report from the mobile viewer.")]
        public event StiExportDataEventHandler ReportExport;

        private void InvokeReportExport(StiReport report, StiExportSettings settings)
        {
            OnReportExport(new StiExportDataEventArgs(report, settings));
        }


        protected virtual void OnReportExport(StiExportDataEventArgs e)
        {
            if (ReportExport != null) ReportExport(this, e);
        }
        #endregion

        #region ReportDesign
        public delegate void StiReportDesignEventHandler(object sender, EventArgs e);

        /// <summary>
        /// Occurs when click on the design button.
        /// </summary>
        [Category("WebForms")]
        [Description("Occurs when click on the design button.")]
        public event StiReportDesignEventHandler ReportDesign;

        private void InvokeReportDesign()
        {
            OnReportDesign(new EventArgs());
        }

        protected virtual void OnReportDesign(EventArgs e)
        {
            if (ReportDesign != null) ReportDesign(this, e);
        }
        #endregion

        #region CustomCallBack
        public delegate void StiCustomCallBackEventHandler(object sender, StiCustomCallBackEventArgs e);

        /// <summary>
        /// Occurs when executing a custom ajax request.
        /// </summary>
        [Category("WebForms")]
        [Description("Occurs when executing a custom ajax request.")]
        public event StiCustomCallBackEventHandler CustomCallBack;

        private void InvokeCustomCallBack(Hashtable parameters)
        {
            OnCustomCallBack(new StiCustomCallBackEventArgs(this.Report, parameters));
        }

        protected virtual void OnCustomCallBack(StiCustomCallBackEventArgs e)
        {
            CustomCallBack(this, e);
        }
        #endregion

        #endregion

        #region Methods.Inner
        internal static bool IsDesignMode
        {
            get
            {
                return (HttpContext.Current == null);
            }
        }

        /// <summary>
        /// �������� url �� �������� � ������������ ������ url, � ������ �������� ������ ������������� ������
        /// </summary>
        internal string InsertSessionId(string url, string sessionId)
        {
            string[] segments = url.Split('/');
            string result = string.Empty;
            int index = 0;

            foreach (string segment in segments)
            {
                index++;

                if (index < segments.Length) result = string.Format("{0}{1}/", result, segment);
                else result = string.Format("{0}({1})/{2}", result, sessionId, segment);
            }

            return result;
        }

        internal string GetApplicationUrl()
        {
            return GetApplicationUrl(false, false);
        }

        internal string GetApplicationUrl(bool addQuerySeparator, bool ignoreRelativeUrls)
        {
            string url = this.Page.Request.Url.AbsoluteUri;
            if (this.UseRelativeUrls && !ignoreRelativeUrls) url = this.Page.Response.ApplyAppPathModifier(this.Page.Request.Url.AbsolutePath);

            if (addQuerySeparator)
            {
                string query = this.Page.Request.Url.Query;
                if (!string.IsNullOrEmpty(query)) url = string.Format("{0}{1}&", url, query);
                else url = string.Format("{0}?", url);
            }
            return url;
        }

        /// <summary>
        /// ���������� ���������� ���� � ������������
        /// </summary>
        internal string GetImageUrl()
        {
            return GetImageUrl(string.Empty, false);
        }

        /// <summary>
        /// ���������� ���������� ���� � ��������� ������ ��� ����������� ��� ����
        /// </summary>
        internal string GetImageUrl(string imageName, bool addQuotes)
        {
            string imageUrl = string.Empty;
            if (IsDesignMode)
            {
                imageUrl = Path.Combine(Environment.GetEnvironmentVariable("Temp"), "StiMobileViewer");
                if (!Directory.Exists(imageUrl)) Directory.CreateDirectory(imageUrl);
                if (!string.IsNullOrEmpty(imageName))
                {
                    imageUrl = Path.Combine(imageUrl, imageName);
                    try
                    {
                        if (!File.Exists(imageUrl))
                        {
                            Bitmap bmp = this.GetImage(imageName);
                            bmp.Save(imageUrl);
                            bmp.Dispose();
                        }
                    }
                    catch
                    {
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(this.ImagesPath))
                {
                    imageUrl = this.ImagesPath + imageName;

                    try
                    {
                        // ��������� ������������� �������� ���������� ��� �����������
                        string fileServerPath = HttpContext.Current.Server.MapPath(imageUrl);
                        if (string.IsNullOrEmpty(imageName))
                        {
                            if (!Directory.Exists(fileServerPath)) imageUrl = string.Empty;
                        }
                        else
                        {
                            if (!File.Exists(fileServerPath)) imageUrl = string.Empty;
                        }
                    }
                    catch
                    {
                    }
                }

                if (string.IsNullOrEmpty(imageUrl))
                {
                    imageUrl = GetApplicationUrl();
                    string query = this.Page.Request.Url.Query;
                    if (!string.IsNullOrEmpty(query)) imageUrl = string.Format("{0}{1}&stimulsoft_mobile_image={2}", imageUrl, query, imageName);
                    else imageUrl = string.Format("{0}?stimulsoft_mobile_image={1}", imageUrl, imageName);
                }
            }

            imageUrl = imageUrl.Replace("'", "\\'").Replace("\"", "&quot;");
            if (addQuotes) return string.Format("'{0}'", imageUrl);
            return imageUrl;
        }

        public string GetViewerStyles()
        {
            string themeName = this.Theme.ToString();
            Assembly a = typeof(StiMobileViewer).Assembly;
            var names = a.GetManifestResourceNames();
            var css = string.Empty;
            var path = string.Format("{0}.{1}.", STYLES_PATH, themeName.StartsWith("Office2013") ? "Office2013" : themeName.StartsWith("Material") ? "Material" : themeName);
            Hashtable constants = null;

            foreach (var name in names)
            {
                if (name.IndexOf(path) == 0 && name.EndsWith(".css"))
                {
                    Stream stream = a.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string cssText = reader.ReadToEnd();
                        if (name.EndsWith(themeName + ".Constants.css")) constants = GetCssConstants(cssText);
                        else if (!name.EndsWith(".Constants.css")) css += cssText + "\r\n";
                    }
                }
            }

            if (constants != null)
            {
                foreach (DictionaryEntry constant in constants)
                {
                    css = css.Replace((string)constant.Key, (string)constant.Value);
                }
            }

            return css;
        }

        public string GetViewerScripts()
        {
            Assembly a = typeof(StiMobileViewer).Assembly;
            var names = a.GetManifestResourceNames();
            var script = string.Empty;

            foreach (var name in names)
            {
                if (name.EndsWith(".js") && !name.EndsWith("jquery-1.8.3.min.js"))
                {
                    Stream stream = a.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        var scriptText = reader.ReadToEnd();

                        if (name.IndexOf("Main.js") != -1)
                        {
                            string[] collections = new string[] { "images", "loc", "fontNames", "encodingDataCollection", "defaultExportSettings", "dateRanges" };
                            for (int i = 0; i < collections.Length; i++)
                            {
                                object collectionObject = null;
                                switch (collections[i])
                                {
                                    case "images": { collectionObject = GetImagesArray(); break; }
                                    case "loc": { collectionObject = StiLocalization.GetLocalization(false); break; }
                                    case "fontNames": { collectionObject = StiFontNames.GetItems(); break; }
                                    case "encodingDataCollection": { collectionObject = StiEncodingDataCollection.GetItems(); break; }
                                    case "defaultExportSettings": { collectionObject = GetDefaultExportSettings(); break; }
                                    case "dateRanges": { collectionObject = StiDateRanges.GetItems(); break; }
                                }
                                string jsCollection = collections[i] == "loc"
                                    ? collectionObject as string
                                    : collectionObject != null ? JSON.Encode(collectionObject) : "null";
                                scriptText = scriptText.Replace("this.options." + collections[i] + " = {};", "this.options." + collections[i] + " = " + (jsCollection != null ? jsCollection : "{}") + ";");
                            }
                        }
                        script += scriptText + "\r\n";
                    }
                }
            }

            return script;
        }

        public bool ProcessScriptRequest()
        {
            var scriptName = GetRequestParam("mobileviewer_script");
            if (scriptName == null) return false;
            Assembly a = typeof(StiMobileViewer).Assembly;
            var names = a.GetManifestResourceNames();
            var script = string.Empty;

            this.GlobalizationFile = "Localization/ru.xml";

            CheckAutoSwitchToMobileTheme();
            foreach (string name in names)
            {
                if (name.EndsWith(".js"))
                {
                    if (name.EndsWith("jquery-1.8.3.min.js")) continue;
                    if (!CloudMode && (name.EndsWith("Controller.js") || name.EndsWith("CloudMethods.js") ||
                        name.EndsWith("InitializeColorControl.js") || name.EndsWith("InitializeColorDialog.js") ||
                        name.EndsWith("InitializeAuthForm.js") || name.EndsWith("InitializeCloudSaveMenu.js") ||
                        name.EndsWith("InitializeCloudSaveForm.js"))) continue;
                    Stream stream = a.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        var scriptText = reader.ReadToEnd();
                        if (name.EndsWith("Main.js"))
                        {
                            string[] collections = new string[] { "images", "loc", "fontNames", "encodingDataCollection", "defaultExportSettings", "dateRanges" };
                            for (int i = 0; i < collections.Length; i++)
                            {
                                object collectionObject = null;
                                switch (collections[i])
                                {
                                    case "images": { collectionObject = GetImagesArray(); break; }
                                    case "loc":
                                        {
                                            if (GetRequestParam("localizationName") != null && GetRequestParam("localizationName") != "en" &&
                                               GetRequestParam("sessionKey") != null || GetRequestParam("shareKey") != null)
                                            {
#if SERVER
                                                var getLocalizationCommand = new StiLocalizationCommands.Get()
                                                {
                                                    Name = GetRequestParam("localizationName") != null ? GetRequestParam("localizationName") : StiLocalization.CultureName,
                                                    SessionKey = GetRequestParam("sessionKey"),
                                                    Set = StiLocalizationSet.Server | StiLocalizationSet.Reports,
                                                    Type = StiLocalizationFormatType.Json
                                                };

                                                var resultLocalizationCommand = RunCommand(getLocalizationCommand) as StiLocalizationCommands.Get;
                                                if (resultLocalizationCommand.ResultSuccess &&
                                                    resultLocalizationCommand.ResultServerJson != null &&
                                                    resultLocalizationCommand.ResultReportsJson != null)
                                                {
                                                    var serverLoc = JSON.Decode(StiEncodingHelper.DecodeString(resultLocalizationCommand.ResultServerJson));
                                                    var reportsLoc = JSON.Decode(StiEncodingHelper.DecodeString(resultLocalizationCommand.ResultReportsJson));
                                                    ((Hashtable)((Hashtable)reportsLoc)["Localization"])["Notices"] = ((Hashtable)((Hashtable)serverLoc)["Localization"])["Notices"];
                                                    collectionObject = JSON.Encode(reportsLoc);
                                                }
#endif
                                            }
                                            else
                                            {
                                                collectionObject = StiLocalization.GetLocalization(false);
                                            }
                                            break;
                                        }
                                    case "fontNames": { collectionObject = StiFontNames.GetItems(); break; }
                                    case "dateRanges": { collectionObject = StiDateRanges.GetItems(); break; }
                                    case "encodingDataCollection": { collectionObject = StiEncodingDataCollection.GetItems(); break; }
                                    case "defaultExportSettings": { collectionObject = GetDefaultExportSettings(); break; }
                                }
                                string jsCollection = collections[i] == "loc"
                                    ? collectionObject as string
                                    : collectionObject != null ? JSON.Encode(collectionObject) : "null";
                                scriptText = scriptText.Replace("this.options." + collections[i] + " = {};", "this.options." + collections[i] + " = " + (jsCollection != null ? jsCollection : "{}") + ";");
                            }
                        }
                        script += scriptText + "\r\n";
                    }
                }
            }

            if (CloudMode || DemoMode)
            {
                Stream stream = a.GetManifestResourceStream(SCRIPTS_PATH + ".jquery-1.8.3.min.js");
                using (StreamReader reader = new StreamReader(stream))
                {
                    script = reader.ReadToEnd() + "\r\n" + script;
                }
            }

            ResponseString(script, "text/javascript");
            return true;
        }

        #endregion

        public StiMobileViewer()
        {
            GlobalizationFile = string.Empty;
            ImagesPath = string.Empty;

            UseCache = true;
            ShowExportToPdf = true;
            ShowExportToPowerPoint = true;
            ShowExportToHtml = true;
            ShowExportToMht = true;
            ShowExportToText = true;
            ShowExportToRtf = true;
            ShowExportToWord2007 = true;
            ShowExportToExcel = true;
            ShowExportToExcelXml = true;
            ShowExportToExcel2007 = true;
            ShowExportToCsv = true;
            ShowExportToDbf = true;
            ShowExportToXml = true;
            ShowExportToOds = true;
            ShowExportToOdt = true;

            ShowExportToBmp = true;
            ShowExportToGif = true;
            ShowExportToJpeg = true;
            ShowExportToPcx = true;
            ShowExportToPng = true;
            ShowExportToTiff = true;
            ShowExportToMetafile = true;

            ExportResponse = true;
            PageBorderColor = Color.Gray;
            PageBorderSize = 1;
            ShowPageShadow = true;

            WebReportStatus = StiWebReportStatus.FirstPass;

            ImageResolution = 100;
            ImageQuality = 0.75f;

            PdfImageCompressionMethod = StiPdfImageCompressionMethod.Jpeg;
            PdfEmbeddedFonts = true;
            PdfStandardFonts = false;
            PdfUseUnicode = true;
            PdfShowDialog = true;
            HtmlShowDialog = true;
            XpsShowDialog = true;

            ExcelUseOnePageHeaderAndFooter = false;
            ExcelPageBreaks = false;
            ExportDataOnly = false;

            RtfExportMode = StiRtfExportMode.Table;
            ImageFormat = ImageFormat.Png;

            HtmlQuality = StiHtmlExportQuality.High;
            HtmlExportMode = StiHtmlExportMode.Table;
            HtmlCharSet = "Unicode (UTF-8)";

            DbfCodePages = StiDbfCodePages.Default;

            CsvSeparator = string.Empty;
            CsvCharSet = "Unicode (UTF-8)";

            ToolBarBackColor = Color.Empty;
            ToolbarFontColor = Color.Empty;
            BackColor = Color.White;

            ScrollBarCssStyle = "";

            ShowToolBar = true;
            ShowParametersButton = true;
            ShowBookmarksButton = true;
            ShowPrintButton = true;
            ShowLastButton = true;
            ShowNextButton = true;
            ShowPrevButton = true;
            ShowFirstButton = true;
            ShowAboutButton = true;

            ShowPageBorders = true;

            ShowViewMode = true;
            ShowZoom = true;
            ShowSave = true;
            ShowCurrentPage = true;

            ContentAlignment = StiContentAlignment.Center;

            AllowBookmarks = true;
            BookmarksTreeWidth = 180;
            BookmarksTreeHeight = 0;

            OpenLinksInNewWindow = true;
            UseRelativeUrls = true;

            ReportScale = 1f;
            ServerTimeOut = new TimeSpan(0, 10, 0);
            ViewMode = StiWebViewMode.OnePage;

            PrintMode = StiPrintMode.WholeReport;
            AllowPrintBookmarks = true;

            webImageHost = new StiWebImageHost(this);
            CacheMode = StiCacheMode.Page;

            this.Height = Unit.Percentage(100);
            this.Width = Unit.Percentage(100);

            try
            {
                System.Reflection.PropertyInfo pi = this.GetType().GetProperty("ClientIDMode");
                if (pi != null) pi.SetValue(this, 1, null);
            }
            catch
            {
            }
        }

        static StiMobileViewer()
        {
            StiWebHelper.InitWeb();
        }
    }
}