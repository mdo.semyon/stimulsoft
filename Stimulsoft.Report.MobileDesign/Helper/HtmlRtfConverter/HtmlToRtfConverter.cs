﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Threading;

namespace Stimulsoft.Report.MobileDesign
{
    public class HtmlToRtfConverter
    {        
        #region Methods

        public string ConvertHtmlToRtf(string htmlText)
        {
            var thread = new Thread(ConvertRtfInSTAThread);
            var threadData = new ConvertHtmlThreadData { HtmlText = htmlText };
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start(threadData);
            thread.Join();
            return threadData.RtfText;
        }

        private void ConvertRtfInSTAThread(object html)
        {
            var threadData = html as ConvertHtmlThreadData;
            var xamlText = HtmlToXamlConverter.ConvertHtmlToXaml(threadData.HtmlText);
            threadData.RtfText = ConvertXamlToRtf(xamlText);
        }

        private class ConvertHtmlThreadData
        {
            public string RtfText { get; set; }
            public string HtmlText { get; set; }
        }

        //----------
        
        private static string ConvertXamlToRtf(string xamlText)
        {
            var richTextBox = new RichTextBox();
            if (string.IsNullOrEmpty(xamlText)) return "";

            var textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);

            using (var xamlMemoryStream = new MemoryStream())
            {
                using (var xamlStreamWriter = new StreamWriter(xamlMemoryStream))
                {
                    xamlStreamWriter.Write(xamlText);
                    xamlStreamWriter.Flush();
                    xamlMemoryStream.Seek(0, SeekOrigin.Begin);

                    textRange.Load(xamlMemoryStream, DataFormats.Xaml);
                }
            }

            using (var rtfMemoryStream = new MemoryStream())
            {

                textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
                textRange.Save(rtfMemoryStream, DataFormats.Rtf);
                rtfMemoryStream.Seek(0, SeekOrigin.Begin);
                using (var rtfStreamReader = new StreamReader(rtfMemoryStream))
                {
                    return rtfStreamReader.ReadToEnd();
                }
            }
        }
        #endregion
    }
}
