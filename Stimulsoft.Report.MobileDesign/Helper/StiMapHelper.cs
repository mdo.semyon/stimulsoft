﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Xml;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Maps;

namespace Stimulsoft.Report.MobileDesign
{
    internal class StiMapHelper
    {        
        public static Hashtable GetStyle(StiMap map)
        {
            Hashtable style = new Hashtable();
            style["type"] = string.IsNullOrEmpty(map.ComponentStyle) ? "StiMapStyleIdent" : "StiMapStyle";
            style["name"] = string.IsNullOrEmpty(map.ComponentStyle) ? map.MapStyle.ToString() : map.ComponentStyle;

            return style;
        }

        private static List<StiMapStyle> GetMapStyles(StiReport report)
        {
            var mapStyles = new List<StiMapStyle>();

            // Сначало добавляем стили из отчета, которые создавал пользователь
            foreach (Stimulsoft.Report.StiBaseStyle style in report.Styles)
            {
                if (style is Stimulsoft.Report.StiMapStyle)
                {
                    var customStyle = new StiMapStyle(style.Name);
                    mapStyles.Add(customStyle);
                }
            }

            mapStyles.AddRange(StiOptions.Services.MapStyles);

            return mapStyles;
        }

        public static string GetMapSampleImage(StiMap gauge, int width, int height, float zoom)
        {
            var svgData = new StiSvgData()
            {
                X = 0,
                Y = 0,
                Width = width,
                Height = height,
                Component = gauge
            };

            var sb = new StringBuilder();

            using (var ms = new StringWriter(sb))
            {
                var writer = new XmlTextWriter(ms);

                writer.WriteStartElement("svg");
                writer.WriteAttributeString("version", "1.1");
                writer.WriteAttributeString("baseProfile", "full");

                writer.WriteAttributeString("xmlns", "http://www.w3.org/2000/svg");
                writer.WriteAttributeString("xmlns:xlink", "http://www.w3.org/1999/xlink");
                writer.WriteAttributeString("xmlns:ev", "http://www.w3.org/2001/xml-events");

                writer.WriteAttributeString("height", svgData.Height.ToString());
                writer.WriteAttributeString("width", svgData.Width.ToString());

                //StiMapSvgHelper.drawMap(writer, svgData, zoom, false);

                writer.WriteFullEndElement();
                writer.Flush();
                ms.Flush();
                writer.Close();
                ms.Close();
            }

            return sb.ToString();
        }

        public static void SetMapStyle(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            string styleType = (string)param["styleType"];
            string styleName = (string)param["styleName"];

            if (component == null) return;
            StiMap map = component as StiMap;
            
            if (styleType == "StiMapStyle")
            {
                map.ComponentStyle = styleName;
            }
            else
            {
                map.ComponentStyle = string.Empty;
                map.MapStyle = (StiMapStyleIdent)Enum.Parse(typeof(StiMapStyleIdent), styleName);
            }
        }

        public static void GetStylesContent(StiReport report, Hashtable param, Hashtable callbackResult, bool forStylesControl)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            StiMap mapCloned = component.Clone() as StiMap;
            ArrayList stylesContent = new ArrayList();

            if (mapCloned != null)
            {
                foreach (var style in GetMapStyles(report))
                {
                    if (style is Maps.StiMapStyleFX)
                    {
                        mapCloned.MapStyle = ((Maps.StiMapStyleFX)style).StyleId;
                    }
                    else {

                        mapCloned.ComponentStyle = style.Name;
                    }
                    
                    int width = forStylesControl ? 138 : 80;

                    Hashtable content = new Hashtable();
                    content["image"] = GetMapSampleImage(mapCloned, width, forStylesControl ? 67 : 80, 1f);
                    content["type"] = style is Maps.StiMapStyleFX ? "StiMapStyleIdent" : "StiMapStyle";
                    content["name"] = style is Maps.StiMapStyleFX ? mapCloned.MapStyle.ToString() : mapCloned.ComponentStyle;
                    content["width"] = width;
                    content["height"] = forStylesControl ? 67 : 80;
                    stylesContent.Add(content);
                }
            }

            callbackResult["stylesContent"] = stylesContent;
        }
    }
}