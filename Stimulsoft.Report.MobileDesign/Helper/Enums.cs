﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;

namespace Stimulsoft.Report.MobileDesign
{

    #region StiShapeId
    internal enum StiShapeId
    {
        StiShapeTypeService = 0,
        StiSnipDiagonalSideCornerRectangleShapeType = 1,
        StiSnipSameSideCornerRectangleShapeType = 2,
        StiTrapezoidShapeType = 3,
        StiRegularPentagonShapeType = 4,
        StiPlusShapeType = 5,
        StiParallelogramShapeType = 6,
        StiMultiplyShapeType = 7,
        StiMinusShapeType = 8,
        StiFrameShapeType = 9,
        StiFlowchartSortShapeType = 10,
        StiFlowchartPreparationShapeType = 11,
        StiFlowchartOffPageConnectorShapeType = 12,
        StiFlowchartManualInputShapeType = 13,
        StiFlowchartDecisionShapeType = 14,
        StiFlowchartCollateShapeType = 15,
        StiFlowchartCardShapeType = 16,
        StiEqualShapeType = 17,
        StiDivisionShapeType = 18,
        StiChevronShapeType = 19,
        StiBentArrowShapeType = 20,
        StiComplexArrowShapeType = 21,
        StiVerticalLineShapeType = 22,
        StiTriangleShapeType = 23,
        StiTopAndBottomLineShapeType = 24,
        StiRoundedRectangleShapeType = 25,
        StiRectangleShapeType = 26,
        StiOvalShapeType = 27,
        StiLeftAndRightLineShapeType = 28,
        StiHorizontalLineShapeType = 29,
        StiDiagonalUpLineShapeType = 30,
        StiDiagonalDownLineShapeType = 31,
        StiArrowShapeType = 32,
    }
    #endregion

    #region BarCodeGeomId
    public enum BarCodeGeomId
    {
        BaseTransform,
        BaseFillRectangle,
        BaseFillRectangle2D,
        BaseDrawRectangle,
        BaseDrawString
    }
    #endregion

    #region StiRequestFromUserType
    public enum StiRequestFromUserType
    {
        ListBool,
        ListChar,
        ListDateTime,
        ListTimeSpan,
        ListDecimal,
        ListFloat,
        ListDouble,
        ListByte,
        ListShort,
        ListInt,
        ListLong,
        ListGuid,
        ListString,
        RangeChar,
        RangeDateTime,
        RangeDouble,
        RangeFloat,
        RangeDecimal,
        RangeGuid,
        RangeByte,
        RangeShort,
        RangeInt,
        RangeLong,
        RangeString,
        RangeTimeSpan,
        ValueBool,
        ValueChar,
        ValueDateTime,
        ValueFloat,
        ValueDouble,
        ValueDecimal,
        ValueGuid,
        ValueImage,
        ValueString,
        ValueTimeSpan,
        ValueShort,
        ValueInt,
        ValueLong,
        ValueSbyte,
        ValueUshort,
        ValueUint,
        ValueUlong,
        ValueByte,
        ValueNullableBool,
        ValueNullableChar,
        ValueNullableDateTime,
        ValueNullableFloat,
        ValueNullableDouble,
        ValueNullableDecimal,
        ValueNullableGuid,
        ValueNullableTimeSpan,
        ValueNullableShort,
        ValueNullableInt,
        ValueNullableLong,
        ValueNullableSbyte,
        ValueNullableUshort,
        ValueNullableUint,
        ValueNullableUlong,
        ValueNullableByte
    }
    #endregion    
}