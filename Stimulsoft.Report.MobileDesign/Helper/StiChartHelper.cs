﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Xml;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Chart;
using Stimulsoft.Base.Drawing;
using System.IO;
using System.Linq;
using Stimulsoft.Base;
using Stimulsoft.Report.Export;

namespace Stimulsoft.Report.MobileDesign
{
    internal class StiChartHelper
    {
        #region Properties
        public static Hashtable GetChartProperties(StiChart chart)
        {
            Hashtable properties = new Hashtable();
            properties["name"] = chart.Name;
            properties["series"] = GetSeriesArray(chart);
            properties["chartImage"] = GetChartSampleImage(chart, 400, 400, 1f);
            properties["typesCollection"] = GetTypesCollection(chart);
            properties["area"] = GetArea(chart);
            properties["style"] = GetStyle(chart);
            properties["labels"] = GetLabels(chart.SeriesLabels);
            properties["properties"] = GetMainProperties(chart);
            properties["constantLines"] = GetConstantLines(chart);
            properties["strips"] = GetStrips(chart);
            properties["conditions"] = GetConditions(chart.SeriesLabelsConditions);
            
            return properties;
        }        
        
        public static ArrayList GetSeriesArray(StiChart chart)
        {
            ArrayList seriesArray = new ArrayList();
            foreach (StiSeries series in chart.Series)
            {
                seriesArray.Add(GetSeries(series));
            }
            return seriesArray;
        }
        
        public static Hashtable GetSeries(StiSeries series)
        {
            Hashtable seriesObject = new Hashtable();
            seriesObject["name"] = series.Title + " [" + series.ToString() + "]";
            seriesObject["type"] = series.GetType().Name;
            seriesObject["properties"] = GetSeriesProperties(series);
            seriesObject["labels"] = GetLabels(series.SeriesLabels);
            seriesObject["conditions"] = GetConditions(series.Conditions);
            seriesObject["filters"] = GetFilters(series.Filters);
            seriesObject["filterMode"] = series.FilterMode;

            return seriesObject;
        }

        public static ArrayList GetConditions(StiChartConditionsCollection conditions)
        {
            ArrayList conditionsArray = new ArrayList();
            
            foreach (StiChartCondition condition in conditions)
            {
                Hashtable propertiesCondition = new Hashtable();
                propertiesCondition["FieldIs"] = condition.Item;
                propertiesCondition["Value"] = StiEncodingHelper.Encode(condition.Value);
                propertiesCondition["Color"] = StiReportEdit.GetStringFromColor(condition.Color);
                propertiesCondition["DataType"] = condition.DataType;
                propertiesCondition["Condition"] = condition.Condition;

                conditionsArray.Add(propertiesCondition);
            }

            return conditionsArray;
        }

        public static ArrayList GetFilters(StiChartFiltersCollection filters)
        {
            ArrayList filtersArray = new ArrayList();

            foreach (StiChartFilter filter in filters)
            {
                Hashtable propertiesFilter = new Hashtable();
                propertiesFilter["FieldIs"] = filter.Item;
                propertiesFilter["DataType"] = filter.DataType;
                propertiesFilter["Value"] = StiEncodingHelper.Encode(filter.Value);
                propertiesFilter["Condition"] = filter.Condition;

                filtersArray.Add(propertiesFilter);
            }

            return filtersArray;
        }

        public static ArrayList GetTypesCollection(StiChart chart)
        {
            ArrayList typesCollection = new ArrayList();
            var types = (chart.Area != null)
                ? chart.Area.GetSeriesTypes()
                : new Type[0];

            foreach (Type type in types)
            {
                typesCollection.Add(type.Name);
            }
            return typesCollection;
        }

        public static Hashtable GetArea(StiChart chart)
        {
            Hashtable area = new Hashtable();
            if (chart.Area != null)
            {
                area["type"] = chart.Area.GetType().Name;
                area["properties"] = GetAreaProperties(chart.Area as StiArea);
            }

            return area;
        }

        public static Hashtable GetStyle(StiChart chart)
        {
            Hashtable style = new Hashtable();
            style["type"] = chart.Style.GetType().Name;
            style["name"] = (chart.Style is StiCustomStyle && ((StiCustomStyleCoreXF)chart.Style.Core).ReportStyle != null)
                ? ((StiCustomStyleCoreXF)chart.Style.Core).ReportStyle.Name : "";

            return style;
        }

        public static Hashtable GetMainProperties(StiChart chart)
        {            
            Hashtable properties = new Hashtable();
            #region Common
            Hashtable propertiesCommon = new Hashtable();
            string[] propNamesCommon = { "AllowApplyStyle", "ProcessAtEnd", "Rotation", "HorSpacing", "VertSpacing", "DataSource", "BusinessObject", "DataRelation",
                "MasterComponent", "CountData", "Filters", "Sort"};
            foreach (string propName in propNamesCommon)
            {
                
                var value = StiReportEdit.GetPropertyValue(propName, chart);
                if (value != null) { propertiesCommon[propName] = value; }
            }

            properties["Common"] = propertiesCommon;
            #endregion
                        
            #region Legend
            Hashtable propertiesLegend = new Hashtable();
            string[] propNamesLegend = { "AllowApplyStyle", "BorderColor", "Brush", "Columns", "Direction", "Font", "HideSeriesWithEmptyTitle", "HorAlignment",
                "HorSpacing", "LabelsColor", "MarkerAlignment", "MarkerBorder", "MarkerSize", "MarkerVisible", "ShowShadow", "Title", "TitleColor", "TitleFont", 
                "VertAlignment", "VertSpacing", "Visible" };
            foreach (string propName in propNamesLegend)
            {
                var value = StiReportEdit.GetPropertyValue(propName, chart.Legend);
                if (value != null) { propertiesLegend[propName] = value; }
            }

            properties["Legend"] = propertiesLegend;
            #endregion
            
            #region Title
            Hashtable propertiesTitle = new Hashtable();
            string[] propNamesTitle = { "Alignment", "AllowApplyStyle", "Antialiasing", "Brush", "Dock", "Font", "Spacing", "Text", "Visible" };
            foreach (string propName in propNamesTitle)
            {
                var value = StiReportEdit.GetPropertyValue(propName, chart.Title);
                if (value != null) { propertiesTitle[propName] = value; }
            }

            properties["Title"] = propertiesTitle;
            #endregion

            #region Table
            Hashtable propertiesTable = new Hashtable();
            string[] propNamesTable = { "AllowApplyStyle", "Font", "GridLineColor", "GridLinesHor", "GridLinesVert", "GridOutline", "MarkerVisible", "Visible",
                "Header.TextAfter", "Header.TextColor", "Header.WordWrap", "Header.Brush", "Header.Font", "DataCells.TextColor",
                "DataCells.ShrinkFontToFit", "DataCells.ShrinkFontToFitMinimumSize", "DataCells.Font" };
            foreach (string propName in propNamesTable)
            {
                var value = StiReportEdit.GetPropertyValue(propName, chart.Table);
                if (value != null) { propertiesTable[propName] = value; }
            }

            properties["Table"] = propertiesTable;
            #endregion

            return properties;
        }

        public static ArrayList GetConstantLines(StiChart chart)
        {
            ArrayList constantLinesArray = new ArrayList();
            foreach (StiConstantLines constantLine in chart.ConstantLines)
            {
                Hashtable constantLineObj = new Hashtable();
                constantLineObj["name"] = constantLine.Text;
                constantLineObj["properties"] = GetConstantLineProperties(constantLine);
                constantLinesArray.Add(constantLineObj);
            }
            return constantLinesArray;
        }

        public static Hashtable GetConstantLineProperties(StiConstantLines constantLine)
        {
            Hashtable properties = new Hashtable();
            
            string[] propNames = { "AxisValue", "LineColor", "LineStyle", "LineWidth", "Orientation", "ShowBehind", "Visible", "AllowApplyStyle", "Antialiasing",
                "Font", "Position", "Text", "TitleVisible" };
            foreach (string propName in propNames)
            {
                var value = StiReportEdit.GetPropertyValue(propName, constantLine);
                if (value != null) { properties[propName] = value; }
            }
                        
            return properties;
        }

        public static ArrayList GetStrips(StiChart chart)
        {
            ArrayList stripsArray = new ArrayList();
            foreach (StiStrips strip in chart.Strips)
            {
                Hashtable stripObj = new Hashtable();
                stripObj["name"] = strip.Text;
                stripObj["properties"] = GetStripsProperties(strip);
                stripsArray.Add(stripObj);
            }
            return stripsArray;
        }

        public static Hashtable GetStripsProperties(StiStrips strip)
        {
            Hashtable properties = new Hashtable();

            string[] propNames = { "AllowApplyStyle", "MaxValue", "MinValue", "Orientation", "ShowBehind", "StripBrush", "Visible", "Antialiasing", "Font", "Text",
                "TitleColor", "TitleVisible" };
            foreach (string propName in propNames)
            {
                var value = StiReportEdit.GetPropertyValue(propName, strip);
                if (value != null) { properties[propName] = value; }
            }

            return properties;
        }

        public static Hashtable GetLabels(IStiSeriesLabels seriesLabels)
        {
            Hashtable labels = new Hashtable();
            if (seriesLabels != null)
            {
                labels["type"] = seriesLabels.GetType().Name;
                labels["properties"] = GetLabelsProperties(seriesLabels as StiSeriesLabels);                
            }
            return labels;
        }

        public static Hashtable GetSeriesProperties(StiSeries series)
        {
            Hashtable properties = new Hashtable();

            #region Common
            Hashtable propertiesCommon = new Hashtable();
            string[] propNamesCommon = { "ValueDataColumn", "Value", "ListOfValues", "ValueDataColumnEnd", "ValueEnd", "ListOfValuesEnd", "ValueDataColumnClose", "ValueClose", "ListOfValuesClose",
                "ValueDataColumnHigh", "ValueHigh", "ListOfValuesHigh","ValueDataColumnLow", "ValueLow", "ListOfValuesLow", "ValueDataColumnOpen", "ValueOpen", "ListOfValuesOpen",
                "WeightDataColumn", "Weight", "ListOfWeights", "ArgumentDataColumn", "Argument", "ListOfArguments", "Format", "SortBy", "SortDirection", "AutoSeriesKeyDataColumn",
                "AutoSeriesColorDataColumn", "AutoSeriesTitleDataColumn", "BorderColor", "BorderColorNegative", "BorderWidth", "Brush", "BrushNegative", "ShowShadow", "AllowApplyStyle", "AllowApplyBrushNegative",
                "AllowApplyColorNegative", "ShowInLegend", "ShowSeriesLabels", "ShowZeros", "Title", "Width", "YAxis", "LabelsOffset", "Lighting", "LineColor", "LineColorNegative",
                "LineStyle", "LineWidth", "ShowNulls", "PointAtCenter", "Tension", "TopmostLine", "AllowApplyBorderColor", "AllowApplyBrush", "Diameter", "CutPieList", "StartAngle", "Distance" };
            foreach (string propName in propNamesCommon)
            {
                var value = StiReportEdit.GetPropertyValue(propName, series);
                if (value != null) { propertiesCommon[propName] = value; }
            }

            properties["Common"] = propertiesCommon;
            #endregion

            #region Marker && LineMarker
            string[] markerGroups = { "Marker", "LineMarker" };
            foreach (string markerGroup in markerGroups)
            {
                System.Reflection.PropertyInfo piMarkerGroup = series.GetType().GetProperty(markerGroup);
                if (piMarkerGroup != null/* && IsBrowsable(piMarkerGroup)*/)
                {
                    Hashtable propertiesMarker = new Hashtable();
                    string[] propNamesMarker = { "Angle", "BorderColor", "Brush", "ShowInLegend", "Size", "Step", "Type", "Visible" };
                    foreach (string propName in propNamesMarker)
                    {
                        var value = StiReportEdit.GetPropertyValue(propName, piMarkerGroup.GetValue(series, null));
                        if (value != null) { propertiesMarker[propName] = value; }
                    }

                    properties[markerGroup] = propertiesMarker;
                }
            }
            #endregion

            #region Interaction
            System.Reflection.PropertyInfo piInteraction = series.GetType().GetProperty("Interaction");
            if (piInteraction != null/* && IsBrowsable(piInteraction)*/)
            {
                Hashtable propertiesInteraction = new Hashtable();
                string[] propNamesInteraction = { "AllowSeries", "AllowSeriesElements", "DrillDownEnabled", "DrillDownPage", "DrillDownReport", "HyperlinkDataColumn",
                    "TagDataColumn", "ToolTipDataColumn", "Hyperlink", "Tag", "ToolTip", "ListOfHyperlinks", "ListOfTags", "ListOfToolTips" };
                foreach (string propName in propNamesInteraction)
                {
                    var value = StiReportEdit.GetPropertyValue(propName, piInteraction.GetValue(series, null));
                    if (value != null) { propertiesInteraction[propName] = value; }
                }

                properties["Interaction"] = propertiesInteraction;
            }
            #endregion
            
            #region TrendLine
            System.Reflection.PropertyInfo piTrendLine = series.GetType().GetProperty("TrendLine");
            if (piTrendLine != null/* && IsBrowsable(piTrendLine)*/)
            {
                Hashtable propertiesTrendLine = new Hashtable();
                string[] propNamesTrendLine = { "LineColor", "LineStyle", "LineWidth", "ShowShadow" };
                foreach (string propName in propNamesTrendLine)
                {
                    var value = StiReportEdit.GetPropertyValue(propName, piTrendLine.GetValue(series, null));
                    if (value != null) { propertiesTrendLine[propName] = value; }
                }

                properties["TrendLine"] = propertiesTrendLine;
            }
            #endregion

            #region TopN
            System.Reflection.PropertyInfo piTopN = series.GetType().GetProperty("TopN");
            if (piTopN != null/* && IsBrowsable(piTopN)*/)
            {
                Hashtable propertiesTopN = new Hashtable();
                string[] propNamesTopN = { "Count", "Mode", "OthersText", "ShowOthers" };
                foreach (string propName in propNamesTopN)
                {
                    var value = StiReportEdit.GetPropertyValue(propName, piTopN.GetValue(series, null));
                    if (value != null) { propertiesTopN[propName] = value; }
                }

                properties["TopN"] = propertiesTopN;
            }
            #endregion

            return properties;
        }

        public static Hashtable GetAreaProperties(StiArea area)
        {
            Hashtable properties = new Hashtable();

            #region Common
            Hashtable propertiesCommon = new Hashtable();
            string[] propNamesCommon = { "Brush", "AllowApplyStyle", "BorderColor", "ColorEach", "RadarStyle", "ReverseHor", "ReverseVert", "ShowShadow" };
            foreach (string propName in propNamesCommon)
            {
                var value = StiReportEdit.GetPropertyValue(propName, area);
                if (value != null) { propertiesCommon[propName] = value; }
            }

            properties["Common"] = propertiesCommon;
            #endregion

            #region XAxis && YAxis && XTopAxis && YRightAxis
            string[] axisTypes = { "XAxis", "YAxis", "XTopAxis", "YRightAxis" };
            foreach (string axisType in axisTypes)
            {
                System.Reflection.PropertyInfo piAxis = area.GetType().GetProperty(axisType);
                if (piAxis != null/* && IsBrowsable(piAxis)*/)
                {
                    Hashtable propertiesAxis = new Hashtable();
                    string[] propAxisNames = { "AllowApplyStyle", "ArrowStyle", "DateTimeStep.Interpolation", "DateTimeStep.NumberOfValues", "DateTimeStep.Step",
                    "Interaction.RangeScrollEnabled", "Interaction.ShowScrollBar", "Labels.Brush", "Labels.AllowApplyStyle", "Labels.Angle", "Labels.Antialiasing", "Labels.Color",
                    "Labels.DrawBorder", "Labels.Font", "Labels.Format", "Labels.RotationLabels", "Labels.Placement", "Labels.Step", "Labels.TextAfter", "Labels.TextAlignment", "Labels.TextBefore",
                    "Labels.Width", "Labels.WordWrap", "LineColor", "LineStyle", "LineWidth", "LogarithmicScale", "Range.Auto", "Range.Minimum", "Range.Maximum", 
                    "ShowEdgeValues", "ShowXAxis", "StartFromZero", "Ticks.Length", "Ticks.LengthUnderLabels", "Ticks.MinorCount", "Ticks.MinorLength", "Ticks.MinorVisible",
                    "Ticks.Step", "Ticks.Visible", "Title.Alignment", "Title.AllowApplyStyle", "Title.Antialiasing", "Title.Color", "Title.Direction", "Title.Font",
                    "Title.Text", "Visible" };
                    foreach (string propName in propAxisNames)
                    {
                        var value = StiReportEdit.GetPropertyValue(propName, piAxis.GetValue(area, null));
                        if (value != null) { propertiesAxis[propName] = value; }
                    }

                    properties[axisType] = propertiesAxis;
                }
            }
            #endregion

            #region GridLinesHor && GridLinesHorRight && GridLinesVert
            string[] gridLinesTypes = { "GridLinesHor", "GridLinesHorRight", "GridLinesVert" };
            foreach (string gridLinesType in gridLinesTypes)
            {

                System.Reflection.PropertyInfo piGridLines = area.GetType().GetProperty(gridLinesType);
                if (piGridLines != null/* && IsBrowsable(piGridLines)*/)
                {
                    Hashtable propertiesGridLines = new Hashtable();
                    string[] propGridLinesNames = { "AllowApplyStyle", "Color", "MinorColor", "MinorCount", "MinorStyle", "MinorVisible", "Style", "Visible" };
                    foreach (string propName in propGridLinesNames)
                    {
                        var value = StiReportEdit.GetPropertyValue(propName, piGridLines.GetValue(area, null));
                        if (value != null) { propertiesGridLines[propName] = value; }
                    }

                    properties[gridLinesType] = propertiesGridLines;
                }
            }
            #endregion

            #region InterlacingHor && InterlacingVert
            string[] interlacingTypes = { "InterlacingHor", "InterlacingVert" };
            foreach (string interlacingType in interlacingTypes)
            {

                System.Reflection.PropertyInfo piInterlacing = area.GetType().GetProperty(interlacingType);
                if (piInterlacing != null/* && IsBrowsable(piInterlacing)*/)
                {
                    Hashtable propertiesInterlacing = new Hashtable();
                    string[] propInterlacingNames = { "AllowApplyStyle", "InterlacedBrush", "Visible" };
                    foreach (string propName in propInterlacingNames)
                    {
                        var value = StiReportEdit.GetPropertyValue(propName, piInterlacing.GetValue(area, null));
                        if (value != null) { propertiesInterlacing[propName] = value; }
                    }

                    properties[interlacingType] = propertiesInterlacing;
                }
            }
            #endregion

            return properties;
        }

        public static Hashtable GetLabelsProperties(StiSeriesLabels labels)
        {
            Hashtable properties = new Hashtable();
                        
            #region Common
            Hashtable propertiesCommon = new Hashtable();
            string[] propNamesCommon = { "AllowApplyStyle", "Angle", "Antialiasing", "DrawBorder", "BorderColor", "Brush", "Font", "Format", "LabelColor", "LegendValueType",
                "LineLength", "MarkerAlignment", "MarkerSize", "MarkerVisible", "PreventIntersection", "ShowInPercent", "ShowNulls", "ShowZeros", "Step", "TextAfter",
                "TextBefore", "UseSeriesColor", "ValueType", "ValueTypeSeparator", "Visible", "Width", "WordWrap" };
            foreach (string propName in propNamesCommon)
            {
                var value = StiReportEdit.GetPropertyValue(propName, labels);
                if (value != null) { propertiesCommon[propName] = value; }
            }

            properties["Common"] = propertiesCommon;
            #endregion

            return properties;
        }
        #endregion

        #region Helper Methods
       
        private static void SetConditionsValue(StiChartConditionsCollection conditions, ArrayList conditionValues)
        {
            conditions.Clear();
            foreach (Hashtable conditionValue in conditionValues) 
            {
                StiChartCondition condition = new StiChartCondition();
                if (conditionValue["FieldIs"] != null) condition.Item = (StiFilterItem)Enum.Parse(typeof(StiFilterItem), (string)conditionValue["FieldIs"]);
                if (conditionValue["DataType"] != null) condition.DataType = (StiFilterDataType)Enum.Parse(typeof(StiFilterDataType), (string)conditionValue["DataType"]);
                if (conditionValue["Condition"] != null) condition.Condition = (StiFilterCondition)Enum.Parse(typeof(StiFilterCondition), (string)conditionValue["Condition"]);
                if (conditionValue["Color"] != null) condition.Color = StiReportEdit.StrToColor((string)conditionValue["Color"]);
                if (conditionValue["Value"] != null) condition.Value = StiEncodingHelper.DecodeString((string)conditionValue["Value"]);
                
                conditions.Add(condition);
            }
        }

        private static void SetFiltersValue(StiChartFiltersCollection filters, ArrayList filterValues)
        {
            filters.Clear();
            foreach (Hashtable filterValue in filterValues)
            {
                StiChartFilter filter = new StiChartFilter();
                if (filterValue["FieldIs"] != null) filter.Item = (StiFilterItem)Enum.Parse(typeof(StiFilterItem), (string)filterValue["FieldIs"]);
                if (filterValue["DataType"] != null) filter.DataType = (StiFilterDataType)Enum.Parse(typeof(StiFilterDataType), (string)filterValue["DataType"]);
                if (filterValue["Condition"] != null) filter.Condition = (StiFilterCondition)Enum.Parse(typeof(StiFilterCondition), (string)filterValue["Condition"]);
                if (filterValue["Value"] != null) filter.Value = StiEncodingHelper.DecodeString((string)filterValue["Value"]);

                filters.Add(filter);
            }
        }

        private static bool IsBrowsable(PropertyInfo p)
        {
            bool res;
            res = true;
            foreach (object att_loopVariable in p.GetCustomAttributes(true))
            {
                if (att_loopVariable is System.ComponentModel.BrowsableAttribute)
                {
                    System.ComponentModel.BrowsableAttribute att;
                    att = (System.ComponentModel.BrowsableAttribute)att_loopVariable;
                    if (!(att as System.ComponentModel.BrowsableAttribute).Browsable)
                    {
                        res = false;
                    }
                }
            }
            return res;
        }

        public static string GetChartSampleImage(StiChart chart, int width, int height, float zoom)
        {
            var svgData = new StiSvgData()
            {
                X = 0,
                Y = 0,
                Width = width,
                Height = height,
                Component = chart
            };

            var sb = new StringBuilder();

            using (var ms = new StringWriter(sb))
            {
                var writer = new XmlTextWriter(ms);

                writer.WriteStartElement("svg");
                writer.WriteAttributeString("version", "1.1");
                writer.WriteAttributeString("baseProfile", "full");

                writer.WriteAttributeString("xmlns", "http://www.w3.org/2000/svg");
                writer.WriteAttributeString("xmlns:xlink", "http://www.w3.org/1999/xlink");
                writer.WriteAttributeString("xmlns:ev", "http://www.w3.org/2001/xml-events");

                writer.WriteAttributeString("height", svgData.Height.ToString());
                writer.WriteAttributeString("width", svgData.Width.ToString());

                StiChartSvgHelper.WriteChart(writer, svgData, zoom, false);

                writer.WriteFullEndElement();
                writer.Flush();
                ms.Flush();
                writer.Close();
                ms.Close();
            }

            return sb.ToString();
        }

        private static void AddDefaultSeries(StiChart chart)
        {
            Type series1Type = null;
            if (chart.Series.Count > 0)
                series1Type = chart.Series[0].GetType();
            else
                series1Type = chart.Area.GetDefaultSeriesType();
            var series1 = (StiSeries)StiActivator.CreateObject(series1Type);

            Type series2Type = null;
            if (chart.Series.Count > 1)
                series2Type = chart.Series[1].GetType();
            else
                series2Type = series1Type;
            var series2 = (StiSeries)StiActivator.CreateObject(series2Type);

            chart.Series.Clear();

            chart.Series.Add(series1);

            if (!(series2 is StiFunnelSeries))
                chart.Series.Add(series2);

            chart.Series.ApplyStyle(chart.Style);
        }

        private static StiChart CloneChart(StiChart chart)
        {
            var chartCloned = (StiChart)chart.Clone();

            chartCloned.Legend.Visible = false;
            chartCloned.SeriesLabels.Visible = false;
            chartCloned.Title.Visible = false;
            chartCloned.Table.Visible = false;

            var area = chartCloned.Area as StiAxisArea;
            if (area != null)
                area.AxisCore.SwitchOff();

            AddDefaultSeries(chartCloned);
            
            chartCloned.ConstantLines.Clear();
            chartCloned.Strips.Clear();
            chartCloned.Core.ApplyStyle(chartCloned.Style);

            chartCloned.Brush = new StiEmptyBrush();
            chartCloned.Area.Brush = new StiEmptyBrush();

            return chartCloned;
        }

        private static void AddUserChartStyles(StiReport report, List<Stimulsoft.Report.Chart.StiChartStyle> chartStyles)
        {
            foreach (Stimulsoft.Report.StiBaseStyle style in report.Styles)
            {
                if (style is Stimulsoft.Report.StiChartStyle)
                {
                    var customStyle = new StiCustomStyle(style.Name);
                    customStyle.CustomCore.ReportChartStyle = style as Stimulsoft.Report.StiChartStyle;
                    chartStyles.Add(customStyle);
                }
            }
        }

        private static List<Stimulsoft.Report.Chart.StiChartStyle> GetChartStyles(StiReport report)
        {
            var chartStyles = new List<Stimulsoft.Report.Chart.StiChartStyle>();
            
            AddUserChartStyles(report, chartStyles);
                                                
            // Затем берем все базовые стили
            foreach (var style in StiOptions.Services.ChartStyles.Where(s => s.ServiceEnabled))
            {
                chartStyles.Add(style);
            }

            chartStyles.Reverse();

            return chartStyles;
        }
        #endregion

        #region Callback Methods
        public static void AddSeries(StiReport report, Hashtable param, Hashtable callbackResult)
        {            
            var component = report.GetComponentByName((string)param["componentName"]);
            if (component == null) return;
            StiChart chart = component as StiChart;

            string seriesType = (string)param["seriesType"];
            Assembly assembly = typeof(StiReport).Assembly;
            var series = assembly.CreateInstance("Stimulsoft.Report.Chart." + seriesType) as StiSeries;

            chart.Series.Add(series);

            callbackResult["properties"] = GetChartProperties(chart as StiChart);
        }

        public static void RemoveSeries(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            int seriesIndex = Convert.ToInt32(param["seriesIndex"]);

            if (component == null || seriesIndex == -1) return;
            StiChart chart = component as StiChart;
            chart.Series.RemoveAt(seriesIndex);
            callbackResult["properties"] = GetChartProperties(chart as StiChart);
        }

        public static void SeriesMove(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            int seriesIndex = Convert.ToInt32(param["seriesIndex"]);
            string direction = (string)param["direction"];

            if (component == null || seriesIndex == -1) return;
            StiChart chart = component as StiChart;

            var series = chart.Series[seriesIndex];
            chart.Series.RemoveAt(seriesIndex);

            if (direction == "Up")
                seriesIndex--;
            else
                seriesIndex++;

            chart.Series.Insert(seriesIndex, series);

            callbackResult["properties"] = GetChartProperties(chart as StiChart);
            callbackResult["selectedIndex"] = seriesIndex;
        }

        public static void AddConstantLineOrStrip(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            if (component == null) return;
            StiChart chart = component as StiChart;

            string itemType = (string)param["itemType"];
            if (itemType == "ConstantLines")
            {
                StiConstantLines constantLine = new StiConstantLines();
                chart.ConstantLines.Add(constantLine);
            }
            else
            {
                StiStrips strip = new StiStrips();
                StiBrush brush = new StiGradientBrush(System.Drawing.Color.FromArgb(100, 250, 240, 150), System.Drawing.Color.FromArgb(100, 150, 120, 60), 90);
                strip.StripBrush = brush;
                chart.Strips.Add(strip);
            }

            callbackResult["properties"] = GetChartProperties(chart as StiChart);
            callbackResult["itemType"] = itemType;
        }

        public static void RemoveConstantLineOrStrip(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            int itemIndex = Convert.ToInt32(param["itemIndex"]);
            string itemType = (string)param["itemType"];

            if (component == null || itemIndex == -1) return;
            StiChart chart = component as StiChart;

            if (itemType == "ConstantLines")
            {
                chart.ConstantLines.RemoveAt(itemIndex);
            }
            else
            {
                chart.Strips.RemoveAt(itemIndex);
            }
            
            callbackResult["properties"] = GetChartProperties(chart as StiChart);
            callbackResult["itemType"] = itemType;
        }

        public static void ConstantLineOrStripMove(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            int itemIndex = Convert.ToInt32(param["itemIndex"]);
            string direction = (string)param["direction"];
            string itemType = (string)param["itemType"];

            if (component == null || itemIndex == -1) return;
            StiChart chart = component as StiChart;

            int nextIndex = (direction == "Up") ? itemIndex - 1 : itemIndex + 1;

            if (itemType == "ConstantLines")
            {
                var constantLine = chart.ConstantLines[itemIndex];
                chart.ConstantLines.RemoveAt(itemIndex);
                chart.ConstantLines.Insert(nextIndex, constantLine);
            }
            else 
            {
                var strip = chart.Strips[itemIndex];
                chart.Strips.RemoveAt(itemIndex);
                chart.Strips.Insert(nextIndex, strip);
            }

            callbackResult["properties"] = GetChartProperties(chart as StiChart);
            callbackResult["selectedIndex"] = nextIndex;
            callbackResult["itemType"] = itemType;
        }

        public static void GetLabelsContent(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            var seriesIndex = (param["seriesIndex"] != null) ? Convert.ToInt32(param["seriesIndex"]) : -1;
            if (component == null) return;
            
            StiChart chart = component as StiChart;
            ArrayList labelsContent = new ArrayList();
            var currentSeries = (seriesIndex != -1) ? chart.Series[seriesIndex] : null;
            

            #region clone chart
            StiChart chartCloned = CloneChart(chart);

            if (currentSeries != null)
            {
                for (int index = 0; index < chartCloned.Series.Count; index++)
                {
                    if (index != seriesIndex)
                        chartCloned.Series.RemoveAt(index);
                }
            }
            #endregion

            if (chartCloned.Series.Count > 0)
                chartCloned.Series[0].ShowSeriesLabels = StiShowSeriesLabels.FromChart;

            #region Check Series
            var services = new List<StiSeriesLabels>();
            foreach (var service in StiOptions.Services.ChartSerieLabels.Where(s => s.ServiceEnabled))
            {
                if (!chart.Area.Core.CheckInLabelsTypes(service.GetType()))
                    continue;

                services.Add(service);
            }
            #endregion

            #region Get button size
            int width;
            int height;
            if (services.Count < 6)
            {
                width = 160;
                height = 110;
            }
            else
            {
                width = 80;
                height = 80;
            }
            #endregion

            #region Create buttons
            
            foreach (var labels in services)
            {
                if (!chart.Area.Core.CheckInLabelsTypes(labels.GetType())) continue;

                chartCloned.SeriesLabels = (StiSeriesLabels)labels.Clone();
                chartCloned.Core.ApplyStyle(chartCloned.Style);

                Hashtable content = new Hashtable();
                content["caption"] = labels.ToString();
                content["image"] = GetChartSampleImage(chartCloned, width, height, 0.5f);
                content["type"] = labels.GetType().Name;
                content["width"] = width;
                content["height"] = height;
                labelsContent.Add(content);
            }
            #endregion

            callbackResult["labelsContent"] = labelsContent;
            if (currentSeries != null) callbackResult["isSeriesLables"] = true;
        }

        public static void GetStylesContent(StiReport report, Hashtable param, Hashtable callbackResult, bool forStylesControl)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            if (component == null) component = new StiChart();
            StiChart chart = component as StiChart;
            ArrayList stylesContent = new ArrayList();

            #region clone chart
            StiChart chartCloned = CloneChart(chart);
            #endregion

            foreach (var style in GetChartStyles(report))
            {
                chartCloned.Style = (Stimulsoft.Report.Chart.StiChartStyle)style.Clone();

                var customStyle = style as StiCustomStyle;
                if (customStyle != null)
                    chartCloned.CustomStyleName = customStyle.CustomCore.ReportChartStyle.Name;

                chartCloned.Core.ApplyStyle(chartCloned.Style);

                int width = forStylesControl ? 138 : 80;

                Hashtable content = new Hashtable();
                content["image"] = GetChartSampleImage(chartCloned, width, forStylesControl ? 67 : 80, 1f);
                content["type"] = style.GetType().Name;
                content["name"] = (style is StiCustomStyle) ? ((StiCustomStyleCoreXF)style.Core).ReportStyleName : "";
                content["width"] = width;
                content["height"] = forStylesControl ? 67 : 80;
                stylesContent.Add(content);
            }

            callbackResult["stylesContent"] = stylesContent;
        }

        public static void SetLabelsType(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            string labelsType = (string)param["labelsType"];
            int seriesIndex = (param["seriesIndex"] != null) ? Convert.ToInt32(param["seriesIndex"]) : -1;

            if (component == null) return;
            StiChart chart = component as StiChart;
            
            Assembly assembly = typeof(StiReport).Assembly;
            var labels = assembly.CreateInstance("Stimulsoft.Report.Chart." + labelsType) as StiSeriesLabels;
            if (labels != null)
            {
                if (seriesIndex != -1)
                {   
                    chart.Series[seriesIndex].SeriesLabels = labels;
                    chart.Series[seriesIndex].SeriesLabels.Core.ApplyStyle(chart.Style);
                }
                else
                {
                    chart.SeriesLabels = labels;
                    chart.SeriesLabels.Core.ApplyStyle(chart.Style);
                }
                
            }
            callbackResult["properties"] = GetChartProperties(chart);
            callbackResult["isSeriesLabels"] = seriesIndex != -1;
        }

        public static void SetChartStyle(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            string styleType = (string)param["styleType"];
            string styleName = (string)param["styleName"];

            if (component == null) return;
            StiChart chart = component as StiChart;

            if (styleType == "StiCustomStyle")
            {
                var reportStyle = chart.Report.Styles[styleName];
                if (reportStyle != null)
                {
                    var customStyle = new StiCustomStyle(reportStyle.Name);
                    customStyle.CustomCore.ReportChartStyle = reportStyle as Stimulsoft.Report.StiChartStyle;                    
                    chart.Style = customStyle;
                    chart.CustomStyleName = customStyle.CustomCore.ReportChartStyle.Name;
                }
            }
            else
            {
                Assembly assembly = typeof(StiReport).Assembly;
                var baseStyle = assembly.CreateInstance("Stimulsoft.Report.Chart." + styleType) as Stimulsoft.Report.Chart.StiChartStyle;
                if (baseStyle != null) chart.Style = baseStyle;
            }
            
            chart.Core.ApplyStyle(chart.Style); 
            callbackResult["properties"] = GetChartProperties(chart);
        }

        public static void SetChartPropertyValue(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            if (component == null) return;
            StiChart chart = component as StiChart;

            string panelName = (string)param["panelName"];
            string propertyName = (string)param["propertyName"];
            int seriesIndex = param["seriesIndex"] != null ? Convert.ToInt32(param["seriesIndex"]) : -1;
            if (param["ownerName"] != null && (string)param["ownerName"] != "ConstantLines" && (string)param["ownerName"] != "Strips") 
            { 
                propertyName = ((string)param["ownerName"]) + "." + propertyName; 
            }
            
            object mainObject = null;

            switch (panelName)
            {
                case "Chart":
                    {
                        if (param["indexConstantLines"] != null) 
                        {
                            int indexConstantLines = Convert.ToInt32(param["indexConstantLines"]);
                            if (indexConstantLines != -1) mainObject = chart.ConstantLines[indexConstantLines];
                        }
                        else if (param["indexStrips"] != null)
                        {
                            int indexStrips = Convert.ToInt32(param["indexStrips"]);
                            if (indexStrips != -1) mainObject = chart.Strips[indexStrips];
                        }
                        else
                        {
                            mainObject = chart;
                        }                        
                        break;
                    }
                case "Series":
                case "SeriesLabels":
                    {
                        mainObject = seriesIndex != -1 ? chart.Series[seriesIndex] : null;
                        if (panelName == "SeriesLabels") { propertyName = "SeriesLabels." + propertyName; }
                        break;
                    }
                case "Labels":
                    {
                        mainObject = chart.SeriesLabels;
                        break;
                    }
                case "Area":
                    {
                        mainObject = chart.Area;
                        break;
                    }
            }

            if (mainObject != null) StiReportEdit.SetPropertyValue(chart.Report, propertyName, mainObject, param["propertyValue"]);
           
            callbackResult["properties"] = GetChartProperties(chart as StiChart);
        }

        public static void SetContainerValue(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            var seriesIndex = (param["seriesIndex"] != null) ? Convert.ToInt32(param["seriesIndex"]) : -1;
            if (component == null) return;
            StiChart chart = (StiChart)component;

            
            switch ((string)param["containerType"])
            {
                case "SeriesConditions" :                
                    {
                        if (seriesIndex != -1) 
                        {
                            SetConditionsValue(chart.Series[seriesIndex].Conditions, (ArrayList)param["value"]);
                        }
                        break;
                    }
                case "LabelsConditions":
                    {
                        SetConditionsValue(chart.SeriesLabelsConditions , (ArrayList)param["value"]);
                        break;
                    }
                case "SeriesFilters":
                    {
                        if (seriesIndex != -1)
                        {
                            if (param["filterMode"] != null)((StiSeries)chart.Series[seriesIndex]).FilterMode = (StiFilterMode)Enum.Parse(typeof(StiFilterMode), (string)param["filterMode"]);
                            SetFiltersValue(chart.Series[seriesIndex].Filters, (ArrayList)param["value"]);
                        }
                        break;
                    }
            
            }

            callbackResult["properties"] = GetChartProperties(chart as StiChart);
            if (param["andCloseForm"] != null) callbackResult["closeChartForm"] = param["andCloseForm"];
        }        
        #endregion
    }
}