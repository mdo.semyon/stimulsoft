﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Xml;
using System.Text;
using System.Reflection;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Gauge;
using Stimulsoft.Report.Components.Gauge.Primitives;
using Stimulsoft.Report.Components.Gauge;
using Stimulsoft.Report.Gauge.Primitives;

namespace Stimulsoft.Report.MobileDesign
{
    internal class StiGaugeHelper
    {
        public static Hashtable GetGaugeProperties(StiGauge gauge)
        {
            Hashtable properties = new Hashtable();
            properties["name"] = gauge.Name;
            properties["scales"] = GetGaugeScales(gauge);
            properties["gaugeImage"] = GetGaugeSampleImage(gauge, 400, 400, 1f);
            properties["style"] = GetStyle(gauge);
            //properties["properties"] = GetMainProperties(chart);

            return properties;
        }

        #region Scale Item
        private static Hashtable GaugeScaleItem(StiScaleBase gaugeScale)
        {
            Hashtable gaugeScaleItem = new Hashtable();
            gaugeScaleItem["typeItem"] = "ScaleItem";
            gaugeScaleItem["type"] = gaugeScale.GetType().Name;
            gaugeScaleItem["text"] = gaugeScale.GetType().Name.Substring(3); //TO DO
            gaugeScaleItem["elements"] = GetGaugeElements(gaugeScale);
            gaugeScaleItem["scaleIndex"] = gaugeScale.Gauge.Scales.IndexOf(gaugeScale);

            return gaugeScaleItem;
        }
        #endregion

        #region Gauge Element Item
        private static Hashtable GaugeElementItem(StiGaugeElement gaugeElement)
        {
            Hashtable gaugeElementItem = new Hashtable();
            gaugeElementItem["typeItem"] = "ElementItem";
            gaugeElementItem["type"] = gaugeElement.GetType().Name;
            gaugeElementItem["text"] = gaugeElement.GetType().Name.Substring(3); //TO DO
            gaugeElementItem["elementIndex"] = gaugeElement.Scale.Items.IndexOf(gaugeElement);

            if (gaugeElement is StiLinearTickMarkCustom || gaugeElement is StiLinearTickLabelCustom)
            {
                gaugeElementItem["tickCustomValues"] = GetTickCustomValues(gaugeElement as IStiTickCustom);
            }
            else if (gaugeElement is StiLinearRangeList)
            {
                gaugeElementItem["linearRanges"] = GetLinearRanges(gaugeElement as StiLinearRangeList);
            }
            else if (gaugeElement is StiRadialRangeList)
            {
                gaugeElementItem["radialRanges"] = GetRadialRanges(gaugeElement as StiRadialRangeList);
            }
            else if (gaugeElement is StiLinearBar || gaugeElement is StiRadialBar)
            {
                gaugeElementItem["barRanges"] = GetBarRanges(gaugeElement as StiBarBase);
            }
            else if (gaugeElement is StiStateIndicator)
            {
                gaugeElementItem["indicatorStateFilters"] = GetIndicatorStateFilters(gaugeElement as StiStateIndicator);
                gaugeElementItem["iconType"] = gaugeElement.Scale is StiLinearScale ? "LinearStateIndicator" : "RadialStateIndicator";
            }

            return gaugeElementItem;
        }
        #endregion

        #region Tick Custom Value Item
        private static Hashtable TickCustomValueItem(StiCustomValueBase tickCustomValue)
        {
            Hashtable tickCustomValueItem = new Hashtable();
            tickCustomValueItem["typeItem"] = "ChildElementItem";
            tickCustomValueItem["type"] = tickCustomValue.GetType().Name;
            tickCustomValueItem["iconType"] = "TickCustomValue";
            tickCustomValueItem["text"] = string.Format("{0}: Value={1}", tickCustomValue.LocalizedName, tickCustomValue.Value);

            return tickCustomValueItem;
        }
        #endregion

        #region Linear Range Item
        private static Hashtable LinearRangeItem(StiLinearRange linearRange)
        {
            Hashtable linearRangeItem = new Hashtable();
            linearRangeItem["typeItem"] = "ChildElementItem";
            linearRangeItem["type"] = linearRange.GetType().Name;
            linearRangeItem["iconType"] = "LinearRange";
            linearRangeItem["text"] = string.Format("Range: {0}-{1}", linearRange.StartValue, linearRange.EndValue);

            return linearRangeItem;
        }
        #endregion

        #region Radial Range Item
        private static Hashtable RadialRangeItem(StiRadialRange radialRange)
        {
            Hashtable radialRangeItem = new Hashtable();
            radialRangeItem["typeItem"] = "ChildElementItem";
            radialRangeItem["type"] = radialRange.GetType().Name;
            radialRangeItem["iconType"] = "RadialRange";
            radialRangeItem["text"] = string.Format("Range: {0}-{1}", radialRange.StartValue, radialRange.EndValue);

            return radialRangeItem;
        }
        #endregion

        #region Bar Range Item
        private static Hashtable BarRangeItem(StiIndicatorRangeInfo barRange)
        {
            Hashtable barRangeItem = new Hashtable();
            barRangeItem["typeItem"] = "ChildElementItem";
            barRangeItem["type"] = barRange.GetType().Name;
            barRangeItem["iconType"] = "BarRangeList";
            barRangeItem["text"] = "IndicatorRangeInfo: Value=" + barRange.Value;

            return barRangeItem;
        }
        #endregion

        #region Indicator State Filter Item
        private static Hashtable IndicatorStateFilterItem(StiStateIndicatorFilter indicatorStateFilter)
        {
            Hashtable indicatorStateFilterItem = new Hashtable();
            indicatorStateFilterItem["typeItem"] = "ChildElementItem";
            indicatorStateFilterItem["type"] = indicatorStateFilter.GetType().Name;
            indicatorStateFilterItem["iconType"] = "StateIndicatorFilter";
            indicatorStateFilterItem["text"] = indicatorStateFilter.ToString();

            return indicatorStateFilterItem;
        }
        #endregion

        #region Collections
        private static ArrayList GetGaugeScales(StiGauge gauge)
        {
            ArrayList gaugeItems = new ArrayList();

            foreach (StiScaleBase scale in gauge.Scales)
            {
                gaugeItems.Add(GaugeScaleItem(scale));
            }

            return gaugeItems;
        }

        private static ArrayList GetGaugeElements(StiScaleBase gaugeScale)
        {
            ArrayList gaugeElements = new ArrayList();

            foreach (StiGaugeElement element in gaugeScale.Items)
            {
                gaugeElements.Add(GaugeElementItem(element));
            }

            return gaugeElements;
        }

        private static ArrayList GetTickCustomValues(IStiTickCustom tickCustom)
        {
            ArrayList tickCustomValues = new ArrayList();

            foreach (StiCustomValueBase tickCustomValue in tickCustom.Values)
            {
                tickCustomValues.Add(TickCustomValueItem(tickCustomValue));
            }

            return tickCustomValues;
        }

        private static ArrayList GetLinearRanges(StiLinearRangeList rangeList)
        {
            ArrayList linearRanges = new ArrayList();

            foreach (StiLinearRange range in rangeList.Ranges)
            {
                linearRanges.Add(LinearRangeItem(range));
            }

            return linearRanges;
        }

        private static ArrayList GetRadialRanges(StiRadialRangeList rangeList)
        {
            ArrayList radialRanges = new ArrayList();

            foreach (StiRadialRange range in rangeList.Ranges)
            {
                radialRanges.Add(RadialRangeItem(range));
            }

            return radialRanges;
        }

        private static ArrayList GetBarRanges(StiBarBase bar)
        {
            ArrayList barRanges = new ArrayList();

            foreach (StiIndicatorRangeInfo barRange in bar.RangeList)
            {
                barRanges.Add(BarRangeItem(barRange));
            }

            return barRanges;
        }

        private static ArrayList GetIndicatorStateFilters(StiStateIndicator indicator)
        {
            ArrayList indicatorFilters = new ArrayList();

            foreach (StiStateIndicatorFilter filter in indicator.Filters)
            {
                indicatorFilters.Add(IndicatorStateFilterItem(filter));
            }

            return indicatorFilters;
        }
        #endregion

        public static Hashtable GetStyle(StiGauge gauge)
        {
            Hashtable style = new Hashtable();
            style["type"] = gauge.Style.GetType().Name;
            style["name"] = (gauge.Style is StiCustomGaugeStyle && ((StiCustomGaugeStyleCoreXF)gauge.Style.Core).ReportStyle != null)
                ? ((StiCustomGaugeStyleCoreXF)gauge.Style.Core).ReportStyle.Name : ((StiGaugeStyleXF)gauge.Style).ServiceName;

            return style;
        }

        private static List<StiGaugeStyleXF> GetGaugeStyles(StiReport report)
        {
            var gaugeStyles = new List<StiGaugeStyleXF>();

            // Сначало добавляем стили из отчета, которые создавал пользователь
            foreach (Stimulsoft.Report.StiBaseStyle style in report.Styles)
            {
                if (style is Stimulsoft.Report.StiGaugeStyle)
                {
                    var customStyle = new StiCustomGaugeStyle(style.Name);
                    customStyle.CustomCore.ReportGaugeStyle = style as Stimulsoft.Report.StiGaugeStyle;
                    gaugeStyles.Add(customStyle);
                }
            }

            gaugeStyles.AddRange(StiOptions.Services.GaugeStyles);

            return gaugeStyles;
        }

        public static string GetGaugeSampleImage(StiGauge gauge, int width, int height, float zoom)
        {
            var svgData = new StiSvgData()
            {
                X = 0,
                Y = 0,
                Width = width,
                Height = height,
                Component = gauge
            };

            var sb = new StringBuilder();

            using (var ms = new StringWriter(sb))
            {
                var writer = new XmlTextWriter(ms);

                writer.WriteStartElement("svg");
                writer.WriteAttributeString("version", "1.1");
                writer.WriteAttributeString("baseProfile", "full");

                writer.WriteAttributeString("xmlns", "http://www.w3.org/2000/svg");
                writer.WriteAttributeString("xmlns:xlink", "http://www.w3.org/1999/xlink");
                writer.WriteAttributeString("xmlns:ev", "http://www.w3.org/2001/xml-events");

                writer.WriteAttributeString("height", svgData.Height.ToString());
                writer.WriteAttributeString("width", svgData.Width.ToString());

                writer.WriteStartElement("rect");

                writer.WriteAttributeString("height", svgData.Height.ToString().Replace(",", "."));
                writer.WriteAttributeString("width", svgData.Width.ToString().Replace(",", "."));

                writer.WriteAttributeString("style", StiGaugeSvgHelper.WriteFillBrush(writer, gauge.Brush, new RectangleF(0, 0, (float)svgData.Width, (float)svgData.Height)));
                writer.WriteFullEndElement();

                StiGaugeSvgHelper.WriteGauge(writer, svgData, zoom, false);

                writer.WriteFullEndElement();
                writer.Flush();
                ms.Flush();
                writer.Close();
                ms.Close();
            }

            return sb.ToString();
        }

        public static void SetGaugeStyle(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            string styleType = (string)param["styleType"];
            string styleName = (string)param["styleName"];

            if (component == null) return;
            StiGauge gauge = component as StiGauge;

            if (styleType == "StiCustomGaugeStyle")
            {
                var reportStyle = gauge.Report.Styles[styleName];
                if (reportStyle != null)
                {
                    var customStyle = new StiCustomGaugeStyle(reportStyle.Name);
                    customStyle.CustomCore.ReportGaugeStyle = reportStyle as StiGaugeStyle;
                    gauge.Style = customStyle;
                    gauge.CustomStyleName = customStyle.CustomCore.ReportGaugeStyle.Name;
                }
            }
            else
            {
                Assembly assembly = typeof(StiReport).Assembly;
                var baseStyle = assembly.CreateInstance("Stimulsoft.Report.Gauge." + styleType) as StiGaugeStyleXF;
                if (baseStyle != null) gauge.Style = (IStiGaugeStyle)baseStyle;
            }

            gauge.ApplyStyle(gauge.Style);
        }

        public static void GetStylesContent(StiReport report, Hashtable param, Hashtable callbackResult, bool forStylesControl)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            StiGauge gaugeCloned = component.Clone() as StiGauge;
            ArrayList stylesContent = new ArrayList();

            if (gaugeCloned != null)
            {
                foreach (var style in GetGaugeStyles(report))
                {
                    gaugeCloned.Style = (StiGaugeStyleXF)style.Clone();
                    gaugeCloned.ApplyStyle(gaugeCloned.Style);

                    int width = forStylesControl ? 132 : 80;

                    Hashtable content = new Hashtable();
                    content["image"] = GetGaugeSampleImage(gaugeCloned, width, forStylesControl ? 61 : 80, 0.5f);
                    content["type"] = style.GetType().Name;
                    content["name"] = gaugeCloned.Style is StiCustomGaugeStyle
                        ? ((StiCustomGaugeStyleCoreXF)gaugeCloned.Style.Core).ReportStyleName
                        : ((StiGaugeStyleXF)gaugeCloned.Style).ServiceName;
                    content["width"] = width;
                    content["height"] = forStylesControl ? 61 : 80;
                    stylesContent.Add(content);
                }
            }

            callbackResult["stylesContent"] = stylesContent;
        }

        public static void ExecuteJSCommand(StiGauge gauge, Hashtable parameters, Hashtable callbackResult)
        {
            Hashtable updateResult = new Hashtable();
            updateResult["command"] = (string)parameters["command"];

            switch ((string)parameters["command"])
            {
                #region AddGaugeItem
                case "AddGaugeItem":
                    {
                        string itemType = parameters["itemType"] as string;
                        string itemBaseType = parameters["itemBaseType"] as string;
                        Hashtable itemIndexes = parameters["itemIndexes"] as Hashtable;

                        if (itemBaseType == "Scale")
                        {
                            AddNewScale(gauge, itemType, updateResult);
                        }
                        else if (itemBaseType == "Element")
                        {
                            AddNewGaugeElement(gauge, itemType, itemIndexes, updateResult);
                        }
                        else if (itemBaseType == "ChildElement")
                        {
                            if (itemType == "StiRadialRange" || itemType == "StiLinearRange")
                            {
                                AddNewRange(gauge, itemType, itemIndexes, updateResult);
                            }
                            else if (itemType == "StiTickCustomValue")
                            {
                                AddNewTickCustomValue(gauge, itemType, itemIndexes, updateResult);
                            }
                            else if (itemType == "StiBarRangeList")
                            {
                                AddNewBarRangeList(gauge, itemType, itemIndexes, updateResult);
                            }
                            else if (itemType == "StiStateIndicatorFilter")
                            {
                                AddNewStateIndicatorFilter(gauge, itemType, itemIndexes, updateResult);
                            }
                        }

                        updateResult["scales"] = GetGaugeScales(gauge);
                        break;
                    }
                #endregion

                #region RemoveGaugeItem
                case "RemoveGaugeItem":
                    {
                        Hashtable itemIndexes = parameters["itemIndexes"] as Hashtable;
                        RemoveGaugeItem(gauge, itemIndexes);
                        updateResult["itemIndexes"] = itemIndexes;
                        break;
                    }
                    #endregion
            }

            updateResult["gaugeImage"] = GetGaugeSampleImage(gauge, 400, 400, 1f);
            callbackResult["updateResult"] = updateResult;
        }

        #region Helper Methods
        private static StiScaleBase GetGaugeScaleBySelectedItem(StiGauge gauge, Hashtable itemIndexes)
        {
            if (itemIndexes != null && itemIndexes["scaleIndex"] != null)
            {
                int scaleIndex = Convert.ToInt32(itemIndexes["scaleIndex"]);

                if (gauge.Scales.Count > 0 && gauge.Scales.Count > scaleIndex)
                    return gauge.Scales[scaleIndex];
            }

            return null;
        }

        private static StiGaugeElement GetGaugeElementBySelectedItem(StiGauge gauge, Hashtable itemIndexes)
        {
            StiScaleBase scale = GetGaugeScaleBySelectedItem(gauge, itemIndexes);

            if (scale != null && itemIndexes != null && itemIndexes["elementIndex"] != null)
            {
                int elementIndex = Convert.ToInt32(itemIndexes["elementIndex"]);
                if (scale.Items.Count > 0 && scale.Items.Count > elementIndex)
                    return scale.Items[elementIndex];
            }

            return null;
        }

        private static void AddNewScale(StiGauge gauge, string scaleType, Hashtable updateResult)
        {
            Assembly assembly = typeof(StiReport).Assembly;
            var scale = assembly.CreateInstance("Stimulsoft.Report.Components.Gauge." + scaleType) as StiScaleBase;
            if (scale != null)
            {
                gauge.Scales.Add(scale);
                updateResult["scaleIndex"] = gauge.Scales.IndexOf(scale);
            }
        }

        private static void AddNewGaugeElement(StiGauge gauge, string elementType, Hashtable itemIndexes, Hashtable updateResult)
        {
            StiScaleBase scale = GetGaugeScaleBySelectedItem(gauge, itemIndexes);

            if (scale != null)
            {
                Assembly assembly = typeof(StiReport).Assembly;
                var element = assembly.CreateInstance("Stimulsoft.Report.Components.Gauge." + elementType) as StiGaugeElement;

                if (element != null)
                {
                    scale.Items.Add(element);

                    updateResult["scaleIndex"] = gauge.Scales.IndexOf(scale);
                    updateResult["elementIndex"] = scale.Items.IndexOf(element);
                }
            }
        }

        private static void AddNewRange(StiGauge gauge, string elementType, Hashtable itemIndexes, Hashtable updateResult)
        {
            var rangeList = GetGaugeElementBySelectedItem(gauge, itemIndexes) as StiScaleRangeList;

            if (rangeList != null)
            {
                Assembly assembly = typeof(StiReport).Assembly;
                StiRangeBase rangeBase = assembly.CreateInstance("Stimulsoft.Report.Components.Gauge." + elementType) as StiRangeBase;
                if (rangeBase != null)
                {
                    rangeList.Ranges.Add(rangeBase);

                    updateResult["scaleIndex"] = gauge.Scales.IndexOf(rangeList.Scale);
                    updateResult["elementIndex"] = rangeList.Scale.Items.IndexOf(rangeList);
                    updateResult["childElementIndex"] = rangeList.Ranges.IndexOf(rangeBase);
                }
            }
        }

        private static void AddNewTickCustomValue(StiGauge gauge, string elementType, Hashtable itemIndexes, Hashtable updateResult)
        {
            StiCustomValueBase customValue = null;
            StiScaleBase scale = GetGaugeScaleBySelectedItem(gauge, itemIndexes);
            var gaugeElement = GetGaugeElementBySelectedItem(gauge, itemIndexes) as IStiTickCustom;

            if (gaugeElement != null)
            {
                if (gaugeElement is StiLinearTickLabelCustom)
                {
                    customValue = new StiLinearTickLabelCustomValue();
                }
                else if (gaugeElement is StiLinearTickMarkCustom)
                {
                    customValue = new StiLinearTickMarkCustomValue();
                }
                else if (gaugeElement is StiRadialTickLabelCustom)
                {
                    customValue = new StiRadialTickLabelCustomValue();
                }
                else if (gaugeElement is StiRadialTickMarkCustom)
                {
                    customValue = new StiRadialTickMarkCustomValue();
                }

                if (customValue != null)
                {
                    gaugeElement.Values.Add(customValue);

                    updateResult["scaleIndex"] = gauge.Scales.IndexOf(scale);
                    updateResult["elementIndex"] = scale.Items.IndexOf(gaugeElement as StiGaugeElement);
                    updateResult["childElementIndex"] = gaugeElement.Values.IndexOf(customValue);
                }
            }
        }

        private static void AddNewBarRangeList(StiGauge gauge, string elementType, Hashtable itemIndexes, Hashtable updateResult)
        {
            StiIndicatorRangeInfo rangeInfo = null;
            var barBase = GetGaugeElementBySelectedItem(gauge, itemIndexes) as StiBarBase;

            if (barBase != null)
            {
                if (barBase is StiRadialBar)
                    rangeInfo = new StiRadialIndicatorRangeInfo();
                else
                    rangeInfo = new StiLinearIndicatorRangeInfo();

                barBase.RangeList.Add(rangeInfo);

                updateResult["scaleIndex"] = gauge.Scales.IndexOf(barBase.Scale);
                updateResult["elementIndex"] = barBase.Scale.Items.IndexOf(barBase);
                updateResult["childElementIndex"] = barBase.RangeList.IndexOf(rangeInfo);
            }
        }

        private static void AddNewStateIndicatorFilter(StiGauge gauge, string elementType, Hashtable itemIndexes, Hashtable updateResult)
        {
            var stateIndicator = GetGaugeElementBySelectedItem(gauge, itemIndexes) as StiStateIndicator;

            if (stateIndicator != null)
            {
                StiStateIndicatorFilter statFilter = new StiStateIndicatorFilter();
                stateIndicator.Filters.Add(statFilter);

                updateResult["scaleIndex"] = gauge.Scales.IndexOf(stateIndicator.Scale);
                updateResult["elementIndex"] = stateIndicator.Scale.Items.IndexOf(stateIndicator);
                updateResult["childElementIndex"] = stateIndicator.Filters.IndexOf(statFilter);
            }
        }

        private static void RemoveGaugeItem(StiGauge gauge, Hashtable itemIndexes)
        {
            StiScaleBase scale = GetGaugeScaleBySelectedItem(gauge, itemIndexes);

            if (scale != null)
            {
                if (itemIndexes["elementIndex"] != null)
                {
                    var element = GetGaugeElementBySelectedItem(gauge, itemIndexes);

                    if (itemIndexes["childElementIndex"] != null)
                    {
                        var index = Convert.ToInt32(itemIndexes["childElementIndex"]);

                        if (element is StiScaleRangeList)
                        {
                            ((StiScaleRangeList)element).Ranges.RemoveAt(index);
                        }
                        else if (element is IStiTickCustom)
                        {
                            ((IStiTickCustom)element).Values.RemoveAt(index);
                        }
                        else if (element is StiBarBase)
                        {
                            ((StiBarBase)element).RangeList.RemoveAt(index);
                        }
                        else if (element is StiStateIndicator)
                        {
                            ((StiStateIndicator)element).Filters.RemoveAt(index);
                        }
                    }
                    else
                    {
                        if (scale.Items.Contains(element))
                            scale.Items.Remove(element);
                    }
                }
                else
                {
                    if (gauge.Scales.Contains(scale))
                        gauge.Scales.Remove(scale);
                }
            }
        }
        #endregion
    }
}