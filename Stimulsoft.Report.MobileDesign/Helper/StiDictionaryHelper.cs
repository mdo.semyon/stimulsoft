#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Linq;
using System.Collections.Generic;
using Stimulsoft.Report.Components;
using Stimulsoft.Base.Drawing;
using System.Drawing;
using System.Collections;
using System.Globalization;
using Stimulsoft.Report.Dictionary;
using System.Drawing.Imaging;
using System.IO;
using Stimulsoft.Base;
using System.Reflection;
using System.Data;
using Stimulsoft.Report.Design;
using Stimulsoft.Report.CodeDom;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Engine;
using Stimulsoft.Report.Helpers;

namespace Stimulsoft.Report.MobileDesign
{
    internal class StiDictionaryHelper
    {
        private static CultureInfo en_us_culture = CultureInfo.CreateSpecificCulture("en-US");

        #region Database Item
        private static Hashtable DatabaseItem(string name, string nameInSource, string alias, bool dataInStore)
        {
            Hashtable dataBaseItem = new Hashtable();
            dataBaseItem["typeItem"] = "DataBase";
            dataBaseItem["typeIcon"] = dataInStore ? "DataStore" : "ConnectionFail";
            dataBaseItem["typeConnection"] = null;
            dataBaseItem["name"] = name;
            dataBaseItem["alias"] = alias;
            dataBaseItem["nameInSource"] = nameInSource;
            dataBaseItem["dataInStore"] = dataInStore;
            dataBaseItem["dataSources"] = new ArrayList();

            return dataBaseItem;
        }

        private static Hashtable DatabaseItem(StiDatabase database)
        {
            Hashtable dataBaseItem = DatabaseItem(database.Name, database.ToString(), database.Alias, false);
            dataBaseItem["typeIcon"] = (database is StiUndefinedDatabase) ? ((database.Inherited) ? "LockedConnection" : "UndefinedConnection") : "Connection";
            dataBaseItem["typeConnection"] = database.GetType().Name;
            
            if (database is StiFileDatabase)
            {
                dataBaseItem["pathData"] = ((StiFileDatabase)database).PathData != null ? StiEncodingHelper.Encode(((StiFileDatabase)database).PathData) : String.Empty;
            }

            if (database is StiXmlDatabase)
            {
                dataBaseItem["pathSchema"] = ((StiXmlDatabase)database).PathSchema != null ? StiEncodingHelper.Encode(((StiXmlDatabase)database).PathSchema) : String.Empty;
                dataBaseItem["xmlType"] = ((StiXmlDatabase)database).XmlType;
            }
            else if (database is StiExcelDatabase)
            {
                dataBaseItem["firstRowIsHeader"] = ((StiExcelDatabase)database).FirstRowIsHeader;
            }
            else if (database is StiDBaseDatabase)
            {
                dataBaseItem["codePage"] = ((StiDBaseDatabase)database).CodePage.ToString();
            }
            else if (database is StiCsvDatabase)
            {
                dataBaseItem["codePage"] = ((StiCsvDatabase)database).CodePage.ToString();
                dataBaseItem["separator"] = ((StiCsvDatabase)database).Separator;
            }
            else if (!(database is StiFileDatabase))
            {
                System.Reflection.PropertyInfo connectionStringProp = database.GetType().GetProperty("ConnectionString");
                dataBaseItem["connectionString"] = (connectionStringProp != null) ? StiEncodingHelper.Encode((string)connectionStringProp.GetValue(database, null)) : "";
                System.Reflection.PropertyInfo promptUserNameAndPasswordProp = database.GetType().GetProperty("PromptUserNameAndPassword");
                dataBaseItem["promptUserNameAndPassword"] = (promptUserNameAndPasswordProp != null) ? (bool)promptUserNameAndPasswordProp.GetValue(database, null) : false;
            }

            return dataBaseItem;
        }
        #endregion

        #region Datasource Item
        private static Hashtable DatasourceItem(StiDataSource datasource)
        {
            string typeIcon = "DataSource";
            if (datasource is StiUndefinedDataSource)
                typeIcon = "UndefinedDataSource";
            else if (datasource.Inherited)
                typeIcon = "LockedDataSource";

            Hashtable datasourceItem = new Hashtable();
            datasourceItem["typeItem"] = "DataSource";
            datasourceItem["typeIcon"] = datasource.IsCloud ? "CloudDataSource" : typeIcon;
            datasourceItem["isCloud"] = datasource.IsCloud;
            datasourceItem["typeDataSource"] = datasource.GetType().Name;
            StiDataAdapterService dataAdapter = datasource.GetDataAdapter();
            if (dataAdapter != null) datasourceItem["typeDataAdapter"] = dataAdapter.GetType().Name;
            datasourceItem["name"] = datasource.Name;
            datasourceItem["correctName"] = StiNameValidator.CorrectName(datasource.Name);
            datasourceItem["nameInSource"] = ((StiDataStoreSource)datasource).NameInSource;
            datasourceItem["alias"] = datasource.Alias;
            datasourceItem["columns"] = GetColumnsTree(datasource.Columns, datasource.IsCloud);
            datasourceItem["parameters"] = GetParametersTree(datasource.Parameters, datasource.IsCloud);
            if (datasource is StiSqlSource && !(datasource is StiNoSqlSource) && !(datasource is StiODataSource))
            {
                datasourceItem["parameterTypes"] = GetDataParameterTypes(datasource as StiSqlSource);
                datasourceItem["reconnectOnEachRow"] = ((StiSqlSource)datasource).ReconnectOnEachRow;
                datasourceItem["commandTimeout"] = ((StiSqlSource)datasource).CommandTimeout.ToString();
            }
            datasourceItem["relations"] = GetRelationsTree(null, datasource.GetParentRelations(), datasource.IsCloud, new Hashtable());
            System.Reflection.PropertyInfo sqlCommandProp = datasource.GetType().GetProperty("SqlCommand");
            if (sqlCommandProp != null) datasourceItem["sqlCommand"] = StiEncodingHelper.Encode((string)sqlCommandProp.GetValue(datasource, null));
            System.Reflection.PropertyInfo typeProp = datasource.GetType().GetProperty("Type");
            if (sqlCommandProp != null) datasourceItem["type"] = ((StiSqlSourceType)typeProp.GetValue(datasource, null)).ToString();

            if (datasource is StiVirtualSource)
            {
                datasourceItem["nameInSource"] = ((StiVirtualSource)datasource).NameInSource;
                datasourceItem["sortData"] = StiReportEdit.GetSortDataProperty(datasource);
                datasourceItem["filterData"] = StiEncodingHelper.Encode(JSON.Encode(StiReportEdit.GetFiltersObject(((StiVirtualSource)datasource).Filters)));
                datasourceItem["filterMode"] = ((StiVirtualSource)datasource).FilterMode.ToString();
                datasourceItem["filterOn"] = ((StiVirtualSource)datasource).FilterOn;
                datasourceItem["groupsData"] = GetGroupColumnsProperty(datasource as StiVirtualSource);
                datasourceItem["resultsData"] = GetResultsProperty(datasource as StiVirtualSource);
            }

            return datasourceItem;
        }
        #endregion

        #region Column Item
        private static Hashtable ColumnItem(StiDataColumn column)
        {
            Hashtable columnItem = new Hashtable();
            columnItem["typeItem"] = "Column";
            columnItem["typeIcon"] = GetIconTypeForColumn(column).ToString();
            columnItem["type"] = GetTypeValueToString(column.Type);
            columnItem["name"] = (column.Name);
            columnItem["correctName"] = StiNameValidator.CorrectName(column.Name);
            columnItem["alias"] = column.Alias;
            columnItem["nameInSource"] = column.NameInSource;
            columnItem["isCalcColumn"] = column is StiCalcDataColumn;
            columnItem["expression"] = column is StiCalcDataColumn ? StiEncodingHelper.Encode(((StiCalcDataColumn)column).Expression) : "";

            return columnItem;
        }
        #endregion

        #region Parameter Item
        private static Hashtable ParameterItem(StiDataParameter parameter)
        {
            Hashtable parameterItem = new Hashtable();
            parameterItem["typeItem"] = "Parameter";
            parameterItem["typeIcon"] = "Parameter";
            parameterItem["type"] = parameter.Type.ToString();
            parameterItem["size"] = parameter.Size.ToString();
            parameterItem["name"] = parameter.Name;
            parameterItem["correctName"] = StiNameValidator.CorrectName(parameter.Name);
            parameterItem["expression"] = StiEncodingHelper.Encode(parameter.Expression);

            return parameterItem;
        }
        #endregion

        #region Relation Item
        private static Hashtable RelationItem(StiDataRelation relation, Hashtable upLevelRelations)
        {
            Hashtable relationItem = new Hashtable();
            relationItem["typeItem"] = "Relation";
            relationItem["typeIcon"] = relation.IsCloud ? "CloudRelation" : (relation.Inherited ? "LockedRelation" : "Relation");
            relationItem["isCloud"] = relation.IsCloud;
            relationItem["name"] = relation.Name;
            relationItem["correctName"] = StiNameValidator.CorrectName(relation.Name);
            relationItem["alias"] = relation.Alias;
            relationItem["nameInSource"] = relation.NameInSource;
            relationItem["parentDataSource"] = relation.ParentSource != null ? relation.ParentSource.Name : "";
            relationItem["childDataSource"] = relation.ChildSource != null ? relation.ChildSource.Name : "";
            ArrayList jsParentColumns = new ArrayList();
            foreach (string parentColumns in relation.ParentColumns) jsParentColumns.Add(parentColumns);
            relationItem["parentColumns"] = jsParentColumns;
            ArrayList jsChildColumns = new ArrayList();
            foreach (string childColumns in relation.ChildColumns) jsChildColumns.Add(childColumns);
            relationItem["childColumns"] = jsChildColumns;

            upLevelRelations[relation.NameInSource] = true;
            relationItem["relations"] = relation.ParentSource != null
                ? GetRelationsTree(relation, relation.ParentSource.GetParentRelations(), relation.ParentSource.IsCloud, upLevelRelations)
                : new ArrayList();

            return relationItem;
        }
        #endregion

        #region BusinessObject Item
        private static Hashtable BusinessObjectItem(StiBusinessObject businessObject)
        {
            Hashtable businessObjectItem = new Hashtable();
            businessObjectItem["typeItem"] = "BusinessObject";
            businessObjectItem["typeIcon"] = "BusinessObject";
            businessObjectItem["category"] = businessObject.Category;
            businessObjectItem["name"] = businessObject.Name;
            businessObjectItem["correctName"] = StiNameValidator.CorrectName(businessObject.Name);
            businessObjectItem["alias"] = businessObject.Alias;
            businessObjectItem["columns"] = GetColumnsTree(businessObject.Columns);
            businessObjectItem["businessObjects"] = GetChildBusinessObjectsTree(businessObject);
            businessObjectItem["fullName"] = businessObject.GetFullName();

            return businessObjectItem;
        }
        #endregion

        #region Variable Item
        private static Hashtable VariableItem(StiVariable variable)
        {
            Hashtable variableItem = new Hashtable();
            variableItem["typeItem"] = "Variable";
            variableItem["typeIcon"] = GetDataColumnImageIdFromType(variable.Type, !variable.Inherited).ToString();
            variableItem["basicType"] = GetVariableBasicType(variable);
            variableItem["type"] = GetVariableType(variable);
            variableItem["name"] = variable.Name;
            variableItem["correctName"] = StiNameValidator.CorrectName(variable.Name);
            variableItem["alias"] = variable.Alias;
            variableItem["category"] = variable.Category;
            variableItem["description"] = StiEncodingHelper.Encode(variable.Description);
            variableItem["initBy"] = variable.InitBy;
            variableItem["readOnly"] = variable.ReadOnly;
            variableItem["allowUseAsSqlParameter"] = variable.AllowUseAsSqlParameter;
            variableItem["requestFromUser"] = variable.RequestFromUser;
            variableItem["allowUserValues"] = variable.DialogInfo.AllowUserValues;
            variableItem["dateTimeFormat"] = variable.DialogInfo.DateTimeType;
            variableItem["dataSource"] = variable.DialogInfo.ItemsInitializationType;
            variableItem["selection"] = variable.Selection.ToString();
            variableItem["formatMask"] = StiEncodingHelper.Encode(variable.DialogInfo.Mask);
            variableItem["items"] = GetItems(variable, (string)variableItem["type"]);
            variableItem["keys"] = variable.DialogInfo.KeysColumn;
            variableItem["values"] = variable.DialogInfo.ValuesColumn;
            variableItem["dependentValue"] = variable.DialogInfo.BindingValue;
            variableItem["dependentVariable"] = variable.DialogInfo.BindingVariable != null ? variable.DialogInfo.BindingVariable.Name : "";
            variableItem["dependentColumn"] = variable.DialogInfo.BindingValuesColumn;

            #region Get Value & Expression
            switch ((string)variableItem["basicType"])
            {
                case "Value":
                case "NullableValue":
                    {
                        if (variable.InitBy == StiVariableInitBy.Value)
                        {
                            if ((string)variableItem["type"] == "image")
                            {
                                variableItem["value"] = variable.ValueObject != null ? StiReportEdit.ImageToBase64(variable.ValueObject as System.Drawing.Image) : null;
                            }
                            else
                            {
                                string valueString = variable.Value != null ? variable.Value : "";
                                if (variable.ValueObject != null && (string)variableItem["type"] == "datetime") valueString = ((DateTime)variable.ValueObject).ToString(en_us_culture);
                                variableItem["value"] = StiEncodingHelper.Encode(valueString);
                            }
                        }
                        else
                            variableItem["expression"] = StiEncodingHelper.Encode(variable.Value);
                        break;
                    }
                case "Range":
                    {
                        if (variable.InitBy == StiVariableInitBy.Value)
                        {
                            object fromObject = ((Range)variable.ValueObject).FromObject;
                            object toObject = ((Range)variable.ValueObject).ToObject;
                            string fromObjectString = string.Empty;
                            string toObjectString = string.Empty;
                            if (fromObject != null) fromObjectString = (((string)variableItem["type"] == "datetime") ? ((DateTime)fromObject).ToString(en_us_culture) : fromObject.ToString());
                            if (toObject != null) toObjectString = (((string)variableItem["type"] == "datetime") ? ((DateTime)toObject).ToString(en_us_culture) : toObject.ToString());
                            variableItem["valueFrom"] = StiEncodingHelper.Encode(fromObjectString);
                            variableItem["valueTo"] = StiEncodingHelper.Encode(toObjectString);
                        }
                        else
                        {
                            variableItem["expressionFrom"] = StiEncodingHelper.Encode(variable.InitByExpressionFrom);
                            variableItem["expressionTo"] = StiEncodingHelper.Encode(variable.InitByExpressionTo);
                        }
                        break;
                    }
            }
            #endregion                        

            return variableItem;
        }
        #endregion

        #region Table Item
        private static Hashtable TableItem(DataTable table)
        {
            Hashtable tableItem = new Hashtable();
            tableItem["typeItem"] = "Table";
            tableItem["name"] = table.TableName;
            tableItem["correctName"] = StiNameValidator.CorrectName(table.TableName);
            tableItem["columns"] = GetColumnsTree(table.Columns);
            if (table.ParentRelations != null && table.ParentRelations.Count > 0)
            {
                tableItem["relations"] = GetRelationsTree(table.ParentRelations);
            }

            return tableItem;
        }
        #endregion

        #region Function Item
        private static Hashtable FunctionItem(StiFunction function, StiReport report)
        {
            Hashtable functionItem = new Hashtable();
            functionItem["typeItem"] = "Function";
            functionItem["typeIcon"] = "Function";
            functionItem["name"] = function.FunctionName;
            functionItem["caption"] = function.GetFunctionString(report.ScriptLanguage);
            functionItem["descriptionHeader"] = function.GetLongFunctionString(report.ScriptLanguage);
            functionItem["description"] = function.Description;
            functionItem["returnDescription"] = function.ReturnDescription;

            ArrayList argumentDescriptions = new ArrayList();
            if (function.ArgumentDescriptions != null)
            {
                foreach (string argumentDescription in function.ArgumentDescriptions)
                {
                    argumentDescriptions.Add(argumentDescription);
                }
            }
            functionItem["argumentDescriptions"] = argumentDescriptions;

            ArrayList argumentNames = new ArrayList();
            if (function.ArgumentNames != null)
            {
                foreach (string argumentName in function.ArgumentNames)
                {
                    argumentNames.Add(argumentName);
                }
            }
            functionItem["argumentNames"] = argumentNames;


            functionItem["text"] = string.Format("{0}({1})",
                            function.FunctionName,
                            (function.ArgumentNames == null || function.ArgumentNames.Length == 0) ? string.Empty :
                            new string(',', function.ArgumentNames.Length - 1));

            return functionItem;
        }
        #endregion

        #region Functions Category Item
        private static Hashtable FunctionsCategoryItem(string name, string typeIcon)
        {
            Hashtable categoryItem = new Hashtable();
            categoryItem["typeItem"] = "FunctionsCategory";
            categoryItem["typeIcon"] = typeIcon;
            categoryItem["name"] = name;
            categoryItem["items"] = new ArrayList();

            return categoryItem;
        }
        #endregion

        #region Resource Item
        private static Hashtable ResourceItem(StiResource resource)
        {
            Hashtable resourceItem = new Hashtable();
            resourceItem["typeItem"] = "Resource";
            resourceItem["type"] = resource.Type;
            resourceItem["typeIcon"] = resource.Content != null ? "Resources.Resource" + resource.Type : "Resources.Resource";
            resourceItem["name"] = resource.Name;
            resourceItem["alias"] = resource.Alias;

            return resourceItem;
        }
        #endregion

        #region Images Gallery Item
        private static Hashtable ImagesGalleryItem(string name, System.Type type, string source)
        {
            Hashtable galleryItem = new Hashtable();
            galleryItem["name"] = name;
            galleryItem["type"] = type.Name;
            galleryItem["src"] = source;

            return galleryItem;
        }
        #endregion

        #region RichText Gallery Item
        private static Hashtable RichTextGalleryItem(string name, System.Type type, string imageName)
        {
            Hashtable galleryItem = new Hashtable();
            galleryItem["name"] = name;
            galleryItem["type"] = type.Name;
            galleryItem["imageName"] = imageName;

            return galleryItem;
        }
        #endregion

        #region Helper Methods
        public static string GetGroupColumnsProperty(StiVirtualSource dataSource)
        {
            ArrayList result = new ArrayList();
            string[] groupColumns = dataSource.GroupColumns;

            if (groupColumns != null)
            {
                for (int i = 0; i < groupColumns.Length; i++)
                {
                    Hashtable oneGroupColumn = new Hashtable();
                    string fullColumnProperty = groupColumns[i];
                    if (fullColumnProperty.StartsWith("DESC"))
                    {
                        oneGroupColumn["direction"] = "DESC";
                        oneGroupColumn["column"] = fullColumnProperty.Replace("DESC", "");
                    }
                    else if (fullColumnProperty.StartsWith("NONE"))
                    {
                        oneGroupColumn["direction"] = "NONE";
                        oneGroupColumn["column"] = fullColumnProperty.Replace("NONE", "");
                    }
                    else
                    {
                        oneGroupColumn["direction"] = "ASC";
                        oneGroupColumn["column"] = fullColumnProperty;
                    }
                    result.Add(oneGroupColumn);
                }
            }

            return result.Count != 0 ? StiEncodingHelper.Encode(JSON.Encode(result)) : string.Empty;
        }

        public static string GetResultsProperty(StiVirtualSource dataSource)
        {
            ArrayList resultArray = new ArrayList();

            string[] results = dataSource.Results;

            if (results != null)
            {
                for (int i = 0; i < results.Length; i += 3)
                {
                    Hashtable oneResult = new Hashtable();
                    oneResult["column"] = results[i];
                    oneResult["aggrFunction"] = results[i + 1];
                    oneResult["name"] = results[i + 2];

                    resultArray.Add(oneResult);
                }
            }

            return resultArray.Count != 0 ? StiEncodingHelper.Encode(JSON.Encode(resultArray)) : string.Empty;
        }

        private static void SetGroupColumnsAndResultsProperty(StiVirtualSource dataSource, object groupsData, object resultsData)
        {
            StiDataSource parentDataSource = dataSource.Dictionary.DataSources[dataSource.NameInSource];

            #region Store calculated columns
            var tempColumns = new StiDataColumnsCollection(dataSource);
            foreach (StiDataColumn tempColumn in dataSource.Columns)
            {
                if (tempColumn is StiCalcDataColumn) tempColumns.Add(tempColumn);
            }
            #endregion

            #region Create collection of columns
            var prevFields = new Hashtable();
            foreach (StiDataColumn column in dataSource.Columns)
            {
                prevFields[column.Name] = column.Type;
            }

            dataSource.Columns.Clear();
            #endregion

            #region Group cloumns
            List<string> groups = new List<string>();

            if (!string.IsNullOrEmpty(groupsData as string))
            {
                ArrayList groupColumnsArray = (ArrayList)JSON.Decode(StiEncodingHelper.DecodeString(groupsData as string));
                for (int i = 0; i < groupColumnsArray.Count; i++)
                {
                    Hashtable groupColumnObject = (Hashtable)groupColumnsArray[i];
                    groups.Add((string)groupColumnObject["direction"] != "ASC"
                        ? (string)groupColumnObject["direction"] + (string)groupColumnObject["column"]
                        : (string)groupColumnObject["column"]);
                }
            }
            dataSource.GroupColumns = groups.ToArray();

            #region Create columns from Groups
            var names = new Hashtable();
            foreach (string group in dataSource.GroupColumns)
            {
                string groupField = group;
                if (groupField.StartsWith("DESC", StringComparison.InvariantCulture))
                {
                    //check for "DESC" word in column name
                    if (parentDataSource != null)
                    {
                        StiDataColumn tempColumn1 = parentDataSource.Columns[groupField];
                        StiDataColumn tempColumn2 = parentDataSource.Columns[groupField.Substring(4)];
                        if (tempColumn1 != null && tempColumn2 == null)
                        {
                            groupField = group;
                        }
                        else
                        {
                            groupField = groupField.Substring(4);
                        }
                    }
                    else
                    {
                        groupField = groupField.Substring(4);
                    }
                }
                else if (groupField.StartsWith("NONE", StringComparison.InvariantCulture)) groupField = groupField.Substring(4);

                var dataColumn = new StiDataColumn(groupField);
                dataColumn.Type = prevFields[dataColumn.Name] as Type;
                if (dataColumn.Type == null)
                {
                    dataColumn.Type = typeof(object);
                    if (parentDataSource != null)
                    {
                        var tempColumn = parentDataSource.Columns[groupField];
                        if (tempColumn != null) dataColumn.Type = tempColumn.Type;
                    }
                }
                dataSource.Columns.Add(dataColumn);
                names[groupField] = groupField;
            }
            #endregion

            #endregion

            #region Results
            List<string> results = new List<string>();
            if (!string.IsNullOrEmpty(resultsData as string))
            {
                ArrayList resultsArray = (ArrayList)JSON.Decode(StiEncodingHelper.DecodeString(resultsData as string));
                foreach (Hashtable result in resultsArray)
                {
                    string column = (string)result["column"];
                    string function = (string)result["aggrFunction"];
                    string name = (string)result["name"];

                    int index2 = 2;
                    if (name == "Count") name += "1";
                    while (names[name] != null)
                    {
                        name = (string)result["name"] + (index2++).ToString();
                    }
                    names[name] = name;

                    results.Add(column);
                    results.Add(function == "No" ? string.Empty : function);
                    results.Add(name);
                }
            }
            dataSource.Results = results.ToArray();

            #region Create columns from results
            int index = 0;
            while (index < dataSource.Results.Length)
            {
                string column = dataSource.Results[index++];
                string function = dataSource.Results[index++];
                string name = dataSource.Results[index++];

                var dataColumn = new StiDataColumn(name);
                if (function == "First" || function == "Last" || function.Length == 0)
                {
                    dataColumn.Type = prevFields[dataColumn.Name] as Type;
                    if (dataColumn.Type == null)
                    {
                        dataColumn.Type = typeof(object);
                        if (parentDataSource != null)
                        {
                            StiDataColumn tempColumn = parentDataSource.Columns[column];
                            if (tempColumn != null) dataColumn.Type = tempColumn.Type;
                        }
                    }
                }
                else if (function == "Count" || function == "CountDistinct")
                {
                    dataColumn.Type = typeof(long);
                }
                else if (function == "MinDate" || function == "MaxDate")
                {
                    dataColumn.Type = typeof(DateTime);
                }
                else if (function == "MinTime" || function == "MaxTime")
                {
                    dataColumn.Type = typeof(TimeSpan);
                }
                else if (function == "MinStr" || function == "MaxStr")
                {
                    dataColumn.Type = typeof(string);
                }
                else
                {
                    dataColumn.Type = prevFields[dataColumn.Name] as Type;
                    if (dataColumn.Type == null) dataColumn.Type = typeof(decimal);
                }

                dataSource.Columns.Add(dataColumn);
            }
            #endregion
            #endregion

            #region Restore calculated columns
            if (tempColumns.Count > 0)
            {
                foreach (StiDataColumn calcColumn in tempColumns)
                {
                    dataSource.Columns.Add(calcColumn);
                }
            }
            #endregion
        }

        private static ArrayList GetDataParameterTypes(StiSqlSource source)
        {
            ArrayList types = new ArrayList();
            var parameterTypes = source.GetParameterTypesEnum();

            if (parameterTypes != null)
            {
                var array = Enum.GetValues(parameterTypes);

                foreach (var type in array)
                {
                    Hashtable typeObject = new Hashtable();
                    typeObject["typeValue"] = (int)type;
                    typeObject["typeName"] = type.ToString();
                    types.Add(typeObject);
                }
            }

            return types;
        }

        private static string GetItemType(StiDialogInfoItem itemVariable)
        {
            if (itemVariable is StiExpressionDialogInfoItem || itemVariable is StiExpressionRangeDialogInfoItem) return "expression";
            return "value";
        }

        private static object GetItemKeyObject(object itemKey, string type, string itemType)
        {
            object keyObject = null;
            if (itemKey != null)
                keyObject = StiEncodingHelper.Encode(type == "datetime" && itemType == "value" ? ((DateTime)itemKey).ToString(en_us_culture) : itemKey.ToString());

            return keyObject;
        }

        private static ArrayList GetItems(StiVariable variable, string type)
        {
            ArrayList items = new ArrayList();
            int index = 0;
            if (variable.DialogInfo.Keys != null && variable.DialogInfo.Keys.Length != 0)
            {
                List<StiDialogInfoItem> itemsVariable = variable.DialogInfo.GetDialogInfoItems(variable.Type);
                foreach (StiDialogInfoItem itemVariable in itemsVariable)
                {
                    Hashtable item = new Hashtable();
                    string itemType = GetItemType(itemVariable);
                    item["type"] = itemType;
                    item["value"] = StiEncodingHelper.Encode(itemVariable.Value);
                    try
                    {
                        string valueBindingString = JSON.Encode(itemVariable.ValueBinding);
                        item["valueBinding"] = itemVariable.ValueBinding;
                    }
                    catch { item["valueBinding"] = itemVariable.ValueBinding.ToString(); }
                    item["key"] = GetItemKeyObject(itemVariable.KeyObject, type, itemType);
                    item["keyTo"] = GetItemKeyObject(itemVariable.KeyObjectTo, type, itemType);

                    items.Add(item);
                    index++;
                }
            }
            return index != 0 ? items : null;
        }

        private static StiDatabase CreateDataBaseByTypeName(string typeDatabase)
        {
            var database = StiOptions.Services.Databases.Where(s => s.ServiceEnabled).FirstOrDefault(d => d.GetType().Name == typeDatabase);
            return database != null ? database.Clone() as StiDatabase : null;
        }

        private static StiDataAdapterService CreateDataAdapterByTypeName(string typeDataAdapter)
        {
            var adapter = StiOptions.Services.DataAdapters.FirstOrDefault(a => typeDataAdapter == a.GetType().Name);
            return adapter != null ? adapter.Clone() as StiDataAdapterService : null;
        }

        private static void CopyProperties(string[] propertyNames, Hashtable fromObject, Hashtable toObject)
        {
            foreach (string propName in propertyNames) toObject[propName] = fromObject[propName];
        }

        private static StiDataColumnsCollection GetColumnsByTypeAndNameOfObject(StiReport report, Hashtable props)
        {
            string currentParentType = (string)props["currentParentType"];
            object resultObject = null;
            if (currentParentType == "DataSource")
            {
                resultObject = report.Dictionary.DataSources[(string)props["currentParentName"]] as StiDataSource;
                if (resultObject != null) return (resultObject as StiDataSource).Columns;
            }
            if (currentParentType == "BusinessObject")
            {
                resultObject = GetBusinessObjectByFullName(report, props["currentParentName"]) as StiBusinessObject;
                return (resultObject as StiBusinessObject).Columns;
            }

            return null;
        }

        private static Hashtable GetDatabaseByName(string name, ArrayList databasesTree)
        {
            foreach (Hashtable database in databasesTree)
                if (((string)database["name"]).ToLowerInvariant() == name.ToLowerInvariant()) return database;

            return null;
        }

        private static void UpdateColumns(StiDataColumnsCollection columns, ArrayList columnsSource)
        {
            columns.Clear();
            foreach (Hashtable columnProps in columnsSource)
            {
                StiDataColumn column;
                if ((bool)columnProps["isCalcColumn"])
                    column = new StiCalcDataColumn();
                else
                    column = new StiDataColumn();
                columns.Add(column);
                ApplyColumnProps(column, columnProps);
            }
        }

        private static void UpdateParameters(StiDataParametersCollection parameters, ArrayList parametersSource)
        {
            parameters.Clear();
            foreach (Hashtable parametersProps in parametersSource)
            {
                StiDataParameter parameter = new StiDataParameter();
                parameters.Add(parameter);
                ApplyParameterProps(parameter, parametersProps);
            }
        }

        private static string GetVariableBasicType(StiVariable variable)
        {
            Dictionary.StiTypeMode typeMode;
            StiType.GetTypeModeFromType(variable.Type, out typeMode);

            return typeMode.ToString();
        }

        private static string GetVariableType(StiVariable variable)
        {
            if (variable.Type == typeof(string) || variable.Type == typeof(StringList) || variable.Type == typeof(StringRange)) return "string";
            if (variable.Type == typeof(char) || variable.Type == typeof(char?) || variable.Type == typeof(CharRange) || variable.Type == typeof(CharList)) return "char";
            if (variable.Type == typeof(bool) || variable.Type == typeof(bool?) || variable.Type == typeof(BoolList)) return "bool";
            if (variable.Type == typeof(DateTime) || variable.Type == typeof(DateTime?) || variable.Type == typeof(DateTimeList) || variable.Type == typeof(DateTimeRange)) return "datetime";
            if (variable.Type == typeof(TimeSpan) || variable.Type == typeof(TimeSpan?) || variable.Type == typeof(TimeSpanList) || variable.Type == typeof(TimeSpanRange)) return "timespan";
            if (variable.Type == typeof(Guid) || variable.Type == typeof(Guid?) || variable.Type == typeof(GuidList) || variable.Type == typeof(GuidRange)) return "guid";
            if (variable.Type == typeof(System.Drawing.Image) || variable.Type == typeof(System.Drawing.Bitmap)) return "image";
            if (variable.Type == typeof(float) || variable.Type == typeof(float?) || variable.Type == typeof(FloatList) || variable.Type == typeof(FloatRange)) return "float";
            if (variable.Type == typeof(double) || variable.Type == typeof(double?) || variable.Type == typeof(DoubleList) || variable.Type == typeof(DoubleRange)) return "double";
            if (variable.Type == typeof(Decimal) || variable.Type == typeof(Decimal?) || variable.Type == typeof(DecimalList) || variable.Type == typeof(DecimalRange)) return "decimal";
            if (variable.Type == typeof(int) || variable.Type == typeof(int?) || variable.Type == typeof(IntList) || variable.Type == typeof(IntRange)) return "int";
            if (variable.Type == typeof(uint) || variable.Type == typeof(uint?)) return "uint";
            if (variable.Type == typeof(short) || variable.Type == typeof(short?) || variable.Type == typeof(ShortList) || variable.Type == typeof(ShortRange)) return "short";
            if (variable.Type == typeof(ushort) || variable.Type == typeof(ushort?)) return "ushort";
            if (variable.Type == typeof(long) || variable.Type == typeof(long?) || variable.Type == typeof(LongList) || variable.Type == typeof(LongRange)) return "long";
            if (variable.Type == typeof(ulong) || variable.Type == typeof(ulong?)) return "ulong";
            if (variable.Type == typeof(byte) || variable.Type == typeof(byte?) || variable.Type == typeof(ByteList) || variable.Type == typeof(ByteRange)) return "byte";
            if (variable.Type == typeof(sbyte) || variable.Type == typeof(sbyte?)) return "sbyte";

            return "object";
        }

        private static object GetValueByType(string stringValue, string typeVariable, string basicType, bool canReturnNull)
        {
            string decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            if (typeVariable == "string") { return stringValue; }
            if (stringValue == "" && basicType == "NullableValue" && canReturnNull) return null;

            if (typeVariable == "float")
            {
                float value = 0;
                float.TryParse(stringValue.Replace(".", ",").Replace(",", decimalSeparator), out value);
                return value;
            }
            if (typeVariable == "double")
            {
                double value = 0;
                double.TryParse(stringValue.Replace(".", ",").Replace(",", decimalSeparator), out value);
                return value;
            }
            if (typeVariable == "decimal")
            {
                decimal value = 0;
                decimal.TryParse(stringValue.Replace(".", ",").Replace(",", decimalSeparator), out value);
                return value;
            }
            if (typeVariable == "int")
            {
                int value = 0;
                int.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "uint")
            {
                uint value = 0;
                uint.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "short")
            {
                short value = 0;
                short.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "ushort")
            {
                ushort value = 0;
                ushort.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "long")
            {
                long value = 0;
                long.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "ulong")
            {
                ulong value = 0;
                ulong.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "byte")
            {
                byte value = 0;
                byte.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "sbyte")
            {
                sbyte value = 0;
                sbyte.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "char")
            {
                char value = ' ';
                char.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "bool")
            {
                bool value = false;
                bool.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "datetime")
            {
                if (stringValue == "" && canReturnNull) return null;
                DateTime value = DateTime.Now;
                DateTime.TryParse(stringValue, en_us_culture, DateTimeStyles.None, out value);
                return value;
            }
            if (typeVariable == "timespan")
            {
                if (stringValue == "" && canReturnNull) return null;
                TimeSpan value = TimeSpan.Zero;
                TimeSpan.TryParse(stringValue, out value);
                return value;
            }
            if (typeVariable == "guid")
            {
                if (stringValue == "" && basicType == "NullableValue" && canReturnNull) return null;
                Guid variableGuid;
                try
                {
                    variableGuid = new Guid(stringValue);
                }
                catch
                {
                    variableGuid = Guid.Empty;
                }
                return variableGuid;
            }
            if (typeVariable == "object")
            {
                if (stringValue == "" && canReturnNull) return null;
                return (object)stringValue;
            }

            return null;
        }

        private static void SetDialogInfoItems(StiVariable variable, object itemsObject, string type, string basicType)
        {
            if (itemsObject == null)
            {
                variable.DialogInfo.Keys = new string[0];
                variable.DialogInfo.Values = new string[0];
                return;
            }

            ArrayList items = (ArrayList)itemsObject;
            string[] keys = new string[items.Count];
            string[] values = new string[items.Count];
            object[] valuesBinding = new object[items.Count];

            int index = 0;
            foreach (Hashtable item in items)
            {
                object keyObject = null;
                string itemType = (string)item["type"];
                string key = item["key"] != null ? StiEncodingHelper.DecodeString((string)item["key"]) : string.Empty;
                string keyTo = item["keyTo"] != null ? StiEncodingHelper.DecodeString((string)item["keyTo"]) : string.Empty;
                string value = item["value"] != null ? StiEncodingHelper.DecodeString((string)item["value"]) : string.Empty;

                try
                {
                    if (itemType == "expression")
                    {
                        if (basicType != "Range")
                            keyObject = string.Format("{{{0}}}", key);
                        else
                            keyObject = string.Format("{{{0}<<|>>{1}}}", key, keyTo);
                    }
                    else
                    {
                        if (basicType != "Range")
                        {
                            if (type != "datetime")
                            {
                                object obj = GetValueByType(key, type, basicType, false);
                                keyObject = obj != null ? obj.ToString() : "";
                            }
                            else
                                keyObject = key;
                        }
                        else
                        {
                            Range range = (Range)StiActivator.CreateObject(variable.Type);
                            range.FromObject = GetValueByType(key, type, basicType, false);
                            range.ToObject = GetValueByType(keyTo, type, basicType, false);
                            keyObject = RangeConverter.RangeToString(range);
                        }
                    }
                }
                catch
                {
                }

                keys[index] = keyObject == null ? string.Empty : keyObject.ToString();
                values[index] = StiEncodingHelper.DecodeString((string)item["value"]);
                valuesBinding[index] = (object)item["valueBinding"];
                index++;
            }
            variable.DialogInfo.Keys = keys;
            variable.DialogInfo.Values = values;
            variable.DialogInfo.ValuesBinding = valuesBinding;
        }

        public static StiBusinessObject GetBusinessObjectByFullName(StiReport report, object fullName)
        {
            StiBusinessObjectsCollection businessObjectsCollection = report.Dictionary.BusinessObjects;
            StiBusinessObject businessObject = null;
            if (fullName != null && ((ArrayList)fullName).Count != 0)
            {
                ArrayList fullNameArray = (ArrayList)fullName;
                for (int i = fullNameArray.Count - 1; i >= 0; i--)
                {
                    businessObject = businessObjectsCollection[(string)fullNameArray[i]];
                    if (businessObject != null)
                    {
                        businessObjectsCollection = businessObject.BusinessObjects;
                    }
                    else return null;
                }
            }
            return businessObject;
        }

        private static Hashtable GetAjaxDataFromDatabaseInformation(StiDatabaseInformation information)
        {
            Hashtable data = new Hashtable();
            List<DataTable>[] collections = new List<DataTable>[] { information.Tables, information.Views, information.StoredProcedures };
            string[] types = new string[] { "Table", "View", "StoredProcedure" };
            for (var i = 0; i < collections.Length; i++)
            {
                ArrayList tables = new ArrayList();
                foreach (DataTable table in collections[i])
                {
                    Hashtable item = TableItem(table);
                    if (table.ExtendedProperties["Query"] is string) item["query"] = table.ExtendedProperties["Query"];
                    item["typeItem"] = types[i];
                    tables.Add(item);
                }
                if (tables.Count > 0) data[types[i] + "s"] = tables;
            }

            return data;
        }

        public class StiSortDataTableComparer : IComparer<DataTable>
        {
            public int Compare(DataTable x, DataTable y)
            {
                if (x.TableName == null)
                {
                    if (y.TableName == null)
                        return 0;

                    return -1;
                }

                if (y.TableName == null)
                    return 1;

                return string.CompareOrdinal(x.TableName, y.TableName);
            }
        }

        private static StiDatabaseInformation ConvertAjaxDatabaseInfoToDatabaseInfo(ArrayList data, bool allInfo)
        {
            StiDatabaseInformation information = new StiDatabaseInformation();
            StiDatabaseInformation allInformation = new StiDatabaseInformation();
            foreach (Hashtable tableObject in data)
            {
                DataTable dataTable = new DataTable((string)tableObject["name"]);

                var columns = allInfo ? (ArrayList)tableObject["allColumns"] : (ArrayList)tableObject["columns"];
                foreach (Hashtable columnObject in columns)
                {
                    DataColumn dataColumn = new DataColumn((string)columnObject["name"], GetTypeFromString((string)columnObject["type"]));
                    dataTable.Columns.Add(dataColumn);
                    if ((string)tableObject["typeItem"] == "StoredProcedure")
                    {
                        dataColumn.Caption = ((string)columnObject["typeItem"] == "Parameter") ? "Parameters" : "Columns";
                    }
                }

                if (tableObject["query"] != null) dataTable.ExtendedProperties["Query"] = tableObject["query"];

                if ((string)tableObject["typeItem"] == "Table")
                {
                    information.Tables.Add(dataTable);
                    information.Tables.Sort(new StiSortDataTableComparer());
                }
                else if ((string)tableObject["typeItem"] == "View")
                {
                    information.Views.Add(dataTable);
                    information.Views.Sort(new StiSortDataTableComparer());
                }
                if ((string)tableObject["typeItem"] == "StoredProcedure")
                {
                    information.StoredProcedures.Add(dataTable);
                    information.StoredProcedures.Sort(new StiSortDataTableComparer());
                }
            }

            return information;
        }

        private static StiDataStoreSource CreateDataStoreSourceFromParams(StiReport report, Hashtable param)
        {
            StiDataStoreSource dataSource = null;
            StiDataStoreAdapterService adapterDataStore = CreateDataAdapterByTypeName((string)param["typeDataAdapter"]) as StiDataStoreAdapterService;
            if (adapterDataStore != null)
            {
                dataSource = Stimulsoft.Base.StiActivator.CreateObject(adapterDataStore.GetDataSourceType()) as StiDataStoreSource;
                if (dataSource != null)
                {
                    dataSource.Dictionary = report.Dictionary;
                    ApplyDataSourceProps(dataSource, param);
                }
            }

            return dataSource;
        }

        private static void SaveDataSourceParam(ref StiDataStoreSource dataSourceWithParam, StiReport report, StiDataStoreSource dataSource, Hashtable param)
        {
            if (dataSource == null) return;
            ArrayList currColumns = (ArrayList)param["columns"];
            ArrayList currParameters = (ArrayList)param["parameters"];

            if (dataSourceWithParam == null)
            {
                dataSourceWithParam = Stimulsoft.Base.StiActivator.CreateObject(dataSource.GetType()) as StiDataStoreSource;
            }

            #region Columns
            var columns = new StiDataColumnsCollection();
            columns.AddRange(dataSourceWithParam.Columns);

            dataSourceWithParam.Columns.Clear();

            for (int i = 0; i < currColumns.Count; i++)
            {
                StiDataColumn column;
                Hashtable columnProps = (Hashtable)currColumns[i];

                if ((bool)columnProps["isCalcColumn"])
                    column = new StiCalcDataColumn();
                else
                    column = new StiDataColumn();
                ApplyColumnProps(column, columnProps);

                var cl = dataSourceWithParam.Columns[(string)columnProps["name"]];
                if (cl == null)
                {
                    dataSourceWithParam.Columns.Add(column);
                }
                else dataSourceWithParam.Columns.Add(cl);
            }
            #endregion

            if (dataSourceWithParam is StiSqlSource)
            {
                #region Parameters
                var parameters = new StiDataParametersCollection();
                parameters.AddRange(dataSourceWithParam.Parameters);
                dataSourceWithParam.Parameters.Clear();

                for (int i = 0; i < currParameters.Count; i++)
                {
                    var parameter = new StiDataParameter();
                    ApplyParameterProps(parameter, (Hashtable)currParameters[i]);

                    var pr = dataSourceWithParam.Parameters[parameter.Name];
                    if (pr == null)
                    {
                        dataSourceWithParam.Parameters.Add(parameter);
                    }
                    else
                    {
                        dataSourceWithParam.Parameters.Add(pr);
                    }
                }
                #endregion

                ((StiSqlSource)dataSourceWithParam).SqlCommand = StiEncodingHelper.DecodeString((string)param["sqlCommand"]);

                if ((string)param["type"] == "Table")
                    ((StiSqlSource)dataSourceWithParam).Type = StiSqlSourceType.Table;
                else
                    ((StiSqlSource)dataSourceWithParam).Type = StiSqlSourceType.StoredProcedure;
            }

            dataSourceWithParam.Name = (string)param["name"];
            dataSourceWithParam.NameInSource = (string)param["nameInSource"];
            dataSourceWithParam.Alias = (string)param["alias"];

            if (dataSourceWithParam.Name == dataSourceWithParam.Alias)
                dataSourceWithParam.Alias = (string)param["name"];
            dataSourceWithParam.Name = (string)param["name"];
        }

        public static Hashtable GetViewDataItemValue(object item, StiDataColumn dictionaryColumn)
        {
            Hashtable resultItem = new Hashtable();
            Type type = dictionaryColumn != null ? dictionaryColumn.Type : item.GetType();
            string value = item.GetType() == typeof(System.Byte[]) ? Convert.ToBase64String((byte[])item) : item.ToString();

            resultItem["type"] = type.Name;
            resultItem["value"] = value;

            if (type == typeof(System.Byte[]) || type == typeof(Image))
            {
                resultItem["value"] = string.Format("data:image;base64,{0}", value);
                resultItem["type"] = "Image";
            }

            return resultItem;
        }

        public static StiFileDatabase CreateNewDatabaseFromResource(StiReport report, StiResource resource)
        {
            var databases = StiOptions.Services.Databases;

            if (resource.Type == StiResourceType.Dbf)
                return databases.FirstOrDefault(a => a.GetType() == typeof(StiDBaseDatabase))
                    .CreateNew() as StiFileDatabase;

            if (resource.Type == StiResourceType.Csv)
                return databases.FirstOrDefault(a => a.GetType() == typeof(StiCsvDatabase))
                    .CreateNew() as StiFileDatabase;

            if (resource.Type == StiResourceType.Xml)
                return databases.FirstOrDefault(a => a.GetType() == typeof(StiXmlDatabase))
                    .CreateNew() as StiFileDatabase;

            if (resource.Type == StiResourceType.Json)
                return databases.FirstOrDefault(a => a.GetType() == typeof(StiJsonDatabase))
                    .CreateNew() as StiFileDatabase;

            if (resource.Type == StiResourceType.Excel)
                return databases.FirstOrDefault(a => a.GetType() == typeof(StiExcelDatabase))
                    .CreateNew() as StiFileDatabase;

            return null;
        }

        public static string GetNewDatabaseName(StiReport report, string fileName)
        {
            string baseName = fileName.Trim();
            var counter = 1;
            while (true)
            {
                var testName = counter == 1 ? baseName : baseName + counter;

                if (!IsExistInDatabases(report, testName))
                    return testName;

                counter++;
            }
        }

        private static bool IsCategoryVariable(StiVariable variable)
        {
            return (string.IsNullOrEmpty(variable.Name) && !string.IsNullOrEmpty(variable.Category));
        }

        private static StiVariable GetVariableCategory(StiReport report, string categoryName)
        {
            foreach (StiVariable variable in report.Dictionary.Variables)
                if (variable.Category == categoryName && variable.Name == string.Empty)
                    return variable;

            return null;
        }

        #region Apply Properties
        private static void ApplyParametersToSqlSourse(StiSqlSource sqlSource, Hashtable parameters)
        {
            //Report Variables
            foreach (DictionaryEntry parameter in parameters)
            {
                string key = parameter.Key as string;
                if (key.StartsWith("{") && key.EndsWith("}"))
                {
                    sqlSource.SqlCommand = sqlSource.SqlCommand.Replace(key, StiEncodingHelper.DecodeString(parameter.Value as string));
                }
            }

            //Sql Parameters
            foreach (StiDataParameter parameter in sqlSource.Parameters)
            {
                if (parameters[parameter.Name] == null) continue;
                string text = StiEncodingHelper.DecodeString(parameters[parameter.Name] as string);
                string sep = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

                parameter.Value = text;

                Type dbType = sqlSource.ConvertDbTypeToType(parameter.Type);
                if (dbType == typeof(DateTime))
                {
                    DateTime d = new DateTime();
                    DateTime.TryParse(text, out d);
                    parameter.ParameterValue = d;
                    if (text == "")
                        parameter.ParameterValue = DBNull.Value;
                }
                else if (dbType == typeof(bool))
                {
                    Boolean b = false;
                    Boolean.TryParse(text, out b);
                    parameter.ParameterValue = b;
                }
                else if (dbType == typeof(string))
                {
                    parameter.ParameterValue = text;
                }
                else if (dbType == typeof(Int64))
                {
                    Int64 num = 0;
                    Int64.TryParse(text, out num);
                    parameter.ParameterValue = num;
                }
                else if (dbType == typeof(Decimal))
                {
                    Decimal num = 0m;
                    Decimal.TryParse(text.Replace(".", ",").Replace(",", sep), out num);
                    parameter.ParameterValue = num;
                }
                else if (dbType == typeof(Double))
                {
                    Double num = 0d;
                    Double.TryParse(text.Replace(".", ",").Replace(",", sep), out num);
                    parameter.ParameterValue = num;
                }
                else if (dbType == typeof(Guid))
                {
                    Guid g = Guid.Empty;
                    Guid.TryParse(text, out g);
                    parameter.ParameterValue = g;
                }
                else
                {
                    parameter.ParameterValue = StiConvert.ChangeType(text, dbType);
                }
            }
        }        
        
        private static void ApplyDataSourceProps(StiDataSource dataSource, Hashtable dataSourceProps)
        {
            string[] props = { "name", "nameInSource", "alias", "sqlCommand", "type", "reconnectOnEachRow", "commandTimeout" };
            foreach (string propName in props)
            {
                string propNameUpper = propName[0].ToString().ToUpperInvariant() + propName.Remove(0, 1);
                System.Reflection.PropertyInfo property = dataSource.GetType().GetProperty(propNameUpper);
                if (property != null && dataSourceProps[propName] != null)
                {
                    object p;
                    if (propName == "type")
                        p = (StiSqlSourceType)Enum.Parse(typeof(StiSqlSourceType), (string)dataSourceProps[propName]);
                    else if (propName == "sqlCommand")
                        p = StiEncodingHelper.DecodeString((string)dataSourceProps[propName]);
                    else if (propName == "reconnectOnEachRow")
                        p = (bool)dataSourceProps[propName];
                    else if (propName == "commandTimeout")
                        p = StiReportEdit.StrToInt((string)dataSourceProps[propName]);
                    else
                        p = (string)dataSourceProps[propName];
                    property.SetValue(dataSource, p, null);
                }
            }

            if (dataSource is StiVirtualSource)
            {
                ((StiVirtualSource)dataSource).NameInSource = (string)dataSourceProps["nameInSource"];
                StiReportEdit.SetSortDataProperty(dataSource, dataSourceProps["sortData"]);
                StiReportEdit.SetFilterDataProperty(dataSource, dataSourceProps["filterData"]);
                ((StiVirtualSource)dataSource).FilterOn = (bool)dataSourceProps["filterOn"];
                ((StiVirtualSource)dataSource).FilterMode = (string)dataSourceProps["filterMode"] == "And" ? StiFilterMode.And : StiFilterMode.Or;
                SetGroupColumnsAndResultsProperty(dataSource as StiVirtualSource, dataSourceProps["groupsData"], dataSourceProps["resultsData"]);
            }
        }

        private static void ApplyConnectionProps(StiDatabase database, Hashtable connectionProps, StiDictionary dictionary)
        {
            string[] props = { "name", "alias", "connectionString", "xmlType", "pathData", "pathSchema", "promptUserNameAndPassword", "codePage",
                "separator", "firstRowIsHeader" };

            foreach (string propName in props)
            {
                string propNameUpper = propName[0].ToString().ToUpperInvariant() + propName.Remove(0, 1);
                System.Reflection.PropertyInfo property = database.GetType().GetProperty(propNameUpper);
                if (property != null && connectionProps[propName] != null)
                {
                    object p;
                    if (propName == "connectionString" || propName == "pathData" || propName == "pathSchema")
                        p = StiEncodingHelper.DecodeString((string)connectionProps[propName]);
                    else if (propName == "promptUserNameAndPassword" || propName == "firstRowIsHeader") p = (bool)connectionProps[propName];
                    else if (propName == "xmlType") p = Enum.Parse(typeof(StiXmlType), (string)connectionProps[propName]);
                    else if (propName == "codePage") p = StiReportEdit.StrToInt((string)connectionProps[propName]);
                    else if (propName == "name")
                    {
                        p = (string)connectionProps[propName];
                        dictionary.RenameDatabase(database, (string)connectionProps[propName]);
                    }
                    else p = (string)connectionProps[propName];

                    property.SetValue(database, p, null);
                }
            }
        }

        private static void ApplyColumnProps(StiDataColumn column, Hashtable columnProps)
        {
            column.Name = (string)columnProps["name"];
            column.NameInSource = (string)columnProps["nameInSource"];
            column.Alias = (string)columnProps["alias"];
            if ((bool)columnProps["isCalcColumn"]) ((StiCalcDataColumn)column).Expression = StiEncodingHelper.DecodeString((string)columnProps["expression"]);
            column.Type = GetTypeFromString((string)columnProps["type"]);
        }

        private static void ApplyParameterProps(StiDataParameter parameter, Hashtable parameterProps)
        {
            parameter.Name = (string)parameterProps["name"];
            parameter.Type = StiReportEdit.StrToInt((string)parameterProps["type"]);
            parameter.Size = StiReportEdit.StrToInt((string)parameterProps["size"]);
            parameter.Expression = StiEncodingHelper.DecodeString((string)parameterProps["expression"]);
        }

        private static void ApplyRelationProps(StiReport report, StiDataRelation relation, Hashtable relationProps)
        {
            relation.Name = (string)relationProps["name"];
            relation.NameInSource = (string)relationProps["nameInSource"];
            relation.Alias = (string)relationProps["alias"];
            relation.ParentSource = report.Dictionary.DataSources[(string)relationProps["parentDataSource"]];
            relation.ChildSource = report.Dictionary.DataSources[(string)relationProps["childDataSource"]];
            relation.ChildColumns = (string[])((ArrayList)relationProps["childColumns"]).ToArray(typeof(string));
            relation.ParentColumns = (string[])((ArrayList)relationProps["parentColumns"]).ToArray(typeof(string));
        }

        private static void ApplyBusinessObjectProps(StiBusinessObject businessObject, Hashtable businessObjectProps)
        {
            businessObject.Name = (string)businessObjectProps["name"];
            businessObject.Alias = (string)businessObjectProps["alias"];
            businessObject.Category = (string)businessObjectProps["category"];
        }

        private static void ApplyVariableProps(StiReport report, StiVariable variable, Hashtable variableProps)
        {
            string type = (string)variableProps["type"];
            string basicType = (string)variableProps["basicType"];
            Dictionary.StiTypeMode typeMode = (Dictionary.StiTypeMode)Enum.Parse(typeof(Dictionary.StiTypeMode), (string)variableProps["basicType"]);
            Type variableType = Dictionary.StiType.GetTypeFromTypeMode(GetTypeFromString((string)variableProps["type"]), typeMode);
            variable.Type = variableType;

            variable.Name = (string)variableProps["name"];
            variable.Alias = (string)variableProps["alias"];
            variable.Description = StiEncodingHelper.DecodeString((string)variableProps["description"]);
            variable.Category = (string)variableProps["category"];
            if (variableProps["initBy"] != null) variable.InitBy = (StiVariableInitBy)Enum.Parse(typeof(StiVariableInitBy), (string)variableProps["initBy"]);
            variable.ReadOnly = (bool)variableProps["readOnly"];
            variable.RequestFromUser = (bool)variableProps["requestFromUser"];
            variable.AllowUseAsSqlParameter = (bool)variableProps["allowUseAsSqlParameter"];
            variable.Selection = (StiSelectionMode)Enum.Parse(typeof(StiSelectionMode), (string)variableProps["selection"]);
            variable.DialogInfo.AllowUserValues = (bool)variableProps["allowUserValues"];
            variable.DialogInfo.DateTimeType = (StiDateTimeType)Enum.Parse(typeof(StiDateTimeType), (string)variableProps["dateTimeFormat"]);
            variable.DialogInfo.ItemsInitializationType = (StiItemsInitializationType)Enum.Parse(typeof(StiItemsInitializationType), (string)variableProps["dataSource"]);
            variable.DialogInfo.Mask = StiEncodingHelper.DecodeString((string)variableProps["formatMask"]);
            variable.DialogInfo.KeysColumn = (string)variableProps["keys"];
            variable.DialogInfo.ValuesColumn = (string)variableProps["values"];
            variable.DialogInfo.BindingValue = (bool)variableProps["dependentValue"];
            variable.DialogInfo.BindingVariable = (string)variableProps["dependentVariable"] == "" ? null : report.Dictionary.Variables[(string)variableProps["dependentVariable"]];
            variable.DialogInfo.BindingValuesColumn = (string)variableProps["dependentColumn"];

            #region Set Value
            if (typeMode == StiTypeMode.Value || typeMode == StiTypeMode.NullableValue)
            {
                if (variable.Type == typeof(Image))
                {
                    variable.ValueObject = variableProps["value"] != null ? StiReportEdit.Base64ToImage((string)variableProps["value"]) : null;
                }
                else
                {
                    if (variable.InitBy == StiVariableInitBy.Value && variableProps["value"] != null)
                    {
                        variable.ValueObject = GetValueByType(StiEncodingHelper.DecodeString((string)variableProps["value"]), type, basicType, true);
                        if (variableProps["value"] as string == String.Empty) variable.Value = variableProps["value"] as string;
                    }
                    if (variable.InitBy == StiVariableInitBy.Expression && variableProps["expression"] != null)
                    {
                        string expression = StiEncodingHelper.DecodeString((string)variableProps["expression"]);
                        if (!String.IsNullOrEmpty(expression) && expression.StartsWith("{") && expression.EndsWith("}"))
                            expression = expression.Substring(1, expression.Length - 2);
                        variable.ValueObject = expression;
                    }
                }
            }
            else if (typeMode == StiTypeMode.Range)
            {
                if (variable.InitBy == StiVariableInitBy.Value && StiTypeFinder.FindType(variableType, typeof(Range)))
                {
                    Range range = Activator.CreateInstance(variableType) as Range;
                    if (variableProps["valueFrom"] != null) range.FromObject = GetValueByType(StiEncodingHelper.DecodeString((string)variableProps["valueFrom"]), type, basicType, true);
                    if (variableProps["valueTo"] != null) range.ToObject = GetValueByType(StiEncodingHelper.DecodeString((string)variableProps["valueTo"]), type, basicType, true);
                    variable.ValueObject = range;
                }
                if (variable.InitBy == StiVariableInitBy.Expression)
                {
                    string expressionFrom = StiEncodingHelper.DecodeString((string)variableProps["expressionFrom"]);
                    if (!String.IsNullOrEmpty(expressionFrom) && expressionFrom.StartsWith("{") && expressionFrom.EndsWith("}"))
                        expressionFrom = expressionFrom.Substring(1, expressionFrom.Length - 2);
                    string expressionTo = StiEncodingHelper.DecodeString((string)variableProps["expressionTo"]);
                    if (!String.IsNullOrEmpty(expressionTo) && expressionTo.StartsWith("{") && expressionTo.EndsWith("}"))
                        expressionTo = expressionTo.Substring(1, expressionFrom.Length - 2);

                    if (variableProps["expressionFrom"] != null) variable.InitByExpressionFrom = expressionFrom;
                    if (variableProps["expressionTo"] != null) variable.InitByExpressionTo = expressionTo;
                }
            }
            #endregion

            SetDialogInfoItems(variable, variableProps["items"], type, basicType);
        }

        private static void ApplyResourceProps(StiReport report, StiResource resource, Hashtable resourceProps)
        {
            resource.Name = (string)resourceProps["name"];
            resource.Alias = (string)resourceProps["alias"];

            if (resourceProps["type"] != null)
            {
                resource.Type = (StiResourceType)Enum.Parse(typeof(StiResourceType), (string)resourceProps["type"]);

                if ((bool)resourceProps["haveContent"])
                {
                    if (resourceProps["loadedContent"] != null)
                    {
                        string contentBase64Str = resourceProps["loadedContent"] as string;
                        resource.Content = Convert.FromBase64String(contentBase64Str.Substring(contentBase64Str.IndexOf("base64,") + 7));
                    }
                }
                else
                {
                    resource.Content = null;
                }
            }
        }
        #endregion

        #region Get Column Icon Type
        public static StiImagesID GetIconTypeForColumn(StiDataColumn column)
        {
            bool inherited = (column.DataSource != null)
                ? column.DataSource.Inherited
                : ((column.BusinessObject != null)
                    ? column.BusinessObject.Inherited
                    : false);

            if (column is StiCalcDataColumn)
                return GetLockedCalcImageIDFromType(column.Type, inherited);
            else
                return GetDataColumnImageIdFromType(column.Type, !inherited);
        }

        public static StiImagesID GetLockedCalcImageIDFromType(Type type, bool inherited)
        {
            if (type == typeof(bool) || type == typeof(bool?))
            {
                return inherited ?
                    StiImagesID.LockedCalcColumnBool :
                    StiImagesID.CalcColumnBool;
            }

            if (type == typeof(char) || type == typeof(char?))
            {
                return inherited ?
                    StiImagesID.LockedCalcColumnChar :
                    StiImagesID.CalcColumnChar;
            }

            if (type == typeof(DateTime) ||
                type == typeof(TimeSpan) ||
                type == typeof(DateTime?) ||
                type == typeof(TimeSpan?))
            {
                return inherited ?
                    StiImagesID.LockedCalcColumnDateTime :
                    StiImagesID.CalcColumnDateTime;
            }

            if (type == typeof(Decimal) ||
                type == typeof(Decimal?))
            {
                return inherited ?
                    StiImagesID.LockedCalcColumnDecimal :
                    StiImagesID.CalcColumnDecimal;
            }

            if (type == typeof(int) ||
                type == typeof(uint) ||
                type == typeof(long) ||
                type == typeof(ulong) ||
                type == typeof(byte) ||
                type == typeof(sbyte) ||
                type == typeof(short) ||
                type == typeof(ushort) ||
                type == typeof(int?) ||
                type == typeof(uint?) ||
                type == typeof(long?) ||
                type == typeof(ulong?) ||
                type == typeof(byte?) ||
                type == typeof(sbyte?) ||
                type == typeof(short?) ||
                type == typeof(ushort?))
            {
                return inherited ?
                    StiImagesID.LockedCalcColumnInt :
                    StiImagesID.CalcColumnInt;
            }

            if (type == typeof(float) ||
                type == typeof(double) ||
                type == typeof(float?) ||
                type == typeof(double?))
            {
                return inherited ?
                    StiImagesID.LockedCalcColumnFloat :
                    StiImagesID.CalcColumnFloat;
            }

            if (type == typeof(System.Drawing.Image))
            {
                return inherited ?
                    StiImagesID.LockedCalcColumnImage :
                    StiImagesID.CalcColumnImage;
            }

            if (type != null && type.IsArray)
            {
                return inherited ?
                    StiImagesID.LockedCalcColumnBinary :
                    StiImagesID.CalcColumnBinary;
            }

            return inherited ?
                StiImagesID.LockedCalcColumnString :
                StiImagesID.CalcColumnString;
        }

        private static StiImagesID GetDataColumnImageIdFromType(Type type, bool isDataColumn)
        {
            #region Simple Types
            if (type == typeof(bool) || type == typeof(bool?))
                return isDataColumn ? StiImagesID.DataColumnBool : StiImagesID.LockedDataColumnBool;

            if (type == typeof(char) || type == typeof(char?))
                return isDataColumn ? StiImagesID.DataColumnChar : StiImagesID.LockedDataColumnChar;

            if (type == typeof(DateTime) ||
                type == typeof(TimeSpan) ||
                type == typeof(DateTime?) ||
                type == typeof(TimeSpan?))
                return isDataColumn ? StiImagesID.DataColumnDateTime : StiImagesID.LockedDataColumnDateTime;

            if (type == typeof(Decimal) ||
                type == typeof(Decimal?))
                return isDataColumn ? StiImagesID.DataColumnDecimal : StiImagesID.LockedDataColumnDecimal;

            if (type == typeof(int) ||
                type == typeof(uint) ||
                type == typeof(long) ||
                type == typeof(ulong) ||
                type == typeof(byte) ||
                type == typeof(sbyte) ||
                type == typeof(short) ||
                type == typeof(ushort) ||
                type == typeof(int?) ||
                type == typeof(uint?) ||
                type == typeof(long?) ||
                type == typeof(ulong?) ||
                type == typeof(byte?) ||
                type == typeof(sbyte?) ||
                type == typeof(short?) ||
                type == typeof(ushort?))
                return isDataColumn ? StiImagesID.DataColumnInt : StiImagesID.LockedDataColumnInt;

            if (type == typeof(float) ||
                type == typeof(double) ||
                type == typeof(float?) ||
                type == typeof(double?))
                return isDataColumn ? StiImagesID.DataColumnFloat : StiImagesID.LockedDataColumnFloat;

            if (type == typeof(System.Drawing.Image) || type == typeof(System.Drawing.Bitmap))
                return isDataColumn ? StiImagesID.DataColumnImage : StiImagesID.LockedDataColumnImage;
            #endregion

            #region Range Types
            if (type == typeof(CharRange))
                return isDataColumn ? StiImagesID.VariableRangeChar : StiImagesID.LockedVariableRangeChar;

            if (type == typeof(DateTimeRange) ||
                type == typeof(TimeSpanRange))
                return isDataColumn ? StiImagesID.VariableRangeDateTime : StiImagesID.LockedVariableRangeDateTime;

            if (type == typeof(DecimalRange))
                return isDataColumn ? StiImagesID.VariableRangeDecimal : StiImagesID.LockedVariableRangeDecimal;

            if (type == typeof(IntRange) ||
                type == typeof(LongRange) ||
                type == typeof(ByteRange) ||
                type == typeof(ShortRange))
                return isDataColumn ? StiImagesID.VariableRangeInt : StiImagesID.LockedVariableRangeInt;

            if (type == typeof(FloatRange) ||
                type == typeof(DoubleRange))
                return isDataColumn ? StiImagesID.VariableRangeFloat : StiImagesID.LockedVariableRangeFloat;

            if (type == typeof(StringRange) ||
                type == typeof(GuidRange))
                return isDataColumn ? StiImagesID.VariableRangeString : StiImagesID.LockedVariableRangeString;
            #endregion

            #region List Types
            if (type == typeof(BoolList))
                return isDataColumn ? StiImagesID.VariableListBool : StiImagesID.LockedVariableListBool;

            if (type == typeof(StringList) ||
                type == typeof(GuidList))
                return isDataColumn ? StiImagesID.VariableListString : StiImagesID.LockedVariableListString;

            if (type == typeof(CharList))
                return isDataColumn ? StiImagesID.VariableListChar : StiImagesID.LockedVariableListChar;

            if (type == typeof(DateTimeList) ||
                type == typeof(TimeSpanList))
                return isDataColumn ? StiImagesID.VariableListDateTime : StiImagesID.LockedVariableListDateTime;

            if (type == typeof(DecimalList))
                return isDataColumn ? StiImagesID.VariableListDecimal : StiImagesID.LockedVariableListDecimal;

            if (type == typeof(IntList) ||
                type == typeof(LongList) ||
                type == typeof(ByteList) ||
                type == typeof(ShortList))
                return isDataColumn ? StiImagesID.VariableListInt : StiImagesID.LockedVariableListInt;

            if (type == typeof(FloatList) ||
                type == typeof(DoubleList))
                return isDataColumn ? StiImagesID.VariableListFloat : StiImagesID.LockedVariableListFloat;
            #endregion

            if (type != null && type.IsArray)
                return isDataColumn ? StiImagesID.DataColumnBinary : StiImagesID.LockedDataColumnBinary;

            return isDataColumn ? StiImagesID.DataColumnString : StiImagesID.LockedDataColumnString;
        }
        #endregion

        #region Type To String
        private static string GetTypeValueToString(Type type)
        {
            if (type == null) return "null";
            if (type == typeof(bool)) return "bool";
            if (type == typeof(byte)) return "byte";
            if (type == typeof(byte[])) return "byte[]";
            if (type == typeof(char)) return "char";
            if (type == typeof(DateTime)) return "datetime";
            if (type == typeof(decimal)) return "decimal";
            if (type == typeof(double)) return "double";
            if (type == typeof(Guid)) return "guid";
            if (type == typeof(short)) return "short";
            if (type == typeof(int)) return "int";
            if (type == typeof(long)) return "long";
            if (type == typeof(sbyte)) return "sbyte";
            if (type == typeof(float)) return "float";
            if (type == typeof(string)) return "string";
            if (type == typeof(TimeSpan)) return "timespan";
            if (type == typeof(ushort)) return "ushort";
            if (type == typeof(uint)) return "uint";
            if (type == typeof(ulong)) return "ulong";
            if (type == typeof(Image)) return "image";

            if (type == typeof(bool?)) return "bool (Nullable)";
            if (type == typeof(byte?)) return "byte (Nullable)";
            if (type == typeof(char?)) return "char (Nullable)";
            if (type == typeof(DateTime?)) return "datetime (Nullable)";
            if (type == typeof(decimal?)) return "decimal (Nullable)";
            if (type == typeof(double?)) return "double (Nullable)";
            if (type == typeof(Guid?)) return "guid (Nullable)";
            if (type == typeof(short?)) return "short (Nullable)";
            if (type == typeof(int?)) return "int (Nullable)";
            if (type == typeof(long?)) return "long (Nullable)";
            if (type == typeof(sbyte?)) return "sbyte (Nullable)";
            if (type == typeof(float?)) return "float (Nullable)";
            if (type == typeof(TimeSpan?)) return "timespan (Nullable)";
            if (type == typeof(ushort?)) return "ushort (Nullable)";
            if (type == typeof(uint?)) return "uint (Nullable)";
            if (type == typeof(ulong?)) return "ulong (Nullable)";

            if (type == typeof(object)) return "object";

            return type.ToString();
        }

        private static string GetTypeVariableToString(StiVariable variable)
        {


            return "";
        }
        #endregion

        #region String To Type
        private static Type GetTypeFromString(string type)
        {
            if (type == "null") return null;
            if (type == "bool") return typeof(bool);
            if (type == "byte") return typeof(byte);
            if (type == "byte[]") return typeof(byte[]);
            if (type == "char") return typeof(char);
            if (type == "datetime") return typeof(DateTime);
            if (type == "decimal") return typeof(decimal);
            if (type == "double") return typeof(double);
            if (type == "guid") return typeof(Guid);
            if (type == "short") return typeof(short);
            if (type == "int") return typeof(int);
            if (type == "long") return typeof(long);
            if (type == "sbyte") return typeof(sbyte);
            if (type == "float") return typeof(float);
            if (type == "string") return typeof(string);
            if (type == "timespan") return typeof(TimeSpan);
            if (type == "ushort") return typeof(ushort);
            if (type == "uint") return typeof(uint);
            if (type == "ulong") return typeof(ulong);
            if (type == "image") return typeof(Image);

            if (type == "bool (Nullable)") return typeof(bool?);
            if (type == "byte (Nullable)") return typeof(byte?);
            if (type == "char (Nullable)") return typeof(char?);
            if (type == "datetime (Nullable)") return typeof(DateTime?);
            if (type == "decimal (Nullable)") return typeof(decimal?);
            if (type == "double (Nullable)") return typeof(double?);
            if (type == "guid (Nullable)") return typeof(Guid?);
            if (type == "short (Nullable)") return typeof(short?);
            if (type == "int (Nullable)") return typeof(int?);
            if (type == "long (Nullable)") return typeof(long?);
            if (type == "sbyte (Nullable)") return typeof(sbyte?);
            if (type == "float (Nullable)") return typeof(float?);
            if (type == "timespan (Nullable)") return typeof(TimeSpan?);
            if (type == "ushort (Nullable)") return typeof(ushort?);
            if (type == "uint (Nullable)") return typeof(uint?);
            if (type == "ulong (Nullable)") return typeof(ulong?);

            if (type == "object") return typeof(object);

            return typeof(object);
        }
        #endregion

        #endregion

        #region Build Tree
        public static Hashtable GetDictionaryTree(StiReport report)
        {
            Hashtable dictionary = new Hashtable();
            dictionary["databases"] = GetDataBasesTree(report);
            dictionary["businessObjects"] = GetBusinessObjectsTree(report);
            dictionary["variables"] = GetVariablesTree(report);
            dictionary["systemVariables"] = GetSystemVariablesTree(report);
            dictionary["functions"] = GetFunctionsTree(report);
            dictionary["resources"] = GetResourcesTree(report);

            return dictionary;
        }

        public static ArrayList GetResourcesTree(StiReport report)
        {
            ArrayList resourcesTree = new ArrayList();
                        
            foreach (StiResource resource in report.Dictionary.Resources)
            {
                resourcesTree.Add(ResourceItem(resource));
            }

            return resourcesTree;
        }

        public static ArrayList GetFunctionsTree(StiReport report)
        {
            var dictionary = report.Dictionary;
            ArrayList functionsTree = new ArrayList();

            var hash = StiFunctions.GetFunctionsGrouppedInCategories();
            var categories = new string[hash.Keys.Count];
            hash.Keys.CopyTo(categories, 0);

            foreach (string category in categories)
            {
                var categoryItem = FunctionsCategoryItem(category, "Folder");
                functionsTree.Add(categoryItem);

                #region Sort Functions
                var list = hash[category] as List<StiFunction>;

                var hashFunctions = new Hashtable();
                foreach (StiFunction function in list)
                {
                    hashFunctions[function.GroupFunctionName] = function.GroupFunctionName;
                }

                var functions = new string[hashFunctions.Count];
                hashFunctions.Keys.CopyTo(functions, 0);
                Array.Sort(functions);
                #endregion

                foreach (string function in functions)
                {
                    var funcs = StiFunctions.GetFunctions(dictionary.Report, function, false);
                    Array.Sort(funcs);

                    var parentNode = categoryItem;
                    if (funcs.Length > 1)
                    {
                        parentNode = FunctionsCategoryItem(function, "Function");
                        ArrayList items = (ArrayList)categoryItem["items"];
                        items.Add(parentNode);
                    }

                    foreach (var func in funcs)
                    {
                        var functionItem = FunctionItem(func, report);
                        ArrayList items = (ArrayList)parentNode["items"];
                        items.Add(functionItem);
                    }
                }
            }

            return functionsTree;
        }

        public static ArrayList GetSystemVariablesTree(StiReport report)
        {
            ArrayList systemVariables = new ArrayList();
            systemVariables.AddRange(StiSystemVariablesHelper.GetSystemVariables(report));

            return systemVariables;
        }

        private static ArrayList GetDataBasesTree(StiReport report)
        {
            StiDictionary dictionary = report.Dictionary;
            ArrayList databasesTree = new ArrayList();

            #region Add Databases
            foreach (StiDatabase database in dictionary.Databases)
            {
                if (dictionary.Restrictions.IsAllowShow(database.Name, StiDataType.Database))
                    databasesTree.Add(DatabaseItem(database));
            }
            #endregion

            #region Add DataSources
            foreach (StiDataSource dataSource in dictionary.DataSources)
            {
                if (dictionary.Restrictions.IsAllowShow(dataSource.Name, Stimulsoft.Report.Dictionary.StiDataType.DataSource))
                {
                    string category = dataSource.GetCategoryName();
                    Hashtable databaseItem = GetDatabaseByName(category, databasesTree);
                    if (databaseItem == null)
                    {
                        databaseItem = DatabaseItem(category, category, category, dataSource is StiCrossTabDataSource);
                        databaseItem["isCloud"] = dataSource.IsCloud;
                        databasesTree.Add(databaseItem);
                    }
                    ((ArrayList)databaseItem["dataSources"]).Add(DatasourceItem(dataSource));
                }
            }
            #endregion                        

            return databasesTree;
        }

        private static bool IsExistInDatabases(StiReport report, string databaseName)
        {
            StiDictionary dictionary = report.Dictionary;
                        
            foreach (StiDatabase database in dictionary.Databases)
            {
                if (dictionary.Restrictions.IsAllowShow(database.Name, StiDataType.Database) && database.Name.ToLowerInvariant() == databaseName.ToLowerInvariant())
                {
                    return true;
                }   
            }

            foreach (StiDataSource dataSource in dictionary.DataSources)
            {
                if (dictionary.Restrictions.IsAllowShow(dataSource.Name, Stimulsoft.Report.Dictionary.StiDataType.DataSource))
                {
                    string category = dataSource.GetCategoryName();                    
                    if (dictionary.Databases[category] == null && category.ToLowerInvariant() == databaseName.ToLowerInvariant())
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static ArrayList GetObjectsTreeByCategories(StiReport report, CollectionBase collectionObjects)
        {
            StiDictionary dictionary = report.Dictionary;
            ArrayList mainTree = new ArrayList();
            Hashtable categories = new Hashtable();

            bool isVariablesCollection = collectionObjects is StiVariablesCollection;
            ArrayList objects;
            StiVariable variable = null;
            StiBusinessObject businessObject = null;

            foreach (object object_ in collectionObjects)
            {
                if (isVariablesCollection) variable = (StiVariable)object_;
                else businessObject = (StiBusinessObject)object_;
                string name = isVariablesCollection ? variable.Name : businessObject.Name;
                string category = isVariablesCollection ? variable.Category : businessObject.Category;
                if (!dictionary.Restrictions.IsAllowShow(name, StiDataType.Variable)) continue;

                if (category == "")
                    objects = mainTree;
                else
                {
                    if (categories[category] == null)
                    {
                        objects = new ArrayList();
                        categories[category] = objects;

                        Hashtable categoryObject = new Hashtable();
                        categoryObject["typeItem"] = "Category";
                        categoryObject["name"] = category;
                        categoryObject["categoryItems"] = objects;
                        mainTree.Add(categoryObject);
                    }
                    else
                        objects = (ArrayList)categories[category];
                }
                if (!isVariablesCollection)
                    objects.Add(BusinessObjectItem(businessObject));
                else
                    if (variable.Name.Length > 0) objects.Add(VariableItem(variable));
            }

            return mainTree;
        }

        private static ArrayList GetBusinessObjectsTree(StiReport report)
        {
            return GetObjectsTreeByCategories(report, report.Dictionary.BusinessObjects);
        }

        private static ArrayList GetChildBusinessObjectsTree(StiBusinessObject businessObject)
        {
            ArrayList businessObjets = new ArrayList();
            if (businessObject.BusinessObjects != null)
            {
                foreach (StiBusinessObject childBusinessObject in businessObject.BusinessObjects)
                    businessObjets.Add(BusinessObjectItem(childBusinessObject));
            }
            return businessObjets;
        }

        private static ArrayList GetVariablesTree(StiReport report)
        {
            return GetObjectsTreeByCategories(report, report.Dictionary.Variables);
        }

        private static ArrayList GetColumnsTree(DataColumnCollection columnsCollection)
        {
            ArrayList columns = new ArrayList();
            foreach (DataColumn column in columnsCollection)
            {
                Hashtable columnItem = new Hashtable();
                columnItem["typeItem"] = column.Caption == "Parameters" ? "Parameter" : "Column";
                columnItem["typeIcon"] = column.Caption == "Parameters" ? "Parameter" : GetDataColumnImageIdFromType(column.DataType, true).ToString();
                columnItem["type"] = GetTypeValueToString(column.DataType);
                columnItem["name"] = column.ColumnName;
                if (column.Caption == "Parameters") columnItem["expression"] = column.Expression;

                columns.Add(columnItem);
            }

            return columns;
        }

        private static ArrayList GetColumnsTree(StiDataColumnsCollection columnsCollection, bool isCloud)
        {
            ArrayList columns = new ArrayList();
            foreach (StiDataColumn column in columnsCollection)
            {
                Hashtable columnObject = ColumnItem(column);
                columnObject["isCloud"] = isCloud;
                columns.Add(columnObject);
            }

            return columns;
        }

        private static ArrayList GetColumnsTree(StiDataColumnsCollection columnsCollection)
        {
            ArrayList columns = new ArrayList();
            foreach (StiDataColumn column in columnsCollection)
                columns.Add(ColumnItem(column));

            return columns;
        }

        private static ArrayList GetParametersTree(StiDataParametersCollection parametersCollection, bool isCloud)
        {
            ArrayList parameters = new ArrayList();
            foreach (StiDataParameter parameter in parametersCollection)
            {
                Hashtable parameterObject = ParameterItem(parameter);
                parameterObject["isCloud"] = isCloud;
                parameters.Add(parameterObject);
            }

            return parameters;
        }

        private static ArrayList GetRelationsTree(StiDataRelation parentRelation, StiDataRelationsCollection relations, bool isCloud, Hashtable upLevelRelations)
        {
            ArrayList relationsTree = new ArrayList();

            foreach (StiDataRelation relation in relations)
            {
                if (parentRelation != relation)
                {
                    if (upLevelRelations[relation.NameInSource] != null) return new ArrayList();
                    Hashtable relationObject = RelationItem(relation, upLevelRelations);
                    relationObject["isCloud"] = isCloud;
                    relationsTree.Add(relationObject);
                }
            }

            return relationsTree;
        }

        private static ArrayList GetRelationsTree(StiDataRelation parentRelation, StiDataRelationsCollection relations, Hashtable upLevelRelations)
        {
            ArrayList relationsTree = new ArrayList();

            foreach (StiDataRelation relation in relations)
            {
                if (parentRelation != relation)
                    relationsTree.Add(RelationItem(relation, upLevelRelations));
            }

            return relationsTree;
        }

        private static ArrayList GetRelationsTree(DataRelationCollection relationsCollection)
        {
            ArrayList relations = new ArrayList();
            foreach (DataRelation relation in relationsCollection)
            {
                Hashtable relationItem = new Hashtable();
                relationItem["typeItem"] = "Relation";
                relationItem["typeIcon"] = "Relation";
                relationItem["name"] = relation.ParentTable.TableName;
                relationItem["correctName"] = StiNameValidator.CorrectName(relation.ParentTable.TableName);
                relationItem["nameInSource"] = relation.RelationName;
                relationItem["alias"] = relation.ParentTable.TableName;
                relationItem["parentDataSource"] = relation.ParentTable.TableName;
                relationItem["childDataSource"] = relation.ChildTable.TableName;
                ArrayList jsParentColumns = new ArrayList();
                foreach (DataColumn parentColumn in relation.ParentColumns) jsParentColumns.Add(parentColumn.ColumnName);
                relationItem["parentColumns"] = jsParentColumns;
                ArrayList jsChildColumns = new ArrayList();
                foreach (DataColumn childColumn in relation.ChildColumns) jsChildColumns.Add(childColumn.ColumnName);
                relationItem["childColumns"] = jsChildColumns;

                relations.Add(relationItem);
            }

            return relations;
        }

        #endregion

        #region Callback Methods
        public static void GetConnectionTypes(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable connections = new Hashtable();

            #region Add Report Connections

            var listCreatedConnection = new List<StiDatabase>();
            foreach (StiDatabase database in report.Dictionary.Databases)
            {
                if (!report.Dictionary.Restrictions.IsAllowShow(database.Name, StiDataType.Database)) continue;
                listCreatedConnection.Add(database);
            }

            if (listCreatedConnection.Count > 0)
            {
                connections["ReportConnections"] = new ArrayList();
                var sortedList = listCreatedConnection.OrderBy(x => x.Name);

                foreach (var database in sortedList)
                {
                    Hashtable properties = new Hashtable();
                    properties["name"] = database.Name;
                    properties["type"] = database.GetType().Name;
                    ((ArrayList)connections["ReportConnections"]).Add(properties);
                }
            }

            #endregion

            #region Get Databases Groups

            #region Add Databases To Groups

            var databases = new Dictionary<StiConnectionType, List<StiDatabase>>();
            foreach (StiDatabase data in StiOptions.Services.Databases.Where(x => (x.ServiceEnabled)))
            {
                if (data is StiUndefinedDatabase) continue;

                List<StiDatabase> list = null;
                if (databases.ContainsKey(data.ConnectionType))
                {
                    list = databases[data.ConnectionType];
                }
                else
                {
                    list = new List<StiDatabase>();
                    databases.Add(data.ConnectionType, list);
                }

                list.Add(data);
            }

            #endregion

            #region SQL

            if (databases.ContainsKey(StiConnectionType.Sql))
            {
                var list = databases[StiConnectionType.Sql].OrderBy(x => x.ConnectionOrder);
                connections["SQL"] = new ArrayList();

                foreach (var database in list)
                {
                    Hashtable properties = new Hashtable();
                    properties["name"] = database.ServiceName;
                    properties["type"] = database.GetType().Name;
                    ((ArrayList)connections["SQL"]).Add(properties);
                }
            }

            #endregion

            #region NoSQL

            if (databases.ContainsKey(StiConnectionType.NoSql))
            {
                var list = databases[StiConnectionType.NoSql].OrderBy(x => x.ConnectionOrder);
                connections["NoSQL"] = new ArrayList();

                foreach (var database in list)
                {
                    Hashtable properties = new Hashtable();
                    properties["name"] = database.ServiceName;
                    properties["type"] = database.GetType().Name;
                    ((ArrayList)connections["NoSQL"]).Add(properties);
                }
            }

            #endregion

            #region Rest

            if (databases.ContainsKey(StiConnectionType.Rest))
            {
                var list = databases[StiConnectionType.Rest].OrderBy(x => x.ConnectionOrder);
                connections["REST"] = new ArrayList();

                foreach (var database in list.OrderBy(x => x.ServiceName))
                {
                    Hashtable properties = new Hashtable();
                    properties["name"] = database.ServiceName;
                    properties["type"] = database.GetType().Name;
                    ((ArrayList)connections["REST"]).Add(properties);
                }
            }

            #endregion

            #region Files

            if (databases.ContainsKey(StiConnectionType.Other))
            {
                var list = databases[StiConnectionType.Other].OrderBy(x => x.ConnectionOrder);

                connections["Files"] = new ArrayList();
                foreach (var database in list.OrderBy(x => x.ServiceName))
                {
                    Hashtable properties = new Hashtable();
                    properties["name"] = database.ServiceName;
                    properties["type"] = database.GetType().Name;
                    ((ArrayList)connections["Files"]).Add(properties);
                }
            }

            #endregion

            #region Objects

            var adapters = StiOptions.Services.DataAdapters.Where(x => (x.ServiceEnabled && x.IsObjectAdapter));
            if (adapters.Count() > 0)
            {
                connections["Objects"] = new ArrayList();
                foreach (var adapter in adapters.OrderBy(x => x.ServiceName))
                {
                    Hashtable properties = new Hashtable();
                    properties["name"] = adapter.ServiceName;
                    properties["type"] = adapter.GetType().Name;
                    ((ArrayList)connections["Objects"]).Add(properties);
                }
            }

            #endregion

            #endregion

            callbackResult["connections"] = connections;
        }

        public static void CreateOrEditConnection(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable connectionProps = (Hashtable)param["connectionFormResult"];
            StiDatabase database = null;

            if ((string)connectionProps["mode"] == "Edit")
                database = report.Dictionary.Databases[(string)connectionProps["oldName"]];
            else
            {
                string typeConnection = (string)connectionProps["typeConnection"];
                database = CreateDataBaseByTypeName(typeConnection);
                if (database != null) report.Dictionary.Databases.Add(database);
            }

            if (database != null)
            {
                ApplyConnectionProps(database, connectionProps, report.Dictionary);
                callbackResult["itemObject"] = DatabaseItem(database);
            }
            callbackResult["mode"] = connectionProps["mode"];
            callbackResult["skipSchemaWizard"] = connectionProps["skipSchemaWizard"];
            callbackResult["databases"] = StiDictionaryHelper.GetDataBasesTree(report);
        }

        public static void DeleteConnection(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiDatabase database = report.Dictionary.Databases[(string)param["connectionName"]];
            bool canDelete = database != null && report.Dictionary.Databases.Contains(database);
            if (canDelete) report.Dictionary.Databases.Remove(database);

            Hashtable dataSourceNames = (Hashtable)param["dataSourceNames"];
            foreach (DictionaryEntry dataSourceName in dataSourceNames)
            {
                StiDataSource dataSource = report.Dictionary.DataSources[(string)dataSourceName.Value];
                if (dataSource != null)
                {
                    try
                    {
                        report.Dictionary.DataSources.Remove(dataSource);
                    }
                    catch (Exception e)
                    {
                        callbackResult["error"] = e.Message;
                    }
                }
            }

            callbackResult["deleteResult"] = canDelete;
            callbackResult["databases"] = StiDictionaryHelper.GetDataBasesTree(report);
        }

        public static void CreateOrEditRelation(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable relationProps = (Hashtable)param["relationFormResult"];
            StiDataRelation relation = null;

            if ((string)relationProps["mode"] == "Edit")
                relation = report.Dictionary.Relations[(string)relationProps["oldNameInSource"]];
            else
            {
                relation = new StiDataRelation();
                report.Dictionary.Relations.Add(relation);
            }

            if (relation != null)
            {
                ApplyRelationProps(report, relation, relationProps);
                callbackResult["itemObject"] = RelationItem(relation, new Hashtable());
            }

            CopyProperties(new string[] { "mode", "oldNameInSource", "changedChildDataSource" }, relationProps, callbackResult);
            callbackResult["databases"] = StiDictionaryHelper.GetDataBasesTree(report);
        }

        public static void DeleteRelation(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiDataRelation relation = report.Dictionary.Relations[(string)param["relationNameInSource"]];
            bool canDelete = relation != null && report.Dictionary.Relations.Contains(relation);
            if (canDelete) report.Dictionary.Relations.Remove(relation);

            callbackResult["deleteResult"] = canDelete;
            callbackResult["databases"] = StiDictionaryHelper.GetDataBasesTree(report);
        }

        public static void CreateOrEditColumn(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable columnProps = (Hashtable)param["columnFormResult"];
            StiDataColumn column = null;
            StiDataColumnsCollection columns = GetColumnsByTypeAndNameOfObject(report, columnProps);

            if (columns != null)
            {
                if ((string)columnProps["mode"] == "Edit")
                    column = columns[(string)columnProps["oldName"]];
                else
                {
                    if ((bool)columnProps["isCalcColumn"])
                        column = new StiCalcDataColumn();
                    else
                        column = new StiDataColumn();
                    columns.Add(column);
                }
            }

            if (column != null)
            {
                ApplyColumnProps(column, columnProps);
                callbackResult["itemObject"] = ColumnItem(column);
            }

            CopyProperties(new string[] { "currentParentType", "currentParentName", "mode" }, columnProps, callbackResult);

            if ((string)columnProps["currentParentType"] == "DataSource")
                callbackResult["databases"] = StiDictionaryHelper.GetDataBasesTree(report);
            else
                callbackResult["businessObjects"] = StiDictionaryHelper.GetBusinessObjectsTree(report);
        }

        public static void DeleteColumn(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            try
            {
                StiDataColumnsCollection columns = GetColumnsByTypeAndNameOfObject(report, param);

                StiDataColumn column = null;
                bool canDelete = false;
                if (columns != null)
                {
                    column = columns[(string)param["columnName"]];
                    canDelete = column != null && !column.Inherited && columns.Contains(column);
                    if (canDelete) columns.Remove(column);
                }
                callbackResult["deleteResult"] = canDelete;
                CopyProperties(new string[] { "currentParentType", "currentParentName", "mode" }, param, callbackResult);
            }
            catch (Exception e)
            {
                callbackResult["error"] = e.Message;
            }
            if ((string)param["currentParentType"] == "DataSource")
                callbackResult["databases"] = StiDictionaryHelper.GetDataBasesTree(report);
            else
                callbackResult["businessObjects"] = StiDictionaryHelper.GetBusinessObjectsTree(report);
        }

        public static void CreateOrEditParameter(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable parameterProps = (Hashtable)param["parameterFormResult"];
            StiDataSource dataSource = report.Dictionary.DataSources[(string)parameterProps["currentParentName"]] as StiDataSource;
            if (dataSource != null)
            {
                StiDataParameter parameter = null;
                StiDataParametersCollection parameters = dataSource.Parameters;

                if ((string)parameterProps["mode"] == "Edit")
                    parameter = parameters[(string)parameterProps["oldName"]];
                else
                {
                    parameter = new StiDataParameter();
                    parameters.Add(parameter);
                }

                if (parameter != null)
                {
                    ApplyParameterProps(parameter, parameterProps);
                    callbackResult["itemObject"] = ParameterItem(parameter);
                }
            }

            CopyProperties(new string[] { "currentParentName", "mode" }, parameterProps, callbackResult);
            callbackResult["databases"] = StiDictionaryHelper.GetDataBasesTree(report);
        }

        public static void DeleteParameter(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            try
            {
                StiDataSource dataSource = report.Dictionary.DataSources[(string)param["currentParentName"]] as StiDataSource;
                if (dataSource != null)
                {
                    StiDataParametersCollection parameters = dataSource.Parameters;
                    StiDataParameter parameter = null;
                    bool canDelete = false;
                    if (parameters != null)
                    {
                        parameter = parameters[(string)param["parameterName"]];
                        canDelete = parameter != null && !parameter.Inherited && parameters.Contains(parameter);
                        if (canDelete) parameters.Remove(parameter);
                    }
                    callbackResult["deleteResult"] = canDelete;
                    CopyProperties(new string[] { "currentParentName", "mode" }, param, callbackResult);
                }
            }
            catch (Exception e)
            {
                callbackResult["error"] = e.Message;
            }

            callbackResult["databases"] = StiDictionaryHelper.GetDataBasesTree(report);
        }

        public static void CreateOrEditDataSource(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable dataSourceProps = (Hashtable)param["dataSourceFormResult"];
            StiDataSource dataSource = null;
            StiDataStoreAdapterService adapterDataStore = null;

            if ((string)dataSourceProps["mode"] == "Edit")
                dataSource = report.Dictionary.DataSources[(string)dataSourceProps["oldName"]];
            else
            {
                adapterDataStore = CreateDataAdapterByTypeName((string)dataSourceProps["typeDataAdapter"]) as StiDataStoreAdapterService;
                if (adapterDataStore != null)
                {
                    var dict = report.Dictionary;
                    dataSource = adapterDataStore.Create(dict);
                    if (dataSource != null) dataSource.Connect(false);
                }
            }

            if (dataSource != null)
            {
                ApplyDataSourceProps(dataSource, dataSourceProps);
                if (!(dataSource is StiVirtualSource))
                {
                    UpdateColumns(dataSource.Columns, (ArrayList)dataSourceProps["columns"]);
                    UpdateParameters(dataSource.Parameters, (ArrayList)dataSourceProps["parameters"]);
                }
                callbackResult["itemObject"] = DatasourceItem(dataSource);
            }
            callbackResult["mode"] = dataSourceProps["mode"];
            callbackResult["databases"] = StiDictionaryHelper.GetDataBasesTree(report);
        }

        public static void DeleteDataSource(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiDataSource dataSource = report.Dictionary.DataSources[(string)param["dataSourceName"]];
            bool canDelete = dataSource != null && report.Dictionary.DataSources.Contains(dataSource);
            if (canDelete)
            {
                report.Dictionary.DataSources.Remove(dataSource);
                if (param["dataSourceNameInSource"] != null && !(dataSource is StiUserSource))
                {
                    bool canRemoveDataStore = true;
                    foreach (StiDataStoreSource dsSource in report.Dictionary.DataSources)
                    {
                        if (dsSource != null && dsSource.NameInSource == param["dataSourceNameInSource"] as string)
                        {
                            canRemoveDataStore = false;
                            break;
                        }
                    }
                    if (canRemoveDataStore)
                    {
                        var dataStore = report.Dictionary.DataStore[param["dataSourceNameInSource"] as string];
                        if (dataStore != null) report.Dictionary.DataStore.Remove(dataStore);
                    }
                }
            }
            callbackResult["deleteResult"] = canDelete;
            callbackResult["databases"] = StiDictionaryHelper.GetDataBasesTree(report);
        }

        public static void CreateOrEditBusinessObject(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable businessObjectProps = (Hashtable)param["businessObjectFormResult"];
            StiBusinessObject businessObject = null;

            if ((string)businessObjectProps["mode"] == "Edit")
                businessObject = GetBusinessObjectByFullName(report, businessObjectProps["businessObjectFullName"]);
            else
            {
                businessObject = new StiBusinessObject();
                StiBusinessObject parentBusinessObject = GetBusinessObjectByFullName(report, businessObjectProps["businessObjectFullName"]);
                StiBusinessObjectsCollection businessObjectsCollection = parentBusinessObject != null
                    ? parentBusinessObject.BusinessObjects
                    : report.Dictionary.BusinessObjects;

                businessObjectsCollection.Add(businessObject);
                if (parentBusinessObject != null) callbackResult["parentBusinessObjectFullName"] = businessObjectProps["businessObjectFullName"];
            }

            if (businessObject != null)
            {
                ApplyBusinessObjectProps(businessObject, businessObjectProps);
                UpdateColumns(businessObject.Columns, (ArrayList)businessObjectProps["columns"]);
                callbackResult["itemObject"] = BusinessObjectItem(businessObject);
            }
            callbackResult["mode"] = businessObjectProps["mode"];
            callbackResult["businessObjects"] = StiDictionaryHelper.GetBusinessObjectsTree(report);
        }

        public static void DeleteBusinessObject(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiBusinessObject businessObject = GetBusinessObjectByFullName(report, param["businessObjectFullName"]);
            bool canDelete = businessObject != null;
            if (canDelete)
            {
                StiBusinessObjectsCollection businessObjectsCollection = businessObject.ParentBusinessObject != null
                    ? businessObject.ParentBusinessObject.BusinessObjects
                    : report.Dictionary.BusinessObjects;

                businessObjectsCollection.Remove(businessObject);
            }
            callbackResult["deleteResult"] = canDelete;
            callbackResult["businessObjects"] = StiDictionaryHelper.GetBusinessObjectsTree(report);
        }

        public static void CreateOrEditVariable(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable variableProps = (Hashtable)param["variableFormResult"];
            StiVariable variable = null;

            if ((string)variableProps["mode"] == "Edit")
                variable = report.Dictionary.Variables[(string)variableProps["oldName"]];
            else
            {
                variable = new StiVariable();
                report.Dictionary.Variables.Add(variable);
            }

            if (variable != null)
            {
                ApplyVariableProps(report, variable, variableProps);
                callbackResult["itemObject"] = VariableItem(variable);
            }

            CopyProperties(new string[] { "mode", "oldName" }, variableProps, callbackResult);
            callbackResult["variables"] = StiDictionaryHelper.GetVariablesTree(report);
        }

        public static void DeleteVariable(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiVariable variable = report.Dictionary.Variables[(string)param["variableName"]];
            bool canDelete = variable != null && report.Dictionary.Variables.Contains(variable);
            if (canDelete) report.Dictionary.Variables.Remove(variable);
            callbackResult["deleteResult"] = canDelete;
            callbackResult["variables"] = StiDictionaryHelper.GetVariablesTree(report);
        }

        public static void DeleteVariablesCategory(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.Dictionary.Variables.RemoveCategory((string)param["categoryName"]);
            callbackResult["variables"] = StiDictionaryHelper.GetVariablesTree(report);
        }

        public static void EditVariablesCategory(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable categoryProps = (Hashtable)param["categoryFormResult"];
            string oldCategoryName = (string)categoryProps["oldName"];
            string newCategoryName = (string)categoryProps["name"];

            foreach (StiVariable variable in report.Dictionary.Variables)
                if (variable.Category == oldCategoryName) variable.Category = newCategoryName;

            callbackResult["newName"] = newCategoryName;
            callbackResult["variables"] = StiDictionaryHelper.GetVariablesTree(report);
        }

        public static void CreateVariablesCategory(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable categoryProps = (Hashtable)param["categoryFormResult"];
            StiVariable category = new StiVariable((string)categoryProps["name"]);
            report.Dictionary.Variables.Add(category);

            callbackResult["name"] = categoryProps["name"];
            callbackResult["variables"] = StiDictionaryHelper.GetVariablesTree(report);
        }

        public static void CreateOrEditResource(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable resourceProps = (Hashtable)param["resourceFormResult"];
            StiResource resource = null;

            if ((string)resourceProps["mode"] == "Edit")
                resource = report.Dictionary.Resources[(string)resourceProps["oldName"]];
            else
            {
                resource = new StiResource();
                report.Dictionary.Resources.Add(resource);
            }

            if (resource != null)
            {
                ApplyResourceProps(report, resource, resourceProps);
                callbackResult["itemObject"] = ResourceItem(resource);
            }

            CopyProperties(new string[] { "mode", "oldName" }, resourceProps, callbackResult);
            callbackResult["resources"] = StiDictionaryHelper.GetResourcesTree(report);
        }

        public static void DeleteResource(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiResource resource = report.Dictionary.Resources[(string)param["resourceName"]];
            bool canDelete = resource != null && report.Dictionary.Resources.Contains(resource);
            if (canDelete) report.Dictionary.Resources.Remove(resource);
            callbackResult["deleteResult"] = canDelete;
            callbackResult["resources"] = StiDictionaryHelper.GetResourcesTree(report);
        }

        public static void DeleteBusinessObjectCategory(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            ArrayList collectionForRemove = new ArrayList();
            foreach (StiBusinessObject businessObject in report.Dictionary.BusinessObjects)
                if (businessObject.Category == (string)param["categoryName"])
                    collectionForRemove.Add(businessObject);

            foreach (StiBusinessObject businessObject in collectionForRemove)
                report.Dictionary.BusinessObjects.Remove(businessObject);

            callbackResult["businessObjects"] = StiDictionaryHelper.GetBusinessObjectsTree(report);
        }

        public static void EditBusinessObjectCategory(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable categoryProps = (Hashtable)param["categoryFormResult"];
            string oldCategoryName = (string)categoryProps["oldName"];
            string newCategoryName = (string)categoryProps["name"];

            foreach (StiBusinessObject businessObject in report.Dictionary.BusinessObjects)
                if (businessObject.Category == oldCategoryName)
                    businessObject.Category = newCategoryName;

            CopyProperties(new string[] { "oldName", "name" }, categoryProps, callbackResult);
            callbackResult["businessObjects"] = StiDictionaryHelper.GetBusinessObjectsTree(report);
        }

        public static void SynchronizeDictionary(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.Dictionary.Synchronize();
            callbackResult["dictionary"] = StiDictionaryHelper.GetDictionaryTree(report);
        }

        public static void NewDictionary(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.Dictionary.Clear();
            callbackResult["dictionary"] = StiDictionaryHelper.GetDictionaryTree(report);
        }

        public static void GetAllConnections(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Assembly assembly = typeof(StiReport).Assembly;
            StiDataAdapterService dataAdapter = CreateDataAdapterByTypeName((string)param["typeDataAdapter"]);
            var result = new Hashtable();
            var resultExistsConnection = new Hashtable();

            if (dataAdapter != null)
            {
                report.Dictionary.Connect(false);
                StiDataCollection dataStore = report.DataStore;

                foreach (StiData data in dataStore)
                {
                    if (dataAdapter is StiBusinessObjectAdapterService)
                    {
                        if (data.Data is DataSet ||
                            data.Data is DataTable ||
                            data.Data is DataView ||
                            data.Data is IDbConnection ||
                            data.Data is StiUserData) continue;
                    }

                    var types = dataAdapter.GetDataTypes();
                    if (types != null)
                    {
                        foreach (Type type in types)
                        {
                            if ((StiTypeFinder.FindInterface(data.Data.GetType(), type)) ||
                                StiTypeFinder.FindType(data.Data.GetType(), type))
                            {
                                string category = dataAdapter.GetDataCategoryName(data);
                                if (result[category] == null) result[category] = new ArrayList();
                                var datas = result[category] as ArrayList;
                                if (!datas.Contains(data)) datas.Add(data.Name);
                            }
                        }
                    }
                }

                report.Dictionary.Disconnect();
            }

            callbackResult["connections"] = result;
        }

        public static void RetrieveColumns(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiDictionary dictionary = report.Dictionary;
            dictionary.Connect(false);
            ArrayList columns = new ArrayList();
            ArrayList parameters = new ArrayList();
            StiDataStoreSource dataSource = null;
            StiDataStoreSource currentDataSource = null;
            StiDataAdapterService adapter = null;
            string nameInSource = (string)param["nameInSource"];

            if ((string)param["mode"] == "Edit") currentDataSource = dictionary.DataSources[(string)param["name"]] as StiDataStoreSource;
            if (currentDataSource == null) currentDataSource = CreateDataStoreSourceFromParams(report, param);
            SaveDataSourceParam(ref dataSource, report, currentDataSource, param);

            if (nameInSource.Trim().Length == 0)
            {
                dictionary.Disconnect();
                callbackResult["error"] = string.Format(StiLocalization.Get("Errors", "FieldRequire"), StiLocalization.Get("Report", "LabelNameInSource"));
                return;
            }

            if (dataSource != null)
                foreach (StiData dt in dictionary.DataStore)
                    if (dt.Name.ToLower(System.Globalization.CultureInfo.InvariantCulture) == nameInSource.ToLower(System.Globalization.CultureInfo.InvariantCulture))
                    {
                        try
                        {
                            if (!(dataSource is StiSqlSource))
                            {
                                adapter = StiDataAdapterService.GetDataAdapter(dataSource);
                                if (adapter != null)
                                {
                                    dictionary.SynchronizeColumns(dt, dataSource);
                                    columns = GetColumnsTree(dataSource.Columns);
                                }
                            }
                            else
                            {
                                #region Get From Sql Data
                                adapter = StiDataAdapterService.GetDataAdapter(dataSource);
                                if (adapter == null) adapter = new StiSqlAdapterService();
                                var sqlSource = dataSource as StiSqlSource;
                                sqlSource.SqlCommand = StiEncodingHelper.DecodeString((string)param["sqlCommand"]);
                                sqlSource.Type = (StiSqlSourceType)Enum.Parse(typeof(StiSqlSourceType), (string)param["type"]);
                                //Apply Parameters
                                if (param["parametersValues"] != null)
                                    ApplyParametersToSqlSourse(sqlSource, (Hashtable)param["parametersValues"]);

                                //Get Columns
                                if (param["onlyParameters"] == null)
                                {
                                    columns = GetColumnsTree(adapter.GetColumnsFromData(dt, sqlSource, param["retrieveColumnsAllowRun"] != null ? CommandBehavior.KeyInfo : CommandBehavior.SchemaOnly));
                                }

                                //Get Parameters
                                if (!(adapter is StiODataAdapterService) && !(adapter is StiMongoDbAdapterService))
                                {
                                    var parametersFromData = adapter.GetParametersFromData(dt, sqlSource);
                                    parameters = GetParametersTree(parametersFromData, false);
                                }
                                #endregion
                            }
                        }
                        catch (Exception e)
                        {
                            callbackResult["error"] = e.Message;
                        }
                    }

            dictionary.Disconnect();
            callbackResult["columns"] = columns;
            callbackResult["parameters"] = parameters;
        }

        public static void GetDatabaseData(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiDatabase database = report.Dictionary.Databases[(string)param["databaseName"]];
            if (database != null)
            {
                try
                {
                    StiDatabaseInformation information = database.GetDatabaseInformation(report);
                    if (information != null) callbackResult["data"] = GetAjaxDataFromDatabaseInformation(information);
                }
                catch (Exception e)
                {
                    callbackResult["error"] = e.Message;
                }
            }
        }

        public static void ApplySelectedData(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            ArrayList data = (ArrayList)param["data"];
            string databaseName = (string)param["databaseName"];
            StiDatabase database = report.Dictionary.Databases[databaseName];
            if (database != null && data.Count > 0)
            {
                try
                {
                    StiDatabaseInformation info = ConvertAjaxDatabaseInfoToDatabaseInfo(data, false);
                    StiDatabaseInformation allInfo = ConvertAjaxDatabaseInfoToDatabaseInfo(data, true);
                    database.ApplyDatabaseInformation(info, report, allInfo);

                    //Add Relations                   
                    foreach (Hashtable dataSourceObject in data)
                    {
                        if (dataSourceObject["relations"] != null)
                        {
                            foreach (Hashtable relationObject in ((ArrayList)dataSourceObject["relations"]))
                            {
                                StiDataRelation relation = new StiDataRelation();
                                report.Dictionary.Relations.Add(relation);
                                StiDictionaryHelper.ApplyRelationProps(report, relation, relationObject);
                            }
                        }
                    }
                    //----------

                    callbackResult["dictionary"] = StiDictionaryHelper.GetDictionaryTree(report);
                    if (data != null && data.Count > 0) callbackResult["selectedDataSource"] = data[data.Count - 1];
                    callbackResult["databaseName"] = databaseName;
                }
                catch (Exception e)
                {
                    callbackResult["error"] = e.Message;
                }
            }
        }

        public static void TestConnection(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            string typeConnection = (string)param["typeConnection"];
            string connectionString = StiEncodingHelper.DecodeString((string)param["connectionString"]);

            var database = CreateDataBaseByTypeName(typeConnection);

            if (database is StiSqlDatabase)
                callbackResult["testResult"] = ((StiSqlDatabase)database).GetDataAdapter().TestConnection(connectionString);
            else if (database is StiNoSqlDatabase)
                callbackResult["testResult"] = ((StiNoSqlDatabase)database).GetDataAdapter().TestConnection(connectionString);
        }

        public static void RunQueryScript(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiDictionary dictionary = report.Dictionary;
            StiDataStoreSource dataSource = null;
            StiDataStoreSource currentDataSource = null;

            try
            {
                if ((string)param["mode"] == "Edit") currentDataSource = dictionary.DataSources[(string)param["name"]] as StiDataStoreSource;
                if (currentDataSource == null) currentDataSource = CreateDataStoreSourceFromParams(report, param);
                SaveDataSourceParam(ref dataSource, report, currentDataSource, param);

                if (param["parametersValues"] != null && dataSource is StiSqlSource) ApplyParametersToSqlSourse(dataSource as StiSqlSource, (Hashtable)param["parametersValues"]);

                if (dataSource != null)
                {
                    if (dataSource.NameInSource == null ||
                        dataSource.NameInSource.Trim().Length == 0)
                    {
                        callbackResult["resultQueryScript"] = string.Format(StiLocalization.Get("Errors", "FieldRequire"), StiLocalization.Get("Report", "LabelNameInSource"));
                        return;
                    }

                    try
                    {
                        dataSource.Dictionary = dictionary;
                        dataSource.Dictionary.ConnectToDatabases(true);
                        dataSource.Connect(true);
                        callbackResult["resultQueryScript"] = "successfully";
                    }
                    finally
                    {
                        dataSource.Disconnect();
                        dictionary.Disconnect();
                    }
                }
            }
            catch (Exception ee)
            {
                callbackResult["resultQueryScript"] = ee.Message;
            }
        }

        public static void ViewData(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiDictionary dictionary = report.Dictionary;
            StiDataStoreSource dataSource = null;
            StiDataStoreSource currentDataSource = null;

            if ((string)param["typeDataAdapter"] == "StiVirtualAdapterService")
            {
                dictionary.Connect();
                dictionary.ConnectVirtualDataSources();
            }
            else
            {
                dictionary.Connect(false);
            }

            try
            {
                if ((string)param["mode"] == "Edit") currentDataSource = dictionary.DataSources[(string)param["name"]] as StiDataStoreSource;
                if (currentDataSource == null) currentDataSource = CreateDataStoreSourceFromParams(report, param);
                SaveDataSourceParam(ref dataSource, report, currentDataSource, param);

                if (dataSource != null)
                {
                    dataSource.Dictionary = dictionary;
                    dataSource.Connect((string)param["typeDataAdapter"] == "StiVirtualAdapterService");

                    if (param["parametersValues"] != null && dataSource is StiSqlSource) ApplyParametersToSqlSourse(dataSource as StiSqlSource, (Hashtable)param["parametersValues"]);

                    var viewDataHelper = new StiViewDataHelper(dataSource);
                    DataTable dataTable = viewDataHelper.ResultDataTable;

                    ArrayList resultData = new ArrayList();
                    List<StiDataColumn> dictionaryColumns = new List<StiDataColumn>();

                    if (dataTable != null)
                    {
                        ArrayList captions = new ArrayList();
                        for (int k = 0; k < dataTable.Columns.Count; k++)
                        {
                            StiDataColumn dictionaryColumn = dataSource.Columns[dataTable.Columns[k].Caption];
                            captions.Add(dataTable.Columns[k].Caption);
                            dictionaryColumns.Add(dictionaryColumn);
                        }
                        resultData.Add(captions);

                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            ArrayList rowArray = new ArrayList();
                            resultData.Add(rowArray);
                            for (int k = 0; k < dataTable.Columns.Count; k++)
                            {
                                rowArray.Add(GetViewDataItemValue(dataTable.Rows[i][k], dictionaryColumns[k]));
                            }
                        }
                    }

                    callbackResult["resultData"] = resultData;
                    callbackResult["dataSourceName"] = dataSource.Name;
                }
            }
            catch (Exception e)
            {
                callbackResult["error"] = e.Message;
            }
            finally
            {
                dataSource.Disconnect();
                dictionary.Disconnect();
            }
        }

        public static void GetSqlParameterTypes(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            string typeDataSource = (string)((Hashtable)param["dataSource"])["typeDataSource"];
            var dataSource = StiOptions.Services.DataSource.FirstOrDefault(a => typeDataSource == a.GetType().Name);

            if (dataSource != null && dataSource is StiSqlSource && !(dataSource is StiMongoDbSource))
            {
                callbackResult["sqlParameterTypes"] = GetDataParameterTypes(dataSource as StiSqlSource);
            }
        }

        public static void CreateFieldOnDblClick(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiDataColumn column = null;
            StiDataParameter parameter = null;
            StiDataSource dataSource = null;
            StiDataBand dataBand = null;
            StiDataBand firstDataBand = null;
            StiDataBand selectedDataBand = null;
            StiPage currentPage = report.Pages[(string)param["pageName"]];
            ArrayList selectedComponentNames = (ArrayList)param["selectedComponents"];
            ArrayList newComponents = new ArrayList();
            double zoom = StiReportEdit.StrToDouble((string)param["zoom"]);

            if (param["columnName"] != null)
            {
                StiDataColumnsCollection columns = GetColumnsByTypeAndNameOfObject(report, param);
                if (columns != null) column = columns[(string)param["columnName"]];
                if (column != null) dataSource = column.DataSource;
            }
            else if (param["parameterName"] != null)
            {
                dataSource = report.Dictionary.DataSources[(string)param["currentParentName"]] as StiDataSource;
                if (dataSource != null)
                {
                    StiDataParametersCollection parameters = dataSource.Parameters;
                    if (parameters != null) parameter = parameters[(string)param["parameterName"]];
                }
            }

            //set not selected all components            
            foreach (StiComponent component in currentPage.GetComponents())
            {
                component.IsSelected = false;
            }

            //set selected current components
            foreach (string componentName in selectedComponentNames)
            {
                StiComponent component = report.GetComponentByName(componentName);
                if (component != null) component.Select();
            }

            #region Search DataBand
            var selectedDataBands = new List<StiDataBand>();

            var comps = currentPage.GetComponents();
            foreach (StiComponent component in comps)
            {
                var band = component as StiDataBand;
                if (band == null) continue;

                if (firstDataBand == null) firstDataBand = band;
                if (selectedDataBand == null && band.IsSelected) selectedDataBand = band;
                if (band.DataSource == dataSource)
                {
                    selectedDataBands.Add(band);
                }
            }

            if (selectedDataBands.Count > 0)
            {
                foreach (var band in selectedDataBands)
                {
                    if (dataBand == null) dataBand = band;
                    else if (((!dataBand.IsSelected) && band.IsSelected))
                    {
                        dataBand = band;
                        break;
                    }
                }
            }
            #endregion

            if (dataBand == null)
            {
                if (selectedDataBand != null) dataBand = selectedDataBand;
                else dataBand = firstDataBand;
            }

            if (dataBand != null)
            {
                StiBaseStyle style = new StiStyle();
                double posX = 0;
                foreach (StiComponent comp in dataBand.Components)
                {
                    if (posX < comp.Right)
                    {
                        posX = comp.Right;
                        if (!(comp is IStiIgnoryStyle)) style = StiBaseStyle.GetStyle(comp);
                    }
                }

                double width = StiAlignValue.AlignToMaxGrid(dataBand.Width / 8,
                    currentPage.GridSize, true);

                var rect = new RectangleD(posX, 0, width, dataBand.Height);

                #region DataBands
                StiComponent newComp = null;

                if (param["resourceName"] != null)
                {
                    if (param["resourceType"] as string == "Image")
                        newComp = new StiImage(rect)
                        {
                            ImageURL = new StiImageURLExpression(StiHyperlinkProcessor.CreateResourceName(param["resourceName"] as string))
                        };
                    else if (param["resourceType"] as string == "Rtf")
                        newComp = new StiRichText(rect)
                        {
                            DataUrl = new StiDataUrlExpression(StiHyperlinkProcessor.CreateResourceName(param["resourceName"] as string))
                        };
                }
                else
                {
                    if (column != null && column.Type == typeof(Image))
                    {
                        newComp = new StiImage(rect)
                        {
                            DataColumn = column.GetColumnPath()
                        };
                    }
                    else
                    {
                        newComp = new StiText(rect)
                        {
                            Text = (string)param["fullName"],
                            Type = StiSystemTextType.DataColumn
                        };
                    }
                }
                
                if (!(newComp is IStiIgnoryStyle))
                {
                    if (param["lastStyleProperties"] != null)
                        StiReportEdit.SetAllProperties(newComp, param["lastStyleProperties"] as ArrayList);
                    else
                        style.SetStyleToComponent(newComp);
                }

                dataBand.Components.Add(newComp);
                newComponents.Add(StiReportEdit.GetComponentMainProperties(newComp, zoom));
                #endregion

                var builder = StiDataBandV1Builder.GetBuilder(typeof(StiDataBand)) as StiDataBandV1Builder;
                var headers = builder.GetHeaders(dataBand);

                #region Headers
                foreach (StiHeaderBand header in headers)
                {
                    posX = 0;
                    foreach (StiComponent comp in header.Components)
                    {
                        if (posX < comp.Right)
                        {
                            posX = comp.Right;
                            if (!(comp is IStiIgnoryStyle)) style = StiBaseStyle.GetStyle(comp);
                        }
                    }

                    var text = new StiText(rect)
                    {
                        Height = header.Height,
                        HorAlignment = StiTextHorAlignment.Center
                    };

                    if (!(text is IStiIgnoryStyle)) style.SetStyleToComponent(text);

                    if (param["resourceName"] != null)
                    {
                        text.Text = param["resourceName"] as string;
                    }
                    else if (column != null)
                    {
                        text.Text = ((StiDataColumn)column).Alias;
                    }
                    else if (parameter != null)
                    {
                        string str = ((StiDataParameter)parameter).Name;
                        if (str.StartsWith("@", StringComparison.InvariantCulture)) str = str.Substring(1);
                        text.Text = str;
                    }                    
                    header.Components.Add(text);
                    newComponents.Add(StiReportEdit.GetComponentMainProperties(text, zoom));
                }
                #endregion
            }

            callbackResult["pageName"] = currentPage.Name;
            callbackResult["newComponents"] = newComponents;
            callbackResult["rebuildProps"] = StiReportEdit.GetPropsRebuildPage(report, currentPage);
        }

        public static void GetParamsFromQueryString(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiSqlSource dataSource = report.Dictionary.DataSources[(string)param["dataSourceName"]] as StiSqlSource;
            if (dataSource != null)
            {
                string queryString = StiEncodingHelper.DecodeString((string)param["queryString"]);
                var exps = dataSource.AllowExpressions ? StiCodeDomExpressionHelper.GetLexem(queryString) : new List<string>();
                var parameters = new ArrayList();
                foreach (string value in exps)
                {
                    if (value.StartsWith("{") && value.EndsWith("}"))
                        parameters.Add(value);
                }

                callbackResult["params"] = parameters;
            }
        }

        public static void GetImagesGallery(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            #region Variables
            var variables = StiGalleriesHelper.GetImageVariables(report);
            ArrayList resultVariables = new ArrayList();

            foreach (var variable in variables)
            {
                resultVariables.Add(ImagesGalleryItem(variable.Name, typeof(StiVariable), StiReportEdit.ImageToBase64(variable.ValueObject as Image)));
            }
            #endregion

            #region Resources
            var resources = StiGalleriesHelper.GetImageResources(report);
            ArrayList resultResources = new ArrayList();

            foreach (var resource in resources)
            {
                string base64Content = string.Empty;
                if (resource.Content != null)
                {
                    if (Stimulsoft.Report.Helpers.StiImageHelper.IsMetafile(resource.Content))
                        base64Content = StiReportEdit.GetBase64PngFromMetaFileBytes(resource.Content);
                    else
                        base64Content = StiReportEdit.ImageToBase64(resource.Content);
                }
                resultResources.Add(ImagesGalleryItem(resource.Name, typeof(StiResource), base64Content));
            }
            #endregion

            #region Columns
            ArrayList resultColumns = new ArrayList();
            if (StiOptions.Designer.Editors.AllowConnectToDataInGallery)
            {
                try
                {
                    var columns = StiGalleriesHelper.GetImageColumns(report);
                    var dataSources = columns.Select(c => c.DataSource).Distinct().ToList();
                    report.Dictionary.Connect(true, dataSources);

                    foreach (var column in columns)
                    {
                        var image = StiGalleriesHelper.GetImageFromColumn(column, report);
                        if (image == null) continue;

                        var columnPath = column.GetColumnPath();
                        resultColumns.Add(ImagesGalleryItem(column.GetColumnPath(), typeof(StiDataColumn), StiReportEdit.ImageToBase64(image)));

                    }
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
            }
            #endregion

            Hashtable imagesGallery = new Hashtable();
            imagesGallery["variables"] = resultVariables;
            imagesGallery["resources"] = resultResources;
            imagesGallery["columns"] = resultColumns;

            callbackResult["imagesGallery"] = imagesGallery;
        }

        public static void GetRichTextGallery(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            #region Variables
            var variables = StiGalleriesHelper.GetRichTextVariables(report);
            ArrayList resultVariables = new ArrayList();

            foreach (var variable in variables)
            {
                string imageName = "Resources." + (variable.ValueObject is string && StiRtfHelper.IsRtfText(variable.ValueObject as string) ? "BigResourceRtf" : "BigResourceTxt");

                resultVariables.Add(RichTextGalleryItem(variable.Name, variable.GetType(), imageName));
            }
            #endregion

            #region Resources
            var resources = StiGalleriesHelper.GetRichTextResources(report);
            ArrayList resultResources = new ArrayList();

            foreach (var resource in resources)
            {
                resultResources.Add(RichTextGalleryItem(resource.Name, resource.GetType(), "Resources.BigResource" + resource.Type.ToString()));
            }
            #endregion

            #region Columns
            ArrayList resultColumns = new ArrayList();
            if (StiOptions.Designer.Editors.AllowConnectToDataInGallery)
            {
                try
                {
                    var columns = StiGalleriesHelper.GetRichTextColumns(report);
                    var dataSources = columns.Select(c => c.DataSource).Distinct().ToList();
                    report.Dictionary.Connect(true, dataSources);

                    foreach (var column in columns)
                    {
                        if (!StiGalleriesHelper.IsRtfColumn(column, report)) continue;

                        var columnPath = column.GetColumnPath();
                        resultColumns.Add(RichTextGalleryItem(column.GetColumnPath(), column.GetType(), "Resources.BigResourceRtf"));

                    }
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
            }
            #endregion

            Hashtable richTextGallery = new Hashtable();
            richTextGallery["variables"] = resultVariables;
            richTextGallery["resources"] = resultResources;
            richTextGallery["columns"] = resultColumns;

            callbackResult["richTextGallery"] = richTextGallery;
        }

        public static void GetSampleConnectionString(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            string typeConnection = (string)param["typeConnection"];
            var database = CreateDataBaseByTypeName(typeConnection);
            if (database is StiSqlDatabase)
            {
                callbackResult["connectionString"] = ((StiSqlDatabase)database).CreateSqlConnector().GetSampleConnectionString();
            }
        }

        public static void CreateDatabaseFromResource(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable resourceProps = param["resourceObject"] as Hashtable;
            StiResource resource = new StiResource();
            report.Dictionary.Resources.Add(resource);
            ApplyResourceProps(report, resource, resourceProps);
            callbackResult["resourceItemObject"] = ResourceItem(resource);
            callbackResult["resources"] = StiDictionaryHelper.GetResourcesTree(report);

            StiFileDatabase database = CreateNewDatabaseFromResource(report, resource);
            if (database != null)
            {
                database.Name = database.Alias = GetNewDatabaseName(report, resource.Name);
                database.PathData = StiHyperlinkProcessor.ResourceIdent + resource.Name;
                report.Dictionary.Databases.Add(database);
                database.CreateDataSources(report.Dictionary);

                callbackResult["newDataBaseName"] = database.Name;
                callbackResult["databases"] = GetDataBasesTree(report);
            }
        }
        
        public static void DeleteAllDataSources(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            if (param["dataSources"] != null)
            {
                ArrayList dataSources = param["dataSources"] as ArrayList;
                foreach (Hashtable dataSourceObject in dataSources)
                {
                    var dataSource = report.Dictionary.DataSources[dataSourceObject["name"] as string];
                    var connectionName = dataSource.GetCategoryName();
                    if (dataSource != null && (param["connectionName"] == null || connectionName == param["connectionName"] as string))
                    {
                        report.Dictionary.DataSources.Remove(dataSource);
                        if (dataSourceObject["nameInSource"] != null)
                        {
                            var dataStore = report.Dictionary.DataStore[dataSourceObject["nameInSource"] as string];
                            if (dataStore != null) report.Dictionary.DataStore.Remove(dataStore);
                        }
                        if (!String.IsNullOrEmpty(connectionName)) {
                            var database = report.Dictionary.Databases[connectionName];
                            if (database != null) {
                                bool allowDelete = true;
                                foreach (StiDataSource dataSource_ in report.Dictionary.DataSources) {
                                    if (dataSource_.GetCategoryName() == connectionName)
                                    {
                                        allowDelete = false;
                                        break;
                                    }                                    
                                }
                                if (allowDelete) report.Dictionary.Databases.Remove(database);
                            } 
                        }
                    }
                }
            }
            else
            {
                report.Dictionary.DataSources.Clear();
                report.Dictionary.Databases.Clear();
                report.Dictionary.DataStore.Clear();
                report.Dictionary.Synchronize();
            }

            callbackResult["databases"] = GetDataBasesTree(report);
        }

        public static void GetVariableItemsFromDataColumn(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.Dictionary.Connect(false);
            ArrayList items = new ArrayList();
            var keys = StiDataColumn.GetDatasFromDataColumn(report.Dictionary, param["keysColumn"] as string);
            var values = StiDataColumn.GetDatasFromDataColumn(report.Dictionary, param["valuesColumn"] as string);

            if (keys != null || values != null)
            {
                int maxLength = Math.Max(keys.Length, values.Length);
                for (int index = 0; index < maxLength; index++)
                {
                    Hashtable item = new Hashtable();
                    item["key"] = index < keys.Length ? keys[index] as string : String.Empty;
                    item["value"] = index < values.Length ? values[index] as string : String.Empty;
                    items.Add(item);
                }
            }

            report.Dictionary.Disconnect();
            callbackResult["items"] = items;
        }

        public static void MoveDictionaryItem(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable fromObject = param["fromObject"] as Hashtable;
            Hashtable toObject = param["toObject"] as Hashtable;
            string direction = param["direction"] as string;

            if (fromObject != null && toObject != null)
            {
                string typeItem = fromObject["typeItem"] as string;

                if ((typeItem == "Variable" || typeItem == "Category"))
                {
                    #region Move Variables 
                    StiVariable fromVariable = fromObject["typeItem"] as string == "Category"
                        ? GetVariableCategory(report, fromObject["name"] as string) : report.Dictionary.Variables[fromObject["name"] as string];

                    StiVariable toVariable = toObject["typeItem"] as string == "Category"
                        ? GetVariableCategory(report, toObject["name"] as string) : report.Dictionary.Variables[toObject["name"] as string];

                    if ((toVariable == null && toObject["typeItem"] as string != "VariablesMainItem") || fromVariable == null || fromVariable == toVariable)
                        return;

                    #region variable to variables main item
                    if (toObject["typeItem"] as string == "VariablesMainItem")
                    {
                        fromVariable.Category = string.Empty;
                        report.Dictionary.Variables.Add(fromVariable);
                        callbackResult["moveCompleted"] = true;
                    }
                    #endregion

                    #region variable to variable
                    else if (!IsCategoryVariable(fromVariable) && !IsCategoryVariable(toVariable))
                    {
                        int fromIndex = report.Dictionary.Variables.IndexOf(fromVariable);
                        int toIndex = report.Dictionary.Variables.IndexOf(toVariable);

                        report.Dictionary.Variables.Remove(fromVariable);
                        int toIndex2 = report.Dictionary.Variables.IndexOf(toVariable);

                        if (fromIndex < toIndex)
                            report.Dictionary.Variables.Insert(toIndex2 + 1, fromVariable);
                        else
                            report.Dictionary.Variables.Insert(toIndex2, fromVariable);

                        fromVariable.Category = toVariable.Category;
                        callbackResult["moveCompleted"] = true;
                    }
                    #endregion

                    #region variable to category
                    else if (!IsCategoryVariable(fromVariable) && IsCategoryVariable(toVariable))
                    {
                        int index = report.Dictionary.Variables.GetLastCategoryIndex(toVariable.Category);
                        report.Dictionary.Variables.Remove(fromVariable);
                        if (index + 1 < report.Dictionary.Variables.Count)
                        {
                            report.Dictionary.Variables.Insert(index, fromVariable);
                        }
                        else
                        {
                            report.Dictionary.Variables.Add(fromVariable);
                        }

                        fromVariable.Category = toVariable.Category;
                        callbackResult["moveCompleted"] = true;
                    }
                    #endregion

                    #region category to category
                    else if (IsCategoryVariable(fromVariable) && IsCategoryVariable(toVariable))
                    {
                        report.Dictionary.Variables.MoveCategoryTo(fromVariable.Category, toVariable.Category);
                        callbackResult["moveCompleted"] = true;
                    }
                    #endregion
                    else
                    {
                        int index = report.Dictionary.Variables.IndexOf(toVariable);
                        report.Dictionary.Variables.Remove(fromVariable);
                        report.Dictionary.Variables.Insert(index, fromVariable);

                        if (!IsCategoryVariable(fromVariable))
                            fromVariable.Category = toVariable.Category;

                        callbackResult["moveCompleted"] = true;
                    }
                    #endregion
                }
                else if (typeItem == "DataSource")
                {
                    #region Move DataSource
                    StiDataSource fromDataSource = report.Dictionary.DataSources[fromObject["name"] as string];
                    StiDataSource toDataSource = report.Dictionary.DataSources[toObject["name"] as string];

                    if (fromDataSource != null && toDataSource != null)
                    {
                        //store relations for this datasource
                        var relations = new StiDataRelationsCollection(report.Dictionary);

                        foreach (StiDataRelation relation in report.Dictionary.Relations)
                        {
                            if (relation.ParentSource == fromDataSource || relation.ChildSource == fromDataSource)
                            {
                                relations.Insert(0, relation);
                            }
                        }

                        int index = report.Dictionary.DataSources.IndexOf(toDataSource);
                        report.Dictionary.DataSources.Remove(fromDataSource);
                        report.Dictionary.DataSources.Insert(index, fromDataSource);

                        //restore relations
                        if (relations.Count > 0)
                        {
                            foreach (StiDataRelation relation in relations)
                            {
                                report.Dictionary.Relations.Add(relation);
                            }
                            relations.Clear();
                        }

                        callbackResult["moveCompleted"] = true;
                    }
                    #endregion;
                }
                else if (typeItem == "Column")
                {
                    #region Move Column
                    StiDataColumnsCollection columns = GetColumnsByTypeAndNameOfObject(report, param);

                    if (columns != null)
                    {
                        StiDataColumn fromColumn = columns[fromObject["name"] as string];
                        StiDataColumn toColumn = columns[toObject["name"] as string];

                        if (toColumn == null || fromColumn == null || fromColumn == toColumn)
                            return;

                        int index = columns.IndexOf(toColumn);
                        columns.Remove(fromColumn);
                        columns.Insert(index, fromColumn);
                        callbackResult["moveCompleted"] = true;
                    }
                    #endregion
                }

                callbackResult["direction"] = direction;
                callbackResult["fromObject"] = param["fromObject"];
                callbackResult["toObject"] = param["toObject"];
                callbackResult["variablesTree"] = GetVariablesTree(report);
            }
        }
        #endregion
    }
}