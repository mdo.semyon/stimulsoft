﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Globalization;
using System.Collections;
using Stimulsoft.Report.Components;

namespace Stimulsoft.Report.MobileDesign
{
    internal class StiDesignReportHelper
    {
        private StiReport report = null;
        
        public Hashtable GetReportToObject()
        {            
            CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            Hashtable reportObject = new Hashtable();
            reportObject["zoom"] = StiReportEdit.DoubleToStr(report != null && report.Info != null ? report.Info.Zoom : 1d);
            reportObject["gridSize"] = StiReportEdit.DoubleToStr(report != null && report.Unit != null ? report.Unit.ConvertToHInches(report.Info.GridSize) : 1d);
            reportObject["dictionary"] = StiDictionaryHelper.GetDictionaryTree(report);
            reportObject["stylesCollection"] = StiStylesHelper.GetStyles(report);
            reportObject["pages"] = GetPages();
            reportObject["properties"] = StiReportEdit.GetReportProperties(report);
            reportObject["info"] = GetReportInfo();

            System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture;

            return reportObject;
        }
        
        public ArrayList GetPages()
        {
            ArrayList pages = new ArrayList();
            for (int pageIndex = 0; pageIndex < report.Pages.Count; pageIndex++) {
                Hashtable page = GetPage(pageIndex);
                if (page != null) pages.Add(page);
            }
            return pages;
        }

        public Hashtable GetPage(int pageIndex)
        {
            StiPage page = report.Pages[pageIndex];
            page.Correct(true);

            Hashtable pageObject = new Hashtable();
            pageObject["name"] = page.Name;
            pageObject["pageIndex"] = pageIndex.ToString();
            pageObject["properties"] = StiReportEdit.GetAllProperties(page);
            if (((Hashtable)pageObject["properties"]).Count == 0) return null;
            pageObject["components"] = GetComponents(page);
            
            return pageObject;
        }

        private ArrayList GetComponents(StiPage page)
        {
            ArrayList components = new ArrayList();

            foreach (StiComponent comp in page.GetComponents())
            {
                components.Add(GetComponent(comp));
            }

            return components;
        }

        private Hashtable GetComponent(StiComponent component)
        {
            Hashtable compObject = new Hashtable();

            compObject["name"] = component.Name;
            compObject["typeComponent"] = component.GetType().Name;
            compObject["componentRect"] = StiReportEdit.GetComponentRect(component);
            compObject["parentName"] = StiReportEdit.GetParentName(component);
            compObject["parentIndex"] = StiReportEdit.GetParentIndex(component).ToString();
            compObject["componentIndex"] = StiReportEdit.GetComponentIndex(component).ToString();
            compObject["childs"] = StiReportEdit.GetAllChildComponents(component);
            compObject["svgContent"] = StiReportEdit.GetSvgContent(component, 1);
            compObject["pageName"] = component.Page.Name;
            compObject["properties"] = StiReportEdit.GetAllProperties(component);

            return compObject;
        }

        private Hashtable GetReportInfo()
        {
            Hashtable properties = new Hashtable();
            string[] propNames = { "ShowHeaders", "ShowRulers", "ShowOrder", "RunDesignerAfterInsert", "UseLastFormat", "ShowDimensionLines", "GenerateLocalizedName", "AlignToGrid",
                "ShowGrid", "GridMode", "GridSizeInch", "GridSizeHundredthsOfInch", "GridSizeCentimetres", "GridSizeMillimeters", "GridSizePixels", "QuickInfoType", "QuickInfoOverlay",
                "AutoSaveInterval", "EnableAutoSaveMode" };
            foreach (string propName in propNames)
            {
                var value = StiReportEdit.GetPropertyValue(propName, report.Info);
                if (value != null) { properties[StiReportEdit.LowerFirstChar(propName)] = value; }
            }

            return properties;
        }

        public StiDesignReportHelper(StiReport report)
        {
            this.report = report;
        }
    }
}