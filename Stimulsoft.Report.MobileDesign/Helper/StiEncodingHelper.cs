﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;

namespace Stimulsoft.Report.MobileDesign
{
    internal static class StiEncodingHelper
    {
        #region Methods.Encode/Decode
        public static string DecodeString(string xml)
        {
            string result = string.Empty;
            try
            {
                result = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(xml));
            }
            catch { }

            return result;
        }

        public static string Encode(string xml)
        {
            return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(xml));
        }
        #endregion
    }
}