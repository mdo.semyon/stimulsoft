﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Drawing.Imaging;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Helpers;
using System.Data;

namespace Stimulsoft.Report.MobileDesign
{
    internal class StiResourcesHelper
    {
        internal static string GetReportThumbnailParameters(StiReport report, double zoom)
        {
            if (!report.IsRendered)
            {
                try
                {
                    report.Render(false);
                }
                catch
                {
                    report = new StiReport();
                }
            }
            if (report.RenderedPages.Count == 0) return string.Empty;

            StiOptions.Export.Html.UseComponentStyleName = false;
            StiHtmlExportService service = new StiHtmlExportService();
            service.RenderAsDocument = false;
            service.Styles = new ArrayList();
            service.ClearOnFinish = false;
            service.RenderStyles = false;

            StiHtmlExportSettings settings = new StiHtmlExportSettings();
            settings.PageRange = new StiPagesRange(0);
            settings.Zoom = zoom;
            settings.ImageFormat = ImageFormat.Png;
            settings.ExportQuality = StiHtmlExportQuality.High;
            settings.ExportBookmarksMode = StiHtmlExportBookmarksMode.ReportOnly;
            settings.RemoveEmptySpaceAtBottom = false;
            settings.UseWatermarkMargins = true;

            MemoryStream stream = new MemoryStream();
            service.ExportHtml(report, stream, settings);
            string htmlText = Encoding.UTF8.GetString(stream.ToArray()).Substring(1);
            stream.Close();

            // Add Styles
            StringWriter writer = new StringWriter();
            StiHtmlTextWriter htmlWriter = new StiHtmlTextWriter(writer);
            service.HtmlWriter = htmlWriter;
            if (service.TableRender != null) service.TableRender.RenderStylesTable(true, false, false);
            htmlWriter.Flush();
            writer.Flush();
            string htmlTextStyles = writer.GetStringBuilder().ToString();
            writer.Close();
            service.Clear();

            StiPage page = report.RenderedPages[0];

            string pageMargins = string.Format("{0}px {1}px {2}px {3}px",
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Top) * zoom),
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Right) * zoom),
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Bottom) * zoom),
                Math.Round(report.Unit.ConvertToHInches(page.Margins.Left) * zoom));

            string pageBackground = GetHtmlColor(StiBrush.ToColor(page.Brush));

            return string.Format("<div style='display: inline-block; border: 1px solid #c6c6c6; background:{2}; padding:{3}'><style type='text/css'>{1}</style>{0}</div>",
                htmlText, htmlTextStyles, pageBackground, pageMargins);
        }

        internal static string GetHtmlColor(Color color)
        {
            return "#" + color.R.ToString("x2") + color.G.ToString("x2") + color.B.ToString("x2");
        }

        public static string GetStringContentForJSFromResourceContent(StiResourceType resourceType, byte[] content)
        {
            string resultContent = null;

            if (resourceType == StiResourceType.Image)
            {
                resultContent = StiReportEdit.ImageToBase64(content);
            }
            else if (resourceType == StiResourceType.Rtf)
            {
                string contentText = StiGZipHelper.ConvertByteArrayToString(content);
                RtfToHtmlConverter rtfConverter = new RtfToHtmlConverter();
                resultContent = StiEncodingHelper.Encode(rtfConverter.ConvertRtfToHtml(contentText));
            }
            else if (resourceType == StiResourceType.Txt)
            {
                string contentText = StiGZipHelper.ConvertByteArrayToString(content);
                resultContent = StiEncodingHelper.Encode(contentText);
            }
            else if (resourceType == StiResourceType.Report)
            {
                StiReport report = new StiReport();

                if ((content[0] == 0x1F && content[0] == 0x8B && content[0] == 0x08) ||
                    (content[0] == 0x50 && content[0] == 0x4B && content[0] == 0x03))
                {
                    report.LoadPackedReport(content);
                }
                else
                {
                    report.Load(content);
                }

                resultContent = StiEncodingHelper.Encode(GetReportThumbnailParameters(report, 0.2));
            }

            return resultContent;
        }

        public static void GetResourceContent(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiResource resource = report.Dictionary.Resources[(string)param["resourceName"]];
            if (resource != null)
            {
                callbackResult["resourceType"] = resource.Type;
                callbackResult["resourceName"] = resource.Name;
                callbackResult["resourceSize"] = resource.Content != null ? resource.Content.Length : 0;
                callbackResult["haveContent"] = resource.Content != null;

                if (resource.Content != null)
                {
                    callbackResult["resourceContent"] = StiResourcesHelper.GetStringContentForJSFromResourceContent(resource.Type, resource.Content);
                }
            }
        }

        public static void GetResourceText(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiResource resource = report.Dictionary.Resources[(string)param["resourceName"]];
            if (resource != null && resource.Content != null)
            {
                callbackResult["resourceText"] = Convert.ToBase64String(resource.Content);
            }
        }

        public static void SetResourceText(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiResource resource = report.Dictionary.Resources[(string)param["resourceName"]];
            if (resource != null && param["resourceText"] != null)
            {
                resource.Content = Convert.FromBase64String(param["resourceText"] as string);
            }
        }

        public static void GetResourceViewData(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            StiResource resource = report.Dictionary.Resources[(string)param["resourceName"]];
            string contentBase64Str = param["resourceContent"] as string;
            byte[] resourceContent = contentBase64Str != null ? Convert.FromBase64String(contentBase64Str.Substring(contentBase64Str.IndexOf("base64,") + 7)) : null;
            if ((resource != null && resource.Content != null) || resourceContent != null)
            {
                DataSet dataSet = StiResourceArrayToDataSet.Get((StiResourceType)Enum.Parse(typeof(StiResourceType), param["resourceType"] as string), resourceContent ?? resource.Content);
                Hashtable resultTables = new Hashtable();

                foreach (DataTable dataTable in dataSet.Tables)
                {
                    ArrayList resultData = new ArrayList();
                    List<StiDataColumn> dictionaryColumns = new List<StiDataColumn>();

                    ArrayList captions = new ArrayList();
                    for (int k = 0; k < dataTable.Columns.Count; k++)
                    {
                        StiDataColumn dictionaryColumn = new StiDataColumn(dataTable.Columns[k].Caption, dataTable.Columns[k].DataType);
                        captions.Add(dataTable.Columns[k].Caption);
                        dictionaryColumns.Add(dictionaryColumn);
                    }
                    resultData.Add(captions);

                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        ArrayList rowArray = new ArrayList();
                        resultData.Add(rowArray);
                        for (int k = 0; k < dataTable.Columns.Count; k++)
                        {
                            rowArray.Add(StiDictionaryHelper.GetViewDataItemValue(dataTable.Rows[i][k], dictionaryColumns[k]));
                        }
                    }

                    resultTables[dataTable.TableName] = resultData;
                }

                callbackResult["dataTables"] = resultTables;
            }
        }

        public static StiResourceType GetResourceTypeByFileName(string fileName)
        {
            fileName = fileName.ToLower();

            if (fileName.EndsWith(".csv")) return StiResourceType.Csv;
            else if (fileName.EndsWith(".dbf")) return StiResourceType.Dbf;
            else if (fileName.EndsWith(".xls") || fileName.EndsWith(".xlsx")) return StiResourceType.Excel;
            else if (fileName.EndsWith(".json")) return StiResourceType.Json;
            else if (fileName.EndsWith(".xml")) return StiResourceType.Xml;
            else if (fileName.EndsWith(".xsd")) return StiResourceType.Xsd;
            else if (fileName.EndsWith(".ttf")) return StiResourceType.FontTtf;
            else if (fileName.EndsWith(".otf")) return StiResourceType.FontOtf;
            else if (fileName.EndsWith(".woff")) return StiResourceType.FontWoff;
            else if (fileName.EndsWith(".ttc")) return StiResourceType.FontTtc;
            else if (fileName.EndsWith(".eot")) return StiResourceType.FontEot;
            else if (fileName.EndsWith(".rtf")) return StiResourceType.Rtf;
            else if (fileName.EndsWith(".txt")) return StiResourceType.Txt;
            else if (fileName.EndsWith(".mrt") || fileName.EndsWith(".mrz")) return StiResourceType.Report;
            else if (fileName.EndsWith(".gif") || fileName.EndsWith(".png") || fileName.EndsWith(".jpeg") || fileName.EndsWith(".jpg") || fileName.EndsWith(".wmf") ||
                fileName.EndsWith(".bmp") || fileName.EndsWith(".tiff") || fileName.EndsWith(".ico") || fileName.EndsWith(".emf") || fileName.EndsWith(".svg"))
                return StiResourceType.Image;

            return StiResourceType.Image;
        }

        public static bool IsDataResourceType(StiResourceType resourceType)
        {
            return (resourceType == StiResourceType.Json ||
                    resourceType == StiResourceType.Csv ||
                    resourceType == StiResourceType.Xml ||
                    resourceType == StiResourceType.Dbf ||
                    resourceType == StiResourceType.Excel);
        }

        public static string ConvertBase64MetaFileToBase64Png(string fileContent)
        {
            byte[] imageBytes = Convert.FromBase64String(fileContent.Substring(fileContent.IndexOf("base64,") + 7));
            return StiReportEdit.GetBase64PngFromMetaFileBytes(imageBytes);
        }
    }
}