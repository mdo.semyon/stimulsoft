#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Web;
using System.Web.UI;
using Stimulsoft.Report.Export;


namespace Stimulsoft.Report.MobileDesign
{
	public sealed class StiReportResponse
	{
        private static int responseCacheTimeout = 60;
        public static int ResponseCacheTimeout
        {
            get
            {
                return responseCacheTimeout;
            }
            set
            {
                responseCacheTimeout = value;
            }
        }

		private static string GetReportFileName(StiReport report)
		{
            return (report.ReportAlias == null || report.ReportAlias.Trim().Length == 0) ? report.ReportName : report.ReportAlias;
		}

        #region Document
        /// <summary>
        /// Exports report to MDC format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsDocument(Page page, StiReport report, string typeReport, string password)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();
            string fileType = ".mdc"; 

            if (typeReport != null)
                switch (typeReport)
                {
                    case "SaveReportMdc":
                        {
                            report.SaveDocument(stream);                            
                            break;
                        }
                    case "SaveReportMdz":
                        {
                            report.SavePackedDocument(stream);
                            fileType = ".mdz";
                            break;
                        }
                    case "SaveReportMdx":
                        {
                            report.SaveEncryptedDocument(stream, password);
                            fileType = ".mdz";
                            break;
                        }
                }
            else
                report.SaveDocument(stream);

            ResponseAs(page, stream, "application/xml", GetReportFileName(report) + fileType, true);
        }
        #endregion

		#region Pdf
        /// <summary>
        /// Exports report to PDF document and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsPdf(Page page, StiReport report)
		{
			ResponseAsPdf(page, report, StiPagesRange.All);
		}

       
		/// <summary>
        /// Exports report to PDF document and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
		public static void ResponseAsPdf(Page page, StiReport report, StiPagesRange pageRange)
		{
			ResponseAsPdf(page, report, pageRange, 1, 100, true, false);
		}

       
		/// <summary>
        /// Exports report to PDF document and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
		public static void ResponseAsPdf(Page page, StiReport report, bool openDialog)
		{
			ResponseAsPdf(page, report, StiPagesRange.All, 1, 100, true, false, openDialog);
		}

        
		/// <summary>
        /// Exports report to PDF document and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageQuality">A float value that sets the quality of exporting images. Default value is 1.</param>
        /// <param name="imageResolution">A float value that sets the resolution of exporting images. Default value is 100.</param>
        /// <param name="embeddedFonts">If embeddedFont is true then, when exporting, fonts of the report will be included in the resulting document.</param>
        /// <param name="standardPdfFonts">If standardPdfFont is true then, when exporting, non-standard fonts of the report will be replaced by the standard fonts in resulting document.</param>
		public static void ResponseAsPdf(Page page, StiReport report, StiPagesRange pageRange,
			float imageQuality, float imageResolution, bool embeddedFonts, bool standardPdfFonts)
		{
			ResponseAsPdf(page, report, pageRange,
				imageQuality, imageResolution, embeddedFonts, standardPdfFonts, true);
		}


        /// <summary>
        /// Exports report to PDF document and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageQuality">A float value that sets the quality of exporting images. Default value is 1.</param>
        /// <param name="imageResolution">A float value that sets the resolution of exporting images. Default value is 100.</param>
        /// <param name="embeddedFonts">If embeddedFont is true then, when exporting, fonts of the report will be included in the resulting document.</param>
        /// <param name="standardPdfFonts">If standardPdfFont is true then, when exporting, non-standard fonts of the report will be replaced by the standard fonts in resulting document.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
		public static void ResponseAsPdf(Page page, StiReport report, StiPagesRange pageRange,
			float imageQuality, float imageResolution, bool embeddedFonts, bool standardPdfFonts, bool openDialog)
		{
			ResponseAsPdf(page, report, pageRange,
				imageQuality, imageResolution, embeddedFonts, standardPdfFonts, openDialog, 
				"", "", StiUserAccessPrivileges.All);
		}


        /// <summary>
        /// Exports report to PDF document and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageQuality">A float value that sets the quality of exporting images. Default value is 1.</param>
        /// <param name="imageResolution">A float value that sets the resolution of exporting images. Default value is 100.</param>
        /// <param name="embeddedFonts">If embeddedFonts is true then, when exporting, fonts of the report will be included in the resulting document.</param>
        /// <param name="standardPdfFonts">If standardPdfFont is true then, when exporting, non-standard fonts of the report will be replaced by the standard fonts in resulting document.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
        /// <param name="passwordUser">The user password that manage the exported document access.</param>
        /// <param name="passwordOwner">The owner password that manage ability to modify exported document.</param>
        /// <param name="userAccessPrivileges">Defines user access privileges to the document exported.</param>
		public static void ResponseAsPdf(Page page, StiReport report, StiPagesRange pageRange,
			float imageQuality, float imageResolution, bool embeddedFont, bool standardPdfFonts, bool openDialog,
			string passwordUser, string passwordOwner, StiUserAccessPrivileges userAccessPrivileges)
		{
			ResponseAsPdf(page, report, pageRange,
				imageQuality, imageResolution, embeddedFont, standardPdfFonts, false, openDialog,
				passwordUser, passwordOwner, userAccessPrivileges, StiPdfEncryptionKeyLength.Bit40);
		}


		/// <summary>
		/// Exports report to PDF document and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		/// <param name="pageRange">Describes pages range for the export.</param>
		/// <param name="imageQuality">A float value that sets the quality of exporting images. Default value is 1.</param>
		/// <param name="imageResolution">A float value that sets the resolution of exporting images. Default value is 100.</param>
		/// <param name="embeddedFonts">If embeddedFonts is true then, when exporting, fonts of the report will be included in the resulting document.</param>
		/// <param name="standardPdfFonts">If standardPdfFont is true then, when exporting, non-standard fonts of the report will be replaced by the standard fonts in resulting document.</param>
		/// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
		/// <param name="passwordUser">The user password that manage the exported document access.</param>
		/// <param name="passwordOwner">The owner password that manage ability to modify exported document.</param>
		/// <param name="userAccessPrivileges">Defines user access privileges to the document exported.</param>
		/// <param name="keyLength">Defines the encryption key length to protect the document access after export.</param>
		public static void ResponseAsPdf(Page page, StiReport report, StiPagesRange pageRange,
			float imageQuality, float imageResolution, bool embeddedFonts, bool standardPdfFonts, 
			bool exportRtfTextAsImage, bool openDialog, 
			string passwordUser, string passwordOwner, StiUserAccessPrivileges userAccessPrivileges, StiPdfEncryptionKeyLength keyLength)
		{
			ResponseAsPdf(page, report, pageRange,
				imageQuality, imageResolution, embeddedFonts, standardPdfFonts, 
				exportRtfTextAsImage, openDialog, 
				passwordUser, passwordOwner, userAccessPrivileges, keyLength, true);
		}

       
		/// <summary>
        /// Exports report to PDF document and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageQuality">A float value that sets the quality of exporting images. Default value is 1.</param>
        /// <param name="imageResolution">A float value that sets the resolution of exporting images. Default value is 100.</param>
        /// <param name="embeddedFonts">If embeddedFonts is true then, when exporting, fonts of the report will be included in the resulting document.</param>
        /// <param name="standardPdfFonts">If standardPdfFont is true then, when exporting, non-standard fonts of the report will be replaced by the standard fonts in resulting document.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
        /// <param name="passwordUser">The user password that manage the exported document access.</param>
        /// <param name="passwordOwner">The owner password that manage ability to modify exported document.</param>
        /// <param name="userAccessPrivileges">Defines user access privileges to the document exported.</param>
        /// <param name="keyLength">Defines the encryption key length to protect the document access after export.</param>
        /// <param name="useUnicode">If useUnicode is true then in pdf files unicode is used.</param>
		public static void ResponseAsPdf(Page page, StiReport report, StiPagesRange pageRange,
			float imageQuality, float imageResolution, bool embeddedFonts, bool standardPdfFonts, 
			bool exportRtfTextAsImage, bool openDialog, 
			string passwordUser, string passwordOwner, StiUserAccessPrivileges userAccessPrivileges, StiPdfEncryptionKeyLength keyLength,
			bool useUnicode)
		{

			StiPdfExportSettings settings = new StiPdfExportSettings();
			settings.PageRange = pageRange;
			settings.ImageQuality = imageQuality;
			settings.ImageResolution = imageResolution;
			settings.EmbeddedFonts = embeddedFonts;
			settings.StandardPdfFonts = standardPdfFonts;
			settings.ExportRtfTextAsImage = exportRtfTextAsImage;
			settings.PasswordInputUser = passwordUser;
			settings.PasswordInputOwner = passwordOwner;
			settings.UserAccessPrivileges = userAccessPrivileges;
			settings.KeyLength = keyLength;
			settings.UseUnicode = useUnicode;
			
			ResponseAsPdf(page, report, openDialog, settings);
		} 

		/// <summary>
		/// Exports report to PDF document and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		/// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
		public static void ResponseAsPdf(Page page, StiReport report, bool openDialog, StiPdfExportSettings settings)
		{
			if (!report.IsRendered)report.Render(false);

			MemoryStream stream = new MemoryStream();

			StiPdfExportService export = new StiPdfExportService();
			export.ExportPdf(report, stream, settings);

			ResponseAs(page, stream, "application/pdf", GetReportFileName(report) + ".pdf", openDialog);
		} 
		#endregion

		#region Xps
		/// <summary>
		/// Exports report to XPS document and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsXps(Page page, StiReport report)
		{
			ResponseAsXps(page, report, StiPagesRange.All);
		}

       
		/// <summary>
		/// Exports report to XPS document and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		/// <param name="pageRange">Describes pages range for the export.</param>
		public static void ResponseAsXps(Page page, StiReport report, StiPagesRange pageRange)
		{
			StiXpsExportSettings settings = new StiXpsExportSettings();
			settings.PageRange = pageRange;

			ResponseAsXps(page, report, pageRange);
		}


		/// <summary>
		/// Exports report to XPS document and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		/// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
		public static void ResponseAsXps(Page page, StiReport report, bool openDialog, StiXpsExportSettings settings)
		{
			if (!report.IsRendered)report.Render(false);

			MemoryStream stream = new MemoryStream();

			StiXpsExportService export = new StiXpsExportService();
			export.ExportXps(report, stream, settings);

            ResponseAs(page, stream, "application/vnd.ms-xpsdocument", GetReportFileName(report) + ".xps", openDialog);
		} 
		#endregion		

        #region Ppt

        /// <summary>
        /// Exports report to PPT format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsPpt(Page page, StiReport report)
        {
            ResponseAsPpt(page, report, StiPagesRange.All);
        }

        /// <summary>
        /// Exports report to PPT format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsPpt(Page page, StiReport report, StiPagesRange pageRange)
        {
            StiPpt2007ExportSettings settings = new StiPpt2007ExportSettings();
            settings.PageRange = pageRange;

            ResponseAsPpt(page, report, true, settings);
        }

        /// <summary>
        /// Exports report to PPT format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsPpt(Page page, StiReport report, bool openDialog, StiPpt2007ExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiPpt2007ExportService export = new StiPpt2007ExportService();
            export.ExportPowerPoint(report, stream, settings);

            ResponseAs(page, stream, "application/mspowerpoint", GetReportFileName(report) + ".ppt", openDialog);
        }

        #endregion

        #region Html
        /// <summary>
        /// Exports report to HTML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageFormat">Format of images for the export.</param>
        public static void ResponseAsHtml(Page page, StiReport report, StiPagesRange pageRange,
            ImageFormat imageFormat)
        {
            ResponseAsHtml(page, report, pageRange, imageFormat, null);
        }


        /// <summary>
        /// Exports report to HTML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageFormat">Format of images for the export.</param>
        /// <param name="htmlImageHost">Class that controls placement of images when exporting.</param>
        public static void ResponseAsHtml(Page page, StiReport report, StiPagesRange pageRange,
            ImageFormat imageFormat, StiHtmlImageHost htmlImageHost)
        {
            ResponseAsHtml(page, report, pageRange, imageFormat, htmlImageHost, true);
        }


        /// <summary>
        /// Exports report to HTML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="htmlImageHost">Class that controls placement of images when exporting.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
        public static void ResponseAsHtml(Page page, StiReport report, StiHtmlImageHost htmlImageHost, bool openDialog)
        {
            ResponseAsHtml(page, report, StiPagesRange.All, ImageFormat.Gif, htmlImageHost, openDialog);

        }


        /// <summary>
        /// Exports report to HTML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageFormat">Format of images for the export.</param>
        /// <param name="htmlImageHost">Class that controls placement of images when exporting.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
        public static void ResponseAsHtml(Page page, StiReport report, StiPagesRange pageRange,
            ImageFormat imageFormat, StiHtmlImageHost htmlImageHost, bool openDialog)
        {
            ResponseAsHtml(page, report, pageRange, imageFormat, htmlImageHost,
                Encoding.UTF8, openDialog);
        }


        /// <summary>
        /// Exports report to HTML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageFormat">Format of images for the export.</param>
        /// <param name="htmlImageHost">Class that controls placement of images when exporting.</param>
        /// <param name="encoding">Sets the code page of the resulting HTML.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
        public static void ResponseAsHtml(Page page, StiReport report, StiPagesRange pageRange,
            ImageFormat imageFormat, StiHtmlImageHost htmlImageHost,
            Encoding encoding, bool openDialog)
        {
            ResponseAsHtml(page, report, pageRange,
                imageFormat, htmlImageHost, StiHtmlExportMode.Table,
                StiHtmlExportQuality.High, encoding, openDialog);
        }


        /// <summary>
        /// Exports report to HTML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageFormat">Format of images for the export.</param>
        /// <param name="htmlImageHost">Class that controls placement of images when exporting.</param>
        /// <param name="exportMode">Sets the mode of report export.</param>
        /// <param name="encoding">Resulting Html page encoding.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
        public static void ResponseAsHtml(Page page, StiReport report, StiPagesRange pageRange,
            ImageFormat imageFormat, StiHtmlImageHost htmlImageHost, StiHtmlExportMode exportMode,
            Encoding encoding, bool openDialog)
        {
            ResponseAsHtml(page, report, pageRange,
                imageFormat, htmlImageHost, exportMode,
                StiHtmlExportQuality.High, encoding, openDialog);
        }


        /// <summary>
        /// Exports report to HTML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsHtml(Page page, StiReport report, StiPagesRange pageRange)
        {
            ResponseAsHtml(page, report, pageRange, ImageFormat.Gif);
        }


        /// <summary>
        /// Exports report to HTML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsHtml(Page page, StiReport report)
        {
            ResponseAsHtml(page, report, StiPagesRange.All);
        }


        /// <summary>
        /// Exports report to HTML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageFormat">Format of images for the export.</param>
        /// <param name="htmlImageHost">Class that controls placement of images during the export.</param>
        /// <param name="exportMode">Sets the mode of report export.</param>
        /// <param name="exportQuality">This parameter always is to be StiHtmlExportQuality.High.</param>
        /// <param name="encoding">Resulting Html page encoding.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
        public static void ResponseAsHtml(Page page, StiReport report, StiPagesRange pageRange,
            ImageFormat imageFormat, StiHtmlImageHost htmlImageHost, StiHtmlExportMode exportMode,
            StiHtmlExportQuality exportQuality, Encoding encoding, bool openDialog)
        {

            StiHtmlExportSettings settings = new StiHtmlExportSettings();
            settings.PageRange = pageRange;
            settings.ImageFormat = imageFormat;
            settings.ExportMode = exportMode;
            settings.ExportQuality = exportQuality;
            settings.Encoding = encoding;

            ResponseAsHtml(page, report, openDialog, settings, htmlImageHost);
        }

        /// <summary>
        /// Exports report to HTML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
        public static void ResponseAsHtml(Page page, StiReport report, bool openDialog, StiHtmlExportSettings settings, StiHtmlImageHost htmlImageHost)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiHtmlExportService export = new StiHtmlExportService();
            export.HtmlImageHost = htmlImageHost;
            export.ExportHtml(report, stream, settings);

            ResponseAs(page, stream, "application/html", GetReportFileName(report) + ".html", openDialog);
        }
        #endregion		

		#region Mht
        /// <summary>
        /// Exports report to Mht format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
		public static void ResponseAsMht(Page page, StiReport report, bool openDialog)
		{
			ResponseAsMht(page, report, Encoding.UTF8, openDialog);
		}


        /// <summary>
        /// Exports report to Mht format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="encoding">Sets the code page of the resulting Mht document.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
		public static void ResponseAsMht(Page page, StiReport report, Encoding encoding, bool openDialog)
		{
			ResponseAsMht(page, report, StiPagesRange.All, ImageFormat.Png, StiHtmlExportMode.Table, encoding, openDialog);
		}


        /// <summary>
        /// Exports report to Mht format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageFormat">Format of images for the export.</param>
        /// <param name="encoding">Sets the code page of the resulting Mht document.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
		public static void ResponseAsMht(Page page, StiReport report, StiPagesRange pageRange,
			ImageFormat imageFormat, Encoding encoding, bool openDialog)
		{
			ResponseAsMht(page, report, pageRange, imageFormat, StiHtmlExportMode.Table, encoding, openDialog);
		}


        /// <summary>
        /// Exports report to Mht format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="imageFormat">Format of images for the export.</param>
        /// <param name="exportMode">This parameter always is to be StiHtmlExportQuality.High.</param>
        /// <param name="encoding">Sets the code page of the resulting Mht document.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
		public static void ResponseAsMht(Page page, StiReport report, StiPagesRange pageRange,
			ImageFormat imageFormat, StiHtmlExportMode exportMode, Encoding encoding, bool openDialog)
		{
			StiMhtExportSettings settings = new StiMhtExportSettings();
			settings.PageRange = pageRange;
			settings.ImageFormat = imageFormat;
			settings.ExportMode = exportMode;
			settings.Encoding = encoding;
			
			ResponseAsMht(page, report, openDialog, settings);
		} 

		
		/// <summary>
		/// Exports report to Mht format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		/// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
		public static void ResponseAsMht(Page page, StiReport report, bool openDialog, StiMhtExportSettings settings)
		{
			if (!report.IsRendered)report.Render(false);

			MemoryStream stream = new MemoryStream();

			StiMhtExportService export = new StiMhtExportService();
			export.ExportMht(report, stream, settings);

			ResponseAs(page, stream, "application/mht", GetReportFileName(report) + ".mht", openDialog);
		} 
		#endregion		

        #region Text
        /// <summary>
        /// Exports report to Text format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsText(Page page, StiReport report)
        {
            ResponseAsText(page, report, StiPagesRange.All);
        }


        /// <summary>
        /// Exports report to Text format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsText(Page page, StiReport report, StiPagesRange pageRange)
        {
            StiTxtExportSettings settings = new StiTxtExportSettings();
            settings.PageRange = pageRange;

            ResponseAsText(page, report, settings);
        }


        /// <summary>
        /// Exports report to Text format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsText(Page page, StiReport report, StiTxtExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiTxtExportService export = new StiTxtExportService();
            export.ExportTxt(report, stream, settings);

            ResponseAs(page, stream, "application/text", GetReportFileName(report) + ".txt", true);
        }
        #endregion

        #region Rtf
        /// <summary>
        /// Exports report to RTF format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsRtf(Page page, StiReport report, StiPagesRange pageRange)
        {
            ResponseAsRtf(page, report, pageRange, true, StiRtfExportMode.Table);
        }


        /// <summary>
        /// Exports report to RTF format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsRtf(Page page, StiReport report)
        {
            ResponseAsRtf(page, report, StiPagesRange.All);
        }

        /// <summary>
        /// Exports report to RTF format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="exportMode">Sets the mode of report exporting.</param>
        public static void ResponseAsRtf(Page page, StiReport report, StiPagesRange pageRange,
            StiRtfExportMode exportMode)
        {
            ResponseAsRtf(page, report, pageRange, true, exportMode);
        }


        /// <summary>
        /// Exports report to RTF format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="exportMode">Sets the mode of report exporting.</param>
        public static void ResponseAsRtf(Page page, StiReport report, StiPagesRange pageRange, bool openDialog,
            StiRtfExportMode exportMode)
        {
            StiRtfExportSettings settings = new StiRtfExportSettings();
            settings.PageRange = pageRange;
            settings.ExportMode = exportMode;

            ResponseAsRtf(page, report, openDialog, settings);
        }


        /// <summary>
        /// Exports report to RTF format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsRtf(Page page, StiReport report, bool openDialog, StiRtfExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiRtfExportService export = new StiRtfExportService();
            export.ExportRtf(report, stream, settings);

            ResponseAs(page, stream, "application/msword", GetReportFileName(report) + ".rtf", true);
        }
        #endregion

        #region Word2007
        /// <summary>
        /// Exports report to Word2007 format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsWord2007(Page page, StiReport report)
        {
            ResponseAsWord2007(page, report, StiPagesRange.All);
        }

        /// <summary>
        /// Exports report to Word2007 format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsWord2007(Page page, StiReport report, StiPagesRange pageRange)
        {
            StiWord2007ExportSettings settings = new StiWord2007ExportSettings();
            settings.PageRange = pageRange;

            ResponseAsWord2007(page, report, settings);
        }

        /// <summary>
        /// Exports report to Word2007 format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsWord2007(Page page, StiReport report, StiWord2007ExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiWord2007ExportService export = new StiWord2007ExportService();
            export.ExportWord(report, stream, settings);

            ResponseAs(page, stream, "application/msword", GetReportFileName(report) + ".docx", true);
        }

        #endregion		

        #region OpenDocument Writer
        /// <summary>
        /// Exports report to OpenDocument Writer format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsOdt(Page page, StiReport report)
        {
            ResponseAsOdt(page, report, StiPagesRange.All);
        }

        /// <summary>
        /// Exports report to OpenDocument Writer format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsOdt(Page page, StiReport report, StiPagesRange pageRange)
        {
            StiOdtExportSettings settings = new StiOdtExportSettings();
            settings.PageRange = pageRange;

            ResponseAsOdt(page, report, settings);
        }

        /// <summary>
        /// Exports report to OpenDocument Writer format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsOdt(Page page, StiReport report, StiOdtExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiOdtExportService export = new StiOdtExportService();
            export.ExportOdt(report, stream, settings);

            ResponseAs(page, stream, "application/vnd.oasis.opendocument.text", GetReportFileName(report) + ".odt", true);
        }

        #endregion

		#region Excel
        /// <summary>
        /// Exports report to Xls format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsXls(Page page, StiReport report)
		{
			ResponseAsXls(page, report, StiPagesRange.All);
		}

		/// <summary>
		/// Exports report to Xls format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		/// <param name="pageRange">Describes pages range for the export.</param>
		public static void ResponseAsXls(Page page, StiReport report, StiPagesRange pageRange)
		{
			StiExcelExportSettings settings = new StiExcelExportSettings();
			settings.PageRange = pageRange;

			ResponseAsXls(page, report, settings);
		}

		/// <summary>
		/// Exports report to Xls format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsXls(Page page, StiReport report, StiExcelExportSettings settings)
		{
			if (!report.IsRendered)report.Render(false);

			MemoryStream stream = new MemoryStream();

			StiExcelExportService export = new StiExcelExportService();
			export.ExportExcel(report, stream, settings);

			ResponseAs(page, stream, "application/excel", GetReportFileName(report) + ".xls", true);
		}

		#endregion

        #region Excel Xml
        /// <summary>
        /// Exports report to XlsXML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsXlsXml(Page page, StiReport report)
        {
            ResponseAsXlsXml(page, report, StiPagesRange.All);
        }


        /// <summary>
        /// Exports report to XlsXML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsXlsXml(Page page, StiReport report, StiPagesRange pageRange)
        {
            StiExcelXmlExportSettings settings = new StiExcelXmlExportSettings();
            settings.PageRange = pageRange;

            ResponseAsXlsXml(page, report, settings);
        }


        /// <summary>
        /// Exports report to XlsXML format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsXlsXml(Page page, StiReport report, StiExcelXmlExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiExcelXmlExportService export = new StiExcelXmlExportService();
            export.ExportExcel(report, stream, settings);

            ResponseAs(page, stream, "application/excel", GetReportFileName(report) + ".xls", true);
        }
        #endregion

		#region Excel2007
		/// <summary>
		/// Exports report to Excel2007 format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsExcel2007(Page page, StiReport report)
		{
			ResponseAsExcel2007(page, report, StiPagesRange.All);
		}

		/// <summary>
		/// Exports report to Excel2007 format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		/// <param name="pageRange">Describes pages range for the export.</param>
		public static void ResponseAsExcel2007(Page page, StiReport report, StiPagesRange pageRange)
		{
			StiExcel2007ExportSettings settings = new StiExcel2007ExportSettings();
			settings.PageRange = pageRange;

			ResponseAsExcel2007(page, report, settings);
		}

		/// <summary>
		/// Exports report to Excel2007 format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsExcel2007(Page page, StiReport report, StiExcel2007ExportSettings settings)
		{
			if (!report.IsRendered)report.Render(false);

			MemoryStream stream = new MemoryStream();

			StiExcel2007ExportService export = new StiExcel2007ExportService();
			export.ExportExcel(report, stream, settings);

			ResponseAs(page, stream, "application/excel", GetReportFileName(report) + ".xlsx", true);
		}

		#endregion        

        #region OpenDocument Calc
        /// <summary>
        /// Exports report to OpenDocument Calc format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsOds(Page page, StiReport report)
        {
            ResponseAsOds(page, report, StiPagesRange.All);
        }

        /// <summary>
        /// Exports report to OpenDocument Calc format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsOds(Page page, StiReport report, StiPagesRange pageRange)
        {
            StiOdsExportSettings settings = new StiOdsExportSettings();
            settings.PageRange = pageRange;

            ResponseAsOds(page, report, settings);
        }

        /// <summary>
        /// Exports report to OpenDocument Calc format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsOds(Page page, StiReport report, StiOdsExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiOdsExportService export = new StiOdsExportService();
            export.ExportOds(report, stream, settings);

            ResponseAs(page, stream, "application/vnd.oasis.opendocument.spreadsheet", GetReportFileName(report) + ".ods", true);
        }

        #endregion

		#region Csv
        /// <summary>
        /// Exports report to CSV format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsCsv(Page page, StiReport report)
        {
            ResponseAsCsv(page, report, StiPagesRange.All);
        }


        /// <summary>
        /// Exports report to CSV format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
		public static void ResponseAsCsv(Page page, StiReport report, StiPagesRange pageRange)
		{
			ResponseAsCsv(page, report, StiPagesRange.All, ";", Encoding.UTF8);
		}

	
		/// <summary>
		/// Exports report to CSV format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		/// <param name="pageRange">Describes pages range for the export.</param>
		public static void ResponseAsCsv(Page page, StiReport report, StiPagesRange pageRange, string separator, Encoding encoding)
		{			
			StiCsvExportSettings settings = new StiCsvExportSettings();
			settings.Encoding = encoding;
			settings.PageRange = pageRange;
			settings.Separator = separator;

			ResponseAsCsv(page, report, settings);
		}


		/// <summary>
		/// Exports report to CSV format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsCsv(Page page, StiReport report, StiCsvExportSettings settings)
		{
			if (!report.IsRendered)report.Render(false);

			MemoryStream stream = new MemoryStream();

			StiCsvExportService export = new StiCsvExportService();
			export.ExportCsv(report, stream, settings);

			ResponseAs(page, stream, "application/excel", GetReportFileName(report) + ".csv", true);
		}
		#endregion

		#region Xml
		/// <summary>
		/// Exports report to xml format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsXml(Page page, StiReport report)
		{
			if (!report.IsRendered)report.Render(false);

			MemoryStream stream = new MemoryStream();

			StiXmlExportService export = new StiXmlExportService();
			export.ExportXml(report, stream);

			ResponseAs(page, stream, "application/xml", GetReportFileName(report) + ".xml", true);
		}

		#endregion

        #region Dbase
        /// <summary>
        /// Exports report to Dbase format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="codePage">Sets the code page of the exported document.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
        public static void ResponseAsDbf(Page page, StiReport report, StiDbfCodePages codePage, bool openDialog)
        {
            ResponseAsDbf(page, report, StiPagesRange.All, codePage, openDialog);
        }


        /// <summary>
        /// Exports report to Dbase format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        /// <param name="codePage">Sets the code page of the exported document.</param>
        /// <param name="openDialog">If openDialog is true then the Open Dialog Box will be displayed. If openDialog is false then the result of the export will be displayed in the browser window.</param>
        public static void ResponseAsDbf(Page page, StiReport report, StiPagesRange pageRange, StiDbfCodePages codePage, bool openDialog)
        {
            StiDbfExportSettings settings = new StiDbfExportSettings();
            settings.CodePage = codePage;
            settings.PageRange = pageRange;

            ResponseAsDbf(page, report, openDialog, settings);
        }

        /// <summary>
        /// Exports report to Dbase format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsDbf(Page page, StiReport report, bool openDialog, StiDbfExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiDbfExportService export = new StiDbfExportService();
            export.ExportDbf(report, stream, settings);

            ResponseAs(page, stream, "application/dbf", GetReportFileName(report) + ".dbf", openDialog);
        }
        #endregion		

        #region Dif

        /// <summary>
        /// Exports report to DIF format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsDif(Page page, StiReport report)
        {
            ResponseAsDif(page, report, StiPagesRange.All);
        }

        /// <summary>
        /// Exports report to DIF format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsDif(Page page, StiReport report, StiPagesRange pageRange)
        {
            StiDifExportSettings settings = new StiDifExportSettings();
            settings.PageRange = pageRange;

            ResponseAsDif(page, report, true, settings);
        }

        /// <summary>
        /// Exports report to DIF format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsDif(Page page, StiReport report, bool openDialog, StiDifExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiDifExportService export = new StiDifExportService();
            export.ExportDif(report, stream, settings);

            ResponseAs(page, stream, "application/mixed", GetReportFileName(report) + ".dif", openDialog);
        }

        #endregion

        #region Sylk

        /// <summary>
        /// Exports report to SYLK format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsSylk(Page page, StiReport report)
        {
            ResponseAsSylk(page, report, StiPagesRange.All);
        }

        /// <summary>
        /// Exports report to SYLK format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsSylk(Page page, StiReport report, StiPagesRange pageRange)
        {
            StiSylkExportSettings settings = new StiSylkExportSettings();
            settings.PageRange = pageRange;

            ResponseAsSylk(page, report, true, settings);
        }

        /// <summary>
        /// Exports report to SYLK format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsSylk(Page page, StiReport report, bool openDialog, StiSylkExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiSylkExportService export = new StiSylkExportService();
            export.ExportSylk(report, stream, settings);

            ResponseAs(page, stream, "application/mixed", GetReportFileName(report) + ".sylk", openDialog);
        }

        #endregion

        #region Bmp
        /// <summary>
        /// Exports report to bmp format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsBmp(Page page, StiReport report)
        {
            ResponseAsBmp(page, report, new StiBmpExportSettings());
        }

		/// <summary>
		/// Exports report to bmp format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsBmp(Page page, StiReport report, StiBmpExportSettings settings)
		{
			if (!report.IsRendered) report.Render(false);

			MemoryStream stream = new MemoryStream();

			StiBmpExportService export = new StiBmpExportService();
			export.ExportImage(report, stream, settings);

			ResponseAs(page, stream, "application/bmp", GetReportFileName(report) + ".bmp", true);
		}

        #endregion

        #region Gif
		/// <summary>
		/// Exports report to gif format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsGif(Page page, StiReport report)
		{
			ResponseAsGif(page, report, new StiGifExportSettings());
		}

        /// <summary>
        /// Exports report to gif format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsGif(Page page, StiReport report, StiGifExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiGifExportService export = new StiGifExportService();
            export.ExportImage(report, stream, settings);

            ResponseAs(page, stream, "application/gif", GetReportFileName(report) + ".gif", true);
        }
        #endregion

        #region Jpeg
		/// <summary>
		/// Exports report to jpeg format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsJpeg(Page page, StiReport report)
		{
			ResponseAsJpeg(page, report, new StiJpegExportSettings());
		}

        /// <summary>
        /// Exports report to jpeg format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsJpeg(Page page, StiReport report, StiJpegExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiJpegExportService export = new StiJpegExportService();
            export.ExportImage(report, stream, settings);

            ResponseAs(page, stream, "application/jpeg", GetReportFileName(report) + ".jpeg", true);
        }
        #endregion

        #region Png
		/// <summary>
		/// Exports report to png format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsPng(Page page, StiReport report)
		{
			ResponseAsPng(page, report, new StiPngExportSettings());
		}

        /// <summary>
        /// Exports report to png format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsPng(Page page, StiReport report, StiPngExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiPngExportService export = new StiPngExportService();
            export.ExportImage(report, stream, settings);

            ResponseAs(page, stream, "application/png", GetReportFileName(report) + ".png", true);
        }
        #endregion

        #region Pcx
		/// <summary>
		/// Exports report to pcx format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsPcx(Page page, StiReport report)
		{
			ResponseAsPcx(page, report, new StiPcxExportSettings());
		}

        /// <summary>
        /// Exports report to pcx format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsPcx(Page page, StiReport report, StiPcxExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiPcxExportService export = new StiPcxExportService();
			export.ExportImage(report, stream, settings);

            ResponseAs(page, stream, "application/pcx", GetReportFileName(report) + ".pcx", true);
        }
        #endregion

        #region Tiff
		/// <summary>
		/// Exports report to tiff format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsTiff(Page page, StiReport report)
		{
			ResponseAsTiff(page, report, new StiTiffExportSettings());
		}

        /// <summary>
        /// Exports report to tiff format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsTiff(Page page, StiReport report, StiTiffExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiTiffExportService export = new StiTiffExportService();
			export.ExportImage(report, stream, settings);

            ResponseAs(page, stream, "application/tiff", GetReportFileName(report) + ".tiff", true);
        }
        #endregion

        #region Metafile
		/// <summary>
		/// Exports report to emf format and saves this document to the webpage response.
		/// </summary>
		/// <param name="page">Webpage to save exported report.</param>
		/// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsMetafile(Page page, StiReport report)
		{
			ResponseAsMetafile(page, report, new StiEmfExportSettings());
		}

        /// <summary>
        /// Exports report to metafile format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
		public static void ResponseAsMetafile(Page page, StiReport report, StiEmfExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiEmfExportService export = new StiEmfExportService();
            export.ExportImage(report, stream, settings);

            ResponseAs(page, stream, "application/emf", GetReportFileName(report) + ".emf", true);
        }
        #endregion

        #region Svg

        /// <summary>
        /// Exports report to SVG format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsSvg(Page page, StiReport report)
        {
            ResponseAsSvg(page, report, StiPagesRange.All);
        }

        /// <summary>
        /// Exports report to SVG format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsSvg(Page page, StiReport report, StiPagesRange pageRange)
        {
            StiSvgExportSettings settings = new StiSvgExportSettings();
            settings.PageRange = pageRange;

            ResponseAsSvg(page, report, true, settings);
        }

        /// <summary>
        /// Exports report to SVG format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsSvg(Page page, StiReport report, bool openDialog, StiSvgExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiSvgExportService export = new StiSvgExportService();
            export.ExportImage(report, stream, settings);

            ResponseAs(page, stream, "application/mixed", GetReportFileName(report) + ".svg", openDialog);
        }

        #endregion

        #region Svgz

        /// <summary>
        /// Exports report to SVGZ format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsSvgz(Page page, StiReport report)
        {
            ResponseAsSvgz(page, report, StiPagesRange.All);
        }

        /// <summary>
        /// Exports report to SVGZ format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        /// <param name="pageRange">Describes pages range for the export.</param>
        public static void ResponseAsSvgz(Page page, StiReport report, StiPagesRange pageRange)
        {
            StiSvgzExportSettings settings = new StiSvgzExportSettings();
            settings.PageRange = pageRange;

            ResponseAsSvgz(page, report, true, settings);
        }

        /// <summary>
        /// Exports report to SVGZ format and saves this document to the webpage response.
        /// </summary>
        /// <param name="page">Webpage to save exported report.</param>
        /// <param name="report">Report, which is to be exported.</param>
        public static void ResponseAsSvgz(Page page, StiReport report, bool openDialog, StiSvgzExportSettings settings)
        {
            if (!report.IsRendered) report.Render(false);

            MemoryStream stream = new MemoryStream();

            StiSvgzExportService export = new StiSvgzExportService();
            export.ExportImage(report, stream, settings);

            ResponseAs(page, stream, "application/mixed", GetReportFileName(report) + ".svgz", openDialog);
        }

        #endregion



		public static void ResponseAs(Page page, MemoryStream stream, string contentType, string fileName, bool openDialog)
		{
            fileName = fileName.Replace(" ", "");

			page.Response.Buffer = true;
			page.Response.ClearContent();
            page.Response.StatusCode = 200;
            if (StiOptions.Web.ClearResponseHeaders) page.Response.ClearHeaders();

            if (openDialog)
            {
                page.Response.ContentType = string.Format("{0};{1};", contentType, fileName);
                page.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            }
            else
            {
                if (contentType != "application/html") page.Response.ContentType = contentType;
            }
            
            if (ResponseCacheTimeout > 0)
            {
                page.Response.Cache.SetExpires(DateTime.Now.AddSeconds(ResponseCacheTimeout));
                page.Response.Cache.SetCacheability(HttpCacheability.Public);
            }
            else
            {
                page.Response.Cache.SetExpires(DateTime.MinValue);
                page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            }

            page.Response.ContentEncoding = Encoding.UTF8;
            page.Response.AddHeader("Content-Length", stream.Length.ToString());
			page.Response.BinaryWrite(stream.ToArray());

            if (StiOptions.Engine.FullTrust)
            {
                if (StiOptions.Web.AllowUseResponseFlush)
                {
                    try
                    {
                        page.Response.Flush();
                    }
                    catch
                    {
                    }
                }
            }

            try
            {
                page.Response.End();
            }
            catch
            {
            }
		}
	}
}
