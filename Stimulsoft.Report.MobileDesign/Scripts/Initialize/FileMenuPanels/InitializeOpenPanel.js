﻿
StiMobileDesigner.prototype.InitializeOpenPanel = function () {
    var openPanel = document.createElement("div");
    openPanel.jsObject = this;
    this.options.openPanel = openPanel;
    this.options.mainPanel.appendChild(openPanel);
    openPanel.style.display = "none";
    openPanel.className = "stiDesignerNewReportPanel";
    openPanel.style.overflow = "auto";
    var jsObject = this;

    var header = this.FileMenuPanelHeader(this.loc.MainMenu.menuFileOpen.replace("&", "").replace("...", ""));
    openPanel.appendChild(header);

    var mainTable = this.CreateHTMLTable();
    openPanel.appendChild(mainTable);
    mainTable.style.margin = "10px 30px 0px 30px";
    mainTable.style.height = "calc(100% - 130px)";

    var mainButtonsTable = this.CreateHTMLTable();
    mainTable.addCell(mainButtonsTable).className = "wizardFormStepsPanel";

    var additionalCell = mainTable.addCell();
    additionalCell.style.verticalAlign = "top";
    openPanel.additionalCell = additionalCell;

    //Import
    var importButtonsTable = this.CreateHTMLTable();
    importButtonsTable.style.display = "none";
    additionalCell.appendChild(importButtonsTable);

    var importButtons = [];
    importButtons.push(["ActiveReports", "Active Reports", "Open.ImportFiles.png"]);
    importButtons.push(["ComponentOneReports", "ComponentOne Reports", "Open.ImportFiles.png"]);
    importButtons.push(["CrystalReports", "Crystal Reports", "Open.ImportFiles.png"]);
    importButtons.push(["FastReports", "FastReport.Net", "Open.ImportFiles.png"]);
    importButtons.push(["ReportSharpShooter", "Report SharpShooter", "Open.ImportFiles.png"]);
    importButtons.push(["RichText", "Rich Text Format", "Open.ImportFiles.png"]);
    importButtons.push(["ReportingServices", "Reporting Services", "Open.ImportFiles.png"]);
    importButtons.push(["TelerikReports", "Telerik Reporting", "Open.ImportFiles.png"]);
    importButtons.push(["VisualFoxPro", "Visual FoxPro", "Open.ImportFiles.png"]);
    //importButtons.push(["XtraReports", "XtraReports", "Open.ImportFiles.png"]);
    
    for (var i = 0; i < importButtons.length; i++) {
        var button = this.RecentPanelButton(importButtons[i][0], null, importButtons[i][1], importButtons[i][2]);
        button.style.margin = "0px 10px 3px 10px";
        importButtonsTable.addCellInNextRow(button);

        button.action = function () {
            if (this.name == "VisualFoxPro") {
                jsObject.ActionImportFile(this.name);
            }
            else {
                var this_ = this;
                var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
                fileMenu.changeVisibleState(false);
                setTimeout(function () {
                    jsObject.ActionImportFile(this_.name);
                }, 200);
            }
        }
    }

    openPanel.openReportFromCloudItem = function (itemObject, notSaveToRecent) {
        var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
        fileMenu.changeVisibleState(false);
        setTimeout(function () {
            if (jsObject.options.report != null && jsObject.options.reportIsModified) {
                var messageForm = jsObject.MessageFormForSave();
                messageForm.changeVisibleState(true);
                messageForm.action = function (state) {
                    if (state) {
                        jsObject.ActionSaveReport(function () { jsObject.OpenReportFromCloud(itemObject, notSaveToRecent); });
                    }
                    else {
                        jsObject.OpenReportFromCloud(itemObject, notSaveToRecent);
                    }
                }
            }
            else {
                jsObject.OpenReportFromCloud(itemObject, notSaveToRecent);
            }
        });
    }

    //Online
    var onlinePanel = this.InitializeOnlineOpenReportPanel();
    jsObject.options.mainPanel.appendChild(onlinePanel);

    //Main Buttons
    var mainButtons = [];
    mainButtons.push(["recentFilesOpen", this.loc.FormDatabaseEdit.RecentConnections, "Open.RecentFiles.png"]);
    if (this.options.cloudMode) {
        mainButtons.push(["onlineItemsOpen", this.getCloudName(), "Connections.BigDataSource.png"]);
    }
    else {
        mainButtons.push(["importFilesOpen", this.loc.ReportOpen.Import, "Open.ImportFiles.png"]);
    }
    mainButtons.push(["browseFilesOpen", this.loc.ReportOpen.Browse, "Open.OpenFiles.png"]);
                       
    for (var i = 0; i < mainButtons.length; i++) {
        var button = this.RecentPanelButton(mainButtons[i][0], "OpenPanelMainButtons", mainButtons[i][1], mainButtons[i][2]);
        button.style.marginBottom = "3px";
        mainButtonsTable.addCellInNextRow(button);

        button.action = function (ignoreSelect) {            
            if (this.isSelected && !ignoreSelect) return;
            this.setSelected(true);

            switch (this.name) {
                case "recentFilesOpen": {
                    importButtonsTable.style.display = "none";
                    onlinePanel.style.display = "none";
                    if (openPanel.recentButtonsTable) openPanel.recentButtonsTable.style.display = "";
                    break;
                }
                case "importFilesOpen": {
                    importButtonsTable.style.display = "";
                    onlinePanel.style.display = "none";
                    if (openPanel.recentButtonsTable) openPanel.recentButtonsTable.style.display = "none";
                    break;
                }
                case "onlineItemsOpen": {
                    importButtonsTable.style.display = "none";
                    onlinePanel.show();
                    if (openPanel.recentButtonsTable) openPanel.recentButtonsTable.style.display = "none";
                    break;
                }
                case "browseFilesOpen": {
                    onlinePanel.style.display = "none";
                    var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
                    fileMenu.changeVisibleState(false);
                    setTimeout(function () { jsObject.ActionOpenReport(); }, 200);
                    break;
                }
            }
        }
    }       
    
    openPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        if (state) {
            //Recent
            jsObject.options.buttons.recentFilesOpen.action(true);

            if (openPanel.recentButtonsTable) additionalCell.removeChild(openPanel.recentButtonsTable);
            openPanel.recentButtonsTable = jsObject.CreateHTMLTable();
            additionalCell.appendChild(openPanel.recentButtonsTable);

            var recentArray = jsObject.GetRecentArray(jsObject.options.cloudMode ? "StimulsoftMobileDesignerOnlineRecentArray" : null);
            if (jsObject.options.cloudMode && recentArray.length == 0) jsObject.options.buttons.onlineItemsOpen.action(true);

            for (var i = 0; i < recentArray.length; i++) {
                var icon = "Open.ReportFile.png";
                //if (jsObject.EndsWith(recentArray[i].name.toString().toLowerCase(), ".mrx")) icon = "Open.ReportFileMrx.png";
                //else if (jsObject.EndsWith(recentArray[i].name.toString().toLowerCase(), ".mrz")) icon = "Open.ReportFileMrz.png";
                //else if (jsObject.options.cloudMode) icon = "CloudItems.BigReportTemplate.png";

                var caption = jsObject.options.cloudMode ? recentArray[i].name : recentArray[i].name + "<br>" + recentArray[i].path;
                var button = jsObject.RecentPanelButton(null, null, caption, icon);
                button.style.margin = "0px 10px 3px 10px";
                if (jsObject.options.cloudMode) button.style.cursor = "pointer";
                button.fileObject = recentArray[i];
                openPanel.recentButtonsTable.addCellInNextRow(button);

                button.action = function () {
                    var this_ = this;
                    var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
                    fileMenu.changeVisibleState(false);
                    setTimeout(function () {
                        if (jsObject.options.cloudMode) {
                            openPanel.openReportFromCloudItem({ Name: this_.fileObject.name, Key: this_.fileObject.path }, true);
                        }
                        else {
                            jsObject.ActionOpenRecentFile(this_.fileObject);
                        }
                    }, 200);
                }
            }
        }
        else {
            onlinePanel.style.display = "none";
        }
    }

    return openPanel;
}

StiMobileDesigner.prototype.RecentPanelButton = function (name, groupName, caption, image) {
    var button = this.SmallButton(name, groupName, caption, image, caption, null, this.GetStyles("StandartSmallButton"), true);
    if (button.imageCell) button.imageCell.style.padding = "0 10px 0 10px";
    if (button.caption) {
        button.caption.style.padding = "0 20px 0 10px";
        button.caption.style.lineHeight = "1.4";
    }
    button.style.minWidth = "250px";
    button.style.height = "50px";
        
    return button;
}

StiMobileDesigner.prototype.InitializeOnlineOpenReportPanel = function () {
    var form = this.BaseForm("onlineOpenReport", this.loc.MainMenu.menuFileOpen.replace("&", "").replace("...", ""), 2);
    var jsObject = this;
       
    form.buttonOk.caption.innerHTML = this.loc.MainMenu.menuFileOpen.replace("&", "").replace("...", "");
    form.buttonOk.style.margin = "15px 15px 15px 0px";
    form.buttonCancel.style.margin = "15px 15px 15px 0px";

    form.buttonCancel.action = function () {
        var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
        fileMenu.changeVisibleState(false);
    }

    //Online
    var onlineTree = jsObject.CloudTree();
    onlineTree.style.padding = "10px 0 10px 0";
    onlineTree.style.width = "550px";
    onlineTree.style.height = "450px";
    onlineTree.progress = jsObject.AddProgressToControl(form.container);
    form.container.appendChild(onlineTree);

    onlineTree.action = function (item) {
        form.buttonOk.setEnabled(item && item.itemObject.Ident == "ReportTemplateItem");
    }

    onlineTree.ondblClickAction = function (item) {
        form.action();
    }
    
    form.show = function (nextFunc) {
        form.style.display = "";
        form.buttonOk.setEnabled(false);
        form.style.left = (jsObject.FindPosX(jsObject.options.openPanel.additionalCell, "stiDesignerMainPanel") + 10) + "px";
        form.style.top = jsObject.FindPosY(jsObject.options.openPanel.additionalCell, "stiDesignerMainPanel") + "px";
        onlineTree.build();
    }

    form.action = function () {
        if (onlineTree.selectedItem && onlineTree.selectedItem.itemObject.Ident == "ReportTemplateItem") {
            var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
            form.changeVisibleState(false);

            setTimeout(function () {
                jsObject.options.openPanel.openReportFromCloudItem(onlineTree.selectedItem.itemObject);
            }, 200);
        }
    }
    
    return form;
}

StiMobileDesigner.prototype.ActionOpenRecentFile = function (fileObject) {}

StiMobileDesigner.prototype.ActionImportFile = function (importType) {}