﻿
StiMobileDesigner.prototype.InitializeNewReportPanel = function () {
    var newReportPanel = document.createElement("div");
    newReportPanel.jsObject = this;
    this.options.newReportPanel = newReportPanel;
    this.options.mainPanel.appendChild(newReportPanel);
    newReportPanel.style.display = "none";
    newReportPanel.className = "stiDesignerNewReportPanel";

    var header = this.FileMenuPanelHeader(this.loc.MainMenu.menuFileNew.replace("&", ""));
    newReportPanel.appendChild(header);

    var buttonsTable = this.CreateHTMLTable();
    buttonsTable.style.margin = "0 16px 0 16px";
    newReportPanel.appendChild(buttonsTable);

    buttonsTable.addCell(this.NewReportPanelButton("blankReportButton", this.loc.Wizards.BlankReport, "BlankReport.png"));
    buttonsTable.addCell(this.NewReportPanelButton("standartReportButton", this.loc.Wizards.StandardReport, "StandartReport.png"));
    buttonsTable.addCell(this.NewReportPanelButton("masterDetailReportButton", this.loc.Wizards.MasterDetailReport, "MasterDetailReport.png"));

    newReportPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";        
    }

    return newReportPanel;
}

StiMobileDesigner.prototype.NewReportPanelButton = function(name, caption, image) {
    var button = this.BigButton(name, null, caption, image, caption, null, this.GetStyles("NewReportButton"), true);
    button.style.marginRight = "30px";
    
    return button;    
}