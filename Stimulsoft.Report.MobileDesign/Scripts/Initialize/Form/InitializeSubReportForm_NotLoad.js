﻿
StiMobileDesigner.prototype.InitializeSubReportForm_ = function () {

    //SubReport Form
    var subReportForm = this.BaseFormPanel("subReportForm", this.loc.Components.StiSubReport, 1, this.HelpLinks["subreportform"]);
    subReportForm.mode = "SubReportPage";

    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerImageFormMainPanel";
    subReportForm.container.appendChild(mainTable);
    subReportForm.container.style.padding = "0px";

    //Buttons
    var buttonProps = [
        ["SubReportPage", "StiSubReport.png", this.loc.Toolbars.TabPage],
        ["SubReportFile", "SubReportLocalFile.png", this.loc.MainMenu.menuFile.replace("&", "")],
        ["SubReportUrl", "SubReportWebUrl.png", this.loc.PropertyMain.Hyperlink],
        ["SubReportParameters", "SubReportParameters.png", this.loc.PropertyMain.Parameters]
    ];

    if (this.options.cloudMode && !this.options.isOnlineVersion) {
        buttonProps.push(["SubReportServer", "SubReportCloud.png", "Server"]);
    }

    //Add Panels && Buttons
    var panelsContainer = mainTable.addCell();
    var buttonsPanel = mainTable.addCell();
    buttonsPanel.style.verticalAlign = "top";
    subReportForm.mainButtons = {};
    subReportForm.panels = {};

    for (var i = 0; i < buttonProps.length; i++) {
        var panel = document.createElement("Div");
        panel.className = "stiDesignerSubReportFormPanel";
        if (i != 0) panel.style.display = "none";
        panelsContainer.appendChild(panel);
        subReportForm.panels[buttonProps[i][0]] = panel;

        var button = this.StandartFormBigButton("subReportForm" + buttonProps[i][0] + "Button", null, buttonProps[i][2], buttonProps[i][1], buttonProps[i][2], 80);
        button.style.margin = "2px";
        button.style.minWidth = "110px";
        if (button.caption) button.caption.style.padding = "4px 3px 4px 3px";
        subReportForm.mainButtons[buttonProps[i][0]] = button;
        buttonsPanel.appendChild(button);

        if (this.options.isJava && i > 2) {
            button.style.display = 'none';
        }

        button.panelName = buttonProps[i][0];
        button.action = function () {
            subReportForm.setMode(this.panelName);
        }

        //add marker
        var marker = document.createElement("div");
        marker.style.display = "none";
        marker.className = "stiInteractionFormMarker";
        var markerInner = document.createElement("div");
        marker.appendChild(markerInner);
        button.style.position = "relative";
        button.appendChild(marker);
        button.marker = marker;
    }

    //Page
    var pageContainer = this.Container("pageContainer", 592, 442);
    pageContainer.style.border = "0px";
    pageContainer.style.margin = "4px auto 0 auto";
    subReportForm.panels.SubReportPage.appendChild(pageContainer);

    //File Name
    var toolBarFileName = this.CreateHTMLTable();
    subReportForm.panels.SubReportFile.appendChild(toolBarFileName);

    var sep2 = document.createElement("Div");
    sep2.className = "stiDesignerFormSeparator";
    subReportForm.panels.SubReportFile.appendChild(sep2);

    var captionCell = toolBarFileName.addCell();
    captionCell.style.padding = "0 8px 0 8px";
    captionCell.innerHTML = this.loc.MainMenu.menuFile.replace("&", "") + ":";

    subReportForm.fileNameTextBox = this.TextBoxWithOpenDialog("subReportFormFileName", 450, ".mrt");
    subReportForm.fileNameTextBox.style.margin = "4px";
    toolBarFileName.addCell(subReportForm.fileNameTextBox);
    subReportForm.fileNameTextBox.openButton.style.display = "none";

    //Url
    subReportForm.urlTextArea = this.TextArea("subReportFormUrl", 586, 440);
    subReportForm.urlTextArea.style.margin = "4px";
    subReportForm.panels.SubReportUrl.appendChild(subReportForm.urlTextArea);
    subReportForm.panels.SubReportUrl.style.overflow = "hidden";

    //Parameters
    var parametersContainer = this.SubReportFormParametersContainer(subReportForm);
    subReportForm.panels.SubReportParameters.appendChild(parametersContainer);

    //Events
    subReportForm.urlTextArea.onmouseup = function () {
        if (this.jsObject.options.itemInDrag) {
            var dictionaryTree = this.jsObject.options.dictionaryTree;
            if (dictionaryTree.selectedItem) { this.value += dictionaryTree.selectedItem.getResultForEditForm(); }
        }
    }
    subReportForm.urlTextArea.ontouchend = function () { this.onmouseup(); }

    //Server
    if (this.options.cloudMode && !this.options.isOnlineVersion) {
        subReportForm.cloudContainer = this.CloudContainer("subReportFormCloudContainer", ["SubReport"], null, 450);
        subReportForm.panels.SubReportServer.appendChild(subReportForm.cloudContainer);
        if (this.options.dictionaryTree.selectedItem) this.options.dictionaryTree.selectedItem.setSelected();
    }

    //Form Methods
    subReportForm.setMode = function (mode) {
        subReportForm.mode = mode;
        for (var panelName in subReportForm.panels) {
            subReportForm.panels[panelName].style.display = mode == panelName ? "" : "none";
            subReportForm.mainButtons[panelName].setSelected(mode == panelName);
        }
        var propertiesPanel = subReportForm.jsObject.options.propertiesPanel;
        propertiesPanel.editFormControl = null;
        propertiesPanel.setEnabled(mode == "SubReportUrl" || mode == "SubReportServer");
        if (mode == "SubReportServer") propertiesPanel.editFormControl = subReportForm.cloudContainer;
    }

    subReportForm.onhide = function () {
        subReportForm.jsObject.options.propertiesPanel.setDictionaryMode(false);
        clearTimeout(this.markerTimer);
    }

    subReportForm.updateMarkers = function () {
        this.mainButtons["SubReportPage"].marker.style.display = pageContainer.selectedItem && pageContainer.selectedItem.itemObject.name != "NotAssigned" ? "" : "none";
        this.mainButtons["SubReportFile"].marker.style.display = subReportForm.fileNameTextBox.textBox.value ? "" : "none";
        this.mainButtons["SubReportUrl"].marker.style.display = subReportForm.urlTextArea.value ? "" : "none";
        this.mainButtons["SubReportParameters"].marker.style.display = parametersContainer.getItems().length > 0 ? "" : "none";
    }

    subReportForm.show = function () {
        var selectedObject = this.jsObject.options.selectedObject || this.jsObject.GetCommonObject(this.jsObject.options.selectedObjects);
        if (!selectedObject) return;

        subReportForm.jsObject.options.propertiesPanel.setDictionaryMode(true);
        subReportForm.changeVisibleState(true);
        subReportForm.setMode("SubReportPage");
        subReportForm.urlTextArea.value = "";
        subReportForm.fileNameTextBox.textBox.value = "";

        var subReportPage = selectedObject.properties["subReportPage"] != "StiEmptyValue" ? selectedObject.properties["subReportPage"] : "[Not Assigned]";
        var subReportUrl = selectedObject.properties["subReportUrl"] != null && selectedObject.properties["subReportUrl"] != "StiEmptyValue"
            ? Base64.decode(selectedObject.properties["subReportUrl"]) : null;

        //Get subreport pages
        pageContainer.clear();
        var items = this.jsObject.GetSubReportItems();
        for (var i = 0; i < items.length; i++) {
            var item = pageContainer.addItemAndNotAction(null, items[i], items[i].caption);
            if (i == 0) item.selected();
        }

        if (subReportUrl) {
            if (subReportUrl.indexOf("file://") == 0) {
                subReportForm.setMode("SubReportFile");
                subReportForm.fileNameTextBox.textBox.value = subReportUrl.replace("file://", "");
            }
            else if (subReportUrl.indexOf(this.jsObject.options.cloudServerUrl) == 0) {
                subReportForm.setMode("SubReportServer");
                var key = subReportUrl.replace(this.jsObject.options.cloudServerUrl, "");
                var item = this.jsObject.options.dictionaryTree.getCloudItemByKey("SubReport", key);
                if (item && subReportForm.cloudContainer) subReportForm.cloudContainer.addItem(item.itemObject);
            }
            else {
                subReportForm.setMode("SubReportUrl");
                subReportForm.urlTextArea.value = subReportUrl;
            }
        }
        else if (subReportPage != "[Not Assigned]") {
            for (var i = 0; i < pageContainer.items.length; i++) {
                if (pageContainer.items[i].itemObject.name == subReportPage) {
                    pageContainer.items[i].selected();
                    break;
                }
            }
        }

        parametersContainer.fill(selectedObject.properties["subReportParameters"]);

        this.updateMarkers();
        this.markerTimer = setInterval(function () {
            subReportForm.updateMarkers();
        }, 250)
    }

    subReportForm.action = function () {
        var selectedObjects = this.jsObject.options.selectedObjects || [this.jsObject.options.selectedObject];
        if (!selectedObjects) return;
        for (var i = 0; i < selectedObjects.length; i++) {
            var selectedObject = selectedObjects[i];
            selectedObject.properties["subReportPage"] = "[Not Assigned]";
            selectedObject.properties["subReportUrl"] = "";

            switch (subReportForm.mode) {
                case "SubReportUrl":
                    {
                        selectedObject.properties.subReportUrl = Base64.encode(subReportForm.urlTextArea.value);
                        break;
                    }
                case "SubReportFile":
                    {
                        selectedObject.properties.subReportUrl = Base64.encode("file://" + subReportForm.fileNameTextBox.textBox.value);
                        break;
                    }
                case "SubReportPage":
                    {
                        selectedObject.properties.subReportPage = pageContainer.selectedItem ? pageContainer.selectedItem.itemObject.name : "[Not Assigned]";
                        break;
                    }
                case "SubReportServer":
                    {
                        if (subReportForm.cloudContainer && subReportForm.cloudContainer.item && this.jsObject.options.cloudServerUrl)
                            selectedObject.properties.subReportUrl = Base64.encode(this.jsObject.options.cloudServerUrl + subReportForm.cloudContainer.item.itemObject.key);
                        break;
                    }
            }
        }
        selectedObject.properties["subReportParameters"] = parametersContainer.getItems();

        subReportForm.jsObject.SendCommandSendProperties(selectedObjects, ["subReportPage", "subReportUrl", "subReportParameters"]);
        this.changeVisibleState(false);
    }

    return subReportForm;
}

StiMobileDesigner.prototype.SubReportFormParametersContainer = function (subReportForm) {
    var container = document.createElement("div");
        
    //Toolbar
    var buttons = [
        ["add", this.loc.Buttons.Add, "Add.png"],
        ["remove", null, "Remove.png"],
        ["separator"],
        ["moveUp", null, "MoveUp.png"],
        ["moveDown", null, "MoveDown.png"]
    ]

    var toolBar = this.CreateHTMLTable();
    toolBar.buttons = {};
    container.appendChild(toolBar);
    container.appendChild(this.FormSeparator());

    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton(null, null, buttons[i][1], buttons[i][2], null, null, this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "4px";
        toolBar.buttons[buttons[i][0]] = button;
        toolBar.addCell(button);
    }

    var table = this.CreateHTMLTable();
    container.appendChild(table);

    //Items Container
    var itemsContainer = this.EasyContainer(180, this.options.isTouchDevice ? 413 : 418);
    itemsContainer.className = "stiDesignerCreateDataColumsButtonsTable";
    table.addCell(itemsContainer);

    //Main Container
    var controlsTable = this.CreateHTMLTable();
    controlsTable.style.margin = "4px";
    table.addCell(controlsTable).style.verticalAlign = "top";

    controlsTable.addTextCell(this.loc.PropertyMain.Name).className = "stiDesignerCaptionControls";
    var nameControl = this.TextBox(null, 200);
    controlsTable.addCell(nameControl).className = "stiDesignerControlCellsBigIntervals";
    nameControl.action = function () {
        if (itemsContainer.selectedItem) { itemsContainer.selectedItem.itemObject.name = this.value; }
    }

    controlsTable.addTextCellInNextRow(this.loc.PropertyMain.Expression).className = "stiDesignerCaptionControls";
    var expressionControl = this.ExpressionControl(null, 200, null, null, true);
    controlsTable.addCellInLastRow(expressionControl).className = "stiDesignerControlCellsBigIntervals";
    expressionControl.action = function () {
        if (itemsContainer.selectedItem) { itemsContainer.selectedItem.itemObject.expression = Base64.encode(this.textBox.value); }
    }

    toolBar.buttons.moveUp.action = function () {
        if (itemsContainer.selectedItem) { itemsContainer.selectedItem.move("Up"); }
    }

    toolBar.buttons.moveDown.action = function () {
        if (itemsContainer.selectedItem) { itemsContainer.selectedItem.move("Down"); }
    }

    toolBar.buttons.add.action = function () {
        var index = itemsContainer.getCountItems() + 1;
        var item = itemsContainer.addItem(this.jsObject.loc.PropertyMain.Parameter + index, { name: this.jsObject.loc.PropertyMain.Parameter + index, expression: "" });
        item.action();
    }

    toolBar.buttons.remove.action = function () {
        if (itemsContainer.selectedItem) { itemsContainer.selectedItem.remove(); }
    }

    itemsContainer.onAction = function () {
        var count = this.getCountItems();
        var index = this.selectedItem ? this.selectedItem.getIndex() : -1;
        toolBar.buttons.moveUp.setEnabled(index > 0);
        toolBar.buttons.moveDown.setEnabled(index != -1 && index < count - 1);
        toolBar.buttons.remove.setEnabled(this.selectedItem);
        controlsTable.style.display = count > 0 ? "" : "none";
        
        if (this.selectedItem) {
            nameControl.value = this.selectedItem.itemObject.name;
            expressionControl.textBox.value = Base64.decode(this.selectedItem.itemObject.expression);
        }
    }

    container.fill = function (parameters) {
        itemsContainer.clear();
        if (!parameters) return;
        for (var i = 0; i < parameters.length; i++) {
            var item = itemsContainer.addItem(parameters[i].name, { name: parameters[i].name, expression: parameters[i].expression });
            if (i == 0) item.action();
        }        
    }

    container.getItems = function () {
        var parameters = [];
        for (var i = 0; i < itemsContainer.childNodes.length; i++) {
            if (itemsContainer.childNodes[i].itemObject) {
                parameters.push(itemsContainer.childNodes[i].itemObject);
            }
        }

        return parameters;
    }

    return container;
}