﻿
StiMobileDesigner.prototype.InitializeEditVariableForm_ = function () {

    //Edit Variable Form
    var editVariableForm = this.BaseForm("editVariableForm", this.loc.FormDictionaryDesigner.VariableNew, 3, this.HelpLinks["variableEdit"]);
    editVariableForm.variable = null;
    editVariableForm.mode = "Edit";
    editVariableForm.controls = {};

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px 0 5px 0";
    editVariableForm.container.appendChild(innerTable);

    //Name, Alias, Description
    var textBoxes = [
        ["name", this.loc.PropertyMain.Name, 200],
        ["alias", this.loc.PropertyMain.Alias, 200],
        ["description", this.loc.PropertyMain.Description, 311]
    ]
    for (var i in textBoxes) {
        var text = innerTable.addCellInNextRow();
        text.className = "stiDesignerCaptionControlsBigIntervals";
        text.innerHTML = textBoxes[i][1] + ":";
        text.style.minWidth = "140px";
        editVariableForm.controls[textBoxes[i][0]] = this.TextBox("editVariableForm" + textBoxes[i][0], textBoxes[i][2]);
        innerTable.addCellInLastRow(editVariableForm.controls[textBoxes[i][0]]).className = "stiDesignerControlCellsBigIntervals";
    }

    //Type
    var textType = innerTable.addCellInNextRow();
    textType.className = "stiDesignerCaptionControlsBigIntervals";
    textType.innerHTML = this.loc.PropertyMain.Type + ":";
    var typeControlsTable = this.CreateHTMLTable();
    innerTable.addCellInLastRow(typeControlsTable).className = "stiDesignerControlCellsBigIntervals";
    editVariableForm.controls.type = this.DropDownList("editVariableFormTypeControl", 170, null, this.GetVariableTypesItems(), true, true);
    //Override
    editVariableForm.controls.type.image.style.width = "16px";
    editVariableForm.controls.type.image.style.margin = "0 8px 0 8px";
    typeControlsTable.addCell(editVariableForm.controls.type).style.padding = "0 10px 0 0";
    editVariableForm.controls.basicType = this.DropDownList("editVariableFormBasicTypeControl", 130, null, this.GetVariableBasicTypesItems(), true);
    typeControlsTable.addCell(editVariableForm.controls.basicType).style.padding = "0 10px 0 0";
    var separator1 = this.Separator();
    innerTable.addCellInNextRow(separator1).setAttribute("colspan", "2");
    separator1.style.margin = "4px 0 4px 0";

    //InitBy && Values
    var controlProps = [
        ["initBy", this.loc.PropertyMain.InitBy + ":", 180, "DropDownList", this.GetFilterFieldIsItems()],
        ["value", this.loc.PropertyMain.Value + ":", 300, "ExpressionControl", null],
        ["valueFrom", this.loc.PropertyMain.RangeFrom + ":", 300, "ExpressionControl", null],
        ["valueTo", this.loc.PropertyMain.RangeTo + ":", 300, "ExpressionControl", null],
        ["expression", this.loc.PropertyEnum.StiFilterItemExpression + ":", 300, "ExpressionControl", null],
        ["expressionFrom", this.loc.PropertyMain.RangeFrom + ":", 300, "ExpressionControl", null],
        ["expressionTo", this.loc.PropertyMain.RangeTo + ":", 300, "ExpressionControl", null],
        ["valueDateTime", this.loc.PropertyMain.Value + ":", 200, "DateControlWithCheckBox", null],
        ["valueDateTimeFrom", this.loc.PropertyMain.RangeFrom + ":", 200, "DateControlWithCheckBox", null],
        ["valueDateTimeTo", this.loc.PropertyMain.RangeTo + ":", 200, "DateControlWithCheckBox", null],
        ["valueBool", this.loc.Report.LabelDefaultValue, 200, "CheckBox", null],
        ["valueImage", this.loc.PropertyMain.Image + ":", 314, "ImageControl", null],
        ["separatorInitBy"],
        ["readOnly", "", null, "CheckBox", this.loc.PropertyMain.ReadOnly],
        ["requestFromUser", "", null, "CheckBox", this.loc.PropertyMain.RequestFromUser],
        ["allowUseAsSqlParameter", "", null, "CheckBox", this.loc.PropertyMain.AllowUsingAsSqlParameter],
        ["separatorReadOnly"],
        ["allowUserValues", "", null, "CheckBox", this.loc.PropertyMain.AllowUserValues],
        ["dataSource", this.loc.PropertyMain.DataSource + ":", 200, "DropDownList", this.GetVariableDataSourceItems()],
        ["selection", this.loc.PropertyMain.Selection + ":", 200, "DropDownList", this.GetVariableSelectionItems()],
        ["items", this.loc.PropertyMain.Items + ":", 280, "TextAreaWithEditButton", 70],
        ["keys", this.loc.PropertyMain.Keys + ":", 200, "DataControl", null],
        ["values", this.loc.PropertyMain.Values + ":", 200, "DataControl", null],
        ["dependentValue", "", null, "CheckBox", this.loc.PropertyMain.DependentValue],
        ["dependentVariable", this.loc.PropertyMain.Variable + ":", 180, "DropDownList", []],
        ["dependentColumn", this.loc.PropertyMain.DependentColumn + ":", 300, "DataControl", null],
        ["formatMask", this.loc.FormFormatEditor.FormatMask, 180, "TextBox", null],
        ["dateTimeFormat", this.loc.FormFormatEditor.DateTimeFormat + ":", 180, "DropDownList", this.GetVariableDateTimeTypesItems()],
    ]

    for (var i in controlProps) {
        editVariableForm.controls[controlProps[i][0] + "Row"] = innerTable.addRow();

        if (controlProps[i][0].indexOf("separator") == 0) {
            var separator = this.Separator();
            innerTable.addCellInLastRow(separator).setAttribute("colspan", "2");
            separator.style.margin = "4px 0 4px 0";
        }
        else {
            var textControl = innerTable.addCellInLastRow();
            textControl.className = "stiDesignerCaptionControlsBigIntervals";
            textControl.innerHTML = controlProps[i][1];
            var control;
            if (controlProps[i][3] == "DropDownList") control = this.DropDownList("editVariableForm" + controlProps[i][0], controlProps[i][2], null, controlProps[i][4], true);
            else if (controlProps[i][3] == "ExpressionControl") control = this.ExpressionControl("editVariableForm" + controlProps[i][0], controlProps[i][2], null, null, true);
            else if (controlProps[i][3] == "DataControl") control = this.DataControl("editVariableForm" + controlProps[i][0], controlProps[i][2]);
            else if (controlProps[i][3] == "TextBox") control = this.TextBox("editVariableForm" + controlProps[i][0], controlProps[i][2]);
            else if (controlProps[i][3] == "DateControlWithCheckBox") control = this.DateControlWithCheckBox("editVariableForm" + controlProps[i][0], controlProps[i][2]);
            else if (controlProps[i][3] == "CheckBox") control = this.CheckBox("editVariableForm" + controlProps[i][0], controlProps[i][4]);
            else if (controlProps[i][3] == "TextAreaWithEditButton") control = this.TextAreaWithEditButton("editVariableForm" + controlProps[i][0], controlProps[i][2], controlProps[i][4], true);
            else if (controlProps[i][3] == "ImageControl") control = this.ImageControl(null, controlProps[i][2], 80);

            control.editVariableForm = editVariableForm;
            editVariableForm.controls[controlProps[i][0]] = control;
            innerTable.addCellInLastRow(control).className = "stiDesignerControlCellsBigIntervals";
        }
    }

    if (this.options.jsMode) editVariableForm.controls.allowUseAsSqlParameter.style.display = "none";

    editVariableForm.typeAndBasicTypeConflicted = function () {
        var basicType = this.variable.basicType;
        var type = this.variable.type;

        if (((basicType == "List" || basicType == "Range")
                  && (type == "sbyte" || type == "ushort" || type == "uint" || type == "ulong" || type == "object" || type == "image"))
             || (basicType == "Range" && type == "bool") || (basicType == "NullableValue" && type == "string"))
            return true;

        return false;
    }

    editVariableForm.changeConflictedControls = function (currPropName, conflictedPropName, currentKey) {
        if (this.variable[currPropName] != currentKey) this.clearItems();
        this.variable[currPropName] = currentKey;

        if (this.typeAndBasicTypeConflicted()) {
            var key = conflictedPropName == "type" ? "string" : "Value";
            //if (currPropName == "basicType" && currentKey == "NullableValue" && this.variable[conflictedPropName] == "string") {
            //    this.controls[currPropName].setKey("Value");
            //    this.variable[currPropName] = "Value";
            //}
            this.controls[conflictedPropName].setKey(key);
            this.variable[conflictedPropName] = key;
        }
        this.showControlsByType();
    }

    var controlNames = ["initBy", "readOnly", "requestFromUser", "dataSource", "dependentValue", "selection"];
    for (var i = 0; i < controlNames.length; i++) {
        editVariableForm.controls[controlNames[i]].action = function () {
            editVariableForm.showControlsByType();
        }
    }

    editVariableForm.controls.items.button.action = function () {
        this.jsObject.InitializeVariableItemsForm(function (variableItemsForm) {
            variableItemsForm.changeVisibleState(true);
        });
    }

    editVariableForm.controls.type.action = function () { editVariableForm.changeConflictedControls("type", "basicType", this.key); }
    editVariableForm.controls.basicType.action = function () { editVariableForm.changeConflictedControls("basicType", "type", this.key); }
    editVariableForm.controls.dateTimeFormat.action = function () { editVariableForm.controls.items.textArea.value = editVariableForm.getItemsStr(); }

    editVariableForm.controls.name.action = function () {
        if (this.oldValue == editVariableForm.controls.alias.value) {
            editVariableForm.controls.alias.value = this.value;
        }
    }

    editVariableForm.clearItems = function () {
        this.variable.items = null;
        this.controls.items.textArea.value = this.getItemsStr();
    }

    editVariableForm.showControlsByType = function () {
        var type = this.controls.type.key;
        var basicType = this.controls.basicType.key;
        var initBy = this.controls.initBy.key;
        var isValueBasicType = basicType != "Range" && basicType != "List";
        var showInitBy = type != "image" && basicType != "List";
        var enabledRequestFromUserCheckBox = !this.controls.readOnly.isChecked && type != "object";
        var showRequestFromUserControls = this.controls.requestFromUser.isChecked && enabledRequestFromUserCheckBox;
        var enabledDataSource = basicType != "Range";
        var showDefault = this.controls.selection.key == "FromVariable" || basicType != "Value";

        this.controls.valueBoolRow.style.display = (showDefault && type == "bool" && isValueBasicType && initBy == "Value") ? "" : "none";
        this.controls.valueImageRow.style.display = (showDefault && type == "image" && isValueBasicType) ? "" : "none";
        this.controls.valueRow.style.display = (showDefault && type != "datetime" && type != "bool" && type != "image" && isValueBasicType && initBy == "Value") ? "" : "none";
        this.controls.valueFromRow.style.display = (showDefault && type != "datetime" && basicType == "Range" && initBy == "Value") ? "" : "none";
        this.controls.valueToRow.style.display = (showDefault && type != "datetime" && basicType == "Range" && initBy == "Value") ? "" : "none";
        this.controls.expressionRow.style.display = (showDefault && type != "image" && isValueBasicType && initBy == "Expression") ? "" : "none";
        this.controls.expressionFromRow.style.display = (showDefault && basicType == "Range" && initBy == "Expression") ? "" : "none";
        this.controls.expressionToRow.style.display = (showDefault && basicType == "Range" && initBy == "Expression") ? "" : "none";
        this.controls.valueDateTimeRow.style.display = (showDefault && type == "datetime" && isValueBasicType && initBy == "Value") ? "" : "none";
        this.controls.valueDateTimeFromRow.style.display = (showDefault && type == "datetime" && basicType == "Range" && initBy == "Value") ? "" : "none";
        this.controls.valueDateTimeToRow.style.display = (showDefault && type == "datetime" && basicType == "Range" && initBy == "Value") ? "" : "none";
        this.controls.initByRow.style.display = showDefault && showInitBy ? "" : "none";
        this.controls.separatorInitByRow.style.display = showDefault && showInitBy ? "" : "none";
        this.controls.requestFromUser.setEnabled(enabledRequestFromUserCheckBox);
        this.controls.separatorReadOnlyRow.style.display = showRequestFromUserControls ? "" : "none";
        this.controls.allowUserValuesRow.style.display = showRequestFromUserControls ? "" : "none";
        this.controls.dataSourceRow.style.display = showRequestFromUserControls ? "" : "none";
        this.controls.selectionRow.style.display = showRequestFromUserControls && basicType == "Value" ? "" : "none";
        this.controls.readOnlyRow.style.display = type != "image" ? "" : "none";
        this.controls.requestFromUserRow.style.display = type != "image" ? "" : "none";
        this.controls.allowUseAsSqlParameterRow.style.display = type != "image" ? "" : "none";

        if (!enabledDataSource) this.controls.dataSource.setKey("Items");
        this.controls.dataSource.setEnabled(enabledDataSource);

        var showColumns = showRequestFromUserControls && this.controls.dataSource.isEnabled && this.controls.dataSource.key == "Columns";
        var showDependentValue = showColumns && type != "object" && basicType == "Value";

        this.controls.itemsRow.style.display = (showRequestFromUserControls && this.controls.dataSource.key == "Items") ? "" : "none";
        this.controls.keysRow.style.display = showColumns ? "" : "none";
        this.controls.valuesRow.style.display = showColumns ? "" : "none";
        this.controls.formatMaskRow.style.display = (showRequestFromUserControls && type == "string") ? "" : "none";
        this.controls.dateTimeFormatRow.style.display = (showRequestFromUserControls && type == "datetime") ? "" : "none";
        this.controls.dependentValueRow.style.display = showDependentValue ? "" : "none";
        this.controls.dependentVariableRow.style.display = (showDependentValue && this.controls.dependentValue.isChecked && this.controls.initBy.key == "Value") ? "" : "none";
        this.controls.dependentColumnRow.style.display = (showDependentValue && this.controls.dependentValue.isChecked && this.controls.initBy.key == "Value") ? "" : "none";
    }

    editVariableForm.onshow = function () {
        this.mode = "Edit";
        if (this.variable == null) { this.variable = this.jsObject.VariableObject(); this.mode = "New"; }
        var caption = this.jsObject.loc.FormDictionaryDesigner["Variable" + this.mode];
        this.caption.innerHTML = caption;
        this.controls.name.hideError();
        this.controls.name.focus();
        this.controls.name.value = this.variable.name;
        this.controls.alias.value = this.variable.alias;
        this.controls.description.value = Base64.decode(this.variable.description);
        this.controls.type.setKey(this.variable.type);
        this.controls.basicType.setKey(this.variable.basicType);
        this.controls.initBy.setKey(this.variable.initBy);
        this.controls.readOnly.setChecked(this.variable.readOnly);
        this.controls.allowUseAsSqlParameter.setChecked(this.variable.allowUseAsSqlParameter);
        this.controls.requestFromUser.setChecked(this.variable.requestFromUser);
        this.controls.allowUserValues.setChecked(this.variable.allowUserValues);
        this.controls.dateTimeFormat.setKey(this.variable.dateTimeFormat);
        this.controls.dataSource.setKey(this.variable.dataSource);
        this.controls.selection.setKey(this.variable.selection);
        this.controls.formatMask.value = Base64.decode(this.variable.formatMask);
        this.controls.items.textArea.value = this.getItemsStr();
        this.controls.keys.textBox.value = this.variable.keys;
        this.controls.values.textBox.value = this.variable.values;
        this.controls.dependentValue.setChecked(this.variable.dependentValue);
        this.controls.dependentVariable.menu.addItems(this.getVariablesItems());
        this.controls.dependentVariable.setKey(this.variable.dependentVariable);
        this.controls.dependentColumn.textBox.value = this.variable.dependentColumn;
        this.controls.valueImage.setImage(null);

        switch (this.variable.basicType) {
            case "Value":
            case "NullableValue":
                {
                    if (this.variable.initBy == "Value") {
                        if (this.variable.type == "image") {
                            this.controls.valueImage.setImage(this.variable.value);
                        }
                        else {
                            this.variable.value = Base64.decode(this.variable.value);
                            if (this.variable.type == "bool")
                                this.controls.valueBool.setChecked(this.variable.value == "true" || this.variable.value == "True");
                            else
                                if (this.variable.type == "datetime") {
                                    if (this.variable.value == "") this.controls.valueDateTime.setChecked(false);
                                    else this.controls.valueDateTime.setKey(new Date(this.variable.value));
                                }
                                else
                                    this.controls.value.textBox.value = this.variable.value != null ? this.variable.value : "";
                        }
                    }
                    else {

                        this.variable.expression = Base64.decode(this.variable.expression);
                        this.controls.expression.textBox.value = this.variable.expression != null ? this.variable.expression : "";
                    }
                    break;
                }
            case "Range":
                {
                    if (this.variable.initBy == "Value") {
                        this.variable.valueFrom = Base64.decode(this.variable.valueFrom);
                        this.variable.valueTo = Base64.decode(this.variable.valueTo);
                        if (this.variable.type == "datetime") {
                            if (this.variable.valueFrom == "") this.controls.valueDateTimeFrom.setChecked(false);
                            else this.controls.valueDateTimeFrom.setKey(new Date(this.variable.valueFrom));
                            if (this.variable.valueTo == "") this.controls.valueDateTimeTo.setChecked(false);
                            else this.controls.valueDateTimeTo.setKey(new Date(this.variable.valueTo));
                        }
                        else {
                            this.controls.valueFrom.textBox.value = this.variable.valueFrom;
                            this.controls.valueTo.textBox.value = this.variable.valueTo;
                        }
                    }
                    else {
                        this.variable.expressionFrom = Base64.decode(this.variable.expressionFrom);
                        this.variable.expressionTo = Base64.decode(this.variable.expressionTo);
                        this.controls.expressionFrom.textBox.value = this.variable.expressionFrom;
                        this.controls.expressionTo.textBox.value = this.variable.expressionTo;
                    }
                    break;
                }
        }

        this.showControlsByType();
    }

    editVariableForm.action = function () {
        var variable = {};
        variable.mode = this.mode;

        if (!this.controls.name.checkNotEmpty(this.jsObject.loc.PropertyMain.Name)) return;
        if ((this.mode == "New" || this.controls.name.value != this.variable.name) &&
            !(this.controls.name.checkExists(this.jsObject.GetVariablesFromDictionary(this.jsObject.options.report.dictionary), "name") &&
                this.controls.name.checkExists(this.jsObject.GetDataSourcesFromDictionary(this.jsObject.options.report.dictionary), "name")))
            return;

        if (this.mode == "Edit") variable.oldName = this.variable.name;
        var variableCategoryItem = this.jsObject.options.dictionaryTree.getCurrentVariableParent();
        variable.category = variableCategoryItem.itemObject.typeItem == "Category" ? variableCategoryItem.itemObject.name : "";
        variable.name = this.controls.name.value;
        variable.alias = this.controls.alias.value;
        variable.description = Base64.encode(this.controls.description.value);
        variable.type = this.controls.type.key;
        variable.basicType = this.controls.basicType.key;
        if (variable.basicType != "List") {
            variable.initBy = this.controls.initBy.key;
            if (variable.type == "image") {
                variable.value = this.controls.valueImage.src;
            }
            else if (variable.initBy == "Expression") {
                if (variable.basicType != "Range") variable.expression = Base64.encode(this.controls.expression.textBox.value);
                else {
                    variable.expressionFrom = Base64.encode(this.controls.expressionFrom.textBox.value);
                    variable.expressionTo = Base64.encode(this.controls.expressionTo.textBox.value);
                }
            }
            else {
                if (variable.basicType != "Range") {
                    variable.value = Base64.encode(variable.type == "bool"
                        ? (this.controls.valueBool.isChecked ? "True" : "False")
                        : (variable.type == "datetime"
                            ? (this.controls.valueDateTime.isChecked ? this.jsObject.DateToStringAmericanFormat(this.controls.valueDateTime.key) : "")
                            : this.controls.value.textBox.value));
                }
                else {
                    variable.valueFrom = Base64.encode(variable.type == "datetime"
                        ? (this.controls.valueDateTimeFrom.isChecked ? this.jsObject.DateToStringAmericanFormat(this.controls.valueDateTimeFrom.key) : "")
                        : this.controls.valueFrom.textBox.value);
                    variable.valueTo = Base64.encode(variable.type == "datetime"
                        ? (this.controls.valueDateTimeTo.isChecked ? this.jsObject.DateToStringAmericanFormat(this.controls.valueDateTimeTo.key) : "")
                        : this.controls.valueTo.textBox.value);
                }
            }
        }

        variable.readOnly = this.controls.readOnly.isChecked && variable.type != "image";
        variable.allowUseAsSqlParameter = this.controls.allowUseAsSqlParameter.isChecked && variable.type != "image";
        variable.requestFromUser = this.controls.requestFromUser.isEnabled && this.controls.requestFromUser.isChecked && variable.type != "image";
        variable.allowUserValues = variable.requestFromUser ? this.controls.allowUserValues.isChecked : true;
        variable.dateTimeFormat = this.controls.dateTimeFormatRow.style.display == "" ? this.controls.dateTimeFormat.key : "DateAndTime";
        variable.dataSource = this.controls.dataSourceRow.style.display == "" ? this.controls.dataSource.key : "Items";
        variable.selection = this.controls.selection.key;
        variable.formatMask = this.controls.formatMaskRow.style.display == "" ? Base64.encode(this.controls.formatMask.value) : "";
        variable.keys = this.controls.keysRow.style.display == "" ? this.controls.keys.textBox.value : "";
        variable.values = this.controls.valuesRow.style.display == "" ? this.controls.values.textBox.value : "";
        variable.dependentValue = this.controls.dependentValueRow.style.display == "" ? this.controls.dependentValue.isChecked : false;
        variable.dependentVariable = this.controls.dependentVariableRow.style.display == "" ? this.controls.dependentVariable.key : "";
        variable.dependentColumn = this.controls.dependentColumnRow.style.display == "" ? this.controls.dependentColumn.textBox.value : "";
        variable.items = this.variable.items;
                
        this.changeVisibleState(false);
        this.jsObject.SendCommandCreateOrEditVariable(variable);
    }

    editVariableForm.getVariablesItems = function () {
        var items = [];
        var variables = this.jsObject.GetVariablesFromDictionary(this.jsObject.options.report.dictionary);
        for (var i in variables)
            if (variables[i].name != this.variable.name)
                items.push(this.jsObject.Item("item" + i, variables[i].name, null, variables[i].name));

        return items;
    }

    editVariableForm.getItemsStr = function () {
        var items = this.variable.items;
        var resultStr = "";
        if (items == null) return "";
        for (var i in items) {
            if (i != 0) resultStr += "; ";
            resultStr += this.getItemCaption(items[i]);
        }

        return resultStr;
    }

    editVariableForm.getItemCaption = function (itemObject) {
        var itemCaption = Base64.decode(itemObject.value || "");
        if (itemCaption == "") {
            var key = itemObject.key != null ? Base64.decode(itemObject.key) : null;
            var keyTo = itemObject.keyTo != null ? Base64.decode(itemObject.keyTo) : null;
            itemCaption = key;
            var isDateTimeValue = this.variable.type == "datetime" && itemObject.type != "expression";
            if (isDateTimeValue) itemCaption = this.jsObject.DateAmericanFormatToLocalFormat(key, this.controls.dateTimeFormat.key);
            if (keyTo != null) {
                if (isDateTimeValue) itemCaption += " - " + this.jsObject.DateAmericanFormatToLocalFormat(keyTo, this.controls.dateTimeFormat.key);
                else itemCaption += " - " + keyTo;
            }
        }
        if (itemObject.type == "expression") itemCaption = "{" + itemCaption + "}";
        if (itemCaption == "") itemCaption = this.jsObject.loc.PropertyMain.Value;

        return itemCaption;
    }

    return editVariableForm;
}