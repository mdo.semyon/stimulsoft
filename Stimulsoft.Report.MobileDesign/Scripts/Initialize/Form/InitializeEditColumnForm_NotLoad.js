﻿
StiMobileDesigner.prototype.InitializeEditColumnForm_ = function () {

    //Edit Column Form
    var editColumnForm = this.BaseForm("editColumnForm", this.loc.PropertyMain.Column, 3, this.HelpLinks["columnEdit"]);
    editColumnForm.column = null;
    editColumnForm.mode = "Edit";

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px 10px 5px 0";
    editColumnForm.container.appendChild(innerTable);

    var textBoxes = [
        ["nameInSource", this.loc.PropertyMain.NameInSource],
        ["name", this.loc.PropertyMain.Name],
        ["alias", this.loc.PropertyMain.Alias]
    ]

    for (var i in textBoxes) {
        editColumnForm[textBoxes[i][0] + "ControlRow"] = innerTable.addRow();
        var text = innerTable.addCellInLastRow();
        text.className = "stiDesignerCaptionControlsBigIntervals";
        text.innerHTML = textBoxes[i][1] + ":";
        editColumnForm[textBoxes[i][0] + "Control"] = this.TextBox("editColumnForm" + textBoxes[i][0], 270);
        innerTable.addCellInLastRow(editColumnForm[textBoxes[i][0] + "Control"]).className = "stiDesignerControlCellsBigIntervals";
    }

    editColumnForm["typeControlRow"] = innerTable.addRow();
    var textType = innerTable.addCellInLastRow();
    textType.className = "stiDesignerCaptionControlsBigIntervals";
    textType.innerHTML = this.loc.PropertyMain.Type + ":";
    editColumnForm["typeControl"] = this.DropDownList("editColumnFormTypeControl", 160, null, this.GetColumnTypesItems(), true);
    innerTable.addCellInLastRow(editColumnForm["typeControl"]).className = "stiDesignerControlCellsBigIntervals";

    editColumnForm["valueControlRow"] = innerTable.addRow();
    var textValue = innerTable.addCellInLastRow();
    textValue.className = "stiDesignerCaptionControlsBigIntervals";
    textValue.innerHTML = this.loc.PropertyMain.Value + ":";
    editColumnForm.valueControl = this.ExpressionControl("editColumnFormValueControl", 300, null, null, true);
    innerTable.addCellInLastRow(editColumnForm.valueControl).className = "stiDesignerControlCellsBigIntervals";

    editColumnForm.nameControl.action = function () {
        if (this.oldValue == editColumnForm.aliasControl.value) {
            editColumnForm.aliasControl.value = this.value;
        }
    }

    editColumnForm.onshow = function () {
        this.mode = "Edit";
        var dictionaryTree = this.jsObject.options.dictionaryTree;
        this.currentParent = dictionaryTree.getCurrentColumnParent();
        if (typeof (this.column) == "string") {
            var parentObject = (this.currentParent.type == "BusinessObject")
                ? this.jsObject.GetBusinessObjectByNameFromDictionary(dictionaryTree.selectedItem.getBusinessObjectFullName())
                : this.jsObject.GetDataSourceByNameFromDictionary(this.currentParent.name);
            this.column = this.jsObject.ColumnObject(this.column == "calcColumn", parentObject ? parentObject.columns : null);
            this.mode = "New";
        }
        var caption = this.jsObject.loc.FormDictionaryDesigner[(this.column.isCalcColumn ? "CalcColumn" : "Column") + this.mode];

        this.caption.innerHTML = caption;
        this.nameInSourceControl.value = this.column.nameInSource;
        this.nameControl.focus();
        this.nameControl.value = this.column.name;
        this.aliasControl.value = this.column.alias;
        this.typeControl.setKey(this.column.type);
        this.valueControlRow.style.display = this.column.isCalcColumn ? "" : "none";
        this.valueControl.textBox.value = Base64.decode(this.column.expression);
    }

    editColumnForm.action = function () {
        this.column.mode = this.mode;
        this.column.currentParentType = this.currentParent.type;
        this.column.currentParentName = this.column.currentParentType == "DataSource"
            ? this.currentParent.name : this.jsObject.options.dictionaryTree.selectedItem.getBusinessObjectFullName();
        if (this.mode == "Edit") this.column["oldName"] = this.column.name;
        this.column.nameInSource = this.nameInSourceControl.value;
        this.column.name = this.nameControl.value;
        this.column.alias = this.aliasControl.value;
        this.column.expression = Base64.encode(this.valueControl.textBox.value);
        this.column.type = this.typeControl.key;

        if (!this.nameControl.checkNotEmpty(this.jsObject.loc.PropertyMain.Name)) return;
        this.changeVisibleState(false);

        this.jsObject.SendCommandCreateOrEditColumn(this.column);
    }

    return editColumnForm;
}