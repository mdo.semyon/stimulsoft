﻿
StiMobileDesigner.prototype.InitializeReportCheckForm_ = function () {

    var reportCheckForm = this.BaseForm("reportCheckForm", this.loc.Report.ReportChecker, 3, this.HelpLinks["reportCheck"]);
    reportCheckForm.buttonCancel.style.display = "none";

    //Toolbar
    var buttons = [        
        //["open", this.loc.Buttons.Open, "Open.png"],
        //["save", this.loc.Buttons.Save, "Save.png"],
        //["separator"],
        ["run", this.loc.MainMenu.menuCheckIssues, "ReportChecker.Run.png"],
        //["separator"],
        //["settings", this.loc.Export.Settings, "Properties.png"],
        ["separator"]
    ]

    var toolBar = this.CreateHTMLTable();
    reportCheckForm.container.appendChild(toolBar);    
    
    for (var i in buttons) {
        if (buttons[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton(null, null, buttons[i][1], buttons[i][2], null, null, this.GetStyles("StandartSmallButton"));
        button.style.margin = "4px";
        toolBar[buttons[i][0]] = button;
        toolBar.addCell(button);
    }

    //toolBar.open.setEnabled(false);
    //toolBar.save.setEnabled(false);
    //toolBar.settings.setEnabled(false);

    toolBar.run.action = function () {
        reportCheckForm.show(true);
    }

    reportCheckForm.show = function (notAnimation) {
        reportCheckForm.jsObject.InitializeCheckPanel(function (checkPanel) {
            if (reportCheckForm.checkPanel) {
                reportCheckForm.container.removeChild(reportCheckForm.checkPanel);
            }
            reportCheckForm.container.appendChild(checkPanel);
            reportCheckForm.checkPanel = checkPanel;
            checkPanel.style.position = "relative";
            checkPanel.style.boxShadow = "";
            checkPanel.style.border = "0px";
            checkPanel.style.borderTop = "1px dotted #c6c6c6";
            reportCheckForm.jsObject.options.checkPanel = null;
            
            checkPanel.show();
            checkPanel.style.left = "";
            checkPanel.style.bottom = "";
            if (!notAnimation) reportCheckForm.changeVisibleState(true);
        });
    }

    reportCheckForm.action = function () {
        reportCheckForm.changeVisibleState(false);
    }

    return reportCheckForm;
}