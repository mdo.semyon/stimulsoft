﻿
StiMobileDesigner.prototype.InitializeFilterForm_ = function () {
    //Filter Form
    var filterForm = this.BaseForm("filterForm", this.loc.PropertyMain.Filter, 2, this.HelpLinks["filter"]);
    filterForm.filterControl = this.FilterControl("filterFormFilterControl", null, 550, 300);
    filterForm.container.appendChild(filterForm.filterControl);
    filterForm.resultControl = null;

    filterForm.onhide = function () { this.resultControl = null; }

    filterForm.onshow = function () {
        this.tempResultControl = (this.resultControl) ? this.resultControl : null;
        this.resultControl = null;
        this.filterControl.currentDataSourceName = null;
        var parent = (this.tempResultControl == null) ? this.jsObject.options.selectedObject.properties : this.tempResultControl.formResult;
        if (this.tempResultControl) this.filterControl.currentDataSourceName = this.tempResultControl.currentDataSourceName;

        var filters = parent.filterData != "" ? JSON.parse(Base64.decode(parent.filterData)) : [];
        this.filterControl.fill(filters, parent.filterOn, parent.filterMode);
    }

    filterForm.action = function () {
        var filterResult = this.filterControl.getValue();
        filterForm.changeVisibleState(false);
        if (!this.tempResultControl) {
            this.jsObject.options.selectedObject.properties.filterOn = filterResult.filterOn;
            this.jsObject.options.selectedObject.properties.filterMode = filterResult.filterMode;
            this.jsObject.options.selectedObject.properties.filterData = Base64.encode(JSON.stringify(filterResult.filters));
            this.jsObject.SendCommandSendProperties(this.jsObject.options.selectedObject, ["filterData", "filterOn", "filterMode"]);
        }
        else {
            this.tempResultControl.formResult = {
                filterData: Base64.encode(JSON.stringify(filterResult.filters)),
                filterOn: filterResult.filterOn,
                filterMode: filterResult.filterMode
            }
            if (this.tempResultControl["setKey"] != null) {
                this.tempResultControl.setKey(this.tempResultControl.formResult);
            }
            else {
                this.tempResultControl.value = this.jsObject.FilterDataToShortString(filterResult.filters);
            }
            this.tempResultControl.action();
        }
    }

    return filterForm;
}