﻿

StiMobileDesigner.prototype.InitializeGroupHeaderForm_ = function () {

    //Group Header Form
    var groupHeaderForm = this.BaseFormPanel("groupHeaderForm", this.loc.FormTitles.GroupConditionForm, 1);
    groupHeaderForm.dataTree = this.options.dataTree;
    groupHeaderForm.mode = "Expression";
    groupHeaderForm.controls = {};

    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerGroupHeaderFormMainPanel";
    groupHeaderForm.container.appendChild(mainTable);
    groupHeaderForm.container.style.padding = "0px";


    //Buttons
    var buttonProps = [
        ["Expression", "BigGroupHeaderExpression.png", this.loc.PropertyMain.Expression],
        ["DataColumn", "BigGroupHeaderDataColumn.png", this.loc.PropertyMain.DataColumn],
        ["SummaryExpression", "BigGroupHeaderSummary.png", this.loc.PropertyMain.Summary]
    ];

    //Add Panels && Buttons
    var panelsContainer = mainTable.addCell();
    var buttonsPanel = mainTable.addCell();
    buttonsPanel.style.verticalAlign = "top";
    groupHeaderForm.mainButtons = {};
    groupHeaderForm.panels = {};

    for (var i = 0; i < buttonProps.length; i++) {
        var panel = document.createElement("Div");
        panel.className = "stiDesignerGroupHeaderFormPanel";
        if (i != 0) panel.style.display = "none";
        panelsContainer.appendChild(panel);
        groupHeaderForm.panels[buttonProps[i][0]] = panel;
        if (buttonProps[i][0] == "SummaryExpression") panel.style.height = this.options.isTouchDevice ? "395px" : "390px";
        var button = this.StandartFormBigButton("groupHeaderForm" + buttonProps[i][0] + "Button", null, buttonProps[i][2], buttonProps[i][1], buttonProps[i][2], 80);
        button.style.margin = "2px";
        groupHeaderForm.mainButtons[buttonProps[i][0]] = button;
        buttonsPanel.appendChild(button);
        button.panelName = buttonProps[i][0];
        button.action = function () {
            groupHeaderForm.setMode(this.panelName);
        }
    }

    //Text Areas
    var textAreas = ["Expression", "SummaryExpression"];
    for (var i = 0; i < textAreas.length; i++) {
        var height = textAreas[i] == "Expression" ? 340 : (this.options.isTouchDevice ? 305 : 315);
        groupHeaderForm["textArea" + textAreas[i]] = this.TextArea("groupHeaderForm" + textAreas[i], 436, height);
        groupHeaderForm["textArea" + textAreas[i]].style.margin = "4px 0 0 4px";
        groupHeaderForm.panels[textAreas[i]].appendChild(groupHeaderForm["textArea" + textAreas[i]]);
        groupHeaderForm["textArea" + textAreas[i]].addInsertButton();
    }

    var controlProps = [
        ["sortDirection", this.loc.PropertyMain.SortDirection + ":",
            this.DropDownList("groupHeaderFormSortDirection", 200, null, this.GetSortDirectionItems(), true, false, null, true), "8px"],
        ["summarySortDirection", this.loc.PropertyMain.SummarySortDirection + ":",
            this.DropDownList("groupHeaderFormSummarySortDirection", 200, null, this.GetSortDirectionItems(), true, false, null, true), "8px 8px 4px 8px"],
        ["summaryType", this.loc.PropertyMain.SummaryType + ":",
            this.DropDownList("groupHeaderFormSummaryType", 200, null, this.GetSummaryTypeItems(), true, false, null, true), "4px 8px 0 8px"]
    ];

    var table = this.CreateHTMLTable();
    for (var i = 0; i < controlProps.length; i++) {
        if (controlProps[i][0] == "summarySortDirection") {
            groupHeaderForm.container.appendChild(table);
            table = this.CreateHTMLTable();
            groupHeaderForm.panels.SummaryExpression.appendChild(table);
        }

        var textCell = table.addTextCellInLastRow(controlProps[i][1]);
        textCell.className = "stiDesignerCaptionControlsBigIntervals";
        textCell.style.width = "200px";
        textCell.style.whiteSpace = "normal";
        table.addCellInLastRow(controlProps[i][2]);
        groupHeaderForm.controls[controlProps[i][0]] = controlProps[i][2];
        groupHeaderForm.controls[controlProps[i][0] + "Row"] = table;
        controlProps[i][2].style.margin = controlProps[i][3];
        table.addRow();
    }

    //Form Methods
    groupHeaderForm.reset = function () {
        groupHeaderForm.dataTree.setKey("");
        groupHeaderForm.textAreaExpression.value = "";
        groupHeaderForm.textAreaSummaryExpression.value = "";
        groupHeaderForm.setMode("Expression");
    }

    groupHeaderForm.setMode = function (mode) {
        groupHeaderForm.mode = mode;
        for (var panelName in groupHeaderForm.panels) {
            groupHeaderForm.panels[panelName].style.display = mode == panelName ? "" : "none";
            groupHeaderForm.mainButtons[panelName].setSelected(mode == panelName);
        }
        var propertiesPanel = groupHeaderForm.jsObject.options.propertiesPanel;
        propertiesPanel.setEnabled(mode == "Expression" || mode == "SummaryExpression");
        propertiesPanel.editFormControl = null;
        if (mode == "Expression") propertiesPanel.editFormControl = groupHeaderForm.textAreaExpression;
        if (mode == "SummaryExpression") propertiesPanel.editFormControl = groupHeaderForm.textAreaSummaryExpression;
        groupHeaderForm.controls.sortDirectionRow.style.display = mode == "SummaryExpression" ? "none" : "";
        mainTable.className = mode == "SummaryExpression" ? "stiDesignerClearAllStyles" : "stiDesignerGroupHeaderFormMainPanel";
    }

    groupHeaderForm.onhide = function () {
        groupHeaderForm.jsObject.options.propertiesPanel.setDictionaryMode(false);
    }

    groupHeaderForm.show = function () {

        groupHeaderForm.changeVisibleState(true);

        //Build Data Tree
        groupHeaderForm.jsObject.options.propertiesPanel.setDictionaryMode(true);
        groupHeaderForm.panels.DataColumn.appendChild(groupHeaderForm.dataTree);
        groupHeaderForm.dataTree.build(null, null, null, true);
        groupHeaderForm.dataTree.action = function () {
            groupHeaderForm.action();
        }

        //Reset Controls
        groupHeaderForm.reset();

        var selectedObject = this.jsObject.options.selectedObject || this.jsObject.GetCommonObject(this.jsObject.options.selectedObjects);
        if (!selectedObject) return;

        var conditionValue = selectedObject.properties["condition"] != null && selectedObject.properties["condition"] != "StiEmptyValue"
            ? Base64.decode(selectedObject.properties["condition"]) : "";
        var subStringConditionValue = conditionValue.length > 1 ? conditionValue.substring(1, conditionValue.length - 1) : "";

        groupHeaderForm.textAreaExpression.value = conditionValue;

        if (groupHeaderForm.dataTree.setKey(subStringConditionValue)) {
            groupHeaderForm.setMode("DataColumn");
        }
        else {
            groupHeaderForm.setMode("Expression");            
            groupHeaderForm.textAreaExpression.focus();
        }

        groupHeaderForm.textAreaSummaryExpression.value = selectedObject.properties["summaryExpression"] && selectedObject.properties["summaryExpression"] != "StiEmptyValue"
            ? Base64.decode(selectedObject.properties["summaryExpression"]) : "";
        groupHeaderForm.controls.sortDirection.setKey(selectedObject.properties["sortDirection"] != "StiEmptyValue" ? selectedObject.properties["sortDirection"] : "0");
        groupHeaderForm.controls.summarySortDirection.setKey(selectedObject.properties["summarySortDirection"] != "StiEmptyValue" ? selectedObject.properties["summarySortDirection"] : "0");
        groupHeaderForm.controls.summaryType.setKey(selectedObject.properties["summaryType"] != "StiEmptyValue" ? selectedObject.properties["summaryType"] : "13");
    }

    groupHeaderForm.action = function () {
        groupHeaderForm.changeVisibleState(false);

        var condition = groupHeaderForm.textAreaExpression.value;

        if (groupHeaderForm.mode == "DataColumn") {
            condition = (groupHeaderForm.dataTree.key ? "{" + groupHeaderForm.dataTree.key + "}" : "");
        }

        var selectedObjects = this.jsObject.options.selectedObjects || [this.jsObject.options.selectedObject];
        if (!selectedObjects) return;
        for (var i = 0; i < selectedObjects.length; i++) {
            var selectedObject = selectedObjects[i];
            selectedObject.properties["condition"] = Base64.encode(condition);
            selectedObject.properties["summaryExpression"] = Base64.encode(groupHeaderForm.textAreaSummaryExpression.value);
            selectedObject.properties["sortDirection"] = groupHeaderForm.controls.sortDirection.key;
            selectedObject.properties["summarySortDirection"] = groupHeaderForm.controls.summarySortDirection.key;
            selectedObject.properties["summaryType"] = groupHeaderForm.controls.summaryType.key;
        }
        this.jsObject.SendCommandSendProperties(selectedObjects, ["condition", "summaryExpression", "sortDirection", "summarySortDirection", "summaryType"]);
    }

    return groupHeaderForm;
}