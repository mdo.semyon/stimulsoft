﻿
StiMobileDesigner.prototype.InitializeEditResourceForm_ = function () {
    var editResourceForm = this.BaseForm("editResourceForm", this.loc.PropertyMain.Column, 3, this.HelpLinks["columnEdit"]);
    editResourceForm.resource = null;
    editResourceForm.controls = {};

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px 0 5px 0";
    editResourceForm.container.appendChild(innerTable);

    var controlProps = [
        ["name", this.loc.PropertyMain.Name, this.TextBox(null, 270)],
        ["alias", this.loc.PropertyMain.Alias, this.TextBox(null, 270)]
    ]

    for (var i = 0; i < controlProps.length; i++) {
        innerTable.addTextCellInNextRow(controlProps[i][1] + ":").className = "stiDesignerCaptionControlsBigIntervals";
        innerTable.addCellInLastRow(controlProps[i][2]).className = "stiDesignerControlCellsBigIntervals";
        editResourceForm.controls[controlProps[i][0]] = controlProps[i][2];
    }

    var resourceContainer = this.ResourceContainer(null, 450, 250);
    resourceContainer.style.margin = "8px";
    editResourceForm.controls.resourceContainer = resourceContainer;
    editResourceForm.container.appendChild(resourceContainer);

    resourceContainer.action = function () {
        editResourceForm.controls.name.value = this.resourceName;
        editResourceForm.controls.alias.value = this.resourceName;
    }

    editResourceForm.controls.name.action = function () {
        if (this.oldValue == editResourceForm.controls.alias.value) {
            editResourceForm.controls.alias.value = this.value;
        }
    }

    editResourceForm.onshow = function () {
        this.mode = "Edit";
        if (this.resource == null) {
            this.resource = this.jsObject.ResourceObject();
            this.mode = "New";
        }
        this.caption.innerHTML = this.jsObject.loc.FormDictionaryDesigner["Resource" + this.mode];
        this.controls.name.hideError();
        this.controls.name.focus();
        this.controls.name.value = this.resource.name;
        this.controls.alias.value = this.resource.alias;
        resourceContainer.clear();
        
        if (this.mode == "Edit") {
            resourceContainer.getResourceContentFromServer(this.resource.name);
        }
        else {
            resourceContainer.setResource(null, this.resource.type, this.resource.name, 0, null, false);
        }
    }

    editResourceForm.action = function () {
        var resource = {};
        resource.mode = this.mode;

        if (!this.controls.name.checkNotEmpty(this.jsObject.loc.PropertyMain.Name)) return;
        if ((this.mode == "New" || this.resource.name != this.controls.name.value) &&
            !this.controls.name.checkExists(this.jsObject.options.report.dictionary.resources, "name"))
            return;

        if (this.mode == "Edit") resource.oldName = this.resource.name;
        resource.name = this.controls.name.value;
        resource.alias = this.controls.alias.value;
        resource.type = resourceContainer.resourceType;
        resource.loadedContent = this.jsObject.options.mvcMode ? encodeURIComponent(resourceContainer.loadedContent) : resourceContainer.loadedContent;
        resource.haveContent = resourceContainer.haveContent;

        this.changeVisibleState(false);
        this.jsObject.SendCommandCreateOrEditResource(resource);
    }

    return editResourceForm;
}