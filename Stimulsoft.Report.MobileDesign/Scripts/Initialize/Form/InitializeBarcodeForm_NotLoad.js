﻿
StiMobileDesigner.prototype.InitializeBarCodeForm_ = function () {
    var barCodeForm = this.BaseFormPanel("barCodeForm", " ", 1, this.HelpLinks["barcodeform"]);
    barCodeForm.controls = {};
    barCodeForm.barCodeSamples = null;

    //Tabs
    var tabs = [];
    tabs.push({ "name": "BarCode", "caption": this.loc.PropertyCategory.BarCodeCategory });
    tabs.push({ "name": "Settings", "caption": this.loc.Export.Settings });
    var tabbedPane = this.TabbedPane("barCodeFormTabbedPane", tabs, this.GetStyles("StandartTab"));
    barCodeForm.tabbedPane = tabbedPane;
    barCodeForm.container.appendChild(tabbedPane);

    for (var i in tabs) {
        var tabsPanel = tabbedPane.tabsPanels[tabs[i].name];
        tabsPanel.style.width = "750px";
        tabsPanel.style.height = "450px";
        tabsPanel.appendChild(this.FormSeparator());

        switch (tabs[i].name) {
            case "BarCode": this.InitializeBarCodeFormBarCodeTabPanel(barCodeForm, tabsPanel); break;
            case "Settings": this.InitializeBarCodeFormSettingsTabPanel(barCodeForm, tabsPanel); break;
        }
    }

    //Hide Header Caption
    barCodeForm.caption.innerHTML = "";
    barCodeForm.caption.appendChild(tabbedPane.tabsPanel);
    barCodeForm.caption.style.padding = "0px";
    barCodeForm.container.style.borderTop = "0px";

    barCodeForm.onshow = function () {
        var selectedObject = this.jsObject.options.selectedObject;
        if (!selectedObject) this.changeVisibleState(false);
        this.selectedObject = selectedObject;

        this.jsObject.options.propertiesPanel.setEditBarCodeMode(true);
        tabbedPane.showTabPanel("BarCode");

        if (!barCodeForm.barCodeSamples){            
            this.jsObject.SendCommandToDesignerServer("GetBarCodeSamples", { componentName: barCodeForm.barCode.name }, function (answer) {
                barCodeForm.barCodeSamples = answer.barCodeSamples;
                barCodeForm.samplesPanel.buildSamples();
            });
        }
        else {
            barCodeForm.samplesPanel.buildSamples();
        }

        barCodeForm.updateControls();
    }

    barCodeForm.onhide = function () {
        this.jsObject.options.propertiesPanel.setEditBarCodeMode(false);
    }

    barCodeForm.action = function () {
        this.changeVisibleState(false);
        barCodeForm.jsObject.SendCommandSendProperties(this.selectedObject, []);
    }

    barCodeForm.cancelAction = function () {
        this.jsObject.SendCommandCanceledEditComponent(barCodeForm.barCode.name);
    }

    barCodeForm.applyBarCodeProperties = function (properties) {
        this.jsObject.SendCommandToDesignerServer("ApplyBarCodeProperties", { componentName: barCodeForm.barCode.name, properties: properties }, function (answer) {
            barCodeForm.barCode = answer.barCode;
            barCodeForm.updateControls();
        });
    }

    barCodeForm.updateControls = function () {
        barCodeForm.barCodeSample.update();
        barCodeForm.propertiesContainer.update();
        var barCodePropertiesPanel = this.jsObject.options.propertiesPanel.editBarCodePropertiesPanel;
        if (barCodePropertiesPanel) {
            barCodePropertiesPanel.updateProperties(barCodeForm.barCode.properties.additional);
        }
    }

    return barCodeForm;
}

//BarCode
StiMobileDesigner.prototype.InitializeBarCodeFormBarCodeTabPanel = function (barCodeForm, parentPanel) {
    var samplesPanel = document.createElement("div");
    samplesPanel.jsObject = this;
    samplesPanel.style.width = parentPanel.style.width;
    samplesPanel.style.height = parentPanel.style.height;
    samplesPanel.style.overflow = "auto";
    barCodeForm.samplesPanel = samplesPanel;
    parentPanel.appendChild(samplesPanel);
    samplesPanel.selectedItem = null;
    samplesPanel.items = {};

    parentPanel.onshow = function () { }

    samplesPanel.buildSamples = function () {
        samplesPanel.clear();

        if (barCodeForm.barCodeSamples) {
            for (var i = 0; i < barCodeForm.barCodeSamples.length; i++) {
                var sampleItem = this.jsObject.BarCodeSampleItem(barCodeForm.barCodeSamples[i]);
                samplesPanel.appendChild(sampleItem);
                samplesPanel.items[barCodeForm.barCodeSamples[i].type] = sampleItem;

                sampleItem.action = function () {
                    if (this.isSelected) return;
                    this.select();
                    barCodeForm.applyBarCodeProperties([{ name: "codeType", value: this.sampleObject.type }]);
                }

                sampleItem.select = function () {
                    if (samplesPanel.selectedItem) samplesPanel.selectedItem.setSelected(false);
                    this.setSelected(true);
                    samplesPanel.selectedItem = this;                    
                }                
            }

            if (samplesPanel.items[barCodeForm.barCode.codeType]) {
                samplesPanel.items[barCodeForm.barCode.codeType].select();
                samplesPanel.autoscroll();
            }
        }
    }

    samplesPanel.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        this.items = {};
        this.selectedItem = null;
    }

    samplesPanel.autoscroll = function () {
        if (this.selectedItem && samplesPanel.offsetHeight > 0) {
            samplesPanel.scrollTop = 0;
            var yPos = this.jsObject.FindPosY(this.selectedItem, null, null, samplesPanel);
            if (yPos + 150 > samplesPanel.offsetHeight) samplesPanel.scrollTop = yPos - samplesPanel.offsetHeight + 150;
        }
    }
}

StiMobileDesigner.prototype.BarCodeSampleItem = function (sampleObject) {
    var item = this.StandartBigButton(null, null, sampleObject.type, " ", sampleObject.type);
    item.sampleObject = sampleObject;
    item.style.display = "inline-block";
    item.style.margin = "10px 0 0 10px";
    item.cellImage.removeChild(item.image);
    var image = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "svg") : document.createElement("svg");
    image.style.margin = "5px";
    image.setAttribute("width", sampleObject.width);
    image.setAttribute("height", sampleObject.height);
    image.setAttribute("x", 0);
    image.setAttribute("y", 0);

    item.cellImage.appendChild(image);
    this.InsertSvgContent(image, sampleObject.image);
    
    return item;
}

//Settings
StiMobileDesigner.prototype.InitializeBarCodeFormSettingsTabPanel = function (barCodeForm, parentPanel) {
    var jsObject = this;
    var mainTable = this.CreateHTMLTable();
    mainTable.style.width = "100%";
    parentPanel.appendChild(mainTable);

    //Properties
    var propertiesContainer = document.createElement("div");
    barCodeForm.propertiesContainer = propertiesContainer;
    propertiesContainer.className = "stiBarCodeFormPropertiesContainer";
    mainTable.addCell(propertiesContainer).style.width = "400px";
        
    var properties = [
       ["code", this.loc.PropertyMain.Code, this.PropertyTextBox("barCodeFormAngle", this.options.propertyControlWidth), "6px"],
       ["angle", this.loc.PropertyMain.Angle, this.PropertyDropDownList("barCodeFormAngle", this.options.propertyControlWidth, this.GetBarCodeAngleItems(), true, false), "6px"],
       ["autoScale", this.loc.PropertyMain.AutoScale, this.CheckBox("barCodeFormAutoScale"), "7px 6px 7px 6px"],
       ["foreColor", this.loc.PropertyMain.ForeColor, this.PropertyColorControl("barCodeFormForeColor", null, this.options.propertyControlWidth), "5px 6px 5px 6px"],
       ["backColor", this.loc.PropertyMain.BackColor, this.PropertyColorControl("barCodeFormBackColor", null, this.options.propertyControlWidth), "5px 6px 5px 6px"],
       ["horAlignment", this.loc.PropertyMain.HorAlignment, this.PropertyDropDownList("barCodeFormHorAlignment", this.options.propertyControlWidth, this.GetHorizontalAlignmentItems(true), true, false), "6px"],
       ["vertAlignment", this.loc.PropertyMain.VertAlignment, this.PropertyDropDownList("barCodeFormVertAlignment", this.options.propertyControlWidth, this.GetVerticalAlignmentItems(), true, false), "6px"],       
       ["showLabelText", this.loc.PropertyMain.ShowLabelText, this.CheckBox("barCodeShowLabelText"), "8px 6px 7px 6px"],
       ["showQuietZones", this.loc.PropertyMain.ShowQuietZones, this.CheckBox("barCodeShowQuietZones"), "7px 6px 8px 6px"],
       ["font", this.loc.PropertyMain.Font, this.PropertyFontControl("barCodeFormFont", "font", true), "6px"]
    ];

    var proprtiesTable = this.CreateHTMLTable();
    proprtiesTable.style.margin = "";
    propertiesContainer.appendChild(proprtiesTable);

    for (var i = 0; i < properties.length; i++) {
        var textCell = proprtiesTable.addTextCellInLastRow(properties[i][1]);
        textCell.className = "stiDesignerCaptionControlsBigIntervals";
        textCell.style.whiteSpace = "normal";
        textCell.style.width = "100%";
        if (properties[i][0] == "font") {
            textCell.style.verticalAlign = "top";
            textCell.style.paddingTop = "10px";
        }

        var control = properties[i][2];
        control.propertyName = properties[i][0];
        control.style.margin = properties[i][3];
        proprtiesTable.addCellInLastRow(control);
        proprtiesTable.addRow();
        jsObject.AddMainMethodsToPropertyControl(control);

        control.action = function () {
            barCodeForm.applyBarCodeProperties([{ name: this.propertyName, value: this.getValue() }]);
        }
    }
    
    propertiesContainer.update = function () {
        var commonProperties = barCodeForm.barCode.properties.common;
        for (var i = 0; i < properties.length; i++) {
            var control = properties[i][2];
            if (commonProperties[control.propertyName] != null) {
                control.setValue(commonProperties[control.propertyName])
            }
            if (control.propertyName == "code") {
                control.setEnabled(barCodeForm.barCode.codeType != "SSCC");
                if (barCodeForm.barCode.codeType == "SSCC") {
                    control.value = Base64.decode(commonProperties[control.propertyName].replace("Base64Code;", "")).replace(/\u001f/g, '');
                }
            }
        }
    }

    //BarCode Sample
    var barCodeSample = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "svg") : document.createElement("svg");
    barCodeForm.barCodeSample = barCodeSample;
    barCodeSample.setAttribute("width", "300");
    barCodeSample.setAttribute("height", "400");
    barCodeSample.style.overflow = "hidden";

    barCodeSample.update = function () {
        jsObject.InsertSvgContent(this, barCodeForm.barCode.sampleImage);
    }

    var cell = mainTable.addCell(barCodeSample);
    cell.style.textAlign = "center";
    cell.style.verticalAlign = "middle";

    parentPanel.onshow = function () {
        
    }
}