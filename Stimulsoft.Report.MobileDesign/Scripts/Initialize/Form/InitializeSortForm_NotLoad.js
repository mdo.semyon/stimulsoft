﻿
StiMobileDesigner.prototype.InitializeSortForm_ = function () {
    //Sort Form
    var sortForm = this.BaseForm("sortForm", this.loc.PropertyMain.Sort, 2, this.HelpLinks["sort"]);
    sortForm.sortControl = this.SortControl("sortFormSortControl", null, 450, 250);
    sortForm.container.appendChild(sortForm.sortControl);
    sortForm.resultControl = null;

    sortForm.onshow = function () {
        this.tempResultControl = (this.resultControl) ? this.resultControl : null;
        this.resultControl = null;
        this.sortControl.currentDataSourceName = null;
        var sortData = (this.tempResultControl == null) ? this.jsObject.options.selectedObject.properties.sortData : this.tempResultControl.formResult;
        if (this.tempResultControl) this.sortControl.currentDataSourceName = this.tempResultControl.currentDataSourceName;
        var sorts = (sortData != "") ? JSON.parse(Base64.decode(sortData)) : [];
        this.sortControl.fill(sorts);
        this.sortControl.sortContainer.addSort({ "column": this.jsObject.loc.FormBand.NoSort, "direction": "ASC" });
    }

    sortForm.action = function () {
        var result = this.sortControl.getValue();
        sortForm.changeVisibleState(false);
        if (!this.tempResultControl) {
            this.jsObject.options.selectedObject.properties.sortData = result.length == 0 ? "" : Base64.encode(JSON.stringify(result));
            this.jsObject.SendCommandSendProperties(this.jsObject.options.selectedObject, ["sortData"]);
        }
        else {
            this.tempResultControl.formResult = result.length == 0 ? "" : Base64.encode(JSON.stringify(result));
            if (this.tempResultControl["setKey"] != null) {
                this.tempResultControl.setKey(this.tempResultControl.formResult);
            }
            else {
                this.tempResultControl.value = this.jsObject.SortDataToShortString(this.tempResultControl.formResult);
            }
            this.tempResultControl.action();
        }
    }

    return sortForm;
}