﻿
StiMobileDesigner.prototype.InitializeBorderSetupForm_ = function () {

    var borderSetupForm = this.BaseForm("borderSetup", this.loc.PropertyMain.Borders, 3, this.HelpLinks["borderform"]);
    var formBlock = this.FormBlock();
    formBlock.style.padding = "5px";
    borderSetupForm.container.appendChild(formBlock);
    var mainTable = this.CreateHTMLTable();
    formBlock.appendChild(mainTable);
    var controlsTable = this.CreateHTMLTable();
    controlsTable.style.width = "300px";
    mainTable.addCell(controlsTable);
    borderSetupForm.sampleBar = this.SampleComponent();
    mainTable.addCell(borderSetupForm.sampleBar).style.verticalAlign = "top";
    borderSetupForm.border = null;

    //Sides
    controlsTable.addRow();
    var cellSides = controlsTable.addCell();
    cellSides.style.padding = "6px 0 6px 0";
    cellSides.setAttribute("colspan", "2");
    var sidesHeader = this.FormBlockHeader(this.loc.PropertyMain.Sides);
    cellSides.appendChild(sidesHeader);

    //Sides Buttons
    var buttonsTable = this.CreateHTMLTable();
    var buttonsCell = controlsTable.addCellInLastRow(buttonsTable);
    buttonsCell.setAttribute("colspan", "2");

    //Style
    controlsTable.addRow();
    var cellStyle = controlsTable.addCellInLastRow();
    cellStyle.style.padding = "6px 0 6px 0";
    cellStyle.setAttribute("colspan", "2");
    var styleHeader = this.FormBlockHeader(this.loc.PropertyMain.Style);
    cellStyle.appendChild(styleHeader);

    borderSetupForm.onshow = function () {
        var controls = this.jsObject.options.controls;
        var buttons = this.jsObject.options.buttons;
        var currentObject = this.jsObject.options.selectedObject || this.jsObject.GetCommonObject(this.jsObject.options.selectedObjects);
        if (!currentObject) return;

        this.border = null;
        if (this.showFunction) this.showFunction();
        var borderArray = this.border ? this.border.split("!") : currentObject.properties["border"].split("!");
        var borderSides = borderArray[0].split(",");

        buttons.borderSetupBorderAll.setSelected(borderArray[0] == "1,1,1,1");
        buttons.borderSetupBorderNone.setSelected(borderArray[0] == "0,0,0,0");
        buttons.borderSetupBorderLeft.setSelected(borderSides[0] != "StiEmptyValue" ? borderSides[0] == "1" : false);
        buttons.borderSetupBorderTop.setSelected(borderSides[1] != "StiEmptyValue" ? borderSides[1] == "1" : false);
        buttons.borderSetupBorderRight.setSelected(borderSides[2] != "StiEmptyValue" ? borderSides[2] == "1" : false);
        buttons.borderSetupBorderBottom.setSelected(borderSides[3] != "StiEmptyValue" ? borderSides[3] == "1" : false);
        controls.borderSetupBorderStyle.setKey(borderArray[3]);
        controls.borderSetupBorderColor.setKey(borderArray[2]);
        controls.borderSetupBorderSize.value = borderArray[1] == "StiEmptyValue" ? "" : borderArray[1];
        controls.borderSetupDropShadow.setChecked(borderArray[4] == "StiEmptyValue" ? false : borderArray[4] == "1");
        controls.borderSetupShadowSize.value = borderArray[5] == "StiEmptyValue" ? "" : borderArray[5];
        controls.borderSetupShadowColor.setKey(borderArray[6] == "StiEmptyValue" ? "StiEmptyValue" : this.jsObject.GetColorFromBrushStr(Base64.decode(borderArray[6])));
        controls.borderSetupTopmost.setChecked(borderArray[7] == "StiEmptyValue" ? false : borderArray[7] == "1");
        this.sampleBar.update();
    }

    borderSetupForm.action = function () {
        var controls = this.jsObject.options.controls;
        var buttons = this.jsObject.options.buttons;
        var border = {};
        border.left = buttons.borderSetupBorderLeft.isSelected ? "1" : "0";
        border.top = buttons.borderSetupBorderTop.isSelected ? "1" : "0";
        border.right = buttons.borderSetupBorderRight.isSelected ? "1" : "0";
        border.bottom = buttons.borderSetupBorderBottom.isSelected ? "1" : "0";
        border.style = controls.borderSetupBorderStyle.key;
        border.color = controls.borderSetupBorderColor.key;
        border.size = controls.borderSetupBorderSize.value;
        border.dropShadow = controls.borderSetupDropShadow.isChecked ? "1" : "0";
        border.sizeShadow = controls.borderSetupShadowSize.value;
        border.brushShadow = Base64.encode("1!" + controls.borderSetupShadowColor.key);
        border.topmost = controls.borderSetupTopmost.isChecked ? "1" : "0";
        this.finishFlag = false;
        if (this.actionFunction) this.actionFunction(border);
        if (this.finishFlag) return;

        var selectedObjects = this.jsObject.options.selectedObjects || [this.jsObject.options.selectedObject];
        if (selectedObjects) {
            for (var i = 0; i < selectedObjects.length; i++) {
                var borderProperty = selectedObjects[i].properties["border"];
                if (!borderProperty) continue;
                var borderObject = this.jsObject.BordersStrToObject(borderProperty);
                borderObject.left = buttons.borderSetupBorderLeft.isSelected ? "1" : "0";
                borderObject.top = buttons.borderSetupBorderTop.isSelected ? "1" : "0";
                borderObject.right = buttons.borderSetupBorderRight.isSelected ? "1" : "0";
                borderObject.bottom = buttons.borderSetupBorderBottom.isSelected ? "1" : "0";
                if (controls.borderSetupBorderStyle.key != "StiEmptyValue") borderObject.style = controls.borderSetupBorderStyle.key;
                if (controls.borderSetupBorderColor.key != "StiEmptyValue") borderObject.color = controls.borderSetupBorderColor.key;
                if (controls.borderSetupBorderSize.value != "") borderObject.size = controls.borderSetupBorderSize.value;
                borderObject.dropShadow = controls.borderSetupDropShadow.isChecked ? "1" : "0";
                if (controls.borderSetupShadowSize.value != "") borderObject.sizeShadow = controls.borderSetupShadowSize.value;
                if (controls.borderSetupShadowColor.key != "StiEmptyValue") borderObject.brushShadow = Base64.encode("1!" + controls.borderSetupShadowColor.key);
                borderObject.topmost = controls.borderSetupTopmost.isChecked ? "1" : "0";
                selectedObjects[i].properties.border = this.jsObject.BordersObjectToStr(borderObject);
            }
            this.jsObject.UpdatePropertiesControls();
            this.jsObject.SendCommandSendProperties(selectedObjects, ["border"]);
        }
        this.changeVisibleState(false);
    }

    //BorderAll
    var borderAllButton = this.StandartSmallButton("borderSetupBorderAll", "borderSetupBorders", null, "BorderAll.png", this.loc.HelpDesigner.BorderSidesAll, null);
    borderAllButton.action = function () {
        var buttons = this.jsObject.options.buttons;
        this.setSelected(true);
        buttons.borderSetupBorderLeft.setSelected(true);
        buttons.borderSetupBorderTop.setSelected(true);
        buttons.borderSetupBorderRight.setSelected(true);
        buttons.borderSetupBorderBottom.setSelected(true);
        this.jsObject.options.forms.borderSetup.sampleBar.update();
    }
    buttonsTable.addCell(borderAllButton).style.padding = "0 2px 0 2px";

    //BorderNone
    var borderNoneButton = this.StandartSmallButton("borderSetupBorderNone", "borderSetupBorders", null, "BorderNone.png", this.loc.HelpDesigner.BorderSidesNone, null);
    borderNoneButton.action = function () {
        var buttons = this.jsObject.options.buttons;
        this.setSelected(true);
        buttons.borderSetupBorderLeft.setSelected(false);
        buttons.borderSetupBorderTop.setSelected(false);
        buttons.borderSetupBorderRight.setSelected(false);
        buttons.borderSetupBorderBottom.setSelected(false);
        this.jsObject.options.forms.borderSetup.sampleBar.update();
    }
    buttonsTable.addCell(borderNoneButton).style.padding = "0 2px 0 2px";

    //Separator
    buttonsTable.addCell(this.HomePanelSeparator());

    var buttonsAction = function () {
        this.setSelected(!this.isSelected);
        this.jsObject.UpdateSidesButtons();
        this.jsObject.options.forms.borderSetup.sampleBar.update();
    }

    //BorderLeft
    var borderLeftButton = this.StandartSmallButton("borderSetupBorderLeft", null, null, "BorderLeft.png", this.loc.HelpDesigner.BorderSidesLeft, null);
    borderLeftButton.action = buttonsAction;
    buttonsTable.addCell(borderLeftButton).style.padding = "0 2px 0 2px";

    //BorderTop
    var borderTopButton = this.StandartSmallButton("borderSetupBorderTop", null, null, "BorderTop.png", this.loc.HelpDesigner.BorderSidesTop, null);
    borderTopButton.action = buttonsAction;
    buttonsTable.addCell(borderTopButton).style.padding = "0 2px 0 2px";

    //BorderRight
    var borderRightButton = this.StandartSmallButton("borderSetupBorderRight", null, null, "BorderRight.png", this.loc.HelpDesigner.BorderSidesRight, null);
    borderRightButton.action = buttonsAction;
    buttonsTable.addCell(borderRightButton).style.padding = "0 2px 0 2px";

    //BorderBottom
    var borderBottomButton = this.StandartSmallButton("borderSetupBorderBottom", null, null, "BorderBottom.png", this.loc.HelpDesigner.BorderSidesBottom, null);
    borderBottomButton.action = buttonsAction;
    buttonsTable.addCell(borderBottomButton).style.padding = "0 2px 0 2px";

    //BorderStyle    
    controlsTable.addRow();
    var borderStyleText = controlsTable.addCellInLastRow();
    borderStyleText.className = "stiDesignerCaptionControls";
    borderStyleText.innerHTML = this.loc.PropertyMain.Style + ":";

    //debugger;
    var borderStyle = this.ImageList("borderSetupBorderStyle", true, true, this.loc.HelpDesigner.BorderStyle, this.GetBorderStyleItems());
    borderStyle.setKey("0");
    borderStyle.action = function () {
        this.jsObject.options.forms.borderSetup.sampleBar.update();
    }
    var borderStyleCell = controlsTable.addCellInLastRow(borderStyle);
    borderStyleCell.className = "stiDesignerControlCellsBigIntervals";
    borderStyleCell.style.paddingLeft = this.options.isTouchDevice ? "4px" : "0px";

    //BorderColor   
    controlsTable.addRow();
    var borderColorText = controlsTable.addCellInLastRow();
    borderColorText.className = "stiDesignerCaptionControls";
    borderColorText.innerHTML = this.loc.PropertyMain.Color + ":";

    var borderColor = this.ColorControl("borderSetupBorderColor", this.loc.PropertyMain.BorderColor);
    borderColor.action = function () {
        this.jsObject.options.forms.borderSetup.sampleBar.update();
    }
    controlsTable.addCellInLastRow(borderColor).className = "stiDesignerControlCellsBigIntervals";

    //BorderSize   
    controlsTable.addRow();
    var borderSizeText = controlsTable.addCellInLastRow();
    borderSizeText.className = "stiDesignerCaptionControls";
    borderSizeText.innerHTML = this.loc.PropertyMain.Size + ":";

    var borderSize = this.TextBoxPositiveIntValue("borderSetupBorderSize", 50)
    borderSize.action = function () {
        this.jsObject.options.forms.borderSetup.sampleBar.update();
    }
    var borderSizeCell = controlsTable.addCellInLastRow(borderSize);
    borderSizeCell.className = "stiDesignerControlCellsBigIntervals";
    borderSizeCell.style.paddingLeft = this.options.isTouchDevice ? "7px" : "3px";

    //Topmost
    var topmostCell = controlsTable.addCellInNextRow();
    topmostCell.className = "stiDesignerCaptionControls";
    topmostCell.style.paddingTop = "6px";
    topmostCell.style.paddingBottom = "6px";
    topmostCell.setAttribute("colspan", "2");

    var topmost = this.CheckBox("borderSetupTopmost", this.loc.PropertyMain.Topmost);
    topmostCell.appendChild(topmost);

    //Shadow
    controlsTable.addRow();
    var cellShadow = controlsTable.addCellInLastRow();
    cellShadow.style.padding = "6px 0 6px 0";
    cellShadow.setAttribute("colspan", "2");
    var shadowHeader = this.FormBlockHeader(this.loc.PropertyMain.Shadow);
    cellShadow.appendChild(shadowHeader);

    //Drop Shadow
    var dropShadowCell = controlsTable.addCellInNextRow();
    dropShadowCell.className = "stiDesignerCaptionControls";
    dropShadowCell.style.paddingTop = "6px";
    dropShadowCell.style.paddingBottom = "6px";
    dropShadowCell.setAttribute("colspan", "2");

    var dropShadow = this.CheckBox("borderSetupDropShadow", this.loc.PropertyMain.DropShadow);
    dropShadow.action = function () {
        this.jsObject.options.forms.borderSetup.sampleBar.update();
    }
    dropShadowCell.appendChild(dropShadow);

    //ShadowSize   
    controlsTable.addRow();
    var shadowSizeText = controlsTable.addCellInLastRow();
    shadowSizeText.className = "stiDesignerCaptionControls";
    shadowSizeText.innerHTML = this.loc.PropertyMain.ShadowSize + ":";

    var shadowSize = this.TextBoxPositiveIntValue("borderSetupShadowSize", 50)
    shadowSize.action = function () {
        this.jsObject.options.forms.borderSetup.sampleBar.update();
    }
    var shadowSizeCell = controlsTable.addCellInLastRow(shadowSize);
    shadowSizeCell.className = "stiDesignerControlCellsBigIntervals";
    shadowSizeCell.style.paddingLeft = this.options.isTouchDevice ? "7px" : "3px";

    //ShadowColor   
    controlsTable.addRow();
    var shadowColorText = controlsTable.addCellInLastRow();
    shadowColorText.className = "stiDesignerCaptionControls";
    shadowColorText.innerHTML = this.loc.PropertyMain.Color + ":";

    var shadowColor = this.ColorControl("borderSetupShadowColor", this.loc.PropertyMain.Color);
    shadowColor.action = function () {
        this.jsObject.options.forms.borderSetup.sampleBar.update();
    }
    controlsTable.addCellInLastRow(shadowColor).className = "stiDesignerControlCellsBigIntervals";

    return borderSetupForm;
}

StiMobileDesigner.prototype.SampleComponent = function () {
    var sampleParent = this.CreateHTMLTable();
    sampleParent.style.width = "90px";
    sampleParent.style.height = "90px";
    sampleParent.style.margin = "10px 10px 10px 15px";    
    sampleParent.style.background = "f5f5f5";
    sampleParent.style.border = "1px solid #d3d3d3";
    sampleParent.jsObject = this;    
    
    sampleParent.bar = document.createElement("div");    
    sampleParent.bar.style.width = "70px";
    sampleParent.bar.style.height = "70px";
    sampleParent.bar.style.display = "inline-block";
    sampleParent.addCell(sampleParent.bar).style.textAlign = "center";
        
    sampleParent.update = function () {        
        var controls = this.jsObject.options.controls;
        var buttons = this.jsObject.options.buttons;        
        var borderColor = "rgb(" + this.jsObject.ColorToRGBStr(controls.borderSetupBorderColor.key) + ")";        
        var currentStyle = controls.borderSetupBorderStyle.key;
        var currentborderSize = controls.borderSetupBorderSize.value;
        var borderSize = (currentStyle == "5") ? "3" : currentborderSize;        
        var styles = ["solid", "dashed", "dashed", "dashed", "dotted", "double", "none"];
        
                    
        this.bar.style.borderLeft = borderSize + "px " + styles[currentStyle] + " " + 
            (buttons.borderSetupBorderLeft.isSelected ? borderColor : "transparent");
        this.bar.style.borderTop = borderSize + "px " + styles[currentStyle] + " " + 
            (buttons.borderSetupBorderTop.isSelected ? borderColor : "transparent");
        this.bar.style.borderRight = borderSize + "px " + styles[currentStyle] + " " + 
            (buttons.borderSetupBorderRight.isSelected ? borderColor : "transparent");
        this.bar.style.borderBottom = borderSize + "px " + styles[currentStyle] + " " + 
            (buttons.borderSetupBorderBottom.isSelected ? borderColor : "transparent");
         
        this.bar.style.boxShadow = controls.borderSetupDropShadow.isChecked 
            ? "3px 3px " + controls.borderSetupShadowSize.value + "px rgba(" + this.jsObject.ColorToRGBStr(controls.borderSetupShadowColor.key) + ",0.8)"
            : "";
    }
    
    return sampleParent;
}

StiMobileDesigner.prototype.UpdateSidesButtons = function () {
    var buttons = this.options.buttons;
    if (buttons.borderSetupBorderLeft.isSelected && buttons.borderSetupBorderTop.isSelected &&
        buttons.borderSetupBorderRight.isSelected && buttons.borderSetupBorderBottom.isSelected)
            buttons.borderSetupBorderAll.setSelected(true);
    else
        if (!buttons.borderSetupBorderLeft.isSelected && !buttons.borderSetupBorderTop.isSelected &&
            !buttons.borderSetupBorderRight.isSelected && !buttons.borderSetupBorderBottom.isSelected)
                buttons.borderSetupBorderNone.setSelected(true);
        else {
            buttons.borderSetupBorderAll.setSelected(false);
            buttons.borderSetupBorderNone.setSelected(false);
        }    
}
