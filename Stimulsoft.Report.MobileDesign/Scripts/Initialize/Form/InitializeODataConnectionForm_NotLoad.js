﻿
StiMobileDesigner.prototype.InitializeODataConnectionForm_ = function () {

    var form = this.BaseForm("oDataConnectionForm", this.loc.FormDatabaseEdit.ConnectionString.replace(":", ""), 4);

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px 0 5px 0";
    form.container.appendChild(innerTable);
        
    var controlProps = [
        ["address", this.loc.Report.Address + ":", this.TextBox(null, 300)],
        ["addressBearer", "Address Bearer:", this.TextBox(null, 300)],
        ["userName", this.loc.Report.LabelUserName, this.TextBox(null, 200)],
        ["password", this.loc.Cloud.labelPassword, this.TextBox(null, 200)],
        ["useBearerAuthentication", "", this.CheckBox(null, this.loc.FormDatabaseEdit.UseBearerAuthentication)]
    ]

    //PathToData
    for (var i in controlProps) {
        form[controlProps[i][0] + "ControlRow"] = innerTable.addRow();
        innerTable.addTextCellInLastRow(controlProps[i][1]).className = "stiDesignerCaptionControlsBigIntervals";
        var control = controlProps[i][2];
        form[controlProps[i][0] + "Control"] = control;
        innerTable.addCellInLastRow(control).className = "stiDesignerControlCellsBigIntervals";
    }

    form.useBearerAuthenticationControl.style.margin = "3px 0 3px 0";
    form.passwordControl.setAttribute("type", "password");

    form.useBearerAuthenticationControl.action = function () {
        form.addressBearerControlRow.style.display = this.isChecked ? "" : "none";
    }
   
    form.getConnectionString = function () {
        var connectStr = this.addressControl.value;
        if (this.addressBearerControl.value && this.useBearerAuthenticationControl.isChecked) { connectStr += ";AddressBearer=" + this.addressBearerControl.value; }
        if (this.userNameControl.value) { connectStr += ";UserName=" + this.userNameControl.value; }
        if (this.passwordControl.value) { connectStr += ";Password=" + this.passwordControl.value; }

        return connectStr;
    }

    form.show = function (connection) {
        this.useBearerAuthenticationControl.setChecked(connection.AddressBearer);
        this.addressBearerControlRow.style.display = this.useBearerAuthenticationControl.isChecked ? "" : "none";
        this.addressBearerControl.value = connection.AddressBearer || "";
        this.addressControl.value = connection.Address || "";
        this.userNameControl.value = connection.UserName || "";
        this.passwordControl.value = connection.Password || "";
        this.changeVisibleState(true);
        this.addressControl.focus();
    }

    form.action = function () {
        this.changeVisibleState(false);
    }

    return form;
}