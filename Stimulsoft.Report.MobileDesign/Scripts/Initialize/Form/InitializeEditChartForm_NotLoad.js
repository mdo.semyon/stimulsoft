﻿
StiMobileDesigner.prototype.InitializeEditChartForm_ = function () {
    //Edit Chart Form
    var editChartForm = this.BaseFormPanel("editChart", " ", 1, this.HelpLinks["chartform"]);
    editChartForm.mode = "create";

    //Chart Image
    editChartForm.chartImage = document.createElement("div");
    editChartForm.chartImage.className = "stiDesignerChartImage";

    editChartForm.chartImage.update = function () {
        this.innerHTML = editChartForm.chartProperties.chartImage;
    }

    //Tabs
    var tabs = [];
    tabs.push({ "name": "Chart", "caption": this.loc.Components.StiChart });
    tabs.push({ "name": "Series", "caption": this.loc.Chart.Serieses });
    tabs.push({ "name": "Area", "caption": this.loc.Chart.Area });
    tabs.push({ "name": "Labels", "caption": this.loc.Chart.Labels });
    tabs.push({ "name": "Styles", "caption": this.loc.PropertyMain.Styles });
    var tabbedPane = this.TabbedPane("editChartTabbedPane", tabs, this.GetStyles("StandartTab"));
    editChartForm.tabbedPane = tabbedPane;
    editChartForm.container.appendChild(tabbedPane);

    for (var i in tabs) {
        tabbedPane.tabsPanels[tabs[i].name].appendChild(this.FormSeparator());
        var tabsPanel = null;
        switch (tabs[i].name) {
            case "Chart": tabsPanel = this.EditChartFormChartTabPanel(editChartForm); break;
            case "Series": tabsPanel = this.EditChartFormSeriesTabPanel(editChartForm); break;
            case "Area": tabsPanel = this.EditChartFormAreaTabPanel(editChartForm); break;
            case "Labels": tabsPanel = this.EditChartFormLabelsTabPanel(editChartForm); break;
            case "Styles": tabsPanel = this.EditChartFormStylesTabPanel(editChartForm); break;
        }

        if (tabsPanel) {
            tabsPanel.style.width = "750px";
            tabsPanel.style.height = "460px";
            tabbedPane.tabsPanels[tabs[i].name].appendChild(tabsPanel);
        }

        //Methods Tabs On Show Event
        tabbedPane.tabsPanels[tabs[i].name].onshow = function () {
            //Move Chart Image To Current Panel
            var imageContainer = editChartForm["imageContainer" + this.name + "Tab"];
            if (imageContainer) {
                imageContainer.appendChild(editChartForm.chartImage);
            }

            //Show Current Properties 
            editChartForm.jsObject.options.propertiesPanel.editChartPropertiesPanel.showInnerPanel(this.name);
            editChartForm.onChangeTabs();

            //On Show Events
            if (this.name == "Chart") {
                var seriesCount = editChartForm.seriesContainer.items.length;
                editChartForm.seriesWizardContainer.style.display = seriesCount == 0 ? "" : "none";
                editChartForm.chartTabMainTable.style.display = seriesCount == 0 ? "none" : "";
                editChartForm.chartPropertiesContainer.buttons.Common.action();
            }
            if (this.name == "Series") {
                editChartForm.seriesPropertiesContainer.update();
            }
            if (this.name == "Area") {
                editChartForm.areaPropertiesContainer.update();
            }
            if (this.name == "Labels") {
                if (editChartForm.chartProperties.area.type != editChartForm.lastAreaTypeForLabels ||
                    editChartForm.chartProperties.style.type + editChartForm.chartProperties.style.name != editChartForm.lastStyleId) {
                    editChartForm.labelsContainer.labelsProgress.style.display = "";
                    editChartForm.labelsContainer.clear();
                    editChartForm.jsObject.SendCommandGetLabelsContent({ componentName: editChartForm.chartProperties.name });
                }
                else {
                    editChartForm.labelPropertiesContainer.buttons.Common.action();
                }
            }
            if (this.name == "Styles") {
                if (editChartForm.chartProperties.area.type != editChartForm.lastAreaTypeForStyles || editChartForm.jsObject.options.stylesIsModified) {
                    editChartForm.jsObject.options.stylesIsModified = false;
                    editChartForm.stylesContainer.stylesProgress.style.display = "";
                    editChartForm.stylesContainer.clear();
                    editChartForm.jsObject.SendCommandGetStylesContent({ componentName: editChartForm.chartProperties.name });
                }
            }
        }
    }

    editChartForm.onChangeTabs = function () {
        var containerNames = ["seriesConditionsPanel", "labelsConditionsPanel", "seriesFiltersPanel"];
        for (var i in containerNames) {
            if (editChartForm[containerNames[i]].container.isModified) {
                editChartForm[containerNames[i]].container.isModified = false;
                editChartForm[containerNames[i]].container.sendValueToServer();
            }
        }
    }

    //Hide Header Caption
    editChartForm.caption.innerHTML = "";
    editChartForm.caption.appendChild(tabbedPane.tabsPanel);
    editChartForm.caption.style.padding = "0px";
    editChartForm.container.style.borderTop = "0px";

    //Form Methods
    editChartForm.onshow = function () {
        editChartForm.jsObject.options.propertiesPanel.setEditChartMode(true);
        editChartForm.seriesContainer.update();
        editChartForm.ConstantLinesContainer.update();
        editChartForm.StripsContainer.update();
        editChartForm.chartImage.update();
        tabbedPane.showTabPanel("Chart");
    }

    editChartForm.onhide = function () {
        editChartForm.jsObject.options.propertiesPanel.setEditChartMode(false);
    }

    editChartForm.cancelAction = function () {
        editChartForm.jsObject.SendCommandCanceledEditComponent(editChartForm.chartProperties.name);
    }

    editChartForm.action = function () {
        var containerNames = ["seriesConditionsPanel", "labelsConditionsPanel", "seriesFiltersPanel"];
        for (var i in containerNames) {
            if (editChartForm[containerNames[i]].container.isModified) {
                editChartForm[containerNames[i]].container.isModified = false;
                editChartForm[containerNames[i]].container.sendValueToServer(true);
                return;
            }
        }
        editChartForm.changeVisibleState(false);
        editChartForm.jsObject.options.report.chartStylesContent = null;
        editChartForm.jsObject.SendCommandSendProperties(editChartForm.currentChartComponent, []);
    }

    return editChartForm;
}

//Chart Tab
StiMobileDesigner.prototype.EditChartFormChartTabPanel = function (editChartForm) {
    var panel = document.createElement("div");
    var mainTable = this.CreateHTMLTable();
    mainTable.style.width = "750px";
    mainTable.style.height = "460px";
    editChartForm.chartTabMainTable = mainTable;
    panel.appendChild(mainTable);

    //Toolbar
    var buttons = [
        ["add", " ", "AddSeries.png", null],
        ["remove", null, "Remove.png", " "],
        ["separator"],
        ["moveUp", null, "MoveUp.png", this.loc.QueryBuilder.MoveUp],
        ["moveDown", null, "MoveDown.png", this.loc.QueryBuilder.MoveDown]
    ]

    var toolBar = this.CreateHTMLTable();
    toolBar.style.margin = "4px";
    editChartForm.stripOrConstantLinesToolBar = toolBar;
    var toolBarCell = mainTable.addCell(toolBar);
    toolBarCell.className = "stiDesignerChartFormToolBarCell";
    toolBarCell.setAttribute("colspan", "2");

    toolBar.changeVisibleState = function (state) {
        toolBar.style.display = state ? "" : "none";
        toolBarCell.className = state ? "stiDesignerChartFormToolBarCell" : "stiDesignerChartFormToolBarCellWithOutBorder";
    }

    toolBar.setMode = function (mode) {
        toolBar.mode = mode; //Strips || ConstantLines
        toolBar.add.caption.innerHTML = mode == "Strips" ? this.jsObject.loc.Chart.AddStrip : this.jsObject.loc.Chart.AddConstantLine;
        toolBar.remove.setAttribute("title", mode == "Strips" ? this.jsObject.loc.Chart.RemoveStrip : this.jsObject.loc.Chart.RemoveConstantLine);
        editChartForm[mode + "Container"].onChange();
    }

    for (var i in buttons) {
        if (buttons[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton("editChartFormStripOrConstantLines" + buttons[i][0], null, buttons[i][1], buttons[i][2],
            buttons[i][1] || buttons[i][3], buttons[i][4], this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "1px";
        toolBar[buttons[i][0]] = button;
        toolBar.addCell(button);
    }

    //Chart Properties Container
    editChartForm.chartPropertiesContainer = this.ChartPropertiesContainer(editChartForm);
    var cellForProperties = mainTable.addCell(editChartForm.chartPropertiesContainer);
    cellForProperties.setAttribute("rowspan", "2");
    cellForProperties.style.width = "1px";

    //Add Strip && Constant Lines Container
    var stripAndConstantLinesCell = mainTable.addCellInNextRow();
    stripAndConstantLinesCell.style.width = "1px";

    var containerNames = ["ConstantLines", "Strips"];
    for (var i in containerNames) {
        var container = this.StripOrConstantLinesContainer(editChartForm, containerNames[i], toolBar);
        editChartForm[containerNames[i] + "Container"] = container;
        stripAndConstantLinesCell.appendChild(container);
        container.style.display = "none";
    }

    //Add ConstantLine Or Strip
    toolBar.add.action = function (itemType) {
        editChartForm.jsObject.SendCommandAddConstantLineOrStrip({
            componentName: editChartForm.chartProperties.name,
            itemType: toolBar.mode
        });
    }

    //Remove ConstantLine Or Strip
    toolBar.remove.action = function () {
        editChartForm.jsObject.SendCommandRemoveConstantLineOrStrip({
            componentName: editChartForm.chartProperties.name,
            itemIndex: editChartForm[toolBar.mode + "Container"].getSelectedIndex(),
            itemType: toolBar.mode
        });
    }

    var itemMove = function (direction) {
        editChartForm.jsObject.SendCommandConstantLineOrStripMove({
            componentName: editChartForm.chartProperties.name,
            itemIndex: editChartForm[toolBar.mode + "Container"].getSelectedIndex(),
            itemType: toolBar.mode,
            direction: direction
        });
    }

    //Series Move Up
    toolBar.moveUp.action = function () { itemMove("Up"); }

    //Series Move Down
    toolBar.moveDown.action = function () { itemMove("Down"); }

    //Chart image container
    editChartForm.imageContainerChartTab = mainTable.addCellInLastRow();
    editChartForm.imageContainerChartTab.style.textAlign = "center";

    //Series wizard container
    var seriesWizardContainer = this.SeriesWizardContainer(editChartForm);
    editChartForm.seriesWizardContainer = seriesWizardContainer;
    panel.appendChild(seriesWizardContainer);

    return panel;
}

StiMobileDesigner.prototype.StripOrConstantLinesContainer = function (editChartForm, containerType, toolBar) {
    var container = this.Container("editChartFormContainer" + containerType, 200, this.options.isTouchDevice ? 420 : 425);
    container.containerType = containerType;
    container.className = "stiDesignerSeriesContainer";

    container.onChange = function () {
        toolBar.remove.setEnabled(this.items.length > 0);
        var selectedIndex = this.getSelectedIndex();
        toolBar.moveUp.setEnabled(selectedIndex != -1 && selectedIndex > 0);
        toolBar.moveDown.setEnabled(selectedIndex != -1 && selectedIndex < this.items.length - 1);
        var editChartPropertiesPanel = editChartForm.jsObject.options.propertiesPanel.editChartPropertiesPanel;
        editChartPropertiesPanel.innerPanels["Chart"].showInnerPanel(this.containerType);
    }

    container.update = function (notSelectedAfter) {
        this.clear();
        var collection = this.containerType == "ConstantLines" ? editChartForm.chartProperties.constantLines : editChartForm.chartProperties.strips;

        for (var i in collection) {
            this.addItemAndNotAction(collection[i].name, collection[i]);
        }
        if (this.items.length > 0 && !notSelectedAfter) this.items[0].action();
    }

    return container;
}

//Chart Properties Container
StiMobileDesigner.prototype.ChartPropertiesContainer = function (editChartForm) {
    var propertiesContainer = this.ContainerWithBigItems("editChartFormChartPropertiesContainer", 120, 460);
    propertiesContainer.className = "stiDesignerSeriesPropertiesContainer";
    propertiesContainer.buttons = {};

    var propNames = [
        ["Common", this.loc.Chart.Common],
        ["Legend", this.loc.PropertyMain.Legend],
        ["Title", this.loc.PropertyMain.Title],
        ["ConstantLines", this.loc.PropertyMain.ConstantLines],
        ["Strips", this.loc.PropertyMain.Strips],
        ["Table", this.loc.PropertyMain.Table]]

    for (var i in propNames) {
        var button = propertiesContainer.addItemAndNotAction(propNames[i][0], propNames[i][1], "StiChart_" + propNames[i][0] + ".png", {});
        button.style.minWidth = "105px";
        propertiesContainer.buttons[propNames[i][0]] = button;
    }

    propertiesContainer.onAction = function () {
        var selectedName = this.selectedItem.name;
        var editChartPropertiesPanel = editChartForm.jsObject.options.propertiesPanel.editChartPropertiesPanel;
        editChartPropertiesPanel.innerPanels["Chart"].showInnerPanel(selectedName);

        editChartForm.stripOrConstantLinesToolBar.changeVisibleState(selectedName == "ConstantLines" || selectedName == "Strips");
        editChartForm.StripsContainer.style.display = (selectedName == "Strips") ? "" : "none";
        editChartForm.ConstantLinesContainer.style.display = (selectedName == "ConstantLines") ? "" : "none";

        if (selectedName == "ConstantLines" || selectedName == "Strips") {
            editChartForm.stripOrConstantLinesToolBar.setMode(selectedName);
        }
    }

    return propertiesContainer;
}

//Series Tab
StiMobileDesigner.prototype.EditChartFormSeriesTabPanel = function (editChartForm) {
    var mainTable = this.CreateHTMLTable();

    //Toolbar
    var buttons = [
        ["addSeries", this.loc.Chart.AddSeries.replace("&", ""), "AddSeries.png", null, "Down"],
        ["removeSeries", null, "Remove.png", this.loc.Chart.RemoveSeries.replace("&", "")],
        ["separator"],
        ["moveUp", null, "MoveUp.png", this.loc.Chart.MoveSeriesUp],
        ["moveDown", null, "MoveDown.png", this.loc.Chart.MoveSeriesDown]
    ]

    var toolBar = this.CreateHTMLTable();
    toolBar.style.margin = "4px";
    editChartForm.seriesToolBar = toolBar;
    var toolBarCell = mainTable.addCell(toolBar);
    editChartForm.toolBarCell = toolBarCell;
    toolBarCell.className = "stiDesignerChartFormToolBarCell";
    toolBarCell.setAttribute("colspan", "2");

    toolBar.changeVisibleState = function (state) {
        toolBar.style.display = state ? "" : "none";
        toolBarCell.className = state ? "stiDesignerChartFormToolBarCell" : "stiDesignerChartFormToolBarCellWithOutBorder";
    }

    for (var i in buttons) {
        if (buttons[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton("editChartForm" + buttons[i][0], null, buttons[i][1], buttons[i][2], buttons[i][1] || buttons[i][3], buttons[i][4], this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "1px";
        toolBar[buttons[i][0]] = button;
        toolBar.addCell(button);
    }

    //Add Series Menu
    var addSeriesMenu = this.InitializeAddSeriesMenu(editChartForm);
    addSeriesMenu.innerContent.style.maxHeight = null;
    toolBar.addSeries.action = function () { addSeriesMenu.changeVisibleState(!this.isSelected); }

    addSeriesMenu.action = function (menuItem) {
        addSeriesMenu.jsObject.SendCommandAddSeries({
            componentName: editChartForm.chartProperties.name,
            seriesType: menuItem.key
        });
    }

    //Remove Series
    toolBar.removeSeries.action = function () {
        editChartForm.jsObject.SendCommandRemoveSeries({
            componentName: editChartForm.chartProperties.name,
            seriesIndex: editChartForm.seriesContainer.getSelectedIndex()
        });
    }

    var seriesMove = function (direction) {
        editChartForm.jsObject.SendCommandSeriesMove({
            componentName: editChartForm.chartProperties.name,
            seriesIndex: editChartForm.seriesContainer.getSelectedIndex(),
            direction: direction
        });
    }

    //Series Move Up
    toolBar.moveUp.action = function () { seriesMove("Up"); }

    //Series Move Down
    toolBar.moveDown.action = function () { seriesMove("Down"); }

    //Series Properties Container
    editChartForm.seriesPropertiesContainer = this.SeriesPropertiesContainer(editChartForm);
    var cellForProperties = mainTable.addCell(editChartForm.seriesPropertiesContainer);
    cellForProperties.setAttribute("rowspan", "2");
    cellForProperties.style.width = "1px";

    //Series Container
    var seriesContainer = this.Container("editChartFormSeriesContainer", 200, this.options.isTouchDevice ? 420 : 425);
    editChartForm.seriesContainer = seriesContainer;
    seriesContainer.className = "stiDesignerSeriesContainer";
    var seriesCell = mainTable.addCellInNextRow(seriesContainer);
    seriesCell.style.width = "1px";

    //Series Labels Container
    var seriesLabelsProgress = this.Progress();
    seriesLabelsProgress.className = "stiDesignerChartFormProgress";
    seriesLabelsProgress.style.display = "none";

    editChartForm.seriesLabelsContainer = this.LabelsContainer(editChartForm, true);
    editChartForm.seriesLabelsContainer.seriesLabelsProgress = seriesLabelsProgress;
    seriesCell.appendChild(seriesLabelsProgress);
    seriesCell.appendChild(editChartForm.seriesLabelsContainer);
    editChartForm.seriesLabelsContainer.style.display = "none";

    seriesContainer.onChange = function () {
        editChartForm.seriesPropertiesContainer.update();
        toolBar.removeSeries.setEnabled(seriesContainer.items.length > 0);
        var selectedIndex = seriesContainer.getSelectedIndex();
        toolBar.moveUp.setEnabled(selectedIndex != -1 && selectedIndex > 0);
        toolBar.moveDown.setEnabled(selectedIndex != -1 && selectedIndex < seriesContainer.items.length - 1);
    }

    seriesContainer.update = function (notSelectedAfter) {
        seriesContainer.clear();
        for (var i in editChartForm.chartProperties.series) {
            seriesContainer.addItemAndNotAction(editChartForm.chartProperties.series[i].name, editChartForm.chartProperties.series[i]);
        }
        if (seriesContainer.items.length > 0 && !notSelectedAfter) seriesContainer.items[0].action();
    }

    //Middle Cell
    editChartForm.middleTable = this.CreateHTMLTable();
    editChartForm.middleTable.style.height = "100%";
    editChartForm.middleTable.style.width = "100%";
    editChartForm.middleCell = mainTable.addCellInLastRow(editChartForm.middleTable);

    //Conditions container
    editChartForm.seriesConditionsPanel = this.EditChartFormConditionsPanel(editChartForm, "SeriesConditions");
    editChartForm.middleCell.appendChild(editChartForm.seriesConditionsPanel);
    editChartForm.seriesConditionsPanel.style.display = "none";

    //Filters container
    editChartForm.seriesFiltersPanel = this.EditChartFormFiltersPanel(editChartForm);
    editChartForm.middleCell.appendChild(editChartForm.seriesFiltersPanel);
    editChartForm.seriesFiltersPanel.style.display = "none";

    //Series Caption
    editChartForm.seriesCaptionCell = editChartForm.middleTable.addCell();
    editChartForm.seriesCaptionCell.className = "stiDesignerChartFormSeriesCaptionCell";
    editChartForm.seriesCaptionCell.style.height = "1px";

    //Chart image container
    editChartForm.imageContainerSeriesTab = editChartForm.middleTable.addCellInNextRow();
    editChartForm.imageContainerSeriesTab.style.textAlign = "center";

    //Show Series Labels Buttons
    editChartForm.showSeriesLabelsButtons = this.ShowSeriesLabelsButtons(editChartForm);
    editChartForm.showSeriesLabelsButtonsCell = editChartForm.middleTable.addCellInNextRow(editChartForm.showSeriesLabelsButtons);
    editChartForm.showSeriesLabelsButtonsCell.className = "stiDesignerChartFormSeriesLabelsButtonsCell";
    editChartForm.showSeriesLabelsButtonsCell.style.height = "1px";

    return mainTable;
}


//Show Series Labels Buttons
StiMobileDesigner.prototype.ShowSeriesLabelsButtons = function (editChartForm) {
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerChartFormShowSeriesLabelsButtons";

    var caption = mainTable.addCell();
    caption.style.padding = "4px 4px 4px 8px";
    caption.innerHTML = this.loc.PropertyMain.ShowSeriesLabels + ":";

    var buttonNames = ["FromChart", "FromSeries"];
    for (var i in buttonNames) {
        var button = this.StandartSmallButton("showSeriesLabels" + buttonNames[i], "showSeriesLabelsButtons", this.loc.PropertyEnum["StiShowSeriesLabels" + buttonNames[i]]);
        button.showSeriesLabels = buttonNames[i];
        mainTable[buttonNames[i]] = button;
        mainTable.addCell(button);
        button.style.margin = "4px";

        button.action = function () {
            this.setSelected(true);
            var seriesIndex = editChartForm.seriesContainer.getSelectedIndex();

            if (seriesIndex != -1 && editChartForm.chartProperties.series[seriesIndex] != null) {
                editChartForm.chartProperties.series[seriesIndex].properties.Common.ShowSeriesLabels = this.showSeriesLabels;
                var showSeriesLabelsControl = editChartForm.jsObject.options.propertiesPanel.editChartPropertiesPanel.innerPanels["Series"].innerPanels["Common"].groups["Behavior"].properties["ShowSeriesLabels"].control;
                showSeriesLabelsControl.setKey(this.showSeriesLabels);
                showSeriesLabelsControl.action();
            }
        }
    }

    return mainTable;
}

//Categories Series Properties Container
StiMobileDesigner.prototype.SeriesPropertiesContainer = function (editChartForm) {
    var propertiesContainer = this.ContainerWithBigItems("editChartFormSeriesPropertiesContainer", 120, 460);
    propertiesContainer.className = "stiDesignerSeriesPropertiesContainer";
    propertiesContainer.buttons = {};

    var propNames = ["Common", "Conditions", "Filters", "Marker", "LineMarker", "Interaction", "TrendLine", "TopN", "SeriesLabels"];

    for (var i in propNames) {
        var caption = propNames[i] == "Common" ? this.loc.Chart.Common : this.loc.PropertyMain[propNames[i]];
        var button = propertiesContainer.addItemAndNotAction(propNames[i], caption, "Series_" + propNames[i] + ".png", {});
        button.style.minWidth = "105px";
        propertiesContainer.buttons[propNames[i]] = button;
        button.style.display = "none";
    }

    propertiesContainer.onAction = function () {
        var selectedName = this.selectedItem.name;
        var editChartPropertiesPanel = editChartForm.jsObject.options.propertiesPanel.editChartPropertiesPanel;
        editChartPropertiesPanel.innerPanels["Series"].showInnerPanel(selectedName);
        editChartForm.onChangeTabs();

        editChartForm.seriesContainer.style.display = (selectedName != "SeriesLabels" && selectedName != "Conditions" && selectedName != "Filters") ? "" : "none";
        editChartForm.middleTable.style.display = (selectedName != "Conditions" && selectedName != "Filters") ? "" : "none";
        editChartForm.seriesToolBar.changeVisibleState(selectedName != "SeriesLabels" && selectedName != "Conditions" && selectedName != "Filters");
        editChartForm.seriesLabelsContainer.style.display = (selectedName == "SeriesLabels") ? "" : "none";
        editChartForm.seriesCaptionCell.style.display = (selectedName == "SeriesLabels") ? "" : "none";
        editChartForm.showSeriesLabelsButtonsCell.style.display = (selectedName == "SeriesLabels") ? "" : "none";
        editChartForm.seriesConditionsPanel.style.display = selectedName == "Conditions" ? "" : "none";
        editChartForm.seriesFiltersPanel.style.display = selectedName == "Filters" ? "" : "none";

        if (selectedName == "SeriesLabels") {
            var seriesIndex = editChartForm.seriesContainer.getSelectedIndex();
            if (seriesIndex != -1) {
                editChartForm.seriesCaptionCell.innerHTML = editChartForm.chartProperties.series[seriesIndex].name;
                editChartForm.showSeriesLabelsButtons[editChartForm.chartProperties.series[seriesIndex].properties.Common.ShowSeriesLabels].setSelected(true);
            }

            if (editChartForm.seriesContainer.selectedItem != null && (
                    editChartForm.seriesContainer.selectedItem != editChartForm.lastSeries ||
                    editChartForm.chartProperties.style.type + editChartForm.chartProperties.style.name != editChartForm.lastStyleIdForSeriesLables)
               ) {
                editChartForm.seriesLabelsContainer.clear();
                editChartForm.seriesLabelsContainer.seriesLabelsProgress.style.display = "";
                editChartForm.jsObject.SendCommandGetLabelsContent({
                    componentName: editChartForm.chartProperties.name,
                    seriesIndex: seriesIndex
                });
            }
        }

        if (selectedName == "Conditions" || selectedName == "Filters") {
            var container = editChartForm["series" + selectedName + "Panel"].container;
            container.clear();
            var seriesIndex = editChartForm.seriesContainer.getSelectedIndex();
            if (seriesIndex != -1) {
                container.addItems(editChartForm.chartProperties.series[seriesIndex][selectedName == "Conditions" ? "conditions" : "filters"]);
                if (selectedName == "Filters") container.toolBar.filterType.setKey(editChartForm.chartProperties.series[seriesIndex].filterMode);
            }
        }
    }

    propertiesContainer.update = function () {
        var currentSeriesType = editChartForm.seriesContainer.selectedItem ? editChartForm.seriesContainer.selectedItem.itemObject.type : null;
        //debugger;
        var showTopN = false;
        var showInteraction = false;
        var showSeriesLabels = false;
        var showTrendLine = false;
        var showLineMarker = false;
        var showMarker = false;
        if (editChartForm.jsObject.options.isJava && currentSeriesType != null) {
            currentSeriesType = currentSeriesType.substring(currentSeriesType.lastIndexOf(".") + 1);
        }
        switch (currentSeriesType) {
            case "StiClusteredColumnSeries":
            case "StiClusteredBarSeries":
                {
                    showTopN = true;
                    showInteraction = true;
                    showSeriesLabels = true;
                    showTrendLine = true;
                    break;
                }
            case "StiStackedColumnSeries":
            case "StiFullStackedColumnSeries":
            case "StiStackedBarSeries":
            case "StiFullStackedBarSeries":
            case "StiPieSeries":
            case "StiFunnelSeries":
            case "StiFunnelWeightedSlicesSeries":
            case "StiDoughnutSeries":
            case "StiTreemapSeries":
                {
                    showTopN = true;
                    showInteraction = true;
                    showSeriesLabels = true;
                    break;
                }
            case "StiLineSeries":
            case "StiSteppedLineSeries":
            case "StiAreaSeries":
            case "StiSteppedAreaSeries":
                {
                    showTopN = true;
                    showLineMarker = true;
                    showMarker = true;
                    showInteraction = true;
                    showSeriesLabels = true;
                    showTrendLine = true;
                    break;
                }
            case "StiStackedLineSeries":
            case "StiFullStackedLineSeries":
            case "StiStackedAreaSeries":
            case "StiFullStackedAreaSeries":
                {
                    showTopN = true;
                    showLineMarker = true;
                    showMarker = true;
                    showInteraction = true;
                    showSeriesLabels = true;
                    break;
                }
            case "StiSplineSeries":
            case "StiSplineAreaSeries":
                {
                    showTopN = true;
                    showMarker = true;
                    showInteraction = true;
                    showSeriesLabels = true;
                    showTrendLine = true;
                    break;
                }
            case "StiStackedSplineSeries":
            case "StiFullStackedSplineSeries":
            case "StiStackedSplineAreaSeries":
            case "StiFullStackedSplineAreaSeries":
            case "StiRadarPointSeries":
            case "StiRadarLineSeries":
            case "StiRadarAreaSeries":
                {
                    showTopN = true;
                    showMarker = true;
                    showInteraction = true;
                    showSeriesLabels = true;
                    break;
                }
            case "StiRangeSeries":
            case "StiSteppedRangeSeries":
                {
                    showLineMarker = true;
                    showMarker = true;
                    showInteraction = true;
                    showSeriesLabels = true;
                    break;
                }
            case "StiSplineRangeSeries":
                {
                    showMarker = true;
                    showInteraction = true;
                    showSeriesLabels = true;
                    break;
                }
            case "StiRangeBarSeries":
            case "StiCandlestickSeries":
            case "StiStockSeries":
            case "StiGanttSeries":
                {
                    showInteraction = true;
                    showSeriesLabels = true;
                    break;
                }
            case "StiScatterSeries":
            case "StiScatterLineSeries":
                {
                    showLineMarker = true;
                    showMarker = true;
                    showInteraction = true;
                    showSeriesLabels = true;
                    showTrendLine = true;
                    break;
                }
            case "StiScatterSplineSeries":
                {
                    showMarker = true;
                    showInteraction = true;
                    showSeriesLabels = true;
                    showTrendLine = true;
                    break;
                }
            case "StiBubbleSeries":
                {
                    showInteraction = true;
                    showSeriesLabels = true;
                    showTrendLine = true;
                    break;
                }

        }

        propertiesContainer.buttons.Common.style.display = "";
        propertiesContainer.buttons.Conditions.style.display = currentSeriesType ? "" : "none";
        propertiesContainer.buttons.Filters.style.display = currentSeriesType ? "" : "none";
        propertiesContainer.buttons.Marker.style.display = showMarker ? "" : "none";
        propertiesContainer.buttons.LineMarker.style.display = showLineMarker ? "" : "none";
        propertiesContainer.buttons.Interaction.style.display = showInteraction ? "" : "none";
        propertiesContainer.buttons.TrendLine.style.display = showTrendLine ? "" : "none";
        propertiesContainer.buttons.TopN.style.display = showTopN ? "" : "none";
        propertiesContainer.buttons.SeriesLabels.style.display = showSeriesLabels ? "" : "none";

        propertiesContainer.buttons.Common.action();
    }

    return propertiesContainer;
}

//Area Tab
StiMobileDesigner.prototype.EditChartFormAreaTabPanel = function (editChartForm) {
    var mainTable = this.CreateHTMLTable();

    //Chart image container
    editChartForm.imageContainerAreaTab = mainTable.addCell();
    editChartForm.imageContainerAreaTab.style.textAlign = "center";

    //Area Properties Container    
    editChartForm.areaPropertiesContainer = this.AreaPropertiesContainer(editChartForm);
    mainTable.addCell(editChartForm.areaPropertiesContainer).style.width = "1px";

    return mainTable;
}

//Categories Area Properties Container
StiMobileDesigner.prototype.AreaPropertiesContainer = function (editChartForm) {
    var propertiesContainer = this.ContainerWithBigItems("editChartFormAreaPropertiesContainer", 120, 460);
    propertiesContainer.className = "stiDesignerSeriesPropertiesContainer";
    propertiesContainer.buttons = {};

    var propNames = ["Common", "XAxis", "YAxis", "XTopAxis", "YRightAxis", "GridLinesHor", "GridLinesHorRight", "GridLinesVert", "InterlacingHor", "InterlacingVert"];

    for (var i in propNames) {
        var captionText = propNames[i] == "Common" ? this.loc.Chart.Common : this.loc.PropertyMain[propNames[i]];
        var button = propertiesContainer.addItemAndNotAction(propNames[i], captionText, "Area_" + propNames[i] + ".png", {});
        button.style.minWidth = "105px";
        propertiesContainer.buttons[propNames[i]] = button;
        button.style.display = "none";
    }

    propertiesContainer.onAction = function () {
        var editChartPropertiesPanel = editChartForm.jsObject.options.propertiesPanel.editChartPropertiesPanel;
        editChartPropertiesPanel.innerPanels["Area"].showInnerPanel("AllProperties");
    }

    propertiesContainer.update = function () {
        var Container = {};
        Container.Common = true;
        Container.GridLinesHor_IStiGridLinesHor = false;
        Container.GridLinesHorRight = false;
        Container.GridLinesVert_IStiGridLinesVert = false;
        Container.InterlacingHor = false;
        Container.InterlacingVert = false;
        Container.XAxis_IStiXAxis = false;
        Container.XTopAxis = false;
        Container.YAxis_IStiYAxis = false;
        Container.YRightAxis = false;
        Container.GridLinesHor_IStiRadarGridLinesHor = false;
        Container.GridLinesVert_IStiRadarGridLinesVert = false;
        Container.XAxis_IStiXRadarAxis = false;
        Container.YAxis_IStiYRadarAxis = false;

        var areaType = editChartForm.seriesContainer.items.length > 0 ? editChartForm.chartProperties.area.type : "";

        if (areaType == "StiClusteredColumnArea" || areaType == "StiStackedColumnArea" || areaType == "StiFullStackedColumnArea" ||
            areaType == "StiRangeArea" || areaType == "StiSplineRangeArea" || areaType == "StiSteppedRangeArea" || areaType == "StiRangeBarArea" ||
            areaType == "StiClusteredBarArea" || areaType == "StiStackedBarArea" || areaType == "StiFullStackedBarArea" ||
            areaType == "StiScatterArea" || areaType == "StiCandlestickArea" || areaType == "StiStockArea" ||
            areaType == "StiGanttArea" || areaType == "StiBubbleArea") {
            Container.GridLinesHor_IStiGridLinesHor = true;
            Container.GridLinesHorRight = true;
            Container.GridLinesVert_IStiGridLinesVert = true;
            Container.InterlacingHor = true;
            Container.InterlacingVert = true;
            Container.XAxis_IStiXAxis = true;
            Container.XTopAxis = true;
            Container.YAxis_IStiYAxis = true;
            Container.YRightAxis = true;
        }
        else if (areaType == "StiRadarAreaArea" || areaType == "StiRadarLineArea" || areaType == "StiRadarPointArea") {
            Container.GridLinesHor_IStiRadarGridLinesHor = true;
            Container.GridLinesVert_IStiRadarGridLinesVert = true;
            Container.InterlacingHor = true;
            Container.InterlacingVert = true;
            Container.XAxis_IStiXRadarAxis = true;
            Container.YAxis_IStiYRadarAxis = true;
        }

        Container.XAxis = Container.XAxis_IStiXAxis || Container.XAxis_IStiXRadarAxis;
        Container.YAxis = Container.YAxis_IStiYAxis || Container.YAxis_IStiYRadarAxis;
        Container.GridLinesHor = Container.GridLinesHor_IStiGridLinesHor || Container.GridLinesHor_IStiRadarGridLinesHor;
        Container.GridLinesVert = Container.GridLinesVert_IStiGridLinesVert || Container.GridLinesVert_IStiRadarGridLinesVert;

        for (var i in propNames) {
            propertiesContainer.buttons[propNames[i]].style.display = Container[propNames[i]] ? "" : "none";
            if (propNames[i] == "XAxis") {
                propertiesContainer.buttons[propNames[i]].image.src = editChartForm.jsObject.options.images["Area_" +
                 (Container.XAxis_IStiXAxis ? "XAxis.png" : "XAxis_IStiXRadarAxis.png")];
            }
            if (propNames[i] == "YAxis") {
                propertiesContainer.buttons[propNames[i]].image.src = editChartForm.jsObject.options.images["Area_" +
                 (Container.YAxis_IStiYAxis ? "YAxis.png" : "YAxis_IStiYRadarAxis.png")];
            }
            if (propNames[i] == "GridLinesHor") {
                propertiesContainer.buttons[propNames[i]].image.src = editChartForm.jsObject.options.images["Area_" +
                 (Container.GridLinesHor_IStiGridLinesHor ? "GridLinesHor.png" : "GridLinesHor_IStiRadarGridLinesHor.png")];
            }
            if (propNames[i] == "GridLinesVert") {
                propertiesContainer.buttons[propNames[i]].image.src = editChartForm.jsObject.options.images["Area_" +
                 (Container.GridLinesVert_IStiGridLinesVert ? "GridLinesVert.png" : "GridLinesVert_IStiRadarGridLinesVert.png")];
            }
        }
        propertiesContainer.buttons.Common.action();

        if (areaType == "") {
            propertiesContainer.buttons.Common.style.display = "none";
            var editChartPropertiesPanel = editChartForm.jsObject.options.propertiesPanel.editChartPropertiesPanel;
            editChartPropertiesPanel.innerPanels["Area"].innerPanels["AllProperties"].style.display = "none";
        }
    }

    return propertiesContainer;
}

//Labels Tab
StiMobileDesigner.prototype.EditChartFormLabelsTabPanel = function (editChartForm) {
    var mainTable = this.CreateHTMLTable();

    //Labels Container
    var labelsProgress = this.Progress();
    labelsProgress.className = "stiDesignerChartFormProgress";
    labelsProgress.style.display = "none";

    editChartForm.labelsContainer = this.LabelsContainer(editChartForm);

    var containerCell = mainTable.addCell();
    containerCell.style.width = "1px";
    containerCell.appendChild(labelsProgress);
    containerCell.appendChild(editChartForm.labelsContainer);
    editChartForm.labelsContainer.labelsProgress = labelsProgress;

    //Chart image container
    editChartForm.imageContainerLabelsTab = mainTable.addCell();
    editChartForm.imageContainerLabelsTab.style.textAlign = "center";

    //Conditions container
    editChartForm.labelsConditionsPanel = this.EditChartFormConditionsPanel(editChartForm, "LabelsConditions");
    editChartForm.labelsConditionsCell = mainTable.addCell(editChartForm.labelsConditionsPanel);
    editChartForm.labelsConditionsCell.style.display = "none";

    //Labels Properties Container
    var labelPropertiesContainer = this.ContainerWithBigItems("editChartFormLabelsPropertiesContainer", 100, 460);
    editChartForm.labelPropertiesContainer = labelPropertiesContainer;
    labelPropertiesContainer.className = "stiDesignerSeriesPropertiesContainer";
    labelPropertiesContainer.buttons = {};
    mainTable.addCell(labelPropertiesContainer).style.width = "1px";

    labelPropertiesContainer.buttons.Common = labelPropertiesContainer.addItemAndNotAction("Common", this.loc.Chart.Common, "Labels_Common.png", {});
    labelPropertiesContainer.buttons.Conditions = labelPropertiesContainer.addItemAndNotAction("Conditions", this.loc.PropertyMain.Conditions, "Series_Conditions.png", {});

    labelPropertiesContainer.onAction = function () {
        var selectedName = this.selectedItem.name;
        var editChartPropertiesPanel = editChartForm.jsObject.options.propertiesPanel.editChartPropertiesPanel;
        editChartPropertiesPanel.innerPanels["Labels"].showInnerPanel(selectedName);
        editChartForm.onChangeTabs();

        editChartForm.labelsContainer.style.display = (selectedName == "Common") ? "" : "none";
        editChartForm.imageContainerLabelsTab.style.display = (selectedName == "Common") ? "" : "none";
        editChartForm.labelsConditionsCell.style.display = (selectedName == "Conditions") ? "" : "none";

        if (selectedName == "Conditions") {
            var container = editChartForm.labelsConditionsPanel.container;
            container.clear();
            container.addItems(editChartForm.chartProperties.conditions);
        }
    }

    return mainTable;
}

//Labels Container
StiMobileDesigner.prototype.LabelsContainer = function (editChartForm, isSeriesLabels) {
    var labelsContainer = this.ContainerWithBigItems(isSeriesLabels ? "editChartFormSeriesLabelsContainer" : "editChartFormLabelsContainer", 200, 460, 80);
    labelsContainer.className = "stiDesignerSeriesContainer";
    labelsContainer.buttons = {};
    labelsContainer.isSeriesLabels = isSeriesLabels;

    labelsContainer.onAction = function () {
        if (labelsContainer.selectedItem != null) {
            var seriesIndex = editChartForm.seriesContainer.getSelectedIndex();
            var params = {
                componentName: editChartForm.chartProperties.name,
                labelsType: labelsContainer.selectedItem.name
            };
            if (labelsContainer.isSeriesLabels) {
                params.seriesIndex = seriesIndex;
            }
            editChartForm.jsObject.SendCommandSetLabelsType(params);
        }
    }

    labelsContainer.update = function (labelsContent) {
        editChartForm[!labelsContainer.isSeriesLabels ? "lastStyleId" : "lastStyleIdForSeriesLables"] = editChartForm.chartProperties.style.type + editChartForm.chartProperties.style.name;
        editChartForm.labelsContainer.labelsProgress.style.display = "none";
        editChartForm.seriesLabelsContainer.seriesLabelsProgress.style.display = "none";

        if (!labelsContainer.isSeriesLabels)
            editChartForm.lastAreaTypeForLabels = editChartForm.chartProperties.area.type;
        else
            editChartForm.lastSeries = editChartForm.seriesContainer.selectedItem;

        labelsContainer.clear();
        var labelsType = editChartForm.chartProperties.labels.type;
        if (labelsContainer.isSeriesLabels) {
            var seriesIndex = editChartForm.seriesContainer.getSelectedIndex();
            if (seriesIndex != -1) labelsType = editChartForm.chartProperties.series[seriesIndex].labels.type;
        }
        for (var i in labelsContent) {
            var name = labelsContent[i].type;
            var button = labelsContainer.addItemAndNotAction(name, labelsContent[i].caption, " ", {});
            button.caption.style.height = "24px";
            button.caption.style.verticalAlign = "top";
            button.cellImage.removeChild(button.image);
            button.image = document.createElement("div");
            button.image.innerHTML = labelsContent[i].image;
            button.cellImage.appendChild(button.image);
            labelsContainer.buttons[name] = button;
            if (labelsType == name) {
                button.selected();
            }
            button.style.display = "inline-block";
        }
        if (!labelsContainer.isSeriesLabels) editChartForm.labelPropertiesContainer.buttons.Common.action();
    }

    return labelsContainer;
}

//Styles Tab
StiMobileDesigner.prototype.EditChartFormStylesTabPanel = function (editChartForm) {
    var mainTable = this.CreateHTMLTable();

    //Toolbar
    editChartForm.stylesToolBar = this.CreateHTMLTable();
    var toolBarCell = mainTable.addCell(editChartForm.stylesToolBar);
    toolBarCell.className = "stiDesignerChartFormToolBarCell";
    toolBarCell.setAttribute("colspan", "2");

    //AddStyle Button
    editChartForm.stylesToolBar.addStyle = this.StandartSmallButton("editChartFormAddStyle", null, this.loc.Toolbars.StyleDesigner, "Styles.StiChartStyle.png");
    editChartForm.stylesToolBar.addStyle.style.margin = "5px";
    editChartForm.stylesToolBar.addCell(editChartForm.stylesToolBar.addStyle);
    editChartForm.stylesToolBar.addStyle.action = function () {
        editChartForm.jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
            styleDesignerForm.changeVisibleState(true);
        });
    };

    //Styles Container
    var stylesProgress = this.Progress();
    editChartForm.stylesContainer = this.StylesContainer(editChartForm);
    stylesProgress.className = "stiDesignerChartFormProgress";
    stylesProgress.style.display = "none";

    var containerCell = mainTable.addCellInNextRow();
    containerCell.style.width = "1px";
    containerCell.appendChild(stylesProgress);
    containerCell.appendChild(editChartForm.stylesContainer);
    editChartForm.stylesContainer.stylesProgress = stylesProgress;

    //Chart image container
    editChartForm.imageContainerStylesTab = mainTable.addCellInLastRow();
    editChartForm.imageContainerStylesTab.style.textAlign = "center";

    return mainTable;
}

//Styles Container
StiMobileDesigner.prototype.StylesContainer = function (editChartForm) {
    var stylesContainer = this.ContainerWithBigItems("editChartFormStylesContainer", 200, this.options.isTouchDevice ? 420 : 425, 80);
    stylesContainer.className = "stiDesignerSeriesContainer";
    stylesContainer.buttons = {};

    stylesContainer.onAction = function () {
        if (stylesContainer.selectedItem != null) {
            var params = {
                componentName: editChartForm.chartProperties.name,
                styleType: stylesContainer.selectedItem.itemObject.type,
                styleName: stylesContainer.selectedItem.itemObject.name
            }
            editChartForm.jsObject.SendCommandSetChartStyle(params);
        }
    }

    stylesContainer.update = function (stylesContent) {
        editChartForm.lastAreaTypeForStyles = editChartForm.chartProperties.area.type;
        stylesContainer.clear();
        for (var i in stylesContent) {
            var name = stylesContent[i].type + stylesContent[i].name;
            var caption = stylesContent[i].name || stylesContent[i].type.replace("Sti", "");
            var button = stylesContainer.addItemAndNotAction(name, caption, " ", stylesContent[i]);
            button.cellImage.removeChild(button.image);
            button.cellImage.style.padding = "5px 5px 0 5px";
            button.image = document.createElement("div");
            button.image.innerHTML = stylesContent[i].image;
            button.cellImage.appendChild(button.image);
            stylesContainer.buttons[name] = button;

            if (editChartForm.chartProperties.style.type + editChartForm.chartProperties.style.name == name) {
                button.selected();
            }
            button.style.display = "inline-block";
        }
        editChartForm.stylesContainer.stylesProgress.style.display = "none";
    }

    return stylesContainer;
}

StiMobileDesigner.prototype.EditChartFormConditionsPanel = function (editChartForm, containerType) {
    var panel = document.createElement("div");

    //Toolbar
    var buttons = [
        ["add", this.loc.Chart.AddCondition.replace("&", ""), "AddCondition.png", null],
        ["remove", null, "Remove.png"],
        ["separator"],
        ["moveUp", null, "MoveUp.png", this.loc.QueryBuilder.MoveUp],
        ["moveDown", null, "MoveDown.png", this.loc.QueryBuilder.MoveDown]
    ]

    var toolBar = this.CreateHTMLTable();
    toolBar.style.margin = "4px";
    panel.appendChild(toolBar);

    for (var i in buttons) {
        if (buttons[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton(null, null, buttons[i][1], buttons[i][2],
            buttons[i][1] || buttons[i][3], buttons[i][4], this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "1px";
        toolBar[buttons[i][0]] = button;
        toolBar.addCell(button);
    }

    toolBar.remove.setEnabled(false);
    toolBar.moveUp.setEnabled(false);
    toolBar.moveDown.setEnabled(false);

    var conditionContainer = this.EditChartFormContainer(editChartForm, containerType, toolBar);
    panel.container = conditionContainer;
    panel.appendChild(conditionContainer);
    conditionContainer.style.width = "628px";
    conditionContainer.style.height = (this.options.isTouchDevice ? 420 : 425) + "px";

    toolBar.add.action = function () { conditionContainer.addItem(); }
    toolBar.remove.action = function () { if (conditionContainer.selectedItem) conditionContainer.selectedItem.remove(); }
    toolBar.moveUp.action = function () { if (conditionContainer.selectedItem) conditionContainer.selectedItem.move("Up"); }
    toolBar.moveDown.action = function () { if (conditionContainer.selectedItem) conditionContainer.selectedItem.move("Down"); }

    return panel;
}

StiMobileDesigner.prototype.EditChartFormFiltersPanel = function (editChartForm) {
    var panel = document.createElement("div");

    //Toolbar
    var buttons = [
        ["add", this.loc.FormBand.AddFilter.replace("&", ""), "AddFilter.png", null],
        ["remove", null, "Remove.png"],
        ["separator"]
    ]

    var toolBar = this.CreateHTMLTable();
    toolBar.style.margin = "4px";
    panel.appendChild(toolBar);

    for (var i in buttons) {
        if (buttons[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton(null, null, buttons[i][1], buttons[i][2],
            buttons[i][1] || buttons[i][3], buttons[i][4], this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "1px";
        toolBar[buttons[i][0]] = button;
        toolBar.addCell(button);
    }

    var filterTypeText = toolBar.addCell();
    filterTypeText.className = "stiDesignerCaptionControls";
    filterTypeText.innerHTML = this.loc.PropertyMain.Type + ": ";
    toolBar.filterType = this.ImageList("EditChartFormFilterType", true, false, null, this.GetFilterTypeItems());
    toolBar.addCell(toolBar.filterType);

    toolBar.remove.setEnabled(false);

    //Container
    var filterContainer = this.EditChartFormContainer(editChartForm, "SeriesFilters", toolBar);
    panel.container = filterContainer;
    panel.appendChild(filterContainer);
    filterContainer.style.width = "628px";
    filterContainer.style.height = (this.options.isTouchDevice ? 420 : 425) + "px";

    toolBar.add.action = function () { filterContainer.addItem(); }
    toolBar.remove.action = function () { if (filterContainer.selectedItem) filterContainer.selectedItem.remove(); }
    toolBar.filterType.action = function () { filterContainer.isModified = true; }

    return panel;
}

//Container
StiMobileDesigner.prototype.EditChartFormContainer = function (editChartForm, containerType, toolBar) {
    var container = document.createElement("div");
    container.jsObject = this;
    container.className = "stiDesignerChartFormContainer";
    container.selectedItem = null;
    container.isModified = false;
    container.containerType = containerType;
    container.toolBar = toolBar;
    container.editChartForm = editChartForm;

    container.addItems = function (conditionsObject) {
        this.clear();
        for (var i in conditionsObject) {
            this.addItem(conditionsObject[i]);
        }
        var items = container.getItems();
        if (items.length > 0) items[0].setSelected();
    }

    container.addItem = function (itemObject) {
        var item = containerType == "SeriesConditions" || containerType == "LabelsConditions"
            ? this.jsObject.EditChartFormConditionsContainerItem(container)
            : this.jsObject.EditChartFormFiltersContainerItem(container);
        this.appendChild(item);
        item.setSelected();
        this.onChange();
        var defaultItemObject = null;
        if (itemObject == null) {
            container.isModified = true;
            defaultItemObject = {
                FieldIs: "Argument",
                DataType: "String",
                Condition: "EqualTo",
                Value: ""
            }
            if (containerType == "SeriesConditions" || containerType == "LabelsConditions")
                defaultItemObject.Color = "255,255,255";

        }
        item.setKey(itemObject || defaultItemObject);

        item.remove = function () {
            var prevItem = item.previousSibling;
            var nextItem = item.nextSibling;
            container.removeChild(item);
            if (container.selectedItem == this) container.selectedItem = null;
            var items = container.getItems();
            if (items.length > 0) {
                if (nextItem) {
                    nextItem.setSelected();
                }
                else if (prevItem) {
                    prevItem.setSelected();
                }
                else items[0].setSelected();
            }
            container.onChange(items);
            container.isModified = true;
        }
    }

    container.getItems = function (itemObject) {
        var items = [];
        for (var num in container.childNodes) {
            if (!container.childNodes[num].isContainerItem) continue;
            items.push(container.childNodes[num]);
        }
        return items;
    }

    container.getValue = function () {
        var result = [];
        for (var num in container.childNodes) {
            if (!container.childNodes[num].isContainerItem) continue;
            result.push(container.childNodes[num].getValue());
        }
        return result;
    }

    container.sendValueToServer = function (andCloseForm) {
        params = {};
        params.value = container.getValue();
        if (this.containerType == "SeriesFilters") params.filterMode = this.toolBar.filterType.key;
        params.componentName = editChartForm.chartProperties.name;
        params.containerType = this.containerType;
        if (this.containerType == "SeriesConditions" || this.containerType == "SeriesFilters") params.seriesIndex = editChartForm.seriesContainer.getSelectedIndex();
        if (andCloseForm) params.andCloseForm = andCloseForm;
        this.jsObject.SendCommandSendContainerValue(params);
    }

    container.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        container.selectedItem = null;
        container.onChange();
    }

    container.onChange = function (items) {
        var items = items || container.getItems();
        toolBar.remove.setEnabled(container.selectedItem != null);
        if (toolBar.moveUp) toolBar.moveUp.setEnabled(container.selectedItem != null && container.selectedItem.getIndex() > 0);
        if (toolBar.moveDown) toolBar.moveDown.setEnabled(container.selectedItem != null && container.selectedItem.getIndex() < items.length - 1);
    }

    return container;
}

//Container Item
StiMobileDesigner.prototype.EditChartFormContainerItem = function (container) {
    var item = document.createElement("div");
    item.jsObject = this;
    item.isSelected = false;
    item.isContainerItem = true;
    item.key = this.newGuid().replace(/-/g, '');
    item.className = "stiDesignerChartFilterPanel";

    item.innerTable = this.CreateHTMLTable();
    item.appendChild(item.innerTable);

    //Captions
    item.fieldIsCaption = item.innerTable.addCell();
    item.fieldIsCaption.innerHTML = this.loc.PropertyMain.FieldIs;

    item.dataTypeCaption = item.innerTable.addCell();
    item.dataTypeCaption.innerHTML = this.loc.PropertyMain.DataType;

    item.conditionCaption = item.innerTable.addCell();
    item.conditionCaption.innerHTML = this.loc.PropertyMain.Condition;

    item.valueCaption = item.innerTable.addCell();
    item.valueCaption.innerHTML = this.loc.PropertyMain.Value;

    //FieldIs
    var seriesIndex = container.editChartForm.seriesContainer.getSelectedIndex();
    var seriesType = seriesIndex != -1 ? container.editChartForm.chartProperties.series[seriesIndex].type : null;
    var items = [];
    if (container.containerType != "SeriesFilters") {
        items = this.GetConditionsFieldIsItems();
    }
    else {
        if (seriesType == "StiGanttSeries") {
            items = this.GetSeriesGanttFiltersFieldIsItems();
        }
        else if (seriesType == "StiCandlestickSeries" || seriesType == "StiStockSeries") {
            items = this.GetSeriesFinancialFiltersFieldIsItems();
        }
        else {
            items = this.GetFiltersFieldIsItems();
        }
    }
    item.fieldIs = this.DinamicDropDownList(item.key + "FieldIs", 130, null, items, true, false);
    item.fieldIs.style.margin = "3px 7px 3px 0";
    item.innerTable.addCellInNextRow(item.fieldIs);
    if (container.containerType == "SeriesFilters") item.fieldIs.action = function () { item.updateControls(); };

    //Data Type
    item.dataType = this.DinamicDropDownList(item.key + "DataType", 130, null, this.GetConditionsDataTypeItems(), true, false);
    item.dataType.style.margin = "3px 7px 3px 0";
    item.innerTable.addCellInLastRow(item.dataType);
    item.dataType.action = function () { item.updateControls(); };

    //Condition
    item.condition = this.DinamicDropDownList(item.key + "DataType", 130, null, null, true, false);
    item.condition.style.margin = "3px 7px 3px 0";
    item.innerTable.addCellInLastRow(item.condition);

    //Value
    item.value = this.ExpressionControl(null, 170, null, null, true);
    item.value.style.margin = "3px 7px 3px 0";
    var valueCell = item.innerTable.addCellInLastRow(item.value);

    //Value Date
    item.valueDate = this.DateControl(null, 170);
    item.valueDate.style.margin = "3px 7px 3px 0";
    valueCell.appendChild(item.valueDate);

    item.innerTable2 = this.CreateHTMLTable();
    item.innerTable2.style.marginTop = "4px";
    item.appendChild(item.innerTable2);

    item.setSelected = function () {
        for (var num in container.childNodes) {
            if (!container.childNodes[num].isContainerItem) continue;
            container.childNodes[num].className = "stiDesignerChartFilterPanel";
            container.childNodes[num].isSelected = false;
        }
        item.isSelected = true;
        item.className = "stiDesignerChartFilterPanelSelected";
        container.selectedItem = this;
    }

    item.onclick = function () {
        if (this.isTouchProcessFlag) return;
        this.action();
    }

    item.ontouchend = function () {
        var this_ = this;
        this.isTouchProcessFlag = true;

        if (this.jsObject.options.fingerIsMoved) return;
        this.action();

        setTimeout(function () {
            this_.isTouchProcessFlag = false;
        }, 1000);
    }

    item.action = function () {
        this.setSelected();
        container.onChange();
        container.isModified = true;
    }

    item.getIndex = function () {
        var index = -1;
        for (var num in container.childNodes) {
            if (!container.childNodes[num].isContainerItem) continue;
            index++;
            if (container.childNodes[num] == this) return index;
        }
        return -1;
    }

    item.move = function (direction) {
        var items = container.getItems();
        var currentIndex = this.getIndex();
        var currentItem = items[currentIndex];
        container.removeChild(currentItem);

        if (direction == "Up") {
            currentIndex--;
            if (currentIndex != -1) container.insertBefore(currentItem, items[currentIndex]);
        }
        else {
            currentIndex++;
            if (currentIndex > items.length - 2)
                container.appendChild(currentItem);
            else
                container.insertBefore(currentItem, items[currentIndex + 1]);
        }

        container.onChange();
        container.isModified = true;
    }

    item.setKey = function (key) {
        if (key.FieldIs != null) item.fieldIs.setKey(key.FieldIs);
        if (key.DataType != null) item.dataType.setKey(key.DataType);
        if (key.Color != null) item.color.setKey(key.Color);
        if (key.Value != null) {
            var value = Base64.decode(key.Value);

            if (key.DataType == "DateTime")
                item.valueDate.setKey(value);
            else
                item.value.textBox.value = value;
        }
        item.updateControls();
        if (key.Condition != null) item.condition.setKey(key.Condition);
    }

    item.getValue = function () {
        var value = Base64.encode(item.dataType.key == "DateType" ? this.jsObject.DateToStringAmericanFormat(item.valueDate.key) : item.value.textBox.value);

        var result = {
            FieldIs: item.fieldIs.key,
            DataType: item.dataType.key,
            Condition: item.condition.key,
            Value: value
        }
        if (container.containerType != "SeriesFilters") result.Color = item.color.key;

        return result;
    }

    return item;
}

//Filters Container Item
StiMobileDesigner.prototype.EditChartFormFiltersContainerItem = function (container) {
    var item = this.EditChartFormContainerItem(container);

    item.updateControls = function () {
        this.condition.addItems(this.jsObject.GetFilterConditionItems(item.dataType.key, true));
        if (!this.condition.haveKey(this.condition.key) && this.condition.items != null && this.condition.items.length > 0) this.condition.setKey(this.condition.items[0].key);
        this.value.style.display = (this.dataType.key != "DateTime") ? "" : "none";
        this.valueDate.style.display = (this.dataType.key == "DateTime") ? "" : "none";
        this.dataType.setEnabled(this.fieldIs.key != "Expression");
        this.condition.setEnabled(this.fieldIs.key != "Expression");
    }

    return item;
}

//Conditions Container Item
StiMobileDesigner.prototype.EditChartFormConditionsContainerItem = function (container) {
    var item = this.EditChartFormContainerItem(container);

    //Color Caption
    item.colorCaption = item.innerTable2.addCellInNextRow();
    item.colorCaption.innerHTML = this.loc.PropertyMain.Color;

    //Color
    item.color = this.ColorControl();
    item.color.style.margin = "3px 7px 3px 0";
    var valueCell = item.innerTable2.addCellInNextRow(item.color);

    item.updateControls = function () {
        this.condition.addItems(this.jsObject.GetFilterConditionItems(item.dataType.key, true));
        if (!this.condition.haveKey(this.condition.key) && this.condition.items != null && this.condition.items.length > 0) this.condition.setKey(this.condition.items[0].key);
        this.value.style.display = (this.dataType.key != "DateTime") ? "" : "none";
        this.valueDate.style.display = (this.dataType.key == "DateTime") ? "" : "none";
    }

    return item;
}

StiMobileDesigner.prototype.SeriesWizardBigButton = function (name, caption, imageName) {
    var button = this.BigButton(name, null, caption, imageName, null, null, this.GetStyles("StandartBigButton"), true);
    button.style.display = "inline-block";
    button.cellImage.style.padding = "4px";
    button.caption.style.padding = "0 4px 4px 4px";
    button.image.style.border = "1px solid #c6c6c6";

    return button;
}

StiMobileDesigner.prototype.SeriesWizardContainer = function (editChartForm) {
    var seriesWizardContainer = document.createElement("div");
    editChartForm.seriesWizardContainer = seriesWizardContainer;
    seriesWizardContainer.className = "stiDesignerChartFormSeriesWizardContainer";
    seriesWizardContainer.style.width = "750px";
    seriesWizardContainer.style.height = "460px";

    var series = [
        "StiClusteredColumnSeries",
        "StiStackedColumnSeries",
        "StiFullStackedColumnSeries",
        "StiClusteredBarSeries",
        "StiStackedBarSeries",
        "StiFullStackedBarSeries",
        "StiPieSeries",
        "StiDoughnutSeries",
        "StiLineSeries",
        "StiSteppedLineSeries",
        "StiStackedLineSeries",
        "StiFullStackedLineSeries",
        "StiSplineSeries",
        "StiStackedSplineSeries",
        "StiFullStackedSplineSeries",
        "StiAreaSeries",
        "StiSteppedAreaSeries",
        "StiStackedAreaSeries",
        "StiFullStackedAreaSeries",
        "StiSplineAreaSeries",
        "StiStackedSplineAreaSeries",
        "StiFullStackedSplineAreaSeries",
        "StiGanttSeries",
        "StiScatterSeries",
        "StiBubbleSeries",
        "StiRadarPointSeries",
        "StiRadarLineSeries",
        "StiRadarAreaSeries",
        "StiRangeSeries",
        "StiSteppedRangeSeries",
        "StiRangeBarSeries",
        "StiSplineRangeSeries",
        "StiFunnelSeries",
        "StiCandlestickSeries",
        "StiStockSeries"
    ]

    for (var i in series) {
        var seriesType = series[i].replace("Sti", "").replace("Series", "");
        var caption = this.loc.Chart[seriesType];
        var button = this.SeriesWizardBigButton(null, caption, "Big" + series[i] + ".png");
        button.seriesType = series[i];
        button.style.margin = "10px 0px 10px 0px";
        button.style.maxWidth = "180px";
        button.caption.style.height = "25px";
        button.cellImage.style.padding = "4px 4px 0 4px";
        seriesWizardContainer.appendChild(button);

        button.action = function () {
            editChartForm.seriesWizardContainer.style.display = "none";
            editChartForm.chartTabMainTable.style.display = "";

            this.jsObject.SendCommandAddSeries({
                componentName: editChartForm.chartProperties.name,
                seriesType: this.seriesType
            });
        }
    }

    return seriesWizardContainer;
}