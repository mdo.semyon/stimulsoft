﻿
StiMobileDesigner.prototype.InitializeColorsCollectionForm_ = function () {

    var form = this.BaseForm("colorsCollectionForm", this.loc.PropertyCategory.ColorsCategory, 3);
    form.controls = {};

    //Main Table
    var mainTable = this.CreateHTMLTable();
    form.container.appendChild(mainTable);
    form.container.style.padding = "0px";

    //Toolbar
    var buttons = [
        ["addColor", this.loc.Buttons.Add, "AddCondition.png"],
        ["removeColor", this.loc.Buttons.Remove, "Remove.png"],
        ["separator"],
        ["moveUp", null, "MoveUp.png"],
        ["moveDown", null, "MoveDown.png"]
    ]

    var toolBar = this.CreateHTMLTable();
    form.container.appendChild(toolBar);
    form.container.appendChild(this.FormSeparator());

    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton(null, null, buttons[i][1], buttons[i][2], null, buttons[i][0] == "addCondition" ? "Down" : null, this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "4px";
        form.controls[buttons[i][0]] = button;
        toolBar.addCell(button);
    }

    //Items Container
    var container = this.EasyContainer(310, 300);
    form.controls.colorsContainer = container;
    form.container.appendChild(container);
    
    //Override addItem
    container.addItem = function (color, notAction) {
        var item = this.jsObject.ColorControl(null, null, null, 270, false);
        item.style.margin = "10px 0 0 15px";
        item.setKey(color);
        container.appendChild(item);
        if (!notAction) container.onAction();

        item.button.action = function(){
            item.action();
        };
        
        var arrowButton = this.jsObject.StandartSmallButton(null, null, null, "ButtonArrowDown.png");
        item.button.innerTable.addCell(arrowButton);
        item.button.arrowCell.style.display = "none";
        arrowButton.style.width = "15px";
        arrowButton.innerTable.style.width = "100%";

        arrowButton.action = function () {
            var colorDialog = item.jsObject.options.menus.colorDialog || item.jsObject.InitializeColorDialog();
            colorDialog.changeVisibleState(!colorDialog.visible, item.button);
        }

        item.action = function () {
            if (container.selectedItem) {
                if (this != container.selectedItem) container.onPreChangeSelection();
                container.selectedItem.button.setSelected(false);
            }
            this.button.setSelected(true);
            container.selectedItem = this;
            container.onAction();
            container.onSelected(this);
        }

        item.select = function () {
            if (container.selectedItem) container.selectedItem.button.setSelected(false);
            this.button.setSelected(true);
            container.selectedItem = this;
        }

        item.remove = function () {
            var nextItem = this.nextSibling;
            var prevItem = this.previousSibling;
            container.removeChild(this);
            if (this == container.selectedItem) {
                container.selectedItem = null;
                var count = container.getCountItems();
                if (count > 0) {
                    if (nextItem)
                        container.selectedItem = nextItem;
                    else if (prevItem)
                        container.selectedItem = prevItem;
                    else
                        container.selectedItem = container.childNodes[0];

                    container.selectedItem.button.setSelected(true);
                }
            }
            container.onAction();
        };

        item.getIndex = function () {
            for (var i = 0; i < container.childNodes.length; i++)
                if (container.childNodes[i] == this) return i;
        };

        item.move = function (direction) {
            var index = this.getIndex();
            container.removeChild(this);
            var count = container.getCountItems();
            var newIndex = direction == "Up" ? index - 1 : index + 1;
            if (direction == "Up" && newIndex == -1) newIndex = 0;
            if (direction == "Down" && newIndex >= count) {
                container.appendChild(this);
                container.onAction();
                return;
            }
            container.insertBefore(this, container.childNodes[newIndex]);
            container.onAction();
        }

        return item;
    }

    container.onAction = function () {
        var count = container.getCountItems();
        var index = container.selectedItem ? container.selectedItem.getIndex() : -1;
        form.controls.moveUp.setEnabled(index > 0);
        form.controls.moveDown.setEnabled(index != -1 && index < count - 1);
        form.controls.removeColor.setEnabled(count > 0 && container.selectedItem);
    }

    form.controls.addColor.action = function () {
        var item = container.addItem("255,255,255");
        item.select();
        container.onAction();
    }

    form.controls.removeColor.setEnabled(false);

    form.controls.removeColor.action = function () {
        if (container.selectedItem) { container.selectedItem.remove(); }
    }

    form.controls.moveUp.action = function () {
        if (container.selectedItem) { container.selectedItem.move("Up"); }
    }

    form.controls.moveDown.action = function () {
        if (container.selectedItem) { container.selectedItem.move("Down"); }
    }    

    form.show = function (colors) {
        container.clear();
        for (var i = 0; i < colors.length; i++) {
            container.addItem(colors[i], true);
        }
        container.onAction();
        form.changeVisibleState(true);
    }

    form.action = function () {
        form.changeVisibleState(false);
    }

    return form;
}