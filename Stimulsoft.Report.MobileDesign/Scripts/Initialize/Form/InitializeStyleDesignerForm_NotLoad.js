﻿
StiMobileDesigner.prototype.InitializeStyleDesignerForm_ = function () {

    var styleDesignerForm = this.BaseFormPanel("styleDesignerForm", this.loc.Toolbars.StyleDesigner, 2, this.HelpLinks["styleDesigner"]);
    styleDesignerForm.buttonsSeparator.style.display = "none";
    styleDesignerForm.buttonsPanel.style.display = "none";

    //MainTable
    var mainTable = this.CreateHTMLTable();
    styleDesignerForm.container.appendChild(mainTable);
    mainTable.style.borderCollapse = "separate";

    //Toolbar
    var buttons = [
        ["addStyle", this.loc.FormStyleDesigner.Add, "StyleAdd.png", null, "Down"],
        ["removeStyle", null, "StyleRemove.png", this.loc.FormStyleDesigner.Remove, false],
        ["separator"],
        ["moveUp", null, "MoveUp.png", false],
        ["moveDown", null, "MoveDown.png", false],
        ["separator"],
        ["applyStyles", this.loc.FormStyleDesigner.ApplyStyles,
            "StylesApply.png", this.loc.FormStyleDesigner.ApplyStyleCollectionToReportComponents, "Down"],
        ["createStyleCollection", null, "StylesCreate.png", this.loc.FormStyleDesigner.CreateStyleCollection, false],
        ["separator"],
        ["duplicateStyle", null, "Styles.StyleDuplicate.png", this.loc.FormStyleDesigner.Duplicate, false],
        ["getStyle", null, "Styles.StylesGet.png", this.loc.FormStyleDesigner.GetStyle, false],
        ["separator"],
        ["copyStyle", null, "Copy.png", this.loc.MainMenu.menuEditCopy.replace("&", ""), false],
        ["cutStyle", null, "Cut.png", this.loc.MainMenu.menuEditCut.replace("&", ""), false],
        ["pasteStyle", null, "PasteSmall.png", this.loc.MainMenu.menuEditPaste.replace("&", ""), false],
        ["separator"],
        ["openStyle", null, "Open.png", this.loc.FormStyleDesigner.Open, false],
        ["saveStyle", null, "Save.png", this.loc.FormStyleDesigner.Save, false]
    ]

    styleDesignerForm.toolBar = this.CreateHTMLTable();
    var toolBarCell = mainTable.addCell(styleDesignerForm.toolBar);
    toolBarCell.className = "stiDesignerStyleDesignerFormToolbarCell";
    toolBarCell.setAttribute("colspan", "2");

    for (var i in buttons) {
        if (buttons[i][0] == "separator") {
            styleDesignerForm.toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton(buttons[i][0], null, buttons[i][1], buttons[i][2], buttons[i][3], buttons[i][4], this.GetStyles("StandartSmallButton"));
        button.style.margin = "4px";
        styleDesignerForm.toolBar[buttons[i][0]] = button;
        styleDesignerForm.toolBar.addCell(button);
    }

    //Move Up
    styleDesignerForm.toolBar.moveUp.action = function () {
        if (styleDesignerForm.itemsContainer.selectedItem) { styleDesignerForm.itemsContainer.selectedItem.move("Up"); }
    }

    //Move Down
    styleDesignerForm.toolBar.moveDown.action = function () {
        if (styleDesignerForm.itemsContainer.selectedItem) { styleDesignerForm.itemsContainer.selectedItem.move("Down"); }
    }

    //Duplicate Style
    styleDesignerForm.toolBar.duplicateStyle.action = function () {
        if (styleDesignerForm.itemsContainer.selectedItem) {
            this.jsObject.SendCommandDuplicateStyle(styleDesignerForm.itemsContainer.selectedItem.styleObject.oldName);
        }
    }

    //Get Style
    styleDesignerForm.toolBar.getStyle.action = function () {
        if  (this.jsObject.options.selectedObject)
            this.jsObject.SendCommandCreateStyleFromComponent(this.jsObject.options.selectedObject.properties.name);
    }

    //Copy Style
    styleDesignerForm.toolBar.copyStyle.action = function () {
        if (styleDesignerForm.itemsContainer.selectedItem) {
            this.jsObject.options.buttons.pasteComponent.setEnabled(false);
            styleDesignerForm.toolBar.pasteStyle.setEnabled(true);
            this.jsObject.SendCommandCopyStyle(styleDesignerForm.itemsContainer.selectedItem.styleObject.oldName);
        }
    }

    //Cut Style
    styleDesignerForm.toolBar.cutStyle.action = function () {
        if (styleDesignerForm.itemsContainer.selectedItem) {
            this.jsObject.options.buttons.pasteComponent.setEnabled(false);
            styleDesignerForm.toolBar.pasteStyle.setEnabled(true);
            this.jsObject.SendCommandCopyStyle(styleDesignerForm.itemsContainer.selectedItem.styleObject.oldName);
            this.jsObject.SendCommandRemoveStyle(styleDesignerForm.itemsContainer.selectedItem.styleObject.oldName, styleDesignerForm.itemsContainer.selectedItem.key);
        }
    }

    //Paste Style
    styleDesignerForm.toolBar.pasteStyle.action = function () {
        this.jsObject.SendCommandPasteStyle();
    }
    
    //Create Styles Collection
    styleDesignerForm.toolBar.createStyleCollection.action = function () {
        this.jsObject.InitializeCreateStyleCollectionForm(function (createStyleCollectionForm) {
            createStyleCollectionForm.changeVisibleState(true);
        });
    }

    //Remove Style
    styleDesignerForm.toolBar.removeStyle.action = function () {
        var selectedItem = styleDesignerForm.itemsContainer.selectedItem;
        if (selectedItem) this.jsObject.SendCommandRemoveStyle(selectedItem.styleObject.oldName, selectedItem.key);
    }

    //Apply Styles Menu
    this.InitializeApplyStylesMenu();
    styleDesignerForm.toolBar.applyStyles.action = function () {
        this.jsObject.options.menus.applyStylesMenu.changeVisibleState(!this.jsObject.options.menus.applyStylesMenu.visible);
    }

    //Add Style Menu
    this.InitializeAddStyleMenu();
    styleDesignerForm.toolBar.addStyle.action = function () {
        this.jsObject.options.menus.addStyleMenu.changeVisibleState(!this.jsObject.options.menus.addStyleMenu.visible);
    }

    //Open Style
    styleDesignerForm.toolBar.openStyle.action = function () {
        if (this.jsObject.options.canOpenFiles) {
            this.jsObject.InitializeOpenDialog("loadStyleFromFile", this.jsObject.StiHandleOpenStyle, ".sts");
            this.jsObject.options.openDialogs.loadStyleFromFile.action();
        }
    }

    //Save Style
    styleDesignerForm.toolBar.saveStyle.action = function () {
        this.jsObject.SendCommandSaveStyle(styleDesignerForm.getStylesCollection());
    }

    //Items Container
    styleDesignerForm.itemsContainer = this.StyleDesignerFormItemsContainer(styleDesignerForm);
    mainTable.addCellInNextRow(styleDesignerForm.itemsContainer).style.width = "100%";

    //PropertiesPanel
    styleDesignerForm.propertiesPanel = this.StyleDesignerFormPropertiesPanel(styleDesignerForm);
    var styleDesignerPropertiesPanel = this.options.propertiesPanel.styleDesignerPropertiesPanel;
    while (styleDesignerPropertiesPanel.childNodes[0]) styleDesignerPropertiesPanel.removeChild(styleDesignerPropertiesPanel.childNodes[0]);
    this.options.propertiesPanel.styleDesignerPropertiesPanel.appendChild(styleDesignerForm.propertiesPanel);

    //Methods
    styleDesignerForm.addStyle = function (styleType) {
        this.jsObject.SendCommandAddStyle(styleType);
    }

    styleDesignerForm.onshow = function () {
        this.toolBar.pasteStyle.setEnabled(false);
        this.jsObject.options.propertiesPanel.setStyleDesignerMode(true);
        this.itemsContainer.addItems(this.jsObject.options.report.stylesCollection);
        this.isModified = false;
    }

    styleDesignerForm.getStylesCollection = function () {
        var stylesCollection = [];
        for (var i = 0; i < this.itemsContainer.childNodes.length; i++) {
            if (this.itemsContainer.childNodes[i].styleObject) {
                stylesCollection.push(this.itemsContainer.childNodes[i].styleObject);
            }
        }
        return stylesCollection;
    }

    styleDesignerForm.onhide = function () {
        this.jsObject.options.propertiesPanel.setStyleDesignerMode(false);
        if (!this.isModified) return;
        this.jsObject.SendCommandUpdateStyles(this.getStylesCollection());
    }

    styleDesignerForm.action = function () {
        this.changeVisibleState(false);
    }

    return styleDesignerForm;
}

StiMobileDesigner.prototype.StyleDesignerFormPropertiesPanel = function (styleDesignerForm) {
    var propertiesPanel = document.createElement("div");
    propertiesPanel.className = "stiDesignerStyleDesignerFormPropertiesPanel";
    propertiesPanel.propertiesGroups = {};

    //Add Properties Groups
    var propertiesGroups = [
        ["main", this.loc.PropertyCategory.MainCategory],
        ["appearance", this.loc.PropertyCategory.AppearanceCategory],
        ["parameters", this.loc.PropertyCategory.ParametersCategory],
        ["gridLines", this.loc.Chart.GridLines],
        ["area", this.loc.PropertyCategory.AreaCategory],
        ["series", this.loc.Chart.Series],
        ["seriesLabels", this.loc.PropertyCategory.SeriesLabelsCategory],
        ["trendLine", this.loc.PropertyCategory.TrendLineCategory],
        ["legend", this.loc.PropertyCategory.LegendCategory],
        ["axis", this.loc.PropertyCategory.AxisCategory],
        ["interlacing", this.loc.PropertyCategory.InterlacingCategory]
    ];

    for (var i in propertiesGroups) {
        var propertiesGroup = this.PropertiesGroup(propertiesGroups[i][0] + "StylesDesigner", propertiesGroups[i][1]);
        propertiesGroup.style.margin = "5px 0 5px 0";
        propertiesGroup.style.display = "none";
        propertiesPanel.propertiesGroups[propertiesGroups[i][0]] = propertiesGroup;
        propertiesPanel.appendChild(propertiesGroup);
        propertiesGroup.changeOpenedState(true);
    }

    //Add Properties Controls
    propertiesPanel.properties_ = {};
    var propAttributes = [
        ["name", this.loc.PropertyMain.Name, this.PropertyTextBox("styleDesignerPropertyName", this.options.propertyControlWidth), "main"],
        ["description", this.loc.PropertyMain.Description, this.PropertyTextBox("styleDesignerPropertyDescription", this.options.propertyControlWidth), "main"],
        ["collectionName", this.loc.PropertyMain.CollectionName, this.PropertyTextBox("styleDesignerPropertyCollectionName", this.options.propertyControlWidth), "main"],
        ["conditions", this.loc.PropertyMain.Conditions, this.PropertyTextBoxWithEditButton("styleDesignerPropertyConditions", this.options.propertyControlWidth, true), "main"],
        ["brush", this.loc.PropertyMain.Brush, this.PropertyBrushControl("styleDesignerPropertyBrush", null, this.options.propertyControlWidth), "appearance"],
        ["textBrush", this.loc.PropertyMain.TextBrush, this.PropertyBrushControl("styleDesignerPropertyTextBrush", null, this.options.propertyControlWidth), "appearance"],
        ["border", this.loc.PropertyMain.Borders, this.PropertyBorderControl("styleDesignerPropertyBorder", this.options.propertyControlWidth), "appearance"],
        ["font", this.loc.PropertyMain.Font, this.PropertyFontControl("styleDesignerPropertyFont", null, true), "appearance"],
        ["horAlignment", this.loc.PropertyMain.HorAlignment, this.PropertyDropDownList("styleDesignerPropertyHorizontalAlignment", this.options.propertyControlWidth, this.GetHorizontalAlignmentItems(), true, false), "appearance"],
        ["vertAlignment", this.loc.PropertyMain.VertAlignment, this.PropertyDropDownList("styleDesignerPropertyVerticalAlignment", this.options.propertyControlWidth, this.GetVerticalAlignmentItems(), true, false), "appearance"],
        ["color", this.loc.PropertyMain.Color, this.PropertyColorControl("styleDesignerPropertyColor", null, this.options.propertyControlWidth), "appearance"],
        ["backColor", this.loc.PropertyMain.BackColor, this.PropertyColorControl("styleDesignerPropertyBackColor", null, this.options.propertyControlWidth), "parameters"],
        ["foreColor", this.loc.PropertyMain.ForeColor, this.PropertyColorControl("styleDesignerPropertyForeColor", null, this.options.propertyControlWidth), "parameters"],
        ["allowUseBackColor", this.loc.PropertyMain.AllowUseBackColor, this.CheckBox("styleDesignerPropertyAllowUseBackColor"), "parameters"],
        ["allowUseForeColor", this.loc.PropertyMain.AllowUseForeColor, this.CheckBox("styleDesignerPropertyAllowUseForeColor"), "parameters"],
        ["allowUseBorderFormatting", this.loc.PropertyMain.AllowUseBorderFormatting, this.CheckBox("styleDesignerPropertyAllowUseBorderFormatting"), "parameters"],
        ["allowUseBorderSides", this.loc.PropertyMain.AllowUseBorderSides, this.CheckBox("styleDesignerPropertyAllowUseBorderSides"), "parameters"],
        ["allowUseBorderSidesFromLocation", this.loc.PropertyMain.AllowUseBorderSidesFromLocation, this.CheckBox("styleDesignerPropertyAllowUseBorderSidesFromLocation"), "parameters"],
        ["allowUseBrush", this.loc.PropertyMain.AllowUseBrush, this.CheckBox("styleDesignerPropertyAllowUseBrush"), "parameters"],
        ["allowUseFont", this.loc.PropertyMain.AllowUseFont, this.CheckBox("styleDesignerPropertyAllowUseFont"), "parameters"],
        ["allowUseImage", this.loc.PropertyMain.AllowUseImage, this.CheckBox("styleDesignerPropertyAllowUseImage"), "parameters"],
        ["allowUseTextBrush", this.loc.PropertyMain.AllowUseTextBrush, this.CheckBox("styleDesignerPropertyAllowUseTextBrush"), "parameters"],
        ["allowUseHorAlignment", this.loc.PropertyMain.AllowUseHorAlignment, this.CheckBox("styleDesignerPropertyAllowUseHorAlignment"), "parameters"],
        ["allowUseVertAlignment", this.loc.PropertyMain.AllowUseVertAlignment, this.CheckBox("styleDesignerPropertyAllowUseVertAlignment"), "parameters"],
        ["basicStyleColor", this.loc.PropertyMain.BasicStyleColor, this.PropertyColorControl("styleDesignerPropertyBasicStyleColor", null, this.options.propertyControlWidth), "parameters"],
        ["styleColors", this.loc.PropertyMain.StyleColors, this.PropertyColorsCollectionControl("styleDesignerPropertyStyleColors", null, this.options.propertyControlWidth), "parameters"],
        ["brushType", this.loc.PropertyMain.BrushType, this.PropertyDropDownList("styleDesignerPropertyBrushType", this.options.propertyControlWidth, this.GetChartStyleBrushTypeItems(), true, false), "parameters"],
        ["chartAreaShowShadow", this.loc.PropertyMain.ChartAreaShowShadow, this.CheckBox("styleDesignerAreaShowShadow"), "area"],
        ["chartAreaBorderColor", this.loc.PropertyMain.ChartAreaBorderColor, this.PropertyColorControl("styleDesignerAreaBorderColor", null, this.options.propertyControlWidth), "area"],
        ["chartAreaBrush", this.loc.PropertyMain.ChartAreaBrush, this.PropertyBrushControl("styleDesignerAreaBrush", null, this.options.propertyControlWidth), "area"],
        ["seriesLabelsBorderColor", this.loc.PropertyMain.SeriesLabelsBorderColor, this.PropertyColorControl("styleDesignerSeriesLabelsBorderColor", null, this.options.propertyControlWidth), "seriesLabels"],
        ["seriesLabelsColor", this.loc.PropertyMain.SeriesLabelsColor, this.PropertyColorControl("styleDesignerSeriesLabelsColor", null, this.options.propertyControlWidth), "seriesLabels"],
        ["seriesLabelsBrush", this.loc.PropertyMain.SeriesLabelsBrush, this.PropertyBrushControl("styleDesignerSeriesLabelsBrush", null, this.options.propertyControlWidth), "seriesLabels"],
        ["seriesLighting", this.loc.PropertyMain.SeriesLighting, this.CheckBox("styleDesignerPropertySeriesLighting"), "series"],
        ["seriesShowShadow", this.loc.PropertyMain.SeriesShowShadow, this.CheckBox("styleDesignerPropertySeriesShowShadow"), "series"],
        ["trendLineShowShadow", this.loc.PropertyMain.TrendLineShowShadow, this.CheckBox("styleDesignerTrendLineShowShadow"), "trendLine"],
        ["trendLineColor", this.loc.PropertyMain.TrendLineColor, this.PropertyColorControl("styleDesignerTrendLineColor", null, this.options.propertyControlWidth), "trendLine"],
        ["legendBorderColor", this.loc.PropertyMain.LegendBorderColor, this.PropertyColorControl("styleDesignerLegendBorderColor", null, this.options.propertyControlWidth), "legend"],
        ["legendLabelsColor", this.loc.PropertyMain.LegendLabelsColor, this.PropertyColorControl("styleDesignerLegendLabelsColor", null, this.options.propertyControlWidth), "legend"],
        ["legendTitleColor", this.loc.PropertyMain.LegendTitleColor, this.PropertyColorControl("styleDesignerLegendTitleColor", null, this.options.propertyControlWidth), "legend"],
        ["legendBrush", this.loc.PropertyMain.LegendBrush, this.PropertyBrushControl("styleDesignerLegendBrush", null, this.options.propertyControlWidth), "legend"],
        ["axisLabelsColor", this.loc.PropertyMain.AxisLabelsColor, this.PropertyColorControl("styleDesignerAxisLabelsColor", null, this.options.propertyControlWidth), "axis"],
        ["axisLineColor", this.loc.PropertyMain.AxisLineColor, this.PropertyColorControl("styleDesignerAxisLineColor", null, this.options.propertyControlWidth), "axis"],
        ["axisTitleColor", this.loc.PropertyMain.AxisTitleColor, this.PropertyColorControl("styleDesignerAxisTitleColor", null, this.options.propertyControlWidth), "axis"],
        ["interlacingHorBrush", this.loc.PropertyMain.InterlacingHorBrush, this.PropertyBrushControl("styleDesignerInterlacingHorBrush", null, this.options.propertyControlWidth), "interlacing"],
        ["interlacingVertBrush", this.loc.PropertyMain.InterlacingVertBrush, this.PropertyBrushControl("styleDesignerInterlacingVertBrush", null, this.options.propertyControlWidth), "interlacing"],
        ["gridLinesHorColor", this.loc.PropertyMain.GridLinesHorColor, this.PropertyColorControl("styleDesignerGridLinesHorColor", null, this.options.propertyControlWidth), "gridLines"],
        ["gridLinesVertColor", this.loc.PropertyMain.GridLinesVertColor, this.PropertyColorControl("styleDesignerGridLinesVertColor", null, this.options.propertyControlWidth), "gridLines"]
    ]

    for (var i in propAttributes) {
        var control = propAttributes[i][2];
        var property = this.Property(null, propAttributes[i][1], control);
        property.name = propAttributes[i][0];
        property.updateCaption();
        propertiesPanel.propertiesGroups[propAttributes[i][3]].container.appendChild(property);
        propertiesPanel.properties_[propAttributes[i][0]] = property;
        control.propertyName = propAttributes[i][0];
        control.groupName = propAttributes[i][3];

        control.action = function () {
            var styleProperties = styleDesignerForm.itemsContainer.selectedItem.styleObject.properties;
            if (this["setKey"] != null) styleProperties[this.propertyName] = this.key;
            else if (this["setChecked"] != null) styleProperties[this.propertyName] = this.isChecked;
            else if (this["value"] != null) styleProperties[this.propertyName] = this.value;
            else if (this["textBox"] != null && this.textBox["value"] != null) styleProperties[this.propertyName] = this.textBox.value;
            styleDesignerForm.itemsContainer.selectedItem.repaint();
            styleDesignerForm.isModified = true;
            this.jsObject.SendCommandApplyStyleProperties(styleDesignerForm.itemsContainer.selectedItem.styleObject);
        }

        if (propAttributes[i][0] == "conditions") {
            control.button.action = function () {
                var this_ = this;
                this.jsObject.InitializeStyleConditionsForm(function (styleConditionsForm) {
                    styleConditionsForm.show(styleDesignerForm, this_.textBox);
                });
            }
        }
    }

    propertiesPanel.updatePropertiesCaptions = function () {
        for (var propertyName in this.properties_) {
            this.properties_[propertyName].updateCaption();
        }
    }
        
    propertiesPanel.updateControls = function (styleObject) {
        var propertyValues = styleObject.properties;
        var propertyRows = this.properties_;
        var showingGroups = {};

        for (var i in propertiesGroups) {
            propertiesPanel.propertiesGroups[propertiesGroups[i][0]].style.display = "none";
        }

        for (var name in propertyRows) {
            propertyRows[name].style.display = propertyValues[name] != null ? "" : "none";
            if (propertyValues[name] != null) {
                var propertyValue = propertyValues[name];
                var propertyControl = propertyRows[name].propertyControl;
                if (!propertyControl) continue;                
                if (propertiesPanel.propertiesGroups[propertyControl.groupName])
                    propertiesPanel.propertiesGroups[propertyControl.groupName].style.display = "";

                if (name == "conditions") {
                    var conditionsText = "[" + ((propertyValue && propertyValue.length > 0) 
                        ? styleDesignerForm.jsObject.loc.PropertyMain.Conditions : styleDesignerForm.jsObject.loc.FormConditions.NoConditions) + "]";
                    propertyControl.textBox.value = conditionsText;
                }
                else if (propertyControl["setKey"] != null) propertyControl.setKey(propertyValue);
                else if (propertyControl["setChecked"] != null) propertyControl.setChecked(propertyValue);
                else if (propertyControl["value"] != null) propertyControl.value = propertyValue;
                else if (propertyControl["textBox"] != null && propertyControl.textBox["value"] != null) propertyControl.textBox.value = propertyValue;
            }
        }
    }

    return propertiesPanel;
}

StiMobileDesigner.prototype.StyleDesignerFormItemsContainer = function (styleDesignerForm) {
    var container = this.Container("styleDesignerItemsContainer", null, 400);
    container.className = "styleDesignerItemsContainer";

    //Override
    container.addItem = function (styleObject) {
        var item = this.jsObject.StyleDesignerFormItem(styleObject, container);
        item.key = this.jsObject.newGuid().replace(/-/g, '');
        this.appendChild(item);        
        item.repaint();
        item.action = function () { this.setSelected(true); }
        item.action();
        this.onChange();
    }

    container.addItems = function (stylesCollection) {
        this.clear();
        for (var i in stylesCollection) {
            this.addItem(stylesCollection[i]);
        }
        if (this.getCountItems() > 0) this.childNodes[0].setSelected(true);
    }

    container.updateStatesMovingButtons = function () {
        var count = this.getCountItems();
        var index = this.selectedItem ? this.selectedItem.getIndex() : -1;
        styleDesignerForm.toolBar.moveUp.setEnabled(index > 0);
        styleDesignerForm.toolBar.moveDown.setEnabled(index != -1 && index < count - 1);
    }

    container.onChange = function () {
        this.updateStatesMovingButtons();
        styleDesignerForm.toolBar.removeStyle.setEnabled(this.selectedItem);
        styleDesignerForm.toolBar.duplicateStyle.setEnabled(this.selectedItem);        
        styleDesignerForm.toolBar.copyStyle.setEnabled(this.selectedItem);
        styleDesignerForm.toolBar.cutStyle.setEnabled(this.selectedItem);
        styleDesignerForm.toolBar.getStyle.setEnabled(this.jsObject.options.selectedObject &&
            this.jsObject.options.selectedObject.typeComponent != "StiPage" && this.jsObject.options.selectedObject.typeComponent != "StiReport");
        styleDesignerForm.isModified = true;
    }

    container.getItemByKey = function (key) {
        for (var i = 0; i < this.childNodes.length; i++) {
            if (this.childNodes[i].key == key) return this.childNodes[i];
        }
        return null;
    }

    container.getCollectionNameItems = function () {
        var collectionNames = {};
        for (var i = 0; i < this.childNodes.length; i++) {
            var collectionName = this.childNodes[i].styleObject.properties.collectionName;
            if (collectionName != "") collectionNames[collectionName] = collectionName;
        }
        var items = [];
        if (this.jsObject.GetCountObjects(collectionNames) == 0) {
            items.push(this.jsObject.Item("itemNotFound", this.jsObject.loc.FormStyleDesigner.StyleCollectionsNotFound, null, "StyleCollectionsNotFound"));
        }
        else {
            for (var i in collectionNames) {
                items.push(this.jsObject.Item("item" + i, collectionNames[i], null, collectionNames[i]));
            }
        }
        return items;
    }

    container.getCountItems = function () {
        return container.childNodes.length;
    }

    return container;
}

StiMobileDesigner.prototype.StyleDesignerFormItem = function (styleObject, container) {
    var name = styleObject.properties.name;
    var item = this.DinamicSmallButton(name, null, name, "Styles." + styleObject.type + "32.png", name, null, this.GetStyles("DesignerStylesItem"), true);
    item.styleObject = styleObject;
    item.styleObject.oldName = styleObject.properties.name;
    item.container = container;

    //Override Styles
    item.style.margin = "2px";
    item.style.padding = "3px";
    item.style.overflow = "hidden";
    item.innerTable.style.width = "100%";
    item.imageCell.style.width = "1px";
    item.imageCell.style.padding = "0 3px 0 3px";
    item.caption.style.padding = "0px 5px 0px 0px";

    var captionContainer = document.createElement("div");
    captionContainer.style.maxHeight = "38px";
    captionContainer.style.overflow = "hidden";

    var captionInnerContainer = document.createElement("div");
    item.captionInnerContainer = captionInnerContainer;
    captionContainer.appendChild(captionInnerContainer);
    captionInnerContainer.innerHTML = item.caption.innerHTML;
    item.caption.innerHTML = "";
    item.caption.appendChild(captionContainer);
    captionInnerContainer.style.position = "relative";

    //Override Methods
    item.onmouseover = null;
    item.onmouseout = null;
    item.onmouseenter = null;
    item.onmouseleave = null;

    item.setSelected = function (state) {
        if (state) {
            if (this.container.selectedItem) this.container.selectedItem.setSelected(false);
            this.container.selectedItem = this;
            this.onSelected();
        }
        this.isSelected = state;
        this.className = (state ? this.styles["selected"] : (this.isEnabled ? (this.isOver ? this.styles["over"] : this.styles["default"]) : this.styles["disabled"])) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    item.repaint = function () {
        var properties = this.styleObject.properties;
        var defaultFont = "Arial!10!0!0!0!0";
        var defaultBrush = "1!transparent";
        var defaultTextBrush = "1!0,0,0";
        var defaultBorder = "default";

        this.captionInnerContainer.innerHTML = properties["description"] || properties["name"];

        var font = (properties["font"] != null && properties["allowUseFont"]) ? properties["font"] : defaultFont;
        var brush = (properties["brush"] != null && properties["allowUseBrush"]) ? properties["brush"] : defaultBrush;
        var textBrush = (properties["textBrush"] != null && properties["allowUseTextBrush"]) ? properties["textBrush"] : defaultTextBrush;
        var border = (properties["border"] != null && properties["allowUseBorderSides"]) ? properties["border"] : defaultBorder;
        this.jsObject.RepaintControlByAttributes(this.innerTable, font, brush, textBrush, border);
        this.captionInnerContainer.style.top = (this.captionInnerContainer.offsetHeight > 40 ? ((this.captionInnerContainer.offsetHeight - 40) / 2 * -1) : 0) + "px";

        if (this.styleObject.type == "StiCrossTabStyle") {
            var sampleTable = this.jsObject.CrossTabSampleTable(32, 32, 5, 5, this.styleObject.properties.color, this.styleObject.properties.color);
            if (this.imageCell) {
                this.imageCell.innerHTML = "";
                this.imageCell.appendChild(sampleTable);
            }
        }
    }

    item.onSelected = function () {
        var this_ = this;
        this.container.updateStatesMovingButtons();
        this.jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
            styleDesignerForm.propertiesPanel.updateControls(this_.styleObject);
        });
    }

    item.remove = function () {
        var nextItem = this.nextSibling;
        var prevItem = this.previousSibling;
        this.container.removeChild(this);
        if (this.container.getCountItems() > 0) {
            if (nextItem)
                nextItem.setSelected(true);
            else if (prevItem)
                prevItem.setSelected(true);
            else
                this.container.childNodes[0].setSelected(true);
        }
        else
            this.container.selectedItem = null;
        this.container.onChange();
    }

    item.getIndex = function () {
        for (var i = 0; i < this.container.childNodes.length; i++)
            if (this.container.childNodes[i] == this) return i;
    };

    item.move = function (direction) {
        var index = this.getIndex();
        this.container.removeChild(this);
        var count = this.container.getCountItems();
        var newIndex = direction == "Up" ? index - 1 : index + 1;
        if (direction == "Up" && newIndex == -1) newIndex = 0;
        if (direction == "Down" && newIndex >= count) {
            this.container.appendChild(this);
            this.container.onChange();
            return;
        }
        this.container.insertBefore(this, this.container.childNodes[newIndex]);
        this.container.onChange();
    }

    return item;
}