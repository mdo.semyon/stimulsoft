﻿
StiMobileDesigner.prototype.InitializeTextEditorForm_ = function () {

    //Text Editor Form
    var textEditorForm = this.BaseFormPanel("textEditor", this.loc.PropertyMain.Text, 3, this.HelpLinks["expression"]);
    textEditorForm.propertyName = "";
    textEditorForm.dataTree = this.options.dataTree;
    textEditorForm.mode = "Expression";

    this.AddProgressToControl(textEditorForm);

    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerImageFormMainPanel";
    textEditorForm.container.appendChild(mainTable);
    textEditorForm.container.style.padding = "0px";

    //Buttons
    var buttonProps = [
        ["Expression", "BigTextExpression.png", this.loc.PropertyMain.Expression],
        ["DataColumn", "BigTextDataColumn.png", this.loc.PropertyMain.DataColumn],
        ["SystemVariable", "BigTextVariable.png", this.loc.PropertyMain.SystemVariable],
        ["SummaryText", "BigTextSummary.png", this.loc.PropertyMain.Summary],
        ["RichText", "BigTextHtml.png", "HTML"]
    ];

    //Add Panels && Buttons
    var panelsContainer = mainTable.addCell();
    var buttonsPanel = mainTable.addCell();
    buttonsPanel.style.verticalAlign = "top";
    textEditorForm.mainButtons = {};
    textEditorForm.panels = {};

    for (var i = 0; i < buttonProps.length; i++) {
        var panel = document.createElement("Div");
        panel.className = "stiDesignerTextEditorFormPanel";
        if (i != 0) panel.style.display = "none";
        panelsContainer.appendChild(panel);
        textEditorForm.panels[buttonProps[i][0]] = panel;
        if (this.options.isTouchDevice) panel.style.width = "630px";
        var button = this.StandartFormBigButton("textEditorForm" + buttonProps[i][0] + "Button", null, buttonProps[i][2], buttonProps[i][1], buttonProps[i][2], 80);
        button.style.margin = "2px";
        textEditorForm.mainButtons[buttonProps[i][0]] = button;
        buttonsPanel.appendChild(button);
        button.panelName = buttonProps[i][0];
        button.action = function () {
            textEditorForm.setMode(this.panelName);
        }
    }

    //Expression
    var expressionTextArea = this.TextArea("textEditorFormExpression", this.options.isTouchDevice ? 612 : 532, 436);
    textEditorForm.expressionTextArea = expressionTextArea;
    textEditorForm.expressionTextArea.style.margin = "6px";
    textEditorForm.panels.Expression.appendChild(textEditorForm.expressionTextArea);
    textEditorForm.panels.Expression.style.overflow = "hidden";
    textEditorForm.expressionTextArea.addInsertButton();

    textEditorForm.expressionTextArea.insertButton.action = function () {
        var dictionaryTree = this.jsObject.options.dictionaryTree;
        if (dictionaryTree && dictionaryTree.selectedItem) {
            var resultDictItem = dictionaryTree.selectedItem.getResultForEditForm();
            textEditorForm.expressionTextArea.insertText(resultDictItem);
        }
    }
        
    //Check Expression
    var checkExpression = this.FormButton(null, null, this.loc.Buttons.Check);
    checkExpression.style.margin = "8px";
    checkExpression.style.maxWidth = "50px";
    textEditorForm.buttonsPanel.style.width = "100%";
    textEditorForm.buttonsPanel.firstChild.style.width = "100%";
    textEditorForm.buttonsPanel.firstChild.tr[0].insertCell(0).appendChild(checkExpression);
    textEditorForm.buttonOk.parentElement.style.width = "1px";
    textEditorForm.buttonCancel.parentElement.style.width = "1px";

    checkExpression.action = function (autoStarted) {
        checkExpression.setEnabled(false);
        this.jsObject.SendCommandCheckExpression(
            Base64.encode(textEditorForm.expressionTextArea.value), function (answer) {
                checkExpression.setEnabled(true);
                if (answer.checkResult) {
                    var message = Base64.decode(answer.checkResult);
                    if (message == "OK" && autoStarted) return;
                    checkExpression.jsObject.InitializeCheckExpressionPopupPanel(function (checkPopupPanel) {
                        checkPopupPanel.show(message, checkExpression);
                    });
                }
            });
    };
            
    //expressionTextArea.onTextChange = function () {
    //    clearTimeout(expressionTextArea.changeTimer);

    //    expressionTextArea.changeTimer = setTimeout(function () {
    //        if (expressionTextArea.value != expressionTextArea.oldText && checkExpression.isEnabled) {
    //            checkExpression.action(true);
    //        }
    //        expressionTextArea.oldText = expressionTextArea.value;
    //    }, 1000);
    //}
    
    //System Variables
    var systemVarsTree = this.SystemVariablesTree();
    systemVarsTree.style.height = "350px";
    systemVarsTree.style.overflow = "auto";
    textEditorForm.panels.SystemVariable.appendChild(systemVarsTree);

    var systemVarsInfoPanel = document.createElement("div");
    systemVarsInfoPanel.className = "stiDesignerTextEditFormSystemVariablesInfo";
    textEditorForm.panels.SystemVariable.appendChild(systemVarsInfoPanel);

    systemVarsTree.onSelectedItem = function (item) {
        systemVarsInfoPanel.innerHTML = textEditorForm.jsObject.GetSystemVariableDescription(item.itemObject.name);
    };
    systemVarsTree.action = function () { textEditorForm.action(); };

    //Summary
    var summaryText = this.SummaryExpression("textEditorFormSummaryText")
    summaryText.style.height = "440px";
    textEditorForm.panels.SummaryText.appendChild(summaryText);

    //RichText
    var richTextEditor = this.RichTextEditor(null, this.options.isTouchDevice ? 616 : 536, this.options.isTouchDevice ? 368 : 370);
    textEditorForm.panels.RichText.appendChild(richTextEditor);
    richTextEditor.action = function () {
        textEditorForm.expressionTextArea.value = richTextEditor.getText();
    }

    var allowHtmlTags = this.CheckBox(null, this.loc.PropertyMain.AllowHtmlTags);
    allowHtmlTags.style.margin = "10px 0 0 6px";
    textEditorForm.panels.RichText.appendChild(allowHtmlTags);

    richTextEditor.onchange = function () {
        allowHtmlTags.setChecked(true);
    }

    //Form Methods
    textEditorForm.reset = function () {
        textEditorForm.dataTree.setKey("");
        textEditorForm.expressionTextArea.value = "";
        summaryText.reset();
        textEditorForm.setMode("Expression");
    }

    textEditorForm.setMode = function (mode) {
        textEditorForm.mode = mode;
        for (var panelName in textEditorForm.panels) {
            textEditorForm.panels[panelName].style.display = mode == panelName ? "" : "none";
            textEditorForm.mainButtons[panelName].setSelected(mode == panelName);
        }
        var propertiesPanel = textEditorForm.jsObject.options.propertiesPanel;
        propertiesPanel.setEnabled(mode == "Expression" || mode == "RichText");
        propertiesPanel.editFormControl = null;
        if (mode == "Expression") {
            propertiesPanel.editFormControl = textEditorForm.expressionTextArea;
            textEditorForm.expressionTextArea.focus();
        }
        else if (mode == "RichText") {
            richTextEditor.setText(textEditorForm.expressionTextArea.value);
            propertiesPanel.editFormControl = richTextEditor;
        }
        else if (mode == "SummaryText") {
            summaryText.controls.expressionTextArea.focus();
        }
        checkExpression.style.display = mode == "Expression" ? "" : "none";
    }

    textEditorForm.onhide = function () {
        textEditorForm.jsObject.options.propertiesPanel.setDictionaryMode(textEditorForm.oldDictionaryMode);
    }

    textEditorForm.onshow = function () {
        textEditorForm.setMode("Expression");
        textEditorForm.container.style.visibility = "hidden";
        var currentObject = this.jsObject.options.selectedObject || this.jsObject.GetCommonObject(this.jsObject.options.selectedObjects);
        textEditorForm.mainButtons.RichText.style.display = currentObject && currentObject.typeComponent == "StiText" ? "" : "none";
        textEditorForm.oldDictionaryMode = this.jsObject.options.propertiesPanel.dictionaryMode;
    }

    textEditorForm.oncompleteshow = function () {
        if (this.showFunction) {
            this.showFunction();
            this.showFunction = null;
            return;
        }

        textEditorForm.progress.show();
        var jsObject = this.jsObject;

        setTimeout(function () {
            textEditorForm.mainButtons.SummaryText.style.display = textEditorForm.propertyName == "text" ? "" : "none";
            textEditorForm.jsObject.options.propertiesPanel.setDictionaryMode(true);

            //Build SystemVariables Tree
            systemVarsInfoPanel.innerHTML = "";
            systemVarsTree.build();
            if (systemVarsTree.firstChild && systemVarsTree.firstChild.childsContainer.childNodes.length > 0) {
                systemVarsTree.firstChild.childsContainer.childNodes[0].setSelected();
            }

            //Build Data Tree
            textEditorForm.panels.DataColumn.appendChild(textEditorForm.dataTree);
            textEditorForm.dataTree.build(null, null, null, true);
            textEditorForm.dataTree.action = function () {
                textEditorForm.action();
            }

            //Reset Controls
            textEditorForm.reset();

            if (jsObject.options.selectedObjects) {
                textEditorForm.setMode("Expression");
                textEditorForm.expressionTextArea.focus();
            }
            else {
                var selectedObject = jsObject.options.selectedObject;
                allowHtmlTags.setChecked(selectedObject.properties.allowHtmlTags);

                var propertyValue = selectedObject.properties[textEditorForm.propertyName] != null
                    ? Base64.decode(selectedObject.properties[textEditorForm.propertyName])
                    : (textEditorForm.resultControl != null ? textEditorForm.resultControl.value : "");

                var textType = selectedObject.properties.textType;
                var subStringPropertyValue = propertyValue.length > 1 ? propertyValue.substring(1, propertyValue.length - 1) : "";
                textEditorForm.expressionTextArea.value = propertyValue;


                if (textType == "DataColumn" || textEditorForm.dataTree.setKey(subStringPropertyValue)) {
                    if (textType == "DataColumn") textEditorForm.dataTree.setKey(subStringPropertyValue);
                    textEditorForm.setMode("DataColumn");
                    textEditorForm.dataTree.autoscroll();
                }
                else if (textType == "SystemVariables" || (textEditorForm.propertyName != "condition" && textEditorForm.jsObject.options.report &&
                textEditorForm.jsObject.IsContainted(textEditorForm.jsObject.options.report.dictionary.systemVariables, subStringPropertyValue))) {
                    textEditorForm.setMode("SystemVariable");
                    var selectedItem = systemVarsTree.mainItem.getChildByName(subStringPropertyValue);
                    if (selectedItem) {
                        selectedItem.setSelected();
                        systemVarsTree.autoscroll();
                    }
                }
                else if (textType == "Totals") {
                    textEditorForm.setMode("SummaryText");
                    summaryText.fill(propertyValue);
                }
                else {
                    textEditorForm.setMode(selectedObject.properties.allowHtmlTags ? "RichText" : "Expression");
                    textEditorForm.expressionTextArea.focus();
                }
            }
            textEditorForm.container.style.visibility = "visible";
            textEditorForm.progress.hide();
            textEditorForm.expressionTextArea.focus();
        }, 50);
    }

    textEditorForm.action = function () {
        textEditorForm.changeVisibleState(false);

        if (this.actionFunction) {
            this.actionFunction();
            this.actionFunction = null;
            return;
        }

        var result = textEditorForm.mode == "SummaryText" ? summaryText.controls.expressionTextArea.value : textEditorForm.expressionTextArea.value;
        var textType = textEditorForm.mode == "SummaryText" ? "Totals" : "Expression";

        if (textEditorForm.mode == "DataColumn") {
            result = (textEditorForm.dataTree.key ? "{" + textEditorForm.dataTree.key + "}" : "");
            textType = "DataColumn";
        }
        else if (textEditorForm.mode == "SystemVariable") {
            result = systemVarsTree.selectedItem ? "{" + systemVarsTree.selectedItem.itemObject.name + "}" : "";
            textType = "SystemVariables";
        }
        else if (textEditorForm.mode == "RichText") {
            result = richTextEditor.getText();
        }
        
        var propertyNames = [textEditorForm.propertyName];
        var propertyValues = [Base64.encode(result)];

        if (textEditorForm.propertyName == "text") {
            propertyNames.push("textType");
            propertyValues.push(textType);
        }

        var currentObject = this.jsObject.options.selectedObject || this.jsObject.GetCommonObject(this.jsObject.options.selectedObjects);
        if (currentObject && currentObject.typeComponent == "StiText") {
            propertyNames.push("allowHtmlTags");
            propertyValues.push(allowHtmlTags.isChecked);
        }

        this.jsObject.ApplyPropertyValue(propertyNames, propertyValues);
    }

    return textEditorForm;
}