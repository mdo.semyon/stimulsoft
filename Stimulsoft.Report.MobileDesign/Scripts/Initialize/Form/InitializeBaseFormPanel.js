﻿
StiMobileDesigner.prototype.BaseFormPanel = function (name, caption, level, helpUrl) {
    var formPanel = this.BaseForm(name, caption, level, helpUrl);
    formPanel.isFormPanel = true;

    //Add Arrow
    //var arrow = document.createElement("div");
    //arrow.className = "stiDesignerFormPanelArrow";
    //formPanel.appendChild(arrow);

    //Override Methods
    formPanel.changeVisibleState = function (state) {
        this.jsObject.options.propertiesPanel.setZIndex(state, formPanel.level);
        if (state) {
            this.style.display = "";
            if (this.jsObject.options.currentForm && this.parentElement == this.jsObject.options.mainPanel) {
                this.jsObject.options.mainPanel.removeChild(this);
                this.jsObject.options.mainPanel.appendChild(this);
            }
            this.onshow();
            this.jsObject.options.propertiesPanel.changeVisibleState(true);
            this.jsObject.SetObjectToCenter(this);
            if (!this.jsObject.options.disabledPanels) this.jsObject.InitializeDisabledPanels();
            this.jsObject.options.disabledPanels[this.level].changeVisibleState(true);
            this.visible = true;

            this.currentFormDownLevel = this.jsObject.options.currentForm && this.jsObject.options.currentForm.visible
                ? this.jsObject.options.currentForm : null;
            this.jsObject.options.currentForm = this;

            d = new Date();
            var endTime = d.getTime() + this.jsObject.options.formAnimDuration;
            this.flag = false;
            this.jsObject.ShowAnimationForm(this, endTime);
        }
        else {
            clearTimeout(this.animationTimer);
            this.visible = false;
            this.jsObject.options.currentForm = this.currentFormDownLevel || null;
            this.style.display = "none";
            if (!this.jsObject.options.forms[this.name]) {
                this.jsObject.options.mainPanel.removeChild(this);
            }
            this.onhide();
            var propertiesPanel = this.jsObject.options.propertiesPanel;
            if (propertiesPanel && !propertiesPanel.styleDesignerMode &&
                !propertiesPanel.editChartMode && !propertiesPanel.editGaugeMode &&
                !propertiesPanel.dictionaryMode && propertiesPanel.fixedViewMode) {
                propertiesPanel.changeVisibleState(false);
            }
            if (!this.jsObject.options.disabledPanels) this.jsObject.InitializeDisabledPanels();
            this.jsObject.options.disabledPanels[this.level].changeVisibleState(false);
        }
    }

    //formPanel.header.saveStartPosition = function (event, currStartX, currStartY) {
    //    var formStartX = formPanel.jsObject.FindPosX(formPanel, "stiDesignerMainPanel");
    //    var formStartY = formPanel.jsObject.FindPosY(formPanel, "stiDesignerMainPanel");
    //    formPanel.jsObject.options.formInDrag = [currStartX, currStartY, formStartX, formStartY, formPanel];
    //    formPanel.minLeftPos = formPanel.jsObject.FindPosX(formPanel.jsObject.options.propertiesPanel, "stiDesignerMainPanel") + formPanel.jsObject.options.propertiesPanel.offsetWidth + 10;
    //    formPanel.minTopPos = formPanel.jsObject.FindPosY(formPanel.jsObject.options.propertiesPanel, "stiDesignerMainPanel") + 10;
    //}

    //formPanel.move = function (evnt) {
    //    var leftPos = formPanel.jsObject.options.formInDrag[2] + (evnt.clientX - formPanel.jsObject.options.formInDrag[0]);
    //    var topPos = formPanel.jsObject.options.formInDrag[3] + (evnt.clientY - formPanel.jsObject.options.formInDrag[1]);

    //    formPanel.style.left = (leftPos < formPanel.minLeftPos ? formPanel.minLeftPos : leftPos) + "px";
    //    formPanel.style.top = (topPos < formPanel.minTopPos ? formPanel.minTopPos : topPos) + "px";
    //}

    return formPanel;
}
