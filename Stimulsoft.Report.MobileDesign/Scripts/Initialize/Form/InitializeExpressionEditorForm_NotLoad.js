﻿

StiMobileDesigner.prototype.InitializeExpressionEditorForm_ = function () {

    //Expression Editor Form
    var expressionEditorForm = this.BaseFormPanel("expressionEditor", this.loc.PropertyMain.Expression, 3, this.HelpLinks["expression"]);

    //Expression
    expressionEditorForm.expressionTextArea = this.TextArea("expressionEditorFormExpression", 436, 440);
    expressionEditorForm.expressionTextArea.style.margin = "4px";
    expressionEditorForm.container.appendChild(expressionEditorForm.expressionTextArea);
    expressionEditorForm.expressionTextArea.addInsertButton();

    expressionEditorForm.expressionTextArea.insertButton.action = function () {
        var dictionaryTree = this.jsObject.options.dictionaryTree;
        if (dictionaryTree && dictionaryTree.selectedItem) {
            var text = dictionaryTree.selectedItem.getResultForEditForm();
            if (expressionEditorForm.resultControl && expressionEditorForm.resultControl.cutExpression &&
                text.indexOf("{") == 0 && this.jsObject.EndsWith(text, "}"))
            {
                text = text.substr(1, text.length - 2);
            }
            expressionEditorForm.expressionTextArea.insertText(text);
        }
    }

    expressionEditorForm.onhide = function () {
        expressionEditorForm.jsObject.options.propertiesPanel.setDictionaryMode(false);
        var propertiesPanel = expressionEditorForm.jsObject.options.propertiesPanel;
        propertiesPanel.setEnabled(expressionEditorForm.propertiesPanelIsEnabled);
        propertiesPanel.style.zIndex = expressionEditorForm.propertiesPanelZIndex;
    }

    expressionEditorForm.onshow = function () {
        var propertiesPanel = expressionEditorForm.jsObject.options.propertiesPanel;
        propertiesPanel.setDictionaryMode(true);
        propertiesPanel.setEnabled(true);
        propertiesPanel.editFormControl = expressionEditorForm.expressionTextArea;
        if (expressionEditorForm.resultControl) {
            var control = expressionEditorForm.resultControl.textBox || expressionEditorForm.resultControl;
            expressionEditorForm.expressionTextArea.value = control.useHiddenValue ? control.hiddenValue : control.value;
            expressionEditorForm.expressionTextArea.cutExpression = expressionEditorForm.resultControl.cutExpression;
        }
        expressionEditorForm.expressionTextArea.focus();
    }

    expressionEditorForm.action = function () {
        if (expressionEditorForm.resultControl) {
            var control = expressionEditorForm.resultControl.textBox || expressionEditorForm.resultControl;
            control.value = expressionEditorForm.expressionTextArea.value;
            if (control.readOnly && control.useHiddenValue) control.hiddenValue = expressionEditorForm.expressionTextArea.value;
            if (expressionEditorForm.resultControl["action"]) {
                expressionEditorForm.resultControl.action();
            }
        }
        expressionEditorForm.changeVisibleState(false);
    }

    return expressionEditorForm;
}

