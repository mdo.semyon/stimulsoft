﻿StiMobileDesigner.prototype.InitializePublishForm_ = function () {
    var form = this.BaseForm("publishForm", this.loc.Cloud.ButtonPublish, 1);
    form.buttonsPanel.style.display = "none";
    form.buttonsSeparator.style.display = "none";

    this.AddProgressToControl(form.container);

    var frame = document.createElement("iframe");
    frame.style.height = "770px";
    frame.style.width = "1200px";
    frame.style.border = "0px";
    frame.overflow = "auto";
    form.container.appendChild(frame);
    form.frame = frame;

    form.show = function (reportString, newWindow) {
        var jsObject = this.jsObject;
        form.container.progress.show();
        form.frame.src = "about:blank";

        var iframeDoc = null;

        form.frame.onload = function () {
            iframeDoc = frame.contentWindow.document;

            frame.onload = function () {
                form.container.progress.hide();
            }
        }

        var params = {
            includedToDesigner: true,
            themeName: jsObject.options.theme,
            localizationName: (!jsObject.options.jsMode ? jsObject.options.cultureName : jsObject.loc["@cultureName"])
        };

        if (jsObject.options.SessionKey) {
            params.sessionKey = jsObject.options.SessionKey;
        }

        if (reportString) {
            params.reportString = reportString;
        }

        //jsObject.postForm(params, iframeDoc, jsObject.options.publishUrl); //TO DO

        var win = newWindow || this.jsObject.openNewWindow(); //Temporarily
        if (win && win.document) {
            jsObject.postForm(params, win.document, jsObject.options.publishUrl);
        }
    }

    return form;
}