﻿
StiMobileDesigner.prototype.InitializeEditCategoryForm_ = function () {

    //Edit Category Form
    var editCategoryForm = this.BaseForm("editCategoryForm", this.loc.PropertyMain.Category, 3);
    editCategoryForm.category = null;
    editCategoryForm.mode = "Edit";

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px 10px 5px 0";
    editCategoryForm.container.appendChild(innerTable);

    var text = innerTable.addCell();
    text.className = "stiDesignerCaptionControlsBigIntervals";
    text.innerHTML = this.loc.PropertyMain.Category;
    editCategoryForm.categoryControl = this.TextBox("editCategoryFormCategoryControl", 250);
    innerTable.addCell(editCategoryForm.categoryControl).className = "stiDesignerControlCellsBigIntervals";

    editCategoryForm.onshow = function () {
        this.mode = "Edit";
        if (this.category == null) {
            this.category = this.jsObject.CategoryObject();
            this.mode = "New";
        }
        var caption = this.jsObject.loc.FormDictionaryDesigner["Category" + this.mode];
        this.caption.innerHTML = caption;
        this.categoryControl.value = this.category.name;
        this.categoryControl.hideError();
        this.categoryControl.focus();
    }

    editCategoryForm.action = function () {
        this.category.mode = this.mode;

        if (!this.categoryControl.checkNotEmpty(this.jsObject.loc.PropertyMain.Category)) return;
        if ((this.mode == "New" || this.categoryControl.value != this.category.name) &&
            !this.categoryControl.checkExists(this.jsObject.GetVariableCategoriesFromDictionary(this.jsObject.options.report.dictionary), "name")) return;

        if (this.mode == "Edit") this.category.oldName = this.category.name;
        this.category.name = this.categoryControl.value;

        this.changeVisibleState(false);

        if (this.mode == "Edit")
            this.jsObject.SendCommandEditCategory(this.category);
        else
            this.jsObject.SendCommandCreateVariablesCategory(this.category);
    }

    return editCategoryForm;
}