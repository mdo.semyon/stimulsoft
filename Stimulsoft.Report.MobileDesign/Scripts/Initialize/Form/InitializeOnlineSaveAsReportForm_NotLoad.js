﻿
StiMobileDesigner.prototype.InitializeOnlineSaveAsReportForm_ = function () {
    var form = this.BaseForm("onlineSaveAsReport", this.loc.MainMenu.menuFileSaveAs.replace("...", ""), 2);
    var jsObject = this;
       
    //New Folder
    var newFolderButton = jsObject.StandartSmallButton(null, null, this.loc.Cloud.FolderWindowTitleNew, "Folder.png");
    newFolderButton.style.margin = "5px 5px 2px 15px";    
    newFolderButton.style.height = "30px";    
    newFolderButton.style.display = "inline-block";
    newFolderButton.imageCell.style.padding = "0 7px 0 9px";
    newFolderButton.caption.style.padding = "0px 15px 0 0px";
    form.container.style.textAlign = "right";
    form.container.appendChild(newFolderButton);
    form.container.appendChild(this.FormSeparator());
        
    //Online
    var onlineTree = jsObject.CloudTree();
    onlineTree.style.padding = "10px 0 10px 0";
    onlineTree.style.width = "100%";
    onlineTree.style.height = "450px";
    onlineTree.progress = jsObject.AddProgressToControl(form.container);
    form.container.appendChild(onlineTree);
      
    newFolderButton.action = function () {
        this.jsObject.InitializeNewFolderForm(function (newFolderForm) {
            newFolderForm.show(function () {
                if (newFolderForm.nameTextBox.value) {
                    var newFolderKey = newFolderForm.jsObject.generateKey();
                    
                    var param = {};
                    param.AllowSignalsReturn = true;
                    param.Items = [{
                        Ident: "FolderItem",
                        Name: newFolderForm.nameTextBox.value,
                        Description: "",
                        Type: "Common",
                        Key: newFolderKey
                    }];

                    if (onlineTree.rootItem && onlineTree.rootItem.itemObject.Key != "root") {
                        param.Items[0].FolderKey = onlineTree.rootItem.itemObject.Key;
                    }
                         
                    onlineTree.progress.show();
                    newFolderForm.changeVisibleState(false);

                    newFolderForm.jsObject.SendCloudCommand("ItemSave", param,
                        function (data) {
                            onlineTree.progress.hide();                            
                            if (data.ResultItems && data.ResultItems.length > 0) {
                                onlineTree.build(onlineTree.rootItem.itemObject, onlineTree.returnItems[onlineTree.rootItem.itemObject.Key], newFolderKey);
                            }
                        },
                        function (data) {
                            var errorMessageForm = newFolderForm.jsObject.options.forms.errorMessageForm || newFolderForm.jsObject.InitializeErrorMessageForm();
                            errorMessageForm.show(newFolderForm.jsObject.formatResultMsg(data));
                        });
                }
                else {
                    onlineTree.progress.hide();
                    var errorMessageForm = newFolderForm.jsObject.options.forms.errorMessageForm || newFolderForm.jsObject.InitializeErrorMessageForm();
                    errorMessageForm.show(newFolderForm.jsObject.loc.Errors.FieldRequire.replace("{0}", newFolderForm.jsObject.loc.PropertyMain.Name));
                }
            })
        });
    }

    //Name Control
    var nameTable = this.CreateHTMLTable();
    var nameLabel = nameTable.addTextCell(this.loc.PropertyMain.Name + ":");
    nameLabel.className = "stiDesignerCaptionControls";
    var nameControl = jsObject.TextBox(null, 600);
    nameTable.addCell(nameControl);
    nameTable.style.margin = "15px 15px 15px 10px";

    form.buttonOk.caption.innerHTML = this.loc.A_WebViewer.SaveReport;
    form.buttonOk.style.margin = "0 0 15px 0";
    form.buttonCancel.style.margin = "0 15px 15px 15px";
    form.insertBefore(nameTable, form.buttonsPanel);

    onlineTree.action = function (item) {
        //this.itemObjectForReplaced = null;
        if (item.itemObject.Ident == "ReportTemplateItem") {
            //this.itemObjectForReplaced = item.itemObject;
            nameControl.value = item.itemObject.Name;
        }
    }

    onlineTree.ondblClickAction = function (item) {
        if (item.itemObject.Ident == "ReportTemplateItem") {
            form.action();
        }
    }

    onlineTree.onbuildcomplete = function (item) {
        form.buttonOk.setEnabled(true);
    }

    onlineTree.checkExistItem = function (itemName, itemIdent) {
        for (var i = 0; i < this.innerContainer.childNodes.length; i++) {
            var item = this.innerContainer.childNodes[i];
            if (item.itemObject && item.itemObject.Ident == itemIdent && item.itemObject.Name == itemName) {
                return item.itemObject;
            }
        }
        return false;
    }

    form.onhide = function () {
        this.jsObject.options.buttons.onlineItemsSaveAs.setSelected(false);
    }

    form.show = function (fileName, nextFunc) {
        this.changeVisibleState(true);
        nameControl.value = fileName;
        nameControl.focus();
        form.buttonOk.setEnabled(false);
        form.nextFunc = nextFunc;
        onlineTree.build();
    }

    form.action = function () {
        var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
        var newItem = onlineTree.checkExistItem(nameControl.value, "ReportTemplateItem");
        if (newItem) {
            //Replase existing online item
            var messageReplaceForm = jsObject.MessageFormForReplaceItem(nameControl.value);
            messageReplaceForm.changeVisibleState(true);
            messageReplaceForm.action = function (state) {
                if (state) {
                    form.changeVisibleState(false);
                    fileMenu.changeVisibleState(false);
                    setTimeout(function () {
                        jsObject.options.cloudParameters.reportTemplateItemKey = newItem.Key;
                        jsObject.options.cloudParameters.reportName = nameControl.value;
                        jsObject.SetWindowTitle(nameControl.value + " - " + jsObject.loc.FormDesigner.title);
                        jsObject.SendCommandItemResourceSave(newItem.Key);
                        if (form.nextFunc) form.nextFunc();
                    }, 200);
                }
            }
        }
        else {
            form.changeVisibleState(false);
            fileMenu.changeVisibleState(false);
            setTimeout(function () {
                //Create new online item
                var folderKey = onlineTree.rootItem && onlineTree.rootItem.itemObject.Key != "root" ? onlineTree.rootItem.itemObject.Key : null;
                jsObject.AddNewReportItemToCloud(nameControl.value, folderKey);
                if (form.nextFunc) form.nextFunc();
            }, 200);
        }        
    }

    return form;
}