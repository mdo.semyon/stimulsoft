﻿
StiMobileDesigner.prototype.FormButton = function (form, name, caption, imageName) {
    var button = this.SmallButton(name, null, caption, imageName, null, null, this.GetStyles("FormButton"), true);
    button.innerTable.style.width = "100%";
    button.style.minWidth = "80px";
    button.caption.style.textAlign = "center";
    button.form = form;

    return button;
}