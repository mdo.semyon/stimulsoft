﻿
StiMobileDesigner.prototype.InitializeViewDataForm_ = function () {
    var viewDataForm = this.BaseForm("viewDataForm", this.loc.FormDictionaryDesigner.ViewData, 4);
    viewDataForm.buttonCancel.style.display = "none";
    viewDataForm.container.style.maxWidth = "900px";
    viewDataForm.container.style.minWidth = "500px";
    viewDataForm.container.style.height = "500px";
    viewDataForm.container.style.overflowY = "hidden";
    viewDataForm.container.style.overflowX = "auto";

    viewDataForm.show = function (data, dataSourceName) {
        viewDataForm.caption.innerHTML = viewDataForm.jsObject.loc.FormDictionaryDesigner.ViewData + " - " + dataSourceName;

        while (this.container.childNodes[0]) this.container.removeChild(this.container.childNodes[0]);
        if (!data) {
            this.changeVisibleState(true);
            return;
        }

        var dataTableHead = this.jsObject.CreateHTMLTable();
        var tableHeadRow = dataTableHead.tr[0];
        this.container.appendChild(dataTableHead);

        var scrollContainer = document.createElement("div");
        scrollContainer.style.overflowY = "auto";
        scrollContainer.style.overflowX = "hidden";
        scrollContainer.style.height = "calc(100% - 20px)";
        this.container.appendChild(scrollContainer);

        var dataTable = this.jsObject.CreateHTMLTable();
        var tableBodyRow = dataTable.tr[0];
        scrollContainer.appendChild(dataTable);

        var updateCellsWidth = function () {
            if (tableHeadRow.childNodes.length == 0 ||
                tableBodyRow.childNodes.length == 0 ||
                tableHeadRow.childNodes.length != tableBodyRow.childNodes.length)
                return;

            for (i = 0; i < tableBodyRow.childNodes.length; i++) {
                var width = Math.max(tableBodyRow.childNodes[i].offsetWidth, tableHeadRow.childNodes[i].offsetWidth);
                if (width > 500) width = Math.min(tableBodyRow.childNodes[i].offsetWidth, tableHeadRow.childNodes[i].offsetWidth);
                if (width > 500) width = 500;
                var divHeader = document.createElement("div");
                var divBody = document.createElement("div");
                divHeader.style.width = divBody.style.width = width + "px";
                divHeader.style.overflow = "hidden";
                divBody.style.overflow = "hidden";
                divHeader.innerHTML = tableHeadRow.childNodes[i].innerHTML;
                divBody.innerHTML = tableBodyRow.childNodes[i].innerHTML;
                tableHeadRow.childNodes[i].innerHTML = tableBodyRow.childNodes[i].innerHTML = "";
                tableHeadRow.childNodes[i].appendChild(divHeader);
                tableBodyRow.childNodes[i].appendChild(divBody);
            }

            if (dataTableHead.offsetWidth < 450) {
                scrollContainer.style.width = dataTableHead.style.width = dataTable.style.width = "600px";
            }
            else {
                scrollContainer.style.width = (dataTableHead.offsetWidth) + "px";
            }
        }

        var cuttingCells = {};

        if (data.length > 1) {
            var imgColumns = {};

            for (var i = 0; i < data[0].length; i++) {
                var headCell = dataTableHead.addTextCell(data[0][i]);
                headCell.style.fontWeight = "bold";
                headCell.style.textAlign = "center";
                headCell.className = "stiDesignerViewDataTableCell";
            }

            for (var i = 1; i < data.length; i++) {
                for (var k = 0; k < data[i].length; k++) {
                    var cell = dataTable.addCellInLastRow();
                    cell.className = "stiDesignerViewDataTableCell";

                    if (data[i][k].type == "Image") {
                        var img = document.createElement("img");
                        img.style.maxWidth = "60px";
                        img.style.maxHeight = "60px";
                        img.src = data[i][k].value;
                        cell.style.width = "60px";
                        cell.style.height = "60px";
                        cell.style.textAlign = "center";
                        cell.appendChild(img);
                    }
                    else {
                        if (data[i][k].value.length > 150 || cuttingCells[k] != null) {
                            cuttingCells[k] = true;
                            var div = document.createElement("div");
                            div.style.width = "250px";
                            div.style.overflow = "hidden";
                            div.style.textOverflow = "ellipsis";
                            div.innerHTML = data[i][k].value.substring(0, 150);
                            cell.appendChild(div);
                            cell.style.width = "250px";
                        }
                        else {
                            cell.innerHTML = data[i][k].value;
                        }
                    }
                }
                dataTable.addRow();
            }
        }

        try {
            var win = this.jsObject.openNewWindow();

            if (win && win.document) {
                this.changeVisibleState(true);
                updateCellsWidth();

                win.document.write("<script>document.title = '" + viewDataForm.jsObject.loc.FormDictionaryDesigner.ViewData + " - " + dataSourceName +
                    "';</script><style> .stiDesignerViewDataTableCell { font-family: Arial; font-size: 12px; padding: 4px 15px 4px 4px; color: #444444;" +
                    "border-bottom: 1px dotted #c6c6c6; border-right: 1px dotted #c6c6c6; } </style>");
                win.document.write(this.container.innerHTML);
                var body = win.document.getElementsByTagName("body")[0];
                body.style.overflow = "hidden";
                body.style.overflowY = "hidden";
                body.style.overflowX = "auto";

                this.changeVisibleState(false);
            }
            else {
                this.changeVisibleState(true);
                updateCellsWidth();
                return;
            }
        }
        catch (e) {
            this.changeVisibleState(true);
            updateCellsWidth();
        }

        this.jsObject.SetObjectToCenter(this);
    }

    viewDataForm.action = function () {
        this.changeVisibleState(false);
    }

    return viewDataForm;
}