﻿
StiMobileDesigner.prototype.InitializeMessageForm = function () {
    var messageForm = this.BaseForm("messageForm", " ", 4);
    messageForm.caption.style.textAlign = "center";    
    messageForm.container.style.fontSize = "14px";
    messageForm.container.style.fontFamily = "Arial";
    messageForm.container.style.padding = "20px 120px 20px 20px";
    messageForm.style.maxWidth = "600px";
    messageForm.style.minWidth = "500px";
    messageForm.isMessageForm = true;

    messageForm.onshow = function () { this.container.innerHTML = this.messageText; }

    //Override 
    while (messageForm.buttonsPanel.childNodes[0]) {
        messageForm.buttonsPanel.removeChild(messageForm.buttonsPanel.childNodes[0]);
    }

    var buttonsTable = this.CreateHTMLTable();
    messageForm.buttonsPanel.appendChild(buttonsTable);

    //Yes
    messageForm.buttonYes = this.FormButton(messageForm, name + "ButtonYes", this.loc.FormFormatEditor.nameYes, null);
    messageForm.buttonYes.action = function () {
        this.form.changeVisibleState(false);
        this.form.action(true);
    };
    buttonsTable.addCell(messageForm.buttonYes).style.padding = "8px 0px 8px 8px";

    //No
    messageForm.buttonNo = this.FormButton(messageForm, name + "ButtonNo", this.loc.FormFormatEditor.nameNo, null);
    messageForm.buttonNo.action = function () {
        this.form.changeVisibleState(false);
        this.form.action(false);
    };
    buttonsTable.addCell(messageForm.buttonNo).style.padding = "8px";

    //Cancel
    messageForm.buttonCancel = this.FormButton(messageForm, name + "ButtonCancel", this.loc.Buttons.Cancel.replace("&", ""), null);
    messageForm.buttonCancel.style.margin = "8px 8px 8px 0px";
    messageForm.buttonCancel.action = function () {
        this.form.changeVisibleState(false);
    };
    buttonsTable.addCell(messageForm.buttonCancel);

    return messageForm;
}

StiMobileDesigner.prototype.MessageFormForSave = function () {
    var messageForm = this.options.forms.messageForm || this.InitializeMessageForm();

    messageForm.messageText = this.loc.Questions.qnSaveChanges.replace("{0}",
        this.options.report.properties.reportFile || Base64.decode(this.options.report.properties.reportName.replace("Base64Code;", "")));
    messageForm.caption.innerHTML = this.loc.FormDesigner.title.toUpperCase();
    messageForm.buttonCancel.style.display = "";
    //messageForm.container.style.minWidth = "300px";

    return messageForm;
}

StiMobileDesigner.prototype.MessageFormForDelete = function () {
    var messageForm = this.options.forms.messageForm || this.InitializeMessageForm();

    messageForm.messageText = this.loc.Questions.qnRemove;
    messageForm.caption.innerHTML = this.loc.FormDesigner.title.toUpperCase();
    messageForm.buttonCancel.style.display = "none";
    //messageForm.container.style.minWidth = "";

    return messageForm;
}

StiMobileDesigner.prototype.MessageFormForReplaceItem = function (itemName) {
    var messageForm = this.options.forms.messageForm || this.InitializeMessageForm();

    messageForm.messageText = this.loc.Questions.qnReplace.replace("{0}", "\"" + itemName + "\"");
    messageForm.caption.innerHTML = this.loc.FormDesigner.title.toUpperCase();
    messageForm.buttonCancel.style.display = "";
    //messageForm.container.style.minWidth = "450px";

    return messageForm;
}

StiMobileDesigner.prototype.InitializeErrorMessageForm = function () {
    var form = this.BaseForm("errorMessageForm", this.loc.Errors.Error, 4);
    form.container.style.borderTop = "0px";
    form.buttonCancel.style.display = "none";
    form.caption.style.textAlign = "center";
    form.container.style.fontSize = "14px";
    form.container.style.fontFamily = "Arial";

    var table = this.CreateHTMLTable();
    form.container.appendChild(table);

    form.image = document.createElement("img");
    form.image.style.padding = "15px";
    form.image.src = this.options.images["ReportChecker.Error32.png"];
    table.addCellInLastRow(form.image);

    form.description = table.addCellInLastRow();
    form.description.className = "stiDesignerMessagesFormDescription";
    //form.description.style.maxWidth = "600px";

    form.show = function (messageText, infoType) {
        if (this.jsObject.options.ignoreAllErrors) return;
        if (this.visible && this.jsObject.options.jsMode) {
            this.description.innerHTML += "<br/><br/>" + messageText;
            this.jsObject.SetObjectToCenter(this);
            return;
        }
        if (this.jsObject.options.forms.errorMessageForm) { //Fixed Bug
            this.jsObject.options.mainPanel.removeChild(this.jsObject.options.forms.errorMessageForm);
            this.jsObject.options.mainPanel.appendChild(this.jsObject.options.forms.errorMessageForm);
        }
        this.image.src = infoType ? this.jsObject.options.images["ReportChecker.Information32.png"] : this.jsObject.options.images["ReportChecker.Error32.png"];
        this.caption.innerHTML = infoType ? this.jsObject.loc.FormDesigner.title : this.jsObject.loc.Errors.Error
        this.changeVisibleState(true);
        this.description.innerHTML = messageText;
        var processImage = this.jsObject.options.processImage || this.jsObject.InitializeProcessImage();
        processImage.hide();
    }

    form.action = function () {
        this.changeVisibleState(false);
        if (this.onAction) {
            this.onAction();
            this.onAction = null;
        }
    }

    return form;
}