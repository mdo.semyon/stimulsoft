﻿
StiMobileDesigner.prototype.InitializeEditGaugeForm_ = function () {
    //Edit Chart Form
    var editGaugeForm = this.BaseFormPanel("editGauge", " ", 1);

    //Chart Image
    editGaugeForm.gaugeImage = document.createElement("div");
    editGaugeForm.gaugeImage.className = "stiDesignerChartImage";

    editGaugeForm.gaugeImage.update = function () {
        this.innerHTML = editGaugeForm.gaugeProperties.gaugeImage;
    }

    //Tabs
    var tabs = [];
    tabs.push({ "name": "Gauge", "caption": this.loc.Components.StiGauge });
    tabs.push({ "name": "Styles", "caption": this.loc.PropertyMain.Styles });
    var tabbedPane = this.TabbedPane("editGaugeTabbedPane", tabs, this.GetStyles("StandartTab"));
    editGaugeForm.tabbedPane = tabbedPane;
    editGaugeForm.container.appendChild(tabbedPane);

    for (var i in tabs) {
        tabbedPane.tabsPanels[tabs[i].name].appendChild(this.FormSeparator());
        var tabsPanel = null;
        switch (tabs[i].name) {
            case "Gauge": tabsPanel = this.GaugeFormGaugeTabPanel(editGaugeForm); break;
            case "Styles": tabsPanel = this.GaugeFormStylesTabPanel(editGaugeForm); break;
        }

        if (tabsPanel) {
            tabsPanel.style.width = "750px";
            tabsPanel.style.height = "460px";
            tabbedPane.tabsPanels[tabs[i].name].appendChild(tabsPanel);
        }

        //Methods Tabs On Show Event
        tabbedPane.tabsPanels[tabs[i].name].onshow = function () {
            //Move Chart Image To Current Panel
            var imageContainer = editGaugeForm["imageContainer" + this.name + "Tab"];
            if (imageContainer) {
                imageContainer.appendChild(editGaugeForm.gaugeImage);
            }

            //if (this.name == "Styles") {
            //    if (editGaugeForm.chartProperties.area.type != editGaugeForm.lastAreaTypeForStyles) {
            //        editGaugeForm.stylesContainer.stylesProgress.style.display = "";
            //        editGaugeForm.stylesContainer.clear();
            //        editGaugeForm.jsObject.SendCommandGetStylesContent({ componentName: editGaugeForm.chartProperties.name });
            //    }
            //}
        }
    }

    //Hide Header Caption
    editGaugeForm.caption.innerHTML = "";
    editGaugeForm.caption.appendChild(tabbedPane.tabsPanel);
    editGaugeForm.caption.style.padding = "0px";
    editGaugeForm.container.style.borderTop = "0px";

    //Form Methods
    editGaugeForm.recieveCommandResult = function (result) {
        editGaugeForm.gaugeProperties.scales = result.scales;
        editGaugeForm.gaugeProperties.gaugeImage = result.gaugeImage;

        switch (result.command) {
            case "AddGaugeItem": {
                editGaugeForm.gaugeTree.update();
                var selectingItem = editGaugeForm.gaugeTree.getItemByIndexes(result.scaleIndex, result.elementIndex, result.childElementIndex);
                if (selectingItem) selectingItem.setSelected();
                break;
            }
            case "RemoveGaugeItem": {
                var selectingItem = editGaugeForm.gaugeTree.getItemByIndexes(result.itemIndexes.scaleIndex, result.itemIndexes.elementIndex, result.itemIndexes.childElementIndex);
                if (selectingItem) selectingItem.remove();
                break;
            }
        }
        editGaugeForm.gaugeImage.update();
    }

    editGaugeForm.onshow = function () {
        editGaugeForm.jsObject.options.propertiesPanel.setEditGaugeMode(true);
        editGaugeForm.gaugeTree.update();
        editGaugeForm.gaugeImage.update();
        tabbedPane.showTabPanel("Gauge");
    }

    editGaugeForm.onhide = function () {
        editGaugeForm.jsObject.options.propertiesPanel.setEditGaugeMode(false);
    }

    editGaugeForm.cancelAction = function () {
        editGaugeForm.jsObject.SendCommandCanceledEditComponent(editGaugeForm.gaugeProperties.name);
    }

    editGaugeForm.action = function () {
        editGaugeForm.changeVisibleState(false);
        //editGaugeForm.jsObject.SendCommandSendProperties(editGaugeForm.currentGaugeComponent, []);
    }

    return editGaugeForm;
}

//Series Tab
StiMobileDesigner.prototype.GaugeFormGaugeTabPanel = function (editGaugeForm) {
    var mainTable = this.CreateHTMLTable();

    //Toolbar
    var buttons = [
        ["addGaugeItem", this.loc.Gauge.AddNewItem, "Gauge.GaugeNew.png", null, "Down"],
        ["removeGaugeItem", null, "Remove.png", this.loc.Buttons.Delete, null],
        ["separator"],
        ["moveUp", null, "MoveUp.png", this.loc.QueryBuilder.MoveUp, null],
        ["moveDown", null, "MoveDown.png", this.loc.QueryBuilder.MoveDown, null]
    ]

    var toolBar = this.CreateHTMLTable();
    toolBar.style.margin = "4px";
    editGaugeForm.gaugeToolBar = toolBar;
    var toolBarCell = mainTable.addCell(toolBar);
    editGaugeForm.toolBarCell = toolBarCell;
    toolBarCell.className = "stiDesignerChartFormToolBarCell";
    toolBarCell.setAttribute("colspan", "2");

    for (var i in buttons) {
        if (buttons[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton("editGaugeForm" + buttons[i][0], null, buttons[i][1], buttons[i][2], buttons[i][3], buttons[i][4], this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "1px";
        toolBar[buttons[i][0]] = button;
        toolBar.addCell(button);
    }

    //Add Gauge Items Menu
    var addGaugeItemsMenu = this.InitializeAddGaugeItemsMenu(editGaugeForm);
    addGaugeItemsMenu.innerContent.style.maxHeight = null;
    toolBar.addGaugeItem.action = function () { addGaugeItemsMenu.changeVisibleState(!this.isSelected); }

    addGaugeItemsMenu.action = function (menuItem) {
        if (menuItem.haveSubMenu) return;
        this.changeVisibleState(false);
        var itemBaseType = menuItem.name.indexOf("Scale") == 0 ? "Scale" : (menuItem.name.indexOf("Element") == 0 ? "Element" : "ChildElement");

        this.jsObject.SendCommandUpdateGaugeComponent(
            editGaugeForm.gaugeProperties.name, {
                command: "AddGaugeItem",
                itemType: menuItem.key,
                itemBaseType: itemBaseType,
                itemIndexes: editGaugeForm.gaugeTree.getSelectedItemIndexes()
            }
        );
    }

    //Remove Series
    toolBar.removeGaugeItem.action = function () {
        this.jsObject.SendCommandUpdateGaugeComponent(
            editGaugeForm.gaugeProperties.name, {
                command: "RemoveGaugeItem",
                itemIndexes: editGaugeForm.gaugeTree.getSelectedItemIndexes()
            }
        );
    }

    var gaugeItemMove = function (direction) {
        //editGaugeForm.jsObject.SendCommandSeriesMove({
        //    componentName: editGaugeForm.gaugeProperties.name,
        //    seriesIndex: editGaugeForm.seriesContainer.getSelectedIndex(),
        //    direction: direction
        //});
    }

    //Scales Move Up
    toolBar.moveUp.action = function () { gaugeItemMove("Up"); }

    //Scales Move Down
    toolBar.moveDown.action = function () { gaugeItemMove("Down"); }

    //Scales Container
    var gaugeTree = this.GaugeTree(editGaugeForm, 300, this.options.isTouchDevice ? 420 : 425);
    gaugeTree.className = "stiDesignerSeriesContainer";
    editGaugeForm.gaugeTree = gaugeTree;
    mainTable.addCellInNextRow(gaugeTree).style.width = "1px";

    //gaugeTree.onChange = function () {
    //    //editGaugeForm.seriesPropertiesContainer.update();
    //    //toolBar.removeSeries.setEnabled(seriesContainer.items.length > 0);
    //    //var selectedIndex = seriesContainer.getSelectedIndex();
    //    //toolBar.moveUp.setEnabled(selectedIndex != -1 && selectedIndex > 0);
    //    //toolBar.moveDown.setEnabled(selectedIndex != -1 && selectedIndex < seriesContainer.items.length - 1);
    //}

    //gaugeTree.update = function (notSelectedAfter) {
    //    //seriesContainer.clear();
    //    //for (var i in editGaugeForm.gaugeProperties.series) {
    //    //    seriesContainer.addItemAndNotAction(editGaugeForm.gaugeProperties.series[i].name, editGaugeForm.gaugeProperties.series[i]);
    //    //}
    //    //if (seriesContainer.items.length > 0 && !notSelectedAfter) seriesContainer.items[0].action();
    //}

    //Chart image container
    editGaugeForm.imageContainerGaugeTab = mainTable.addCellInLastRow();
    editGaugeForm.imageContainerGaugeTab.style.textAlign = "center";

    return mainTable;
}

//Styles Tab
StiMobileDesigner.prototype.GaugeFormStylesTabPanel = function (editGaugeForm) {
    var mainTable = this.CreateHTMLTable();

    //Toolbar
    editGaugeForm.stylesToolBar = this.CreateHTMLTable();
    var toolBarCell = mainTable.addCell(editGaugeForm.stylesToolBar);
    toolBarCell.className = "stiDesignerChartFormToolBarCell";
    toolBarCell.setAttribute("colspan", "2");

    //AddStyle Button
    editGaugeForm.stylesToolBar.addStyle = this.StandartSmallButton("editGaugeFormAddStyle", null, this.loc.Toolbars.StyleDesigner, "Styles.StiChartStyle.png");
    editGaugeForm.stylesToolBar.addStyle.style.margin = "5px";
    editGaugeForm.stylesToolBar.addCell(editGaugeForm.stylesToolBar.addStyle);
    editGaugeForm.stylesToolBar.addStyle.action = function () {
        editGaugeForm.jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
            styleDesignerForm.changeVisibleState(true);
        });
    };

    //Styles Container
    var stylesProgress = this.Progress();
    editGaugeForm.stylesContainer = this.GaugeStylesContainer(editGaugeForm);
    stylesProgress.className = "stiDesignerChartFormProgress";
    stylesProgress.style.display = "none";

    var containerCell = mainTable.addCellInNextRow();
    containerCell.style.width = "1px";
    containerCell.appendChild(stylesProgress);
    containerCell.appendChild(editGaugeForm.stylesContainer);
    editGaugeForm.stylesContainer.stylesProgress = stylesProgress;

    //Chart image container
    editGaugeForm.imageContainerStylesTab = mainTable.addCellInLastRow();
    editGaugeForm.imageContainerStylesTab.style.textAlign = "center";

    return mainTable;
}

//Gauge Tree
StiMobileDesigner.prototype.GaugeTree = function (editGaugeForm, width, height) {
    var tree = this.Tree(width, height);

    tree.update = function () {
        tree.build(editGaugeForm.gaugeProperties.scales);
    }

    tree.build = function (scales) {
        this.clear();
        this.mainItem = this.jsObject.TreeItem(editGaugeForm.gaugeProperties.name, "Gauge.Small.StiGauge.png", { typeItem: "mainItem" }, this, null, "mainItem");
        this.appendChild(this.mainItem);
        this.mainItem.style.margin = "8px";
        this.mainItem.setOpening(true);
        this.mainItem.setSelected();

        var buildTreeSection = function (parentItem, collection) {
            for (var i = 0; i < collection.length; i++) {
                var item = tree.jsObject.TreeItem(collection[i].text, "Gauge.Small." + (collection[i].iconType || collection[i].type.substring(3)) + ".png", collection[i], tree, null);
                parentItem.addChild(item);
                item.setOpening(true);

                if (collection[i].typeItem == "ChildElementItem") {
                    item.itemObject.childElementIndex = i;
                    item.itemObject.elementIndex = item.parent.itemObject.elementIndex;
                    item.itemObject.scaleIndex = item.parent.itemObject.scaleIndex;
                }
                else if (collection[i].typeItem == "ElementItem") {
                    item.itemObject.scaleIndex = item.parent.itemObject.scaleIndex;
                }

                if (collection[i].elements) buildTreeSection(item, collection[i].elements);
                if (collection[i].tickCustomValues) buildTreeSection(item, collection[i].tickCustomValues);
                if (collection[i].linearRanges) buildTreeSection(item, collection[i].linearRanges);
                if (collection[i].radialRanges) buildTreeSection(item, collection[i].radialRanges);
                if (collection[i].barRanges) buildTreeSection(item, collection[i].barRanges);
                if (collection[i].indicatorStateFilters) buildTreeSection(item, collection[i].indicatorStateFilters);

                item.getIndex = function () {
                    return (item.parent ? Array.prototype.indexOf.call(item.parent.childsContainer.childNodes, this) : null);
                }
            }
        }

        buildTreeSection(this.mainItem, scales);
    }

    tree.getItemByIndexes = function (scaleIndex, elementIndex, childElementIndex) {
        if (tree.mainItem && scaleIndex != null) {
            var scaleItem = tree.mainItem.childsContainer.childNodes[scaleIndex];
            if (elementIndex != null) {
                var elementItem = scaleItem.childsContainer.childNodes[elementIndex];
                if (childElementIndex != null) {
                    return elementItem.childsContainer.childNodes[childElementIndex];
                }
                else {
                    return elementItem;
                }
            }
            else {
                return scaleItem;
            }
        }

        return null;
    }

    tree.getSelectedItemIndexes = function () {
        var indexes = {};

        if (this.selectedItem && this.selectedItem != this.mainItem) {
            if (this.selectedItem.itemObject.typeItem == "ChildElementItem") {
                indexes.childElementIndex = this.selectedItem.getIndex();
                indexes.elementIndex = this.selectedItem.parent.getIndex();
                indexes.scaleIndex = this.selectedItem.parent.parent.getIndex();
            }
            else if (this.selectedItem.itemObject.typeItem == "ElementItem") {
                indexes.elementIndex = this.selectedItem.getIndex();
                indexes.scaleIndex = this.selectedItem.parent.getIndex();
            }
            else if (this.selectedItem.itemObject.typeItem == "ScaleItem") {
                indexes.scaleIndex = this.selectedItem.getIndex();
            }
        }

        return indexes;
    }

    tree.onSelectedItem = function (item) {
        //debugger;
    }

    return tree;
}

//Styles Container
StiMobileDesigner.prototype.GaugeStylesContainer = function (editGaugeForm) {
    var stylesContainer = this.ContainerWithBigItems("editGaugeFormStylesContainer", 200, this.options.isTouchDevice ? 420 : 425, 80);
    stylesContainer.className = "stiDesignerSeriesContainer";
    stylesContainer.buttons = {};

    stylesContainer.onAction = function () {
        if (stylesContainer.selectedItem != null) {
            var params = {
                componentName: editGaugeForm.gaugeProperties.name,
                styleType: stylesContainer.selectedItem.itemObject.type,
                styleName: stylesContainer.selectedItem.itemObject.name
            }
            editGaugeForm.jsObject.SendCommandSetChartStyle(params);
        }
    }

    stylesContainer.update = function (stylesContent) {
        editGaugeForm.lastAreaTypeForStyles = editGaugeForm.gaugeProperties.area.type;
        stylesContainer.clear();
        for (var i in stylesContent) {
            var name = stylesContent[i].type + stylesContent[i].name;
            var button = stylesContainer.addItemAndNotAction(name, null, " ", stylesContent[i]);
            button.cellImage.removeChild(button.image);
            button.image = document.createElement("div");
            button.image.innerHTML = stylesContent[i].image;
            button.cellImage.appendChild(button.image);
            stylesContainer.buttons[name] = button;

            if (editGaugeForm.gaugeProperties.style.type + editGaugeForm.gaugeProperties.style.name == name) {
                button.selected();
            }
            button.style.display = "inline-block";
        }
        editGaugeForm.stylesContainer.stylesProgress.style.display = "none";
    }

    return stylesContainer;
}