﻿
StiMobileDesigner.prototype.DictionaryTree = function () {
    var dictionaryTree = this.Tree();
    dictionaryTree.style.margin = "10px";
    this.options.dictionaryTree = dictionaryTree;

    dictionaryTree.clear = function () {
        if (this.mainItems) {
            var mainItemsNames = ["DataSources", "BusinessObjects", "Variables", "Resources", "Images", "RichTexts", "SubReports"];
            for (var i = 0; i < mainItemsNames.length; i++) {
                if (this.mainItems[mainItemsNames[i]]) {
                    this.removeChild(this.mainItems[mainItemsNames[i]]);
                    this.mainItems[mainItemsNames[i]] = null;
                }
            }
            this.mainItems["SystemVariables"].style.display = "none";
            this.mainItems["Functions"].style.display = "none";
        }
        this.items = {};
    }

    dictionaryTree.build = function (dictionary, buildOnlyOneLevel) {
        if (dictionary == null) return;
        this.clear();

        if (!this.mainItems){
            this.mainItems = {};
            this.mainItems["SystemVariables"] = this.jsObject.DictionaryTreeItem(this.jsObject.loc.PropertyMain.SystemVariables, "SystemVariables.png", { name: "SystemVariablesMainItem", typeItem: "SystemVariablesMainItem" });
            this.mainItems["Functions"] = this.jsObject.DictionaryTreeItem(this.jsObject.loc.PropertyMain.Functions, "Function.png", { name: "FunctionsMainItem", typeItem: "FunctionsMainItem" });
            this.appendChild(this.mainItems["SystemVariables"]);
            this.appendChild(this.mainItems["Functions"]);
            this.addSystemVariables(dictionary.systemVariables, this.mainItems["SystemVariables"]);
            this.addFunctions(dictionary.functions, this.mainItems["Functions"]);
        }
        else {
            this.mainItems["SystemVariables"].style.display = "";
            this.mainItems["Functions"].style.display = "";
        }
        
        this.mainItems["Resources"] = this.jsObject.DictionaryTreeItem(this.jsObject.loc.PropertyMain.Resources, "Resources.Resource.png", { name: "ResourcesMainItem", typeItem: "ResourcesMainItem" });
        this.appendChild(this.mainItems["Resources"]);
        this.mainItems["DataSources"] = this.jsObject.DictionaryTreeItem(this.jsObject.loc.PropertyMain.DataSources, "DataSource.png", { name: "DataSourceMainItem", typeItem: "DataSourceMainItem" });
        this.mainItems["BusinessObjects"] = this.jsObject.DictionaryTreeItem(this.jsObject.loc.Report.BusinessObjects, "BusinessObject.png", { name: "BusinessObjectMainItem", typeItem: "BusinessObjectMainItem" });
        this.mainItems["Variables"] = this.jsObject.DictionaryTreeItem(this.jsObject.loc.PropertyMain.Variables, "Variable.png", { name: "VariablesMainItem", typeItem: "VariablesMainItem" });
        
        var mainItemsNames = ["DataSources", "BusinessObjects", "Variables"];
        for (var i = 0; i < mainItemsNames.length; i++) { 
            this.insertBefore(this.mainItems[mainItemsNames[i]], this.mainItems["SystemVariables"]);
        }

        this.addTreeItems(dictionary.databases, this.mainItems["DataSources"], buildOnlyOneLevel);
        this.addTreeItems(dictionary.businessObjects, this.mainItems["BusinessObjects"]);
        this.addTreeItems(dictionary.variables, this.mainItems["Variables"]);
        this.addTreeItems(dictionary.resources, this.mainItems["Resources"]);
        this.mainItems["DataSources"].setSelected();

        if (this.jsObject.options.isJava) {
            this.mainItems.BusinessObjects.style.display = "none";
        }

        if (this.jsObject.options.jsMode) {
            this.mainItems.BusinessObjects.style.display = "none";
        }

        //Cloud Server Mode
        if (dictionary.attachedItems) {
            mainItemsNames = ["Image", "RichText", "SubReport"];
            for (var i = 0; i < mainItemsNames.length; i++) {
                var categoryName = mainItemsNames[i] + "s";
                if (dictionary.attachedItems[mainItemsNames[i]]) {
                    var captionCategory = this.jsObject.options.cloudReportsClient
                        ? this.jsObject.options.cloudReportsClient.loc.ReportTemplate["Category" + categoryName] : categoryName;
                    this.mainItems[categoryName] = this.jsObject.DictionaryTreeItem(captionCategory, "Cloud" + mainItemsNames[i] + ".png", { name: categoryName + "MainItem", typeItem: categoryName + "MainItem" });
                    this.appendChild(this.mainItems[categoryName]);
                    this.addTreeItems(dictionary.attachedItems[mainItemsNames[i]], this.mainItems[categoryName]);
                }
            }
        }
    }

    dictionaryTree.getCloudItemByKey = function (itemType, key) {
        var mainItem = this.mainItems[itemType + "s"];
        if (mainItem) {
            for (var i in mainItem.childs) {
                if (mainItem.childs[i].itemObject.key == key) return mainItem.childs[i];
            }
        }
        return null;
    }

    //Add Tree Items
    dictionaryTree.addTreeItems = function (collection, parentItem, buildOnlyOneLevel) {
        if (collection) {
            //Sort only connections
            if (parentItem == this.mainItems["DataSources"]) {
                collection.sort(this.jsObject.SortByName);
            }

            for (var i = 0; i < collection.length; i++) {
                if (collection[i].typeItem == "Category") {
                    var categoryItem = parentItem.addChild(this.jsObject.DictionaryTreeItemFromObject(this.jsObject.CategoryObject(collection[i].name)));
                    this.addTreeItems(collection[i].categoryItems, categoryItem);
                    continue;
                }

                var childItem = parentItem.addChild(this.jsObject.DictionaryTreeItemFromObject(collection[i]));
                var childCollection = [];

                switch (collection[i].typeItem) {
                    case "DataBase": { childCollection = collection[i].dataSources; break; }
                    case "DataSource": { childCollection = collection[i].relations; break; }
                    case "Relation": { childCollection = collection[i].relations; break; }
                    case "BusinessObject":
                        {
                            childItem.itemObject.fullName = childItem.getBusinessObjectStringFullName();
                            childCollection = collection[i].businessObjects;
                            break;
                        }
                }

                if (buildOnlyOneLevel) {
                    var showOpenIcon = childCollection.length > 0;
                    
                    if (!showOpenIcon && childItem.itemObject.typeItem == "Relation") {
                        var dataSource = this.jsObject.GetDataSourceByNameFromDictionary(childItem.itemObject.parentDataSource);
                        if (dataSource && ((dataSource.columns && dataSource.columns.length > 0) ||(dataSource.parameters && dataSource.parameters.length > 0)))  {
                            showOpenIcon = true;
                        }
                    }                    
                    
                    if (!showOpenIcon &&
                        ((childItem.itemObject.columns && childItem.itemObject.columns.length > 0) ||
                        (childItem.itemObject.parameters && childItem.itemObject.parameters.length > 0)))
                    {
                        showOpenIcon = true;
                    }
                    
                    if (childItem.iconOpening && showOpenIcon) {
                        childItem.iconOpening.style.visibility = "visible";
                        childItem.buildChildsNotCompleted = true;
                        childItem.childCollection = childCollection;
                    }
                }
                else {
                    this.addTreeItems(childCollection, childItem);
                }
            }

            if (parentItem.itemObject["columns"] || parentItem.itemObject["parameters"]) {
                if (parentItem.itemObject["columns"]) this.addColumns(parentItem, parentItem.itemObject["columns"]);
                if (parentItem.itemObject["parameters"]) this.addParameters(parentItem, parentItem.itemObject["parameters"]);
                return;
            }

            if (parentItem.itemObject.typeItem == "Relation") {
                var dataSource = this.jsObject.GetDataSourceByNameFromDictionary(parentItem.itemObject.parentDataSource);
                if (dataSource) this.addColumns(parentItem, dataSource.columns);
                if (dataSource) this.addParameters(parentItem, dataSource.parameters);
            }
        }
    }
    
    /* --------------- System Variables Tree ---------------------- */
    dictionaryTree.addSystemVariables = function (systemVariables, parentItem) {
        for (var i = 0; i < systemVariables.length; i++) {
            var item = this.jsObject.DictionaryTreeItem(systemVariables[i], "SystemVariable" + systemVariables[i] + ".png",
            {
                typeItem: "SystemVariable", name: systemVariables[i],
                typeIcon: "SystemVariable" + systemVariables[i]
            });
            parentItem.addChild(item);
        }
    }

    /* --------------- Functions Tree ---------------------- */
    dictionaryTree.addFunctions = function (functions, parentItem) {
        for (var i = 0; i < functions.length; i++) {
            if (functions[i].typeItem == "FunctionsCategory") {
                var categoryItem = dictionaryTree.jsObject.DictionaryTreeItem(functions[i].name, functions[i].typeIcon + ".png", functions[i], dictionaryTree);
                parentItem.addChild(categoryItem);
                dictionaryTree.addFunctions(functions[i].items, categoryItem);
            }
            else {
                var functionItem = dictionaryTree.jsObject.DictionaryTreeItem(functions[i].caption, functions[i].typeIcon + ".png", functions[i], dictionaryTree);
                parentItem.addChild(functionItem);
            }
        }
    }

    /* --------------- Variables Tree ---------------------- */
    dictionaryTree.addVariable = function (variableObject) {
        var parentItem = this.getCurrentVariableParent();
        var variableItem = this.jsObject.DictionaryTreeItemFromObject(variableObject);
        parentItem.addChild(variableItem);
        variableItem.setSelected();
        variableItem.openTree();
    }

    dictionaryTree.editVariable = function (variableObject) {
        this.selectedItem.itemObject = variableObject;
        this.selectedItem.repaint();
    }

    dictionaryTree.createVariablesCategory = function (answer) {
        var categories = this.jsObject.options.report.dictionary.variables["Categories"];
        var canCreate = true;
        if (categories)
            for (var nameCategory in categories)
                if (answer.name == nameCategory) canCreate = false;

        if (canCreate) {
            var categoryItem = this.jsObject.DictionaryTreeItemFromObject(this.jsObject.CategoryObject(answer.name));
            this.mainItems["Variables"].addChild(categoryItem);
            categoryItem.setSelected();
            categoryItem.openTree();
        }
    }

    /* --------------- Resources Tree ---------------------- */
    dictionaryTree.addResource = function (resourceObject) {
        var resourceItem = this.jsObject.DictionaryTreeItemFromObject(resourceObject);
        this.mainItems["Resources"].addChild(resourceItem);
        resourceItem.setSelected();
        resourceItem.openTree();
    }

    dictionaryTree.editResource = function (resourceObject) {
        this.selectedItem.itemObject = resourceObject;
        this.selectedItem.repaint();
    }

    /* --------------- BusinessObject Tree ---------------------- */

    dictionaryTree.deleteBusinessObject = function () {
        if (!this.selectedItem) return;
        var businessObjectParentItem = this.selectedItem.parent;
        this.selectedItem.remove();
        if (this.jsObject.GetCountObjects(businessObjectParentItem.childs) == 0 && businessObjectParentItem.itemObject.typeItem == "Category")
            businessObjectParentItem.remove();
    }

    dictionaryTree.addBusinessObject = function (businessObjectObject, parentBusinessObjectFullName) {
        var parentItem = parentBusinessObjectFullName == null ? this.mainItems["BusinessObjects"] : this.getItemByNameAndType(parentBusinessObjectFullName, "BusinessObject");
        if (!parentItem) return;
        var category = businessObjectObject.category;
        if (category != "") {
            for (var i in parentItem.childs)
                if (parentItem.childs[i].itemObject.typeItem == "Category" && parentItem.childs[i].itemObject.name == category) {
                    parentItem = parentItem.childs[i];
                    break;
                }
            if (parentItem.itemObject.typeItem == "BusinessObjectMainItem") {
                var categoryItem = this.jsObject.DictionaryTreeItemFromObject(this.jsObject.CategoryObject(category));
                parentItem.addChild(categoryItem);
                parentItem = categoryItem;
            }
        }

        var businessObjectItem = this.jsObject.DictionaryTreeItemFromObject(businessObjectObject);

        if (parentBusinessObjectFullName != null) {
            this.removeColumns(parentItem);
            parentItem.addChild(businessObjectItem);
            var parentBusinessObjectInDictionary = this.jsObject.GetBusinessObjectByNameFromDictionary(parentBusinessObjectFullName);
            if (parentBusinessObjectInDictionary) this.addColumns(parentItem, parentBusinessObjectInDictionary.columns);
        }
        else {
            parentItem.addChild(businessObjectItem);
        }
        this.addTreeItems(businessObjectObject.businessObjects, businessObjectItem);
        businessObjectItem.setSelected();
        businessObjectItem.openTree();
    }

    dictionaryTree.editBusinessObject = function (businessObjectNewObject) {
        var businessObjectOldObject = this.selectedItem.itemObject;
        if (businessObjectOldObject.category == businessObjectNewObject.category) {
            this.selectedItem.repaint(businessObjectNewObject);
            this.updateColumns(this.selectedItem, businessObjectNewObject.columns);
        }
        else {
            this.deleteBusinessObject();
            this.addBusinessObject(businessObjectNewObject);
        }
    }

    dictionaryTree.editBusinessObjectCategory = function (answer) {
        var oldCategoryName = answer.oldName;
        var newCategoryName = answer.name;
        var parentItem = this.mainItems["BusinessObjects"];
        var categoryItem = this.getItemByNameAndType(oldCategoryName, "Category");
        if (categoryItem) {
            categoryItem.itemObject.name = newCategoryName;
            categoryItem.repaint();
            for (var i in categoryItem.childs) {
                if (categoryItem.childs[i].itemObject.typeItem == "BusinessObject")
                    categoryItem.childs[i].itemObject.category = newCategoryName;
            }
        }
    }

    /* --------------- DataSources Tree ---------------------- */

    /* Connection */
    dictionaryTree.addConnectionToTree = function (connectionObject) {
        var connectionItem = this.jsObject.DictionaryTreeItemFromObject(connectionObject);
        this.mainItems["DataSources"].addChild(connectionItem);
        connectionItem.setSelected();
        connectionItem.openTree();
    }

    dictionaryTree.setConnectionItemFail = function (connectionItem) {
        connectionItem.itemObject.typeIcon = "ConnectionFail";
        connectionItem.itemObject.nameInSource = connectionItem.itemObject.name;
        connectionItem.repaint();
    }

    dictionaryTree.createOrEditConnection = function (answer) {
        var newConnectionItem = this.mainItems["DataSources"].getChildByName(answer.itemObject.name);
        if (answer.mode == "Edit") {
            var currConnectionItem = this.selectedItem;
            if (newConnectionItem) {
                if (currConnectionItem.itemObject.name != newConnectionItem.itemObject.name) {
                    newConnectionItem.repaint(answer.itemObject);
                    if (this.jsObject.GetCountObjects(currConnectionItem.childs) == 0)
                        currConnectionItem.remove();
                    else
                        this.setConnectionItemFail(currConnectionItem);
                }
                else {
                    newConnectionItem.repaint(answer.itemObject);
                }
            }
            else {                
                currConnectionItem.removeAllChilds();
                var databaseInDictionary = this.jsObject.GetObjectByPropertyValueFromCollection(answer.databases, "name", answer.itemObject.name);
                if (databaseInDictionary) {
                    var newItemObject = this.jsObject.CopyObject(databaseInDictionary);
                    currConnectionItem.repaint(newItemObject);
                    dictionaryTree.addTreeItems(newItemObject.dataSources, currConnectionItem);
                }
            }
        }
        else {
            if (newConnectionItem)
                newConnectionItem.repaint(answer.itemObject);
            else
                this.addConnectionToTree(answer.itemObject);
        }
    }

    /* DataSource */
    dictionaryTree.deleteDataSource = function () {
        var dataBaseItem = this.selectedItem.parent;
        this.selectedItem.remove();
        if (dataBaseItem.itemObject.typeItem == "DataBase" &&
            dataBaseItem.itemObject.typeIcon == "ConnectionFail" &&
            this.jsObject.GetCountObjects(dataBaseItem.childs) == 0)
            dataBaseItem.remove();
    }

    dictionaryTree.addDataSource = function (dataSourceObject) {
        var connectionName = this.getConnectionNameFromNameInSource(dataSourceObject);
        var connectionForDataSource = this.jsObject.options.dictionaryTree.mainItems["DataSources"].getChildByName(connectionName);
        var dataSourceItem = this.jsObject.DictionaryTreeItemFromObject(dataSourceObject);

        if (connectionForDataSource.buildChildsNotCompleted) connectionForDataSource.completeBuildTree(true);

        if (connectionForDataSource) {
            connectionForDataSource.addChild(dataSourceItem);
        }
        else {
            var newConnectionObject = this.jsObject.ConnectionObject();
            newConnectionObject.typeIcon = "ConnectionFail";
            newConnectionObject.name = connectionName;
            newConnectionObject.alias = connectionName;
            newConnectionObject.nameInSource = connectionName;
            var newConnectionItem = this.jsObject.DictionaryTreeItemFromObject(newConnectionObject);
            this.mainItems["DataSources"].addChild(newConnectionItem);
            newConnectionItem.addChild(dataSourceItem);
        }

        this.updateColumns(dataSourceItem, dataSourceObject.columns);
        this.updateParameters(dataSourceItem, dataSourceObject.parameters);
        dataSourceItem.setSelected();
        dataSourceItem.openTree();
    }

    dictionaryTree.editDataSource = function (dataSourceNewObject) {
        if (this.selectedItem.buildChildsNotCompleted) this.selectedItem.completeBuildTree();

        var dataSourceOldObject = this.selectedItem.itemObject;
        if (dataSourceOldObject.nameInSource == dataSourceNewObject.nameInSource) {
            this.selectedItem.repaint(dataSourceNewObject);
            this.updateColumns(this.selectedItem, dataSourceNewObject.columns);
            this.updateParameters(this.selectedItem, dataSourceNewObject.parameters);
        }
        else {
            this.deleteDataSource();
            this.addDataSource(dataSourceNewObject);
        }
    }

    /* Relations */
    dictionaryTree.addRelation = function (relationObject) {
        var mainItemDataSources = this.jsObject.options.dictionaryTree.mainItems["DataSources"];

        for (var childId in mainItemDataSources.childs) {
            if (mainItemDataSources.childs[childId].buildChildsNotCompleted) {
                mainItemDataSources.childs[childId].completeBuildTree(true);
            }
        }

        var childDataSource = this.jsObject.GetDataSourceByNameFromDictionary(relationObject.childDataSource);
        var childDataSourceItem = this.getItemByNameAndType(relationObject.childDataSource, "DataSource");
        var parentDataSourceItem = this.getItemByNameAndType(relationObject.parentDataSource, "DataSource");
        if (!childDataSourceItem || !parentDataSourceItem || !childDataSource) return;
        var selectedItem = null;

        
        for (var i in this.items) {
            if ((this.items[i].itemObject.typeItem == "Relation" && this.items[i].itemObject.parentDataSource == relationObject.childDataSource) || (this.items[i] == childDataSourceItem)) {
                var relationItem = this.jsObject.DictionaryTreeItemFromObject(relationObject);
                if (this.items[i].buildChildsNotCompleted) this.items[i].completeBuildTree();
                this.removeColumns(this.items[i]);
                this.removeParameters(this.items[i]);
                this.items[i].addChild(relationItem);
                if (this.items[i].itemObject.relations) {
                    this.items[i].itemObject.relations.push(relationObject);
                }
                this.addColumns(this.items[i], childDataSource.columns);
                this.addParameters(this.items[i], childDataSource.parameters);
                this.addTreeItems(relationObject.relations, relationItem);
                if (this.items[i] == childDataSourceItem) selectedItem = relationItem;
            }
        }
        if (selectedItem) { selectedItem.setSelected(); selectedItem.openTree(); }
    }

    dictionaryTree.editRelation = function (answer) {
        if (answer.changedChildDataSource) {
            this.selectedItem.remove();
            for (var i in this.items)
                if (this.items[i].itemObject.typeItem == "Relation" && this.items[i].itemObject.nameInSource == answer.oldNameInSource)
                    this.items[i].remove();
            this.addRelation(answer.itemObject);
        }
        else {
            var selectedItem = this.selectedItem;
            var parentDataSource = this.jsObject.GetDataSourceByNameFromDictionary(answer.itemObject.parentDataSource);
            var columns = parentDataSource ? parentDataSource.columns : null;
            var parameters = parentDataSource ? parentDataSource.parameters : null;
            if (selectedItem.buildChildsNotCompleted) selectedItem.completeBuildTree();
            if (!columns && !parameters) return;
            this.selectedItem.repaint(answer.itemObject);
            this.updateColumns(this.selectedItem, columns);
            this.updateParameters(this.selectedItem, parameters);
            for (var i in this.items)
                if (this.items[i].itemObject.typeItem == "Relation" && this.items[i].itemObject.nameInSource == answer.oldNameInSource) {
                    this.items[i].repaint(answer.itemObject);
                    this.updateColumns(this.items[i], columns);
                    this.updateParameters(this.items[i], parameters);
                }
            selectedItem.setSelected();
            selectedItem.openTree();
        }
    }

    dictionaryTree.deleteRelation = function () {
        var parent = this.selectedItem.parent;

        var selectedRelationObject = this.selectedItem.itemObject;
        for (var i in this.items)
            if (this.items[i].itemObject.typeItem == "Relation" &&
                this.items[i].itemObject.nameInSource == selectedRelationObject.nameInSource &&
                this.items[i].itemObject.name == selectedRelationObject.name) {
                var parentItem = this.items[i].parent;
                if (parentItem && parentItem.itemObject.relations) {
                    for (var k = 0; k < parentItem.itemObject.relations.length; k++) {
                        if (selectedRelationObject.nameInSource == parentItem.itemObject.relations[k].nameInSource) {
                            parentItem.itemObject.relations.splice(k, 1);
                        }
                    }
                }
                this.items[i].remove();
            }
    }

    /* Columns */
    dictionaryTree.addColumn = function (answer) {
        var columnObject = answer.itemObject
        var isBusinessObject = answer.currentParentType == "BusinessObject";
        var parentName = answer.currentParentName;
        var selectedItem = null;

        if (!isBusinessObject) {
            var parentDataSource = this.getItemByNameAndType(parentName, "DataSource");
            if (!parentDataSource) return;
            if (parentDataSource.buildChildsNotCompleted) parentDataSource.completeBuildTree();
            for (var i in this.items) {
                if ((this.items[i].itemObject.typeItem == "Relation" && this.items[i].itemObject.parentDataSource == parentName) ||
                    (this.items[i].itemObject.typeItem == "DataSource" && this.items[i].itemObject.name == parentName)) {
                    var columnItem = this.jsObject.DictionaryTreeItemFromObject(columnObject);
                    this.items[i].addChild(columnItem);
                    if (this.items[i].itemObject.columns) this.items[i].itemObject.columns.push(columnObject);
                    if (this.items[i] == parentDataSource) selectedItem = columnItem;
                }
            }
        }
        else {
            var columnItem = this.jsObject.DictionaryTreeItemFromObject(columnObject);
            var parentBusinessObjectItem = this.getItemByNameAndType(parentName, "BusinessObject");
            if (parentBusinessObjectItem) parentBusinessObjectItem.addChild(columnItem);
            if (parentBusinessObjectItem.itemObject.columns) parentBusinessObjectItem.itemObject.columns.push(columnObject);
            selectedItem = columnItem;
        }

        if (selectedItem) {
            selectedItem.setSelected();
            selectedItem.openTree();
        }
    }

    dictionaryTree.editColumn = function (answer) {
        var columnObject = answer.itemObject
        var currentParentType = answer.currentParentType;
        var currentParentName = answer.currentParentName;
        if (currentParentType == "BusinessObject")
            this.selectedItem.repaint(columnObject);
        else {
            var columnName = this.selectedItem.itemObject.name;
            for (var i in this.items)
                if (this.isColumnItemOfDataSource(this.items[i], columnName, currentParentName)) this.items[i].repaint(columnObject);
        }
    }

    dictionaryTree.deleteColumn = function (answer) {
        var currentParentType = answer.currentParentType;
        var currentParentName = answer.currentParentName;
        var columnObject = this.selectedItem.itemObject;

        var deleteColumnObjectFromParent = function (parentItem) {
            if (parentItem && parentItem.itemObject.columns) {
                for (var k = 0; k < parentItem.itemObject.columns.length; k++) {
                    if (parentItem.itemObject.columns[k].name == columnObject.name) {
                        parentItem.itemObject.columns.splice(k, 1);
                    }
                }
            }
        }

        if (currentParentType == "BusinessObject") {
            this.selectedItem.remove();
            deleteColumnObjectFromParent(this.selectedItem.parent);
        }
        else {
            var parent = this.selectedItem.parent;
            var columnObject = this.selectedItem.itemObject;
            for (var key in this.items) {
                if (this.isColumnItemOfDataSource(this.items[key], columnObject.name, currentParentName)) {
                    deleteColumnObjectFromParent(this.items[key].parent);
                    this.items[key].remove();
                }
            }
        }
    }

    /* Parameters */
    dictionaryTree.addParameter = function (answer) {
        var parameterObject = answer.itemObject
        var parentName = answer.currentParentName;
        var selectedItem = null;

        var parentDataSource = this.getItemByNameAndType(parentName, "DataSource");
        if (!parentDataSource) return;
        if (parentDataSource.buildChildsNotCompleted) parentDataSource.completeBuildTree();
        if (parentDataSource.itemObject.parameters) parentDataSource.itemObject.parameters.push(parameterObject);

        for (var i in this.items) {
            var parentItem = this.items[i];
            if ((parentItem.itemObject.typeItem == "Relation" && parentItem.itemObject.parentDataSource == parentName) ||
                (parentItem.itemObject.typeItem == "DataSource" && parentItem.itemObject.name == parentName)) {
                var haveParameters = false;
                for (var key in parentItem.childs) {
                    if (parentItem.childs[key].itemObject.typeItem == "Parameters") {
                        parentItem = parentItem.childs[key];
                        haveParameters = true;
                        break;
                    }
                }
                if (!haveParameters) {
                    var parametersItem = this.jsObject.DictionaryTreeItem(this.jsObject.loc.PropertyMain.Parameters, "Parameter.png", { name: "Parameters", typeItem: "Parameters" }, dictionaryTree);
                    parentItem.addChild(parametersItem);
                    parentItem = parametersItem;
                }

                var parameterItem = this.jsObject.DictionaryTreeItemFromObject(parameterObject);
                parentItem.addChild(parameterItem);
                if (this.items[i] == parentDataSource) selectedItem = parameterItem;
            }
        }

        if (selectedItem) {
            selectedItem.setSelected();
            selectedItem.openTree();
        }
    }

    dictionaryTree.editParameter = function (answer) {
        var parameterObject = answer.itemObject
        var currentParentName = answer.currentParentName;
        var parameterName = this.selectedItem.itemObject.name;
        for (var i in this.items)
            if (this.isParameterItemOfDataSource(this.items[i], parameterName, currentParentName))
                this.items[i].repaint(parameterObject);
    }

    dictionaryTree.deleteParameter = function (answer) {
        var currentParentName = answer.currentParentName;
        var parent = this.selectedItem.parent;
        var mainParent = parent.parent;
        var parameterObject = this.selectedItem.itemObject;

        if (mainParent && mainParent.itemObject.parameters) {
            for (var k = 0; k < mainParent.itemObject.parameters.length; k++) {
                if (mainParent.itemObject.parameters[k].name == parameterObject.name) {
                    mainParent.itemObject.parameters.splice(k, 1);
                }
            }
        }

        for (var i in this.items)
            if (this.isParameterItemOfDataSource(this.items[i], parameterObject.name, currentParentName)) {
                if (this.items[i].parent && this.jsObject.GetCountObjects(this.items[i].parent.childs) == 1) {
                    parent = mainParent;
                    this.items[i].parent.remove();
                }
                else
                    this.items[i].remove();
            }
    }

    /* Helper Methods */
    dictionaryTree.getCurrentColumnParent = function (columnItem) {
        var currItem = columnItem || this.selectedItem;
        if (!currItem) return;
        var parent = { "name": "", "type": "" };

        while (currItem.parent != null) {
            if (currItem.itemObject.typeItem == "DataSource" || currItem.itemObject.typeItem == "BusinessObject") {
                parent.type = currItem.itemObject.typeItem;
                parent.name = currItem.itemObject.name;
                parent.parameterTypes = currItem.itemObject.parameterTypes;
                return parent;
            }
            if (currItem.itemObject.typeItem == "Relation") {
                parent.type = "DataSource";
                parent.name = currItem.itemObject.parentDataSource;
                return parent;
            }
            currItem = currItem.parent;
        }
        return parent;
    }

    dictionaryTree.getCurrentVariableParent = function () {
        if (!this.selectedItem) return this.mainItems["Variables"];
        var currItem = this.selectedItem;

        while (currItem.parent != null) {
            if (currItem.itemObject.typeItem == "Category" && currItem.parent == this.mainItems["Variables"])
                return currItem;
            currItem = currItem.parent;
        }

        return this.mainItems["Variables"];
    }

    dictionaryTree.getItemByNameAndType = function (name, type) {
        if (name == null) return null;
        if (typeof (name) == "string") {
            for (var i in this.items)
                if (this.items[i].itemObject.typeItem == type && this.items[i].itemObject.name == name)
                    return this.items[i];
        }
        else {
            if (name.length == 0) return null;
            for (var i in this.items) {
                if (this.items[i].itemObject.typeItem == type && this.items[i].itemObject.name == name[0]) {
                    var resultItem = this.items[i];
                    if (name.length == 1 && resultItem.parent != null && resultItem.parent.itemObject.typeItem != "BusinessObject") {
                        return resultItem;
                    }
                    else {
                        var flag = true;
                        var parentItem = resultItem.parent;
                        for (var k = 1; k < name.length; k++) {
                            if (parentItem != null && parentItem.itemObject.typeItem == type && parentItem.itemObject.name == name[k]) {
                                parentItem = parentItem.parent;
                            }
                            else {
                                flag = false;
                            }
                            if (flag) return resultItem;
                        }
                    }
                }
            }
        }

        return null;
    }

    dictionaryTree.getItemCaption = function (name, alias) {
        if (alias == null) return name;
        return name == alias ? name : name + " [" + alias + "]";
    }

    dictionaryTree.getConnectionNameFromNameInSource = function (dataSourceObject) {
        var nameInSourceArray = dataSourceObject.nameInSource.split(".");
        var nameInSourceFirstName = nameInSourceArray[0];
        var connectionName = nameInSourceFirstName == ""
            ? (dataSourceObject["typeDataAdapter"]
                ? this.jsObject.GetLocalizedAdapterName(dataSourceObject.typeDataAdapter)
                : this.jsObject.loc.Database.Connection)
            : nameInSourceFirstName

        return connectionName;
    }

    dictionaryTree.isColumnItemOfDataSource = function (item, columnName, dataSourceName) {
        if (item.itemObject.typeItem == "Column" && item.itemObject.name == columnName
                && ((item.parent.itemObject.typeItem == "Relation" && item.parent.itemObject.parentDataSource == dataSourceName) ||
                    (item.parent.itemObject.typeItem == "DataSource" && item.parent.itemObject.name == dataSourceName)))
            return true;
        return false;
    }

    dictionaryTree.isParameterItemOfDataSource = function (item, parameterName, dataSourceName) {
        var parameterParent = item.parent ? item.parent.parent : null;
        if (parameterParent && item.itemObject.typeItem == "Parameter" && item.itemObject.name == parameterName
                && ((parameterParent.itemObject.typeItem == "Relation" && parameterParent.itemObject.parentDataSource == dataSourceName) ||
                    (parameterParent.itemObject.typeItem == "DataSource" && parameterParent.itemObject.name == dataSourceName)))
            return true;
        return false;
    }

    //Columns
    dictionaryTree.addColumns = function (item, columns) {
        if (!columns) return;

        for (var i = 0; i < columns.length; i++) {
            item.addChild(this.jsObject.DictionaryTreeItemFromObject(columns[i]));
        }
    }

    dictionaryTree.removeColumns = function (item) {
        for (var i in item.childs)
            if (item.childs[i].itemObject.typeItem == "Column") item.childs[i].remove();
    }

    dictionaryTree.updateColumns = function (item, columns) {
        this.removeColumns(item);
        this.addColumns(item, columns);
    }

    //Parameters
    dictionaryTree.addParameters = function (item, parameters) {
        if (!parameters || parameters.length == 0) return;

        parameters.sort(this.jsObject.SortByName);

        var parametersItem = this.jsObject.DictionaryTreeItem(this.jsObject.loc.PropertyMain.Parameters, "Parameter.png", { name: "Parameters", typeItem: "Parameters" }, dictionaryTree);
        item.addChild(parametersItem);

        for (var i = 0; i < parameters.length; i++) {
            parametersItem.addChild(this.jsObject.DictionaryTreeItemFromObject(parameters[i]));
        }
    }

    dictionaryTree.removeParameters = function (item) {
        for (var i in item.childs)
            if (item.childs[i].itemObject.typeItem == "Parameter" || item.childs[i].itemObject.typeItem == "Parameters") item.childs[i].remove();
    }

    dictionaryTree.updateParameters = function (item, parameters) {
        this.removeParameters(item);
        this.addParameters(item, parameters);
    }

    //Override
    dictionaryTree.onSelectedItem = function (selectedItem) {
        var dictionaryPanel = this.jsObject.options.dictionaryPanel;
        dictionaryPanel.toolBar.updateControls();
        dictionaryPanel.descriptionPanel.hide();

        if (selectedItem) {
            for (var i in this.jsObject.options.containers) {
                var container = this.jsObject.options.containers[i];
                container.getItemButton.setEnabled(container.canInsertItem(selectedItem.itemObject));
            }
            dictionaryPanel.setFocused(true);
            if (selectedItem.itemObject){
                if (selectedItem.itemObject.typeItem == "SystemVariable") {
                    dictionaryPanel.descriptionPanel.show(this.jsObject.GetSystemVariableDescription(selectedItem.itemObject.name));
                }
                else if (selectedItem.itemObject.typeItem == "Function") {
                    dictionaryPanel.descriptionPanel.show(this.jsObject.GetFunctionDescription(selectedItem.itemObject));
                }
            }
        }
    }

    return dictionaryTree;
}

StiMobileDesigner.prototype.DictionaryTreeItem = function (caption, imageName, itemObject, tree) {
    var dictionaryTreeItem = this.TreeItem(caption, imageName, itemObject, tree || this.options.dictionaryTree);

    //Override
    dictionaryTreeItem.repaint = function (itemObject) {
        if (itemObject == null) itemObject = this.itemObject;
        this.button.image.src = this.jsObject.options.images[
            (itemObject.typeIcon == "ConnectionFail" ? this.jsObject.GetConnectionFailIconName(itemObject) : itemObject.typeIcon) + ".png"];
        this.button.captionCell.innerHTML = itemObject.typeItem == "DataBase" ? itemObject.nameInSource : this.tree.getItemCaption(itemObject.name, itemObject.alias);
        this.itemObject = itemObject;
    }

    dictionaryTreeItem.setSelected = function () {
        if (this.tree.selectedItem) {
            this.tree.selectedItem.button.className = this.jsObject.options.isTouchDevice ? "stiDesignerTreeItemButton_Touch" : "stiDesignerTreeItemButton_Mouse";
            this.tree.selectedItem.isSelected = false;
        }
        this.button.className = this.jsObject.options.isTouchDevice ? "stiDesignerTreeItemButtonSelected_Touch" : "stiDesignerTreeItemButtonSelected_Mouse";
        this.tree.selectedItem = this;
        this.isSelected = true;
        this.tree.onSelectedItem(this);

        if (this.jsObject.options.dictionaryPanel && !this.jsObject.options.dictionaryPanel.isFocused) {
            if (this.button.className.indexOf("stiDesignerTreeItemButtonSelectedNotActive") < 0)
                this.button.className += " stiDesignerTreeItemButtonSelectedNotActive";
        }
    }

    dictionaryTreeItem.button.ondblclick = function () {
        var propertiesPanel = dictionaryTreeItem.jsObject.options.propertiesPanel;
        if (propertiesPanel.dictionaryMode && propertiesPanel.editFormControl) {
            if (propertiesPanel.editFormControl["addItem"] != null) {
                propertiesPanel.editFormControl.addItem(this.treeItem.itemObject);
            }
            else {
                var text = dictionaryTreeItem.getResultForEditForm();
                if (propertiesPanel.editFormControl.cutExpression && text.indexOf("{") == 0 && this.jsObject.EndsWith(text, "}")) {
                    text = text.substr(1, text.length - 2);
                }
                if ("insertText" in propertiesPanel.editFormControl) {
                    propertiesPanel.editFormControl.insertText(text);
                }
                else {
                    propertiesPanel.editFormControl.value += text;
                }
            }
        }
        else {
            if (this.jsObject.options.menus.dictionarySettingsMenu.controls.createFieldOnDoubleClick.isChecked &&
                (this.treeItem.itemObject.typeItem == "Parameter" || this.treeItem.itemObject.typeItem == "Column" ||
                (this.treeItem.itemObject.typeItem == "Resource" && (this.treeItem.itemObject.type == "Image" || this.treeItem.itemObject.type == "Rtf")))) {
                    this.jsObject.SendCommandCreateFieldOnDblClick(this.treeItem);
            }
            else if (this.jsObject.options.dictionaryPanel.toolBar.controls["EditItem"].isEnabled) {
                this.jsObject.EditItemDictionaryTree();
            }
        }
    }

    dictionaryTreeItem.getResultForEditForm = function () {
        var currItem = this;
        var fullName = "";
        if (currItem.itemObject.typeItem == "Variable" || currItem.itemObject.typeItem == "SystemVariable") {
            fullName = currItem.itemObject.correctName || currItem.itemObject.name;
        }
        else if (currItem.itemObject.typeItem == "Function") {
            fullName = currItem.itemObject.text;
        }
        else if (currItem.itemObject.typeItem == "Column") {
            while (currItem.parent != null) {
                if (fullName != "") fullName = "." + fullName;
                fullName = (currItem.itemObject.correctName || currItem.itemObject.name) + fullName;
                if (currItem.itemObject.typeItem == "BusinessObject" && currItem.parent != null && currItem.parent.itemObject.typeItem != "BusinessObject"
                    || currItem.itemObject.typeItem == "DataSource") break;
                currItem = currItem.parent;
            }
        }
        else if (currItem.itemObject.typeItem == "Parameter") {
            fullName = ".Parameters[\"" + (currItem.itemObject.correctName || currItem.itemObject.name) + "\"].ParameterValue";
            while (currItem.parent != null) {
                if (currItem.itemObject.typeItem == "DataSource") {
                    fullName = (currItem.itemObject.correctName || currItem.itemObject.name) + fullName;
                    break;
                }
                currItem = currItem.parent;
            }
        }

        fullName = fullName ? "{" + fullName + "}" : "";

        return fullName;
    }

    dictionaryTreeItem.getChildNames = function () {
        if (this.buildChildsNotCompleted) this.completeBuildTree(true);

        var childNames = {};
        for (var i in this.childs)
            childNames[i] = this.childs[i].itemObject.name;

        return childNames;
    }

    dictionaryTreeItem.getBusinessObjectFullName = function () {
        var currItem = this;
        var fullName = [];
        while (currItem.parent != null) {
            if (currItem.itemObject.typeItem == "BusinessObject") {
                fullName.push(currItem.itemObject.name);
            }
            currItem = currItem.parent;
        }

        return fullName;
    }

    dictionaryTreeItem.getBusinessObjectStringFullName = function () {
        var currItem = this;
        var fullName = "";
        while (currItem.parent != null) {
            if (currItem.itemObject.typeItem == "BusinessObject") {
                if (fullName != "") fullName = "." + fullName;
                fullName = (currItem.itemObject.correctName || currItem.itemObject.name) + fullName;
            }
            currItem = currItem.parent;
        }

        return fullName;
    }

    dictionaryTreeItem.canShow = function () {
        var permissionDataSources = this.jsObject.options.permissionDataSources;
        var permissionDataConnections = this.jsObject.options.permissionDataConnections;
        var permissionBusinessObjects = this.jsObject.options.permissionBusinessObjects;
        var permissionVariables = this.jsObject.options.permissionVariables;
        var permissionDataRelations = this.jsObject.options.permissionDataRelations;
        var permissionDataColumns = this.jsObject.options.permissionDataColumns;
        var permissionResources = this.jsObject.options.permissionResources;
        var permissionSqlParameters = this.jsObject.options.permissionSqlParameters;

        var getPermissionResult = function (itemObject, permissionType) {
            return (itemObject.typeItem == "DataBase" && !(permissionDataConnections.indexOf("All") >= 0 || permissionDataConnections.indexOf(permissionType) >= 0)) ||
            ((itemObject.typeItem == "DataSource" || itemObject.typeItem == "DataSourceMainItem")
                && !(permissionDataSources.indexOf("All") >= 0 || permissionDataSources.indexOf(permissionType) >= 0)) ||
            ((itemObject.typeItem == "BusinessObject" || itemObject.typeItem == "BusinessObjectMainItem")
                && !(permissionBusinessObjects.indexOf("All") >= 0 || permissionBusinessObjects.indexOf(permissionType) >= 0)) ||
            ((itemObject.typeItem == "Variable" || itemObject.typeItem == "VariablesMainItem")
                && !(permissionVariables.indexOf("All") >= 0 || permissionVariables.indexOf(permissionType) >= 0)) ||
            ((itemObject.typeItem == "Resource" || itemObject.typeItem == "ResourcesMainItem")
                && !(permissionResources.indexOf("All") >= 0 || permissionResources.indexOf(permissionType) >= 0)) ||
            ((itemObject.typeItem == "Parameter" || itemObject.typeItem == "Parameters")
                && !(permissionSqlParameters.indexOf("All") >= 0 || permissionSqlParameters.indexOf(permissionType) >= 0)) ||
            (itemObject.typeItem == "Relation" && !(permissionDataRelations.indexOf("All") >= 0 || permissionDataRelations.indexOf(permissionType) >= 0)) ||
            (itemObject.typeItem == "Column" && !(permissionDataColumns.indexOf("All") >= 0 || permissionDataColumns.indexOf(permissionType) >= 0))
        }

        return !(getPermissionResult(dictionaryTreeItem.itemObject, "View"));
    }

    dictionaryTreeItem.canDragged = function () {
        return (this.itemObject.typeItem == "DataSource" || this.itemObject.typeItem == "Column" || this.itemObject.typeItem == "SystemVariable" ||
            this.itemObject.typeItem == "Variable" || this.itemObject.typeItem == "BusinessObject" || this.itemObject.typeItem == "Function" || this.itemObject.typeItem == "Category" ||
            this.itemObject.typeItem == "Parameter" || (this.itemObject.typeItem == "Resource" && this.itemObject.type == "Image")) ||
            this.itemObject.isCloudAttachedItem;
    }

    dictionaryTreeItem.canMove = function (direction) {        
        if (!this.itemObject) return false;
                
        if (this.itemObject.typeItem == "Column") {
            return ((direction == "Up" && this.previousSibling && this.previousSibling.itemObject.typeItem == "Column") ||
                    (direction == "Down" && this.nextSibling && this.nextSibling.itemObject.typeItem == "Column"))
        }
        else if (this.itemObject.typeItem == "Variable")
        {
            var item = null;

            if (direction == "Up") {
                item = this.previousSibling || this.parent.previousSibling || this.parent.parent;
            }
            else if (direction == "Down") {
                item = this.nextSibling || this.parent.nextSibling || this.parent.parent;
            }

            if (item && item.itemObject && (item.itemObject.typeItem == "Variable" || item.itemObject.typeItem == "Category" || item.itemObject.typeItem == "VariablesMainItem")) {
                return item.itemObject;
            }
        }
        else if (this.itemObject.typeItem == "DataSource" ||
            (this.itemObject.typeItem == "Category" && this.parent && this.parent.itemObject.typeItem == "VariablesMainItem"))
        {
            return ((direction == "Up" && this.previousSibling && this.previousSibling.itemObject) ||
                    (direction == "Down" && this.nextSibling && this.nextSibling.itemObject))
        }
            
        return false;
    }

    //Touch Start
    dictionaryTreeItem.button.ontouchstart = function (event, mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);

        if (event) event.preventDefault();
        this.action();

        if (this.treeItem.canDragged() && event.button != 2) {
            var itemInDragObject;
            
            if (this.treeItem.itemObject.typeItem == "Resource" && this.treeItem.itemObject.type == "Image") {
                itemInDragObject = this.jsObject.ComponentForDragDrop(this.treeItem.itemObject, "StiImage");
            }
            else {
                itemInDragObject = this.jsObject.TreeItemForDragDrop(this.treeItem.itemObject, this.treeItem.tree);
            }

            itemInDragObject.originalItem = this.treeItem;

            if (itemInDragObject.itemObject.typeItem != "BusinessObject") {
                var fullName = this.treeItem.getResultForEditForm();
                itemInDragObject.itemObject.fullName = Base64.encode(fullName);
            }
            itemInDragObject.beginingOffset = 0;
            this.jsObject.options.itemInDrag = itemInDragObject;
        }

        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    //Mouse Down
    dictionaryTreeItem.button.onmousedown = function (event) {
        if (this.isTouchStartFlag) return;
        this.ontouchstart(event, true);
    }

    //Mouse Up
    dictionaryTreeItem.button.onmouseup = function (event) {
        if (this.jsObject.options.itemInDrag) {
            var selectedItem = this.jsObject.options.dictionaryTree.selectedItem;
            if (selectedItem) {
                var fromObject = selectedItem.itemObject;
                var toItem = this.treeItem;
                var toObject = toItem ? toItem.itemObject : null;
                this.jsObject.SendCommandMoveDictionaryItem(fromObject, toObject);
            }
        }
    }

    //Override opening method
    dictionaryTreeItem.iconOpening.action = function () {
        if (this.treeItem.tree.isDisable) return;
        this.treeItem.isOpening = !this.treeItem.isOpening;
        this.treeItem.childsRow.style.display = this.treeItem.isOpening ? "" : "none";
        var imgName = this.treeItem.isOpening ? "IconCloseItem.png" : "IconOpenItem.png";
        if (this.jsObject.options.isTouchDevice) imgName = imgName.replace(".png", "Big.png");
        this.treeItem.iconOpening.src = this.jsObject.options.images[imgName];
        this.treeItem.setSelected();
        
        if (this.treeItem.isOpening && this.treeItem.buildChildsNotCompleted && this.treeItem.childCollection) {
            if (this.treeItem.itemObject.typeItem == "DataBase") {
                this.treeItem.tree.addTreeItems(this.treeItem.childCollection, this.treeItem, true);
            }
            else {
                var dataSource = this.jsObject.GetDataSourceByNameAndNameInSourceFromDictionary(this.treeItem.itemObject.name, this.treeItem.itemObject.nameInSource);
                if (dataSource) {
                    this.treeItem.childCollection = this.jsObject.CopyObject(dataSource.relations);
                    this.treeItem.itemObject.columns = this.jsObject.CopyObject(dataSource.columns);
                    this.treeItem.itemObject.parameters = this.jsObject.CopyObject(dataSource.parameters);
                }                
                this.treeItem.tree.addTreeItems(this.treeItem.childCollection, this.treeItem);
            }
            delete this.treeItem.buildChildsNotCompleted;
            delete this.treeItem.childCollection;
        }
    }

    dictionaryTreeItem.completeBuildTree = function (buildOnlyOneLevel) {
        if (this.buildChildsNotCompleted && this.childCollection) {
            this.tree.addTreeItems(this.childCollection, this, buildOnlyOneLevel);
            delete this.buildChildsNotCompleted;
            delete this.childCollection;
        }
    }

    dictionaryTreeItem.style.display = dictionaryTreeItem.canShow() ? "" : "none";

    return dictionaryTreeItem;

}

StiMobileDesigner.prototype.GetConnectionFailIconName = function (itemObject) {
    var iconName = this.options.cloudMode
        ? (itemObject.isCloud ? "CloudDataBase" : "DataStore")
        : "ConnectionFail";

    return iconName;
}

StiMobileDesigner.prototype.DictionaryTreeItemFromObject = function (itemObject, tree) {
    var dictionaryTreeItem = this.DictionaryTreeItem(
            itemObject.typeItem == "DataBase" ? itemObject.nameInSource : this.options.dictionaryTree.getItemCaption(itemObject.name, itemObject.alias),
            (itemObject.typeIcon == "ConnectionFail" ? this.GetConnectionFailIconName(itemObject) : itemObject.typeIcon) + ".png",
                itemObject, tree || this.options.dictionaryTree);

    return dictionaryTreeItem;
}

StiMobileDesigner.prototype.EditItemDictionaryTree = function () {
    var jsObject = this;
    var selectedItem = this.options.dictionaryTree.selectedItem;
    var typeItem = selectedItem.itemObject.typeItem.replace("DataBase", "Connection").replace("BusinessObject", "DataSource");
    var form = selectedItem.itemObject.typeDataSource == "StiVirtualSource"
        ? this.options.forms.editDataSourceFromOtherDatasourcesForm
        : this.options.forms["edit" + typeItem + "Form"];

    if (!form) {
        switch (typeItem) {
            case "DataSource":
                {
                    if (selectedItem.itemObject.typeDataSource == "StiVirtualSource") {
                        this.InitializeEditDataSourceFromOtherDatasourcesForm(function (editDataSourceFromOtherDatasourcesForm) {
                            editDataSourceFromOtherDatasourcesForm.datasource = jsObject.CopyObject(selectedItem.itemObject);
                            editDataSourceFromOtherDatasourcesForm.changeVisibleState(true);
                        });
                    }
                    else {
                        this.InitializeEditDataSourceForm(function (editDataSourceForm) {
                            editDataSourceForm.datasource = jsObject.CopyObject(selectedItem.itemObject);
                            editDataSourceForm.changeVisibleState(true);
                        });
                    }
                    break;
                }
            case "Category":
                this.InitializeEditCategoryForm(function (editCategoryForm) {
                    editCategoryForm.category = jsObject.CopyObject(selectedItem.itemObject);
                    editCategoryForm.changeVisibleState(true);
                });
                break;
            case "Column":
                this.InitializeEditColumnForm(function (editColumnForm) {
                    editColumnForm.column = jsObject.CopyObject(selectedItem.itemObject);
                    editColumnForm.changeVisibleState(true);
                });
                break;
            case "Connection":
                this.InitializeEditConnectionForm(function (editConnectionForm) {
                    editConnectionForm.connection = jsObject.CopyObject(selectedItem.itemObject);
                    editConnectionForm.changeVisibleState(true);
                });
                break;
            case "Parameter":
                this.InitializeEditParameterForm(function (editParameterForm) {
                    editParameterForm.parameter = jsObject.CopyObject(selectedItem.itemObject);
                    editParameterForm.changeVisibleState(true);
                });
                break;
            case "Relation":
                this.InitializeEditRelationForm(function (editRelationForm) {
                    editRelationForm.relation = jsObject.CopyObject(selectedItem.itemObject);
                    editRelationForm.changeVisibleState(true);
                });
                break;
            case "Variable":
                this.InitializeEditVariableForm(function (editVariableForm) {
                    editVariableForm.variable = jsObject.CopyObject(selectedItem.itemObject);
                    editVariableForm.changeVisibleState(true);
                });
                break;
            case "Resource":
                this.InitializeEditResourceForm(function (editResourceForm) {
                    editResourceForm.resource = jsObject.CopyObject(selectedItem.itemObject);
                    editResourceForm.changeVisibleState(true);
                });
                break;
        }
    }
    if (!form) return;
    form[typeItem.toLowerCase()] = this.CopyObject(selectedItem.itemObject);
    form.changeVisibleState(true);
}

StiMobileDesigner.prototype.DeleteItemDictionaryTree = function () {
    var messageForm = this.MessageFormForDelete();
    messageForm.changeVisibleState(true);
    messageForm.action = function (state) {
        if (state) {
            var selectedItem = this.jsObject.options.dictionaryTree.selectedItem;            
            switch (selectedItem.itemObject.typeItem) {
                case "DataBase": {
                    if (selectedItem.itemObject.typeIcon == "ConnectionFail") {
                        if (selectedItem.buildChildsNotCompleted) selectedItem.completeBuildTree(true);
                        var dataSources = [];
                        for (var key in selectedItem.childs){
                            dataSources.push(selectedItem.childs[key].itemObject);
                        }                        
                        var jsObject = this.jsObject;
                        jsObject.SendCommandToDesignerServer("DeleteAllDataSources", { dataSources: dataSources, connectionName: selectedItem.itemObject.name }, function (answer) {
                            if (answer.databases) {
                                jsObject.options.dictionaryTree.selectedItem.remove();
                                jsObject.options.report.dictionary.databases = answer.databases;
                                jsObject.ClearAllGalleries();
                                jsObject.UpdateStateUndoRedoButtons();
                            }
                        }); break;
                    }
                    else {
                        this.jsObject.SendCommandDeleteConnection(selectedItem);
                    }
                    break;
                }
                case "Relation": { this.jsObject.SendCommandDeleteRelation(selectedItem); break; }
                case "Column": { this.jsObject.SendCommandDeleteColumn(selectedItem); break; }
                case "Parameter": { this.jsObject.SendCommandDeleteParameter(selectedItem); break; }
                case "DataSource": { this.jsObject.SendCommandDeleteDataSource(selectedItem); break; }
                case "BusinessObject": { this.jsObject.SendCommandDeleteBusinessObject(selectedItem); break; }
                case "Category": { this.jsObject.SendCommandDeleteCategory(selectedItem); break; }
                case "Variable": { this.jsObject.SendCommandDeleteVariable(selectedItem); break; }
                case "Resource": { this.jsObject.SendCommandDeleteResource(selectedItem); break; }
            }
        }
    }
}

StiMobileDesigner.prototype.SetOpeningAllChildDictionaryTree = function (state) {
    var selectedItem = this.options.dictionaryTree.selectedItem;
    if (selectedItem) {
        selectedItem.setOpening(state);
        var childs = selectedItem.getAllChilds();
        for (var i = 0; i < childs.length; i++) childs[i].setOpening(state);
    }
}

StiMobileDesigner.prototype.HiddenMainTreeItem = function (tree, haveItemNone) {
    var mainItem = this.TreeItem("Main", null, { "typeItem": "MainItem" }, tree);
    mainItem.style.margin = "12px";
    mainItem.childsRow.style.display = "";
    mainItem.button.style.display = "none";
    mainItem.iconOpening.style.display = "none";
    tree.mainItem = mainItem;

    if (haveItemNone) {
        var itemNone = this.TreeItem(this.loc.Report.NotAssigned, "ItemNo.png", { "typeItem": "NoItem" }, tree);
        mainItem.addChild(itemNone);
        itemNone.setSelected();
        tree.itemNone = itemNone;
        itemNone.button.ondblclick = function () { tree.action(); }
    }

    tree.getItem = function (keyValue, keyName, typeItem) {
        for (var i in tree.items) {
            if (tree.items[i].itemObject[keyName || "name"] == keyValue && (!typeItem || (typeItem && tree.items[i].itemObject.typeItem == typeItem))) {
                return tree.items[i];
            }
        }
        return null;
    }

    return mainItem;
}

StiMobileDesigner.prototype.SystemVariablesTree = function () {
    var systemVarsTree = this.Tree();

    systemVarsTree.build = function () {
        systemVarsTree.clear();

        var mainItemSysVars = this.jsObject.HiddenMainTreeItem(systemVarsTree);
        systemVarsTree.appendChild(mainItemSysVars);

        if (this.jsObject.options.report) {
            var systemVariables = this.jsObject.options.report.dictionary.systemVariables;
            for (var i = 0; i < systemVariables.length; i++) {
                var item = this.jsObject.TreeItem(systemVariables[i], "SystemVariable" + systemVariables[i] + ".png", { typeItem: "SystemVariable", name: systemVariables[i] }, systemVarsTree);
                mainItemSysVars.addChild(item);

                item.button.ondblclick = function () { systemVarsTree.action(); }
            }
        }
    }

    return systemVarsTree;
}

StiMobileDesigner.prototype.VariablesTree = function () {
    var varsTree = this.Tree();

    varsTree.build = function () {
        varsTree.clear();

        var mainItemVars = this.jsObject.HiddenMainTreeItem(varsTree, true);
        varsTree.appendChild(mainItemVars);

        if (this.jsObject.options.report) {
            var variables = this.jsObject.GetVariablesFromDictionary(this.jsObject.options.report.dictionary);
            for (var i = 0; i < variables.length; i++) {
                var item = this.jsObject.DictionaryTreeItemFromObject(variables[i], varsTree);                
                mainItemVars.addChild(item);

                item.button.ondblclick = function () { varsTree.action(); }
            }
        }
    }

    return varsTree;
}

StiMobileDesigner.prototype.DataSourcesTree = function (width, height) {
    var dataTree = this.Tree();
    dataTree.isDisable = false;
    if (width) dataTree.style.width = width + "px";
    if (height) dataTree.style.height = height + "px";

    dataTree.setEnabled = function (state) {
        dataTree.style.opacity = state ? "1" : "0.2";
        dataTree.isDisable = !state;
        if (!state && dataTree.itemNone) dataTree.itemNone.setSelected();
    }

    dataTree.addTreeItems = function (collection, parentItem, openAllItems) {
        if (collection) {
            collection.sort(this.jsObject.SortByName);
            for (var i = 0; i < collection.length; i++) {
                if (collection[i].typeItem == "Category") {
                    var categoryItem = parentItem.addChild(this.jsObject.DictionaryTreeItemFromObject(this.jsObject.CategoryObject(collection[i].name), dataTree));
                    this.addTreeItems(collection[i].categoryItems, categoryItem);
                    categoryItem.button.ondblclick = null;
                    if (openAllItems) categoryItem.setOpening(true);
                    continue;
                }
                
                var childItem = parentItem.addChild(this.jsObject.DictionaryTreeItemFromObject(collection[i], dataTree));
                if (openAllItems) childItem.setOpening(true);
                childItem.button.ondblclick = (childItem.itemObject.typeItem == "DataSource" || childItem.itemObject.typeItem == "BusinessObject")
                    ? function () { if (!dataTree.isDisable) dataTree.action(); } : null;
                var childCollection;
                switch (collection[i].typeItem) {
                    case "DataBase": { childCollection = collection[i].dataSources; break; }
                    case "BusinessObject": { if (!dataTree.oneLevelBusinessObjects) childCollection = collection[i].businessObjects; break; }
                }
                this.addTreeItems(childCollection, childItem);
            }
        }
    }
    
    dataTree.build = function (openAllItems, hideBusinessObject) {
        dataTree.clear();

        var mainItem = this.jsObject.HiddenMainTreeItem(dataTree, true);
        dataTree.appendChild(mainItem);

        var dataSourcesItem = this.jsObject.DictionaryTreeItem(this.jsObject.loc.PropertyMain.DataSources, "DataSource.png", { name: "DataSourceMainItem", typeItem: "DataSourceMainItem" }, dataTree);
        mainItem.addChild(dataSourcesItem);

        var businessObjectsItem = !hideBusinessObject
            ? this.jsObject.DictionaryTreeItem(this.jsObject.loc.Report.BusinessObjects, "BusinessObject.png", { name: "BusinessObjectMainItem", typeItem: "BusinessObjectMainItem" }, dataTree)
            : null;

        if (businessObjectsItem) mainItem.addChild(businessObjectsItem);

        if (openAllItems) {
            dataSourcesItem.setOpening(true);
            if (businessObjectsItem) businessObjectsItem.setOpening(true);
        }

        if (this.jsObject.options.report) {
            var dictionary = this.jsObject.options.report.dictionary;
            this.addTreeItems(dictionary.databases, dataSourcesItem, openAllItems);
            if (businessObjectsItem) this.addTreeItems(dictionary.businessObjects, businessObjectsItem, openAllItems);
        }
    }

    return dataTree;
}

StiMobileDesigner.prototype.RelationsTree = function (width, height) {
    var relTree = this.Tree();
    if (width) relTree.style.width = width + "px";
    if (height) relTree.style.height = height + "px";

    relTree.build = function (dataSource) {
        relTree.clear();

        var mainItem = this.jsObject.HiddenMainTreeItem(relTree, true);
        relTree.appendChild(mainItem);

        if (dataSource && dataSource.relations) {
            for (var i = 0; i < dataSource.relations.length; i++) {
                var relationItem = mainItem.addChild(this.jsObject.DictionaryTreeItemFromObject(dataSource.relations[i], relTree));
                relationItem.button.ondblclick = function () { relTree.action(); }
            }
        }
    }

    return relTree;
}

StiMobileDesigner.prototype.MasterComponentsTree = function () {
    var masterTree = this.Tree();

    masterTree.build = function (dataSource) {
        masterTree.clear();

        var mainItem = this.jsObject.HiddenMainTreeItem(masterTree, true);
        masterTree.appendChild(mainItem);

        if (this.jsObject.options.report) {
            var masterComponents = this.jsObject.GetMasterComponentItems(true);
            if (!masterComponents) return;
            for (var i = 0; i < masterComponents.length; i++) {
                var item = this.jsObject.TreeItem(masterComponents[i].name, "Small" + masterComponents[i].type + ".png", masterComponents[i], masterTree);
                mainItem.addChild(item);
                item.button.ondblclick = function () { masterTree.action(); }                
            } 
        }
    }

    return masterTree;
}

StiMobileDesigner.prototype.BaseItemForDragDrop = function () {

}

StiMobileDesigner.prototype.TreeItemForDragDrop = function (itemObject, tree, noImage) {
    var treeItem = this.DictionaryTreeItemFromObject(itemObject, tree);
    this.options.mainPanel.appendChild(treeItem);
    if (tree && tree.items[treeItem.id]) delete tree.items[treeItem.id];

    treeItem.style.position = "absolute";
    treeItem.style.display = "none";
    treeItem.style.width = "";
    treeItem.iconOpening.style.visibility = "hidden";
    treeItem.button.ondblclick = null;
    treeItem.button.ontouchstart = null;
    treeItem.button.onmousedown = null;
    treeItem.button.ontouchend = null;
    treeItem.button.onmouseup = null;
    treeItem.button.onmousemove = null;
    treeItem.button.ontouchmove = null;
    treeItem.style.zIndex = "300";

    treeItem.button.className = "stiDesignerTreeItemForDragDrop" + (this.options.isTouchDevice ? "_Touch" : "_Mouse");
    if (noImage && treeItem.button.imageCell) {
        treeItem.button.imageCell.style.display = "none";
        if (treeItem.button.captionCell) treeItem.button.captionCell.style.padding = "3px 5px 3px 5px";
    }

    treeItem.move = function (event, offsetX, offsetY) {
        treeItem.style.display = "";
        var clientX = event.touches ? event.touches[0].pageX : event.clientX;
        var clientY = event.touches ? event.touches[0].pageY : event.clientY;

        var designerOffsetX = this.jsObject.FindPosX(this.jsObject.options.mainPanel);
        var designerOffsetY = this.jsObject.FindPosY(this.jsObject.options.mainPanel);
        clientX -= designerOffsetX;
        clientY -= designerOffsetY;

        if (offsetX) clientX += offsetX;
        if (offsetY) clientY += offsetY;

        this.style.left = clientX + "px";
        this.style.top = (clientY + 20) + "px";
    }

    return treeItem;
}

StiMobileDesigner.prototype.ComponentForDragDrop = function (itemObject, typeComponent) {
    var parent = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "svg") : document.createElement("svg");
    this.options.mainPanel.appendChild(parent);
    parent.style.position = "absolute";
    parent.style.display = "none";
    parent.style.zIndex = "300";
    parent.itemObject = itemObject;
    parent.jsObject = this;

    var compObject = {};

    switch (typeComponent) {
        case "StiImage": {
            var size = this.ConvertPixelToUnit(100);

            compObject = {
                componentRect: "0!0!" + size + "!" + size,
                typeComponent: "StiImage",
                properties: {
                    border: "0,0,0,0!1!0,0,0!0!0!0!0",
                    brush: "1!transparent",
                    svgContent: ""
                }
            };
            break;
        }
    }

    var component = this.CreateComponent(compObject);
    parent.appendChild(component);
    component.repaint();
    parent.setAttribute("width", parseInt(component.getAttribute("width")) + 5);
    parent.setAttribute("height", parseInt(component.getAttribute("height")) + 5);
    
    parent.move = function (event, offsetX, offsetY) {
        parent.style.display = "";
        var clientX = event.touches ? event.touches[0].pageX : event.clientX;
        var clientY = event.touches ? event.touches[0].pageY : event.clientY;

        var designerOffsetX = this.jsObject.FindPosX(this.jsObject.options.mainPanel);
        var designerOffsetY = this.jsObject.FindPosY(this.jsObject.options.mainPanel);
        clientX -= designerOffsetX;
        clientY -= designerOffsetY;

        if (offsetX) clientX += offsetX;
        if (offsetY) clientY += offsetY;

        this.style.left = (clientX + 2) + "px";
        this.style.top = (clientY + 2) + "px";
    }

    return parent;
}