﻿
//Helper Methods
StiMobileDesigner.prototype.GetObjectByPropertyValueFromCollection = function (collection, propertyName, propertyValue) {
    for (var index in collection)
        if (collection[index][propertyName] == propertyValue) return collection[index];

    return null;
}

StiMobileDesigner.prototype.GetCollectionByCollectionName = function (collection, collectionName) {
    var resultCollection = [];
    for (var index in collection)
        for (var index2 in collection[index][collectionName])
            resultCollection.push(collection[index][collectionName][index2]);

    return resultCollection;
}

StiMobileDesigner.prototype.GetCollectionFromCategoriesCollection = function (categoriesCollection) {
    var resultCollection = [];
    for (var i = 0; i < categoriesCollection.length; i++) {
        if (categoriesCollection[i].typeItem == "Category" && categoriesCollection[i].categoryItems) {
            var categoryItems = categoriesCollection[i].categoryItems;
            for (var k = 0; k < categoryItems.length; k++) {
                resultCollection.push(categoryItems[k]);
            }
        }
        else
            resultCollection.push(categoriesCollection[i]);
    }

    return resultCollection;
}

//Get DataSources Collection
StiMobileDesigner.prototype.GetDataSourcesFromDictionary = function (dictionary) {
    return this.GetCollectionByCollectionName(dictionary.databases, "dataSources");
}

//Get Relations Collection
StiMobileDesigner.prototype.GetRelationsFromDictionary = function (dictionary) {
    return this.GetCollectionByCollectionName(this.GetDataSourcesFromDictionary(dictionary), "relations");
}

//Get Variables Collection
StiMobileDesigner.prototype.GetVariablesFromDictionary = function (dictionary) {
    return this.GetCollectionFromCategoriesCollection(dictionary.variables);
}

//Get BusinessObjects Collection
StiMobileDesigner.prototype.GetBusinessObjectsFromDictionary = function (dictionary) {
    var addBusinessObjectsToArray = function (array, businessObjects) {
        for (var i = 0; i < businessObjects.length; i++) {
            array.push(businessObjects[i]);
            if (businessObjects[i].businessObjects) {
                addBusinessObjectsToArray(array, businessObjects[i].businessObjects);
            }
        }
    }

    var parentBusinessObjects = this.GetCollectionFromCategoriesCollection(dictionary.businessObjects);
    for (var i = 0; i < parentBusinessObjects.length; i++) {
        if (parentBusinessObjects[i].businessObjects) {
            addBusinessObjectsToArray(parentBusinessObjects, parentBusinessObjects[i].businessObjects);
        }
    }

    return parentBusinessObjects;
}

//Get Variable Categories Collection
StiMobileDesigner.prototype.GetVariableCategoriesFromDictionary = function (dictionary) {
    var categoriesCollection = [];
    for (var i = 0; i < dictionary.variables.length; i++)
        if (dictionary.variables[i].typeItem == "Category")
            categoriesCollection.push(dictionary.variables[i]);

    return categoriesCollection;
}

//DataSource By Name
StiMobileDesigner.prototype.GetDataSourceByNameFromDictionary = function (name) {
    var dataSources = this.GetDataSourcesFromDictionary(this.options.report.dictionary);
    return this.GetObjectByPropertyValueFromCollection(dataSources, "name", name);
}

//DataSource By Name And NameInSource
StiMobileDesigner.prototype.GetDataSourceByNameAndNameInSourceFromDictionary = function (name, nameInSource) {
    var dataSources = this.GetDataSourcesFromDictionary(this.options.report.dictionary);
    for (var index in dataSources) {
        if (dataSources[index].name == name && dataSources[index].nameInSource == nameInSource)
            return dataSources[index];
    }

    return null;
}

//BusinessObject By Name
StiMobileDesigner.prototype.GetBusinessObjectByNameFromDictionary = function (name) {
    if (!name) return null;
    var businessObjects = this.GetBusinessObjectsFromDictionary(this.options.report.dictionary);
    if (typeof (name) == "string" || name.length == 1) {
        return this.GetObjectByPropertyValueFromCollection(businessObjects, "name", typeof (name) == "string" ? name : name[0]);
    }
    else {
        for (var i = name.length - 1; i >= 0; i--) {
            var businessObject = this.GetObjectByPropertyValueFromCollection(businessObjects, "name", name[i]);
            if (businessObject) businessObjects = businessObject.businessObjects;
        }
        return businessObject;
    }
}

//Relation By Name
StiMobileDesigner.prototype.GetRelationByNameFromObject = function (object, name) {
    if (!object) return null;
    var relations = object.relations;
    return this.GetObjectByPropertyValueFromCollection(relations, "name", name);
}

//Relation By NameInSource
StiMobileDesigner.prototype.GetRelationByNameInSourceFromObject = function (object, nameInSource) {
    if (!object) return null;
    var relations = object.relations;
    return this.GetObjectByPropertyValueFromCollection(relations, "nameInSource", nameInSource);
}

//Columns By Name
StiMobileDesigner.prototype.GetColumnByNameFromDataSource = function (dataSource, name) {
    if (!dataSource) return null;
    return this.GetObjectByPropertyValueFromCollection(dataSource.columns, "name", name);
}

//Variable By Name
StiMobileDesigner.prototype.GetVariableByNameFromDictionary = function (name) {
    var allVariables = this.GetCollectionFromCategoriesCollection(this.options.report.dictionary.variables);
    return this.GetObjectByPropertyValueFromCollection(allVariables, "name", name);
}

//Get Items For Menu
StiMobileDesigner.prototype.GetDataSourceItemsFromDictionary = function () {
    var dataSources = this.GetDataSourcesFromDictionary(this.options.report.dictionary);
    var items = [];
    for (var index in dataSources)
        items.push(this.Item("dataSource" + index, dataSources[index].name, null, dataSources[index].name));

    return items;
}


StiMobileDesigner.prototype.GetAllRelationsFromDataSource = function (dataSource, relationFullName, relations, relationsArray) {
    if (dataSource) {
        if (relations == null) relations = dataSource.relations;
        for (var i in relations) {
            var currentRelationFullName = relationFullName + (relationFullName != "" ? "." : "") + relations[i].name;
            relationsArray.push(currentRelationFullName);
            var currentRelation = relations[i];
            this.GetAllRelationsFromDataSource(dataSource, currentRelationFullName, currentRelation.relations, relationsArray);
        }
    }
}

StiMobileDesigner.prototype.GetRelationsItemsFromDictionary = function (object) {
    if (!object) return null;
    var items = [];
    var relations = object.relations;

    for (var index in relations) {
        items.push(this.Item("relation" + index, relations[index].name, null,
            this.options.selectedObject.typeComponent != "StiImage" ? relations[index].name : relations[index].nameInSource));
    }

    return items.length == 0 ? null : items;
}

StiMobileDesigner.prototype.GetColumnsItemsFromDictionary = function (dataSource) {
    if (!dataSource) return null;
    var items = [];
    var columns = dataSource.columns;
    for (var index in columns)
        items.push(this.Item("columns" + index, columns[index].name, null, columns[index].name));

    return items;
}

StiMobileDesigner.prototype.GetNewName = function (type, collection, name) {

    var defaultName = name || (type == "DataBase" ? this.loc.Database.Connection : this.loc.PropertyMain[type]);
    if (defaultName) defaultName = defaultName.replace(/ /g, '');

    if (!collection) {
        switch (type) {
            case "DataBase": { collection = this.options.report.dictionary.databases; break; }
            case "DataSource": { collection = this.GetDataSourcesFromDictionary(this.options.report.dictionary); break; }
            case "BusinessObject": { collection = this.GetBusinessObjectsFromDictionary(this.options.report.dictionary); break; }
            case "Relation": { collection = this.GetRelationsFromDictionary(this.options.report.dictionary); break; }
            case "Variable": { collection = this.GetVariablesFromDictionary(this.options.report.dictionary); break; }
            case "Category": { collection = this.GetVariableCategoriesFromDictionary(this.options.report.dictionary); break; }
            case "Resource": { collection = this.options.report.dictionary.resources; break; }
        }
    }

    index = 0;
    flag = false;
    if (collection)
        while (!flag) {
            index++;
            flag = true;
            for (var i in collection) {
                if (collection[i].name && typeof (collection[i].name) == "string" &&
                    collection[i].name.toLowerCase() == defaultName.toLowerCase() + (index == 1 ? "" : index)) {
                    flag = false;
                    break;
                }
            }
        }

    return defaultName + (index == 1 ? "" : index);
}

StiMobileDesigner.prototype.GetTypeIcon = function (type) {
    if (type == "bool" || type == "bool (Nullable)") return "DataColumnBool";
    if (type == "char" || type == "char (Nullable)") return "DataColumnChar";
    if (type == "datetime" || type == "timespan" || type == "datetime (Nullable)" || type == "timespan (Nullable)") return "DataColumnDateTime";
    if (type == "decimal" || type == "decimal (Nullable)") return "DataColumnDecimal";
    if (type == "int" || type == "uint" || type == "long" || type == "ulong" || type == "byte" || type == "sbyte" || type == "short" || type == "ushort" ||
        type == "int (Nullable)" || type == "uint (Nullable)" || type == "long (Nullable)" || type == "ulong (Nullable)" || type == "byte (Nullable)" ||
        type == "sbyte (Nullable)" || type == "short (Nullable)" || type == "ushort (Nullable)")
        return "DataColumnInt";
    if (type == "float" || type == "double" || type == "float (Nullable)" || type == "double (Nullable)") return "DataColumnFloat";
    if (type == "image") return "DataColumnImage";
    if (type == "object") return "DataColumnBinary";

    return "DataColumnString";
}

StiMobileDesigner.prototype.CanEditConnectionString = function (connection) {
    return connection.typeConnection == "StiODataDatabase";
}

StiMobileDesigner.prototype.ParseODataConnectionString = function (connectionString) {
    var connection = {};
    if (connectionString) {
        connectionArray = connectionString.split(";");
        for (var i = 0; i < connectionArray.length; i++) {
            var connectItem = connectionArray[i].trim();
            if (connectItem.indexOf("AddressBearer=") == 0) connection["AddressBearer"] = connectItem.substring("AddressBearer=".length);
            else if (connectItem.indexOf("UserName=") == 0) connection["UserName"] = connectItem.substring("UserName=".length);
            else if (connectItem.indexOf("Password=") == 0) connection["Password"] = connectItem.substring("Password=".length);
            else connection["Address"] = connectItem;
        }
    }

    return connection;
}