﻿
StiMobileDesigner.prototype.InitializeToolbox = function () {
    var toolbox = document.createElement("div");
    this.options.toolbox = toolbox;
    this.options.mainPanel.appendChild(toolbox);
    toolbox.className = "stiDesignerToolbox";
    toolbox.jsObject = this;
    toolbox.style.display = "none";
    toolbox.visible = false;
    toolbox.style.left = "0px";
    toolbox.style.bottom = this.options.statusPanel.offsetHeight + "px";
    toolbox.style.top = (this.options.toolBar.offsetHeight + this.options.workPanel.offsetHeight) + "px";
    toolbox.style.width = this.options.isTouchDevice ? "36px" : "30px";
    toolbox.style.zIndex = 2;
    toolbox.buttons = {};
    toolbox.selectedComponent = null;

    toolbox.resetChoose = function () {
        this.jsObject.options.drawComponent = false;
        this.jsObject.options.paintPanel.changeCursorType(false);

        if (this.selectedComponent) {
            this.selectedComponent.setSelected(false);
            this.selectedComponent = null;
        }

        if (!this.jsObject.options.componentsIntoInsertTab) {
            if (this.buttons.bands) this.buttons.bands.setSelected(false);
            if (this.buttons.crossBands) this.buttons.crossBands.setSelected(false);
            if (this.buttons.components) this.buttons.components.setSelected(false);
            if (this.buttons.shapes) this.buttons.shapes.setSelected(false);
            if (this.buttons.infographics) this.buttons.infographics.setSelected(false);
        }
    }

    toolbox.setChoose = function (selectedElement) {
        this.jsObject.options.drawComponent = true;
        this.jsObject.options.paintPanel.setCopyStyleMode(false);
        this.jsObject.options.paintPanel.changeCursorType(true);
        this.jsObject.options.toolbox.selectedComponent = selectedElement;

        if (selectedElement.menu && selectedElement.menu.parentButton) {
            selectedElement.menu.parentButton.setSelected(true);
        }
        else {
            selectedElement.setSelected(true);
        }
    }

    toolbox.update = function (components) {
        var jsObject = this.jsObject;

        if (toolbox.mainTable) {
            toolbox.removeChild(toolbox.mainTable);
        }

        toolbox.mainTable = jsObject.CreateHTMLTable();
        toolbox.mainTable.style.margin = "3px 0 0 3px";
        toolbox.appendChild(toolbox.mainTable);

        var addComponentButtons = function (componentTypes) {
            for (var i in componentTypes) {
                if (jsObject.options.visibilityComponents[componentTypes[i]] ||
                    jsObject.options.visibilityBands[componentTypes[i]] ||
                    jsObject.options.visibilityCrossBands[componentTypes[i]]) {
                    var button = jsObject.ToolboxButton("toolBox" + componentTypes[i], "SmallComponents." + componentTypes[i] + ".png",
                        ["<b>" + jsObject.loc.Components[componentTypes[i]] + "</b><br><br>" + jsObject.loc.HelpComponents[componentTypes[i]], jsObject.HelpLinks["insertcomponent"]]);
                    button.toolboxOwner = true;
                    button.name = componentTypes[i];
                    toolbox.buttons[componentTypes[i]] = button;
                    toolbox.mainTable.addCellInNextRow(button);
                    button.setEnabled(jsObject.options.report != null);

                    button.onmouseenter = function () {
                        var this_ = this;
                        if (!this.isEnabled || (this["haveMenu"] && this.isSelected) || this.jsObject.options.isTouchClick) return;
                        this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
                        this.isOver = true;
                        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object")
                            this.jsObject.options.toolTip.showWithDelay(
                                this.toolTip[0],
                                this.toolTip[1],
                                button.jsObject.options.isTouchDevice ? 38 : 32,
                                button.jsObject.FindPosY(this_, "stiDesignerMainPanel")
                            );
                    }

                    button.action = function () {
                        toolbox.resetChoose();
                        this.setSelected(!this.isSelected);
                        if (this.isSelected) toolbox.setChoose(this);
                    }
                }
            }
        }

        var componentTypes = jsObject.options.componentsIntoInsertTab || components || jsObject.GetComponentsIntoInsertTab();

        if (!jsObject.options.componentsIntoInsertTab) {
            var menuButtons = [
                ["bands", "Toolbox.Bands.png"],
                ["crossBands", "Toolbox.CrossBands.png"],
                ["components", "Toolbox.Components.png"],
                ["shapes", "SmallComponents.StiShape.png"],
                ["infographics", "Toolbox.Infographics.png"]
            ];

            for (var i = 0; i < menuButtons.length; i++) {
                var button = jsObject.ToolboxButton("toolBox" + menuButtons[i][0], menuButtons[i][1], null, true);
                toolbox.mainTable.addCellInNextRow(button);
                toolbox.buttons[menuButtons[i][0]] = button;
                button.setEnabled(jsObject.options.report != null);
            }

            toolbox.mainTable.addCellInNextRow(jsObject.ToolboxSeparator());
        }

        var bandsMenu = jsObject.InitializeToolboxBandsMenu(toolbox.buttons.bands);
        toolbox.buttons.bands.action = function () {
            bandsMenu.changeVisibleState(!bandsMenu.visible);
        }

        var crossBandsMenu = jsObject.InitializeToolboxCrossBandsMenu(toolbox.buttons.crossBands);
        toolbox.buttons.crossBands.action = function () {
            crossBandsMenu.changeVisibleState(!crossBandsMenu.visible);
        }

        var componentsMenu = jsObject.InitializeToolboxComponentsMenu(toolbox.buttons.components);
        toolbox.buttons.components.action = function () {
            componentsMenu.changeVisibleState(!componentsMenu.visible);
        }

        var shapesMenu = jsObject.InitializeToolboxShapesMenu(toolbox.buttons.shapes);
        toolbox.buttons.shapes.action = function () {
            shapesMenu.changeVisibleState(!shapesMenu.visible);
        }

        var infographicsMenu = jsObject.InitializeToolboxInfographicsMenu();
        toolbox.buttons.infographics.action = function () {
            infographicsMenu.changeVisibleState(!infographicsMenu.visible);
        }

        var addSetupButtonSeparator = false;

        if (jsObject.Is_array(componentTypes)) {
            addComponentButtons(componentTypes);
        }
        else {
            var addSep = false;
            for (var groupName in componentTypes) {
                if (componentTypes[groupName].length > 0) {
                    var componentsInGroup = componentTypes[groupName];
                    var visibleComponents = [];
                    for (var i = 0; i < componentsInGroup.length; i++) {
                        if (jsObject.options.visibilityComponents[componentsInGroup[i]] ||
                            jsObject.options.visibilityBands[componentsInGroup[i]] ||
                            jsObject.options.visibilityCrossBands[componentsInGroup[i]]) {
                            visibleComponents.push(componentsInGroup[i]);
                        }
                    }
                    if (visibleComponents.length > 0) {
                        if (addSep) toolbox.mainTable.addCellInNextRow(jsObject.ToolboxSeparator());
                        addSep = true;
                        addSetupButtonSeparator = true;
                    }
                }
                addComponentButtons(componentTypes[groupName]);
            }
        }

        if (jsObject.options.showSetupToolboxButton) {
            if (addSetupButtonSeparator) toolbox.mainTable.addCellInNextRow(jsObject.ToolboxSeparator());
            var setupToolboxButton = jsObject.ToolboxButton(null, "Toolbox.SmallSetupToolbox.png", jsObject.loc.FormDesigner.SetupToolbox);
            toolbox.mainTable.addCellInNextRow(setupToolboxButton);

            setupToolboxButton.action = function () {
                this.jsObject.InitializeSetupToolboxForm(function (form) {
                    form.changeVisibleState(true);
                });
            }
        }
    }

    toolbox.changeVisibleState = function (state) {
        this.visible = state;
        this.style.display = state ? "" : "none";
        var paintPanel = this.jsObject.options.paintPanel;
        var pagesPanel = this.jsObject.options.pagesPanel;
        var propertiesPanel = this.jsObject.options.propertiesPanel;
        var marginLeft = propertiesPanel.fixedViewMode ? 30 : 0;
        propertiesPanel.style.left = (this.offsetWidth + marginLeft) + "px";
        propertiesPanel.showButtonsPanel.style.left = this.offsetWidth + "px";
        paintPanel.style.left = ((propertiesPanel.fixedViewMode ? 0 : propertiesPanel.offsetWidth) + this.offsetWidth) + "px";
        if (pagesPanel) {
            pagesPanel.style.left = ((propertiesPanel.fixedViewMode ? 0 : propertiesPanel.offsetWidth) + this.offsetWidth) + "px";
            pagesPanel.updateScrollButtons();
        }
    }

    if (this.options.showToolbox) {
        toolbox.changeVisibleState(true);
        toolbox.update();
    }

    return toolbox;
}

StiMobileDesigner.prototype.ToolboxSeparator = function () {
    var sep = document.createElement("div");
    sep.className = "stiDesignerHomePanelSeparator";
    sep.style.height = "1px";
    sep.style.margin = "2px 0 2px 0";
    sep.style.width = this.options.isTouchDevice ? "30px" : "24px";

    return sep;
}

StiMobileDesigner.prototype.ToolboxButton = function (name, imageName, tooltip, menu) {
    var button = this.StandartSmallButton(name, null, null, imageName, tooltip);
    button.style.width = this.options.isTouchDevice ? "30px" : "24px";
    button.style.height = this.options.isTouchDevice ? "30px" : "24px";
    button.innerTable.style.width = "100%";
    button.imageName = imageName;

    if (menu) {
        var arrow = document.createElement("img");
        button.arrow = arrow;
        arrow.style.marginBottom = "2px";
        arrow.src = this.options.images["Toolbox.SmallArrowRight.png"];
        var arrowCell = button.innerTable.addCellInNextRow();
        arrowCell.style.textAlign = "center";
        arrowCell.appendChild(arrow);
        button.style.height = "30px";
    }
    else {
        this.AddDragEventsToComponentButton(button);
    }

    return button;
}

StiMobileDesigner.prototype.AddDragEventsToComponentButton = function (button) {
    button.onmousedown = function (event) {
        if (this.isTouchStartFlag || !this.isEnabled) return;
        this.jsObject.options.buttonPressed = this;
        this.ontouchstart(event, true);
    }

    button.ontouchstart = function (event, mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.fingerIsMoved = false;
        this.jsObject.options.buttonPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);

        if (this.jsObject.options.controlsIsFocused) {
            this.jsObject.options.controlsIsFocused.blur(); //fixed bug when drag&drop component from toolbar
        }
        if (event && !this.isTouchStartFlag) event.preventDefault();
        if (event.button != 2 && this.name/* && ComponentCollection[this.name]*/) {
            var image = this.name + ".png";

            if (!this.jsObject.options.images[image]) {
                if (this.name.indexOf("StiShape;") == 0) {
                    var shapeTypeArray = this.name.split(";");
                    if (shapeTypeArray.length >= 2) {
                        image = "Shapes." + shapeTypeArray[1] + ".png";
                    }
                } /*else if (this.name.indexOf("Infographic;StiChart;") == 0) {
                    var chartTypeArray = this.name.split(";");
                    if (chartTypeArray.length >= 3) {
                        image = chartTypeArray[2] + ".png";
                    }
                }*/
            }

            if (!this.jsObject.options.images[image]) return;

            var componentButtonInDrag = this.jsObject.StandartBigButton(null, null, this.jsObject.loc.Components[this.name], image);

            this.jsObject.options.mainPanel.appendChild(componentButtonInDrag);

            componentButtonInDrag.ownerButton = this;
            componentButtonInDrag.style.opacity = "0.7";
            componentButtonInDrag.style.position = "absolute";
            componentButtonInDrag.style.display = "none";
            componentButtonInDrag.style.zIndex = "300";
            componentButtonInDrag.beginingOffset = 0;
            this.jsObject.options.componentButtonInDrag = componentButtonInDrag;

            componentButtonInDrag.move = function (event, offsetX, offsetY) {
                componentButtonInDrag.style.display = "";
                var clientX = event.touches ? event.touches[0].pageX : event.clientX;
                var clientY = event.touches ? event.touches[0].pageY : event.clientY;

                var designerOffsetX = this.jsObject.FindPosX(this.jsObject.options.mainPanel);
                var designerOffsetY = this.jsObject.FindPosY(this.jsObject.options.mainPanel);
                clientX -= designerOffsetX;
                clientY -= designerOffsetY;

                if (offsetX) clientX += offsetX;
                if (offsetY) clientY += offsetY;

                this.style.left = (clientX + 10) + "px";
                this.style.top = (clientY + 15) + "px";
            }
        }
    }

    button.ontouchend = function (event) {
        this.jsObject.options.currentPage.ontouchend();
    }
}