﻿
StiMobileDesigner.prototype.ToolButton = function (name, caption, isDinamic) {
    var button = this.SmallButton(name, "ToolButtons", caption, null, null, null, isDinamic ? this.GetStyles("ToolButtonDinamic") : this.GetStyles("ToolButton"), true);
    button.isDinamic = isDinamic;
    button.style.margin = "2px 2px 0 2px";
    button.style.height = "30px";
    button.style.minWidth = "30px";
    button.innerTable.style.width = "100%";

    button.action = function () {
        this.setSelected(true);
        this.jsObject.ExecuteAction(this.name);
    }

    //Override
    button.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    button.onmouseenter = function () {
        if (!this.isEnabled || this.isSelected || this.jsObject.options.isTouchClick) return;
        this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = true;
    }

    button.onmouseleave = function () {
        if (!this.isEnabled || this.isSelected) return;
        this.className = this.styles["default"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = false;
    }

    button.setSelected = function (state) {
        if (this.groupName && state)
            for (var name in this.jsObject.options.buttons) {
                if (this.groupName == this.jsObject.options.buttons[name].groupName) {
                    this.jsObject.options.buttons[name].setSelected(false);
                }
            }

        this.isSelected = state;
        this.parentElement.className = state ? "stiDesignerToolButtonSelectedCell" : "stiDesignerToolButtonCell";
        this.className = (state ? this.styles["selected"] : this.styles["default"]) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    return button;
} 