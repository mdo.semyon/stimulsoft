﻿
StiMobileDesigner.prototype.ExecuteAction = function (name) {
    var jsObject = this;
    switch (name) {
        case "newReport":
            {
                var newReportPanel = this.options.newReportPanel || this.InitializeNewReportPanel();
                newReportPanel.changeVisibleState(true);
                break;
            }
        case "infoReport":
            {
                var infoReportPanel = this.options.infoReportPanel || this.InitializeInfoReportPanel();
                infoReportPanel.changeVisibleState(true);
                break;
            }        
        case "blankReportButton":
            {
                var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
                fileMenu.changeVisibleState(false);
                setTimeout(function () {
                    if (jsObject.options.cloudParameters && jsObject.options.cloudParameters.thenOpenWizard) {
                        jsObject.options.cloudParameters.thenOpenWizard = false;
                    }
                    else {
                        jsObject.ActionNewReport();
                    }
                }, 200);
                break;
            }
        case "standartReportButton":
        case "masterDetailReportButton":
            {
                this.InitializeWizardForm(function (wizardForm) {
                    wizardForm.typeReport = name == "masterDetailReportButton" ? "MasterDetail" : "Standart";
                    var showWizard = function () {
                        var dataSources = jsObject.options.report ? jsObject.GetDataSourcesFromDictionary(jsObject.options.report.dictionary) : null;
                        wizardForm.dataSourcesFromServer = dataSources;
                        wizardForm.changeVisibleState(true);
                    }
                    if (!jsObject.options.report) {
                        jsObject.SendCommandCreateReport(function () {
                            showWizard();
                            jsObject.options.toolBar.changeVisibleState(false);
                            jsObject.options.workPanel.changeVisibleState(false);
                        });
                    }
                    else {
                        showWizard();
                    }
                });
                break;
            }
        case "openReport":
            {
                if (this.options.cloudMode) {
                    var openPanel = this.options.openPanel || this.InitializeOpenPanel();
                    openPanel.changeVisibleState(true);
                }
                else {
                    var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
                    fileMenu.changeVisibleState(false);
                    setTimeout(function () { jsObject.ActionOpenReport(); }, 200);
                }
                break;
            }
        case "saveReport":
            {
                var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();

                if (this.options.cloudMode && !this.options.cloudParameters.reportTemplateItemKey && !this.options.report.properties.reportFile) {
                    fileMenu.action(fileMenu.items.saveAsReport);
                    fileMenu.items.saveReport.setSelected(true);
                    if (this.options.saveAsPanel) this.options.saveAsPanel.header.innerHTML = this.loc.A_WebViewer.SaveReport;
                }
                else {
                    fileMenu.changeVisibleState(false);
                    setTimeout(function () { jsObject.ActionSaveReport(); }, 200);
                }
                break;
            }
        case "saveAsReport":
            {
                if (this.options.cloudMode) {
                    var saveAsPanel = this.options.saveAsPanel || this.InitializeSaveAsPanel();
                    saveAsPanel.changeVisibleState(true);
                }
                else {
                    var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
                    fileMenu.changeVisibleState(false);
                    setTimeout(function () { jsObject.ActionSaveAsReport(); }, 200);
                }
                break;
            }
        case "closeReport":
            {
                var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
                fileMenu.changeVisibleState(false);
                setTimeout(function () { jsObject.ActionCloseReport(); }, 200);
                break;
            }
        case "exitDesigner":
            {
                if (this.options.isJava)
                    window.history.back();
                else {
                    var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
                    fileMenu.changeVisibleState(false);
                    setTimeout(function () { jsObject.ActionExitDesigner(); }, 200);
                    break;
                }
            }
        case "closeFileMenu":
            {
                var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
                fileMenu.changeVisibleState(false);
                break;
            }
        case "aboutDesigner":
            {
                var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
                fileMenu.changeVisibleState(false);
                setTimeout(function () {
                    var aboutPanel = jsObject.options.aboutPanel || jsObject.InitializeAboutPanel();
                    aboutPanel.changeVisibleState(true);
                }, 200);
                break;
            }
        case "help":
            {
                var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
                fileMenu.changeVisibleState(false);
                setTimeout(function () {
                    var lang = jsObject.options.helpLanguage == "ru" ? "ru" : "en";
                    jsObject.openNewWindow("https://www.stimulsoft.com/" + lang + "/documentation/online/user-manual/index.html?reports_designer.htm");
                }, 200);
                break;
            }
        case "fileButton":
            {
                var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
                fileMenu.changeVisibleState(true);
                break;
            }
        case "homeToolButton":
            {
                this.options.workPanel.showPanel(this.options.homePanel);
                this.options.workPanel.changeVisibleState(true);
                break;
            }
        case "insertToolButton":
            {
                if (!this.options.insertPanel) this.InitializeInsertPanel();
                this.options.workPanel.showPanel(this.options.insertPanel);
                this.options.workPanel.changeVisibleState(true);
                break;
            }
        case "pageToolButton":
            {
                if (!this.options.pagePanel) this.InitializePagePanel();
                this.options.pagePanel.updateControls();
                this.options.workPanel.showPanel(this.options.pagePanel);
                this.options.workPanel.changeVisibleState(true);
                break;
            }
        case "layoutToolButton":
            {
                if (!this.options.layoutPanel) this.InitializeLayoutPanel();
                this.options.layoutPanel.updateControls();
                this.options.workPanel.showPanel(this.options.layoutPanel);
                this.options.workPanel.changeVisibleState(true);
                break;
            }
        case "previewToolButton":
            {
                if (this.options.previewMode) return;
                if (!this.options.previewPanel) this.InitializePreviewPanel();

                if (this.options.cloudMode) {
                    if (this.options.viewerContainer) this.options.viewerContainer.addFrame();
                    if (this.options.buttons.previewToolButton) {
                        this.options.buttons.previewToolButton.progress.style.visibility = "visible";
                    }
                    this.options.workPanel.showPanel(this.options.previewPanel);
                    var reportName = this.options.report.properties.reportFile || "Report.mrt";
                    reportName = reportName.substring(0, reportName.length - 4);
                    if (this.options.cloudParameters.reportTemplateItemKey) {
                        reportName = this.options.cloudParameters.reportName;
                    }                    
                    this.ShowReportInTheViewer(reportName);
                }
                else {
                    if (!this.options.viewer && !this.options.mvcMode && !this.options.jsMode) {
                        var jsObject = this;
                        var processImage = this.options.processImage || this.InitializeProcessImage();
                        processImage.show();
                        
                        var viewerParameters = window["js" + jsObject.options.viewerId + "Parameters"];
                        var createViewer = function (viewerParameters) {
                            if (viewerParameters) {
                                var jsViewer = window["js" + jsObject.options.viewerId] = new StiMobileViewer(viewerParameters);
                                jsObject.options.viewer = jsViewer.options.mobileViewer;
                                jsObject.options.viewerContainer.appendChild(jsObject.options.viewer);
                                jsObject.options.viewer.style.display = "";
                                jsViewer.options.jsDesigner = jsObject;
                            }
                            jsObject.options.workPanel.showPanel(jsObject.options.previewPanel);
                            jsObject.options.workPanel.changeVisibleState(true);
                            jsObject.options.processImage.hide();
                            jsObject.SendCommandLoadReportToViewer();
                        }

                        if (typeof (StiMobileViewer) != 'undefined') {
                            createViewer(viewerParameters);
                        }
                        else {
                            this.LoadStyle(this.options.stylesUrl.replace("MobileDesignerStyleName", "Viewer"));
                            this.LoadScript(this.options.scriptsUrl.replace("MobileDesignerScriptName", "Viewer"), function () {
                                createViewer(viewerParameters);
                            });
                        }
                    }
                    else {
                        this.options.workPanel.showPanel(this.options.previewPanel);
                        this.options.workPanel.changeVisibleState(true);
                        this.SendCommandLoadReportToViewer();
                    }
                }
                break;
            }
        case "zoomIn":
            {
                var zoom = Math.round(this.options.report.zoom * 10) / 10;
                if (zoom <= 1.9) {
                    this.options.report.zoom = zoom > this.options.report.zoom ? zoom : zoom + 0.1;
                    this.PreZoomPage(this.options.currentPage);
                }
                else {
                    if (this.options.report.zoom != 2) {
                        this.options.report.zoom = 2;
                        this.PreZoomPage(this.options.currentPage);
                    }
                }
                break;
            }
        case "zoomOut":
            {
                var zoom = Math.round(this.options.report.zoom * 10) / 10;
                if (zoom >= 0.2) {
                    this.options.report.zoom = zoom < this.options.report.zoom ? zoom : zoom - 0.1;
                    this.PreZoomPage(this.options.currentPage);
                }
                else {
                    if (this.options.report.zoom != 0.1) {
                        this.options.report.zoom = 0.1;
                        this.PreZoomPage(this.options.currentPage);
                    }
                }
                break;
            }
        case "unitButton":
            {
                this.options.menus.unitMenu.changeVisibleState(!this.options.menus.unitMenu.visible);
                break;
            }
        case "insertBands":
            {
                this.options.menus.bandsMenu.changeVisibleState(!this.options.menus.bandsMenu.visible);
                break;
            }
        case "insertCrossBands":
            {
                this.options.menus.crossBandsMenu.changeVisibleState(!this.options.menus.crossBandsMenu.visible);
                break;
            }
        case "insertComponents":
            {
                this.options.menus.componentsMenu.changeVisibleState(!this.options.menus.componentsMenu.visible);
                break;
            }
        case "insertShapes":
            {
                this.options.menus.shapesMenu.changeVisibleState(!this.options.menus.shapesMenu.visible);
                break;
            }
        case "insertInfographics":
            {
                this.options.menus.infographicsMenu.changeVisibleState(!this.options.menus.infographicsMenu.visible);
                break;
            }
        case "insertPanelAddPage":
        case "addPage":
            {
                this.SendCommandAddPage(this.options.currentPage.properties.pageIndex);
                break;
            }
        case "removePage":
            {
                this.options.currentPage.remove();
                break;
            }
        case "duplicatePage":
            {
                this.SendCommandDuplicatePage(this.options.currentPage.properties.pageIndex);
                break;
            }
        case "groupBlockPageSetupButton":
            {
                this.InitializePageSetupForm(function (pageSetupForm) {
                    pageSetupForm.changeVisibleState(true);
                });
                break;
            }
        case "removeComponent":
            {
                if (this.options.selectedObjects) this.RemoveComponent(this.options.selectedObjects);
                else if (this.options.selectedObject) this.options.selectedObject.remove();
                break;
            }
        case "copyComponent":
            {
                if (this.options.selectedObjects) this.CopyComponent(this.options.selectedObjects);
                else if (this.options.selectedObject) this.options.selectedObject.copy();
                break;
            }
        case "cutComponent":
            {
                if (this.options.selectedObjects) this.CutComponent(this.options.selectedObjects);
                else if (this.options.selectedObject) this.options.selectedObject.cut();
                break;
            }
        case "pasteComponent":
            {
                this.SendCommandGetFromClipboard();
                break;
            }
        case "aboutButton":
            {
                var aboutPanel = this.options.aboutPanel || this.InitializeAboutPanel();
                aboutPanel.changeVisibleState(true);
                break;
            }
        case "showToolBarButton":
            {
                this.options.workPanel.changeVisibleState(true);
                this.options.workPanel.visibleState = true;
                break;
            }
        case "hideToolbarButton":
            {
                this.options.workPanel.changeVisibleState(false);
                this.options.workPanel.visibleState = false;
                break;
            }        
        case "undoButton":
            {
                this.SendCommandUndo();
                break;
            }
        case "redoButton":
            {
                this.SendCommandRedo();
                break;
            }
        case "resizeDesigner":
            {
                this.ResizeDesigner();
                break;
            }
        case "groupBlockBordersButton":
            {
                this.InitializeBorderSetupForm(function (borderSetupForm) {
                    borderSetupForm.showFunction = null;
                    borderSetupForm.actionFunction = null;
                    borderSetupForm.changeVisibleState(true);
                });
                break;
            }
        case "reportSetup":
            {                
                setTimeout(function () {
                    jsObject.InitializeReportSetupForm(function (reportSetupForm) {
                        reportSetupForm.show();
                    });
                }, 200);
                break;
            }
        case "pageSetup":
            {
                this.InitializePageSetupForm(function (pageSetupForm) {
                    pageSetupForm.changeVisibleState(true);
                });
                break;
            }
        case "pageMoveLeft":
            {
                this.SendCommandPageMove("Left", this.options.currentPage.properties.pageIndex);
                break;
            }
        case "pageMoveRight":
            {
                this.SendCommandPageMove("Right", this.options.currentPage.properties.pageIndex);
                break;
            }
        case "optionsDesigner":
            {
                var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
                fileMenu.changeVisibleState(false);
                setTimeout(function () {
                    jsObject.InitializeOptionsForm(function (optionsForm) {
                        optionsForm.show();
                    });
                }, 200);
                break;
            }
        case "groupBlockReportButton":
            {
                jsObject.InitializeReportSetupForm(function (reportSetupForm) {
                    reportSetupForm.show();
                });
                break;
            }
    }
}