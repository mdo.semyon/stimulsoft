﻿
StiMobileDesigner.prototype.ShapesMenu = function () {
    var menu = this.VerticalMenu("shapesMenu", this.options.buttons.insertShapes, "Down", null, this.GetStyles("MenuStandartItem"));
    var hideParentButton = true;
    menu.innerContent.style.minWidth = "200px";

    var basicShapesHeader = this.ShapesMenuHeader(this.loc.Shapes.BasicShapes);
    basicShapesHeader.style.display = "none";
    menu.innerContent.appendChild(basicShapesHeader);

    var basicShapes = ["StiHorizontalLinePrimitive", "StiVerticalLinePrimitive", "StiRectanglePrimitive", "StiRoundedRectanglePrimitive"];
    
    for (var i = 0; i < basicShapes.length; i++) {
        if (this.options.visibilityComponents[basicShapes[i]]) {
            basicShapesHeader.style.display = "";
            hideParentButton = false;
            var button = this.ShapesMenuButton(menu, basicShapes[i], "Shapes." + basicShapes[i] + ".png", this.loc.HelpComponents[basicShapes[i]]);
            menu.innerContent.appendChild(button);
        }
    }   

    if (this.options.visibilityComponents.StiShape) {
        hideParentButton = false;

        var otherShapes = [
            {
                category: this.loc.Shapes.EquationShapes,
                items: ["StiPlusShapeType", "StiMinusShapeType", "StiMultiplyShapeType", "StiDivisionShapeType", "StiEqualShapeType"]
            },
            {
                category: this.loc.Shapes.BlockArrows,
                items: ["StiArrowShapeTypeRight", "StiArrowShapeTypeLeft", "StiArrowShapeTypeUp", "StiArrowShapeTypeDown", "StiComplexArrowShapeType",
                    "StiFlowchartSortShapeType", "StiBentArrowShapeType", "StiChevronShapeType"]
            },
            {
                category: this.loc.Shapes.Lines,
                items: ["StiDiagonalUpLineShapeType", "StiDiagonalDownLineShapeType", "StiHorizontalLineShapeType", "StiLeftAndRightLineShapeType",
                    "StiTopAndBottomLineShapeType", "StiVerticalLineShapeType"]
            },
            {
                category: this.loc.Shapes.Flowchart,
                items: ["StiOvalShapeType", "StiRectangleShapeType", "StiTriangleShapeType", "StiFlowchartCardShapeType", "StiFlowchartCollateShapeType",
                    "StiFlowchartDecisionShapeType", "StiFlowchartManualInputShapeType", "StiFlowchartOffPageConnectorShapeType", "StiFlowchartPreparationShapeType",
                    "StiFrameShapeType", "StiParallelogramShapeType", "StiRegularPentagonShapeType", "StiTrapezoidShapeType", "StiSnipSameSideCornerRectangleShapeType",
                    "StiSnipDiagonalSideCornerRectangleShapeType"]
            }
        ]

        for (var i = 0; i < otherShapes.length; i++) {
            var header = this.ShapesMenuHeader(otherShapes[i].category);
            menu.innerContent.appendChild(header);

            for (var k = 0; k < otherShapes[i].items.length; k++) {
                var toolTip = otherShapes[i].items[k].replace("ShapeType", "").replace("Sti", "");
                var button = this.ShapesMenuButton(menu, "StiShape;" + otherShapes[i].items[k], "Shapes." + otherShapes[i].items[k] + ".png", toolTip);
                menu.innerContent.appendChild(button);
            }
        }
    }

    if (hideParentButton) this.options.buttons.insertShapes.style.display = "none";

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.options.insertPanel.resetChoose();
        this.jsObject.options.insertPanel.setChoose(menuItem);
    }

    return menu;
}

StiMobileDesigner.prototype.ShapesMenuHeader = function (text) {
    var header = document.createElement("div");
    header.innerHTML = text;
    header.className = "stiShapesMenuHeader";
    
    return header;
}

StiMobileDesigner.prototype.ShapesMenuButton = function (menu, name, imageName, toolTip) {
    var button = this.StandartSmallButton(null, null, null, imageName, toolTip, null, true);
    button.style.display = "inline-block";
    button.name = name;
    button.style.width = button.style.height = "25px";
    button.innerTable.style.width = "100%";

    this.AddDragEventsToComponentButton(button);

    button.action = function () {
        menu.action(this);
    }

    return button;
}