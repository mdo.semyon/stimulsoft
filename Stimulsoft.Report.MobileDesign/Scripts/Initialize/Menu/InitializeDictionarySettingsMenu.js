﻿
StiMobileDesigner.prototype.InitializeDictionarySettingsMenu = function () {
    var dictionarySettingsMenu = this.VerticalMenu("dictionarySettingsMenu", this.options.propertiesPanel.dictionaryToolBar.controls["Settings"], "Down", null, this.GetStyles("MenuStandartItem"));
    dictionarySettingsMenu.controls = {};

    var settingsControls = [
        ["createFieldOnDoubleClick", this.CheckBox(null, this.loc.PropertyMain.CreateFieldOnDoubleClick), "8px"],
        ["createLabel", this.CheckBox(null, this.loc.PropertyMain.CreateLabel), "0px 8px 8px 8px"],
        ["useAliases", this.CheckBox(null, this.loc.PropertyMain.UseAliases), "0px 8px 8px 8px"]
    ]

    var setSettingsToCookie = function () {
        var settings = {};
        for (var i = 0; i < settingsControls.length; i++) {
            settings[settingsControls[i][0]] = dictionarySettingsMenu.controls[settingsControls[i][0]].isChecked;
        }
        dictionarySettingsMenu.jsObject.SetCookie("StiMobileDesignerDictionarySettings", JSON.stringify(settings));
    }

    for (var i = 0; i < settingsControls.length; i++) {
        var control = settingsControls[i][1];
        dictionarySettingsMenu.controls[settingsControls[i][0]] = control;
        control.style.margin = settingsControls[i][2];
        dictionarySettingsMenu.innerContent.appendChild(control);
        control.action = function () {
            setSettingsToCookie();
        }
    }

    var settings = {
        createFieldOnDoubleClick: false,
        createLabel: false,
        useAliases: false
    };

    var jsonSettings = this.GetCookie("StiMobileDesignerDictionarySettings");
    if (jsonSettings) settings = JSON.parse(jsonSettings);

    for (var i = 0; i < settingsControls.length; i++) {
        dictionarySettingsMenu.controls[settingsControls[i][0]].setChecked(settings[settingsControls[i][0]]);
    }

    dictionarySettingsMenu.controls.useAliases.style.display = "none"; //Temporary

    return dictionarySettingsMenu;
}