﻿
StiMobileDesigner.prototype.InitializePropertiesSettingsMenu = function () {
    var menu = this.VerticalMenu("propertiesSettingsMenu", this.options.propertiesPanel.propertiesToolBar.controls["Settings"], "Down", null, this.GetStyles("MenuStandartItem"));
    menu.controls = {};
    
    var settingsControls = [
        ["localizeProperties", this.CheckBox(null, this.loc.FormDesigner.LocalizePropertyGrid), "8px"]
    ]
    
    for (var i = 0; i < settingsControls.length; i++) {
        var control = settingsControls[i][1];
        menu.controls[settingsControls[i][0]] = control;
        control.style.margin = settingsControls[i][2];
        menu.innerContent.appendChild(control);
    }
    
    menu.controls.localizeProperties.setChecked(this.options.propertiesPanel.localizePropertyGrid);

    menu.controls.localizeProperties.action = function () {
        this.jsObject.options.propertiesPanel.localizePropertyGrid = this.isChecked;
        this.jsObject.SetCookie("StimulsoftMobileDesignerLocalizePropertyGrid", this.isChecked ? "true" : "false");
        this.jsObject.options.propertiesPanel.updatePropertiesCaptions();
    }


    return menu;
}