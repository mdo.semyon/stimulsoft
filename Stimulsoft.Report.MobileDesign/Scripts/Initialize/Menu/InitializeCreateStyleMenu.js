﻿
StiMobileDesigner.prototype.InitializeAddStyleMenu = function () {

    var menu = this.VerticalMenu("addStyleMenu", this.options.buttons.addStyle, "Down", this.GetAddStyleMenuItems(), this.GetStyles("MenuMiddleItem"))

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
            styleDesignerForm.addStyle(menuItem.key);
        });
    }

    return menu;
}