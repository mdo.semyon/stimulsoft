﻿
StiMobileDesigner.prototype.InitializeDictionaryContextMenu = function () {

    var items = this.GetDictionaryNewItems();
    items.push("separator3_0");
    items.push(this.Item("viewData", this.loc.FormDictionaryDesigner.ViewData, "Query.ViewData.png", "viewData"));
    items.push("separator3");
    items.push(this.Item("editItem", this.loc.QueryBuilder.Edit, "Edit.png", "editItem"));
    items.push(this.Item("deleteItem", this.loc.MainMenu.menuEditDelete.replace("&", ""), "Remove.png", "deleteItem"));
    items.push("separator4");
    items.push(this.Item("expandAll", this.loc.Report.ExpandAll, "ExpandAll.png", "expandAll"));
    items.push(this.Item("collapseAll", this.loc.Report.CollapseAll, "CollapseAll.png", "collapseAll"));

    var menu = this.BaseContextMenu("dictionaryContextMenu", "Up", items, this.GetStyles("MenuStandartItem"));

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        switch (menuItem.key) {
            case "dataSourceNew":
                {
                    this.jsObject.InitializeSelectConnectionForm(function (selectConnectionForm) {
                        selectConnectionForm.changeVisibleState(true);
                    });
                    break;
                }
            case "businessObjectNew":
                {
                    this.jsObject.InitializeEditDataSourceForm(function (editDataSourceForm) {
                        editDataSourceForm.datasource = "BusinessObject";
                        editDataSourceForm.changeVisibleState(true);
                    });
                    break;
                }
            case "relationNew":
                {
                    this.jsObject.InitializeEditRelationForm(function (editRelationForm) {
                        editRelationForm.relation = null;
                        editRelationForm.changeVisibleState(true);
                    });
                    break;
                }
            case "columnNew":
            case "calcColumnNew":
                {
                    this.jsObject.InitializeEditColumnForm(function (editColumnForm) {
                        editColumnForm.column = menuItem.key == "columnNew" ? "column" : "calcColumn";
                        editColumnForm.changeVisibleState(true);
                    });
                    break;
                }
            case "parameterNew":
                {
                    this.jsObject.InitializeEditParameterForm(function (editParameterForm) {
                        editParameterForm.parameter = null;
                        editParameterForm.changeVisibleState(true);
                    });
                    break;
                }
            case "variableNew":
                {
                    this.jsObject.InitializeEditVariableForm(function (editVariableForm) {
                        editVariableForm.variable = null;
                        editVariableForm.changeVisibleState(true);
                    });
                    break;
                }
            case "categoryNew":
                {
                    this.jsObject.InitializeEditCategoryForm(function (editCategoryForm) {
                        editCategoryForm.category = null;
                        editCategoryForm.changeVisibleState(true);
                    });
                    break;
                }
            case "resourceNew":
                {
                    if (this.jsObject.options.dictionaryPanel.checkResourcesCount()) return;
                    this.jsObject.InitializeEditResourceForm(function (editResourceForm) {
                        editResourceForm.resource = null;
                        editResourceForm.changeVisibleState(true);
                    });
                    break;
                }
            case "editItem": { this.jsObject.EditItemDictionaryTree(); break; }
            case "deleteItem": { this.jsObject.DeleteItemDictionaryTree(); break; }
            case "expandAll": { this.jsObject.SetOpeningAllChildDictionaryTree(true); break; }
            case "collapseAll": { this.jsObject.SetOpeningAllChildDictionaryTree(false); break; }
            case "viewData":
                {
                    var selectedItemObject = this.jsObject.options.dictionaryTree.selectedItem.itemObject;
                    var jsObject = this.jsObject;

                    if (selectedItemObject.typeItem == "DataSource") {
                        jsObject.InitializeEditDataSourceForm(function (editDataSourceForm) {
                            editDataSourceForm.datasource = jsObject.CopyObject(selectedItemObject);
                            editDataSourceForm.onshow();
                            editDataSourceForm.checkParametersValuesAndShowValuesForm(function (parameters) {
                                editDataSourceForm.jsObject.SendCommandViewData(parameters);
                            });
                        });
                    }
                    break;
                }
        }
    }

    return menu;
}