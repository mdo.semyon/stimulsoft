﻿
StiMobileDesigner.prototype.InitializeToolboxInfographicsMenu = function () {
    var menu = this.HorizontalMenu("toolboxInfographicsMenu", this.options.toolbox.buttons.infographics, "Right", this.GetAddSeriesItems(), this.GetStyles("MenuMiddleItem"));

    menu.type = "Menu";
    menu.firstChild.style.maxHeight = "1000px";

    this.InitializeSubMenu("toolboxInfographicsClusteredColumnMenu", this.GetChartClusteredColumnItems(), menu.items["ClusteredColumn"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsLineMenu", this.GetChartLineItems(), menu.items["Line"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsAreaMenu", this.GetChartAreaItems(), menu.items["Area"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsRangeMenu", this.GetChartRangeItems(), menu.items["Range"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsClusteredBarMenu", this.GetChartClusteredBarItems(), menu.items["ClusteredBar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsScatterMenu", this.GetChartScatterItems(), menu.items["Scatter"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsPieMenu", this.GetChartPieItems(), menu.items["Pie"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsRadarMenu", this.GetChartRadarItems(), menu.items["Radar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsFunnelMenu", this.GetChartFunnelItems(), menu.items["Funnel"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsFinancialMenu", this.GetChartFinancialItems(), menu.items["Financial"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsOthersMenu", this.GetChartOthersItems(), menu.items["Others"], menu, "MenuMiddleItem");

    if (this.options.toolbox.buttons.infographics)
        this.options.toolbox.buttons.infographics.style.display = this.options.visibilityComponents.StiChart ? "" : "none";

    menu.action = function (menuItem) {
        menuItem.name = "Infographic;StiChart;" + menuItem.key;        
        menu.changeVisibleState(false);

        if (this.jsObject.options.insertPanel) {
            this.jsObject.options.insertPanel.resetChoose();
        }

        this.jsObject.options.drawComponent = true;
        this.jsObject.options.paintPanel.setCopyStyleMode(false);
        this.jsObject.options.paintPanel.changeCursorType(true);

        if (this.jsObject.options.toolbox)
            this.jsObject.options.toolbox.selectedComponent = menuItem;
    }

    return menu;
}
