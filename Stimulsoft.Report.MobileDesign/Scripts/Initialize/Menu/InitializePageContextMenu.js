﻿
StiMobileDesigner.prototype.InitializePageContextMenu = function () {

    var items = [];
    items.push(this.Item("addPage", this.loc.MainMenu.menuFilePageNew, "AddPage.png", "addPage"));
    items.push(this.Item("removePage", this.loc.Buttons.Delete, "Remove.png", "removePage"));
    items.push(this.Item("duplicatePage", this.loc.Buttons.Duplicate, "Duplicate.png", "duplicatePage"));
    items.push(this.Item("renamePage", this.loc.Buttons.Rename, " ", "renamePage"));    
    items.push("separator1");
    items.push(this.Item("pageMoveLeft", this.loc.Buttons.MoveLeft, "PageMoveLeft.png", "pageMoveLeft"));
    items.push(this.Item("pageMoveRight", this.loc.Buttons.MoveRight, "PageMoveRight.png", "pageMoveRight"));
    items.push("separator2");
    items.push(this.Item("pageSetup", this.loc.Toolbars.ToolbarPageSetup, "PageSetup.png", "pageSetup"));

    var menu = this.BaseContextMenu("pageContextMenu", "Up", items, this.GetStyles("MenuStandartItem"));

    menu.action = function (menuItem) {
        if (menuItem.key == "renamePage") {
            this.pageButton.setEditMode(true);
        }
        else {
            this.jsObject.ExecuteAction(menuItem.key);
        }
        this.changeVisibleState(false);
    }

    menu.onshow = function () {
        var pagesCount = this.jsObject.options.paintPanel.getPagesCount();
        var currentPage = this.jsObject.options.currentPage;
        if (currentPage) {
            var pageIndex = this.jsObject.StrToInt(currentPage.properties.pageIndex);
            this.items["pageMoveLeft"].setEnabled(pageIndex > 0);
            this.items["pageMoveRight"].setEnabled(pageIndex < pagesCount - 1);
            this.items["removePage"].setEnabled(pagesCount > 1);
        }
    }

    return menu;
}