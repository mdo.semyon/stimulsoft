﻿
StiMobileDesigner.prototype.CrossBandsMenu = function () {
    var componentTypes = ["StiCrossGroupHeaderBand", "StiCrossGroupFooterBand", "StiCrossHeaderBand", "StiCrossFooterBand", "StiCrossDataBand"];;

    var items = [];

    for (var i = 0; i < componentTypes.length; i++) {
        if (this.options.visibilityCrossBands[componentTypes[i]]) {
            items.push(this.Item(componentTypes[i], this.loc.Components[componentTypes[i]], "SmallComponents." + componentTypes[i] + ".png", componentTypes[i]));
        }
    }

    if (items.length == 0) parentButton.style.display = "none";

    var menu = this.VerticalMenu("crossBandsMenu", this.options.buttons.insertCrossBands, "Down", items, this.GetStyles("MenuStandartItem"));

    for (var i in menu.items) {
        this.AddDragEventsToComponentButton(menu.items[i]);
        menu.items[i].setAttribute("title", this.loc.HelpComponents[i]);
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.options.insertPanel.resetChoose();
        this.jsObject.options.insertPanel.setChoose(menuItem);
    }

    return menu;
}