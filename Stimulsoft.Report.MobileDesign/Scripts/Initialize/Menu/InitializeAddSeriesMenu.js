﻿
StiMobileDesigner.prototype.InitializeAddSeriesMenu = function (editChartForm) {

    var menu = this.VerticalMenu("addSeriesMenu", editChartForm.seriesToolBar.addSeries, "Down", this.GetAddSeriesItems(), this.GetStyles("MenuMiddleItem"))

    this.InitializeSubMenu("chartClusteredColumnMenu", this.GetChartClusteredColumnItems(), menu.items["ClusteredColumn"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("chartLineMenu", this.GetChartLineItems(), menu.items["Line"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("chartAreaMenu", this.GetChartAreaItems(), menu.items["Area"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("chartRangeMenu", this.GetChartRangeItems(), menu.items["Range"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("chartClusteredBarMenu", this.GetChartClusteredBarItems(), menu.items["ClusteredBar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("chartScatterMenu", this.GetChartScatterItems(), menu.items["Scatter"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("chartPieMenu", this.GetChartPieItems(), menu.items["Pie"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("chartRadarMenu", this.GetChartRadarItems(), menu.items["Radar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("chartFunnelMenu", this.GetChartFunnelItems(), menu.items["Funnel"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("chartFinancialMenu", this.GetChartFinancialItems(), menu.items["Financial"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("chartOthersMenu", this.GetChartOthersItems(), menu.items["Others"], menu, "MenuMiddleItem");

    menu.update = function () {
        var types = editChartForm.chartProperties.typesCollection;

        for (var mainItemName in menu.items) {
            if (mainItemName == "separator") continue;
            var mainItem = menu.items[mainItemName];
            var categoryEnabled = false;
            for (subItemName in mainItem.menu.items) {
                if (subItemName == "separator") continue;
                var subItem = mainItem.menu.items[subItemName];
                var chartType = subItem.key;
                var finded = false;

                for (var i in types) {
                    var type = this.jsObject.options.isJava ? types[i].substring(types[i].lastIndexOf(".") + 1) : types[i];
                    if (type == chartType) {
                        finded = true;
                        categoryEnabled = true;
                        break;
                    }
                }

                if (editChartForm.seriesContainer.items.length == 0) {
                    subItem.setEnabled(true);
                    categoryEnabled = true;
                }
                else
                    subItem.setEnabled(finded);

            }
            mainItem.setEnabled(categoryEnabled);
        }
    }

    menu.onshow = function () {
        menu.update();
    }

    return menu;
}

StiMobileDesigner.prototype.InitializeSubMenu = function (name, items, parentButton, parentMenu, style) {

    var menu = this.HorizontalMenu(name, parentButton, "Right", items, this.GetStyles(style || "MenuStandartItem"));
    menu.parentMenu = parentMenu;
    parentButton.menu = menu;

    parentButton.showSubMenu = function () {
        if (parentMenu.currentSubMenu && parentButton.menu != parentMenu.currentSubMenu) {
            parentMenu.currentSubMenu.changeVisibleState(false);
        }
        if (!parentButton.menu.visible) {
            parentButton.menu.changeVisibleState(true);
            parentMenu.currentSubMenu = parentButton.menu;
        }
    }

    parentButton.action = function () {
        this.onmouseover();
    }

    if (!this.options.isTouchDevice) {
        parentButton.onmouseover = function () {
            if (!this.isEnabled) return;
            this.className = this.styles["over"] + "_Mouse";
            this.isOver = true;
            clearTimeout(parentMenu.subMenuShowTimer);
            parentMenu.subMenuShowTimer = setTimeout(function () {
                if (parentButton.isOver) parentButton.showSubMenu();
            }, 200);
        }
    }

    menu.action = function (menuItem) {
        menu.changeVisibleState(false);
        parentMenu.changeVisibleState(false);
        parentMenu.action(menuItem);
    }

    return menu;
}