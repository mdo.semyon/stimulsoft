﻿
StiMobileDesigner.prototype.InitializeToolboxBandsMenu = function (parentButton) {    

    var componentTypes = ["StiReportTitleBand", "StiReportSummaryBand", "StiPageHeaderBand", "StiPageFooterBand", "StiGroupHeaderBand",
        "StiGroupFooterBand", "StiHeaderBand", "StiFooterBand", "StiColumnHeaderBand", "StiColumnFooterBand", "StiDataBand", "StiHierarchicalBand",
        "StiChildBand", "StiEmptyBand", "StiOverlayBand"];

    var items = [];

    for (var i = 0; i < componentTypes.length; i++) {
        if (this.options.visibilityBands[componentTypes[i]]) {
            items.push(this.Item(componentTypes[i], this.loc.Components[componentTypes[i]], "SmallComponents." + componentTypes[i] + ".png", componentTypes[i]));
        }
    }

    if (items.length == 0) parentButton.style.display = "none";

    var menu = this.HorizontalMenu("toolboxBandsMenu", parentButton, "Right", items, this.GetStyles("MenuStandartItem"));

    for (var i in menu.items) {
        this.AddDragEventsToComponentButton(menu.items[i]);
        menu.items[i].setAttribute("title", this.loc.HelpComponents[i]);
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.options.toolbox.resetChoose();
        this.jsObject.options.toolbox.setChoose(menuItem);
    }

    return menu;
}