﻿
StiMobileDesigner.prototype.InitializeAddGaugeItemsMenu = function (editGaugeForm) {
    var menu = this.VerticalMenu("addGaugeItemsMenu", editGaugeForm.gaugeToolBar.addGaugeItem, "Down", this.GetAddGaugeItems(), this.GetStyles("MenuMiddleItem"))
    
    this.InitializeSubMenu("gaugeRadialScaleMenu", this.GetGaugeRadialScaleItems(), menu.items["ScaleRadial"], menu, "MenuMiddleItem").innerContent.style.maxHeight = "600px";
    this.InitializeSubMenu("gaugeLinearScaleMenu", this.GetGaugeLinearScaleItems(), menu.items["ScaleLinear"], menu, "MenuMiddleItem").innerContent.style.maxHeight = "600px";

    for (var name in menu.items) {
        if (name.indexOf("separator") < 0 && !menu.items[name].haveSubMenu) {
            menu.items[name].onmouseover = function () {
                var menuItem = this;
                if (this.jsObject.options.isTouchDevice || !this.isEnabled) return;
                this.className = this.styles["over"] + "_Mouse";
                this.isOver = true;
                clearTimeout(menu.subMenuShowTimer);
                menu.subMenuShowTimer = setTimeout(function () {
                    if (menuItem.isOver && menu.currentSubMenu) {
                        menu.currentSubMenu.changeVisibleState(false);
                    }
                }, 200);
            }
        }
    }

    //menu.update = function () {
    //    var types = editGaugeForm.chartProperties.typesCollection;

    //    for (var mainItemName in menu.items) {
    //        if (mainItemName == "separator") continue;
    //        var mainItem = menu.items[mainItemName];
    //        var categoryEnabled = false;
    //        for (subItemName in mainItem.menu.items) {
    //            if (subItemName == "separator") continue;
    //            var subItem = mainItem.menu.items[subItemName];
    //            var chartType = subItem.key;
    //            var finded = false;

    //            for (var i in types) {
    //                var type = this.jsObject.options.isJava ? types[i].substring(types[i].lastIndexOf(".") + 1) : types[i];
    //                if (type == chartType) {
    //                    finded = true;
    //                    categoryEnabled = true;
    //                    break;
    //                }
    //            }

    //            if (editChartForm.seriesContainer.items.length == 0) {
    //                subItem.setEnabled(true);
    //                categoryEnabled = true;
    //            }
    //            else
    //                subItem.setEnabled(finded);

    //        }
    //        mainItem.setEnabled(categoryEnabled);
    //    }
    //}

    menu.onshow = function () {
        //    menu.update();
    }

    return menu;
}