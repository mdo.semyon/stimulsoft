﻿
StiMobileDesigner.prototype.InitializeApplyStylesMenu = function () {

    var styleDesignerForm = this.options.forms.styleDesignerForm;
    var menu = this.VerticalMenu("applyStylesMenu", styleDesignerForm.toolBar.applyStyles, "Down", null, this.GetStyles("MenuStandartItem"))

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        if (menuItem.key != "StyleCollectionsNotFound") {
            this.jsObject.SendCommandUpdateStyles(styleDesignerForm.getStylesCollection(), menuItem.key);
        }
    }

    menu.onshow = function () {
        this.addItems(styleDesignerForm.itemsContainer.getCollectionNameItems());
    }

    return menu;
}