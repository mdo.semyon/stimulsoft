﻿
StiMobileDesigner.prototype.InfographicsMenu = function () {
    var menu = this.VerticalMenu("infographicsMenu", this.options.buttons.insertInfographics, "Down", this.GetAddSeriesItems(), this.GetStyles("MenuMiddleItem"));

    menu.firstChild.style.maxHeight = "1000px";

    this.InitializeSubMenu("infographicsClusteredColumnMenu", this.GetChartClusteredColumnItems(), menu.items["ClusteredColumn"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsLineMenu", this.GetChartLineItems(), menu.items["Line"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsAreaMenu", this.GetChartAreaItems(), menu.items["Area"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsRangeMenu", this.GetChartRangeItems(), menu.items["Range"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsClusteredBarMenu", this.GetChartClusteredBarItems(), menu.items["ClusteredBar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsScatterMenu", this.GetChartScatterItems(), menu.items["Scatter"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsPieMenu", this.GetChartPieItems(), menu.items["Pie"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsRadarMenu", this.GetChartRadarItems(), menu.items["Radar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsFunnelMenu", this.GetChartFunnelItems(), menu.items["Funnel"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsFinancialMenu", this.GetChartFinancialItems(), menu.items["Financial"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsOthersMenu", this.GetChartOthersItems(), menu.items["Others"], menu, "MenuMiddleItem");

    if (this.options.buttons["insertInfographics"])
        this.options.buttons["insertInfographics"].style.display = this.options.visibilityComponents.StiChart ? "" : "none";

    menu.action = function (menuItem) {
        menuItem.name = "Infographic;StiChart;" + menuItem.key;
        menu.changeVisibleState(false);

        if (this.jsObject.options.insertPanel) {
            this.jsObject.options.insertPanel.resetChoose();
        }

        this.jsObject.options.drawComponent = true;
        this.jsObject.options.paintPanel.setCopyStyleMode(false);
        this.jsObject.options.paintPanel.changeCursorType(true);

        if (this.jsObject.options.insertPanel)
            this.jsObject.options.insertPanel.selectedComponent = menuItem;
    }

    return menu;
}
