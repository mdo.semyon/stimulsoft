﻿
StiMobileDesigner.prototype.VerticalMenu = function (name, parentButton, animDirection, items, itemsStyles, cutMenu) {
    var menu = this.BaseMenu(name, parentButton, animDirection);
    menu.itemsStyles = itemsStyles;
    menu.cutMenu = cutMenu;

    menu.addItems = function (items) {
        while (this.innerContent.childNodes[0]) {
            this.innerContent.removeChild(this.innerContent.childNodes[0]);
        }
        for (var index in items) {
            if (typeof (items[index]) != "string") {
                var item = (items[index].styleProperties != null)
                    ? this.jsObject.VerticalMenuItemForStyles(this, items[index], this.itemsStyles)
                    : this.jsObject.VerticalMenuItem(this, items[index].name, items[index].caption, items[index].imageName, items[index].key, this.itemsStyles, items[index].haveSubMenu);
                this.innerContent.appendChild(item);
            }
            else
                this.innerContent.appendChild(this.jsObject.VerticalMenuSeparator(this, items[index]));
        }
    }
        
    menu.addItems(items);

    return menu;
}

StiMobileDesigner.prototype.VerticalMenuItem = function (menu, itemName, caption, imageName, key, styles, haveSubMenu) {
    var menuItem = document.createElement("div");
    menuItem.jsObject = this;
    menuItem.menu = menu;
    menuItem.name = itemName;
    menuItem.key = key;
    menuItem.caption_ = caption;
    menuItem.imageName = imageName;
    menuItem.styles = styles;
    menuItem.id = this.generateKey();
    menuItem.className = this.options.isTouchDevice ? styles["default"] + "_Touch" : styles["default"] + "_Mouse";
    menu.items[itemName] = menuItem;
    menuItem.isEnabled = true;
    menuItem.isSelected = false;
    menuItem.selectedItem = null;
    menuItem.haveSubMenu = haveSubMenu;
    menuItem.isOver = false;
    if (menu.cutMenu) menuItem.setAttribute("title", caption);

    var innerTable = this.CreateHTMLTable();
    menuItem.appendChild(innerTable);
    menuItem.innerTable = innerTable;
    innerTable.style.height = "100%";
    innerTable.style.width = "100%";

    if (imageName != null) {
        menuItem.cellImage = innerTable.addCell();
        menuItem.cellImage.style.width = "1px";
        menuItem.cellImage.style.fontSize = "0px";
        menuItem.cellImage.style.padding = "0 5px 0 5px";
        menuItem.cellImage.style.textAlign = "left";
        if (this.options.images[imageName]) {
            var img = document.createElement("img");
            menuItem.image = img;
            menuItem.cellImage.appendChild(img);
            img.src = this.options.images[imageName];
        }
        else {
            menuItem.cellImage.style.width = "16px";
        }
    }

    if (caption != null || typeof (caption) == "undefined") {
        var captionCell = innerTable.addCell();
        menuItem.caption = captionCell;
        captionCell.style.padding = "0 20px 0 5px";
        captionCell.style.textAlign = "left";
        captionCell.style.whiteSpace = "nowrap";
        if (caption) captionCell.innerHTML = caption;
        if (itemName.indexOf("fontItem") == 0) {
            captionCell.style.fontSize = "16px";
            captionCell.style.fontFamily = caption;
            captionCell.style.lineHeight = "1";
            menuItem.setAttribute("title", caption);
        }
    }

    if (haveSubMenu != null) {
        menuItem.arrowImg = document.createElement("img");
        menuItem.arrowCell = innerTable.addCell();
        menuItem.arrowCell.style.textAlign = "right";
        menuItem.arrowCell.appendChild(menuItem.arrowImg);
        menuItem.arrowImg.src = this.options.images["ArrowRight.png"];
    }

    menuItem.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = true;
    }

    menuItem.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.className = (this.isSelected ? this.styles["selected"] : this.styles["default"]) + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = false;
    }

    menuItem.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    menuItem.onmousedown = function () {
        if (this.isTouchStartFlag || !this.isEnabled) return;
        this.jsObject.options.menuItemPressed = this;
    }

    menuItem.onclick = function () {
        if (this.isTouchEndFlag || !this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.action();
    }

    menuItem.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.fingerIsMoved = false;
        this.jsObject.options.menuItemPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    menuItem.ontouchend = function () {
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        this.className = this.styles["over"] + "_Touch";
        setTimeout(function () {
            this_.className = this_.styles["default"] + "_Touch";
            this_.action();
        }, 150);
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    menuItem.action = function () {
        this.menu.action(this);
    }

    menuItem.setEnabled = function (state) {
        this.isEnabled = state;
        if (this.image) this.image.style.opacity = state ? "1" : "0.3";
        if (this.arrowImg) this.arrowImg.style.opacity = state ? "1" : "0.3";
        this.className = (state ? this.styles["default"] : this.styles["disabled"]) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    menuItem.setSelected = function (state) {
        if (!state) {
            this.isSelected = false;
            this.className = this.jsObject.options.isTouchDevice ? this.styles["default"] + "_Touch" : this.styles["default"] + "_Mouse";
            return;
        }
        if (this.menu.selectedItem != null) {
            this.menu.selectedItem.className = this.jsObject.options.isTouchDevice ? this.styles["default"] + "_Touch" : this.styles["default"] + "_Mouse";
            this.menu.selectedItem.isSelected = false;
        }
        this.className = this.jsObject.options.isTouchDevice ? this.styles["selected"] + "_Touch" : this.styles["selected"] + "_Mouse";
        this.menu.selectedItem = this;
        this.isSelected = true;
    }

    return menuItem;
}

StiMobileDesigner.prototype.VerticalMenuSeparator = function (menu, name) {
    var menuSeparator = document.createElement("div");
    menuSeparator.className = "stiDesignerVerticalMenuSeparator";
    if (menu) menu.items[name] = menuSeparator;

    return menuSeparator;
}

StiMobileDesigner.prototype.VerticalMenuItemForStyles = function (menu, itemAttrs, styles) {
    var item = this.VerticalMenuItem(menu, itemAttrs.name, itemAttrs.caption, itemAttrs.imageName, itemAttrs.key, styles)
    item.itemAttrs = itemAttrs;

    var styleProperties = itemAttrs.styleProperties.type == "StiChartStyle" ? itemAttrs.styleProperties.properties : itemAttrs.styleProperties;
    
    //Override Styles
    item.style.margin = "2px";
    item.style.padding = "3px";
    item.style.overflow = "hidden";
    item.style.minWidth = "120px";
    item.innerTable.style.width = "100%";
    item.caption.style.padding = "0px 5px 0px 5px";

    var maxHeight = this.options.isTouchDevice ? 28 : 22;
    var captionContainer = document.createElement("div");
    captionContainer.style.maxHeight = maxHeight + "px";
    captionContainer.style.maxWidth = "200px";
    captionContainer.style.overflow = "hidden";

    var captionInnerContainer = document.createElement("div");
    item.captionInnerContainer = captionInnerContainer;
    captionContainer.appendChild(captionInnerContainer);
    captionInnerContainer.innerHTML = item.caption.innerHTML;
    item.caption.innerHTML = "";
    item.caption.appendChild(captionContainer);
    item.captionInnerContainer = captionInnerContainer;
    captionInnerContainer.style.position = "relative";
    if (styleProperties.font) {
        var captionInnerContainerHeight = parseInt(styleProperties.font.split("!")[1]) * 1.33;
        captionInnerContainer.style.top = (captionInnerContainerHeight > maxHeight ? ((captionInnerContainerHeight - maxHeight) / 2 * -1) : 0) + "px";
    }
    this.RepaintControlByAttributes(item.innerTable, styleProperties.font, styleProperties.brush, styleProperties.textBrush, styleProperties.border);

    return item;
}

StiMobileDesigner.prototype.VerticalMenuItemForChartStyles = function (menu, properties) {
    var item = this.VerticalMenuItem(menu, properties.type + "_" + properties.name, properties.name, null, { type: properties.type, name: properties.name }, this.GetStyles("MenuBigItem"));
    item.style.height = "auto";

    var text = document.createElement("div");
    text.style.fontSize = "12px";
    text.style.fontFamily = "Arial";
    text.style.float = "left";
    text.style.margin = "-7px 0px 3px 10px";
    text.innerHTML = properties.name ? properties.name : properties.type.replace("Sti", "");
    item.caption.style.padding = "0px";

    item.caption.innerHTML = properties.image;
    item.caption.appendChild(text);

    return item;
}

StiMobileDesigner.prototype.VerticalMenuItemForGaugeStyles = function (menu, properties) {
    var item = this.VerticalMenuItem(menu, properties.type + "_" + properties.name, properties.name, null, { type: properties.type, name: properties.name }, this.GetStyles("MenuBigItem"));
    item.style.height = "auto";

    var text = document.createElement("div");
    text.style.fontSize = "12px";
    text.style.fontFamily = "Arial";
    text.style.float = "left";
    text.style.margin = "5px 0px 5px 10px";
    text.innerHTML = properties.name ? properties.name : properties.type.replace("Sti", "");
    item.caption.style.padding = "3px";

    item.caption.innerHTML = properties.image;
    item.caption.appendChild(text);

    return item;
}

StiMobileDesigner.prototype.VerticalMenuItemForMapStyles = function (menu, properties) {
    return this.VerticalMenuItemForGaugeStyles(menu, properties);
}

StiMobileDesigner.prototype.VerticalMenuItemForCrossTabStyles = function (menu, styleObject) {
    var item = this.VerticalMenuItem(menu, styleObject.properties.name, styleObject.properties.name, null, styleObject.key, this.GetStyles("MenuBigItem"));

    item.style.height = "auto";
    item.caption.style.padding = "8px";
    item.caption.innerHTML = "";
        
    item.tableContainer = document.createElement("div");
    item.caption.appendChild(item.tableContainer);
    item.tableContainer.appendChild(this.CrossTabSampleTable(118, 47, 13, 5, styleObject.properties.bottomColor, styleObject.properties.topColor));

    return item;
}

StiMobileDesigner.prototype.VerticalMenuItemForTableStyles = function (menu, styleObject) {
    var item = this.VerticalMenuItem(menu, styleObject.styleId, styleObject.styleId, null, styleObject.styleId, this.GetStyles("MenuBigItem"));

    item.style.height = "auto";
    item.caption.style.padding = "8px";
    item.caption.innerHTML = "";

    item.tableContainer = document.createElement("div");
    item.caption.appendChild(item.tableContainer);
    item.tableContainer.appendChild(this.SampleTable(118, 47, 13, 5, styleObject.headerColor, styleObject.footerColor, styleObject.dataColor));

    return item;
}

StiMobileDesigner.prototype.CrossTabSampleTable = function (width, height, countColumns, countRows, bottomColor, topColor) {
    var table = this.CreateHTMLTable();
    table.style.width = width + "px";
    table.style.height = height + "px";
    table.style.borderCollapse = "collapse";

    var topHTMLColor = topColor == "transparent" ? "transparent" : "rgb(" + this.RgbaToRgb(topColor) + ")";
    var bottomHTMLColor = bottomColor == "transparent" ? "transparent" : "rgb(" + this.RgbaToRgb(bottomColor) + ")";

    for (var row = 0; row < countRows; row++) {
        for (var col = 0; col < countColumns; col++) {
            var cell = table.addCellInLastRow();
            cell.style.border = "1px solid #d3d3d3";
            if (row == 0 && col != 0) cell.style.background = bottomHTMLColor;
            else if (col == 0 && row != 0) cell.style.background = topHTMLColor;
            else cell.style.background = "#ffffff";
        }
        table.addRow();
    }

    return table;
}

StiMobileDesigner.prototype.SampleTable = function (width, height, countColumns, countRows, headerColor, footerColor, dataColor) {
    var table = this.CreateHTMLTable();
    table.style.width = width + "px";
    table.style.height = height + "px";
    table.style.borderCollapse = "collapse";

    var headerHTMLColor = headerColor == "transparent" ? "transparent" : "rgb(" + this.RgbaToRgb(headerColor) + ")";
    var footerHTMLColor = footerColor == "transparent" ? "transparent" : "rgb(" + this.RgbaToRgb(footerColor) + ")";
    var dataHTMLColor = dataColor == "transparent" ? "transparent" : "rgb(" + this.RgbaToRgb(dataColor) + ")";

    for (var row = 0; row < countRows; row++) {
        for (var col = 0; col < countColumns; col++) {
            var cell = table.addCellInLastRow();
            cell.style.border = "1px solid #d3d3d3";
            if (row == 0) cell.style.background = headerHTMLColor;
            else if (row == countRows - 1) cell.style.background = footerHTMLColor;
            else cell.style.background = dataHTMLColor;
        }
        table.addRow();
    }

    return table;
}

StiMobileDesigner.prototype.RgbaToRgb = function (rgba) {
    var colorArray = rgba.split(",");
    if (colorArray.length == 4) return rgba.substring(rgba.indexOf(",") + 1);
    else return rgba;
}