﻿
StiMobileDesigner.prototype.BrushMenu = function (name, parentButton) {

    var menu = this.BaseMenu(name, parentButton, "Down");
    menu.innerContent.style.padding = "2px";
    menu.selectedHeaderButton = null;

    menu.headerButtons = [];
    menu.headerButtons.push(this.BrushHeaderButton(name, "Empty", "BrushHeaderButtons",
        this.loc.Report.StiEmptyBrush, "BrushEmpty.png", null, 0));
    menu.headerButtons.push(this.BrushHeaderButton(name, "Solid", "BrushHeaderButtons",
        this.loc.Report.StiSolidBrush, "BrushSolid.png", null, 1));
    menu.headerButtons.push(this.BrushHeaderButton(name, "Hatch", "BrushHeaderButtons",
        this.loc.Report.StiHatchBrush, "BrushHatch.png", null, 2));
    menu.headerButtons.push(this.BrushHeaderButton(name, "Gradient", "BrushHeaderButtons",
        this.loc.Report.StiGradientBrush, "BrushGradient.png", null, 3));
    menu.headerButtons.push(this.BrushHeaderButton(name, "Glare", "BrushHeaderButtons",
        this.loc.Report.StiGlareBrush, "BrushGlare.png", null, 4));
    menu.headerButtons.push(this.BrushHeaderButton(name, "Glass", "BrushHeaderButtons",
        this.loc.Report.StiGlassBrush, "BrushGlass.png", null, 5));

    var headerButtonsTable = this.CreateHTMLTable();
    headerButtonsTable.cellSpacing = 3;
    menu.innerContent.appendChild(headerButtonsTable);

    for (var i in menu.headerButtons) {
        if (i < 3)
            headerButtonsTable.addCell(menu.headerButtons[i]);
        else {
            if (i == 3) headerButtonsTable.addRow();
            headerButtonsTable.addCellInLastRow(menu.headerButtons[i]);
        }
    }

    menu.innerContent.appendChild(this.Separator());

    //Panels
    menu.panels = [];
    for (var i in menu.headerButtons) {
        menu.panels[i] = document.createElement("div");
        menu.panels[i].key = i;
        menu.panels[i].jsObject = this;
        menu.panels[i].style.display = "none";
        menu.panels[i].style.padding = "4px 0 4px 0";
        menu.innerContent.appendChild(menu.panels[i]);
        menu.panels[i].parentMenu = menu;

        menu.panels[i].show = function () {
            for (var i in this.parentMenu.panels) {
                if (this.parentMenu.panels[i] == this) {
                    this.parentMenu.headerButtons[i].setSelected(true);
                    this.parentMenu.selectedHeaderButton = this.parentMenu.headerButtons[i];
                    this.parentMenu.panels[i].style.display = "";
                }
                else
                    this.parentMenu.panels[i].style.display = "none";
            }
        }
    }

    //Add Panels
    menu.panels[1].appendChild(this.SolidBrushTable(menu));
    menu.panels[2].appendChild(this.HatchBrushTable(menu));
    menu.panels[3].appendChild(this.GradientBrushTable(menu));
    menu.panels[4].appendChild(this.GlareBrushTable(menu));
    menu.panels[5].appendChild(this.GlassBrushTable(menu));

    menu.updateControls = function (brushStr) {
        if (!brushStr) return;
        if (brushStr == "StiEmptyValue") {            
            menu.panels[0].show();
            menu.headerButtons[0].setSelected(false);
            return;
        }
        var controls = this.jsObject.options.controls;
        brushArray = brushStr.split("!");
        switch (brushArray[0]) {
            case "1":
                {
                    controls[this.name + "SolidColor"].setKey(brushArray[1]);
                    break;
                }
            case "2":
                {
                    controls[this.name + "HatchForeColor"].setKey(brushArray[1]);
                    controls[this.name + "HatchBackColor"].setKey(brushArray[2]);
                    controls[this.name + "HatchStyle"].setKey(brushArray[3]);
                    break;
                }
            case "3":
                {
                    controls[this.name + "GradientStartColor"].setKey(brushArray[1]);
                    controls[this.name + "GradientEndColor"].setKey(brushArray[2]);
                    controls[this.name + "GradientAngle"].value = brushArray[3];
                    break;
                }
            case "4":
                {
                    controls[this.name + "GlareStartColor"].setKey(brushArray[1]);
                    controls[this.name + "GlareEndColor"].setKey(brushArray[2]);
                    controls[this.name + "GlareAngle"].value = brushArray[3];
                    controls[this.name + "GlareFocus"].value = brushArray[4];
                    controls[this.name + "GlareScale"].value = brushArray[5];
                    break;
                }
            case "5":
                {
                    controls[this.name + "GlassColor"].setKey(brushArray[1]);
                    controls[this.name + "GlassBlend"].value = brushArray[2];
                    controls[this.name + "GlassDrawHatch"].setChecked(brushArray[3] == "1");
                    break;
                }
        }
        menu.panels[brushArray[0]].show();
    }

    menu.onshow = function () { this.updateControls(this.parentButton.key); }

    menu.action = function () { };

    menu.changedControls = function () {
        var controls = this.jsObject.options.controls;
        this.parentButton.key = "0";
        switch (this.selectedHeaderButton.key) {
            case 1: { this.parentButton.key = "1!" + controls[this.name + "SolidColor"].key; break; }
            case 2:
                {
                    this.parentButton.key = "2!" + controls[this.name + "HatchForeColor"].key + "!" +
                controls[this.name + "HatchBackColor"].key + "!" + controls[this.name + "HatchStyle"].key; break;
                }
            case 3:
                {
                    this.parentButton.key = "3!" + controls[this.name + "GradientStartColor"].key + "!" +
                controls[this.name + "GradientEndColor"].key + "!" + controls[this.name + "GradientAngle"].value; break;
                }
            case 4:
                {
                    this.parentButton.key = "4!" + controls[this.name + "GlareStartColor"].key + "!" +
                controls[this.name + "GlareEndColor"].key + "!" + controls[this.name + "GlareAngle"].value +
                "!" + controls[this.name + "GlareFocus"].value + "!" + controls[this.name + "GlareScale"].value; break;
                }
            case 5:
                {
                    this.parentButton.key = "5!" + controls[this.name + "GlassColor"].key + "!" + controls[this.name + "GlassBlend"].value +
                "!" + (controls[this.name + "GlassDrawHatch"].isChecked ? "1" : "0"); break;
                }
        }
        this.action();
    }

    return menu;
}

StiMobileDesigner.prototype.BrushHeaderButton = function (menuName, name, groupName, caption, imageName, toolTip, key) {
    var brushHeaderButton = this.StandartSmallButton(menuName + name, menuName + groupName, caption, imageName, toolTip, null);
    brushHeaderButton.key = key;
    brushHeaderButton.menuName = menuName; 
    
    brushHeaderButton.action = function () {
        var menu = this.jsObject.options.menus[this.menuName];        
        menu.panels[this.key].show();
        switch (this.key) {
            case 1: { menu.updateControls("1!255,255,255"); break; }
            case 2: { menu.updateControls("2!0,0,0!255,255,255!3"); break; }
            case 3: { menu.updateControls("3!255,255,255!0,0,0!0"); break; }
            case 4: { menu.updateControls("4!0,0,0!255,255,255!0!0.5!1"); break; }
            case 5: { menu.updateControls("5!128,128,128!0.2!1"); break; }
        }                
        menu.changedControls();
    }
    
    return brushHeaderButton;
}

//Solid
StiMobileDesigner.prototype.SolidBrushTable = function (menu) {
    var table = this.CreateHTMLTable();
    var solidColorText = table.addCell();
    solidColorText.className = "stiDesignerCaptionControls";
    solidColorText.innerHTML = this.loc.PropertyMain.Color + ":";
    
    var solidColor = this.ColorControl(menu.name + "SolidColor", null);    
    solidColor.parentMenu = menu;
    solidColor.action = function() { this.parentMenu.changedControls(); }    
    var solidColorCell = table.addCell(solidColor)
    solidColorCell.className = "stiDesignerControlCells";    
        
    return table;
}

//Hatch
StiMobileDesigner.prototype.HatchBrushTable = function (menu) {    
    
    var table = this.CreateHTMLTable();

    //Fore Color
    var hatchForeColorText = table.addCell();
    hatchForeColorText.className = "stiDesignerCaptionControls";
    hatchForeColorText.innerHTML = this.loc.PropertyMain.ForeColor + ":";
    
    var hatchForeColor = this.ColorControl(menu.name + "HatchForeColor", null);    
    hatchForeColor.parentMenu = menu;
    hatchForeColor.action = function() { this.parentMenu.changedControls(); }    
    var hatchForeColorCell = table.addCell(hatchForeColor)
    hatchForeColorCell.className = "stiDesignerControlCells";
    
    //Back Color
    table.addRow();
    
    var hatchBackColorText = table.addCellInLastRow();
    hatchBackColorText.className = "stiDesignerCaptionControls";
    hatchBackColorText.innerHTML = this.loc.PropertyMain.BackColor + ":";
    
    var hatchBackColor = this.ColorControl(menu.name + "HatchBackColor", null);    
    hatchBackColor.parentMenu = menu;
    hatchBackColor.action = function() { this.parentMenu.changedControls(); }    
    var hatchBackColorCell = table.addCellInLastRow(hatchBackColor);
    hatchBackColorCell.className = "stiDesignerControlCells";
    
    //Style
    table.addRow();
    
    var hatchStyleText = table.addCellInLastRow();
    hatchStyleText.className = "stiDesignerCaptionControls";
    hatchStyleText.innerHTML = this.loc.PropertyMain.Style + ":";

    var items = [];
    if (this.loc) {
        var hatchStyles = [
            [this.loc.PropertyHatchStyle.BackwardDiagonal, "3"],
            [this.loc.PropertyHatchStyle.LargeGrid, "4"],
            [this.loc.PropertyHatchStyle.DarkDownwardDiagonal, "20"],
            [this.loc.PropertyHatchStyle.DarkHorizontal, "29"],
            [this.loc.PropertyHatchStyle.DarkUpwardDiagonal, "21"],
            [this.loc.PropertyHatchStyle.DarkVertical, "28"],
            [this.loc.PropertyHatchStyle.DashedDownwardDiagonal, "30"],
            [this.loc.PropertyHatchStyle.DashedHorizontal, "32"],
            [this.loc.PropertyHatchStyle.DashedUpwardDiagonal, "31"],
            [this.loc.PropertyHatchStyle.DashedVertical, "33"],
            [this.loc.PropertyHatchStyle.DiagonalBrick, "38"],
            [this.loc.PropertyHatchStyle.DiagonalCross, "5"],
            [this.loc.PropertyHatchStyle.Divot, "42"],
            [this.loc.PropertyHatchStyle.DottedDiamond, "44"],
            [this.loc.PropertyHatchStyle.DottedGrid, "43"],
            [this.loc.PropertyHatchStyle.ForwardDiagonal, "2"],
            [this.loc.PropertyHatchStyle.Horizontal, "0"],
            [this.loc.PropertyHatchStyle.HorizontalBrick, "39"],
            [this.loc.PropertyHatchStyle.LargeCheckerBoard, "50"],
            [this.loc.PropertyHatchStyle.LargeConfetti, "35"],
            [this.loc.PropertyHatchStyle.LightDownwardDiagonal, "18"],
            [this.loc.PropertyHatchStyle.LightHorizontal, "25"],
            [this.loc.PropertyHatchStyle.LightUpwardDiagonal, "19"],
            [this.loc.PropertyHatchStyle.LightVertical, "24"],
            [this.loc.PropertyHatchStyle.NarrowHorizontal, "27"],
            [this.loc.PropertyHatchStyle.NarrowVertical, "26"],
            [this.loc.PropertyHatchStyle.OutlinedDiamond, "51"],
            [this.loc.PropertyHatchStyle.Percent05, "6"],
            [this.loc.PropertyHatchStyle.Percent10, "7"],
            [this.loc.PropertyHatchStyle.Percent20, "8"],
            [this.loc.PropertyHatchStyle.Percent25, "9"],
            [this.loc.PropertyHatchStyle.Percent30, "10"],
            [this.loc.PropertyHatchStyle.Percent40, "11"],
            [this.loc.PropertyHatchStyle.Percent50, "12"],
            [this.loc.PropertyHatchStyle.Percent60, "13"],
            [this.loc.PropertyHatchStyle.Percent70, "14"],
            [this.loc.PropertyHatchStyle.Percent75, "15"],
            [this.loc.PropertyHatchStyle.Percent80, "16"],
            [this.loc.PropertyHatchStyle.Percent90, "17"],
            [this.loc.PropertyHatchStyle.Plaid, "41"],
            [this.loc.PropertyHatchStyle.Shingle, "45"],
            [this.loc.PropertyHatchStyle.SmallCheckerBoard, "49"],
            [this.loc.PropertyHatchStyle.SmallConfetti, "34"],
            [this.loc.PropertyHatchStyle.SmallGrid, "48"],
            [this.loc.PropertyHatchStyle.SolidDiamond, "52"],
            [this.loc.PropertyHatchStyle.Sphere, "47"],
            [this.loc.PropertyHatchStyle.Trellis, "46"],
            [this.loc.PropertyHatchStyle.Vertical, "1"],
            [this.loc.PropertyHatchStyle.Weave, "40"],
            [this.loc.PropertyHatchStyle.WideDownwardDiagonal, "22"],
            [this.loc.PropertyHatchStyle.WideUpwardDiagonal, "23"],
            [this.loc.PropertyHatchStyle.ZigZag, "36"],
        ]
        for (var i in hatchStyles) {
            items.push(this.Item("HatchStyleItem" + hatchStyles[i][1], hatchStyles[i][0], null, hatchStyles[i][1]));
        }
    }
    else {
        for (var i in this.options.hatchStyles) {
            items.push(this.Item("HatchStyleItem" + this.options.hatchStyles[i].key, this.options.hatchStyles[i].value, null, this.options.hatchStyles[i].key));
        }
    }
                                    
    var hatchStyle = this.DropDownList(menu.name + "HatchStyle", 150, null, items, true, false, null, true);
    hatchStyle.parentMenu = menu;
    if (hatchStyle.menu.innerContent) hatchStyle.menu.innerContent.style.maxHeight = "230px";
    hatchStyle.action = function() { this.parentMenu.changedControls(); }    
    var hatchStyleCell = table.addCellInLastRow(hatchStyle);
    hatchStyleCell.style.paddingLeft = this.options.isTouchDevice ? "7px" : "3px";
    hatchStyleCell.className = "stiDesignerControlCells";
        
    return table;
}

//Gradient
StiMobileDesigner.prototype.GradientBrushTable = function (menu) {    
    
    var table = this.CreateHTMLTable();

    //Start Color
    var gradientStartColorText = table.addCell();
    gradientStartColorText.className = "stiDesignerCaptionControls";
    gradientStartColorText.innerHTML = this.loc.PropertyMain.StartColor + ":";
    
    var gradientStartColor = this.ColorControl(menu.name + "GradientStartColor", null);    
    gradientStartColor.parentMenu = menu;
    gradientStartColor.action = function() { this.parentMenu.changedControls(); }    
    var gradientStartColorCell = table.addCell(gradientStartColor)
    gradientStartColorCell.className = "stiDesignerControlCells";
    
    //End Color
    table.addRow();
    
    var gradientEndColorText = table.addCellInLastRow();
    gradientEndColorText.className = "stiDesignerCaptionControls";
    gradientEndColorText.innerHTML = this.loc.PropertyMain.EndColor + ":";
    
    var gradientEndColor = this.ColorControl(menu.name + "GradientEndColor", null);
    gradientEndColor.parentMenu = menu;
    gradientEndColor.action = function() { this.parentMenu.changedControls(); }
    var gradientEndColorCell = table.addCellInLastRow(gradientEndColor);
    gradientEndColorCell.className = "stiDesignerControlCells";
    
    //Angle
    table.addRow();
    
    var gradientAngleText = table.addCellInLastRow();
    gradientAngleText.className = "stiDesignerCaptionControls";
    gradientAngleText.innerHTML = this.loc.PropertyMain.Angle + ":";

    var gradientAngle = this.TextBox(menu.name + "GradientAngle", 50);    
    gradientAngle.parentMenu = menu;
    gradientAngle.action = function() { 
        this.value = this.jsObject.StrToDouble(this.value);
        this.parentMenu.changedControls(); 
    }    
    var gradientAngleCell = table.addCellInLastRow(gradientAngle);
    gradientAngleCell.style.paddingLeft = this.options.isTouchDevice ? "7px" : "3px";
    gradientAngleCell.className = "stiDesignerControlCells";
        
    return table;
}

//Glare
StiMobileDesigner.prototype.GlareBrushTable = function (menu) {    
    
    var table = this.CreateHTMLTable();

    //Start Color
    var glareStartColorText = table.addCell();
    glareStartColorText.className = "stiDesignerCaptionControls";
    glareStartColorText.innerHTML = this.loc.PropertyMain.StartColor + ":";
    
    var glareStartColor = this.ColorControl(menu.name + "GlareStartColor", null);    
    glareStartColor.parentMenu = menu;
    glareStartColor.action = function() { this.parentMenu.changedControls(); }    
    var glareStartColorCell = table.addCell(glareStartColor)
    glareStartColorCell.className = "stiDesignerControlCells";
    
    //End Color
    table.addRow();
    
    var glareEndColorText = table.addCellInLastRow();
    glareEndColorText.className = "stiDesignerCaptionControls";
    glareEndColorText.innerHTML = this.loc.PropertyMain.EndColor + ":";
    
    var glareEndColor = this.ColorControl(menu.name + "GlareEndColor", null);    
    glareEndColor.parentMenu = menu;
    glareEndColor.action = function() { this.parentMenu.changedControls(); }    
    var glareEndColorCell = table.addCellInLastRow(glareEndColor);
    glareEndColorCell.className = "stiDesignerControlCells";
    
    //Angle
    table.addRow();
    
    var glareAngleText = table.addCellInLastRow();
    glareAngleText.className = "stiDesignerCaptionControls";
    glareAngleText.innerHTML = this.loc.PropertyMain.Angle + ":";

    var glareAngle = this.TextBox(menu.name + "GlareAngle", 50);    
    glareAngle.parentMenu = menu;
    glareAngle.action = function() { 
        this.value = this.jsObject.StrToDouble(this.value);
        this.parentMenu.changedControls(); 
    }    
    var glareAngleCell = table.addCellInLastRow(glareAngle);
    glareAngleCell.style.paddingLeft = this.options.isTouchDevice ? "7px" : "3px";
    glareAngleCell.className = "stiDesignerControlCells";
    
    //Focus
    table.addRow();
    
    var glareFocusText = table.addCellInLastRow();
    glareFocusText.className = "stiDesignerCaptionControls";
    glareFocusText.innerHTML = this.loc.PropertyMain.Focus + ":";

    var glareFocus = this.TextBox(menu.name + "GlareFocus", 50);    
    glareFocus.parentMenu = menu;
    glareFocus.action = function() { 
        this.value = this.jsObject.StrToDouble(this.value);
        this.parentMenu.changedControls(); 
    }    
    var glareFocusCell = table.addCellInLastRow(glareFocus);
    glareFocusCell.style.paddingLeft = this.options.isTouchDevice ? "7px" : "3px";
    glareFocusCell.className = "stiDesignerControlCells";
    
    //Scale
    table.addRow();
    
    var glareScaleText = table.addCellInLastRow();
    glareScaleText.className = "stiDesignerCaptionControls";
    glareScaleText.innerHTML = this.loc.PropertyMain.Scale + ":";

    var glareScale = this.TextBox(menu.name + "GlareScale", 50);    
    glareScale.parentMenu = menu;
    glareScale.action = function() { 
        this.value = this.jsObject.StrToDouble(this.value);
        this.parentMenu.changedControls(); 
    }    
    var glareScaleCell = table.addCellInLastRow(glareScale);
    glareScaleCell.style.paddingLeft = this.options.isTouchDevice ? "7px" : "3px";
    glareScaleCell.className = "stiDesignerControlCells";
        
    return table;
}

//Glass
StiMobileDesigner.prototype.GlassBrushTable = function (menu) {
    var table = this.CreateHTMLTable();
    
    //Color
    var glassColorText = table.addCell();
    glassColorText.className = "stiDesignerCaptionControls";
    glassColorText.innerHTML = this.loc.PropertyMain.Color + ":";
    
    var glassColor = this.ColorControl(menu.name + "GlassColor", null);    
    glassColor.parentMenu = menu;
    glassColor.action = function() { this.parentMenu.changedControls(); }    
    var glassColorCell = table.addCell(glassColor)
    glassColorCell.className = "stiDesignerControlCells";    
    
    //Blend
    table.addRow();
    
    var glassBlendText = table.addCellInLastRow();
    glassBlendText.className = "stiDesignerCaptionControls";
    glassBlendText.innerHTML = this.loc.PropertyMain.Blend + ":";

    var glassBlend = this.TextBox(menu.name + "GlassBlend", 50);    
    glassBlend.parentMenu = menu;
    glassBlend.action = function() { 
        this.value = this.jsObject.StrToDouble(this.value);
        this.parentMenu.changedControls(); 
    }    
    var glassBlendCell = table.addCellInLastRow(glassBlend);
    glassBlendCell.style.paddingLeft = this.options.isTouchDevice ? "7px" : "3px";
    glassBlendCell.className = "stiDesignerControlCells";
    
    //Draw Hatch
    table.addRow();
    
    var drawHatchCell = table.addCellInLastRow();
    drawHatchCell.className = "stiDesignerCaptionControls";
    drawHatchCell.style.paddingTop = "5px";
    drawHatchCell.style.paddingBottom = "5px";
    drawHatchCell.setAttribute("colspan", "2");
        
    var drawHatch = this.CheckBox(menu.name + "GlassDrawHatch", this.loc.PropertyMain.DrawHatch);
    drawHatchCell.appendChild(drawHatch);
    drawHatch.parentMenu = menu;
    drawHatch.action = function() { 
        this.parentMenu.changedControls(); 
    }    
    
    return table;
}