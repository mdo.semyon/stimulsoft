﻿var GlobalProcessImage;

StiMobileDesigner.prototype.ProcessImageStatusPanel = function () {
    var processImage = this.CreateHTMLTable(); ;
    processImage.jsObject = this;
    processImage.style.display = "none";
    processImage.className = "stiDesignerProcessImageStatusPanel stiDesignerClearAllStyles";
    this.options.processImageStatusPanel = processImage;        

    var img = this.ProgressMini("white");
    processImage.addCell(img).style.padding = "0 3px 0 3px";

    var text = processImage.addCell();
    text.innerHTML = this.loc.A_WebViewer.Loading.replace("...", "");
    text.style.fontSize = "12px";
    text.style.padding = "0 5px 0 0";

    processImage.show = function () {
        this.style.display = "";
    }

    processImage.hide = function () {
        this.style.display = "none";
    }

    return processImage;
}

StiMobileDesigner.prototype.InitializeProcessImage = function () { 
    var processImage = this.Progress();
    processImage.jsObject = this;
    processImage.style.display = "none";
    this.options.processImage = processImage;
    this.options.mainPanel.appendChild(processImage);
    processImage.style.top = "50%"
    processImage.style.left = "50%"
    processImage.style.marginLeft = "-32px";
    processImage.style.marginTop = "-100px";

    processImage.show = function () {
        this.style.display = "";
        if (!this.jsObject.options.disabledPanels) this.jsObject.InitializeDisabledPanels();
        this.jsObject.options.disabledPanels[6].style.display = "";
    }

    processImage.hide = function () {
        this.style.display = "none";
        if (!this.jsObject.options.disabledPanels) this.jsObject.InitializeDisabledPanels();
        this.jsObject.options.disabledPanels[6].style.display = "none";
    }

    if (this.options.demoMode) {
        processImage.show();
        GlobalProcessImage = processImage;
        window.onload = function () { if (GlobalProcessImage) GlobalProcessImage.hide(); }
    }

    return processImage;
}