﻿
StiMobileDesigner.prototype.PagesButton = function (name, alias) {
    var captionButton = alias ? name + " [" + Base64.decode(alias) + "]" : name;
    var button = this.SmallButton(null, null, captionButton, null, captionButton, false, this.GetStyles("PagesButton"));
    button.key = name;
    button.caption.style.padding = "0 15px 0 15px";
    button.caption.style.textAlign = "center";
    button.style.minWidth = "75px";
    button.innerTable.style.width = "100%";

    button.action = function () {
        var pages = this.jsObject.options.pagesPanel.pagesContainer.pages;
        for (var i = 0; i < pages.length; i++) {
            if (this != pages[i]) {
                pages[i].setSelected(false);
            }
        }
        this.setSelected(true);
        this.jsObject.options.paintPanel.showPage(this.jsObject.options.report.pages[this.key]);
    }

    //Override
    button.onmouseenter = function () {
        if (!this.isEnabled || this.isSelected || this.jsObject.options.isTouchClick) return;
        this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = true;
    }

    button.setSelected = function (state) {
        if (!state) this.setEditMode(false);
        if (this.leftSep) this.leftSep.setSelected(state);
        if (this.rightSep) this.rightSep.setSelected(state);
        this.isSelected = state;
        this.className = (state ? this.styles["selected"] : (this.isEnabled ? (this.isOver ? this.styles["over"] : this.styles["default"]) : this.styles["disabled"])) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    button.onmouseup = function (event) {
        if (this.isTouchEndFlag || this.jsObject.options.isTouchClick) return;
        if (event.button == 2) {
            event.stopPropagation();
            this.action();
            var pageMenu = this.jsObject.options.menus.pageContextMenu || this.jsObject.InitializePageContextMenu();
            var point = this.jsObject.FindMousePosOnMainPanel(event);
            pageMenu.pageButton = this;
            pageMenu.show(point.xPixels - 2, point.yPixels - 2, "Up", "Left");
        }
        return false;
    }

    var contextTimer = null;
    button.ontouchstart = function (event) {
        this.jsObject.options.fingerIsMoved = false;
        this.jsObject.options.buttonPressed = this;
        var this_ = this;
        contextTimer = setTimeout(function () {
            if (this_.jsObject.options.fingerIsMoved) return;
            this_.action();
            var pageMenu = this_.jsObject.options.menus.pageContextMenu || this_.jsObject.InitializePageContextMenu();
            var point = this_.jsObject.FindMousePosOnMainPanel(event);
            pageMenu.pageButton = this_;
            pageMenu.show(point.xPixels - 2, point.yPixels - 2, "Up");
        }, 1000);
    }

    button.ontouchend = function (event) {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        clearTimeout(contextTimer);
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        this.action();
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    button.oncontextmenu = function (event) {
        return false;
    }

    var editTextBox = this.TextBox(null, 70);
    button.innerTable.addCell(editTextBox);
    editTextBox.style.display = "none";
    editTextBox.style.margin = "0px 4px 1px 4px";
    editTextBox.style.height = "18px";
    editTextBox.style.border = "0px";

    editTextBox.action = function () {
        button.setEditMode(false);
        this.jsObject.SendCommandRenameComponent(button.ownerPage, this.value);
    }

    button.setEditMode = function (state) {
        editTextBox.style.display = state ? "" : "none";
        this.caption.style.display = !state ? "" : "none";
        if (state) {
            editTextBox.value = this.captionText;
            editTextBox.focus();
        }
    }

    button.ondblclick = function () {
        this.setEditMode(true);
    }

    return button;
}
