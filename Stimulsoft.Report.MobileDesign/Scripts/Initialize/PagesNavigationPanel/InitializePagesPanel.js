﻿
StiMobileDesigner.prototype.InitializePagesPanel = function () {
    var pagesPanel = document.createElement("div");
    pagesPanel.id = this.options.mobileDesigner.id + "pagesPanel";
    pagesPanel.className = "stiDesignerPagesPanel";
    pagesPanel.jsObject = this;
    this.options.pagesPanel = pagesPanel;
    this.options.mainPanel.appendChild(pagesPanel);

    pagesPanel.style.bottom = this.options.statusPanel.offsetHeight + "px";
    pagesPanel.style.right = "0px";
    pagesPanel.style.left = this.options.propertiesPanel.offsetWidth + "px";
    pagesPanel.style.height = "33px";
    pagesPanel.style.overflow = "hidden";

    var innerTable = this.CreateHTMLTable();
    var scrollLeftButton = this.StandartSmallButton("scrollLeft", null, null, "ArrowLeft.png");
    scrollLeftButton.style.height = pagesPanel.style.height;
    scrollLeftButton.style.width = "33px";
    scrollLeftButton.innerTable.style.width = "100%";
    innerTable.addCell(scrollLeftButton);
    var scrollRightButton = this.StandartSmallButton("scrollRight", null, null, "ArrowRight.png");
    scrollRightButton.style.height = pagesPanel.style.height;
    scrollRightButton.style.width = "33px";
    scrollRightButton.innerTable.style.width = "100%";
    innerTable.addCell(scrollRightButton);

    var addPageButton = this.StandartSmallButton("addPage", null, null, "PagePlus.png", this.loc.MainMenu.menuFilePageNew);
    addPageButton.style.width = "33px";
    addPageButton.style.marginLeft = "2px";
    addPageButton.innerTable.style.width = "100%";
    addPageButton.style.height = pagesPanel.style.height;

    pagesPanel.appendChild(innerTable);

    var pagesContainer = document.createElement("div");
    pagesContainer.pages = [];
    pagesPanel.appendChild(pagesContainer);
    pagesPanel.pagesContainer = pagesContainer;
    pagesContainer.className = "stiDesignerPagesContainer"
    pagesContainer.style.height = "70px";
    pagesContainer.style.left = "68px";
    
    pagesPanel.updateScrollButtons = function () {
        if (!pagesContainer || !pagesContainer.innerTable) return;
        var haveScroll = pagesContainer.innerTable.offsetWidth > pagesContainer.offsetWidth;
        scrollLeftButton.setEnabled(haveScroll);
        scrollRightButton.setEnabled(haveScroll);
    }

    this.addEvent(window, 'resize', function () {
        pagesPanel.updateScrollButtons();
    });

    scrollLeftButton.action = function () { pagesContainer.scrollLeft = pagesContainer.scrollLeft - 40; };
    scrollRightButton.action = function () { pagesContainer.scrollLeft = pagesContainer.scrollLeft + 40; };
    scrollLeftButton.onmousedown = function () {
        if (!this.isEnabled) return;
        this.jsObject.options.buttonPressed = this;
        this.jsObject.options.scrollLeftTimer = setInterval(function () { scrollLeftButton.action(); }, 80);
    }
    scrollRightButton.onmousedown = function () {
        if (!this.isEnabled) return;
        this.jsObject.options.buttonPressed = this;
        this.jsObject.options.scrollRightTimer = setInterval(function () { scrollRightButton.action(); }, 80);
    }

    pagesContainer.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
    }

    pagesContainer.updatePages = function () {
        this.pages = [];
        this.clear();
        var pagesCount = pagesPanel.jsObject.options.paintPanel.getPagesCount();
        var innerTable = pagesPanel.jsObject.CreateHTMLTable();
        pagesContainer.innerTable = innerTable;
        innerTable.style.height = "33px";
        this.appendChild(innerTable);
        var pageButton = null;
        var selectedButton = null;

        for (var i = 0; i < pagesCount; i++) {
            var page = pagesPanel.jsObject.options.paintPanel.findPageByIndex(i);
            if (page) {
                var leftSep = pagesPanel.jsObject.PagesPanelSeparator();
                if (pageButton) pageButton.rightSep = leftSep;
                pageButton = pagesPanel.jsObject.PagesButton(page.properties.name, page.properties.aliasName);
                pageButton.ownerPage = page;
                this.pages.push(pageButton);
                pageButton.leftSep = leftSep;
                innerTable.addCell(leftSep);
                innerTable.addCell(pageButton).style.verticalAlign = "top";
                if (page == pagesPanel.jsObject.options.currentPage) selectedButton = pageButton;
            }
        }
        if (pageButton) {
            var rightSep = pagesPanel.jsObject.PagesPanelSeparator();
            pageButton.rightSep = rightSep;
            innerTable.addCell(rightSep);
        }
        if (selectedButton) selectedButton.setSelected(true);
        pagesPanel.updateScrollButtons();
        innerTable.addCell(addPageButton);
        addPageButton.onmouseleave();
    }

    pagesPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        this.jsObject.options.paintPanel.style.bottom = (this.jsObject.options.statusPanel.offsetHeight + this.offsetHeight) + "px";
    }
}

//Separator
StiMobileDesigner.prototype.PagesPanelSeparator = function () {
    var separator = document.createElement("div");
    separator.jsObject = this;
    separator.style.width = "1px";
    separator.style.height = "23px";
    separator.className = "stiDesignerHomePanelSeparator";

    separator.setSelected = function (state) {
        separator.style.height = state ? "30px" : "20px";
        separator.parentElement.style.verticalAlign = state ? "top" : "middle";
    }

    return separator;
}
