﻿
StiMobileDesigner.prototype.InitializeStatusPanel = function () {
    var statusPanel = document.createElement("div");
    statusPanel.className = "stiDesignerStatusPanel";
    statusPanel.jsObject = this;
    this.options.statusPanel = statusPanel;
    this.options.mainPanel.appendChild(statusPanel);

    statusPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        this.jsObject.options.paintPanel.style.bottom = (this.offsetHeight + this.jsObject.options.pagesPanel.offsetHeight) + "px";
    }

    var innerTable = this.CreateHTMLTable();
    innerTable.style.width = "100%";
    statusPanel.appendChild(innerTable);

    /* Unit Button */
    var unitButton = this.StatusPanelButton("unitButton", this.loc.PropertyEnum.StiReportUnitTypeCentimeters, null, null, "Up", 35);
    unitButton.innerTable.style.margin = "0 10px 0 10px";
    unitButton.style.marginLeft = "3px";
    innerTable.addCell(unitButton);
    this.UnitMenu();

    unitButton.updateCaption = function (reportUnit) {
        var captionText = "";
        switch (reportUnit) {
            case "cm": { captionText = unitButton.jsObject.loc.PropertyEnum.StiReportUnitTypeCentimeters; break; }
            case "hi": { captionText = unitButton.jsObject.loc.PropertyEnum.StiReportUnitTypeHundredthsOfInch; break; }
            case "in": { captionText = unitButton.jsObject.loc.PropertyEnum.StiReportUnitTypeInches; break; }
            case "mm": { captionText= unitButton.jsObject.loc.PropertyEnum.StiReportUnitTypeMillimeters   ; break; }
        }
        unitButton.caption.innerHTML = captionText;
    }

    var sep1 = document.createElement("div");
    sep1.className = "stiDesignerStatusPanelSeparator";
    innerTable.addCell(sep1);

    /* Report Checker */
    var reportCheckerButton = this.StatusPanelButton("reportCheckerButton", this.loc.MainMenu.menuCheckIssues, "Arrows.ArrowRightWhite.png", null, null, 35);
    innerTable.addCell(reportCheckerButton);

    reportCheckerButton.action = function () {
        var jsObject = this.jsObject;
        jsObject.SendCommandGetReportCheckItems(function (answer) {
            if (answer.checkItems) {
                jsObject.InitializeCheckPanel(function (checkPanel) {
                    checkPanel.show(answer.checkItems);
                    checkPanel.style.left = "10px";
                    checkPanel.style.bottom = "45px";
                });
            }
        });
    }

    innerTable.addCell(this.StatusPanelSeparator());

    /* Component Name */
    statusPanel.componentNameCell = innerTable.addCell();
    statusPanel.componentNameCell.style.padding = "0px 10px 0px 10px";
    statusPanel.componentNameCell.style.whiteSpace = "nowrap";

    innerTable.addCell(this.StatusPanelSeparator());

    /* Component Name */
    statusPanel.positionsCell = innerTable.addCell();
    statusPanel.positionsCell.style.display = "none";
    statusPanel.positionsCell.style.padding = "0px 10px 0px 10px";
    statusPanel.positionsCell.style.whiteSpace = "nowrap";
        
    var sep4 = this.StatusPanelSeparator();
    sep4.style.display = "none";
    innerTable.addCell(sep4);

    statusPanel.showPositions = function (x, y, width, height) {
        var posText = "";
        if (x != null) posText += " X:" + this.jsObject.RoundByGridSize(this.jsObject.StrToDouble(x));
        if (y != null) posText += " Y:" + this.jsObject.RoundByGridSize(this.jsObject.StrToDouble(y));
        if (width != null) posText += " " + this.jsObject.loc.PropertyMain.Width + ":" + this.jsObject.RoundByGridSize(this.jsObject.StrToDouble(width));
        if (height != null) posText += " " + this.jsObject.loc.PropertyMain.Height + ":" + this.jsObject.RoundByGridSize(this.jsObject.StrToDouble(height));
        statusPanel.positionsCell.innerHTML = posText;
        statusPanel.positionsCell.style.display = posText ? "" : "none";
        sep4.style.display = posText ? "" : "none";
    }

    /* Loading Image */
    innerTable.addCell(this.ProcessImageStatusPanel()).style.width = "100%";

    //Page Width & Height
    innerTable.addCell(this.StatusPanelSeparator());

    var pageWidthButton = this.StatusPanelButton("zoomPageWidth", null, "ZoomPageWidth.png", this.loc.PropertyMain.PageWidth, null, null, 35);
    innerTable.addCell(pageWidthButton);

    pageWidthButton.action = function () {
        this.jsObject.SetZoomBy("width");
    }

    var pageHeightButton = this.StatusPanelButton("zoomPageHeight", null, "ZoomPageHeight.png", this.loc.PropertyMain.PageHeight, null, null, 35);
    innerTable.addCell(pageHeightButton);

    pageHeightButton.action = function () {
        this.jsObject.SetZoomBy("height");
    }

    innerTable.addCell(this.StatusPanelSeparator());

    /* Zoom */
    innerTable.addCell(this.ZoomControl());
}

StiMobileDesigner.prototype.StatusPanelSeparator = function () {
    var sep = document.createElement("div");
    sep.className = "stiDesignerStatusPanelSeparator";

    return sep;
}

StiMobileDesigner.prototype.SetZoomBy = function (zoomType) {
    if (!this.options.currentPage || !this.options.report) return;
    var newZoom = ((this.options.paintPanel["offset" + this.UpperFirstChar(zoomType)] - this.options.paintPanelPadding * 2) *
        this.options.report.zoom / this.options.currentPage[zoomType + "Px"]);
    if (newZoom > 2) newZoom = 2;
    this.options.report.zoom = newZoom;
    this.PreZoomPage(this.options.currentPage);
}