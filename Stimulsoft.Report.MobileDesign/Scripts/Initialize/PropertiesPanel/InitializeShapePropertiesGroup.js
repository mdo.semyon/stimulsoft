﻿
StiMobileDesigner.prototype.ShapePropertiesGroup = function () {
    var shapePropertiesGroup = this.PropertiesGroup("shapePropertiesGroup", this.loc.PropertyCategory.ShapeCategory);
    shapePropertiesGroup.style.margin = "5px 0 5px 0";
    shapePropertiesGroup.style.display = "none";

    //Shape Type
    var controlPropertyShapeType = this.PropertyDropDownList("controlPropertyShapeType", this.options.propertyControlWidth, this.GetShapeTypeItems(), true, false);
    controlPropertyShapeType.action = function () {
        this.jsObject.ApplyPropertyValue("shapeType", this.key);
    }
    shapePropertiesGroup.container.appendChild(this.Property("shapeType", this.loc.PropertyMain.ShapeType, controlPropertyShapeType))
    
    //Style
    var controlPropertyShapeBorderStyle = this.PropertyDropDownList("controlPropertyShapeBorderStyle", this.options.propertyControlWidth, this.GetBorderStyleItems(), true, true);
    controlPropertyShapeBorderStyle.action = function () {
        this.jsObject.ApplyPropertyValue("shapeBorderStyle", this.key);
    };
    shapePropertiesGroup.container.appendChild(this.Property("shapeBorderStyle", this.loc.PropertyMain.Style, controlPropertyShapeBorderStyle, "Style"))
    
    //Size
    var controlPropertyShapeBorderSize = this.PropertyTextBox("controlPropertyShapeBorderSize", this.options.propertyNumbersControlWidth);
    controlPropertyShapeBorderSize.action = function() {        
        this.value = Math.abs(this.jsObject.StrToDouble(this.value));
        this.jsObject.ApplyPropertyValue("size", this.value);
    }
    shapePropertiesGroup.container.appendChild(this.Property("shapeBorderSize", this.loc.PropertyMain.Size, controlPropertyShapeBorderSize, "Size"));
    
    //Border Color
    var controlPropertyShapeBorderColor = this.PropertyColorControl("controlPropertyShapeBorderColor", null, this.options.propertyControlWidth);
    controlPropertyShapeBorderColor.action = function () {
        this.jsObject.ApplyPropertyValue("shapeBorderColor", this.key);
    }    
    shapePropertiesGroup.container.appendChild(this.Property("shapeBorderColor", this.loc.PropertyMain.BorderColor, controlPropertyShapeBorderColor, "BorderColor"));
    
    return shapePropertiesGroup;
}