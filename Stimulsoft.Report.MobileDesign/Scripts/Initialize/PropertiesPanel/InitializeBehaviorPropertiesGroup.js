﻿
StiMobileDesigner.prototype.BehaviorPropertiesGroup = function () {
    var behaviorPropertiesGroup = this.PropertiesGroup("behaviorPropertiesGroup", this.loc.PropertyCategory.BehaviorCategory);
    behaviorPropertiesGroup.style.margin = "5px 0 5px 0";
    behaviorPropertiesGroup.style.display = "none";

    //Interaction Button
    var interactionButtonBlock = this.PropertyBlockWithButton("propertiesInteractionButtonBlock", "PropertyPanel.PropertyInteraction.png", this.loc.PropertyMain.Interaction + "...");
    behaviorPropertiesGroup.container.appendChild(interactionButtonBlock);

    interactionButtonBlock.button.action = function () {
        this.jsObject.InitializeInteractionForm(function (interactionForm) {
            interactionForm.show();
        });
    }

    //Anchor
    var controlPropertyAnchor = this.PropertyAnchorControl("controlPropertyAnchor", this.options.propertyControlWidth);
    controlPropertyAnchor.action = function () {
        this.jsObject.ApplyPropertyValue("anchor", this.key);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("anchor", this.loc.PropertyMain.Anchor, controlPropertyAnchor));

    //AutoWidth
    var controlPropertyAutoWidth = this.CheckBox("controlPropertyAutoWidth");
    controlPropertyAutoWidth.action = function () {
        this.jsObject.ApplyPropertyValue("autoWidth", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("autoWidth", this.loc.PropertyMain.AutoWidth, controlPropertyAutoWidth));

    //CalcInvisible
    var controlPropertyCalcInvisible = this.CheckBox("controlPropertyCalcInvisible");
    controlPropertyCalcInvisible.action = function () {
        this.jsObject.ApplyPropertyValue("calcInvisible", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("calcInvisible", this.loc.PropertyMain.CalcInvisible, controlPropertyCalcInvisible));

    //CanGrow
    var controlPropertyCanGrow = this.CheckBox("controlPropertyCanGrow");
    controlPropertyCanGrow.action = function () {
        this.jsObject.ApplyPropertyValue("canGrow", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("canGrow", this.loc.PropertyMain.CanGrow, controlPropertyCanGrow));

    //CanShrink
    var controlPropertyCanShrink = this.CheckBox("controlPropertyCanShrink");
    controlPropertyCanShrink.action = function () {
        this.jsObject.ApplyPropertyValue("canShrink", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("canShrink", this.loc.PropertyMain.CanShrink, controlPropertyCanShrink));

    //CanBreak
    var controlPropertyCanBreak = this.CheckBox("controlPropertyCanBreak");
    controlPropertyCanBreak.action = function () {
        this.jsObject.ApplyPropertyValue("canBreak", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("canBreak", this.loc.PropertyMain.CanBreak, controlPropertyCanBreak));

    //GrowToHeight
    var controlPropertyGrowToHeight = this.CheckBox("controlPropertyGrowToHeight");
    controlPropertyGrowToHeight.action = function () {
        this.jsObject.ApplyPropertyValue("growToHeight", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("growToHeight", this.loc.PropertyMain.GrowToHeight, controlPropertyGrowToHeight));
        
    //DockStyle
    var controlPropertyDockStyle = this.PropertyDropDownList("controlPropertyDockStyle", this.options.propertyControlWidth, this.GetDockStyleItems(), true, false);
    controlPropertyDockStyle.action = function () {
        this.jsObject.ApplyPropertyValue("dockStyle", this.key);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("dockStyle", this.loc.PropertyMain.DockStyle, controlPropertyDockStyle));

    //Enabled
    var controlPropertyEnabled = this.CheckBox("controlPropertyEnabled");
    controlPropertyEnabled.action = function () {
        this.jsObject.ApplyPropertyValue("enabled", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("enabled", this.loc.PropertyMain.Enabled, controlPropertyEnabled));

    //KeepGroupTogether
    var controlPropertyKeepGroupTogether = this.CheckBox("controlPropertyKeepGroupTogether");
    controlPropertyKeepGroupTogether.action = function () {
        this.jsObject.ApplyPropertyValue("keepGroupTogether", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("keepGroupTogether", this.loc.PropertyMain.KeepGroupTogether, controlPropertyKeepGroupTogether));

    //KeepGroupHeaderTogether
    var controlPropertyKeepGroupHeaderTogether = this.CheckBox("controlPropertyKeepGroupHeaderTogether");
    controlPropertyKeepGroupHeaderTogether.action = function () {
        this.jsObject.ApplyPropertyValue("keepGroupHeaderTogether", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("keepGroupHeaderTogether", this.loc.PropertyMain.KeepGroupHeaderTogether, controlPropertyKeepGroupHeaderTogether));

    //KeepGroupFooterTogether
    var controlPropertyKeepGroupFooterTogether = this.CheckBox("controlPropertyKeepGroupFooterTogether");
    controlPropertyKeepGroupFooterTogether.action = function () {
        this.jsObject.ApplyPropertyValue("keepGroupFooterTogether", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("keepGroupFooterTogether", this.loc.PropertyMain.KeepGroupFooterTogether, controlPropertyKeepGroupFooterTogether));

    //KeepHeaderTogether
    var controlPropertyKeepHeaderTogether = this.CheckBox("controlPropertyKeepHeaderTogether");
    controlPropertyKeepHeaderTogether.action = function () {
        this.jsObject.ApplyPropertyValue("keepHeaderTogether", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("keepHeaderTogether", this.loc.PropertyMain.KeepHeaderTogether, controlPropertyKeepHeaderTogether));

    //KeepFooterTogether
    var controlPropertyKeepFooterTogether = this.CheckBox("controlPropertyKeepFooterTogether");
    controlPropertyKeepFooterTogether.action = function () {
        this.jsObject.ApplyPropertyValue("keepFooterTogether", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("keepFooterTogether", this.loc.PropertyMain.KeepFooterTogether, controlPropertyKeepFooterTogether));

    //KeepDetailsTogether
    var controlPropertyKeepDetailsTogether = this.CheckBox("controlPropertyKeepDetailsTogether");
    controlPropertyKeepDetailsTogether.action = function () {
        this.jsObject.ApplyPropertyValue("keepDetailsTogether", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("keepDetailsTogether", this.loc.PropertyMain.KeepDetailsTogether, controlPropertyKeepDetailsTogether));

    //PrintAtBottom
    var controlPropertyPrintAtBottom = this.CheckBox("controlPropertyPrintAtBottom");
    controlPropertyPrintAtBottom.action = function () {
        this.jsObject.ApplyPropertyValue("printAtBottom", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("printAtBottom", this.loc.PropertyMain.PrintAtBottom, controlPropertyPrintAtBottom));

    //PrintIfDetailEmpty
    var controlPropertyPrintIfDetailEmpty = this.CheckBox("controlPropertyPrintIfDetailEmpty");
    controlPropertyPrintIfDetailEmpty.action = function () {
        this.jsObject.ApplyPropertyValue("printIfDetailEmpty", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("printIfDetailEmpty", this.loc.PropertyMain.PrintIfDetailEmpty, controlPropertyPrintIfDetailEmpty));

    //PrintOn
    var controlPropertyPrintOn = this.PropertyDropDownList("controlPropertyPrintOn", this.options.propertyControlWidth, this.GetPrintOnItems(), true, false);
    controlPropertyPrintOn.action = function () {
        this.jsObject.ApplyPropertyValue("printOn", this.key);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("printOn", this.loc.PropertyMain.PrintOn, controlPropertyPrintOn));

    //PrintOnAllPages
    var controlPropertyPrintOnAllPages = this.CheckBox("controlPropertyPrintOnAllPages");
    controlPropertyPrintOnAllPages.action = function () {
        this.jsObject.ApplyPropertyValue("printOnAllPages", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("printOnAllPages", this.loc.PropertyMain.PrintOnAllPages, controlPropertyPrintOnAllPages));

    //Printable
    var controlPropertyPrintable = this.CheckBox("controlPropertyPrintable");
    controlPropertyPrintable.action = function () {
        this.jsObject.ApplyPropertyValue("printable", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("printable", this.loc.PropertyMain.Printable, controlPropertyPrintable));

    //PrintIfEmpty
    var controlPropertyPrintIfEmpty = this.CheckBox("controlPropertyPrintIfEmpty");
    controlPropertyPrintIfEmpty.action = function () {
        this.jsObject.ApplyPropertyValue("printIfEmpty", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("printIfEmpty", this.loc.PropertyMain.PrintIfEmpty, controlPropertyPrintIfEmpty));

    //ResetPageNumber
    var controlPropertyResetPageNumber = this.CheckBox("controlPropertyResetPageNumber");
    controlPropertyResetPageNumber.action = function () {
        this.jsObject.ApplyPropertyValue("resetPageNumber", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("resetPageNumber", this.loc.PropertyMain.ResetPageNumber, controlPropertyResetPageNumber));

    //PrintOnPreviousPage
    var controlPropertyPrintOnPreviousPage = this.CheckBox("controlPropertyPrintOnPreviousPage");
    controlPropertyPrintOnPreviousPage.action = function () {
        this.jsObject.ApplyPropertyValue("printOnPreviousPage", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("printOnPreviousPage", this.loc.PropertyMain.PrintOnPreviousPage, controlPropertyPrintOnPreviousPage));

    //PrintHeadersFootersFromPreviousPage
    var controlPropertyPrintHeadersFootersFromPreviousPage = this.CheckBox("controlPropertyPrintHeadersFootersFromPreviousPage");
    controlPropertyPrintHeadersFootersFromPreviousPage.action = function () {
        this.jsObject.ApplyPropertyValue("printHeadersFootersFromPreviousPage", this.isChecked);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("printHeadersFootersFromPreviousPage", this.loc.PropertyMain.PrintHeadersFootersFromPreviousPage, controlPropertyPrintHeadersFootersFromPreviousPage));

    //Shift Mode
    var controlPropertyShiftMode = this.PropertyShiftModeControl("controlPropertyShiftMode", this.options.propertyControlWidth);
    controlPropertyShiftMode.action = function () {
        this.jsObject.ApplyPropertyValue("shiftMode", this.key);
    }
    behaviorPropertiesGroup.container.appendChild(this.Property("shiftMode", this.loc.PropertyMain.ShiftMode, controlPropertyShiftMode));

    return behaviorPropertiesGroup;
}