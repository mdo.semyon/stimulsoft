﻿
StiMobileDesigner.prototype.PropertyColorControl = function (name, toolTip, width) {
    var propertyColorControl = this.ColorControl(name, toolTip, true);

    //Ovveride New Button
    var newButton = this.SmallButton(name + "Button", null, "0,0,0", "BrushSolid.png", toolTip, "Down", this.GetStyles("PropertiesBrushControlButton"));
    newButton.style.width = (width + 4) + "px";
    newButton.caption.style.width = "100%";
    newButton.colorControl = propertyColorControl;

    var colorBar = document.createElement("div");
    colorBar.className = "stiImageControlBrushPropertyForSolid";

    var imageCell = newButton.image.parentElement;
    imageCell.removeChild(newButton.image);
    imageCell.appendChild(colorBar);
    newButton.image = colorBar;

    //Override Old Button
    var oldButtonCell = propertyColorControl.button.parentElement;
    oldButtonCell.removeChild(propertyColorControl.button);
    oldButtonCell.appendChild(newButton);
    propertyColorControl.button = newButton;

    //Override Methods
    propertyColorControl.button.action = function () {
        var colorDialog = this.jsObject.options.menus.colorDialog || this.jsObject.InitializeColorDialog();
        colorDialog.rightToLeft = this.colorControl.rightToLeft;
        colorDialog.changeVisibleState(!colorDialog.visible, this);
    }

    propertyColorControl.button.choosedColor = function (key) {
        this.colorControl.setKey(key);
        this.colorControl.action();
    };

    propertyColorControl.setKey = function (key) {
        this.key = key;
        if (key == "StiEmptyValue") {
            this.button.image.style.opacity = 0;
            this.button.caption.innerHTML = "";
            return;
        }
        this.button.image.style.opacity = 1;
        var color;
        if (key == "transparent")
            color = "255,255,255";
        else {
            var colors = key.split(",");
            if (colors.length == 4) {
                this.button.image.style.opacity = this.jsObject.StrToInt(colors[0]) / 255;
                colors.splice(0, 1);
            }
            color = colors[0] + "," + colors[1] + "," + colors[2];
        }

        this.button.image.style.background = "rgb(" + color + ")";
        var colorName = this.jsObject.GetColorNameByRGB(this.key);
        this.button.caption.innerHTML = colorName || this.key;
    };

    return propertyColorControl;
}