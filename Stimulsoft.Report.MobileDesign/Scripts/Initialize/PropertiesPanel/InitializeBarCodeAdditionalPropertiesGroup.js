﻿
StiMobileDesigner.prototype.BarCodeAdditionalPropertiesGroup = function () {
    var barCodeAdditionalPropertiesGroup = this.PropertiesGroup("barCodeAdditionalPropertiesGroup", this.loc.PropertyCategory.BarCodeAdditionalCategory);
    barCodeAdditionalPropertiesGroup.style.margin = "5px 0 5px 0";
    barCodeAdditionalPropertiesGroup.style.display = "none";
    
    //Angle
    var controlPropertyBarCodeAngle = this.PropertyDropDownList("controlPropertyBarCodeAngle", this.options.propertyControlWidth, this.GetBarCodeAngleItems(), true, false);
    controlPropertyBarCodeAngle.action = function () {
        this.jsObject.ApplyPropertyValue("barCodeAngle", this.key);
    }
    barCodeAdditionalPropertiesGroup.container.appendChild(this.Property("barCodeAngle", this.loc.PropertyMain.Angle, controlPropertyBarCodeAngle, "Angle"));
    
    //Auto Scale
    var controlPropertyBarCodeAutoScale = this.CheckBox("controlPropertyBarCodeAutoScale");
    controlPropertyBarCodeAutoScale.action = function() {
        this.jsObject.ApplyPropertyValue("autoScale", this.isChecked);
    }
    barCodeAdditionalPropertiesGroup.container.appendChild(this.Property("barCodeAutoScale", this.loc.PropertyMain.AutoScale, controlPropertyBarCodeAutoScale, "AutoScale"));
    
    //Font
    var controlPropertyBarCodeFont = this.PropertyFontControl("BarCodeFont", "font");
    barCodeAdditionalPropertiesGroup.container.appendChild(this.Property("barCodeFont", this.loc.PropertyMain.Font, controlPropertyBarCodeFont, "Font"));
    
    //Fore Color
    var controlPropertyBarCodeForeColor = this.PropertyColorControl("controlPropertyBarCodeForeColor", null, this.options.propertyControlWidth);
    controlPropertyBarCodeForeColor.action = function () {
        this.jsObject.ApplyPropertyValue("foreColor", this.key);
    }
    barCodeAdditionalPropertiesGroup.container.appendChild(this.Property("barCodeForeColor", this.loc.PropertyMain.ForeColor, controlPropertyBarCodeForeColor, "ForeColor"));

    //Back Color
    var controlPropertyBarCodeBackColor = this.PropertyColorControl("controlPropertyBarCodeBackColor", null, this.options.propertyControlWidth);
    controlPropertyBarCodeBackColor.action = function () {
        this.jsObject.ApplyPropertyValue("backColor", this.key);
    }
    barCodeAdditionalPropertiesGroup.container.appendChild(this.Property("barCodeBackColor", this.loc.PropertyMain.BackColor, controlPropertyBarCodeBackColor, "BackColor"));

    //Show LabelText
    var controlPropertyBarCodeShowLabelText = this.CheckBox("controlPropertyBarCodeShowLabelText");
    controlPropertyBarCodeShowLabelText.action = function() {
        this.jsObject.ApplyPropertyValue("showLabelText", this.isChecked);
    }
    barCodeAdditionalPropertiesGroup.container.appendChild(this.Property("barCodeShowLabelText", this.loc.PropertyMain.ShowLabelText, controlPropertyBarCodeShowLabelText, "ShowLabelText"));

    //Show QuietZones
    var controlPropertyBarCodeShowQuietZones = this.CheckBox("controlPropertyBarCodeShowQuietZones");
    controlPropertyBarCodeShowQuietZones.action = function () {
        this.jsObject.ApplyPropertyValue("showQuietZones", this.isChecked);
    }
    barCodeAdditionalPropertiesGroup.container.appendChild(this.Property("barCodeShowQuietZones", this.loc.PropertyMain.ShowQuietZones, controlPropertyBarCodeShowQuietZones, "ShowQuietZones"));
                  
    return barCodeAdditionalPropertiesGroup;
}