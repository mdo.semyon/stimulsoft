﻿
StiMobileDesigner.prototype.AppearancePropertiesGroup = function () {
    var appearancePropertiesGroup = this.PropertiesGroup("appearancePropertiesGroup", this.loc.PropertyCategory.AppearanceCategory);
    appearancePropertiesGroup.style.margin = "5px 0 5px 0";
    appearancePropertiesGroup.style.display = "none";

    //Brush
    var controlPropertyBrush = this.PropertyBrushControl("controlPropertyBrush", null, this.options.propertyControlWidth);
    controlPropertyBrush.action = function () {
        this.jsObject.ApplyPropertyValue("brush", this.key);
    }
    appearancePropertiesGroup.container.appendChild(this.Property("brush", this.loc.PropertyMain.Brush, controlPropertyBrush));

    //Border
    var controlPropertyBorder = this.PropertyTextBoxWithEditButton("controlPropertyBorder", this.options.propertyControlWidth, true);
    controlPropertyBorder.button.action = function () {
        this.jsObject.InitializeBorderSetupForm(function (borderSetupForm) {

            borderSetupForm.actionFunction = function (border) {
                this.jsObject.options.controls.controlPropertyBorder.value = this.jsObject.BorderObjectToShotStr(border);
            }
            borderSetupForm.actionFunction = null;
            borderSetupForm.showFunction = null;
            borderSetupForm.changeVisibleState(true);
        });
    }
    appearancePropertiesGroup.container.appendChild(this.Property("border", this.loc.PropertyMain.Border, controlPropertyBorder));

    //Conditions
    var controlPropertyConditions = this.PropertyTextBoxWithEditButton("controlPropertyConditions", this.options.propertyControlWidth, true);
    controlPropertyConditions.button.action = function () {
        this.jsObject.InitializeConditionsForm(function (conditionsForm) {
            conditionsForm.show();
        });
    }
    appearancePropertiesGroup.container.appendChild(this.Property("conditions", this.loc.PropertyMain.Conditions, controlPropertyConditions));

    //Component Style
    var controlPropertyComponentStyle = this.PropertyDropDownList("controlPropertyComponentStyle", this.options.propertyControlWidth, this.GetComponentStyleItems(), true, false);
    controlPropertyComponentStyle.action = function () {
        this.jsObject.ApplyPropertyValue("componentStyle", this.key, true);
    }
    appearancePropertiesGroup.container.appendChild(this.Property("componentStyle", this.loc.PropertyMain.ComponentStyle, controlPropertyComponentStyle));

    //Odd Style
    var controlPropertyOddStyle = this.PropertyDropDownList("controlPropertyOddStyle", this.options.propertyControlWidth, this.GetComponentStyleItems(), true, false);
    controlPropertyOddStyle.action = function () {
        this.jsObject.ApplyPropertyValue("oddStyle", this.key);
    }
    appearancePropertiesGroup.container.appendChild(this.Property("oddStyle", this.loc.PropertyMain.OddStyle, controlPropertyOddStyle));

    //Even Style
    var controlPropertyEvenStyle = this.PropertyDropDownList("controlPropertyEvenStyle", this.options.propertyControlWidth, this.GetComponentStyleItems(), true, false);
    controlPropertyEvenStyle.action = function () {
        this.jsObject.ApplyPropertyValue("evenStyle", this.key);
    }
    appearancePropertiesGroup.container.appendChild(this.Property("evenStyle", this.loc.PropertyMain.EvenStyle, controlPropertyEvenStyle));

    //AutoWidth
    var controlPropertyUseParentStyles = this.CheckBox("controlPropertyUseParentStyles");
    controlPropertyUseParentStyles.action = function () {
        this.jsObject.ApplyPropertyValue("useParentStyles", this.isChecked);
    }
    appearancePropertiesGroup.container.appendChild(this.Property("useParentStyles", this.loc.PropertyMain.UseParentStyles, controlPropertyUseParentStyles));

    return appearancePropertiesGroup;
}