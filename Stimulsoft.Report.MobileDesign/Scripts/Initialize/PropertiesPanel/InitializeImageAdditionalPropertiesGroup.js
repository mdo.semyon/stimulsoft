﻿
StiMobileDesigner.prototype.ImageAdditionalPropertiesGroup = function () {
    var imageAdditionalPropertiesGroup = this.PropertiesGroup("imageAdditionalPropertiesGroup", this.loc.PropertyCategory.ImageAdditionalCategory);
    imageAdditionalPropertiesGroup.style.margin = "5px 0 5px 0";
    imageAdditionalPropertiesGroup.style.display = "none";
    
    //Horizontal Alignment
    var controlPropertyHorAlign = this.PropertyDropDownList("controlPropertyImageHorizontalAlignment",
        this.options.propertyControlWidth, this.GetHorizontalAlignmentItems(true), true, false);
    controlPropertyHorAlign.action = function () {
        this.jsObject.ApplyPropertyValue("horAlignment", this.key);
    }
    imageAdditionalPropertiesGroup.container.appendChild(this.Property("imageHorizontalAlignment",
        this.loc.PropertyMain.HorAlignment, controlPropertyHorAlign, "HorAlignment"));
    
    //Vertical Alignment
    var controlPropertyVertAlign = this.PropertyDropDownList("controlPropertyImageVerticalAlignment",
        this.options.propertyControlWidth, this.GetVerticalAlignmentItems(), true, false);
    controlPropertyVertAlign.action = function () {
        this.jsObject.ApplyPropertyValue("vertAlignment", this.key);
    }
    imageAdditionalPropertiesGroup.container.appendChild(this.Property("imageVerticalAlignment",
        this.loc.PropertyMain.VertAlignment, controlPropertyVertAlign, "VertAlignment"));
    
    //Image Aspect Ratio
    var controlPropertyImageAspectRatioControl = this.CheckBox("controlPropertyImageAspectRatio");
    controlPropertyImageAspectRatioControl.action = function() {
        this.jsObject.ApplyPropertyValue("ratio", this.isChecked);
    }
    imageAdditionalPropertiesGroup.container.appendChild(this.Property("imageAspectRatio",
        this.loc.PropertyMain.AspectRatio, controlPropertyImageAspectRatioControl, "AspectRatio"));
        
    //MultipleFactor
    var controlPropertyImageMultipleFactor = this.PropertyTextBox("controlPropertyImageMultipleFactor", this.options.propertyNumbersControlWidth);
    controlPropertyImageMultipleFactor.action = function() {        
        this.value = Math.abs(this.jsObject.StrToDouble(this.value));
        this.jsObject.ApplyPropertyValue("imageMultipleFactor", this.value);
    }
    imageAdditionalPropertiesGroup.container.appendChild(this.Property("imageMultipleFactor",
        this.loc.PropertyMain.ImageMultipleFactor, controlPropertyImageMultipleFactor, "MultipleFactor"));
        
    //Image Rotation
    var controlPropertyImageRotation = this.PropertyDropDownList("controlPropertyImageRotation",
        this.options.propertyControlWidth, this.GetImageRotationItems(), true, false);
    controlPropertyImageRotation.action = function () {
        this.jsObject.ApplyPropertyValue("rotation", this.key);
    }
    imageAdditionalPropertiesGroup.container.appendChild(this.Property("imageRotation",
        this.loc.PropertyMain.ImageRotation, controlPropertyImageRotation));
    
    //Image Stretch
    var controlPropertyImageStretchControl = this.CheckBox("controlPropertyImageStretch");
    controlPropertyImageStretchControl.action = function() {
        this.jsObject.ApplyPropertyValue("stretch", this.isChecked);
    }
    imageAdditionalPropertiesGroup.container.appendChild(this.Property("imageStretch",
        this.loc.PropertyMain.ImageStretch, controlPropertyImageStretchControl, "Stretch"));
                   
    return imageAdditionalPropertiesGroup;
}