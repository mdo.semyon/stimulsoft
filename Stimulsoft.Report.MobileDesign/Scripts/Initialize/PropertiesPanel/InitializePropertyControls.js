﻿
//DropDown List
StiMobileDesigner.prototype.PropertyDropDownList = function (name, width, items, readOnly, showImage, toolTip, cutMenu) {               
    var propertyDropDownList = this.DropDownList(name, width, toolTip, items, readOnly, showImage, 18, cutMenu != null ? cutMenu : true);
    if (propertyDropDownList.menu.innerContent) propertyDropDownList.menu.innerContent.style.maxHeight = "230px";
    return propertyDropDownList;
}

//TextBox
StiMobileDesigner.prototype.PropertyTextBox = function (name, width) {               
    return this.TextBox(name, width, 18);
}

//TextBox With EditButton
StiMobileDesigner.prototype.PropertyTextBoxWithEditButton = function (name, width, readOnly) {
    var textBoxWithEditButton = this.TextBoxWithEditButton(name, width, 18);
    if (readOnly) {
        textBoxWithEditButton.textBox.readOnly = readOnly;
        textBoxWithEditButton.textBox.style.cursor = "default";
        textBoxWithEditButton.textBox.mainControl = textBoxWithEditButton;

        textBoxWithEditButton.textBox.onclick = function () {
            if (!this.isTouchEndFlag)
                this.mainControl.button.onclick();
        }

        textBoxWithEditButton.textBox.ontouchend = function () {
            var this_ = this;
            this.isTouchEndFlag = true;
            clearTimeout(this.isTouchEndTimer);
            this.mainControl.button.ontouchend();
            this.isTouchEndTimer = setTimeout(function () {
                this_.isTouchEndFlag = false;
            }, 1000);
        }
    }

    return textBoxWithEditButton;
}

//Data Control With EditButton
StiMobileDesigner.prototype.PropertyDataControl = function (name, width) {
    var dataControl = this.PropertyTextBoxWithEditButton(name, width, true);

    dataControl.button.action = function () {
        this.key = this.textBox.value;
        var this_ = this;

        this.jsObject.InitializeDataColumnForm(function (dataColumnForm) {
            dataColumnForm.parentButton = this_;
            dataColumnForm.changeVisibleState(true);
            
            dataColumnForm.action = function () {
                this.changeVisibleState(false);
                this.parentButton.textBox.value = this.dataTree.key;
                if (dataControl["action"] != null) dataControl.action();
            }
        });
    }

    return dataControl;
}

//Expression Control
StiMobileDesigner.prototype.PropertyExpressionControl = function (name, width, readOnly, cutExpression) {
    var expressionControl = this.ExpressionControl(name, width, 18, readOnly != null ? readOnly : true, cutExpression);

    return expressionControl;
}

//Font List
StiMobileDesigner.prototype.PropertyFontList = function (name, width) {               
    var propertyFontList = this.FontList(name, width, 18, true);
    if (propertyFontList.menu.innerContent) propertyFontList.menu.innerContent.style.maxHeight = "230px";
    return propertyFontList;
}

//PropertyDataDropDownList
StiMobileDesigner.prototype.PropertyDataDropDownList = function (name, width, toolTip) {
    var propertyDataDropDownList = this.PropertyDropDownList(name, width, null, true, false, toolTip);
    
    //Override
    propertyDataDropDownList.addItems = function (items) {
        this.items = [];
        this.items.push(this.jsObject.Item("NotAssigned",
            this.jsObject.loc.Report.NotAssigned, null, "[Not Assigned]"));
        if (!items) return;
        for (var index in items) {
            this.items.push(items[index]);    
        }
    }
    
    propertyDataDropDownList.reset = function () {
        this.setKey("[Not Assigned]");
    }
    
    return propertyDataDropDownList;
}

//PropertyBusinessObject
StiMobileDesigner.prototype.PropertyBusinessObject = function (name) {
    var control = this.PropertyTextBoxWithEditButton(name, this.options.propertyControlWidth, true);

    control.button.action = function () {
        var this_ = this;
        
        this.jsObject.InitializeDataBusinessObjectForm(function (dataBusinessObjectForm) {
            dataBusinessObjectForm.dataTree.build("BusinessObject");
            this_.key = control.textBox.value;
            dataBusinessObjectForm.parentButton = this_;
            dataBusinessObjectForm.changeVisibleState(true);

            dataBusinessObjectForm.action = function () {
                dataBusinessObjectForm.changeVisibleState(false);
                dataBusinessObjectForm.parentButton.textBox.value = this.dataTree.key != ""
                ? this.dataTree.key
                : dataBusinessObjectForm.jsObject.loc.Report.NotAssigned;
                control.action();
            }
        });
    }

    control.action = function () { };

    return control;
}

//Property Filter
StiMobileDesigner.prototype.PropertyFilter = function (name) {

    var control = this.PropertyTextBoxWithEditButton(name, this.options.propertyControlWidth, true);

    control.button.action = function () {
        this.jsObject.InitializeFilterForm(function (filterForm) {
            filterForm.resultControl = control;
            filterForm.resultControl.formResult = control.key;

            if (control.dataSourceControl) {
                control.currentDataSourceName = control.dataSourceControl.key;
                filterForm.changeVisibleState(true);
            }
        });
    }

    control.setKey = function (key) {
        if (key == null) return;
        control.key = key;
        control.textBox.value = this.jsObject.FilterDataToShortString(JSON.parse(Base64.decode(key.filterData)));
    }

    return control;
}

//Property Sort
StiMobileDesigner.prototype.PropertySort = function (name) {

    var control = this.PropertyTextBoxWithEditButton(name, this.options.propertyControlWidth, true);

    control.button.action = function () {
        this.jsObject.InitializeSortForm(function (sortForm) {
            sortForm.resultControl = control;
            sortForm.resultControl.formResult = control.key;

            if (control.dataSourceControl) {
                control.currentDataSourceName = control.dataSourceControl.key;
                sortForm.changeVisibleState(true);
            }
        });
    }

    control.setKey = function (key) {
        if (key == null) return;
        control.key = key;
        control.textBox.value = this.jsObject.SortDataToShortString(key);
    }

    return control;
}

//Property Sort
StiMobileDesigner.prototype.PropertyBlockWithButton = function (name, imageName, caption) {
    var propertyBlock = this.CreateHTMLTable();
    propertyBlock.name = name;
    if (name) this.options.controls[name] = propertyBlock;
    propertyBlock.style.width = "100%";
    var image = document.createElement("img");
    propertyBlock.image = image;
    if (this.options.images[imageName]) image.src = this.options.images[imageName];
    image.style.margin = "4px 0px 4px 6px";
    var imageCell = propertyBlock.addCell(image);
    imageCell.style.width = "1px";
    imageCell.style.fontSize = "0";
    imageCell.style.lineHeight = "0";

    var button = this.SmallButton(null, null, caption, null, null, null, this.GetStyles("FormButton"));
    propertyBlock.button = button;
    //button.style.height = "26px";
    button.style.width = (this.options.propertyControlWidth + 128) + "px";
    button.style.margin = "4px 0px 4px 8px";
    button.innerTable.style.width = "100%";
    button.caption.style.textAlign = "center";
    propertyBlock.addCell(button);
    
    return propertyBlock;
}

//ColorsCollection
StiMobileDesigner.prototype.PropertyColorsCollectionControl = function (name, toolTip, width) {
    var colorsControl = this.ColorsCollectionControl(name, toolTip, width, 18);

    return colorsControl;
}