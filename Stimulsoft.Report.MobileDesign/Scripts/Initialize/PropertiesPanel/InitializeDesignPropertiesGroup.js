﻿
StiMobileDesigner.prototype.DesignPropertiesGroup = function () {
    var designPropertiesGroup = this.PropertiesGroup("designPropertiesGroup", this.loc.PropertyCategory.DesignCategory);
    designPropertiesGroup.style.margin = "5px 0 5px 0";
    designPropertiesGroup.style.display = "none";

    //Name
    var controlPropertyComponentName = this.PropertyTextBox("controlPropertyComponentName", this.options.propertyControlWidth);
    controlPropertyComponentName.action = function () {
        if (this.jsObject.options.selectedObject)
            this.jsObject.SendCommandRenameComponent(this.jsObject.options.selectedObject, this.value);
    }
    designPropertiesGroup.container.appendChild(this.Property("componentName",
        this.loc.PropertyMain.Name, controlPropertyComponentName, "Name"));

    //Alias
    var controlPropertyAlias = this.PropertyTextBox("controlPropertyAlias", this.options.propertyControlWidth);
    controlPropertyAlias.action = function () {
        this.jsObject.ApplyPropertyValue("aliasName", Base64.encode(this.value));
    }
    designPropertiesGroup.container.appendChild(this.Property("aliasName",
        this.loc.PropertyMain.Alias, controlPropertyAlias, "Alias"));

    //Restrictions
    var controlPropertyRestrictions = this.PropertyRestrictionsControl("controlPropertyRestrictions", this.options.propertyControlWidth);
    controlPropertyRestrictions.action = function () {
        this.jsObject.ApplyPropertyValue("restrictions", this.key);
    }
    designPropertiesGroup.container.appendChild(this.Property("restrictions",
        this.loc.PropertyMain.Restrictions, controlPropertyRestrictions));

    //Locked
    var controlPropertyLocked = this.CheckBox("controlPropertyLocked");
    controlPropertyLocked.action = function () {
        this.jsObject.ApplyPropertyValue("locked", this.isChecked);
    }
    designPropertiesGroup.container.appendChild(this.Property("locked", this.loc.PropertyMain.Locked, controlPropertyLocked));
    
    //Linked
    var controlPropertyLinked = this.CheckBox("controlPropertyLinked");
    controlPropertyLinked.action = function () {
        this.jsObject.ApplyPropertyValue("linked", this.isChecked);
    }
    designPropertiesGroup.container.appendChild(this.Property("linked", this.loc.PropertyMain.Linked, controlPropertyLinked));

    //LargeHeight
    var controlPropertyLargeHeight = this.CheckBox("controlPropertyLargeHeight");
    controlPropertyLargeHeight.action = function () {
        this.jsObject.ApplyPropertyValue("largeHeight", this.isChecked);
    }
    designPropertiesGroup.container.appendChild(this.Property("largeHeight",
        this.loc.PropertyMain.LargeHeight, controlPropertyLargeHeight));

    //LargeHeightFactor
    var controlLargeHeightFactor = this.PropertyTextBox("controlPropertyLargeHeightFactor", this.options.propertyNumbersControlWidth);
    controlLargeHeightFactor.action = function () {
        var val = Math.abs(this.jsObject.StrToInt(this.value));
        this.value = val == 0 ? 1 : val;
        this.jsObject.ApplyPropertyValue("largeHeightFactor", this.value);
    }
    designPropertiesGroup.container.appendChild(this.Property("largeHeightFactor",
        this.loc.PropertyMain.LargeHeightFactor, controlLargeHeightFactor));
            

    return designPropertiesGroup;
}