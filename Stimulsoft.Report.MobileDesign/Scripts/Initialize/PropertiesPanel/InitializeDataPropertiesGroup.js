﻿
StiMobileDesigner.prototype.DataPropertiesGroup = function () {
    var dataPropertiesGroup = this.PropertiesGroup("dataPropertiesGroup", this.loc.PropertyCategory.DataCategory);
    dataPropertiesGroup.style.margin = "5px 0 5px 0";
    dataPropertiesGroup.style.display = "none";

    //Vertical Alignment
    var controlPropertyVertAlign = this.PropertyDropDownList("controlPropertyDataVerticalAlignment", this.options.propertyControlWidth, this.GetVerticalAlignmentItems(), true, false);
    controlPropertyVertAlign.action = function () {
        this.jsObject.ApplyPropertyValue("vertAlignment", this.key);
    }
    dataPropertiesGroup.container.appendChild(this.Property("dataVerticalAlignment", this.loc.PropertyMain.VertAlignment, controlPropertyVertAlign, "VertAlignment"));

    return dataPropertiesGroup;
}