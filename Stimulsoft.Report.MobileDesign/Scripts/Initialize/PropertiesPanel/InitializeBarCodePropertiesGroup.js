﻿
StiMobileDesigner.prototype.BarCodePropertiesGroup = function () {
    var barCodePropertiesGroup = this.PropertiesGroup("barCodePropertiesGroup", this.loc.PropertyCategory.BarCodeCategory);
    barCodePropertiesGroup.style.margin = "5px 0 5px 0";
    barCodePropertiesGroup.style.display = "none";

    //Code
    var controlPropertyCode = this.PropertyTextBoxWithEditButton("controlPropertyBarCode", this.options.propertyControlWidth);
    controlPropertyCode.textBox.action = function () {
        this.jsObject.ApplyPropertyValue("code", Base64.encode(this.value));
    }
    controlPropertyCode.button.action = function () {
        this.jsObject.InitializeTextEditorForm(function (textEditorForm) {
            textEditorForm.propertyName = "code";
            textEditorForm.changeVisibleState(true);
        });
    }
    barCodePropertiesGroup.container.appendChild(this.Property("barCode", this.loc.PropertyMain.Code, controlPropertyCode, "Code"));

    //BarCode Type
    var controlPropertyBarCodeType = this.PropertyDropDownList("controlPropertyBarCodeType", this.options.propertyControlWidth, this.GetBarCodeTypeItems(), true, false);
    controlPropertyBarCodeType.action = function () {
        this.jsObject.ApplyPropertyValue("codeType", this.key);
    }
    barCodePropertiesGroup.container.appendChild(this.Property("barCodeType", this.loc.PropertyMain.BarCodeType, controlPropertyBarCodeType))

    //Horizontal Alignment
    var controlPropertyHorAlign = this.PropertyDropDownList("controlPropertyBarCodeHorizontalAlignment", this.options.propertyControlWidth, this.GetHorizontalAlignmentItems(true), true, false);
    controlPropertyHorAlign.action = function () {
        this.jsObject.ApplyPropertyValue("horAlignment", this.key);
    }
    barCodePropertiesGroup.container.appendChild(this.Property("barCodeHorizontalAlignment", this.loc.PropertyMain.HorAlignment, controlPropertyHorAlign, "HorAlignment"));

    //Vertical Alignment
    var controlPropertyVertAlign = this.PropertyDropDownList("controlPropertyBarCodeVerticalAlignment", this.options.propertyControlWidth, this.GetVerticalAlignmentItems(), true, false);
    controlPropertyVertAlign.action = function () {
        this.jsObject.ApplyPropertyValue("vertAlignment", this.key);
    }
    barCodePropertiesGroup.container.appendChild(this.Property("barCodeVerticalAlignment", this.loc.PropertyMain.VertAlignment, controlPropertyVertAlign, "VertAlignment"));

    return barCodePropertiesGroup;
}