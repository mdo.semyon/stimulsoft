﻿
//Border Control
StiMobileDesigner.prototype.PropertyBorderControl = function (name, width) {
    var borderControl = this.PropertyTextBoxWithEditButton(name, width, true);
    borderControl.key = null;

    borderControl.button.action = function () {
        this.jsObject.InitializeBorderSetupForm(function (borderSetupForm) {
            
            borderSetupForm.actionFunction = function (border) {
                borderSetupForm.finishFlag = true;
                borderSetupForm.changeVisibleState(false);
                borderControl.setKey(this.jsObject.BordersObjectToStr(border));
                borderControl.action();
            }
            
            borderSetupForm.showFunction = function () {
                borderSetupForm.border = borderControl.key;
            }
            
            borderSetupForm.changeVisibleState(true);
        });
    }

    borderControl.setKey = function (key) {
        this.key = key;
        this.textBox.value = this.jsObject.BorderObjectToShotStr(this.jsObject.BordersStrToObject(key));
    }

    borderControl.action = function () { }

    return borderControl;
}