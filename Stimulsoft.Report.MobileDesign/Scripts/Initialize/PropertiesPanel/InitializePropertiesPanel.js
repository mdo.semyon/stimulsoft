﻿
StiMobileDesigner.prototype.InitializePropertiesPanel = function () {
    if (this.options.propertiesPanel) {
        this.options.mainPanel.removeChild(this.options.propertiesPanel);
        if (this.options.propertiesPanel.showButtonsPanel) {
            this.options.mainPanel.removeChild(this.options.propertiesPanel.showButtonsPanel);
        }
    }

    var propertiesPanel = document.createElement("div");
    propertiesPanel.id = this.options.mobileDesigner.id + "propertiesPanel";
    this.options.propertiesPanel = propertiesPanel;
    this.options.mainPanel.appendChild(propertiesPanel);
    propertiesPanel.jsObject = this;
    propertiesPanel.fixedViewMode = false;
    propertiesPanel.className = "stiDesignerPropertiesPanel";
    propertiesPanel.style.left = (this.options.toolbox ? this.options.toolbox.offsetWidth : 0) + "px";
    propertiesPanel.style.bottom = this.options.statusPanel.offsetHeight + "px";
    propertiesPanel.style.top = (this.options.toolBar.offsetHeight + this.options.workPanel.offsetHeight) + "px";
    propertiesPanel.style.width = this.options.propertiesGridWidth + "px";
    propertiesPanel.style.zIndex = 2;
    propertiesPanel.dictionaryMode = false;
    propertiesPanel.editFormControl = null;
    propertiesPanel.eventsMode = false;
    propertiesPanel.isEnabled = true;

    //Show Properties Panel Button
    propertiesPanel.showButtonsPanel = this.PropertiesPanelShowButtonsPanel(propertiesPanel);
    this.options.mainPanel.appendChild(propertiesPanel.showButtonsPanel);

    //Header
    propertiesPanel.header = this.PropertiesPanelHeader(propertiesPanel);
    propertiesPanel.appendChild(propertiesPanel.header);

    //Components List
    var componentsListPanel = document.createElement("div");
    componentsListPanel.className = "stiDesignerComponentsListPanel";

    var componentsTable = this.CreateHTMLTable();
    componentsListPanel.appendChild(componentsTable);

    var componentsList = this.DropDownList("componentsList", this.options.propertiesGridWidth - (this.options.isTouchDevice ? 52 : 45), null, null, true, null, 21, true);
    componentsTable.addCell(componentsList);

    var sortButton = this.StandartSmallButton("componentsListSort", null, null, "SortAZ.png", this.loc.Report.Alphabetical);
    sortButton.style.marginLeft = "5px";
    componentsTable.addCell(sortButton);

    sortButton.action = function () {
        this.setSelected(!this.isSelected);
    }

    componentsList.button.action = function () {
        if (!componentsList.menu.visible) {
            var componentsItems = this.jsObject.GetAllComponentsItems(sortButton.isSelected);
            componentsList.menu.addItems(componentsItems);
            componentsList.menu.changeVisibleState(true);
        }
        else
            componentsList.menu.changeVisibleState(false);
    }

    componentsList.action = function () {        
        if (this.key && this.jsObject.options.report) {

            if (this.key.endsWith(" : " + this.jsObject.loc.Components.StiReport)) {
                this.jsObject.options.report.setSelected();
            }
            else if (this.key.endsWith(" : " + this.jsObject.loc.Components.StiPage)) {
                var page = this.jsObject.options.report.pages[this.key.replace(" : " + this.jsObject.loc.Components.StiPage, "")];
                if (page) this.jsObject.options.paintPanel.showPage(page);
            }
            else {
                var component = this.jsObject.options.report.getComponentByName(this.key.substring(0, this.key.lastIndexOf(" : ")));
                if (component) {
                    if (component.properties.pageName &&
                        component.properties.pageName != this.jsObject.options.currentPage.properties.name &&
                        this.jsObject.options.report.pages[component.properties.pageName]) {
                        this.jsObject.options.paintPanel.showPage(this.jsObject.options.report.pages[component.properties.pageName]);
                    }
                    component.setSelected();
                }
            }

            this.jsObject.UpdatePropertiesControls();
        }
    }

    componentsListPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        propertiesPanel.containers["Properties"].style.top = state ? (propertiesPanel.jsObject.options.isTouchDevice ? "74px" : "69px") : "35px";
    }

    propertiesPanel.appendChild(componentsListPanel);

    propertiesPanel.setEventsMode = function (state) {
        this.propertiesToolBar.controls.PropertiesTab.setSelected(!state);
        this.propertiesToolBar.controls.EventsTab.setSelected(state);
        this.eventsMode = state;
        this.mainPropertiesPanel.style.display = state ? "none" : "";
        if (state)
            this.eventsPropertiesPanel.show();
        else
            this.eventsPropertiesPanel.hide();
    }

    //Container Properties 
    propertiesPanel.containers = {};
    propertiesPanel.containers["Properties"] = document.createElement("div");
    propertiesPanel.containers["Properties"].className = "stiDesignerPropertiesPanelInnerContent";
    propertiesPanel.containers["Properties"].style.top = this.options.isTouchDevice ? "74px" : "69px"; //"35px";
    propertiesPanel.appendChild(propertiesPanel.containers["Properties"]);
    propertiesPanel.containers["Properties"].style.display = "none";

    //Add Main Properties
    propertiesPanel.mainPropertiesPanel = document.createElement("div");
    propertiesPanel.containers["Properties"].appendChild(propertiesPanel.mainPropertiesPanel);
       
    propertiesPanel.eventsPropertiesPanel = this.EventsPropertiesPanel("div");
    propertiesPanel.containers["Properties"].appendChild(propertiesPanel.eventsPropertiesPanel);

    //Add Style Designer Properties
    propertiesPanel.styleDesignerPropertiesPanel = document.createElement("div");
    propertiesPanel.styleDesignerPropertiesPanel.style.display = "none";
    propertiesPanel.containers["Properties"].appendChild(propertiesPanel.styleDesignerPropertiesPanel);

    //Add Main Properties
    propertiesPanel.mainPropertiesPanel = document.createElement("div");
    propertiesPanel.containers["Properties"].appendChild(propertiesPanel.mainPropertiesPanel);

    //Container Dictionary    
    propertiesPanel.containers["Dictionary"] = this.DictionaryPanel();
    propertiesPanel.containers["Dictionary"].style.display = "none";
    propertiesPanel.appendChild(propertiesPanel.containers["Dictionary"]);

    //Container Report Tree    
    propertiesPanel.containers["ReportTree"] = this.ReportTreePanel();
    propertiesPanel.containers["ReportTree"].style.display = "none";
    propertiesPanel.appendChild(propertiesPanel.containers["ReportTree"]);

    //Footer
    propertiesPanel.footer = document.createElement("div");
    propertiesPanel.footer.className = "stiDesignerPropertiesPanelFooter";
    propertiesPanel.appendChild(propertiesPanel.footer);
    propertiesPanel.footer.appendChild(this.PropertiesPanelFooter());

    //Design Button
    var designButtonBlock = this.PropertyBlockWithButton("propertiesDesignButtonBlock", "StiText.png", this.loc.Buttons.Design + "...");
    designButtonBlock.style.display = "none";
    designButtonBlock.style.marginTop = "5px";
    propertiesPanel.mainPropertiesPanel.appendChild(designButtonBlock);

    designButtonBlock.button.action = function () {
        this.jsObject.ShowComponentForm(this.jsObject.options.selectedObject || this.jsObject.GetCommonObject(this.jsObject.options.selectedObjects));
    }

    //Disabled Panel
    var disabledPanel = document.createElement("div");
    disabledPanel.className = "stiDesignerDisabledPanelOfPropertiesPanel";
    disabledPanel.style.display = "none";
    propertiesPanel.appendChild(disabledPanel);

    propertiesPanel.getCurrentPanelName = function (state) {
        if (propertiesPanel.containers["Dictionary"] && propertiesPanel.containers["Dictionary"].style.display == "") return "Dictionary";
        else if (propertiesPanel.containers["Properties"] && propertiesPanel.containers["Properties"].style.display == "") return "Properties";
        return null;
    }

    propertiesPanel.setEnabled = function (state) {
        this.isEnabled = state;
        disabledPanel.style.display = !state ? "" : "none";
    }

    if (!this.options.showPanelPropertiesAndDictionary) {
        propertiesPanel.style.display = "none";
        propertiesPanel.showButtonsPanel.style.display = "none";
    }

    propertiesPanel.changeVisibleState = function (state) {
        if (propertiesPanel.jsObject.options.showPropertiesGrid) {
            propertiesPanel.style.display = state ? "" : "none";
        }
        propertiesPanel.hideButton.image.src = propertiesPanel.jsObject.options.images[propertiesPanel.fixedViewMode ? "HidePanelFixedMode.png" : "HidePanel.png"];
        propertiesPanel.className = propertiesPanel.fixedViewMode ? "stiDesignerPropertiesPanelFixedMode" : "stiDesignerPropertiesPanel";
        var marginVert = propertiesPanel.fixedViewMode ? 5 : 0;
        var marginLeft = propertiesPanel.fixedViewMode ? 30 : 0;
        propertiesPanel.style.bottom = (propertiesPanel.jsObject.options.statusPanel.offsetHeight + marginVert) + "px";
        propertiesPanel.style.top = (propertiesPanel.jsObject.options.toolBar.offsetHeight + propertiesPanel.jsObject.options.workPanel.offsetHeight + marginVert) + "px";
        propertiesPanel.style.left = ((propertiesPanel.jsObject.options.toolbox ? propertiesPanel.jsObject.options.toolbox.offsetWidth : 0) + marginLeft) + "px";
        if (!state) {
            if (propertiesPanel.jsObject.options.buttons["showPropertiesPanelButton"]) {
                propertiesPanel.jsObject.options.buttons["showPropertiesPanelButton"].setSelected(false);
            }
            if (propertiesPanel.jsObject.options.buttons["showDictionaryPanelButton"]) {
                propertiesPanel.jsObject.options.buttons["showDictionaryPanelButton"].setSelected(false);
            }
        }
        else {
            propertiesPanel.footer.style.display = !propertiesPanel.fixedViewMode ? "" : "none";
            var bottom = (propertiesPanel.fixedViewMode ? 0 : 30) + "px";
            propertiesPanel.containers["Properties"].style.bottom = bottom;
            propertiesPanel.containers["Dictionary"].style.bottom = bottom;
        }

        var toolbox = propertiesPanel.jsObject.options.toolbox;
        propertiesPanel.jsObject.options.paintPanel.style.left = ((propertiesPanel.fixedViewMode ? 0 : propertiesPanel.offsetWidth) + (toolbox ? toolbox.offsetWidth : 0)) + "px";
        if (propertiesPanel.jsObject.options.pagesPanel) {
            propertiesPanel.jsObject.options.pagesPanel.style.left = ((propertiesPanel.fixedViewMode ? 0 : propertiesPanel.offsetWidth) + (toolbox ? toolbox.offsetWidth : 0)) + "px";
            propertiesPanel.jsObject.options.pagesPanel.updateScrollButtons();
        }
    }

    propertiesPanel.setStyleDesignerMode = function (state) {
        if (state) {
            propertiesPanel.returnToPanel = propertiesPanel.getCurrentPanelName();
        }

        propertiesPanel.styleDesignerMode = state;
        propertiesPanel.showContainer("Properties");
        var buttons = propertiesPanel.jsObject.options.buttons;

        if (state && buttons.showPropertiesPanelButton) {
            buttons.showPropertiesPanelButton.setSelected(true);
        }

        propertiesPanel.mainPropertiesPanel.style.display = state ? "none" : "";
        propertiesPanel.styleDesignerPropertiesPanel.style.display = state ? "" : "none";
        componentsListPanel.changeVisibleState(!state);

        buttons.DictionaryTabButton.style.display = state ? "none" : "";
        if (buttons.showDictionaryPanelButton) {
            buttons.showDictionaryPanelButton.style.display = state ? "none" : "";
        }
        if (state) {
            if (propertiesPanel.editCrossTabMode) propertiesPanel.editCrossTabPropertiesPanel.style.display = "none";
        }
        else {
            if (propertiesPanel.editChartMode) { propertiesPanel.setEditChartMode(true); }
            else if (propertiesPanel.editGaugeMode) { propertiesPanel.setEditGaugeMode(true); }
            else if (propertiesPanel.editCrossTabMode) { propertiesPanel.setEditCrossTabMode(true); }
            else if (propertiesPanel.editBarCodeMode) { propertiesPanel.setEditBarCodeMode(true); }
            else if (propertiesPanel.returnToPanel) propertiesPanel.showContainer(propertiesPanel.returnToPanel);
        }
    }

    propertiesPanel.setEditMode = function (state, editForm) {
        var buttons = propertiesPanel.jsObject.options.buttons;
        if (state && buttons.showPropertiesPanelButton) {
            buttons.showPropertiesPanelButton.setSelected(true);
            propertiesPanel.setZIndex(true, editForm.level);
        }
        propertiesPanel.mainPropertiesPanel.style.display = state ? "none" : "";
        buttons.DictionaryTabButton.style.display = state ? "none" : "";
        if (buttons.showDictionaryPanelButton) {
            buttons.showDictionaryPanelButton.style.display = state ? "none" : "";
        }
        buttons.ReportTreeTabButton.style.display = state ? "none" : "";
        if (buttons.showReportTreePanelButton) {
            buttons.showReportTreePanelButton.style.display = state ? "none" : "";
        }
        if (!state && propertiesPanel.returnToPanel) {
            propertiesPanel.showContainer(propertiesPanel.returnToPanel);
        }
    }

    propertiesPanel.setEditChartMode = function (state) {
        if (state) { propertiesPanel.returnToPanel = propertiesPanel.getCurrentPanelName(); }
        propertiesPanel.editChartMode = state;
        propertiesPanel.showContainer("Properties");
        componentsListPanel.changeVisibleState(!state);
        propertiesPanel.jsObject.InitializeEditChartForm(function (editChartForm) {
            propertiesPanel.setEditMode(state, editChartForm);
            if (!propertiesPanel.editChartPropertiesPanel) {
                propertiesPanel.editChartPropertiesPanel = propertiesPanel.jsObject.ChartPropertiesPanel();
                propertiesPanel.containers["Properties"].appendChild(propertiesPanel.editChartPropertiesPanel);
            }
            propertiesPanel.editChartPropertiesPanel.style.display = state ? "" : "none";
        });
    }

    propertiesPanel.setEditCrossTabMode = function (state) {
        if (state) { propertiesPanel.returnToPanel = propertiesPanel.getCurrentPanelName(); }
        propertiesPanel.editCrossTabMode = state;
        propertiesPanel.showContainer("Properties");
        componentsListPanel.changeVisibleState(!state);
        propertiesPanel.jsObject.InitializeCrossTabForm(function (editCrossTabForm) {
            propertiesPanel.setEditMode(state, editCrossTabForm);
            if (!propertiesPanel.editCrossTabPropertiesPanel) {
                propertiesPanel.editCrossTabPropertiesPanel = propertiesPanel.jsObject.CrossTabPropertiesPanel();
                propertiesPanel.containers["Properties"].appendChild(propertiesPanel.editCrossTabPropertiesPanel);
            }
            propertiesPanel.editCrossTabPropertiesPanel.style.display = state ? "" : "none";
        });
    }

    propertiesPanel.setEditBarCodeMode = function (state) {
        if (state) { propertiesPanel.returnToPanel = propertiesPanel.getCurrentPanelName(); }
        propertiesPanel.editBarCodeMode = state;
        propertiesPanel.showContainer("Properties");
        componentsListPanel.changeVisibleState(!state);
        propertiesPanel.jsObject.InitializeBarCodeForm(function (barCodeForm) {
            propertiesPanel.setEditMode(state, barCodeForm);
            if (!propertiesPanel.editBarCodePropertiesPanel) {
                propertiesPanel.editBarCodePropertiesPanel = propertiesPanel.jsObject.BarCodePropertiesPanel();
                propertiesPanel.containers["Properties"].appendChild(propertiesPanel.editBarCodePropertiesPanel);
            }
            propertiesPanel.editBarCodePropertiesPanel.style.display = state ? "" : "none";
        });
    }

    propertiesPanel.setEditGaugeMode = function (state) {
        if (state) { propertiesPanel.returnToPanel = propertiesPanel.getCurrentPanelName(); }
        propertiesPanel.editGaugeMode = state;
        propertiesPanel.showContainer("Properties");
        componentsListPanel.changeVisibleState(!state);
        propertiesPanel.jsObject.InitializeEditGaugeForm(function (editGaugeForm) {
            propertiesPanel.setEditMode(state, editGaugeForm);
            if (!propertiesPanel.editChartPropertiesPanel) {
                propertiesPanel.editChartPropertiesPanel = propertiesPanel.jsObject.ChartPropertiesPanel();
                propertiesPanel.containers["Properties"].appendChild(propertiesPanel.editChartPropertiesPanel);
            }
            propertiesPanel.editChartPropertiesPanel.style.display = state ? "" : "none";
        });
    }

    propertiesPanel.setDictionaryMode = function (state) {
        if (state) {
            propertiesPanel.returnToPanel = propertiesPanel.getCurrentPanelName();
            propertiesPanel.returnToEventsMode = propertiesPanel.eventsMode;
        }
        propertiesPanel.setEnabled(!state);
        propertiesPanel.dictionaryMode = state;
        var buttons = propertiesPanel.jsObject.options.buttons;
        if (propertiesPanel.jsObject.options.showDictionary) {
            propertiesPanel.showContainer("Dictionary");
        }
        if (state && buttons.showDictionaryPanelButton) {
            buttons.showDictionaryPanelButton.setSelected(true);
        }
        buttons.PropertiesTabButton.style.display = state ? "none" : "";
        buttons.ReportTreeTabButton.style.display = state ? "none" : "";
        if (buttons.showPropertiesPanelButton) {
            buttons.showPropertiesPanelButton.style.display = state ? "none" : "";
        }
        if (buttons.showReportTreePanelButton) {
            buttons.showReportTreePanelButton.style.display = state ? "none" : "";
        }
        if (!state) {
            if (propertiesPanel.editChartMode) { propertiesPanel.setEditChartMode(true); }
            else if (propertiesPanel.editGaugeMode) { propertiesPanel.setEditGaugeMode(true); }
            else if (propertiesPanel.editCrossTabMode) { propertiesPanel.setEditCrossTabMode(true); }
            else if (propertiesPanel.editBarCodeMode) { propertiesPanel.setEditBarCodeMode(true); }
            else if (propertiesPanel.returnToPanel) propertiesPanel.showContainer(propertiesPanel.returnToPanel);
            if (propertiesPanel.returnToEventsMode) propertiesPanel.setEventsMode(true);
        }
    }

    propertiesPanel.showContainer = function (containerName) {
        for (var name in this.containers) {
            this.containers[name].style.display = name == containerName ? "" : "none";
            if (this.jsObject.options.buttons[containerName + "TabButton"]) this.jsObject.options.buttons[containerName + "TabButton"].setSelected(true);
            this.jsObject.options.propertiesPanel.headerCaption.innerHTML = this.jsObject.loc.Panels[containerName == "StyleDesignerProperties" ? "Properties" : containerName];
        }
        if (propertiesPanel.jsObject.options.buttons["show" + containerName + "PanelButton"] && propertiesPanel.fixedViewMode) {
            propertiesPanel.jsObject.options.buttons["show" + containerName + "PanelButton"].setSelected(true);
        }
        this.jsObject.SetCookie("StimulsoftMobileDesignerLastTabOnPropertiesPanel", containerName);
        if (containerName == "ReportTree" && this.jsObject.options.reportTree) {
            this.jsObject.options.reportTree.selectedItem = null;
            this.jsObject.options.reportTree.build();
        }

        propertiesPanel.propertiesToolBar.changeVisibleState(
            containerName == "Properties" &&
            !propertiesPanel.styleDesignerMode &&
            !propertiesPanel.editChartMode &&
            !propertiesPanel.editGaugeMode &&
            !propertiesPanel.editCrossTabMode &&
            !propertiesPanel.editBarCodeMode
        );

        propertiesPanel.dictionaryToolBar.changeVisibleState(containerName == "Dictionary");
    }

    propertiesPanel.setZIndex = function (state, level) {
        var zIndex = state ? (level * 10 + 1) : 2;
        propertiesPanel.style.zIndex = zIndex;
        propertiesPanel.showButtonsPanel.style.zIndex = zIndex;
    }

    propertiesPanel.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.ontouchstart(true);
    }

    propertiesPanel.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.propertiesPanelPressed = true;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    //Prepare places for groups
    var groupNames = ["Data", "Primitive", "Cell", "Hierarchical", "CrossTab", "Check", "ZipCode", "Container", "Shape", "BarCode", "BarCodeAdditional", "ImageAdditional", "Text",
        "TextAdditional", "Page", "PageAdditional", "Table", "HeaderTable", "FooterTable", "PageAndColumnBreak", "Columns", "Position", "Appearance", "Behavior",
        "Design", "ReportDescription", "ReportMain"];
    propertiesPanel.places = {};

    for (var i = 0; i < groupNames.length; i++) {
        var place = document.createElement("div");
        propertiesPanel.places[groupNames[i]] = place;
        propertiesPanel.mainPropertiesPanel.appendChild(place);
    }

    propertiesPanel.updateControls = function () {
        if (this.eventsMode) {
            this.eventsPropertiesPanel.update();
        }

        var controls = this.jsObject.options.controls;
        var buttons = this.jsObject.options.buttons;
        var properties = this.jsObject.options.properties;
        var propertiesGroups = this.jsObject.options.propertiesGroups;
        var report = this.jsObject.options.report;
        var currentObject = this.jsObject.options.selectedObject || this.jsObject.GetCommonObject(this.jsObject.options.selectedObjects);
        //if (!currentObject) return;
        var currProps = currentObject ? currentObject.properties : null;
        var styleObject = currentObject ? this.jsObject.getStyleObject(currProps.componentStyle) : null;
        designButtonBlock.style.display = "none";

        //Page Group
        var showPageGroup = (report && currentObject.typeComponent == "StiPage");
        if (showPageGroup && !propertiesGroups.pagePropertiesGroup) propertiesPanel.places["Page"].appendChild(this.jsObject.PagePropertiesGroup());
        if (propertiesGroups.pagePropertiesGroup) propertiesGroups.pagePropertiesGroup.style.display = showPageGroup ? "" : "none";
        if (showPageGroup) {
            controls.controlPropertyPageSize.setKey(currProps.paperSize);
            controls.controlPropertyPageWidth.value = this.jsObject.StrToDouble(currProps.unitWidth);
            controls.controlPropertyPageHeight.value = this.jsObject.StrToDouble(currProps.unitHeight);
            controls.controlPropertyPageOrientation.setKey(currProps.orientation);
            controls.controlPropertyPageMargins.setValue(currProps.unitMargins, true);
            controls.controlPropertyPageNumberOfCopies.value = currProps.numberOfCopies;
        }

        //Page Additional
        var showPageAdditionalGroup = (report && currentObject.typeComponent == "StiPage");
        if (showPageAdditionalGroup && !propertiesGroups.pageAdditionalPropertiesGroup) propertiesPanel.places["PageAdditional"].appendChild(this.jsObject.PageAdditionalPropertiesGroup());
        if (propertiesGroups.pageAdditionalPropertiesGroup) propertiesGroups.pageAdditionalPropertiesGroup.style.display = showPageAdditionalGroup ? "" : "none";
        if (showPageAdditionalGroup) {
            controls.controlPropertyStopBeforePrint.value = currProps.stopBeforePrint;
            controls.controlPropertyTitleBeforeHeader.setChecked(currProps.titleBeforeHeader);
            controls.controlPropertyMirrorMargins.setChecked(currProps.mirrorMargins);
            controls.controlPropertyPageUnlimitedHeight.setChecked(currProps.unlimitedHeight);
            controls.controlPropertyPageUnlimitedBreakable.setChecked(currProps.unlimitedBreakable);
            controls.controlPropertySegmentPerWidth.value = currProps.segmentPerWidth;
            controls.controlPropertySegmentPerHeight.value = currProps.segmentPerHeight;
        }

        //Columns Group
        var showColumns = report && currProps["columns"] != null;
        if (showColumns && !propertiesGroups.columnsPropertiesGroup) propertiesPanel.places["Columns"].appendChild(this.jsObject.ColumnsPropertiesGroup());
        if (propertiesGroups.columnsPropertiesGroup) propertiesGroups.columnsPropertiesGroup.style.display = showColumns ? "" : "none";
        if (showColumns) {
            properties.columns.style.display = currProps["columns"] != null ? "" : "none";
            if (currProps["columns"] != null) controls.controlPropertyColumns.value = currProps.columns != "StiEmptyValue" ? currProps.columns : "";
            properties.columnWidth.style.display = currProps["columnWidth"] != null ? "" : "none";
            if (currProps["columnWidth"] != null) controls.controlPropertyColumnWidth.value = currProps.columnWidth != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.columnWidth) : "";
            properties.columnGaps.style.display = currProps["columnGaps"] != null ? "" : "none";
            if (currProps["columnGaps"] != null) controls.controlPropertyColumnGaps.value = currProps.columnGaps != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.columnGaps) : "";
            properties.rightToLeft.style.display = currProps["rightToLeft"] != null ? "" : "none";
            if (currProps["rightToLeft"] != null) controls.controlPropertyRightToLeft.setChecked(currProps.rightToLeft);
            properties.columnDirection.style.display = currProps["columnDirection"] != null ? "" : "none";
            if (currProps["columnDirection"] != null) controls.controlPropertyColumnDirection.setKey(currProps.columnDirection != "StiEmptyValue" ? currProps.columnDirection : "");
            properties.minRowsInColumn.style.display = currProps["minRowsInColumn"] != null ? "" : "none";
            if (currProps["minRowsInColumn"] != null) controls.controlPropertyMinRowsInColumn.value = currProps.minRowsInColumn != "StiEmptyValue" ? currProps.minRowsInColumn : "";
        }

        //Appearance Group 
        var showAppearance = report && (currProps["brush"] != null || currProps["border"] != null || currProps["componentStyle"] != null);
        if (showAppearance && !propertiesGroups.appearancePropertiesGroup) propertiesPanel.places["Appearance"].appendChild(this.jsObject.AppearancePropertiesGroup());
        if (propertiesGroups.appearancePropertiesGroup) propertiesGroups.appearancePropertiesGroup.style.display = showAppearance ? "" : "none";
        if (showAppearance) {
            properties.brush.style.display = currProps["brush"] != null && !styleObject.allowUseBrush ? "" : "none";
            if (currProps["brush"] != null) controls.controlPropertyBrush.setKey(currProps.brush);
            properties.border.style.display = currProps["border"] != null && !styleObject.allowUseBorderFormatting && !styleObject.allowUseBorderSides ? "" : "none";
            if (currProps["border"] != null) {
                controls.controlPropertyBorder.value = this.jsObject.BorderObjectToShotStr(this.jsObject.BordersStrToObject(currProps.border));
            }
            properties.conditions.style.display = currProps["conditions"] != null ? "" : "none";
            if (currProps["conditions"] != null) {
                var conditionsText = "[" + (currProps.conditions != "" && currProps.conditions != "StiEmptyValue"
                    ? this.jsObject.loc.PropertyMain.Conditions : this.jsObject.loc.FormConditions.NoConditions) + "]";
                controls.controlPropertyConditions.value = conditionsText;
            }
            properties.componentStyle.style.display = currProps["componentStyle"] != null ? "" : "none";
            if (currProps["componentStyle"] != null) {
                controls.controlPropertyComponentStyle.menu.addItems(this.jsObject.GetComponentStyleItems());
                controls.controlPropertyComponentStyle.setKey(currProps.componentStyle);
            }
            properties.oddStyle.style.display = currProps["oddStyle"] != null ? "" : "none";
            if (currProps["oddStyle"] != null) {
                controls.controlPropertyOddStyle.menu.addItems(this.jsObject.GetComponentStyleItems());
                controls.controlPropertyOddStyle.setKey(currProps.oddStyle);
            }
            properties.evenStyle.style.display = currProps["evenStyle"] != null ? "" : "none";
            if (currProps["evenStyle"] != null) {
                controls.controlPropertyEvenStyle.menu.addItems(this.jsObject.GetComponentStyleItems());
                controls.controlPropertyEvenStyle.setKey(currProps.evenStyle);
            }
            properties.useParentStyles.style.display = currProps["useParentStyles"] != null ? "" : "none";
            if (currProps["useParentStyles"] != null) controls.controlPropertyUseParentStyles.setChecked(currProps.useParentStyles);
        }

        //Behavior Group
        var showBehavior = report && currProps["enabled"] != null;
        if (showBehavior && !propertiesGroups.behaviorPropertiesGroup) propertiesPanel.places["Behavior"].appendChild(this.jsObject.BehaviorPropertiesGroup());
        if (propertiesGroups.behaviorPropertiesGroup) propertiesGroups.behaviorPropertiesGroup.style.display = showBehavior ? "" : "none";
        if (showBehavior) {
            controls.propertiesInteractionButtonBlock.style.display = currProps["interaction"] != null ? "" : "none";
            properties.anchor.style.display = currProps["anchor"] != null ? "" : "none";
            if (currProps["anchor"] != null) controls.controlPropertyAnchor.setKey(currProps.anchor);
            properties.autoWidth.style.display = currProps["autoWidth"] != null ? "" : "none";
            if (currProps["autoWidth"] != null) controls.controlPropertyAutoWidth.setChecked(currProps.autoWidth);
            properties.calcInvisible.style.display = currProps["calcInvisible"] != null ? "" : "none";
            if (currProps["calcInvisible"] != null) controls.controlPropertyCalcInvisible.setChecked(currProps.calcInvisible);
            properties.canGrow.style.display = currProps["canGrow"] != null ? "" : "none";
            if (currProps["canGrow"] != null) controls.controlPropertyCanGrow.setChecked(currProps.canGrow);
            properties.canShrink.style.display = currProps["canShrink"] != null ? "" : "none";
            if (currProps["canShrink"] != null) controls.controlPropertyCanShrink.setChecked(currProps.canShrink);
            properties.canBreak.style.display = currProps["canBreak"] != null ? "" : "none";
            if (currProps["canBreak"] != null) controls.controlPropertyCanBreak.setChecked(currProps.canBreak);
            properties.growToHeight.style.display = currProps["growToHeight"] != null ? "" : "none";
            if (currProps["growToHeight"] != null) controls.controlPropertyGrowToHeight.setChecked(currProps.growToHeight);
            properties.dockStyle.style.display = currProps["dockStyle"] != null ? "" : "none";
            if (currProps["dockStyle"] != null) controls.controlPropertyDockStyle.setKey(currProps.dockStyle);
            properties.enabled.style.display = currProps["enabled"] != null ? "" : "none";
            if (currProps["enabled"] != null) controls.controlPropertyEnabled.setChecked(currProps.enabled);
            properties.printOn.style.display = currProps["printOn"] != null ? "" : "none";
            if (currProps["printOn"] != null) controls.controlPropertyPrintOn.setKey(currProps.printOn);
            properties.printable.style.display = currProps["printable"] != null ? "" : "none";
            if (currProps["printable"] != null) controls.controlPropertyPrintable.setChecked(currProps.printable);
            properties.printIfEmpty.style.display = (currProps["printIfEmpty"] != null && currentObject.typeComponent != "StiCrossTab" && currentObject.typeComponent) ? "" : "none";
            if (currProps["printIfEmpty"] != null) controls.controlPropertyPrintIfEmpty.setChecked(currProps.printIfEmpty);
            properties.printOnAllPages.style.display = currProps["printOnAllPages"] != null ? "" : "none";
            if (currProps["printOnAllPages"] != null) controls.controlPropertyPrintOnAllPages.setChecked(currProps.printOnAllPages);
            properties.resetPageNumber.style.display = currProps["resetPageNumber"] != null ? "" : "none";
            if (currProps["resetPageNumber"] != null) controls.controlPropertyResetPageNumber.setChecked(currProps.resetPageNumber);
            properties.printOnPreviousPage.style.display = currProps["printOnPreviousPage"] != null ? "" : "none";
            if (currProps["printOnPreviousPage"] != null) controls.controlPropertyPrintOnPreviousPage.setChecked(currProps.printOnPreviousPage);
            properties.printHeadersFootersFromPreviousPage.style.display = currProps["printHeadersFootersFromPreviousPage"] != null ? "" : "none";
            if (currProps["printHeadersFootersFromPreviousPage"] != null) controls.controlPropertyPrintHeadersFootersFromPreviousPage.setChecked(currProps.printHeadersFootersFromPreviousPage);
            properties.printAtBottom.style.display = currProps["printAtBottom"] != null ? "" : "none";
            if (currProps["printAtBottom"] != null) controls.controlPropertyPrintAtBottom.setChecked(currProps.printAtBottom);
            properties.printIfDetailEmpty.style.display = currProps["printIfDetailEmpty"] != null ? "" : "none";
            if (currProps["printIfDetailEmpty"] != null) controls.controlPropertyPrintIfDetailEmpty.setChecked(currProps.printIfDetailEmpty);
            properties.keepGroupTogether.style.display = currProps["keepGroupTogether"] != null ? "" : "none";
            if (currProps["keepGroupTogether"] != null) controls.controlPropertyKeepGroupTogether.setChecked(currProps.keepGroupTogether);
            properties.keepGroupHeaderTogether.style.display = currProps["keepGroupHeaderTogether"] != null ? "" : "none";
            if (currProps["keepGroupHeaderTogether"] != null) controls.controlPropertyKeepGroupHeaderTogether.setChecked(currProps.keepGroupHeaderTogether);
            properties.keepGroupFooterTogether.style.display = currProps["keepGroupFooterTogether"] != null ? "" : "none";
            if (currProps["keepGroupFooterTogether"] != null) controls.controlPropertyKeepGroupFooterTogether.setChecked(currProps.keepGroupFooterTogether);
            properties.keepHeaderTogether.style.display = currProps["keepHeaderTogether"] != null ? "" : "none";
            if (currProps["keepHeaderTogether"] != null) controls.controlPropertyKeepHeaderTogether.setChecked(currProps.keepHeaderTogether);
            properties.keepFooterTogether.style.display = currProps["keepFooterTogether"] != null ? "" : "none";
            if (currProps["keepFooterTogether"] != null) controls.controlPropertyKeepFooterTogether.setChecked(currProps.keepFooterTogether);
            properties.keepDetailsTogether.style.display = currProps["keepDetailsTogether"] != null ? "" : "none";
            if (currProps["keepDetailsTogether"] != null) controls.controlPropertyKeepDetailsTogether.setChecked(currProps.keepDetailsTogether);
            properties.shiftMode.style.display = currProps["shiftMode"] != null ? "" : "none";
            if (currProps["shiftMode"] != null) controls.controlPropertyShiftMode.setKey(currProps.shiftMode);
        }

        //Position Group
        var showPosition = (report && currentObject.typeComponent != "StiPage" && currentObject.typeComponent != "StiReport" &&
            currentObject.typeComponent != "StiTable" && !this.jsObject.IsTableCell(currentObject));
        if (this.jsObject.options.selectedObjects) {
            for (var i = 0; i < this.jsObject.options.selectedObjects.length; i++)
                if (this.jsObject.IsTableCell(this.jsObject.options.selectedObjects[i])) showPosition = false;
        }
        if (showPosition && !propertiesGroups.positionPropertiesGroup) propertiesPanel.places["Position"].appendChild(this.jsObject.PositionPropertiesGroup());
        if (propertiesGroups.positionPropertiesGroup) propertiesGroups.positionPropertiesGroup.style.display = showPosition ? "" : "none";
        if (showPosition) {
            var positionArray = this.jsObject.options.selectedObjects
                ? this.jsObject.GetCommonPositionsArray(this.jsObject.options.selectedObjects)
                : (ComponentCollection[currentObject.typeComponent] ? ComponentCollection[currentObject.typeComponent][5].split(",") : ["0", "0", "0", "0"]);
            properties.left.style.display = positionArray[0] == "1" ? "" : "none";
            var leftValue = currProps.clientLeft || currProps.unitLeft;
            if (leftValue == "StiEmptyValue") leftValue = "";
            controls.controlPropertyLeft.value = leftValue;
            properties.top.style.display = positionArray[1] == "1" ? "" : "none";
            var topValue = currProps.clientTop || currProps.unitTop;
            if (topValue == "StiEmptyValue") topValue = "";
            controls.controlPropertyTop.value = topValue;
            properties.width.style.display = positionArray[2] == "1" ? "" : "none";
            controls.controlPropertyWidth.value = currProps.unitWidth != "StiEmptyValue" ? currProps.unitWidth : "";
            properties.height.style.display = positionArray[3] == "1" ? "" : "none";
            controls.controlPropertyHeight.value = currProps.unitHeight != "StiEmptyValue" ? currProps.unitHeight : "";
            properties.minSize.style.display = currProps["minSize"] != null ? "" : "none";
            if (currProps["minSize"] != null) controls.controlPropertyMinSize.setValue(currProps["minSize"] == "StiEmptyValue" ? "" : currProps.minSize);
            properties.maxSize.style.display = currProps["maxSize"] != null ? "" : "none";
            if (currProps["maxSize"] != null) controls.controlPropertyMaxSize.setValue(currProps["maxSize"] == "StiEmptyValue" ? "" : currProps.maxSize);
            properties.minHeight.style.display = currProps["minHeight"] != null ? "" : "none";
            if (currProps["minHeight"] != null) controls.controlPropertyMinHeight.value = currProps["minHeight"] == "StiEmptyValue" ? "" : currProps.minHeight;
            properties.maxHeight.style.display = currProps["maxHeight"] != null ? "" : "none";
            if (currProps["maxHeight"] != null) controls.controlPropertyMaxHeight.value = currProps["maxHeight"] == "StiEmptyValue" ? "" : currProps.maxHeight;
            properties.minWidth.style.display = currProps["minWidth"] != null ? "" : "none";
            if (currProps["minWidth"] != null) controls.controlPropertyMinWidth.value = currProps["minWidth"] == "StiEmptyValue" ? "" : currProps.minWidth;
            properties.maxWidth.style.display = currProps["maxWidth"] != null ? "" : "none";
            if (currProps["maxWidth"] != null) controls.controlPropertyMaxWidth.value = currProps["maxWidth"] == "StiEmptyValue" ? "" : currProps.maxWidth;
        }

        //Page And Column Break Group
        var showPageAndColumnBreak = report && (currProps["newPageBefore"] != null || currProps["newPageAfter"] != null ||
            currProps["newColumnBefore"] != null || currProps["newColumnAfter"] != null || currProps["skipFirst"] != null);
        if (showPageAndColumnBreak && !propertiesGroups.pageAndColumnBreakPropertiesGroup) propertiesPanel.places["PageAndColumnBreak"].appendChild(this.jsObject.PageAndColumnBreakPropertiesGroup());
        if (propertiesGroups.pageAndColumnBreakPropertiesGroup) propertiesGroups.pageAndColumnBreakPropertiesGroup.style.display = showPageAndColumnBreak ? "" : "none";
        if (showPageAndColumnBreak) {
            properties.newPageBefore.style.display = currProps["newPageBefore"] != null ? "" : "none";
            if (currProps["newPageBefore"] != null) controls.controlPropertyNewPageBefore.setChecked(currProps.newPageBefore);
            properties.newPageAfter.style.display = currProps["newPageAfter"] != null ? "" : "none";
            if (currProps["newPageAfter"] != null) controls.controlPropertyNewPageAfter.setChecked(currProps.newPageAfter);
            properties.newColumnBefore.style.display = currProps["newColumnBefore"] != null ? "" : "none";
            if (currProps["newColumnBefore"] != null) controls.controlPropertyNewColumnBefore.setChecked(currProps.newColumnBefore);
            properties.newColumnAfter.style.display = currProps["newColumnAfter"] != null ? "" : "none";
            if (currProps["newColumnAfter"] != null) controls.controlPropertyNewColumnAfter.setChecked(currProps.newColumnAfter);
            properties.skipFirst.style.display = currProps["skipFirst"] != null ? "" : "none";
            if (currProps["skipFirst"] != null) controls.controlPropertySkipFirst.setChecked(currProps.skipFirst);
            properties.breakIfLessThan.style.display = currProps["breakIfLessThan"] != null ? "" : "none";
            if (currProps["breakIfLessThan"] != null) controls.controlPropertyBreakIfLessThan.value = currProps.breakIfLessThan;
        }

        //CellGroup
        var showCellGroup = report && this.jsObject.IsTableCell(currentObject);
        if (showCellGroup && !propertiesGroups.cellPropertiesGroup) propertiesPanel.places["Cell"].appendChild(this.jsObject.CellPropertiesGroup());
        if (propertiesGroups.cellPropertiesGroup) propertiesGroups.cellPropertiesGroup.style.display = showCellGroup ? "" : "none";
        if (showCellGroup) {
            properties.cellType.style.display = currProps["cellType"] != null ? "" : "none";
            if (currProps["cellType"] != null) controls.controlPropertyCellType.setKey(currProps.cellType);
            properties.cellDockStyle.style.display = currProps["cellDockStyle"] != null ? "" : "none";
            if (currProps["cellDockStyle"] != null) controls.controlPropertyCellDockStyle.setKey(currProps.cellDockStyle);
            properties.fixedWidth.style.display = currProps["fixedWidth"] != null ? "" : "none";
            if (currProps["fixedWidth"] != null) controls.controlPropertyFixedWidth.setChecked(currProps.fixedWidth);
        }

        //Rich Text
        var showRichText = report && currentObject.typeComponent && (currentObject.typeComponent == "StiRichText" || currentObject.typeComponent == "StiTableCellRichText");
        if (showRichText) designButtonBlock.style.display = "";

        //Text Group
        var showText = report && currentObject.typeComponent && (currentObject.typeComponent == "StiText" || currentObject.typeComponent == "StiTextInCells" ||
            currentObject.typeComponent == "StiTableCell");

        if (showText && !propertiesGroups.textPropertiesGroup) propertiesPanel.places["Text"].appendChild(this.jsObject.TextPropertiesGroup());
        if (propertiesGroups.textPropertiesGroup) propertiesGroups.textPropertiesGroup.style.display = showText ? "" : "none";
        if (showText) {
            designButtonBlock.style.display = "";
            properties.text.style.display = currProps["text"] != null ? "" : "none";
            if (currProps["text"] != null) controls.controlPropertyText.value = currProps.text != "StiEmptyValue" ? Base64.decode(currProps.text) : "";
            properties.textBrush.style.display = currProps["textBrush"] != null && !styleObject.allowUseTextBrush ? "" : "none";
            if (currProps["textBrush"] != null) controls.controlPropertyTextBrush.setKey(currProps.textBrush);
            properties.font.style.display = currProps["font"] != null && !styleObject.allowUseFont ? "" : "none";
            if (currProps["font"] != null) {
                var font = currProps.font.split("!");
                controls.controlPropertyfontName.setKey(font[0]);
                controls.controlPropertyfontSize.setKey(font[1]);
                buttons.controlPropertyfontBold.setSelected(font[2] == "1");
                buttons.controlPropertyfontItalic.setSelected(font[3] == "1");
                buttons.controlPropertyfontUnderline.setSelected(font[4] == "1");
                buttons.controlPropertyfontStrikeout.setSelected(font[5] == "1");
            }
            properties.textHorizontalAlignment.style.display = currProps["horAlignment"] != null && !styleObject.allowUseHorAlignment ? "" : "none";
            if (currProps["horAlignment"] != null) controls.controlPropertyTextHorizontalAlignment.setKey(currProps.horAlignment);
            properties.textVerticalAlignment.style.display = currProps["vertAlignment"] != null && !styleObject.allowUseVertAlignment ? "" : "none";
            if (currProps["vertAlignment"] != null) controls.controlPropertyTextVerticalAlignment.setKey(currProps.vertAlignment);
            properties.textFormat.style.display = currProps["textFormat"] != null ? "" : "none";
            if (currProps["textFormat"] != null) controls.controlPropertyTextFormat.value = this.jsObject.GetTextFormatLocalizedName(currProps.textFormat.type);
            properties.cellWidth.style.display = currProps["cellWidth"] != null ? "" : "none";
            if (currProps["cellWidth"] != null) controls.controlPropertyCellWidth.value = currProps.cellWidth != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.cellWidth) : "";
            properties.cellHeight.style.display = currProps["cellHeight"] != null ? "" : "none";
            if (currProps["cellHeight"] != null) controls.controlPropertyCellHeight.value = currProps.cellHeight != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.cellHeight) : "";
            properties.horizontalSpacing.style.display = currProps["horizontalSpacing"] != null ? "" : "none";
            if (currProps["horizontalSpacing"] != null)
                controls.controlPropertyHorizontalSpacing.value = currProps.horizontalSpacing != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.horizontalSpacing) : "";
            properties.verticalSpacing.style.display = currProps["verticalSpacing"] != null ? "" : "none";
            if (currProps["verticalSpacing"] != null)
                controls.controlPropertyVerticalSpacing.value = currProps.verticalSpacing != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.verticalSpacing) : "";
        }

        //Text Additional Group
        var showAdditionalText = report && (showText || currentObject.typeComponent == "StiRichText" || currentObject.typeComponent == "StiTableCellRichText");
        if (showAdditionalText && !propertiesGroups.textAdditionalPropertiesGroup)
            propertiesPanel.places["TextAdditional"].appendChild(this.jsObject.TextAdditionalPropertiesGroup());
        if (propertiesGroups.textAdditionalPropertiesGroup) propertiesGroups.textAdditionalPropertiesGroup.style.display = showAdditionalText ? "" : "none";

        if (showAdditionalText) {
            properties.textAngle.style.display = currProps["textAngle"] != null ? "" : "none";
            if (currProps["textAngle"] != null) controls.controlPropertyTextAngle.value = currProps.textAngle != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.textAngle) : "";
            properties.textMargins.style.display = currProps["textMargins"] != null ? "" : "none";
            if (currProps["textMargins"] != null) controls.controlPropertyTextMargins.setValue(currProps["textMargins"] == "StiEmptyValue" ? "" : currProps.textMargins);
            properties.wordWrap.style.display = currProps["wordWrap"] != null ? "" : "none";
            if (currProps["wordWrap"] != null) controls.controlPropertyWordWrap.setChecked(currProps.wordWrap);
            properties.editableText.style.display = currProps["editableText"] != null ? "" : "none";
            if (currProps["editableText"] != null) controls.controlPropertyEditableText.setChecked(currProps.editableText);
            properties.hideZeros.style.display = currProps["hideZeros"] != null ? "" : "none";
            if (currProps["hideZeros"] != null) controls.controlPropertyHideZeros.setChecked(currProps.hideZeros);
            properties.onlyText.style.display = currProps["onlyText"] != null ? "" : "none";
            if (currProps["onlyText"] != null) controls.controlPropertyOnlyText.setChecked(currProps.onlyText);
            properties.continuousText.style.display = currProps["continuousText"] != null ? "" : "none";
            if (currProps["continuousText"] != null) controls.controlPropertyContinuousText.setChecked(currProps.continuousText);
            properties.maxNumberOfLines.style.display = currProps["maxNumberOfLines"] != null ? "" : "none";
            if (currProps["maxNumberOfLines"] != null) controls.controlPropertyMaxNumberOfLines.value = currProps.maxNumberOfLines != "StiEmptyValue" ? currProps.maxNumberOfLines : "";
            properties.allowHtmlTags.style.display = currProps["allowHtmlTags"] != null ? "" : "none";
            if (currProps["allowHtmlTags"] != null) controls.controlPropertyAllowHtmlTags.setChecked(currProps.allowHtmlTags);
            properties.rightToLeftText.style.display = currProps["rightToLeft"] != null ? "" : "none";
            if (currProps["rightToLeft"] != null) controls.controlPropertyRightToLeftText.setChecked(currProps.rightToLeft);
            properties.trimming.style.display = currProps["trimming"] != null ? "" : "none";
            if (currProps["trimming"] != null) controls.controlPropertyTrimming.setKey(currProps.trimming);
            properties.textOptionsRightToLeft.style.display = currProps["textOptionsRightToLeft"] != null ? "" : "none";
            if (currProps["textOptionsRightToLeft"] != null) controls.controlPropertyTextOptionsRightToLeft.setChecked(currProps.textOptionsRightToLeft);
            properties.processAt.style.display = currProps["processAt"] != null ? "" : "none";
            if (currProps["processAt"] != null) controls.controlPropertyProcessAt.setKey(currProps.processAt);
            properties.processingDuplicates.style.display = currProps["processingDuplicates"] != null ? "" : "none";
            if (currProps["processingDuplicates"] != null) controls.controlPropertyProcessingDuplicates.setKey(currProps.processingDuplicates);
            properties.shrinkFontToFit.style.display = currProps["shrinkFontToFit"] != null ? "" : "none";
            if (currProps["shrinkFontToFit"] != null) controls.controlPropertyShrinkFontToFit.setChecked(currProps.shrinkFontToFit);
            properties.shrinkFontToFitMinimumSize.style.display = currProps["shrinkFontToFitMinimumSize"] != null ? "" : "none";
            if (currProps["shrinkFontToFitMinimumSize"] != null)
                controls.controlPropertyShrinkFontToFitMinimumSize.value = currProps.shrinkFontToFitMinimumSize != "StiEmptyValue" ? currProps.shrinkFontToFitMinimumSize : "";
        }

        //Image Group
        var showImage = report && currentObject.typeComponent && (currentObject.typeComponent == "StiImage" || currentObject.typeComponent == "StiTableCellImage");
        if (showImage) designButtonBlock.style.display = "";

        //Image Additional Group
        if (showImage && !propertiesGroups.imageAdditionalPropertiesGroup) propertiesPanel.places["ImageAdditional"].appendChild(this.jsObject.ImageAdditionalPropertiesGroup());
        if (propertiesGroups.imageAdditionalPropertiesGroup) propertiesGroups.imageAdditionalPropertiesGroup.style.display = showImage ? "" : "none";
        if (showImage) {
            controls.controlPropertyImageHorizontalAlignment.setKey(currProps.horAlignment);
            controls.controlPropertyImageVerticalAlignment.setKey(currProps.vertAlignment);
            controls.controlPropertyImageAspectRatio.setChecked(currProps.ratio);
            controls.controlPropertyImageStretch.setChecked(currProps.stretch);
            controls.controlPropertyImageRotation.setKey(currProps.rotation);
            controls.controlPropertyImageMultipleFactor.value = currProps.imageMultipleFactor != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.imageMultipleFactor) : "";
        }

        //BarCode Group
        var showBarCode = report && currentObject.typeComponent == "StiBarCode";
        if (showBarCode && !propertiesGroups.barCodePropertiesGroup) propertiesPanel.places["BarCode"].appendChild(this.jsObject.BarCodePropertiesGroup());
        if (propertiesGroups.barCodePropertiesGroup) propertiesGroups.barCodePropertiesGroup.style.display = showBarCode ? "" : "none";

        if (showBarCode) {
            designButtonBlock.style.display = "";
            controls.controlPropertyBarCode.value = currProps.code != "StiEmptyValue" ? Base64.decode(currProps.code) : "";
            properties.barCode.style.display = currProps.codeType != "SSCC" ? "" : "none";
            controls.controlPropertyBarCodeHorizontalAlignment.setKey(currProps.horAlignment);
            controls.controlPropertyBarCodeVerticalAlignment.setKey(currProps.vertAlignment);
            controls.controlPropertyBarCodeType.setKey(currProps.codeType);
        }

        //BarCode Additional Group
        if (showBarCode && !propertiesGroups.barCodeAdditionalPropertiesGroup) propertiesPanel.places["BarCodeAdditional"].appendChild(this.jsObject.BarCodeAdditionalPropertiesGroup());
        if (propertiesGroups.barCodeAdditionalPropertiesGroup) propertiesGroups.barCodeAdditionalPropertiesGroup.style.display = showBarCode ? "" : "none";
        if (showBarCode) {
            controls.controlPropertyBarCodeAngle.setKey(currProps.barCodeAngle);
            controls.controlPropertyBarCodeAutoScale.setChecked(currProps.autoScale);
            var font = currProps.font.split("!");
            controls.controlPropertyBarCodeFontName.setKey(font[0]);
            controls.controlPropertyBarCodeFontSize.setKey(font[1]);
            buttons.controlPropertyBarCodeFontBold.setSelected(font[2] == "1");
            buttons.controlPropertyBarCodeFontItalic.setSelected(font[3] == "1");
            buttons.controlPropertyBarCodeFontUnderline.setSelected(font[4] == "1");
            buttons.controlPropertyBarCodeFontStrikeout.setSelected(font[5] == "1");
            controls.controlPropertyBarCodeShowLabelText.setChecked(currProps.showLabelText);
            controls.controlPropertyBarCodeShowQuietZones.setChecked(currProps.showQuietZones);
            controls.controlPropertyBarCodeForeColor.setKey(currProps.foreColor);
            controls.controlPropertyBarCodeBackColor.setKey(currProps.backColor);
        }

        //Shape Group
        var showShape = report && currentObject.typeComponent == "StiShape";
        if (showShape && !propertiesGroups.shapePropertiesGroup) propertiesPanel.places["Shape"].appendChild(this.jsObject.ShapePropertiesGroup());
        if (propertiesGroups.shapePropertiesGroup) propertiesGroups.shapePropertiesGroup.style.display = showShape ? "" : "none";
        if (showShape) {
            controls.controlPropertyShapeType.setKey(currProps.shapeType);
            if (currProps.shapeBorderStyle == "StiEmptyValue") controls.controlPropertyShapeBorderStyle.setKey("6");
            controls.controlPropertyShapeBorderStyle.setKey(currProps.shapeBorderStyle);
            controls.controlPropertyShapeBorderSize.value = currProps.size != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.size) : "";
            controls.controlPropertyShapeBorderColor.setKey(currProps.shapeBorderColor);
        }

        //Primitive Group
        var showPrimitive = report && currProps.isPrimitiveComponent;
        if (showPrimitive && !propertiesGroups.primitivePropertiesGroup) propertiesPanel.places["Primitive"].appendChild(this.jsObject.PrimitivePropertiesGroup());
        if (propertiesGroups.primitivePropertiesGroup) propertiesGroups.primitivePropertiesGroup.style.display = showPrimitive ? "" : "none";
        if (showPrimitive) {
            controls.controlPropertyPrimitiveColor.setKey(currProps.color);
            controls.controlPropertyPrimitiveStyle.setKey(currProps.style != "StiEmptyValue" ? currProps.style : "6");
            controls.controlPropertyPrimitiveSize.value = currProps.size != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.size) : "";
            properties.primitiveRound.style.display = currProps["round"] != null && currProps.round != "StiEmptyValue" ? "" : "none";
            if (currProps["round"] != null) controls.controlPropertyPrimitiveRound.value = currProps.round;

            properties.primitiveLeftSide.style.display = currProps["leftSide"] != null && currProps.leftSide != "StiEmptyValue" ? "" : "none";
            if (currProps["leftSide"] != null) controls.controlPropertyPrimitiveLeftSide.setChecked(currProps.leftSide);
            properties.primitiveRightSide.style.display = currProps["rightSide"] != null && currProps.rightSide != "StiEmptyValue" ? "" : "none";
            if (currProps["rightSide"] != null) controls.controlPropertyPrimitiveRightSide.setChecked(currProps.rightSide);
            properties.primitiveTopSide.style.display = currProps["topSide"] != null && currProps.topSide != "StiEmptyValue" ? "" : "none";
            if (currProps["topSide"] != null) controls.controlPropertyPrimitiveTopSide.setChecked(currProps.topSide);
            properties.primitiveBottomSide.style.display = currProps["bottomSide"] != null && currProps.bottomSide != "StiEmptyValue" ? "" : "none";
            if (currProps["bottomSide"] != null) controls.controlPropertyPrimitiveBottomSide.setChecked(currProps.bottomSide);

            propertiesGroups.startCapPropertiesGroup.style.display = currProps["startCapColor"] != null ? "" : "none";
            if (currProps["startCapColor"] != null) controls.controlPropertyStartCapColor.setKey(currProps.startCapColor);
            if (currProps["startCapFill"] != null) controls.controlPropertyStartCapFill.setChecked(currProps.startCapFill);
            if (currProps["startCapWidth"] != null) controls.controlPropertyStartCapWidth.value = currProps.startCapWidth;
            if (currProps["startCapHeight"] != null) controls.controlPropertyStartCapHeight.value = currProps.startCapHeight;
            if (currProps["startCapStyle"] != null) controls.controlPropertyStartCapStyle.setKey(currProps.startCapStyle);

            propertiesGroups.endCapPropertiesGroup.style.display = currProps["endCapColor"] != null ? "" : "none";
            if (currProps["endCapColor"] != null) controls.controlPropertyEndCapColor.setKey(currProps.endCapColor);
            if (currProps["endCapFill"] != null) controls.controlPropertyEndCapFill.setChecked(currProps.endCapFill);
            if (currProps["endCapWidth"] != null) controls.controlPropertyEndCapWidth.value = currProps.endCapWidth;
            if (currProps["endCapHeight"] != null) controls.controlPropertyEndCapHeight.value = currProps.endCapHeight;
            if (currProps["endCapStyle"] != null) controls.controlPropertyEndCapStyle.setKey(currProps.endCapStyle);
        }

        //Container Group
        var showContainer = report && currentObject.typeComponent == "StiClone";
        if (showContainer && !propertiesGroups.containerPropertiesGroup) propertiesPanel.places["Container"].appendChild(this.jsObject.ContainerPropertiesGroup());
        if (propertiesGroups.containerPropertiesGroup) propertiesGroups.containerPropertiesGroup.style.display = showContainer ? "" : "none";
        if (showContainer) {            
            if (currProps["container"] != null) {
                var containerText = currProps.container != "[Not Assigned]"
                    ? (currProps.container == "StiEmptyValue" ? "" : currProps.container)
                    : "[" + this.jsObject.loc.Report.NotAssigned + "]";
                controls.controlPropertyCloneContainer.value = containerText;
            }
        }

        //ZipCode Group
        var showZipCode = report && currentObject.typeComponent == "StiZipCode";
        if (showZipCode && !propertiesGroups.zipCodePropertiesGroup) propertiesPanel.places["ZipCode"].appendChild(this.jsObject.ZipCodePropertiesGroup());
        if (propertiesGroups.zipCodePropertiesGroup) propertiesGroups.zipCodePropertiesGroup.style.display = showZipCode ? "" : "none";
        if (showZipCode) {
            designButtonBlock.style.display = "";
            controls.controlPropertyZipCode.value = currProps.code != "StiEmptyValue" ? Base64.decode(currProps.code) : "";
            controls.controlPropertyZipCodeSize.value = currProps.size != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.size) : "";
            controls.controlPropertyZipCodeForeColor.setKey(currProps.foreColor);
            controls.controlPropertyZipCodeRatio.setChecked(currProps.ratio);
        }

        //Check Group
        var showCheck = report && (currentObject.typeComponent == "StiCheckBox" || currentObject.typeComponent == "StiTableCellCheckBox");
        if (showCheck && !propertiesGroups.checkPropertiesGroup) propertiesPanel.places["Check"].appendChild(this.jsObject.CheckPropertiesGroup());
        if (propertiesGroups.checkPropertiesGroup) propertiesGroups.checkPropertiesGroup.style.display = showCheck ? "" : "none";
        if (showCheck) {
            controls.controlPropertyChecked.value = currProps.checked != "StiEmptyValue" ? Base64.decode(currProps.checked) : "";
            if (currProps.checkStyleForTrue == "StiEmptyValue") controls.controlPropertyCheckStyleForTrue.setKey("None");
            if (currProps.checkStyleForFalse == "StiEmptyValue") controls.controlPropertyCheckStyleForFalse.setKey("None");
            controls.controlPropertyCheckStyleForTrue.setKey(currProps.checkStyleForTrue);
            controls.controlPropertyCheckStyleForFalse.setKey(currProps.checkStyleForFalse);
            controls.controlPropertyCheckValues.setKey(currProps.checkValues);
            controls.controlPropertyCheckSize.value = currProps.size != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.size) : "";
            controls.controlPropertyCheckContourColor.setKey(currProps.contourColor);
            controls.controlPropertyCheckTextBrush.setKey(currProps.textBrush);
            controls.controlPropertyCheckEditable.setChecked(currProps.editable);
        }

        //CrossTab Group
        var showCrossTab = report && currentObject.typeComponent == "StiCrossTab";
        if (showCrossTab && !propertiesGroups.crossTabPropertiesGroup) propertiesPanel.places["CrossTab"].appendChild(this.jsObject.CrossTabPropertiesGroup());
        if (propertiesGroups.crossTabPropertiesGroup) propertiesGroups.crossTabPropertiesGroup.style.display = showCrossTab ? "" : "none";
        if (showCrossTab) {
            controls.controlPropertyCrossTabEmptyValue.value = currProps.crossTabEmptyValue != "StiEmptyValue" ? Base64.decode(currProps.crossTabEmptyValue) : "";
            controls.controlPropertyCrossTabHorAlign.setKey(currProps.crossTabHorAlign);
            controls.controlPropertyCrossTabPrintIfEmpty.setChecked(currProps.printIfEmpty);
            controls.controlPropertyCrossTabRightToLeft.setChecked(currProps.rightToLeft);
            controls.controlPropertyCrossTabWrap.setChecked(currProps.crossTabWrap);
            properties.crossTabWrapGap.style.display = currProps.crossTabWrap ? "" : "none";
            controls.controlPropertyCrossTabWrapGap.value = currProps.crossTabWrapGap != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.crossTabWrapGap) : "";
        }

        //Hierarchical Group
        var showHierarchical = report && currentObject.typeComponent == "StiHierarchicalBand";
        if (showHierarchical && !propertiesGroups.hierarchicalPropertiesGroup) propertiesPanel.places["Hierarchical"].appendChild(this.jsObject.HierarchicalPropertiesGroup());
        if (propertiesGroups.hierarchicalPropertiesGroup) propertiesGroups.hierarchicalPropertiesGroup.style.display = showHierarchical ? "" : "none";
        if (showHierarchical) {
            controls.controlPropertyKeyDataColumn.value = currProps.keyDataColumn != "StiEmptyValue"
                ? (currProps.keyDataColumn ? currProps.keyDataColumn : this.jsObject.loc.Report.NotAssigned)
                : "";
            controls.controlPropertyMasterKeyDataColumn.value = currProps.masterKeyDataColumn != "StiEmptyValue"
                ? (currProps.masterKeyDataColumn ? currProps.masterKeyDataColumn : this.jsObject.loc.Report.NotAssigned)
                : "";
            controls.controlPropertyParentValue.value = currProps.parentValue != "StiEmptyValue" ? Base64.decode(currProps.parentValue) : "";
            controls.controlPropertyIndent.value = currProps.indent != "StiEmptyValue" ? this.jsObject.StrToDouble(currProps.indent) : "";
            controls.controlPropertyHeaders.value = currProps.headers != "StiEmptyValue" ? Base64.decode(currProps.headers) : "";
            controls.controlPropertyFooters.value = currProps.footers != "StiEmptyValue" ? Base64.decode(currProps.footers) : "";
        }

        //Design Group
        var showDesignGroup = report && currentObject.typeComponent != "StiReport";
        if (showDesignGroup && !propertiesGroups.designPropertiesGroup) propertiesPanel.places["Design"].appendChild(this.jsObject.DesignPropertiesGroup());
        if (propertiesGroups.designPropertiesGroup) propertiesGroups.designPropertiesGroup.style.display = showDesignGroup ? "" : "none";

        if (showDesignGroup) {
            properties.componentName.style.display = currProps["name"] != null && currProps.name != "StiEmptyValue" ? "" : "none";
            if (currProps["name"] != null) controls.controlPropertyComponentName.value = currProps.name;
            properties.aliasName.style.display = currProps["aliasName"] != null ? "" : "none";
            if (currProps["aliasName"] != null) controls.controlPropertyAlias.value = currProps.aliasName != "StiEmptyValue" ? Base64.decode(currProps.aliasName) : "";
            properties.largeHeight.style.display = currProps["largeHeight"] != null ? "" : "none";
            if (currProps["largeHeight"] != null) controls.controlPropertyLargeHeight.setChecked(currProps.largeHeight);
            properties.largeHeightFactor.style.display = currProps["largeHeightFactor"] != null ? "" : "none";
            if (currProps["largeHeightFactor"] != null) controls.controlPropertyLargeHeightFactor.value = currProps.largeHeightFactor != "StiEmptyValue" ? currProps.largeHeightFactor : "";
            properties.restrictions.style.display = currProps["restrictions"] != null && this.jsObject.options.modifyRestrictions && !this.jsObject.IsTableCell(currentObject) ? "" : "none";
            if (currProps["restrictions"] != null) controls.controlPropertyRestrictions.setKey(currProps.restrictions);
            properties.locked.style.display = currProps["locked"] != null ? "" : "none";
            if (currProps["locked"] != null) controls.controlPropertyLocked.setChecked(currProps.locked);
            properties.linked.style.display = currProps["linked"] != null ? "" : "none";
            if (currProps["linked"] != null) controls.controlPropertyLinked.setChecked(currProps.linked);
        }

        //Data Group
        var showDataGroup = report && currentObject.typeComponent && currentObject.typeComponent == "StiOverlayBand";
        if (showDataGroup && !propertiesGroups.dataPropertiesGroup) propertiesPanel.places["Data"].appendChild(this.jsObject.DataPropertiesGroup());
        if (propertiesGroups.dataPropertiesGroup) propertiesGroups.dataPropertiesGroup.style.display = showDataGroup ? "" : "none";
        if (showDataGroup) {
            properties.dataVerticalAlignment.style.display = currProps["vertAlignment"] != null ? "" : "none";
            if (currProps["vertAlignment"] != null) controls.controlPropertyDataVerticalAlignment.setKey(currProps.vertAlignment);
        }

        //Design Button
        var showCondition = report && currentObject.typeComponent && (currentObject.typeComponent == "StiGroupHeaderBand" || currentObject.typeComponent == "StiCrossGroupHeaderBand");
        var showData = report && currProps["dataSource"] != null && currentObject.typeComponent && currentObject.typeComponent != "StiChart" && currentObject.typeComponent != "StiCrossTab";
        if (showCondition || showData) designButtonBlock.style.display = "";

        //TableGroup
        var showTableGroup = report && currentObject.typeComponent == "StiTable";
        if (showTableGroup) {
            if (!propertiesGroups.tablePropertiesGroup) propertiesPanel.places["Table"].appendChild(this.jsObject.TablePropertiesGroup());
            if (!propertiesGroups.headerTablePropertiesGroup) propertiesPanel.places["HeaderTable"].appendChild(this.jsObject.HeaderOrFooterTablePropertiesGroup("header"));
            if (!propertiesGroups.footerTablePropertiesGroup) propertiesPanel.places["FooterTable"].appendChild(this.jsObject.HeaderOrFooterTablePropertiesGroup("footer"));
        }
        if (propertiesGroups.tablePropertiesGroup) propertiesGroups.tablePropertiesGroup.style.display = showTableGroup ? "" : "none";
        if (propertiesGroups.headerTablePropertiesGroup) propertiesGroups.headerTablePropertiesGroup.style.display = showTableGroup ? "" : "none";
        if (propertiesGroups.footerTablePropertiesGroup) propertiesGroups.footerTablePropertiesGroup.style.display = showTableGroup ? "" : "none";

        if (showTableGroup) {
            var tableProperties = ["tableAutoWidth", "autoWidthType", "columnCount", "rowCount", "headerRowsCount", "footerRowsCount", "tableRightToLeft", "dockableTable", "headerPrintOn",
                "headerCanGrow", "headerCanShrink", "headerCanBreak", "headerPrintAtBottom", "headerPrintIfEmpty", "headerPrintOnAllPages", "headerPrintOnEvenOddPages",
                "footerPrintOn", "footerCanGrow", "footerCanShrink", "footerCanBreak", "footerPrintAtBottom", "footerPrintIfEmpty", "footerPrintOnAllPages", "footerPrintOnEvenOddPages"];

            for (var i = 0; i < tableProperties.length; i++) {
                var upperPropertyName = this.jsObject.UpperFirstChar(tableProperties[i]);
                if (properties[tableProperties[i]]) properties[tableProperties[i]].style.display = currProps[tableProperties[i]] != null ? "" : "none";
                if (currProps[tableProperties[i]] != null && controls["controlProperty" + upperPropertyName])
                    this.jsObject.SetControlValue(controls["controlProperty" + upperPropertyName], currProps[tableProperties[i]]);
            }
        }

        //Chart 
        var showChart = report && currentObject.typeComponent && currentObject.typeComponent == "StiChart" && this.jsObject.options.selectedObject;
        if (showChart) designButtonBlock.style.display = "";

        //CrossTab 
        var showCrossTab = report && currentObject.typeComponent && currentObject.typeComponent == "StiCrossTab" && this.jsObject.options.selectedObject;
        if (showCrossTab) designButtonBlock.style.display = "";

        //SubReport
        var showSubReport = report && currentObject.typeComponent && currentObject.typeComponent == "StiSubReport";
        if (showSubReport) designButtonBlock.style.display = "";

        //Report
        var showReportGroup = report && currentObject.typeComponent == "StiReport";
        if (showReportGroup && !propertiesGroups.reportDescriptionPropertiesGroup)
            propertiesPanel.places["ReportDescription"].appendChild(this.jsObject.ReportDescriptionPropertiesGroup());
        if (showReportGroup && !propertiesGroups.reportMainPropertiesGroup)
            propertiesPanel.places["ReportMain"].appendChild(this.jsObject.ReportMainPropertiesGroup());

        if (propertiesGroups.reportDescriptionPropertiesGroup) propertiesGroups.reportDescriptionPropertiesGroup.style.display = showReportGroup && !this.jsObject.options.cloudMode ? "" : "none";
        if (propertiesGroups.reportMainPropertiesGroup) propertiesGroups.reportMainPropertiesGroup.style.display = showReportGroup ? "" : "none";
        if (showReportGroup) {
            propertiesGroups.reportDescriptionPropertiesGroup.changeOpenedState(true);
            propertiesGroups.reportMainPropertiesGroup.changeOpenedState(true);
            controls["controlReportPropertyReportUnit"].setKey(currProps["reportUnit"]);

            var propertyNames = ["ReportName", "ReportAlias", "ReportAuthor", "ReportDescription", "AutoLocalizeReportOnRun", "CacheAllData", "CacheTotals",
                "CalculationMode", "ConvertNulls", "Collate", "Culture", "EngineVersion", "NumberOfPass", "PreviewMode", "ReportCacheMode",
                "ParametersOrientation", "RequestParameters", "ScriptLanguage", "StopBeforePage", "StoreImagesInResources"];
            for (var i = 0; i < propertyNames.length; i++) {
                var controlProperty = controls["controlReportProperty" + propertyNames[i]];
                var propertyValue = currProps[this.jsObject.LowerFirstChar(propertyNames[i])];
                if (controlProperty) { controlProperty.setValue(propertyValue); }
            }
        }

        //Set Design Button Image        
        if (report && designButtonBlock.style.display == "") {
            if (currentObject.typeComponent && this.jsObject.options.images[currentObject.typeComponent + ".png"]) {
                designButtonBlock.image.style.display = "";
                designButtonBlock.image.src = this.jsObject.options.images[currentObject.typeComponent + ".png"];
            }
            else {
                designButtonBlock.image.style.display = "none";
            }
        }

        //Components List
        var compListValue = "";
        if (this.jsObject.options.selectedObject) {
            if (this.jsObject.options.selectedObject.typeComponent == "StiReport") {
                compListValue = Base64.decode(this.jsObject.options.report.properties.reportName.replace("Base64Code;", "")) + " : " + this.jsObject.loc.Components.StiReport;
            }
            else {
                compListValue = this.jsObject.options.selectedObject.properties.name + " : " + this.jsObject.loc.Components[this.jsObject.options.selectedObject.typeComponent];
            }
        }
        componentsList.setKey(compListValue);

        propertiesPanel.openFirstGroup();
    }

    propertiesPanel.updatePropertiesCaptions = function () {
        for (var propertyName in this.jsObject.options.properties) {
            var property = this.jsObject.options.properties[propertyName];
            property.caption.innerHTML = this.localizePropertyGrid ? property.captionText : property.getOriginalPropertyName();
        }
        if (this.editChartPropertiesPanel) this.editChartPropertiesPanel.updatePropertiesCaptions();
        if (this.eventsPropertiesPanel) this.eventsPropertiesPanel.updatePropertiesCaptions();
        if (this.jsObject.options.forms.styleDesignerForm) this.jsObject.options.forms.styleDesignerForm.propertiesPanel.updatePropertiesCaptions();
    }

    propertiesPanel.openFirstGroup = function () {
        for (var i = 0; i < propertiesPanel.mainPropertiesPanel.childNodes.length; i++) {
            var propertiesGroup = propertiesPanel.mainPropertiesPanel.childNodes[i];
            if (propertiesGroup.firstChild && propertiesGroup.firstChild.style.display == "" && propertiesGroup.firstChild["changeOpenedState"] != null) {
                propertiesGroup.firstChild.changeOpenedState(true);
                break;
            }
        }
    }

    var lastContainerName = this.GetCookie("StimulsoftMobileDesignerLastTabOnPropertiesPanel");

    if (this.options.showPropertiesGrid && (lastContainerName == "Properties" || !lastContainerName || (!this.options.showDictionary && !this.options.showReportTree)))
        propertiesPanel.showContainer("Properties");
    else if (this.options.showDictionary && (lastContainerName == "Dictionary" || !lastContainerName || (!this.options.showPropertiesGrid && !this.options.showReportTree)))
        propertiesPanel.showContainer("Dictionary")
    else if (this.options.showReportTree && (lastContainerName == "ReportTree" || !lastContainerName || (!this.options.showPropertiesGrid && !this.options.showDictionary)))
        propertiesPanel.showContainer("ReportTree");
}

StiMobileDesigner.prototype.PropertiesPanelHeader = function (propertiesPanel) {
    var header = document.createElement("div");
    header.className = "stiDesignerPropertiesPanelHeader";

    var headerTable = this.CreateHTMLTable();
    header.appendChild(headerTable);
    headerTable.style.height = "100%";
    headerTable.style.width = "100%";

    propertiesPanel.headerCaption = headerTable.addCell();
    propertiesPanel.headerCaption.style.width = "100%";
    propertiesPanel.headerCaption.style.paddingLeft = this.options.isTouchDevice ? "10px" : "8px";
    propertiesPanel.headerCaption.innerHTML = this.loc.Panels.Properties;

    //Properties Toolbar
    var toolBarProps = this.CreateHTMLTable();
    toolBarProps.style.height = "100%";
    propertiesPanel.propertiesToolBar = toolBarProps;
    toolBarProps.controls = {};
    headerTable.addCell(toolBarProps);

    var buttons = [
        ["PropertiesTab", this.loc.Report.PropertiesTab, "PropertiesTab.png"],
        ["EventsTab", this.loc.Report.EventsTab, "EventsTab.png"],
        ["Settings", this.loc.Export.Settings, "Settings.png"]
    ]

    for (var i in buttons) {
        var button = this.StandartSmallButton("propertiesToolbar" + buttons[i][0], null, null, buttons[i][2], buttons[i][1]);
        toolBarProps.controls[buttons[i][0]] = button;
        button.style.marginRight = this.options.isTouchDevice ? "3px" : "5px";
        toolBarProps.addCell(button);
    }

    if (this.options.isJava) {
        toolBarProps.controls.EventsTab.style.display = 'none';
    }

    toolBarProps.controls.PropertiesTab.setSelected(true);
    toolBarProps.controls.PropertiesTab.action = function () { propertiesPanel.setEventsMode(false); }
    toolBarProps.controls.EventsTab.action = function () { propertiesPanel.setEventsMode(true); }

    var localizeState = this.GetCookie("StimulsoftMobileDesignerLocalizePropertyGrid");
    propertiesPanel.localizePropertyGrid = localizeState ? localizeState == "true" : true;

    var propertiesSettingsMenu = this.InitializePropertiesSettingsMenu();

    toolBarProps.controls.Settings.action = function () {
        propertiesSettingsMenu.changeVisibleState(!propertiesSettingsMenu.visible);
    }

    toolBarProps.changeVisibleState = function (state) {
        toolBarProps.style.display = state ? "" : "none";
        if (!state) propertiesPanel.setEventsMode(false);
    }

    //Dictionary Toolbar
    var toolBarDict = this.CreateHTMLTable();
    toolBarDict.style.height = "100%";
    propertiesPanel.dictionaryToolBar = toolBarDict;
    toolBarDict.controls = {};
    headerTable.addCell(toolBarDict);

    var buttons = [
        ["Settings", this.loc.Export.Settings, "Settings.png"]
    ]

    for (var i in buttons) {
        var button = this.StandartSmallButton("dictionaryToolbar" + buttons[i][0], null, null, buttons[i][2], buttons[i][1]);
        toolBarDict.controls[buttons[i][0]] = button;
        button.style.marginRight = this.options.isTouchDevice ? "3px" : "5px";
        toolBarDict.addCell(button);
    }

    var dictionarySettingsMenu = this.InitializeDictionarySettingsMenu();

    toolBarDict.controls.Settings.action = function () {
        dictionarySettingsMenu.changeVisibleState(!dictionarySettingsMenu.visible);
    }

    toolBarDict.changeVisibleState = function (state) {
        toolBarDict.style.display = state ? "" : "none";
    }

    //Hide button
    propertiesPanel.hideButton = this.StandartSmallButton("hidePropertiesPanelButton", null, null, "HidePanel.png");
    propertiesPanel.hideButton.style.marginRight = this.options.isTouchDevice ? "3px" : "5px";
    headerTable.addCell(propertiesPanel.hideButton);

    propertiesPanel.hideButton.action = function () {
        propertiesPanel.fixedViewMode = !propertiesPanel.fixedViewMode;
        propertiesPanel.changeVisibleState(!propertiesPanel.fixedViewMode);
        propertiesPanel.showButtonsPanel.changeVisibleState(propertiesPanel.fixedViewMode);
    }

    return header;
}

StiMobileDesigner.prototype.PropertiesPanelFooter = function () {
    var footerTable = this.CreateHTMLTable();
    footerTable.style.height = "100%";

    var buttonProps = [
        ["PropertiesTabButton", this.loc.Panels.Properties, "Properties.png", this.options.showPropertiesGrid],
        ["DictionaryTabButton", this.loc.Panels.Dictionary, "Dictionary.png", this.options.showDictionary],
        ["ReportTreeTabButton", this.loc.Panels.ReportTree, "ReportTree.png", this.options.showReportTree]
    ]
    var buttons = {};

    for (var i in buttonProps) {
        buttons[buttonProps[i][0]] = this.PropertiesPanelFooterTabButton(buttonProps[i][0], buttonProps[i][1], null);
        var tubButtonCell = footerTable.addCell(buttons[buttonProps[i][0]]);
        buttons[buttonProps[i][0]].style.margin = "0px 2px 2px 2px";
        tubButtonCell.style.verticalAlign = "top";
        tubButtonCell.style.display = buttonProps[i][3] ? "" : "none";
    }

    buttons["PropertiesTabButton"].action = function () { this.jsObject.options.propertiesPanel.showContainer("Properties"); }
    buttons["DictionaryTabButton"].action = function () { this.jsObject.options.propertiesPanel.showContainer("Dictionary"); }
    buttons["ReportTreeTabButton"].action = function () { this.jsObject.options.propertiesPanel.showContainer("ReportTree"); }

    return footerTable;
}

StiMobileDesigner.prototype.PropertiesPanelShowButtonsPanel = function (propertiesPanel) {
    var buttonsPanel = document.createElement("div");
    buttonsPanel.className = "stiDesignerPropertiesPanelShowButtonsPanel";
    buttonsPanel.style.display = "none";
    buttonsPanel.style.zIndex = 2;
    buttonsPanel.style.top = (this.options.toolBar.offsetHeight + this.options.workPanel.offsetHeight + 3) + "px";
    buttonsPanel.style.left = (this.options.toolbox ? this.options.toolbox.offsetWidth : 0) + "px";
    var jsObject = this;

    buttonsPanel.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.ontouchstart(true);
    }

    buttonsPanel.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        jsObject.options.propertiesPanelPressed = true;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    var buttonProps = [
        ["Properties", this.options.showPropertiesGrid],
        ["Dictionary", this.options.showDictionary],
        ["ReportTree", this.options.showReportTree]
    ]

    for (var i in buttonProps) {
        if (buttonProps[i][1]) {
            var button = this.SmallButton("show" + buttonProps[i][0] + "PanelButton", "PropertiesPanelGroup", null, null, null, null, this.GetStyles("VerticalButton"), true);
            button.innerHTML = this.loc.Panels[buttonProps[i][0]];
            button.panelName = buttonProps[i][0];
            buttonsPanel.appendChild(button);

            if ((buttonProps[i][0] == "Dictionary" && this.options.showPropertiesGrid) ||
                (buttonProps[i][0] == "ReportTree" && (this.options.showPropertiesGrid || this.options.showDictionary))) {
                button.style.marginTop = "70px";
            }

            button.action = function () {
                var show = !this.isSelected;
                propertiesPanel.changeVisibleState(show);
                if (show) {
                    propertiesPanel.showContainer(this.panelName);
                    this.setSelected(true);
                }
            }
        }
    }

    buttonsPanel.changeVisibleState = function (state) {
        buttonsPanel.style.display = state ? "" : "none";
        if (state && buttonsPanel.style.zIndex != propertiesPanel.upperZIndex) {
            buttonsPanel.style.zIndex = propertiesPanel.fixedViewMode ? "2" : null;
        }
    }

    return buttonsPanel;
}


StiMobileDesigner.prototype.PropertiesPanelFooterTabButton = function (name, caption, imageName) {
    var button = this.SmallButton(name, "PropertiesGridTabs", caption, imageName, null, null, this.GetStyles("PropertiesPanelFooterTabButton"), true);
    if (button.caption) {
        button.caption.style.padding = "0 10px 0 10px";
        button.caption.style.textAlign = "center";
    }
    button.style.minWidth = "70px";
    button.innerTable.style.width = "100%";

    //Override
    button.onmouseenter = function () {
        if (!this.isEnabled || this.isSelected || this.jsObject.options.isTouchClick) return;
        this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = true;
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object")
            this.jsObject.options.toolTip.showWithDelay(
                this.toolTip[0],
                this.toolTip[1],
                this.toolTip.length == 3 ? this.toolTip[2].left : this.jsObject.FindPosX(this, "stiDesignerMainPanel"),
                this.toolTip.length == 3 ? this.toolTip[2].top : this.jsObject.options.toolBar.offsetHeight + this.jsObject.options.workPanel.offsetHeight - 1
            );
    }

    return button
}

StiMobileDesigner.prototype.AddMainMethodsToPropertyControl = function (control) {
    if (!control) return;

    control.getValue = function () {
        var type = this.controlType;
        if (type == "DropdownList" || (!type && this["setKey"])) { return this.key; }
        else if (type == "Checkbox" || (!type && this["setChecked"])) { return this.isChecked; }
        else if (type == "Textbox" || (!type && this["value"] != null)) { return "Base64Code;" + Base64.encode(this.value); }

        return null;
    }

    control.setValue = function (value) {
        if (typeof (value) == "string" && value.indexOf("Base64Code;") == 0) {
            value = value.replace("Base64Code;", "");
            value = Base64.decode(value);
        }

        var type = this.controlType;
        if (type == "DropdownList" || (!type && this["setKey"])) { this.setKey(value); }
        else if (type == "Checkbox" || (!type && this["setChecked"])) { this.setChecked(value); }
        else if (type == "Textbox" || (!type && this["value"] != null)) { this.value = value; }
    }
}