﻿
StiMobileDesigner.prototype.PropertyConditionsControl = function (name, width) {
    var conditionsControl = this.PropertyTextBoxWithEditButton(name, width, true);
    conditionsControl.key = null;

    conditionsControl.button.action = function () {
        this.jsObject.InitializeConditionsForm(function (conditionsForm) {

            conditionsForm.action = function () {
                conditionsForm.changeVisibleState(false);
                conditionsForm.controls.itemsContainer.onPreChangeSelection();
                var conditions = [];
                for (var i = 0; i < conditionsForm.controls.itemsContainer.childNodes.length; i++) {
                    conditions.push(conditionsForm.controls.itemsContainer.childNodes[i].itemObject);
                }
                conditionsControl.setKey(conditions.length > 0 ? Base64.encode(JSON.stringify(conditions)) : "");
                conditionsControl.action();
            }

            conditionsForm.show(conditionsControl.key);
        });
    }

    conditionsControl.setKey = function (key) {
        this.key = key;
        this.textBox.value = "[" + (key == "" ? this.jsObject.loc.FormConditions.NoConditions : this.jsObject.loc.PropertyMain.Conditions) + "]";
    }

    conditionsControl.action = function () { }

    return conditionsControl;
}