﻿
StiMobileDesigner.prototype.ZipCodePropertiesGroup = function () {
    var zipCodePropertiesGroup = this.PropertiesGroup("zipCodePropertiesGroup", this.loc.PropertyCategory.ZipCodeCategory);
    zipCodePropertiesGroup.style.margin = "5px 0 5px 0";
    zipCodePropertiesGroup.style.display = "none";

    //Code
    var controlPropertyCode = this.PropertyTextBoxWithEditButton("controlPropertyZipCode", this.options.propertyControlWidth);
    controlPropertyCode.textBox.action = function () {
        this.jsObject.ApplyPropertyValue("code", Base64.encode(this.value));
    }
    controlPropertyCode.button.action = function () {
        this.jsObject.InitializeTextEditorForm(function (textEditorForm) {
            textEditorForm.propertyName = "code";
            textEditorForm.changeVisibleState(true);
        });
    }
    zipCodePropertiesGroup.container.appendChild(this.Property("zipCode", this.loc.PropertyMain.Code, controlPropertyCode, "Code"));

    //Size
    var controlPropertyZipCodeSize = this.PropertyTextBox("controlPropertyZipCodeSize", this.options.propertyNumbersControlWidth);
    controlPropertyZipCodeSize.action = function () {
        this.value = Math.abs(this.jsObject.StrToDouble(this.value));
        this.jsObject.ApplyPropertyValue("size", this.value);
    }
    zipCodePropertiesGroup.container.appendChild(this.Property("zipCodeSize", this.loc.PropertyMain.Size, controlPropertyZipCodeSize, "Size"));

    //Fore Color
    var controlPropertyZipCodeForeColor = this.PropertyColorControl("controlPropertyZipCodeForeColor", null, this.options.propertyControlWidth);
    controlPropertyZipCodeForeColor.action = function () {
        this.jsObject.ApplyPropertyValue("foreColor", this.key);
    }
    zipCodePropertiesGroup.container.appendChild(this.Property("zipCodeForeColor", this.loc.PropertyMain.ForeColor, controlPropertyZipCodeForeColor, "ForeColor"));

    //Ratio
    var controlPropertyZipCodeRatioControl = this.CheckBox("controlPropertyZipCodeRatio");
    controlPropertyZipCodeRatioControl.action = function () {
        this.jsObject.ApplyPropertyValue("ratio", this.isChecked);
    }
    zipCodePropertiesGroup.container.appendChild(this.Property("zipCodeRatio", this.loc.PropertyMain.Ratio, controlPropertyZipCodeRatioControl, "Ratio"));

    return zipCodePropertiesGroup;
}