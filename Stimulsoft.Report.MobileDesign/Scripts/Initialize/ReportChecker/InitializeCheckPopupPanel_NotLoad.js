﻿
StiMobileDesigner.prototype.InitializeCheckPopupPanel_ = function (checkItems) {
    var panel = document.createElement("div");
    panel.jsObject = this;

    //Add Arrow
    //var arrow = document.createElement("div");
    //arrow.className = "stiDesignerCheckPopupPanelArrow";
    //panel.appendChild(arrow);

    if (this.options.checkPopupPanel) {
        this.options.checkPopupPanel.hide();
    }

    this.options.checkPopupPanel = panel;
    this.options.mainPanel.appendChild(panel);
    panel.className = "stiDesignerCheckPopupPanel";
    panel.jsObject = this;

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px";
    panel.appendChild(innerTable);

    var errorCount = 0;
    var warningCount = 0;
    var informationCount = 0;
    var reportRenderingMessageCount = 0;

    for (var i = 0; i < checkItems.length; i++) {
        switch (checkItems[i].status) {
            case "Error": errorCount++; break;
            case "Warning": warningCount++; break;
            case "Information": informationCount++; break;
            case "ReportRenderingMessage": reportRenderingMessageCount++; break;
        }
    }

    var addRow = function (imageName, text) {
        var img = document.createElement("img");
        img.src = panel.jsObject.options.images["ReportChecker." + imageName];
        img.style.margin = "5px 10px 5px 5px";
        innerTable.addCellInNextRow(img);
        innerTable.addTextCellInLastRow(text).style.paddingRight = "20px";
    }

    if (errorCount > 0) addRow("Error.png", this.loc.Report.InfoMessage.replace("{0}", errorCount).replace("{1}", this.loc.Report.Errors));
    if (warningCount > 0) addRow("Warning.png", this.loc.Report.InfoMessage.replace("{0}", warningCount).replace("{1}", this.loc.Report.Warnings));
    if (informationCount > 0) addRow("Information.png", this.loc.Report.InfoMessage.replace("{0}", informationCount).replace("{1}", this.loc.Report.InformationMessages));
    if (reportRenderingMessageCount > 0) addRow("ReportRenderingMessage.png", this.loc.Report.InfoMessage.replace("{0}", reportRenderingMessageCount).replace("{1}", this.loc.Report.ReportRenderingMessages));
    
    var moreDetails = document.createElement("div");
    panel.appendChild(moreDetails);
    moreDetails.style.padding = "25px 30px 30px 30px";
    moreDetails.style.textAlign = "center";
    moreDetails.style.fontSize = "11px";
    moreDetails.innerHTML = this.loc.Report.ClickForMoreDetails;

    panel.hide = function () {
        this.jsObject.options.mainPanel.removeChild(this);
        this.jsObject.options.checkPopupPanel = null;
        clearTimeout(this.hideTimer);
    }

    panel.onmousedown = function () {
        if (!this.isTouchStartFlag) this.ontouchstart(true);
    }

    panel.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        panel.pressed();
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }
    
    panel.pressed = function () {
        this.hide();
        this.jsObject.options.checkPanelPressed = true;

        this.jsObject.InitializeCheckPanel(function (checkPanel) {
            checkPanel.show(checkItems);
            checkPanel.style.left = "10px";
            checkPanel.style.bottom = "45px";
        });
    }

    panel.style.left = "10px";
    panel.style.bottom = "47px";
    //arrow.style.top = (panel.offsetHeight - 2) + "px";

    //Show with animation
    var d = new Date();
    var endTime = d.getTime() + 300;
    panel.style.opacity = 0;
    this.ShowAnimationForm(panel, endTime);

    //Hide after 15 sec.
    panel.hideTimer = setTimeout(function () {
        if (panel) {
            panel.hide();
        }
    }, 12000);

    return panel;
}