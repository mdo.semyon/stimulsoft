﻿
StiMobileDesigner.prototype.InitializeCheckExpressionPopupPanel_ = function () {
    var panel = document.createElement("div");
    panel.jsObject = this;

    //Add Arrow
    var arrow = document.createElement("div");
    arrow.className = "stiDesignerCheckPopupPanelArrow";
    panel.appendChild(arrow);

    if (this.options.checkPopupPanel) {
        this.options.checkPopupPanel.hide();
    }

    this.options.checkPopupPanel = panel;
    this.options.mainPanel.appendChild(panel);
    panel.className = "stiDesignerCheckPopupPanel";
    panel.style.minWidth = "150px";
    panel.jsObject = this;

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "10px 20px 10px 10px";
    panel.appendChild(innerTable);
        
    var img = document.createElement("img");
    img.style.marginRight = "10px";
    innerTable.addCell(img);
    
    var textCell = innerTable.addCell();

    panel.hide = function () {
        this.jsObject.options.mainPanel.removeChild(this);
        this.jsObject.options.checkPopupPanel = null;
        clearTimeout(this.hideTimer);
    }

    panel.show = function (messageText, parentButton) {
        img.src = this.jsObject.options.images["ReportChecker." + (messageText == "OK" ? "Information32.png" : "Error32.png")];
        textCell.innerHTML = messageText;
        this.style.left = (this.jsObject.FindPosX(parentButton, "stiDesignerMainPanel") + 5) + "px";
        this.style.top = (this.jsObject.FindPosY(parentButton, "stiDesignerMainPanel") - this.offsetHeight - 10) + "px";
        arrow.style.top = (this.offsetHeight - 2) + "px";
    }

    //Show with animation
    var d = new Date();
    var endTime = d.getTime() + 300;
    panel.style.opacity = 0;
    this.ShowAnimationForm(panel, endTime);

    //Hide after 15 sec.
    panel.hideTimer = setTimeout(function () {
        if (panel) {
            panel.hide();
        }
    }, 12000);

    return panel;
}