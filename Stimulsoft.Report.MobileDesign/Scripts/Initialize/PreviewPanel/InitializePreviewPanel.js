﻿
StiMobileDesigner.prototype.InitializePreviewPanel = function () {
    var previewPanel = this.ChildWorkPanel("previewPanel", "stiDesignerPreviewPanel");
    previewPanel.style.display = "none";

    var viewerContainer = document.createElement("div");
    this.options.viewerContainer = viewerContainer;
    this.options.mainPanel.appendChild(viewerContainer);
    viewerContainer.style.position = "absolute";
    viewerContainer.style.background = "#ffffff";
    viewerContainer.style.zIndex = "30";
    viewerContainer.style.display = "none";
    viewerContainer.visible = false;
    viewerContainer.style.bottom = "0px";
    viewerContainer.style.right = "0px";
    viewerContainer.style.left = "0px";
    viewerContainer.style.top = this.options.toolBar.offsetHeight + "px";

    viewerContainer.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
    }

    viewerContainer.addFrame = function () {
        if (viewerContainer.frame) viewerContainer.removeChild(viewerContainer.frame);
        viewerContainer.frame = document.createElement("iframe");
        viewerContainer.appendChild(viewerContainer.frame);
        viewerContainer.frame.style.position = "absolute";
        viewerContainer.frame.style.width = "100%";
        viewerContainer.frame.style.height = "100%";
        viewerContainer.frame.style.border = "0px";
    }
        
    previewPanel.onshow = function () {
        this.jsObject.options.previewMode = true;
        this.undoButtonState = this.jsObject.options.buttons.undoButton.isEnabled;
        this.redoButtonState = this.jsObject.options.buttons.redoButton.isEnabled;
        this.jsObject.options.buttons.undoButton.setEnabled(false);
        this.jsObject.options.buttons.redoButton.setEnabled(false);
        if (this.jsObject.options.jsMode && this.jsObject.options.buttons.saveReportHotButton) {
            this.jsObject.options.buttons.saveReportHotButton.setEnabled(false);
        }
        this.jsObject.options.viewerContainer.changeVisibleState(true);
        if (!this.jsObject.options.cloudMode && this.jsObject.options.viewer) {
            (this.jsObject.options.viewer.jsObject.controls || this.jsObject.options.viewer.jsObject.options).reportPanel.style.top = this.jsObject.options.viewer.jsObject.options.toolbar.offsetHeight + "px";
        }
    }

    previewPanel.onhide = function () {
        this.jsObject.options.previewMode = false;
        this.jsObject.options.buttons.undoButton.setEnabled(this.undoButtonState);
        this.jsObject.options.buttons.redoButton.setEnabled(this.redoButtonState);
        if (this.jsObject.options.jsMode && this.jsObject.options.buttons.saveReportHotButton) {
            this.jsObject.options.buttons.saveReportHotButton.setEnabled(true);
        }
        this.jsObject.options.viewerContainer.changeVisibleState(false);

        if (this.jsObject.options.cloudMode && !this.jsObject.options.isOnlineVersion) {
            var previewFrame = this.jsObject.options.viewerContainer.frame;
            var win = previewFrame.contentWindow || previewFrame.window;
            if (win && win.jsStiCloudReportsMobileViewer) {
                win.jsStiCloudReportsMobileViewer.StopRenderReportTask();
            }

            if (this.jsObject.options.buttons.previewToolButton)
                this.jsObject.options.buttons.previewToolButton.progress.style.visibility = "hidden";
        }
        else if (this.jsObject.options.viewer) {
            var viewerExportForm = (this.jsObject.options.viewer.jsObject.controls || this.jsObject.options.viewer.jsObject.options).forms.exportForm;
            if (viewerExportForm && viewerExportForm.visible) viewerExportForm.changeVisibleState(false);
            (this.jsObject.options.viewer.jsObject.controls || this.jsObject.options.viewer.jsObject.options).processImage.hide();
        }

        if (this.jsObject.SendCommandCloseViewer) this.jsObject.SendCommandCloseViewer();
    }
}