﻿
StiMobileDesigner.prototype.InitializeDatePicker = function () {
    var datePicker = this.BaseMenu("datePicker", null, "Down");
    datePicker.style.zIndex = "45";
    datePicker.parentDataControl = null;
    datePicker.dayButtons = [];
    datePicker.key = new Date();
    datePicker.showTime = true;

    //Add Header Buttons
    var headerButtonsTable = this.CreateHTMLTable();
    datePicker.innerContent.appendChild(headerButtonsTable);

    //Prev Month
    datePicker.prevMonthButton = this.StandartSmallButton("datePickerPrevMonthButton", null, null, "ArrowLeft.png", null, null, true);
    datePicker.prevMonthButton.style.margin = "1px 2px 0 1px";
    datePicker.prevMonthButton.datePicker = datePicker;
    datePicker.prevMonthButton.action = function () {
        var month = this.datePicker.key.getMonth();
        var year = this.datePicker.key.getFullYear();
        month--;
        if (month == -1) { month = 11; year--; }
        var countDaysInMonth = this.jsObject.GetCountDaysOfMonth(year, month);
        if (countDaysInMonth < this.datePicker.key.getDate()) this.datePicker.key.setDate(countDaysInMonth);
        this.datePicker.key.setMonth(month); this.datePicker.key.setYear(year);
        this.datePicker.fill();
        this.datePicker.action();
    };
    headerButtonsTable.addCell(datePicker.prevMonthButton);

    //Month DropDownList
    datePicker.monthDropDownList = this.DropDownList("datePickerMonthDropDownList", this.options.isTouchDevice ? 79 : 81, null, this.GetMonthesForDatePickerItems(), true);
    datePicker.monthDropDownList.style.margin = "1px 2px 0 0";
    datePicker.monthDropDownList.datePicker = datePicker;
    datePicker.monthDropDownList.action = function () {
        var countDaysInMonth = this.jsObject.GetCountDaysOfMonth(this.datePicker.key.getFullYear(), parseInt(this.key));
        if (countDaysInMonth < this.datePicker.key.getDate()) this.datePicker.key.setDate(countDaysInMonth);
        this.datePicker.key.setMonth(parseInt(this.key));
        this.datePicker.repaintDays();
        this.datePicker.action();
    };
    headerButtonsTable.addCell(datePicker.monthDropDownList);

    //Override menu
    datePicker.monthDropDownList.menu.style.zIndex = "47";
    datePicker.monthDropDownList.menu.datePicker = datePicker;
    datePicker.monthDropDownList.menu.onmousedown = function () {
        if (!this.isTouchStartFlag) this.ontouchstart(true); 
    }
    datePicker.monthDropDownList.menu.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.dropDownListMenuPressed = this;
        this.datePicker.ontouchstart();
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    //Year TextBox
    datePicker.yearTextBox = this.TextBox("datePickerYearTextBox", 30, "Year");
    datePicker.yearTextBox.style.margin = "1px 2px 0 0";
    datePicker.yearTextBox.datePicker = datePicker;
    datePicker.yearTextBox.action = function () {
        var year = this.jsObject.StrToCorrectPositiveInt(this.value);
        this.value = year;
        this.datePicker.key.setYear(year);
        this.datePicker.repaintDays();
        this.datePicker.action();
    };
    headerButtonsTable.addCell(datePicker.yearTextBox);

    //Next Month
    datePicker.nextMonthButton = this.StandartSmallButton("datePickerNextMonthButton", null, null, "ArrowRight.png", "NextMonth", null, null, true);
    datePicker.nextMonthButton.datePicker = datePicker;
    datePicker.nextMonthButton.style.margin = "1px 1px 0 0";
    datePicker.nextMonthButton.action = function () {
        var month = this.datePicker.key.getMonth();
        var year = this.datePicker.key.getFullYear();
        month++;
        if (month == 12) { month = 0; year++; }
        var countDaysInMonth = this.jsObject.GetCountDaysOfMonth(year, month);
        if (countDaysInMonth < this.datePicker.key.getDate()) this.datePicker.key.setDate(countDaysInMonth);
        this.datePicker.key.setMonth(month); this.datePicker.key.setYear(year);
        this.datePicker.fill();
        this.datePicker.action();
    };
    headerButtonsTable.addCell(datePicker.nextMonthButton);

    //Separator
    var separator = document.createElement("div");
    separator.style.margin = "2px 0 2px 0";
    separator.className = "stiDesignerDatePickerSeparator";
    datePicker.innerContent.appendChild(separator);

    datePicker.daysTable = this.CreateHTMLTable();
    datePicker.innerContent.appendChild(datePicker.daysTable);

    //Add Day Of Week
    if (this.options.datePickerFirstDayOfWeek == "Sunday") {
        this.options.dayOfWeekCollection.splice(6, 1);
        this.options.dayOfWeekCollection.splice(0, 0, "Sunday");
    }

    for (i = 0; i < 7; i++) {
        var dayOfWeekCell = datePicker.daysTable.addCell();
        dayOfWeekCell.className = "stiDesignerDatePickerDayOfWeekCell";
        var dayName = this.loc.A_WebViewer["Day" + this.options.dayOfWeekCollection[i]];
        if (dayName) dayOfWeekCell.innerHTML = dayName.toString().substring(0, 1).toUpperCase();
        if (i == (this.options.datePickerFirstDayOfWeek == "Sunday" ? 6 : 5)) dayOfWeekCell.style.color = "#0000ff";
        if (i == (this.options.datePickerFirstDayOfWeek == "Sunday" ? 0 : 6)) dayOfWeekCell.style.color = "#ff0000";
    }

    //Add Day Cells    
    datePicker.daysTable.addRow();
    var rowCount = 1;
    for (i = 0; i < 42; i++) {
        var dayButton = this.DatePickerDayButton("dayButton" + i);
        dayButton.datePicker = datePicker;
        dayButton.style.margin = "1px";
        datePicker.dayButtons.push(dayButton);
        datePicker.daysTable.addCellInRow(rowCount, dayButton);
        if ((i + 1) % 7 == 0) { datePicker.daysTable.addRow(); rowCount++ }
    }

    //Separator2
    var separator2 = document.createElement("div");
    separator2.style.margin = "2px 0 2px 0";
    separator2.className = "stiDesignerDatePickerSeparator";
    datePicker.innerContent.appendChild(separator2);

    //Time
    var timeTable = this.CreateHTMLTable();
    timeTable.style.width = "100%";
    timeTable.className = "stiDesignerTextContainer";
    datePicker.innerContent.appendChild(timeTable);
    timeTable.addTextCell(this.loc.FormFormatEditor.Time + ":").style.padding = "0 4px 0 4px";
    var timeControl = this.TextBox(null, 90);
    timeControl.style.margin = "1px 2px 2px 2px";
    var timeControlCell = timeTable.addCell(timeControl);
    timeControlCell.style.width = "100%";
    timeControlCell.style.textAlign = "right";
    datePicker.time = timeControl;

    timeControl.action = function () {
        var time = this.jsObject.stringToTime(this.value);
        datePicker.key.setHours(time.hours);
        datePicker.key.setMinutes(time.minutes);
        datePicker.key.setSeconds(time.seconds);
        this.value = this.jsObject.formatDate(datePicker.key, "h:nn:ss");
        datePicker.action();
    };

    datePicker.repaintDays = function () {
        var month = this.key.getMonth();
        var year = this.key.getFullYear();
        var countDaysInMonth = this.jsObject.GetCountDaysOfMonth(year, month);
        var firstDay = this.jsObject.GetDayOfWeek(year, month, 1);
        if (this.jsObject.options.datePickerFirstDayOfWeek == "Monday") firstDay--;
        else if (firstDay == 7 && this.jsObject.options.datePickerFirstDayOfWeek == "Sunday") firstDay = 0;

        for (i = 0; i < 42; i++) {
            var numDay = i - firstDay + 1;
            var isSelectedDay = (numDay == this.key.getDate());
            var dayButton = this.dayButtons[i];

            if (!((i < firstDay) || (i - firstDay > countDaysInMonth - 1))) {
                dayButton.numberOfDay = numDay;
                dayButton.caption.innerHTML = numDay;
                dayButton.setEnabled(true);
                dayButton.setSelected(isSelectedDay);
            }
            else {
                dayButton.caption.innerHTML = "";
                dayButton.setEnabled(false);
            }
        }
    }

    datePicker.fill = function () {
        this.yearTextBox.value = this.key.getFullYear();
        this.monthDropDownList.setKey(this.key.getMonth());
        this.repaintDays();
        if (this.showTime) {
            this.time.value = this.jsObject.formatDate(this.key, "h:nn:ss");
        }
    }

    datePicker.onshow = function () {
        this.key = new Date(this.parentDataControl.key.toString());
        this.fill();
        separator2.style.display = this.showTime ? "" : "none";
        timeTable.style.display = this.showTime ? "" : "none";
    };

    datePicker.action = function () {
        this.parentDataControl.setKey(this.key);
        this.parentDataControl.action();
    };

    //Ovveride Methods
    datePicker.onmousedown = function () {
        if (!this.isTouchStartFlag) this.ontouchstart(true);
    }

    datePicker.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.datePickerPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    datePicker.changeVisibleState = function (state) {
        if (state) {
            this.onshow();
            this.style.display = "";
            this.visible = true;
            this.style.overflow = "hidden";
            this.parentDataControl.setSelected(true);
            this.parentButton.setSelected(true);
            this.jsObject.options.currentDatePicker = this;
            this.style.width = this.innerContent.offsetWidth + "px";
            this.style.height = this.innerContent.offsetHeight + "px";
            this.style.left = (this.jsObject.FindPosX(this.parentDataControl, "stiDesignerMainPanel")) + "px";
            var browserHeight = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight) - this.jsObject.FindPosY(this.jsObject.options.mainPanel);
            var animationDirection =
                (this.jsObject.FindPosY(this.parentDataControl, "stiDesignerMainPanel") + this.parentDataControl.offsetHeight + this.offsetHeight > browserHeight) &&
                (this.jsObject.FindPosY(this.parentDataControl, "stiDesignerMainPanel") - this.offsetHeight > 0)
                ? "Up" : "Down";
            this.style.top = animationDirection == "Down"
                ? (this.jsObject.FindPosY(this.parentDataControl, "stiDesignerMainPanel") + this.parentDataControl.offsetHeight + 1) + "px"
                : (this.jsObject.FindPosY(this.parentDataControl, "stiDesignerMainPanel") - this.offsetHeight) + "px";
            this.innerContent.style.top = (animationDirection == "Down" ? -this.innerContent.offsetHeight : this.innerContent.offsetHeight) + "px";

            d = new Date();
            var endTime = d.getTime() + this.jsObject.options.menuAnimDuration;
            this.jsObject.ShowAnimationVerticalMenu(this, 0, endTime);
        }
        else {
            clearTimeout(this.innerContent.animationTimer);
            this.visible = false;
            //this.showTime = false;
            this.parentDataControl.setSelected(false);
            this.parentButton.setSelected(false);
            this.style.display = "none";
            if (this.jsObject.options.currentDatePicker == this) this.jsObject.options.currentDatePicker = null;
        }
    }

    return datePicker;
}

StiMobileDesigner.prototype.DatePickerDayButton = function (name) {
    var button = this.SmallButton(name, null, "10", null, null, null, this.GetStyles("DatePickerDayButton"));
    button.caption.style.textAlign = "center";
    button.innerTable.style.width = "100%";
    button.caption.style.padding = "0px";
    button.numberOfDay = 1;
    button.action = function () {
        this.datePicker.key.setDate(parseInt(this.numberOfDay));
        this.setSelected(true);
        this.datePicker.action();
        this.datePicker.changeVisibleState(false);
    }

    return button;
}

//Helper Methods
StiMobileDesigner.prototype.GetDayOfWeek = function (year, month) {
    result = new Date(year, month, 1).getDay();
    if (result == 0) result = 7;
    return result;
}

StiMobileDesigner.prototype.GetCountDaysOfMonth = function (year, month) {
    var countDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    var count = countDaysInMonth[month];

    if (month == 1)
        if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))
            count = 29;
        else
            count = 28;
    return count;
}