﻿
StiMobileDesigner.prototype.RadioButton = function (name, groupName, caption) {
    var radioButton = this.CreateHTMLTable();
    radioButton.jsObject = this;
    if (name != null) this.options.radioButtons[name] = radioButton;
    radioButton.name = name != null ? name : this.generateKey();
    radioButton.id = radioButton.name;
    radioButton.isEnabled = true;
    radioButton.isChecked = false;
    radioButton.groupName = groupName;
    radioButton.className = "stiMobileDesignerRadioButton";
    radioButton.captionText = caption;

    radioButton.outCircle = document.createElement("div");
    radioButton.outCircle.className = "stiMobileDesignerRadioButtonOutCircle";
    radioButton.circleCell = radioButton.addCell(radioButton.outCircle);

    radioButton.innerCircle = document.createElement("div");
    radioButton.innerCircle.style.visibility = "hidden";
    radioButton.innerCircle.className = this.options.isTouchDevice ? "stiMobileDesignerRadioButtonInnerCircle_Touch" : "stiMobileDesignerRadioButtonInnerCircle_Mouse";
    radioButton.outCircle.appendChild(radioButton.innerCircle);

    //Caption
    if (caption != null || typeof (caption) == "undefined") {
        radioButton.captionCell = radioButton.addCell();
        radioButton.captionCell.style.paddingLeft = "4px";
        radioButton.captionCell.style.whiteSpace = "nowrap";
        if (caption) radioButton.captionCell.innerHTML = caption;
    }

    radioButton.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    radioButton.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.outCircle.className = "stiMobileDesignerRadioButtonOutCircleOver";
    }

    radioButton.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.outCircle.className = "stiMobileDesignerRadioButtonOutCircle";
    }

    radioButton.onclick = function () {
        if (this.isTouchEndFlag || !this.isEnabled || this.jsObject.options.isTouchClick) return;
        radioButton.setChecked(true);
        radioButton.action();
    }

    radioButton.ontouchend = function () {
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        this.outCircle.className = "stiMobileDesignerRadioButtonOutCircleOver";
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        setTimeout(function () {
            this_.outCircle.className = "stiMobileDesignerRadioButtonOutCircle";
            this_.setChecked(true);
            this_.action();
        }, 150);
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    radioButton.ontouchstart = function () {
        this.jsObject.options.fingerIsMoved = false;
    }

    radioButton.setEnabled = function (state) {
        this.innerCircle.style.opacity = state ? "1" : "0.3";
        this.isEnabled = state;
        this.className = state ? "stiMobileDesignerRadioButton" : "stiMobileDesignerRadioButtonDisabled";
        this.outCircle.className = state ? "stiMobileDesignerRadioButtonOutCircle" : "stiMobileDesignerRadioButtonOutCircleDisabled";
    }

    radioButton.setChecked = function (state) {
        if (this.groupName && state)
            for (var name in this.jsObject.options.radioButtons) {
                if (this.groupName == this.jsObject.options.radioButtons[name].groupName)
                    this.jsObject.options.radioButtons[name].setChecked(false);
            }

        this.innerCircle.style.visibility = (state) ? "visible" : "hidden";
        this.isChecked = state;
        this.onChecked();
    }

    radioButton.onChecked = function () { }
    radioButton.action = function () { }

    return radioButton;
}