﻿
StiMobileDesigner.prototype.DropDownList = function (name, width, toolTip, items, readOnly, showImage, height, cutMenu) {
    var dropDownList = this.CreateHTMLTable();
    dropDownList.jsObject = this;
    dropDownList.style.borderCollapse = "separate";
    if (name != null) this.options.controls[name] = dropDownList;
    dropDownList.name = name != null ? name : this.generateKey();
    dropDownList.key = null;
    dropDownList.imageCell = null;
    dropDownList.readOnly = readOnly;
    dropDownList.items = (items == null) ? {} : items;
    dropDownList.isEnabled = true;
    dropDownList.isSelected = false;
    dropDownList.isOver = false;
    dropDownList.toolTip = toolTip;
    dropDownList.fullWidth = width;
    dropDownList.cutMenu = cutMenu;

    if (toolTip && typeof (toolTip) != "object") { dropDownList.setAttribute("title", toolTip); }

    //Image
    if (showImage) {
        dropDownList.image = document.createElement("img");
        dropDownList.image.className = "stiDesignerDropDownListImage";
        dropDownList.imageCell = dropDownList.addCell();
        dropDownList.imageCell.appendChild(dropDownList.image);
        dropDownList.imageCell.className = "stiDesignerDropDownListImageCell";
    }

    var textBoxWidth = width - (this.options.isTouchDevice ? 24 : 16) - (showImage ? 38 : 0);
    dropDownList.textBox = this.TextBox(name + "TextBox", textBoxWidth, height);
    dropDownList.textBox.readOnly = readOnly;
    dropDownList.textBox.style.cursor = readOnly ? "default" : "text";
    if (readOnly) {
        dropDownList.textBox.onclick = function () {
            if (!this.isTouchEndFlag && !this.jsObject.options.isTouchClick) 
                this.dropDownList.button.onclick();
        }
        dropDownList.textBox.ontouchend = function () {
            var this_ = this;
            this.isTouchEndFlag = true;
            clearTimeout(this.isTouchEndTimer);
            this.dropDownList.button.ontouchend();
            this.isTouchEndTimer = setTimeout(function () {
                this_.isTouchEndFlag = false;
            }, 1000);
        }
    }
    dropDownList.textBox.dropDownList = dropDownList;
    dropDownList.addCell(dropDownList.textBox).style.fontSize = "0px";

    if (showImage) {
        dropDownList.textBox.style.borderLeft = "0px";
    }

    //DropDownButton
    dropDownList.button = this.SmallButton(name + "DropDownButton", null, null, "DropDownButton.png", null, null, this.GetStyles("DropDownListButton"));
    dropDownList.button.style.height = this.options.isTouchDevice ? "26px" : "21px";
    if (height) dropDownList.button.style.height = (this.options.isTouchDevice ? height + 5 : height) + "px";
    dropDownList.button.dropDownList = dropDownList;
    dropDownList.addCell(dropDownList.button).style.fontSize = "0px";

    //Menu
    dropDownList.menu = this.DropDownListMenu(dropDownList);
    dropDownList.menu.isDinamic = (items == null);
    if (items != null) dropDownList.menu.addItems(items);

    dropDownList.addItems = function (items) {
        dropDownList.items = items;
        dropDownList.menu.addItems(items);
    }

    dropDownList.getItemByKey = function (key) {
        if (!this.items) return null;
        for (var i in this.items) {
            if (this.items[i].key == key) return this.items[i];
        }
        return null;
    }

    dropDownList.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    dropDownList.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        this.button.isOver = true;
        this.textBox.isOver = true;
        if (!this.isSelected) {
            this.button.className = this.button.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
            this.textBox.className = "stiDesignerTextBoxOver" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
            if (this.imageCell) this.imageCell.className = "stiDesignerDropDownListImageCellOver";
        }
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object" && !this.isSelected)
            this.jsObject.options.toolTip.showWithDelay(
                this.toolTip[0],
                this.toolTip[1],
                this.toolTip.length == 3 ? this.toolTip[2].left : this.jsObject.FindPosX(this, "stiDesignerMainPanel"),
                this.toolTip.length == 3 ? this.toolTip[2].top : this.jsObject.options.toolBar.offsetHeight + this.jsObject.options.workPanel.offsetHeight - 1
            );
    }

    dropDownList.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.isOver = false;
        this.button.isOver = false;
        this.textBox.isOver = false;
        if (!this.isSelected) {
            this.button.className = this.button.styles["default"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
            this.textBox.className = "stiDesignerTextBox" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
            if (this.imageCell) this.imageCell.className = "stiDesignerDropDownListImageCell";
        }
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object") this.jsObject.options.toolTip.hideWithDelay();
    }

    dropDownList.setEnabled = function (state) {
        this.isEnabled = state;
        this.button.setEnabled(state);
        this.textBox.setEnabled(state);
        if (this.imageCell) {
            this.imageCell.className = state ? "stiDesignerDropDownListImageCell" : "stiDesignerDropDownListImageCellDisabled";
            this.image.style.visibility = state ? "visible" : "hidden";
        }
    }

    dropDownList.setSelected = function (state) {
        this.isSelected = state;
        this.textBox.setSelected(state);
        this.button.setSelected(state);
        if (this.imageCell) this.imageCell.className = state ? "stiDesignerDropDownListImageCellSelected" : "stiDesignerDropDownListImageCell";
    }

    dropDownList.setKey = function (key) {
        this.key = key;
        if (key == null) return;
        if (key == "StiEmptyValue") {
            this.textBox.value = "";
            return;
        }
        for (var itemName in this.items)
            if (key == this.items[itemName].key) {
                this.textBox.value = this.items[itemName].caption;
                if (this.image) this.image.src = this.jsObject.options.images[this.items[itemName].imageName];
                return;
            }
        this.textBox.value = key.toString();
    }

    dropDownList.haveKey = function (key) {
        for (var num in this.items)
            if (this.items[num].key == key) return true;
        return false;
    }

    dropDownList.action = function () { }

    //Override methods
    dropDownList.button.onmouseover = null;
    dropDownList.button.onmouseleave = null;
    dropDownList.button.onmouseenter = null;
    dropDownList.textBox.onmouseover = null;
    dropDownList.textBox.onmouseenter = null;
    dropDownList.textBox.onmouseleave = null;

    dropDownList.textBox.onfocus = function () {
        this.jsObject.options.controlsIsFocused = this;
        dropDownList.hideError();
    }

    dropDownList.textBox.onblur = function () {
        this.isOver = false;
        this.dropDownList.isOver = false;
        this.dropDownList.button.isOver = false;
        this.dropDownList.setSelected(false);
        this.jsObject.options.controlsIsFocused = false;
        dropDownList.hideError();
        this.action();
    }

    dropDownList.textBox.action = function () {
        if (!this.dropDownList.readOnly) {
            this.dropDownList.setKey(this.value);
            this.dropDownList.action();
        }
    }

    dropDownList.button.action = function () {
        dropDownList.hideError();
        if (!this.dropDownList.menu.visible) {
            if (this.dropDownList.menu.isDinamic) this.dropDownList.menu.addItems(this.dropDownList.items);
            if (this.jsObject.options.showTooltips && this.dropDownList.toolTip && typeof (this.dropDownList.toolTip) == "object") this.jsObject.options.toolTip.hide();
            this.dropDownList.menu.changeVisibleState(true);
        }
        else
            this.dropDownList.menu.changeVisibleState(false);
    }

    dropDownList.hideError = function () {
        this.textBox.hideError();
    }

    dropDownList.showError = function (text) {
        var img = document.createElement("img");
        img.src = this.jsObject.options.images["Warning.png"];
        img.style.marginLeft = (width + 10) + "px";
        img.style.position = "absolute";
        img.style.marginTop = this.jsObject.options.isTouchDevice ? "7px" : "5px";
        img.title = text;

        if (this.textBox.parentElement) {
            this.textBox.hideError();
            this.textBox.errorImage = img;
            this.textBox.parentElement.insertBefore(img, this.textBox);
        }

        var i = 0;
        var intervalTimer = setInterval(function () {
            img.style.display = i % 2 != 0 ? "" : "none";
            i++;
            if (i > 5) clearInterval(intervalTimer);
        }, 400);
    }

    return dropDownList;
}

StiMobileDesigner.prototype.DropDownListMenu = function (dropDownList) {
    var menu = this.VerticalMenu(dropDownList.name + "Menu", dropDownList.button, "Down", dropDownList.items, this.GetStyles("MenuStandartItem"), dropDownList.cutMenu);
    menu.dropDownList = dropDownList;
    if (dropDownList.cutMenu) {
        menu.innerContent.style.width = (dropDownList.fullWidth + 3) + "px";
        menu.innerContent.style.overflowX = "hidden";
    }
    else {
        menu.innerContent.style.minWidth = (dropDownList.fullWidth + 3) + "px";
    }

    menu.changeVisibleState = function (state) {
        if (state) {
            this.onshow();
            this.style.display = "";
            this.visible = true;
            this.style.overflow = "hidden";
            var dropDownList = this.parentButton.dropDownList;
            dropDownList.setSelected(true);
            this.jsObject.options.currentDropDownListMenu = this;
            this.style.width = this.innerContent.offsetWidth + "px";
            this.style.height = this.innerContent.offsetHeight + "px";
            this.style.left = (this.jsObject.FindPosX(dropDownList, "stiDesignerMainPanel")) + "px";
            var browserHeight = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight) - this.jsObject.FindPosY(this.jsObject.options.mainPanel);

            var animationDirection =
                (this.jsObject.FindPosY(dropDownList, "stiDesignerMainPanel") + dropDownList.offsetHeight + this.offsetHeight > browserHeight) &&
                (this.jsObject.FindPosY(dropDownList, "stiDesignerMainPanel") - this.offsetHeight > 0)
                ? "Up" : "Down";
            this.style.top = animationDirection == "Down"
                ? (this.jsObject.FindPosY(dropDownList, "stiDesignerMainPanel") + this.parentButton.offsetHeight + 2) + "px"
                : (this.jsObject.FindPosY(dropDownList, "stiDesignerMainPanel") - this.offsetHeight) + "px";
            this.innerContent.style.top = (animationDirection == "Down" ? -this.innerContent.offsetHeight : this.innerContent.offsetHeight) + "px";

            d = new Date();
            var endTime = d.getTime() + this.jsObject.options.menuAnimDuration;
            this.jsObject.ShowAnimationVerticalMenu(this, 0, endTime);
        }
        else {
            clearTimeout(this.innerContent.animationTimer);
            this.visible = false;
            this.parentButton.dropDownList.setSelected(false);
            this.style.display = "none";
            if (this.jsObject.options.currentDropDownListMenu == this) this.jsObject.options.currentDropDownListMenu = null;
        }
    }

    menu.onmousedown = function () {
        if (!this.isTouchStartFlag) this.ontouchstart(true);
    }

    menu.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.dropDownListMenuPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.dropDownList.key = menuItem.key;
        this.dropDownList.textBox.value = menuItem.captionInnerContainer ? menuItem.captionInnerContainer.innerHTML : menuItem.caption.innerHTML;
        if (this.dropDownList.image) this.dropDownList.image.src = this.jsObject.options.images[menuItem.imageName];
        this.dropDownList.action();
    }

    menu.onshow = function () {
        if (this.dropDownList.key == null) return;
        for (var itemName in this.items) {
            if (this.dropDownList.key == this.items[itemName].key) {
                this.items[itemName].setSelected(true);
                return;
            }
            else
                if (itemName.indexOf("separator") != 0) this.items[itemName].setSelected(false);
        }
    }

    return menu;
}

StiMobileDesigner.prototype.DinamicDropDownList = function (name, width, toolTip, items, readOnly, showImage, height, cutMenu) {
    var dropDownList = this.DropDownList(name, width, toolTip, items, readOnly, showImage, height, cutMenu);

    delete this.options.controls[name];
    delete this.options.controls[name + "TextBox"];    
    delete this.options.buttons[name + "DropDownButton"];
    delete this.options.menus[name + "Menu"];

    return dropDownList;
}

//Data DropDownList
StiMobileDesigner.prototype.DataDropDownList = function (name, toolTip) {
    var dataDropDownList = this.DropDownList(name, 135, toolTip, null, true);

    dataDropDownList.addItems = function (items) {
        this.items = [];
        this.items.push(this.jsObject.Item("NotAssigned", this.jsObject.loc.Report.NotAssigned, null, "[Not Assigned]"));
        if (!items) return;
        for (var index in items) {
            this.items.push(items[index]);
        }
    }

    dataDropDownList.reset = function () {
        this.setKey("[Not Assigned]");
    }

    return dataDropDownList;
}