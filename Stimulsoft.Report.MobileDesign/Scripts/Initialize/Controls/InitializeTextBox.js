﻿
StiMobileDesigner.prototype.TextBox = function (name, width, height, toolTip) {
    var textBox = document.createElement("input");
    textBox.jsObject = this;
    textBox.isEnabled = true;
    textBox.isSelected = false;
    textBox.isOver = false;
    textBox.toolTip = toolTip;
    textBox.className = this.options.isTouchDevice ? "stiDesignerTextBox_Touch" : "stiDesignerTextBox_Mouse";
    textBox.style.height = this.options.isTouchDevice ? "26px" : "21px"
    if (height) textBox.style.height = (this.options.isTouchDevice ? height + 5 : height) + "px";
    if (toolTip && typeof (toolTip) != "object") { textBox.setAttribute("title", toolTip); }

    if (width != null) {
        textBox.style.width = typeof(width) == "string" ? width : (width + "px");
    }
    
    if (name) {
        this.options.controls[name] = textBox;
        textBox.name = name;
    }

    textBox.setEnabled = function (state) {
        this.isEnabled = state;
        this.disabled = !state;
        this.className = (state ? "stiDesignerTextBox" : "stiDesignerTextBoxDisabled") +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    textBox.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    textBox.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        if (!this.isSelected) this.className = "stiDesignerTextBoxOver" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        if (this.toolTip && typeof (this.toolTip) == "object")
            this.jsObject.options.toolTip.showWithDelay(
                this.toolTip[0],
                this.toolTip[1],
                this.toolTip.length == 3 ? this.toolTip[2].left : this.jsObject.FindPosX(this, "stiDesignerMainPanel"),
                this.toolTip.length == 3 ? this.toolTip[2].top : this.jsObject.options.toolBar.offsetHeight + this.jsObject.options.workPanel.offsetHeight - 1
            );
    }

    textBox.onfocus = function () {
        this.jsObject.options.controlsIsFocused = this;
        this.hideError();
        this.oldValue = this.value;
    }

    textBox.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.isOver = false;
        if (!this.isSelected) this.className = "stiDesignerTextBox" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        if (this.toolTip && typeof (this.toolTip) == "object") this.jsObject.options.toolTip.hideWithDelay();
    }

    textBox.setSelected = function (state) {
        this.isSelected = state;
        this.className = (state ? "stiDesignerTextBoxOver" : (this.isEnabled ? (this.isOver ? "stiDesignerTextBoxOver" : "stiDesignerTextBox") : "stiDesignerTextBoxDisabled")) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    textBox.onblur = function () {
        this.isOver = false;
        this.setSelected(false);
        this.jsObject.options.controlsIsFocused = false;
        if (!this.readOnly) this.action();
        this.hideError();
    }

    textBox.onkeypress = function (event) {
        if (this.readOnly) return false;
        if (event && event.keyCode == 13) {
            this.blur();
            this.actionOnKeyEnter();
            return false;
        }
    }

    textBox.setReadOnly = function (state) {
        this.style.cursor = state ? "default" : "";
        this.readOnly = state;
        try {
            this.setAttribute("unselectable", state ? "on" : "off");
            this.setAttribute("onselectstart", state ? "return false" : "");
        }
        catch (e) { };
    }
    
    textBox.hideError = function () {
        if (this.parentElement && this.errorImage) {
            this.parentElement.removeChild(this.errorImage);
            this.errorImage = null;
        }
    }

    textBox.showError = function (text) {
        var img = document.createElement("img");
        img.src = this.jsObject.options.images["Warning.png"];
        img.style.marginLeft = (width + 10) + "px";
        img.style.position = "absolute";
        img.style.marginTop = this.jsObject.options.isTouchDevice ? "7px" : "5px";
        img.title = text;

        if (this.parentElement) {
            this.hideError();
            this.errorImage = img;
            this.parentElement.insertBefore(img, this);
        }
        
        var i = 0;
        var intervalTimer = setInterval(function () {
            img.style.display = i % 2 != 0 ? "" : "none";
            i++;
            if (i > 5) clearInterval(intervalTimer);
        }, 400);
    }

    textBox.checkNotEmpty = function (fieldName) {        
        if (this.value == "") {
            var text = fieldName ? this.jsObject.loc.Errors.FieldRequire.replace("{0}", fieldName) : this.jsObject.loc.Errors.FieldRequire.replace("'{0}'", "");
            this.showError(text);
            return false;
        }
        return true;
    }

    textBox.checkExists = function (collection, propertyName) {
        if (collection && propertyName) {
            for (var index in collection) {
                var propertValue = collection[index][propertyName];
                if (typeof (propertValue) == "string" && propertValue.toLowerCase() == this.value.toLowerCase()) {
                    this.showError(this.jsObject.loc.Errors.NameExists.replace("{0}", this.value));
                    return false;
                }
            }
        }
        return true;
    }

    textBox.action = function () { };

    textBox.actionOnKeyEnter = function () { };

    return textBox;
}

StiMobileDesigner.prototype.TextBoxDoubleValue = function (name, width) {
    var textBox = this.TextBox(name, width);

    textBox.action = function() {
        this.value = this.jsObject.StrToDouble(this.value);
    }
    
    return textBox;
}

StiMobileDesigner.prototype.TextBoxIntValue = function (name, width) {
    var textBox = this.TextBox(name, width);
    
    textBox.action = function() {
        this.value = this.jsObject.StrToInt(this.value);
    }
    
    return textBox;
}

StiMobileDesigner.prototype.TextBoxPositiveIntValue = function (name, width) {
    var textBox = this.TextBox(name, width);
    
    textBox.action = function() {
        this.value = this.jsObject.StrToCorrectPositiveInt(this.value);
    }
    
    return textBox;
}

StiMobileDesigner.prototype.TextBoxPositiveDoubleValue = function (name, width) {
    var textBox = this.TextBox(name, width);
    
    textBox.action = function() {        
        this.value = Math.abs(this.jsObject.StrToDouble(this.value));
    }
    
    return textBox;
}

StiMobileDesigner.prototype.DinamicTextBox = function (name, width, height) {
    var textBox = this.TextBox(name, width, height);
    delete this.options.controls[name];

    return textBox;
}