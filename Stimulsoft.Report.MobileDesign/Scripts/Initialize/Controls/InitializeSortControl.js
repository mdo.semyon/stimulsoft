﻿
StiMobileDesigner.prototype.SortControl = function (name, columns, widthContainer, heightContainer) {
    var sortControl = document.createElement("div");
    sortControl.jsObject = this;
    sortControl.toolBar = this.CreateHTMLTable();
    sortControl.toolBar.style.margin = "4px";
    sortControl.appendChild(sortControl.toolBar);
    sortControl.columns = columns;
    sortControl.currentDataSourceName = null;
    sortControl.noSortText = this.loc.FormBand.NoSort;

    //Add Sort
    var addSortText = this.loc.FormBand.AddSort.replace("&", "");
    sortControl.addSortButton = this.DinamicStandartSmallButton(name + "AddButton", null, addSortText, "AddSort.png", addSortText, null);
    sortControl.toolBar.addCell(sortControl.addSortButton);
    sortControl.addSortButton.action = function () {
        this.sortContainer.addSort({ "column": sortControl.noSortText, "direction": "ASC" });
    }

    //Remove Sort
    var removeSortText = this.loc.FormBand.RemoveSort.replace("&", "");
    sortControl.removeSortButton = this.DinamicStandartSmallButton(name + "RemoveButton", null, removeSortText, "Remove.png", removeSortText, null);
    sortControl.toolBar.addCell(sortControl.removeSortButton);
    sortControl.removeSortButton.action = function () { this.sortContainer.removeSort(); }

    sortControl.toolBar.addCell(this.HomePanelSeparator()).style.padding = "0 2px 0 2px";
    sortControl.moveUpButton = this.DinamicStandartSmallButton(name + "MoveUp", null, null, "MoveUp.png", null, null);
    sortControl.moveDownButton = this.DinamicStandartSmallButton(name + "MoveDown", null, null, "MoveDown.png", null, null);
    sortControl.moveUpButton.setEnabled(false);
    sortControl.moveDownButton.setEnabled(false);
    sortControl.toolBar.addCell(sortControl.moveUpButton);
    sortControl.toolBar.addCell(sortControl.moveDownButton);

    sortControl.moveUpButton.action = function () {
        if (sortControl.sortContainer.selectedItem) { sortControl.sortContainer.selectedItem.move("Up"); }
    }

    sortControl.moveDownButton.action = function () {
        if (sortControl.sortContainer.selectedItem) { sortControl.sortContainer.selectedItem.move("Down"); }
    }

    //Container
    sortControl.sortContainer = this.SortContainer(sortControl);
    if (widthContainer) sortControl.sortContainer.style.width = widthContainer + "px";
    if (heightContainer) sortControl.sortContainer.style.height = heightContainer + "px";
    sortControl.appendChild(sortControl.sortContainer);
    sortControl.addSortButton.sortContainer = sortControl.sortContainer;
    sortControl.removeSortButton.sortContainer = sortControl.sortContainer;

    sortControl.sortContainer.onAction = function () {
        var count = this.getCountItems();
        var index = this.selectedItem ? this.selectedItem.getIndex() : -1;
        sortControl.moveUpButton.setEnabled(index > 0);
        sortControl.moveDownButton.setEnabled(index != -1 && index < count - 1);
    }

    sortControl.fill = function (sorts) {
        this.sortContainer.clear();
        if (!sorts) return;
        for (var i = 0; i < sorts.length; i++) this.sortContainer.addSort(sorts[i], true);
        this.sortContainer.onAction();
    }

    sortControl.getValue = function () {
        var result = [];
        for (var num = 0; num < this.sortContainer.childNodes.length; num++) {
            if (this.sortContainer.childNodes[num].column.textBox.value != sortControl.noSortText) {
                var columnKey = this.sortContainer.childNodes[num].column.key || this.sortContainer.childNodes[num].column.textBox.value;
                result.push({ "direction": this.sortContainer.childNodes[num].direction.key, "column": columnKey });
            }
        }
        return result;
    }

    return sortControl;
}

StiMobileDesigner.prototype.SortContainer = function (sortControl) {
    var sortContainer = document.createElement("div");
    sortControl.sortContainer = sortContainer;
    sortContainer.jsObject = this;
    sortContainer.className = "stiDesignerSortContainer";
    sortContainer.sortControl = sortControl;
    sortContainer.selectedItem = null;

    sortContainer.addSort = function (sortObject, notAction) {
        var sortItem = this.jsObject.SortItem(this);
        this.appendChild(sortItem);
        sortItem.direction.setKey(sortObject.direction);
        if (this.sortControl.columns != null)
            sortItem.column.setKey(sortObject.column);
        else
            sortItem.column.textBox.value = sortObject.column;
        sortItem.setSelected();
        this.sortControl.removeSortButton.setEnabled(true);
        if (!notAction) this.onAction();
    }

    sortContainer.removeSort = function () {
        for (var num in this.childNodes) {
            if (this.childNodes[num].isSelected) {
                this.removeChild(this.childNodes[num]);
                this.selectedItem = null;
            }
        }
        if (this.childNodes.length > 0) this.childNodes[0].setSelected();
        this.sortControl.removeSortButton.setEnabled(this.childNodes.length > 0);
        this.onAction();
    }

    sortContainer.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        this.sortControl.removeSortButton.setEnabled(false);
        this.sortControl.moveUpButton.setEnabled(false);
        this.sortControl.moveDownButton.setEnabled(false);
    }

    sortContainer.getCountItems = function () {
        return sortContainer.childNodes.length;
    }

    sortContainer.onAction = function () { };

    return sortContainer;
}


StiMobileDesigner.prototype.SortItem = function (sortContainer) {
    var sortItem = document.createElement("div");
    sortItem.jsObject = this;
    sortItem.sortContainer = sortContainer;
    sortItem.key = this.newGuid().replace(/-/g, '');
    sortItem.isSelected = false;
    sortItem.className = "stiDesignerSortPanel";
    sortItem.innerTable = this.CreateHTMLTable();
    sortItem.appendChild(sortItem.innerTable);

    var textCell = sortItem.innerTable.addCell();
    textCell.innerHTML = this.loc.FormBand.SortBy;
    textCell.style.padding = "5px";
    sortItem.textCell = textCell;

    //Column
    if (sortContainer.sortControl.columns != null) {
        sortItem.column = this.DinamicDropDownList(sortItem.key + "Column", 180, null, sortContainer.sortControl.columns, true, false);
        sortItem.innerTable.addCell(sortItem.column).style.padding = "5px";
    }
    else {
        sortItem.column = this.ExpressionControl(null, 180);
        sortItem.innerTable.addCell(sortItem.column).style.padding = "5px";

        //FX
        var fxButton = sortItem.column.button;
        fxButton.image.src = this.options.images["Function.png"];
        fxButton.style.width = this.options.isTouchDevice ? "23px" : "18px";
        fxButton.imageCell.style.padding = "0px";

        fxButton.action = function () {
            this.jsObject.InitializeTextEditorForm(function (textEditorForm) {
                textEditorForm.controlTextBox = sortItem.column.textBox;

                textEditorForm.showFunction = function () {
                    this.container.style.visibility = "visible";
                    this.mainButtons.SummaryText.style.display = "none";
                    this.mainButtons.DataColumn.style.display = "none";
                    this.mainButtons.SystemVariable.style.display = "none";
                    this.mainButtons.RichText.style.display = "none";
                    this.jsObject.options.propertiesPanel.setDictionaryMode(true);
                    this.setMode("Expression");
                    this.expressionTextArea.focus();
                    this.expressionTextArea.value = sortItem.column.textBox.value == sortContainer.sortControl.noSortText ? "" : this.controlTextBox.value;
                }

                textEditorForm.actionFunction = function () {
                    this.controlTextBox.value = this.expressionTextArea.value;
                }

                textEditorForm.changeVisibleState(true);
            });
        }

        //Column
        var columnButton = this.StandartSmallButton(null, null, null, "TextEditButton.png", null, null);
        columnButton.imageCell.style.padding = "0px";
        columnButton.style.height = this.options.isTouchDevice ? "26px" : "21px";
        columnButton.style.width = this.options.isTouchDevice ? "23px" : "18px";
        columnButton.innerTable.style.width = "100%";
        sortItem.column.addCell(columnButton).className = "stiDesignerTextBoxEditButton";

        columnButton.action = function () {
            var currentDataSourceName = (sortContainer.sortControl.currentDataSourceName != null)
                ? sortContainer.sortControl.currentDataSourceName : this.jsObject.options.selectedObject.properties["dataSource"];
            this.key = currentDataSourceName + "." + sortItem.column.textBox.value;

            this.jsObject.InitializeDataColumnForm(function (dataColumnForm) {
                dataColumnForm.dataTree.build("Column", currentDataSourceName);
                dataColumnForm.needBuildTree = false;
                dataColumnForm.parentButton = columnButton;
                dataColumnForm.changeVisibleState(true);

                dataColumnForm.action = function () {
                    this.changeVisibleState(false);
                    sortItem.column.textBox.value = this.dataTree.key != ""
                        ? this.dataTree.key.substring(this.dataTree.key.indexOf(".") + 1, this.dataTree.key.length)
                        : sortContainer.sortControl.noSortText;
                }
            });
        }
    }

    //Direction
    sortItem.direction = this.DinamicImageList(sortItem.key + "Direction", true, false, null, this.GetSortDirectionItemsForSortForm());
    sortItem.direction.button.caption.style.fontSize = "12px";
    sortItem.innerTable.addCell(sortItem.direction).style.padding = "5px";

    sortItem.setSelected = function () {
        for (var num in this.sortContainer.childNodes) {
            this.sortContainer.childNodes[num].className = "stiDesignerSortPanel";
            this.sortContainer.childNodes[num].isSelected = false;
        }
        this.sortContainer.selectedItem = this;
        this.isSelected = true;
        this.className = "stiDesignerSortPanelSelected";
    }

    sortItem.onclick = function () {
        if (this.isTouchEndFlag || this.jsObject.options.isTouchClick) return;
        this.action();
    }

    sortItem.ontouchend = function () {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        if (this.jsObject.options.fingerIsMoved) return;
        this.action();
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    sortItem.action = function () {
        this.setSelected();
        sortContainer.onAction();
    }

    sortItem.getIndex = function () {
        for (var i = 0; i < sortContainer.childNodes.length; i++)
            if (sortContainer.childNodes[i] == this) return i;
    };

    sortItem.move = function (direction) {
        var index = this.getIndex();
        sortContainer.removeChild(this);
        var count = sortContainer.getCountItems();
        var newIndex = direction == "Up" ? index - 1 : index + 1;
        if (direction == "Up" && newIndex == -1) newIndex = 0;
        if (direction == "Down" && newIndex >= count) {
            sortContainer.appendChild(this);
            sortContainer.onAction();
            return;
        }
        sortContainer.insertBefore(this, sortContainer.childNodes[newIndex]);
        sortContainer.onAction();
    }

    return sortItem;
}