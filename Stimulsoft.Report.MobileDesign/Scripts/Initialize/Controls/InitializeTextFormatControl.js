﻿
StiMobileDesigner.prototype.TextFormatControl = function (name) {
    var textFormatControl = document.createElement("div");
    textFormatControl.jsObject = this;
    textFormatControl.name = name;
    textFormatControl.key = null;
    textFormatControl.isEnabled = true;
    this.options.controls[name] = textFormatControl;

    //Button
    textFormatControl.button = this.BigButton(name + "Button", null, " ", " ",
        [this.loc.HelpDesigner.TextFormat, this.HelpLinks["textformat"]], true, this.GetStyles("StandartBigButton"));
    textFormatControl.button.textFormatControl = textFormatControl;
    textFormatControl.appendChild(textFormatControl.button);

    textFormatControl.button.action = function () {
        this.jsObject.options.menus[this.textFormatControl.name + "Menu"].changeVisibleState(!this.jsObject.options.menus[this.textFormatControl.name + "Menu"].visible);
    }

    //Menu
    textFormatControl.menu = this.VerticalMenu(name + "Menu", textFormatControl.button, "Down", this.GetTextFormatItems(true), this.GetStyles("MenuMiddleItem"));
    textFormatControl.menu.textFormatControl = textFormatControl;

    textFormatControl.menu.action = function (menuItem) {
        menuItem.setSelected(true);
        this.changeVisibleState(false);
        this.textFormatControl.setKey(menuItem.key);
        this.textFormatControl.action();
    }

    textFormatControl.menu.onshow = function () {
        if (this.items[this.textFormatControl.key])
            this.items[this.textFormatControl.key].setSelected(true);
        else {
            if (this.selectedItem) {
                this.selectedItem.setSelected(false);
                this.selectedItem = null;
            }
        }
    }

    //Override 
    textFormatControl.setKey = function (key) {
        this.key = key;
        this.button.caption.innerHTML = key == "StiCustomFormatService" ? this.jsObject.loc.FormFormatEditor.Custom : "";
        this.button.innerTable.style.marginTop = key == "StiEmptyValue" ? "6px" : "0px";
        this.button.arrow.style.marginTop = key == "StiEmptyValue" ? "5px" : "0px";
        this.button.image.style.visibility = key == "StiEmptyValue" ? "hidden" : "visible";
        this.button.cellImage.style.border = key == "StiEmptyValue" ? "1px dashed rgb(180, 180, 180)" : "0px";
        this.button.cellImage.style.display = key == "StiCustomFormatService" ? "none" : "";
        this.button.caption.style.paddingTop = key == "StiCustomFormatService" ? "15px" : "0px";

        if (this.menu.items[key]) {
            this.button.caption.innerHTML = this.menu.items[key].caption.innerHTML;
            this.button.image.src = this.jsObject.options.images[this.menu.items[key].imageName];
        }
    };

    textFormatControl.setEnabled = function (state) {
        this.isEnabled = state;
        if (this.button.image) this.button.image.style.opacity = state ? "1" : "0.3";
        this.button.setEnabled(state);
    }

    textFormatControl.action = function () { };

    return textFormatControl;
}