﻿
StiMobileDesigner.prototype.TabbedPane = function (name, tabsArray, stylesTabs) {
    var tabbedPane = document.createElement("div");
    tabbedPane.className = "stiDesignerTabbedPane";
    tabbedPane.jsObject = this;
    tabbedPane.name = name;
    tabbedPane.tabs = {};
    tabbedPane.tabsPanels = {};
    tabbedPane.selectedTab = null;
    this.options.controls[name] = tabbedPane;
    var tabsCellClass = "stiDesignerTabsCell stiDesignerClearAllStyles";

    //Tabs
    var tabsPanel = document.createElement("div");
    tabsPanel.className = "stiDesignerTabsPanel";
    tabbedPane.appendChild(tabsPanel);
    tabbedPane.tabsPanel = tabsPanel;

    var tabsPanelTable = this.CreateHTMLTable();
    tabsPanel.appendChild(tabsPanelTable);

    var tabbedPaneContainer = document.createElement("div");
    tabbedPaneContainer.className = "stiDesignerTabbedPaneContainer";
    tabbedPane.container = tabbedPaneContainer;
    tabbedPane.appendChild(tabbedPaneContainer);

    for (var index in tabsArray) {
        var tab = this.Tab(tabbedPane, tabsArray[index].name, tabsArray[index].caption, stylesTabs);
        tabbedPane.tabs[tabsArray[index].name] = tab;
        var cell = tabsPanelTable.addCell(tab);
        cell.className = tabsCellClass;
        cell.style.width = "1px";

        if (index != tabsArray.length - 1) {
            var sep = document.createElement("div");
            sep.className = "stiDesignerTabbedPaneSeparator";
            var cellSep = tabsPanelTable.addCell(sep);
            cellSep.className = tabsCellClass;
            cellSep.style.width = "1px";
        }

        var tabPanel = document.createElement("div");
        tabPanel.className = "stiDesignerTabbedPanePanel";
        tabPanel.name = tabsArray[index].name;
        tabbedPane.tabsPanels[tabPanel.name] = tabPanel;
        tabbedPaneContainer.appendChild(tabPanel);

        tabPanel.onshow = function () { };
    }
    tabsPanelTable.addCell().className = tabsCellClass;

    tabbedPane.showTabPanel = function (name) {
        for (var namePanel in this.tabsPanels)
            if (namePanel == name) {
                this.tabsPanels[namePanel].style.display = "";
                this.tabs[name].setSelected(true);
                this.selectedTab = this.tabs[name];
                this.tabsPanels[namePanel].onshow();
            }
            else {
                this.tabsPanels[namePanel].style.display = "none";
            }
    }

    tabbedPane.showTabPanel(tabsArray[0].name);

    return tabbedPane;
}

StiMobileDesigner.prototype.Tab = function (tabbedPane, name, caption, styles) {
    if (caption != null) caption = caption.substr(0, 1).toUpperCase() + caption.substr(1);
    var tab = this.SmallButton(tabbedPane.name + name, tabbedPane.name, caption, null, null, null, styles);
    tab.tabbedPane = tabbedPane;
    tab.panelName = name;
    tab.removeChild(tab.innerTable);
    tab.header = document.createElement("div");
    tab.appendChild(tab.header);
    tab.appendChild(tab.innerTable);
    tab.innerTable.style.margin = "2px 6px 7px 6px";
    tab.header.style.height = "5px";

    tab.action = function () { this.tabbedPane.showTabPanel(this.panelName); }

    //Override
    tab.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    tab.onmouseenter = function () {
        if (!this.isEnabled || this.isSelected || this.jsObject.options.isTouchClick) return;
        this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = true;
    }

    tab.onmouseleave = function () {
        if (!this.isEnabled || this.isSelected) return;
        this.className = this.styles["default"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = false;
    }

    tab.setSelected = function (state) {
        if (this.groupName && state)
            for (var name in this.jsObject.options.buttons) {
                if (this.groupName == this.jsObject.options.buttons[name].groupName) {
                    this.jsObject.options.buttons[name].setSelected(false);
                }
            }

        this.isSelected = state;
        this.parentElement.className = state ? "stiDesignerTabsSelectedCell" : "stiDesignerTabsCell";
        this.parentElement.className += " stiDesignerClearAllStyles";
        this.className = (state ? this.styles["selected"] : (this.isEnabled ? this.styles["default"] : this.styles["disabled"])) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.header.className = state ? "stiDesignerStandartTabHeader" : "";
    }

    return tab;
}