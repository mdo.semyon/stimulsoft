﻿
StiMobileDesigner.prototype.DateControlWithCheckBox = function (name, width, toolTip) {
    var dateControlWithCheckBox = this.CreateHTMLTable();
    dateControlWithCheckBox.key = new Date();
    dateControlWithCheckBox.isChecked = true;
    dateControlWithCheckBox.isEnabled = true;

    dateControlWithCheckBox.dateControl = this.DateControl(name, width, toolTip);
    dateControlWithCheckBox.dateControl.parentControl = dateControlWithCheckBox;
    dateControlWithCheckBox.addCell(dateControlWithCheckBox.dateControl);
    dateControlWithCheckBox.dateControl.action = function () { this.parentControl.action(); }
    dateControlWithCheckBox.dateControl.setKey = function (key) {
        this.key = key;
        this.parentControl.key = key;
        this.textBox.value = key.toLocaleString();
    }

    dateControlWithCheckBox.checkBox = this.CheckBox(name + "CheckBox");
    dateControlWithCheckBox.checkBox.style.marginLeft = "5px";
    dateControlWithCheckBox.checkBox.parentControl = dateControlWithCheckBox;
    dateControlWithCheckBox.addCell(dateControlWithCheckBox.checkBox);
    dateControlWithCheckBox.checkBox.action = function () { this.parentControl.dateControl.setEnabled(this.isChecked); }
    dateControlWithCheckBox.checkBox.setChecked = function (state) {
        this.image.style.visibility = (state) ? "visible" : "hidden";
        this.isChecked = state;
        this.parentControl.isChecked = state;
    }

    dateControlWithCheckBox.setEnabled = function (state) {
        this.checkBox.setEnabled(state);
        this.dateControl.setEnabled(state);
        this.isEnabled = state;
    }

    dateControlWithCheckBox.setKey = function (key) {
        this.key = key;
        this.dateControl.setKey(key);
    }

    dateControlWithCheckBox.setChecked = function (state) {
        this.checkBox.setChecked(state)
        this.isChecked = state;
        this.dateControl.setEnabled(this.isChecked);
    }

    dateControlWithCheckBox.setChecked(true);

    dateControlWithCheckBox.action = function () { }

    return dateControlWithCheckBox;
}

StiMobileDesigner.prototype.DinamicDateControlWithCheckBox = function (name, width, toolTip) {
    var dateControlWithCheckBox = this.DateControlWithCheckBox(name, width, toolTip);

    delete this.options.controls[name];
    delete this.options.controls[name + "CheckBox"];
    delete this.options.buttons[name + "DropDownButton"];

    return dateControlWithCheckBox;
}