﻿
StiMobileDesigner.prototype.ResourceContainer = function (name, width, height) {
    var resourceContainer = document.createElement("div");
    resourceContainer.className = "stiResourceContainerWithBorder";
    resourceContainer.loadedContent = null;
    resourceContainer.haveContent = false;
    resourceContainer.resourceType = null;
    resourceContainer.resourceName = null;
    resourceContainer.jsObject = this;
    this.AddProgressToControl(resourceContainer);
    if (name != null) this.options.controls[name] = resourceContainer;
    if (!width) width = 250;
    if (!height) height = 150;
    resourceContainer.progress.style.marginTop = (height / 2 - 50) + "px";

    var innerTable = this.CreateHTMLTable();
    innerTable.style.width = "100%";
    resourceContainer.appendChild(innerTable);
    
    //Container
    var innerContainer = document.createElement("div");
    innerContainer.style.display = "none";
    innerContainer.className = "stiResourceInnerContainer";
    if (width) innerContainer.style.maxWidth = width + "px";
    if (height) innerContainer.style.maxHeight = height + "px";
    var contentCell = innerTable.addCell(innerContainer);
    contentCell.style.textAlign = "center";
    contentCell.style.width = width + "px";
    contentCell.style.height = height + "px";
    
    var hintText = document.createElement("div");
    hintText.className = "stiDesignerTextContainer stiDragAndDropHintText";
    hintText.innerHTML = this.loc.FormDictionaryDesigner.TextDropFileHere;
    contentCell.appendChild(hintText);

    //Buttons
    var buttonsCell = innerTable.addCell();
    buttonsCell.style.verticalAlign = "top";
    buttonsCell.style.width = "1px";

    var buttonProps = [
        ["openButton", "Open.png", this.loc.MainMenu.menuFileOpen.replace("&", "").replace("...", ""), "4px"],
        ["removeButton", "Remove.png", this.loc.MainMenu.menuEditDelete.replace("&", ""), "0 4px 4px 4px"],
        ["viewButton", "View.png", this.loc.Cloud.ButtonView, "0 4px 4px 4px"],
        ["editButton", "EditButton.png", this.loc.MainMenu.menuEditEdit, "0 4px 4px 4px"]
    ]
    
    for (var i = 0; i < buttonProps.length; i++) {
        var button = this.SmallButton(null, null, null, buttonProps[i][1], buttonProps[i][2], null, this.GetStyles("FormButton"));
        resourceContainer[buttonProps[i][0]] = button;
        button.style.margin = buttonProps[i][3];
        buttonsCell.appendChild(button);
    }
        
    resourceContainer.removeButton.action = function () {
        resourceContainer.clear();
        resourceContainer.action();
    }

    resourceContainer.editButton.action = function () {        
        var jsObject = this.jsObject;
        
        var showTextEditForm = function (text) {
            jsObject.InitializeTextEditorFormOnlyText(function (textEditorOnlyText) {

                textEditorOnlyText.showFunction = function () {
                    this.textArea.value = text;
                }

                textEditorOnlyText.actionFunction = function () {
                    var newContent = Base64.encode(this.textArea.value);
                    if (resourceContainer.loadedContent) {
                        newContent = "data:text/plain;base64," + newContent;
                        resourceContainer.setResource(newContent, resourceContainer.resourceType, resourceContainer.resourceName,
                            resourceContainer.resourceSize, null, resourceContainer.haveContent);
                    }
                    else {
                        jsObject.SendCommandSetResourceText(resourceContainer.resourceName, newContent);
                        resourceContainer.setResource(null, resourceContainer.resourceType, resourceContainer.resourceName,
                            resourceContainer.resourceSize, newContent, resourceContainer.haveContent);
                    }
                }

                textEditorOnlyText.changeVisibleState(true);
            });
        }

        if (resourceContainer.loadedContent) {
            var text = Base64.decode(resourceContainer.loadedContent.substring(resourceContainer.loadedContent.indexOf("base64,") + "base64,".length));
            showTextEditForm(text);
        }
        else {
            resourceContainer.setProgress(true);
            this.jsObject.SendCommandGetResourceText(resourceContainer.resourceName, function (answer) {
                resourceContainer.setProgress(false);
                if (answer.resourceText != null) {
                    showTextEditForm(Base64.decode(answer.resourceText));
                }
            });
        }
    }

    resourceContainer.viewButton.action = function () {
        resourceContainer.setProgress(true);
        var jsObject = this.jsObject;

        this.jsObject.SendCommandGetResourceViewData(resourceContainer.resourceName, resourceContainer.resourceType, resourceContainer.loadedContent,
            function (answer) {
                resourceContainer.setProgress(false);
                if (answer.dataTables != null) {
                    jsObject.InitializeResourceViewDataForm(function (resourceViewDataForm) {
                        resourceViewDataForm.show(answer.dataTables, resourceContainer.resourceName);
                    });
                }
            });
    }

    resourceContainer.removeButton.setEnabled(false);

    var filesMask = ".bmp,.gif,.jpeg,.jpg,.png,.tiff,.ico,.emf,.wmf,.svg,.csv,.dbf,.xls,.xlsx,.json,.xml,.xsd,.ttf,.otf,.woff,.ttc,.eot,.rtf,.mrt,.mrz,.txt";

    resourceContainer.openButton.action = function () {
        if (this.jsObject.options.canOpenFiles) {
            var openDialog = this.jsObject.InitializeOpenDialog("resourceContainerImageDialog", function (evt) {
                var files = evt.target.files;
                if (files[0].size > resourceContainer.jsObject.options.reportResourcesMaximumSize) {
                    var errorMessageForm = resourceContainer.jsObject.options.forms.errorMessageForm || resourceContainer.jsObject.InitializeErrorMessageForm();
                    errorMessageForm.show(resourceContainer.jsObject.loc.Notices.QuotaMaximumFileSizeExceeded + "<br><br>" +
                        resourceContainer.jsObject.loc.PropertyMain.Maximum + ": " +
                        resourceContainer.jsObject.numberWithSpaces(resourceContainer.jsObject.options.reportResourcesMaximumSize));
                    return;
                }

                for (var i = 0, f; f = files[i]; i++) {
                    var reader = new FileReader();
                    reader.jsObject = this.jsObject;

                    reader.onload = (function (theFile) {
                        return function (e) {
                            reader.jsObject.ResetOpenDialogs();
                            var resourceName =  files[0].name.substring(0,  files[0].name.lastIndexOf("."));
                            resourceContainer.setResource(e.target.result, resourceContainer.getResourceTypeByFileName(files[0].name), resourceName, files[0].size, null, true);
                            resourceContainer.action();
                            reader.jsObject.ReturnFocusToDesigner();
                        };
                    })(f);

                    reader.readAsDataURL(f);
                }
            }, filesMask);
            openDialog.action();
        }
    }

    resourceContainer.action = function () { }

    resourceContainer.clear = function () {
        innerContainer.innerHTML = "";
        resourceContainer.loadedContent = null;
        resourceContainer.haveContent = false;
        resourceContainer.openButton.setEnabled(true);
        resourceContainer.removeButton.setEnabled(false);
        resourceContainer.viewButton.setEnabled(false);
        resourceContainer.editButton.setEnabled(false);
        resourceContainer.progress.hide();
        hintText.style.display = "";
        contentCell.style.textAlign = "center";
        contentCell.style.verticalAlign = "middle";
    }

    resourceContainer.setResource = function (loadedContent, type, name, size, contentFromServer, haveContent) {
        resourceContainer.clear();
        innerContainer.style.display = haveContent ? "" : "none";
        innerContainer.style.overflow = "auto";
        innerContainer.style.lineHeight = "2";
        resourceContainer.resourceType = type;
        resourceContainer.resourceName = name;
        resourceContainer.resourceSize = size;
        resourceContainer.loadedContent = loadedContent;
        resourceContainer.contentFromServer = contentFromServer;
        resourceContainer.haveContent = haveContent;

        if (!haveContent) return;
        resourceContainer.removeButton.setEnabled(true);
        resourceContainer.viewButton.setEnabled(this.isDataResourceType(type)/* || type == "Report"*/);
        resourceContainer.editButton.setEnabled(this.isTextResourceType(type));
        hintText.style.display = "none";

        if (type == "Image" && (loadedContent || contentFromServer)) {
            var img = document.createElement("img");

            img.onerror = function () {
                img.style.display = "none";
                if (img.src && img.src.indexOf("data:image/x-wmf;") >= 0) {
                    resourceContainer.jsObject.SendCommandToDesignerServer("ConvertMetaFileToPng", { fileContent: img.src }, function (answer) {
                        if (answer.fileContent) {
                            img.src = answer.fileContent;
                            img.style.display = "";
                        }
                    });
                }
            }

            img.src = loadedContent || contentFromServer;
            img.style.maxWidth = innerContainer.style.maxWidth;
            img.style.maxHeight = innerContainer.style.maxHeight;
            innerContainer.style.overflow = "hidden";
            innerContainer.appendChild(img);            
        }
        else if (type == "Rtf" && (loadedContent || contentFromServer)) {
            var rtfContent = document.createElement("div");
            rtfContent.style.maxWidth = innerContainer.style.maxWidth;
            rtfContent.style.maxHeight = innerContainer.style.maxHeight;
            innerContainer.appendChild(rtfContent);

            if (contentFromServer) {
                rtfContent.innerHTML = Base64.decode(contentFromServer);
            }
            else if (loadedContent) {
                resourceContainer.setProgress(true);
                this.jsObject.SendCommandConvertResourceContent(loadedContent, type, function (answer) {
                    resourceContainer.setProgress(false);
                    rtfContent.innerHTML = Base64.decode(answer.content);
                });
            }
        }
        else if (type == "Txt" && (loadedContent || contentFromServer)) {
            var txtContent = this.jsObject.TextArea(null, width - 10, height - 10);
            txtContent.style.border = "0";
            txtContent.readOnly = true;
            txtContent.style.cursor = "default";
            txtContent.style.background = "transparent";
            innerContainer.appendChild(txtContent);

            if (contentFromServer) {
                txtContent.value = Base64.decode(contentFromServer);
            }
            else if (loadedContent) {
                txtContent.value = Base64.decode(loadedContent.substring(loadedContent.indexOf("base64,") + "base64,".length));
            }
        }
        else if (type == "Report") {
            var reportContent = document.createElement("div");
            reportContent.style.maxWidth = innerContainer.style.maxWidth;
            reportContent.style.maxHeight = innerContainer.style.maxHeight;
            reportContent.style.overflow = "hidden";
            innerContainer.appendChild(reportContent);

            if (contentFromServer) {
                reportContent.innerHTML = Base64.decode(contentFromServer);
            }
            else {
                resourceContainer.setProgress(true);
                this.jsObject.SendCommandConvertResourceContent(loadedContent, type, function (answer) {
                    resourceContainer.setProgress(false);
                    reportContent.innerHTML = Base64.decode(answer.content);
                });
            }
        }
        else {
            var imageName = this.jsObject.options.images["Resources.BigResource" + type + ".png"]
            ? "Resources.BigResource" + type + ".png" : "Resources.BigResource.png";
            var item = this.jsObject.EasyContainerItem(name + ", " + this.jsObject.GetHumanFileSize(size, true), imageName);
            item.style.margin = "8px";
            contentCell.style.textAlign = "left";
            contentCell.style.verticalAlign = "top";
            innerContainer.style.overflow = "hidden";
            innerContainer.appendChild(item);
        }
    }

    resourceContainer.getResourceContentFromServer = function (resourceName) {
        resourceContainer.setProgress(true);
        this.jsObject.SendCommandGetResourceContent(resourceName, function (answer) {
            resourceContainer.setProgress(false);
            resourceContainer.setResource(null, answer.resourceType, answer.resourceName, answer.resourceSize, answer.resourceContent, answer.haveContent);
        });
    }

    var buttonsStates = {};

    resourceContainer.setProgress = function (state) {
        if (state)
            resourceContainer.progress.show();
        else
            resourceContainer.progress.hide();
                
        for (var i = 0; i < buttonProps.length; i++) {
            if (state) buttonsStates[buttonProps[i][0]] = resourceContainer[buttonProps[i][0]].isEnabled;
            resourceContainer[buttonProps[i][0]].setEnabled(state ? false : buttonsStates[buttonProps[i][0]]);
        }
    }

    resourceContainer.getResourceTypeByFileName = function (fileName) {
        fileName = fileName.toLowerCase();

        if (fileName.endsWith(".csv")) return "Csv";
        else if (fileName.endsWith(".dbf")) return "Dbf";
        else if (fileName.endsWith(".xls") || fileName.endsWith(".xlsx")) return "Excel";
        else if (fileName.endsWith(".json")) return "Json";
        else if (fileName.endsWith(".xml")) return "Xml";
        else if (fileName.endsWith(".xsd")) return "Xsd";
        else if (fileName.endsWith(".ttf")) return "FontTtf";
        else if (fileName.endsWith(".otf")) return "FontOtf";
        else if (fileName.endsWith(".woff")) return "FontWoff";
        else if (fileName.endsWith(".ttc")) return "FontTtc";
        else if (fileName.endsWith(".eot")) return "FontEot";
        else if (fileName.endsWith(".rtf")) return "Rtf";
        else if (fileName.endsWith(".txt")) return "Txt";
        else if (fileName.endsWith(".mrt") || fileName.endsWith(".mrz")) return "Report";
        else if (fileName.endsWith(".gif") || fileName.endsWith(".png") || fileName.endsWith(".jpeg") || fileName.endsWith(".jpg") || fileName.endsWith(".wmf") ||
            fileName.endsWith(".bmp") || fileName.endsWith(".tiff") || fileName.endsWith(".ico") || fileName.endsWith(".emf") || fileName.endsWith(".svg")) return "Image";
        else  return null;
    }

    resourceContainer.isDataResourceType = function (resourceType) {
        return (resourceType == "Json" ||
                resourceType == "Csv" ||
                resourceType == "Xml" ||
                resourceType == "Dbf" ||
                resourceType == "Excel");
    }

    resourceContainer.isTextResourceType = function (resourceType) {
        return (resourceType == "Json" ||
                resourceType == "Csv" ||
                resourceType == "Xml" ||
                resourceType == "Xsd" ||
                resourceType == "Txt");
    }

    resourceContainer.isFontResourceType = function (resourceType) {
        return (resourceType == "FontOtf" ||
                resourceType == "FontTtc" ||
                resourceType == "FontTtf");
    }

    this.AddDragAndDropToContainer(resourceContainer, function (files, content) {
        if (files[0].size > resourceContainer.jsObject.options.reportResourcesMaximumSize) {
            var errorMessageForm = resourceContainer.jsObject.options.forms.errorMessageForm || resourceContainer.jsObject.InitializeErrorMessageForm();
            errorMessageForm.show(resourceContainer.jsObject.loc.Notices.QuotaMaximumFileSizeExceeded + "<br><br>" +
                resourceContainer.jsObject.loc.PropertyMain.Maximum + ": " +
                resourceContainer.jsObject.numberWithSpaces(resourceContainer.jsObject.options.reportResourcesMaximumSize));
            return;
        }

        var resourceName =  files[0].name.substring(0,  files[0].name.lastIndexOf("."));
        var fileExt = files[0].name.toLowerCase().substring(files[0].name.lastIndexOf("."));
        if (filesMask.indexOf(fileExt) >= 0) {
            resourceContainer.setResource(content, resourceContainer.getResourceTypeByFileName(files[0].name.toLowerCase()), resourceName, files[0].size, null, true);
            resourceContainer.action();
        }
    });

    return resourceContainer;
}