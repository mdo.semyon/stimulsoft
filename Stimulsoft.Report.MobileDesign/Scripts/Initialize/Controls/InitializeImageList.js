﻿
StiMobileDesigner.prototype.ImageList = function (name, showCaption, showImage, toolTip, items) {
    var imageList = this.CreateHTMLTable();
    imageList.jsObject = this;
    imageList.name = name;
    imageList.key = null;
    imageList.isEnabled = true;
    imageList.items = (items == null) ? {} : items;
    this.options.controls[name] = imageList;

    imageList.button = this.StandartSmallButton(name + "Button", null, showCaption ? " " : null, showImage ? " " : null, toolTip, "Down");
    imageList.button.imageList = imageList;
    imageList.addCell(imageList.button);
    imageList.menu = this.VerticalMenu(name + "Menu", imageList.button, "Down", items, this.GetStyles("MenuStandartItem"));
    imageList.menu.type = "ImageListMenu";
    imageList.menu.imageList = imageList;
    this.options.mainPanel.appendChild(imageList.menu);
    imageList.menu.isDinamic = (items == null);
    if (items != null) imageList.menu.addItems(items);

    imageList.setKey = function (key) {
        this.key = key;
        if (this.button.image != null && key == "StiEmptyValue") {
            this.button.image.src = this.jsObject.options.images["BorderStyleNone.png"];
        }
        for (var itemName in this.items)
            if (key == this.items[itemName].key) {
                if (this.button.caption != null) this.button.caption.innerHTML = this.items[itemName].caption;
                if (this.button.image != null) this.button.image.src = this.jsObject.options.images[this.items[itemName].imageName];
                return;
            }
    }

    imageList.button.action = function () {
        if (!this.imageList.menu.visible) {
            if (this.imageList.menu.isDinamic) this.imageList.menu.addItems(this.imageList.items);
            this.imageList.menu.changeVisibleState(true);
        }
        else
            this.imageList.menu.changeVisibleState(false);
    }

    imageList.menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.imageList.key = menuItem.key;
        if (this.imageList.button.caption != null) this.imageList.button.caption.innerHTML = menuItem.caption.innerHTML;
        if (this.imageList.button.image != null) this.imageList.button.image.src = this.jsObject.options.images[menuItem.imageName];
        this.imageList.action();
    }

    imageList.menu.onshow = function () {
        if (this.imageList.key == null) return;
        for (var itemName in this.items)
            if (this.imageList.key == this.items[itemName].key) {
                this.items[itemName].setSelected(true);
                return;
            }
    }

    imageList.menu.onmousedown = function () {
        if (!this.isTouchStartFlag) this.ontouchstart(true);
    }

    imageList.menu.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.imageListMenuPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    //Override methods
    imageList.setEnabled = function (state) {
        this.isEnabled = state;
        this.button.setEnabled(state);
    }
    imageList.action = function () { }

    return imageList;
}

StiMobileDesigner.prototype.DinamicImageList = function (name, showCaption, showImage, toolTip, items) {
    var imageList = this.ImageList(name, showCaption, showImage, toolTip, items);
    delete this.options.controls[name];
    delete this.options.buttons[name + "Button"];
    delete this.options.menus[name + "Menu"];

    return imageList;
}