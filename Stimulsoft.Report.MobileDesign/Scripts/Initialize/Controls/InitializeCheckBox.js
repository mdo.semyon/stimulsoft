
StiMobileDesigner.prototype.CheckBox = function (name, caption) {
    var checkBox = this.CreateHTMLTable();
    checkBox.jsObject = this;
    checkBox.name = name != null ? name : this.generateKey();
    checkBox.id = this.options.mobileDesigner.id + checkBox.name;
    if (name != null) this.options.controls[name] = checkBox;
    checkBox.isEnabled = true;
    checkBox.isChecked = false;
    checkBox.className = this.options.isTouchDevice ? "stiDesignerCheckBox_Touch" : "stiDesignerCheckBox_Mouse";
    checkBox.className += " stiDesignerClearAllStyles";

    //Image
    checkBox.imageBlock = document.createElement("div");
    checkBox.imageBlock.className = this.options.isTouchDevice ? "stiDesignerCheckBoxImageBlock_Touch" : "stiDesignerCheckBoxImageBlock_Mouse";
    var imageBlockCell = checkBox.addCell(checkBox.imageBlock);
    imageBlockCell.style.padding = "0px";
    imageBlockCell.style.border = "0px";
    if (this.options.isTouchDevice) imageBlockCell.style.padding = "1px 3px 1px 1px";

    checkBox.image = document.createElement("img");
    checkBox.image.src = this.options.images["CheckBox.png"];
    checkBox.image.style.visibility = "hidden";
    var imgTable = this.CreateHTMLTable();
    imgTable.style.width = "100%";
    imgTable.style.height = "100%";
    checkBox.imageBlock.appendChild(imgTable);
    imgTable.addCell(checkBox.image).style.textAlign = "center";

    //Caption
    if (caption != null || typeof (caption) == "undefined") {
        checkBox.captionCell = checkBox.addCell();
        checkBox.captionCell.style.padding = "0px";
        checkBox.captionCell.style.border = "0px";
        if (!this.options.isTouchDevice) checkBox.captionCell.style.padding = "1px 0 0 4px";
        checkBox.captionCell.style.whiteSpace = "nowrap";
        if (caption) checkBox.captionCell.innerHTML = caption;
    }

    checkBox.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    checkBox.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.imageBlock.className = "stiDesignerCheckBoxImageBlockOver" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    checkBox.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.imageBlock.className = "stiDesignerCheckBoxImageBlock" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    checkBox.onclick = function () {
        if (this.isTouchEndFlag || !this.isEnabled ||this.jsObject.options.isTouchClick) return;
        checkBox.setChecked(!checkBox.isChecked);
        checkBox.action();
    }

    checkBox.ontouchend = function () {
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        this.imageBlock.className = this.jsObject.options.isTouchDevice ? "stiDesignerCheckBoxImageBlockOver_Touch" : "stiDesignerCheckBoxImageBlockOver_Mouse";
        setTimeout(function () {
            this_.imageBlock.className = this_.jsObject.options.isTouchDevice ? "stiDesignerCheckBoxImageBlock_Touch" : "stiDesignerCheckBoxImageBlock_Mouse";
            this_.setChecked(!this_.isChecked);
            this_.action();
        }, 150);
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    checkBox.ontouchstart = function () {
        this.jsObject.options.fingerIsMoved = false;
    }

    checkBox.setEnabled = function (state) {
        this.image.style.opacity = state ? "1" : "0.3";
        this.isEnabled = state;
        this.className = (state ? "stiDesignerCheckBox" : "stiDesignerCheckBoxDisabled") +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.className += " stiDesignerClearAllStyles";
        this.imageBlock.className = (state ? "stiDesignerCheckBoxImageBlock" : "stiDesignerCheckBoxImageBlockDisabled") +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    checkBox.setChecked = function (state) {
        this.image.style.visibility = (state) ? "visible" : "hidden";
        this.isChecked = state || false;
    }

    checkBox.action = function () { }

    return checkBox;
}

StiMobileDesigner.prototype.DinamicCheckBox = function (name, caption) {
    var checkBox = this.CheckBox(name, caption);
    delete this.options.controls[name];

    return checkBox;
}