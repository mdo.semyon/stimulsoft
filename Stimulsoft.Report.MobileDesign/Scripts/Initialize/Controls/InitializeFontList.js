﻿
StiMobileDesigner.prototype.FontList = function (name, width, height, cutMenu, toolTip) {
    var fontList = this.DropDownList(name, width, toolTip ? toolTip : this.loc.HelpDesigner.FontName, this.GetFontNamesItems(), true, false, height, cutMenu);
    fontList.isFontList = true;

    return fontList;
}