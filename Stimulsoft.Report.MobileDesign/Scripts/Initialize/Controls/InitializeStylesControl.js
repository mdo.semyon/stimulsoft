﻿
StiMobileDesigner.prototype.StylesControl = function (name) {
    var stylesControl = document.createElement("div");
    stylesControl.jsObject = this;
    stylesControl.name = name;
    stylesControl.key = null;
    stylesControl.isEnabled = true;
    this.options.controls[name] = stylesControl;

    //Button
    var button = this.BigButton(name + "Button", null, " ", true, [this.loc.PropertyMain.Style, this.HelpLinks["style"]], true, this.GetStyles("StandartBigButton"));
    stylesControl.appendChild(button);

    //Override
    button.style.width = "120px";
    button.style.padding = "3px";
    button.innerTable.style.height = this.options.isTouchDevice ? "83px" : "63px";

    button.cellArrow.style.padding = "5px 0 5px 0";
    button.cellImage.style.display = "none";

    button.caption.style.padding = "0px";
    button.caption.style.height = "100%";

    var captionContainer = document.createElement("div");
    captionContainer.maxHeight = this.options.isTouchDevice ? 75 : 60;
    captionContainer.style.maxHeight = captionContainer.maxHeight + "px";
    captionContainer.style.overflow = "hidden";

    var captionInnerContainer = document.createElement("div");
    captionContainer.appendChild(captionInnerContainer);
    captionInnerContainer.innerHTML = button.caption.innerHTML;
    button.caption.innerHTML = "";
    button.caption.appendChild(captionContainer);
    captionInnerContainer.style.position = "relative";
    captionInnerContainer.style.maxWidth = "118px";

    button.setEnabled = function (state) {
        if (this.arrow) this.arrow.style.opacity = state ? "1" : "0.3";
        if (this.caption) this.caption.style.opacity = state ? "1" : "0.3";
        this.isEnabled = state;
        if (!state && !this.isOver) this.isOver = false;
        this.className = (state ? (this.isOver ? this.styles["over"] : this.styles["default"]) : this.styles["disabled"]) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    button.action = function () {
        this.jsObject.options.menus[stylesControl.name + "Menu"].changeVisibleState(!this.jsObject.options.menus[stylesControl.name + "Menu"].visible);
    }

    //Menu
    stylesControl.menu = this.VerticalMenu(name + "Menu", button, "Down", null, this.GetStyles("MenuStandartItem"));

    stylesControl.menu.action = function (menuItem) {
        menuItem.setSelected(true);
        this.changeVisibleState(false);
        stylesControl.setKey(menuItem.key);
        stylesControl.action();
    }

    stylesControl.menu.onshow = function () {
        if (stylesControl.typeComponent == "StiChart" || stylesControl.typeComponent == "StiCrossTab" ||
            stylesControl.typeComponent == "StiGauge" || stylesControl.typeComponent == "StiMap" ||
            stylesControl.typeComponent == "StiTable") return;

        this.addItems(this.jsObject.GetComponentStyleItems());

        for (var itemName in this.items)
            if (this.items[itemName].key == stylesControl.key)
                this.items[itemName].setSelected(true);
    }

    stylesControl.addItemsToMenu = function (stylesContent) {
        while (this.menu.innerContent.childNodes[0]) {
            this.menu.innerContent.removeChild(this.menu.innerContent.childNodes[0]);
        }
        if (!stylesContent) return;
        captionInnerContainer.innerHTML = this.key != "StiEmptyValue" ? this.jsObject.loc.FormConditions.SelectStyle : "";

        if (this.typeComponent == "StiMap") {
            for (var i = 0; i < stylesContent.length; i++) {
                var item = this.jsObject.VerticalMenuItemForMapStyles(this.menu, stylesContent[i]);
                this.menu.innerContent.appendChild(item);

                if (this.key && this.key.name == stylesContent[i].name && this.key.type == stylesContent[i].type) {
                    item.setSelected(true);
                    captionInnerContainer.innerHTML = stylesContent[i].image;
                }
            }
            this.repaint();
        }
        else if (this.typeComponent == "StiGauge") {
            for (var i = 0; i < stylesContent.length; i++) {
                var item = this.jsObject.VerticalMenuItemForGaugeStyles(this.menu, stylesContent[i]);
                this.menu.innerContent.appendChild(item);
                if (this.key && this.key.name == stylesContent[i].name && this.key.type == stylesContent[i].type) {
                    item.setSelected(true);
                    captionInnerContainer.innerHTML = stylesContent[i].image;
                }
            }
            this.repaint();
        }
        else if (this.typeComponent == "StiChart") {
            for (var i = 0; i < stylesContent.length; i++) {
                var item = this.jsObject.VerticalMenuItemForChartStyles(this.menu, stylesContent[i]);
                this.menu.innerContent.appendChild(item);
                if (this.key && this.key.name == stylesContent[i].name && this.key.type == stylesContent[i].type) {
                    item.setSelected(true);
                    captionInnerContainer.innerHTML = stylesContent[i].image;
                }
            }
            this.repaint();
        }
        else if (this.typeComponent == "StiCrossTab") {
            var crossTabStyles = [];
            if (this.jsObject.options.report.stylesCollection) {
                for (var i = 0; i < this.jsObject.options.report.stylesCollection.length; i++) {
                    if (this.jsObject.options.report.stylesCollection[i].type == "StiCrossTabStyle") {
                        crossTabStyles.push({
                            key: {
                                crossTabStyle: this.jsObject.options.report.stylesCollection[i].properties.name
                            },
                            properties: this.jsObject.options.report.stylesCollection[i].properties
                        });
                    }
                }
            }

            for (var i = 0; i < stylesContent.length; i++) {
                crossTabStyles.push({
                    key: {
                        crossTabStyle: stylesContent[i].properties.name,
                        crossTabStyleIndex: i
                    },
                    properties: stylesContent[i].properties
                });
            }

            for (var i = 0; i < crossTabStyles.length; i++) {
                var item = this.jsObject.VerticalMenuItemForCrossTabStyles(this.menu, crossTabStyles[i]);
                this.menu.innerContent.appendChild(item);

                if ((crossTabStyles[i].key.crossTabStyleIndex != null && this.key.crossTabStyleIndex == crossTabStyles[i].key.crossTabStyleIndex) ||
                    (crossTabStyles[i].key.crossTabStyleIndex == null && this.key.crossTabStyle == crossTabStyles[i].key.crossTabStyle)) {
                    item.setSelected(true);
                    captionInnerContainer.innerHTML = item.tableContainer.innerHTML;
                }
            }

            this.repaint();
        }
        else if (this.typeComponent == "StiTable") {
            for (var i = 0; i < stylesContent.length; i++) {
                var item = this.jsObject.VerticalMenuItemForTableStyles(this.menu, stylesContent[i]);
                this.menu.innerContent.appendChild(item);
                if (this.key == stylesContent[i].styleId) {
                    item.setSelected(true);
                    captionInnerContainer.innerHTML = item.tableContainer.innerHTML;
                }
            }

            this.repaint();
        }

        stylesControl.menu.innerContent.style.width = "155px";
    }

    stylesControl.updateItemsAndSetKey = function () {
        var jsObject = this.jsObject;

        //Chart Styles
        if (this.typeComponent == "StiChart") {
            var chartStylesContent = jsObject.options.report.chartStylesContent;
            if (!chartStylesContent) {
                jsObject.SendCommandToDesignerServer("GetChartStylesContent",
                   { componentName: jsObject.options.selectedObject ? jsObject.options.selectedObject.properties.name : null },
                   function (answer) {
                       jsObject.options.report.chartStylesContent = answer.stylesContent;
                       stylesControl.addItemsToMenu(answer.stylesContent);
                   });
                return;
            }
            stylesControl.addItemsToMenu(chartStylesContent);
        }
        //CrossTab Styles
        else if (this.typeComponent == "StiCrossTab") {
            var crossTabStylesContent = jsObject.options.report.crossTabStylesContent;
            if (!crossTabStylesContent) {
                jsObject.SendCommandGetCrossTabStylesContent(function (crossTabStylesContent) {
                    jsObject.options.report.crossTabStylesContent = crossTabStylesContent;
                    stylesControl.addItemsToMenu(crossTabStylesContent);
                });
                return;
            }
            stylesControl.addItemsToMenu(crossTabStylesContent);
        }
            //Table Styles
        else if (this.typeComponent == "StiTable") {
            var tableStylesContent = jsObject.options.report.tableStylesContent;
            if (!tableStylesContent) {
                jsObject.SendCommandGetTableStylesContent(function (tableStylesContent) {
                    jsObject.options.report.tableStylesContent = tableStylesContent;
                    stylesControl.addItemsToMenu(tableStylesContent);
                });
                return;
            }
            stylesControl.addItemsToMenu(tableStylesContent);
        }
        //Gauge Styles
        else if (this.typeComponent == "StiGauge") {
            var gaugeStylesContent = jsObject.options.report.gaugeStylesContent;
            jsObject.SendCommandGetGaugeStylesContent(function (gaugeStylesContent) {
                jsObject.options.report.gaugeStylesContent = gaugeStylesContent;
                stylesControl.addItemsToMenu(gaugeStylesContent);
            });
            return;
        }
            //Map Styles
        else if (this.typeComponent == "StiMap") {
            var mapStylesContent = jsObject.options.report.mapStylesContent;
            jsObject.SendCommandGetMapStylesContent(function (mapStylesContent) {
                jsObject.options.report.mapStylesContent = mapStylesContent;
                stylesControl.addItemsToMenu(mapStylesContent);
            });
            return
        }
    }

    //Override 
    stylesControl.setKey = function (key) {
        this.key = key;
        var commonSelectedObject = this.jsObject.options.selectedObject || this.jsObject.GetCommonObject(this.jsObject.options.selectedObjects);
        this.typeComponent = (commonSelectedObject && commonSelectedObject.typeComponent) ? commonSelectedObject.typeComponent : "Any";

        if (this.jsObject.options.report && (this.typeComponent == "StiChart" || this.typeComponent == "StiCrossTab" ||
            this.typeComponent == "StiGauge" || this.typeComponent == "StiMap" || this.typeComponent == "StiTable")) {
            this.updateItemsAndSetKey();
        }
        else {
            captionInnerContainer.innerHTML = key != "[None]"
                ? (key == "StiEmptyValue" ? "" : key)
                : this.jsObject.loc.FormConditions.SelectStyle;
            this.repaint();
        }
    };

    stylesControl.setEnabled = function (state) {
        this.isEnabled = state;
        button.setEnabled(state);
    }

    stylesControl.repaint = function () {
        var font = "Arial!8!0!0!0!0";
        var brush = "1!transparent";
        var textBrush = "1!0,0,0";
        var border = "default";

        captionInnerContainer.style.top = "0px";
        captionInnerContainer.style.margin = "0px";

        if (this.typeComponent == "StiChart" || this.typeComponent == "StiGauge" || this.typeComponent == "StiCrossTab" || this.typeComponent == "StiTable") {
            this.jsObject.RepaintControlByAttributes(button.caption, font, brush, textBrush, border);
            if (this.key != "[None]" && this.key != "StiEmptyValue") {
                button.caption.style.border = "0px";
                if (this.typeComponent == "StiChart" || this.typeComponent == "StiMap") {
                    captionInnerContainer.style.margin = "-10px -10px -9px -10px";
                }
                else if (this.typeComponent == "StiGauge") {
                    captionInnerContainer.style.margin = "0px 0px 0px -10px";
                }
            }
        }
        else {
            if (this.jsObject.options.report) {
                var stylesCollection = this.jsObject.options.report.stylesCollection;
                for (var i in stylesCollection) {
                    var properties = stylesCollection[i].properties;
                    if (properties.name == this.key) {
                        if (properties["font"] != null && properties["allowUseFont"]) font = properties.font;
                        if (properties["brush"] != null && properties["allowUseBrush"]) brush = properties.brush;
                        if (properties["textBrush"] != null && properties["allowUseTextBrush"]) textBrush = properties.textBrush;
                        if (properties["border"] != null && properties["allowUseBorderSides"]) border = properties.border;
                    }
                }
            }
            var captionInnerContainerHeight = parseInt(font.split("!")[1]) * 1.33;
            captionInnerContainer.style.top = (captionInnerContainerHeight > captionContainer.maxHeight ? ((captionInnerContainerHeight - captionContainer.maxHeight) / 2 * -1) : 0) + "px";
            this.jsObject.RepaintControlByAttributes(button.caption, font, brush, textBrush, border);
        }
    }

    stylesControl.action = function () { };

    return stylesControl;
}