﻿
StiMobileDesigner.prototype.TreeItem = function (caption, imageName, itemObject, tree, showCheckBox, id) {
    var treeItem = this.CreateHTMLTable();
    treeItem.id = id || this.newGuid().replace(/-/g, '');
    treeItem.jsObject = this;
    treeItem.isSelected = false;
    treeItem.isOpening = false;
    treeItem.isChecked = false;
    treeItem.itemObject = itemObject;
    treeItem.tree = tree;
    tree.items[treeItem.id] = treeItem;
    treeItem.childs = {};
    treeItem.parent = null;

    treeItem.addChild = function (childItem) {
        this.childsContainer.appendChild(childItem);
        childItem.parent = this;
        this.childs[childItem.id] = childItem;
        this.iconOpening.style.visibility = "visible";
        this.tree.onAddItem(childItem);

        return childItem;
    }

    treeItem.remove = function () {
        if (this.parent) {
            var nextItem = this.nextSibling;
            if (!nextItem) nextItem = this.previousSibling;
            if (!nextItem) nextItem = this.parent;

            this.parent.childsContainer.removeChild(this);
            delete this.parent.childs[this.id];
            delete this.tree.items[this.id];
            for (var key in this.childs)
                this.childs[key].remove();
            this.parent.isOpening = this.jsObject.GetCountObjects(this.parent.childs) > 0;
            this.parent.iconOpening.style.visibility = this.parent.isOpening ? "visible" : "hidden";
            this.tree.onRemoveItem(this);
            if (nextItem) nextItem.setSelected();
        }
    }

    treeItem.move = function (direction) {
        if (this.parent) {
            var index = this.getIndex();
            this.parent.childsContainer.removeChild(this);
            var count = this.parent.childsContainer.childNodes.length;
            var newIndex = direction == "Up" ? index - 1 : index + 1;
            if (direction == "Up" && newIndex == -1) newIndex = 0;
            if (direction == "Down" && newIndex >= count) {
                this.parent.childsContainer.appendChild(this);
                return;
            }
            this.parent.childsContainer.insertBefore(this, this.parent.childsContainer.childNodes[newIndex]);
        }
    }

    treeItem.getIndex = function () {
        if (this.parent) {
            var index = 0;
            for (i = 0; i < this.parent.childsContainer.childNodes.length; i++) {
                if (this == this.parent.childsContainer.childNodes[i]) {
                    return i;
                }
            }
        }
        return -1;
    }

    treeItem.getCountElementsInCurrent = function () {
        return this.parent ? this.parent.childsContainer.childNodes.length : 0;
    }

    treeItem.removeAllChilds = function () {        
        for (var i in this.childs) {
            this.childs[i].remove();
        }
    }

    treeItem.setSelected = function () {
        if (this.tree.selectedItem) {
            this.tree.selectedItem.button.className = this.jsObject.options.isTouchDevice ? "stiDesignerTreeItemButton_Touch" : "stiDesignerTreeItemButton_Mouse";
            this.tree.selectedItem.isSelected = false;
        }
        this.button.className = this.jsObject.options.isTouchDevice ? "stiDesignerTreeItemButtonSelected_Touch" : "stiDesignerTreeItemButtonSelected_Mouse";
        this.tree.selectedItem = this;
        this.isSelected = true;
        this.tree.onSelectedItem(this);
    }

    treeItem.openTree = function () {
        var item = this.parent;
        while (item != null) {
            item.isOpening = true;
            item.childsRow.style.display = "";
            item.iconOpening.src = this.jsObject.options.images[this.jsObject.options.isTouchDevice ? "IconCloseItemBig.png" : "IconCloseItem.png"];
            item = item.parent;
        }
    }

    treeItem.setOpening = function (state) {
        if (this.buildChildsNotCompleted && state) this.completeBuildTree(true);
        this.isOpening = state;
        this.childsRow.style.display = state ? "" : "none";
        var imageType = state ? "Close" : "Open";
        this.iconOpening.src = this.jsObject.options.images[this.jsObject.options.isTouchDevice ? "Icon" + imageType + "ItemBig.png" : "Icon" + imageType + "Item.png"];
    }

    //Opening icon
    treeItem.iconOpening = document.createElement("img");
    treeItem.iconOpening.jsObject = this;
    treeItem.iconOpening.treeItem = treeItem;
    treeItem.addCell(treeItem.iconOpening).style.width = "1px";
    treeItem.iconOpening.src = this.options.images[this.options.isTouchDevice ? "IconOpenItemBig.png" : "IconOpenItem.png"];
    treeItem.iconOpening.className = this.options.isTouchDevice ? "stiDesignerTreeItemIconOpening_Touch" : "stiDesignerTreeItemIconOpening_Mouse";
    treeItem.iconOpening.style.visibility = "hidden";

    treeItem.iconOpening.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.action();
    }

    treeItem.iconOpening.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        this.action();
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    treeItem.iconOpening.action = function () {
        if (this.treeItem.tree.isDisable) return;
        this.treeItem.isOpening = !this.treeItem.isOpening;
        this.treeItem.childsRow.style.display = this.treeItem.isOpening ? "" : "none";
        var imgName = this.treeItem.isOpening ? "IconCloseItem.png" : "IconOpenItem.png";
        if (this.jsObject.options.isTouchDevice) imgName = imgName.replace(".png", "Big.png");
        this.treeItem.iconOpening.src = this.jsObject.options.images[imgName];
        this.treeItem.setSelected();
    }

    //Button
    treeItem.button = this.CreateHTMLTable();
    treeItem.addCell(treeItem.button);
    treeItem.button.treeItem = treeItem;
    treeItem.button.jsObject = this;
    treeItem.button.isSelected = false;
    treeItem.button.className = this.options.isTouchDevice ? "stiDesignerTreeItemButton_Touch" : "stiDesignerTreeItemButton_Mouse";

    treeItem.getAllChilds = function (childsArray) {
        if (!childsArray) childsArray = [];
        for (var i in this.childs) {
            childsArray.push(this.childs[i]);
            this.childs[i].getAllChilds(childsArray);
        }

        return childsArray;
    }

    treeItem.getChildByName = function (name, useOtherNameProperties) {
        for (var i in this.childs)
            if (this.childs[i].itemObject.name == name || (useOtherNameProperties && (this.childs[i].itemObject.correctName == name || this.childs[i].itemObject.nameInSource == name)))
                return this.childs[i];

        return false;
    }

    treeItem.getFullName = function (useNameInSourcesInRelation) {
        var currItem = this;
        var fullName = "";
        while (currItem.parent != null) {
            if (fullName != "") fullName = "." + fullName;
            if (currItem.itemObject.typeItem == "Relation" && useNameInSourcesInRelation && currItem.itemObject.nameInSource) {
                fullName = currItem.itemObject.nameInSource + fullName;
            }
            else {
                fullName = (currItem.itemObject.correctName || currItem.itemObject.name) + fullName;
            }
            currItem = currItem.parent;
        }
        return fullName;
    }

    //Checkbox
    if (showCheckBox) {
        treeItem.checkBox = this.CheckBox(null);
        treeItem.button.addCell(treeItem.checkBox).style.width = "1px";
        treeItem.checkBox.treeItem = treeItem;
        treeItem.checkBox.style.marginLeft = "2px";

        treeItem.setChecked = function (state) {
            this.isChecked = state;
            this.checkBox.setChecked(state);
        }

        treeItem.checkBox.action = function () {
            if (this.treeItem.tree.isDisable) return;
            this.treeItem.setChecked(this.isChecked);
            var childs = this.treeItem.getAllChilds();
            for (var i in childs) {
                childs[i].setChecked(this.isChecked);
            }
            if (this.treeItem.parent) {
                if (this.isChecked) {
                    this.treeItem.parent.setChecked(true);
                }
            }
            if (this.treeItem.tree["onChecked"]) this.treeItem.tree.onChecked(this.treeItem);
        }
    }

    if (imageName != null) {
        treeItem.button.image = document.createElement("img");
        treeItem.button.imageCell = treeItem.button.addCell(treeItem.button.image);
        treeItem.button.imageCell.style.fontSize = "0px"
        if (this.options.images[imageName]) treeItem.button.image.src = this.options.images[imageName];
        treeItem.button.image.className = this.options.isTouchDevice ? "stiDesignerTreeItemButtonImage_Touch" : "stiDesignerTreeItemButtonImage_Mouse";
    }

    if (caption != null || typeof (caption) == "undefined") {
        treeItem.button.captionCell = treeItem.button.addCell();
        treeItem.button.captionCell.className = this.options.isTouchDevice ? "stiDesignerTreeItemButtonCaption_Touch" : "stiDesignerTreeItemButtonCaption_Mouse";
        if (caption) treeItem.button.captionCell.innerHTML = caption;
    }

    treeItem.button.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.action();
    }

    treeItem.button.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        this.action();
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    treeItem.button.action = function () {
        if (this.treeItem.tree.isDisable) return;
        if (this.treeItem.tree.selectedItem != this.treeItem) {
            this.treeItem.setSelected();
            this.treeItem.tree.onActionItem(this.treeItem);
        }
        else
            this.treeItem.setSelected();
    }

    //ChildsRow
    treeItem.childsRow = treeItem.addRow();
    treeItem.childsRow.style.display = "none";

    //Empty Cell
    treeItem.addCellInLastRow();

    //Childs Container
    treeItem.childsContainer = treeItem.addCellInLastRow();
    treeItem.childsContainer.style.textAlign = "left";

    return treeItem;
}

StiMobileDesigner.prototype.Tree = function (width, height) {
    var tree = document.createElement("div");
    tree.jsObject = this;
    tree.items = {};
    tree.selectedItem = null;
    if (width) tree.style.width = width + "px";
    if (height) tree.style.height = height + "px";

    //Events
    tree.onSelectedItem = function (item) { };
    tree.onRemoveItem = function (item) { };
    tree.onAddItem = function (item) { };
    tree.onChecked = function (item) { };
    tree.onActionItem = function (item) { };
    tree.action = function () { };

    tree.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        this.items = {};
        this.selectedItem = null;
    }

    tree.autoscroll = function () {        
        if (this.selectedItem && this.offsetHeight > 0) {            
            var scrollContainer = this.style.overflow == "auto" ? this : this.parentNode;
            
            if (scrollContainer) {
                scrollContainer.scrollTop = 0;
                var yPos = this.jsObject.FindPosY(this.selectedItem, null, null, scrollContainer);
                if (yPos + 100 > scrollContainer.offsetHeight) scrollContainer.scrollTop = yPos - scrollContainer.offsetHeight + 100;
            }
        }
    }

    return tree;
}

StiMobileDesigner.prototype.TreeItemWithCheckBox = function (caption, imageName, itemObject, tree) {
    var item = this.TreeItem(caption, imageName, itemObject, tree, true);
    item.style.margin = "2px 0 2px 0";

    if (item.checkBox) {
        item.checkBox.action = function () {
            if (this.treeItem.tree.isDisable) return;
            this.treeItem.setChecked(this.isChecked);
            var childs = this.treeItem.getAllChilds();
            for (var i in childs) {
                childs[i].setChecked(this.isChecked);
            }

            var setCheckedParent = function (treeItem) {
                if (treeItem.parent) {
                    if (treeItem.isChecked) {
                        treeItem.parent.setChecked(true);
                        setCheckedParent(treeItem.parent);
                    }
                }
            }

            setCheckedParent(this.treeItem);
        }
    }

    return item;
}
   