﻿
StiMobileDesigner.prototype.TextBoxEnumerator = function (name, width, toolTip, readOnly, maxValue, minValue) {
    var textBoxEnumerator = this.CreateHTMLTable();
    textBoxEnumerator.jsObject = this;
    if (name != null) this.options.controls[name] = textBoxEnumerator;
    textBoxEnumerator.name = name != null ? name : this.generateKey();
    textBoxEnumerator.readOnly = readOnly;
    textBoxEnumerator.isEnabled = true;
    textBoxEnumerator.isSelected = false;
    textBoxEnumerator.isOver = false;
    textBoxEnumerator.isFocused = false;
    textBoxEnumerator.maxValue = maxValue;
    textBoxEnumerator.minValue = minValue;
    if (toolTip) textBoxEnumerator.setAttribute("title", toolTip);
    textBoxEnumerator.className = this.options.isTouchDevice ? "stiDesignerTextBoxEnumerator_Touch" : "stiDesignerTextBoxEnumerator_Mouse";

    //TextBox
    textBoxEnumerator.textBox = document.createElement("input");
    textBoxEnumerator.textBox.jsObject = this;
    textBoxEnumerator.textBox.style.width = (this.options.isTouchDevice ? width - 25 : width - 17) + "px";
    textBoxEnumerator.textBox.textBoxEnumerator = textBoxEnumerator;
    textBoxEnumerator.textBox.className = "stiDesignerTextBoxEnumerator_TextBox" + (this.options.isTouchDevice ? "_Touch" : "_Mouse") + " stiDesignerTextBoxEnumerator_TextBoxDefault";
    var textboxCell = textBoxEnumerator.addCell(textBoxEnumerator.textBox);
    textboxCell.style.fontSize = "0";
    textboxCell.style.lineHeight = "0";
    textBoxEnumerator.textBox.readOnly = readOnly;
    textBoxEnumerator.textBox.style.cursor = readOnly ? "default" : "text";
    textBoxEnumerator.textBox.onfocus = function () { this.textBoxEnumerator.isFocused = true; this.textBoxEnumerator.setSelected(true); }
    textBoxEnumerator.textBox.onblur = function () {
        this.textBoxEnumerator.isFocused = false;
        this.textBoxEnumerator.setSelected(false);
        this.setCorrectValue(this.jsObject.StrToInt(this.value));
        this.textBoxEnumerator.action();
    }
    textBoxEnumerator.textBox.onkeypress = function (event) {
        if (this.textBoxEnumerator.readOnly) return false;
        if (event && event.keyCode == 13) {
            this.setCorrectValue(this.jsObject.StrToInt(this.value));
            this.textBoxEnumerator.action();
            return false;
        }
    }
    textBoxEnumerator.textBox.setCorrectValue = function (value) {
        if (this.textBoxEnumerator.maxValue != null && value > this.textBoxEnumerator.maxValue) { this.value = this.textBoxEnumerator.maxValue; return; }
        if (this.textBoxEnumerator.minValue != null && value < this.textBoxEnumerator.minValue) { this.value = this.textBoxEnumerator.minValue; return; }
        this.value = value;
    }

    textBoxEnumerator.textBox.setEnabled = function (state) {
        this.isEnabled = state;
        this.disabled = !state;
        this.className = "stiDesignerTextBoxEnumerator_TextBox" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse") +
            " stiDesignerTextBoxEnumerator_TextBox" + (state ? "Default" : "Disabled");
    }

    var buttonsCell = textBoxEnumerator.addCell();

    //Button Up
    textBoxEnumerator.buttonUp = this.TextBoxEnumeratorButton("ButtonArrowUpBlack.png");
    textBoxEnumerator.buttonUp.textBox = textBoxEnumerator.textBox;
    textBoxEnumerator.buttonUp.style.marginBottom = "1px";
    buttonsCell.appendChild(textBoxEnumerator.buttonUp);
    textBoxEnumerator.buttonUp.action = function () {
        var value = this.jsObject.StrToInt(this.textBox.value);
        value++;
        this.textBox.setCorrectValue(value);
        textBoxEnumerator.action();
    }

    //Button Down
    textBoxEnumerator.buttonDown = this.TextBoxEnumeratorButton("ButtonArrowDown.png");
    textBoxEnumerator.buttonDown.textBox = textBoxEnumerator.textBox;
    buttonsCell.appendChild(textBoxEnumerator.buttonDown);
    textBoxEnumerator.buttonDown.action = function () {
        var value = this.jsObject.StrToInt(this.textBox.value);
        value--;
        this.textBox.setCorrectValue(value);
        textBoxEnumerator.action();
    }

    //Methods
    textBoxEnumerator.setEnabled = function (state) {
        this.isEnabled = state;
        this.textBox.setEnabled(state);
        this.buttonUp.setEnabled(state);
        this.buttonDown.setEnabled(state);
        this.className = (state ? "stiDesignerTextBoxEnumerator" : "stiDesignerTextBoxEnumeratorDisabled") +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    textBoxEnumerator.onmouseover = function () {
        if (this.jsObject.options.isTouchDevice || !this.isEnabled) return;
        this.isOver = true;
        if (!this.isSelected && !this.isFocused) this.className = "stiDesignerTextBoxEnumeratorOver_Mouse";
    }

    textBoxEnumerator.onmouseout = function () {
        if (this.jsObject.options.isTouchDevice || !this.isEnabled) return;
        this.isOver = false;
        if (!this.isSelected && !this.isFocused) this.className = "stiDesignerTextBoxEnumerator_Mouse";
    }

    textBoxEnumerator.setSelected = function (state) {
        this.isSelected = state;
        this.className = (state ? "stiDesignerTextBoxEnumeratorOver" :
            (this.isEnabled ? (this.isOver ? "stiDesignerTextBoxEnumeratorOver" : "stiDesignerTextBoxEnumerator") : "stiDesignerTextBoxEnumeratorDisabled")) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    textBoxEnumerator.setValue = function (value) {
        this.textBox.setCorrectValue(typeof (value) == "string" ? this.jsObject.StrToInt(value) : value);
    }

    textBoxEnumerator.action = function () { };

    return textBoxEnumerator;
}

StiMobileDesigner.prototype.TextBoxEnumeratorButton = function (imageName) {
    var button = this.SmallButton(null, null, null, imageName, null, null, this.GetStyles("TextBoxEnumeratorButton"));

    //Override
    button.imageCell.removeChild(button.image);
    button.image = document.createElement("div");
    button.image.style.background = "url(" + this.options.images[imageName] + ")";
    button.image.style.width = "5px";
    button.image.style.height = "3px";
    button.image.style.margin = "0 3px 0 3px";
    button.imageCell.appendChild(button.image);

    return button;
}