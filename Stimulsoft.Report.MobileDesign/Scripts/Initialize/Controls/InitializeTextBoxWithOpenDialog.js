﻿
StiMobileDesigner.prototype.TextBoxWithOpenDialog = function (name, width, filesMask) {
    var textBoxParent = document.createElement("div");
    textBoxParent.jsObject = this;
    if (name) this.options.controls[name] = textBoxParent;

    var openButton = this.StandartSmallButton(null, null, null, "Open.png");
    textBoxParent.openButton = openButton;
    textBoxParent.appendChild(openButton);
    var buttonSize = this.options.isTouchDevice ? 26 : 21;
    openButton.style.width = buttonSize + "px";
    openButton.style.height = buttonSize + "px";
    openButton.style.margin = "1px 0 0 " + (width - buttonSize + 5) + "px";
    openButton.style.position = "absolute";
    
    var textBox = this.TextBox(null, width);
    textBoxParent.appendChild(textBox);
    textBoxParent.textBox = textBox;
    textBoxParent.filesMask = filesMask;
    
    openButton.action = function () {
        if (this.jsObject.options.canOpenFiles) {
            var openDialog = this.jsObject.InitializeOpenDialog(name + "OpenDialog", function (evt) {
                textBox.value = evt.target.value;
                textBox.action();
            }, textBoxParent.filesMask);
            openDialog.action();
        }
    }

    textBoxParent.setValue = function (value) {
        textBox.value = value;
    }

    textBoxParent.getValue = function () {
        return textBox.value;
    }

    textBoxParent.action = function () { }
    textBox.action = function () { textBoxParent.action(); }

    return textBoxParent;
}