﻿
StiMobileDesigner.prototype.MarginsControl = function (name, width, height) {
    var control = this.CreateHTMLTable();
    control.className = "stiDesignerMarginsControl stiDesignerMarginsControlDefault";
    control.isEnabled = true;
    control.isOver = false;
    var arrows = ["Left", "Right", "Top", "Bottom"];
    if (!width) width = 150;
    if (name) {
        this.options.controls[name] = control;
        control.name = name;
    }

    for (var i = 0; i < arrows.length; i++) {
        var textBox = this.TextBox(null, width / 4 - 30, height);
        textBox.style.border = "0px";
        control["value" + arrows[i]] = textBox;
        control.addCell(textBox);
        var img = document.createElement("img");
        img.style.margin = "0 4px 1px 0";
        img.src = this.options.images["Arrows.SmallArrow" + arrows[i] + ".png"];
        control["cell" + arrows[i]] = control.addCell(img);
        if (i < 3) control["cell" + arrows[i]].className = "stiDesignerMarginsControlInnerCell";
        control["cell" + arrows[i]].style.background = "#ffffff";
        textBox.action = function () {
            this.value = Math.abs(this.jsObject.StrToDouble(this.value));
            control.action();
        }
    }

    control.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    control.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        this.className = "stiDesignerMarginsControl stiDesignerMarginsControlOver";
        for (var i = 0; i < arrows.length - 1; i++) {
            control["cell" + arrows[i]].className = "stiDesignerMarginsControlInnerCellOver";
        }
    }

    control.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.isOver = false;
        this.className = "stiDesignerMarginsControl stiDesignerMarginsControlDefault";
        for (var i = 0; i < arrows.length - 1; i++) {
            control["cell" + arrows[i]].className = "stiDesignerMarginsControlInnerCell";
        }
    }

    control.setValue = function (value, isPageMargins) {
        var marginsArray = value.split(isPageMargins ? "!" : ";");
        control.valueLeft.value = !value ? "" : (marginsArray.length != 4 ? 0 : marginsArray[0]);
        control.valueRight.value = !value ? "" : (marginsArray.length != 4 ? 0 : marginsArray[isPageMargins ? 2 : 1]);
        control.valueTop.value = !value ? "" : (marginsArray.length != 4 ? 0 : marginsArray[isPageMargins ? 1 : 2]);
        control.valueBottom.value = !value ? "" : (marginsArray.length != 4 ? 0 : marginsArray[3]);
    }

    control.getValue = function (isPageMargins) {
        if (isPageMargins) {
            return control.valueLeft.value + "!" + control.valueTop.value + "!" + control.valueRight.value + "!" + control.valueBottom.value;
        }
        else {
            return control.valueLeft.value + ";" + control.valueRight.value + ";" + control.valueTop.value + ";" + control.valueBottom.value;
        }
    }

    control.setEnabled = function (state) {
        this.isEnabled = state;
        for (var i = 0; i < arrows.length; i++) {
            control["value" + arrows[i]].disabled = !state;
        }
    }

    control.action = function () { }

    return control;
}

//TextBox
StiMobileDesigner.prototype.PropertyMarginsControl = function (name, width) {
    return this.MarginsControl(name, width, 18);
}