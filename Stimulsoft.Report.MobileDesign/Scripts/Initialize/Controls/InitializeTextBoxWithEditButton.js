﻿
StiMobileDesigner.prototype.TextBoxWithEditButton = function (name, width, height, readOnly) {
    var textBoxWithButton = this.CreateHTMLTable();
    textBoxWithButton.style.borderCollapse = "separate";
    var buttonWidth = this.options.isTouchDevice ? 23 : 15;
    var textBoxWidth = width - (buttonWidth + 1);
    textBoxWithButton.textBox = this.TextBox(name, textBoxWidth, height);
    textBoxWithButton.textBox.mainControl = textBoxWithButton;
    textBoxWithButton.addCell(textBoxWithButton.textBox).style.fontSize = "0px";

    textBoxWithButton.button = this.StandartSmallButton(name + "Button", null, null, "TextEditButton.png", this.loc.QueryBuilder.Edit, null);
    textBoxWithButton.button.textBox = textBoxWithButton.textBox;

    //Override
    textBoxWithButton.button.innerTable.style.width = "100%";
    textBoxWithButton.button.style.width = buttonWidth + "px";
    if (height) textBoxWithButton.button.style.height = (this.options.isTouchDevice ? height + 5 : height) + "px";
    else textBoxWithButton.button.style.height = this.options.isTouchDevice ? "26px" : "21px";
    var buttonCell = textBoxWithButton.addCell(textBoxWithButton.button);
    buttonCell.className = "stiDesignerTextBoxEditButton";

    if (readOnly) {
        textBoxWithButton.textBox.readOnly = readOnly;
        textBoxWithButton.textBox.style.cursor = "default";
        textBoxWithButton.textBox.onclick = function () { if (!this.jsObject.options.isTouchDevice) this.mainControl.button.onclick(); }
        textBoxWithButton.textBox.ontouchend = function () { this.mainControl.button.ontouchend(); }
    }

    textBoxWithButton.action = function () { };

    textBoxWithButton.textBox.action = function () { textBoxWithButton.action(); }

    textBoxWithButton.setEnabled = function (state) {
        this.textBox.setEnabled(state);
        this.button.setEnabled(state);
        buttonCell.className = state ? "stiDesignerTextBoxEditButton" : "stiDesignerTextBoxEditButtonDisabled";
    }

    return textBoxWithButton;
}

StiMobileDesigner.prototype.DinamicTextBoxWithEditButton = function (name, width, height, readOnly) {
    var textBox = this.TextBoxWithEditButton(name, width, height, readOnly);
    delete this.options.controls[name];
    delete this.options.buttons[name + "Button"];

    return textBox;
}

//Expression Control
StiMobileDesigner.prototype.ExpressionControl = function (name, width, height, readOnly, cutExpression) {
    var expressionControl = this.TextBoxWithEditButton(name, width, height, readOnly);
    expressionControl.cutExpression = cutExpression;
    
    if (readOnly) {
        expressionControl.textBox.readOnly = readOnly;
        expressionControl.textBox.style.cursor = "default";
        expressionControl.textBox.mainControl = expressionControl;

        expressionControl.textBox.onclick = function () {
            if (!this.isTouchEndFlag)
                this.mainControl.button.onclick();
        }

        expressionControl.textBox.ontouchend = function () {
            var this_ = this;
            this.isTouchEndFlag = true;
            clearTimeout(this.isTouchEndTimer);
            this.mainControl.button.ontouchend();
            this.isTouchEndTimer = setTimeout(function () {
                this_.isTouchEndFlag = false;
            }, 1000);
        }
    }

    expressionControl.button.action = function () {
        this.jsObject.InitializeExpressionEditorForm(function (expressionEditorForm) {
            var propertiesPanel = expressionEditorForm.jsObject.options.propertiesPanel;
            expressionEditorForm.propertiesPanelZIndex = propertiesPanel.style.zIndex;
            expressionEditorForm.propertiesPanelIsEnabled = propertiesPanel.isEnabled;
            expressionEditorForm.resultControl = expressionControl;
            expressionEditorForm.changeVisibleState(true);
        });
    }

    return expressionControl;
}

