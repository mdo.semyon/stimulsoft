﻿
StiMobileDesigner.prototype.SmallButton = function (name, groupName, caption, imageName, toolTip, arrow, styles, allwaysEnabled) {
    var button = document.createElement("div");
    button.jsObject = this;
    button.name = name != null ? name : this.generateKey();
    button.id = this.options.mobileDesigner.id + button.name;
    if (name != null) this.options.buttons[name] = button;
    button.groupName = groupName;
    button.styles = styles;
    button.isEnabled = true;
    button.isSelected = false;
    button.isOver = false;
    button.image = null;
    button.caption = null;
    button.captionText = caption;
    button.arrow = null;
    button.allwaysEnabled = allwaysEnabled;
    button.toolTip = toolTip;
    button.className = this.options.isTouchDevice ? styles["default"] + "_Touch" : styles["default"] + "_Mouse";

    var innerTable = this.CreateHTMLTable();
    button.innerTable = innerTable;
    innerTable.style.height = "100%";
    button.appendChild(innerTable);

    if (imageName) {
        button.image = document.createElement("img");
        if (this.options.images[imageName]) button.image.src = this.options.images[imageName];
        button.imageCell = innerTable.addCell(button.image);
        button.imageCell.style.padding = (this.options.isTouchDevice && caption == null) ? "0 7px 0 7px" : "0 3px 0 3px";
        button.imageCell.style.textAlign = "center";
        button.imageCell.style.fontSize = "0px";
    }

    if (caption != null || typeof (caption) == "undefined") {
        button.caption = innerTable.addCell();
        button.caption.style.padding = (arrow ? "0 0 " : "0 5px ") + (imageName ? "0 0" : "0 5px");
        button.caption.style.whiteSpace = "nowrap";
        button.caption.style.textAlign = "left";
        if (caption) button.caption.innerHTML = caption;
    }

    if (arrow) {
        button.arrow = document.createElement("img");
        button.arrow.src = this.options.images["ButtonArrow" + arrow + ".png"];
        button.arrowCell = innerTable.addCell(button.arrow);
        button.arrowCell.style.padding = caption ? "0 5px 0 5px" : (this.options.isTouchDevice ? "0 7px 0 0" : "0 5px 0 2px");
        button.arrowCell.style.fontSize = "0px";
    }

    if (toolTip && typeof (toolTip) != "object") { button.setAttribute("title", toolTip); }

    button.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    button.onmouseenter = function () {
        if (!this.isEnabled || (this["haveMenu"] && this.isSelected) || this.jsObject.options.isTouchClick) return;
        this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = true;
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object")
            this.jsObject.options.toolTip.showWithDelay(
                this.toolTip[0],
                this.toolTip[1],
                this.toolTip.length == 3 ? this.toolTip[2].left : this.jsObject.FindPosX(this, "stiDesignerMainPanel"),
                this.toolTip.length == 3 ? this.toolTip[2].top : this.jsObject.options.toolBar.offsetHeight + this.jsObject.options.workPanel.offsetHeight - 1
            );
    }

    button.onmouseleave = function () {
        this.isOver = false;
        if (!this.isEnabled) return;
        this.className = (this.isSelected ? this.styles["selected"] : this.styles["default"]) + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object") this.jsObject.options.toolTip.hideWithDelay();
    }

    button.onmousedown = function () {
        if (this.isTouchStartFlag || !this.isEnabled) return;
        this.jsObject.options.buttonPressed = this;
    }

    button.onclick = function () {
        if (this.isTouchEndFlag || !this.isEnabled || this.jsObject.options.isTouchClick) return;
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object") this.jsObject.options.toolTip.hide();
        this.action();
    }

    button.ontouchend = function () {
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        this.jsObject.options.buttonsTimer = [this, this.className, setTimeout(function () {
            this_.jsObject.options.buttonsTimer = null;
            this_.className = this_.styles["default"] + "_Touch";
            if (this_.jsObject.options.showTooltips && this_.toolTip && typeof (this_.toolTip) == "object") this_.jsObject.options.toolTip.hide();
            this_.action();
        }, 150)];
        this.className = this.jsObject.options.isTouchDevice ? this.styles["over"] + "_Touch" : this.styles["over"] + "_Mouse";
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    button.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.fingerIsMoved = false;
        this.jsObject.options.buttonPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    button.setEnabled = function (state) {
        if (this.image) this.image.style.opacity = state ? "1" : "0.3";
        if (this.arrow) this.arrow.style.opacity = state ? "1" : "0.3";
        this.isEnabled = state;
        if (!state && !this.isOver) this.isOver = false;
        this.className = (state ? (this.isOver ? this.styles["over"] : (this.isSelected ? this.styles["selected"] : this.styles["default"])) : this.styles["disabled"]) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    button.setSelected = function (state) {
        if (this.groupName && state)
            for (var name in this.jsObject.options.buttons) {
                if (this.groupName == this.jsObject.options.buttons[name].groupName)
                    this.jsObject.options.buttons[name].setSelected(false);
            }
        this.isSelected = state;
        this.className = (state ? this.styles["selected"] : (this.isEnabled ? (this.isOver ? this.styles["over"] : this.styles["default"]) : this.styles["disabled"])) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    button.action = function () { this.jsObject.ExecuteAction(this.name); }

    return button;
}

StiMobileDesigner.prototype.DinamicSmallButton = function (name, groupName, caption, imageName, toolTip, arrow, styles, allwaysEnabled) {
    var smallButton = this.SmallButton(name, groupName, caption, imageName, toolTip, arrow, styles, allwaysEnabled);
    delete this.options.buttons[name];

    return smallButton;
}