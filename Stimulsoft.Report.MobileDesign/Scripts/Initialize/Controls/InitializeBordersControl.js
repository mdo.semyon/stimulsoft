﻿
StiMobileDesigner.prototype.BordersControl = function () {
    var bordersControl = this.CreateHTMLTable();
    bordersControl.isEnabled = true;
    bordersControl.controls = {};
    bordersControl.key = "None";

    var controlProps = [
        ["BorderAll", this.StandartSmallButton(null, null, null, "BorderAll.png"), "4px 1px 4px 2px"],
        ["BorderNone", this.StandartSmallButton(null, null, null, "BorderNone.png"), "4px 2px 4px 1px"],
        ["separator"],
        ["BorderLeft", this.StandartSmallButton(null, null, null, "BorderLeft.png"), "4px 1px 4px 2px"],
        ["BorderTop", this.StandartSmallButton(null, null, null, "BorderTop.png"), "4px 1px 4px 1px"],
        ["BorderRight", this.StandartSmallButton(null, null, null, "BorderRight.png"), "4px 1px 4px 1px"],
        ["BorderBottom", this.StandartSmallButton(null, null, null, "BorderBottom.png"), "4px 2px 4px 1px"]
    ]

    for (var i = 0; i < controlProps.length; i++) {
        if (controlProps[i][0] == "separator") {
            var sep = this.HomePanelSeparator();
            bordersControl.addCell(sep);
            sep.style.margin = "4px 2px 4px 2px";
            continue;
        }

        var control = controlProps[i][1];
        control.propertyName = controlProps[i][0];
        control.style.margin = controlProps[i][2];
        bordersControl.controls[control.propertyName] = control;
        bordersControl.addCell(control);
        control.action = function () {
            var key = "";
            var borders = ["BorderLeft", "BorderRight", "BorderTop", "BorderBottom"];
            if (this.propertyName == "BorderAll" || this.propertyName == "BorderNone") {
                if (!this.isSelected) {
                    bordersControl.controls.BorderAll.setSelected(this.propertyName == "BorderAll");
                    bordersControl.controls.BorderNone.setSelected(this.propertyName == "BorderNone");
                    for (var i = 0; i < borders.length; i++) { bordersControl.controls[borders[i]].setSelected(this.propertyName == "BorderAll"); }
                    key = this.propertyName == "BorderAll" ? "All" : "None";
                }
            }
            else {
                this.setSelected(!this.isSelected);
                var countBorders = 0;
                for (var i = 0; i < borders.length; i++) {
                    if (bordersControl.controls[borders[i]].isSelected) {
                        var side = borders[i].replace("Border", "");
                        if (key != "") key += ", ";
                        key += side;
                        countBorders++;
                    }
                }
                bordersControl.controls.BorderAll.setSelected(countBorders == 4);
                bordersControl.controls.BorderNone.setSelected(countBorders == 0);
                if (countBorders == 4) key = "All";
                if (countBorders == 4) key = "None";
            }

            bordersControl.key = key;
            bordersControl.action();
        }
    }

    bordersControl.action = function () { };

    bordersControl.setEnabled = function (state) {
        for (var i = 0; i < controlProps.length; i++) {
            if (controlProps[i][0] == "separator") continue;
            bordersControl.controls[controlProps[i][0]].setEnabled(state);
        }
    }

    bordersControl.setKey = function (key) {
        bordersControl.key = key;
        bordersControl.controls.BorderAll.setSelected(key == "All");
        bordersControl.controls.BorderNone.setSelected(key == "None");
        bordersControl.controls.BorderLeft.setSelected(key == "All" || key.indexOf("Left") >= 0);
        bordersControl.controls.BorderRight.setSelected(key == "All" || key.indexOf("Right") >= 0);
        bordersControl.controls.BorderTop.setSelected(key == "All" || key.indexOf("Top") >= 0);
        bordersControl.controls.BorderBottom.setSelected(key == "All" || key.indexOf("Bottom") >= 0);
    }

    return bordersControl;
}