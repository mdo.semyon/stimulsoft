﻿
StiMobileDesigner.prototype.BigButton = function (name, groupName, caption, imageName, toolTip, arrow, styles, allwaysEnabled, maxWidth) {
    var button = document.createElement("div");
    button.jsObject = this;
    button.name = name != null ? name : this.generateKey();
    button.id = this.options.mobileDesigner.id + button.name;
    if (name != null) this.options.buttons[name] = button;
    button.groupName = groupName;
    button.styles = styles;
    button.isEnabled = true;
    button.isSelected = false;
    button.isOver = false;
    button.arrow = null;
    button.toolTip = toolTip;
    button.allwaysEnabled = allwaysEnabled;
    button.className = this.options.isTouchDevice ? styles["default"] + "_Touch" : styles["default"] + "_Mouse";

    var innerTable = this.CreateHTMLTable();
    button.innerTable = innerTable;
    innerTable.style.height = "100%";
    innerTable.style.width = "100%";
    button.appendChild(innerTable);

    if (imageName) {
        button.imageName = imageName;
        button.image = document.createElement("img");
        if (this.options.images[imageName]) button.image.src = this.options.images[imageName];
        button.cellImage = innerTable.addCell(button.image);
        button.cellImage.style.padding = this.options.isTouchDevice ? "10px 2px 7px 2px" : "2px 2px 0px 2px";
        button.cellImage.style.textAlign = "center";
    }

    if (caption != null || typeof (caption) == "undefined") {
        button.caption = innerTable.addCellInNextRow();
        button.caption.style.padding = this.options.isTouchDevice ? "0 3px 7px 3px" : "0 3px 2px 3px";
        button.caption.style.textAlign = "center";
        button.caption.style.lineHeight = "1.1";
        if (maxWidth) {
            var div = document.createElement("div");
            div.style.display = "inline-block";
            div.innerHTML = caption;
            this.options.mainPanel.appendChild(div);
            var textWidth = div.offsetWidth;
            this.options.mainPanel.removeChild(div);
            if (maxWidth < textWidth / 2) maxWidth = textWidth / 2 + 5;
            button.caption.style.maxWidth = maxWidth + "px";
        }
        if (caption) button.caption.innerHTML = caption;
    }

    if (arrow) {
        button.arrow = document.createElement("img");
        button.arrow.src = this.options.images["ButtonArrowDown.png"];
        button.cellArrow = innerTable.addCellInNextRow(button.arrow);
        button.cellArrow.style.padding = "0 0 5px 0";
        button.cellArrow.style.textAlign = "center";
    }

    if (toolTip && typeof (toolTip) != "object") { button.setAttribute("title", toolTip); }

    button.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    button.onmouseenter = function () {
        if (!this.isEnabled || (this["haveMenu"] && this.isSelected) || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object")
            this.jsObject.options.toolTip.showWithDelay(
                this.toolTip[0],
                this.toolTip[1],
                this.toolTip.length == 3 ? this.toolTip[2].left : this.jsObject.FindPosX(this, "stiDesignerMainPanel"),
                this.toolTip.length == 3 ? this.toolTip[2].top : this.jsObject.options.toolBar.offsetHeight + this.jsObject.options.workPanel.offsetHeight - 1
            );
    }
    button.onmouseleave = function () {
        this.isOver = false;
        if (!this.isEnabled) return;
        this.className = (this.isSelected ? this.styles["selected"] : this.styles["default"]) + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object") this.jsObject.options.toolTip.hideWithDelay();
    }

    button.onmousedown = function () {
        if (this.isTouchStartFlag || !this.isEnabled) return;
        this.jsObject.options.buttonPressed = this;
    }

    button.onclick = function () {
        if (this.isTouchEndFlag || !this.isEnabled || this.jsObject.options.isTouchClick) return;
        if (this.jsObject.options.showTooltips && this.toolTip && typeof (this.toolTip) == "object") this.jsObject.options.toolTip.hide();
        this.action();
    }

    button.ontouchend = function () {
        if (!this.isEnabled || this.jsObject.options.fingerIsMoved) return;
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        this.jsObject.options.buttonsTimer = [this, this.className, setTimeout(function () {
            this_.jsObject.options.buttonsTimer = null;
            this_.className = this_.styles["default"] + "_Touch";
            if (this_.jsObject.options.showTooltips && this_.toolTip && typeof (this_.toolTip) == "object") this_.jsObject.options.toolTip.hide();
            this_.action();
        }, 150)];
        this.className = this.jsObject.options.isTouchDevice ? this.styles["over"] + "_Touch" : this.styles["over"] + "_Mouse";

        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    button.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.fingerIsMoved = false;
        this.jsObject.options.buttonPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    button.setEnabled = function (state) {
        if (this.image) this.image.style.opacity = state ? "1" : "0.3";
        if (this.arrow) this.arrow.style.opacity = state ? "1" : "0.3";
        this.isEnabled = state;
        if (!state && !this.isOver) this.isOver = false;
        this.className = (state ? (this.isOver ? this.styles["over"] : this.styles["default"]) : this.styles["disabled"]) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    button.setSelected = function (state) {
        if (this.groupName && state)
            for (var name in this.jsObject.options.buttons) {
                if (this.groupName == this.jsObject.options.buttons[name].groupName)
                    this.jsObject.options.buttons[name].setSelected(false);
            }

        this.isSelected = state;
        this.className = (state ? this.styles["selected"] : (this.isEnabled ? (this.isOver ? this.styles["over"] : this.styles["default"]) : this.styles["disabled"])) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    button.action = function () { this.jsObject.ExecuteAction(this.name); }

    return button;
}