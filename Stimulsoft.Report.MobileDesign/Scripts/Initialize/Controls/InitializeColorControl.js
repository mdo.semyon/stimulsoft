﻿
StiMobileDesigner.prototype.ColorControl = function (name, toolTip, rightToLeft, width, showBorder) {
    var colorControl = this.CreateHTMLTable();
    colorControl.jsObject = this;
    colorControl.name = name != null ? name : this.generateKey();
    colorControl.key = null;
    colorControl.isEnabled = true;
    colorControl.rightToLeft = rightToLeft;
    if (name != null) this.options.controls[name] = colorControl;
    if (showBorder) colorControl.className = "stiColorControlWithBorder";

    //Button
    colorControl.button = this.StandartSmallButton(name != null ? name + "Button" : null, null, null, true, toolTip, "Down");
    colorControl.button.colorControl = colorControl;
    colorControl.addCell(colorControl.button);
    if (showBorder) colorControl.button.style.height = this.options.isTouchDevice ? "26px" : "21px";

    //Override image
    var newImageParent = document.createElement("div");
    newImageParent.className = showBorder ? "stiColorControlImageWithoutBorder" : "stiColorControlImage";
    var newImage = document.createElement("div");
    newImage.style.height = "100%";
    newImageParent.appendChild(newImage);
    if (width) newImageParent.style.width = (width - 16) + "px";

    var imageCell = colorControl.button.image.parentElement;
    imageCell.removeChild(colorControl.button.image);
    imageCell.appendChild(newImageParent);
    colorControl.button.image = newImage;

    colorControl.button.action = function () {
        var colorDialog = this.jsObject.options.menus.colorDialog || this.jsObject.InitializeColorDialog();
        colorDialog.rightToLeft = this.colorControl.rightToLeft;
        colorDialog.changeVisibleState(!colorDialog.visible, this);
    }

    colorControl.button.choosedColor = function (key) {
        this.colorControl.setKey(key);
        this.colorControl.action();
    };

    //Color Control
    colorControl.setKey = function (key) {
        this.key = key;
        if (key == "StiEmptyValue") {
            this.button.image.style.opacity = 0;
            return;
        }
        this.button.image.style.opacity = 1;
        var color;
        if (key == "transparent")
            color = "255,255,255";
        else {
            var colors = key.split(",");
            if (colors.length == 4) {
                this.button.image.style.opacity = this.jsObject.StrToInt(colors[0]) / 255;
                colors.splice(0, 1);
            }
            color = colors[0] + "," + colors[1] + "," + colors[2];
        }

        this.button.image.style.background = "rgb(" + color + ")";
    };

    //Override methods
    colorControl.setEnabled = function (state) {
        this.isEnabled = state;
        this.button.setEnabled(state);
        if (showBorder) this.className = state ? "stiColorControlWithBorder" : "stiColorControlWithBorderDisabled";
    }

    colorControl.action = function () { };

    return colorControl;
}