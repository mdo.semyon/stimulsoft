﻿
StiMobileDesigner.prototype.ReportTreePanel = function () {
    var reportTreePanel = document.createElement("div");
    reportTreePanel.jsObject = this;
    reportTreePanel.className = "stiDesignerPropertiesPanelInnerContent";
    this.options.reportTreePanel = reportTreePanel;
    reportTreePanel.style.top = "35px";

    reportTreePanel.appendChild(this.ReportTree());

    return reportTreePanel;
}

StiMobileDesigner.prototype.ReportTree = function () {
    var reportTree = this.Tree();
    reportTree.style.margin = "8px";
    reportTree.openingKeys = {};
    this.options.reportTree = reportTree;
    var jsObject = this;


    reportTree.addComponents = function (parentItem, components) {
        for (var componentName in components) {
            var imageName = jsObject.options.images["SmallComponents." + components[componentName].typeComponent + ".png"] != null
                ? "SmallComponents." + components[componentName].typeComponent + ".png" : "SmallComponents.StiText.png";

            var componentItem = jsObject.ReportTreeItem(componentName, imageName, components[componentName], reportTree, null, componentName);
            parentItem.addChild(componentItem);
            reportTree.addEvents(componentItem, components[componentName].properties.events);
            if (reportTree.openingKeys[componentItem.id]) componentItem.setOpening(true);

            var childsStr = components[componentName].properties.childs;
            if (childsStr) {
                var childNames = childsStr.split(",");
                var childs = {};

                for (var indexChild = 0; indexChild < childNames.length; indexChild++) {
                    var child = jsObject.options.report.pages[components[componentName].properties.pageName].components[childNames[indexChild]];
                    if (child && child.properties.parentName == componentName) childs[child.properties.name] = child;
                }
                reportTree.addComponents(componentItem, childs);
            }
        }
    }

    reportTree.addPages = function (parentItem, pages) {
        for (var pageName in pages) {
            var pageItem = jsObject.ReportTreeItem(pageName, "SmallComponents.StiPage.png", pages[pageName], reportTree, null, pageName);
            parentItem.addChild(pageItem);
            if (reportTree.openingKeys[pageItem.id]) pageItem.setOpening(true);
            var pageChilds = {};
            for (var compName in pages[pageName].components) {
                if (pages[pageName].components[compName].properties.parentName == pageName)
                    pageChilds[compName] = pages[pageName].components[compName];
            }
            reportTree.addEvents(pageItem, pages[pageName].properties.events);
            reportTree.addComponents(pageItem, pageChilds);
        }
    }

    reportTree.addEvents = function (parentItem, events) {
        for (var eventName in events) {
            if (eventName && events[eventName]) {
                var eventItem = jsObject.ReportTreeItem(eventName.replace("Event", ""), "EventsTab.png", null, reportTree, null, parentItem.id + "_" + eventName);
                parentItem.addChild(eventItem);
            }
        }
    }

    reportTree.build = function () {
        if (jsObject.options.propertiesPanel.containers.ReportTree.style.display == "none") return;
        reportTree.clear();

        if (jsObject.options.report) {
            var reportCaption = jsObject.loc.Components.StiReport + " [" + Base64.decode(jsObject.options.report.properties.reportName.replace("Base64Code;", "")) + "]";
            reportTree.reportItem = jsObject.ReportTreeItem(reportCaption, "SmallComponents.StiReport.png", jsObject.options.report, reportTree, null, "reportItem");
            reportTree.appendChild(reportTree.reportItem);
            reportTree.reportItem.setOpening(true);

            reportTree.addEvents(reportTree.reportItem, jsObject.options.report.properties.events);
            reportTree.addPages(reportTree.reportItem, jsObject.options.report.pages);
        }

        if (reportTree.selectedItem && reportTree.items[reportTree.selectedItem.id]) {
            reportTree.items[reportTree.selectedItem.id].setSelected();
        }
        else {
            var selectedObject = jsObject.options.selectedObjects ? jsObject.options.selectedObjects[0] : jsObject.options.selectedObject;
            if (selectedObject) {
                var item = selectedObject.typeComponent == "StiReport" ? reportTree.reportItem : reportTree.items[selectedObject.properties.name];
                if (item) item.setSelected();
            }
        }
    }

    reportTree.onActionItem = function (item) {
        if (item.itemObject) {
            if (item.itemObject.typeComponent == "StiPage") {
                this.jsObject.options.paintPanel.showPage(item.itemObject);
            }
            else {
                if (item.itemObject.properties.pageName &&
                    item.itemObject.properties.pageName != this.jsObject.options.currentPage.properties.name &&
                    this.jsObject.options.report.pages[item.itemObject.properties.pageName]) {
                    this.jsObject.options.paintPanel.showPage(this.jsObject.options.report.pages[item.itemObject.properties.pageName]);
                }
                item.itemObject.setSelected();
            }

            this.jsObject.UpdatePropertiesControls();
        }
    };

    reportTree.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        this.items = {};
    }

    reportTree.reset = function () {
        reportTree.openingKeys = {};
        reportTree.selectedItem = null;
    }

    return reportTree;
}

StiMobileDesigner.prototype.ReportTreeItem = function (caption, imageName, itemObject, tree, showCheckBox, id) {
    var reportTreeItem = this.TreeItem(caption, imageName, itemObject, tree, showCheckBox, id);

    reportTreeItem.onmousedown = function (event) {
        if (event) event.preventDefault();
        return false;
    }

    //Override
    reportTreeItem.iconOpening.action = function () {
        if (this.treeItem.tree.isDisable) return;
        this.treeItem.isOpening = !this.treeItem.isOpening;
        this.treeItem.childsRow.style.display = this.treeItem.isOpening ? "" : "none";
        var imgName = this.treeItem.isOpening ? "IconCloseItem.png" : "IconOpenItem.png";
        if (this.jsObject.options.isTouchDevice) imgName = imgName.replace(".png", "Big.png");
        this.treeItem.iconOpening.src = this.jsObject.options.images[imgName];
        this.treeItem.setSelected();
        this.treeItem.tree.onActionItem(this.treeItem);

        if (this.treeItem.id != "reportItem") {
            if (this.treeItem.isOpening) {
                this.treeItem.tree.openingKeys[this.treeItem.id] = true;
            }
            else {
                if (this.treeItem.tree.openingKeys[this.treeItem.id]) {
                    delete this.treeItem.tree.openingKeys[this.treeItem.id];
                }
            }
        }
    }

    reportTreeItem.openTree = function () {
        var item = this.parent;
        while (item != null) {
            item.isOpening = true;
            this.tree.openingKeys[item.id] = true;
            item.childsRow.style.display = "";
            item.iconOpening.src = this.jsObject.options.images[this.jsObject.options.isTouchDevice ? "IconCloseItemBig.png" : "IconCloseItem.png"];
            item = item.parent;
        }
    }

    return reportTreeItem;
}
