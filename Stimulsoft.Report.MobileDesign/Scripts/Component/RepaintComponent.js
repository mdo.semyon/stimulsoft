
//-------------------component--------------------------------
StiMobileDesigner.prototype.RepaintComponent = function (component) {
    if (this.IsTableCell(component) && component.properties.enabled == false) {
        component.style.display = "none";
    };

    var pageMarginsPx = component.properties.pageName ? this.options.report.pages[component.properties.pageName].marginsPx : [0, 0, 0, 0];
    var compLeftPx = (this.ConvertUnitToPixel(this.StrToDouble(component.properties.unitLeft)) * this.options.report.zoom + pageMarginsPx[0]);
    var compTopPx = (this.ConvertUnitToPixel(this.StrToDouble(component.properties.unitTop)) * this.options.report.zoom + pageMarginsPx[1]);
    var compWidthPx = (this.ConvertUnitToPixel(this.StrToDouble(component.properties.unitWidth)) * this.options.report.zoom);
    var compHeightPx = (this.ConvertUnitToPixel(this.StrToDouble(component.properties.unitHeight)) * this.options.report.zoom);
    component.realWidth = compWidthPx;
    component.realHeight = compHeightPx;

    var roundedPaintRect = this.GetRoundedPaintRect([compLeftPx, compTopPx, compWidthPx, compHeightPx]);

    component.setAttribute("left", roundedPaintRect[0]);
    component.setAttribute("top", roundedPaintRect[1]);
    component.setAttribute("width", roundedPaintRect[2]);
    component.setAttribute("height", roundedPaintRect[3]);
    component.setAttribute("transform", "translate(" + roundedPaintRect[0] + ", " + roundedPaintRect[1] + ")");

    if (component.typeComponent == "StiCrossTab")
        this.RepaintCrossTabFields(component);
    if (ComponentCollection[component.typeComponent][6] != "0")
        this.RepaintCorners(component);

    this.RepaintBorder(component);
    this.RepaintShadow(component);
    this.RepaintBackGround(component);
    this.RepaintContent(component);
    this.RepaintColumnsLines(component);

    if (ComponentCollection[component.typeComponent][2] != "none")
        this.RepaintHeader(component);
    if (ComponentCollection[component.typeComponent][3] != "none" || component.typeComponent.indexOf("StiTableCell") == 0)
        this.RepaintNameContent(component);
    if (component.typeComponent == "StiHorizontalLinePrimitive" || component.typeComponent == "StiVerticalLinePrimitive")
        this.RepaintPrimitiveLine(component);
    if (component.typeComponent == "StiRectanglePrimitive")
        this.RepaintPrimitiveRectangle(component);
    if (component.typeComponent == "StiRoundedRectanglePrimitive")
        this.RepaintPrimitiveRoundedRectangle(component);

    if (this.options.isTouchDevice && !component.isCrossTabField)
        this.RepaintResizingIcons(component);
    else
        this.RepaintResizingPoints(component);
}

//-------------------Border------------------------------
StiMobileDesigner.prototype.RepaintBorder = function (component) {
    var borderStyles = ["", "9,3", "9,2,2,2", "9,2,2,2,2,2", "2,2", "", "none"];
    var borderProps = (component.properties["border"]) ? component.properties.border.split("!") : ["none"];
    var showBorders = borderProps[0] == "none" || borderProps[0] == "0,0,0,0" || borderProps[3] == "6" ? false : true;
    if (borderProps[0] != "none") borderVisibleProps = borderProps[0].split(",");

    for (borderNum = 0; borderNum < 8; borderNum++) {
        component.controls.borders[borderNum].style.stroke = showBorders
            ? (borderProps[2] == "transparent"
                ? "transparent"
                : "rgb(" + this.ColorToRGBStr(borderProps[2]) + ")")
            : (ComponentCollection[component.typeComponent][2] != "none"
                ? ComponentCollection[component.typeComponent][0]
                : "#c6c6c6");
        if (component.typeComponent == "StiTextInCells") component.controls.borders[borderNum].style.stroke = "transparent"; //exception
        component.controls.borders[borderNum].style.strokeWidth = (showBorders && borderProps[3] != "5") ? borderProps[1] : "1";
        component.controls.borders[borderNum].style.strokeDasharray = showBorders ? borderStyles[borderProps[3]] : ComponentCollection[component.typeComponent][1];
        if (showBorders) {
            if (borderNum >= 4) component.controls.borders[borderNum].style.visibility = (borderVisibleProps[borderNum - 4] == "1" && borderProps[3] == "5") ? "visible" : "hidden";
            else component.controls.borders[borderNum].style.visibility = borderVisibleProps[borderNum] == "1" ? "visible" : "hidden";
        }
        else
            component.controls.borders[borderNum].style.visibility = (ComponentCollection[component.typeComponent][1] == "none" || borderNum >= 4) ? "hidden" : "visible";
    }

    var borderSize = parseInt(showBorders && borderProps[3] != "5" ? borderProps[1] : "1");
    var XOffset = (borderSize % 2 != 0) ? this.options.xOffset : 0;
    var YOffset = (borderSize % 2 != 0) ? this.options.yOffset : 0;

    var modifyWidth = parseInt(component.getAttribute("width"));
    var modifyHeight = parseInt(component.getAttribute("height"));
    bordersPosition = [
            [0 + XOffset, 0 + YOffset, 0 + XOffset, modifyHeight + YOffset],
            [0 + XOffset, 0 + YOffset, modifyWidth + XOffset, 0 + YOffset],
            [modifyWidth + XOffset, 0 + YOffset, modifyWidth + XOffset, modifyHeight + YOffset],
            [modifyWidth + XOffset, modifyHeight + YOffset, 0 + XOffset, modifyHeight + YOffset],
            [2 + XOffset, 2 + YOffset, 2 + XOffset, modifyHeight + YOffset - 2],
            [2 + XOffset, 2 + YOffset, modifyWidth + XOffset - 2, 2 + YOffset],
            [modifyWidth + XOffset - 2, 2 + YOffset, modifyWidth + XOffset - 2, modifyHeight + YOffset - 2],
            [modifyWidth + XOffset - 2, modifyHeight + YOffset - 2, 2 + XOffset, modifyHeight + YOffset - 2]
    ];

    for (borderNum = 0; borderNum < 8; borderNum++) {
        component.controls.borders[borderNum].setAttribute("x1", bordersPosition[borderNum][0]);
        component.controls.borders[borderNum].setAttribute("y1", bordersPosition[borderNum][1]);
        component.controls.borders[borderNum].setAttribute("x2", bordersPosition[borderNum][2]);
        component.controls.borders[borderNum].setAttribute("y2", bordersPosition[borderNum][3]);
    }
}

/* Primitives */
StiMobileDesigner.prototype.RepaintPrimitiveLine = function (component) {
    var countLines = component.properties.style == "5" ? 2 : 1;
    var lineSize = countLines == 2 ? 1 : parseInt(component.properties.size);
    lineSize = parseInt(lineSize * this.options.report.zoom);
    if (lineSize == 0) lineSize = 1;
    var spacesSize = lineSize < 2 ? 2 : lineSize;
    var borderStyles = ["", lineSize * 3 + "," + spacesSize, lineSize * 6 + "," + spacesSize + "," + spacesSize + "," + spacesSize,
        lineSize * 6 + "," + spacesSize + "," + spacesSize + "," + spacesSize + "," + spacesSize + "," + spacesSize, lineSize + "," + spacesSize, "", "none"];
    var XOffset = (lineSize % 2 != 0) ? this.options.xOffset : 0;
    var YOffset = (lineSize % 2 != 0) ? this.options.yOffset : 0;
    var modifyWidth = parseInt(component.getAttribute("width"));
    var modifyHeight = parseInt(component.getAttribute("height"));
    var startPos = countLines == 2 ? 0 : 1;

    for (var i = 0; i < countLines; i++) {
        var line = component.controls.borders[i];
        line.style.strokeWidth = lineSize;
        line.style.stroke = component.properties.color == "transparent" ? "transparent" : "rgb(" + this.ColorToRGBStr(component.properties.color) + ")";
        line.style.strokeDasharray = borderStyles[component.properties.style];
        line.style.visibility = "visible";
        line.setAttribute("x1", component.typeComponent == "StiHorizontalLinePrimitive" ? XOffset : -1 + XOffset + (i == 0 ? startPos : 2));
        line.setAttribute("y1", component.typeComponent == "StiHorizontalLinePrimitive" ? -1 + YOffset + (i == 0 ? startPos : 2) : 0 + YOffset);
        line.setAttribute("x2", component.typeComponent == "StiHorizontalLinePrimitive" ? modifyWidth + XOffset : -1 + XOffset + (i == 0 ? startPos : 2));
        line.setAttribute("y2", component.typeComponent == "StiHorizontalLinePrimitive" ? -1 + YOffset + (i == 0 ? startPos : 2) : modifyHeight + YOffset);
    }
}

StiMobileDesigner.prototype.RepaintPrimitiveRectangle = function (component) {
    var countLines = component.properties.style == "5" ? 8 : 4;
    var lineSize = countLines == 8 ? 1 : parseInt(component.properties.size);
    lineSize = parseInt(lineSize * this.options.report.zoom);
    if (lineSize == 0) lineSize = 1;
    var spacesSize = lineSize < 2 ? 2 : lineSize;
    var borderStyles = ["", lineSize * 3 + "," + spacesSize, lineSize * 6 + "," + spacesSize + "," + spacesSize + "," + spacesSize,
        lineSize * 6 + "," + spacesSize + "," + spacesSize + "," + spacesSize + "," + spacesSize + "," + spacesSize, lineSize + "," + spacesSize, "", "none"];
    var XOffset = (lineSize % 2 != 0) ? this.options.xOffset : 0;
    var YOffset = (lineSize % 2 != 0) ? this.options.yOffset : 0;
    var modifyWidth = parseInt(component.getAttribute("width"));
    var modifyHeight = parseInt(component.getAttribute("height"));

    var linePositions = [
        [0 + XOffset, 0 + YOffset, 0 + XOffset, modifyHeight + YOffset],
        [0 + XOffset, 0 + YOffset, modifyWidth + XOffset, 0 + YOffset],
        [modifyWidth + XOffset, 0 + YOffset, modifyWidth + XOffset, modifyHeight + YOffset],
        [modifyWidth + XOffset, modifyHeight + YOffset, 0 + XOffset, modifyHeight + YOffset],
        [2 + XOffset, 2 + YOffset, 2 + XOffset, modifyHeight + YOffset - 2],
        [2 + XOffset, 2 + YOffset, modifyWidth + XOffset - 2, 2 + YOffset],
        [modifyWidth + XOffset - 2, 2 + YOffset, modifyWidth + XOffset - 2, modifyHeight + YOffset - 2],
        [modifyWidth + XOffset - 2, modifyHeight + YOffset - 2, 2 + XOffset, modifyHeight + YOffset - 2]
    ];

    for (var i = 0; i < countLines; i++) {
        var line = component.controls.borders[i];
        line.style.strokeWidth = lineSize;
        line.style.stroke = component.properties.color == "transparent" ? "transparent" : "rgb(" + this.ColorToRGBStr(component.properties.color) + ")";
        line.style.strokeDasharray = borderStyles[component.properties.style];
        line.style.visibility = ((component.properties.leftSide && (i == 0 || i == 4)) || (component.properties.rightSide && (i == 2 || i == 6)) ||
            (component.properties.topSide && (i == 1 || i == 5)) || (component.properties.bottomSide && (i == 3 || i == 7))) ? "visible" : "hidden";
        line.setAttribute("x1", linePositions[i][0]);
        line.setAttribute("y1", linePositions[i][1]);
        line.setAttribute("x2", linePositions[i][2]);
        line.setAttribute("y2", linePositions[i][3]);
    }
}

StiMobileDesigner.prototype.RepaintPrimitiveRoundedRectangle = function (component) {
    var countRect = component.properties.style == "5" ? 2 : 1;
    var lineSize = countRect == 2 ? 1 : parseInt(component.properties.size);
    lineSize = parseInt(lineSize * this.options.report.zoom);
    if (lineSize == 0) lineSize = 1;
    var spacesSize = lineSize < 2 ? 2 : lineSize;
    var borderStyles = ["", lineSize * 3 + "," + spacesSize, lineSize * 6 + "," + spacesSize + "," + spacesSize + "," + spacesSize,
        lineSize * 6 + "," + spacesSize + "," + spacesSize + "," + spacesSize + "," + spacesSize + "," + spacesSize, lineSize + "," + spacesSize, "", "none"];
    var XOffset = (lineSize % 2 != 0) ? this.options.xOffset : 0;
    var YOffset = (lineSize % 2 != 0) ? this.options.yOffset : 0;

    if (!component.controls.roundedRectangles) {
        component.controls.roundedRectangles = [];
        for (var i = 1; i <= 2; i++) {
            var rect = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'rect') : document.createElement("rect");
            component.controls.roundedRectangles.push(rect);
            component.appendChild(rect);
        }
    }

    component.controls.roundedRectangles[1].style.visibility = countRect == 2 ? "visible" : "hidden";

    for (var i = 0; i < countRect; i++) {
        var rect = component.controls.roundedRectangles[i];
        var width = parseInt(component.getAttribute("width"));
        var height = parseInt(component.getAttribute("height"));
        rect.setAttribute("width", i == 0 ? width : width - 4);
        rect.setAttribute("height", i == 0 ? height : height - 4);
        rect.setAttribute("x", i == 0 ? 0 + XOffset : 2 + XOffset);
        rect.setAttribute("y", i == 0 ? 0 + YOffset : 2 + YOffset);
        var round = this.StrToDouble(component.properties.round);
        rect.setAttribute("rx", round * 100);
        rect.setAttribute("ry", round * 100);
        rect.style.fill = "none";
        rect.style.stroke = component.properties.color == "transparent" ? "transparent" : "rgb(" + this.ColorToRGBStr(component.properties.color) + ")";
        rect.style.strokeWidth = lineSize;
        rect.style.strokeDasharray = borderStyles[component.properties.style];
    }
}

//--------------Resizing Points---------------------------------
StiMobileDesigner.prototype.RepaintResizingPoints = function (component) {
    var resizingPoints = component.controls.resizingPoints;
    if (!resizingPoints) return;
    var modifyWidth = parseInt(component.getAttribute("width"));
    var modifyHeight = parseInt(component.getAttribute("height"));

    for (i = 0; i <= 7; i++) {
        var resizingPoint = resizingPoints[i];
        if (!resizingPoint) {
            continue;
        }
        else {
            resizingPoint.style.stroke = component.properties.locked && !this.IsTableCell(component) ? "red" : "#696969";
            var width = parseInt(resizingPoint.getAttribute("width"));
            var height = parseInt(resizingPoint.getAttribute("height"));

            if (i == 0 || i == 6 || i == 7) resizingPoint.setAttribute("x", parseInt(-width / 2) + this.options.xOffset);
            if (i == 2 || i == 3 || i == 4) resizingPoint.setAttribute("x", parseInt(modifyWidth - width / 2) + this.options.xOffset);
            if (i == 1 || i == 5) {
                var x = parseInt(modifyWidth / 2 - width / 2) + this.options.xOffset;
                if (component.typeComponent == "StiVerticalLinePrimitive") x--;
                resizingPoint.setAttribute("x", x);
            }
            if (i == 0 || i == 1 || i == 2) resizingPoint.setAttribute("y", parseInt(-height / 2) + this.options.yOffset);
            if (i == 4 || i == 5 || i == 6) resizingPoint.setAttribute("y", parseInt(modifyHeight - height / 2) + this.options.yOffset);
            if (i == 3 || i == 7) {
                var y = parseInt(modifyHeight / 2 - height / 2) + this.options.yOffset;
                if (component.typeComponent == "StiHorizontalLinePrimitive") y--;
                resizingPoint.setAttribute("y", y);
            }
        }
    }
}

//--------------Resizing Icons---------------------------------
StiMobileDesigner.prototype.RepaintResizingIcons = function (component) {
    var modifyWidth = parseInt(component.getAttribute("width"));
    var modifyHeight = parseInt(component.getAttribute("height"));
    var iconNumbers = ComponentCollection[component.typeComponent][4].split(",");
    var headerType = ComponentCollection[component.typeComponent][2];
    var headerSize = (headerType == "up" && component.controls.header.getAttribute("height")) ? parseInt(component.controls.header.getAttribute("height")) : 0;

    for (i = 0; i < iconNumbers.length; i++) {
        var numIcon = iconNumbers[i];
        var resizingIcon = component.controls.resizingIcons[numIcon];
        if (numIcon == 0 || numIcon == 3) resizingIcon.setAttribute("x", this.options.isTouchDevice ? -36 : -23);
        if (numIcon == 1 || numIcon == 2) resizingIcon.setAttribute("x", this.options.isTouchDevice ? modifyWidth - 13 : modifyWidth);
        if (numIcon == 0 || numIcon == 1) resizingIcon.setAttribute("y", (this.options.isTouchDevice ? -36 : -23) - headerSize);
        if (numIcon == 3 || numIcon == 2) resizingIcon.setAttribute("y", this.options.isTouchDevice ? modifyHeight - 13 : modifyHeight);
    }

    var editIcon = component.controls.editIcon;
    if (editIcon) {
        editIcon.style.opacity = 0.8;
        editIcon.setAttribute("x", modifyWidth - 29);
        editIcon.setAttribute("y", component.typeComponent.indexOf("Band") != -1 || component.typeComponent == "StiTable"
            ? (component.typeComponent.indexOf("Cross") != -1 ? modifyHeight - 24 - 5 : -24 - 5) : -23);
    }
}

//-------------------Corners--------------------------------------
StiMobileDesigner.prototype.RepaintCorners = function (component) {
    var borderProps = (component.properties["border"]) ? component.properties.border.split("!") : ["none"];
    var showAllBorder = (borderProps[0] == "1,1,1,1") ? true : false;
    for (i = 0; i < 4; i++) { component.controls.corners[i].style.display = (showAllBorder) ? "none" : ""; }
    if (showAllBorder) return;

    var modifyWidth = parseInt(component.getAttribute("width")) + this.options.xOffset;
    var modifyHeight = parseInt(component.getAttribute("height")) + this.options.yOffset;

    var widthCorn = 4 + this.options.xOffset;
    var heightCorn = 4 + this.options.yOffset;

    var tempX = modifyWidth - widthCorn;
    var tempY = modifyHeight - heightCorn;

    component.controls.corners[0].setAttribute("points", this.options.xOffset + " " + heightCorn + ", " + this.options.xOffset + " " + this.options.yOffset + ", " + widthCorn + " " + this.options.yOffset);
    component.controls.corners[1].setAttribute("points", tempX + " " + this.options.yOffset + ", " + modifyWidth + " " + this.options.yOffset + ", " + modifyWidth + " " + heightCorn);
    component.controls.corners[2].setAttribute("points", modifyWidth + " " + tempY + ", " + modifyWidth + " " + modifyHeight + ", " + tempX + " " + modifyHeight);
    component.controls.corners[3].setAttribute("points", this.options.xOffset + " " + tempY + ", " + this.options.xOffset + " " + modifyHeight + ", " + widthCorn + " " + modifyHeight);
}

//---------------------BackGround--------------------------------
StiMobileDesigner.prototype.RepaintBackGround = function (component) {
    component.controls.background.setAttribute("width", component.getAttribute("width"));
    component.controls.background.setAttribute("height", component.getAttribute("height"));
    component.controls.background.setAttribute("x", 0);
    component.controls.background.setAttribute("y", 0);
    component.controls.background.style.fill = "transparent";

    if (component.properties.isPrimitiveComponent) {
        var lineSize = parseInt(component.properties.size);
        component.controls.background.style.opacity = 0.5;

        if (component.typeComponent == "StiRectanglePrimitive" || component.typeComponent == "StiRoundedRectanglePrimitive") {
            component.controls.background.style.fill = "none";
            component.controls.background.style.strokeWidth = lineSize + 4;
            if (component.typeComponent == "StiRoundedRectanglePrimitive") {
                var round = this.StrToDouble(component.properties.round);
                component.controls.background.setAttribute("rx", round * 100);
                component.controls.background.setAttribute("ry", round * 100);
            }

            component.onmouseover = function () {
                if (!this.jsObject.options.in_drag && !this.jsObject.options.in_resize) {
                    this.controls.background.style.stroke = "rgb(255,231,105)";
                }
            }
            component.onmouseout = function () { this.controls.background.style.stroke = "transparent"; }
        }
        else {
            component.controls.background.setAttribute("width", parseInt(component.getAttribute("width")) + lineSize + 3);
            component.controls.background.setAttribute("height", parseInt(component.getAttribute("height")) + lineSize + 3);
            component.controls.background.setAttribute("x", -2 - lineSize / 2);
            component.controls.background.setAttribute("y", -2 - lineSize / 2);
            component.onmouseover = function () {
                if (!this.jsObject.options.in_drag && !this.jsObject.options.in_resize) {
                    this.controls.background.style.fill = "rgb(255,231,105)";
                }
            }
            component.onmouseout = function () { this.controls.background.style.fill = "transparent"; }
        }
        return;
    }

    if (component.typeComponent == "StiShape" || component.typeComponent == "StiTextInCells" || !component.properties["brush"]) return; //exception

    var brushProps = component.properties.brush.split("!");

    switch (brushProps[0]) {
        case "0":
            {
                component.controls.background.style.fill = "transparent";
                break;
            }
        case "1":
            {
                if (brushProps[1] == "transparent") {
                    var defaultColor = ComponentCollection[component.typeComponent][0];
                    component.controls.background.style.fill = defaultColor;
                    if (component.typeComponent == "StiText" || component.typeComponent == "StiTextInCells")
                        component.controls.background.style.opacity = 0.35;
                    else
                        component.controls.background.style.opacity = 0.15;
                }
                else {
                    var colors = brushProps[1].split(",");
                    if (colors.length == 3) {
                        component.controls.background.style.fill = "rgb(" + colors[0] + "," + colors[1] + "," + colors[2] + ")";
                        component.controls.background.style.opacity = 1;
                    }
                    if (colors.length == 4) {
                        component.controls.background.style.fill = "rgb(" + colors[1] + "," + colors[2] + "," + colors[3] + ")";
                        component.controls.background.style.opacity = this.StrToInt(colors[0]) / 255;
                    }
                }
                break;
            }
        case "2":
        case "3":
        case "4":
        case "5":
            {
                component.controls.background.style.fill = "transparent";
                component.controls.background.style.opacity = 1;
                break;
            }
    }

    //-------------- Conditions Icon ---------------------------------
    if (component.properties.conditions) {
        var conditionIcon = component.controls.conditionIcon;
        if (!conditionIcon) {
            conditionIcon = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'image') : document.createElement("image");
            component.appendChild(conditionIcon);
            component.controls.conditionIcon = conditionIcon;
            conditionIcon.setAttribute("height", 8);
            conditionIcon.setAttribute("width", 9);
            conditionIcon.href.baseVal = this.options.images["SmallCondition.png"];
        }
        conditionIcon.setAttribute("x", 0);
        conditionIcon.setAttribute("y", parseInt(component.getAttribute("height")) - 9);
    }
    else {
        if (component.controls.conditionIcon) {
            component.removeChild(component.controls.conditionIcon);
            component.controls.conditionIcon = null;
        }
    }
}

//---------------------Header--------------------------------------
StiMobileDesigner.prototype.RepaintHeader = function (component) {
    if (!this.options.report.info.showHeaders) {
        component.controls.header.style.display = "none";
        return;
    }
    var headerType = ComponentCollection[component.typeComponent][2];
    var backGroundColor = ComponentCollection[component.typeComponent][0];

    if (headerType != "none") {
        component.controls.header.style.fill = backGroundColor;
        component.controls.header.style.stroke = backGroundColor;
        component.controls.header.setAttribute("width", component.getAttribute("width"));
        component.controls.header.setAttribute("x", this.options.xOffset);
        component.controls.header.style.opacity = 0.8;

        var headerSize = this.StrToInt((component.properties.headerSize ? this.StrToDouble(component.properties.headerSize) : 0) * this.options.report.zoom);

        if (headerType == "up") {
            component.controls.header.setAttribute("y", -headerSize + this.options.yOffset - 1);
        }
        else {
            var height = parseInt(component.getAttribute("height"));
            component.controls.header.setAttribute("y", height + this.options.yOffset);
        }
        component.controls.header.setAttribute("height", headerSize + 1);
    }
}

//---------------------NameContent----------------------------------
StiMobileDesigner.prototype.RepaintNameContent = function (component) {
    var compWidth = component.getAttribute("width");
    var compHeight = component.getAttribute("height");
    if (component.controls.nameText) component.controls.nameText.textContent = "";
    var typeNameContent = ComponentCollection[component.typeComponent][3];
    if (typeNameContent == "none") return false;

    if (typeNameContent == "up" || typeNameContent == "down") {
        var headerSize = parseInt(component.controls.header.getAttribute("height"));
        component.controls.nameContent.setAttribute("height", headerSize);
        component.controls.nameContent.setAttribute("x", "0");
        component.controls.nameContent.setAttribute("y", typeNameContent == "up" ? -headerSize : compHeight);

        fontSize = Math.round(12 * this.options.report.zoom);
        component.controls.nameText.setAttribute("font-size", fontSize);
        component.controls.nameText.setAttribute("x", "2");
        component.controls.nameText.setAttribute("y", Math.round(headerSize - (headerSize - fontSize) / 1.5));
    }

    if (typeNameContent == "center") {
        var bigFont = component.typeComponent != "StiImage" && component.typeComponent != "StiTableCellImage" &&
            component.typeComponent != "StiRichText" && component.typeComponent != "StiTableCellRichText";

        fontSize = Math.round((bigFont ? 18 : 12) * this.options.report.zoom);
        component.controls.nameText.setAttribute("font-size", fontSize);
        if (bigFont) {
            component.controls.nameText.style.stroke = "none";
            component.controls.nameText.style.fill = "#808080";
        }
        component.controls.nameContent.setAttribute("height", compHeight);
        component.controls.nameText.setAttribute("y", fontSize);
        component.controls.nameText.setAttribute("x", "2");
    }

    component.controls.nameContent.setAttribute("width", compWidth);
    if (component.typeComponent == "StiImage" || component.typeComponent == "StiTableCellImage" ||
        component.typeComponent == "StiRichText" || component.typeComponent == "StiTableCellRichText") {

        var imageDataColumn = Base64.decode(component.properties.imageDataColumn || component.properties.richTextDataColumn || "");
        var imageUrl = Base64.decode(component.properties.imageUrl || component.properties.richTextUrl || "");
        var imageData = Base64.decode(component.properties.imageData || "");
        var imageFile = Base64.decode(component.properties.imageFile || "");

        var text = imageDataColumn
            ? this.loc.PropertyMain.DataColumn + ": " + imageDataColumn
            : imageUrl
                ? (component.typeComponent == "StiRichText" || component.typeComponent == "StiTableCellRichText" ? "URL" : this.loc.PropertyMain.ImageURL) + ": " + imageUrl
                : imageData
                    ? this.loc.PropertyMain.ImageData + ": " + imageData
                    : imageFile
                        ? this.loc.MainMenu.menuFile.replace("&", "") + ": " + imageFile
                        : "";

        if ((component.typeComponent == "StiRichText" && component.properties.svgContent) ||
            (component.typeComponent == "StiImage" || component.typeComponent == "StiTableCellImage") &&
            component.properties.imageContentForPaint && imageUrl.indexOf(this.options.cloudServerUrl) < 0) {
            text = "";
        }

        component.controls.nameText.textContent = text;
    }
    else if (component.typeComponent == "StiSubReport") {
        var text = component.properties.subReportPage != "[Not Assigned]"
            ? this.loc.Components.StiSubReport + ":  " + component.properties.subReportPage
            : component.properties.subReportUrl ? this.loc.Components.StiSubReport + ":  " + Base64.decode(component.properties.subReportUrl) : component.properties.name;

        component.controls.nameText.textContent = text;
    }
    else {
        component.controls.nameText.textContent = component.properties.name;
        var aliasName = Base64.decode(component.properties.aliasName);
        if (aliasName) component.controls.nameText.textContent = component.properties.name + " [" + aliasName + "] ";
    }

    var notAssignedText = this.loc.Report.NotAssigned;

    if (component.typeComponent == "StiDataBand" || component.typeComponent == "StiHierarchicalBand" || component.typeComponent == "StiTable") {
        if (component.properties.businessObject != null && component.properties.businessObject != "[Not Assigned]") {
            component.controls.nameText.textContent += "; " + this.loc.PropertyMain.BusinessObject +
                ": " + (component.properties.businessObject == "[Not Assigned]" ? notAssignedText : component.properties.businessObject);
        }
        else {
            component.controls.nameText.textContent += "; " + this.loc.PropertyMain.DataSource +
                ": " + (component.properties.dataSource == "[Not Assigned]"
                    ? (component.properties.countData == 0 ? notAssignedText : component.properties.countData)
                    : component.properties.dataSource);
        }
        if (component.properties.masterComponent != null && component.properties.masterComponent != "[Not Assigned]") {
            component.controls.nameText.textContent += "; " + this.loc.PropertyMain.MasterComponent +
                ": " + (component.properties.masterComponent == "[Not Assigned]" ? notAssignedText : component.properties.masterComponent);
        }
    }
    if (component.typeComponent == "StiGroupHeaderBand") {
        component.controls.nameText.textContent += "; " + this.loc.PropertyMain.Condition +
            ": " + (component.properties.condition ? Base64.decode(component.properties.condition) : "");
    }
}

//------------------------Content-------------------------------
StiMobileDesigner.prototype.RepaintContent = function (component) {
    var svgContentStr = Base64.decode(component.properties.svgContent);
    this.RepaintImageContent(component, svgContentStr);
    this.RepaintSvgContent(component, svgContentStr);
}

//------------------------SvgContent-------------------------------
StiMobileDesigner.prototype.RepaintInnerContent = function (component, innerObjects) {
    if (!innerObjects || !this.options.in_resize || component.typeComponent == "StiCrossTab" || component.typeComponent == "StiCrossField") return;
    var compStartWidth = this.options.in_resize[2].width || component.startWidth;
    var compStartHeight = this.options.in_resize[2].height || component.startHeight;
    var compRealWidth = this.StrToDouble(component.getAttribute("width"));
    var compRealHeight = this.StrToDouble(component.getAttribute("height"));

    if (innerObjects.image) {
        if (component.typeComponent == "StiZipCode") {
            if (!component.properties.ratio) innerObjects.image.setAttribute("preserveAspectRatio", "none");
            innerObjects.image.setAttribute("width", compRealWidth);
            innerObjects.image.setAttribute("height", compRealHeight);
            return;
        }
    }

    if (innerObjects.rect) {
        innerObjects.rect.setAttribute("width", component.getAttribute("width"));
        innerObjects.rect.setAttribute("height", component.getAttribute("height"));
    }

    var innerObject = innerObjects.image || innerObjects.g || innerObjects.text;
    if (innerObject) {
        var x = this.StrToDouble(innerObject.getAttribute("x") || "0");
        var y = this.StrToDouble(innerObject.getAttribute("y") || "0");

        if (innerObjects.g) {
            //barcode component || text component(angle != 0)
            switch (component.properties.textAngle || component.properties.barCodeAngle) {
                case "90":
                    if (component.properties.vertAlignment == "Bottom") x += (compRealWidth - compStartWidth) / this.options.report.zoom;
                    if (component.properties.vertAlignment == "Center") x += ((compRealWidth - compStartWidth) / 2) / this.options.report.zoom;
                    if (component.properties.horAlignment == "Left") y += (compRealHeight - compStartHeight) / this.options.report.zoom;
                    if (component.properties.horAlignment == "Center") y += ((compRealHeight - compStartHeight) / 2) / this.options.report.zoom;
                    break;
                case "180":
                    if (component.properties.horAlignment == "Left") x += (compRealWidth - compStartWidth) / this.options.report.zoom;
                    if (component.properties.horAlignment == "Center") x += ((compRealWidth - compStartWidth) / 2) / this.options.report.zoom;
                    if (component.properties.vertAlignment == "Top") y += (compRealHeight - compStartHeight) / this.options.report.zoom;
                    if (component.properties.vertAlignment == "Center") y += ((compRealHeight - compStartHeight) / 2) / this.options.report.zoom;
                    break;
                case "270":
                    if (component.properties.vertAlignment == "Top") x += (compRealWidth - compStartWidth) / this.options.report.zoom;
                    if (component.properties.vertAlignment == "Center") x += ((compRealWidth - compStartWidth) / 2) / this.options.report.zoom;
                    if (component.properties.horAlignment == "Right") y += (compRealHeight - compStartHeight) / this.options.report.zoom;
                    if (component.properties.horAlignment == "Center") y += ((compRealHeight - compStartHeight) / 2) / this.options.report.zoom;
                    break;
                default:
                    if (component.typeComponent == "StiBarCode") {
                        if (component.properties.vertAlignment == "Bottom") y += (compRealHeight - compStartHeight) / this.options.report.zoom;
                        if (component.properties.vertAlignment == "Center") y += ((compRealHeight - compStartHeight) / 2) / this.options.report.zoom;
                        if (component.properties.horAlignment == "Right") x += (compRealWidth - compStartWidth) / this.options.report.zoom;
                        if (component.properties.horAlignment == "Center") x += ((compRealWidth - compStartWidth) / 2) / this.options.report.zoom;
                    }
                    else {
                        x += ((compRealWidth - compStartWidth) / 2) / this.options.report.zoom
                        y += ((compRealHeight - compStartHeight) / 2) / this.options.report.zoom;
                    }
            }
            var oldTransform = innerObject.getAttribute("transform");
            var newTransform = "translate(" + x + "," + y + ")";
            if (oldTransform && oldTransform.indexOf("translate") >= 0) {
                var startIndex = oldTransform.indexOf("translate(") + "translate(".length;
                var endIndex = oldTransform.indexOf(")", startIndex);
                newTransform = oldTransform.substring(0, startIndex) + x + "," + y + oldTransform.substring(endIndex);
            }
            innerObject.setAttribute("transform", newTransform);
        }
        else {
            //image component || text component(angle == 0)
            switch (component.properties.horAlignment) {
                case "Center": x += ((compRealWidth - compStartWidth) / 2) / this.options.report.zoom; break;
                case "Right": x += (compRealWidth - compStartWidth) / this.options.report.zoom; break;
            }

            switch (component.properties.vertAlignment) {
                case "Center": y += ((compRealHeight - compStartHeight) / 2) / this.options.report.zoom; break;
                case "Bottom": y += (compRealHeight - compStartHeight) / this.options.report.zoom; break;
            }

            innerObject.setAttribute("x", x);
            innerObject.setAttribute("y", y);
        }
    }
}

StiMobileDesigner.prototype.RepaintSvgContent = function (component, svgContentStr) {
    var jsObject = this;
    if (svgContentStr == "") {
        while (component.controls.svgContent.firstChild != null) component.controls.svgContent.removeChild(component.controls.svgContent.firstChild);
        component.controls.svgContent.visibility = "hidden";
    }
    else {
        component.controls.svgContent.style.visibility = "visible";
        var zoom = this.options.report.zoom;
        var temp = document.createElement("div");
        temp.innerHTML = "<svg>" + svgContentStr + "</svg>";
        while (component.controls.svgContent.firstChild != null) component.controls.svgContent.removeChild(component.controls.svgContent.firstChild);
        var innerObjects = {};

        var writeElementXY = function (element) {
            innerObjects.g = element;
            var xyPosStr = element.getAttribute("transform");
            xyPosStr = xyPosStr.substring(xyPosStr.indexOf("translate(") + "translate(".length);
            xyPosStr = xyPosStr.substring(0, xyPosStr.indexOf(")"));
            var xyPos = xyPosStr.split(" ");
            if (xyPos.length == 1) xyPos = xyPosStr.split(",");
            innerObjects.g.setAttribute("x", xyPos[0]);
            innerObjects.g.setAttribute("y", xyPos.length > 1 ? xyPos[1] : 0);
        }

        Array.prototype.slice.call(temp.childNodes[0].childNodes).forEach(function (el) {
            component.controls.svgContent.appendChild(el);
            if (el.tagName == "image") innerObjects.image = el;
            if (el.tagName == "text") innerObjects.text = el;
            if (el.tagName == "rect") innerObjects.rect = el;
            if (el.tagName == "g") {
                if (component.typeComponent == "StiChart" || component.typeComponent == "StiBarCode" || component.typeComponent == "StiCheckBox" || component.typeComponent == "StiTableCellCheckBox") {
                    if (el.getAttribute("transform") && el.getAttribute("transform").indexOf("translate") >= 0) {
                        writeElementXY(el);
                    }
                }
                else {
                    while (el.firstChild != null) {
                        if (el.firstChild.tagName == "g" && el.firstChild.getAttribute("transform") &&
                            el.firstChild.getAttribute("transform").indexOf("translate") >= 0) {
                            writeElementXY(el.firstChild);
                            break;
                        }
                        el = el.firstChild;
                    }
                }
            }

            if (zoom != 1) {
                var oldTransform = el.getAttribute("transform");
                var newTransform = "scale(" + zoom + ")";
                if (oldTransform) newTransform += (" " + oldTransform);
                el.setAttribute("transform", newTransform);
            }
        });

        if (innerObjects.rect) {
            var fillAttr = innerObjects.rect.getAttribute("fill");
            if (fillAttr.indexOf("hatch") >= 0)
                innerObjects.rect.setAttribute("transform", "scale(1)");
        }

        if (innerObjects.text) {
            var textMargins = component.properties.textMargins;
            if (textMargins && textMargins != "0;0;0;0") {
                var x = parseInt(innerObjects.text.getAttribute("x"));
                var y = parseInt(innerObjects.text.getAttribute("y"));
                var margins = textMargins.split(";");
                if (component.properties.horAlignment == "Left" || component.properties.horAlignment == "Center") x += parseInt(margins[0]) * zoom;
                if (component.properties.horAlignment == "Right") x -= parseInt(margins[1]) * zoom;
                if (component.properties.vertAlignment == "Top") y += parseInt(margins[2]) * zoom;
                if (component.properties.vertAlignment == "Bottom") y -= parseInt(margins[3]) * zoom;
                innerObjects.text.setAttribute("x", x);
                innerObjects.text.setAttribute("y", y);
            }
        }

        this.RepaintInnerContent(component, innerObjects);
    }

    component.controls.svgContent.setAttribute("width", component.getAttribute("width"));
    component.controls.svgContent.setAttribute("height", component.getAttribute("height"));
    component.controls.svgContent.setAttribute("x", 0);
    component.controls.svgContent.setAttribute("y", 0);
}

//------------------------ImageContent-------------------------------
StiMobileDesigner.prototype.RepaintImageContent = function (component, svgContentStr) {
    if ((component.typeComponent == "StiImage" || component.typeComponent == "StiTableCellImage") &&
        (component.properties.imageSrc || component.properties.imageContentForPaint)) {
        //Only StiImage
        try {
            var imageSrc = component.properties.imageSrc || component.properties.imageContentForPaint;
            var isWmfImage = imageSrc.indexOf("data:image/x-wmf") >= 0;
            var isSvgImage = imageSrc.indexOf("data:image/svg+xml") >= 0;

            if (isWmfImage && component.properties.imageContentForPaint) {
                imageSrc = component.properties.imageContentForPaint; //Wmf image type
            }
            var zoom = this.options.report.zoom;
            var image = new Image();
            if (imageSrc) image.src = imageSrc;

            var imageContent = component.controls.imageContent;
            imageContent.style.display = "";
            var parentImageContent = component.controls.parentImageContent
            var componentWidth = parseInt(component.realWidth);
            var componentHeight = parseInt(component.realHeight);
            parentImageContent.setAttribute("width", componentWidth);
            parentImageContent.setAttribute("height", componentHeight);
            var multipleFactor = this.StrToDouble(component.properties.imageMultipleFactor);
            var stretch = component.properties.stretch || isSvgImage || isWmfImage;
            var aspectRatio = component.properties.ratio && !isSvgImage && !isWmfImage;


            var newWidth = stretch ? componentWidth : image.width * multipleFactor * zoom;
            var newHeight = stretch ? componentHeight : image.height * multipleFactor * zoom;

            if (stretch && aspectRatio && image.width > 0 && image.height > 0) {
                var wFactor = componentWidth / image.width;
                var hFactor = componentHeight / image.height;
                if (Math.abs(wFactor) < Math.abs(hFactor)) {
                    newWidth = componentWidth;
                    newHeight = image.height * wFactor;
                }
                else {
                    newHeight = componentHeight;
                    newWidth = image.width * hFactor;
                }
            }

            imageContent.setAttribute("width", newWidth);
            imageContent.setAttribute("height", newHeight);

            var x = 0;
            var y = 0;
            if (component.properties.horAlignment == "Center") x = componentWidth / 2 - newWidth / 2;
            else if (component.properties.horAlignment == "Right") x = componentWidth - newWidth;
            if (component.properties.vertAlignment == "Center") y = componentHeight / 2 - newHeight / 2;
            else if (component.properties.vertAlignment == "Bottom") y = componentHeight - newHeight;

            imageContent.href.baseVal = imageSrc;
            imageContent.setAttribute("x", stretch && !aspectRatio ? 0 : x);
            imageContent.setAttribute("y", stretch && !aspectRatio ? 0 : y);

            var angle = 0;
            switch (component.properties.rotation) {
                case "Rotate90CW": angle = 90; break;
                case "Rotate90CCW": angle = -90; break;
                case "Rotate180": angle = 180; break;
            }

            var flipAttributes = "";
            if (component.properties.rotation == "FlipHorizontal") flipAttributes = "scale(-1, 1) translate(-" + (x * 2 + newWidth) + ", 0) ";
            if (component.properties.rotation == "FlipVertical") flipAttributes = "scale(-1, 1) translate(-" + (y * 2 + newHeight) + ", 0) ";

            imageContent.setAttribute("transform", flipAttributes + "rotate(" + angle + ", " + (x + newWidth / 2) + ", " + (y + newHeight / 2) + " )");

            if (stretch && !aspectRatio)
                imageContent.setAttribute("preserveAspectRatio", "none");
            else
                imageContent.removeAttribute("preserveAspectRatio");

            image = null;
        }
        catch (e) { }
        return;
    }
    else {
        component.controls.imageContent.style.display = "none";
        return;
    }

    component.controls.imageContent.setAttribute("width", (component.controls.imageContent.href.baseVal != "") ? parseInt(component.realWidth) : 0);
    component.controls.imageContent.setAttribute("height", (component.controls.imageContent.href.baseVal != "") ? parseInt(component.realHeight) : 0);
    component.controls.imageContent.setAttribute("x", "0");
    component.controls.imageContent.setAttribute("y", "0");
}

//---------------------BackGround--------------------------------
StiMobileDesigner.prototype.RepaintShadow = function (component) {
    var borderProps = (component.properties["border"]) ? component.properties.border.split("!") : null;
    var showShadow = borderProps != null && borderProps[4] == "1";
    component.controls.shadow.style.display = showShadow ? "" : "none";
    if (!showShadow) return;
    var shadowSize = this.StrToInt(borderProps[5]);
    component.controls.shadow.setAttribute("width", parseInt(component.getAttribute("width")) + shadowSize);
    component.controls.shadow.setAttribute("height", parseInt(component.getAttribute("height")) + shadowSize);
    component.controls.shadow.setAttribute("x", shadowSize);
    component.controls.shadow.setAttribute("y", shadowSize);
    var shadowColor = this.GetColorFromBrushStr(Base64.decode(borderProps[6]));
    if (shadowColor != "transparent") shadowColor = "rgb(" + shadowColor + ")";
    component.controls.shadow.style.fill = shadowColor;
}

//-------------------CrossTab Fields------------------------------
StiMobileDesigner.prototype.RepaintCrossTabFields = function (component) {
    while (component.controls.crossTabContainer.childNodes[0]) component.controls.crossTabContainer.removeChild(component.controls.crossTabContainer.childNodes[0]);
    component.controls.crossTabContainer.setAttribute("width", component.getAttribute("width"));
    component.controls.crossTabContainer.setAttribute("height", component.getAttribute("height"));

    if (component.properties.crossTabFields) {
        var compObjects = component.properties.crossTabFields.components;

        for (var i = 0; i < compObjects.length; i++) {
            var newComponent = this.CreateCrossTabFieldComponent(compObjects[i]);
            newComponent.repaint();
            component.controls.crossTabContainer.appendChild(newComponent);
            newComponent.parentContainer = component.controls.crossTabContainer;
        }
    }
}
