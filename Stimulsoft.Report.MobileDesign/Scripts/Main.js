
function StiMobileDesigner(parameters) {    
    this.defaultParameters = {};
    this.options = parameters;

    this.options.buttons = {};
    this.options.controls = {};
    this.options.menus = {};
    this.options.forms = {};
    this.options.radioButtons = {};
    this.options.callbackFunctions = {};
    this.options.openDialogs = {};
    this.options.properties = {};
    this.options.propertiesGroups = {};
    this.options.dataBasesTreeOpeningArray = {};
    this.options.dataBasesTreeOpeningArrayTemp = {};
    this.options.paintPanelPadding = 30;
    this.options.previewPageNumber = 0;
    this.options.previewCountPages = 0;
    this.options.commands = [];
    this.options.touchZoom = {};
    this.options.startZoom = 0;
    this.options.oldDeltaPos = 0;
    this.options.timeUpdateCache = 180000;
    this.options.modifyRestrictions = true;
    this.options.mobileDesigner = document.getElementById(parameters.mobileDesignerId);
    this.options.head = document.getElementsByTagName("head")[0];
    this.options.interfaceType = parameters.interfaceType;
    this.options.supportTouchInterface = this.IsTouchDevice();
    this.options.isTouchDevice = parameters.interfaceType == "Auto"
        ? (this.GetCookie("StimulsoftMobileDesignerInterfaceType")
            ? this.options.supportTouchInterface && this.GetCookie("StimulsoftMobileDesignerInterfaceType") == "Touch"
            : this.options.supportTouchInterface)
        : parameters.interfaceType == "Touch";
    this.options.canOpenFiles = window.File && window.FileReader && window.FileList && window.Blob;
    this.options.menuAnimDuration = parameters.showAnimation ? 150 : 0;
    this.options.formAnimDuration = parameters.showAnimation ? 200 : 0;
    this.options.xOffset = parameters.focusingX ? 0 : 0.5;
    this.options.yOffset = parameters.focusingY ? 0 : 0.5;
    this.options.containers = {};
    this.options.designerIsFocused = true;
    this.options.fontSizes = this.GetFontSizes();
    this.options.monthesCollection = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    this.options.dayOfWeekCollection = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    this.options.themeColors = { Blue: "#19478a", Carmine: "#912c2f", Green: "#0b6433", Orange: "#b73a1c", Purple: "#8653a5", Teal: "#23645c", Violet: "#6d3069" };
    this.options.propertyControlWidth = this.options.propertiesGridWidth - this.options.propertiesGridLabelWidth - 35;
    this.options.propertyNumbersControlWidth = Math.max(this.options.propertyControlWidth - 100, 40);
    this.options.showPanelPropertiesAndDictionary = this.options.showDictionary || this.options.showPropertiesGrid || this.options.showReportTree;
    this.options.requestTimeout = 120;
    this.options.previewReportGuid = this.generateKey();
    if (this.options.fullScreenMode) this.options.mobileDesigner.style.zIndex = "500000";

    var setupToolboxCookie = this.GetCookie("StimulsoftMobileDesignerSetupToolbox");
    var setupToolbox = setupToolboxCookie ? JSON.parse(setupToolboxCookie) : null;
    this.options.showToolbox = setupToolbox ? setupToolbox.showToolbox : true;
    this.options.showInsertTab = setupToolbox ? setupToolbox.showInsertTab : true;
    this.options.publishUrl = "https://publish.stimulsoft.com/";

    if (parameters.loc) {
        var loc = JSON.parse(parameters.loc);
        this.loc = loc.Localization || loc;
        delete this.options.loc;
    }
    else {
        alert("js scripts complete!");
        return;
    }

    if (this.options.cloudMode && this.options.cloudParameters) {
        if (this.options.cloudParameters.isTouchDevice != null) {
            this.options.isTouchDevice = this.options.cloudParameters.isTouchDevice == "true";
        }

        this.SetWindowIcon(document, this.options.cloudParameters.favIcon);
        this.SetWindowTitle(this.options.cloudParameters.reportName + " - " + this.loc.FormDesigner.title);

        if (this.options.cloudParameters.isOnlineVersion) {
            this.options.isOnlineVersion = true;
            this.options.reportResourcesMaximumSize = this.options.dVers ? this.options.cloudParameters.maxResourceSizeTrial : this.options.cloudParameters.maxResourceSizeDeveloper;
            this.options.reportResourcesMaximumCount = 5;
        }

        var requestChangesCookie = this.GetCookie("StimulsoftMobileDesignerRequestChangesWhenSaving");
        this.options.requestChangesWhenSaving = requestChangesCookie == null || requestChangesCookie == "true";
    }

    //Data Tree
    this.options.dataTree = this.DataTree();
    this.options.mobileDesigner.jsObject = this;
    this.options.mainPanel = document.getElementById(this.options.mobileDesigner.id + "_MainPanel");

    if (parameters.scrollbiParameters && parameters.scrollbiParameters.errorMessage) {
        var errorMessageForm = this.options.forms.errorMessageForm || this.InitializeErrorMessageForm();
        errorMessageForm.show(parameters.scrollbiParameters.errorMessage);
    }
                    
    var jsObject = this;
    this.CreateMetaTag();
    this.InitializeDesigner();        
    this.InitializeToolBar();
    this.InitializeWorkPanel();
    this.InitializeHomePanel();
    this.InitializeStatusPanel();    
    this.InitializePropertiesPanel();
    this.InitializePagesPanel();
    this.InitializePaintPanel();
    this.InitializeToolbox();
    this.InitializeToolTip();
    if (this.options.jsMode) this.InitializePreviewPanel();    
    
    this.SetEnabledAllControls(false);

    this.addEvent(document, 'mousemove', function (event) {
        jsObject.DocumentMouseMove(event);
    });

    this.addEvent(document, 'touchmove', function (event) {
        jsObject.DocumentTouchMove(event);
    });

    this.addEvent(document, 'touchend', function (event) {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);

        jsObject.DocumentTouchEnd(event);

        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    });

    this.addEvent(document, 'mouseup', function (event) {
        if (this.isTouchEndTimer) return;
        jsObject.DocumentMouseUp(event);
    });
        
    //Load Report
    if (this.options.jsMode) {
        jsObject.CloseReport();
    }
    else {
        var processImage = this.InitializeProcessImage();
        processImage.show();

        if (document.readyState == 'complete') {
            jsObject.BuildDesignerComplete();
        }
        else if (window.addEventListener) {
            window.addEventListener('load', function () { jsObject.BuildDesignerComplete(); });
        }
        else {
            window.attachEvent('onload', function () { jsObject.BuildDesignerComplete(); });
        }
    }
}

StiMobileDesigner.prototype.mergeOptions = function (fromObject, toObject) {
    for (var value in fromObject) {
        if (toObject[value] === undefined || typeof toObject[value] !== "object") toObject[value] = fromObject[value];
        else this.mergeOptions(fromObject[value], toObject[value]);
    }
}

StiMobileDesigner.prototype.BuildDesignerComplete = function () {
    var jsObject = this;
    var cloudParameters = jsObject.options.cloudParameters;

    if (cloudParameters && cloudParameters.resourceItems && JSON.parse(cloudParameters.resourceItems).length > 0) {        
        try {
            var navigator = window.opener.document.getElementById("StiCloudReportsWebClient");
            if (navigator) {
                (this.options.processImage || this.InitializeProcessImage()).show();
                var intervalId = setInterval(function () {
                    if (navigator.jsObject.options.loadingResourcesComplete) {
                        clearInterval(intervalId);
                        jsObject.GetReportForDesigner();
                    }
                }, 200);
            }
        }
        catch(e) {
            jsObject.GetReportForDesigner();
        }
    }
    else {
        this.GetReportForDesigner();
    };

    //Update Images Array
    var actionFunction = this.options.callbackFunction.replace("callbackParams", JSON.stringify({ command: "UpdateImagesArray" }));
    eval(actionFunction);

    //Load All Scripts
    this.LoadStyle(this.options.stylesUrl.replace("MobileDesignerStyleName", "Viewer"));
    this.LoadScript(this.options.scriptsUrl.replace("MobileDesignerScriptName", "AllNotLoadedScripts"), function () {});
}

StiMobileDesigner.prototype.GetReportForDesigner = function () {
    var jsObject = this;
    var params = null;
    var cloudParameters = jsObject.options.cloudParameters;

    if (cloudParameters) {
        params = {
            sessionKey: cloudParameters.sessionKey,
            reportTemplateItemKey: cloudParameters.reportTemplateItemKey,
            resourceItems: cloudParameters.resourceItems,
            attachedItems: cloudParameters.attachedItems
        }

        if (jsObject.options.isOnlineVersion) {
            params.isOnlineVersion = true;            
        }
    }
    
    this.SendCommandToDesignerServer("GetReportForDesigner", params, function (answer) {
        if (answer.reportObject) {
            jsObject.LoadReport(jsObject.ParseReport(answer.reportObject));
        }
        if ((jsObject.options.cloudParameters && jsObject.options.cloudParameters.thenOpenWizard) || jsObject.options.runWizardAfterLoad) {
            var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
            fileMenu.changeVisibleState(true);
        }
    });
}
