
StiMobileDesigner.prototype.CreateComponentEvents = function (component) {

    //Component Double Click
    component.ondblclick = function (event) {
        this.completeMouseDown = false;
        if (this.completeDblClick) return;
        this.completeDblClick = true;
        var canSelected = (this.properties.restrictions && (this.properties.restrictions == "All" || this.properties.restrictions.indexOf("AllowSelect") >= 0)) ||
            !this.properties.restrictions;
        if (!canSelected) return;
        this.setSelected();
        if (!this.jsObject.options.selectedObjects) {
            this.jsObject.ShowComponentForm(this);
        }
        if (this.jsObject.options.selectingRect) {
            this.jsObject.options.selectingRect.parentPage.removeChild(this.jsObject.options.selectingRect);
            this.jsObject.options.selectingRect = false;
        }
    }

    //Component Touch Start
    component.ontouchstart = function (event, mouseProcess) {
        if (this.jsObject.options.clipboardMode) return;

        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);

        var canSelected = (this.properties.restrictions && (this.properties.restrictions == "All" || this.properties.restrictions.indexOf("AllowSelect") >= 0)) ||
            !this.properties.restrictions;
        if (!canSelected) return;

        if (this.jsObject.options.paintPanel.copyStyleMode) {
            this.jsObject.SetStylePropertiesToComponent(this, this.jsObject.options.copyStyleProperties);
            return;
        }

        this.jsObject.options.mouseMoved = false;
        this.jsObject.options.componentIsTouched = false;
        if (this.jsObject.options.isTouchDevice && event) {
            event.preventDefault();
            if (event.touches.length > 1) {
                this.jsObject.options.zoomWithTouch = true;
                return;
            }
            this.jsObject.options.startMousePos = [event.touches[0].pageX, event.touches[0].pageY];
            this.jsObject.options.currentComponent = this;
        }

        if (!this.jsObject.options.selectedObjects) {
            if (this_.jsObject.options.currentPage) this_.jsObject.options.currentPage.updateComponentsLevels();
            if (this_.jsObject.options.currentComponent) this_.jsObject.options.currentComponent.setOnTopLevel();
        }

        var selectedObject = this.jsObject.options.selectedObject;
        if (selectedObject != null && selectedObject.typeComponent != "StiPage" && selectedObject.typeComponent != "StiReport") {
            selectedObject.changeVisibilityStateResizingIcons(false);
        }

        if (!this.jsObject.options.in_resize && !this.jsObject.options.drawComponent) {
            if (this.jsObject.options.selectedObjects && this.jsObject.IsContainted(this.jsObject.options.selectedObjects, this.jsObject.options.currentComponent)) {
                var selectedObjects = this.jsObject.options.selectedObjects;
                this.jsObject.options.in_drag = [selectedObjects, [], [], []];
                for (var i = 0; i < selectedObjects.length; i++) {
                    this.jsObject.options.in_drag[1].push(parseInt(selectedObjects[i].getAttribute("left")));
                    this.jsObject.options.in_drag[2].push(parseInt(selectedObjects[i].getAttribute("top")));
                    this.jsObject.options.in_drag[3].push(selectedObjects[i].getAllChildsComponents());
                }
                if (this.properties.isPrimitiveComponent) {
                    this.controls.background.style.fill = "transparent";
                    this.controls.background.style.stroke = "transparent";
                }
            }
            else if (this.jsObject.options.currentComponent) {
                var xPosComponent = parseInt(this.jsObject.options.currentComponent.getAttribute("left"));
                var yPosComponent = parseInt(this.jsObject.options.currentComponent.getAttribute("top"));
                this.jsObject.options.in_drag = [this.jsObject.options.currentComponent, xPosComponent, yPosComponent, this.jsObject.options.currentComponent.getAllChildsComponents()];
            }
        }

        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    //Component Touch End
    component.ontouchend = function (event) {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        if (event) event.preventDefault();
        var canSelected = (this.properties.restrictions && (this.properties.restrictions == "All" || this.properties.restrictions.indexOf("AllowSelect") >= 0)) ||
            !this.properties.restrictions;
        if (!canSelected) return;
        if (this.jsObject.options.zoomWithTouch) return;
        this.jsObject.options.componentIsTouched = true;

        var thisComponent;
        if (this.jsObject.options.in_resize) thisComponent = this.jsObject.options.in_resize[0];
        if (this.jsObject.options.in_drag) thisComponent = this.jsObject.options.in_drag[0];

        if (!this.jsObject.options.mouseMoved) {
            if (!this.jsObject.Is_array(thisComponent)) {
                thisComponent.setSelected();
                this.jsObject.UpdatePropertiesControls();
            }
        }
        else {
            var components = this.jsObject.Is_array(thisComponent) ? thisComponent : [thisComponent];
            var marginsPx;

            for (var i = 0; i < components.length; i++) {
                this.jsObject.ApplyComponentSizes(components[i]);
            }

            if (this.jsObject.Is_array(thisComponent)) {
                this.jsObject.PaintSelectedLines();
                this.jsObject.UpdatePropertiesControls();
            }

            if (this.jsObject.options.in_resize) {
                var components = [this.jsObject.options.in_resize[0]];
                if (this.jsObject.options.in_resize.length > 3 && this.jsObject.options.in_resize[0].typeComponent != "StiTable") {
                    components = components.concat(this.jsObject.options.in_resize[3]);
                }
                this.jsObject.SendCommandChangeRectComponent(components, "ResizeComponent", null, this.jsObject.options.in_resize[1]);
            }

            if (this.jsObject.options.in_drag) {
                this.jsObject.SendCommandChangeRectComponent(this.jsObject.options.in_drag[0], "MoveComponent");
            }
        }

        this.jsObject.options.in_resize = false;
        this.jsObject.options.in_drag = false;
        this.jsObject.options.movingCloneComponent = false;

        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    //Component Touch Move
    component.ontouchmove = function (event) {
        if (event) event.preventDefault();
        if (this.jsObject.options.zoomWithTouch) return;
        this.jsObject.options.mouseMoved = true;
        mouseCurrentXPos = event.touches[0].pageX;
        mouseCurrentYPos = event.touches[0].pageY;

        if (this.jsObject.options.in_drag) {
            this.jsObject.MoveComponent(mouseCurrentXPos, mouseCurrentYPos);
        }

        if (this.jsObject.options.in_resize)
            if (this.jsObject.Is_array(this.jsObject.options.in_resize[0]))
                this.jsObject.ResizeComponents(mouseCurrentXPos, mouseCurrentYPos);
            else
                this.jsObject.ResizeComponent(mouseCurrentXPos, mouseCurrentYPos);
    }

    //Component MouseDown
    component.onmousedown = function (event) {
        if (this.isTouchStartFlag) return;
        if (this.jsObject.options.controlsIsFocused) {
            this.jsObject.options.controlsIsFocused.blur(); //fixed bug when drag&drop component from toolbar
        }
        if ((this.jsObject.options.CTRL_pressed || this.jsObject.options.SHIFT_pressed) &&
            event &&
            event.button == 2 &&
            this.jsObject.options.selectedObjects &&
            this.jsObject.IsContainted(this.jsObject.options.selectedObjects, this))
        {            
            this.ontouchstart(null, true);
            return;
        }

        if (event && event.button != 2 && this.completeMouseDown) {
            component.completeDblMouseDown = true;
            return;
        }

        var canSelected = (this.properties.restrictions && (this.properties.restrictions == "All" || this.properties.restrictions.indexOf("AllowSelect") >= 0)) ||
            !this.properties.restrictions;
        if (!canSelected) return;

        this.jsObject.options.startCopyWithCTRL = this.jsObject.options.CTRL_pressed;
        this.jsObject.options.currentComponent = this;
        event.preventDefault();
        this.jsObject.options.startMousePos = [event.clientX || event.x, event.clientY || event.y];
        this.ontouchstart(null, true);

        component.completeMouseDown = true;
        component.completeDblMouseDown = false;
        component.completeDblClick = false;
        setTimeout(function () { component.completeMouseDown = false; }, 300);
    }

    //Component MouseUp
    component.onmouseup = function (event) {
        if (this.isTouchEndFlag || this.jsObject.options.isTouchClick) return;
        this.jsObject.options.componentIsTouched = true;
        if (this.jsObject.options.CTRL_pressed) this.jsObject.options.CTRL_pressed = this;
        if (this.jsObject.options.SHIFT_pressed) this.jsObject.options.SHIFT_pressed = this;
        if (this.jsObject.options.startCopyWithCTRL) this.jsObject.options.startCopyWithCTRL = this;

        if (this.completeDblMouseDown) {
            this.ondblclick();
            return;
        }
    }
}