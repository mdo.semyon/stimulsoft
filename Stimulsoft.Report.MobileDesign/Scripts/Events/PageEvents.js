
// Events
StiMobileDesigner.prototype.CreatePageEvents = function (page) {

    //Page Touch Start
    page.ontouchstart = function (event, mouseProcess, eventButton) {
        //debugger;
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);

        this.jsObject.options.pagePressed = true;

        if (this.jsObject.options.isTouchDevice && event) {
            this.jsObject.options.eventTouch = event;
            this.jsObject.options.thisPage = this;
            this.jsObject.options.touchZoom.firstDistance = 0;
            this.jsObject.options.touchZoom.secondDistance = 0;
            this.jsObject.options.touchZoom.zoomStep = 0;
            this.jsObject.options.zoomWithTouch = false;

            if (event.touches.length > 1) {
                if (this.jsObject.options.selectedObject) this.jsObject.options.selectedObject.setSelected();
                this.jsObject.options.zoomWithTouch = true;
                this.jsObject.options.in_resize = false;
                this.jsObject.options.in_drag = false;
                this.jsObject.options.movingCloneComponent = false;
                return;
            }
        }

        this.jsObject.options.componentIsTouched = false;

        if (this.jsObject.options.drawComponent) {
            var selectedObject = this.jsObject.options.selectedObject;
            if (selectedObject && selectedObject.typeComponent != "StiPage" && selectedObject.typeComponent != "StiReport")
                this.jsObject.options.selectedObject.changeVisibilityStateResizingIcons(false);
            this.jsObject.options.eventTouch.preventDefault();
            this.jsObject.options.startPoint = this.jsObject.FindMousePosOnSvgPage(this.jsObject.options.thisPage, this.jsObject.options.eventTouch);
            this.jsObject.options.currentPoint = this.jsObject.options.startPoint;
            this.jsObject.options.cursorRect = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'polygon') : document.createElement("polygon");
            this.jsObject.options.cursorRect.style.strokeWidth = "1";
            this.jsObject.options.cursorRect.style.stroke = this.jsObject.options.themeColors[this.jsObject.GetThemeColor()];
            this.jsObject.options.cursorRect.style.strokeDasharray = "3,3";
            this.jsObject.options.cursorRect.style.fill = "transparent";
            this.jsObject.options.thisPage.appendChild(this.jsObject.options.cursorRect);
        }
        else if (eventButton != 2 &&
                !this.jsObject.options.in_drag &&
                !this.jsObject.options.in_resize &&
                !this.jsObject.options.paintPanel.copyStyleMode &&
                !(this.jsObject.options.selectedObjects && this.jsObject.IsContainted(this.jsObject.options.selectedObjects, this.jsObject.options.currentComponent))) {


            this.jsObject.options.startPoint = this.jsObject.FindMousePosOnSvgPage(this.jsObject.options.thisPage, this.jsObject.options.eventTouch);
            this.jsObject.options.currentPoint = this.jsObject.options.startPoint;
            if (this.jsObject.options.selectingRect) this.jsObject.options.selectingRect.parentPage.removeChild(this.jsObject.options.selectingRect);
            this.jsObject.options.selectingRect = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'polygon') : document.createElement("polygon");
            this.jsObject.options.selectingRect.style.fill = this.jsObject.options.themeColors[this.jsObject.GetThemeColor()];
            this.jsObject.options.selectingRect.style.fillOpacity = "0.1";
            this.jsObject.options.selectingRect.style.strokeWidth = "1";
            this.jsObject.options.selectingRect.style.stroke = this.jsObject.options.themeColors[this.jsObject.GetThemeColor()];
            this.jsObject.options.selectingRect.parentPage = this.jsObject.options.thisPage;
            this.jsObject.options.thisPage.appendChild(this.jsObject.options.selectingRect);
        }

        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    //Page Touch End
    page.ontouchend = function (event, mouseProcess) {
        var this_ = this;
        this.isTouchEndFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchEndTimer);

        if (this.jsObject.options.isTouchDevice) this.jsObject.options.thisPage = this;
        this.jsObject.options.pageIsTouched = true;
        var point = this.jsObject.FindMousePosOnSvgPage(this.jsObject.options.thisPage, this.jsObject.options.eventTouch);

        var movingCloneComponent = this.jsObject.options.movingCloneComponent;
        if (movingCloneComponent) {
            this.jsObject.ApplyComponentSizes(movingCloneComponent);

            this.jsObject.SendCommandCreateMovingCopyComponent(
                movingCloneComponent.properties.name,
                movingCloneComponent.properties.unitLeft + "!" + movingCloneComponent.properties.unitTop + "!" + movingCloneComponent.properties.unitWidth + "!" + movingCloneComponent.properties.unitHeight,
                function () {
                    var page = this_.jsObject.options.report.pages[movingCloneComponent.properties.pageName];
                    page.removeChild(movingCloneComponent);
                    for (var i = 0 ; i < movingCloneComponent.cloneChilds.length; i++) {
                        page.removeChild(movingCloneComponent.cloneChilds[i]);
                    }
                }
            );
            this.jsObject.options.movingCloneComponent = false;
            this.jsObject.options.in_drag = false;
        }

        if (this.jsObject.options.componentButtonInDrag) {
            this.jsObject.SendCommandCreateComponent(this.jsObject.options.thisPage.properties.name,
                this.jsObject.options.componentButtonInDrag.ownerButton.name.replace("_", ""), point.xUnits + "!" + point.yUnits + "!" + 0 + "!" + 0);
            this.jsObject.options.mobileDesigner.pressedDown();
        }

        if (this.jsObject.options.itemInDrag) {
            var itemObject = this.jsObject.options.itemInDrag.itemObject;
            if (itemObject.typeItem == "DataSource" || itemObject.typeItem == "BusinessObject") {
                var jsObject = this.jsObject;
                jsObject.InitializeCreateDataForm(function (dataFrom) {
                    var currentItemObject = (itemObject.typeItem == "DataSource")
                        ? jsObject.GetDataSourceByNameFromDictionary(itemObject.name)
                        : jsObject.GetBusinessObjectByNameFromDictionary(itemObject.name);
                    dataFrom.show(currentItemObject || itemObject, { x: point.xUnits.toString(), y: point.yUnits.toString() }, jsObject.options.thisPage.properties.name);
                });
            }
            else if ((itemObject.typeItem == "Resource" && itemObject.type == "Image") ||
                    (itemObject.typeItem == "Variable" && itemObject.type == "image") ||
                    (itemObject.typeItem == "Column" && (itemObject.type == "byte[]" || itemObject.type == "image"))) {
                this.jsObject.SendCommandCreateImageComponent(itemObject, { x: point.xUnits.toString(), y: point.yUnits.toString() }, this.jsObject.options.thisPage.properties.name);
            }
            else if (itemObject.typeItem != "Category") {
                this.jsObject.SendCommandCreateTextComponent(itemObject, { x: point.xUnits.toString(), y: point.yUnits.toString() }, this.jsObject.options.thisPage.properties.name);
            }
        }

        var multiSelect = false;
        if (this.jsObject.options.selectingRect && !this.jsObject.options.CTRL_pressed) {
            this.jsObject.MultiSelectComponents(this.jsObject.options.thisPage);
            multiSelect = true;
        }

        if (this.jsObject.options.drawComponent) {
            this.jsObject.options.thisPage.removeChild(this.jsObject.options.cursorRect);
            this.jsObject.options.cursorRect = false;

            var compWidth = Math.abs(this.jsObject.options.startPoint.xUnits - this.jsObject.options.currentPoint.xUnits)
            var compHeight = Math.abs(this.jsObject.options.startPoint.yUnits - this.jsObject.options.currentPoint.yUnits)
            var compLeft = (this.jsObject.options.startPoint.xUnits < this.jsObject.options.currentPoint.xUnits)
                ? this.jsObject.options.startPoint.xUnits
                : this.jsObject.options.currentPoint.xUnits;
            var compTop = (this.jsObject.options.startPoint.yUnits < this.jsObject.options.currentPoint.yUnits)
                ? this.jsObject.options.startPoint.yUnits
                : this.jsObject.options.currentPoint.yUnits;

            var selectedComponent = this.jsObject.options.insertPanel && this.jsObject.options.insertPanel.selectedComponent
                ? this.jsObject.options.insertPanel.selectedComponent
                : (this.jsObject.options.toolbox && this.jsObject.options.toolbox.selectedComponent ? this.jsObject.options.toolbox.selectedComponent : null);

            this.jsObject.SendCommandCreateComponent(
                this.jsObject.options.thisPage.properties.name,
                selectedComponent.name.replace("_", ""),
                compLeft + "!" + compTop + "!" + compWidth + "!" + compHeight,
                (selectedComponent.rowCount && selectedComponent.columnCount ? { rowCount: selectedComponent.rowCount, columnCount: selectedComponent.columnCount } : null));

            if (this.jsObject.options.insertPanel) this.jsObject.options.insertPanel.resetChoose();
            if (this.jsObject.options.toolbox) this.jsObject.options.toolbox.resetChoose();
            this.jsObject.options.startPoint = false;
        }
        else if ((!this.jsObject.options.componentIsTouched || multiSelect) &&
            !this.jsObject.options.in_resize &&
            !this.jsObject.options.in_drag &&
            !this.jsObject.options.zoomWithTouch) {
            if (this.jsObject.options.selectedObjects) {
                this.jsObject.PaintSelectedLines();
                if (this.jsObject.options.selectingRect) {
                    this.jsObject.options.selectingRect.parentPage.removeChild(this.jsObject.options.selectingRect);
                    this.jsObject.options.selectingRect = false;
                }
            }
            else {
                this.jsObject.options.thisPage.setSelected();
                this.jsObject.options.thisPage.updateComponentsLevels();
            }
            this.jsObject.UpdatePropertiesControls();
        }

        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    //Page Touch Move
    page.ontouchmove = function (event) {
        if (this.jsObject.options.isTouchDevice) {
            if (event && event.touches.length > 1) {
                event.preventDefault();
                this.jsObject.options.in_resize = false;
                this.jsObject.options.in_drag = false;
                this.jsObject.options.movingCloneComponent = false;
                this.jsObject.options.touchZoom.zoomStep++;

                if (this.jsObject.options.touchZoom.firstDistance == 0)
                    this.jsObject.options.touchZoom.firstDistance = Math.sqrt(Math.pow(event.touches[0].pageX - event.touches[1].pageX, 2) + Math.pow(event.touches[0].pageY - event.touches[1].pageY, 2));

                if (this.jsObject.options.touchZoom.zoomStep > 7) {
                    this.jsObject.options.touchZoom.zoomStep = 0;
                    this.jsObject.options.touchZoom.secondDistance = Math.sqrt(Math.pow(event.touches[0].pageX - event.touches[1].pageX, 2) + Math.pow(event.touches[0].pageY - event.touches[1].pageY, 2));
                    if (this.jsObject.options.touchZoom.secondDistance > this.jsObject.options.touchZoom.firstDistance && (Math.round(this.jsObject.options.report.zoom * 10) / 10 < 2))
                        this.jsObject.options.report.zoom += 0.1; this.jsObject.PreZoomPage(this.jsObject.options.currentPage);
                    if (this.jsObject.options.touchZoom.secondDistance < this.jsObject.options.touchZoom.firstDistance && (Math.round(this.jsObject.options.report.zoom * 10) / 10 > 0.1))
                        this.jsObject.options.report.zoom -= 0.1; this.jsObject.PreZoomPage(this.jsObject.options.currentPage);
                    return;
                }
            }

            if (event) this.jsObject.options.eventTouch = event;
            this.jsObject.options.thisPage = this;
        }

        if ((this.jsObject.options.startPoint && this.jsObject.options.drawComponent && this.jsObject.options.cursorRect) || this.jsObject.options.selectingRect) {
            this.jsObject.options.currentPoint = this.jsObject.FindMousePosOnSvgPage(this.jsObject.options.thisPage, this.jsObject.options.eventTouch);
            x1 = this.jsObject.options.startPoint.xPixels + this.jsObject.options.xOffset;
            y1 = this.jsObject.options.startPoint.yPixels + this.jsObject.options.yOffset;
            x2 = this.jsObject.options.currentPoint.xPixels + this.jsObject.options.xOffset;
            y2 = this.jsObject.options.currentPoint.yPixels + this.jsObject.options.yOffset;
            var rect = this.jsObject.options.cursorRect || this.jsObject.options.selectingRect;
            var selectedComponent = this.jsObject.options.insertPanel && this.jsObject.options.insertPanel.selectedComponent
                ? this.jsObject.options.insertPanel.selectedComponent
                : (this.jsObject.options.toolbox && this.jsObject.options.toolbox.selectedComponent ? this.jsObject.options.toolbox.selectedComponent : null);

            if (selectedComponent && selectedComponent.name == "StiHorizontalLinePrimitive") y2 = y1;
            if (selectedComponent && selectedComponent.name == "StiVerticalLinePrimitive") x2 = x1;
            if (rect) rect.setAttribute("points", x1 + "," + y1 + " " + x1 + "," + y2 + " " + x2 + "," + y2 + " " + x2 + "," + y1);
        }
    }


    //Page Mouse Down
    page.onmousedown = function (event) {
        if (!this.isTouchStartFlag && !this.jsObject.options.isTouchClick) {
            this.jsObject.options.eventTouch = event;
            this.jsObject.options.thisPage = this;
            this.jsObject.options.pagePressed = true;
            this.ontouchstart(null, true, event.button);
        }
    }

    //Page Mouse Up
    page.onmouseup = function (event) {
        if (!this.isTouchEndFlag && !this.jsObject.options.isTouchClick) {
            this.jsObject.options.thisPage = this;
            this.ontouchend(null, true);

            if (event.button == 2) {
                this.jsObject.DocumentMouseUp(event);
                var componentContextMenu = this.jsObject.InitializeComponentContextMenu();
                var point = this.jsObject.FindMousePosOnMainPanel(event);
                componentContextMenu.show(point.xPixels + 3, point.yPixels + 3, "Down", "Right");
                return false;
            }
        }
    }

    page.oncontextmenu = function (event) {
        return false;
    }

    //Page Mouse Move
    page.onmousemove = function (event) {
        this.jsObject.options.eventTouch = event;
        this.jsObject.options.thisPage = this;
        this.ontouchmove();

        if (this.jsObject.options.selectedObject != null && !this.jsObject.options.in_resize && !this.jsObject.options.in_drag &&
           (this.jsObject.options.selectedObject.typeComponent == "StiPage" || this.jsObject.options.selectedObject.typeComponent == "StiReport")) {
            var currentPoint = this.jsObject.FindMousePosOnSvgPage(this.jsObject.options.currentPage, event);
            this.jsObject.options.statusPanel.showPositions(currentPoint.xUnits, currentPoint.yUnits);
        }
    }

    //Page Double Click
    page.ondblclick = function (event) {
        this.jsObject.options.pageIsDblClick = true;
        if (!this.jsObject.options.componentIsTouched) {
            this.jsObject.InitializePageSetupForm(function (pageSetupForm) {
                pageSetupForm.changeVisibleState(true);
            });
        }
    }

    page.onmouseover = function () {
        this.jsObject.options.mouseOverPage = true;
    }

    page.onmouseout = function () {
        this.jsObject.options.mouseOverPage = false;
    }
}