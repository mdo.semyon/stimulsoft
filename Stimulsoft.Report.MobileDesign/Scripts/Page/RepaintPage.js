
//--------------------------Page---------------------------------------

StiMobileDesigner.prototype.RepaintPage = function (page, rebuildGrigLines) {
    page.widthPx = parseInt(this.ConvertUnitToPixel(this.StrToDouble(page.properties.unitWidth)) * this.options.report.zoom);
    page.heightPx = parseInt(this.ConvertUnitToPixel(this.StrToDouble(page.properties.unitHeight)) * this.options.report.zoom);

    var marginsStr = page.properties.unitMargins.split("!");
    var verticalMarginsPx = parseInt(this.ConvertUnitToPixel(this.StrToDouble(marginsStr[1]) + this.StrToDouble(marginsStr[3])) * this.options.report.zoom);
    var horizontalMarginsPx = parseInt(this.ConvertUnitToPixel(this.StrToDouble(marginsStr[0]) + this.StrToDouble(marginsStr[2])) * this.options.report.zoom);

    var segmentPerHeight = this.StrToDouble(page.properties.segmentPerHeight);
    var segmentPerWidth = this.StrToDouble(page.properties.segmentPerWidth);
    if (segmentPerWidth > 1) page.widthPx = ((page.widthPx - horizontalMarginsPx) * segmentPerWidth) + horizontalMarginsPx;
    if (segmentPerHeight > 1) page.heightPx = ((page.heightPx - verticalMarginsPx) * segmentPerHeight) + verticalMarginsPx;

    var largeHeightFactor = (page.properties.largeHeight) ? this.StrToInt(page.properties.largeHeightFactor) : this.StrToDouble(page.properties.largeHeightAutoFactor);
    page.heightPx = (page.heightPx - verticalMarginsPx) * largeHeightFactor + verticalMarginsPx;

    page.setAttribute("width", page.widthPx);
    page.setAttribute("height", page.heightPx);

    if (this.options.report && this.options.report.info.showGrid) this.RepaintGridLines(page, rebuildGrigLines);
    this.RepaintPageBorder(page);
    this.RepaintPageBrush(page);
    this.RepaintPageWaterMark(page);
    this.RepaintPageWaterMarkImage(page);
    this.RepaintMultiSelectObjects(page);
    this.RepaintColumnsLines(page);
    if (page.controls.dWaterMarkParent) this.RepaintPageDWaterMark(page);
}

StiMobileDesigner.prototype.RepaintGridLines = function (page, rebuildGrigLines) {
    if (rebuildGrigLines) this.CreatePageGridLines(page);
    if (!page.controls.gridLines) return;
    for (var i = 0; i < page.controls.gridLines.length; i++) {
        var line = page.controls.gridLines[i].repaint();
    }
}

StiMobileDesigner.prototype.RepaintLargeHeightLines = function (page) {
    var largeHeightFactor = page.properties.largeHeight ? this.StrToInt(page.properties.largeHeightFactor) : this.StrToDouble(page.properties.largeHeightAutoFactor);
    if (largeHeightFactor > 1) {
        var redBorder = function () {
            var redBorder = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "line") : document.createElement("line");
            redBorder.style.strokeDasharray = "2,2";
            redBorder.style.stroke = "#ff0000";

            return redBorder;
        }

        if (!page.controls.redOutBorder && !page.controls.redInnerBorder) {
            page.controls.redOutBorder = redBorder();
            page.controls.redInnerBorder = redBorder();
            page.appendChild(page.controls.redOutBorder);
            page.appendChild(page.controls.redInnerBorder);
        }
        else {
            page.controls.redOutBorder.style.display = "";
            page.controls.redInnerBorder.style.display = "";
        }

        var innerBorderY = (page.heightPx - page.marginsPx[3] - page.marginsPx[1]) / largeHeightFactor + page.marginsPx[1];
        var innerBorderPosition = [page.widthPx - page.marginsPx[2] + this.options.xOffset, innerBorderY + this.options.yOffset, page.marginsPx[0] + this.options.xOffset, innerBorderY + this.options.yOffset];
        var outBorderPosition = [0, innerBorderY + page.marginsPx[3] + this.options.yOffset, page.widthPx, innerBorderY + page.marginsPx[3] + this.options.yOffset];
        var attrs = ["x1", "y1", "x2", "y2"];

        for (var i = 0; i < attrs.length; i++) {
            page.controls.redInnerBorder.setAttribute(attrs[i], innerBorderPosition[i]);
            page.controls.redOutBorder.setAttribute(attrs[i], outBorderPosition[i]);
        }
    }
    else if (page.controls.redOutBorder && page.controls.redInnerBorder) {
        page.controls.redOutBorder.style.display = "none";
        page.controls.redInnerBorder.style.display = "none";
    }
}

StiMobileDesigner.prototype.RepaintPageSegmentLines = function (page) {
    var segmentPerHeight = this.StrToDouble(page.properties.segmentPerHeight);
    var segmentPerWidth = this.StrToDouble(page.properties.segmentPerWidth);
    
    //Remove old lines
    if (page.controls.pageSegmentLines) {
        for (var i = 0; i < page.controls.pageSegmentLines.length; i++) {
            page.removeChild(page.controls.pageSegmentLines[i]);
        }
    }
    
    page.controls.pageSegmentLines = [];

    //Add new lines
    var addSegmentLine = function (x1, y1, x2, y2, isBorder) {
        var line = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "line") : document.createElement("line");
        if (!isBorder) line.style.strokeDasharray = "5,3";
        line.style.stroke = "#0000ff";
        line.setAttribute("x1", x1);
        line.setAttribute("y1", y1);
        line.setAttribute("x2", x2);
        line.setAttribute("y2", y2);
        if (isBorder)
            page.appendChild(line);
        else
            page.insertBefore(line, page.controls.borders[0]);
        page.controls.pageSegmentLines.push(line);

        return line;
    }

    var pageInnerWidth = page.widthPx - page.marginsPx[0] - page.marginsPx[2];
    var pageInnerHeight = page.heightPx - page.marginsPx[1] - page.marginsPx[3];

    if (segmentPerWidth > 1 || segmentPerHeight > 1) {
        var y1 = page.marginsPx[1];
        var y2 = page.heightPx - page.marginsPx[3];
                
        for (var i = 0; i <= segmentPerWidth; i++) {
            var x = page.marginsPx[0] + (pageInnerWidth / segmentPerWidth) * i;
            addSegmentLine(x, y1, x, y2, i == 0 || i == segmentPerWidth);
        }

        var x1 = page.marginsPx[0];
        var x2 = page.widthPx - page.marginsPx[2];
        
        for (var i = 0; i <= segmentPerHeight; i++) {
            var y = page.marginsPx[1] + (pageInnerHeight / segmentPerHeight) * i;
            addSegmentLine(x1, y, x2, y, i == 0 || i == segmentPerHeight);
        }
    }
}

StiMobileDesigner.prototype.RepaintPageBorder = function (page) {
    var margins = page.properties.unitMargins.split("!");
    page.marginsPx = [];
    for (i = 0; i < 4; i++) {
        page.marginsPx[i] = parseInt(this.ConvertUnitToPixel(this.StrToDouble(margins[i])) * this.options.report.zoom);
    }
    var borderStyles = ["", "9,3", "9,2,2,2", "9,2,2,2,2,2", "2,2", "", "none"];
    var borderProps = page.properties.border.split("!");
    var borderVisibleProps = borderProps[0].split(",");
    for (borderNum = 0; borderNum < 8; borderNum++) {
        showBorder = (borderVisibleProps[borderNum < 4 ? borderNum : borderNum - 4] == "1" && borderProps[3] != "6") ? true : false;
        page.controls.borders[borderNum].style.stroke = showBorder ? ((borderProps[2] == "transparent" ? "transparent" : "rgb(" + this.ColorToRGBStr(borderProps[2]) + ")")) : "#787878";
        if (borderNum >= 4) { page.controls.borders[borderNum].style.visibility = (borderVisibleProps[borderNum - 4] == "1" && borderProps[3] == "5") ? "visible" : "hidden"; }
        page.controls.borders[borderNum].style.strokeWidth = (showBorder && borderProps[3] != "5") ? borderProps[1] : "1";
        page.controls.borders[borderNum].style.strokeDasharray = showBorder ? borderStyles[borderProps[3]] : "";
    }

    var borderSize = parseInt(showBorder && borderProps[3] != "5" ? borderProps[1] : "1");
    var XOffset = (borderSize % 2 != 0) ? this.options.xOffset : 0;
    var YOffset = (borderSize % 2 != 0) ? this.options.yOffset : 0;

    var tempX = page.widthPx - page.marginsPx[2];
    var tempY = page.heightPx - page.marginsPx[3];

    var bordersPosition = [
            [page.marginsPx[0] + XOffset, page.marginsPx[1] + YOffset, page.marginsPx[0] + XOffset, tempY + YOffset],
            [page.marginsPx[0] + XOffset, page.marginsPx[1] + YOffset, tempX + XOffset, page.marginsPx[1] + YOffset],
            [tempX + XOffset, page.marginsPx[1] + YOffset, tempX + XOffset, tempY + YOffset],
            [tempX + XOffset, tempY + YOffset, page.marginsPx[0] + XOffset, tempY + YOffset],
            [page.marginsPx[0] + XOffset + 2, page.marginsPx[1] + YOffset + 2, page.marginsPx[0] + XOffset + 2, tempY + YOffset - 2],
            [page.marginsPx[0] + XOffset + 2, page.marginsPx[1] + YOffset + 2, tempX + XOffset - 2, page.marginsPx[1] + YOffset + 2],
            [tempX + XOffset - 2, page.marginsPx[1] + YOffset + 2, tempX + XOffset - 2, tempY + YOffset - 2],
            [tempX + XOffset - 2, tempY + YOffset - 2, page.marginsPx[0] + XOffset + 2, tempY + YOffset - 2]
        ];

    for (borderNum = 0; borderNum < 8; borderNum++) {
        page.controls.borders[borderNum].setAttribute("x1", bordersPosition[borderNum][0]);
        page.controls.borders[borderNum].setAttribute("y1", bordersPosition[borderNum][1]);
        page.controls.borders[borderNum].setAttribute("x2", bordersPosition[borderNum][2]);
        page.controls.borders[borderNum].setAttribute("y2", bordersPosition[borderNum][3]);
    }

    this.RepaintLargeHeightLines(page);
    this.RepaintPageSegmentLines(page);
}

StiMobileDesigner.prototype.RepaintPageBrush = function (page) {
    brushProps = page.properties.brush.split("!");
    page.controls.gradient.rect.style.display = "none";

    //remove old hatch brush
    while (page.controls.svgHatchBrush.childNodes[0]) page.controls.svgHatchBrush.removeChild(page.controls.svgHatchBrush.childNodes[0]);

    switch (brushProps[0]) {
        case "0": { page.style.background = "rgb(255,255,255)"; break; }
        case "1": { page.style.background = "rgb(" + this.ColorToRGBStr(brushProps[1]) + ")"; break; }
        case "2":
            {
                var svgHatchBrush = page.jsObject.GetSvgHatchBrush(brushProps, page.widthPx, page.heightPx);
                page.controls.svgHatchBrush.appendChild(svgHatchBrush);
                svgHatchBrush.setAttribute("width", page.widthPx);
                svgHatchBrush.setAttribute("height", page.heightPx);
                break;
            }
        case "3":
            {
                page.controls.gradient.stop1.setAttribute("stop-color", "rgb(" + this.ColorToRGBStr(brushProps[1]) + ")");
                if (page.controls.gradient.stop2.parentNode) page.controls.gradient.stop2.parentNode.removeChild(page.controls.gradient.stop2);
                page.controls.gradient.stop3.setAttribute("stop-color", "rgb(" + this.ColorToRGBStr(brushProps[2]) + ")");                
                var angle = this.StrToInt(brushProps[3]);
                page.controls.gradient.setAttribute("x2", Math.abs(angle - 90) + "%");
                page.controls.gradient.setAttribute("y2", angle + "%");
                page.controls.gradient.rect.style.display = "";
                break;
            }

        case "4":
            {
                page.controls.gradient.stop1.setAttribute("stop-color", "rgb(" + this.ColorToRGBStr(brushProps[1]) + ")");
                page.controls.gradient.stop2.setAttribute("stop-color", "rgb(" + this.ColorToRGBStr(brushProps[2]) + ")");
                page.controls.gradient.insertBefore(page.controls.gradient.stop2, page.controls.gradient.stop3);
                page.controls.gradient.stop3.setAttribute("stop-color", "rgb(" + this.ColorToRGBStr(brushProps[1]) + ")");                
                var angle = this.StrToInt(brushProps[3]);
                page.controls.gradient.setAttribute("x2", Math.abs(angle - 90) + "%");
                page.controls.gradient.setAttribute("y2", angle + "%");
                page.controls.gradient.rect.style.display = "";
                break;
            }
        case "5": { page.style.background = "#dcdcdc"; break; }
    }
}

StiMobileDesigner.prototype.RepaintPageWaterMark = function (page) {
    textWaterMark = Base64.decode(page.properties.waterMarkText);

    if (!page.properties.waterMarkEnabled || textWaterMark == "") { page.controls.waterMarkParent.style.display = "none"; return; }
    else page.controls.waterMarkParent.style.display = "";
    
    page.controls.waterMarkText.textContent = textWaterMark;
    page.controls.waterMarkChild.setAttribute("transform", "rotate(-" + page.properties.waterMarkAngle + ")");

    fontArray = page.properties.waterMarkFont.split("!");
    page.controls.waterMarkText.style.fontFamily = fontArray[0]; 
    
    fontSize = (fontArray[1] * this.options.report.zoom);
    page.controls.waterMarkText.style.fontSize = fontSize + "pt";
    
    page.controls.waterMarkText.style.fontWeight = (fontArray[2] == "1") ? "bold" : "";
    page.controls.waterMarkText.style.fontStyle = (fontArray[3] == "1") ? "italic" : "";
    page.controls.waterMarkText.style.textDecoration = (fontArray[4] == "1") ? "underline" : "";
    page.controls.waterMarkText.style.textAnchor = "middle";    

    textBrushArray = page.properties.waterMarkTextBrush.split("!");
    if (textBrushArray[0] == "0") {
        page.controls.waterMarkText.style.fill = "transparent";
    }
    else if (textBrushArray[0] == "1") {
        color = textBrushArray[1].split(",")
        if (color.length == 4) {
            page.controls.waterMarkText.style.fill = "rgb(" + color[1] + "," + color[2] + "," + color[3] + ")";
            page.controls.waterMarkText.style.fillOpacity = parseInt(color[0]) / 255;
        }
        else {
            page.controls.waterMarkText.style.fill = (color == "transparent") ? color : "rgb(" + color[0] + "," + color[1] + "," + color[2] + ")";
            page.controls.waterMarkText.style.fillOpacity = 1;
        }
    }
    else if (textBrushArray[0] == "3") {
        page.controls.waterMarkGradient.stop1.setAttribute("stop-color", "rgb(" + this.ColorToRGBStr(textBrushArray[1]) + ")");
        if (page.controls.waterMarkGradient.stop2.parentNode) page.controls.waterMarkGradient.stop2.parentNode.removeChild(page.controls.waterMarkGradient.stop2);
        page.controls.waterMarkGradient.stop3.setAttribute("stop-color", "rgb(" + this.ColorToRGBStr(textBrushArray[2]) + ")");
        var angle = this.StrToInt(textBrushArray[3]);
        page.controls.waterMarkGradient.setAttribute("x2", Math.abs(angle - 90) + "%");
        page.controls.waterMarkGradient.setAttribute("y2", angle + "%");
        page.controls.waterMarkText.style.fill = "url(#" + page.controls.waterMarkGradient.id + ")";
        page.controls.waterMarkText.style.stroke = "none";
        page.controls.waterMarkText.style.fillOpacity = Math.min(this.GetOpacityFromColor(textBrushArray[1]), this.GetOpacityFromColor(textBrushArray[2]));

    }
    else if (textBrushArray[0] == "4") {
        page.controls.waterMarkGradient.stop1.setAttribute("stop-color", "rgb(" + this.ColorToRGBStr(textBrushArray[1]) + ")");
        page.controls.waterMarkGradient.stop2.setAttribute("stop-color", "rgb(" + this.ColorToRGBStr(textBrushArray[2]) + ")");
        page.controls.waterMarkGradient.insertBefore(page.controls.waterMarkGradient.stop2, page.controls.waterMarkGradient.stop3);
        page.controls.waterMarkGradient.stop3.setAttribute("stop-color", "rgb(" + this.ColorToRGBStr(textBrushArray[1]) + ")");
        var angle = this.StrToInt(brushProps[3]);
        page.controls.waterMarkGradient.setAttribute("x2", Math.abs(angle - 90) + "%");
        page.controls.waterMarkGradient.setAttribute("y2", angle + "%");
        page.controls.waterMarkText.style.fillOpacity = Math.min(this.GetOpacityFromColor(textBrushArray[1]), this.GetOpacityFromColor(textBrushArray[2]));
    }
    else {
        page.controls.waterMarkText.style.fill = "#dcdcdc";
        page.controls.waterMarkText.style.fillOpacity = 1;
    }
 
    page.controls.waterMarkParent.setAttribute("transform", "translate(" + (page.widthPx / 2) + ", " + (page.heightPx / 2) + ")");
}

StiMobileDesigner.prototype.RepaintPageWaterMarkImage = function (page) {
    var watermarkImageSrc = page.properties.watermarkImageSrc || page.properties.watermarkImageContentForPaint;    
    if (watermarkImageSrc) {
        var isWmfImage = watermarkImageSrc.indexOf("data:image/x-wmf") >= 0;

        if (isWmfImage && page.properties.watermarkImageContentForPaint) {
            watermarkImageSrc = page.properties.watermarkImageContentForPaint; //Wmf image type
        }

        page.controls.waterMarkImage.style.display = "";
        page.controls.waterMarkImage.href.baseVal = watermarkImageSrc;
        var sizeWatermark = page.properties.watermarkImageSize.split(";");
        var multipleFactor = this.StrToInt(page.properties.waterMarkMultipleFactor);
        var widthWatermark = sizeWatermark[0] * multipleFactor * this.options.report.zoom;
        var heightWatermark = sizeWatermark[1] * multipleFactor * this.options.report.zoom;
        
        if (page.properties.waterMarkStretch) {            
            page.controls.waterMarkImage.style.display = "";
            page.controls.waterMarkImage.setAttribute("preserveAspectRatio", "none");
            page.controls.waterMarkImage.setAttribute("width", page.widthPx);
            page.controls.waterMarkImage.setAttribute("height", page.heightPx);
            page.controls.waterMarkImage.setAttribute("x", "0");
            page.controls.waterMarkImage.setAttribute("y", "0");
        }
        else {                
            if (page.properties.waterMarkTiling) {
                page.controls.waterMarkImage.style.display = "none";
                page.controls.gradient.rect.style.display = "none";
                page.style.backgroundImage = "url(" + watermarkImageSrc + ")";
                page.style.backgroundRepeat = "repeat";
                page.style.backgroundSize = parseInt((widthWatermark / page.widthPx) * 100) + "%";
            }
            else {
                page.controls.waterMarkImage.setAttribute("preserveAspectRatio", "");
                page.controls.waterMarkImage.setAttribute("width", widthWatermark);
                page.controls.waterMarkImage.setAttribute("height", heightWatermark);
                this.SetWatermarkImagePos(page, widthWatermark, heightWatermark);
            }
        }
        
    }
    else {
        page.controls.waterMarkImage.style.display = "none";
        page.controls.waterMarkImage.href.baseVal = "";
    }
}

StiMobileDesigner.prototype.RepaintPageDWaterMark = function (page) {
    page.controls.dWaterMarkText.textContent = this.getT(true);
    page.controls.dWaterMarkChild.setAttribute("transform", "rotate(-45)");
    page.controls.dWaterMarkText.style.fontFamily = "Arial"; 
    fontSize = 100 * this.options.report.zoom;
    page.controls.dWaterMarkText.style.fontSize = fontSize + "pt";
    page.controls.dWaterMarkText.style.fontWeight = "bold";
    page.controls.dWaterMarkText.style.textAnchor = "middle";    
    page.controls.dWaterMarkText.style.fill = "rgb(0,0,0)";
    page.controls.dWaterMarkText.style.fillOpacity = "0.3";
    page.controls.dWaterMarkParent.setAttribute("transform", "translate(" + ((page.widthPx / 2) + this.options.report.zoom * 40) + ", " + (page.heightPx / 2) + ")");
}

StiMobileDesigner.prototype.RepaintMultiSelectObjects = function (page) {
    if (this.options.multiSelectHelperControls && this.options.multiSelectHelperControls.page == page) {
        var lines = this.options.multiSelectHelperControls.lines;
        for (var i = 0; i < lines.length; i ++) {
            lines[i].repaint();
        }
    }
}
