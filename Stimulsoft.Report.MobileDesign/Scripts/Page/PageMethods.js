
StiMobileDesigner.prototype.RebuildPage = function (page, componentsProps) {
    for (var componentName in componentsProps) {
        var component = page.components[componentName];
        if (component) {
            component.properties.parentName = componentsProps[componentName].parentName;
            component.properties.parentIndex = componentsProps[componentName].parentIndex;
            component.properties.componentIndex = componentsProps[componentName].componentIndex;
            component.properties.childs = componentsProps[componentName].childs;
            component.properties.clientLeft = componentsProps[componentName].clientLeft;
            component.properties.clientTop = componentsProps[componentName].clientTop;
            if (componentsProps[componentName].svgContent) component.properties.svgContent = componentsProps[componentName].svgContent;

            oldRect = component.properties.unitLeft + "!" + component.properties.unitTop + "!" +
                component.properties.unitWidth + "!" + component.properties.unitHeight;
            newRect = componentsProps[componentName].componentRect;

            if (oldRect != newRect) {
                rect = componentsProps[componentName].componentRect.split("!");
                component.properties.unitLeft = rect[0];
                component.properties.unitTop = rect[1];
                component.properties.unitWidth = rect[2];
                component.properties.unitHeight = rect[3];
                component.repaint();
            }
        }
    }

    page.updateComponentsLevels();
}


StiMobileDesigner.prototype.AddComponents = function (page) {
    var parentIndex = 0;
    var flag = false;
    do {
        flag = false;
        var tempArray = [];
        for (var componentName in page.components) {
            var component = page.components[componentName];
            if (component.properties.parentIndex == parentIndex) {
                tempArray.push({ index: this.StrToInt(component.properties.componentIndex), component: component });
                flag = true;
            }
        }
        tempArray.sort(this.SortByIndex);
        for (var i = 0; i < tempArray.length; i++) {
            page.appendChild(tempArray[i].component);
        }
        parentIndex++;
    }
    while (flag)
}

StiMobileDesigner.prototype.RemoveComponents = function (page) {
    for (var componentName in page.components) {
        var component = page.components[componentName];
        if (component) page.removeChild(component);
    }
}

StiMobileDesigner.prototype.UpdateComponentsLevels = function (page) {
    this.RemoveComponents(page);
    this.AddComponents(page);
}

StiMobileDesigner.prototype.AddPage = function (answer, notShowAfterCreated) {
    var page = this.CreatePage(answer);
    page.repaint();
    this.options.report.pages[page.properties.name] = page;
    this.options.report.pages[page.properties.name].components = {};
    this.ChangePageIndexes(answer.pageIndexes);
    this.options.paintPanel.addPage(page);
    if (!notShowAfterCreated) {
        this.options.paintPanel.showPage(page);
        page.setSelected();
    }
    this.UpdatePropertiesControls();
    this.options.pagesPanel.pagesContainer.updatePages();
}

StiMobileDesigner.prototype.RemovePage = function (page) {
    if (this.options.paintPanel.getPagesCount() < 2) return;
    this.SendCommandRemovePage(page);

    this.options.paintPanel.removePage(page);
    delete page;
    delete this.options.report.pages[page.properties.name];
    this.ChangePageIndexes();
    var firstPage = this.options.paintPanel.findPageByIndex(0);
    if (firstPage) {
        firstPage.setSelected();
        this.UpdatePropertiesControls();
        this.options.paintPanel.showPage(firstPage);
    }
    this.options.pagesPanel.pagesContainer.updatePages();
}

StiMobileDesigner.prototype.RepaintAllComponentsOnPage = function (page) {
    for (var componentName in this.options.report.pages[page.properties.name].components) {
        var component = this.options.report.pages[page.properties.name].components[componentName];
        if (component) component.repaint();
    }
}

StiMobileDesigner.prototype.ChangePageIndexes = function (pagesIndexes) {
    if (pagesIndexes) {
        for (var pageName in pagesIndexes)
            this.options.report.pages[pageName].properties.pageIndex = pagesIndexes[pageName];
    }
    else {
        var offset = 0;
        for (var i = 0; i < this.options.paintPanel.getPagesCount() ; i++) {
            if (!this.options.paintPanel.findPageByIndex(i)) offset = 1;
            if (offset == 1) {
                var page = this.options.paintPanel.findPageByIndex(i + offset);
                if (page) page.properties.pageIndex = i.toString();
            }
        }
    }
}

StiMobileDesigner.prototype.RenamePage = function (page, newName) {
    var oldName = page.properties.name;
    this.options.report.pages[newName] = page;
    delete this.options.report.pages[oldName];
    page.properties.name = newName;
    this.options.pagesPanel.pagesContainer.updatePages();

    for (var componentName in page.components) {
        var component = page.components[componentName];
        if (component) {
            if (oldName == component.properties.parentName) component.properties.parentName = newName;
            component.properties.pageName = newName;
        }
    }
}

StiMobileDesigner.prototype.CheckLargeHeight = function (page, largeHeightAutoFactor) {
    if (page && page.properties.largeHeight == "0" && largeHeightAutoFactor != page.properties.largeHeightAutoFactor) {
        page.properties.largeHeightAutoFactor = largeHeightAutoFactor;
        page.repaint(true);
    }
}

StiMobileDesigner.prototype.MultiSelectComponents = function (page) {
    if (page && this.options.selectingRect) {
        var selectedObjects = this.GetComponentsInSelectingRect(page, this.options.selectingRect);
        this.DeleteSelectedLines();
        this.options.selectedObjects = selectedObjects;
        page.removeChild(this.options.selectingRect);
    }
    this.options.selectingRect = false;
}

StiMobileDesigner.prototype.GetComponentsInSelectingRect = function (page, selectingRect) {
    var components = [];
    var bands = [];
    var pointsAttribute = selectingRect.getAttribute("points");
    if (!pointsAttribute) return null;
    var points = pointsAttribute.split(" ");
    var leftRect, rightRect, topRect, bottomRect;
    for (var i = 0; i < points.length; i++) {
        var point = points[i].split(",");
        var xPoint = this.StrToDouble(point[0]);
        var yPoint = this.StrToDouble(point[1]);
        if (!leftRect) {
            leftRect = rightRect = xPoint;
            topRect = bottomRect = yPoint;
        }
        else {
            if (xPoint <= leftRect) leftRect = xPoint;
            if (xPoint >= rightRect) rightRect = xPoint;
            if (yPoint <= topRect) topRect = yPoint;
            if (yPoint >= bottomRect) bottomRect = yPoint;
        }
    }

    for (var name in page.components) {
        var component = page.components[name];
        if (component.style.display == "none") continue;
        var leftComp = this.StrToDouble(component.getAttribute("left"));
        var topComp = this.StrToDouble(component.getAttribute("top"));
        var rightComp = leftComp + this.StrToDouble(component.getAttribute("width"));
        var bottomComp = topComp + this.StrToDouble(component.getAttribute("height"));

        if (!(leftComp > rightRect || rightComp < leftRect || topComp > bottomRect || bottomComp < topRect)) {
            if (this.IsBandComponent(component)) bands.push(component);
            else components.push(component);
        }
    }

    var result = (components.length == 0) ? bands : components;
    if (this.options.SHIFT_pressed) {
        result = bands.concat(components);
    }

    return result.length == 0 ? null : result;
}