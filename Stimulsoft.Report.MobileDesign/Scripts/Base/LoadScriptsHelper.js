
//Loading Methods
StiMobileDesigner.prototype.LoadScriptWithProcessImage = function (src, callback, appendTo) {
    var processImage = this.options.processImage || this.InitializeProcessImage();
    processImage.show();

    this.LoadScript(src, function () {
        processImage.hide();
        callback();
    }, appendTo);
}

StiMobileDesigner.prototype.LoadScript = function (src, callback, appendTo) {    
    var script = document.createElement('script');
    if (!appendTo) {
        appendTo = document.getElementsByTagName('head')[0];
    }
    if (script.readyState && !script.onload) {
        // IE, Opera
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        }
    }
    else {
        // Rest
        script.onload = callback;
    }
    script.src = src;
    appendTo.appendChild(script);
}

StiMobileDesigner.prototype.LoadStyle = function (src) {
    var link = document.createElement("link");
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("type", "text/css");
    link.setAttribute("href", src);
    document.getElementsByTagName("head")[0].appendChild(link)
}

StiMobileDesigner.prototype.ExecuteScript = function (scriptProps, callbackFunction, allwaysCreate, args) {
    var jsObject = this;

    if (jsObject[scriptProps.initMethod + "_"])
        callbackFunction(allwaysCreate ? jsObject[scriptProps.initMethod + "_"](args) : (jsObject.options.forms[scriptProps.formName] || jsObject[scriptProps.initMethod + "_"](args)));
    else
        jsObject.LoadScriptWithProcessImage(jsObject.options.scriptsUrl.replace("MobileDesignerScriptName", scriptProps.scriptName), function () { callbackFunction(jsObject[scriptProps.initMethod + "_"](args)); });
}

StiMobileDesigner.prototype.InitializeBorderSetupForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeBorderSetupForm", formName: "borderSetup", scriptName: "InitializeBorderSetupForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeConditionsForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeConditionsForm", formName: "conditionsForm", scriptName: "InitializeConditionsForm" }, callbackFunction, true);
}

StiMobileDesigner.prototype.InitializeCreateDataForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeCreateDataForm", formName: "createDataForm", scriptName: "InitializeCreateDataForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeCreateStyleCollectionForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeCreateStyleCollectionForm", formName: "createStyleCollectionForm", scriptName: "InitializeCreateStyleCollectionForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeCrossTabForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeCrossTabForm", formName: "crossTabForm", scriptName: "InitializeCrossTabForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeBarCodeForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeBarCodeForm", formName: "barCodeForm", scriptName: "InitializeBarCodeForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeDataBusinessObjectForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeDataBusinessObjectForm", formName: "dataBusinessObject", scriptName: "InitializeDataBusinessObjectForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeDataColumnForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeDataColumnForm", formName: "dataColumn", scriptName: "InitializeDataColumnForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeDataForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeDataForm", formName: "dataForm", scriptName: "InitializeDataForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditCategoryForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditCategoryForm", formName: "editCategoryForm", scriptName: "InitializeEditCategoryForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditChartForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditChartForm", formName: "editChart", scriptName: "InitializeEditChartForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditGaugeForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditGaugeForm", formName: "editGauge", scriptName: "InitializeEditGaugeForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditColumnForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditColumnForm", formName: "editColumnForm", scriptName: "InitializeEditColumnForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditConnectionForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditConnectionForm", formName: "editConnectionForm", scriptName: "InitializeEditConnectionForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditDataSourceForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditDataSourceForm", formName: "editDataSourceForm", scriptName: "InitializeEditDataSourceForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditDataSourceFromOtherDatasourcesForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditDataSourceFromOtherDatasourcesForm", formName: "editDataSourceFromOtherDatasourcesForm", scriptName: "InitializeEditDataSourceFromOtherDatasourcesForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditParameterForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditParameterForm", formName: "editParameterForm", scriptName: "InitializeEditParameterForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditRelationForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditRelationForm", formName: "editRelationForm", scriptName: "InitializeEditRelationForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditRichTextForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditRichTextForm", formName: "richTextForm", scriptName: "InitializeEditRichTextForm" }, callbackFunction, true);
}

StiMobileDesigner.prototype.InitializeEditVariableForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditVariableForm", formName: "editVariableForm", scriptName: "InitializeEditVariableForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeExpressionEditorForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeExpressionEditorForm", formName: "expressionEditor", scriptName: "InitializeExpressionEditorForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeFilterForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeFilterForm", formName: "filterForm", scriptName: "InitializeFilterForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeGroupHeaderForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeGroupHeaderForm", formName: "groupHeaderForm", scriptName: "InitializeGroupHeaderForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeImageForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeImageForm", formName: "imageForm", scriptName: "InitializeImageForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeInteractionForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeInteractionForm", formName: "interactionForm", scriptName: "InitializeInteractionForm" }, callbackFunction, true);
}

StiMobileDesigner.prototype.InitializeMoreColorsForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeMoreColorsForm", formName: "moreColors", scriptName: "InitializeMoreColorsForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeNameInSourceForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeNameInSourceForm", formName: "nameInSourceForm", scriptName: "InitializeNameInSourceForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeOptionsForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeOptionsForm", formName: "optionsForm", scriptName: "InitializeOptionsForm" }, callbackFunction, true);
}

StiMobileDesigner.prototype.InitializePageSetupForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializePageSetupForm", formName: "pageSetup", scriptName: "InitializePageSetupForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeParametersValuesForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeParametersValuesForm", formName: "editParametersValuesForm", scriptName: "InitializeParametersValuesForm" }, callbackFunction, true);
}

StiMobileDesigner.prototype.InitializeReportSetupForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeReportSetupForm", formName: "reportSetupForm", scriptName: "InitializeReportSetupForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeSaveDescriptionForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeSaveDescriptionForm", formName: "saveDescriptionForm", scriptName: "InitializeSaveDescriptionForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeSelectConnectionForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeSelectConnectionForm", formName: "selectConnectionForm", scriptName: "InitializeSelectConnectionForm" }, callbackFunction, true);
}

StiMobileDesigner.prototype.InitializeSelectDataForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeSelectDataForm", formName: "selectDataForm", scriptName: "InitializeSelectDataForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeSortForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeSortForm", formName: "sortForm", scriptName: "InitializeSortForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeStyleDesignerForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeStyleDesignerForm", formName: "styleDesignerForm", scriptName: "InitializeStyleDesignerForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeSubReportForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeSubReportForm", formName: "subReportForm", scriptName: "InitializeSubReportForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeTextEditorForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeTextEditorForm", formName: "textEditor", scriptName: "InitializeTextEditorForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeTextEditorFormOnlyText = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeTextEditorFormOnlyText", formName: "textEditorOnlyText", scriptName: "InitializeTextEditorFormOnlyText" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeTextFormatForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeTextFormatForm", formName: "textFormatForm", scriptName: "InitializeTextFormatForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeVariableItemsForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeVariableItemsForm", formName: "variableItemsForm", scriptName: "InitializeVariableItemsForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeViewDataForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeViewDataForm", formName: "viewDataForm", scriptName: "InitializeViewDataForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeStyleConditionsForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeStyleConditionsForm", formName: "styleConditionsForm", scriptName: "InitializeStyleConditionsForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeCheckPanel = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeCheckPanel", formName: "checkPanel", scriptName: "InitializeCheckPanel" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeCheckPopupPanel = function (checkItems, callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeCheckPopupPanel", formName: "checkPopupPanel", scriptName: "InitializeCheckPopupPanel" }, callbackFunction, true, checkItems);
}

StiMobileDesigner.prototype.InitializeCheckExpressionPopupPanel = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeCheckExpressionPopupPanel", formName: "checkPopupPanel", scriptName: "InitializeCheckExpressionPopupPanel" }, callbackFunction, true);
}

StiMobileDesigner.prototype.InitializeReportCheckForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeReportCheckForm", formName: "reportCheckForm", scriptName: "InitializeReportCheckForm" }, callbackFunction, true);
}

StiMobileDesigner.prototype.InitializeGlobalizationEditorForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeGlobalizationEditorForm", formName: "globalizationEditorForm", scriptName: "InitializeGlobalizationEditorForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeEditResourceForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeEditResourceForm", formName: "editResourceForm", scriptName: "InitializeEditResourceForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeResourceViewDataForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeResourceViewDataForm", formName: "resourceViewDataForm", scriptName: "InitializeResourceViewDataForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeODataConnectionForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeODataConnectionForm", formName: "oDataConnectionForm", scriptName: "InitializeODataConnectionForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeSaveReportForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeSaveReportForm", formName: "saveReport", scriptName: "InitializeSaveReportForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeOnlineSaveAsReportForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeOnlineSaveAsReportForm", formName: "onlineSaveAsReport", scriptName: "InitializeOnlineSaveAsReportForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeNewFolderForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeNewFolderForm", formName: "newFolder", scriptName: "InitializeNewFolderForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeWizardForm = function (callbackFunction) {
    this.ExecuteScript({
        initMethod: "InitializeWizardForm", formName: "wizardForm",
        scriptName: "InitializeWizardFormControls;InitializeWizardFormColumns;InitializeWizardFormColumnsOrder;InitializeWizardFormDataSource;" +
            "InitializeWizardFormFilter;InitializeWizardFormGroups;InitializeWizardFormLayout;InitializeWizardFormSort;InitializeWizardFormTheme;" +
            "InitializeWizardFormTotals;InitializeWizardRelationsForm;InitializeWizardForm"
    }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeSetupToolboxForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeSetupToolboxForm", formName: "setupToolboxForm", scriptName: "InitializeSetupToolboxForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeColorsCollectionForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeColorsCollectionForm", formName: "colorsCollectionForm", scriptName: "InitializeColorsCollectionForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeCloneContainerForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeCloneContainerForm", formName: "cloneContainerForm", scriptName: "InitializeCloneContainerForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializeSelectColumnsForVariableForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializeSelectColumnsForVariableForm", formName: "selectColumnsForVariable", scriptName: "InitializeSelectColumnsForVariableForm" }, callbackFunction);
}

StiMobileDesigner.prototype.InitializePublishForm = function (callbackFunction) {
    this.ExecuteScript({ initMethod: "InitializePublishForm", formName: "publishForm", scriptName: "InitializePublishForm" }, callbackFunction);
}