
//Add Command To Stack
StiMobileDesigner.prototype.AddCommandToStack = function (params) {
    params.reportGuid = this.options.reportGuid;
    params.clipboardId = this.options.clipboardId;
    params.undoArrayId = this.options.undoArrayId;
    params.componentCloneId = this.options.componentCloneId;
    params.reportCheckersId = this.options.reportCheckersId;
    params.previewReportGuid = this.options.previewReportGuid;
    if (this.options.report) params.reportFile = this.options.report.properties.reportFile;
    if (this.options.isJava){
    	if (this.options.report) params.reportFile = Base64.encode(params.reportFile); 
    }    
    this.options.commands.push(params);
    if (this.options.commands.length == 1) this.ExecuteCommandFromStack();
}

StiMobileDesigner.prototype.ShowMainLoadProcess = function (command) {
    var commandsWithMainLoadProcess = "CreateReport;OpenReport;SaveReport;CloseReport;WizardResult;GetPreviewPages;" +
        "GetConnectionTypes;CreateOrEditConnection;DeleteConnection;CreateOrEditRelation;DeleteRelation;CreateOrEditColumn;CreateOrEditRelation;" +
        "DeleteRelation;CreateOrEditColumn;DeleteColumn;CreateOrEditDataSource;DeleteDataSource;GetAllConnections;RetrieveColumns;SynchronizeDictionary;" +
        "CreateOrEditBusinessObject;DeleteBusinessObject;UpdateStyles;AddStyle;RemoveStyle;GetReportFromData;ItemResourceSave;StartEditChartComponent;AddSeries;RemoveSeries;" +
        "SeriesMove;SetLabelsType;AddConstantLineOrStrip;RemoveConstantLineOrStrip;ConstantLineOrStripMove;SendContainerValue;CreateTextComponent;CreateDataComponent;" +
        "RunQueryScript;ViewData;ApplyDesignerOptions;StartEditCrossTabComponent;UpdateCrossTabComponent;Undo;Redo;GetReportForDesigner;" +
        "OpenStyle;SaveStyle;GetGlobalizationStrings;SetCultureSettingsToReport;GetCultureSettingsFromReport;StartEditGaugeComponent;UpdateGaugeComponent;CreateOrEditResource" +
        "DeleteResource;CreateDatabaseFromResource;LoadReportFromCloud;MoveDictionaryItem;GetVariableItemsFromDataColumn";

    if (commandsWithMainLoadProcess.indexOf(command) != -1) return true;

    return false;
}

StiMobileDesigner.prototype.ExecuteCommandFromStack = function () {
    var params = this.options.commands[0];
    actionFunction = this.options.callbackFunction.replace("callbackParams", this.EncodeSymbols(JSON.stringify(params)));
    if (params.command != "UpdateCache") {
        if (this.ShowMainLoadProcess(params.command)) {
            var processImage = this.options.processImage || this.InitializeProcessImage();
            processImage.show();
        }
        else {
            if (this.options.processImageStatusPanel) this.options.processImageStatusPanel.show();
        }
    }
    eval(actionFunction);
    clearTimeout(this.options.timerAjax);

    var jsObject = this;
    this.options.timerAjax = setTimeout(function () {
        jsObject.Synchronization();
    }, params.command != "Synchronization" ? 120000 : 15000);
}

StiMobileDesigner.prototype.Synchronization = function () {
    this.options.commands = [];
    this.SendCommandSynchronization();
}

//Fixed bugs with some symblos
StiMobileDesigner.prototype.EncodeSymbols = function (str) {
    str = str.replace(/'/g, '\\\''); // (')
    str = str.replace(/\\\"/g, '&ldquo;'); // (\")
    return str;
}

//Error
StiMobileDesigner.prototype.errorFromServer = function (args, context) {
}

//Receive
StiMobileDesigner.prototype.receveFromServer = function (args, context) {
    var answer = { command: null };
    try {
        answer = JSON.parse(args);
    }
    catch (e) {
        alert("JSON: Parser error!");
        return;
    }

    var jsObject = this;

    if (!jsObject.options) {
        var designer = document.getElementById(answer.designerId);
        var jsObject = designer ? designer.jsObject : eval("js" + answer.designerId);
    }

    clearTimeout(jsObject.options.timerAjax);

    if (answer.error || answer.infoMessage) {
        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
        var text = answer.error || answer.infoMessage;

        errorMessageForm.show(text, answer.infoMessage != null);
        if (jsObject.options.viewer) (jsObject.options.viewer.jsObject.controls || jsObject.options.viewer.jsObject.options).processImage.hide();
        
        if (answer.checkItems && answer.checkItems.length > 0) {
            jsObject.InitializeCheckPopupPanel(answer.checkItems, function (checkPopupPanel) { });
        }
    }
    else {
        switch (answer.command) {
            case "SessionCompleted":
                {
                    var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                    errorMessageForm.show("Session on the server is complete!");
                    break;
                }
            case "SynchronizationError":
                {
                    var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                    errorMessageForm.onAction = function () {
                        jsObject.options.ignoreBeforeUnload = true;
                        location.reload(true);
                    }
                    errorMessageForm.show("The website is temporarily unavailable. Updating is in progress...");
                    break;
                }
            case "Synchronization":
                {
                    jsObject.LoadReport(jsObject.ParseReport(answer.reportObject));
                    break;
                }
            case "CreateReport":
                {                    
                    if (answer.needClearAfterOldReport) jsObject.CloseReport();
                    if (jsObject.options.cloudParameters) jsObject.options.cloudParameters.reportTemplateItemKey = null;
                    jsObject.options.reportGuid = answer.reportGuid;
                    jsObject.LoadReport(jsObject.ParseReport(answer.reportObject));
                    if (jsObject.options.callbackFunctions[answer["callbackFunctionId"]]) {
                        jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer);
                        delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    }
                    break;
                }
            case "GetReportFromData":
            case "OpenReport":
                {
                    jsObject.CloseReport();
                    if (answer["errorMessage"] != null) {
                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        errorMessageForm.show(answer["errorMessage"]);
                    }
                    else {
                        if (answer.reportGuid == null && answer.reportObject == null) {
                            var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                            errorMessageForm.show(jsObject.loc.Errors.Error);
                        }
                        else {
                            if (answer.command == "OpenReport" && jsObject.options.cloudParameters) {
                                jsObject.options.cloudParameters.reportTemplateItemKey = null;
                            }
                            jsObject.options.reportGuid = answer.reportGuid;
                            var reportObject = jsObject.ParseReport(answer.reportObject);
                            reportObject.encryptedPassword = answer.encryptedPassword;
                            jsObject.LoadReport(reportObject);
                        }                        
                    }
                    break;
                }
            case "ItemResourceSave":
                {
                    if (answer["errorMessage"] != null) {
                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        errorMessageForm.show(answer["errorMessage"]);
                    }
                    else {
                        jsObject.options.reportIsModified = false;
                    }
                    break;
                }
            case "CloneItemResourceSave":
                {
                    if (answer["errorMessage"] != null) {
                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        errorMessageForm.show(answer["errorMessage"]);
                    } else {
                        if (answer["resultItemKey"]) {
                            if (jsObject.options.isOnlineVersion) {
                                var viewerUrl = jsObject.options.cloudParameters.viewerUrl;

                                if (!jsObject.options.viewerContainer) jsObject.InitializePreviewPanel();
                                jsObject.options.viewerContainer.addFrame();
                                jsObject.options.viewerContainer.changeVisibleState(true);

                                var url = viewerUrl + "?params=" + Base64.encode(
                                    jsObject.options.cloudParameters.sessionKey + (jsObject.options.dVers ? "1" : "0") + answer["resultItemKey"] + ";" +
                                    jsObject.options.cloudParameters.localizationName + ";" +
                                    jsObject.options.cloudParameters.themeName);

                                var previewFrame = jsObject.options.viewerContainer.frame;
                                previewFrame.src = url;

                                var loadFlag = false;

                                previewFrame.onload = function () {
                                    if (!loadFlag) {
                                        if (jsObject.options.buttons.previewToolButton) jsObject.options.buttons.previewToolButton.progress.style.visibility = "hidden";
                                        loadFlag = true;
                                    }
                                }
                            }
                            else {
                                var navigatorUrl = jsObject.options.cloudParameters.navigatorUrl;
                                var viewerParameters = jsObject.CopyObject(jsObject.options.cloudParameters);
                                viewerParameters["reportName"] = Base64.encode(answer["reportName"]);
                                viewerParameters["reportTemplateItemKey"] = answer["resultItemKey"];
                                if (viewerParameters["attachedItems"]) delete viewerParameters["attachedItems"];
                                delete viewerParameters["navigatorUrl"];
                                var viewerHref = navigatorUrl.replace("main.aspx", "viewer");
                                if (viewerParameters.isOnlineVersion) viewerHref = viewerParameters.viewerUrl;
                                if (!jsObject.options.viewerContainer) jsObject.InitializePreviewPanel();
                                jsObject.options.viewerContainer.addFrame();
                                jsObject.options.viewerContainer.changeVisibleState(true);

                                var previewFrame = jsObject.options.viewerContainer.frame;
                                previewFrame.src = "about:blank";
                                var loadFlag = false;

                                previewFrame.onload = function () {
                                    if (!loadFlag) {
                                        if (jsObject.options.buttons.previewToolButton) jsObject.options.buttons.previewToolButton.progress.style.visibility = "hidden";
                                        loadFlag = true;
                                        var doc = this.contentDocument || this.document;
                                        jsObject.options.ignoreBeforeUnload = true;
                                        jsObject.postForm(viewerParameters, doc, viewerHref);
                                        setTimeout(function () { jsObject.options.ignoreBeforeUnload = false; }, 500);
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
            case "CloseReport":
                {
                    jsObject.CloseReport();
                    break;
                }
            case "SaveReport":
                {
                    if (answer.infoMessage || answer.errorMessage) {
                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        errorMessageForm.show(answer.infoMessage || answer.errorMessage, answer.infoMessage);
                    }
                    break;
                }
            case "MoveComponent":
            case "ResizeComponent":
                {
                    var componentsNames = [];
                    var firstComponent;
                    for (var i = 0; i < answer.components.length; i++) {
                        var component = jsObject.options.report.pages[answer.pageName].components[answer.components[i].componentName];
                        if (i == 0) firstComponent = component;
                        componentsNames.push(answer.components[i].componentName);
                        if (answer.components[i].svgContent != null) component.properties.svgContent = answer.components[i].svgContent;
                        component.repaint();                        
                        if (component.typeComponent == "StiSubReport" && jsObject.options.report.pages[component.properties.subReportPage])
                            jsObject.SendCommandSendProperties(jsObject.options.report.pages[component.properties.subReportPage], []);
                    }
                    jsObject.CheckLargeHeight(jsObject.options.report.pages[answer.pageName], answer.largeHeightAutoFactor);
                    jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                    jsObject.PaintSelectedLines();                    
                    if (answer.components.length == 1 || (answer.command == "ResizeComponent" && !answer.isMultiResize)) {
                        if (firstComponent.typeComponent == "StiTable" && answer.cells) {
                            jsObject.RebuildTable(firstComponent, answer.cells);
                        }

                        firstComponent.setSelected();
                        firstComponent.setOnTopLevel();
                    }
                    else {
                        jsObject.SetSelectedObjectsByNames(jsObject.options.report.pages[answer.pageName], componentsNames);
                    }
                    jsObject.UpdatePropertiesControls();
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "CreateComponent":
                {
                    if (!jsObject.options.lastComponentStyleProperties) jsObject.options.lastComponentStyleProperties = {};
                    var selectedObject = jsObject.options.selectedObjects ? jsObject.options.selectedObjects[0] : jsObject.options.selectedObject;

                    if (selectedObject && !jsObject.IsBandComponent(selectedObject) && jsObject.options.report.info.useLastFormat &&
                        selectedObject.typeComponent != "StiPage" && selectedObject.typeComponent != "StiReport") {
                        jsObject.SaveCurrentStylePropertiesToObject(jsObject.options.lastComponentStyleProperties);
                    }

                    var component = jsObject.CreateComponent(answer);
                    if (component) {
                        component.repaint();
                        jsObject.CheckLargeHeight(jsObject.options.report.pages[component.properties.pageName], answer.largeHeightAutoFactor);
                        jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                        jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;
                        jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                        component.setOnTopLevel();
                        component.setSelected();                        

                        if (answer.tableCells) {
                            for (var i = 0; i < answer.tableCells.length; i++) {
                                var cell = jsObject.CreateComponent(answer.tableCells[i]);
                                if (cell) {
                                    cell.repaint();
                                    jsObject.options.report.pages[cell.properties.pageName].appendChild(cell);
                                    jsObject.options.report.pages[cell.properties.pageName].components[cell.properties.name] = cell;                                    
                                }
                            }
                        }

                        if (answer.newSubReportPage) jsObject.AddPage(answer.newSubReportPage, true);
                        jsObject.UpdatePropertiesControls();
                        if (jsObject.options.report.info.runDesignerAfterInsert) jsObject.ShowComponentForm(component);
                        jsObject.UpdateStateUndoRedoButtons();
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    }
                    break;
                }
            case "RemoveComponent":
                {
                    if (answer.rebuildProps && answer.pageName) {
                        jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.CheckLargeHeight(jsObject.options.report.pages[answer.pageName], answer.largeHeightAutoFactor);
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    }
                    break;
                }
            case "AddPage":
                {
                    jsObject.AddPage(answer);
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "RemovePage":
                {
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "SendProperties":
                {
                    var checkLargeHeight = false;
                    var components = answer.components;
                    for (var i = 0; i < components.length; i++) {
                        var object = components[i].typeComponent == "StiPage"
                        ? jsObject.options.report.pages[components[i].componentName]
                        : jsObject.options.report.pages[answer.pageName].components[components[i].componentName];
                        if (!object) continue;

                        jsObject.WriteAllProperties(object, components[i].properties);

                        if (object.typeComponent == "StiPage") {
                            object.repaint(true);
                            object.rebuild(answer.rebuildProps);
                            object.repaintAllComponents();
                        }
                        else {
                            object.properties.svgContent = components[i].svgContent;
                            object.repaint();
                            checkLargeHeight = true;
                        }
                    }
                    if (checkLargeHeight && answer.pageName && answer.largeHeightAutoFactor) {
                        jsObject.CheckLargeHeight(jsObject.options.report.pages[answer.pageName], answer.largeHeightAutoFactor);
                    }
                    if (answer.rebuildProps) {
                        jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                        var selectedObjects = jsObject.options.selectedObjects || [jsObject.options.selectedObject];
                        if (selectedObjects) {
                            for (var i = 0; i < selectedObjects.length; i++) {
                                if (selectedObjects[i].typeComponent != "StiPage" && selectedObjects[i].typeComponent != "StiReport") selectedObjects[i].setOnTopLevel();
                            }
                        }
                    }

                    jsObject.UpdatePropertiesControls();
                    jsObject.UpdateStateUndoRedoButtons();
                    break;
                }
            case "ChangeUnit":
                {
                    jsObject.options.report.properties.reportUnit = answer.reportUnit;
                    jsObject.options.buttons.unitButton.updateCaption(answer.reportUnit);
                    jsObject.options.report.gridSize = jsObject.ConvertUnitToPixel(jsObject.StrToDouble(answer.gridSize));
                    jsObject.ConvertAllComponentsToCurrentUnit(answer.pagePositions, answer.compPositions);
                    if (jsObject.options.selectedObject) jsObject.options.selectedObject.setSelected();
                    jsObject.UpdatePropertiesControls();
                    jsObject.UpdateStateUndoRedoButtons();
                    break;
                }
            case "RebuildPage":
                {
                    jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                    break;
                }
            case "LoadReportToViewer":
                {
                    var params = {}
                    params.previewReportGuid = jsObject.options.previewReportGuid;
                    params.pageNumber = 0;
                    params.zoom = (jsObject.options.viewer.jsObject.reportParams || jsObject.options.viewer.jsObject.options).zoom || 100;
                    params.viewmode = (jsObject.options.viewer.jsObject.reportParams ? jsObject.options.viewer.jsObject.reportParams.viewMode : jsObject.options.viewer.jsObject.options.menuViewMode) || "OnePage";                    
                    if (typeof jsObject.options.viewer.jsObject.postAction != 'undefined') jsObject.options.viewer.jsObject.postAction();
                    else jsObject.options.viewer.jsObject.sendToServer("LoadReportFromCache", params);
                    if (answer.checkItems && answer.checkItems.length > 0) jsObject.InitializeCheckPopupPanel(answer.checkItems, function (checkPopupPanel) { });
                    break;
                }
            case "SetToClipboard":
                {
                    jsObject.options.buttons.pasteComponent.setEnabled(true);
                    break;
                }
            case "GetFromClipboard":
                {
                    var components = answer["components"];
                    var countComponents = jsObject.GetCountObjects(components);
                    if (countComponents > 0) {
                        jsObject.options.clipboardMode = true;
                        for (var i = 0; i < countComponents; i++) {
                            var component = jsObject.CreateComponent(components[i]);
                            if (component) {
                                component.repaint();
                                jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                                jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;                                
                                if (!jsObject.isTouchDevice) {
                                    if (i == 0) {
                                        jsObject.options.in_drag = [[], [], [], [], true];
                                        var pagePositions = jsObject.FindPagePositions();
                                        jsObject.options.startMousePos = [
                                            pagePositions.posX + parseInt(component.getAttribute("left")) - 3,
                                            pagePositions.posY + parseInt(component.getAttribute("top")) - 3
                                        ];
                                    }
                                    jsObject.options.in_drag[0].push(component);
                                    jsObject.options.in_drag[1].push(parseInt(component.getAttribute("left")));
                                    jsObject.options.in_drag[2].push(parseInt(component.getAttribute("top")));
                                    jsObject.options.in_drag[3].push(component.getAllChildsComponents());
                                }
                            }
                        }
                        if (jsObject.isTouchDevice && answer.rebuildProps)
                            jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                        jsObject.UpdateStateUndoRedoButtons();
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                        if (!jsObject.options.mouseOverPage) {
                            jsObject.PasteCurrentClipboardComponent();
                        }
                    }
                    break;
                }
            case "Undo":
                {
                    if (answer["reportGuid"] && answer["reportObject"]) {
                        jsObject.options.report = null;
                        jsObject.options.reportGuid = answer.reportGuid;
                        jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                        jsObject.options.reportIsModified = true;
                        jsObject.options.buttons.undoButton.setEnabled(answer.enabledUndoButton);
                        jsObject.options.buttons.redoButton.setEnabled(true);
                    }
                    if (answer.selectedObjectName) jsObject.BackToSelectedComponent(answer.selectedObjectName);
                    break;
                }
            case "Redo":
                {
                    if (answer["reportGuid"] && answer["reportObject"]) {
                        jsObject.options.report = null;
                        jsObject.options.reportGuid = answer.reportGuid;
                        jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                        jsObject.options.reportIsModified = true;
                    }
                    jsObject.options.buttons.redoButton.setEnabled(answer.enabledRedoButton);
                    jsObject.options.buttons.undoButton.setEnabled(true);
                    if (answer.selectedObjectName) jsObject.BackToSelectedComponent(answer.selectedObjectName);
                    break;
                }
            case "RenameComponent":
                {
                    if (answer.newName == answer.oldName) jsObject.UpdatePropertiesControls();
                    else {
                        if (answer.typeComponent == "StiPage") {
                            var page = jsObject.options.report.pages[answer.oldName];
                            if (page) page.rename(answer.newName);
                        }
                        else {
                            var component = jsObject.FindComponentByName(answer.oldName);
                            if (component) component.rename(answer.newName);
                            jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                        }
                    }
                    jsObject.options.statusPanel.componentNameCell.innerHTML = answer.newName;
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "WizardResult":
                {
                    jsObject.options.reportGuid = answer.reportGuid;
                    jsObject.LoadReport(jsObject.ParseReport(answer.reportObject));
                    jsObject.options.reportIsModified = true;
                    if (jsObject.options.cloudParameters){
                        if (!jsObject.options.cloudParameters.thenOpenWizard) {
                            jsObject.options.cloudParameters.reportTemplateItemKey = null;
                        }
                        jsObject.options.cloudParameters.thenOpenWizard = false;
                    }
                    break;
                }
            case "GetConnectionTypes":
                {
                    jsObject.options.forms.selectConnectionForm.fillConnections(answer.connections);
                    break;
                }
            case "NewDictionary":
            case "SynchronizeDictionary":
                {
                    var attachedItems = jsObject.options.report.dictionary.attachedItems;
                    answer.dictionary.attachedItems = attachedItems;
                    jsObject.options.report.dictionary = answer.dictionary;
                    jsObject.options.dictionaryTree.build(answer.dictionary, true);
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "CreateOrEditConnection":
                {
                    jsObject.options.report.dictionary.databases = answer.databases;
                    if (answer.itemObject) jsObject.options.dictionaryTree.createOrEditConnection(answer);
                    jsObject.options.dictionaryPanel.toolBar.updateControls();
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    if (answer.mode == "New") {
                        var processImage = jsObject.options.processImage || jsObject.InitializeProcessImage();
                        processImage.hide();

                        if (answer.skipSchemaWizard) {
                            jsObject.InitializeEditDataSourceForm(function (editDataSourceForm) {
                                editDataSourceForm.datasource = jsObject.GetDataAdapterTypeFromDatabaseType(answer.itemObject.typeConnection);
                                editDataSourceForm.nameInSource = answer.itemObject.name;
                                editDataSourceForm.changeVisibleState(true);
                            });
                        }
                        else {
                            jsObject.InitializeSelectDataForm(function (selectDataForm) {
                                selectDataForm.databaseName = answer.itemObject.name;
                                selectDataForm.typeConnection = answer.itemObject.typeConnection;
                                selectDataForm.connectionObject = answer.itemObject;
                                selectDataForm.changeVisibleState(true);
                            });
                        }
                    }
                    break;
                }
            case "DeleteConnection":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.selectedItem.remove();
                        jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.ClearAllGalleries();
                        jsObject.UpdateStateUndoRedoButtons();
                    }
                    break;
                }
            case "CreateOrEditRelation":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New") jsObject.options.dictionaryTree.addRelation(answer.itemObject);
                        else jsObject.options.dictionaryTree.editRelation(answer);
                        jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                        if (jsObject.options.forms.dataForm && jsObject.options.forms.dataForm.visible) {
                            jsObject.options.forms.dataForm.rebuildTrees(answer.itemObject.nameInSource, "Relation");
                        }
                        if (jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                            jsObject.options.forms.crossTabForm.tabbedPane.tabsPanels.Data.rebuildTrees(answer.itemObject.nameInSource, "Relation");
                        }
                    }
                    break;
                }
            case "DeleteRelation":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.deleteRelation();
                        jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "CreateOrEditColumn":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New") jsObject.options.dictionaryTree.addColumn(answer);
                        else jsObject.options.dictionaryTree.editColumn(answer);
                        if (answer.databases) jsObject.options.report.dictionary.databases = answer.databases;
                        if (answer.businessObjects) jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteColumn":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.deleteColumn(answer);
                        if (answer.databases) jsObject.options.report.dictionary.databases = answer.databases;
                        if (answer.businessObjects) jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "CreateOrEditParameter":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New") jsObject.options.dictionaryTree.addParameter(answer);
                        else jsObject.options.dictionaryTree.editParameter(answer);
                        if (answer.databases) jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteParameter":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.deleteParameter(answer);
                        if (answer.databases) jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "CreateOrEditDataSource":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New")
                            jsObject.options.dictionaryTree.addDataSource(answer.itemObject);
                        else
                            jsObject.options.dictionaryTree.editDataSource(answer.itemObject);
                        jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        if (jsObject.options.forms.dataForm && jsObject.options.forms.dataForm.visible) {
                            jsObject.options.forms.dataForm.rebuildTrees(answer.itemObject.name, "DataSource");
                        }
                        if (jsObject.options.forms.editDataSourceFromOtherDatasourcesForm && jsObject.options.forms.editDataSourceFromOtherDatasourcesForm.visible) {
                            jsObject.options.forms.editDataSourceFromOtherDatasourcesForm.rebuildTrees(answer.itemObject.name);
                        }
                        if (jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                            jsObject.options.forms.crossTabForm.tabbedPane.tabsPanels.Data.rebuildTrees(answer.itemObject.name, "DataSource");
                        }
                        if (jsObject.options.forms.wizardForm && jsObject.options.forms.wizardForm.visible) {
                            var dataSources = jsObject.options.report ? jsObject.GetDataSourcesFromDictionary(jsObject.options.report.dictionary) : null;
                            jsObject.options.forms.wizardForm.dataSourcesFromServer = dataSources;
                            jsObject.options.forms.wizardForm.onshow(true);
                        }
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteDataSource":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.deleteDataSource();
                        jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "CreateOrEditBusinessObject":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New")
                            jsObject.options.dictionaryTree.addBusinessObject(answer.itemObject, answer.parentBusinessObjectFullName);
                        else
                            jsObject.options.dictionaryTree.editBusinessObject(answer.itemObject);
                        jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                        jsObject.UpdateStateUndoRedoButtons();
                        if (jsObject.options.forms.dataForm && jsObject.options.forms.dataForm.visible) {
                            jsObject.options.forms.dataForm.rebuildTrees(answer.itemObject.name, "BusinessObject");
                        }
                        if (jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                            jsObject.options.forms.crossTabForm.tabbedPane.tabsPanels.Data.rebuildTrees(answer.itemObject.name, "BusinessObject");
                        }
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteBusinessObject":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.deleteBusinessObject();
                        jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteBusinessObjectCategory":
                {
                    jsObject.options.dictionaryTree.selectedItem.remove();
                    jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "EditBusinessObjectCategory":
                {
                    jsObject.options.dictionaryTree.editBusinessObjectCategory(answer);
                    jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "DeleteVariablesCategory":
                {
                    jsObject.options.dictionaryTree.selectedItem.remove();
                    jsObject.options.report.dictionary.variables = answer.variables;
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "EditVariablesCategory":
                {
                    jsObject.options.dictionaryTree.selectedItem.itemObject.name = answer.newName;
                    jsObject.options.dictionaryTree.selectedItem.repaint();
                    jsObject.options.report.dictionary.variables = answer.variables;
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "CreateVariablesCategory":
                {
                    jsObject.options.dictionaryTree.createVariablesCategory(answer);
                    jsObject.options.report.dictionary.variables = answer.variables;
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "CreateOrEditVariable":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New")
                            jsObject.options.dictionaryTree.addVariable(answer.itemObject);
                        else
                            jsObject.options.dictionaryTree.editVariable(answer.itemObject);
                        jsObject.options.report.dictionary.variables = answer.variables;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteVariable":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.selectedItem.remove();
                        jsObject.options.report.dictionary.variables = answer.variables;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "CreateOrEditResource":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New")
                            jsObject.options.dictionaryTree.addResource(answer.itemObject);
                        else
                            jsObject.options.dictionaryTree.editResource(answer.itemObject);
                        jsObject.options.report.dictionary.resources = answer.resources;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteResource":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.selectedItem.remove();
                        jsObject.options.report.dictionary.resources = answer.resources;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "GetAllConnections":
                {
                    jsObject.InitializeNameInSourceForm(function (nameInSourceForm) {
                        nameInSourceForm.connections = answer.connections;
                        nameInSourceForm.changeVisibleState(true);
                    });
                    break;
                }
            case "RetrieveColumns":
                {
                    if (answer.columns) {
                        jsObject.InitializeEditDataSourceForm(function (editDataSourceForm) {
                            var currentColumns = editDataSourceForm.columnsAndParametersTree.getItemObjects("Column");
                            var currentParameters = editDataSourceForm.columnsAndParametersTree.getItemObjects("Parameter");
                            var allColumns = jsObject.ConcatColumns(currentColumns, answer.columns);
                            var allParameters = jsObject.ConcatColumns(currentParameters, answer.parameters);
                            allColumns.sort(jsObject.SortByName);
                            allParameters.sort(jsObject.SortByName);
                            editDataSourceForm.columnsAndParametersTree.addColumnsAndParameters(allColumns, allParameters, true);
                            editDataSourceForm.columnsAndParametersTree.parametersItem.style.display = editDataSourceForm.datasource.parameterTypes ? "" : "none";
                            editDataSourceForm.columnsAndParametersTree.onSelectedItem();
                        });
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "RemoveStyle":
                {
                    if (answer.deleteResult) {
                        jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
                            var removingItem = styleDesignerForm.itemsContainer.getItemByKey(answer.itemKey);
                            if (removingItem) removingItem.remove();
                        });
                    }
                    break;
                }
            case "AddStyle":
            case "PasteStyle":
            case "DuplicateStyle":
            case "CreateStyleFromComponent":
                {
                    if (answer.styleObject) {
                        jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
                            styleDesignerForm.itemsContainer.addItem(answer.styleObject);
                        });
                    }
                    break;
                }
            case "UpdateStyles":
                {
                    jsObject.options.report = null;
                    jsObject.options.reportGuid = answer.reportGuid;
                    jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                    jsObject.options.reportIsModified = true;
                    jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
                        styleDesignerForm.itemsContainer.updateStatesMovingButtons();
                        styleDesignerForm.isModified = false;
                        jsObject.UpdateStateUndoRedoButtons();
                        if (answer.selectedObjectName) jsObject.BackToSelectedComponent(answer.selectedObjectName);
                        if (jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                            jsObject.options.forms.crossTabForm.controls.stylesContainer.update();
                        }

                        jsObject.options.report.chartStylesContent = null;
                        var editChartForm = jsObject.options.forms.editChart;
                        if (editChartForm && editChartForm.visible) {
                            editChartForm.stylesContainer.stylesProgress.style.display = "";
                            editChartForm.stylesContainer.clear();
                            editChartForm.jsObject.SendCommandGetStylesContent({ componentName: editChartForm.chartProperties.name });
                        }
                    });
                    break;
                }
            case "CreateStyleCollection":
                {
                    jsObject.options.report.stylesCollection = answer.stylesCollection;
                    jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
                        styleDesignerForm.itemsContainer.addItems(jsObject.options.report.stylesCollection)
                    });
                    break;
                }
            case "StartEditChartComponent":
                {
                    if (answer.properties) {
                        jsObject.InitializeEditChartForm(function (editChartForm) {
                            editChartForm.chartProperties = answer.properties;
                            editChartForm.changeVisibleState(true);
                        });
                    }
                    break;
                }
            case "StartEditGaugeComponent":
                {
                    if (answer.properties) {
                        jsObject.InitializeEditGaugeForm(function (editGaugeForm) {
                            editGaugeForm.gaugeProperties = answer.properties;
                            editGaugeForm.changeVisibleState(true);
                        });
                    }
                    break;
                }
            case "AddSeries":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        var lastSeries = editChartForm.chartProperties.series[editChartForm.chartProperties.series.length - 1];
                        var seriesItem = editChartForm.seriesContainer.addItem(lastSeries.name, lastSeries);
                        editChartForm.chartImage.update();
                    }
                    break;
                }
            case "SeriesMove":
            case "RemoveSeries":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        editChartForm.seriesContainer.update();
                        editChartForm.chartImage.update();
                        if (answer.selectedIndex != null) {
                            editChartForm.seriesContainer.items[answer.selectedIndex].action();
                        }
                    }
                    break;
                }
            case "AddConstantLineOrStrip":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        var itemType = answer.itemType;
                        var container = editChartForm[itemType + "Container"];
                        var collection = itemType == "ConstantLines" ? editChartForm.chartProperties.constantLines : editChartForm.chartProperties.strips;
                        var lastItem = collection[collection.length - 1];
                        var newItem = container.addItem(lastItem.name, lastItem);
                        editChartForm.chartImage.update();
                    }
                    break;
                }
            case "ConstantLineOrStripMove":
            case "RemoveConstantLineOrStrip":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        var itemType = answer.itemType;
                        var container = editChartForm[itemType + "Container"];
                        container.update();
                        editChartForm.chartImage.update();
                        if (answer.selectedIndex != null) {
                            container.items[answer.selectedIndex].action();
                        }
                    }
                    break;
                }
            case "GetLabelsContent":
                {
                    if (answer.labelsContent) {
                        var editChartForm = jsObject.options.forms.editChart;
                        if (answer.isSeriesLables) {
                            editChartForm.seriesLabelsContainer.update(answer.labelsContent);
                        }
                        else {
                            editChartForm.labelsContainer.update(answer.labelsContent);
                        }
                    }
                    break;
                }
            case "GetStylesContent":
                {
                    if (answer.stylesContent) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.stylesContainer.update(answer.stylesContent);
                    }
                    break;
                }
            case "SetLabelsType":
            case "SetChartStyle":
            case "SetChartPropertyValue":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        editChartForm.chartImage.update();
                        if (answer.command == "SetLabelsType") {
                            if (!answer.isSeriesLabels)
                                editChartForm.labelPropertiesContainer.buttons.Common.action();
                            else
                                editChartForm.seriesPropertiesContainer.buttons.SeriesLabels.action();
                        }
                    }
                    break;
                }
            case "SendContainerValue":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        editChartForm.chartImage.update();
                    }
                    if (answer.closeChartForm) editChartForm.action();
                    break;
                }
            case "GetDatabaseData":
                {
                    jsObject.InitializeSelectDataForm(function (selectDataForm) {
                        selectDataForm.buildTree(answer.data)
                    });
                    break;
                }
            case "ApplySelectedData":
                {
                    if (answer.dictionary) {
                        var attachedItems = jsObject.options.report.dictionary.attachedItems;
                        answer.dictionary.attachedItems = attachedItems;
                        jsObject.options.report.dictionary = answer.dictionary;
                        jsObject.options.dictionaryTree.build(answer.dictionary, true);
                        var selectedDatabaseItem = null;
                        for (var key in jsObject.options.dictionaryTree.mainItems.DataSources.childs) {
                            var databaseItem = jsObject.options.dictionaryTree.mainItems.DataSources.childs[key];
                            if (databaseItem.itemObject.name == answer.databaseName) {
                                selectedDatabaseItem = databaseItem;
                                break;
                            }
                        }
                        if (selectedDatabaseItem) {
                            selectedDatabaseItem.openTree();
                            selectedDatabaseItem.setOpening(true);
                            for (var key in selectedDatabaseItem.childs) {
                                var dataSourceItem = selectedDatabaseItem.childs[key];
                                if (dataSourceItem.itemObject.name.indexOf(answer.selectedDataSource.name) == 0) {
                                    dataSourceItem.setSelected();
                                    break;
                                }
                            }
                        }
                        if (jsObject.options.forms.dataForm && jsObject.options.forms.dataForm.visible) {
                            jsObject.options.forms.dataForm.rebuildTrees(answer.selectedDataSource.name, "DataSource");
                        }
                        if (jsObject.options.forms.editDataSourceFromOtherDatasourcesForm && jsObject.options.forms.editDataSourceFromOtherDatasourcesForm.visible) {
                            jsObject.options.forms.editDataSourceFromOtherDatasourcesForm.rebuildTrees(answer.selectedDataSource.name);
                        }
                        if (jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                            jsObject.options.forms.crossTabForm.tabbedPane.tabsPanels.Data.rebuildTrees(answer.selectedDataSource.name, "DataSource");
                        }
                        if (jsObject.options.forms.wizardForm && jsObject.options.forms.wizardForm.visible) {
                            var dataSources = jsObject.options.report ? jsObject.GetDataSourcesFromDictionary(jsObject.options.report.dictionary) : null;
                            jsObject.options.forms.wizardForm.dataSourcesFromServer = dataSources;
                            jsObject.options.forms.wizardForm.onshow(true);
                        }
                    }
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "CreateTextComponent":
                {
                    if (answer.newComponents) {
                        for (var i = 0; i < answer.newComponents.length; i++) {
                            var component = jsObject.CreateComponent(answer.newComponents[i]);
                            if (component) {
                                component.repaint();
                                jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                                jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;
                                jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                                component.setOnTopLevel();
                                component.setSelected();
                                jsObject.UpdatePropertiesControls();
                                jsObject.UpdateStateUndoRedoButtons();
                            }
                        }
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    }
                    break;
                }
            case "CreateDataComponent":
                {
                    if (answer.pageComponents) {
                        var pageComponents = JSON.parse(answer.pageComponents);
                        var lastComponent = null;
                        for (var componentName in pageComponents.components) {
                            var componentProps = pageComponents.components[componentName];

                            if (jsObject.options.report.pages[answer.pageName].components[componentProps.name] == null) {
                                var component = jsObject.CreateComponent(componentProps);
                                if (component) {
                                    component.repaint();
                                    lastComponent = component;
                                    jsObject.options.report.pages[answer.pageName].appendChild(component);
                                    jsObject.options.report.pages[answer.pageName].components[componentProps.name] = component;
                                }
                            }
                        }
                        if (lastComponent) {
                            lastComponent.setOnTopLevel();
                            lastComponent.setSelected();
                            jsObject.UpdatePropertiesControls();
                            jsObject.UpdateStateUndoRedoButtons();
                        }
                        jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    }
                    break;
                }
            case "SetReportProperties":
                {
                    if (jsObject.options.report) {
                        jsObject.WriteAllProperties(jsObject.options.report, answer.properties);
                        jsObject.UpdatePropertiesControls();
                        jsObject.UpdateStateUndoRedoButtons();
                    }
                    break;
                }
            case "PageMove":
                {
                    jsObject.ChangePageIndexes(answer.pageIndexes);
                    jsObject.options.pagesPanel.pagesContainer.updatePages();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "TestConnection":
                {
                    jsObject.InitializeEditConnectionForm(function (editConnectionForm) {
                        editConnectionForm.testConnection.setEnabled(true);
                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        errorMessageForm.show(answer.testResult, true);
                    });
                    break;
                }
            case "RunQueryScript":
                {
                    var text = answer.resultQueryScript == "successfully"
                        ? jsObject.loc.FormDictionaryDesigner.ExecutedSQLStatementSuccessfully
                        : (answer.resultQueryScript || jsObject.loc.DesignerFx.ConnectionError);
                    var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                    errorMessageForm.show(text, answer.resultQueryScript == "successfully");
                    break;
                }
            case "ViewData":
                {                                        
                    jsObject.InitializeViewDataForm(function (viewDataForm) {
                        viewDataForm.show(answer.resultData, answer.dataSourceName);
                    });
                    break;
                }
            case "ApplyDesignerOptions":
                {
                    jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                    jsObject.options.reportIsModified = true;
                    break;
                }
            case "GetSqlParameterTypes":
                {
                    if (jsObject.options.forms.editDataSourceForm && jsObject.options.forms.editDataSourceForm.visible &&
                        answer.sqlParameterTypes && jsObject.options.forms.editDataSourceForm.datasource) {
                        jsObject.options.forms.editDataSourceForm.datasource.parameterTypes = answer.sqlParameterTypes;
                        jsObject.options.forms.editDataSourceForm.columnToolBar.parameterNew.style.display = "";
                        jsObject.options.forms.editDataSourceForm.columnToolBar.retrieveColumnsAndParameters.style.display = "";
                        jsObject.options.forms.editDataSourceForm.columnsAndParametersTree.parametersItem.style.display = "";
                    }
                    break;
                }
            case "AlignToGridComponents":
            case "ChangeArrangeComponents":
            case "ChangeSizeComponents":
                {
                    jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                    jsObject.PaintSelectedLines();
                    jsObject.UpdatePropertiesControls();
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "UpdateSampleTextFormat":
                {
                    var textFormatForm = jsObject.options.forms.textFormatForm;
                    if (textFormatForm && textFormatForm.visible) {
                        textFormatForm.sampleContainer.innerHTML = answer.sampleText;
                    }
                    break;
                }
            case "StartEditCrossTabComponent":
                {
                    jsObject.InitializeCrossTabForm(function (editCrossTabForm) {
                        editCrossTabForm.changeVisibleState(true);
                    });
                    break;
                }
            case "UpdateCrossTabComponent":
                {
                    jsObject.InitializeCrossTabForm(function (editCrossTabForm) {
                        editCrossTabForm.recieveCommandResult(answer.updateResult);
                    });
                    break;
                }
            case "GetCrossTabColorStyles":
                {
                    if (answer.colorStyles && jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                        jsObject.options.forms.crossTabForm.controls.stylesContainer.fill(answer.colorStyles);
                    }
                    break;
                }
            case "DuplicatePage":
                {
                    if (answer["reportGuid"] && answer["reportObject"]) {
                        jsObject.options.report = null;
                        jsObject.options.reportGuid = answer.reportGuid;
                        jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                        jsObject.options.reportIsModified = true;
                        jsObject.UpdateStateUndoRedoButtons();
                        var pageButton = jsObject.options.pagesPanel.pagesContainer.pages[answer.selectedPageIndex];
                        if (pageButton) pageButton.action();
                    }
                    break;
                }
            case "SetEventValue":
                {
                    break;
                }
            case "GetCrossTabStylesContent":
            case "GetTableStylesContent":
            case "GetGaugeStylesContent":
            case "GetMapStylesContent":
                {
                    if (jsObject.options.callbackFunctions[answer["callbackFunctionId"]]) {
                        jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer["stylesContent"]);
                        delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    }
                    break;
                }
            case "ChangeTableComponent":
                {
                    if (answer.result) {
                        switch (answer.result.command) {
                            case "convertTo":
                            case "insertColumnToLeft":
                            case "insertColumnToRight":
                            case "deleteColumn":
                            case "insertRowAbove":
                            case "insertRowBelow":
                            case "deleteRow":
                            case "joinCells":
                            case "changeColumnsOrRowsCount":
                            case "applyStyle":
                                {
                                    var cells = answer.result.cells;
                                    var page = jsObject.options.report.pages[answer.result.pageName];
                                    var table = page.components[answer.result.tableName];
                                    if (cells && table) {
                                        jsObject.RebuildTable(table, cells, answer.result.command != "convertTo");
                                        if (answer.result.tableProperties) {
                                            jsObject.CreateComponentProperties(table, answer.result.tableProperties);
                                            table.repaint();
                                        }

                                        if (answer.result.selectedCells) {
                                            if (answer.result.selectedCells.length == 0) {
                                                table.setSelected();
                                            }
                                            else if (answer.result.selectedCells.length == 1) {
                                                var cell = page.components[answer.result.selectedCells[0]];
                                                if (cell) cell.setSelected();
                                            }
                                            else {
                                                jsObject.SetSelectedObjectsByNames(page, answer.result.selectedCells);
                                            }
                                        }
                                        jsObject.UpdatePropertiesControls();
                                        jsObject.UpdateStateUndoRedoButtons();
                                    }
                                    if (answer.result.rebuildProps) page.rebuild(answer.result.rebuildProps);
                                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                                    break;
                                }

                        }
                    }
                    break;
                }            
            case "UpdateImagesArray":
                {
                    jsObject.options.images = answer.images;
                    jsObject.InitializeTextEditorForm(function () { });
                    jsObject.InitializeImageForm(function () { });
                    jsObject.InitializeDataForm(function () { });
                    if (jsObject.options.commands.length == 0) return;
                    break;
                }
            case "OpenStyle":
                {
                    jsObject.options.report.stylesCollection = answer.stylesCollection;

                    if (jsObject.options.forms.styleDesignerForm) {
                        jsObject.options.forms.styleDesignerForm.isModified = true;
                        jsObject.options.forms.styleDesignerForm.itemsContainer.addItems(jsObject.options.report.stylesCollection);
                    }
                    break;
                }
            case "CreateFieldOnDblClick":
                {
                    var page = jsObject.options.report.pages[answer.pageName];
                    var newComponents = answer.newComponents;
                    var lastComponent = null;

                    //Add or change new cells
                    for (var i = 0; i < newComponents.length; i++) {
                        var compObject = newComponents[i];

                        var component = jsObject.CreateComponent(compObject);
                        page.appendChild(component);
                        page.components[compObject.name] = component;
                        component.repaint();
                        lastComponent = component;
                    }

                    jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);

                    if (lastComponent) {
                        lastComponent.setOnTopLevel();
                        lastComponent.setSelected();
                        jsObject.UpdatePropertiesControls();
                        jsObject.UpdateStateUndoRedoButtons();
                    }
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "GetParamsFromQueryString":
                {
                    if (jsObject.options.callbackFunctions[answer["callbackFunctionId"]]) {
                        jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer.params);
                        delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    }
                    break;
                }
            case "CreateMovingCopyComponent":
                {                    
                    var components = answer["components"];
                    var countComponents = jsObject.GetCountObjects(components);
                    if (countComponents > 0) {
                        for (var i = 0; i < countComponents; i++) {
                            var component = jsObject.CreateComponent(components[i]);
                            if (component) {
                                component.repaint();
                                jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                                jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;

                                if (i == 0) {
                                    component.setOnTopLevel();
                                    component.setSelected();
                                }
                            }
                        }
                        jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                        jsObject.UpdateStateUndoRedoButtons();
                    }

                    if (jsObject.options.callbackFunctions[answer["callbackFunctionId"]]) {
                        jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer.params);
                        delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    }
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "UpdateGaugeComponent":
                {
                    jsObject.InitializeEditGaugeForm(function (editGaugeForm) {
                        editGaugeForm.recieveCommandResult(answer.updateResult);
                    });
                    break;
                }
            case "CreateImageComponent":
                {
                    if (answer.newComponent) {
                        var component = jsObject.CreateComponent(answer.newComponent);
                        if (component) {
                            component.repaint();
                            jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                            jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;
                            jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                            component.setOnTopLevel();
                            component.setSelected();
                            jsObject.UpdatePropertiesControls();
                            jsObject.UpdateStateUndoRedoButtons();
                        }
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    }
                    break;
                }
            case "CreateDatabaseFromResource":
                {
                    if (answer.resourceItemObject) {
                        jsObject.options.dictionaryTree.addResource(answer.resourceItemObject);
                        jsObject.options.report.dictionary.resources = answer.resources;
                        jsObject.ClearAllGalleries();
                    }
                    if (answer.newDataBaseName) {
                        answer.itemObject = jsObject.GetObjectByPropertyValueFromCollection(answer.databases, "name", answer.newDataBaseName);
                        if (answer.itemObject) {
                            answer.mode = "New";
                            jsObject.options.report.dictionary.databases = answer.databases;
                            jsObject.options.dictionaryTree.createOrEditConnection(answer);
                            var newConnectionItem = jsObject.options.dictionaryTree.mainItems["DataSources"].getChildByName(answer.itemObject.name);
                            if (newConnectionItem) {
                                for (var key in newConnectionItem.childs) newConnectionItem.childs[key].remove();
                                jsObject.options.dictionaryTree.addTreeItems(answer.itemObject.dataSources, newConnectionItem);
                            }
                            jsObject.options.dictionaryPanel.toolBar.updateControls();
                        }                        
                    }
                    if (jsObject.options.forms.wizardForm && jsObject.options.forms.wizardForm.visible) {
                        var dataSources = jsObject.options.report ? jsObject.GetDataSourcesFromDictionary(jsObject.options.report.dictionary) : null;
                        jsObject.options.forms.wizardForm.dataSourcesFromServer = dataSources;
                        jsObject.options.forms.wizardForm.onshow(true);
                    }

                    jsObject.UpdateStateUndoRedoButtons();
                    break;
                }
            case "StartEditBarCodeComponent":
                {                    
                    if (answer.barCode) {
                        jsObject.InitializeBarCodeForm(function (editBarCodeForm) {
                            editBarCodeForm.barCode = answer.barCode;
                            editBarCodeForm.changeVisibleState(true);
                        });
                    }
                    break;
                }
            case "MoveDictionaryItem":
                {
                    if (answer.moveCompleted) {
                        var fromItem = jsObject.options.dictionaryTree.selectedItem;
                        var direction = answer.direction;
                        if (direction) {
                            var toItem = direction == "Down"
                                ? (fromItem.nextSibling || (fromItem && fromItem.itemObject.typeItem == "Variable"
                                    ? (fromItem.parent.nextSibling || fromItem.parent.parent) : null))
                                : (fromItem.previousSibling || (fromItem && fromItem.itemObject.typeItem == "Variable"
                                    ? (fromItem.parent.previousSibling || fromItem.parent.parent) : null))

                            if (toItem != null && fromItem) {
                                if (fromItem.itemObject.typeItem == "Variable" && (toItem.itemObject.typeItem == "Category" || toItem.itemObject.typeItem == "VariablesMainItem")) {
                                    fromItem.remove();
                                    var item = toItem.addChild(fromItem);
                                    item.setSelected();
                                    toItem.setOpening(true);
                                }
                                else if (fromItem.itemObject.typeItem == "Variable" && fromItem.parent.itemObject.typeItem == "Category" &&
                                    toItem.itemObject.typeItem == "Variable" && toItem.parent.itemObject.typeItem == "VariablesMainItem") {
                                    fromItem.remove();
                                    var item = toItem.parent.addChild(fromItem);
                                    item.setSelected();
                                }
                                else {
                                    fromItem.move(answer.direction);
                                }
                                jsObject.options.dictionaryPanel.toolBar.updateControls();
                            }
                        }
                        else if (answer.fromObject && answer.toObject &&
                                (answer.fromObject.typeItem == "Variable" || answer.fromObject.typeItem == "Category") &&
                                (answer.toObject.typeItem == "Variable" || answer.toObject.typeItem == "Category")) {

                            //Drag & Drop Variables or Category
                            var dictionaryTree = jsObject.options.dictionaryTree;
                             
                            //Save opening items
                            var openingCategories = {};
                            var allItems = dictionaryTree.mainItems["Variables"].getAllChilds();
                            for (var i = 0; i < allItems.length; i++) {
                                if (allItems[i].itemObject.typeItem == "Category" && allItems[i].isOpening) {
                                    openingCategories[allItems[i].itemObject.name] = true;
                                }
                            }

                            //repaint variables tree
                            while (dictionaryTree.mainItems["Variables"].childsContainer.childNodes[0]) {
                                dictionaryTree.mainItems["Variables"].childsContainer.childNodes[0].remove();
                            }                                
                            dictionaryTree.addTreeItems(answer.variablesTree, dictionaryTree.mainItems["Variables"]);
                            
                            allItems = dictionaryTree.mainItems["Variables"].getAllChilds();
                            for (var i = 0; i < allItems.length; i++) {
                                if (allItems[i].itemObject.typeItem == "Category" && openingCategories[allItems[i].itemObject.name]) {
                                    allItems[i].setOpening(true);
                                }
                                if (allItems[i].itemObject.name == answer.fromObject.name) {
                                    allItems[i].setSelected();
                                    allItems[i].openTree();
                                }
                            }
                        }
                    }
                    break;
                }
            default: {                
                if (answer.command && answer.callbackFunctionId && jsObject.options.callbackFunctions[answer["callbackFunctionId"]] != null) {
                    jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer);
                    delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    break;
                }
            }
        }
    }

    if (jsObject.options.commands.length >= 1) jsObject.options.commands.splice(0, 1);
    if (jsObject.options.commands.length == 0) {
        if (jsObject.options.processImageStatusPanel) jsObject.options.processImageStatusPanel.hide();
        if (jsObject.options.processImage) jsObject.options.processImage.hide();
    }
    else jsObject.ExecuteCommandFromStack();
}

//Send Create Report
StiMobileDesigner.prototype.SendCommandCreateReport = function (callbackFunction, needClearAfterOldReport) {
    var params = {};
    params.command = "CreateReport";
    params.callbackFunctionId = this.generateKey();
    if (needClearAfterOldReport) params.needClearAfterOldReport = true;
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Send Open Report
StiMobileDesigner.prototype.SendCommandOpenReport = function (fileContent, fileName, reportParams, filePath) {
    var params = {};
    params.command = "OpenReport";
    params.fileName = fileName;
    params.filePath = filePath;
    params.password = reportParams.password;
    params.isPacked = reportParams.isPacked;
    params.content = fileContent;
    this.AddCommandToStack(params);
}

//Send Save Report
StiMobileDesigner.prototype.SendCommandSaveReport = function () {
    this.options.reportIsModified = false;

    if (this.options.haveSaveEvent) {
        var params = {};
        params.command = "SaveReport";
        params.reportFile = this.options.report.properties.reportFile;
        if (this.options.report.encryptedPassword) params.encryptedPassword = this.options.report.encryptedPassword;

        if (this.options.scrollbiMode && this.options.scrollbiParameters) {
            params.scrollbiParameters = this.options.scrollbiParameters;
            params.scrollbiParameters.reportName = this.options.report.properties.reportFile;
        }
        this.AddCommandToStack(params);
    }
    else {
        var jsObject = this;
        this.options.ignoreBeforeUnload = true;
        var params = {
            command: "DownloadReport",
            reportFile: this.options.report.properties.reportFile,
            reportGuid: this.options.reportGuid
        }
        if (this.options.report.encryptedPassword) params.encryptedPassword = this.options.report.encryptedPassword;
        this.postForm(params);
        setTimeout(function () { jsObject.options.ignoreBeforeUnload = false; }, 500);
    }
}

//Send Save As Report
StiMobileDesigner.prototype.SendCommandSaveAsReport = function () {    
    this.options.reportIsModified = false;

    if (this.options.haveSaveAsEvent) {        
        var params = {};
        params.command = "SaveAsReport";
        params.reportFile = this.options.report.properties.reportFile;
        if (this.options.report.encryptedPassword) params.encryptedPassword = this.options.report.encryptedPassword;
        this.AddCommandToStack(params);
    }
    else {
        var jsObject = this;
        this.options.ignoreBeforeUnload = true;
        var params = {
            command: "DownloadReport",
            reportFile: this.options.report.properties.reportFile,
            reportGuid: this.options.reportGuid
        }
        if (this.options.report.encryptedPassword) params.encryptedPassword = this.options.report.encryptedPassword;
        this.postForm(params);
        setTimeout(function () { jsObject.options.ignoreBeforeUnload = false; }, 500);
    }
}

//Send Close Report
StiMobileDesigner.prototype.SendCommandCloseReport = function () {
    var params = {};
    params.command = "CloseReport";
    this.AddCommandToStack(params);
}

//Send Change Rect Component
StiMobileDesigner.prototype.SendCommandChangeRectComponent = function (component, command, runFromProperty, resizeType) {
    if (!component) return;
    var params = {};
    params.command = command;
    params.zoom = this.options.report.zoom.toString();
    params.runFromProperty = runFromProperty;
    params.resizeType = resizeType;
    params.components = [];
    if (this.options.in_drag && this.options.in_drag.length > 4) params.moveAfterCopyPaste = true;

    var components = this.Is_array(component) ? component : [component];
    for (var i = 0; i < components.length; i++) {
        if (command == "MoveComponent" && this.IsTableCell(components[i])) continue;
        if (components[i].properties.unitLeft == null || components[i].properties.unitTop == null ||
            components[i].properties.unitWidth == null || components[i].properties.unitHeight == null)
            continue;
        if (!params.pageName) params.pageName = components[i].properties.pageName;
        var compParams = {
            componentName: components[i].properties.name,
            invertWidth: components[i].properties.invertWidth,
            invertHeight: components[i].properties.invertHeight,
            componentRect: components[i].properties.unitLeft + "!" + components[i].properties.unitTop + "!" +
                components[i].properties.unitWidth + "!" + components[i].properties.unitHeight
        }
        params.components.push(compParams);
    }
    if (params.components.length == 0) return;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Component
StiMobileDesigner.prototype.SendCommandCreateComponent = function (pageName, typeComponent, componentRect, additionalParams) {
    var params = {};
    params.command = "CreateComponent";
    params.pageName = pageName;
    params.typeComponent = typeComponent;
    params.componentRect = componentRect;
    params.zoom = this.options.report.zoom.toString();
    if (additionalParams) params.additionalParams = additionalParams;
    
    var selectedObject = this.options.selectedObjects ? this.options.selectedObjects[0] : this.options.selectedObject;

    if (selectedObject && !this.IsBandComponent(selectedObject) && this.options.report.info.useLastFormat &&
        selectedObject.typeComponent != "StiPage" && selectedObject.typeComponent != "StiReport") {
        params.lastStyleProperties = this.GetStylePropertiesFromComponent(selectedObject);
    }
    
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Remove Component
StiMobileDesigner.prototype.SendCommandRemoveComponent = function (component) {
    var params = {};
    params.command = "RemoveComponent";
    params.components = [];

    var components = this.Is_array(component) ? component : [component];
    for (var i = 0; i < components.length; i++) {
        params.components.push(components[i].properties.name);
    }
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Add Page
StiMobileDesigner.prototype.SendCommandAddPage = function (pageIndex) {
    var params = {};
    params.command = "AddPage";
    params.pageIndex = pageIndex.toString();
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Remove Page
StiMobileDesigner.prototype.SendCommandRemovePage = function (page) {
    var params = {};
    params.command = "RemovePage";
    params.pageName = page.properties.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Rebuild Page
StiMobileDesigner.prototype.SendCommandRebuildPage = function (page) {
    var params = {};
    params.command = "RebuildPage";
    params.pageName = page.properties.name;
    this.AddCommandToStack(params);
}

//Send Change Unit
StiMobileDesigner.prototype.SendCommandChangeUnit = function (reportUnit) {
    var params = {};
    params.command = "ChangeUnit";
    params.reportUnit = reportUnit;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Properties
StiMobileDesigner.prototype.SendCommandSendProperties = function (object, propertiesNames, updateAllControls) {
    var params = {};
    params.command = "SendProperties";
    params.updateAllControls = updateAllControls ? true : false;
    params.zoom = this.options.report.zoom.toString();
    params.components = [];

    var objects = this.Is_array(object) ? object : [object];
    for (var i = 0; i < objects.length; i++) {
        var cannotChange = objects[i].properties.restrictions && !(objects[i].properties.restrictions == "All" || objects[i].properties.restrictions.indexOf("AllowChange") >= 0);
        var properties = [];
        var component = {};

        for (var num = 0; num < propertiesNames.length; num++) {
            if (typeof(objects[i].properties[propertiesNames[num]]) != "undefined") {
                properties.push({
                    name: propertiesNames[num],
                    value: objects[i].properties[propertiesNames[num]]
                });

                if (propertiesNames[num] == "restrictions") cannotChange = false;
            }
        }

        component.typeComponent = objects[i].typeComponent;
        component.componentName = objects[i].properties.name;
        component.properties = properties;
        if (cannotChange) component.cannotChange = true;
        params.components.push(component);
    }
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Get Preview Pages
StiMobileDesigner.prototype.SendCommandLoadReportToViewer = function () {
    if (this.options.report == null) return;
    (this.options.viewer.jsObject.controls || this.options.viewer.jsObject.options).reportPanel.clear();
    (this.options.viewer.jsObject.controls || this.options.viewer.jsObject.options).processImage.show();
    var params = {
        command: "LoadReportToViewer",
        clientGuid: this.options.viewer.jsObject.options.clientGuid
    };
    this.AddCommandToStack(params);
}

//Set To Clipboard
StiMobileDesigner.prototype.SendCommandSetToClipboard = function (component) {
    this.options.clipboard = true;
    var params = {};
    params.command = "SetToClipboard";
    params.pageName = this.options.currentPage.properties.name;
    params.components = [];
    var components = this.Is_array(component) ? component : [component];
    for (var i = 0; i < components.length; i++) {
        params.components.push(components[i].properties.name);
    }
    this.AddCommandToStack(params);
}

//Get From Clipboard
StiMobileDesigner.prototype.SendCommandGetFromClipboard = function () {
    var params = {};
    params.command = "GetFromClipboard";
    params.pageName = this.options.currentPage.properties.name;
    params.zoom = this.options.report.zoom.toString();    
    this.AddCommandToStack(params); 
}

//Send Synchronize
StiMobileDesigner.prototype.SendCommandSynchronization = function () {
    var params = {};
    params.command = "Synchronization";
    this.AddCommandToStack(params);
}

//Send Undo
StiMobileDesigner.prototype.SendCommandUndo = function () {
    var params = {};
    params.command = "Undo";
    params.zoom = this.options.report.zoom.toString();
    params.selectedObjectName = this.options.selectedObject ? this.options.selectedObject.properties.name : null;
    this.AddCommandToStack(params);
}

//Send Redo
StiMobileDesigner.prototype.SendCommandRedo = function () {
    var params = {};
    params.command = "Redo";
    params.zoom = this.options.report.zoom.toString();
    params.selectedObjectName = this.options.selectedObject ? this.options.selectedObject.properties.name : null;
    this.AddCommandToStack(params);
}

//Send Rename Component
StiMobileDesigner.prototype.SendCommandRenameComponent = function (component, newName) {
    var params = {};
    params.command = "RenameComponent";
    params.typeComponent = component.typeComponent;
    params.oldName = component.properties.name;
    params.newName = newName;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Wizard Result
StiMobileDesigner.prototype.SendCommandWizardResult = function (wizardResult) {
    var params = {};
    params.command = "WizardResult";
    params.wizardResult = wizardResult;
    if (this.options.cloudParameters && this.options.report && this.options.report.dictionary.attachedItems) {
        params.attachedItems = this.options.report.getAttachedItems();
        params.sessionKey = this.options.cloudParameters.sessionKey;
    }
    var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
    fileMenu.changeVisibleState(false);
    this.AddCommandToStack(params);
}

//Send ExitDesigner
StiMobileDesigner.prototype.SendCommandExitDesigner = function () {
    this.postForm({ command: "ExitDesigner" });
}

//Send Synchronize Dictionary
StiMobileDesigner.prototype.SendCommandSynchronizeDictionary = function () {
    var params = {};
    params.command = "SynchronizeDictionary";
    this.AddCommandToStack(params);
}

//Send New Dictionary
StiMobileDesigner.prototype.SendCommandNewDictionary = function () {
    var params = {};
    params.command = "NewDictionary";
    this.AddCommandToStack(params);
}

//Send Get Connection Types
StiMobileDesigner.prototype.SendCommandGetConnectionTypes = function () {
    var params = {};
    params.command = "GetConnectionTypes";
    this.AddCommandToStack(params);
}

//Send Create Or Edit Connection
StiMobileDesigner.prototype.SendCommandCreateOrEditConnection = function (connectionFormResult) {
    var params = {};
    params.command = "CreateOrEditConnection";
    params.connectionFormResult = connectionFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Connection
StiMobileDesigner.prototype.SendCommandDeleteConnection = function (selectedItem) {
    var params = {};
    params.command = "DeleteConnection";
    params.connectionName = selectedItem.itemObject.name;
    params.dataSourceNames = selectedItem.getChildNames();
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Or Edit Relation
StiMobileDesigner.prototype.SendCommandCreateOrEditRelation = function (relationFormResult) {
    var params = {};
    params.command = "CreateOrEditRelation";
    params.relationFormResult = relationFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Relation
StiMobileDesigner.prototype.SendCommandDeleteRelation = function (selectedItem) {
    var params = {};
    params.command = "DeleteRelation";
    params.relationNameInSource = selectedItem.itemObject.nameInSource;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Or Edit Column
StiMobileDesigner.prototype.SendCommandCreateOrEditColumn = function (columnFormResult) {
    var params = {};
    params.command = "CreateOrEditColumn";
    params.columnFormResult = columnFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Column
StiMobileDesigner.prototype.SendCommandDeleteColumn = function (selectedItem) {
    var params = {};
    params.command = "DeleteColumn";
    params.columnName = selectedItem.itemObject.name;
    var columnParent = this.options.dictionaryTree.getCurrentColumnParent();
    params.currentParentType = columnParent.type;
    params.currentParentName = (columnParent.type == "BusinessObject") ? selectedItem.getBusinessObjectFullName() : columnParent.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Or Edit Parameter
StiMobileDesigner.prototype.SendCommandCreateOrEditParameter = function (parameterFormResult) {
    var params = {};
    params.command = "CreateOrEditParameter";
    params.parameterFormResult = parameterFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Parameter
StiMobileDesigner.prototype.SendCommandDeleteParameter = function (selectedItem) {
    var params = {};
    params.command = "DeleteParameter";
    params.parameterName = selectedItem.itemObject.name;
    var parameterParent = this.options.dictionaryTree.getCurrentColumnParent();
    params.currentParentName = parameterParent.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Or Edit Data Source
StiMobileDesigner.prototype.SendCommandCreateOrEditDataSource = function (dataSourceFormResult) {
    var params = {};
    params.command = "CreateOrEditDataSource";
    params.dataSourceFormResult = dataSourceFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete DataSource
StiMobileDesigner.prototype.SendCommandDeleteDataSource = function (selectedItem) {
    var params = {};
    params.command = "DeleteDataSource";
    params.dataSourceName = selectedItem.itemObject.name;
    params.dataSourceNameInSource = selectedItem.itemObject.nameInSource;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Or Edit BusinessObject
StiMobileDesigner.prototype.SendCommandCreateOrEditBusinessObject = function (businessObjectFormResult) {
    var params = {};
    params.command = "CreateOrEditBusinessObject";
    params.businessObjectFormResult = businessObjectFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete BusinessObject
StiMobileDesigner.prototype.SendCommandDeleteBusinessObject = function (selectedItem) {
    var params = {};
    params.command = "DeleteBusinessObject";
    params.businessObjectFullName = selectedItem.getBusinessObjectFullName();
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Get All Connections
StiMobileDesigner.prototype.SendCommandGetAllConnections = function (typeDataAdapter) {
    var params = {};
    params.command = "GetAllConnections";
    params.typeDataAdapter = typeDataAdapter;
    this.AddCommandToStack(params);
}

//Send RetrieveColumns
StiMobileDesigner.prototype.SendCommandRetrieveColumns = function (params) {
    params.command = "RetrieveColumns";
    this.AddCommandToStack(params);
}

//Send Delete Category
StiMobileDesigner.prototype.SendCommandDeleteCategory = function (selectedItem) {
    var params = {};
    params.command = selectedItem.parent.itemObject.typeItem == "VariablesMainItem"
        ? "DeleteVariablesCategory"
        : "DeleteBusinessObjectCategory";
    params.categoryName = selectedItem.itemObject.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Edit Category
StiMobileDesigner.prototype.SendCommandEditCategory = function (categoryFormResult) {
    var params = {};
    params.command = this.options.dictionaryTree.selectedItem.parent.itemObject.typeItem == "VariablesMainItem"
        ? "EditVariablesCategory"
        : "EditBusinessObjectCategory";
    params.categoryFormResult = categoryFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Update Cache
StiMobileDesigner.prototype.SendCommandUpdateCache = function () {
    var jsObject = this;
    if (this.options.allowAutoUpdateCache) {
        this.options.timerUpdateCache = setTimeout(function () {
            jsObject.SendCommandUpdateCache();
        }, this.options.timeUpdateCache);

        var params = {};
        params.command = "UpdateCache";
        this.AddCommandToStack(params);
    }
}

//Send Create Or Edit Variable
StiMobileDesigner.prototype.SendCommandCreateOrEditVariable = function (variableFormResult) {
    var params = {};
    params.command = "CreateOrEditVariable";
    params.variableFormResult = variableFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Variable
StiMobileDesigner.prototype.SendCommandDeleteVariable = function (selectedItem) {
    var params = {};
    params.command = "DeleteVariable";
    params.variableName = selectedItem.itemObject.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Variables Category
StiMobileDesigner.prototype.SendCommandCreateVariablesCategory = function (categoryFormResult) {
    var params = {};
    params.command = "CreateVariablesCategory";
    params.categoryFormResult = categoryFormResult;
    this.options.reportIsModified = true;    
    this.AddCommandToStack(params);
}

//Send Create Or Edit Resource
StiMobileDesigner.prototype.SendCommandCreateOrEditResource = function (resourceFormResult) {
    var params = {};
    params.command = "CreateOrEditResource";
    params.resourceFormResult = resourceFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Resource
StiMobileDesigner.prototype.SendCommandDeleteResource = function (selectedItem) {
    var params = {};
    params.command = "DeleteResource";
    params.resourceName = selectedItem.itemObject.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Update Styles
StiMobileDesigner.prototype.SendCommandUpdateStyles = function (stylesCollection, name) {
    this.options.reportIsModified = true;
    this.options.stylesIsModified = true;
    var params = {};
    params.command = "UpdateStyles";
    if (name != null) params.name = name;
    params.stylesCollection = stylesCollection;
    params.zoom = this.options.report.zoom.toString();
    if (this.options.selectedObject) params.selectedObjectName = this.options.selectedObject.properties.name;
    this.AddCommandToStack(params);
}

//Send Add Style
StiMobileDesigner.prototype.SendCommandAddStyle = function (type) {
    this.options.reportIsModified = true;
    var params = {};
    params.command = "AddStyle";
    params.type = type;
    this.AddCommandToStack(params);
}

//Send Remove Style
StiMobileDesigner.prototype.SendCommandRemoveStyle = function (styleOldName, itemKey) {
    this.options.reportIsModified = true;
    var params = {};
    params.command = "RemoveStyle";
    params.styleOldName = styleOldName;
    params.itemKey = itemKey;
    this.AddCommandToStack(params);
}

//Send Create Style Collection
StiMobileDesigner.prototype.SendCommandCreateStyleCollection = function (styleCollectionProperties) {
    this.options.reportIsModified = true;
    var params = {};
    params.command = "CreateStyleCollection";
    params.styleCollectionProperties = styleCollectionProperties;
    this.AddCommandToStack(params);
}

//Send Get Report From Data
StiMobileDesigner.prototype.SendCommandGetReportFromData = function (params) {    
    params.command = "GetReportFromData";
    this.AddCommandToStack(params);
}

//Send Item Resource Save
StiMobileDesigner.prototype.SendCommandItemResourceSave = function (itemKey, customMessage) {
    var params = {
        command: "ItemResourceSave",
        reportTemplateItemKey: itemKey,
        sessionKey: this.options.cloudParameters.sessionKey
    }
    if (customMessage) params.customMessage = customMessage;
    if (this.options.isOnlineVersion) params.isOnlineVersion = true;
    this.AddCommandToStack(params);
}

//Send Clone Item Resource Save
StiMobileDesigner.prototype.SendCommandCloneItemResourceSave = function (params) {
    params.command = "CloneItemResourceSave";
    
    this.AddCommandToStack(params);
}

//Send Clone Chart Component
StiMobileDesigner.prototype.SendCommandStartEditChartComponent = function (componentName) {    
    var params = {};
    params.componentName = componentName;
    params.command = "StartEditChartComponent";
    this.AddCommandToStack(params);
}

// Send Canceled Edit Component
StiMobileDesigner.prototype.SendCommandCanceledEditComponent = function (componentName) {
    var params = {};
    params.componentName = componentName;
    params.command = "CanceledEditComponent";
    this.AddCommandToStack(params);
}

// Send Add Series
StiMobileDesigner.prototype.SendCommandAddSeries = function (params) {
    params.command = "AddSeries";
    this.AddCommandToStack(params);
}

// Send Remove Series
StiMobileDesigner.prototype.SendCommandRemoveSeries = function (params) {
    params.command = "RemoveSeries";
    this.AddCommandToStack(params);
}

// Send Series Move
StiMobileDesigner.prototype.SendCommandSeriesMove = function (params) {
    params.command = "SeriesMove";
    this.AddCommandToStack(params);
}

// Get Labels Content
StiMobileDesigner.prototype.SendCommandGetLabelsContent = function (params) {
    params.command = "GetLabelsContent";
    this.AddCommandToStack(params);
}

// Get Styles Content
StiMobileDesigner.prototype.SendCommandGetStylesContent = function (params) {
    params.command = "GetStylesContent";
    this.AddCommandToStack(params);
}

// Set Labels Type
StiMobileDesigner.prototype.SendCommandSetLabelsType = function (params) {
    params.command = "SetLabelsType";
    this.AddCommandToStack(params);
}

// Set Chart Style
StiMobileDesigner.prototype.SendCommandSetChartStyle = function (params) {
    params.command = "SetChartStyle";
    this.AddCommandToStack(params);
}

// Set Chart Property Value
StiMobileDesigner.prototype.SendCommandSetChartPropertyValue = function (params) {
    params.command = "SetChartPropertyValue";
    this.AddCommandToStack(params);
}

// Send Add ConstantLine Or Strip
StiMobileDesigner.prototype.SendCommandAddConstantLineOrStrip = function (params) {
    params.command = "AddConstantLineOrStrip";
    this.AddCommandToStack(params);
}

// Send Remove ConstantLine Or Strip
StiMobileDesigner.prototype.SendCommandRemoveConstantLineOrStrip = function (params) {
    params.command = "RemoveConstantLineOrStrip";
    this.AddCommandToStack(params);
}

// Send ConstantLine Or Strip Move
StiMobileDesigner.prototype.SendCommandConstantLineOrStripMove = function (params) {
    params.command = "ConstantLineOrStripMove";
    this.AddCommandToStack(params);
}

// Send Container Value
StiMobileDesigner.prototype.SendCommandSendContainerValue = function (params) {
    params.command = "SendContainerValue";
    this.AddCommandToStack(params);
}

// Get Database Data
StiMobileDesigner.prototype.SendCommandGetDatabaseData = function (databaseName) {
    var params = {
        command : "GetDatabaseData",
        databaseName : databaseName
    }
    this.AddCommandToStack(params);
} 

// Apply Selected Data
StiMobileDesigner.prototype.SendCommandApplySelectedData = function (data, databaseName) {
    var params = {
        command: "ApplySelectedData",
        data: data,
        databaseName: databaseName
    }
    this.AddCommandToStack(params);
}

// Create Text Component
StiMobileDesigner.prototype.SendCommandCreateTextComponent = function (itemObject, point, pageName) {
    this.options.reportIsModified = true;
    var params = {
        command: "CreateTextComponent",
        itemObject: itemObject,
        pageName: pageName,
        zoom: this.options.report.zoom.toString(),
        point: point
    }

    if (this.options.dictionaryPanel) {
        params.createLabel = this.options.menus.dictionarySettingsMenu.controls.createLabel.isChecked;
    }

    var selectedObject = this.options.selectedObjects ? this.options.selectedObjects[0] : this.options.selectedObject;

    if (selectedObject && !this.IsBandComponent(selectedObject) && this.options.report.info.useLastFormat &&
        selectedObject.typeComponent != "StiPage" && selectedObject.typeComponent != "StiReport") {
        params.lastStyleProperties = this.GetStylePropertiesFromComponent(selectedObject);
    }

    this.AddCommandToStack(params);
}

// Create Data Component
StiMobileDesigner.prototype.SendCommandCreateDataComponent = function (params) {
    this.options.reportIsModified = true;
    params.command = "CreateDataComponent";
    params.zoom = this.options.report.zoom.toString();
    this.AddCommandToStack(params);
}

//Send Report Properties
StiMobileDesigner.prototype.SendCommandSetReportProperties = function (propertiesNames) {
    var properties = {};
    var report = this.options.report;
    if (!report) return;

    for (var num in propertiesNames) {
        if (report.properties[propertiesNames[num]] != null) {
            properties[propertiesNames[num]] = report.properties[propertiesNames[num]];
        }
    }

    var params = {};
    params.command = "SetReportProperties";
    params.properties = properties;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Page Move
StiMobileDesigner.prototype.SendCommandPageMove = function (direction, pageIndex) {
    var params = {};
    params.command = "PageMove";
    params.direction = direction;
    params.pageIndex = pageIndex;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Command Test Connection
StiMobileDesigner.prototype.SendCommandTestConnection = function (typeConnection, connectionString) {
    var params = {};
    params.command = "TestConnection";
    params.typeConnection = typeConnection;
    params.connectionString = connectionString;
    this.AddCommandToStack(params);
}

//Send Command Run Query Script
StiMobileDesigner.prototype.SendCommandRunQueryScript = function (params) {
    params.command = "RunQueryScript";
    this.AddCommandToStack(params);
}

//Send Command View Data
StiMobileDesigner.prototype.SendCommandViewData = function (params) {
    params.command = "ViewData";
    this.AddCommandToStack(params);
}

//Send Command View Data
StiMobileDesigner.prototype.SendCommandApplyDesignerOptions = function (designerOptions) {
    var params = {
        command: "ApplyDesignerOptions",
        designerOptions: designerOptions,
        zoom: this.options.report.zoom.toString()
    }
    this.SetCookie("StimulsoftMobileDesignerOptions", Base64.encode(JSON.stringify(designerOptions)));
    this.AddCommandToStack(params);
}

//Send Command Get Sql Parameter Types
StiMobileDesigner.prototype.SendCommandGetSqlParameterTypes = function (dataSource) {
    var params = {
        command: "GetSqlParameterTypes",
        dataSource: dataSource
    }
    this.AddCommandToStack(params);
}

//Send Command Align To Grid
StiMobileDesigner.prototype.SendCommandAlignToGridComponents = function () {
    var params = {};
    params.command = "AlignToGridComponents";
    params.components = [];
    params.pageName = this.options.currentPage.properties.name;

    var components = [];
    if (this.options.selectedObjects) components = this.options.selectedObjects;
    else if (this.options.selectedObject) components.push(this.options.selectedObject);

    for (var i = 0; i < components.length; i++) {
        params.components.push(components[i].properties.name);
    }
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Command Change Arrange Components
StiMobileDesigner.prototype.SendCommandChangeArrangeComponents = function (arrangeCommand) {
    var params = {};
    params.command = "ChangeArrangeComponents";
    params.arrangeCommand = arrangeCommand;
    params.components = [];
    params.pageName = this.options.currentPage.properties.name;

    var components = [];
    if (this.options.selectedObjects) components = this.options.selectedObjects;
    else if (this.options.selectedObject) components.push(this.options.selectedObject);

    for (var i = 0; i < components.length; i++) {
        params.components.push(components[i].properties.name);
    }
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Update Sample Text Format
StiMobileDesigner.prototype.SendCommandUpdateSampleTextFormat = function (textFormat) {
    var params = {};
    params.command = "UpdateSampleTextFormat";
    params.textFormat = textFormat;

    this.AddCommandToStack(params);
}

//Send Clone CrossTab Component
StiMobileDesigner.prototype.SendCommandStartEditCrossTabComponent = function (componentName) {
    var params = {};
    params.componentName = componentName;
    params.command = "StartEditCrossTabComponent";
    this.AddCommandToStack(params);
}

//Send Update CrossTab Component
StiMobileDesigner.prototype.SendCommandUpdateCrossTabComponent = function (componentName, updateParameters) {
    var params = {};
    params.componentName = componentName;
    params.updateParameters = updateParameters;
    params.command = "UpdateCrossTabComponent";
    this.AddCommandToStack(params);
}

//Send Get CrossTab Color Styles
StiMobileDesigner.prototype.SendCommandGetCrossTabColorStyles = function () {
    var params = {};
    params.command = "GetCrossTabColorStyles";
    this.AddCommandToStack(params);
}

//Send Duplicate Page
StiMobileDesigner.prototype.SendCommandDuplicatePage = function (pageIndex) {
    var params = {};
    params.command = "DuplicatePage";
    params.pageIndex = pageIndex;
    this.AddCommandToStack(params);
}

//Send Set Event Value
StiMobileDesigner.prototype.SendCommandSetEventValue = function (components, eventName, eventValue) {
    var params = {};
    params.command = "SetEventValue";
    params.components = components;
    params.eventValue = eventValue;
    params.eventName = eventName;
    this.AddCommandToStack(params);
}

//Get CrossTab Styles Content
StiMobileDesigner.prototype.SendCommandGetCrossTabStylesContent = function (callbackFunction) {
    var params = {};
    params.command = "GetCrossTabStylesContent";
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Get Gauge Styles Content
StiMobileDesigner.prototype.SendCommandGetGaugeStylesContent = function (callbackFunction) {
    var params = {};
    params.command = "GetGaugeStylesContent";
    params.componentName = this.options.selectedObject ? this.options.selectedObject.properties.name : this.options.selectedObjects[0].properties.name;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Get Map Styles Content
StiMobileDesigner.prototype.SendCommandGetMapStylesContent = function (callbackFunction) {
    var params = {};
    params.command = "GetMapStylesContent";
    params.componentName = this.options.selectedObject ? this.options.selectedObject.properties.name : this.options.selectedObjects[0].properties.name;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Get Table Styles Content
StiMobileDesigner.prototype.SendCommandGetTableStylesContent = function (callbackFunction) {
    var params = {};
    params.command = "GetTableStylesContent";
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Change Table
StiMobileDesigner.prototype.SendCommandChangeTableComponent = function (changeParameters) {
    var selectedObjects = this.options.selectedObject ? [this.options.selectedObject] : this.options.selectedObjects;
    if (!selectedObjects) return;

    changeParameters.cells = [];
    for (var i = 0; i < selectedObjects.length; i++) {
        changeParameters.cells.push(selectedObjects[i].properties.name);
    }

    var params = {
        command: "ChangeTableComponent",
        tableName: selectedObjects[0].typeComponent == "StiTable" ? selectedObjects[0].properties.name : selectedObjects[0].properties.parentName,
        zoom: this.options.report.zoom.toString(),
        changeParameters: changeParameters
    };

    this.AddCommandToStack(params);
}

//Send Open Style
StiMobileDesigner.prototype.SendCommandOpenStyle = function (fileContent, fileName) {
    var params = {};
    params.command = "OpenStyle";
    params.content = fileContent;
    this.AddCommandToStack(params);
}

//Send Save Style
StiMobileDesigner.prototype.SendCommandSaveStyle = function (stylesCollection) {
    this.options.ignoreBeforeUnload = true;
    if (this.options.mvcMode) {
        var params = {
            command: "DownloadStyles",
            stylesCollection: JSON.stringify(stylesCollection)
        };
        this.AddMainParameters(params);
        this.postForm(this.options.requestUrl.replace("{action}", this.options.actionDesignerEvent), params);
    }
    else {
        this.postForm({ command: "DownloadStyles", reportGuid: this.options.reportGuid, stylesCollection: JSON.stringify(stylesCollection) });
    }
    var jsObject = this;
    setTimeout(function () { jsObject.options.ignoreBeforeUnload = false; }, 500);
}

//Send Copy Style
StiMobileDesigner.prototype.SendCommandCopyStyle = function (styleOldName) {
    var params = {};
    params.command = "CopyStyle";
    params.styleOldName = styleOldName;
    this.AddCommandToStack(params);
}

//Send Paste Style
StiMobileDesigner.prototype.SendCommandPasteStyle = function () {
    var params = {};
    params.command = "PasteStyle";
    this.AddCommandToStack(params);
}

//Send Duplicate Style
StiMobileDesigner.prototype.SendCommandDuplicateStyle = function (styleOldName) {
    var params = {};
    params.command = "DuplicateStyle";
    params.styleOldName = styleOldName;
    this.AddCommandToStack(params);
}

//Send Apply Style Properties
StiMobileDesigner.prototype.SendCommandApplyStyleProperties = function (styleObject) {
    var params = {};
    params.command = "ApplyStyleProperties";
    params.styleOldName = styleObject.oldName;
    params.styleProperties = styleObject.properties;
    this.AddCommandToStack(params);
}

//Send Create Style From Component
StiMobileDesigner.prototype.SendCommandCreateStyleFromComponent = function (componentName) {
    var params = {};
    params.command = "CreateStyleFromComponent";
    params.componentName = componentName;
    this.AddCommandToStack(params);
}

//Send Change Size Components
StiMobileDesigner.prototype.SendCommandChangeSizeComponents = function (actionName) {
    var params = {};
    params.command = "ChangeSizeComponents";
    params.actionName = actionName;    
    params.pageName = this.options.currentPage.properties.name;
    params.zoom = this.options.report.zoom.toString();
    params.components = [];

    var components = [];
    if (this.options.selectedObjects) components = this.options.selectedObjects;
    else if (this.options.selectedObject) components.push(this.options.selectedObject);

    for (var i = 0; i < components.length; i++) {
        params.components.push(components[i].properties.name);
    }

    this.AddCommandToStack(params);
}

//Send Delete Column
StiMobileDesigner.prototype.SendCommandCreateFieldOnDblClick = function (selectedItem) {
    var params = {};
    params.command = "CreateFieldOnDblClick";
    params.pageName = this.options.currentPage.properties.name;
    params.fullName = selectedItem.getResultForEditForm();
    params.selectedComponents = [];
    params.zoom = this.options.report.zoom.toString();

    var selectedComponents = [];
    if (this.options.selectedObjects) selectedComponents = this.options.selectedObjects;
    else if (this.options.selectedObject) selectedComponents.push(this.options.selectedObject);

    for (var i = 0; i < selectedComponents.length; i++) {
        params.selectedComponents.push(selectedComponents[i].properties.name);
    }

    if (this.options.report.info.useLastFormat) {
        if (selectedComponents.length > 0 && !this.IsBandComponent(selectedComponents[0]) && selectedComponents[0].typeComponent != "StiPage" && selectedComponents[0].typeComponent != "StiReport") {
            params.lastStyleProperties = this.GetStylePropertiesFromComponent(selectedComponents[0]);
        }
    }

    if (selectedItem.itemObject.typeItem == "Column") {
        params.columnName = selectedItem.itemObject.name;
        var columnParent = this.options.dictionaryTree.getCurrentColumnParent();
        params.currentParentType = columnParent.type;
        params.currentParentName = (columnParent.type == "BusinessObject") ? selectedItem.getBusinessObjectFullName() : columnParent.name;
    }
    else if (selectedItem.itemObject.typeItem == "Parameter") {
        params.parameterName = selectedItem.itemObject.name;
        var parameterParent = this.options.dictionaryTree.getCurrentColumnParent();
        params.currentParentName = parameterParent.name;
    }
    else if (selectedItem.itemObject.typeItem == "Resource") {
        params.resourceName = selectedItem.itemObject.name;
        params.resourceType = selectedItem.itemObject.type;
    }

    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Get Params From QueryString
StiMobileDesigner.prototype.SendCommandGetParamsFromQueryString = function (queryString, dataSourceName, callbackFunction) {
    var params = {};
    params.command = "GetParamsFromQueryString";
    params.queryString = queryString;
    params.dataSourceName = dataSourceName;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Create Moving Copy Component
StiMobileDesigner.prototype.SendCommandCreateMovingCopyComponent = function (componentName, componentRect, callbackFunction) {
    var params = {};
    params.command = "CreateMovingCopyComponent";
    params.components = [componentName];
    params.pageName = this.options.currentPage.properties.name;
    params.componentRect = componentRect;
    params.zoom = this.options.report.zoom.toString();
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Get Report Check Items
StiMobileDesigner.prototype.SendCommandGetReportCheckItems = function (callbackFunction) {
    var params = {};
    params.command = "GetReportCheckItems";
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Get Check Preview
StiMobileDesigner.prototype.SendCommandGetCheckPreview = function (checkIndex, callbackFunction) {
    var params = {};
    params.command = "GetCheckPreview";
    params.checkIndex = checkIndex;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Action Check
StiMobileDesigner.prototype.SendCommandActionCheck = function (checkIndex, actionIndex, callbackFunction) {
    var params = {};
    params.command = "ActionCheck";
    params.checkIndex = checkIndex;
    params.actionIndex = actionIndex;
    params.zoom = this.options.report.zoom.toString();
    params.selectedObjectName = this.options.selectedObject.properties.name;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Check Expression
StiMobileDesigner.prototype.SendCommandCheckExpression = function (expressionText, callbackFunction) {
    var params = {};
    params.command = "CheckExpression";
    params.expressionText = expressionText;
    params.componentName = this.options.selectedObject ? this.options.selectedObject.properties.name : null;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Get Globalization Strings
StiMobileDesigner.prototype.SendCommandGetGlobalizationStrings = function (callbackFunction) {
    var params = {};
    params.command = "GetGlobalizationStrings";    
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Add Globalization Strings
StiMobileDesigner.prototype.SendCommandAddGlobalizationStrings = function (cultureName, callbackFunction) {
    var params = {};
    params.command = "AddGlobalizationStrings";
    params.callbackFunctionId = this.generateKey();
    params.cultureName = cultureName;
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Remove Globalization Strings
StiMobileDesigner.prototype.SendCommandRemoveGlobalizationStrings = function (index, callbackFunction) {
    var params = {};
    params.command = "RemoveGlobalizationStrings";
    params.callbackFunctionId = this.generateKey();
    params.index = index;
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Get Culture Settings From Report
StiMobileDesigner.prototype.GetCultureSettingsFromReport = function (index, callbackFunction) {
    var params = {};
    params.command = "GetCultureSettingsFromReport";
    params.callbackFunctionId = this.generateKey();
    params.index = index;
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Set Culture Settings To Report
StiMobileDesigner.prototype.SetCultureSettingsToReport = function (cultureName, callbackFunction) {
    var params = {};
    params.command = "SetCultureSettingsToReport";    
    params.callbackFunctionId = this.generateKey();
    params.cultureName = cultureName;
    params.zoom = this.options.report.zoom.toString();
    params.selectedObjectName = this.options.selectedObject.properties.name;
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Apply Globalization Strings
StiMobileDesigner.prototype.SendCommandApplyGlobalizationStrings = function (index, propertyName, propertyValue) {
    var params = {};
    params.command = "ApplyGlobalizationStrings";
    params.index = index;
    params.propertyName = propertyName;
    params.propertyValue = propertyValue;
    this.AddCommandToStack(params);
}

//Send Clone Chart Component
StiMobileDesigner.prototype.SendCommandStartEditGaugeComponent = function (componentName) {
    var params = {};
    params.componentName = componentName;
    params.command = "StartEditGaugeComponent";
    this.AddCommandToStack(params);
}

//Get Resource Content
StiMobileDesigner.prototype.SendCommandGetResourceContent = function (resourceName, callbackFunction) {
    var params = {};
    params.command = "GetResourceContent";
    params.resourceName = resourceName;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Convert Resource Content
StiMobileDesigner.prototype.SendCommandConvertResourceContent = function (content, type, callbackFunction) {
    var params = {};
    params.command = "ConvertResourceContent";
    params.content = content;
    params.type = type;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Get Resource Text
StiMobileDesigner.prototype.SendCommandGetResourceText = function (resourceName, callbackFunction) {
    var params = {};
    params.command = "GetResourceText";
    params.resourceName = resourceName;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Set Resource Text
StiMobileDesigner.prototype.SendCommandSetResourceText = function (resourceName, resourceText) {
    var params = {};
    params.command = "SetResourceText";
    params.resourceName = resourceName;
    params.resourceText = resourceText;

    this.AddCommandToStack(params);
}

//Get Resource View Data
StiMobileDesigner.prototype.SendCommandGetResourceViewData = function (resourceName, resourceType, resourceContent, callbackFunction) {
    var params = {};
    params.command = "GetResourceViewData";
    params.resourceName = resourceName;
    params.resourceType = resourceType;
    if (resourceContent) params.resourceContent = resourceContent;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Send Update Gauge Component
StiMobileDesigner.prototype.SendCommandUpdateGaugeComponent = function (componentName, updateParameters) {
    var params = {};
    params.componentName = componentName;
    params.updateParameters = updateParameters;
    params.command = "UpdateGaugeComponent";
    this.AddCommandToStack(params);
}

// Create Image Component
StiMobileDesigner.prototype.SendCommandCreateImageComponent = function (itemObject, point, pageName) {
    this.options.reportIsModified = true;
    var params = {
        command: "CreateImageComponent",
        itemObject: itemObject,
        pageName: pageName,
        zoom: this.options.report.zoom.toString(),
        point: point
    }

    var selectedObject = this.options.selectedObjects ? this.options.selectedObjects[0] : this.options.selectedObject;

    if (selectedObject && !this.IsBandComponent(selectedObject) && this.options.report.info.useLastFormat &&
        selectedObject.typeComponent != "StiPage" && selectedObject.typeComponent != "StiReport") {
        params.lastStyleProperties = this.GetStylePropertiesFromComponent(selectedObject);
    }

    this.AddCommandToStack(params);
}

//Get Sample Connection String
StiMobileDesigner.prototype.SendCommandGetSampleConnectionString = function (typeConnection, callbackFunction) {
    var params = {};
    params.command = "GetSampleConnectionString";
    params.typeConnection = typeConnection;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Create Data Item From Resource
StiMobileDesigner.prototype.SendCommandCreateDatabaseFromResource = function (resourceObject) {
    var params = {};
    params.command = "CreateDatabaseFromResource";
    params.resourceObject = resourceObject;

    this.AddCommandToStack(params);
}

//Send Clone BarCode Component
StiMobileDesigner.prototype.SendCommandStartEditBarCodeComponent = function (componentName) {
    var params = {};
    params.componentName = componentName;
    params.command = "StartEditBarCodeComponent";
    this.AddCommandToStack(params);
}

//Move Dictionary Item
StiMobileDesigner.prototype.SendCommandMoveDictionaryItem = function (fromObject, toObject, direction) {
    var params = {};
    params.command = "MoveDictionaryItem";
    params.direction = direction;
    params.fromObject = fromObject;
    params.toObject = toObject;

    if (fromObject.typeItem == "Column") {
        var columnParent = this.options.dictionaryTree.getCurrentColumnParent();
        params.currentParentType = columnParent.type;
        params.currentParentName = (columnParent.type == "BusinessObject") ? this.options.dictionaryTree.selectedItem.getBusinessObjectFullName() : columnParent.name;
    }

    this.AddCommandToStack(params);
}

//Send Any Command
StiMobileDesigner.prototype.SendCommandToDesignerServer = function (commandName, parameters, callbackFunction) {
    var params = {};
    params.command = commandName;

    if (callbackFunction) {
        params.callbackFunctionId = this.generateKey();
        this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    }

    if (parameters) {
        for (var key in parameters) {
            params[key] = parameters[key];
        }
    }

    this.AddCommandToStack(params);
}