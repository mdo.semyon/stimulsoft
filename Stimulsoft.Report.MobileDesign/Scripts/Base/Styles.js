
StiMobileDesigner.prototype.GetStyles = function (name) {
    var styles = {
        "ComponentButton" :
            {
                "default" : "stiDesignerComponentButton",
                "over" : "stiDesignerComponentButtonOver",
                "selected" : "stiDesignerComponentButtonSelected",
                "disabled" : "stiDesignerComponentButtonDisabled"
            },
        "StatusPanelButton" :
            {
                "default" : "stiDesignerStatusPanelButton",
                "over" : "stiDesignerStatusPanelButtonOver",
                "selected" : "stiDesignerStatusPanelButtonSelected",
                "disabled" : "stiDesignerStatusPanelButtonDisabled"
            },
        "ToolButton" :
            {
                "default" : "stiDesignerToolButton",
                "over" : "stiDesignerToolButtonOver",
                "selected" : "stiDesignerToolButtonSelected",
                "disabled" : "stiDesignerToolButton"
            },
       "ToolButtonDinamic" :
            {
                "default" : "stiDesignerToolButtonDinamic",
                "over" : "stiDesignerToolButtonOverDinamic",
                "selected" : "stiDesignerToolButtonSelectedDinamic",
                "disabled" : "stiDesignerToolButtonDinamic"
            },
       "FileButton" :
            {
                "default" : "stiDesignerFileButton",
                "over" : "stiDesignerFileButtonOver",
                "selected" : "stiDesignerFileButton",
                "disabled" : "stiDesignerFileButton"
            },
       "FormButton" :
            {
                "default" : "stiDesignerFormButton",
                "over" : "stiDesignerFormButtonOver",
                "selected" : "stiDesignerFormButtonSelected",
                "disabled" : "stiDesignerFormButtonDisabled"
            },      
       "GroupBlockButton" :
            {
                "default" : "stiDesignerGroupBlockButton",
                "over" : "stiDesignerGroupBlockButtonOver",
                "selected" : "stiDesignerGroupBlockButtonSelected",
                "disabled" : "stiDesignerGroupBlockButton"
            },
       "StandartSmallButton" :
            {
                "default" : "stiDesignerStandartSmallButton",
                "over" : "stiDesignerStandartSmallButtonOver",
                "selected" : "stiDesignerStandartSmallButtonSelected",
                "disabled" : "stiDesignerStandartSmallButtonDisabled"
            },
       "StandartBigButton" :
            {
                "default" : "stiDesignerStandartBigButton",
                "over" : "stiDesignerStandartBigButtonOver",
                "selected" : "stiDesignerStandartBigButtonSelected",
                "disabled" : "stiDesignerStandartBigButtonDisabled"
            },
       "DropDownListButton" :
            {
                "default" : "stiDesignerDropDownListButton",
                "over" : "stiDesignerDropDownListButtonOver",
                "selected" : "stiDesignerDropDownListButtonSelected",
                "disabled" : "stiDesignerDropDownListButtonDisabled"
            },
       "MenuStandartItem" :
            {
                "default" : "stiDesignerMenuStandartItem",
                "over" : "stiDesignerMenuStandartItemOver",
                "selected" : "stiDesignerMenuStandartItemSelected",
                "disabled" : "stiDesignerMenuStandartItemDisabled"
            },
       "MenuBigItem" :
            {
                "default" : "stiDesignerMenuBigItem",
                "over" : "stiDesignerMenuBigItemOver",
                "selected" : "stiDesignerMenuBigItemSelected",
                "disabled" : "stiDesignerMenuBigItemDisabled"
            },
       "MenuMiddleItem" :
            {
                "default" : "stiDesignerMenuMiddleItem",
                "over" : "stiDesignerMenuMiddleItemOver",
                "selected" : "stiDesignerMenuMiddleItemSelected",
                "disabled" : "stiDesignerMenuMiddleItemDisabled"
            },
       "PagesButton" :
            {
                "default" : "stiDesignerPagesButton",
                "over" : "stiDesignerPagesButtonOver",
                "selected" : "stiDesignerPagesButtonSelected",
                "disabled" : "stiDesignerPagesButtonDisabled"
            },
       "StandartTab" :
            {
                "default" : "stiDesignerStandartTab",
                "over" : "stiDesignerStandartTabOver",
                "selected" : "stiDesignerStandartTabSelected",
                "disabled" : "stiDesignerStandartTabDisabled"
            },
       "PropertyButton" :
            {
                "default" : "stiDesignerPropertyButton",
                "over" : "stiDesignerPropertyButtonOver",
                "selected" : "stiDesignerPropertyButtonSelected",
                "disabled" : "stiDesignerPropertyButtonDisabled"
            },
       "PropertiesGroupHeaderButton" :
            {
                "default" : "stiDesignerPropertiesGroupHeaderButton",
                "over" : "stiDesignerPropertiesGroupHeaderButtonOver",
                "selected" : "stiDesignerPropertiesGroupHeaderButtonSelected",
                "disabled" : "stiDesignerPropertiesGroupHeaderButtonDisabled"
            },
       "PropertiesBrushControlButton" :
            {
                "default" : "stiDesignerPropertiesBrushControlButton",
                "over" : "stiDesignerPropertiesBrushControlButtonOver",
                "selected" : "stiDesignerPropertiesBrushControlButtonSelected",
                "disabled" : "stiDesignerPropertiesBrushControlButtonDisabled"
            },
       "NewReportButton" :
            {
                "default" : "stiDesignerNewReportButton",
                "over" : "stiDesignerNewReportButtonOver",
                "selected" : "stiDesignerNewReportButtonSelected",
                "disabled" : "stiDesignerNewReportButtonDisabled"
            },
        "WizardFormColumnsOrderItem" :
            {
                "default" : "stiDesignerWizardFormColumnsOrderItem",
                "over" : "stiDesignerWizardFormColumnsOrderItemOver",
                "selected" : "stiDesignerWizardFormColumnsOrderItemSelected",
                "disabled" : "stiDesignerWizardFormColumnsOrderItemDisabled"
            },
        "HyperlinkButton":
            {
                "default": "stiDesignerHyperlinkButton",
                "over": "stiDesignerHyperlinkButtonOver",
                "selected": "stiDesignerHyperlinkButtonSelected",
                "disabled": "stiDesignerHyperlinkButtonDisabled"
            },
        "PropertiesPanelFooterTabButton" :
            {
                "default" : "stiDesignerPropertiesPanelFooterTabButton",
                "over" : "stiDesignerPropertiesPanelFooterTabButtonOver",
                "selected" : "stiDesignerPropertiesPanelFooterTabButtonSelected",
                "disabled" : "stiDesignerPropertiesPanelFooterTabButtonDisabled"
            },
        "DatePickerDayButton":
           {
                "default": "stiDesignerDatePickerDayButton",
                "over": "stiDesignerDatePickerDayButtonOver",
                "selected": "stiDesignerDatePickerDayButtonSelected",
                "disabled": "stiDesignerDatePickerDayButtonDisabled"
           },
        "DateControlDropDownListButton":
           {
                "default" : "stiDesignerDateControlDropDownListButton",
                "over" : "stiDesignerDateControlDropDownListButtonOver",
                "selected" : "stiDesignerDateControlDropDownListButtonSelected",
                "disabled" : "stiDesignerDateControlDropDownListButtonDisabled"
           },
        "DesignerStylesItem":
           {
               "default": "stiDesignerDesignerStylesItem",
               "over": "stiDesignerDesignerStylesItemOver",
               "selected": "stiDesignerDesignerStylesItemSelected",
               "disabled": "stiDesignerDesignerStylesItemDisabled"
           },
        "VerticalButton":
            {
                "default": "stiDesignerVerticalButton",
                "over": "stiDesignerVerticalButtonOver",
                "selected": "stiDesignerVerticalButtonSelected",
                "disabled": "stiDesignerVerticalButtonDisabled"
            },
        "ContainerItem":
            {
                "default": "stiDesignerContainerItem",
                "over": "stiDesignerContainerItemOver",
                "selected": "stiDesignerContainerItemSelected",
                "disabled": "stiDesignerContainerItemDisabled"
            },
        "PinButton" :
            {
                "default" : "stiDesignerPinButton",
                "over" : "stiDesignerPinButtonOver",
                "selected" : "stiDesignerPinButtonSelected",
                "disabled" : "stiDesignerPinButtonDisabled"
            },
        "GroupPanelHeader":
            {
                "default": "stiDesignerGroupPanelHeader",
                "over": "stiDesignerGroupPanelHeader",
                "selected": "stiDesignerGroupPanelHeader",
                "disabled": "stiDesignerGroupPanelHeader"
            },
        "TextBoxEnumeratorButton":
            {
                "default": "stiDesignerTextBoxEnumeratorButton",
                "over": "stiDesignerTextBoxEnumeratorButtonOver",
                "selected": "stiDesignerTextBoxEnumeratorButtonSelected",
                "disabled": "stiDesignerTextBoxEnumeratorButtonDisabled"
            }
    }
    
    return styles[name];
}