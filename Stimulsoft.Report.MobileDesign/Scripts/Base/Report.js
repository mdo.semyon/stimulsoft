
StiMobileDesigner.prototype.ParseReport = function(reportInObject) {
    if (reportInObject == null) return null;
    return JSON.parse(reportInObject);
}

StiMobileDesigner.prototype.LoadReport = function (reportObject, notResetToHomePanel) {
    if (reportObject == null) {
        this.options.report = null;
        if (this.options.processImage) this.options.processImage.hide();
        return;
    }

    this.SetEnabledAllControls(true);

    if (!notResetToHomePanel) {
        this.options.workPanel.showPanel(this.options.homePanel);
        this.options.buttons.homeToolButton.setSelected(true);
    }

    if (!this.options.runWizardAfterLoad && (!this.options.forms.authForm || (this.options.forms.authForm && !this.options.forms.authForm.visible))) {
        this.options.workPanel.changeVisibleState(true);
    }

    this.options.homePanel.updateControls();
    this.options.report = this.InitializeReportObject();
    if (reportObject.encryptedPassword != null) this.options.report.encryptedPassword = reportObject.encryptedPassword;
    this.options.reportIsModified = false;
    this.options.report.zoom = this.StrToDouble(reportObject.zoom);
    this.options.controls.zoomScale.setZoomPosition();
    this.options.report.properties = reportObject.properties;
    this.options.report.info = reportObject.info;
    this.options.buttons.unitButton.updateCaption(this.options.report.properties.reportUnit);
    this.options.buttons.undoButton.setEnabled(false);
    this.options.buttons.redoButton.setEnabled(false);
    this.options.report.gridSize = this.StrToDouble(reportObject.gridSize);
    this.options.report.dictionary = reportObject.dictionary;
    this.options.dictionaryTree.build(reportObject.dictionary, true);
    this.options.report.stylesCollection = reportObject.stylesCollection;
    this.options.report.pages = {};
    this.options.paintPanel.clear();
    this.ClearAllGalleries();
    if (this.options.showPreviewButton) this.options.buttons.previewToolButton.style.display = "";

    for (var indexPage in reportObject.pages) {
        var page = this.CreatePage(reportObject.pages[indexPage]);
        page.repaint();
        this.options.paintPanel.addPage(page);
        this.options.report.pages[page.properties.name] = page;
        this.options.report.pages[page.properties.name].components = {};

        for (var numComponent in reportObject.pages[indexPage].components) {
            if (ComponentCollection[reportObject.pages[indexPage].components[numComponent].typeComponent]) {
                var component = this.CreateComponent(reportObject.pages[indexPage].components[numComponent]);
                if (component) {
                    component.repaint();
                    this.options.report.pages[page.properties.name].components[component.properties.name] = component;
                }
            }
        }

        page.addComponents();
        if (page.properties.pageIndex == 0) {
            this.options.paintPanel.showPage(page);
            this.options.currentPage.setSelected();
        }
    }

    if (this.GetCountObjects(reportObject.pages) == 0) {
        this.SetEnabledAllControls(false);
        var errorMessageForm = this.options.forms.errorMessageForm || this.InitializeErrorMessageForm();
        errorMessageForm.show(this.loc.Errors.Error);
    }
    this.options.pagesPanel.pagesContainer.updatePages();
    clearTimeout(this.options.timerUpdateCache);

    var jsObject = this;
    this.options.timerUpdateCache = setTimeout(function () {
        jsObject.SendCommandUpdateCache();
    }, this.options.timeUpdateCache);

    clearTimeout(this.options.timerAutoSave);
    var this_ = this;
    if (this.options.report.info.enableAutoSaveMode && this.options.report.info.autoSaveInterval) {
        this.options.timerAutoSave = setInterval(function () {
            if (this_.options.report) {
                if (this_.options.cloudMode && this_.options.cloudParameters.reportTemplateItemKey) {
                    this_.SendCommandItemResourceSave(this_.options.cloudParameters.reportTemplateItemKey, Base64.encode(textArea.value));
                }
                else {
                    this_.ActionSaveReport();
                }
            }
        }, this.StrToInt(this.options.report.info.autoSaveInterval) * 60000);
    }

    var reportFile = this.options.report.properties.reportFile;
    if (reportFile != null) reportFile = reportFile.substring(reportFile.lastIndexOf("/")).substring(reportFile.lastIndexOf("\\"));
    var reportName = reportFile || Base64.decode(this.options.report.properties.reportName.replace("Base64Code;", ""));

    if (jsObject.options.cloudParameters && jsObject.options.cloudParameters.reportTemplateItemKey && jsObject.options.cloudParameters.reportName) {
        reportName = jsObject.options.cloudParameters.reportName;
    }
    
    this.SetWindowTitle(reportName ? reportName + " - " + this.loc.FormDesigner.title : this.loc.FormDesigner.title);

    var processImage = this.options.processImage || this.InitializeProcessImage();
    processImage.hide();
    if (this.options.reportTree) {
        this.options.reportTree.reset();
        this.options.reportTree.build();
    }
}

StiMobileDesigner.prototype.CloseReport = function () {    
    this.options.report = null;
    this.options.selectedObject = null;
    this.options.reportGuid = null;
    this.options.reportIsModified = false;
    this.options.previewPageNumber = 0;
    this.SetEnabledAllControls(false);
    this.options.workPanel.showPanel(this.options.homePanel);
    this.options.buttons.homeToolButton.setSelected(true);
    this.options.paintPanel.clear();
    this.options.pagesPanel.pagesContainer.clear();
    this.options.dictionaryTree.clear();
    this.options.homePanel.updateControls();
    this.options.propertiesPanel.updateControls();
    this.ClearAllGalleries();
    if (this.options.showPreviewButton) this.options.buttons.previewToolButton.style.display = "none";
    if (this.options.layoutPanel) this.options.layoutPanel.updateControls();
    if (this.options.pagePanel) this.options.pagePanel.updateControls();
    if (this.options.dictionaryPanel) this.options.dictionaryPanel.createDataHintItem.style.display = "none";
    clearTimeout(this.options.timerUpdateCache);
    clearTimeout(this.options.timerAutoSave);
    if (!this.options.cloudMode) {
        this.SetWindowTitle(this.loc.FormDesigner.title);
    }
    if (this.options.reportTree) this.options.reportTree.clear();
}

StiMobileDesigner.prototype.ActionCloseReport = function () {  
    if (this.options.reportIsModified) {
        var messageForm = this.MessageFormForSave();
        messageForm.changeVisibleState(true);
        messageForm.action = function (state) {
            if (state) {
                var jsObject = this.jsObject;
                jsObject.ActionSaveReport(function () { jsObject.SendCommandCloseReport(); });
            }
            else { this.jsObject.SendCommandCloseReport(); }
        }
    }
    else this.SendCommandCloseReport();        
}

StiMobileDesigner.prototype.ActionNewReport = function () {    
    this.options.workPanel.showPanel(this.options.homePanel);
    this.options.buttons.homeToolButton.setSelected(true);

    if (this.options.report != null) {
        if (this.options.reportIsModified) {
            var messageForm = this.MessageFormForSave();
            messageForm.changeVisibleState(true);
            messageForm.action = function (state) {
                if (state) {
                    var jsObject = this.jsObject;
                    jsObject.ActionSaveReport(function () { jsObject.SendCommandCreateReport(null, true); });
                }
                else { this.jsObject.SendCommandCreateReport(null, true); }
            }
        }
        else { this.SendCommandCreateReport(null, true); }
    }
    else { this.SendCommandCreateReport(); }
}

StiMobileDesigner.prototype.ActionOpenReport = function () {
    this.options.workPanel.showPanel(this.options.homePanel);
    this.options.buttons.homeToolButton.setSelected(true);
    this.InitializeOpenDialog("openReport", this.StiHandleOpenReport, ".mrt,.mrz,.mrx");
    if (this.options.report != null && this.options.reportIsModified) {
        var messageForm = this.MessageFormForSave();
        messageForm.changeVisibleState(true);
        messageForm.action = function (state) {
            if (state) {
                var jsObject = this.jsObject;
                this.jsObject.ActionSaveReport(function () { jsObject.options.openDialogs.openReport.action(); });
            }
            else {
                this.jsObject.options.openDialogs.openReport.action();
            }
        }
    }
    else { this.options.openDialogs.openReport.action(); }
}

StiMobileDesigner.prototype.ActionSaveReport = function (nextFunc) {
    //debugger;
    if (this.options.cloudMode) {
        if (this.options.cloudParameters.reportTemplateItemKey) {
            this.InitializeSaveDescriptionForm(function (saveDescriptionForm) {
                saveDescriptionForm.nextFunc = nextFunc || null;
                if (saveDescriptionForm.jsObject.options.requestChangesWhenSaving) {
                    saveDescriptionForm.changeVisibleState(true);
                    saveDescriptionForm.textArea.focus();
                }
                else {
                    saveDescriptionForm.action(true);
                }
            });
        }
        else if (this.options.report.properties.reportFile) {
            this.SendCommandSaveReport();
            if (nextFunc) nextFunc();
        }
        else {
            var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
            fileMenu.changeVisibleState(true);
            fileMenu.action(fileMenu.items.saveAsReport);
            fileMenu.items.saveReport.setSelected(true);
            if (this.options.saveAsPanel) {
                this.options.saveAsPanel.header.innerHTML = this.loc.A_WebViewer.SaveReport;
                this.options.saveAsPanel.nextFunc = nextFunc;
            }
        }
    }
    else if (this.options.showSaveDialog && !this.options.report.properties.reportFile) {
        this.InitializeSaveReportForm(function (saveReportForm) {
            saveReportForm.show(false, nextFunc);
        });
    }
    else {
        this.SendCommandSaveReport();
        if (nextFunc) nextFunc();
    }
}

StiMobileDesigner.prototype.ActionSaveAsReport = function () {
    if (this.options.showSaveDialog) {
        this.InitializeSaveReportForm(function (saveReportForm) {
            saveReportForm.show(true);
        });
    }
    else {
        this.SendCommandSaveAsReport();
    }
}

StiMobileDesigner.prototype.OpenReportFromCloud = function (itemObject, notSaveToRecent) {
    var jsObject = this;
    var params = { 
        sessionKey: this.options.cloudParameters.sessionKey,
        itemObject: itemObject 
    };
    if (this.options.isOnlineVersion) params.isOnlineVersion = true;

    this.SendCommandToDesignerServer("LoadReportFromCloud", params, function (answer) {
        if (answer.reportObject && answer.reportGuid) {
            jsObject.options.cloudParameters.reportTemplateItemKey = itemObject.Key;
            jsObject.options.cloudParameters.reportName = itemObject.Name;
            jsObject.CloseReport();
            jsObject.options.reportGuid = answer.reportGuid;
            var reportObject = jsObject.ParseReport(answer.reportObject);
            jsObject.LoadReport(reportObject);
            jsObject.SetWindowTitle(itemObject.Name + " - " + jsObject.loc.FormDesigner.title);
            if (!notSaveToRecent) jsObject.SaveFileToRecentArray(itemObject.Name, itemObject.Key, "StimulsoftMobileDesignerOnlineRecentArray");
        }
        else {
            var errorMessage = answer["errorMessage"] || jsObject.loc.Notices.IsNotFound.replace("{0}", itemObject.Name);
            var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
            errorMessageForm.show(errorMessage);
        }
    });
}

StiMobileDesigner.prototype.AddNewReportItemToCloud = function (reportName, folderKey) {
    var jsObject = this;
    var params = {
        SaveEmptyResources: true,
        AllowSignalsReturn: true,
        Items: [{
            Ident: "ReportTemplateItem",
            Key: this.generateKey(),
            Name: reportName,
            Description: "",
            AttachedItems: this.options.report ? this.options.report.getAttachedItems() : []
        }]
    }

    if (folderKey) params.Items[0].FolderKey = folderKey;

    var processImage = this.options.processImage || this.InitializeProcessImage();
    processImage.show();

    this.SendCloudCommand("ItemSave", params,
        function (data) {
            processImage.hide();
            jsObject.SendCommandItemResourceSave(params.Items[0].Key);
            jsObject.options.cloudParameters.reportTemplateItemKey = params.Items[0].Key;
            jsObject.options.cloudParameters.reportName = reportName;
            jsObject.SetWindowTitle(reportName + " - " + jsObject.loc.FormDesigner.title);
        },
        function (data) {
            processImage.hide();
            var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
            errorMessageForm.show(jsObject.formatResultMsg(data));
        });
}

StiMobileDesigner.prototype.InitializeReportObject = function () {
    var report = {
        jsObject: this,
        typeComponent: "StiReport",
        properties: {},
        info: this.CopyObject(this.options.defaultDesignerOptions)
    };

    report.setSelected = function () {
        this.jsObject.SetSelectedObject(this);
    }


    report.getAttachedItems = function () {
        var attachedItems = [];

        if (report.dictionary.attachedItems) {
            for (var itemType in report.dictionary.attachedItems) {
                var items = report.dictionary.attachedItems[itemType];
                if (items.length > 0) {
                    for (var i = 0; i < items.length; i++) {
                        attachedItems.push(items[i].key);
                    }
                }
            }
        }

        return attachedItems;
    }

    report.getComponentByName = function (name) {
        for (var pageName in this.pages) {
            for (var componentName in this.pages[pageName].components) {
                if (name == componentName) {
                    return this.pages[pageName].components[componentName];
                }
            }
        }
        return null;
    }

    return report;
}