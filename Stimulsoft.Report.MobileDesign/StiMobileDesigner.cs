﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Caching;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using System.Globalization;
using System.Drawing.Printing;
using System.Data;
using Stimulsoft.Base.Serializing;
using System.Runtime.Serialization.Formatters.Binary;
using Stimulsoft.Report.SaveLoad;
using Stimulsoft.Report.Mobile;
using Stimulsoft.Report.Components.TextFormats;
using Stimulsoft.Report.Chart;
using Stimulsoft.Report.CrossTab;
using Stimulsoft.Report.Components.Table;
using System.Threading;
using Stimulsoft.Base.Licenses;
using Stimulsoft.Report.Check;
using Stimulsoft.Report.Gauge;
using Stimulsoft.Report.Helpers;
using System.Text.RegularExpressions;
using System.Linq;
using System.Security.Cryptography;
using Stimulsoft.Report.BarCodes;
using Stimulsoft.Base.Cloud;

using StiResource = Stimulsoft.Report.Dictionary.StiResource;
#if SERVER
using Stimulsoft.Server;
using Stimulsoft.Client;
using Stimulsoft.Server.Objects;
using Stimulsoft.Server.Connect;
#endif

namespace Stimulsoft.Report.MobileDesign
{
#if !SERVER
    [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiWebDesigner component instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
#endif
    public class StiMobileDesigner :
        WebControl,
        INamingContainer,
        ICallbackEventHandler
    {
        #region Cache Helper
        private static class DataSerializer
        {
            public static byte[] Serialize(object o)
            {
                if (o == null)
                {
                    return null;
                }

                BinaryFormatter binaryFormatter = new BinaryFormatter();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    binaryFormatter.Serialize(memoryStream, o);
                    byte[] objectDataAsStream = memoryStream.ToArray();
                    return objectDataAsStream;
                }
            }

            public static T Deserialize<T>(byte[] stream)
            {
                if (stream == null)
                {
                    return default(T);
                }

                BinaryFormatter binaryFormatter = new BinaryFormatter();
                using (MemoryStream memoryStream = new MemoryStream(stream))
                {
                    T result = (T)binaryFormatter.Deserialize(memoryStream);
                    return result;
                }
            }
        }

        abstract public class StiCacheHelper
        {
            abstract public void SaveObjectToCache(object obj, string guid);

            abstract public object GetObjectFromCache(string guid);

            public static object GetObjectFromCacheData(byte[] fileData)
            {
                object data = DataSerializer.Deserialize<object>(fileData);
                try
                {
                    if (data is byte[])
                    {
                        StiReport report = new StiReport();
                        report.Load((byte[])data);
                        report.Info.Zoom = 1;

                        return report;
                    }
                    else if (data is string)
                    {
                        return (string)data;
                    }
                    else if (data is ArrayList)
                    {
                        ArrayList array = (ArrayList)data;
                        for (int i = 0; i < array.Count; i++)
                        {
                            if (array[i] != null)
                            {
                                if (array[i] is byte[])
                                {
                                    StiReport report = new StiReport();
                                    report.Load((byte[])array[i]);
                                    report.Info.Zoom = 1;
                                    array[i] = report;
                                }
                                else if (array[i] is string)
                                {
                                    string str = (string)array[i];
                                    if (!str.StartsWith("<?xml"))
                                    {
                                        string typeComponent = str.Substring(0, str.IndexOf(";"));
                                        string strComponent = str.Substring(str.IndexOf(";") + 1);

                                        Assembly assembly = typeof(StiReport).Assembly;
                                        object component = assembly.CreateInstance("Stimulsoft.Report.Components." + typeComponent) as StiComponent;
                                        if (component == null) component = assembly.CreateInstance("Stimulsoft.Report.Chart." + typeComponent) as StiComponent;
                                        else if (component == null) component = assembly.CreateInstance("Stimulsoft.Report.CrossTab." + typeComponent) as StiComponent;

                                        if (component != null)
                                        {
                                            using (var stringReader = new StringReader(strComponent))
                                            {
                                                var sr = new StiSerializing(new StiReportObjectStringConverter());
                                                sr.Deserialize(component, stringReader, "StiComponent");
                                                stringReader.Close();
                                            }
                                        }
                                        array[i] = component;
                                    }
                                }
                                else if (array[i] is Hashtable)
                                {
                                    StiReport report = new StiReport();
                                    report.Load(((Hashtable)array[i])["report"] as byte[]);
                                    report.Info.Zoom = 1;

                                    StiReportContainer reportContainer = new StiReportContainer(
                                        report, (bool)((Hashtable)array[i])["resourcesIncluded"], ((Hashtable)array[i])["command"] as string);

                                    array[i] = reportContainer;
                                }
                            }
                        }
                        return array;
                    }
                }
                finally
                {
                }

                return null;
            }

            public static byte[] GetCacheDataFromObject(object obj)
            {
                byte[] data = null;

                if (obj is StiReport)
                {
                    data = DataSerializer.Serialize(((StiReport)obj).SaveToByteArray());
                }
                else if (obj is string)
                {
                    data = DataSerializer.Serialize((string)obj);
                }
                else if (obj is ArrayList)
                {
                    ArrayList array = (ArrayList)obj;
                    for (int i = 0; i < array.Count; i++)
                    {
                        if (array[i] != null)
                        {
                            if (array[i] is StiReport)
                            {
                                array[i] = ((StiReport)array[i]).SaveToByteArray();
                            }
                            else if (array[i] is StiComponent)
                            {
                                var sr = new StiSerializing(new StiReportObjectStringConverter());
                                var sb = new StringBuilder();
                                using (var stringWriter = new StringWriter(sb))
                                {
                                    sr.Serialize(array[i], stringWriter, "StiComponent", StiSerializeTypes.SerializeToAll);
                                    stringWriter.Close();
                                    array[i] = array[i].GetType().Name + ";" + sb.ToString();
                                }
                            }
                            else if (array[i] is StiReportContainer)
                            {
                                Hashtable reportContainer = new Hashtable();
                                reportContainer["command"] = (array[i] as StiReportContainer).command;
                                reportContainer["resourcesIncluded"] = (array[i] as StiReportContainer).resourcesIncluded;
                                reportContainer["report"] = (array[i] as StiReportContainer).report.SaveToByteArray();
                                array[i] = reportContainer;
                            }
                        }
                    }
                    data = DataSerializer.Serialize(array);
                }

                return data;
            }
        }

        abstract public class StiCacheHelperForViewer : StiMobileViewer.StiCacheHelper { }
        #endregion

        #region Constants
        internal static string SCRIPTS_PATH = "Stimulsoft.Report.MobileDesign.Scripts";
        internal static string STYLES_PATH = "Stimulsoft.Report.MobileDesign.Css";
        internal static string IMAGES_PATH = "Stimulsoft.Report.MobileDesign.Images";
        internal static string cloudServerAdress = "https://cloud.stimulsoft.com/";
        #endregion

        #region Stimulsoft Reports.Server Client Methods
#if SERVER        
        private StiCommand RunCommand(StiCommand command)
        {
            return StiCommandToServer.RunCommand(command);
        }

        private void CreateDataSourcesFromAttachedItem(StiReport report, ArrayList attachedItems, string sessionKey)
        {
            if (attachedItems != null && attachedItems.Count > 0)
            {
                List<string> attachedKeys = new List<string>();
                foreach (Hashtable attachedItem in attachedItems)
                {
                    attachedKeys.Add(attachedItem["key"] as string);
                }

                StiDataWorker.Report.RegisterData(report, attachedKeys, null);
            }
        }

        public Hashtable GetReportAttachedItems(ArrayList itemKeys, string sessionKey)
        {
            Hashtable attachedItems = new Hashtable();

            if (itemKeys != null && itemKeys.Count > 0)
            {
                var commands = new List<StiCommand>();

                for (int i = 0; i < itemKeys.Count; i++)
                {
                    var getItemCommand = new StiItemCommands.Get
                    {
                        ItemKey = (string)itemKeys[i],
                        SessionKey = sessionKey
                    };
                    commands.Add(getItemCommand);
                }

                var runCommandList = new StiCommandListCommands.Run
                {
                    Commands = commands,
                    ContinueAfterError = true,
                    SessionKey = sessionKey
                };

                var resultCommandList = RunCommand(runCommandList) as StiCommandListCommands.Run;

                if (resultCommandList == null || !resultCommandList.ResultSuccess)
                {
                    return null;
                }

                if (resultCommandList.ResultCommands != null)
                {
                    foreach (var stiCommand in runCommandList.ResultCommands)
                    {
                        var command = (StiItemCommands.Get)stiCommand;
                        if (!command.ResultSuccess && command.ResultItem != null) continue;
                        if (!StiAttachedItemHelper.CanAttachToReportTemplate(command.ResultItem)) continue;

                        string itemGroup = StiAttachedItemHelper.GetDictionaryCloudItemsGroupName(command.ResultItem);
                        if (itemGroup != "Unknown")
                        {
                            Hashtable item = new Hashtable();
                            item["name"] = command.ResultItem.Name;
                            item["description"] = command.ResultItem.Description;
                            item["key"] = command.ResultItem.Key;
                            item["typeItem"] = itemGroup;
                            item["typeIcon"] = "Cloud" + itemGroup;
                            item["isCloudAttachedItem"] = true;
                            if (command.ResultItem is StiFileItem) item["fileType"] = ((StiFileItem)command.ResultItem).FileType;

                            if (attachedItems[itemGroup] == null) attachedItems[itemGroup] = new ArrayList();
                            ((ArrayList)attachedItems[itemGroup]).Add(item);
                        }
                    }
                }
            }

            return attachedItems;
        }

        public static void AddResourcesToReport(StiReport report, ArrayList resourceItems, string sessionKey, bool isOnlineVersion)
        {
            if (resourceItems != null && resourceItems.Count > 0)
            {
                StiServerConnection connection = null;
                var commands = new List<StiCommand>();

                foreach (Hashtable resourceItem in resourceItems)
                {
                    var getItemCommand = new StiItemResourceCommands.Get
                    {
                        ItemKey = resourceItem["key"] as string,
                        SessionKey = sessionKey
                    };
                    commands.Add(getItemCommand);
                }

                var runCommandList = new StiCommandListCommands.Run
                {
                    Commands = commands,
                    ContinueAfterError = true,
                    SessionKey = sessionKey
                };

                if (isOnlineVersion)
                {
                    connection = new StiServerConnection(cloudServerAdress);
                    connection.Accounts.Users.Login(sessionKey, false);
                    runCommandList = connection.RunCommand(runCommandList) as StiCommandListCommands.Run;
                }
                else
                {
                    runCommandList = StiCommandToServer.RunCommand(runCommandList) as StiCommandListCommands.Run;
                }

                if (runCommandList.ResultCommands != null)
                {
                    foreach (var stiCommand in runCommandList.ResultCommands)
                    {
                        int itemIndex = runCommandList.ResultCommands.IndexOf(stiCommand);
                        if (itemIndex < resourceItems.Count)
                        {
                            var resultResourceCommand = (StiItemResourceCommands.Get)stiCommand;
                            if (resultResourceCommand.ResultSuccess && resultResourceCommand.ResultResource != null)
                            {
                                string fileName = ((Hashtable)resourceItems[itemIndex])["fileName"] as string;
                                string itemKey = ((Hashtable)resourceItems[itemIndex])["key"] as string;
                                string resourceName = fileName.Substring(0, fileName.IndexOf("."));
                                if (String.IsNullOrEmpty(resourceName)) resourceName = StiNameCreation.CreateResourceName(report, "Resource");

                                StiResource newResource = new StiResource()
                                {
                                    Name = resourceName,
                                    Content = resultResourceCommand.ResultResource,
                                    Type = StiResourcesHelper.GetResourceTypeByFileName(fileName)
                                };
                                report.Dictionary.Resources.Add(newResource);

                                //Add datasources to report
                                if (StiResourcesHelper.IsDataResourceType(newResource.Type))
                                {
                                    StiFileDatabase database = StiDictionaryHelper.CreateNewDatabaseFromResource(report, newResource);
                                    if (database != null)
                                    {
                                        database.Name = database.Alias = StiDictionaryHelper.GetNewDatabaseName(report, newResource.Name);
                                        database.PathData = StiHyperlinkProcessor.ResourceIdent + newResource.Name;
                                        report.Dictionary.Databases.Add(database);
                                        database.CreateDataSources(report.Dictionary);
                                    }
                                }
                            }
                        }
                    }
                }

                //Remove temp resource items
                List<string> deletingItems = new List<string>();
                foreach (Hashtable resourceItem in resourceItems)
                {
                    deletingItems.Add(resourceItem["key"] as string);
                }
                var commandDelete = new StiItemCommands.Delete()
                {
                    SessionKey = sessionKey,
                    ItemKeys = deletingItems,
                    ResultSuccess = true,
                    AllowMoveToRecycleBin = false,
                    AllowNotifications = false
                };

                if (isOnlineVersion)
                {
                    commandDelete = connection.RunCommand(commandDelete) as StiItemCommands.Delete;
                }
                else
                {
                    commandDelete = StiCommandToServer.RunCommand(commandDelete) as StiItemCommands.Delete;
                }
            }
        }
#endif
        #endregion

        #region Events Args
        #region StiSaveReportEventArgs
        public delegate void StiSaveReportEventHandler(object sender, StiSaveReportEventArgs e);

        public class StiSaveReportEventArgs : EventArgs
        {
            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
            }

            private Hashtable additionalParameters = null;
            public Hashtable AdditionalParameters
            {
                get
                {
                    return additionalParameters;
                }
            }

            private string password = null;
            public string Password
            {
                get
                {
                    return password;
                }
            }

            public StiSaveReportEventArgs(StiReport report, Hashtable additionalParameters)
            {
                this.report = report;
                this.additionalParameters = additionalParameters;
            }

            public StiSaveReportEventArgs(StiReport report, string password)
            {
                this.report = report;
                this.password = password;
            }
        }
        #endregion                

        #region StiSaveAsReportEventArgs
        public delegate void StiSaveAsReportEventHandler(object sender, StiSaveAsReportEventArgs e);

        public class StiSaveAsReportEventArgs : EventArgs
        {
            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
            }

            private string reportFile = "Report";
            public string ReportFile
            {
                get
                {
                    return reportFile;
                }
            }

            private string password = null;
            public string Password
            {
                get
                {
                    return password;
                }
            }

            public StiSaveAsReportEventArgs(StiReport report, string reportFile, string password)
            {
                this.report = report;
                this.reportFile = reportFile;
                this.password = password;
            }
        }
        #endregion                

        #region StiLoadReportEventArgs : Obsolete
        //[Obsolete("This delegate is obsolete. Please use StiGetReportEventHandler instead.")]
        public delegate void StiLoadReportEventHandler(object sender, StiLoadReportEventArgs e);

        //[Obsolete("This class is obsolete. Please use StiGetReportEventArgs instead.")]
        public class StiLoadReportEventArgs : EventArgs
        {
            private DataSet reportDataSet = null;
            public DataSet ReportDataSet
            {
                get
                {
                    return reportDataSet;
                }
                set
                {
                    reportDataSet = value;
                }
            }

            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
            }

            public StiLoadReportEventArgs(StiReport report)
            {
                this.report = report;
            }
        }
        #endregion

        #region StiGetReportEventArgs
        public delegate void StiGetReportEventHandler(object sender, StiGetReportEventArgs e);

        public class StiGetReportEventArgs : EventArgs
        {
            private DataSet reportDataSet = null;
            public DataSet ReportDataSet
            {
                get
                {
                    return reportDataSet;
                }
                set
                {
                    reportDataSet = value;
                }
            }

            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
            }

            public StiGetReportEventArgs(StiReport report)
            {
                this.report = report;
            }
        }
        #endregion

        #region StiCreateReportEventArgs
        public delegate void StiCreateReportEventHandler(object sender, StiCreateReportEventArgs e);

        public class StiCreateReportEventArgs : EventArgs
        {
            private DataSet reportDataSet = null;
            public DataSet ReportDataSet
            {
                get
                {
                    return reportDataSet;
                }
                set
                {
                    reportDataSet = value;
                }
            }

            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
            }

            public StiCreateReportEventArgs(StiReport report)
            {
                this.report = report;
            }
        }
        #endregion

        #region StiPreviewReportEventArgs
        public delegate void StiPreviewReportEventHandler(object sender, StiPreviewReportEventArgs e);

        public class StiPreviewReportEventArgs : EventArgs
        {
            private DataSet reportDataSet = null;
            public DataSet ReportDataSet
            {
                get
                {
                    return reportDataSet;
                }
                set
                {
                    reportDataSet = value;
                }
            }

            private StiReport report = null;
            public StiReport Report
            {
                get
                {
                    return report;
                }
            }

            public StiPreviewReportEventArgs(StiReport report)
            {
                this.report = report;
            }
        }
        #endregion

        #region StiExitDesignerEventArgs : Obsolete
        //[Obsolete("This delegate is obsolete. Please use StiExitEventHandler instead.")]
        public delegate void StiExitDesignerEventHandler(object sender, StiExitDesignerEventArgs e);

        //[Obsolete("This class is obsolete. Please use StiExitEventArgs instead.")]
        public class StiExitDesignerEventArgs : EventArgs
        {
            public StiExitDesignerEventArgs()
            {

            }
        }
        #endregion

        #region StiExitEventArgs
        public delegate void StiExitEventHandler(object sender, StiExitEventArgs e);

        public class StiExitEventArgs : EventArgs
        {
            public StiExitEventArgs()
            {

            }
        }
        #endregion
        #endregion

        #region Events
        #region SaveReport
        /// <summary>
        /// Fires when the user saves the report.
        /// </summary>
        [Description("Fires when the user saves the report.")]
        public event StiSaveReportEventHandler SaveReport;

        private void InvokeSaveReport(StiReport report, Hashtable additionalParameters)
        {
            OnSaveReport(new StiSaveReportEventArgs(report, additionalParameters));
        }

        //private void InvokeSaveReport(StiReport report)
        //{
        //    OnSaveReport(new StiSaveReportEventArgs(report));
        //}

        private void InvokeSaveReport(StiReport report, string password)
        {
            OnSaveReport(new StiSaveReportEventArgs(report, password));
        }

        protected virtual void OnSaveReport(StiSaveReportEventArgs e)
        {
            if (SaveReport != null)
            {
                SaveReport(this, e);
            }
        }
        #endregion       

        #region SaveAsReport
        /// <summary>
        /// Fires when the user saves the report.
        /// </summary>
        [Description("Fires when the user saves the report.")]
        public event StiSaveAsReportEventHandler SaveAsReport;

        //private void InvokeSaveAsReport(StiReport report, string reportFile)
        //{
        //    OnSaveAsReport(new StiSaveAsReportEventArgs(report, reportFile));
        //}

        private void InvokeSaveAsReport(StiReport report, string reportFile, string password)
        {
            OnSaveAsReport(new StiSaveAsReportEventArgs(report, reportFile, password));
        }

        protected virtual void OnSaveAsReport(StiSaveAsReportEventArgs e)
        {
            if (SaveAsReport != null)
            {
                SaveAsReport(this, e);
            }
        }
        #endregion       

        #region GetReport
        [Obsolete("This event is obsolete. Please use GetReport instead.")]
        [Browsable(false)]
        public event StiLoadReportEventHandler LoadReport;

        /// <summary>
        /// Fires when the designer requires the report.
        /// </summary>
        [Description("Fires when the designer requires the report.")]
        public event StiGetReportEventHandler GetReport;

        private void InvokeGetReport(StiReport report)
        {
            if (GetReport != null)
            {
                OnGetReport(new StiGetReportEventArgs(report));
            }
            else
            {
                OnLoadReport(new StiLoadReportEventArgs(report));
            }
        }

        protected virtual void OnGetReport(StiGetReportEventArgs e)
        {
            if (GetReport != null)
            {
                GetReport(this, e);
                StiReport currReport = this.Report;
                if (currReport != null && !currReport.IsDocument && e.ReportDataSet != null) currReport.RegData(e.ReportDataSet);
                if (CacheHelper != null && currReport != null) CacheHelper.SaveObjectToCache(currReport, this.ReportGuid);
            }
        }

        //[Obsolete("This event is obsolete. Please use GetReport instead.")]
        protected virtual void OnLoadReport(StiLoadReportEventArgs e)
        {
            if (LoadReport != null)
            {
                LoadReport(this, e);
                StiReport currReport = this.Report;
                if (currReport != null && !currReport.IsDocument && e.ReportDataSet != null) currReport.RegData(e.ReportDataSet);
                if (CacheHelper != null && currReport != null) CacheHelper.SaveObjectToCache(currReport, this.ReportGuid);
            }
        }
        #endregion  

        #region CreateReport
        /// <summary>
        /// Fires when the user creates a new report.
        /// </summary>
        [Description("Fires when the user creates a new report.")]
        public event StiCreateReportEventHandler CreateReport;

        private void InvokeCreateReport(StiReport report)
        {
            OnCreateReport(new StiCreateReportEventArgs(report));
        }

        protected virtual void OnCreateReport(StiCreateReportEventArgs e)
        {
            if (CreateReport != null)
            {
                CreateReport(this, e);
                StiReport currReport = this.Report;
                if (currReport != null && !currReport.IsDocument && e.ReportDataSet != null) currReport.RegData(e.ReportDataSet);
                if (CacheHelper != null && currReport != null) CacheHelper.SaveObjectToCache(currReport, this.ReportGuid);
            }
        }
        #endregion        

        #region PreviewReport
        /// <summary>
        /// Fires when the user previews the report.
        /// </summary>
        [Description("Fires when the user previews the report.")]
        public event StiPreviewReportEventHandler PreviewReport;

        private void InvokePreviewReport(StiReport report)
        {
            OnPreviewReport(new StiPreviewReportEventArgs(report));
        }

        protected virtual void OnPreviewReport(StiPreviewReportEventArgs e)
        {
            if (PreviewReport != null)
            {
                PreviewReport(this, e);
                StiReport currReport = this.Report;
                if (currReport != null && !currReport.IsDocument && e.ReportDataSet != null) currReport.RegData(e.ReportDataSet);
                if (CacheHelper != null && currReport != null) CacheHelper.SaveObjectToCache(currReport, this.ReportGuid);
            }
        }
        #endregion                        

        #region Exit
        [Obsolete("This event is obsolete. Please use Exit instead.")]
        [Browsable(false)]
        public event StiExitDesignerEventHandler ExitDesigner;

        /// <summary>
        /// Fires when the user pressed Exit in the main menu.
        /// </summary>
        [Description("Fires when the user pressed Exit in the main menu.")]
        public event StiExitEventHandler Exit;

        private void InvokeExit()
        {
            if (Exit != null) { OnExit(new StiExitEventArgs()); }
            else { OnExitDesigner(new StiExitDesignerEventArgs()); }
        }

        protected virtual void OnExit(StiExitEventArgs e)
        {
            if (Exit != null)
            {
                Exit(this, e);
            }
        }

        //[Obsolete("This event is obsolete. Please use GetReport instead.")]
        protected virtual void OnExitDesigner(StiExitDesignerEventArgs e)
        {
            if (ExitDesigner != null)
            {
                ExitDesigner(this, e);
            }
        }
        #endregion       
        #endregion

        #region Properties
        #region CloudMode
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool CloudMode
        {
            get
            {
                object cloudMode = this.ViewState["CloudMode"];
                return cloudMode is bool ? (bool)cloudMode : false;
            }
            set
            {
                this.ViewState["CloudMode"] = value;
            }
        }
        #endregion

        #region ScrollbiMode
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool ScrollbiMode
        {
            get
            {
                object scrollbiMode = this.ViewState["ScrollbiMode"];
                return scrollbiMode is bool ? (bool)scrollbiMode : false;
            }
            set
            {
                this.ViewState["ScrollbiMode"] = value;
            }
        }
        #endregion

        #region ScrollbiParameters
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        private Hashtable scrollbiParameters = null;
        public Hashtable ScrollbiParameters
        {
            get
            {
                return scrollbiParameters;
            }
            set
            {
                scrollbiParameters = value;
            }
        }
        #endregion

        #region ComponentsIntoInsertTab
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        private StiDesignerComponents[] componentsIntoInsertTab = null;
        public StiDesignerComponents[] ComponentsIntoInsertTab
        {
            get
            {
                return componentsIntoInsertTab;
            }
            set
            {
                componentsIntoInsertTab = value;
            }
        }
        #endregion

        #region Cache Helper
        private StiCacheHelper cacheHelper = null;
        /// <summary>
        /// This property to override methods of caching. 
        /// </summary>
        public StiCacheHelper CacheHelper
        {
            get
            {
                return cacheHelper;
            }
            set
            {
                cacheHelper = value;
            }
        }
        #endregion

        #region Cache Helper For Viewer
        private StiCacheHelperForViewer cacheHelperForViewer = null;
        /// <summary>
        /// This property to override methods of caching for viewer. 
        /// </summary>
        public StiCacheHelperForViewer CacheHelperForViewer
        {
            get
            {
                return cacheHelperForViewer;
            }
            set
            {
                cacheHelperForViewer = value;
            }
        }
        #endregion

        #region Settings
        #region Theme
        /// <summary>
        /// Gets or sets the current visual theme which is used for drawing visual elements of the mobile designer.
        /// </summary>        
        [Category("Settings")]
        [DefaultValue(StiMobileDesignerTheme.Office2013)]
        [Description("Gets or sets the current visual theme which is used for drawing visual elements of the mobile designer.")]
        public StiMobileDesignerTheme Theme
        {
            get
            {
                object theme = this.ViewState["Theme"];
                return theme is StiMobileDesignerTheme ? (StiMobileDesignerTheme)theme : StiMobileDesignerTheme.Office2013WhiteBlue;
            }
            set
            {
                if (value == StiMobileDesignerTheme.Blue || value == StiMobileDesignerTheme.Office2013) value = StiMobileDesignerTheme.Office2013WhiteBlue;
                if (value == StiMobileDesignerTheme.Purple) value = StiMobileDesignerTheme.Office2013WhitePurple;
                this.ViewState["Theme"] = value;
            }
        }
        #endregion

        #region WebImageHost
        private StiWebImageHost webImageHost = null;
        /// <summary>
        /// Gets or sets the StiWebImageHost object which converts images to urls.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets the StiWebImageHost object which converts images to urls.")]
        public StiWebImageHost WebImageHost
        {
            get
            {
                return webImageHost;
            }
            set
            {
                webImageHost = value;
            }
        }
        #endregion

        #region ServerTimeout
        private TimeSpan serverTimeout = new TimeSpan(0, 20, 0);
        /// <summary>
        /// Gets or sets a value which indicates how much time do the report rendering result will be stored in the server cache.
        /// </summary>
        [Category("Settings")]
        [Description("Gets or sets a value which indicates how much time do the report rendering result will be stored in the server cache.")]
        public TimeSpan ServerTimeout
        {
            get
            {
                return serverTimeout;
            }
            set
            {
                serverTimeout = value;
            }
        }
        #endregion

        #region AllowAutoUpdateCache
        /// <summary>
        /// Allows the designer to update the cache automatically.
        /// </summary>
        [DefaultValue(true)]
        [Category("Settings")]
        [Description("Allows the designer to update the cache automatically.")]
        public bool AllowAutoUpdateCache
        {
            get
            {
                object allowAutoUpdateCache = this.ViewState["AllowAutoUpdateCache"];
                return allowAutoUpdateCache is bool ? (bool)allowAutoUpdateCache : true;
            }
            set
            {
                this.ViewState["AllowAutoUpdateCache"] = value;
            }
        }
        #endregion


        #region ReportGuid
        private string reportGuid = string.Empty;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public string ReportGuid
        {
            get
            {
                if (reportGuid == string.Empty) reportGuid = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);
                return reportGuid;
            }
            set
            {
                reportGuid = value;
            }
        }
        #endregion

        #region Report
        private StiReport report = null;
        /// <summary>
        /// Gets or sets a report object which is shown in the Mobile Designer.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets a report object which is shown in the Mobile Designer.")]
        public StiReport Report
        {
            get
            {
                if (report == null)
                {
                    if (CacheHelper != null)
                    {
                        report = CacheHelper.GetObjectFromCache(ReportGuid) as StiReport;
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Page) report = Page.Cache[ReportGuid] as StiReport;
                        else report = Page.Session[ReportGuid] as StiReport;

                        //Ïåðåñîõðàíÿåì êýø äëÿ ñáðîñà òàéìàóòîâ
                        if (report != null)
                        {
                            if (CacheMode == StiCacheMode.Session)
                            {
                                Page.Session.Remove(ReportGuid);
                                Page.Session.Add(ReportGuid, report);
                            }
                            else
                            {
                                Page.Cache.Remove(ReportGuid);
                                Page.Cache.Add(ReportGuid, report, null, Cache.NoAbsoluteExpiration, serverTimeout, CacheItemPriority.Low, null);
                            }
                        }
                    }
                }

                if (report != null) report.Info.ForceDesigningMode = true;
                return report;
            }
            set
            {
                report = value;
                if (!DesignMode && report != null)
                {
                    if (CacheHelper != null)
                    {
                        CacheHelper.SaveObjectToCache(report, ReportGuid);
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Session)
                        {
                            Page.Session.Remove(ReportGuid);
                            Page.Session.Add(ReportGuid, report);
                        }
                        else
                        {
                            Page.Cache.Remove(ReportGuid);
                            Page.Cache.Add(ReportGuid, report, null, Cache.NoAbsoluteExpiration, serverTimeout, CacheItemPriority.Low, null);
                        }
                    }
                }
            }
        }
        #endregion

        #region ReportForPreview
        private StiReport reportForPreview = null;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public StiReport ReportForPreview
        {
            get
            {
                if (reportForPreview == null)
                {
                    if (CacheHelper != null)
                    {
                        reportForPreview = CacheHelper.GetObjectFromCache(ReportGuid + "ForPreview") as StiReport;
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Page) reportForPreview = Page.Cache[ReportGuid + "ForPreview"] as StiReport;
                        else reportForPreview = Page.Session[ReportGuid + "ForPreview"] as StiReport;

                        //Ïåðåñîõðàíÿåì êýø äëÿ ñáðîñà òàéìàóòîâ
                        if (report != null)
                        {
                            if (CacheMode == StiCacheMode.Session)
                            {
                                Page.Session.Remove(ReportGuid + "ForPreview");
                                Page.Session.Add(ReportGuid + "ForPreview", reportForPreview);
                            }
                            else
                            {
                                Page.Cache.Remove(ReportGuid + "ForPreview");
                                Page.Cache.Add(ReportGuid + "ForPreview", reportForPreview, null, Cache.NoAbsoluteExpiration, serverTimeout, CacheItemPriority.Low, null);
                            }
                        }
                    }
                }

                if (reportForPreview != null) report.Info.ForceDesigningMode = true;
                return reportForPreview;
            }
            set
            {
                reportForPreview = value;
                if (!DesignMode && reportForPreview != null)
                {
                    if (CacheHelper != null)
                    {
                        CacheHelper.SaveObjectToCache(reportForPreview, ReportGuid + "ForPreview");
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Session)
                        {
                            Page.Session.Remove(ReportGuid + "ForPreview");
                            Page.Session.Add(ReportGuid + "ForPreview", reportForPreview);
                        }
                        else
                        {
                            Page.Cache.Remove(ReportGuid + "ForPreview");
                            Page.Cache.Add(ReportGuid + "ForPreview", reportForPreview, null, Cache.NoAbsoluteExpiration, serverTimeout, CacheItemPriority.Low, null);
                        }
                    }
                }
            }
        }
        #endregion

        #region ClipboardId
        private string clipboardId = string.Empty;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public string ClipboardId
        {
            get
            {
                if (clipboardId == string.Empty) clipboardId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);
                return clipboardId;
            }
            set
            {
                clipboardId = value;
            }
        }
        #endregion

        #region Clipboard
        private string clipboard = null;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public string Clipboard
        {
            get
            {
                if (clipboard == null)
                {
                    if (CacheHelper != null)
                    {
                        clipboard = CacheHelper.GetObjectFromCache(ClipboardId) as string;
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Page) clipboard = Page.Cache[ClipboardId] as string;
                        else clipboard = Page.Session[ClipboardId] as string;
                    }
                }

                return clipboard;
            }
            set
            {
                clipboard = value;
                if (!DesignMode)
                {

                    if (CacheHelper != null)
                    {
                        CacheHelper.SaveObjectToCache(clipboard, ClipboardId);
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Session)
                        {
                            Page.Session.Remove(ClipboardId);
                            Page.Session.Add(ClipboardId, clipboard);
                        }
                        else
                        {
                            Page.Cache.Remove(ClipboardId);
                            Page.Cache.Add(ClipboardId, clipboard, null, Cache.NoAbsoluteExpiration, serverTimeout, CacheItemPriority.Low, null);
                        }
                    }
                }
            }
        }
        #endregion

        #region UndoArrayId
        private string undoArrayId = string.Empty;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public string UndoArrayId
        {
            get
            {
                if (undoArrayId == string.Empty) undoArrayId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);
                return undoArrayId;
            }
            set
            {
                undoArrayId = value;
            }
        }
        #endregion

        #region UndoArray
        private ArrayList undoArray = null;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public ArrayList UndoArray
        {
            get
            {
                if (undoArray == null)
                {

                    if (CacheHelper != null)
                    {
                        undoArray = CacheHelper.GetObjectFromCache(UndoArrayId) as ArrayList;
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Page) undoArray = Page.Cache[UndoArrayId] as ArrayList;
                        else undoArray = Page.Session[UndoArrayId] as ArrayList;
                    }
                }

                return undoArray;
            }
            set
            {
                undoArray = value;
                if (!DesignMode)
                {
                    if (CacheHelper != null)
                    {
                        CacheHelper.SaveObjectToCache(undoArray, UndoArrayId);
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Session)
                        {
                            Page.Session.Remove(UndoArrayId);
                            Page.Session.Add(UndoArrayId, undoArray);
                        }
                        else
                        {
                            Page.Cache.Remove(UndoArrayId);
                            Page.Cache.Add(UndoArrayId, undoArray, null, Cache.NoAbsoluteExpiration, serverTimeout, CacheItemPriority.Low, null);
                        }
                    }
                }
            }
        }
        #endregion

        #region ComponentCloneId
        private string componentCloneId = string.Empty;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public string ComponentCloneId
        {
            get
            {
                if (componentCloneId == string.Empty) componentCloneId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);
                return componentCloneId;
            }
            set
            {
                componentCloneId = value;
            }
        }
        #endregion

        #region ComponentClone
        private string componentClone = null;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public string ComponentClone
        {
            get
            {
                if (componentClone == null)
                {
                    if (CacheHelper != null)
                    {
                        componentClone = CacheHelper.GetObjectFromCache(ComponentCloneId) as string;
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Page) componentClone = Page.Cache[ComponentCloneId] as string;
                        else componentClone = Page.Session[ComponentCloneId] as string;
                    }
                }

                return componentClone;
            }
            set
            {
                componentClone = value;
                if (!DesignMode)
                {
                    if (CacheHelper != null)
                    {
                        CacheHelper.SaveObjectToCache(componentClone, ComponentCloneId);
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Session)
                        {
                            Page.Session.Remove(ComponentCloneId);
                            Page.Session.Add(ComponentCloneId, componentClone);
                        }
                        else
                        {
                            Page.Cache.Remove(ComponentCloneId);
                            Page.Cache.Add(ComponentCloneId, componentClone, null, Cache.NoAbsoluteExpiration, serverTimeout, CacheItemPriority.Low, null);
                        }
                    }
                }
            }
        }
        #endregion        

        #region ReportCheckersId
        private string reportCheckersId = string.Empty;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public string ReportCheckersId
        {
            get
            {
                if (reportCheckersId == string.Empty) reportCheckersId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);
                return reportCheckersId;
            }
            set
            {
                reportCheckersId = value;
            }
        }
        #endregion

        #region ReportCheckers
        private List<StiCheck> reportCheckers = null;
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public List<StiCheck> ReportCheckers
        {
            get
            {
                if (reportCheckers == null)
                {
                    if (CacheHelper != null)
                    {
                        reportCheckers = CacheHelper.GetObjectFromCache(ReportCheckersId) as List<StiCheck>;
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Page) reportCheckers = Page.Cache[ReportCheckersId] as List<StiCheck>;
                        else reportCheckers = Page.Session[ReportCheckersId] as List<StiCheck>;
                    }
                }

                return reportCheckers;
            }
            set
            {
                reportCheckers = value;
                if (!DesignMode)
                {
                    if (CacheHelper != null)
                    {
                        CacheHelper.SaveObjectToCache(reportCheckers, ReportCheckersId);
                    }
                    else
                    {
                        if (CacheMode == StiCacheMode.Session)
                        {
                            Page.Session.Remove(ReportCheckersId);
                            Page.Session.Add(ReportCheckersId, reportCheckers);
                        }
                        else
                        {
                            Page.Cache.Remove(ReportCheckersId);
                            Page.Cache.Add(ReportCheckersId, reportCheckers, null, Cache.NoAbsoluteExpiration, serverTimeout, CacheItemPriority.Low, null);
                        }
                    }
                }
            }
        }
        #endregion

        #region ImagesPath
#if SERVER
        private string imagesPath = "ImagesCache";
        /// <summary>
        /// Gets or sets a path to the folder with images which is used for showing the Mobile Designer.
        /// </summary>        
        [Category("Behaviour")]
        [DefaultValue("ImagesCache")]
        [Description("Gets or sets a path to the folder with images which is used for showing the Mobile Designer.")]
        public string ImagesPath
        {
            get
            {
                return imagesPath;
            }
            set
            {
                imagesPath = value;
            }
        }
#else
        /// <summary>
        /// Gets or sets a path to the folder with images which is used for showing the Mobile Designer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue("")]
        [Description("Gets or sets a path to the folder with images which is used for showing the Mobile Designer.")]
        public string ImagesPath
        {
            get
            {
                object imagesPath = this.ViewState["ImagesPath"];
                return imagesPath is string ? (string)imagesPath : string.Empty;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string separator = "/";
                    if (value.IndexOf("//") >= 0) separator = "//";
                    if (!value.EndsWith(separator)) value += separator;
                }
                this.ViewState["ImagesPath"] = value;
            }
        }
#endif
        #endregion

        #region ImageFormat
        /// <summary>
        /// Gets or sets an image format.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public ImageFormat ImageFormat
        {
            get
            {
                object imageFormat = this.ViewState["ImageFormat"];
                return imageFormat is ImageFormat ? (ImageFormat)imageFormat : ImageFormat.Png;
            }
            set
            {
                this.ViewState["ImageFormat"] = value;
            }
        }
        #endregion

        #region CacheMode
        /// <summary>
        /// Gets or sets the mode of the report caching.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(StiCacheMode.Page)]
        [Description("Gets or sets the mode of the report caching.")]
        public StiCacheMode CacheMode
        {
            get
            {
                object cacheMode = this.ViewState["CacheMode"];
                return cacheMode is StiCacheMode ? (StiCacheMode)cacheMode : StiCacheMode.Page;
            }
            set
            {
                this.ViewState["CacheMode"] = value;
            }
        }
        #endregion

        #region UseRelativeUrls
        /// <summary>
        /// Gets or sets a value which indicates that the mobile designer will use relative or absolute URLs for getting images.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the mobile designer will use relative or absolute URLs for getting images.")]
        public bool UseRelativeUrls
        {
            get
            {
                object useRelativeUrls = this.ViewState["UseRelativeUrls"];
                return useRelativeUrls is bool ? (bool)useRelativeUrls : true;
            }
            set
            {
                this.ViewState["UseRelativeUrls"] = value;
            }
        }
        #endregion

        #region GlobalizationFile
        /// <summary>
        /// Gets or sets a path to the globalization file for the mobile designer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue("")]
        [Description("Gets or sets a path to the globalization file for the mobile designer.")]
        public string GlobalizationFile
        {
            get
            {
                object globalizationFile = this.ViewState["GlobalizationFile"];
                return globalizationFile is string ? (string)globalizationFile : string.Empty;
            }
            set
            {
                this.ViewState["GlobalizationFile"] = value;
            }
        }
        #endregion

        #region UndoLevel
        [Browsable(false)]
        [Description("Internal use only.")]
        public int UndoLevel = 6;
        #endregion

        #region Show Animation
        /// <summary>
        /// Gets or sets a value which indicates that animation is enabled.
        /// </summary>
        [DefaultValue(true)]
        [Category("Settings")]
        [Description("Gets or sets a value which indicates that animation is enabled.")]
        public bool ShowAnimation
        {
            get
            {
                object showAnimation = this.ViewState["ShowAnimation"];
                return showAnimation is bool ? (bool)showAnimation : true;
            }
            set
            {
                this.ViewState["ShowAnimation"] = value;
            }
        }
        #endregion                

        #region DefaultUnit
        /// <summary>
        /// Gets or sets a default value of unit in the mobile designer.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(StiReportUnitType.Centimeters)]
        [Description("Gets or sets a default value of the report unit in the mobile designer")]
        public StiReportUnitType DefaultUnit
        {
            get
            {
                object defaultUnit = this.ViewState["DefaultUnit"];
                return defaultUnit is StiReportUnitType ? (StiReportUnitType)defaultUnit : StiReportUnitType.Centimeters;
            }
            set
            {
                this.ViewState["DefaultUnit"] = value;
            }
        }
        #endregion

        #region FocusingX
        /// <summary>
        /// Focusing on the x axis. Required if the lines are displayed double.
        /// </summary>
        [DefaultValue(false)]
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool FocusingX
        {
            get
            {
                object focusingX = this.ViewState["FocusingX"];
                return focusingX is bool ? (bool)focusingX : false;
            }
            set
            {
                this.ViewState["FocusingX"] = value;
            }
        }
        #endregion

        #region FocusingY
        /// <summary>
        /// Focusing on the y axis. Required if the lines are displayed double.
        /// </summary>
        [DefaultValue(false)]
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool FocusingY
        {
            get
            {
                object focusingY = this.ViewState["FocusingY"];
                return focusingY is bool ? (bool)focusingY : false;
            }
            set
            {
                this.ViewState["FocusingY"] = value;
            }
        }
        #endregion

        #region DemoMode
        /// <summary>
        /// Internal use only.
        /// </summary>
        [Browsable(false)]
        [Description("Internal use only.")]
        public bool DemoMode
        {
            get
            {
                object demoMode = this.ViewState["DemoMode"];
                return demoMode is bool ? (bool)demoMode : false;
            }
            set
            {
                this.ViewState["DemoMode"] = value;
            }
        }
        #endregion

        #region ShowSaveDialog
        /// <summary>
        /// Gets or sets a visibility of the save dialog of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Settings")]
        [Description("Gets or sets a visibility of the save dialog of the mobile designer.")]
        public bool ShowSaveDialog
        {
            get
            {
                object showSaveDialog = this.ViewState["ShowSaveDialog"];
                return showSaveDialog is bool ? (bool)showSaveDialog : true;
            }
            set
            {
                this.ViewState["ShowSaveDialog"] = value;
            }
        }
        #endregion

        #region ShowTooltips
        /// <summary>
        /// Gets or sets a value which indicates that show or hide tooltips.
        /// </summary>
        [DefaultValue(true)]
        [Category("Settings")]
        [Description("Gets or sets a value which indicates that show or hide tooltips.")]
        public bool ShowTooltips
        {
            get
            {
                object showTooltips = this.ViewState["ShowTooltips"];
                return showTooltips is bool ? (bool)showTooltips : true;
            }
            set
            {
                this.ViewState["ShowTooltips"] = value;
            }
        }
        #endregion        

        #region ShowTooltipsHelp
        /// <summary>
        /// Gets or sets a value which indicates that show or hide tooltips help icon.
        /// </summary>
        [DefaultValue(true)]
        [Category("Settings")]
        [Description("Gets or sets a value which indicates that show or hide tooltips help icon.")]
        public bool ShowTooltipsHelp
        {
            get
            {
                object showTooltipsHelp = this.ViewState["ShowTooltipsHelp"];
                return showTooltipsHelp is bool ? (bool)showTooltipsHelp : true;
            }
            set
            {
                this.ViewState["ShowTooltipsHelp"] = value;
            }
        }
        #endregion

        #region ShowDialogHelp
        /// <summary>
        /// Gets or sets a value which indicates that show or hide help icon in dialogs.
        /// </summary>
        [DefaultValue(true)]
        [Category("Settings")]
        [Description("Gets or sets a value which indicates that show or hide help icon in dialogs.")]
        public bool ShowDialogHelp
        {
            get
            {
                object showDialogHelp = this.ViewState["ShowDialogHelp"];
                return showDialogHelp is bool ? (bool)showDialogHelp : true;
            }
            set
            {
                this.ViewState["ShowDialogHelp"] = value;
            }
        }
        #endregion

        #region PassQueryParametersForResources
        private bool passQueryParametersForResources = true;
        /// <summary>
        /// Gets or sets a value which enables or disables the transfer of URL parameters when requesting the scripts and styles of the mobile viewer.
        /// </summary>  
        [Category("Settings")]
        [DefaultValue(true)]
        [Description("Gets or sets the interface type of the mobile viewer.")]
        public bool PassQueryParametersForResources
        {
            get
            {
                return passQueryParametersForResources;
            }
            set
            {
                passQueryParametersForResources = value;
            }
        }
        #endregion

        #region InterfaceType
        /// <summary>
        /// Gets or sets the interface type of the mobile designer.
        /// </summary>        
        [Category("Settings")]
        [DefaultValue(StiInterfaceType.Auto)]
        [Description("Gets or sets the interface type of the mobile designer.")]
        public StiInterfaceType InterfaceType
        {
            get
            {
                object interfaceType = this.ViewState["InterfaceType"];
                return interfaceType is StiInterfaceType ? (StiInterfaceType)interfaceType : StiInterfaceType.Auto;
            }
            set
            {
                this.ViewState["InterfaceType"] = value;
            }
        }
        #endregion

        #region CustomCss
        /// <summary>
        /// Gets or sets a path to the custom css file for the mobile designer.
        /// </summary>        
        [Category("Settings")]
        [DefaultValue("")]
        [Description("Gets or sets a path to the custom css file for the mobile designer.")]
        public string CustomCss
        {
            get
            {
                object customCss = this.ViewState["CustomCss"];
                return customCss is string ? (string)customCss : string.Empty;
            }
            set
            {
                this.ViewState["CustomCss"] = value;
            }
        }
        #endregion

        #region DatePickerFirstDayOfWeek
        /// <summary>
        /// Gets or sets the first day of week in the date picker.
        /// </summary>        
        [Category("Settings")]
        [DefaultValue(StiFirstDayOfWeek.Monday)]
        [Description("Gets or sets the first day of week in the date picker.")]
        public StiFirstDayOfWeek DatePickerFirstDayOfWeek
        {
            get
            {
                object datePickerFirstDayOfWeek = this.ViewState["DatePickerFirstDayOfWeek"];
                return datePickerFirstDayOfWeek is StiFirstDayOfWeek ? (StiFirstDayOfWeek)datePickerFirstDayOfWeek : StiFirstDayOfWeek.Monday;
            }
            set
            {
                this.ViewState["DatePickerFirstDayOfWeek"] = value;
            }
        }
        #endregion

        #region AllowChangeWindowTitle
        /// <summary>
        /// Allow the designer to change the window title.
        /// </summary>
        [DefaultValue(true)]
        [Category("Settings")]
        [Description("Allow the designer to change the window title.")]
        public bool AllowChangeWindowTitle
        {
            get
            {
                object allowChangeWindowTitle = this.ViewState["AllowChangeWindowTitle"];
                return allowChangeWindowTitle is bool ? (bool)allowChangeWindowTitle : true;
            }
            set
            {
                this.ViewState["AllowChangeWindowTitle"] = value;
            }
        }
        #endregion
        #endregion

        #region ToolBar
        #region ShowSetupToolboxButton
        /// <summary>
        /// Gets or sets a visibility of the setup toolbox button in the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the setup toolbox button in the mobile designer.")]
        public bool ShowSetupToolboxButton
        {
            get
            {
                object showSetupToolboxButton = this.ViewState["ShowSetupToolboxButton"];
                return showSetupToolboxButton is bool ? (bool)showSetupToolboxButton : true;
            }
            set
            {
                this.ViewState["ShowSetupToolboxButton"] = value;
            }
        }
        #endregion

        #region ShowInsertButton
        /// <summary>
        /// Gets or sets a visibility of the insert button in the toolbar of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the insert button in the toolbar of the mobile designer.")]
        public bool ShowInsertButton
        {
            get
            {
                object showInsertButton = this.ViewState["ShowInsertButton"];
                return showInsertButton is bool ? (bool)showInsertButton : true;
            }
            set
            {
                this.ViewState["ShowInsertButton"] = value;
            }
        }
        #endregion

        #region ShowLayoutButton
        /// <summary>
        /// Gets or sets a visibility of the layout button in the toolbar of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the layout button in the toolbar of the mobile designer.")]
        public bool ShowLayoutButton
        {
            get
            {
                object showLayoutButton = this.ViewState["ShowLayoutButton"];
                return showLayoutButton is bool ? (bool)showLayoutButton : true;
            }
            set
            {
                this.ViewState["ShowLayoutButton"] = value;
            }
        }
        #endregion

        #region ShowPageButton
        /// <summary>
        /// Gets or sets a visibility of the page button in the toolbar of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the page button in the toolbar of the mobile designer.")]
        public bool ShowPageButton
        {
            get
            {
                object showPageButton = this.ViewState["ShowPageButton"];
                return showPageButton is bool ? (bool)showPageButton : true;
            }
            set
            {
                this.ViewState["ShowPageButton"] = value;
            }
        }
        #endregion

        #region ShowPreviewButton
        /// <summary>
        /// Gets or sets a visibility of the preview button in the toolbar of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the preview button in the toolbar of the mobile designer.")]
        public bool ShowPreviewButton
        {
            get
            {
                object showPreviewButton = this.ViewState["ShowPreviewButton"];
                return showPreviewButton is bool ? (bool)showPreviewButton : true;
            }
            set
            {
                this.ViewState["ShowPreviewButton"] = value;
            }
        }
        #endregion        

        #region ShowSaveButton
        /// <summary>
        /// Gets or sets a visibility of the save button in the toolbar of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the save button in the toolbar of the mobile designer.")]
        public bool ShowSaveButton
        {
            get
            {
                object showSaveButton = this.ViewState["ShowSaveButton"];
                return showSaveButton is bool ? (bool)showSaveButton : true;
            }
            set
            {
                this.ViewState["ShowSaveButton"] = value;
            }
        }
        #endregion

        #region ShowAboutButton
        /// <summary>
        /// Gets or sets a visibility of the about button in the toolbar of the mobile designer.
        /// </summary>
        [DefaultValue(false)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the about button in the toolbar of the mobile designer.")]
        public bool ShowAboutButton
        {
            get
            {
                object showAboutButton = this.ViewState["ShowAboutButton"];
                return showAboutButton is bool ? (bool)showAboutButton : false;
            }
            set
            {
                this.ViewState["ShowAboutButton"] = value;
            }
        }
        #endregion
                
        #region ShowFileMenu
        /// <summary>
        /// Gets or sets a visibility of the file menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the file menu of the mobile designer.")]
        public bool ShowFileMenu
        {
            get
            {
                object showFileMenu = this.ViewState["ShowFileMenu"];
                return showFileMenu is bool ? (bool)showFileMenu : true;
            }
            set
            {
                this.ViewState["ShowFileMenu"] = value;
            }
        }
        #endregion

        #region ShowFileMenuNew
        /// <summary>
        /// Gets or sets a visibility of the item New in the file menu.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item New in the file menu.")]
        public bool ShowFileMenuNew
        {
            get
            {
                object showFileMenuNew = this.ViewState["ShowFileMenuNew"];
                return showFileMenuNew is bool ? (bool)showFileMenuNew : true;
            }
            set
            {
                this.ViewState["ShowFileMenuNew"] = value;
            }
        }
        #endregion

        #region ShowFileMenuOpen
        /// <summary>
        /// Gets or sets a visibility of the item Open in the file menu.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item Open in the file menu.")]
        public bool ShowFileMenuOpen
        {
            get
            {
                object showFileMenuOpen = this.ViewState["ShowFileMenuOpen"];
                return showFileMenuOpen is bool ? (bool)showFileMenuOpen : true;
            }
            set
            {
                this.ViewState["ShowFileMenuOpen"] = value;
            }
        }
        #endregion

        #region ShowFileMenuSave
        /// <summary>
        /// Gets or sets a visibility of the item Save in the file menu.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item Save in the file menu.")]
        public bool ShowFileMenuSave
        {
            get
            {
                object showFileMenuSave = this.ViewState["ShowFileMenuSave"];
                return showFileMenuSave is bool ? (bool)showFileMenuSave : true;
            }
            set
            {
                this.ViewState["ShowFileMenuSave"] = value;
            }
        }
        #endregion

        #region ShowFileMenuSaveAs
        /// <summary>
        /// Gets or sets a visibility of the item Save As in the file menu.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item Save As in the file menu.")]
        public bool ShowFileMenuSaveAs
        {
            get
            {
                object showFileMenuSaveAs = this.ViewState["ShowFileMenuSaveAs"];
                return showFileMenuSaveAs is bool ? (bool)showFileMenuSaveAs : true;
            }
            set
            {
                this.ViewState["ShowFileMenuSaveAs"] = value;
            }
        }
        #endregion

        #region ShowFileMenuClose
        /// <summary>
        /// Gets or sets a visibility of the item Close in the file menu.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item Close in the file menu.")]
        public bool ShowFileMenuClose
        {
            get
            {
                object showFileMenuClose = this.ViewState["ShowFileMenuClose"];
                return showFileMenuClose is bool ? (bool)showFileMenuClose : true;
            }
            set
            {
                this.ViewState["ShowFileMenuClose"] = value;
            }
        }
        #endregion

        #region ShowFileMenuExit
        /// <summary>
        /// Gets or sets a visibility of the item Exit in the file menu.
        /// </summary>
        [DefaultValue(false)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item Exit in the file menu.")]
        public bool ShowFileMenuExit
        {
            get
            {
                object showFileMenuExit = this.ViewState["ShowFileMenuExit"];
                return showFileMenuExit is bool ? (bool)showFileMenuExit : false;
            }
            set
            {
                this.ViewState["ShowFileMenuExit"] = value;
            }
        }
        #endregion

        #region ShowFileMenuReportSetup
        /// <summary>
        /// Gets or sets a visibility of the item Report Setup in the file menu.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item Report Setup in the file menu.")]
        public bool ShowFileMenuReportSetup
        {
            get
            {
                object showFileMenuReportSetup = this.ViewState["ShowFileMenuReportSetup"];
                return showFileMenuReportSetup is bool ? (bool)showFileMenuReportSetup : true;
            }
            set
            {
                this.ViewState["ShowFileMenuReportSetup"] = value;
            }
        }
        #endregion

        #region ShowFileMenuOptions
        /// <summary>
        /// Gets or sets a visibility of the item Options in the file menu.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item Options in the file menu.")]
        public bool ShowFileMenuOptions
        {
            get
            {
                object showFileMenuOptions = this.ViewState["ShowFileMenuOptions"];
                return showFileMenuOptions is bool ? (bool)showFileMenuOptions : true;
            }
            set
            {
                this.ViewState["ShowFileMenuOptions"] = value;
            }
        }
        #endregion

        #region ShowFileMenuInfo
        /// <summary>
        /// Gets or sets a visibility of the item Info in the file menu.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item Info in the file menu.")]
        public bool ShowFileMenuInfo
        {
            get
            {
                object showFileMenuInfo = this.ViewState["ShowFileMenuInfo"];
                return showFileMenuInfo is bool ? (bool)showFileMenuInfo : true;
            }
            set
            {
                this.ViewState["ShowFileMenuInfo"] = value;
            }
        }
        #endregion

        #region ShowFileMenuAbout
        /// <summary>
        /// Gets or sets a visibility of the item About in the file menu.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item About in the file menu.")]
        public bool ShowFileMenuAbout
        {
            get
            {
                object showFileMenuAbout = this.ViewState["ShowFileMenuAbout"];
                return showFileMenuAbout is bool ? (bool)showFileMenuAbout : true;
            }
            set
            {
                this.ViewState["ShowFileMenuAbout"] = value;
            }
        }
        #endregion

        #region ShowFileMenuHelp
        /// <summary>
        /// Gets or sets a visibility of the item Help in the file menu.
        /// </summary>
        [DefaultValue(true)]
        [Category("Toolbar")]
        [Description("Gets or sets a visibility of the item Help in the file menu.")]
        public bool ShowFileMenuHelp
        {
            get
            {
                object showFileMenuHelp = this.ViewState["ShowFileMenuHelp"];
                return showFileMenuHelp is bool ? (bool)showFileMenuHelp : true;
            }
            set
            {
                this.ViewState["ShowFileMenuHelp"] = value;
            }
        }
        #endregion
        #endregion

        #region Bands
        #region ShowReportTitleBand
        /// <summary>
        /// Gets or sets a visibility of the ReportTitleBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the ReportTitleBand item in the bands menu of the mobile designer.")]
        public bool ShowReportTitleBand
        {
            get
            {
                object showReportTitleBand = this.ViewState["ShowReportTitleBand"];
                return showReportTitleBand is bool ? (bool)showReportTitleBand : true;
            }
            set
            {
                this.ViewState["ShowReportTitleBand"] = value;
            }
        }
        #endregion

        #region ShowReportSummaryBand
        /// <summary>
        /// Gets or sets a visibility of the ReportSummaryBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the ReportSummaryBand item in the bands menu of the mobile designer.")]
        public bool ShowReportSummaryBand
        {
            get
            {
                object showReportSummaryBand = this.ViewState["ShowReportSummaryBand"];
                return showReportSummaryBand is bool ? (bool)showReportSummaryBand : true;
            }
            set
            {
                this.ViewState["ShowReportSummaryBand"] = value;
            }
        }
        #endregion

        #region ShowPageHeaderBand
        /// <summary>
        /// Gets or sets a visibility of the PageHeaderBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the PageHeaderBand item in the bands menu of the mobile designer.")]
        public bool ShowPageHeaderBand
        {
            get
            {
                object showPageHeaderBand = this.ViewState["ShowPageHeaderBand"];
                return showPageHeaderBand is bool ? (bool)showPageHeaderBand : true;
            }
            set
            {
                this.ViewState["ShowPageHeaderBand"] = value;
            }
        }
        #endregion

        #region ShowPageFooterBand
        /// <summary>
        /// Gets or sets a visibility of the PageFooterBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the PageFooterBand item in the bands menu of the mobile designer.")]
        public bool ShowPageFooterBand
        {
            get
            {
                object showPageFooterBand = this.ViewState["ShowPageFooterBand"];
                return showPageFooterBand is bool ? (bool)showPageFooterBand : true;
            }
            set
            {
                this.ViewState["ShowPageFooterBand"] = value;
            }
        }
        #endregion

        #region ShowGroupHeaderBand
        /// <summary>
        /// Gets or sets a visibility of the GroupHeaderBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the GroupHeaderBand item in the bands menu of the mobile designer.")]
        public bool ShowGroupHeaderBand
        {
            get
            {
                object showGroupHeaderBand = this.ViewState["ShowGroupHeaderBand"];
                return showGroupHeaderBand is bool ? (bool)showGroupHeaderBand : true;
            }
            set
            {
                this.ViewState["ShowGroupHeaderBand"] = value;
            }
        }
        #endregion

        #region ShowGroupFooterBand
        /// <summary>
        /// Gets or sets a visibility of the GroupFooterBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the GroupFooterBand item in the bands menu of the mobile designer.")]
        public bool ShowGroupFooterBand
        {
            get
            {
                object showGroupFooterBand = this.ViewState["ShowGroupFooterBand"];
                return showGroupFooterBand is bool ? (bool)showGroupFooterBand : true;
            }
            set
            {
                this.ViewState["ShowGroupFooterBand"] = value;
            }
        }
        #endregion

        #region ShowHeaderBand
        /// <summary>
        /// Gets or sets a visibility of the HeaderBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the HeaderBand item in the bands menu of the mobile designer.")]
        public bool ShowHeaderBand
        {
            get
            {
                object showHeaderBand = this.ViewState["ShowHeaderBand"];
                return showHeaderBand is bool ? (bool)showHeaderBand : true;
            }
            set
            {
                this.ViewState["ShowHeaderBand"] = value;
            }
        }
        #endregion

        #region ShowFooterBand
        /// <summary>
        /// Gets or sets a visibility of the FooterBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the FooterBand item in the bands menu of the mobile designer.")]
        public bool ShowFooterBand
        {
            get
            {
                object showFooterBand = this.ViewState["ShowFooterBand"];
                return showFooterBand is bool ? (bool)showFooterBand : true;
            }
            set
            {
                this.ViewState["ShowFooterBand"] = value;
            }
        }
        #endregion

        #region ShowColumnHeaderBand
        /// <summary>
        /// Gets or sets a visibility of the ColumnHeaderBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the ColumnHeaderBand item in the bands menu of the mobile designer.")]
        public bool ShowColumnHeaderBand
        {
            get
            {
                object showColumnHeaderBand = this.ViewState["ShowColumnHeaderBand"];
                return showColumnHeaderBand is bool ? (bool)showColumnHeaderBand : true;
            }
            set
            {
                this.ViewState["ShowColumnHeaderBand"] = value;
            }
        }
        #endregion

        #region ShowColumnFooterBand
        /// <summary>
        /// Gets or sets a visibility of the ColumnFooterBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the ColumnFooterBand item in the bands menu of the mobile designer.")]
        public bool ShowColumnFooterBand
        {
            get
            {
                object showColumnFooterBand = this.ViewState["ShowColumnFooterBand"];
                return showColumnFooterBand is bool ? (bool)showColumnFooterBand : true;
            }
            set
            {
                this.ViewState["ShowColumnFooterBand"] = value;
            }
        }
        #endregion

        #region ShowDataBand
        /// <summary>
        /// Gets or sets a visibility of the DataBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the DataBand item in the bands menu of the mobile designer.")]
        public bool ShowDataBand
        {
            get
            {
                object showDataBand = this.ViewState["ShowDataBand"];
                return showDataBand is bool ? (bool)showDataBand : true;
            }
            set
            {
                this.ViewState["ShowDataBand"] = value;
            }
        }
        #endregion

        #region ShowHierarchicalBand
        /// <summary>
        /// Gets or sets a visibility of the HierarchicalBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the HierarchicalBand item in the bands menu of the mobile designer.")]
        public bool ShowHierarchicalBand
        {
            get
            {
                object showHierarchicalBand = this.ViewState["ShowHierarchicalBand"];
                return showHierarchicalBand is bool ? (bool)showHierarchicalBand : true;
            }
            set
            {
                this.ViewState["ShowHierarchicalBand"] = value;
            }
        }
        #endregion

        #region ShowChildBand
        /// <summary>
        /// Gets or sets a visibility of the ChildBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the ChildBand item in the bands menu of the mobile designer.")]
        public bool ShowChildBand
        {
            get
            {
                object showChildBand = this.ViewState["ShowChildBand"];
                return showChildBand is bool ? (bool)showChildBand : true;
            }
            set
            {
                this.ViewState["ShowChildBand"] = value;
            }
        }
        #endregion

        #region ShowEmptyBand
        /// <summary>
        /// Gets or sets a visibility of the EmptyBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the EmptyBand item in the bands menu of the mobile designer.")]
        public bool ShowEmptyBand
        {
            get
            {
                object showEmptyBand = this.ViewState["ShowEmptyBand"];
                return showEmptyBand is bool ? (bool)showEmptyBand : true;
            }
            set
            {
                this.ViewState["ShowEmptyBand"] = value;
            }
        }
        #endregion

        #region ShowOverlayBand
        /// <summary>
        /// Gets or sets a visibility of the OverlayBand item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the OverlayBand item in the bands menu of the mobile designer.")]
        public bool ShowOverlayBand
        {
            get
            {
                object showOverlayBand = this.ViewState["ShowOverlayBand"];
                return showOverlayBand is bool ? (bool)showOverlayBand : true;
            }
            set
            {
                this.ViewState["ShowOverlayBand"] = value;
            }
        }
        #endregion

        #region ShowTable
        /// <summary>
        /// Gets or sets a visibility of the Table item in the bands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Bands")]
        [Description("Gets or sets a visibility of the Table item in the bands menu of the mobile designer.")]
        public bool ShowTable
        {
            get
            {
                object showTable = this.ViewState["ShowTable"];
                return showTable is bool ? (bool)showTable : true;
            }
            set
            {
                this.ViewState["ShowTable"] = value;
            }
        }
        #endregion
        #endregion

        #region CrossBands
        #region ShowCrossTab
        /// <summary>
        /// Gets or sets a visibility of the CrossTab item in the crossbands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("CrossBands")]
        [Description("Gets or sets a visibility of the CrossTab item in the crossbands menu of the mobile designer.")]
        public bool ShowCrossTab
        {
            get
            {
                object showCrossTab = this.ViewState["ShowCrossTab"];
                return showCrossTab is bool ? (bool)showCrossTab : true;
            }
            set
            {
                this.ViewState["ShowCrossTab"] = value;
            }
        }
        #endregion

        #region ShowCrossGroupHeaderBand
        /// <summary>
        /// Gets or sets a visibility of the CrossGroupHeaderBand item in the crossbands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("CrossBands")]
        [Description("Gets or sets a visibility of the CrossGroupHeaderBand item in the crossbands menu of the mobile designer.")]
        public bool ShowCrossGroupHeaderBand
        {
            get
            {
                object showCrossGroupHeaderBand = this.ViewState["ShowCrossGroupHeaderBand"];
                return showCrossGroupHeaderBand is bool ? (bool)showCrossGroupHeaderBand : true;
            }
            set
            {
                this.ViewState["ShowCrossGroupHeaderBand"] = value;
            }
        }
        #endregion       

        #region ShowCrossGroupFooterBand
        /// <summary>
        /// Gets or sets a visibility of the CrossGroupFooterBand item in the crossbands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("CrossBands")]
        [Description("Gets or sets a visibility of the CrossGroupFooterBand item in the crossbands menu of the mobile designer.")]
        public bool ShowCrossGroupFooterBand
        {
            get
            {
                object showCrossGroupFooterBand = this.ViewState["ShowCrossGroupFooterBand"];
                return showCrossGroupFooterBand is bool ? (bool)showCrossGroupFooterBand : true;
            }
            set
            {
                this.ViewState["ShowCrossGroupFooterBand"] = value;
            }
        }
        #endregion       

        #region ShowCrossHeaderBand
        /// <summary>
        /// Gets or sets a visibility of the CrossHeaderBand item in the crossbands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("CrossBands")]
        [Description("Gets or sets a visibility of the CrossHeaderBand item in the crossbands menu of the mobile designer.")]
        public bool ShowCrossHeaderBand
        {
            get
            {
                object showCrossHeaderBand = this.ViewState["ShowCrossHeaderBand"];
                return showCrossHeaderBand is bool ? (bool)showCrossHeaderBand : true;
            }
            set
            {
                this.ViewState["ShowCrossHeaderBand"] = value;
            }
        }
        #endregion       

        #region ShowCrossFooterBand
        /// <summary>
        /// Gets or sets a visibility of the CrossFooterBand item in the crossbands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("CrossBands")]
        [Description("Gets or sets a visibility of the CrossFooterBand item in the crossbands menu of the mobile designer.")]
        public bool ShowCrossFooterBand
        {
            get
            {
                object showCrossFooterBand = this.ViewState["ShowCrossFooterBand"];
                return showCrossFooterBand is bool ? (bool)showCrossFooterBand : true;
            }
            set
            {
                this.ViewState["ShowCrossFooterBand"] = value;
            }
        }
        #endregion       

        #region ShowCrossDataBand
        /// <summary>
        /// Gets or sets a visibility of the CrossDataBand item in the crossbands menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("CrossBands")]
        [Description("Gets or sets a visibility of the CrossDataBand item in the crossbands menu of the mobile designer.")]
        public bool ShowCrossDataBand
        {
            get
            {
                object showCrossDataBand = this.ViewState["ShowCrossDataBand"];
                return showCrossDataBand is bool ? (bool)showCrossDataBand : true;
            }
            set
            {
                this.ViewState["ShowCrossDataBand"] = value;
            }
        }
        #endregion
        #endregion

        #region Components
        #region ShowText
        /// <summary>
        /// Gets or sets a visibility of the Text item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Text item in the components menu of the mobile designer.")]
        public bool ShowText
        {
            get
            {
                object showText = this.ViewState["ShowText"];
                return showText is bool ? (bool)showText : true;
            }
            set
            {
                this.ViewState["ShowText"] = value;
            }
        }
        #endregion

        #region ShowTextInCells
        /// <summary>
        /// Gets or sets a visibility of the TextInCells item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the TextInCells item in the components menu of the mobile designer.")]
        public bool ShowTextInCells
        {
            get
            {
                object showTextInCells = this.ViewState["ShowTextInCells"];
                return showTextInCells is bool ? (bool)showTextInCells : true;
            }
            set
            {
                this.ViewState["ShowTextInCells"] = value;
            }
        }
        #endregion

        #region ShowRichText
        /// <summary>
        /// Gets or sets a visibility of the RichText item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the RichText item in the components menu of the mobile designer.")]
        public bool ShowRichText
        {
            get
            {
                object showRichText = this.ViewState["ShowRichText"];
                return showRichText is bool ? (bool)showRichText : true;
            }
            set
            {
                this.ViewState["ShowRichText"] = value;
            }
        }
        #endregion

        #region ShowImage
        /// <summary>
        /// Gets or sets a visibility of the Image item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Image item in the components menu of the mobile designer.")]
        public bool ShowImage
        {
            get
            {
                object showImage = this.ViewState["ShowImage"];
                return showImage is bool ? (bool)showImage : true;
            }
            set
            {
                this.ViewState["ShowImage"] = value;
            }
        }
        #endregion

        #region ShowBarCode
        /// <summary>
        /// Gets or sets a visibility of the BarCode item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the BarCode item in the components menu of the mobile designer.")]
        public bool ShowBarCode
        {
            get
            {
                object showBarCode = this.ViewState["ShowBarCode"];
                return showBarCode is bool ? (bool)showBarCode : true;
            }
            set
            {
                this.ViewState["ShowBarCode"] = value;
            }
        }
        #endregion

        #region ShowShape
        /// <summary>
        /// Gets or sets a visibility of the Shape item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Shape item in the components menu of the mobile designer.")]
        public bool ShowShape
        {
            get
            {
                object showShape = this.ViewState["ShowShape"];
                return showShape is bool ? (bool)showShape : true;
            }
            set
            {
                this.ViewState["ShowShape"] = value;
            }
        }
        #endregion

        #region ShowHorizontalLinePrimitive
        /// <summary>
        /// Gets or sets a visibility of the Horizontal Line Primitive item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Horizontal Line Primitive item in the components menu of the mobile designer.")]
        public bool ShowHorizontalLinePrimitive
        {
            get
            {
                object showHorizontalLinePrimitive = this.ViewState["ShowHorizontalLinePrimitive"];
                return showHorizontalLinePrimitive is bool ? (bool)showHorizontalLinePrimitive : true;
            }
            set
            {
                this.ViewState["ShowHorizontalLinePrimitive"] = value;
            }
        }
        #endregion

        #region ShowVerticalLinePrimitive
        /// <summary>
        /// Gets or sets a visibility of the Vertical Line Primitive item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Vertical Line Primitive item in the components menu of the mobile designer.")]
        public bool ShowVerticalLinePrimitive
        {
            get
            {
                object showVerticalLinePrimitive = this.ViewState["ShowVerticalLinePrimitive"];
                return showVerticalLinePrimitive is bool ? (bool)showVerticalLinePrimitive : true;
            }
            set
            {
                this.ViewState["ShowVerticalLinePrimitive"] = value;
            }
        }
        #endregion

        #region ShowRectanglePrimitive
        /// <summary>
        /// Gets or sets a visibility of the Rectangle Primitive item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Rectangle Primitive item in the components menu of the mobile designer.")]
        public bool ShowRectanglePrimitive
        {
            get
            {
                object showRectanglePrimitive = this.ViewState["ShowRectanglePrimitive"];
                return showRectanglePrimitive is bool ? (bool)showRectanglePrimitive : true;
            }
            set
            {
                this.ViewState["ShowRectanglePrimitive"] = value;
            }
        }
        #endregion

        #region ShowRoundedRectanglePrimitive
        /// <summary>
        /// Gets or sets a visibility of the Rounded Rectangle Primitive item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Rounded Rectangle Primitive item in the components menu of the mobile designer.")]
        public bool ShowRoundedRectanglePrimitive
        {
            get
            {
                object showRoundedRectanglePrimitive = this.ViewState["ShowRoundedRectanglePrimitive"];
                return showRoundedRectanglePrimitive is bool ? (bool)showRoundedRectanglePrimitive : true;
            }
            set
            {
                this.ViewState["ShowRoundedRectanglePrimitive"] = value;
            }
        }
        #endregion

        #region ShowPanel
        /// <summary>
        /// Gets or sets a visibility of the Panel item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Panel item in the components menu of the mobile designer.")]
        public bool ShowPanel
        {
            get
            {
                object showPanel = this.ViewState["ShowPanel"];
                return showPanel is bool ? (bool)showPanel : true;
            }
            set
            {
                this.ViewState["ShowPanel"] = value;
            }
        }
        #endregion

        #region ShowClone
        /// <summary>
        /// Gets or sets a visibility of the Clone item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Clone item in the components menu of the mobile designer.")]
        public bool ShowClone
        {
            get
            {
                object showClone = this.ViewState["ShowClone"];
                return showClone is bool ? (bool)showClone : true;
            }
            set
            {
                this.ViewState["ShowClone"] = value;
            }
        }
        #endregion

        #region ShowCheckBox
        /// <summary>
        /// Gets or sets a visibility of the CheckBox item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the CheckBox item in the components menu of the mobile designer.")]
        public bool ShowCheckBox
        {
            get
            {
                object showCheckBox = this.ViewState["ShowCheckBox"];
                return showCheckBox is bool ? (bool)showCheckBox : true;
            }
            set
            {
                this.ViewState["ShowCheckBox"] = value;
            }
        }
        #endregion

        #region ShowSubReport
        /// <summary>
        /// Gets or sets a visibility of the SubReport item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the SubReport item in the components menu of the mobile designer.")]
        public bool ShowSubReport
        {
            get
            {
                object showSubReport = this.ViewState["ShowSubReport"];
                return showSubReport is bool ? (bool)showSubReport : true;
            }
            set
            {
                this.ViewState["ShowSubReport"] = value;
            }
        }
        #endregion

        #region ShowZipCode
        /// <summary>
        /// Gets or sets a visibility of the ZipCode item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the ZipCode item in the components menu of the mobile designer.")]
        public bool ShowZipCode
        {
            get
            {
                object showZipCode = this.ViewState["ShowZipCode"];
                return showZipCode is bool ? (bool)showZipCode : true;
            }
            set
            {
                this.ViewState["ShowZipCode"] = value;
            }
        }
        #endregion

        #region ShowChart
        /// <summary>
        /// Gets or sets a visibility of the Chart item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Chart item in the components menu of the mobile designer.")]
        public bool ShowChart
        {
            get
            {
                object showChart = this.ViewState["ShowChart"];
                return showChart is bool ? (bool)showChart : true;
            }
            set
            {
                this.ViewState["ShowChart"] = value;
            }
        }
        #endregion

        #region ShowMap
        /// <summary>
        /// Gets or sets a visibility of the Map item in the components menu of the mobile designer.
        /// </summary>
        [DefaultValue(false)]
        [Browsable(false)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Map item in the components menu of the mobile designer.")]
        public bool ShowMap
        {
            get
            {
                object showMap = this.ViewState["ShowMap"];
                return showMap is bool ? (bool)showMap : false;
            }
            set
            {
                this.ViewState["ShowMap"] = value;
            }
        }
        #endregion

        #region ShowGauge
        /// <summary>
        /// Gets or sets a visibility of the Gauge item in the components menu of the mobile designer.
        /// </summary>
        [Browsable(false)]
        [DefaultValue(false)]
        [Category("Components")]
        [Description("Gets or sets a visibility of the Gauge item in the components menu of the mobile designer.")]
        public bool ShowGauge
        {
            get
            {
                object showGauge = this.ViewState["ShowGauge"];
                return showGauge is bool ? (bool)showGauge : false;
            }
            set
            {
                this.ViewState["ShowGauge"] = value;
            }
        }
        #endregion
        #endregion

        #region Properties Grid
        #region ShowPropertiesGrid
        /// <summary>
        /// Gets or sets a visibility of the properties grid in the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Properties Grid")]
        [Description("Gets or sets a visibility of the properties grid in the mobile designer.")]
        public bool ShowPropertiesGrid
        {
            get
            {
                object showPropertiesGrid = this.ViewState["ShowPropertiesGrid"];
                return showPropertiesGrid is bool ? (bool)showPropertiesGrid : true;
            }
            set
            {
                this.ViewState["ShowPropertiesGrid"] = value;
            }
        }
        #endregion

        #region PropertiesGridWidth
        /// <summary>
        /// Gets or sets a width of the properties grid in the mobile designer.
        /// </summary>
        [Category("Properties Grid")]
        [DefaultValue(370)]
        [Description("Gets or sets a width of the properties grid in the mobile designer.")]
        public int PropertiesGridWidth
        {
            get
            {
                object propertiesGridWidth = this.ViewState["PropertiesGridWidth"];
                return propertiesGridWidth is int ? (int)propertiesGridWidth : 370;
            }
            set
            {
                this.ViewState["PropertiesGridWidth"] = value;
            }
        }
        #endregion

        #region PropertiesGridLabelWidth
        /// <summary>
        /// Gets or sets a width of the property label in properties grid in the mobile designer.
        /// </summary>
        [Category("Properties Grid")]
        [DefaultValue(160)]
        [Description("Gets or sets a width of the property label in properties grid in the mobile designer.")]
        public int PropertiesGridLabelWidth
        {
            get
            {
                object propertiesGridLabelWidth = this.ViewState["PropertiesGridLabelWidth"];
                return propertiesGridLabelWidth is int ? (int)propertiesGridLabelWidth : 160;
            }
            set
            {
                this.ViewState["PropertiesGridLabelWidth"] = value;
            }
        }
        #endregion
        #endregion

        #region ShowDictionary
        /// <summary>
        /// Gets or sets a visibility of the dictionary in the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Dictionary")]
        [Description("Gets or sets a visibility of the dictionary in the mobile designer.")]
        public bool ShowDictionary
        {
            get
            {
                object showDictionary = this.ViewState["ShowDictionary"];
                return showDictionary is bool ? (bool)showDictionary : true;
            }
            set
            {
                this.ViewState["ShowDictionary"] = value;
            }
        }
        #endregion

        #region ShowReportTree
        /// <summary>
        /// Gets or sets a visibility of the report tree in the mobile designer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Dictionary")]
        [Description("Gets or sets a visibility of the report tree in the mobile designer.")]
        public bool ShowReportTree
        {
            get
            {
                object showReportTree = this.ViewState["ShowReportTree"];
                return showReportTree is bool ? (bool)showReportTree : true;
            }
            set
            {
                this.ViewState["ShowReportTree"] = value;
            }
        }
        #endregion

        #region PermissionDataSources
        /// <summary>
        /// Gets or sets a value of permissions for datasources in the mobile designer.
        /// </summary>
        [DefaultValue(StiDesignerPermissions.All)]
        [Category("Dictionary")]
        [Description("Gets or sets a value of permissions for datasources in the mobile designer.")]
        public StiDesignerPermissions PermissionDataSources
        {
            get
            {
                object permissionDataSources = this.ViewState["PermissionDataSources"];
                return permissionDataSources is StiDesignerPermissions ? (StiDesignerPermissions)permissionDataSources : StiDesignerPermissions.All;
            }
            set
            {
                this.ViewState["PermissionDataSources"] = value;
            }
        }
        #endregion

        #region PermissionDataConnections
        /// <summary>
        /// Gets or sets a value of permissions for connections in the mobile designer.
        /// </summary>
        [DefaultValue(StiDesignerPermissions.All)]
        [Category("Dictionary")]
        [Description("Gets or sets a value of permissions for connections in the mobile designer.")]
        public StiDesignerPermissions PermissionDataConnections
        {
            get
            {
                object permissionDataConnections = this.ViewState["PermissionDataConnections"];
                return permissionDataConnections is StiDesignerPermissions ? (StiDesignerPermissions)permissionDataConnections : StiDesignerPermissions.All;
            }
            set
            {
                this.ViewState["PermissionDataConnections"] = value;
            }
        }
        #endregion

        #region PermissionDataColumns
        /// <summary>
        /// Gets or sets a value of permissions for columns in the mobile designer.
        /// </summary>
        [DefaultValue(StiDesignerPermissions.All)]
        [Category("Dictionary")]
        [Description("Gets or sets a value of permissions for columns in the mobile designer.")]
        public StiDesignerPermissions PermissionDataColumns
        {
            get
            {
                object permissionDataColumns = this.ViewState["PermissionDataColumns"];
                return permissionDataColumns is StiDesignerPermissions ? (StiDesignerPermissions)permissionDataColumns : StiDesignerPermissions.All;
            }
            set
            {
                this.ViewState["PermissionDataColumns"] = value;
            }
        }
        #endregion

        #region PermissionDataRelations
        /// <summary>
        /// Gets or sets a value of permissions for relations in the mobile designer.
        /// </summary>
        [DefaultValue(StiDesignerPermissions.All)]
        [Category("Dictionary")]
        [Description("Gets or sets a value of permissions for relations in the mobile designer.")]
        public StiDesignerPermissions PermissionDataRelations
        {
            get
            {
                object permissionDataRelations = this.ViewState["PermissionDataRelations"];
                return permissionDataRelations is StiDesignerPermissions ? (StiDesignerPermissions)permissionDataRelations : StiDesignerPermissions.All;
            }
            set
            {
                this.ViewState["PermissionDataRelations"] = value;
            }
        }
        #endregion

        #region PermissionBusinessObjects
        /// <summary>
        /// Gets or sets a value of permissions for business objects in the mobile designer.
        /// </summary>
        [DefaultValue(StiDesignerPermissions.All)]
        [Category("Dictionary")]
        [Description("Gets or sets a value of permissions for business objects in the mobile designer.")]
        public StiDesignerPermissions PermissionBusinessObjects
        {
            get
            {
                object permissionBusinessObjects = this.ViewState["PermissionBusinessObjects"];
                return permissionBusinessObjects is StiDesignerPermissions ? (StiDesignerPermissions)permissionBusinessObjects : StiDesignerPermissions.All;
            }
            set
            {
                this.ViewState["PermissionBusinessObjects"] = value;
            }
        }
        #endregion

        #region PermissionVariables
        /// <summary>
        /// Gets or sets a value of permissions for variables in the mobile designer.
        /// </summary>
        [DefaultValue(StiDesignerPermissions.All)]
        [Category("Dictionary")]
        [Description("Gets or sets a value of permissions for variables in the mobile designer.")]
        public StiDesignerPermissions PermissionVariables
        {
            get
            {
                object permissionVariables = this.ViewState["PermissionVariables"];
                return permissionVariables is StiDesignerPermissions ? (StiDesignerPermissions)permissionVariables : StiDesignerPermissions.All;
            }
            set
            {
                this.ViewState["PermissionVariables"] = value;
            }
        }
        #endregion

        #region PermissionResources
        /// <summary>
        /// Gets or sets a value of permissions for resources in the mobile designer.
        /// </summary>
        [DefaultValue(StiDesignerPermissions.All)]
        [Category("Dictionary")]
        [Description("Gets or sets a value of permissions for resources in the mobile designer.")]
        public StiDesignerPermissions PermissionResources
        {
            get
            {
                object permissionResources = this.ViewState["PermissionResources"];
                return permissionResources is StiDesignerPermissions ? (StiDesignerPermissions)permissionResources : StiDesignerPermissions.All;
            }
            set
            {
                this.ViewState["PermissionResources"] = value;
            }
        }
        #endregion

        #region PermissionSqlParameters
        /// <summary>
        /// Gets or sets a value of permissions for sql parameters in the mobile designer.
        /// </summary>
        [DefaultValue(StiDesignerPermissions.All)]
        [Category("Dictionary")]
        [Description("Gets or sets a value of permissions for sql parameters in the mobile designer.")]
        public StiDesignerPermissions PermissionSqlParameters
        {
            get
            {
                object permissionSqlParameters = this.ViewState["PermissionSqlParameters"];
                return permissionSqlParameters is StiDesignerPermissions ? (StiDesignerPermissions)permissionSqlParameters : StiDesignerPermissions.All;
            }
            set
            {
                this.ViewState["PermissionSqlParameters"] = value;
            }
        }
        #endregion

        #region ReportDisplayMode
        /// <summary>
        /// Gets or sets a method how will show a report in the preview mode.
        /// </summary>
        [Category("Settings")]
        [DefaultValue(StiReportDisplayMode.Table)]
        [Description("Gets or sets a method how the mobile viewer will show a report.")]
        public StiReportDisplayMode ReportDisplayMode
        {
            get
            {
                object reportDisplayMode = this.ViewState["ReportDisplayMode"];
                return reportDisplayMode is StiReportDisplayMode ? (StiReportDisplayMode)reportDisplayMode : StiReportDisplayMode.Table;
            }
            set
            {
                this.ViewState["ReportDisplayMode"] = value;
            }
        }
        #endregion
        #endregion

        #region Fields
        public Hashtable callbackResult = null;
        private string jsParameters = string.Empty;
        private string viewerId = string.Empty;
        public StiMobileViewer Viewer = null;
        #endregion

        #region Methods
        #region Methods.Inner
        /// <summary>
        /// Returns true if specified stream contains packed report.
        /// </summary>
        private static bool IsPackedFile(Stream stream)
        {
            if (stream.Length < 4) return false;

            int first = stream.ReadByte();
            int second = stream.ReadByte();
            int third = stream.ReadByte();
            stream.Seek(-3, SeekOrigin.Current);

            return IsPackedFile(first, second, third);
        }


        /// <summary>
        /// Returns true if specified stream contains packed report.
        /// </summary>
        private static bool IsJsonFile(Stream stream)
        {
            if (stream.Length < 2) return false;

            int first = stream.ReadByte();
            stream.Seek(-1, SeekOrigin.Current);

            return IsJsonFile(first);
        }

        internal static bool IsJsonFile(int first)
        {
            if (first == 0x0000007b) return true;//JSON

            return false;
        }

        internal static bool IsPackedFile(byte[] bytes)
        {
            if (bytes == null || bytes.Length < 3)
                return false;

            return IsPackedFile(bytes[0], bytes[1], bytes[2]);
        }

        internal static bool IsPackedFile(int first, int second, int third)
        {
            //Variant checks bytes for xml signature
            //if (first == 0xEF && second == 0xBB && third == 0xBF)return false;
            //if (first == 60 && second == 63 && third == 120)return false;
            //return true;

            //Variant checks bytes for zip signature
            if (first == 0x1F && second == 0x8B && third == 0x08) return true;//Gzip
            if (first == 0x50 && second == 0x4B && third == 0x03) return true;//PKZip

            return false;
        }


        /// <summary>
        /// Returns true if specified stream contains encrypted report.
        /// </summary>
        private static bool IsEncryptedFile(Stream stream)
        {
            if (stream.Length < 4) return false;

            int first = stream.ReadByte();
            int second = stream.ReadByte();
            int third = stream.ReadByte();
            stream.Seek(-3, SeekOrigin.Current);

            return IsEncryptedFile(first, second, third);
        }

        internal static bool IsEncryptedFile(int first, int second, int third)
        {
            if (first == 0x6D && second == 0x64 && third == 0x78) return true;  //mdx
            return false;
        }

        private void ResponseString(string message)
        {
            var buffer = Encoding.UTF8.GetBytes(message);
            ResponseBuffer(buffer, false, "text/plain");
        }

        //Use only for resources, browser cache enabled
        private void ResponseString(string message, string contentType)
        {
            var buffer = Encoding.UTF8.GetBytes(message);
            ResponseBuffer(buffer, true, contentType);
        }

        private void ResponseBuffer(byte[] buffer)
        {
            ResponseBuffer(buffer, false, null);
        }

        private void ResponseBuffer(byte[] buffer, bool enableBrowserCache, string contentType)
        {
            Page.Response.Buffer = true;
            Page.Response.ClearContent();
            Page.Response.ClearHeaders();
            if (enableBrowserCache)
            {
                Page.Response.AddHeader("Cache-Control", "public, max-age=31536000"); // 1 year cache
                Page.Response.Cache.SetExpires(DateTime.Now.AddYears(1));
                Page.Response.Cache.SetCacheability(HttpCacheability.Public);
            }
            else
                Page.Response.AddHeader("Cache-Control", "no-store");
            Page.Response.AddHeader("Content-Length", buffer.Length.ToString());
            Page.Response.ContentEncoding = Encoding.UTF8;

            if (contentType != null) Page.Response.ContentType = contentType;
            else Page.Response.ContentType = "application/octet-stream";

            Page.Response.BinaryWrite(buffer);

            try
            {
                if (StiOptions.Web.AllowUseResponseFlush) Page.Response.Flush();
                Page.Response.End();
            }
            catch
            {

            }
        }

        private static void ResponseFile(Page page, MemoryStream stream, string contentType, string fileName)
        {
            fileName = fileName.Replace(" ", "");

            page.Response.Buffer = true;
            page.Response.ClearContent();
            page.Response.StatusCode = 200;
            if (StiOptions.Web.ClearResponseHeaders) page.Response.ClearHeaders();
            page.Response.ContentType = string.Format("{0};{1};", contentType, fileName);
            page.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            page.Response.Cache.SetExpires(DateTime.MinValue);
            page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            page.Response.ContentEncoding = Encoding.UTF8;
            page.Response.AddHeader("Content-Length", stream.Length.ToString());
            page.Response.BinaryWrite(stream.ToArray());

            if (StiOptions.Web.AllowGCCollect)
            {
                GC.Collect();
                GC.Collect();
            }

            try
            {
                if (StiOptions.Web.AllowUseResponseFlush) page.Response.Flush();
                if (StiOptions.Web.AllowUseResponseEnd) page.Response.End();
                else
                {
                    HttpContext.Current.Response.SuppressContent = true;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch
            {
            }
        }

        private void CopyReportDictionary(StiReport reportFrom, StiReport reportTo)
        {
            reportTo.Dictionary.DataStore.Clear();
            reportTo.Dictionary.DataSources.Clear();
            reportTo.Dictionary.Databases.Clear();
            reportTo.Dictionary.Relations.Clear();
            reportTo.Dictionary.Variables.Clear();
            reportTo.Dictionary.Resources.Clear();
            reportTo.Dictionary.Restrictions.Clear();

            if (StiOptions.Designer.NewReport.AllowRegisterDataStoreFromOldReportInNewReport)
                reportTo.Dictionary.DataStore.RegData(reportFrom.Dictionary.DataStore);

            if (StiOptions.Designer.NewReport.AllowRegisterDatabasesFromOldReportInNewReport)
                reportTo.Dictionary.Databases.AddRange(reportFrom.Dictionary.Databases);

            if (StiOptions.Designer.NewReport.AllowRegisterDataSourcesFromOldReportInNewReport)
                reportTo.Dictionary.DataSources.AddRange(reportFrom.Dictionary.DataSources);

            if (StiOptions.Designer.NewReport.AllowRegisterRelationsFromOldReportInNewReport)
                reportTo.Dictionary.Relations.AddRange(reportFrom.Dictionary.Relations);

            if (StiOptions.Designer.NewReport.AllowRegisterVariablesFromOldReportInNewReport)
                reportTo.Dictionary.Variables.AddRange(reportFrom.Dictionary.Variables);

            if (StiOptions.Designer.NewReport.AllowRegisterResourcesFromOldReportInNewReport)
                reportTo.Dictionary.Resources.AddRange(reportFrom.Dictionary.Resources);

            if (StiOptions.Designer.NewReport.AllowRegisterRestrictionsFromOldReportInNewReport)
                reportTo.Dictionary.Restrictions = reportFrom.Dictionary.Restrictions;

            reportTo.Tag = reportFrom.Tag;
            reportTo.Dictionary.Synchronize();
        }

        internal string InsertSessionId(string url, string sessionId)
        {
            string[] segments = url.Split('/');
            string result = string.Empty;
            int index = 0;

            foreach (string segment in segments)
            {
                index++;

                if (index < segments.Length) result = string.Format("{0}{1}/", result, segment);
                else result = string.Format("{0}({1})/{2}", result, sessionId, segment);
            }

            return result;
        }

        internal static bool IsDesignMode
        {
            get
            {
                return (HttpContext.Current == null);
            }
        }

        private object GetRequest(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null) return HttpContext.Current.Request.Params[key];
            if (!IsDesignMode) return this.Page.Request.Params[key];

            return null;
        }

        internal string GetApplicationUrl()
        {
            return GetApplicationUrl(false);
        }

        private Hashtable GetImagesArray(bool returnBase64Data)
        {
            Hashtable images = new Hashtable();

            Assembly a = typeof(StiMobileDesigner).Assembly;
            var names = a.GetManifestResourceNames();

            var path = IMAGES_PATH + ".Office2013.";

            foreach (var name in names)
            {
                if (name.IndexOf(path) == 0 && (name.EndsWith(".png") || name.EndsWith(".gif") || name.EndsWith(".cur")))
                {
                    string imageName = name.Replace(path, "");

                    if (returnBase64Data || imageName.IndexOf("_FirstLoadingImages") >= 0)
                    {
                        imageName = imageName.Replace("_FirstLoadingImages.", "");

                        Stream stream = a.GetManifestResourceStream(name);
                        if (stream == null) return null;

                        MemoryStream ms = new MemoryStream();
                        stream.CopyTo(ms);
                        byte[] buffer = ms.ToArray();
                        ms.Dispose();
                        stream.Dispose();

                        if (name.EndsWith(".cur"))
                        {
                            images[imageName] = string.Format("data:application/cur;base64,{0}", Convert.ToBase64String(buffer));
                        }
                        else
                        {
                            images[imageName] = string.Format("data:image/{0};base64,{1}", imageName.Substring(imageName.Length - 3), Convert.ToBase64String(buffer));
                        }
                    }
                    else
                    {
                        images[imageName] = GetImageUrl(imageName);
                    }
                }
            }

            return images;
        }

        private StiReport GetNewReport()
        {
            StiReport newReport = new StiReport();
            if (!StiOptions.Engine.FullTrust) newReport.CalculationMode = StiCalculationMode.Interpretation;
            newReport.ReportUnit = DefaultUnit;
            newReport.Pages[0].PaperSize = PaperKind.Custom;
            newReport.Pages[0].PageWidth = newReport.Unit.ConvertFromHInches(827d);
            newReport.Pages[0].PageHeight = newReport.Unit.ConvertFromHInches(1169d);
            newReport.Info.Zoom = 1;

            return newReport;
        }

        private string GetCurrentHelpLanguage()
        {
            string language;
            switch (StiLocalization.CultureName)
            {
                case "ru":
                    language = "ru";
                    break;

                //case "de":
                //    language = "de";
                //    break;

                default:
                    language = "en";
                    break;
            }

            return language;
        }

        private string GetProductVersion(bool isShort)
        {
            string[] values = StiVersion.Version.Split('.');
            string versionString = string.Format("{0}.{1}.{2}", values[0], values[1], values[2]);
            if (values[3] != "0") versionString = string.Format("{0}.{1}", versionString, values[3]);
            if (isShort) return versionString;
            versionString = string.Format("{0} from {1}", versionString, StiVersion.CreationDate);

            #region Trial
#if CLOUD
            var isTrial = StiCloudPlan.IsTrialPlan(reportGuid);
#else
            var key = StiLicenseKeyValidator.GetLicenseKey();
            var isTrial = !StiLicenseKeyValidator.IsValid(StiProductIdent.Web, key);
            if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key))isTrial = true;

            #region IsValidLicenseKey
            if (!isTrial)
            {
                try
                {
                    using (var rsa = new RSACryptoServiceProvider(512))
                    using (var sha = new SHA1CryptoServiceProvider())
                    {
                        rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                        isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
                    }
                }
                catch (Exception)
                {
                    isTrial = true;
                }
            }
            #endregion
#endif

            if (!isTrial) versionString += " ";
            #endregion

            return versionString;
        }

        private string GetJSParameters()
        {
            if (!IsDesignMode && !string.IsNullOrEmpty(GlobalizationFile))
            {
                try
                {
                    if (GlobalizationFile.ToLower() == "default" || GlobalizationFile == "en" || GlobalizationFile == "en.xml") StiLocalization.LoadDefaultLocalization();
                    else StiLocalization.Load(Page.Request.PhysicalApplicationPath + GlobalizationFile);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

#if SERVER
            var isTrial = StiVersionX.IsSvr;
#else
            var key = StiLicenseKeyValidator.GetLicenseKey();
            var isTrial = !StiLicenseKeyValidator.IsValidInDesignerOrOnSpecifiedPlatform(StiProductIdent.Web, key);
            if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key))isTrial = true;

            #region IsValidLicenseKey
            if (!isTrial)
            {
                try
                {
                    using (var rsa = new RSACryptoServiceProvider(512))
                    using (var sha = new SHA1CryptoServiceProvider())
                    {
                        rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                        isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
                    }
                }
                catch (Exception)
                {
                    isTrial = true;
                }
            }
            #endregion
#endif

            Hashtable parameters = new Hashtable();
            Hashtable designParams = GetDesignerParametersFromCloud();

            //Is Online Mode
            if (CloudMode && designParams == null && Page.Request.Params["params"] != null)
            {
                designParams = new Hashtable();
                string paramsStr = (Page.Request.Params["params"] as string).Replace(" ", "+");
                paramsStr = Encoding.UTF8.GetString(Convert.FromBase64String(paramsStr));
                string[] paramsItems = paramsStr.Split(';');
                designParams["sessionKey"] = paramsItems[0].Substring(0, 32);

                if (paramsItems[0].Substring(32, 1) == "1")
                    designParams["dVers"] = true;

                if (!string.IsNullOrEmpty(paramsItems[0].Substring(33)))
                    designParams["reportTemplateItemKey"] = paramsItems[0].Substring(33);

                designParams["localizationName"] = paramsItems[1];
                designParams["themeName"] = paramsItems[2];
                designParams["reportName"] = paramsItems[3];
                designParams["viewerUrl"] = paramsItems[4];
                designParams["restUrl"] = paramsItems[5];
                designParams["resourceItems"] = paramsItems[6];
                designParams["attachedItems"] = paramsItems[7];
                if (paramsItems.Length > 8 && paramsItems[8] as string != "null") designParams["versionKey"] = paramsItems[8];
                designParams["isOnlineVersion"] = true;
                designParams["permissionDataSources"] = "All";
                designParams["favIcon"] = "favicon.ico";
                
                designParams["maxResourceSizeDeveloper"] = StiCloudConsts.Developer.MaxResourceSize;
                designParams["maxResourceSizeTrial"] = StiCloudConsts.Trial.MaxResourceSize;
            }

            if (CloudMode && designParams != null)
            {
                Theme = (StiMobileDesignerTheme)Enum.Parse(typeof(StiMobileDesignerTheme), (string)designParams["themeName"]);
                if (designParams["reportName"] != null && !designParams.Contains("isOnlineVersion"))
                    designParams["reportName"] = StiEncodingHelper.DecodeString(designParams["reportName"] as string);
            }

            if (DemoMode && GetRequestParam("themename") != null)
            {
                Theme = (StiMobileDesignerTheme)Enum.Parse(typeof(StiMobileDesignerTheme), (string)GetRequestParam("themename"));
            }

            //Settings
            parameters["mobileDesignerId"] = this.ClientID;
            parameters["viewerId"] = this.viewerId;
            parameters["clipboardId"] = this.ClipboardId;
            parameters["undoArrayId"] = this.UndoArrayId;
            parameters["componentCloneId"] = this.ComponentCloneId;
            parameters["reportCheckersId"] = this.ReportCheckersId;
            parameters["callbackFunction"] = this.Page.ClientScript.GetCallbackEventReference(this, "'callbackParams'", "js" + this.ClientID + ".receveFromServer", "null", "js" + this.ClientID + ".errorFromServer", true);
            parameters["imagesPath"] = this.GetImageUrl("");
            parameters["dVers"] = CloudMode && designParams != null ? designParams.Contains("dVers") : isTrial;
            parameters["theme"] = CloudMode && designParams != null ? designParams["themeName"] : this.Theme.ToString();
            parameters["showAnimation"] = ShowAnimation;
            parameters["defaultUnit"] = DefaultUnit.ToString();
            parameters["focusingX"] = FocusingX;
            parameters["focusingY"] = FocusingY;
            parameters["helpLanguage"] = CloudMode && designParams != null ? designParams["localizationName"] : GetCurrentHelpLanguage();
            parameters["demoMode"] = DemoMode;
            parameters["productVersion"] = GetProductVersion(false);
            parameters["showSaveDialog"] = ShowSaveDialog;
            parameters["fullScreenMode"] = this.Width == Unit.Empty && this.Height == Unit.Empty;
            parameters["haveExitDesignerEvent"] = Exit != null;
            parameters["haveSaveEvent"] = SaveReport != null;
            parameters["haveSaveAsEvent"] = SaveAsReport != null;
            parameters["showTooltips"] = ShowTooltips;
            parameters["showTooltipsHelp"] = ShowTooltipsHelp;
            parameters["showDialogHelp"] = ShowDialogHelp;
            parameters["interfaceType"] = InterfaceType.ToString();
            parameters["requestUrl"] = GetApplicationUrl();
            parameters["cloudMode"] = CloudMode;
            parameters["scrollbiMode"] = ScrollbiMode;
            if (ScrollbiMode) parameters["scrollbiParameters"] = ScrollbiParameters;
            parameters["cloudServerUrl"] = StiHyperlinkProcessor.ServerIdent;
            parameters["resourceIdent"] = StiHyperlinkProcessor.ResourceIdent;
            parameters["variableIdent"] = StiHyperlinkProcessor.VariableIdent;
            parameters["cacheHelper"] = CacheHelper != null;
            parameters["defaultDesignerOptions"] = StiDesignerOptionsHelper.GetDefaultDesignerOptions();
            parameters["showPropertiesWhichUsedFromStyles"] = StiOptions.Designer.PropertyGrid.ShowPropertiesWhichUsedFromStyles;
            parameters["runWizardAfterLoad"] = StiOptions.Designer.RunWizardAfterLoad;
            parameters["datePickerFirstDayOfWeek"] = DatePickerFirstDayOfWeek.ToString();
            parameters["reportResourcesMaximumSize"] = StiOptions.Engine.ReportResources.MaximumSize;
            parameters["allowChangeWindowTitle"] = AllowChangeWindowTitle;
            parameters["showSetupToolboxButton"] = ShowSetupToolboxButton;
            parameters["allowAutoUpdateCache"] = AllowAutoUpdateCache;

            string url = GetRequestUrl("mobiledesigner_script", "MobileDesignerScriptName", PassQueryParametersForResources);
            url += url.IndexOf("?") > 0 ? "&" : "?";
            url += "mobiledesigner_version=" + GetProductVersion(true);
            parameters["scriptsUrl"] = url;

            url = GetRequestUrl("stimobiledesigner_css", "MobileDesignerStyleName", PassQueryParametersForResources);
            url += url.IndexOf("?") > 0 ? "&" : "?";
            url += "mobiledesigner_version=" + GetProductVersion(true);
            parameters["stylesUrl"] = url;

            if (ComponentsIntoInsertTab != null)
            {
                ArrayList componentsArray = new ArrayList();
                componentsArray.AddRange(ComponentsIntoInsertTab);
                parameters["componentsIntoInsertTab"] = componentsArray;
            }

            //Collections
            string[] collections = new string[] { "images", "loc", "paperSizes", "hatchStyles", "summaryTypes", "aggrigateFunctions", "fontNames",
                "conditions", "iconSetArrays", "dBaseCodePages", "csvCodePages", "textFormats", "currencySymbols", "dateFormats", "timeFormats", "customFormats", "cultures"};

            for (int i = 0; i < collections.Length; i++)
            {
                object collectionObject = null;
                switch (collections[i])
                {
                    case "images": { collectionObject = GetImagesArray(false); break; }
                    case "loc":
                        {
                            if (CloudMode && ((Page.Request.Params["localizationName"] != null && Page.Request.Params["sessionKey"] != null) ||
                                (designParams != null && designParams.Contains("isOnlineVersion"))))
                            {

                                if (designParams.Contains("isOnlineVersion"))
                                {
                                    GlobalizationFile = "Localizations/" + designParams["localizationName"] as string + ".xml";
                                    StiLocalization.Load(Page.Request.PhysicalApplicationPath + GlobalizationFile);
                                    collectionObject = StiLocalization.GetLocalization(false);
                                }
#if SERVER
                                else
                                {
                                    var getLocalizationCommand = new StiLocalizationCommands.Get()
                                    {
                                        Name = Page.Request.Params["localizationName"],
                                        SessionKey = Page.Request.Params["sessionKey"],
                                        Set = StiLocalizationSet.Reports,
                                        Type = StiLocalizationFormatType.Json
                                    };

                                    var resultLocalizationCommand = RunCommand(getLocalizationCommand) as StiLocalizationCommands.Get;
                                    if (resultLocalizationCommand.ResultSuccess && resultLocalizationCommand.ResultReportsJson != null)
                                        collectionObject = StiEncodingHelper.DecodeString(resultLocalizationCommand.ResultReportsJson);
                                }
#endif
                            }
                            else
                            {
                                collectionObject = StiLocalization.GetLocalization(false);
                            }
                            break;
                        }
                    case "paperSizes": { collectionObject = StiPaperSizes.GetItems(); break; }
                    case "hatchStyles": { collectionObject = StiHatchStyles.GetItems(); break; }
                    case "summaryTypes": { collectionObject = StiSummaryTypes.GetItems(); break; }
                    case "aggrigateFunctions": { collectionObject = StiAggrigateFunctions.GetItems(); break; }
                    case "fontNames": { collectionObject = StiFontNames.GetItems(); break; }
                    case "conditions": { collectionObject = StiDefaultConditions.GetItems(); break; }
                    case "iconSetArrays": { collectionObject = StiIconSetArrays.GetItems(); break; }
                    case "dBaseCodePages": { collectionObject = StiCodePageHelper.GetDBaseCodePageItems(); break; }
                    case "csvCodePages": { collectionObject = StiCodePageHelper.GetCsvCodePageItems(); break; }
                    case "textFormats": { collectionObject = StiTextFormatHelper.GetTextFormatItems(); break; }
                    case "currencySymbols": { collectionObject = StiTextFormatHelper.GetCurrencySymbols(); break; }
                    case "dateFormats": { collectionObject = StiTextFormatHelper.GetDateAndTimeFormats("date", new StiDateFormatService()); break; }
                    case "timeFormats": { collectionObject = StiTextFormatHelper.GetDateAndTimeFormats("time", new StiTimeFormatService()); break; }
                    case "customFormats": { collectionObject = StiTextFormatHelper.GetDateAndTimeFormats("custom", new StiCustomFormatService()); break; }
                    case "cultures": { collectionObject = StiCultureHelper.GetItems(CultureTypes.SpecificCultures); break; }
                }
                parameters[collections[i]] = collectionObject;
            }

            //ToolBar
            parameters["showInsertButton"] = ShowInsertButton;
            parameters["showPageButton"] = ShowPageButton;
            parameters["showLayoutButton"] = ShowLayoutButton;
            parameters["showPreviewButton"] = ShowPreviewButton;
            parameters["showFileMenu"] = ShowFileMenu;
            parameters["showSaveButton"] = ShowSaveButton;
            parameters["showAboutButton"] = ShowAboutButton;
            parameters["showFileMenuNew"] = ShowFileMenuNew;
            parameters["showFileMenuOpen"] = ShowFileMenuOpen;
            parameters["showFileMenuSave"] = ShowFileMenuSave;
            parameters["showFileMenuSaveAs"] = ShowFileMenuSaveAs;
            parameters["showFileMenuClose"] = ShowFileMenuClose;
            parameters["showFileMenuExit"] = ShowFileMenuExit;
            parameters["showFileMenuReportSetup"] = ShowFileMenuReportSetup;
            parameters["showFileMenuOptions"] = ShowFileMenuOptions;
            parameters["showFileMenuInfo"] = ShowFileMenuInfo;
            parameters["showFileMenuAbout"] = ShowFileMenuAbout;
            parameters["showFileMenuHelp"] = ShowFileMenuHelp;

            //Bands
            Hashtable visibilityBands = new Hashtable();
            parameters["visibilityBands"] = visibilityBands;
            visibilityBands["StiReportTitleBand"] = ShowReportTitleBand;
            visibilityBands["StiReportSummaryBand"] = ShowReportSummaryBand;
            visibilityBands["StiPageHeaderBand"] = ShowPageHeaderBand;
            visibilityBands["StiPageFooterBand"] = ShowPageFooterBand;
            visibilityBands["StiGroupHeaderBand"] = ShowGroupHeaderBand;
            visibilityBands["StiGroupFooterBand"] = ShowGroupFooterBand;
            visibilityBands["StiHeaderBand"] = ShowHeaderBand;
            visibilityBands["StiFooterBand"] = ShowFooterBand;
            visibilityBands["StiColumnHeaderBand"] = ShowColumnHeaderBand;
            visibilityBands["StiColumnFooterBand"] = ShowColumnFooterBand;
            visibilityBands["StiDataBand"] = ShowDataBand;
            visibilityBands["StiHierarchicalBand"] = ShowHierarchicalBand;
            visibilityBands["StiChildBand"] = ShowChildBand;
            visibilityBands["StiEmptyBand"] = ShowEmptyBand;
            visibilityBands["StiOverlayBand"] = ShowOverlayBand;
            visibilityBands["StiTable"] = ShowTable;

            //Bands
            Hashtable visibilityCrossBands = new Hashtable();
            parameters["visibilityCrossBands"] = visibilityCrossBands;
            visibilityCrossBands["StiCrossTab"] = ShowCrossTab;
            visibilityCrossBands["StiCrossGroupHeaderBand"] = ShowCrossGroupHeaderBand;
            visibilityCrossBands["StiCrossGroupFooterBand"] = ShowCrossGroupFooterBand;
            visibilityCrossBands["StiCrossHeaderBand"] = ShowCrossHeaderBand;
            visibilityCrossBands["StiCrossFooterBand"] = ShowCrossFooterBand;
            visibilityCrossBands["StiCrossDataBand"] = ShowCrossDataBand;

            //Components
            Hashtable visibilityComponents = new Hashtable();
            parameters["visibilityComponents"] = visibilityComponents;
            visibilityComponents["StiText"] = ShowText;
            visibilityComponents["StiTextInCells"] = ShowTextInCells;
            visibilityComponents["StiRichText"] = ShowRichText;
            visibilityComponents["StiImage"] = ShowImage;
            visibilityComponents["StiBarCode"] = ShowBarCode;
            visibilityComponents["StiShape"] = ShowShape;
            visibilityComponents["StiPanel"] = ShowPanel;
            visibilityComponents["StiClone"] = ShowClone;
            visibilityComponents["StiCheckBox"] = ShowCheckBox;
            visibilityComponents["StiSubReport"] = ShowSubReport;
            visibilityComponents["StiZipCode"] = ShowZipCode;
            visibilityComponents["StiChart"] = ShowChart;
            visibilityComponents["StiMap"] = ShowMap;
            visibilityComponents["StiGauge"] = ShowGauge;
            visibilityComponents["StiHorizontalLinePrimitive"] = ShowHorizontalLinePrimitive;
            visibilityComponents["StiVerticalLinePrimitive"] = ShowVerticalLinePrimitive;
            visibilityComponents["StiRectanglePrimitive"] = ShowRectanglePrimitive;
            visibilityComponents["StiRoundedRectanglePrimitive"] = ShowRoundedRectanglePrimitive;

            //Properties Grid
            parameters["showPropertiesGrid"] = ShowPropertiesGrid;
            parameters["propertiesGridWidth"] = PropertiesGridWidth;
            parameters["propertiesGridLabelWidth"] = PropertiesGridLabelWidth;

            //Dictionary
            parameters["showDictionary"] = ShowDictionary;
            parameters["permissionDataSources"] = CloudMode && designParams != null ? (string)designParams["permissionDataSources"] : PermissionDataSources.ToString();
            parameters["permissionDataColumns"] = CloudMode && designParams != null ? (string)designParams["permissionDataSources"] : PermissionDataColumns.ToString();
            parameters["permissionDataRelations"] = CloudMode && designParams != null ? (string)designParams["permissionDataSources"] : PermissionDataRelations.ToString();
            parameters["permissionDataConnections"] = CloudMode && designParams != null ? (string)designParams["permissionDataSources"] : PermissionDataConnections.ToString();
            parameters["permissionBusinessObjects"] = CloudMode && designParams != null ? "None" : PermissionBusinessObjects.ToString();
            parameters["permissionVariables"] = PermissionVariables.ToString();
            parameters["permissionResources"] = PermissionResources.ToString();
            parameters["permissionSqlParameters"] = PermissionSqlParameters.ToString();

            //Report Tree
            parameters["showReportTree"] = ShowReportTree;

            //Cursors
            parameters["urlCursorStyleSet"] = GetImageUrl("Cursors.StyleSet.cur");
            parameters["urlCursorPen"] = GetImageUrl("Cursors.Pen.cur");

            //Report            
            if (designParams == null)
            {
                #region Get Report Standart Mode
                StiReport currentReport = this.Report;
                Hashtable designerOptions = StiDesignerOptionsHelper.GetDesignerOptions(Page);

                if (currentReport != null)
                {
                    if (String.IsNullOrEmpty(currentReport.ReportFile))
                        currentReport.ReportFile = !String.IsNullOrEmpty(currentReport.ReportName) ? currentReport.ReportName + ".mrt" : "Report.mrt";
                    InvokeGetReport(currentReport);
                    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(designerOptions, currentReport);
                    parameters["reportGuid"] = this.ReportGuid;
                    currentReport.Info.Zoom = 1;
                    this.Report = currentReport;
                }
                else
                {
                    StiReport newReport = GetNewReport();
                    InvokeCreateReport(newReport);
                    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(designerOptions, newReport);
                    parameters["reportGuid"] = this.ReportGuid;
                    newReport.Info.Zoom = 1;
                    this.Report = newReport;
                }
                #endregion
            }
            else if (CloudMode)
            {
                #region Get Report Cloud Server Mode
#if SERVER
                if (designParams.Contains("isOnlineVersion") && designParams.Contains("reportTemplateItemKey"))
                {
                    StiReport newReport = new StiReport();

                    var connection = new StiServerConnection(cloudServerAdress);
                    connection.Accounts.Users.Login(designParams["sessionKey"] as string, false);

                    var commandResourceGet = new StiItemResourceCommands.Get
                    {
                        ItemKey = designParams["reportTemplateItemKey"] as string
                    };

                    if (designParams.Contains("versionKey") && !String.IsNullOrEmpty(designParams["versionKey"] as string))
                    {
                        commandResourceGet.VersionKey = designParams["versionKey"] as string;
                    }

                    commandResourceGet = connection.RunCommand(commandResourceGet) as StiItemResourceCommands.Get;

                    if (commandResourceGet.ResultResource != null && commandResourceGet.ResultResource.Length > 0)
                    {
                        try
                        {
                            newReport.Load(commandResourceGet.ResultResource);
                            newReport.Info.Zoom = 1;
                            StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), newReport);
                        }
                        catch (Exception e)
                        {
                            parameters["error"] = e.Message;
                        };
                    }

                    this.Report = newReport;
                    parameters["cloudParameters"] = designParams;
                    parameters["reportGuid"] = this.ReportGuid;
                }
                else
                {
                    if (designParams.Contains("reportTemplateItemKey"))
                    {
                        var getReportForDesignCommand = new StiReportCommands.Design()
                        {
                            ReportTemplateItemKey = (string)designParams["reportTemplateItemKey"],
                            SessionKey = (string)designParams["sessionKey"]
                        };

                        if (designParams.ContainsKey("versionKey"))
                        {
                            getReportForDesignCommand.VersionKey = (string)designParams["versionKey"];
                        }

                        var resultReportForDesignCommand = RunCommand(getReportForDesignCommand) as StiReportCommands.Design;

                        if (resultReportForDesignCommand.ResultSuccess && resultReportForDesignCommand.ResultReport != null)
                        {
                            StiReport newReport = new StiReport();
                            try
                            {
                                newReport.Load(new MemoryStream(resultReportForDesignCommand.ResultReport));
                                newReport.Info.Zoom = 1;
                                StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), newReport);
                            }
                            catch (Exception e) { parameters["error"] = e.Message; };

                            this.Report = newReport;
                            parameters["cloudParameters"] = designParams;
                            parameters["reportGuid"] = this.ReportGuid;
                        }
                    }
                    else
                    {
                        StiReport newReport = new StiReport();
                        if (designParams.Contains("isOnlineVersion")) newReport.CalculationMode = StiCalculationMode.Interpretation;
                        newReport.Info.Zoom = 1;
                        StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), newReport);
                        this.Report = newReport;
                        parameters["cloudParameters"] = designParams;
                        parameters["reportGuid"] = this.ReportGuid;
                    }
                }
#endif
                #endregion
            }

            //JS Helper
            //GetResourcesForJsDesigner(parameters);
            //------

            return JSON.Encode(parameters);
        }

        private void SaveReportForPreviewToCache(StiReport report, string guid)
        {
            if (CacheHelper != null)
            {
                CacheHelper.SaveObjectToCache(report, guid);
            }
            else
            {
                if (CacheMode == StiCacheMode.Session)
                {
                    Page.Session.Remove(guid);
                    Page.Session.Add(guid, report);
                }
                else
                {
                    Page.Cache.Remove(guid);
                    Page.Cache.Add(guid, report, null, Cache.NoAbsoluteExpiration, serverTimeout, CacheItemPriority.Low, null);
                }
            }
        }

        private void ViewerGetReportData(object sender, StiReportDataEventArgs e)
        {
            InvokePreviewReport(e.Report);
        }

        private string DecodeSymbols(string str)
        {
            str = str.Replace("&ldquo;", "\\\""); //Fixed bug with symbols (\");

            return str;
        }

        private StiMobileViewer GetViewer()
        {
            StiMobileViewer viewer = new StiMobileViewer();
            viewer.FullScreen = true;
            viewer.DesignerMode = true;
            viewer.ShowDesignButton = false;
            viewer.ShowAboutButton = false;
            viewer.DesignerMode = true;
            viewer.CacheMode = this.CacheMode == StiCacheMode.Page ? Stimulsoft.Report.Mobile.StiCacheMode.Page : Stimulsoft.Report.Mobile.StiCacheMode.Session;
            viewer.Theme = (Stimulsoft.Report.Mobile.StiMobileViewerTheme)Enum.Parse(typeof(Stimulsoft.Report.Mobile.StiMobileViewerTheme), this.Theme.ToString());
            viewer.InterfaceType = (Stimulsoft.Report.Mobile.StiInterfaceType)Enum.Parse(typeof(Stimulsoft.Report.Mobile.StiInterfaceType), this.InterfaceType.ToString());
            viewer.ID = "Viewer";
            viewer.Style.Add("display", "none");
            viewer.GetReportData += new StiReportDataEventHandler(ViewerGetReportData);
            viewer.ReportDisplayMode = (Stimulsoft.Report.Mobile.StiReportDisplayMode)Enum.Parse(typeof(Stimulsoft.Report.Mobile.StiReportDisplayMode), this.ReportDisplayMode.ToString());
            viewer.CustomCss = CustomCss;
            viewer.PageBorderColor = Color.FromArgb(198, 198, 198);
            viewer.BackColor = Color.FromArgb(241, 241, 241);
            viewer.ShowPageShadow = false;
            viewer.CountColumnsParameters = 1;
            if (this.CacheHelper != null) viewer.CacheHelper = this.CacheHelperForViewer;

            return viewer;
        }
        #endregion

        #region ICallbackEventHandler
        public void RaiseCallbackEvent(string eventArg)
        {
            callbackResult = new Hashtable();

            try
            {
                Hashtable param = (Hashtable)JSON.Decode(DecodeSymbols(eventArg));
                string command = (string)param["command"];
                callbackResult["command"] = command;
                if (param["callbackFunctionId"] != null) callbackResult["callbackFunctionId"] = param["callbackFunctionId"];
                if (param["reportGuid"] != null) this.ReportGuid = (string)param["reportGuid"];
                this.ClipboardId = (string)param["clipboardId"];
                this.UndoArrayId = (string)param["undoArrayId"];
                this.ComponentCloneId = (string)param["componentCloneId"];
                this.ReportCheckersId = (string)param["reportCheckersId"];
                StiReport currentReport = this.Report;
                if (currentReport != null) currentReport.Info.Zoom = 1;

                #region Session Completed
                if (currentReport == null && command != "CloseReport" && command != "CreateReport" && command != "GetReportFromData" &&
                    command != "OpenReport" && command != "WizardResult" && command != "UpdateCache" && command != "GetReportForDesigner" &&
                    command != "UpdateImagesArray" && command != "Synchronization")
                {
                    param["command"] = "SessionCompleted";
                    callbackResult["command"] = "SessionCompleted";
                }
                #endregion

                #region Synchronization
                if (command == "Synchronization")
                {
                    if (currentReport == null)
                        callbackResult["command"] = "SynchronizationError";
                    else
                        callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport);
                }
                #endregion

                #region Update Cache
                else if (command == "UpdateCache")
                {
                    callbackResult["command"] = "UpdateCache";
                }
                #endregion

                #region Get Report For Designer
                else if (command == "GetReportForDesigner")
                {

                    Hashtable attachedItems = null;
                    currentReport.Info.Zoom = 1;
#if SERVER
                    if (param["resourceItems"] != null)
                    {
                        AddResourcesToReport(currentReport, (ArrayList)JSON.Decode((string)param["resourceItems"]), (string)param["sessionKey"], param.Contains("isOnlineVersion"));
                    }

                    if (param["attachedItems"] != null && !param.Contains("isOnlineVersion"))
                    {
                        attachedItems = GetReportAttachedItems((ArrayList)JSON.Decode((string)param["attachedItems"]), (string)param["sessionKey"]);

                        if (/*param["reportTemplateItemKey"] == null &&*/ attachedItems["Data"] != null)
                        {
                            CreateDataSourcesFromAttachedItem(currentReport, attachedItems["Data"] as ArrayList, (string)param["sessionKey"]);
                        }
                    }
#endif
                    callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport, attachedItems);
                }
                #endregion

                #region Create Report
                else if (command == "CreateReport")
                {
                    StiReport newReport = GetNewReport();
                    if (currentReport != null) CopyReportDictionary(currentReport, newReport);
                    InvokeCreateReport(newReport);
                    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), newReport);
                    callbackResult["reportGuid"] = this.ReportGuid;
                    callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(newReport);
                    callbackResult["needClearAfterOldReport"] = param["needClearAfterOldReport"];
                    currentReport = newReport;
                    this.UndoArray = new ArrayList();
                }
                #endregion

                #region Open Report
                else if (command == "OpenReport")
                {
                    try
                    {
                        string fileContent = (string)param["content"];

                        string content = fileContent.Substring(fileContent.IndexOf("base64,") + "base64,".Length);
                        byte[] contentBytes = System.Convert.FromBase64String(content);

                        StiReport report = new StiReport();
                        MemoryStream stream = new MemoryStream();
                        stream.Write(contentBytes, 0, contentBytes.Length);
                        StiReportSLService service = null;
                        stream.Position = 0;
                        string fileName = (string)param["fileName"];

                        if (param["password"] != null)
                        {
                            report.LoadEncryptedReport(stream, (string)param["password"]);
                            callbackResult["encryptedPassword"] = (string)param["password"];
                        }
                        else if (IsPackedFile(stream))
                        {
                            report.LoadPackedReport(stream);
                        }
                        else
                        {
                            if (IsJsonFile(stream))
                            {
                                service = new StiJsonReportSLService();
                            }
                            else
                            {
                                service = new StiXmlReportSLService();
                            }
                            report.Load(service, stream);
                        }
                        if (stream != null) stream.Close();

                        report.Info.Zoom = 1;
                        if (param["fileName"] != null) report.ReportFile = (string)param["fileName"];
                        if (!StiOptions.Engine.FullTrust) report.CalculationMode = StiCalculationMode.Interpretation;
                        InvokeGetReport(report);
                        report.Info.ForceDesigningMode = true;
                        currentReport = report;
                        StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), currentReport);

                        string reportObjectStr = StiReportEdit.WriteReportInObject(currentReport);
                        if (reportObjectStr != null)
                        {
                            callbackResult["reportObject"] = reportObjectStr;
                            callbackResult["reportGuid"] = this.ReportGuid;
                        }
                        else
                        {
                            callbackResult["errorMessage"] = "Loading report error: Json parser error!";
                        }
                    }
                    catch (Exception e)
                    {
                        callbackResult["reportGuid"] = null;
                        callbackResult["reportObject"] = null;
                        callbackResult["error"] = "Loading report ERROR! " + e.Message;
                    }
                    this.UndoArray = new ArrayList();
                }
                #endregion

                #region Close Report
                else if (command == "CloseReport")
                {
                    currentReport = null;
                }
                #endregion

                #region Save Report
                else if (command == "SaveReport")
                {
                    if (param["reportFile"] != null) currentReport.ReportFile = (string)param["reportFile"];
                    if (ScrollbiMode && param["scrollbiParameters"] != null)
                    {
                        InvokeSaveReport(currentReport, param["scrollbiParameters"] as Hashtable);
                    }
                    else
                    {
                        InvokeSaveReport(currentReport, param["encryptedPassword"] as string);
                    }

                    if (ScrollbiParameters != null)
                    {
                        if (ScrollbiParameters["errorMessage"] != null) callbackResult["errorMessage"] = ScrollbiParameters["errorMessage"];
                        if (ScrollbiParameters["infoMessage"] != null) callbackResult["infoMessage"] = ScrollbiParameters["infoMessage"];
                        if (ScrollbiParameters["urlInvoiceTamplates"] != null) callbackResult["urlInvoiceTamplates"] = ScrollbiParameters["urlInvoiceTamplates"];
                    }
                }
                #endregion

                #region Save As Report
                else if (command == "SaveAsReport")
                {
                    if (param["reportFile"] != null) currentReport.ReportFile = (string)param["reportFile"];
                    InvokeSaveAsReport(currentReport, currentReport.ReportFile, param["encryptedPassword"] as string);
                }
                #endregion

                #region Change Rect Component
                else if (command == "MoveComponent" || command == "ResizeComponent")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.ChangeRectComponent(currentReport, param, callbackResult);
                }
                #endregion

                #region Create Component
                else if (command == "CreateComponent")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.CreateComponent(currentReport, param, callbackResult);
                }
                #endregion

                #region Remove Component
                else if (command == "RemoveComponent")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.RemoveComponent(currentReport, param, callbackResult);
                }
                #endregion

                #region Add Page
                else if (command == "AddPage")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.AddPage(currentReport, param, callbackResult);
                }
                #endregion

                #region Remove Page
                else if (command == "RemovePage")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.RemovePage(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Properties
                else if (command == "SendProperties")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.ReadAllPropertiesFromString(currentReport, param, callbackResult);
                }
                #endregion

                #region ChangeUnit
                else if (command == "ChangeUnit")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.ChangeUnit(currentReport, (string)param["reportUnit"]);
                    callbackResult["reportUnit"] = param["reportUnit"];
                    callbackResult["gridSize"] = currentReport.Info.GridSize.ToString();
                    StiReportEdit.GetAllComponentsPositions(currentReport, callbackResult);
                }
                #endregion

                #region Rebuild Page
                else if (command == "RebuildPage")
                {
                    callbackResult["pageName"] = (string)param["pageName"];
                    callbackResult["rebuildProps"] = StiReportEdit.GetPropsRebuildPage(currentReport, currentReport.Pages[(string)param["pageName"]]);
                }
                #endregion

                #region LoadReportToViewer
                else if (command == "LoadReportToViewer")
                {
                    StiReport cloneReport = StiReportEdit.CloneReport(currentReport, true);
                    InvokePreviewReport(cloneReport);
                    try
                    {
                        List<StiCheck> checks = StiReportCheckHelper.CheckReport(this, currentReport, param);
                        callbackResult["checkItems"] = StiReportCheckHelper.GetChecksJSCollection(checks);
                    }
                    catch (Exception e)
                    {
                        Console.Write(e.Message);
                    }
                    SaveReportForPreviewToCache(cloneReport, (string)param["previewReportGuid"]);
                }
                #endregion

                #region SetToClipboard
                else if (command == "SetToClipboard")
                {
                    StiReportEdit.SetToClipboard(this, currentReport, param, callbackResult);
                }
                #endregion

                #region GetFromClipboard
                else if (command == "GetFromClipboard")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.GetFromClipboard(this, currentReport, param, callbackResult);
                }
                #endregion

                #region Undo
                else if (command == "Undo")
                {
                    currentReport = StiReportEdit.GetUndoStep(this, currentReport, param, callbackResult);
                }
                #endregion

                #region Redo
                else if (command == "Redo")
                {
                    currentReport = StiReportEdit.GetRedoStep(this, currentReport, param, callbackResult);
                }
                #endregion

                #region RenameComponent
                else if (command == "RenameComponent")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.RenameComponent(currentReport, param, callbackResult);
                }
                #endregion

                #region WizardResult
                else if (command == "WizardResult")
                {
                    StiReport newReport = GetNewReport();
                    if (currentReport != null) CopyReportDictionary(currentReport, newReport);
                    InvokeCreateReport(newReport);
                    Hashtable wizardResult = (Hashtable)param["wizardResult"];
                    currentReport = StiWizardHelper.GetReportFromWizardOptions(newReport, (Hashtable)wizardResult["reportOptions"], (Hashtable)wizardResult["dataSources"]);
                    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), currentReport);
                    callbackResult["reportGuid"] = this.ReportGuid;
                    Hashtable attachedItems = null;
#if SERVER
                    if (param["attachedItems"] != null && param["sessionKey"] != null)
                    {
                        attachedItems = GetReportAttachedItems((ArrayList)param["attachedItems"], (string)param["sessionKey"]);
                    }
#endif
                    callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport, attachedItems);
                    this.UndoArray = new ArrayList();
                }
                #endregion

                #region GetConnectionTypes
                else if (command == "GetConnectionTypes")
                {
                    StiDictionaryHelper.GetConnectionTypes(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditConnection
                else if (command == "CreateOrEditConnection")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.CreateOrEditConnection(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteConnection
                else if (command == "DeleteConnection")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.DeleteConnection(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditRelation
                else if (command == "CreateOrEditRelation")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.CreateOrEditRelation(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteRelation
                else if (command == "DeleteRelation")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.DeleteRelation(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditColumn
                else if (command == "CreateOrEditColumn")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.CreateOrEditColumn(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteColumn
                else if (command == "DeleteColumn")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.DeleteColumn(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditParameter
                else if (command == "CreateOrEditParameter")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.CreateOrEditParameter(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteParameter
                else if (command == "DeleteParameter")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.DeleteParameter(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditDataSource
                else if (command == "CreateOrEditDataSource")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.CreateOrEditDataSource(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteDataSource
                else if (command == "DeleteDataSource")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.DeleteDataSource(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditBusinessObject
                else if (command == "CreateOrEditBusinessObject")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.CreateOrEditBusinessObject(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteBusinessObject
                else if (command == "DeleteBusinessObject")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.DeleteBusinessObject(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteBusinessObjectCategory
                else if (command == "DeleteBusinessObjectCategory")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.DeleteBusinessObjectCategory(currentReport, param, callbackResult);
                }
                #endregion

                #region EditBusinessObjectCategory
                else if (command == "EditBusinessObjectCategory")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.EditBusinessObjectCategory(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditVariable
                else if (command == "CreateOrEditVariable")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.CreateOrEditVariable(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteVariable
                else if (command == "DeleteVariable")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.DeleteVariable(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteVariablesCategory
                else if (command == "DeleteVariablesCategory")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.DeleteVariablesCategory(currentReport, param, callbackResult);
                }
                #endregion

                #region EditVariablesCategory
                else if (command == "EditVariablesCategory")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.EditVariablesCategory(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateVariablesCategory
                else if (command == "CreateVariablesCategory")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.CreateVariablesCategory(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditResource
                else if (command == "CreateOrEditResource")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command, true);
                    StiDictionaryHelper.CreateOrEditResource(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteResource
                else if (command == "DeleteResource")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command, true);
                    StiDictionaryHelper.DeleteResource(currentReport, param, callbackResult);
                }
                #endregion

                #region SynchronizeDictionary
                else if (command == "SynchronizeDictionary")
                {
                    StiDictionaryHelper.SynchronizeDictionary(currentReport, param, callbackResult);
                }
                #endregion

                #region NewDictionary
                else if (command == "NewDictionary")
                {
                    StiDictionaryHelper.NewDictionary(currentReport, param, callbackResult);
                }
                #endregion

                #region GetAllConnections
                else if (command == "GetAllConnections")
                {
                    StiDictionaryHelper.GetAllConnections(currentReport, param, callbackResult);
                }
                #endregion

                #region RetrieveColumns
                else if (command == "RetrieveColumns")
                {
                    StiDictionaryHelper.RetrieveColumns(currentReport, param, callbackResult);
                }
                #endregion

                #region UpdateStyles
                else if (command == "UpdateStyles")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiStylesHelper.UpdateStyles(currentReport, param, callbackResult);
                }
                #endregion

                #region AddStyle
                else if (command == "AddStyle")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiStylesHelper.AddStyle(currentReport, param, callbackResult);
                }
                #endregion

                #region RemoveStyle
                else if (command == "RemoveStyle")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiStylesHelper.RemoveStyle(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateStyleCollection
                else if (command == "CreateStyleCollection")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiStylesHelper.CreateStyleCollection(currentReport, param, callbackResult);
                }
                #endregion

                #region CloneChartComponent
                else if (command == "StartEditChartComponent")
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null)
                    {
                        StiReportEdit.SaveComponentClone(this, component);
                        callbackResult["properties"] = StiChartHelper.GetChartProperties(component as StiChart);
                    }
                }
                #endregion

                #region CanceledEditComponent
                else if (command == "CanceledEditComponent")
                {
                    StiReportEdit.CanceledEditComponent(this, currentReport, param);
                }
                #endregion

                #region Add Series
                else if (command == "AddSeries")
                {
                    StiChartHelper.AddSeries(currentReport, param, callbackResult);
                }
                #endregion

                #region Remove Series
                else if (command == "RemoveSeries")
                {
                    StiChartHelper.RemoveSeries(currentReport, param, callbackResult);
                }
                #endregion

                #region Series Move
                else if (command == "SeriesMove")
                {
                    StiChartHelper.SeriesMove(currentReport, param, callbackResult);
                }
                #endregion

                #region Add ConstantLineOrStrip
                else if (command == "AddConstantLineOrStrip")
                {
                    StiChartHelper.AddConstantLineOrStrip(currentReport, param, callbackResult);
                }
                #endregion

                #region Remove ConstantLineOrStrip
                else if (command == "RemoveConstantLineOrStrip")
                {
                    StiChartHelper.RemoveConstantLineOrStrip(currentReport, param, callbackResult);
                }
                #endregion

                #region ConstantLineOrStrip Move
                else if (command == "ConstantLineOrStripMove")
                {
                    StiChartHelper.ConstantLineOrStripMove(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Labels Content
                else if (command == "GetLabelsContent")
                {
                    StiChartHelper.GetLabelsContent(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Styles Content
                else if (command == "GetStylesContent")
                {
                    StiChartHelper.GetStylesContent(currentReport, param, callbackResult, false);
                }
                #endregion

                #region Set Labels Type
                else if (command == "SetLabelsType")
                {
                    StiChartHelper.SetLabelsType(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Chart Style
                else if (command == "SetChartStyle")
                {
                    StiChartHelper.SetChartStyle(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Chart Property Value
                else if (command == "SetChartPropertyValue")
                {
                    StiChartHelper.SetChartPropertyValue(currentReport, param, callbackResult);
                }
                #endregion

                #region Send Container Value
                else if (command == "SendContainerValue")
                {
                    StiChartHelper.SetContainerValue(currentReport, param, callbackResult);
                }
                #endregion

                #region GetReportFromData
                else if (command == "GetReportFromData")
                {
#if SERVER
                    var getReportForDesignCommand = new StiReportCommands.Design()
                    {
                        ReportTemplateItemKey = (string)param["reportTemplateItemKey"],
                        SessionKey = (string)param["sessionKey"]
                    };

                    if (param.ContainsKey("VersionKey"))
                    {
                        getReportForDesignCommand.VersionKey = (string)param["VersionKey"];
                    }

                    //Get attached items
                    Hashtable attachedItems = GetReportAttachedItems((ArrayList)param["attachedItems"], (string)param["sessionKey"]);
                    var resultReportForDesignCommand = RunCommand(getReportForDesignCommand) as StiReportCommands.Design;

                    if (resultReportForDesignCommand.ResultSuccess && resultReportForDesignCommand.ResultReport != null)
                    {
                        StiReport newReport = new StiReport();
                        try
                        {
                            newReport.Load(new MemoryStream(resultReportForDesignCommand.ResultReport));
                        }
                        catch { };
                        newReport.Info.Zoom = 1;
                        StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), newReport);
                        currentReport = newReport;
                        callbackResult["reportGuid"] = this.ReportGuid;
                        callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport, attachedItems);
                    }
                    else
                    {
                        if (resultReportForDesignCommand.ResultNotice != null)
                            callbackResult["errorMessage"] = resultReportForDesignCommand.ResultNotice.CustomMessage;
                    }
#endif
                }
                #endregion

                #region ItemResourceSave
                else if (command == "ItemResourceSave")
                {
                    if (currentReport != null)
                    {
#if SERVER
                        StiServerConnection connection = null;

                        if (param.Contains("isOnlineVersion"))
                        {
                            connection = new StiServerConnection(cloudServerAdress);
                            connection.Accounts.Users.Login(param["sessionKey"] as string, false);
                        }

                        var itemResourceSaveCommand = new StiItemResourceCommands.Save()
                        {
                            ItemKey = (string)param["reportTemplateItemKey"],
                            SessionKey = (string)param["sessionKey"],
                            Resource = currentReport.SaveToByteArray()
                        };

                        string customMessage = param["customMessage"] as string;
                        if (!string.IsNullOrEmpty(customMessage))
                        {
                            itemResourceSaveCommand.VersionInfo = StiNotice.Create(StiEncodingHelper.DecodeString(customMessage));
                        }

                        var resultResourceSaveCommand = new StiItemResourceCommands.Save();

                        if (param.Contains("isOnlineVersion"))
                        {
                            resultResourceSaveCommand = connection.RunCommand(itemResourceSaveCommand) as StiItemResourceCommands.Save;
                        }
                        else
                        {
                            resultResourceSaveCommand = RunCommand(itemResourceSaveCommand) as StiItemResourceCommands.Save;
                        }

                        if (!resultResourceSaveCommand.ResultSuccess && resultResourceSaveCommand.ResultNotice != null)
                        {
                            callbackResult["errorMessage"] = resultResourceSaveCommand.ResultNotice.CustomMessage;
                        }
#endif
                    }
                }
                #endregion

                #region CloneItemResourceSave
                else if (command == "CloneItemResourceSave")
                {
                    if (currentReport != null)
                    {
#if SERVER
                        var reportTemplateKey = StiKeyHelper.GenerateKey();

                        var tempItem = new StiReportTemplateItem();
                        tempItem.GenerateNewStateKey();
                        tempItem.Visible = false;
                        tempItem.Expires = DateTime.UtcNow + StiClientConfiguration.Reports.ReportViewInDesignerLifeTime;
                        tempItem.Key = reportTemplateKey;
                        tempItem.Name = !string.IsNullOrEmpty(param["reportName"] as string) ? param["reportName"] as string : "Report";
                        if (param["attachedItems"] != null) tempItem.AttachedItems = ((ArrayList)param["attachedItems"]).Cast<string>().ToList();

                        var itemSaveCommand = new StiItemCommands.Save()
                        {
                            Items = tempItem.ToItems(),
                            SessionKey = (string)param["sessionKey"]
                        };

                        StiServerConnection connection = null;
                        var resultItemSaveCommand = new StiItemCommands.Save();

                        if (param.Contains("isOnlineVersion"))
                        {
                            connection = new StiServerConnection(cloudServerAdress);
                            connection.Accounts.Users.Login(param["sessionKey"] as string, false);
                            resultItemSaveCommand = connection.RunCommand(itemSaveCommand) as StiItemCommands.Save;
                        }
                        else
                        {
                            resultItemSaveCommand = RunCommand(itemSaveCommand) as StiItemCommands.Save;
                        }

                        if (resultItemSaveCommand.ResultSuccess)
                        {
                            var resultResourceSaveCommand = new StiItemResourceCommands.Save()
                            {
                                ItemKey = reportTemplateKey,
                                SessionKey = (string)param["sessionKey"],
                                Resource = currentReport.SaveToByteArray()
                            };

                            if (param.Contains("isOnlineVersion"))
                            {
                                resultResourceSaveCommand = connection.RunCommand(resultResourceSaveCommand) as StiItemResourceCommands.Save;
                            }
                            else
                            {
                                resultResourceSaveCommand = RunCommand(resultResourceSaveCommand) as StiItemResourceCommands.Save;
                            }

                            if (resultResourceSaveCommand.ResultSuccess)
                            {
                                callbackResult["resultItemKey"] = reportTemplateKey;
                                callbackResult["reportName"] = param["reportName"];
                            }
                            else if (resultResourceSaveCommand.ResultNotice != null)
                            {
                                callbackResult["errorMessage"] = resultResourceSaveCommand.ResultNotice.CustomMessage;
                            }
                        }
                        else if (resultItemSaveCommand.ResultNotice != null)
                        {
                            callbackResult["errorMessage"] =
                                resultItemSaveCommand.ResultNotice.CustomMessage != null ? resultItemSaveCommand.ResultNotice.CustomMessage : resultItemSaveCommand.ResultNotice.Ident.ToString();
                        }
#endif
                    }
                }
                #endregion

                #region GetDatabaseData
                else if (command == "GetDatabaseData")
                {
                    StiDictionaryHelper.GetDatabaseData(currentReport, param, callbackResult);
                }
                #endregion

                #region ApplySelectedData
                else if (command == "ApplySelectedData")
                {
                    StiDictionaryHelper.ApplySelectedData(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateTextComponent
                else if (command == "CreateTextComponent")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.CreateTextComponentFromDictionary(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateDataComponent
                else if (command == "CreateDataComponent")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.CreateDataComponentFromDictionary(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Report Properties
                else if (command == "SetReportProperties")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.SetReportProperties(currentReport, param, callbackResult);
                }
                #endregion

                #region Page Move
                else if (command == "PageMove")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.PageMove(currentReport, param, callbackResult);
                }
                #endregion

                #region Test Connection
                else if (command == "TestConnection")
                {
                    StiDictionaryHelper.TestConnection(currentReport, param, callbackResult);
                }
                #endregion

                #region Run Query Script
                else if (command == "RunQueryScript")
                {
                    StiDictionaryHelper.RunQueryScript(currentReport, param, callbackResult);
                }
                #endregion

                #region View Data
                else if (command == "ViewData")
                {
                    StiDictionaryHelper.ViewData(currentReport, param, callbackResult);
                }
                #endregion

                #region Apply Designer Options
                else if (command == "ApplyDesignerOptions")
                {
                    currentReport.Info.Zoom = StiReportEdit.StrToDouble((string)param["zoom"]);
                    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport((Hashtable)param["designerOptions"], currentReport);
                    if (param["reportFile"] != null) currentReport.ReportFile = (string)param["reportFile"];
                    callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport);
                }
                #endregion

                #region GetSqlParameterTypes
                else if (command == "GetSqlParameterTypes")
                {
                    StiDictionaryHelper.GetSqlParameterTypes(currentReport, param, callbackResult);
                }
                #endregion

                #region Align To Grid
                else if (command == "AlignToGridComponents")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.AlignToGridComponents(currentReport, param, callbackResult);
                }
                #endregion

                #region Change Arrange
                else if (command == "ChangeArrangeComponents")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.ChangeArrangeComponents(currentReport, param, callbackResult);
                }
                #endregion

                #region Update Sample Text Format
                else if (command == "UpdateSampleTextFormat")
                {
                    StiFormatService service = StiTextFormatHelper.GetFormatService((Hashtable)param["textFormat"]);
                    if (!service.IsFormatStringFromVariable) callbackResult["sampleText"] = service.Format(service.Sample);
                    service = null;
                }
                #endregion

                #region CloneCrossTabComponent
                else if (command == "StartEditCrossTabComponent")
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null)
                    {
                        StiReportEdit.SaveComponentClone(this, component);
                    }
                }
                #endregion

                #region UpdateCrossTabComponent
                else if (command == "UpdateCrossTabComponent")
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is StiCrossTab)
                    {
                        StiCrossTabHelper crossTabHelper = new StiCrossTabHelper(component as StiCrossTab);
                        crossTabHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        crossTabHelper.RestorePositions();
                        crossTabHelper = null;
                    }
                }
                #endregion

                #region GetCrossTabColorStyles
                else if (command == "GetCrossTabColorStyles")
                {
                    callbackResult["colorStyles"] = StiCrossTabHelper.GetColorStyles();
                }
                #endregion

                #region DuplicatePage
                else if (command == "DuplicatePage")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.DuplicatePage(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Event Value
                else if (command == "SetEventValue")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.SetEventValue(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Chart Styles Content
                else if (command == "GetChartStylesContent")
                {
                    StiChartHelper.GetStylesContent(currentReport, param, callbackResult, true);
                }
                #endregion

                #region Get Gauge Styles Content
                else if (command == "GetGaugeStylesContent")
                {
                    StiGaugeHelper.GetStylesContent(currentReport, param, callbackResult, true);
                }
                #endregion

                #region Get Map Styles Content
                else if (command == "GetMapStylesContent")
                {
                    StiMapHelper.GetStylesContent(currentReport, param, callbackResult, true);
                }
                #endregion

                #region Get CrossTab Styles Content
                else if (command == "GetCrossTabStylesContent")
                {
                    callbackResult["stylesContent"] = StiCrossTabHelper.GetColorStyles();
                }
                #endregion

                #region Get Table Styles Content
                else if (command == "GetTableStylesContent")
                {
                    callbackResult["stylesContent"] = StiTableHelper.GetTableStyles();
                }
                #endregion

                #region ChangeTableComponent
                else if (command == "ChangeTableComponent")
                {
                    var table = currentReport.GetComponentByName((string)param["tableName"]);
                    if (table != null && table is StiTable)
                    {
                        StiTableHelper tableHelper = new StiTableHelper(table as StiTable, StiReportEdit.StrToDouble((string)param["zoom"]));
                        tableHelper.ExecuteJSCommand(param["changeParameters"] as Hashtable, callbackResult);
                        tableHelper = null;
                    }
                }
                #endregion

                #region UpdateImagesArray
                else if (command == "UpdateImagesArray")
                {
                    callbackResult["images"] = GetImagesArray(true);
                    return;
                }
                #endregion

                #region Open Style
                else if (command == "OpenStyle")
                {
                    StiStylesHelper.OpenStyle(currentReport, param, callbackResult);
                }
                #endregion

                #region Copy Style
                else if (command == "CopyStyle")
                {
                    StiStylesHelper.CopyStyle(this, currentReport, param, callbackResult);
                }
                #endregion

                #region Paste Style
                else if (command == "PasteStyle")
                {
                    StiStylesHelper.PasteStyle(this, currentReport, param, callbackResult);
                }
                #endregion

                #region Duplicate Style
                else if (command == "DuplicateStyle")
                {
                    StiStylesHelper.DuplicateStyle(currentReport, param, callbackResult);
                }
                #endregion

                #region Apply Style Properties
                else if (command == "ApplyStyleProperties")
                {
                    Hashtable styleProperties = (Hashtable)param["styleProperties"];
                    StiBaseStyle style = report.Styles[(string)param["styleOldName"]];
                    if (style != null) StiStylesHelper.ApplyStyleProperties(style, styleProperties);
                }
                #endregion

                #region Get Style From Component
                else if (command == "CreateStyleFromComponent")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiStylesHelper.CreateStyleFromComponent(currentReport, param, callbackResult);
                }
                #endregion

                #region Change Size Components
                else if (command == "ChangeSizeComponents")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.ChangeSizeComponents(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateFieldOnDblClick
                else if (command == "CreateFieldOnDblClick")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.CreateFieldOnDblClick(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Params From Query String
                else if (command == "GetParamsFromQueryString")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.GetParamsFromQueryString(currentReport, param, callbackResult);
                }
                #endregion

                #region Create Moving Copy Component
                else if (command == "CreateMovingCopyComponent")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.CreateMovingCopyComponent(this, currentReport, param, callbackResult);
                }
                #endregion

                #region Get Report Check Items
                else if (command == "GetReportCheckItems")
                {
                    List<StiCheck> checks = StiReportCheckHelper.CheckReport(this, currentReport, param);
                    callbackResult["checkItems"] = StiReportCheckHelper.GetChecksJSCollection(checks);
                }
                #endregion

                #region Get Check Preview
                else if (command == "GetCheckPreview")
                {
                    StiReportCheckHelper.GetCheckPreview(this, currentReport, param, callbackResult);
                }
                #endregion

                #region Action Check
                else if (command == "ActionCheck")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportCheckHelper.ActionCheck(this, currentReport, param, callbackResult);
                }
                #endregion

                #region Check Expression
                else if (command == "CheckExpression")
                {
                    StiReportCheckHelper.CheckExpression(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Globalization Strings
                else if (command == "GetGlobalizationStrings")
                {
                    callbackResult["globalizationStrings"] = StiCultureHelper.GetReportGlobalizationStrings(currentReport);
                    callbackResult["cultures"] = StiCultureHelper.GetItems(CultureTypes.NeutralCultures);
                }
                #endregion

                #region Add Globalization Strings
                else if (command == "AddGlobalizationStrings")
                {
                    StiCultureHelper.AddReportGlobalizationStrings(currentReport, param, callbackResult);
                }
                #endregion

                #region Remove Globalization Strings
                else if (command == "RemoveGlobalizationStrings")
                {
                    StiCultureHelper.RemoveReportGlobalizationStrings(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Culture Settings From Report
                else if (command == "GetCultureSettingsFromReport")
                {
                    StiCultureHelper.GetCultureSettingsFromReport(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Culture Settings To Report
                else if (command == "SetCultureSettingsToReport")
                {
                    StiCultureHelper.SetCultureSettingsToReport(currentReport, param, callbackResult);
                }
                #endregion

                #region Apply Globalization Strings
                else if (command == "ApplyGlobalizationStrings")
                {
                    StiCultureHelper.ApplyGlobalizationStrings(currentReport, param, callbackResult);
                }
                #endregion

                #region CloneGaugeComponent
                else if (command == "StartEditGaugeComponent")
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null)
                    {
                        StiReportEdit.SaveComponentClone(this, component);
                        callbackResult["properties"] = StiGaugeHelper.GetGaugeProperties(component as StiGauge);
                    }
                }
                #endregion

                #region Get Resource Content
                else if (command == "GetResourceContent")
                {
                    StiResourcesHelper.GetResourceContent(currentReport, param, callbackResult);
                }
                #endregion

                #region Convert Resource Content
                else if (command == "ConvertResourceContent")
                {
                    StiResourceType type = (StiResourceType)Enum.Parse(typeof(StiResourceType), param["type"] as string);
                    string contentBase64Str = param["content"] as string;
                    byte[] content = Convert.FromBase64String(contentBase64Str.Substring(contentBase64Str.IndexOf("base64,") + 7));
                    callbackResult["content"] = StiResourcesHelper.GetStringContentForJSFromResourceContent(type, content);
                }
                #endregion

                #region Get Resource Text
                else if (command == "GetResourceText")
                {
                    StiResourcesHelper.GetResourceText(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Resource Text
                else if (command == "SetResourceText")
                {
                    StiResourcesHelper.SetResourceText(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Resource View Data
                else if (command == "GetResourceViewData")
                {
                    StiResourcesHelper.GetResourceViewData(currentReport, param, callbackResult);
                }
                #endregion

                #region UpdateGaugeComponent
                else if (command == "UpdateGaugeComponent")
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is StiGauge)
                    {
                        StiGaugeHelper.ExecuteJSCommand(component as StiGauge, param["updateParameters"] as Hashtable, callbackResult);
                    }
                }
                #endregion

                #region Get Images Gallery
                else if (command == "GetImagesGallery")
                {
                    StiDictionaryHelper.GetImagesGallery(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateImageComponent
                else if (command == "CreateImageComponent")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiReportEdit.CreateImageComponentFromDictionary(currentReport, param, callbackResult);
                }
                #endregion

                #region GetSampleConnectionString
                else if (command == "GetSampleConnectionString")
                {
                    StiDictionaryHelper.GetSampleConnectionString(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateDatabaseFromResource
                else if (command == "CreateDatabaseFromResource")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.CreateDatabaseFromResource(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Rich Text Gallery
                else if (command == "GetRichTextGallery")
                {
                    StiDictionaryHelper.GetRichTextGallery(currentReport, param, callbackResult);
                }
                #endregion

                #region Get RichText Content
                else if (command == "GetRichTextContent")
                {
                    callbackResult["content"] = StiGalleriesHelper.GetHtmlStringFromRichTextItem(currentReport, param["itemObject"] as Hashtable);
                }
                #endregion

                #region Delete All DataSources
                else if (command == "DeleteAllDataSources")
                {
                    StiReportEdit.AddReportToUndoArray(this, currentReport, command);
                    StiDictionaryHelper.DeleteAllDataSources(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Report Cloud Server Mode                
                else if (command == "LoadReportFromCloud")
                {
#if SERVER
                    Hashtable itemObject = param["itemObject"] as Hashtable;

                    if (param.Contains("isOnlineVersion"))
                    {
                        StiReport newReport = new StiReport();

                        var connection = new StiServerConnection(cloudServerAdress);
                        connection.Accounts.Users.Login(param["sessionKey"] as string, false);

                        var commandResourceGet = new StiItemResourceCommands.Get
                        {
                            ItemKey = itemObject["Key"] as string,
                            SessionKey = param["sessionKey"] as string
                        };

                        if (itemObject.ContainsKey("VersionKey"))
                        {
                            commandResourceGet.VersionKey = itemObject["VersionKey"] as string;
                        }

                        commandResourceGet = connection.RunCommand(commandResourceGet) as StiItemResourceCommands.Get;

                        if (commandResourceGet.ResultSuccess && commandResourceGet.ResultResource != null && commandResourceGet.ResultResource.Length > 0)
                        {
                            try
                            {
                                newReport.Load(commandResourceGet.ResultResource);
                                newReport.Info.Zoom = 1;
                                StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), newReport);
                            }
                            catch (Exception e)
                            {
                                callbackResult["error"] = e.Message;
                            };
                        }
                        else if (commandResourceGet.ResultNotice != null)
                        {
                            callbackResult["errorMessage"] = commandResourceGet.ResultNotice.CustomMessage;
                        }

                        currentReport = newReport;
                        string reportObjectStr = StiReportEdit.WriteReportInObject(currentReport);

                        if (reportObjectStr != null)
                        {
                            callbackResult["reportObject"] = reportObjectStr;
                            callbackResult["reportGuid"] = this.ReportGuid;
                        }
                    }
                    else
                    {
                        var getReportForDesignCommand = new StiReportCommands.Design()
                        {
                            ReportTemplateItemKey = itemObject["Key"] as string,
                            SessionKey = param["sessionKey"] as string
                        };

                        if (itemObject.ContainsKey("VersionKey"))
                        {
                            getReportForDesignCommand.VersionKey = itemObject["VersionKey"] as string;
                        }

                        Hashtable attachedItems = GetReportAttachedItems(itemObject["AttachedItems"] as ArrayList, param["sessionKey"] as string);
                        getReportForDesignCommand = RunCommand(getReportForDesignCommand) as StiReportCommands.Design;

                        if (getReportForDesignCommand.ResultSuccess && getReportForDesignCommand.ResultReport != null)
                        {
                            StiReport newReport = new StiReport();
                            try
                            {
                                newReport.Load(getReportForDesignCommand.ResultReport);
                                newReport.Info.Zoom = 1;
                                StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), newReport);
                            }
                            catch (Exception e) { callbackResult["error"] = e.Message; };

                            currentReport = newReport;
                            string reportObjectStr = StiReportEdit.WriteReportInObject(currentReport, attachedItems);
                            if (reportObjectStr != null)
                            {
                                callbackResult["reportObject"] = reportObjectStr;
                                callbackResult["reportGuid"] = this.ReportGuid;
                            }
                        }
                        else
                        {
                            if (getReportForDesignCommand.ResultNotice != null)
                                callbackResult["errorMessage"] = getReportForDesignCommand.ResultNotice.CustomMessage;
                        }
                    }
#endif
                }
                #endregion

                #region CloneBarCodeComponent
                else if (command == "StartEditBarCodeComponent")
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null)
                    {
                        StiReportEdit.SaveComponentClone(this, component);
                        callbackResult["barCode"] = StiBarCodeHelper.GetBarCodeJSObject(component as StiBarCode);
                    }
                }
                #endregion

                #region Get BarCode Samples
                else if (command == "GetBarCodeSamples")
                {
                    StiBarCodeHelper.GetBarCodeSamples(currentReport, param, callbackResult);
                }
                #endregion

                #region Apply BarCode Properties
                else if (command == "ApplyBarCodeProperties")
                {
                    StiBarCodeHelper.ApplyBarCodeProperties(currentReport, param, callbackResult);
                }
                #endregion

                #region GetVariableItemsFromDataColumn
                else if (command == "GetVariableItemsFromDataColumn")
                {
                    StiDictionaryHelper.GetVariableItemsFromDataColumn(currentReport, param, callbackResult);
                }
                #endregion

                #region Move Dictionary Item
                else if (command == "MoveDictionaryItem")
                {
                    StiDictionaryHelper.MoveDictionaryItem(currentReport, param, callbackResult);
                }
                #endregion

                #region Convert MetaFile To Png
                else if (command == "ConvertMetaFileToPng")
                {
                    callbackResult["fileContent"] = StiResourcesHelper.ConvertBase64MetaFileToBase64Png(param["fileContent"] as string);
                }
                #endregion

                #region GetReportString
                else if (command == "GetReportString")
                {
                    if (currentReport != null)
                        callbackResult["reportString"] = StiEncodingHelper.Encode(currentReport.SaveToString());
                }
                #endregion

                this.Report = currentReport;
            }
            catch (Exception e)
            {
#if SERVER
                StiLog.WriteException(e);
#endif
                callbackResult["error"] = e.Message;
            }
        }

        public string GetCallbackResult()
        {
            callbackResult["designerId"] = this.ClientID;
            return JSON.Encode(this.callbackResult);
        }

        public static Hashtable GetRequestParams(Page page)
        {
            if (page.Request.Params["__CALLBACKPARAM"] != null)
            {
                string text = StiEncodingHelper.DecodeString(page.Request.Params["__CALLBACKPARAM"]);
                if (String.IsNullOrEmpty(text))
                    text = page.Request.Params["__CALLBACKPARAM"] as string;
                if (!String.IsNullOrEmpty(text))
                    return JSON.Decode(text) as Hashtable;
            }
            return null;
        }
        #endregion

        #region Process Request
        private Hashtable GetDesignerParametersFromCloud()
        {
            if (!CloudMode || HttpContext.Current.Request.Form.AllKeys == null || HttpContext.Current.Request.Form.AllKeys.Length == 0) return null;
            string[] keys = HttpContext.Current.Request.Form.AllKeys;
            Hashtable parameters = new Hashtable();
            for (int i = 0; i < keys.Length; i++)
            {
                parameters[keys[i]] = HttpContext.Current.Request.Form[keys[i]];
            }
            return parameters;
        }

        private string GetRequestParam(string key)
        {
            var value = Page.Request.QueryString[key];
            return value != null ? value : Page.Request.Params[key];
        }

        internal string GetImageUrl(string imageName)
        {
            string imageUrl = string.Empty;
            {
                if (IsDesignMode)
                {
                    imageUrl = Path.Combine(Environment.GetEnvironmentVariable("Temp"), "StiNewMobileDesigner");
                    if (!Directory.Exists(imageUrl)) Directory.CreateDirectory(imageUrl);
                    if (!string.IsNullOrEmpty(imageName))
                    {
                        imageUrl = Path.Combine(imageUrl, imageName);
                        try
                        {
                            if (!File.Exists(imageUrl))
                            {
                                Bitmap bmp = this.GetImage(imageName);
                                bmp.Save(imageUrl);
                                bmp.Dispose();
                            }
                        }
                        catch
                        {
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(this.ImagesPath))
                    {
                        imageUrl = this.ImagesPath + imageName;

                        try
                        {
                            // Ïðîâåðÿåì ñóùåñòâîâàíèå çàäàííîé äèðåêòîðèè èëè èçîáðàæåíèÿ
                            string fileServerPath = HttpContext.Current.Server.MapPath(imageUrl);
                            if (string.IsNullOrEmpty(imageName))
                            {
                                if (!Directory.Exists(fileServerPath)) imageUrl = string.Empty;
                            }
                            else
                            {
                                if (!File.Exists(fileServerPath)) imageUrl = string.Empty;
                            }
                        }
                        catch
                        {
                        }
                    }

                    if (string.IsNullOrEmpty(imageUrl))
                    {
                        imageUrl = this.Page.Request.Url.AbsolutePath;
                        if (this.UseRelativeUrls)
                        {
                            imageUrl = this.Page.Request.RawUrl;

                            // Åñëè âêëþ÷åí ôèëüòð url-îâ è âêëþ÷åíî ñîõðàíåíèå ñåññèè â url (Cookless mode), òî ôîðìèðóåì ïðàâèëüíûé url
                            string aspFilterSessionId = this.Page.Request.Headers.Get("AspFilterSessionId");
                            if (!string.IsNullOrEmpty(aspFilterSessionId) && aspFilterSessionId.Length > 0) imageUrl = InsertSessionId(imageUrl, aspFilterSessionId);
                        }

                        string query = this.Page.Request.Url.Query;
                        if (!string.IsNullOrEmpty(query)) imageUrl = string.Format("{0}{1}&stimulsoft_image_mobiledesigner={2}", imageUrl, query, imageName);
                        else imageUrl = string.Format("{0}?stimulsoft_image_mobiledesigner={1}", imageUrl, imageName);
                    }
                }
            }

            imageUrl = imageUrl.Replace("'", "\\'").Replace("\"", "&quot;");
            return imageUrl;
        }

        internal Bitmap GetImage(string imageName)
        {
            Assembly a = typeof(StiMobileDesigner).Assembly;

            Stream stream = a.GetManifestResourceStream(IMAGES_PATH + "." +
                (this.Theme.ToString().StartsWith("Office2013") ? "Office2013" : this.Theme.ToString()) + "." + imageName);
            if (stream != null) return new Bitmap(stream);
            return null;
        }

        private static byte[] StreamToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        internal byte[] GetByteArray(string resourseName)
        {
            Assembly a = typeof(StiMobileDesigner).Assembly;

            Stream stream = a.GetManifestResourceStream(IMAGES_PATH + "." +
                (this.Theme.ToString().StartsWith("Office2013") ? "Office2013" : this.Theme.ToString()) + "." + resourseName);
            if (stream != null) return StreamToByteArray(stream);
            return null;
        }

        public bool ProcessScriptRequest()
        {
            var scriptName = GetRequestParam("mobiledesigner_script");
            if (scriptName == null) return false;
            var script = string.Empty;

            //Viewer Scripts
            if (scriptName == "Viewer" || scriptName == "AllNotLoadedScripts")
            {
                StiMobileViewer viewer = GetViewer();
                string viewerScript = viewer.GetViewerScripts();
                viewer.Dispose();

                if (scriptName == "Viewer")
                {
                    ResponseString(viewerScript, "text/javascript");
                    return true;
                }
                else if (scriptName == "AllNotLoadedScripts")
                {
                    script += viewerScript;
                }
            }

            //Designer Scripts
            Assembly a = typeof(StiMobileDesigner).Assembly;
            var names = a.GetManifestResourceNames();

            string[] scriptNames = scriptName.Split(';');

            foreach (string scriptName_ in scriptNames)
            {
                foreach (var name in names)
                {
                    if (name.EndsWith(".js") && ((scriptName == "DesignerScripts" && !name.EndsWith("_NotLoad.js"))
                        || name.EndsWith(scriptName_ + "_NotLoad.js") || (scriptName == "AllNotLoadedScripts" && name.EndsWith("_NotLoad.js"))))
                    {
                        Stream stream = a.GetManifestResourceStream(name);
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            var scriptText = reader.ReadToEnd();
                            script += scriptText + "\r\n";
                        }
                    }
                }
            }

            ResponseString(script, "text/javascript");
            return true;
        }

        private Hashtable GetCssConstants(string cssText)
        {
            Hashtable constants = new Hashtable();
            int startIndex = cssText.IndexOf('@');
            var constantsStr = cssText.Substring(startIndex, cssText.LastIndexOf(';') - startIndex);
            string[] constantsArray = constantsStr.Split(';');
            for (int i = 0; i < constantsArray.Length; i++)
            {
                string[] tmpArray = constantsArray[i].Split('=');
                if (tmpArray.Length == 2)
                {
                    constants[tmpArray[0].Trim()] = tmpArray[1];
                }
            }

            return constants;
        }

        public bool ProcessCssStyleRequest()
        {
            var themeName = GetRequestParam("stimobiledesigner_css");
            if (themeName == null) return false;

            if (DemoMode && GetRequestParam("themename") != null)
            {
                Theme = (StiMobileDesignerTheme)Enum.Parse(typeof(StiMobileDesignerTheme), (string)GetRequestParam("themename"));
            }

            if (themeName.StartsWith("Viewer") && string.IsNullOrEmpty(CustomCss))
            {
                StiMobileViewer viewer = GetViewer();
                string viewerStyles = viewer.GetViewerStyles();
                ResponseString(viewerStyles, "text/css");
                viewer.Dispose();
                return true;
            }

            Assembly a = typeof(StiMobileDesigner).Assembly;
            var names = a.GetManifestResourceNames();
            var css = string.Empty;
            string pathThemeName = themeName.StartsWith("Office2013") ? "Office2013" : themeName;
            var path = string.Format("{0}.{1}.", STYLES_PATH, pathThemeName);
            Hashtable constants = null;

            foreach (var name in names)
            {
                if (name.IndexOf(path) == 0 && name.EndsWith(".css"))
                {
                    Stream stream = a.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string cssText = reader.ReadToEnd();
                        if (name.EndsWith(themeName + ".Constants.css")) constants = GetCssConstants(cssText);
                        else if (!name.EndsWith(".Constants.css")) css += cssText + "\r\n";
                    }
                }
            }

            if (constants != null)
            {
                foreach (DictionaryEntry constant in constants)
                {
                    css = css.Replace((string)constant.Key, (string)constant.Value);
                }
            }

            ResponseString(css, "text/css");
            return true;
        }

        public bool ProcessImageRequest()
        {
            string imageGuid = GetRequest("stimulsoft_image_mobiledesigner") as string;
            if (imageGuid == null) return false;

            byte[] imageBuffer = null;
            if (imageGuid.EndsWith(".gif") || imageGuid.EndsWith(".png") || imageGuid.EndsWith(".cur"))
            {
                if (imageGuid.EndsWith(".cur"))
                {
                    imageBuffer = GetByteArray(imageGuid);
                }
                else
                {
                    System.Drawing.Image image = GetImage(imageGuid);
                    if (image != null) imageBuffer = StiImageConverter.ImageToBytes(image);
                }
            }
            else
            {
                if (CacheMode == StiCacheMode.Page) imageBuffer = (byte[])this.Page.Cache[imageGuid];
                else imageBuffer = (byte[])this.Page.Session[imageGuid];
            }

            this.Page.Response.ClearContent();
            this.Page.Response.ContentType = imageGuid.EndsWith(".cur") ? "application/octet-stream" : "image/Png";
            if (imageBuffer != null) this.Page.Response.BinaryWrite(imageBuffer);
            this.Page.Response.End();

            return true;
        }
        #endregion

        #region Render Resources

        private void RenderCssStyles()
        {
            if (DemoMode && GetRequestParam("themename") != null)
            {
                Theme = (StiMobileDesignerTheme)Enum.Parse(typeof(StiMobileDesignerTheme), (string)GetRequestParam("themename"));
            }

            var url = GetRequestUrl("stimobiledesigner_css", Theme.ToString(), PassQueryParametersForResources);
            url += url.IndexOf("?") > 0 ? "&" : "?";
            url += "mobiledesigner_version=" + GetProductVersion(true);
            var cssDesigner = string.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />", string.IsNullOrEmpty(CustomCss) ? url : CustomCss);
            RegisterClientScriptBlockIntoHeader(url, cssDesigner);
        }

        private void RenderScripts()
        {
            var url = GetRequestUrl("mobiledesigner_script", "DesignerScripts", PassQueryParametersForResources);
            url += url.IndexOf("?") > 0 ? "&" : "?";
            url += "mobiledesigner_version=" + GetProductVersion(true);
            var script = string.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", url);
            RegisterClientScriptBlockIntoHeader(url, script);
        }

        private void RegisterClientScriptBlockIntoHeader(string key, string script)
        {
            if (HttpContext.Current.Items.Contains("scriptblock_" + key)) return;

            HttpContext.Current.Items.Add("scriptblock_" + key, string.Empty);
            int? index = HttpContext.Current.Items["__ScriptResourceIndex"] as int?;
            if (index == null) index = 0;
            Page.Header.Controls.Add(new LiteralControl(script));
            HttpContext.Current.Items["__ScriptResourceIndex"] = index;
        }

        private string GetRequestUrl(string parameter, string value)
        {
            return GetRequestUrl(parameter, value, true);
        }

        private string GetRequestUrl(string parameter, string value, bool passQueryParams)
        {
            string url = this.Page.Request.Url.AbsolutePath;
            if (this.UseRelativeUrls) url = this.Page.Response.ApplyAppPathModifier(url);

            if (parameter != null && value != null)
            {
                string query = passQueryParams ? this.Page.Request.Url.Query : string.Empty;
                if (!string.IsNullOrEmpty(query)) url = string.Format("{0}{1}&{2}={3}", url, query, parameter, value);
                else url = string.Format("{0}?{1}={2}", url, parameter, value);

                if (CloudMode && parameter == "mobiledesigner_script")
                {
                    if (this.Page.Request.Params["localizationName"] != null) url += "&localizationName=" + this.Page.Request.Params["localizationName"];
                    if (this.Page.Request.Params["sessionKey"] != null) url += "&sessionKey=" + this.Page.Request.Params["sessionKey"];
                }

                url = url.Replace("'", "\\'").Replace("\"", "&quot;");
            }

            return url;
        }

        private StiEmptyObject emptyObject = new StiEmptyObject();

        private string GetWebResourceUrl(string resourceName)
        {
            return Page.ClientScript.GetWebResourceUrl(emptyObject.GetType(), resourceName);
        }

        private ArrayList GetResourceNamesBySubstring(string subString)
        {
            ArrayList resultResurceNames = new ArrayList();
            Assembly assembly = typeof(StiMobileDesigner).Assembly;

            string[] resourceNames = assembly.GetManifestResourceNames();
            foreach (string resourceName in resourceNames)
                if (resourceName.IndexOf(subString) != -1) resultResurceNames.Add(resourceName);

            return resultResurceNames;
        }

        internal string GetApplicationUrl(bool addQuerySeparator)
        {
            string url = this.Page.Request.Url.AbsoluteUri;
            if (this.UseRelativeUrls) url = this.Page.Response.ApplyAppPathModifier(this.Page.Request.Url.AbsolutePath);

            if (addQuerySeparator)
            {
                string query = this.Page.Request.Url.Query;
                if (!string.IsNullOrEmpty(query)) url = string.Format("{0}{1}&", url, query);
                else url = string.Format("{0}?", url);
            }
            return url;
        }
        #endregion

        #region Override Methods
        protected override void CreateChildControls()
        {
            this.Controls.Clear();

            Panel mainPanel = new Panel();
            mainPanel.ID = "MainPanel";
            mainPanel.CssClass = "stiDesignerMainPanel";
            this.Controls.Add(mainPanel);

            if (!DesignMode)
            {
                if (CloudMode)
                    jsParameters = GetJSParameters();
                else
                {
                    this.Viewer = GetViewer();
                    this.Controls.Add(this.Viewer);
                    this.Viewer.CssClass = "StiMobileViewerClass";
                    this.viewerId = this.Viewer.ClientID;
                }
            }

            if (this.Width == Unit.Empty && this.Height == Unit.Empty)
            {
                this.Height = Unit.Empty;
                this.Style.Add("position", "absolute");
                this.Style.Add("top", "0px");
                this.Style.Add("left", "0px");
                this.Style.Add("right", "0px");
                this.Style.Add("bottom", "0px");
            }
            else
            {
                mainPanel.Width = this.Width;
                mainPanel.Height = this.Height;
            }

            base.CreateChildControls();
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            #region Design Mode
            if (IsDesignMode)
            {
                Panel panel = new Panel();
                //panel.Style.Add("background", "red");
                panel.BorderColor = this.BorderColor.IsEmpty ? Color.DarkGray : this.BorderColor;
                panel.BorderWidth = this.BorderWidth.IsEmpty ? 2 : this.BorderWidth;
                panel.BorderStyle = this.BorderStyle == BorderStyle.NotSet ? BorderStyle.Solid : this.BorderStyle;
                panel.Width = this.Width;
                panel.Height = this.Height;
                panel.Style.Add("overflow", "hidden");

                #region ToolBar
                Table tableToolBar = new Table();
                tableToolBar.Style.Add("width", "100%");
                tableToolBar.Style.Add("height", "56px");
                tableToolBar.CellPadding = 0;
                tableToolBar.CellSpacing = 0;
                TableRow rowToolBar = new TableRow();
                TableCell cellToolBarLeft = new TableCell();
                TableCell cellToolBarMiddle = new TableCell();
                TableCell cellToolBarRight = new TableCell();
                tableToolBar.Rows.Add(rowToolBar);
                rowToolBar.Cells.Add(cellToolBarLeft);
                rowToolBar.Cells.Add(cellToolBarMiddle);
                rowToolBar.Cells.Add(cellToolBarRight);

                Panel leftPanel = new Panel();
                leftPanel.Style.Add("width", "542px");
                leftPanel.Style.Add("height", "119px");
                cellToolBarLeft.Controls.Add(leftPanel);

                Panel rightPanel = new Panel();
                rightPanel.Style.Add("width", "25px");
                rightPanel.Style.Add("height", "119px");
                cellToolBarRight.Controls.Add(rightPanel);

                leftPanel.Style.Add("background", "url(" + GetImageUrl("ToolBarLeftHalf.gif") + ")");
                cellToolBarMiddle.Style.Add("background", "url(" + GetImageUrl("ToolBarMiddleHalf.gif") + ")");
                rightPanel.Style.Add("background", "url(" + GetImageUrl("ToolBarRightHalf.gif") + ")");
                cellToolBarMiddle.Style.Add("height", "119px");
                cellToolBarMiddle.Style.Add("width", "100%");

                panel.Controls.Add(tableToolBar);
                #endregion

                #region Center
                Table tableCenter = new Table();
                tableCenter.Style.Add("height", "100%");
                tableCenter.Style.Add("width", "100%");
                tableCenter.Style.Add("background", "#f0f0f0");
                tableCenter.Style.Add("color", "black");
                TableRow rowCenter = new TableRow();
                TableCell cellCenter = new TableCell();
                tableCenter.Rows.Add(rowCenter);
                rowCenter.Cells.Add(cellCenter);

                cellCenter.Text = "Mobile Designer";
                cellCenter.HorizontalAlign = HorizontalAlign.Center;
                cellCenter.VerticalAlign = VerticalAlign.Middle;

                panel.Controls.Add(tableCenter);
                #endregion

                panel.RenderControl(output);
            }
            #endregion

            #region Runtime Mode
            else
            {
                base.RenderContents(output);
            }
            #endregion
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsDesignMode)
            {
                this.EnsureChildControls();
                RenderCssStyles();
                RenderScripts();
            }

            base.OnPreRender(e);
        }

        protected override void Render(HtmlTextWriter output)
        {

            base.Render(output);
            output.Write(string.Format("<script language=\"javascript\" type=\"text/javascript\">var js{0} = new StiMobileDesigner({1});</script>",
                this.ClientID,
                CloudMode || DesignMode ? jsParameters : GetJSParameters()));
        }

        protected override void OnInit(EventArgs e)
        {
            if (!IsDesignMode)
            {
                if (DemoMode && Page.Request.Params["localization"] != null)
                {
                    this.GlobalizationFile = "Localization/" + Page.Request.Params["localization"] as string + ".xml";
                }
                if (Page.Request.Params["command"] == "DownloadReport")
                {
                    this.ReportGuid = Page.Request.Params["reportGuid"];
                    string reportFile = Page.Request.Params["reportFile"] ?? "Report.mrt";
                    string password = Page.Request.Params["encryptedPassword"] as string;

                    if (this.Report != null)
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            if (!String.IsNullOrEmpty(password))
                            {
                                this.Report.SaveEncryptedReport(memoryStream, password);

                                if (reportFile.ToLower().EndsWith(".mrt"))
                                {
                                    reportFile = reportFile.Substring(0, reportFile.Length - 4) + ".mrx";
                                }
                                else if (!reportFile.ToLower().EndsWith(".mrx"))
                                {
                                    reportFile += ".mrx";
                                }
                            }
                            else
                            {
                                this.Report.Save(memoryStream);
                            }
                            ResponseFile(Page, memoryStream, "application/octet-stream", reportFile);
                            memoryStream.Close();
                        }
                    }
                }
                if (Page.Request.Params["command"] == "DownloadStyles")
                {
                    StiReport tempReport = new StiReport();

                    if (Page.Request.Params["stylesCollection"] != null)
                    {
                        ArrayList stylesCollection = JSON.Decode(Page.Request.Params["stylesCollection"] as string) as ArrayList;
                        StiStylesHelper.WriteStylesToReport(tempReport, stylesCollection);

                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            tempReport.Styles.Save(memoryStream);
                            ResponseFile(Page, memoryStream, "application/octet-stream", "Styles.sts");
                            memoryStream.Close();
                            return;
                        }
                    }
                }
                else if (Page.Request.Params["command"] == "ExitDesigner")
                {
                    InvokeExit();
                }
            }
            if (this.ProcessImageRequest()) return;
            if (!IsDesignMode)
            {
                if (this.ProcessCssStyleRequest()) return;
                if (this.ProcessScriptRequest()) return;
            }

            base.OnInit(e);
        }
        #endregion
        #endregion

        #region For Js Designer
        private static void WriteToFile(string file, string text)
        {
            var path = Path.GetDirectoryName(file);
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);

            var stream = File.Create(file);
            var buffer = Encoding.UTF8.GetBytes(text);
            stream.Write(buffer, 0, buffer.Length);
            stream.Flush();
            stream.Close();
        }

        public void GetResourcesForJsDesigner(Hashtable parameters)
        {
            string projectPath = "D:\\JsDesigner";

            //Override Parameters
            parameters["report"] = null;
            parameters["demoMode"] = false;
            parameters["dVers"] = false;
            parameters["haveExitDesignerEvent"] = false;
            parameters["haveSaveEvent"] = true;
            parameters["haveSaveAsEvent"] = true;
            parameters["jsMode"] = true;
            parameters["loc"] = "";
            parameters["textFormats"] = "";
            parameters["dateFormats"] = "";
            parameters["timeFormats"] = "";
            parameters["images"] = GetImagesArray(true);

            Assembly a = typeof(StiMobileDesigner).Assembly;
            var names = a.GetManifestResourceNames();
            string scripts = string.Empty;

            foreach (var name in names)
            {
                if (name.IndexOf(SCRIPTS_PATH) == 0 && name.EndsWith(".js"))
                {
                    Stream stream = a.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream)) scripts += reader.ReadToEnd() + "\r\n";
                    stream.Dispose();
                }
            }

            string jsParameters = JSON.Encode(parameters);

            scripts = scripts.Replace("function StiMobileDesigner", "function StiJsDesigner").Replace("StiMobileDesigner.prototype", "StiJsDesigner.prototype");
            scripts = scripts.Replace("this.defaultParameters = {};",
                "this.defaultParameters = " + jsParameters + "; " +
                "this.mergeOptions(parameters, this.defaultParameters); " +
                "parameters = this.defaultParameters;");
            WriteToFile(Path.Combine(projectPath, "Scripts", "source.designer.js"), scripts);

            //Write Styles
            string[] themes = new string[] {"Office2013DarkGrayBlue", "Office2013DarkGrayCarmine", "Office2013DarkGrayGreen", "Office2013DarkGrayOrange", "Office2013DarkGrayPurple",
                "Office2013DarkGrayTeal", "Office2013DarkGrayViolet", "Office2013LightGrayBlue", "Office2013LightGrayCarmine", "Office2013LightGrayGreen", "Office2013LightGrayOrange",
                "Office2013LightGrayPurple", "Office2013LightGrayTeal", "Office2013LightGrayViolet", "Office2013WhiteBlue", "Office2013WhiteCarmine", "Office2013WhiteGreen", "Office2013WhiteOrange",
                "Office2013WhitePurple", "Office2013WhiteTeal", "Office2013WhiteViolet", "Office2013VeryDarkGrayBlue", "Office2013VeryDarkGrayCarmine", "Office2013VeryDarkGrayGreen",
                "Office2013VeryDarkGrayOrange", "Office2013VeryDarkGrayPurple", "Office2013VeryDarkGrayTeal", "Office2013VeryDarkGrayViolet" };

            for (int i = 0; i < themes.Length; i++)
            {
                string themeName = themes[i];
                var stylesStr = string.Empty;
                string pathThemeName = themeName.StartsWith("Office2013") ? "Office2013" : themeName;
                var path = string.Format("{0}.{1}.", STYLES_PATH, pathThemeName);
                Hashtable constants = null;

                foreach (var name in names)
                {
                    if (name.IndexOf(path) == 0 && name.EndsWith(".css"))
                    {
                        Stream stream = a.GetManifestResourceStream(name);
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            string cssText = reader.ReadToEnd();
                            if (name.EndsWith(themeName + ".Constants.css")) constants = GetCssConstants(cssText);
                            else if (!name.EndsWith(".Constants.css")) stylesStr += cssText + "\r\n";
                        }
                    }
                }

                if (constants != null)
                {
                    foreach (DictionaryEntry constant in constants)
                    {
                        stylesStr = stylesStr.Replace((string)constant.Key, (string)constant.Value);
                    }
                }

                WriteToFile(Path.Combine(projectPath, "Css", "stimulsoft.designer." + themes[i].Insert("Office2013".Length, ".").ToLower() + ".css"), stylesStr);
            }
        }
        #endregion

        #region Constructor
        public StiMobileDesigner()
        {
            GlobalizationFile = string.Empty;
            UseRelativeUrls = false;
            CacheMode = StiCacheMode.Page;
            ServerTimeout = new TimeSpan(0, 20, 0);
            Report = null;
            GlobalizationFile = string.Empty;
            this.Height = Unit.Empty;
            this.Width = Unit.Empty;
        }
        #endregion
    }
}