﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.Components;
using Stimulsoft.Report.Helper;

namespace Stimulsoft.Report.Check
{
    public class StiMoveComponentToPrintablePageAreaAction : StiAction
    {
        public override string Name
        {
            get
            {
                return StiLocalizationExt.Get("CheckActions", "StiMoveComponentToPrintablePageAreaActionShort");
            }
        }

        public override string Description
        {
            get
            {
                return StiLocalizationExt.Get("CheckActions", "StiMoveComponentToPrintablePageAreaActionLong");
            }
        }

        public override void Invoke(StiReport report, object element, string elementName)
        {
            base.Invoke(report, null, null);

            StiComponent comp = element as StiComponent;
            if (comp == null) return;
            StiPage page = comp.Page;
            if (page == null) return;

            Stimulsoft.Base.Drawing.RectangleD compRect = comp.GetPaintRectangle(false, false);
            Stimulsoft.Base.Drawing.RectangleD pageRect = page.ClientRectangle;

            if (compRect.Left < pageRect.Left)
            {
                comp.Left = 0;
            }
            
            if (compRect.Top < pageRect.Top)
            {
                comp.Top = 0;
            }

            if (compRect.Right > pageRect.Right)
            {
                comp.Left -= compRect.Right - pageRect.Right;
            }

            if (compRect.Bottom > pageRect.Bottom)
            {
                comp.Top -= compRect.Bottom - pageRect.Bottom;
            }
        }
    }
}
