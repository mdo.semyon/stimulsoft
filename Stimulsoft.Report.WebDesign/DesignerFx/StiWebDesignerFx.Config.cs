#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.IO;
using System.Linq;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Web
{
    public partial class StiWebDesignerFx :
        WebControl,
        INamingContainer
    {
        private string GetLinksTarget(string value)
        {
            if (value == "Blank") return StiTargetWindow.Blank;
            if (value == "Self") return StiTargetWindow.Self;
            if (value == "Top") return StiTargetWindow.Top;
            return value;
        }

        private string GetLocalizationsList()
        {
            if (string.IsNullOrEmpty(LocalizationDirectory)) return string.Empty;

            #region Get the localization files directory

            string path = LocalizationDirectory;
            if (!Directory.Exists(path)) path = HttpContext.Current.Server.MapPath(LocalizationDirectory);
            if (!Directory.Exists(path)) path = HttpContext.Current.Server.MapPath("/" + LocalizationDirectory);

            #endregion

            #region Get the localization files list

            if (Directory.Exists(path))
            {
                var dir = new DirectoryInfo(path);

                XmlDocument xml = new XmlDocument();
                XmlElement list = xml.CreateElement("LocalizationsList");
                xml.AppendChild(list);

                XmlElement value;
                XmlElement element;

                string languages = string.Empty;
                string language;
                string description;
                string cultureName;

                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    if (file.FullName.EndsWith(".xml") && !file.FullName.EndsWith(".ext.xml"))
                    {
                        try
                        {
                            StiLocalization.GetParam(file.FullName, out language, out description, out cultureName);
                            if (!string.IsNullOrEmpty(language))
                            {
                                value = xml.CreateElement("Value");
                                list.AppendChild(value);

                                element = xml.CreateElement("FileName");
                                element.InnerText = Path.Combine(path, file.Name);
                                value.AppendChild(element);

                                element = xml.CreateElement("Language");
                                element.InnerText = language;
                                value.AppendChild(element);

                                element = xml.CreateElement("Description");
                                element.InnerText = description;
                                value.AppendChild(element);

                                element = xml.CreateElement("CultureName");
                                element.InnerText = cultureName;
                                value.AppendChild(element);
                            }
                        }
                        catch
                        {
                        }
                    }
                }

                return list.InnerXml;
            }

            #endregion

            return string.Empty;
        }

        /// <summary>
        /// Get the list of registered data adapters.
        /// </summary>
        private string GetDataAdaptersList()
        {
            var adapters = string.Empty;
            foreach (var service in StiOptions.Services.DataAdapters.Where(s => s.ServiceEnabled))
            {
                adapters += service.GetType() + "\n";
            }

            #region Databases

            string result = string.Format(
                "<AdapterXmlData>{0}</AdapterXmlData>" +
                "<AdapterJsonData>{0}</AdapterJsonData>" +
                "<AdapterDB2>{1}</AdapterDB2>" +
                "<AdapterFirebird>{2}</AdapterFirebird>" +
                "<AdapterInformix>{3}</AdapterInformix>" +
                "<AdapterMSAccess>{4}</AdapterMSAccess>" +
                "<AdapterMySql>{5}</AdapterMySql>" +
                "<AdapterOdbc>{6}</AdapterOdbc>" +
                "<AdapterOleDb>{7}</AdapterOleDb>" +
                "<AdapterOracle>{8}</AdapterOracle>" +
                "<AdapterOracleODP>{9}</AdapterOracleODP>" +
                "<AdapterPostgreSQL>{10}</AdapterPostgreSQL>" +
                "<AdapterSql>{11}</AdapterSql>" +
                "<AdapterSqlCe>{12}</AdapterSqlCe>" +
                "<AdapterSQLite>{13}</AdapterSQLite>" +
                "<AdapterSybaseAds>{14}</AdapterSybaseAds>" +
                "<AdapterSybaseAse>{15}</AdapterSybaseAse>" +
                "<AdapterUniDirect>{16}</AdapterUniDirect>" +
                "<AdapterDataView>{17}</AdapterDataView>" +
                "<AdapterVistaDB>{18}</AdapterVistaDB>" +
                "<AdapterVirtual>{19}</AdapterVirtual>" +
                "<AdapterDBase>{20}</AdapterDBase>" +
                "<AdapterUser>{21}</AdapterUser>" +
                "<AdapterBusinessObject>{22}</AdapterBusinessObject>" +
                "<AdapterCrossTab>{23}</AdapterCrossTab>" +
                "<AdapterCsv>{24}</AdapterCsv>" +
                "<AdapterDotConnectUniversal>{25}</AdapterDotConnectUniversal>" +
                "<AdapterEffiProz>{26}</AdapterEffiProz>",
                adapters.IndexOf("StiDataTableAdapterService") >= 0,
                adapters.IndexOf("StiDB2AdapterService") >= 0,
                adapters.IndexOf("StiFirebirdAdapterService") >= 0,
                adapters.IndexOf("StiInformixAdapterService") >= 0,
                adapters.IndexOf("StiMSAccessAdapterService") >= 0,
                adapters.IndexOf("StiMySqlAdapterService") >= 0,
                adapters.IndexOf("StiOdbcAdapterService") >= 0,
                adapters.IndexOf("StiOleDbAdapterService") >= 0,
                adapters.IndexOf("StiOracleAdapterService") >= 0,
                adapters.IndexOf("StiOracleODPAdapterService") >= 0,
                adapters.IndexOf("StiPostgreSQLAdapterService") >= 0,
                adapters.IndexOf("StiSqlAdapterService") >= 0,
                adapters.IndexOf("StiSqlCeAdapterService") >= 0,
                adapters.IndexOf("StiSQLiteAdapterService") >= 0,
                adapters.IndexOf("StiSybaseAdsAdapterService") >= 0,
                adapters.IndexOf("StiSybaseAseAdapterService") >= 0,
                adapters.IndexOf("StiUniDirectAdapterService") >= 0,
                adapters.IndexOf("StiDataViewAdapterService") >= 0,
                adapters.IndexOf("StiVistaDBAdapterService") >= 0,
                adapters.IndexOf("StiVirtualAdapterService") >= 0,
                adapters.IndexOf("StiDBaseAdapterService") >= 0,
                adapters.IndexOf("StiUserAdapterService") >= 0,
                false, //adapters.IndexOf("StiBusinessObjectAdapterService") >= 0,
                adapters.IndexOf("StiCrossTabAdapterService") >= 0,
                adapters.IndexOf("StiCsvAdapterService") >= 0,
                adapters.IndexOf("StiDotConnectUniversalAdapterService") >= 0,
                adapters.IndexOf("StiEffiProzAdapterService") >= 0);

            #endregion

            return result;
        }

        private string RenderConfig()
        {
            // Create the Xml document
            XmlDocument doc = new XmlDocument();
            XmlDeclaration declaration = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(declaration);

            XmlElement category = null;
            XmlElement subcategory = null;
            XmlElement element = null;

            // Create the main node
            XmlElement config = doc.CreateElement("StiSerializer");
            config.SetAttribute("type", "Net");
            config.SetAttribute("version", "1.02");
            config.SetAttribute("application", "StiOptions");
            doc.AppendChild(config);

            #region Connection

            category = doc.CreateElement("Connection");
            config.AppendChild(category);

            element = doc.CreateElement("ClientRepeatCount");
            element.InnerText = RepeatCount.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ClientRequestTimeout");
            element.InnerText = RequestTimeout.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("EnableDataLogger");
            element.InnerText = EnableDataLogger.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowCancelButton");
            element.InnerText = ShowCancelButton.ToString();
            category.AppendChild(element);

            #endregion

            #region Appearance

            category = doc.CreateElement("Appearance");
            config.AppendChild(category);

            element = doc.CreateElement("ShowTooltips");
            element.InnerText = ShowTooltips.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowTooltipsHelp");
            element.InnerText = ShowTooltipsHelp.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowFormsHelp");
            element.InnerText = ShowDialogsHelp.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowFormsHints");
            element.InnerText = ShowDialogsHints.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("DateFormat");
            element.InnerText = DateFormat;
            category.AppendChild(element);

            #endregion

            #region AboutDialog

            category = doc.CreateElement("AboutDialog");
            config.AppendChild(category);

            element = doc.CreateElement("TextLine1");
            element.InnerText = AboutDialogTextLine1;
            category.AppendChild(element);

            element = doc.CreateElement("TextLine2");
            element.InnerText = AboutDialogTextLine2;
            category.AppendChild(element);

            element = doc.CreateElement("Url");
            element.InnerText = AboutDialogUrl;
            category.AppendChild(element);

            element = doc.CreateElement("UrlText");
            element.InnerText = AboutDialogUrlText;
            category.AppendChild(element);

            element = doc.CreateElement("FrameworkType");
            element.InnerText = "ASP.NET";
            category.AppendChild(element);

            #endregion

            #region Localization

            subcategory = doc.CreateElement("Localizations");
            config.AppendChild(subcategory);

            element = doc.CreateElement("Localization");
            element.InnerText = Localization;
            subcategory.AppendChild(element);

            string localizationsList = GetLocalizationsList();
            if (!string.IsNullOrEmpty(localizationsList))
            {
                element = doc.CreateElement("LocalizationsList");
                element.InnerXml = localizationsList;
                subcategory.AppendChild(element);
            }

            #endregion

            #region Print

            category = doc.CreateElement("Print");
            config.AppendChild(category);

            element = doc.CreateElement("ShowPrintDialog");
            element.InnerText = ShowPrintDialog.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AutoPageOrientation");
            element.InnerText = AutoPageOrientation.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AutoPageScale");
            element.InnerText = AutoPageScale.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowDefaultPrint");
            element.InnerText = AllowDefaultPrint.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowPrintToPdf");
            element.InnerText = AllowPrintToPdf.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowPrintToHtml");
            element.InnerText = AllowPrintToHtml.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("PrintAsBitmap");
            element.InnerText = PrintAsBitmap.ToString();
            category.AppendChild(element);

            #endregion

            #region Theme

            element = doc.CreateElement("Theme");
            element.InnerText = Theme.ToString().Replace("Office2007", string.Empty);
            config.AppendChild(element);

            #endregion

            #region Other

            element = doc.CreateElement("DesignerEventFunction");
            element.InnerText = DesignerEventFunction;
            config.AppendChild(element);

            #endregion

            #region Designer

            category = doc.CreateElement("Designer");
            config.AppendChild(category);

            element = doc.CreateElement("SaveMode");
            element.InnerText = SaveReportMode.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("SaveAsMode");
            element.InnerText = SaveReportAsMode.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("SaveAsOnServer");
            element.InnerText = (SaveReportAs != null).ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ExitOnServer");
            element.InnerText = (Exit != null).ToString();
            category.AppendChild(element);

            element = doc.CreateElement("CreateReportOnServer");
            element.InnerText = (CreateReport != null).ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AutoSaveInterval");
            element.InnerText = AutoSaveInterval.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("RunWizardAfterLoad");
            element.InnerText = RunWizardAfterLoad.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("Title");
            element.InnerText = BrowserTitle;
            category.AppendChild(element);

            element = doc.CreateElement("ExitUrl");
            element.InnerText = ExitUrl;
            category.AppendChild(element);

            #region Appearance

            subcategory = doc.CreateElement("Appearance");
            category.AppendChild(subcategory);

            element = doc.CreateElement("GridSizeCentimetres");
            element.InnerText = GridSizeInCentimetres.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("GridSizeHundredthsOfInch");
            element.InnerText = GridSizeInHundredthsOfInches.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("GridSizeInch");
            element.InnerText = GridSizeInInches.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("GridSizeMillimeters");
            element.InnerText = GridSizeInMillimeters.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSaveFileDialog");
            element.InnerText = ShowSaveDialog.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowMoreDetailsButton");
            element.InnerText = ShowMoreDetailsButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("SetReportModifiedWhenOpening");
            element.InnerText = SetReportModifiedWhenOpening.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Database

            element = doc.CreateElement("Database");
            element.InnerXml = GetDataAdaptersList();
            category.AppendChild(element);

            #endregion

            #region Dictionary

            subcategory = doc.CreateElement("Dictionary");
            category.AppendChild(subcategory);

            element = doc.CreateElement("AllowModifyConnections");
            element.InnerText = AllowModifyConnections.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("AllowModifyDataSources");
            element.InnerText = AllowModifyDataSources.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("AllowModifyDictionary");
            element.InnerText = AllowModifyDictionary.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("AllowModifyRelations");
            element.InnerText = AllowModifyRelations.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("AllowModifyVariables");
            element.InnerText = AllowModifyVariables.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowConnectionType");
            element.InnerText = ShowConnectionType.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowActionsMenu");
            element.InnerText = ShowActionsMenu.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowTestConnectionButton");
            element.InnerText = ShowTestConnectionButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForBusinessObject");
            element.InnerText = ShowOnlyAliasForBusinessObjects.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForDatabase");
            element.InnerText = ShowOnlyAliasForConnections.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForDataSource");
            element.InnerText = ShowOnlyAliasForDataSources.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForDataRelation");
            element.InnerText = ShowOnlyAliasForDataRelations.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForDataColumn");
            element.InnerText = ShowOnlyAliasForDataColumns.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForVariable");
            element.InnerText = ShowOnlyAliasForVariables.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region MainMenu

            subcategory = doc.CreateElement("MainMenu");
            category.AppendChild(subcategory);

            element = doc.CreateElement("ShowClose");
            element.InnerText = ShowFileMenuClose.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExitButton");
            element.InnerText = (ShowFileMenuExit && (Exit != null || !string.IsNullOrEmpty(ExitUrl))).ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNew");
            element.InnerText = ShowFileMenuNew.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNewPage");
            element.InnerText = ShowFileMenuNewPage.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNewReport");
            element.InnerText = ShowFileMenuNewReport.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNewReportWithWizard");
            element.InnerText = ShowFileMenuNewReportWithWizard.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOpenReport");
            element.InnerText = ShowFileMenuOpen.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreview");
            element.InnerText = ShowFileMenuPreview.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviewAsPdf");
            element.InnerText = ShowFileMenuPreviewAsPdf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviewAsHtml");
            element.InnerText = ShowFileMenuPreviewAsHtml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviewAsXps");
            element.InnerText = ShowFileMenuPreviewAsXps.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSaveAs");
            element.InnerText = ShowFileMenuSaveAs.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSaveReport");
            element.InnerText = ShowFileMenuSave.ToString();
            subcategory.AppendChild(element);
            
            element = doc.CreateElement("Caption");
            element.InnerText = FileMenuCaption;
            subcategory.AppendChild(element);

            #endregion

            #region Toolbar

            subcategory = doc.CreateElement("Toolbar");
            category.AppendChild(subcategory);

            element = doc.CreateElement("ShowSelectLanguage");
            element.InnerText = ShowSelectLanguage.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowAboutButton");
            element.InnerText = ShowFileMenuAbout.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowCodeTab");
            element.InnerText = ShowCodeTab.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviewReportTab");
            element.InnerText = ShowPreviewReportTab.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowDictionaryTab");
            element.InnerText = ShowDictionaryTab.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowEventsTab");
            element.InnerText = ShowEventsTab.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Zoom

            element = doc.CreateElement("Zoom");
            element.InnerText = Zoom.ToString();
            subcategory.AppendChild(element);

            #endregion

            #endregion

            #region Viewer

            category = doc.CreateElement("Viewer");
            config.AppendChild(category);

            element = doc.CreateElement("DefaultEmail");
            element.InnerText = DefaultEmailAddress.ToString();
            category.AppendChild(element);

            #region Appearance

            subcategory = doc.CreateElement("Appearance");
            category.AppendChild(subcategory);

            element = doc.CreateElement("AutoHideScrollbars");
            element.InnerText = AutoHideScrollbars.ToString();
            subcategory.AppendChild(element);

            Color color = CurrentPageBorderColor;
            element = doc.CreateElement("CurrentPageBorderColor");
            element.InnerText = string.Format("{0}, {1}, {2}, {3}", color.A, color.R, color.G, color.B);
            subcategory.AppendChild(element);

            element = doc.CreateElement("OpenLinksTarget");
            element.InnerText = GetLinksTarget(OpenLinksWindow);
            subcategory.AppendChild(element);

            element = doc.CreateElement("VariablesPanelColumns");
            element.InnerText = ParametersPanelColumnsCount.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("VariablesPanelEditorWidth");
            element.InnerText = ParametersPanelEditorWidth.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowEmailDialog");
            element.InnerText = ShowEmailDialog.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowEmailExportDialog");
            element.InnerText = ShowEmailExportDialog.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Toolbar

            subcategory = doc.CreateElement("Toolbar");
            category.AppendChild(subcategory);

            element = doc.CreateElement("ShowMainToolbar");
            element.InnerText = ShowPreviewToolbar.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNavigateToolbar");
            element.InnerText = ShowPreviewNavigatePanel.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowViewModeToolbar");
            element.InnerText = ShowPreviewViewModePanel.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Buttons

            element = doc.CreateElement("ShowAboutButton");
            element.InnerText = "False";
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowBookmarksButton");
            element.InnerText = ShowPreviewBookmarksButton.ToString();
            subcategory.AppendChild(element);
            
            element = doc.CreateElement("ShowFindButton");
            element.InnerText = ShowPreviewFindButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowFirstPageButton");
            element.InnerText = ShowPreviewFirstPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowFullScreenButton");
            element.InnerText = ShowPreviewFullScreenButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowGoToPageButton");
            element.InnerText = ShowPreviewGoToPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowLastPageButton");
            element.InnerText = ShowPreviewLastPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNextPageButton");
            element.InnerText = ShowPreviewNextPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOpenButton");
            element.InnerText = ShowPreviewOpenButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeContinuousButton");
            element.InnerText = ShowPreviewContinuousPageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeMultipleButton");
            element.InnerText = ShowPreviewMultiplePageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeSingleButton");
            element.InnerText = ShowPreviewSinglePageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowParametersButton");
            element.InnerText = ShowPreviewParametersButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowParametersPanel");
            element.InnerText = ShowPreviewParametersButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviousPageButton");
            element.InnerText = ShowPreviewPreviousPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPrintButton");
            element.InnerText = ShowPreviewPrintButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSaveButton");
            element.InnerText = ShowPreviewSaveButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSendEMailButton");
            element.InnerText = ShowPreviewSendEmailButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowThumbnailsButton");
            element.InnerText = ShowPreviewThumbnailsButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowDesignButton");
            element.InnerText = "False";
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExitButton");
            element.InnerText = "False";
            subcategory.AppendChild(element);

            #endregion

            #region Zoom

            element = doc.CreateElement("ShowZoom");
            element.InnerText = ShowPreviewZoomButtons.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Exports

            subcategory = doc.CreateElement("Exports");
            category.AppendChild(subcategory);

            element = doc.CreateElement("ShowExportDialog");
            element.InnerText = ShowExportDialog.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDocument");
            element.InnerText = ShowExportToDocument.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPdf");
            element.InnerText = ShowExportToPdf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToXps");
            element.InnerText = ShowExportToXps.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPpt");
            element.InnerText = ShowExportToPowerPoint.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToHtml");
            element.InnerText = ShowExportToHtml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToMht");
            element.InnerText = ShowExportToMht.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToText");
            element.InnerText = ShowExportToText.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToRtf");
            element.InnerText = ShowExportToRtf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToWord2007");
            element.InnerText = ShowExportToWord2007.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToOpenDocumentWriter");
            element.InnerText = ShowExportToOpenDocumentWriter.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcel");
            element.InnerText = ShowExportToExcel.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcelXml");
            element.InnerText = ShowExportToExcelXml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcel2007");
            element.InnerText = ShowExportToExcel2007.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToOpenDocumentCalc");
            element.InnerText = ShowExportToOpenDocumentCalc.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToCsv");
            element.InnerText = ShowExportToCsv.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDbf");
            element.InnerText = ShowExportToDbf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToXml");
            element.InnerText = ShowExportToXml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDif");
            element.InnerText = ShowExportToDif.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSylk");
            element.InnerText = ShowExportToSylk.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToBmp");
            element.InnerText = ShowExportToImageBmp.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToGif");
            element.InnerText = ShowExportToImageGif.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToJpeg");
            element.InnerText = ShowExportToImageJpeg.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPcx");
            element.InnerText = ShowExportToImagePcx.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPng");
            element.InnerText = ShowExportToImagePng.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToTiff");
            element.InnerText = ShowExportToImageTiff.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToMetafile");
            element.InnerText = ShowExportToImageMetafile.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSvg");
            element.InnerText = ShowExportToImageSvg.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSvgz");
            element.InnerText = ShowExportToImageSvgz.ToString();
            subcategory.AppendChild(element);

            #endregion

            #endregion

            return StiGZipHelper.Pack(doc.OuterXml);
        }
    }
}
