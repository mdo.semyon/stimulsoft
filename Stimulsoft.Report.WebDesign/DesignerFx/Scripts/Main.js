function StiScriptsFx() {
    this.addEvent(window, 'beforeunload', function () {
        if (scriptsFx.sendCommand("IsReportModified") == "true") {
            var msg = "Are you sure you want to leave this page without saving the report?";
            if (typeof e == "undefined") e = window.event;
            if (e) e.returnValue = msg;
            return msg;
        }
    });
    this.getPrintFrame(true);
}

StiScriptsFx.prototype.changeTitle = function (title) {
    window.document.title = title;
}

// command: Open, Exit (for Viewer), Open, Save, Exit, ExitImmediately, IsReportModified (for Designer)
// id: web component id
// value: XML report template or snapshot for Open command
StiScriptsFx.prototype.sendCommand = function (command, id, value) {
    var flashMovie = document.getElementById(id + "_swf");
    if (!flashMovie) {
        var elems = document.getElementsByTagName("object");
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].id && elems[i].id.indexOf("_swf") > 0) {
                flashMovie = elems[i];
                break;
            }
        }
    }
    if (flashMovie && flashMovie.send) return flashMovie.send(command, value);
    return null;
}

StiScriptsFx.prototype.getNavigatorName = function () {
    if (!navigator) return "Unknown";
    var userAgent = navigator.userAgent;

    if (userAgent.indexOf("Edge") >= 0) return "Edge";
    if (userAgent.indexOf('MSIE') >= 0 || userAgent.indexOf('Trident') >= 0) return "MSIE";
    if (userAgent.indexOf('Gecko') >= 0) {
        if (userAgent.indexOf('Chrome') >= 0) return "Chrome";
        if (userAgent.indexOf('Safari') >= 0) return "Safari";
        return "Mozilla";
    }
    if (userAgent.indexOf('Opera') >= 0) return "Opera";
    return "Unknown";
}

StiScriptsFx.prototype.addEvent = function (element, eventName, fn) {
    if (element.addEventListener) element.addEventListener(eventName, fn, false);
    else if (element.attachEvent) element.attachEvent('on' + eventName, fn);
}

StiScriptsFx.prototype.getPrintFrame = function (createNew) {
    var printFrame = document.getElementById("stiPrintFrame");
    if (printFrame) {
        if (!createNew) return printFrame;
        document.body.removeChild(printFrame);
    }

    printFrame = document.createElement("iframe");
    printFrame.id = "stiPrintFrame";
    printFrame.name = "stiPrintFrame";
    printFrame.width = "0";
    printFrame.height = "0";
    printFrame.style.position = "absolute";
    printFrame.style.border = "none";
    document.body.appendChild(printFrame, document.body.firstChild);

    return printFrame;
}

StiScriptsFx.prototype.printAsHtml = function (text) {
    var printFrame = this.getPrintFrame(true);
    setTimeout(function () {
        printFrame.contentWindow.document.open();
        printFrame.contentWindow.document.write(text);
        printFrame.contentWindow.document.close();
        setTimeout(function () {
            printFrame.contentWindow.focus();
            printFrame.contentWindow.print();
        });
    });
}

StiScriptsFx.prototype.printAsPdf = function (dataBase64) {
    var data = window.atob(dataBase64);
    var array = Uint8Array.from(Array.prototype.map.call(data, function (x) { return x.charCodeAt(0); }));
    var blob = new Blob([array], { type: "application/pdf" });
    var url = URL.createObjectURL(blob);
    var printFrame = this.getPrintFrame(false);
    printFrame.src = url;
}

var scriptsFx = new StiScriptsFx();