#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Caching;
using Stimulsoft.Base;
using System.Globalization;

namespace Stimulsoft.Report.Web
{
    public partial class StiWebDesignerFx :
        WebControl,
        INamingContainer
    {
        #region Appearance
        
        private StiDesignerFxTheme theme = StiDesignerFxTheme.Office2013;
        /// <summary>
        /// Gets or sets the current visual theme which is used for drawing visual elements of the designer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiDesignerFxTheme.Office2013)]
        [Description("Gets or sets the current visual theme which is used for drawing visual elements of the designer.")]
        public StiDesignerFxTheme Theme
        {
            get
            {
                return theme;
            }
            set
            {
                theme = value;
            }
        }
        
        private string localization = string.Empty;
        /// <summary>
        /// Gets or sets a path to the localization file for the designer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets a path to the localization file for the designer.")]
        public string Localization
        {
            get
            {
                if (!IsDesignMode && string.IsNullOrEmpty(localization)) return "Default";
                return localization;
            }
            set
            {
                localization = value;
            }
        }

        private string localizationDirectory = string.Empty;
        /// <summary>
        /// Gets or sets a path to the localization files. These localizations are displayed on the toolbar of the designer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets a path to the localization files. These localizations are displayed on the toolbar of the designer.")]
        public string LocalizationDirectory
        {
            get
            {
                if (!IsDesignMode && string.IsNullOrEmpty(localizationDirectory)) return "Localization";
                return localizationDirectory;
            }
            set
            {
                if (IsDesignMode) localizationDirectory = value;
                else
                {
                    if (string.IsNullOrEmpty(value)) localizationDirectory = string.Empty;
                    else localizationDirectory = value;
                }
            }
        }

        private string aboutDialogTextLine1 = string.Empty;
        /// <summary>
        /// Gets or sets the text to display in the About dialog of the designer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets the text to display in the About dialog of the designer.")]
        public string AboutDialogTextLine1
        {
            get
            {
                return aboutDialogTextLine1;
            }
            set
            {
                aboutDialogTextLine1 = value;
            }
        }

        private string aboutDialogTextLine2 = string.Empty;
        /// <summary>
        /// Gets or sets the text to display in the About dialog of the designer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets the text to display in the About dialog of the designer.")]
        public string AboutDialogTextLine2
        {
            get
            {
                return aboutDialogTextLine2;
            }
            set
            {
                aboutDialogTextLine2 = value;
            }
        }

        private string aboutDialogUrl = string.Empty;
        /// <summary>
        /// Gets or sets the URL to display in the About dialog of the designer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets the URL to display in the About dialog of the designer.")]
        public string AboutDialogUrl
        {
            get
            {
                return aboutDialogUrl;
            }
            set
            {
                aboutDialogUrl = value;
            }
        }

        private string aboutDialogUrlText = string.Empty;
        /// <summary>
        /// Gets or sets the URL text to display in the About dialog of the designer.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Gets or sets the URL text to display in the About dialog of the designer.")]
        public string AboutDialogUrlText
        {
            get
            {
                return aboutDialogUrlText;
            }
            set
            {
                aboutDialogUrlText = value;
            }
        }

        private double gridSizeInCentimetres = 0.2;
        /// <summary>
        /// Gets or sets the report page grid size of the designer in centimetres.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(0.2)]
        [Description("Gets or sets the report page grid size of the designer in centimetres.")]
        public double GridSizeInCentimetres
        {
            get
            {
                return gridSizeInCentimetres;
            }
            set
            {
                gridSizeInCentimetres = value;
            }
        }

        private double gridSizeInHundredthsOfInches = 10;
        /// <summary>
        /// Gets or sets the report page grid size of the designer in hundredths of inches.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(10)]
        [Description("Gets or sets the report page grid size of the designer in hundredths of inches.")]
        public double GridSizeInHundredthsOfInches
        {
            get
            {
                return gridSizeInHundredthsOfInches;
            }
            set
            {
                gridSizeInHundredthsOfInches = value;
            }
        }

        private double gridSizeInInches = 0.1;
        /// <summary>
        /// Gets or sets the report page grid size of the designer in inches.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(0.1)]
        [Description("Gets or sets the report page grid size of the designer in inches.")]
        public double GridSizeInInches
        {
            get
            {
                return gridSizeInInches;
            }
            set
            {
                gridSizeInInches = value;
            }
        }

        private double gridSizeInMillimeters = 2;
        /// <summary>
        /// Gets or sets the report page grid size of the designer in millimeters.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(2)]
        [Description("Gets or sets the report page grid size of the designer in millimeters.")]
        public double GridSizeInMillimeters
        {
            get
            {
                return gridSizeInMillimeters;
            }
            set
            {
                gridSizeInMillimeters = value;
            }
        }

        private bool showTooltips = true;
        /// <summary>
        /// Gets or sets a value which indicates that show or hide tooltips.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that show or hide tooltips.")]
        public bool ShowTooltips
        {
            get
            {
                return showTooltips;
            }
            set
            {
                showTooltips = value;
            }
        }

        private bool showTooltipsHelp = true;
        /// <summary>
        /// Gets or sets a value which indicates that show or hide the help link in tooltips.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that show or hide the help link in tooltips.")]
        public bool ShowTooltipsHelp
        {
            get
            {
                return showTooltipsHelp;
            }
            set
            {
                showTooltipsHelp = value;
            }
        }

        private bool showDialogsHelp = true;
        /// <summary>
        /// Gets or sets a value which indicates that show or hide the help button in dialogs.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that show or hide the help button in dialogs.")]
        public bool ShowDialogsHelp
        {
            get
            {
                return showDialogsHelp;
            }
            set
            {
                showDialogsHelp = value;
            }
        }

        private bool showDialogsHints = true;
        /// <summary>
        /// Gets or sets a value which indicates that show or hide help tips in dialogs.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that show or hide help tips in dialogs.")]
        public bool ShowDialogsHints
        {
            get
            {
                return showDialogsHints;
            }
            set
            {
                showDialogsHints = value;
            }
        }

        private bool showMoreDetailsButton = true;
        /// <summary>
        /// Gets or sets the value that displays or hides the More Details button in the error dialog.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(true)]
        [Description("Gets or sets the value that displays or hides the More Details button in the error dialog.")]
        public bool ShowMoreDetailsButton
        {
            get
            {
                return showMoreDetailsButton;
            }
            set
            {
                showMoreDetailsButton = value;
            }
        }
        
        private bool showCancelButton = false;
        /// <summary>
        /// Gets or sets the value that displays or hides the Cancel button in the window of getting the data from the server.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that displays or hides the Cancel button in the window of getting the data from the server.")]
        public bool ShowCancelButton
        {
            get
            {
                return showCancelButton;
            }
            set
            {
                showCancelButton = value;
            }
        }
        
        private StiWMode flashWMode = StiWMode.Window;
        /// <summary>
        /// Gets or sets the Window Mode property of the SWF file for transparency, layering, positioning, and rendering in the browser.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiWMode.Window)]
        [Description("Gets or sets the Window Mode property of the SWF file for transparency, layering, positioning, and rendering in the browser.")]
        public StiWMode FlashWMode
        {
            get
            {
                return flashWMode;
            }
            set
            {
                flashWMode = value;
            }
        }
        
        private string dateFormat = StiDateFormatMode.FromClient;
        /// <summary>
        /// Gets or sets a date format for datetime values in designer. To use a server date format please set the StiDateFormatMode.FromServer or "FromServer" string value.
        /// The default is the client system date format.
        /// </summary>
        [Category("Appearance")]
        [Description("Gets or sets a date format for datetime values in designer. To use a server date format please set the StiDateFormatMode.FromServer or \"FromServer\" string value. The default is the client system date format.")]
        public string DateFormat
        {
            get
            {
                if (dateFormat == StiDateFormatMode.FromServer) return CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern + " " + CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern;
                return dateFormat;
            }
            set
            {
                dateFormat = value;
            }
        }
        
        /// <summary>
        /// Gets or sets an images quality for the report components such as Rich Text, Rotated Text, Charts, Bar Codes.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiImagesQuality.Normal)]
        [Description("Gets or sets an images quality for the report components such as Rich Text, Rotated Text, Charts, Bar Codes.")]
        public StiImagesQuality ImagesQuality
        {
            get
            {
                return StiReportHelperFx.ImagesQuality;
            }
            set
            {
                StiReportHelperFx.ImagesQuality = value;
            }
        }
        
        private string browserTitle = string.Empty;
        /// <summary>
        /// The designer window title in full screen moode.
        /// </summary>
        [Category("Appearance")]
        [Description("The designer window title in full screen moode.")]
        public string BrowserTitle
        {
            get
            {
                return browserTitle;
            }
            set
            {
                browserTitle = value;
            }
        }

        private bool autoHideScrollbars = false;
        /// <summary>
        /// Gets or sets the value that allows automatically hide report scrollbars in preview if not required.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that allows automatically hide report scrollbars in preview if not required.")]
        public bool AutoHideScrollbars
        {
            get
            {
                return autoHideScrollbars;
            }
            set
            {
                autoHideScrollbars = value;
            }
        }

        private Color currentPageBorderColor = Color.Gold;
        /// <summary>
        /// Gets or sets a color of the border of the current report page in preview.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(typeof(Color), "Gold")]
        [Description("Gets or sets a color of the border of the current report page in preview.")]
        [TypeConverter(typeof(WebColorConverter))]
        public Color CurrentPageBorderColor
        {
            get
            {
                return currentPageBorderColor;
            }
            set
            {
                currentPageBorderColor = value;
            }
        }

        private string openLinksWindow = StiTargetWindow.Blank;
        /// <summary>
        /// Gets or sets a browser window to open links from the report.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiTargetWindow.Blank)]
        [Description("Gets or sets a browser window to open links from the report.")]
        public string OpenLinksWindow
        {
            get
            {
                return openLinksWindow;
            }
            set
            {
                openLinksWindow = value;
            }
        }

        private string openExportedReportWindow = StiTargetWindow.Blank;
        /// <summary>
        /// Gets or sets a browser window to open the exported report.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(StiTargetWindow.Blank)]
        [Description("Gets or sets a browser window to open the exported report.")]
        public string OpenExportedReportWindow
        {
            get
            {
                return openExportedReportWindow;
            }
            set
            {
                openExportedReportWindow = value;
            }
        }

        private int parametersPanelColumnsCount = 2;
        /// <summary>
        /// Gets or sets a count columns in preview parameters panel.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(2)]
        [Description("Gets or sets a count columns in preview parameters panel.")]
        public int ParametersPanelColumnsCount
        {
            get
            {
                return parametersPanelColumnsCount;
            }
            set
            {
                parametersPanelColumnsCount = value;
            }
        }

        private int parametersPanelEditorWidth = 200;
        /// <summary>
        /// Gets or sets a editor width in preview parameters panel.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(200)]
        [Description("Gets or sets a editor width in preview parameters panel.")]
        public int ParametersPanelEditorWidth
        {
            get
            {
                return parametersPanelEditorWidth;
            }
            set
            {
                parametersPanelEditorWidth = value;
            }
        }

        #endregion

        #region Behaviour

        private StiSaveMode saveReportMode = StiSaveMode.Hidden;
        /// <summary>
        /// Gets or sets the save report mode - Hidden (AJAX mode), Visible (POST mode) or New Window.
        /// </summary>
        [Category("Behaviour")]
        [DefaultValue(StiSaveMode.Hidden)]
        [Description("Gets or sets the save report mode - Hidden (AJAX mode), Visible (POST mode) or New Window.")]
        public StiSaveMode SaveReportMode
        {
            get
            {
                return saveReportMode;
            }
            set
            {
                saveReportMode = value;
            }
        }

        private StiSaveMode saveReportAsMode = StiSaveMode.Hidden;
        /// <summary>
        /// Gets or sets the save report mode - Hidden (AJAX mode), Visible (POST mode) or New Window.
        /// </summary>
        [Category("Behaviour")]
        [DefaultValue(StiSaveMode.Hidden)]
        [Description("Gets or sets the save report mode - Hidden (AJAX mode), Visible (POST mode) or New Window.")]
        public StiSaveMode SaveReportAsMode
        {
            get
            {
                return saveReportAsMode;
            }
            set
            {
                saveReportAsMode = value;
            }
        }

        private bool runWizardAfterLoad = false;
        /// <summary>
        /// Gets or sets the value that displays the new report wizard at designer startup.
        /// </summary>
        [Category("Behaviour")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that displays the new report wizard at designer startup.")]
        public bool RunWizardAfterLoad
        {
            get
            {
                return runWizardAfterLoad;
            }
            set
            {
                runWizardAfterLoad = value;
            }
        }

        private bool showSaveDialog = false;
        /// <summary>
        /// Gets or sets the value that displays or hides the report name dialog at saving.
        /// </summary>
        [Category("Behaviour")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that displays or hides the report name dialog at saving.")]
        public bool ShowSaveDialog
        {
            get
            {
                return showSaveDialog;
            }
            set
            {
                showSaveDialog = value;
            }
        }
        
        private bool setReportModifiedWhenOpening = false;
        /// <summary>
        /// Gets or sets the value that marks a report as modified when it is opened.
        /// </summary>
        [Category("Behaviour")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that marks a report as modified when it is opened.")]
        public bool SetReportModifiedWhenOpening
        {
            get
            {
                return setReportModifiedWhenOpening;
            }
            set
            {
                setReportModifiedWhenOpening = value;
            }
        }

        private int autoSaveInterval = 0;
        /// <summary>
        /// Gets or sets an auto save interval of the report in minutes. The default value is 0 (auto save feature is disabled).
        /// </summary>
        [Category("Behaviour")]
        [DefaultValue(0)]
        [Description("Gets or sets an auto save interval of the report in minutes. The default value is 0 (auto save feature is disabled).")]
        public int AutoSaveInterval
        {
            get
            {
                return autoSaveInterval;
            }
            set
            {
                autoSaveInterval = value;
            }
        }

        private string designerEventFunction = string.Empty;
        /// <summary>
        /// Gets or sets the name of the JavaScript function that will be called by the designer when performing actions.
        /// </summary>
        [Category("Behaviour")]
        [DefaultValue("")]
        [Description("Gets or sets the name of the JavaScript function that will be called by the designer when performing actions.")]
        public string DesignerEventFunction
        {
            get
            {
                return designerEventFunction;
            }
            set
            {
                designerEventFunction = value;
            }
        }
        
        private string exitUrl = string.Empty;
        /// <summary>
        /// The URL address, by which transfer will be done when clicking the Exit button in the main menu.
        /// </summary>
        [Category("Behaviour")]
        [DefaultValue("")]
        [Description("The URL address, by which transfer will be done when clicking the Exit button in the main menu.")]
        public string ExitUrl
        {
            get
            {
                return exitUrl;
            }
            set
            {
                exitUrl = value;
            }
        }

        #endregion

        #region Dictionary

        private bool allowModifyDictionary = true;
        /// <summary>
        /// Gets or sets the value that enables or disables the ability to modify the dictionary of the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(true)]
        [Description("Gets or sets the value that enables or disables the ability to modify the dictionary of the designer.")]
        public bool AllowModifyDictionary
        {
            get
            {
                return allowModifyDictionary;
            }
            set
            {
                allowModifyDictionary = value;
            }
        }

        private bool allowModifyConnections = true;
        /// <summary>
        /// Gets or sets the value that enables or disables the ability to modify connections in the dictionary of the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(true)]
        [Description("Gets or sets the value that enables or disables the ability to modify connections in the dictionary of the designer.")]
        public bool AllowModifyConnections
        {
            get
            {
                return allowModifyConnections;
            }
            set
            {
                allowModifyConnections = value;
            }
        }

        private bool allowModifyDataSources = true;
        /// <summary>
        /// Gets or sets the value that enables or disables the ability to modify data sources in the dictionary of the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(true)]
        [Description("Gets or sets the value that enables or disables the ability to modify data sources in the dictionary of the designer.")]
        public bool AllowModifyDataSources
        {
            get
            {
                return allowModifyDataSources;
            }
            set
            {
                allowModifyDataSources = value;
            }
        }

        private bool allowModifyRelations = true;
        /// <summary>
        /// Gets or sets the value that enables or disables the ability to modify relations in the dictionary of the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(true)]
        [Description("Gets or sets the value that enables or disables the ability to modify relations in the dictionary of the designer.")]
        public bool AllowModifyRelations
        {
            get
            {
                return allowModifyRelations;
            }
            set
            {
                allowModifyRelations = value;
            }
        }

        private bool allowModifyVariables = true;
        /// <summary>
        /// Gets or sets the value that enables or disables the ability to modify variables in the dictionary of the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(true)]
        [Description("Gets or sets the value that enables or disables the ability to modify variables in the dictionary of the designer.")]
        public bool AllowModifyVariables
        {
            get
            {
                return allowModifyVariables;
            }
            set
            {
                allowModifyVariables = value;
            }
        }

        private bool showConnectionType = true;
        /// <summary>
        /// Gets or sets the value that displays or hides the connection type in the dictionary of the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(true)]
        [Description("Gets or sets the value that displays or hides the connection type in the dictionary of the designer.")]
        public bool ShowConnectionType
        {
            get
            {
                return showConnectionType;
            }
            set
            {
                showConnectionType = value;
            }
        }

        private bool showActionsMenu = true;
        /// <summary>
        /// Gets or sets the value that displays or hides the actions menu in the dictionary of the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(true)]
        [Description("Gets or sets the value that displays or hides the actions menu in the dictionary of the designer.")]
        public bool ShowActionsMenu
        {
            get
            {
                return showActionsMenu;
            }
            set
            {
                showActionsMenu = value;
            }
        }

        private bool showTestConnectionButton = true;
        /// <summary>
        /// Gets or sets the value that displays or hides the test connection button in the connection dialog.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(true)]
        [Description("Gets or sets the value that displays or hides the test connection button in the connection dialog.")]
        public bool ShowTestConnectionButton
        {
            get
            {
                return showTestConnectionButton;
            }
            set
            {
                showTestConnectionButton = value;
            }
        }

        private bool showOnlyAliasForBusinessObjects = false;
        /// <summary>
        /// Gets or sets the value that displays or hides the alias for bisiness objects in the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that displays or hides the alias for bisiness objects in the designer.")]
        public bool ShowOnlyAliasForBusinessObjects
        {
            get
            {
                return showOnlyAliasForBusinessObjects;
            }
            set
            {
                showOnlyAliasForBusinessObjects = value;
            }
        }

        private bool showOnlyAliasForConnections = false;
        /// <summary>
        /// Gets or sets the value that displays or hides the alias for connections in the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that displays or hides the alias for connections in the designer.")]
        public bool ShowOnlyAliasForConnections
        {
            get
            {
                return showOnlyAliasForConnections;
            }
            set
            {
                showOnlyAliasForConnections = value;
            }
        }

        private bool showOnlyAliasForDataSources = false;
        /// <summary>
        /// Gets or sets the value that displays or hides the alias for data sources in the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that displays or hides the alias for data sources in the designer.")]
        public bool ShowOnlyAliasForDataSources
        {
            get
            {
                return showOnlyAliasForDataSources;
            }
            set
            {
                showOnlyAliasForDataSources = value;
            }
        }

        private bool showOnlyAliasForDataRelations = false;
        /// <summary>
        /// Gets or sets the value that displays or hides the alias for data relations in the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that displays or hides the alias for data relations in the designer.")]
        public bool ShowOnlyAliasForDataRelations
        {
            get
            {
                return showOnlyAliasForDataRelations;
            }
            set
            {
                showOnlyAliasForDataRelations = value;
            }
        }

        private bool showOnlyAliasForDataColumns = false;
        /// <summary>
        /// Gets or sets the value that displays or hides the alias for data columns in the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that displays or hides the alias for data columns in the designer.")]
        public bool ShowOnlyAliasForDataColumns
        {
            get
            {
                return showOnlyAliasForDataColumns;
            }
            set
            {
                showOnlyAliasForDataColumns = value;
            }
        }

        private bool showOnlyAliasForVariables = false;
        /// <summary>
        /// Gets or sets the value that displays or hides the alias for variables in the designer.
        /// </summary>
        [Category("Dictionary")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that displays or hides the alias for variables in the designer.")]
        public bool ShowOnlyAliasForVariables
        {
            get
            {
                return showOnlyAliasForVariables;
            }
            set
            {
                showOnlyAliasForVariables = value;
            }
        }

        #endregion

        #region Email
        
        private bool showEmailDialog = true;
        /// <summary>
        /// Gets or sets a value which allows to display the Email dialog, or send Email with the default settings.
        /// </summary>
        [Category("Email")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows to display the Email dialog, or send Email with the default settings.")]
        public bool ShowEmailDialog
        {
            get
            {
                return showEmailDialog;
            }
            set
            {
                showEmailDialog = value;
            }
        }
        
        private bool showEmailExportDialog = true;
        /// <summary>
        /// Gets or sets a value which allows to display the export dialog for Email, or export report for Email with the default settings.
        /// </summary>
        [Category("Email")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows to display the export dialog for Email, or export report for Email with the default settings.")]
        public bool ShowEmailExportDialog
        {
            get
            {
                return showEmailExportDialog;
            }
            set
            {
                showEmailExportDialog = value;
            }
        }
        
        private string defaultEmailAddress = string.Empty;
        /// <summary>
        /// Gets or sets the default email address of the message created in the preview.
        /// </summary>
        [Category("Email")]
        [DefaultValue("")]
        [Description("Gets or sets the default email address of the message created in the preview.")]
        public string DefaultEmailAddress
        {
            get
            {
                return defaultEmailAddress;
            }
            set
            {
                defaultEmailAddress = value;
            }
        }

        #endregion

        #region Exports

        private bool showExportDialog = true;
        /// <summary>
        /// Gets or sets a value which allows to display the export dialog, or to export with the default settings.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows to display the export dialog, or to export with the default settings.")]
        public bool ShowExportDialog
        {
            get
            {
                return showExportDialog;
            }
            set
            {
                showExportDialog = value;
            }
        }
        
        private bool showExportToDocument = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the report document file.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the report document file.")]
        public bool ShowExportToDocument
        {
            get
            {
                return showExportToDocument;
            }
            set
            {
                showExportToDocument = value;
            }
        }
        
        private bool showExportToPdf = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the PDF format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the PDF format.")]
        public bool ShowExportToPdf
        {
            get
            {
                return showExportToPdf;
            }
            set
            {
                showExportToPdf = value;
            }
        }
        
        private bool showExportToXps = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the XPS format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the XPS format.")]
        public bool ShowExportToXps
        {
            get
            {
                return showExportToXps;
            }
            set
            {
                showExportToXps = value;
            }
        }
        
        private bool showExportToPowerPoint = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the Power Point 2007-2010 format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the Power Point 2007-2010 format.")]
        public bool ShowExportToPowerPoint
        {
            get
            {
                return showExportToPowerPoint;
            }
            set
            {
                showExportToPowerPoint = value;
            }
        }
        
        private bool showExportToHtml = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the HTML format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the HTML format.")]
        public bool ShowExportToHtml
        {
            get
            {
                return showExportToHtml;
            }
            set
            {
                showExportToHtml = value;
            }
        }
        
        private bool showExportToHtml5 = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the HTML5 format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the HTML5 format.")]
        public bool ShowExportToHtml5
        {
            get
            {
                return showExportToHtml5;
            }
            set
            {
                showExportToHtml5 = value;
            }
        }
        
        private bool showExportToMht = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the MHT (Web Archive) format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the MHT (Web Archive) format.")]
        public bool ShowExportToMht
        {
            get
            {
                return showExportToMht;
            }
            set
            {
                showExportToMht = value;
            }
        }
        
        private bool showExportToText = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the TEXT format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the TEXT format.")]
        public bool ShowExportToText
        {
            get
            {
                return showExportToText;
            }
            set
            {
                showExportToText = value;
            }
        }
        
        private bool showExportToRtf = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the Rich Text format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the Rich Text format.")]
        public bool ShowExportToRtf
        {
            get
            {
                return showExportToRtf;
            }
            set
            {
                showExportToRtf = value;
            }
        }
        
        private bool showExportToWord2007 = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the Word 2007-2010 format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the Word 2007-2010 format.")]
        public bool ShowExportToWord2007
        {
            get
            {
                return showExportToWord2007;
            }
            set
            {
                showExportToWord2007 = value;
            }
        }
        
        private bool showExportToOpenDocumentWriter = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the Open Document Text format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the Open Document Text format.")]
        public bool ShowExportToOpenDocumentWriter
        {
            get
            {
                return showExportToOpenDocumentWriter;
            }
            set
            {
                showExportToOpenDocumentWriter = value;
            }
        }
        
        private bool showExportToExcel = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the Excel BIFF format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the Excel BIFF format.")]
        public bool ShowExportToExcel
        {
            get
            {
                return showExportToExcel;
            }
            set
            {
                showExportToExcel = value;
            }
        }
        
        private bool showExportToExcelXml = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the Excel XML format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the Excel XML format.")]
        public bool ShowExportToExcelXml
        {
            get
            {
                return showExportToExcelXml;
            }
            set
            {
                showExportToExcelXml = value;
            }
        }
        
        private bool showExportToExcel2007 = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the Excel 2007-2010 format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the Excel 2007-2010 format.")]
        public bool ShowExportToExcel2007
        {
            get
            {
                return showExportToExcel2007;
            }
            set
            {
                showExportToExcel2007 = value;
            }
        }
        
        private bool showExportToOpenDocumentCalc = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the Open Document Calc format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the Open Document Calc format.")]
        public bool ShowExportToOpenDocumentCalc
        {
            get
            {
                return showExportToOpenDocumentCalc;
            }
            set
            {
                showExportToOpenDocumentCalc = value;
            }
        }
        
        private bool showExportToCsv = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the CSV format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the CSV format.")]
        public bool ShowExportToCsv
        {
            get
            {
                return showExportToCsv;
            }
            set
            {
                showExportToCsv = value;
            }
        }
        
        private bool showExportToDbf = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the DBF format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the DBF format.")]
        public bool ShowExportToDbf
        {
            get
            {
                return showExportToDbf;
            }
            set
            {
                showExportToDbf = value;
            }
        }
        
        private bool showExportToXml = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the XML format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the XML format.")]
        public bool ShowExportToXml
        {
            get
            {
                return showExportToXml;
            }
            set
            {
                showExportToXml = value;
            }
        }
        
        private bool showExportToDif = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the DIF format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the DIF format.")]
        public bool ShowExportToDif
        {
            get
            {
                return showExportToDif;
            }
            set
            {
                showExportToDif = value;
            }
        }
        
        private bool showExportToSylk = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the Sylk format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the Sylk format.")]
        public bool ShowExportToSylk
        {
            get
            {
                return showExportToSylk;
            }
            set
            {
                showExportToSylk = value;
            }
        }
        
        private bool showExportToImageBmp = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the BMP image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the BMP image format.")]
        public bool ShowExportToImageBmp
        {
            get
            {
                return showExportToImageBmp;
            }
            set
            {
                showExportToImageBmp = value;
            }
        }
        
        private bool showExportToImageGif = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the GIF image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the GIF image format.")]
        public bool ShowExportToImageGif
        {
            get
            {
                return showExportToImageGif;
            }
            set
            {
                showExportToImageGif = value;
            }
        }
        
        private bool showExportToImageJpeg = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the JPEG image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the JPEG image format.")]
        public bool ShowExportToImageJpeg
        {
            get
            {
                return showExportToImageJpeg;
            }
            set
            {
                showExportToImageJpeg = value;
            }
        }
        
        private bool showExportToImagePcx = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the PCX image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the PCX image format.")]
        public bool ShowExportToImagePcx
        {
            get
            {
                return showExportToImagePcx;
            }
            set
            {
                showExportToImagePcx = value;
            }
        }
        
        private bool showExportToImagePng = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the PNG image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the PNG image format.")]
        public bool ShowExportToImagePng
        {
            get
            {
                return showExportToImagePng;
            }
            set
            {
                showExportToImagePng = value;
            }
        }
        
        private bool showExportToImageTiff = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the TIFF image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the TIFF image format.")]
        public bool ShowExportToImageTiff
        {
            get
            {
                return showExportToImageTiff;
            }
            set
            {
                showExportToImageTiff = value;
            }
        }
        
        private bool showExportToImageMetafile = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the Metafile image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the Metafile image format.")]
        public bool ShowExportToImageMetafile
        {
            get
            {
                return showExportToImageMetafile;
            }
            set
            {
                showExportToImageMetafile = value;
            }
        }
        
        private bool showExportToImageSvg = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the SVG image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the SVG image format.")]
        public bool ShowExportToImageSvg
        {
            get
            {
                return showExportToImageSvg;
            }
            set
            {
                showExportToImageSvg = value;
            }
        }
        
        private bool showExportToImageSvgz = true;
        /// <summary>
        /// Gets or sets a value which indicates that the user can save the report from the preview to the SVGZ image format.
        /// </summary>
        [Category("Exports")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the user can save the report from the preview to the SVGZ image format.")]
        public bool ShowExportToImageSvgz
        {
            get
            {
                return showExportToImageSvgz;
            }
            set
            {
                showExportToImageSvgz = value;
            }
        }

        #endregion

        #region File Menu

        private bool showFileMenuNew = true;
        /// <summary>
        /// Gets or sets a visibility of the New item in the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the New item in the file menu.")]
        public bool ShowFileMenuNew
        {
            get
            {
                return showFileMenuNew;
            }
            set
            {
                showFileMenuNew = value;
            }
        }

        private bool showFileMenuNewReport = true;
        /// <summary>
        /// Gets or sets a visibility of the New Report item in the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the New Report item in the file menu.")]
        public bool ShowFileMenuNewReport
        {
            get
            {
                return showFileMenuNewReport;
            }
            set
            {
                showFileMenuNewReport = value;
            }
        }

        private bool showFileMenuNewReportWithWizard = true;
        /// <summary>
        /// Gets or sets a visibility of the New Report with Wizard item in the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the New Report with Wizard item in the file menu.")]
        public bool ShowFileMenuNewReportWithWizard
        {
            get
            {
                return showFileMenuNewReportWithWizard;
            }
            set
            {
                showFileMenuNewReportWithWizard = value;
            }
        }

        private bool showFileMenuNewPage = true;
        /// <summary>
        /// Gets or sets a visibility of the New Page item in the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the New Page item in the file menu.")]
        public bool ShowFileMenuNewPage
        {
            get
            {
                return showFileMenuNewPage;
            }
            set
            {
                showFileMenuNewPage = value;
            }
        }

        private bool showFileMenuOpen = true;
        /// <summary>
        /// Gets or sets a visibility of the Open Report item in the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Open Report item in the file menu.")]
        public bool ShowFileMenuOpen
        {
            get
            {
                return showFileMenuOpen;
            }
            set
            {
                showFileMenuOpen = value;
            }
        }

        private bool showFileMenuSave = true;
        /// <summary>
        /// Gets or sets a visibility of the Save Report item in the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Save Report item in the file menu.")]
        public bool ShowFileMenuSave
        {
            get
            {
                return showFileMenuSave;
            }
            set
            {
                showFileMenuSave = value;
            }
        }

        private bool showFileMenuSaveAs = true;
        /// <summary>
        /// Gets or sets a visibility of the Save Report As item in the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Save Report As item in the file menu.")]
        public bool ShowFileMenuSaveAs
        {
            get
            {
                return showFileMenuSaveAs;
            }
            set
            {
                showFileMenuSaveAs = value;
            }
        }

        private bool showFileMenuPreview = true;
        /// <summary>
        /// Gets or sets a visibility of the Preview item in the file menu. Only for Office2007 theme.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Preview item in the file menu. Only for Office2007 theme.")]
        public bool ShowFileMenuPreview
        {
            get
            {
                return showFileMenuPreview;
            }
            set
            {
                showFileMenuPreview = value;
            }
        }

        private bool showFileMenuPreviewAsPdf = true;
        /// <summary>
        /// Gets or sets a visibility of the Preview as PDF item in the file menu. Only for Office2007 theme.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Preview as PDF item in the file menu. Only for Office2007 theme.")]
        public bool ShowFileMenuPreviewAsPdf
        {
            get
            {
                return showFileMenuPreviewAsPdf;
            }
            set
            {
                showFileMenuPreviewAsPdf = value;
            }
        }

        private bool showFileMenuPreviewAsHtml = true;
        /// <summary>
        /// Gets or sets a visibility of the Preview as HTML item in the file menu. Only for Office2007 theme.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Preview as HTML item in the file menu. Only for Office2007 theme.")]
        public bool ShowFileMenuPreviewAsHtml
        {
            get
            {
                return showFileMenuPreviewAsHtml;
            }
            set
            {
                showFileMenuPreviewAsHtml = value;
            }
        }

        private bool showFileMenuPreviewAsXps = true;
        /// <summary>
        /// Gets or sets a visibility of the Preview as XPS item in the file menu. Only for Office2007 theme.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Preview as XPS item in the file menu. Only for Office2007 theme.")]
        public bool ShowFileMenuPreviewAsXps
        {
            get
            {
                return showFileMenuPreviewAsXps;
            }
            set
            {
                showFileMenuPreviewAsXps = value;
            }
        }

        private bool showFileMenuClose = true;
        /// <summary>
        /// Gets or sets a visibility of the Close item in the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Close item in the file menu.")]
        public bool ShowFileMenuClose
        {
            get
            {
                return showFileMenuClose;
            }
            set
            {
                showFileMenuClose = value;
            }
        }

        private bool showFileMenuAbout = true;
        /// <summary>
        /// Gets or sets a visibility of the About item in the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the About item in the file menu.")]
        public bool ShowFileMenuAbout
        {
            get
            {
                return showFileMenuAbout;
            }
            set
            {
                showFileMenuAbout = value;
            }
        }

        private bool showFileMenuExit = true;
        /// <summary>
        /// Gets or sets a visibility of the Exit item in the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Exit item in the file menu.")]
        public bool ShowFileMenuExit
        {
            get
            {
                return showFileMenuExit;
            }
            set
            {
                showFileMenuExit = value;
            }
        }

        private string fileMenuCaption = string.Empty;
        /// <summary>
        /// Gets or sets the text to display in the top of the file menu.
        /// </summary>
        [Category("FileMenu")]
        [DefaultValue("")]
        [Description("Gets or sets the text to display in the top of the file menu.")]
        public string FileMenuCaption
        {
            get
            {
                if (!IsDesignMode && string.IsNullOrEmpty(fileMenuCaption)) return "Default";
                return fileMenuCaption;
            }
            set
            {
                fileMenuCaption = value;
            }
        }

        #endregion

        #region Print
        
        private bool showPrintDialog = true;
        /// <summary>
        /// Gets or sets a value which allows to display the Print dialog, or print report with the default settings.
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows to display the Print dialog, or print report with the default settings.")]
        public bool ShowPrintDialog
        {
            get
            {
                return showPrintDialog;
            }
            set
            {
                showPrintDialog = value;
            }
        }
        
        private bool autoPageOrientation = true;
        /// <summary>
        /// Gets or sets a value which allows the automatic page orientation when printing the report.
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows the automatic page orientation when printing the report.")]
        public bool AutoPageOrientation
        {
            get
            {
                return autoPageOrientation;
            }
            set
            {
                autoPageOrientation = value;
            }
        }
        
        private bool autoPageScale = true;
        /// <summary>
        /// Gets or sets a value which allows the automatic page scale when printing the report.
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows the automatic page scale when printing the report.")]
        public bool AutoPageScale
        {
            get
            {
                return autoPageScale;
            }
            set
            {
                autoPageScale = value;
            }
        }
        
        private bool allowDefaultPrint = true;
        /// <summary>
        /// Gets or sets a value which allows Default printing (by Flash engine).
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows Default printing (by Flash engine).")]
        public bool AllowDefaultPrint
        {
            get
            {
                return allowDefaultPrint;
            }
            set
            {
                allowDefaultPrint = value;
            }
        }
        
        private bool allowPrintToPdf = true;
        /// <summary>
        /// Gets or sets a value which allows printing in PDF format.
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows printing in PDF format.")]
        public bool AllowPrintToPdf
        {
            get
            {
                return allowPrintToPdf;
            }
            set
            {
                allowPrintToPdf = value;
            }
        }
        
        private bool allowPrintToHtml = true;
        /// <summary>
        /// Gets or sets a value which allows printing in HTML format.
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows printing in HTML format.")]
        public bool AllowPrintToHtml
        {
            get
            {
                return allowPrintToHtml;
            }
            set
            {
                allowPrintToHtml = value;
            }
        }
        
        private bool printAsBitmap = true;
        /// <summary>
        /// Gets or sets a value which allows send the report to print as Image (only for Default format).
        /// </summary>
        [Category("Print")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which allows send the report to print as Image (only for Default format).")]
        public bool PrintAsBitmap
        {
            get
            {
                return printAsBitmap;
            }
            set
            {
                printAsBitmap = value;
            }
        }

        #endregion

        #region Toolbar

        private bool showSelectLanguage = true;
        /// <summary>
        /// Gets or sets a visibility of the Select Language control of the designer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Select Language control of the designer.")]
        public bool ShowSelectLanguage
        {
            get
            {
                return showSelectLanguage;
            }
            set
            {
                showSelectLanguage = value;
            }
        }

        private bool showCodeTab = false;
        /// <summary>
        /// Gets or sets a visibility of the Code Tab of the designer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(false)]
        [Description("Gets or sets a visibility of the Code Tab of the designer.")]
        public bool ShowCodeTab
        {
            get
            {
                return showCodeTab;
            }
            set
            {
                showCodeTab = value;
            }
        }

        private bool showPreviewReportTab = true;
        /// <summary>
        /// Gets or sets a visibility of the Preview Report Tab of the designer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Preview Report Tab of the designer.")]
        public bool ShowPreviewReportTab
        {
            get
            {
                return showPreviewReportTab;
            }
            set
            {
                showPreviewReportTab = value;
            }
        }

        private bool showDictionaryTab = true;
        /// <summary>
        /// Gets or sets a visibility of the Dictionary Tab of the designer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Dictionary Tab of the designer.")]
        public bool ShowDictionaryTab
        {
            get
            {
                return showDictionaryTab;
            }
            set
            {
                showDictionaryTab = value;
            }
        }

        private bool showEventsTab = true;
        /// <summary>
        /// Gets or sets a visibility of the Events Tab of the designer.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Events Tab of the designer.")]
        public bool ShowEventsTab
        {
            get
            {
                return showEventsTab;
            }
            set
            {
                showEventsTab = value;
            }
        }

        private int zoom = StiZoomModeFx.Default;
        /// <summary>
        /// Gets or sets the report page zoom.
        /// </summary>
        [Category("Toolbar")]
        [DefaultValue(StiZoomModeFx.Default)]
        [Description("Gets or sets the report page zoom.")]
        public int Zoom
        {
            get
            {
                return zoom;
            }
            set
            {
                if (value > 500) zoom = 500;
                if (value < 0 || (value < 10 && value > 4)) zoom = 10;
                else zoom = value;
            }
        }

        #endregion

        #region Preview Toolbar

        private bool showPreviewToolbar = true;
        /// <summary>
        /// Gets or sets a value which indicates that toolbar will be shown in the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that toolbar will be shown in the preview.")]
        public bool ShowPreviewToolbar
        {
            get
            {
                return showPreviewToolbar;
            }
            set
            {
                showPreviewToolbar = value;
            }
        }

        private bool showPreviewNavigatePanel = true;
        /// <summary>
        /// Gets or sets a value which indicates that navigate panel will be shown in the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that navigate panel will be shown in the preview.")]
        public bool ShowPreviewNavigatePanel
        {
            get
            {
                return showPreviewNavigatePanel;
            }
            set
            {
                showPreviewNavigatePanel = value;
            }
        }
        
        private bool showPreviewViewModePanel = true;
        /// <summary>
        /// Gets or sets a value which indicates that view mode panel will be shown in the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that view mode panel will be shown in the preview.")]
        public bool ShowPreviewViewModePanel
        {
            get
            {
                return showPreviewViewModePanel;
            }
            set
            {
                showPreviewViewModePanel = value;
            }
        }
        
        private bool showPreviewPrintButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Print button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Print button in the toolbar of the preview.")]
        public bool ShowPreviewPrintButton
        {
            get
            {
                return showPreviewPrintButton;
            }
            set
            {
                showPreviewPrintButton = value;
            }
        }
        
        private bool showPreviewOpenButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Open button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Open button in the toolbar of the preview.")]
        public bool ShowPreviewOpenButton
        {
            get
            {
                return showPreviewOpenButton;
            }
            set
            {
                showPreviewOpenButton = value;
            }
        }
        
        private bool showPreviewSaveButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Save button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Save button in the toolbar of the preview.")]
        public bool ShowPreviewSaveButton
        {
            get
            {
                return showPreviewSaveButton;
            }
            set
            {
                showPreviewSaveButton = value;
            }
        }
        
        private bool showPreviewSendEmailButton = false;
        /// <summary>
        /// Gets or sets a visibility of the Send Email button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(false)]
        [Description("Gets or sets a visibility of the Send Email button in the toolbar of the preview.")]
        public bool ShowPreviewSendEmailButton
        {
            get
            {
                return showPreviewSendEmailButton;
            }
            set
            {
                showPreviewSendEmailButton = value;
            }
        }
        
        private bool showPreviewBookmarksButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Bookmarks button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Bookmarks button in the toolbar of the preview.")]
        public bool ShowPreviewBookmarksButton
        {
            get
            {
                return showPreviewBookmarksButton;
            }
            set
            {
                showPreviewBookmarksButton = value;
            }
        }
        
        private bool showPreviewParametersButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Parameters button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Parameters button in the toolbar of the preview.")]
        public bool ShowPreviewParametersButton
        {
            get
            {
                return showPreviewParametersButton;
            }
            set
            {
                showPreviewParametersButton = value;
            }
        }
        
        private bool showPreviewThumbnailsButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Thumbnails button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Thumbnails button in the toolbar of the preview.")]
        public bool ShowPreviewThumbnailsButton
        {
            get
            {
                return showPreviewThumbnailsButton;
            }
            set
            {
                showPreviewThumbnailsButton = value;
            }
        }
        
        private bool showPreviewFindButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Find button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Find button in the toolbar of the preview.")]
        public bool ShowPreviewFindButton
        {
            get
            {
                return showPreviewFindButton;
            }
            set
            {
                showPreviewFindButton = value;
            }
        }
        
        private bool showPreviewFullScreenButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Full Screen button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Full Screen button in the toolbar of the preview.")]
        public bool ShowPreviewFullScreenButton
        {
            get
            {
                return showPreviewFullScreenButton;
            }
            set
            {
                showPreviewFullScreenButton = value;
            }
        }
        
        private bool showPreviewFirstPageButton = true;
        /// <summary>
        /// Gets or sets a visibility of the First Page button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the First Page button in the toolbar of the preview.")]
        public bool ShowPreviewFirstPageButton
        {
            get
            {
                return showPreviewFirstPageButton;
            }
            set
            {
                showPreviewFirstPageButton = value;
            }
        }
        
        private bool showPreviewPreviousPageButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Prev Page button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Prev Page button in the toolbar of the preview.")]
        public bool ShowPreviewPreviousPageButton
        {
            get
            {
                return showPreviewPreviousPageButton;
            }
            set
            {
                showPreviewPreviousPageButton = value;
            }
        }
        
        private bool showPreviewGoToPageButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Go To Page button in the navigate panel of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Go To Page button in the navigate panel of the preview.")]
        public bool ShowPreviewGoToPageButton
        {
            get
            {
                return showPreviewGoToPageButton;
            }
            set
            {
                showPreviewGoToPageButton = value;
            }
        }
        
        private bool showPreviewNextPageButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Next Page button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Next Page button in the toolbar of the preview.")]
        public bool ShowPreviewNextPageButton
        {
            get
            {
                return showPreviewNextPageButton;
            }
            set
            {
                showPreviewNextPageButton = value;
            }
        }
        
        private bool showPreviewLastPageButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Last Page button in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Last Page button in the toolbar of the preview.")]
        public bool ShowPreviewLastPageButton
        {
            get
            {
                return showPreviewLastPageButton;
            }
            set
            {
                showPreviewLastPageButton = value;
            }
        }
        
        private bool showPreviewSinglePageViewModeButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Single Page button in the view mode panel of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Single Page button in the view mode panel of the preview.")]
        public bool ShowPreviewSinglePageViewModeButton
        {
            get
            {
                return showPreviewSinglePageViewModeButton;
            }
            set
            {
                showPreviewSinglePageViewModeButton = value;
            }
        }
        
        private bool showPreviewContinuousPageViewModeButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Continuous Page button in the view mode panel of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Continuous Page button in the view mode panel of the preview.")]
        public bool ShowPreviewContinuousPageViewModeButton
        {
            get
            {
                return showPreviewContinuousPageViewModeButton;
            }
            set
            {
                showPreviewContinuousPageViewModeButton = value;
            }
        }
        
        private bool showPreviewMultiplePageViewModeButton = true;
        /// <summary>
        /// Gets or sets a visibility of the Multiple Page button in the view mode panel of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of the Multiple Page button in the view mode panel of the preview.")]
        public bool ShowPreviewMultiplePageViewModeButton
        {
            get
            {
                return showPreviewMultiplePageViewModeButton;
            }
            set
            {
                showPreviewMultiplePageViewModeButton = value;
            }
        }
        
        private bool showPreviewZoomButtons = true;
        /// <summary>
        /// Gets or sets a visibility of Zoom buttons in the toolbar of the preview.
        /// </summary>
        [Category("Preview")]
        [DefaultValue(true)]
        [Description("Gets or sets a visibility of Zoom buttons in the toolbar of the preview.")]
        public bool ShowPreviewZoomButtons
        {
            get
            {
                return showPreviewZoomButtons;
            }
            set
            {
                showPreviewZoomButtons = value;
            }
        }

        #endregion

        #region Server

        private static StiCacheHelper cacheHelper = null;
        /// <summary>
        /// Gets or sets an instance of the StiCacheHelper class that will be used for report caching on the server side.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets an instance of the StiCacheHelper class that will be used for report caching on the server side.")]
        public static StiCacheHelper CacheHelper
        {
            get
            {
                if (cacheHelper == null) cacheHelper = new StiCacheHelper();
                return cacheHelper;
            }
            set
            {
                cacheHelper = value;
            }
        }

        private int requestTimeout = 30;
        /// <summary>
        /// Gets or sets time which indicates how many seconds the client side will wait for the response from the server side. The default value is 30 seconds.
        /// </summary>
        [Category("Server")]
        [DefaultValue(30)]
        [Description("Gets or sets time which indicates how many seconds the client side will wait for the response from the server side. The default value is 30 seconds.")]
        public int RequestTimeout
        {
            get
            {
                return requestTimeout;
            }
            set
            {
                // Min 1 sec. Max 6 hours.
                requestTimeout = Math.Max(1, Math.Min(21600, value));
            }
        }

        private int cacheTimeout = 10;
        /// <summary>
        /// Gets or sets time which indicates how many minutes the result of the report rendering will be stored in the server cache or session. The default value is 10 minutes.
        /// </summary>
        [Category("Server")]
        [DefaultValue(10)]
        [Description("Gets or sets time which indicates how many minutes the result of the report rendering will be stored in the server cache or session. The default value is 10 minutes.")]
        public int CacheTimeout
        {
            get
            {
                return cacheTimeout;
            }
            set
            {
                cacheTimeout = value;
            }
        }

        private StiServerCacheMode cacheMode = StiServerCacheMode.ObjectCache;
        /// <summary>
        /// Gets or sets the mode of the report caching.
        /// </summary>
        [Category("Server")]
        [DefaultValue(StiServerCacheMode.ObjectCache)]
        [Description("Gets or sets the mode of the report caching.")]
        public StiServerCacheMode CacheMode
        {
            get
            {
                return cacheMode;
            }
            set
            {
                cacheMode = value;
            }
        }
        
        private CacheItemPriority cacheItemPriority = CacheItemPriority.Default;
        /// <summary>
        /// Specifies the relative priority of report, stored in the system cache.
        /// </summary>
        [Category("Server")]
        [DefaultValue(CacheItemPriority.Default)]
        [Description("Specifies the relative priority of report, stored in the system cache.")]
        public CacheItemPriority CacheItemPriority
        {
            get
            {
                return cacheItemPriority;
            }
            set
            {
                cacheItemPriority = value;
            }
        }

        private int repeatCount = 1;
        /// <summary>
        /// Gets or sets the the number of repeats of requests of the server side to the client side, when getting errors of obtaining data.
        /// </summary>
        [Category("Server")]
        [DefaultValue(1)]
        [Description("Gets or sets the the number of repeats of requests of the server side to the client side, when getting errors of obtaining data.")]
        public int RepeatCount
        {
            get
            {
                return repeatCount;
            }
            set
            {
                repeatCount = value;
            }
        }

        private bool enableDataLogger = false;
        /// <summary>
        /// Gets or sets the value that enables or disables the logging of all requests/responses.
        /// </summary>
        [Category("Server")]
        [DefaultValue(false)]
        [Description("Gets or sets the value that enables or disables the logging of all requests/responses.")]
        public bool EnableDataLogger
        {
            get
            {
                return enableDataLogger;
            }
            set
            {
                enableDataLogger = value;
            }
        }

        private bool useRelativeUrls = true;
        /// <summary>
        /// Gets or sets a value which indicates that the designer will use relative or absolute URLs.
        /// </summary>
        [Category("Server")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which indicates that the designer will use relative or absolute URLs.")]
        public bool UseRelativeUrls
        {
            get
            {
                return useRelativeUrls;
            }
            set
            {
                useRelativeUrls = value;
            }
        }
        
        private bool passQueryParametersForResources = true;
        /// <summary>
        /// Gets or sets a value which enables or disables the transfer of URL parameters when requesting the scripts and styles of the designer.
        /// </summary>
        [Category("Server")]
        [DefaultValue(true)]
        [Description("Gets or sets a value which enables or disables the transfer of URL parameters when requesting the scripts and styles of the designer.")]
        public bool PassQueryParametersForResources
        {
            get
            {
                return passQueryParametersForResources;
            }
            set
            {
                passQueryParametersForResources = value;
            }
        }

        #endregion

        #region Report

        private StiReport report = null;
        /// <summary>
        /// Gets or sets a report object which to edit in the designer.
        /// </summary>
        [Browsable(false)]
        [Description("Gets or sets a report object which to edit in the designer.")]
        public StiReport Report
        {
            get
            {
                if (report == null) report = GetReportObject(this.RequestParams);
                return report;
            }
            set
            {
                report = value;
                if (this.RequestParams.Component != StiComponentType.DesignerFx) this.requestParams = this.CreateRequestParams();
                this.RequestParams.Cache.Helper.SaveReportInternal(this.RequestParams, report);
            }
        }

        #endregion

        #region Internal

        private string SaveReportErrorString = string.Empty;
        private bool SaveReportSendToClient = false;

        private static bool IsDesignMode
        {
            get
            {
                return HttpContext.Current == null;
            }
        }

        private StiRequestParams requestParams = null;
        private StiRequestParams RequestParams
        {
            get
            {
                if (requestParams == null) requestParams = GetRequestParams();
                return requestParams;
            }
        }

        private string clientGuid = null;
        private string ClientGuid
        {
            get
            {
                if (clientGuid == null) clientGuid = StiGuidUtils.NewGuid();
                return clientGuid;
            }
            set
            {
                clientGuid = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public override ClientIDMode ClientIDMode
        {
            get
            {
                return base.ClientIDMode;
            }
            set
            {
                base.ClientIDMode = value;
            }
        }
        
        [Browsable(false)]
        public string UserScript { get; set; }

        #endregion


        #region Obsolete
        
        [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the enum StiServerCacheMode instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public enum StiCacheMode
        {
            Page,
            Session
        }
        
        [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the enum StiDesignerFxTheme instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public enum StiTheme
        {
            Blue,
            Office2013
        }

        [Obsolete("This property is obsolete and no longer used in the component. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool DataEncryption
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        [Obsolete("This property is obsolete and no longer used in the component. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool DataCompression
        {
            get
            {
                return true;
            }
            set
            {
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the CacheMode property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool UseCache
        {
            get
            {
                return CacheMode != StiServerCacheMode.None;
            }
            set
            {
                if (UseCache == false) CacheMode = StiServerCacheMode.None;
            }
        }
        
        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the CacheTimeout property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public TimeSpan ServerTimeout
        {
            get
            {
                return new TimeSpan(0, CacheTimeout, 0);
            }
            set
            {
                CacheTimeout = (int)value.TotalMinutes;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the CacheMode property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public string AppCacheDirectory
        {
            get
            {
                return string.Empty;
            }
            set
            {
            }
        }
        
        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the FlashWMode property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public StiWMode WMode
        {
            get
            {
                return FlashWMode;
            }
            set
            {
                FlashWMode = value;
            }
        }
        
        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the FlashWMode property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public StiTheme ThemeName
        {
            get
            {
                if (Theme == StiDesignerFxTheme.Office2007Blue) return StiTheme.Blue;
                return StiTheme.Office2013;
            }
            set
            {
                if (value == StiTheme.Blue) Theme = StiDesignerFxTheme.Office2007Blue;
                else Theme = StiDesignerFxTheme.Office2013;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the DataFormat property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public string ParametersPanelDateFormat
        {
            get
            {
                return DateFormat;
            }
            set
            {
                DateFormat = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the SaveReportMode property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public StiSaveMode SaveMode
        {
            get
            {
                return SaveReportMode;
            }
            set
            {
                SaveReportMode = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the SaveReportAsMode property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public StiSaveMode SaveAsMode
        {
            get
            {
                return SaveReportAsMode;
            }
            set
            {
                SaveReportAsMode = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowDialogsHelp property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowFormsHelp
        {
            get
            {
                return ShowDialogsHelp;
            }
            set
            {
                ShowDialogsHelp = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowDialogsHints property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowFormsHints
        {
            get
            {
                return ShowDialogsHints;
            }
            set
            {
                ShowDialogsHints = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowSaveDialog property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool ShowSaveFileDialog
        {
            get
            {
                return ShowSaveDialog;
            }
            set
            {
                ShowSaveDialog = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuNew property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowNew
        {
            get
            {
                return ShowFileMenuNew;
            }
            set
            {
                ShowFileMenuNew = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuNewReport property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowNewReport
        {
            get
            {
                return ShowFileMenuNewReport;
            }
            set
            {
                ShowFileMenuNewReport = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuNewReportWithWizard property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowNewReportWithWizard
        {
            get
            {
                return ShowFileMenuNewReportWithWizard;
            }
            set
            {
                ShowFileMenuNewReportWithWizard = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuNewPage property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowNewPage
        {
            get
            {
                return ShowFileMenuNewPage;
            }
            set
            {
                ShowFileMenuNewPage = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuOpen property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowOpenReport
        {
            get
            {
                return ShowFileMenuOpen;
            }
            set
            {
                ShowFileMenuOpen = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuSave property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowSaveReport
        {
            get
            {
                return ShowFileMenuSave;
            }
            set
            {
                ShowFileMenuSave = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuSaveAs property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowSaveAs
        {
            get
            {
                return ShowFileMenuSaveAs;
            }
            set
            {
                ShowFileMenuSaveAs = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuPreview property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowPreview
        {
            get
            {
                return ShowFileMenuPreview;
            }
            set
            {
                ShowFileMenuPreview = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuPreviewAsPdf property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowPreviewAsPdf
        {
            get
            {
                return ShowFileMenuPreviewAsPdf;
            }
            set
            {
                ShowFileMenuPreviewAsPdf = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuPreviewAsHtml property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowPreviewAsHtml
        {
            get
            {
                return ShowFileMenuPreviewAsHtml;
            }
            set
            {
                ShowFileMenuPreviewAsHtml = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuPreviewAsXps property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowPreviewAsXps
        {
            get
            {
                return ShowFileMenuPreviewAsXps;
            }
            set
            {
                ShowFileMenuPreviewAsXps = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuClose property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowClose
        {
            get
            {
                return ShowFileMenuClose;
            }
            set
            {
                ShowFileMenuClose = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuAbout property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowAbout
        {
            get
            {
                return ShowFileMenuAbout;
            }
            set
            {
                ShowFileMenuAbout = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the ShowFileMenuExit property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool MainMenuShowExit
        {
            get
            {
                return ShowFileMenuExit;
            }
            set
            {
                ShowFileMenuExit = value;
            }
        }

        [Obsolete("This property is obsolete. It will be removed in next versions. Please use the FileMenuCaption property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public string MainMenuCaption
        {
            get
            {
                return FileMenuCaption;
            }
            set
            {
                FileMenuCaption = value;
            }
        }

        #endregion
    }
}
