#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.IO;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Export;
using System;
using System.Data;

namespace Stimulsoft.Report.Web
{
    public partial class StiWebDesignerFx :
        WebControl,
        INamingContainer
    {
        #region GetReport

        /// <summary>
        /// The event occurs when the report template is requested after running the designer.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs when the report template is requested after running the designer.")]
        public event StiReportDataEventHandler GetReport;

        private void InvokeGetReport()
        {
            var e = new StiReportDataEventArgs(this.RequestParams, this.Report);
            OnGetReport(e);
            this.report = e.Report != null ? e.Report : new StiReport();
        }

        protected virtual void OnGetReport(StiReportDataEventArgs e)
        {
            if (GetReport != null) GetReport(this, e);
        }

        #endregion

        #region CreateReport

        /// <summary>
        /// The event occurs when creating a new report template using the file menu.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs when creating a new report template using the file menu.")]
        public event StiReportDataEventHandler CreateReport;

        private void InvokeCreateReport()
        {
            StiReportDataEventArgs e = new StiReportDataEventArgs(this.RequestParams, this.Report);
            OnCreateReport(e);
            if (e.Report != null) this.report = e.Report;
        }

        protected virtual void OnCreateReport(StiReportDataEventArgs e)
        {
            if (CreateReport != null) CreateReport(this, e);
        }

        #endregion

        #region SaveReport / SaveReportAs

        /// <summary>
        /// The event occurs when saving the report template.
        /// </summary>
        [Description("The event occurs when saving a report template.")]
        public event StiSaveReportEventHandler SaveReport;

        /// <summary>
        /// The event occurs when saving as file the report template.
        /// </summary>
        [Description("The event occurs when 'saving as' a report template.")]
        public event StiSaveReportEventHandler SaveReportAs;

        private void InvokeSaveReport()
        {
            var e = new StiSaveReportEventArgs(this.RequestParams, this.Report);

            if (this.RequestParams.Designer.IsSaveAs) OnSaveReportAs(e);
            else OnSaveReport(e);
            
            if (!string.IsNullOrEmpty(e.ErrorString)) this.SaveReportErrorString = e.ErrorString;
            else if (e.ErrorCode >= 0) this.SaveReportErrorString = e.ErrorCode.ToString();
            else this.SaveReportErrorString = "-1";

            if (e.SendReportToClient) this.report = e.Report;
            this.SaveReportSendToClient = e.SendReportToClient &&
                (this.RequestParams.Designer.IsSaveAs && this.SaveReportAsMode == StiSaveMode.Hidden ||
                !this.RequestParams.Designer.IsSaveAs && this.SaveReportMode == StiSaveMode.Hidden);
        }

        protected virtual void OnSaveReport(StiSaveReportEventArgs e)
        {
            if (SaveReport != null) SaveReport(this, e);
        }

        protected virtual void OnSaveReportAs(StiSaveReportEventArgs e)
        {
            if (SaveReportAs != null) SaveReportAs(this, e);
        }

        #endregion

        #region PreviewReport

        /// <summary>
        /// The event occurs before rendering the report for preview. Allowed to change the properties of the report, register the data.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs before rendering the report for preview. Allowed to change the properties of the report, register the data.")]
        public event StiReportDataEventHandler PreviewReport;

        private void InvokePreviewReport()
        {
            OnPreviewReport(new StiReportDataEventArgs(this.RequestParams, this.Report));
        }

        protected virtual void OnPreviewReport(StiReportDataEventArgs e)
        {
            if (PreviewReport != null) PreviewReport(this, e);
        }

        #endregion

        #region PrintReport

        /// <summary>
        /// The event occurs before the printing of the report from the preview.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs before the printing of the report from the preview.")]
        public event StiPrintReportEventHandler PrintReport;

        private void InvokePrintReport(StiExportSettings settings)
        {
            OnPrintReport(new StiPrintReportEventArgs(this.RequestParams, this.Report, settings));
        }

        protected virtual void OnPrintReport(StiPrintReportEventArgs e)
        {
            if (PrintReport != null) PrintReport(this, e);
        }

        #endregion

        #region ExportReport

        /// <summary>
        /// The event occurs before the exporting of the report from the preview tab.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs before the exporting of the report from the preview tab.")]
        public event StiExportReportEventHandler ExportReport;

        private void InvokeExportReport(StiExportSettings settings)
        {
            OnExportReport(new StiExportReportEventArgs(this.RequestParams, this.Report, settings));
        }

        protected virtual void OnExportReport(StiExportReportEventArgs e)
        {
            if (ExportReport != null) ExportReport(this, e);
            else if (ReportExport != null) ReportExport(this, e);
        }

        /// <summary>
        /// The event occurs after the report is exported from the preview tab.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs after the report is exported from the preview tab.")]
        public event StiExportReportResponseEventHandler ExportReportResponse;

        private void InvokeExportReportResponse(StiExportSettings settings, Stream stream, string fileName, string contentType)
        {
            OnExportReportResponse(new StiExportReportResponseEventArgs(this.RequestParams, this.Report, settings, stream, fileName, contentType));
        }

        protected virtual void OnExportReportResponse(StiExportReportResponseEventArgs e)
        {
            if (ExportReportResponse != null) ExportReportResponse(this, e);
        }

        #endregion

        #region EmailReport

        /// <summary>
        /// The event occurs when sending a report via Email. In this event it is necessary to set settings for sending Email.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs when sending a report via Email. In this event it is necessary to set settings for sending Email.")]
        public event StiEmailReportEventHandler EmailReport;

        private void InvokeEmailReport(StiExportSettings settings, StiEmailOptions options)
        {
            OnEmailReport(new StiEmailReportEventArgs(this.RequestParams, this.Report, settings, options));
        }

        protected virtual void OnEmailReport(StiEmailReportEventArgs e)
        {
            if (EmailReport != null) EmailReport(this, e);
        }

        #endregion

        #region Exit

        /// <summary>
        /// The event occurs when clicking on the Exit button in the main menu.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs when clicking on the Exit button in the main menu.")]
        public event StiReportDataEventHandler Exit;

        private void InvokeExit()
        {
            OnExit(new StiReportDataEventArgs(this.RequestParams, this.Report));
        }

        protected virtual void OnExit(StiReportDataEventArgs e)
        {
            if (Exit != null) Exit(this, e);
        }

        #endregion


        #region Obsolete

        #region PreInit

        [Obsolete("This delegate is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiPreInitEventHandler(object sender, StiPreInitEventArgs e);

        [Obsolete("This class is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiPreInitEventArgs : EventArgs
        {
            private StiWebViewerFx viewer = null;
            public StiWebViewerFx Viewer
            {
                get
                {
                    return viewer;
                }
            }

            public StiPreInitEventArgs(StiWebViewerFx viewer)
            {
                this.viewer = viewer;
            }
        }

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the ASP.NET Page_Load or ASP.NET Page_PreInit event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiPreInitEventHandler PreInit;

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the ASP.NET Page_Load or ASP.NET Page_PreInit event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        protected virtual void OnPreInit(StiPreInitEventArgs e)
        {
            if (PreInit != null) PreInit(this, e);
        }

        #endregion

        #region StiGetReportEventArgs

        [Obsolete("This delegate is obsolete. Please use StiReportDataEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiGetReportEventHandler(object sender, StiGetReportEventArgs e);

        [Obsolete("This class is obsolete. Please use StiReportDataEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiGetReportEventArgs : StiReportDataEventArgs
        {
            public StiGetReportEventArgs(StiRequestParams requestParams, StiReport report) : base(requestParams, report)
            {
            }
        }

        #endregion

        #region StiPreviewDataSetEventArgs

        [Obsolete("This delegate is obsolete. Please use StiReportDataEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiPreviewDataSetEventHandler(object sender, StiPreviewDataSetEventArgs e);
        
        [Obsolete("This class is obsolete. Please use StiReportDataEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiPreviewDataSetEventArgs : StiReportDataEventArgs
        {
            private DataSet previewDataSet = null;
            public DataSet PreviewDataSet
            {
                get
                {
                    return previewDataSet;
                }
                set
                {
                    previewDataSet = value;
                }
            }

            public StiPreviewDataSetEventArgs(StiRequestParams requestParams, StiReport report) : base(requestParams, report)
            {
            }
        }

        #endregion
        
        #region StiPreviewReportEventArgs

        [Obsolete("This delegate is obsolete. Please use StiReportDataEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiPreviewReportEventHandler(object sender, StiPreviewReportEventArgs e);

        [Obsolete("This class is obsolete. Please use StiReportDataEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiPreviewReportEventArgs : StiReportDataEventArgs
        {
            private DataSet previewDataSet = null;
            public DataSet PreviewDataSet
            {
                get
                {
                    return previewDataSet;
                }
                set
                {
                    previewDataSet = value;
                }
            }

            public StiPreviewReportEventArgs(StiRequestParams requestParams, StiReport report) : base(requestParams, report)
            {
            }
        }

        #endregion
        
        #region StiExitEventArgs

        [Obsolete("This delegate is obsolete. Please use StiReportDataEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiExitEventHandler(object sender, StiExitEventArgs e);

        [Obsolete("This class is obsolete. Please use StiReportDataEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiExitEventArgs : StiReportDataEventArgs
        {
            public StiExitEventArgs(StiRequestParams requestParams, StiReport report) : base(requestParams, report)
            {
            }
        }

        #endregion

        #region ReportExport

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the ExportReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiExportReportEventHandler ReportExport;

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the ExportReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        protected virtual void OnReportExport(StiExportReportEventArgs e)
        {
        }

        #endregion

        #region ProcessReportBeforeRender

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the PreviewReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiProcessReportBeforeRenderEventHandler ProcessReportBeforeRender;

        [Obsolete("This event is obsolete. It will be removed in next versions. Please use the PreviewReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        protected virtual void OnProcessReportBeforeRender(StiProcessReportBeforeRenderEventArgs e)
        {
            if (ProcessReportBeforeRender != null) ProcessReportBeforeRender(this, e);
        }

        [Obsolete("This delegate is obsolete. Please use StiReportDataEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiProcessReportBeforeRenderEventHandler(object sender, StiProcessReportBeforeRenderEventArgs e);

        [Obsolete("This class is obsolete. Please use StiReportDataEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiProcessReportBeforeRenderEventArgs : StiReportDataEventArgs
        {
            public StiProcessReportBeforeRenderEventArgs(StiRequestParams requestParams, StiReport report) : base(requestParams, report)
            {
            }
        }

        #endregion
        
        #endregion
    }
}
