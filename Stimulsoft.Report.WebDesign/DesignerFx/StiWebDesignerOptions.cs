#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.ComponentModel;

namespace Stimulsoft.Report.Web
{
    [Obsolete("This class with all static options is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx component properties instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class StiWebDesignerOptions
    {
        #region Class: StiZoomMode

        [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiZoomModeFx enum instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public sealed class StiZoomMode
        {
            public const int Default = 0;
            public const int OnePage = 1;
            public const int TwoPages = 2;
            public const int PageWidth = 3;
            public const int PageHeight = 4;
        }

        #endregion

        #region Enum: StiImageQuality

        [Obsolete("This class is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiImageQuality component properties instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public enum StiImageQuality
        {
            Low,
            Normal,
            High
        }

        #endregion


        #region Appearance
        public sealed class Appearance
        {
            private static double gridSizeCentimetres = 0.2;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.GridSizeInCentimetres property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static double GridSizeCentimetres
            {
                get
                {
                    return gridSizeCentimetres;
                }
                set
                {
                    gridSizeCentimetres = value;
                }
            }

            private static double gridSizeHundredthsOfInch = 10;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.GridSizeInHundredthsOfInches property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static double GridSizeHundredthsOfInch
            {
                get
                {
                    return gridSizeHundredthsOfInch;
                }
                set
                {
                    gridSizeHundredthsOfInch = value;
                }
            }

            private static double gridSizeInch = 0.1;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.GridSizeInInches property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static double GridSizeInch
            {
                get
                {
                    return gridSizeInch;
                }
                set
                {
                    gridSizeInch = value;
                }
            }

            private static double gridSizeMillimeters = 2;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.GridSizeInMillimeters property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static double GridSizeMillimeters
            {
                get
                {
                    return gridSizeMillimeters;
                }
                set
                {
                    gridSizeMillimeters = value;
                }
            }

            private static bool showTooltipsHelp = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowTooltipsHelp property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowTooltipsHelp
            {
                get
                {
                    return showTooltipsHelp;
                }
                set
                {
                    showTooltipsHelp = value;
                }
            }

            private static bool showFormsHelp = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowFormsHelp property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowFormsHelp
            {
                get
                {
                    return showFormsHelp;
                }
                set
                {
                    showFormsHelp = value;
                }
            }

            private static bool showFormsHints = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowFormsHints property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowFormsHints
            {
                get
                {
                    return showFormsHints;
                }
                set
                {
                    showFormsHints = value;
                }
            }

            private static string dateFormat = "dd.MM.yyyy";
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.DateFormat property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string DateFormat
            {
                get
                {
                    return dateFormat;
                }
                set
                {
                    dateFormat = value;
                }
            }

            private static bool showSaveFileDialog = false;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowSaveFileDialog property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowSaveFileDialog
            {
                get
                {
                    return showSaveFileDialog;
                }
                set
                {
                    showSaveFileDialog = value;
                }
            }

            private static bool showMoreDetailsButton = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowMoreDetailsButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowMoreDetailsButton
            {
                get
                {
                    return showMoreDetailsButton;
                }
                set
                {
                    showMoreDetailsButton = value;
                }
            }

            private static bool setReportModifiedWhenOpening = false;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.SetReportModifiedWhenOpening property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool SetReportModifiedWhenOpening
            {
                get
                {
                    return setReportModifiedWhenOpening;
                }
                set
                {
                    setReportModifiedWhenOpening = value;
                }
            }
        }
        #endregion

        #region AboutDialog
        public sealed class AboutDialog
        {
            private static string textLine1 = string.Empty;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AboutDialogTextLine1 property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string TextLine1
            {
                get
                {
                    return textLine1;
                }
                set
                {
                    textLine1 = value;
                }
            }

            private static string textLine2 = string.Empty;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AboutDialogTextLine2 property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string TextLine2
            {
                get
                {
                    return textLine2;
                }
                set
                {
                    textLine2 = value;
                }
            }

            private static string url = string.Empty;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AboutDialogUrl property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string Url
            {
                get
                {
                    return url;
                }
                set
                {
                    url = value;
                }
            }

            private static string urlText = string.Empty;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AboutDialogUrlText property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string UrlText
            {
                get
                {
                    return urlText;
                }
                set
                {
                    urlText = value;
                }
            }
        }
        #endregion

        #region Connection
        public sealed class Connection
        {
            private static int clientRequestTimeout = 20;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.RequestTimeout property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static int ClientRequestTimeout
            {
                get
                {
                    return clientRequestTimeout;
                }
                set
                {
                    clientRequestTimeout = value;
                }
            }

            private static int clientRepeatCount = 1;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.RepeatCount property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static int ClientRepeatCount
            {
                get
                {
                    return clientRepeatCount;
                }
                set
                {
                    clientRepeatCount = value;
                }
            }

            private static bool relativeUrls = false;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.UseRelativeUrls property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool RelativeUrls
            {
                get
                {
                    return relativeUrls;
                }
                set
                {
                    relativeUrls = value;
                }
            }

            private static bool enableDataLogger = false;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.EnableDataLogger property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool EnableDataLogger
            {
                get
                {
                    return enableDataLogger;
                }
                set
                {
                    enableDataLogger = value;
                }
            }

            private static bool showCancelButton = false;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowCancelButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowCancelButton
            {
                get
                {
                    return showCancelButton;
                }
                set
                {
                    showCancelButton = value;
                }
            }
        }
        #endregion

        #region Dictionary
        public sealed class Dictionary
        {
            private static bool allowModifyDictionary = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AllowModifyDictionary property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AllowModifyDictionary
            {
                get
                {
                    return allowModifyDictionary;
                }
                set
                {
                    allowModifyDictionary = value;
                }
            }

            private static bool allowModifyConnections = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AllowModifyConnections property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AllowModifyConnections
            {
                get
                {
                    return allowModifyConnections;
                }
                set
                {
                    allowModifyConnections = value;
                }
            }

            private static bool allowModifyDataSources = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AllowModifyDataSources property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AllowModifyDataSources
            {
                get
                {
                    return allowModifyDataSources;
                }
                set
                {
                    allowModifyDataSources = value;
                }
            }

            private static bool allowModifyRelations = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AllowModifyRelations property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AllowModifyRelations
            {
                get
                {
                    return allowModifyRelations;
                }
                set
                {
                    allowModifyRelations = value;
                }
            }

            private static bool allowModifyVariables = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AllowModifyVariables property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool AllowModifyVariables
            {
                get
                {
                    return allowModifyVariables;
                }
                set
                {
                    allowModifyVariables = value;
                }
            }

            private static bool showConnectionType = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowConnectionType property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowConnectionType
            {
                get
                {
                    return showConnectionType;
                }
                set
                {
                    showConnectionType = value;
                }
            }

            private static bool showActionsMenu = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowActionsMenu property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowActionsMenu
            {
                get
                {
                    return showActionsMenu;
                }
                set
                {
                    showActionsMenu = value;
                }
            }

            private static bool showTestConnectionButton = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowTestConnectionButton property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowTestConnectionButton
            {
                get
                {
                    return showTestConnectionButton;
                }
                set
                {
                    showTestConnectionButton = value;
                }
            }
        }
        #endregion

        #region MainMenu
        public sealed class MainMenu
        {
            private static bool showNew = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowNew property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowNew
            {
                get
                {
                    return showNew;
                }
                set
                {
                    showNew = value;
                }
            }

            private static bool showNewReport = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowNewReport property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowNewReport
            {
                get
                {
                    return showNewReport;
                }
                set
                {
                    showNewReport = value;
                }
            }

            private static bool showNewReportWithWizard = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowNewReportWithWizard property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowNewReportWithWizard
            {
                get
                {
                    return showNewReportWithWizard;
                }
                set
                {
                    showNewReportWithWizard = value;
                }
            }

            private static bool showNewPage = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowNewPage property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowNewPage
            {
                get
                {
                    return showNewPage;
                }
                set
                {
                    showNewPage = value;
                }
            }

            private static bool showOpenReportFromGoogleDocs = false;
            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowOpenReportFromGoogleDocs
            {
                get
                {
                    return showOpenReportFromGoogleDocs;
                }
                set
                {
                    showOpenReportFromGoogleDocs = value;
                }
            }

            private static bool showOpenReport = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowOpenReport property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowOpenReport
            {
                get
                {
                    return showOpenReport;
                }
                set
                {
                    showOpenReport = value;
                }
            }

            private static bool showSaveReport = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowSaveReport property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowSaveReport
            {
                get
                {
                    return showSaveReport;
                }
                set
                {
                    showSaveReport = value;
                }
            }

            private static bool showSaveAs = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowSaveAs property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowSaveAs
            {
                get
                {
                    return showSaveAs;
                }
                set
                {
                    showSaveAs = value;
                }
            }

            private static bool showSaveAsToGoogleDocs = false;
            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowSaveAsToGoogleDocs
            {
                get
                {
                    return showSaveAsToGoogleDocs;
                }
                set
                {
                    showSaveAsToGoogleDocs = value;
                }
            }

            private static bool showDeletePage = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowDeletePage
            {
                get
                {
                    return showDeletePage;
                }
                set
                {
                    showDeletePage = value;
                }
            }

            private static bool showPreview = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowPreview property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPreview
            {
                get
                {
                    return showPreview;
                }
                set
                {
                    showPreview = value;
                }
            }

            private static bool showPreviewAsPdf = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowPreviewAsPdf property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPreviewAsPdf
            {
                get
                {
                    return showPreviewAsPdf;
                }
                set
                {
                    showPreviewAsPdf = value;
                }
            }

            private static bool showPreviewAsHtml = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowPreviewAsHtml property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPreviewAsHtml
            {
                get
                {
                    return showPreviewAsHtml;
                }
                set
                {
                    showPreviewAsHtml = value;
                }
            }

            private static bool showPreviewAsXps = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowPreviewAsXps property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPreviewAsXps
            {
                get
                {
                    return showPreviewAsXps;
                }
                set
                {
                    showPreviewAsXps = value;
                }
            }

            private static bool showClose = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowClose property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowClose
            {
                get
                {
                    return showClose;
                }
                set
                {
                    showClose = value;
                }
            }

            private static bool showExitButton = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowExit property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowExitButton
            {
                get
                {
                    return showExitButton;
                }
                set
                {
                    showExitButton = value;
                }
            }

            private static string caption = "Default";
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuCaption property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static string Caption
            {
                get
                {
                    return caption;
                }
                set
                {
                    caption = value;
                }
            }
        }
        #endregion

        #region Toolbar
        public sealed class Toolbar
        {
            private static bool showSelectLanguage = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowSelectLanguage property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowSelectLanguage
            {
                get
                {
                    return showSelectLanguage;
                }
                set
                {
                    showSelectLanguage = value;
                }
            }

            private static bool showAboutButton = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.MainMenuShowAbout property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowAboutButton
            {
                get
                {
                    return showAboutButton;
                }
                set
                {
                    showAboutButton = value;
                }
            }

            private static bool showCodeTab = false;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowCodeTab property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowCodeTab
            {
                get
                {
                    return showCodeTab;
                }
                set
                {
                    showCodeTab = value;
                }
            }

            private static bool showPreviewReportTab = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewReportTab property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowPreviewReportTab
            {
                get
                {
                    return showPreviewReportTab;
                }
                set
                {
                    showPreviewReportTab = value;
                }
            }

            private static bool showDictionaryTab = true;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowDictionaryTab property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool ShowDictionaryTab
            {
                get
                {
                    return showDictionaryTab;
                }
                set
                {
                    showDictionaryTab = value;
                }
            }

            private static int zoom = 100;
            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.Zoom property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static int Zoom
            {
                get
                {
                    return zoom;
                }
                set
                {
                    zoom = value;
                }
            }

            #region Obsolete properties

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool HideAboutDialog
            {
                get
                {
                    return ShowAboutButton;
                }
                set
                {
                    ShowAboutButton = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.Zoom property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static int ZoomMode
            {
                get
                {
                    return Zoom;
                }
                set
                {
                    Zoom = value;
                }
            }

            #endregion
        }
        #endregion

        #region Viewer
        public sealed class Viewer
        {
            #region Print
            public sealed class Print
            {
                private static bool autoPageOrientation = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AutoPageOrientation property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool AutoPageOrientation
                {
                    get
                    {
                        return autoPageOrientation;
                    }
                    set
                    {
                        autoPageOrientation = value;
                    }
                }

                private static bool autoPageScale = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AutoPageScale property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool AutoPageScale
                {
                    get
                    {
                        return autoPageScale;
                    }
                    set
                    {
                        autoPageScale = value;
                    }
                }

                private static bool allowDefaultPrint = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AllowDefaultPrint property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool AllowDefaultPrint
                {
                    get
                    {
                        return allowDefaultPrint;
                    }
                    set
                    {
                        allowDefaultPrint = value;
                    }
                }

                private static bool allowPrintToPdf = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AllowPrintToPdf property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool AllowPrintToPdf
                {
                    get
                    {
                        return allowPrintToPdf;
                    }
                    set
                    {
                        allowPrintToPdf = value;
                    }
                }

                private static bool allowPrintToHtml = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AllowPrintToHtml property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool AllowPrintToHtml
                {
                    get
                    {
                        return allowPrintToHtml;
                    }
                    set
                    {
                        allowPrintToHtml = value;
                    }
                }

                private static bool showPrintDialog = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPrintDialog property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowPrintDialog
                {
                    get
                    {
                        return showPrintDialog;
                    }
                    set
                    {
                        showPrintDialog = value;
                    }
                }

                private static StiImageQuality imageQuality = StiImageQuality.Normal;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiReportHelperFx.ImageQuality option instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static StiImageQuality ImageQuality
                {
                    get
                    {
                        return imageQuality;
                    }
                    set
                    {
                        imageQuality = value;
                    }
                }
            }

            #endregion

            #region Exports
            public sealed class Exports
            {
                private static bool showExportDialog = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportDialog property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportDialog
                {
                    get
                    {
                        return showExportDialog;
                    }
                    set
                    {
                        showExportDialog = value;
                    }
                }

                private static bool showExportToDocument = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToDocument property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToDocument
                {
                    get
                    {
                        return showExportToDocument;
                    }
                    set
                    {
                        showExportToDocument = value;
                    }
                }

                private static bool showExportToPdf = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToPdf property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToPdf
                {
                    get
                    {
                        return showExportToPdf;
                    }
                    set
                    {
                        showExportToPdf = value;
                    }
                }

                private static bool showExportToXps = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToXps property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToXps
                {
                    get
                    {
                        return showExportToXps;
                    }
                    set
                    {
                        showExportToXps = value;
                    }
                }

                private static bool showExportToPpt = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToPowerPoint property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToPpt
                {
                    get
                    {
                        return showExportToPpt;
                    }
                    set
                    {
                        showExportToPpt = value;
                    }
                }

                private static bool showExportToHtml = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToHtml property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToHtml
                {
                    get
                    {
                        return showExportToHtml;
                    }
                    set
                    {
                        showExportToHtml = value;
                    }
                }
                
                private static bool showExportToMht = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToMht property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToMht
                {
                    get
                    {
                        return showExportToMht;
                    }
                    set
                    {
                        showExportToMht = value;
                    }
                }

                private static bool showExportToText = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToText property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToText
                {
                    get
                    {
                        return showExportToText;
                    }
                    set
                    {
                        showExportToText = value;
                    }
                }

                private static bool showExportToRtf = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToRtf property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToRtf
                {
                    get
                    {
                        return showExportToRtf;
                    }
                    set
                    {
                        showExportToRtf = value;
                    }
                }

                private static bool showExportToWord2007 = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToWord2007 property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToWord2007
                {
                    get
                    {
                        return showExportToWord2007;
                    }
                    set
                    {
                        showExportToWord2007 = value;
                    }
                }

                private static bool showExportToOpenDocumentWriter = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToOpenDocumentWriter property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToOpenDocumentWriter
                {
                    get
                    {
                        return showExportToOpenDocumentWriter;
                    }
                    set
                    {
                        showExportToOpenDocumentWriter = value;
                    }
                }

                private static bool showExportToExcel = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToExcel property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToExcel
                {
                    get
                    {
                        return showExportToExcel;
                    }
                    set
                    {
                        showExportToExcel = value;
                    }
                }

                private static bool showExportToExcelXml = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToExcelXml property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToExcelXml
                {
                    get
                    {
                        return showExportToExcelXml;
                    }
                    set
                    {
                        showExportToExcelXml = value;
                    }
                }

                private static bool showExportToExcel2007 = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToExcel2007 property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToExcel2007
                {
                    get
                    {
                        return showExportToExcel2007;
                    }
                    set
                    {
                        showExportToExcel2007 = value;
                    }
                }

                private static bool showExportToOpenDocumentCalc = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToOpenDocumentCalc property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToOpenDocumentCalc
                {
                    get
                    {
                        return showExportToOpenDocumentCalc;
                    }
                    set
                    {
                        showExportToOpenDocumentCalc = value;
                    }
                }

                private static bool showExportToCsv = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToCsv property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToCsv
                {
                    get
                    {
                        return showExportToCsv;
                    }
                    set
                    {
                        showExportToCsv = value;
                    }
                }

                private static bool showExportToDbf = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToDbf property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToDbf
                {
                    get
                    {
                        return showExportToDbf;
                    }
                    set
                    {
                        showExportToDbf = value;
                    }
                }

                private static bool showExportToXml = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToXml property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToXml
                {
                    get
                    {
                        return showExportToXml;
                    }
                    set
                    {
                        showExportToXml = value;
                    }
                }

                private static bool showExportToDif = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToDif property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToDif
                {
                    get
                    {
                        return showExportToDif;
                    }
                    set
                    {
                        showExportToDif = value;
                    }
                }

                private static bool showExportToSylk = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToSylk property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToSylk
                {
                    get
                    {
                        return showExportToSylk;
                    }
                    set
                    {
                        showExportToSylk = value;
                    }
                }

                private static bool showExportToBmp = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToImageBmp property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToBmp
                {
                    get
                    {
                        return showExportToBmp;
                    }
                    set
                    {
                        showExportToBmp = value;
                    }
                }

                private static bool showExportToGif = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToImageGif property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToGif
                {
                    get
                    {
                        return showExportToGif;
                    }
                    set
                    {
                        showExportToGif = value;
                    }
                }

                private static bool showExportToJpeg = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToImageJpeg property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToJpeg
                {
                    get
                    {
                        return showExportToJpeg;
                    }
                    set
                    {
                        showExportToJpeg = value;
                    }
                }

                private static bool showExportToPcx = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToImagePcx property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToPcx
                {
                    get
                    {
                        return showExportToPcx;
                    }
                    set
                    {
                        showExportToPcx = value;
                    }
                }

                private static bool showExportToPng = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToImagePng property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToPng
                {
                    get
                    {
                        return showExportToPng;
                    }
                    set
                    {
                        showExportToPng = value;
                    }
                }

                private static bool showExportToTiff = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToImageTiff property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToTiff
                {
                    get
                    {
                        return showExportToTiff;
                    }
                    set
                    {
                        showExportToTiff = value;
                    }
                }

                private static bool showExportToMetafile = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToImageMetafile property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToMetafile
                {
                    get
                    {
                        return showExportToMetafile;
                    }
                    set
                    {
                        showExportToMetafile = value;
                    }
                }

                private static bool showExportToSvg = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToImageSvg property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToSvg
                {
                    get
                    {
                        return showExportToSvg;
                    }
                    set
                    {
                        showExportToSvg = value;
                    }
                }

                private static bool showExportToSvgz = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowExportToImageSvgz property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowExportToSvgz
                {
                    get
                    {
                        return showExportToSvgz;
                    }
                    set
                    {
                        showExportToSvgz = value;
                    }
                }
            }
            #endregion

            #region Toolbar
            public sealed class Toolbar
            {
                private static bool showMainToolbar = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewToolbar property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowMainToolbar
                {
                    get
                    {
                        return showMainToolbar;
                    }
                    set
                    {
                        showMainToolbar = value;
                    }
                }

                private static bool showPrintButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewPrintButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowPrintButton
                {
                    get
                    {
                        return showPrintButton;
                    }
                    set
                    {
                        showPrintButton = value;
                    }
                }

                private static bool showOpenButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewOpenButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowOpenButton
                {
                    get
                    {
                        return showOpenButton;
                    }
                    set
                    {
                        showOpenButton = value;
                    }
                }

                private static bool showSaveButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewSaveButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowSaveButton
                {
                    get
                    {
                        return showSaveButton;
                    }
                    set
                    {
                        showSaveButton = value;
                    }
                }

                private static bool showSendEMailButton = false;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewSendEmailButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowSendEMailButton
                {
                    get
                    {
                        return showSendEMailButton;
                    }
                    set
                    {
                        showSendEMailButton = value;
                    }
                }

                private static bool showBookmarksButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewBookmarksButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowBookmarksButton
                {
                    get
                    {
                        return showBookmarksButton;
                    }
                    set
                    {
                        showBookmarksButton = value;
                    }
                }

                private static bool showParametersButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewParametersButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowParametersButton
                {
                    get
                    {
                        return showParametersButton;
                    }
                    set
                    {
                        showParametersButton = value;
                    }
                }

                private static bool showParametersPanel = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewParametersButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowParametersPanel
                {
                    get
                    {
                        return showParametersPanel;
                    }
                    set
                    {
                        showParametersPanel = value;
                    }
                }

                private static bool showThumbnailsButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewThumbnailsButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowThumbnailsButton
                {
                    get
                    {
                        return showThumbnailsButton;
                    }
                    set
                    {
                        showThumbnailsButton = value;
                    }
                }

                private static bool showFindButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewFindButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowFindButton
                {
                    get
                    {
                        return showFindButton;
                    }
                    set
                    {
                        showFindButton = value;
                    }
                }
                
                private static bool showFullScreenButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewFullScreenButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowFullScreenButton
                {
                    get
                    {
                        return showFullScreenButton;
                    }
                    set
                    {
                        showFullScreenButton = value;
                    }
                }

                private static bool showNavigateToolbar = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewNavigatePanel property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowNavigateToolbar
                {
                    get
                    {
                        return showNavigateToolbar;
                    }
                    set
                    {
                        showNavigateToolbar = value;
                    }
                }

                private static bool showFirstPageButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewFirstPageButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowFirstPageButton
                {
                    get
                    {
                        return showFirstPageButton;
                    }
                    set
                    {
                        showFirstPageButton = value;
                    }
                }

                private static bool showPreviousPageButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewPreviousPageButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowPreviousPageButton
                {
                    get
                    {
                        return showPreviousPageButton;
                    }
                    set
                    {
                        showPreviousPageButton = value;
                    }
                }

                private static bool showGoToPageButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewGoToPageButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowGoToPageButton
                {
                    get
                    {
                        return showGoToPageButton;
                    }
                    set
                    {
                        showGoToPageButton = value;
                    }
                }

                private static bool showNextPageButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewNextPageButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowNextPageButton
                {
                    get
                    {
                        return showNextPageButton;
                    }
                    set
                    {
                        showNextPageButton = value;
                    }
                }

                private static bool showLastPageButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewLastPageButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowLastPageButton
                {
                    get
                    {
                        return showLastPageButton;
                    }
                    set
                    {
                        showLastPageButton = value;
                    }
                }

                private static bool showViewModeToolbar = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewViewModePanel property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowViewModeToolbar
                {
                    get
                    {
                        return showViewModeToolbar;
                    }
                    set
                    {
                        showViewModeToolbar = value;
                    }
                }

                private static bool showPageViewModeSingleButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewSinglePageViewModeButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowPageViewModeSingleButton
                {
                    get
                    {
                        return showPageViewModeSingleButton;
                    }
                    set
                    {
                        showPageViewModeSingleButton = value;
                    }
                }

                private static bool showPageViewModeContinuousButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewContinuousPageViewModeButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowPageViewModeContinuousButton
                {
                    get
                    {
                        return showPageViewModeContinuousButton;
                    }
                    set
                    {
                        showPageViewModeContinuousButton = value;
                    }
                }

                private static bool showPageViewModeMultipleButton = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewMultiplePageViewModeButton property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowPageViewModeMultipleButton
                {
                    get
                    {
                        return showPageViewModeMultipleButton;
                    }
                    set
                    {
                        showPageViewModeMultipleButton = value;
                    }
                }

                private static bool showZoom = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowPreviewZoomButtons property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowZoom
                {
                    get
                    {
                        return showZoom;
                    }
                    set
                    {
                        showZoom = value;
                    }
                }

                private static int zoom = StiZoomMode.Default;
                [Obsolete("This static option is obsolete. It will be removed in next versions.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static int Zoom
                {
                    get
                    {
                        return zoom;
                    }
                    set
                    {
                        zoom = value;
                    }
                }
            }
            #endregion

            #region Mail

            public sealed class Mail
            {
                private static string addressFrom = string.Empty;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static string AddressFrom
                {
                    get
                    {
                        return addressFrom;
                    }
                    set
                    {
                        addressFrom = value;
                    }
                }

                private static string addressTo = string.Empty;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static string AddressTo
                {
                    get
                    {
                        return addressTo;
                    }
                    set
                    {
                        addressTo = value;
                    }
                }

                private static string subject = string.Empty;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static string Subject
                {
                    get
                    {
                        return subject;
                    }
                    set
                    {
                        subject = value;
                    }
                }

                private static string body = string.Empty;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static string Body
                {
                    get
                    {
                        return body;
                    }
                    set
                    {
                        body = value;
                    }
                }

                private static string host = "localhost";
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static string Host
                {
                    get
                    {
                        return host;
                    }
                    set
                    {
                        host = value;
                    }
                }

                private static int port = 25;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static int Port
                {
                    get
                    {
                        return port;
                    }
                    set
                    {
                        port = value;
                    }
                }

                private static string userName = string.Empty;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static string UserName
                {
                    get
                    {
                        return userName;
                    }
                    set
                    {
                        userName = value;
                    }
                }

                private static string password = string.Empty;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiEmailOptions class in the EmailReport event instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static string Password
                {
                    get
                    {
                        return password;
                    }
                    set
                    {
                        password = value;
                    }
                }
            }

            #endregion

            #region Appearance

            public sealed class Appearance
            {
                private static bool autoHideScrollbars = false;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AutoHideScrollbars property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool AutoHideScrollbars
                {
                    get
                    {
                        return autoHideScrollbars;
                    }
                    set
                    {
                        autoHideScrollbars = value;
                    }
                }

                private static Color currentPageBorderColor = Color.Gold;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.CurrentPageBorderColor property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static Color CurrentPageBorderColor
                {
                    get
                    {
                        return currentPageBorderColor;
                    }
                    set
                    {
                        currentPageBorderColor = value;
                    }
                }

                private static string openLinksTarget = "_blank";
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.OpenLinksWindow property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static string OpenLinksTarget
                {
                    get
                    {
                        return openLinksTarget;
                    }
                    set
                    {
                        openLinksTarget = value;
                    }
                }

                private static int variablesPanelColumns = 2;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ParametersPanelColumnsCount property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static int VariablesPanelColumns
                {
                    get
                    {
                        return variablesPanelColumns;
                    }
                    set
                    {
                        variablesPanelColumns = value;
                    }
                }

                private static int variablesPanelEditorWidth = 200;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ParametersPanelEditorWidth property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static int VariablesPanelEditorWidth
                {
                    get
                    {
                        return variablesPanelEditorWidth;
                    }
                    set
                    {
                        variablesPanelEditorWidth = value;
                    }
                }

                private static bool showEmailDialog = true;
                [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.ShowEmailDialog property instead.")]
                [EditorBrowsable(EditorBrowsableState.Never)]
                public static bool ShowEmailDialog
                {
                    get
                    {
                        return showEmailDialog;
                    }
                    set
                    {
                        showEmailDialog = value;
                    }
                }
            }

            #endregion
        }
        #endregion

        private static bool allowModifyTemplate = true;
        [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AllowModifyTemplate property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool AllowModifyTemplate
        {
            get
            {
                return allowModifyTemplate;
            }
            set
            {
                allowModifyTemplate = value;
            }
        }

        private static int autoSaveInterval = 0;
        [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiWebDesignerFx.AutoSaveInterval property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static int AutoSaveInterval
        {
            get
            {
                return autoSaveInterval;
            }
            set
            {
                autoSaveInterval = value;
            }
        }

        private static bool sendSavedReportToClient = false;
        [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the SendReportToClient property in the SaveReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool SendSavedReportToClient
        {
            get
            {
                return sendSavedReportToClient;
            }
            set
            {
                sendSavedReportToClient = value;
            }
        }

        private static bool allowGCCollect = false;
        [Obsolete("This static option is obsolete. It will be removed in next versions. Please use the StiOptions.Web.AllowGCCollect option instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool AllowGCCollect
        {
            get
            {
                return allowGCCollect;
            }
            set
            {
                allowGCCollect = value;
            }
        }
        
        #region Obsolete

        public sealed class Menu
        {
            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool NewEnabled
            {
                get
                {
                    return MainMenu.ShowNew;
                }
                set
                {
                    MainMenu.ShowNew = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool NewReport
            {
                get
                {
                    return MainMenu.ShowNewReport;
                }
                set
                {
                    MainMenu.ShowNewReport = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool NewReportWithWizard
            {
                get
                {
                    return MainMenu.ShowNewReportWithWizard;
                }
                set
                {
                    MainMenu.ShowNewReportWithWizard = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool NewPage
            {
                get
                {
                    return MainMenu.ShowNewPage;
                }
                set
                {
                    MainMenu.ShowNewPage = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool OpenReport
            {
                get
                {
                    return MainMenu.ShowOpenReport;
                }
                set
                {
                    MainMenu.ShowOpenReport = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool SaveReport
            {
                get
                {
                    return MainMenu.ShowSaveReport;
                }
                set
                {
                    MainMenu.ShowSaveReport = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool SaveAs
            {
                get
                {
                    return MainMenu.ShowSaveAs;
                }
                set
                {
                    MainMenu.ShowSaveAs = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool DeletePage
            {
                get
                {
                    return MainMenu.ShowDeletePage;
                }
                set
                {
                    MainMenu.ShowDeletePage = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool Preview
            {
                get
                {
                    return MainMenu.ShowPreview;
                }
                set
                {
                    MainMenu.ShowPreview = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool PreviewAsPdf
            {
                get
                {
                    return MainMenu.ShowPreviewAsPdf;
                }
                set
                {
                    MainMenu.ShowPreviewAsPdf = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool PreviewAsHtml
            {
                get
                {
                    return MainMenu.ShowPreviewAsHtml;
                }
                set
                {
                    MainMenu.ShowPreviewAsHtml = value;
                }
            }

            [Obsolete("This static option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public static bool Close
            {
                get
                {
                    return MainMenu.ShowClose;
                }
                set
                {
                    MainMenu.ShowClose = value;
                }
            }
        }

        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool ModifyDictionary
        {
            get
            {
                return Dictionary.AllowModifyDictionary;
            }
            set
            {
                Dictionary.AllowModifyDictionary = value;
            }
        }

        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool ModifyConnections
        {
            get
            {
                return Dictionary.AllowModifyConnections;
            }
            set
            {
                Dictionary.AllowModifyConnections = value;
            }
        }

        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool ModifyDataSources
        {
            get
            {
                return Dictionary.AllowModifyDataSources;
            }
            set
            {
                Dictionary.AllowModifyDataSources = value;
            }
        }

        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool ModifyVariables
        {
            get
            {
                return Dictionary.AllowModifyVariables;
            }
            set
            {
                Dictionary.AllowModifyVariables = value;
            }
        }

        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool ModifyTemplate
        {
            get
            {
                return AllowModifyTemplate;
            }
            set
            {
                AllowModifyTemplate = value;
            }
        }

        private static bool allowScale = true;
        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool AllowScale
        {
            get
            {
                return allowScale;
            }
            set
            {
                allowScale = value;
            }
        }

        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool CodeTabVisible
		{
			get
			{
                return Toolbar.ShowCodeTab;
			}
			set
			{
                Toolbar.ShowCodeTab = value;
			}
		}

        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool DictionaryTabVisible
        {
            get
            {
                return Toolbar.ShowDictionaryTab;
            }
            set
            {
                Toolbar.ShowDictionaryTab = value;
            }
        }

        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool ShowWizardOnStartup
        {
            get
            {
                return StiOptions.Designer.RunWizardAfterLoad;
            }
            set
            {
                StiOptions.Designer.RunWizardAfterLoad = value;
            }
        }

        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool UseCompleteUrl
        {
            get
            {
                return !Connection.RelativeUrls;
            }
            set
            {
                Connection.RelativeUrls = !value;
            }
        }

        [Obsolete("This static option is obsolete. It will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool ExitButtonVisible
        {
            get
            {
                return MainMenu.ShowExitButton;
            }
            set
            {
                MainMenu.ShowExitButton = value;
            }
        }

        #endregion
        
        static StiWebDesignerOptions()
        {
            StiWebHelper.InitWeb();
        }
	}
}
