#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Export;
using System.IO;

namespace Stimulsoft.Report.Web
{
    [ToolboxBitmap(typeof(StiWebDesignerFx), "Designer.Images.StiWebDesigner.bmp")]
    [ToolboxItem(false)]
    public partial class StiWebDesignerFx :
        WebControl,
        INamingContainer
    {
        protected override void OnInit(EventArgs e)
        {
            if (this.RequestParams.Action != StiAction.Undefined &&
                this.RequestParams.Component == StiComponentType.DesignerFx &&
                (this.RequestParams.Id == this.ID || this.RequestParams.Action == StiAction.Resource))
            {
                StiExportSettings settings = null;
                StiWebActionResult result = null;
                this.ClientGuid = this.RequestParams.Cache.ClientGuid;

                switch (this.RequestParams.Action)
                {
                    case StiAction.Resource:
                        result = StiDesignerResourcesHelper.Get(this.RequestParams);
                        break;

                    case StiAction.GetReport:
                        InvokeGetReport();
                        result = StiReportHelperFx.GetReportTemplateResult(this.RequestParams, this.Report, null);
                        break;

                    case StiAction.CreateReport:
                        InvokeCreateReport();
                        result = StiReportHelperFx.GetReportTemplateResult(this.RequestParams, this.Report, null);
                        break;

                    case StiAction.SaveReport:
                        InvokeSaveReport();
                        if (this.SaveReportSendToClient) result = StiReportHelperFx.GetReportTemplateResult(this.RequestParams, this.Report, SaveReportErrorString);
                        else result = StiWebActionResult.StringResult(requestParams, SaveReportErrorString);
                        break;

                    case StiAction.PreviewReport:
                    case StiAction.Variables:
                    case StiAction.Sorting:
                    case StiAction.DrillDown:
                        InvokePreviewReport();
                        result = StiReportHelperFx.GetInteractionResult(this.RequestParams, this.Report);
                        break;

                    case StiAction.PrintReport:
                        settings = GetExportSettings(this.RequestParams);
                        InvokePrintReport(settings);
                        if (settings is StiPdfExportSettings) ((StiPdfExportSettings)settings).AutoPrintMode = StiPdfAutoPrintMode.Dialog;
                        result = StiExportsHelper.ExportReportResult(this.RequestParams, this.Report, settings);
                        break;

                    case StiAction.ExportReport:
                        settings = GetExportSettings(this.RequestParams);
                        InvokeExportReport(settings);
                        result = StiExportsHelper.ExportReportResult(this.RequestParams, this.Report, settings);
                        if (ExportReportResponse != null)
                        {
                            var stream = new MemoryStream(result.Data);
                            InvokeExportReportResponse(settings, stream, result.FileName, result.ContentType);
                            result = new StiWebActionResult(stream, result.ContentType, result.FileName);
                        }
                        break;

                    case StiAction.EmailReport:
                        settings = GetExportSettings(this.RequestParams);
                        StiEmailOptions options = GetEmailOptions(this.RequestParams);
                        InvokeEmailReport(settings, options);
                        result = StiExportsHelper.EmailReportResult(this.RequestParams, this.Report, settings, options);
                        break;

                    case StiAction.GetLocalization:
                        result = StiLocalizationHelperFx.GetLocalizationResult(this.RequestParams);
                        break;

                    case StiAction.TestConnection:
                        result = StiDictionaryHelperFx.TestConnection(this.RequestParams);
                        break;

                    case StiAction.RetrieveColumns:
                        result = StiDictionaryHelperFx.RetrieveColumns(this.RequestParams);
                        break;

                    case StiAction.GetReportCode:
                        result = StiReportHelperFx.GetReportCodeResult(this.RequestParams);
                        break;

                    case StiAction.GetImage:
                        result = StiReportHelperFx.GetComponentImageResult(this.RequestParams);
                        break;

                    case StiAction.Exit:
                        InvokeExit();
                        break;
                }

                if (result != null)
                {
                    bool useBrowserCache = this.RequestParams.Action == StiAction.Resource;
                    StiReportResponse.ResponseBuffer(result.Data, result.ContentType, useBrowserCache, result.FileName);
                }
            }

            base.OnInit(e);
        }

        #region Internal

        /// <summary>
        /// Get the URL for viewer requests
        /// </summary>
        private static string GetRequestUrl(bool useRelativeUrls, bool passQueryParameters)
        {
            if (HttpContext.Current == null) return null;

            string result = HttpContext.Current.Request.Url.AbsoluteUri;
            if (useRelativeUrls) result = HttpContext.Current.Response.ApplyAppPathModifier(
                passQueryParameters ? HttpContext.Current.Request.Url.PathAndQuery : HttpContext.Current.Request.Url.AbsolutePath
            );

            return result;
        }

        /// <summary>
        /// Get the URL for viewer resources
        /// </summary>
        private static string GetResourceUrl(bool useRelativeUrls, bool passQueryParameters)
        {
            string resourceUrl = GetRequestUrl(useRelativeUrls, passQueryParameters);
            resourceUrl += resourceUrl.IndexOf("?") > 0 ? "&" : "?";
            resourceUrl += "stiweb_component=DesignerFx&stiweb_action=Resource&stiweb_data={0}&stiweb_version=" + StiVersionHelper.AssemblyVersion;
            
            return resourceUrl;
        }

        /// <summary>
        /// Create default viewer RequestParams to save the report into cache
        /// </summary>
        private StiRequestParams CreateRequestParams()
        {
            StiRequestParams requestParams = new StiRequestParams();
            requestParams.Action = StiAction.GetReport;
            requestParams.Component = StiComponentType.DesignerFx;
            requestParams.Id = this.ID;
            requestParams.Cache.Timeout = new TimeSpan(0, this.CacheTimeout, 0);
            requestParams.Cache.Mode = this.CacheMode;
            requestParams.Cache.Priority = this.CacheItemPriority;
            requestParams.Cache.ClientGuid = this.ClientGuid;
            requestParams.Cache.Helper = CacheHelper;
            requestParams.Server.UseRelativeUrls = this.UseRelativeUrls;
            requestParams.Server.UseCompression = true;
            requestParams.Server.PassQueryParametersForResources = this.PassQueryParametersForResources;

            return requestParams;
        }

        #endregion

        public StiWebDesignerFx()
        {
            StiWebHelper.InitWeb();

            this.ClientIDMode = System.Web.UI.ClientIDMode.Static;
            if (this.BackColor.IsEmpty) this.BackColor = Color.White;

            this.Width = Unit.Empty;
            this.Height = Unit.Empty;
        }
	}
}
