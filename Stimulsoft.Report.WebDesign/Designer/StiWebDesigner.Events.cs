#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Stimulsoft.Report.Web
{
    public partial class StiWebDesigner :
        WebControl,
        INamingContainer
    {
        #region GetReport

        /// <summary>
        /// The event occurs when the report template is requested after running the designer.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs when the report template is requested after running the designer.")]
        public event StiReportDataEventHandler GetReport;

        private void InvokeGetReport(StiReport currentReport)
        {
            StiReportDataEventArgs e = new StiReportDataEventArgs(this.RequestParams, currentReport);
            OnGetReport(e);
            this.report = GetReportForDesigner(this.RequestParams, e.Report);
        }

        protected virtual void OnGetReport(StiReportDataEventArgs e)
        {
            if (GetReport != null) GetReport(this, e);
        }

        #endregion

        #region OpenReport

        /// <summary>
        /// The event occurs when the report template is opened using the file menu.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs when the report template is opened using the file menu.")]
        public event StiReportDataEventHandler OpenReport;

        private void InvokeOpenReport(StiReport currentReport)
        {
            var e = new StiReportDataEventArgs(this.RequestParams, currentReport);
            OnOpenReport(e);
            this.report = e.Report;
        }

        protected virtual void OnOpenReport(StiReportDataEventArgs e)
        {
            if (OpenReport != null) OpenReport(this, e);
            else if (LoadReport != null) LoadReport(this, e);
        }

        #endregion

        #region CreateReport

        /// <summary>
        /// The event occurs when creating a new report template using the file menu.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs when creating a new report template using the file menu.")]
        public event StiReportDataEventHandler CreateReport;

        private void InvokeCreateReport(StiReport newReport)
        {
            var e = new StiReportDataEventArgs(this.RequestParams, newReport);
            OnCreateReport(e);
            this.report = e.Report;
        }

        protected virtual void OnCreateReport(StiReportDataEventArgs e)
        {
            if (CreateReport != null) CreateReport(this, e);
        }

        #endregion

        #region SaveReport / SaveReportAs

        /// <summary>
        /// The event occurs when saving the report template.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs when saving a report template.")]
        public event StiSaveReportEventHandler SaveReport;

        /// <summary>
        /// The event occurs when saving as file the report template.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs when 'saving as' a report template.")]
        public event StiSaveReportEventHandler SaveReportAs;

        private void InvokeSaveReport(StiReport currentReport)
        {
            var e = new StiSaveReportEventArgs(this.RequestParams, currentReport);

            if (this.RequestParams.Designer.IsSaveAs) OnSaveReportAs(e);
            else OnSaveReport(e);

            if (!string.IsNullOrEmpty(e.ErrorString)) this.SaveReportErrorString = e.ErrorString;
            else if (e.ErrorCode >= 0) this.SaveReportErrorString = "Error at saving. Error code: " + e.ErrorCode.ToString();
            else this.SaveReportErrorString = string.Empty;

            if (e.SendReportToClient) this.report = e.Report;
        }

        protected virtual void OnSaveReport(StiSaveReportEventArgs e)
        {
            if (SaveReport != null) SaveReport(this, e);
        }

        protected virtual void OnSaveReportAs(StiSaveReportEventArgs e)
        {
            if (SaveReportAs != null) SaveReportAs(this, e);
            else if (SaveAsReport != null) SaveAsReport(this, e);
        }

        #endregion

        #region PreviewReport

        /// <summary>
        /// The event occurs before rendering the report for preview. Allowed to change the properties of the report, register the data.
        /// </summary>
        [Category("Designer")]
        [Description("The event occurs before rendering the report for preview. Allowed to change the properties of the report, register the data.")]
        public event StiReportDataEventHandler PreviewReport;

        private void InvokePreviewReport(StiReport previewReport)
        {
            var e = new StiReportDataEventArgs(this.RequestParams, previewReport);
            OnPreviewReport(e);
            this.RequestParams.Report = e.Report; // Set report for LoadReportToViewer command
        }

        protected virtual void OnPreviewReport(StiReportDataEventArgs e)
        {
            if (PreviewReport != null) PreviewReport(this, e);
        }

        #endregion

        #region ExportReport

        /// <summary>
        /// The event occurs before the exporting of the report from the preview tab.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs before the exporting of the report from the preview tab.")]
        public event StiExportReportEventHandler ExportReport;

        private void InvokeExportReport(StiExportReportEventArgs e)
        {
            OnExportReport(e);
        }

        protected virtual void OnExportReport(StiExportReportEventArgs e)
        {
            if (ExportReport != null) ExportReport(this, e);
        }

        /// <summary>
        /// The event occurs after the report is exported from the preview tab.
        /// </summary>
        [Category("Viewer")]
        [Description("The event occurs after the report is exported from the preview tab.")]
        public event StiExportReportResponseEventHandler ExportReportResponse;

        private void InvokeExportReportResponse(StiExportReportResponseEventArgs e)
        {
            OnExportReportResponse(e);
        }

        protected virtual void OnExportReportResponse(StiExportReportResponseEventArgs e)
        {
            if (ExportReportResponse != null) ExportReportResponse(this, e);
        }

        #endregion

        #region Exit

        /// <summary>
        /// Fires when the user pressed Exit in the file menu.
        /// </summary>
        [Category("Designer")]
        [Description("Fires when the user pressed Exit in the file menu.")]
        public event StiReportDataEventHandler Exit;

        private void InvokeExit(StiReport currentReport)
        {
            var e = new StiReportDataEventArgs(this.RequestParams, currentReport);
            OnExit(e);
        }

        protected virtual void OnExit(StiReportDataEventArgs e)
        {
            if (Exit != null) Exit(this, e);
            else if (ExitDesigner != null) ExitDesigner(this, e);
        }
        
        #endregion
        

        #region Obsolete

        #region StiGetReportEventArgs

        [Obsolete("This delegate is obsolete. Please use StiReportDataEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiGetReportEventHandler(object sender, StiGetReportEventArgs e);

        [Obsolete("This class is obsolete. Please use StiReportDataEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiGetReportEventArgs : StiReportDataEventArgs
        {
            private DataSet reportDataSet = null;
            [Obsolete("This property is obsolete. Please use e.Report.RegData(dataSet) method instead.")]
            public DataSet ReportDataSet
            {
                get
                {
                    return reportDataSet;
                }
                set
                {
                    reportDataSet = value;
                    if (this.Report != null) this.Report.RegData(value);
                }
            }

            public StiGetReportEventArgs(StiRequestParams reqestParams, StiReport report) : base(reqestParams, report)
            {
            }
        }

        #endregion

        #region StiCreateReportEventArgs

        [Obsolete("This delegate is obsolete. Please use StiReportDataEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiCreateReportEventHandler(object sender, StiCreateReportEventArgs e);

        [Obsolete("This class is obsolete. Please use StiReportDataEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiCreateReportEventArgs : StiReportDataEventArgs
        {
            private DataSet reportDataSet = null;
            [Obsolete("This property is obsolete. Please use e.Report.RegData(dataSet) method instead.")]
            public DataSet ReportDataSet
            {
                get
                {
                    return reportDataSet;
                }
                set
                {
                    reportDataSet = value;
                    if (this.Report != null) this.Report.RegData(value);
                }
            }
            
            public StiCreateReportEventArgs(StiRequestParams reqestParams, StiReport report) : base(reqestParams, report)
            {
            }
        }

        #endregion

        #region LoadReport

        [Obsolete("This delegate is obsolete. Please use StiReportDataEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public delegate void StiLoadReportEventHandler(object sender, StiReportDataEventArgs e);

        [Obsolete("This class is obsolete. Please use StiReportDataEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiLoadReportEventArgs : StiReportDataEventArgs
        {
            private DataSet reportDataSet = null;
            [Obsolete("This property is obsolete. Please use e.Report.RegData(dataSet) method instead.")]
            public DataSet ReportDataSet
            {
                get
                {
                    return reportDataSet;
                }
                set
                {
                    reportDataSet = value;
                    if (this.Report != null) this.Report.RegData(value);
                }
            }

            public StiLoadReportEventArgs(StiRequestParams reqestParams, StiReport report) : base(reqestParams, report)
            {
            }
        }

        [Obsolete("This event is obsolete. Please use OpenReport event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiLoadReportEventHandler LoadReport;

        [Obsolete("This class is obsolete. Please use StiReportDataEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected virtual void OnLoadReport(StiReportDataEventArgs e)
        {
            OnOpenReport(e);
        }

        #endregion

        #region StiSaveAsReportEventArgs

        [Obsolete("This event is obsolete. Please use SaveReportAs event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiSaveReportEventHandler SaveAsReport;

        [Obsolete("This delegate is obsolete. Please use StiSaveReportEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public delegate void StiSaveAsReportEventHandler(object sender, StiSaveAsReportEventArgs e);

        [Obsolete("This class is obsolete. Please use StiSaveReportEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiSaveAsReportEventArgs : StiSaveReportEventArgs
        {
            [Obsolete("This property is obsolete. Please use FileName property instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            public string ReportFile
            {
                get
                {
                    return this.FileName;
                }
            }

            public StiSaveAsReportEventArgs(StiRequestParams reqestParams, StiReport report) : base(reqestParams, report)
            {
            }
        }

        #endregion 

        #region StiPreviewReportEventArgs

        [Obsolete("This delegate is obsolete. Please use StiReportDataEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public delegate void StiPreviewReportEventHandler(object sender, StiPreviewReportEventArgs e);

        [Obsolete("This class is obsolete. Please use StiReportDataEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiPreviewReportEventArgs : StiReportDataEventArgs
        {
            private DataSet reportDataSet = null;
            [Obsolete("This property is obsolete. Please use e.Report.RegData(dataSet) method instead.")]
            public DataSet ReportDataSet
            {
                get
                {
                    return reportDataSet;
                }
                set
                {
                    reportDataSet = value;
                    if (this.Report != null) this.Report.RegData(value);
                }
            }
            
            public StiPreviewReportEventArgs(StiRequestParams reqestParams, StiReport report) : base(reqestParams, report)
            {
            }
        }

        #endregion

        #region ExitDesigner

        [Obsolete("This event is obsolete. Please use Exit event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public event StiExitDesignerEventHandler ExitDesigner;

        [Obsolete("This event is obsolete. Please use Exit event instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        protected virtual void OnExitDesigner(StiReportDataEventArgs e)
        {
            if (ExitDesigner != null) ExitDesigner(this, e);
        }

        [Obsolete("This delegate is obsolete. Please use StiSaveReportEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public delegate void StiExitDesignerEventHandler(object sender, StiReportDataEventArgs e);

        [Obsolete("This class is obsolete. Please use StiSaveReportEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiExitDesignerEventArgs : StiExitEventArgs
        {
            public StiExitDesignerEventArgs(StiRequestParams reqestParams, StiReport report) : base(reqestParams, report)
            {
            }
        }

        #endregion

        #region StiExitEventArgs

        [Obsolete("This delegate is obsolete. Please use StiSaveReportEventHandler instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public delegate void StiExitEventHandler(object sender, StiExitEventArgs e);

        [Obsolete("This class is obsolete. Please use StiSaveReportEventArgs instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiExitEventArgs : StiReportDataEventArgs
        {
            public StiExitEventArgs(StiRequestParams reqestParams, StiReport report) : base(reqestParams, report)
            {
            }
        }

        #endregion

        #endregion
    }
}
