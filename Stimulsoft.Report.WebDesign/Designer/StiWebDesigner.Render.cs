#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Helpers;
using Stimulsoft.Base.Licenses;
using System.Globalization;
using System.Security.Cryptography;
using Stimulsoft.Report.Components.TextFormats;
using System.Web;
using Stimulsoft.Base;
using System.Text;
using Stimulsoft.Base.Cloud;
using Stimulsoft.Report.Dashboard;

#if SERVER
using Stimulsoft.Server.Objects;
#endif

namespace Stimulsoft.Report.Web
{
    public partial class StiWebDesigner :
        WebControl,
        INamingContainer
    {
        #region JSON parameters

        private string RenderJsonParameters()
        {
#if SERVER
            var isTrial = StiVersionX.IsSvr;
#else
            #region LicenseKey
            var key = StiLicenseKeyValidator.GetLicenseKey();
            var isTrial = !StiLicenseKeyValidator.IsValidInDesignerOrOnSpecifiedPlatform(StiProductIdent.Web, key);
            if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key))isTrial = true;

            #region IsValidLicenseKey
            if (!isTrial)
            {
                try
                {
                    using (var rsa = new RSACryptoServiceProvider(512))
                    using (var sha = new SHA1CryptoServiceProvider())
                    {
                        rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                        isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
                    }
                }
                catch (Exception)
                {
                    isTrial = true;
                }
            }
            #endregion
            #endregion
#endif

            var productVersion = StiVersionHelper.ProductVersion.Trim();
            if (!isTrial) productVersion += " ";

            // Create JS parameters table
            Hashtable parameters = new Hashtable();
            Hashtable designParams = null;

            //Set Theme and Localization for Demo mode
            if (DemoMode)
            {
                string themeName = Page.Request.Params.Get("themename");
                if (themeName != null)
                {
                    this.Theme = (StiDesignerTheme)Enum.Parse(typeof(StiDesignerTheme), themeName);
                    this.Viewer.Theme = (StiViewerTheme)Enum.Parse(typeof(StiViewerTheme), themeName);
                }

                string localizationName = Page.Request.Params.Get("localization");
                if (localizationName != null)
                {
                    this.Localization = localizationName;
                    this.Viewer.Localization = localizationName;
                }
            }

            //Is Online Mode
            if (CloudMode)
            {
                //Parse parameters
                designParams = new Hashtable();
                productVersion = productVersion.Trim();

                if (Page.Request.Params["params"] != null)
                {
                    try
                    {
                        string paramsStr = (Page.Request.Params["params"] as string).Replace(" ", "+");
                        paramsStr = Encoding.UTF8.GetString(Convert.FromBase64String(paramsStr));
                        string[] paramsItems = paramsStr.Split(';');

                        if (paramsItems[0] as string == "demomode")
                        {
                            //Run from demo.stimulsoft.com
                            designParams["demomode"] = true;
                        }
                        else
                        {
                            //Run from cloud.stimulsoft.com
                            if (paramsItems[0].Length >= 32) designParams["sessionKey"] = paramsItems[0].Substring(0, 32);

                            if (paramsItems[0].Length >= 33 && paramsItems[0].Substring(32, 1) == "0")
                                productVersion += " ";

                            if (paramsItems[0].Length >= 65) designParams["userKey"] = paramsItems[0].Substring(33, 32);

                            if (paramsItems[0].Length >= 66 && !string.IsNullOrEmpty(paramsItems[0].Substring(65)))
                                designParams["reportTemplateItemKey"] = paramsItems[0].Substring(65);
                        }

                        //Set Localization
                        if (paramsItems.Length > 1)
                        {
                            string localizationName = paramsItems[1] as string;
                            designParams["localizationName"] = localizationName;
                        }

                        //Set Theme
                        if (paramsItems.Length > 2)
                        {
                            string themeName = paramsItems[2] as string;
                            designParams["themeName"] = themeName;
                            this.Theme = (StiDesignerTheme)Enum.Parse(typeof(StiDesignerTheme), themeName);
                            this.Viewer.Theme = (StiViewerTheme)Enum.Parse(typeof(StiViewerTheme), themeName);
                        }

                        if (paramsItems.Length > 3) designParams["reportName"] = paramsItems[3];
                        if (paramsItems.Length > 4 && paramsItems[4] as string != "null") designParams["versionKey"] = paramsItems[4];
                    }
                    catch (Exception e)
                    {
                        Console.Write(e.Message);
                    }
                }

                //Additional parameters
                designParams["restUrl"] = CloudServerAdress;
                designParams["isOnlineVersion"] = true;
                designParams["permissionDataSources"] = "All";
                designParams["favIcon"] = "favicon.ico";
                designParams["maxResourceSizeDeveloper"] = StiCloudConsts.Developer.MaxResourceSize;
                designParams["maxResourceSizeTr"] = StiCloudConsts.Trial.MaxResourceSize;
                designParams["maxItemsTr"] = StiCloudConsts.Trial.MaxItems;

                parameters["cloudParameters"] = designParams;
            }

            // Try to load localization file
            if (!IsDesignMode) StiCollectionsHelper.LoadLocalizationFile(HttpContext.Current, this.LocalizationDirectory, this.Localization);

            // Settings: General
            parameters["mobileDesignerId"] = this.ClientID;
            parameters["viewerId"] = this.ID + "Viewer";
            parameters["cloudMode"] = this.CloudMode;
            parameters["demoMode"] = this.DemoMode;
            //parameters["dVers"] = CloudMode && designParams != null ? designParams.Contains("dVers") : isTrial;
            parameters["theme"] = this.Theme;
            parameters["cultureName"] = StiLocalization.CultureName;
            parameters["productVersion"] = productVersion;
            parameters["shortProductVersion"] = StiVersionHelper.AssemblyVersion;
            parameters["frameworkType"] = "ASP.NET";
            parameters["dashboardAssemblyLoaded"] = StiDashboardAssembly.IsAssemblyLoaded && StiDashboardExportAssembly.IsAssemblyLoaded && StiDashboardDrawingAssembly.IsAssemblyLoaded;

            // Settings: Report cache
            parameters["clientGuid"] = this.ClientGuid;
            parameters["reportGuid"] = this.ClientGuid;
            parameters["cacheMode"] = this.CacheMode;
            parameters["requestTimeout"] = this.RequestTimeout;
            parameters["cacheTimeout"] = this.CacheTimeout;
            parameters["cacheItemPriority"] = this.CacheItemPriority;
            parameters["useRelativeUrls"] = this.UseRelativeUrls;
            parameters["allowAutoUpdateCache"] = this.AllowAutoUpdateCache;
            parameters["cacheHelper"] = CacheHelper != null;
            parameters["useCacheForResources"] = this.UseCacheForResources;

            // Settings: URLs
            string resourcesUrl = this.GetResourcesUrl();
            parameters["useCompression"] = this.UseCompression;
            parameters["passQueryParametersForResources"] = this.PassQueryParametersForResources;
            parameters["cloudServerUrl"] = StiHyperlinkProcessor.ServerIdent;
            parameters["requestUrl"] = GetRequestUrl(this.UseRelativeUrls, true);
            parameters["stylesUrl"] = string.IsNullOrEmpty(this.CustomCss) ? resourcesUrl.Replace("&stiweb_data=", "&stiweb_theme=") + this.Theme.ToString() : this.CustomCss;
            parameters["scriptsUrl"] = resourcesUrl;

            // Settings
            parameters["showAnimation"] = this.ShowAnimation;
            parameters["defaultUnit"] = this.DefaultUnit;
            parameters["focusingX"] = this.FocusingX;
            parameters["focusingY"] = this.FocusingY;
            parameters["helpLanguage"] = (this.CloudMode && designParams != null && designParams["localizationName"] != null)
                ? designParams["localizationName"] 
                : (StiLocalization.CultureName == "ru" ? "ru" : "en");
            parameters["showSaveDialog"] = this.ShowSaveDialog;
            parameters["fullScreenMode"] = this.Width == Unit.Empty && this.Height == Unit.Empty;
            parameters["haveExitDesignerEvent"] = this.Exit != null;
            parameters["haveSaveEvent"] = this.SaveReport != null;
            parameters["haveSaveAsEvent"] = this.SaveReportAs != null || this.SaveAsReport != null;
            parameters["showTooltips"] = this.ShowTooltips;
            parameters["showTooltipsHelp"] = this.ShowTooltipsHelp;
            parameters["showDialogHelp"] = this.ShowDialogsHelp;
            parameters["interfaceType"] = this.InterfaceType;
            parameters["undoMaxLevel"] = this.UndoMaxLevel;
            parameters["resourceIdent"] = StiHyperlinkProcessor.ResourceIdent;
            parameters["variableIdent"] = StiHyperlinkProcessor.VariableIdent;
            parameters["defaultDesignerOptions"] = StiDesignerOptionsHelper.GetDefaultDesignerOptions();
            parameters["showPropertiesWhichUsedFromStyles"] = StiOptions.Designer.PropertyGrid.ShowPropertiesWhichUsedFromStyles;
            parameters["runWizardAfterLoad"] = StiOptions.Designer.RunWizardAfterLoad;
            parameters["datePickerFirstDayOfWeek"] = this.DatePickerFirstDayOfWeek;
            parameters["reportResourcesMaximumSize"] = StiOptions.Engine.ReportResources.MaximumSize;
            parameters["allowChangeWindowTitle"] = this.AllowChangeWindowTitle;
            parameters["saveReportMode"] = this.SaveReportMode;
            parameters["saveReportAsMode"] = this.SaveReportAsMode;
            parameters["checkReportBeforePreview"] = this.CheckReportBeforePreview;

            if (ComponentsIntoInsertTab != null)
            {
                ArrayList componentsArray = new ArrayList();
                componentsArray.AddRange(ComponentsIntoInsertTab);
                parameters["componentsIntoInsertTab"] = componentsArray;
            }

            // Collections
            string[] collections = new string[] {
                "images", "loc", "locFiles", "paperSizes", "hatchStyles", "summaryTypes", "aggrigateFunctions", "fontNames", "conditions", "iconSetArrays",
                "dBaseCodePages", "csvCodePages", "textFormats", "currencySymbols", "dateFormats", "timeFormats", "customFormats", "cultures"
            };

            for (int i = 0; i < collections.Length; i++)
            {
                object collectionObject = null;
                switch (collections[i])
                {
                    case "loc":
                        if (CloudMode && Page.Request.Params["localizationName"] != null && Page.Request.Params["sessionKey"] != null)
                        {
#if SERVER
                            var getLocalizationCommand = new StiLocalizationCommands.Get()
                            {
                                Name = Page.Request.Params["localizationName"],
                                SessionKey = Page.Request.Params["sessionKey"],
                                Set = StiLocalizationSet.Reports,
                                Type = StiLocalizationFormatType.Json
                            };

                            var resultLocalizationCommand = RunCommand(getLocalizationCommand) as StiLocalizationCommands.Get;
                            if (resultLocalizationCommand.ResultSuccess && resultLocalizationCommand.ResultReportsJson != null)
                                collectionObject = StiEncodingHelper.DecodeString(resultLocalizationCommand.ResultReportsJson);
#endif
                        }
                        else
                        {
                            collectionObject = StiLocalization.GetLocalization(false);
                        }
                        break;

                    case "locFiles": collectionObject = StiCollectionsHelper.GetLocalizationsList(HttpContext.Current, this.LocalizationDirectory); break;
                    case "images": collectionObject = StiDesignerResourcesHelper.GetImagesArray(this.CreateRequestParams(), resourcesUrl); break;
                    case "paperSizes": collectionObject = StiPaperSizes.GetItems(); break;
                    case "hatchStyles": collectionObject = StiHatchStyles.GetItems(); break;
                    case "summaryTypes": collectionObject = StiSummaryTypes.GetItems(); break;
                    case "aggrigateFunctions": collectionObject = StiAggrigateFunctions.GetItems(); break;
                    case "fontNames": collectionObject = StiFontNames.GetItems(); break;
                    case "conditions": collectionObject = StiDefaultConditions.GetItems(); break;
                    case "iconSetArrays": collectionObject = StiIconSetArrays.GetItems(); break;
                    case "dBaseCodePages": collectionObject = StiCodePageHelper.GetDBaseCodePageItems(); break;
                    case "csvCodePages": collectionObject = StiCodePageHelper.GetCsvCodePageItems(); break;
                    case "textFormats": collectionObject = StiTextFormatHelper.GetTextFormatItems(); break;
                    case "currencySymbols": collectionObject = StiTextFormatHelper.GetCurrencySymbols(); break;
                    case "dateFormats": collectionObject = StiTextFormatHelper.GetDateAndTimeFormats("date", new StiDateFormatService()); break;
                    case "timeFormats": collectionObject = StiTextFormatHelper.GetDateAndTimeFormats("time", new StiTimeFormatService()); break;
                    case "customFormats": collectionObject = StiTextFormatHelper.GetDateAndTimeFormats("custom", new StiCustomFormatService()); break;
                    case "cultures": collectionObject = StiCultureHelper.GetItems(CultureTypes.SpecificCultures); break;
                }

                parameters[collections[i]] = collectionObject;
            }

            // ToolBar
            parameters["showInsertButton"] = ShowInsertButton;
            parameters["showLayoutButton"] = ShowLayoutButton;
            parameters["showPageButton"] = ShowPageButton;
            parameters["showPreviewButton"] = ShowPreviewButton;
            parameters["showFileMenu"] = ShowFileMenu;
            parameters["showSaveButton"] = ShowSaveButton;
            parameters["showAboutButton"] = ShowAboutButton;
            parameters["showSetupToolboxButton"] = ShowSetupToolboxButton;
            parameters["showFileMenuNew"] = ShowFileMenuNew;
            parameters["showFileMenuOpen"] = ShowFileMenuOpen;
            parameters["showFileMenuSave"] = ShowFileMenuSave;
            parameters["showFileMenuSaveAs"] = ShowFileMenuSaveAs;
            parameters["showFileMenuClose"] = ShowFileMenuClose;
            parameters["showFileMenuExit"] = ShowFileMenuExit;
            parameters["showFileMenuReportSetup"] = ShowFileMenuReportSetup;
            parameters["showFileMenuOptions"] = ShowFileMenuOptions;
            parameters["showFileMenuInfo"] = ShowFileMenuInfo;
            parameters["showFileMenuAbout"] = ShowFileMenuAbout;
            parameters["showFileMenuHelp"] = ShowFileMenuHelp;

            // Bands
            Hashtable visibilityBands = new Hashtable();
            parameters["visibilityBands"] = visibilityBands;
            visibilityBands["StiReportTitleBand"] = ShowReportTitleBand;
            visibilityBands["StiReportSummaryBand"] = ShowReportSummaryBand;
            visibilityBands["StiPageHeaderBand"] = ShowPageHeaderBand;
            visibilityBands["StiPageFooterBand"] = ShowPageFooterBand;
            visibilityBands["StiGroupHeaderBand"] = ShowGroupHeaderBand;
            visibilityBands["StiGroupFooterBand"] = ShowGroupFooterBand;
            visibilityBands["StiHeaderBand"] = ShowHeaderBand;
            visibilityBands["StiFooterBand"] = ShowFooterBand;
            visibilityBands["StiColumnHeaderBand"] = ShowColumnHeaderBand;
            visibilityBands["StiColumnFooterBand"] = ShowColumnFooterBand;
            visibilityBands["StiDataBand"] = ShowDataBand;
            visibilityBands["StiHierarchicalBand"] = ShowHierarchicalBand;
            visibilityBands["StiChildBand"] = ShowChildBand;
            visibilityBands["StiEmptyBand"] = ShowEmptyBand;
            visibilityBands["StiOverlayBand"] = ShowOverlayBand;
            visibilityBands["StiTable"] = ShowTable;

            // Bands
            Hashtable visibilityCrossBands = new Hashtable();
            parameters["visibilityCrossBands"] = visibilityCrossBands;
            visibilityCrossBands["StiCrossTab"] = ShowCrossTab;
            visibilityCrossBands["StiCrossGroupHeaderBand"] = ShowCrossGroupHeaderBand;
            visibilityCrossBands["StiCrossGroupFooterBand"] = ShowCrossGroupFooterBand;
            visibilityCrossBands["StiCrossHeaderBand"] = ShowCrossHeaderBand;
            visibilityCrossBands["StiCrossFooterBand"] = ShowCrossFooterBand;
            visibilityCrossBands["StiCrossDataBand"] = ShowCrossDataBand;

            // Components
            Hashtable visibilityComponents = new Hashtable();
            parameters["visibilityComponents"] = visibilityComponents;
            visibilityComponents["StiText"] = ShowText;
            visibilityComponents["StiTextInCells"] = ShowTextInCells;
            visibilityComponents["StiRichText"] = ShowRichText;
            visibilityComponents["StiImage"] = ShowImage;
            visibilityComponents["StiBarCode"] = ShowBarCode;
            visibilityComponents["StiShape"] = ShowShape;
            visibilityComponents["StiPanel"] = ShowPanel;
            visibilityComponents["StiClone"] = ShowClone;
            visibilityComponents["StiCheckBox"] = ShowCheckBox;
            visibilityComponents["StiSubReport"] = ShowSubReport;
            visibilityComponents["StiZipCode"] = ShowZipCode;
            visibilityComponents["StiChart"] = ShowChart;
            visibilityComponents["StiMap"] = ShowMap;
            visibilityComponents["StiGauge"] = ShowGauge;
            visibilityComponents["StiHorizontalLinePrimitive"] = ShowHorizontalLinePrimitive;
            visibilityComponents["StiVerticalLinePrimitive"] = ShowVerticalLinePrimitive;
            visibilityComponents["StiRectanglePrimitive"] = ShowRectanglePrimitive;
            visibilityComponents["StiRoundedRectanglePrimitive"] = ShowRoundedRectanglePrimitive;

            // DashboardElements
            Hashtable visibilityDashboardElements = new Hashtable();
            parameters["visibilityDashboardElements"] = visibilityDashboardElements;
            visibilityDashboardElements["StiTableElement"] = ShowTableElement;
            visibilityDashboardElements["StiChartElement"] = ShowChartElement;
            visibilityDashboardElements["StiGaugeElement"] = ShowGaugeElement;
            visibilityDashboardElements["StiPivotElement"] = ShowPivotElement;
            visibilityDashboardElements["StiIndicatorElement"] = ShowIndicatorElement;
            visibilityDashboardElements["StiProgressElement"] = ShowProgressElement;
            visibilityDashboardElements["StiMapElement"] = ShowMapElement;
            visibilityDashboardElements["StiImageElement"] = ShowImageElement;
            visibilityDashboardElements["StiTextElement"] = ShowTextElement;
            visibilityDashboardElements["StiPanelElement"] = ShowPanelElement;
            visibilityDashboardElements["StiShapeElement"] = ShowShapeElement;

            // Properties Grid
            parameters["showPropertiesGrid"] = ShowPropertiesGrid;
            parameters["propertiesGridWidth"] = PropertiesGridWidth;
            parameters["propertiesGridLabelWidth"] = PropertiesGridLabelWidth;

            // Dictionary
            parameters["showDictionary"] = ShowDictionary;
            parameters["permissionDataSources"] = CloudMode && designParams != null ? (string)designParams["permissionDataSources"] : PermissionDataSources.ToString();
            parameters["permissionDataColumns"] = CloudMode && designParams != null ? (string)designParams["permissionDataSources"] : PermissionDataColumns.ToString();
            parameters["permissionDataRelations"] = CloudMode && designParams != null ? (string)designParams["permissionDataSources"] : PermissionDataRelations.ToString();
            parameters["permissionDataConnections"] = CloudMode && designParams != null ? (string)designParams["permissionDataSources"] : PermissionDataConnections.ToString();
            parameters["permissionBusinessObjects"] = CloudMode && designParams != null ? "None" : PermissionBusinessObjects.ToString();
            parameters["permissionVariables"] = PermissionVariables.ToString();
            parameters["permissionResources"] = PermissionResources.ToString();
            parameters["permissionSqlParameters"] = PermissionSqlParameters.ToString();

            parameters["showOnlyAliasForComponents"] = StiOptions.Dictionary.ShowOnlyAliasForComponents;
            parameters["showOnlyAliasForPages"] = StiOptions.Dictionary.ShowOnlyAliasForPages;
            parameters["showOnlyAliasForDatabase"] = StiOptions.Dictionary.ShowOnlyAliasForDatabase;
            parameters["showOnlyAliasForData"] = StiOptions.Dictionary.ShowOnlyAliasForData;
            parameters["showOnlyAliasForVariable"] = StiOptions.Dictionary.ShowOnlyAliasForVariable;
            parameters["showOnlyAliasForResource"] = StiOptions.Dictionary.ShowOnlyAliasForResource;
            parameters["showOnlyAliasForDataSource"] = StiOptions.Dictionary.ShowOnlyAliasForDataSource;
            parameters["showOnlyAliasForBusinessObject"] = StiOptions.Dictionary.ShowOnlyAliasForBusinessObject;
            parameters["showOnlyAliasForDataColumn"] = StiOptions.Dictionary.ShowOnlyAliasForDataColumn;
            parameters["showOnlyAliasForDataRelation"] = StiOptions.Dictionary.ShowOnlyAliasForDataRelation;

            // Report Tree
            parameters["showReportTree"] = ShowReportTree;

            // Cursors
            StiRequestParams requestParams = this.CreateRequestParams();
            parameters["urlCursorStyleSet"] = GetImageUrl(requestParams, resourcesUrl + "Cursors.StyleSet.cur");
            parameters["urlCursorPen"] = GetImageUrl(requestParams, resourcesUrl + "Cursors.Pen.cur");
            
            //JS Helper
            //GetResourcesForJsDesigner(parameters);
            //------
            
            return JSON.Encode(parameters);
        }

        #endregion
        
        #region Override Methods

        protected override void CreateChildControls()
        {
            this.Controls.Clear();

            Panel mainPanel = new Panel();
            mainPanel.ID = this.ID + "_MainPanel";
            mainPanel.CssClass = "stiDesignerMainPanel";
            this.Controls.Add(mainPanel);

            if (this.Width == Unit.Empty && this.Height == Unit.Empty)
            {
                this.Height = Unit.Empty;
                this.Style.Add("position", "absolute");
                this.Style.Add("top", "0");
                this.Style.Add("right", "0");
                this.Style.Add("bottom", "0");
                this.Style.Add("left", "0");
            }
            else
            {
                mainPanel.Width = this.Width;
                mainPanel.Height = this.Height;
            }

            if (!IsDesignMode)
            {
                this.Controls.Add(this.Viewer);

                var jsParameters = RenderJsonParameters();
                var initScript = new StiJavaScript();
                initScript.Text = string.Format("var js{0} = new StiMobileDesigner({1});", this.ID, jsParameters);
                mainPanel.Controls.Add(initScript);
            }
            
            base.CreateChildControls();
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            #region Design Mode
            if (IsDesignMode)
            {
                StiRequestParams requestParams = this.CreateRequestParams();

                Panel panel = new Panel();
                panel.Width = this.Width == Unit.Empty ? Unit.Percentage(100) : this.Width;
                panel.Height = this.Height == Unit.Empty ? Unit.Percentage(100) : this.Height;
                panel.Style.Add("overflow", "hidden");

                Table mainTable = new Table();
                mainTable.CellPadding = 0;
                mainTable.CellSpacing = 0;
                mainTable.Width = Unit.Percentage(100);
                mainTable.Height = Unit.Percentage(100);
                mainTable.BorderColor = this.BorderColor.IsEmpty ? Color.DarkGray : this.BorderColor;
                mainTable.BorderWidth = this.BorderWidth.IsEmpty ? 2 : this.BorderWidth;
                mainTable.BorderStyle = this.BorderStyle == BorderStyle.NotSet ? BorderStyle.Solid : this.BorderStyle;
                panel.Controls.Add(mainTable);

                TableRow rowToolbar = new TableRow();
                rowToolbar.VerticalAlign = VerticalAlign.Top;
                mainTable.Rows.Add(rowToolbar);

                TableCell cellToolbarLeft = new TableCell();
                cellToolbarLeft.Style.Add("width", "966px");
                cellToolbarLeft.Style.Add("height", "133px");
                cellToolbarLeft.Style.Add("background", string.Format("url('{0}')", GetImageUrl(requestParams, "DesignToolbarLeftHalf.png")));
                rowToolbar.Cells.Add(cellToolbarLeft);

                TableCell cellToolbarMiddle = new TableCell();
                cellToolbarMiddle.Style.Add("height", "133px");
                cellToolbarMiddle.Style.Add("background", string.Format("url('{0}')", GetImageUrl(requestParams, "DesignToolbarMiddleHalf.png")));
                rowToolbar.Cells.Add(cellToolbarMiddle);

                TableCell cellToolbarRight = new TableCell();
                cellToolbarRight.Style.Add("width", "24px");
                cellToolbarRight.Style.Add("height", "133px");
                rowToolbar.Cells.Add(cellToolbarRight);

                Panel rightPanel = new Panel();
                rightPanel.Style.Add("width", "24px");
                rightPanel.Style.Add("height", "133px");
                rightPanel.Style.Add("background", string.Format("url('{0}')", GetImageUrl(requestParams, "DesignToolbarRightHalf.png")));
                cellToolbarRight.Controls.Add(rightPanel);

                TableRow rowCaption = new TableRow();
                mainTable.Rows.Add(rowCaption);
                
                TableCell cellCaption = new TableCell();
                cellCaption.ColumnSpan = 3;
                cellCaption.Height = Unit.Percentage(100);
                cellCaption.Font.Name = "Arial";
                cellCaption.Text = "<strong>HTML5 Web Designer</strong><br />" + this.ID;
                cellCaption.HorizontalAlign = HorizontalAlign.Center;
                rowCaption.Cells.Add(cellCaption);

                panel.RenderControl(output);
            }
            #endregion

            #region Runtime Mode
            else
            {
                base.RenderContents(output);
            }
            #endregion
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            if (!IsDesignMode)
            {
                this.EnsureChildControls();

                string url = this.GetResourcesUrl();
                string script = string.Format("<script type=\"text/javascript\" src=\"{0}DesignerScripts\"></script>", url);
                RegisterClientScriptBlockIntoHeader(url, script);
            }

            base.OnPreRender(e);
        }
        
        #endregion
    }
}
