#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Export;
using System.Data;
using System.Collections;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Helpers;
using System.Collections.Generic;

#if SERVER
using Stimulsoft.Server.Objects;
using Stimulsoft.Server;
using Stimulsoft.Server.Reports.Objects;
#endif

namespace Stimulsoft.Report.Web
{
    public partial class StiWebDesigner :
        WebControl,
        INamingContainer
    {
#region Stimulsoft Reports.Server Client Methods
#if SERVER
        private StiCommand RunCommand(StiCommand command)
        {
            return StiCommandToServer.RunCommand(command);
        }

        private void CreateDataSourcesFromAttachedItem(StiReport report, ArrayList attachedItems, string sessionKey)
        {
            if (attachedItems != null && attachedItems.Count > 0)
            {
                List<string> attachedKeys = new List<string>();
                foreach (Hashtable attachedItem in attachedItems)
                {
                    attachedKeys.Add(attachedItem["key"] as string);
                }

                //StiDataWorker.Report.RegisterData(report, attachedKeys, null);
            }
        }

        public Hashtable GetReportAttachedItems(ArrayList itemKeys, string sessionKey)
        {
            Hashtable attachedItems = new Hashtable();

            if (itemKeys != null && itemKeys.Count > 0)
            {
                var commands = new List<StiCommand>();

                for (int i = 0; i < itemKeys.Count; i++)
                {
                    var getItemCommand = new StiItemCommands.Get
                    {
                        ItemKey = (string)itemKeys[i],
                        SessionKey = sessionKey
                    };
                    commands.Add(getItemCommand);
                }

                var runCommandList = new StiCommandListCommands.Run
                {
                    Commands = commands,
                    ContinueAfterError = true,
                    SessionKey = sessionKey
                };

                var resultCommandList = RunCommand(runCommandList) as StiCommandListCommands.Run;

                if (resultCommandList == null || !resultCommandList.ResultSuccess)
                {
                    return null;
                }

                if (resultCommandList.ResultCommands != null)
                {
                    foreach (var stiCommand in runCommandList.ResultCommands)
                    {
                        var command = (StiItemCommands.Get)stiCommand;
                        if (!command.ResultSuccess && command.ResultItem != null) continue;
                        if (!StiAttachedItemHelper.CanAttachToReportTemplate(command.ResultItem)) continue;

                        string itemGroup = StiAttachedItemHelper.GetDictionaryCloudItemsGroupName(command.ResultItem);
                        if (itemGroup != "Unknown")
                        {
                            Hashtable item = new Hashtable();
                            item["name"] = command.ResultItem.Name;
                            item["description"] = command.ResultItem.Description;
                            item["key"] = command.ResultItem.Key;
                            item["typeItem"] = itemGroup;
                            item["typeIcon"] = "Cloud" + itemGroup;
                            item["isCloudAttachedItem"] = true;
                            if (command.ResultItem is StiFileItem) item["fileType"] = ((StiFileItem)command.ResultItem).FileType;

                            if (attachedItems[itemGroup] == null) attachedItems[itemGroup] = new ArrayList();
                            ((ArrayList)attachedItems[itemGroup]).Add(item);
                        }
                    }
                }
            }

            return attachedItems;
        }

        public static void AddResourcesToReport(StiReport report, ArrayList resourceItems, string sessionKey)
        {
            if (resourceItems != null && resourceItems.Count > 0)
            {
                var commands = new List<StiCommand>();

                foreach (Hashtable resourceItem in resourceItems)
                {
                    var getItemCommand = new StiItemResourceCommands.Get
                    {
                        ItemKey = resourceItem["key"] as string,
                        SessionKey = sessionKey
                    };
                    commands.Add(getItemCommand);
                }

                var runCommandList = new StiCommandListCommands.Run
                {
                    Commands = commands,
                    ContinueAfterError = true,
                    SessionKey = sessionKey
                };
                var resultCommandList = StiCommandToServer.RunCommand(runCommandList) as StiCommandListCommands.Run;

                if (resultCommandList.ResultCommands != null)
                {
                    foreach (var stiCommand in runCommandList.ResultCommands)
                    {
                        int itemIndex = runCommandList.ResultCommands.IndexOf(stiCommand);
                        if (itemIndex < resourceItems.Count)
                        {
                            var resultResourceCommand = (StiItemResourceCommands.Get)stiCommand;
                            if (resultResourceCommand.ResultSuccess && resultResourceCommand.ResultResource != null)
                            {
                                string fileName = ((Hashtable)resourceItems[itemIndex])["fileName"] as string;
                                string itemKey = ((Hashtable)resourceItems[itemIndex])["key"] as string;
                                string resourceName = fileName.Substring(0, fileName.IndexOf("."));
                                if (String.IsNullOrEmpty(resourceName)) resourceName = StiNameCreation.CreateResourceName(report, "Resource");

                                StiResource newResource = new StiResource()
                                {
                                    Name = resourceName,
                                    Content = resultResourceCommand.ResultResource,
                                    Type = StiReportResourcesHelper.GetResourceTypeByFileName(fileName)
                                };
                                report.Dictionary.Resources.Add(newResource);
                                                                
                                //Add datasources to report
                                if (StiReportResourcesHelper.IsDataResourceType(newResource.Type))
                                {
                                    StiFileDatabase database = StiDictionaryHelper.CreateNewDatabaseFromResource(report, newResource);
                                    if (database != null)
                                    {
                                        database.Name = database.Alias = StiDictionaryHelper.GetNewDatabaseName(report, newResource.Name);
                                        database.PathData = StiHyperlinkProcessor.ResourceIdent + newResource.Name;
                                        report.Dictionary.Databases.Add(database);
                                        database.CreateDataSources(report.Dictionary);
                                    }
                                }
                            }
                        }
                    }
                }

                //Remove temp resource items
                List<string> deletingItems = new List<string>();
                foreach (Hashtable resourceItem in resourceItems)
                {
                    deletingItems.Add(resourceItem["key"] as string);
                }
                var commandDelete = new StiItemCommands.Delete()
                {
                    SessionKey = sessionKey,
                    ItemKeys = deletingItems,
                    ResultSuccess = true,
                    AllowMoveToRecycleBin = false,
                    AllowNotifications = false
                };
                var resultDeleteCommand = StiCommandToServer.RunCommand(commandDelete) as StiItemCommands.Delete;
            }
        }
#endif
#endregion
    }
}
