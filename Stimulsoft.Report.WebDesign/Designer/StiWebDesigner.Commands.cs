#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Stimulsoft.Report.BarCodes;
using Stimulsoft.Report.Gauge;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Components.Table;
using Stimulsoft.Report.Components.TextFormats;
using Stimulsoft.Report.CrossTab;
using Stimulsoft.Report.Chart;
using Stimulsoft.Report.Check;
using Stimulsoft.Base;
using Stimulsoft.Report.Design;
using Stimulsoft.Report.Maps;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Web.Heplers.Dashboards;

#if SERVER
using Stimulsoft.Server;
using Stimulsoft.Client;
//using Stimulsoft.Server.Config;
using Stimulsoft.Server.Objects;
using Stimulsoft.Server.Data;
using Stimulsoft.Server.Reports.Objects;
using Stimulsoft.Server.Connect;
#endif

#if NETCORE
using Stimulsoft.System.Web.UI;
using Stimulsoft.System.Web.UI.WebControls;
#else
using System.Web.UI;
using System.Web.UI.WebControls;
#endif

namespace Stimulsoft.Report.Web
{
    public partial class StiWebDesigner :
        WebControl,
        INamingContainer
    {
        #region Process Request
#if !NETCORE
        internal void ProcessRequest(StiRequestParams requestParams)
        {
            // Process preview tab requests
            if (requestParams.Component == StiComponentType.Viewer) this.Viewer.ProcessRequest(requestParams);

            // Process designer requests
            if (requestParams.Component == StiComponentType.Designer && 
                requestParams.Action != StiAction.Undefined &&
                (requestParams.Id == this.ID || requestParams.Action == StiAction.Resource))
            {
                this.clientGuid = requestParams.Cache.ClientGuid;
                StiWebActionResult result = null;
                StiReport currentReport = this.Report;

                switch (requestParams.Action)
                {
                    case StiAction.Resource:
                        result = StiDesignerResourcesHelper.Get(requestParams);
                        break;

                    case StiAction.GetReport:
                        InvokeGetReport(currentReport);
                        break;

                    case StiAction.OpenReport:
                        try
                        {
                            // The try/catch block is needed for opening incorrect report file or wrong report password
                            currentReport = LoadReportFromContent(requestParams);
                            InvokeOpenReport(currentReport);
                        }
                        catch (Exception e)
                        {
                            result = StiWebActionResult.ErrorResult(requestParams, e.Message);
                        }
                        break;

                    case StiAction.CreateReport:
                        StiReport newReport = GetNewReport(requestParams);
                        if (currentReport != null) StiReportEdit.CopyReportDictionary(currentReport, newReport);
                        InvokeCreateReport(newReport);
                        break;

                    case StiAction.SaveReport:
                        InvokeSaveReport(currentReport);
                        if (!string.IsNullOrEmpty(this.SaveReportErrorString)) result = StiWebActionResult.ErrorResult(requestParams, SaveReportErrorString);
                        break;

                    case StiAction.PreviewReport:
                        StiReport previewReport = StiReportEdit.CloneReport(currentReport, true);
                        InvokePreviewReport(previewReport);
                        break;

                    case StiAction.Exit:
                        InvokeExit(currentReport);
                        result = new StiWebActionResult();
                        break;

                    case StiAction.RunCommand:
                        if (requestParams.Designer.Command == StiDesignerCommand.SetLocalization)
                        {
                            this.Localization = requestParams.Localization;
                            return;
                        }
                        break;
                }

                if (result == null)
                {
                    currentReport = this.report; // Get report from event handlers
                    result = CommandResult(requestParams, currentReport);
                }

                bool useBrowserCache = requestParams.Action == StiAction.Resource;
                StiReportResponse.ResponseBuffer(result.Data, result.ContentType, useBrowserCache, result.FileName);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            StiRequestParams requestParams = this.RequestParams;
            this.ProcessRequest(requestParams);
            
            base.OnInit(e);
        }
#endif
        #endregion

        #region Commands processor

        internal static StiWebActionResult CommandResult(StiRequestParams requestParams, StiReport currentReport)
        {
            var callbackResult = new Hashtable();
            StiDesignReportHelper.ApplyParamsToReport(currentReport, requestParams);

            try
            {
                Hashtable param = requestParams.All;
                StiDesignerCommand command = requestParams.Designer.Command;
                callbackResult["command"] = command;
                if (param["callbackFunctionId"] != null) callbackResult["callbackFunctionId"] = param["callbackFunctionId"];

                #region Session Completed
                if (currentReport == null && command != StiDesignerCommand.CloseReport && command != StiDesignerCommand.CreateReport && 
                    command != StiDesignerCommand.GetReportFromData && command != StiDesignerCommand.OpenReport &&
                    command != StiDesignerCommand.WizardResult && command != StiDesignerCommand.LoadReportFromCloud &&
                    command != StiDesignerCommand.UpdateCache && command != StiDesignerCommand.GetReportForDesigner &&
                    command != StiDesignerCommand.UpdateImagesArray && command != StiDesignerCommand.Synchronization)
                {
                    param["command"] = "SessionCompleted";
                    callbackResult["command"] = "SessionCompleted";
                }
                #endregion

                #region Synchronization
                if (command == StiDesignerCommand.Synchronization)
                {
                    if (currentReport == null)
                        callbackResult["command"] = "SynchronizationError";
                    else
                        callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport);
                }
                #endregion

                #region Update Cache
                else if (command == StiDesignerCommand.UpdateCache)
                {
                    callbackResult["command"] = "UpdateCache";
                }
                #endregion

                #region Get Report For Designer
                else if (command == StiDesignerCommand.GetReportForDesigner)
                {
                    Hashtable attachedItems = null;
#if SERVER
                    //if (param["resourceItems"] != null)
                    //{
                    //    AddResourcesToReport(currentReport, (ArrayList)JSON.Decode((string)param["resourceItems"]), (string)param["sessionKey"]);
                    //}

                    //if (param["attachedItems"] != null)
                    //{
                    //    attachedItems = GetReportAttachedItems((ArrayList)JSON.Decode((string)param["attachedItems"]), (string)param["sessionKey"]);

                    //    if (param["reportTemplateItemKey"] == null && attachedItems["Data"] != null)
                    //    {
                    //        CreateDataSourcesFromAttachedItem(currentReport, attachedItems["Data"] as ArrayList, (string)param["sessionKey"]);
                    //    }
                    //}
#endif
                    if (currentReport != null) callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport, attachedItems);
                }
                #endregion

                #region Create Report
                else if (command == StiDesignerCommand.CreateReport)
                {   
                    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(requestParams.HttpContext), currentReport);
                    callbackResult["reportGuid"] = requestParams.Cache.ClientGuid;
                    callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport);
                    callbackResult["needClearAfterOldReport"] = param["needClearAfterOldReport"];
                    StiReportEdit.ClearUndoArray(requestParams);
                }
                #endregion

                #region Open Report
                else if (command == StiDesignerCommand.OpenReport)
                {
                    try
                    {
                        currentReport.Info.ForceDesigningMode = true;
                        StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(requestParams.HttpContext), currentReport);

                        string reportObjectStr = StiReportEdit.WriteReportInObject(currentReport);
                        if (reportObjectStr != null)
                        {
                            callbackResult["reportObject"] = reportObjectStr;
                            callbackResult["reportGuid"] = requestParams.Cache.ClientGuid;
                        }
                        else
                        {
                            callbackResult["errorMessage"] = "Loading report error: Json parser error!";
                        }

                        if (requestParams.Designer.Password != null) callbackResult["encryptedPassword"] = requestParams.Designer.Password;
                    }
                    catch (Exception e)
                    {
                        callbackResult["reportGuid"] = null;
                        callbackResult["reportObject"] = null;
                        callbackResult["error"] = "Loading report error. " + e.Message;
                    }
                    StiReportEdit.ClearUndoArray(requestParams);
                }
                #endregion

                #region Close Report
                else if (command == StiDesignerCommand.CloseReport)
                {
                    currentReport = null;
                }
                #endregion

                #region Save Report
                else if (command == StiDesignerCommand.SaveReport)
                {
                }
                #endregion

                #region Save As Report
                else if (command == StiDesignerCommand.SaveAsReport)
                {
                }
                #endregion

                #region Change Rect Component
                else if (command == StiDesignerCommand.MoveComponent || command == StiDesignerCommand.ResizeComponent)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.ChangeRectComponent(currentReport, param, callbackResult);
                }
                #endregion

                #region Create Component
                else if (command == StiDesignerCommand.CreateComponent)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.CreateComponent(currentReport, param, callbackResult);
                }
                #endregion

                #region Remove Component
                else if (command == StiDesignerCommand.RemoveComponent)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.RemoveComponent(currentReport, param, callbackResult);
                }
                #endregion

                #region Add Page
                else if (command == StiDesignerCommand.AddPage)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.AddPage(currentReport, param, callbackResult);
                }
                #endregion

                #region Add Dashboard
                else if (command == StiDesignerCommand.AddDashboard)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDashboardHelper.AddDashboard(currentReport, param, callbackResult);
                }
                #endregion

                #region Remove Page
                else if (command == StiDesignerCommand.RemovePage)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.RemovePage(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Properties
                else if (command == StiDesignerCommand.SendProperties)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.ReadAllPropertiesFromString(currentReport, param, callbackResult);
                }
                #endregion

                #region ChangeUnit
                else if (command == StiDesignerCommand.ChangeUnit)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.ChangeUnit(currentReport, (string)param["reportUnit"]);
                    callbackResult["reportUnit"] = param["reportUnit"];
                    callbackResult["gridSize"] = currentReport.Info.GridSize.ToString();
                    StiReportEdit.GetAllComponentsPositions(currentReport, callbackResult);
                }
                #endregion

                #region Rebuild Page
                else if (command == StiDesignerCommand.RebuildPage)
                {
                    callbackResult["pageName"] = (string)param["pageName"];
                    callbackResult["rebuildProps"] = StiReportEdit.GetPropsRebuildPage(currentReport, currentReport.Pages[(string)param["pageName"]]);
                }
                #endregion

                #region LoadReportToViewer
                else if (command == StiDesignerCommand.LoadReportToViewer)
                {
                    // Clear design mode
                    requestParams.Report.Info.ForceDesigningMode = false;
                    requestParams.Report.Designer = null;

                    if (requestParams.Designer.CheckReportBeforePreview)
                    {
                        try
                        {
                            List<StiCheck> checks = StiReportCheckHelper.CheckReport(requestParams, requestParams.Report, false);
                            callbackResult["checkItems"] = StiReportCheckHelper.GetChecksJSCollection(checks);
                        }
                        catch (Exception e)
                        {
                            Console.Write(e.Message);
                        }
                    }
                    
                    string viewerReportGuid = string.Format("{0}_{1}", requestParams.Id + "Viewer", param["viewerClientGuid"]);
                    requestParams.Cache.Helper.SavePreviewReportInternal(requestParams, requestParams.Report, viewerReportGuid);
                    return StiWebActionResult.JsonResult(requestParams, callbackResult);
                }
                #endregion

                #region SetToClipboard
                else if (command == StiDesignerCommand.SetToClipboard)
                {
                    StiReportEdit.SetToClipboard(requestParams, currentReport, param, callbackResult);
                }
                #endregion

                #region GetFromClipboard
                else if (command == StiDesignerCommand.GetFromClipboard)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.GetFromClipboard(requestParams, currentReport, param, callbackResult);
                }
                #endregion

                #region Undo
                else if (command == StiDesignerCommand.Undo)
                {
                    currentReport = StiReportEdit.GetUndoStep(requestParams, currentReport, param, callbackResult);
                }
                #endregion

                #region Redo
                else if (command == StiDesignerCommand.Redo)
                {
                    currentReport = StiReportEdit.GetRedoStep(requestParams, currentReport, param, callbackResult);
                }
                #endregion

                #region RenameComponent
                else if (command == StiDesignerCommand.RenameComponent)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.RenameComponent(currentReport, param, callbackResult);
                }
                #endregion

                #region WizardResult
                else if (command == StiDesignerCommand.WizardResult)
                {
                    Hashtable wizardResult = (Hashtable)param["wizardResult"];
                    currentReport = StiWizardHelper.GetReportFromWizardOptions(currentReport, (Hashtable)wizardResult["reportOptions"], (Hashtable)wizardResult["dataSources"]);
                    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(requestParams.HttpContext), currentReport);
                    callbackResult["reportGuid"] = requestParams.Cache.ClientGuid;
                    Hashtable attachedItems = null;
#if SERVER
                    //if (param["attachedItems"] != null && param["sessionKey"] != null)
                    //{
                    //    attachedItems = GetReportAttachedItems((ArrayList)param["attachedItems"], (string)param["sessionKey"]);
                    //}
#endif
                    callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport, attachedItems);
                    StiReportEdit.ClearUndoArray(requestParams);
                }
                #endregion

                #region GetConnectionTypes
                else if (command == StiDesignerCommand.GetConnectionTypes)
                {
                    StiDictionaryHelper.GetConnectionTypes(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditConnection
                else if (command == StiDesignerCommand.CreateOrEditConnection)
                {
                    if (StiDashboardAssembly.IsAssemblyLoaded) StiCacheCleaner.Clean();
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.CreateOrEditConnection(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteConnection
                else if (command == StiDesignerCommand.DeleteConnection)
                {
                    if (StiDashboardAssembly.IsAssemblyLoaded) StiCacheCleaner.Clean();
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.DeleteConnection(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditRelation
                else if (command == StiDesignerCommand.CreateOrEditRelation)
                {
                    if (StiDashboardAssembly.IsAssemblyLoaded) StiCacheCleaner.Clean();
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.CreateOrEditRelation(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteRelation
                else if (command == StiDesignerCommand.DeleteRelation)
                {
                    if (StiDashboardAssembly.IsAssemblyLoaded) StiCacheCleaner.Clean();
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.DeleteRelation(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditColumn
                else if (command == StiDesignerCommand.CreateOrEditColumn)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.CreateOrEditColumn(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteColumn
                else if (command == StiDesignerCommand.DeleteColumn)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.DeleteColumn(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditParameter
                else if (command == StiDesignerCommand.CreateOrEditParameter)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.CreateOrEditParameter(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteParameter
                else if (command == StiDesignerCommand.DeleteParameter)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.DeleteParameter(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditDataSource
                else if (command == StiDesignerCommand.CreateOrEditDataSource)
                {
                    if (StiDashboardAssembly.IsAssemblyLoaded) StiCacheCleaner.Clean();
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.CreateOrEditDataSource(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteDataSource
                else if (command == StiDesignerCommand.DeleteDataSource)
                {
                    if (StiDashboardAssembly.IsAssemblyLoaded) StiCacheCleaner.Clean();
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.DeleteDataSource(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditBusinessObject
                else if (command == StiDesignerCommand.CreateOrEditBusinessObject)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.CreateOrEditBusinessObject(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteBusinessObject
                else if (command == StiDesignerCommand.DeleteBusinessObject)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.DeleteBusinessObject(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteBusinessObjectCategory
                else if (command == StiDesignerCommand.DeleteBusinessObjectCategory)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.DeleteBusinessObjectCategory(currentReport, param, callbackResult);
                }
                #endregion

                #region EditBusinessObjectCategory
                else if (command == StiDesignerCommand.EditBusinessObjectCategory)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.EditBusinessObjectCategory(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditVariable
                else if (command == StiDesignerCommand.CreateOrEditVariable)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.CreateOrEditVariable(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteVariable
                else if (command == StiDesignerCommand.DeleteVariable)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.DeleteVariable(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteVariablesCategory
                else if (command == StiDesignerCommand.DeleteVariablesCategory)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.DeleteVariablesCategory(currentReport, param, callbackResult);
                }
                #endregion

                #region EditVariablesCategory
                else if (command == StiDesignerCommand.EditVariablesCategory)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.EditVariablesCategory(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateVariablesCategory
                else if (command == StiDesignerCommand.CreateVariablesCategory)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.CreateVariablesCategory(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateOrEditResource
                else if (command == StiDesignerCommand.CreateOrEditResource)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport, true);
                    StiDictionaryHelper.CreateOrEditResource(currentReport, param, callbackResult);
                }
                #endregion

                #region DeleteResource
                else if (command == StiDesignerCommand.DeleteResource)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport, true);
                    StiDictionaryHelper.DeleteResource(currentReport, param, callbackResult);
                }
                #endregion

                #region SynchronizeDictionary
                else if (command == StiDesignerCommand.SynchronizeDictionary)
                {
                    if (StiDashboardAssembly.IsAssemblyLoaded) StiCacheCleaner.Clean();
                    StiDictionaryHelper.SynchronizeDictionary(currentReport, param, callbackResult);
                }
                #endregion

                #region NewDictionary
                else if (command == StiDesignerCommand.NewDictionary)
                {
                    if (StiDashboardAssembly.IsAssemblyLoaded) StiCacheCleaner.Clean();
                    StiDictionaryHelper.NewDictionary(currentReport, param, callbackResult);
                }
                #endregion

                #region GetAllConnections
                else if (command == StiDesignerCommand.GetAllConnections)
                {
                    StiDictionaryHelper.GetAllConnections(currentReport, param, callbackResult);
                }
                #endregion

                #region RetrieveColumns
                else if (command == StiDesignerCommand.RetrieveColumns)
                {
                    StiDictionaryHelper.RetrieveColumns(currentReport, param, callbackResult);
                }
                #endregion

                #region UpdateStyles
                else if (command == StiDesignerCommand.UpdateStyles)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiStylesHelper.UpdateStyles(currentReport, param, callbackResult);
                }
                #endregion

                #region AddStyle
                else if (command == StiDesignerCommand.AddStyle)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiStylesHelper.AddStyle(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateStyleCollection
                else if (command == StiDesignerCommand.CreateStyleCollection)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiStylesHelper.CreateStyleCollection(currentReport, param, callbackResult);
                }
                #endregion

                #region CloneChartComponent
                else if (command == StiDesignerCommand.StartEditChartComponent)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null)
                    {
                        StiReportEdit.SaveComponentClone(requestParams, component);
                        callbackResult["properties"] = StiChartHelper.GetChartProperties(component as StiChart);
                    }
                }
                #endregion

                #region CloneMapComponent
                else if (command == StiDesignerCommand.StartEditMapComponent)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null)
                    {
                        StiReportEdit.SaveComponentClone(requestParams, component);
                        callbackResult["properties"] = StiMapHelper.GetMapProperties(component as StiMap);
                        callbackResult["svgContent"] = StiReportEdit.GetSvgContent(component, StiReportEdit.StrToDouble(param["zoom"] as string));
                    }
                }
                #endregion

                #region CanceledEditComponent
                else if (command == StiDesignerCommand.CanceledEditComponent)
                {
                    StiReportEdit.CanceledEditComponent(requestParams, currentReport, param);
                }
                #endregion

                #region Add Series
                else if (command == StiDesignerCommand.AddSeries)
                {
                    StiChartHelper.AddSeries(currentReport, param, callbackResult);
                }
                #endregion

                #region Remove Series
                else if (command == StiDesignerCommand.RemoveSeries)
                {
                    StiChartHelper.RemoveSeries(currentReport, param, callbackResult);
                }
                #endregion

                #region Series Move
                else if (command == StiDesignerCommand.SeriesMove)
                {
                    StiChartHelper.SeriesMove(currentReport, param, callbackResult);
                }
                #endregion

                #region Add ConstantLineOrStrip
                else if (command == StiDesignerCommand.AddConstantLineOrStrip)
                {
                    StiChartHelper.AddConstantLineOrStrip(currentReport, param, callbackResult);
                }
                #endregion

                #region Remove ConstantLineOrStrip
                else if (command == StiDesignerCommand.RemoveConstantLineOrStrip)
                {
                    StiChartHelper.RemoveConstantLineOrStrip(currentReport, param, callbackResult);
                }
                #endregion

                #region ConstantLineOrStrip Move
                else if (command == StiDesignerCommand.ConstantLineOrStripMove)
                {
                    StiChartHelper.ConstantLineOrStripMove(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Labels Content
                else if (command == StiDesignerCommand.GetLabelsContent)
                {
                    StiChartHelper.GetLabelsContent(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Styles Content
                else if (command == StiDesignerCommand.GetStylesContent)
                {
                    StiChartHelper.GetStylesContent(currentReport, param, callbackResult, false);
                }
                #endregion

                #region Set Labels Type
                else if (command == StiDesignerCommand.SetLabelsType)
                {
                    StiChartHelper.SetLabelsType(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Chart Style
                else if (command == StiDesignerCommand.SetChartStyle)
                {
                    StiChartHelper.SetChartStyle(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Chart Property Value
                else if (command == StiDesignerCommand.SetChartPropertyValue)
                {
                    StiChartHelper.SetChartPropertyValue(currentReport, param, callbackResult);
                }
                #endregion

                #region Send Container Value
                else if (command == StiDesignerCommand.SendContainerValue)
                {
                    StiChartHelper.SetContainerValue(currentReport, param, callbackResult);
                }
                #endregion

                #region GetReportFromData
                else if (command == StiDesignerCommand.GetReportFromData)
                {
#if SERVER
                    //var getReportForDesignCommand = new StiReportCommands.Design()
                    //{
                    //    ReportTemplateItemKey = (string)param["reportTemplateItemKey"],
                    //    SessionKey = (string)param["sessionKey"]
                    //};

                    //if (param.ContainsKey("VersionKey"))
                    //{
                    //    getReportForDesignCommand.VersionKey = (string)param["VersionKey"];
                    //}

                    ////Get attached items
                    //Hashtable attachedItems = GetReportAttachedItems((ArrayList)param["attachedItems"], (string)param["sessionKey"]);
                    //var resultReportForDesignCommand = RunCommand(getReportForDesignCommand) as StiReportCommands.Design;

                    //if (resultReportForDesignCommand.ResultSuccess && resultReportForDesignCommand.ResultReport != null)
                    //{
                    //    StiReport newReport = new StiReport();
                    //    try
                    //    {
                    //        newReport.Load(new MemoryStream(resultReportForDesignCommand.ResultReport));
                    //    }
                    //    catch { };
                    //    newReport.Info.Zoom = 1;
                    //    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), newReport);
                    //    currentReport = newReport;
                    //    callbackResult["reportGuid"] = this.ReportGuid;
                    //    callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport, attachedItems);
                    //}
                    //else
                    //{
                    //    if (resultReportForDesignCommand.ResultNotice != null)
                    //        callbackResult["errorMessage"] = resultReportForDesignCommand.ResultNotice.CustomMessage;
                    //}
#endif
                }
                #endregion

                #region ItemResourceSave
                else if (command == StiDesignerCommand.ItemResourceSave)
                {
                    if (currentReport != null)
                    {
#if SERVER
                        try
                        {
                            StiServerConnection connection = null;

                            if (param.Contains("isOnlineVersion"))
                            {
                                connection = new StiServerConnection(CloudServerAdress);
                                connection.Accounts.Users.Login(param["sessionKey"] as string, false);
                            }

                            var itemResourceSaveCommand = new StiItemResourceCommands.Save()
                            {
                                ItemKey = (string)param["reportTemplateItemKey"],
                                SessionKey = (string)param["sessionKey"],
                                Resource = currentReport.SaveToByteArray()
                            };

                            string customMessage = param["customMessage"] as string;
                            if (!string.IsNullOrEmpty(customMessage))
                            {
                                itemResourceSaveCommand.VersionInfo = StiNotice.Create(StiEncodingHelper.DecodeString(customMessage));
                            }

                            var resultResourceSaveCommand = new StiItemResourceCommands.Save();

                            if (param.Contains("isOnlineVersion"))
                            {
                                resultResourceSaveCommand = connection.RunCommand(itemResourceSaveCommand) as StiItemResourceCommands.Save;
                            }
                            else
                            {
                                //resultResourceSaveCommand = RunCommand(itemResourceSaveCommand) as StiItemResourceCommands.Save;
                            }

                            if (!resultResourceSaveCommand.ResultSuccess && resultResourceSaveCommand.ResultNotice != null)
                            {
                                callbackResult["errorMessage"] = StiNoticeConverter.Convert(resultResourceSaveCommand.ResultNotice);
                            }
                        }
                        catch (StiServerException e)
                        {
                            string msg = e.Notice != null ? StiNoticeConverter.Convert(e.Notice) : e.Message;
                            callbackResult["errorMessage"] = msg;

                            if (e.Notice != null && e.Notice.Ident == StiNoticeIdent.IsNotRecognized && e.Notice.Arguments[0] == "Item")
                            {
                                callbackResult["openSaveAsDialog"] = true;
                            }
                        }
#endif
                    }
                }
                #endregion
                
                #region GetDatabaseData
                else if (command == StiDesignerCommand.GetDatabaseData)
                {
                    StiDictionaryHelper.GetDatabaseData(currentReport, param, callbackResult);
                }
                #endregion

                #region ApplySelectedData
                else if (command == StiDesignerCommand.ApplySelectedData)
                {
                    if (StiDashboardAssembly.IsAssemblyLoaded) StiCacheCleaner.Clean();
                    StiDictionaryHelper.ApplySelectedData(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateTextComponent
                else if (command == StiDesignerCommand.CreateTextComponent)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.CreateTextComponentFromDictionary(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateDataComponent
                else if (command == StiDesignerCommand.CreateDataComponent)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.CreateDataComponentFromDictionary(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Report Properties
                else if (command == StiDesignerCommand.SetReportProperties)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.SetReportProperties(currentReport, param, callbackResult);
                }
                #endregion

                #region Page Move
                else if (command == StiDesignerCommand.PageMove)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.PageMove(currentReport, param, callbackResult);
                }
                #endregion

                #region Test Connection
                else if (command == StiDesignerCommand.TestConnection)
                {
                    StiDictionaryHelper.TestConnection(currentReport, param, callbackResult);
                }
                #endregion

                #region Run Query Script
                else if (command == StiDesignerCommand.RunQueryScript)
                {
                    StiDictionaryHelper.RunQueryScript(currentReport, param, callbackResult);
                }
                #endregion

                #region View Data
                else if (command == StiDesignerCommand.ViewData)
                {
                    StiDictionaryHelper.ViewData(currentReport, param, callbackResult);
                }
                #endregion

                #region Apply Designer Options
                else if (command == StiDesignerCommand.ApplyDesignerOptions)
                {
                    currentReport.Info.Zoom = StiReportEdit.StrToDouble((string)param["zoom"]);
                    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport((Hashtable)param["designerOptions"], currentReport);
                    if (param["reportFile"] != null) currentReport.ReportFile = (string)param["reportFile"];
                    callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(currentReport);
                }
                #endregion

                #region GetSqlParameterTypes
                else if (command == StiDesignerCommand.GetSqlParameterTypes)
                {
                    StiDictionaryHelper.GetSqlParameterTypes(currentReport, param, callbackResult);
                }
                #endregion

                #region Align To Grid
                else if (command == StiDesignerCommand.AlignToGridComponents)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.AlignToGridComponents(currentReport, param, callbackResult);
                }
                #endregion

                #region Change Arrange
                else if (command == StiDesignerCommand.ChangeArrangeComponents)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.ChangeArrangeComponents(currentReport, param, callbackResult);
                }
                #endregion

                #region Update Sample Text Format
                else if (command == StiDesignerCommand.UpdateSampleTextFormat)
                {
                    StiFormatService service = StiTextFormatHelper.GetFormatService((Hashtable)param["textFormat"]);
                    if (!service.IsFormatStringFromVariable) callbackResult["sampleText"] = service.Format(service.Sample);
                    service = null;
                }
                #endregion

                #region CloneCrossTabComponent
                else if (command == StiDesignerCommand.StartEditCrossTabComponent)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null)
                    {
                        StiReportEdit.SaveComponentClone(requestParams, component);
                    }
                }
                #endregion

                #region UpdateCrossTabComponent
                else if (command == StiDesignerCommand.UpdateCrossTabComponent)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is StiCrossTab)
                    {
                        StiCrossTabHelper crossTabHelper = new StiCrossTabHelper(component as StiCrossTab);
                        crossTabHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        crossTabHelper.RestorePositions();
                        crossTabHelper = null;
                    }
                }
                #endregion

                #region GetCrossTabColorStyles
                else if (command == StiDesignerCommand.GetCrossTabColorStyles)
                {
                    callbackResult["colorStyles"] = StiCrossTabHelper.GetColorStyles();
                }
                #endregion

                #region DuplicatePage
                else if (command == StiDesignerCommand.DuplicatePage)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.DuplicatePage(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Event Value
                else if (command == StiDesignerCommand.SetEventValue)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.SetEventValue(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Chart Styles Content
                else if (command == StiDesignerCommand.GetChartStylesContent)
                {
                    StiChartHelper.GetStylesContent(currentReport, param, callbackResult, true);
                }
                #endregion

                #region Get Gauge Styles Content
                else if (command == StiDesignerCommand.GetGaugeStylesContent)
                {
                    StiGaugeHelper.GetStylesContent(currentReport, param, callbackResult, true);
                }
                #endregion

                #region Get Map Styles Content
                else if (command == StiDesignerCommand.GetMapStylesContent)
                {
                    StiMapHelper.GetStylesContent(currentReport, param, callbackResult);
                }
                #endregion

                #region Get CrossTab Styles Content
                else if (command == StiDesignerCommand.GetCrossTabStylesContent)
                {
                    callbackResult["stylesContent"] = StiCrossTabHelper.GetColorStyles();
                }
                #endregion

                #region Get Table Styles Content
                else if (command == StiDesignerCommand.GetTableStylesContent)
                {
                    callbackResult["stylesContent"] = StiTableHelper.GetTableStyles();
                }
                #endregion

                #region Get Dashboard Styles Content
                else if (command == StiDesignerCommand.GetDashboardStylesContent)
                {
                    callbackResult["stylesContent"] = StiDashboardHelper.GetDashboardStyles(currentReport, param, callbackResult);
                }
                #endregion

                #region Change Table Component
                else if (command == StiDesignerCommand.ChangeTableComponent)
                {
                    var table = currentReport.GetComponentByName((string)param["tableName"]);
                    if (table != null && table is StiTable)
                    {
                        StiTableHelper tableHelper = new StiTableHelper(table as StiTable, StiReportEdit.StrToDouble((string)param["zoom"]));
                        tableHelper.ExecuteJSCommand(param["changeParameters"] as Hashtable, callbackResult);
                        tableHelper = null;
                    }
                }
                #endregion

                #region Updat eImages Array
                else if (command == StiDesignerCommand.UpdateImagesArray)
                {
                    callbackResult["images"] = StiDesignerResourcesHelper.GetImagesArray(requestParams, null);
                    return StiWebActionResult.JsonResult(requestParams, callbackResult);
                }
                #endregion

                #region Open Style
                else if (command == StiDesignerCommand.OpenStyle)
                {
                    StiStylesHelper.OpenStyle(requestParams, currentReport, callbackResult);
                }
                #endregion

                #region Get Style From Component
                else if (command == StiDesignerCommand.CreateStylesFromComponents)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiStylesHelper.CreateStylesFromComponents(currentReport, param, callbackResult);
                }
                #endregion

                #region Change Size Components
                else if (command == StiDesignerCommand.ChangeSizeComponents)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.ChangeSizeComponents(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateFieldOnDblClick
                else if (command == StiDesignerCommand.CreateFieldOnDblClick)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.CreateFieldOnDblClick(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Params From Query String
                else if (command == StiDesignerCommand.GetParamsFromQueryString)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.GetParamsFromQueryString(currentReport, param, callbackResult);
                }
                #endregion

                #region Create Moving Copy Component
                else if (command == StiDesignerCommand.CreateMovingCopyComponent)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.CreateMovingCopyComponent(requestParams, currentReport, param, callbackResult);
                }
                #endregion

                #region Get Report Check Items
                else if (command == StiDesignerCommand.GetReportCheckItems)
                {
                    List<StiCheck> checks = StiReportCheckHelper.CheckReport(requestParams, currentReport, true);
                    callbackResult["checkItems"] = StiReportCheckHelper.GetChecksJSCollection(checks);
                }
                #endregion

                #region Get Check Preview
                else if (command == StiDesignerCommand.GetCheckPreview)
                {
                    StiReportCheckHelper.GetCheckPreview(requestParams, currentReport, param, callbackResult);
                }
                #endregion

                #region Action Check
                else if (command == StiDesignerCommand.ActionCheck)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportCheckHelper.ActionCheck(requestParams, currentReport, param, callbackResult);
                }
                #endregion

                #region Check Expression
                else if (command == StiDesignerCommand.CheckExpression)
                {
                    StiReportCheckHelper.CheckExpression(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Globalization Strings
                else if (command == StiDesignerCommand.GetGlobalizationStrings)
                {
                    callbackResult["globalizationStrings"] = StiCultureHelper.GetReportGlobalizationStrings(currentReport);
                    callbackResult["cultures"] = StiCultureHelper.GetItems(CultureTypes.NeutralCultures);
                }
                #endregion

                #region Add Globalization Strings
                else if (command == StiDesignerCommand.AddGlobalizationStrings)
                {
                    StiCultureHelper.AddReportGlobalizationStrings(currentReport, param, callbackResult);
                }
                #endregion

                #region Remove Globalization Strings
                else if (command == StiDesignerCommand.RemoveGlobalizationStrings)
                {
                    StiCultureHelper.RemoveReportGlobalizationStrings(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Culture Settings From Report
                else if (command == StiDesignerCommand.GetCultureSettingsFromReport)
                {
                    StiCultureHelper.GetCultureSettingsFromReport(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Culture Settings To Report
                else if (command == StiDesignerCommand.SetCultureSettingsToReport)
                {
                    StiCultureHelper.SetCultureSettingsToReport(currentReport, param, callbackResult);
                }
                #endregion

                #region Apply Globalization Strings
                else if (command == StiDesignerCommand.ApplyGlobalizationStrings)
                {
                    StiCultureHelper.ApplyGlobalizationStrings(currentReport, param, callbackResult);
                }
                #endregion

                #region CloneGaugeComponent
                else if (command == StiDesignerCommand.StartEditGaugeComponent)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null)
                    {
                        StiReportEdit.SaveComponentClone(requestParams, component);
                        callbackResult["properties"] = StiGaugeHelper.GetGaugeProperties(component as StiGauge);
                    }
                }
                #endregion

                #region Get Resource Content
                else if (command == StiDesignerCommand.GetResourceContent)
                {
                    StiReportResourcesHelper.GetResourceContent(currentReport, param, callbackResult);
                }
                #endregion

                #region Convert Resource Content
                else if (command == StiDesignerCommand.ConvertResourceContent)
                {
                    StiResourceType type = (StiResourceType)Enum.Parse(typeof(StiResourceType), param["type"] as string);
                    callbackResult["content"] = StiReportResourcesHelper.GetStringContentForJSFromResourceContent(type, requestParams.Data);
                }
                #endregion

                #region Get Resource Text
                else if (command == StiDesignerCommand.GetResourceText)
                {
                    StiReportResourcesHelper.GetResourceText(currentReport, param, callbackResult);
                }
                #endregion

                #region Set Resource Text
                else if (command == StiDesignerCommand.SetResourceText)
                {
                    StiReportResourcesHelper.SetResourceText(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Resource View Data
                else if (command == StiDesignerCommand.GetResourceViewData)
                {
                    StiReportResourcesHelper.GetResourceViewData(currentReport, param, callbackResult);
                }
                #endregion

                #region UpdateGaugeComponent
                else if (command == StiDesignerCommand.UpdateGaugeComponent)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is StiGauge)
                    {
                        StiGaugeHelper.ExecuteJSCommand(component as StiGauge, param["updateParameters"] as Hashtable, callbackResult);
                    }
                }
                #endregion

                #region Get Images Gallery
                else if (command == StiDesignerCommand.GetImagesGallery)
                {
                    StiDictionaryHelper.GetImagesGallery(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateComponentFromResource
                else if (command == StiDesignerCommand.CreateComponentFromResource)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiReportEdit.CreateComponentFromResource(currentReport, param, callbackResult);
                }
                #endregion

                #region GetSampleConnectionString
                else if (command == StiDesignerCommand.GetSampleConnectionString)
                {
                    StiDictionaryHelper.GetSampleConnectionString(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateDatabaseFromResource
                else if (command == StiDesignerCommand.CreateDatabaseFromResource)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.CreateDatabaseFromResource(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Rich Text Gallery
                else if (command == StiDesignerCommand.GetRichTextGallery)
                {
                    StiDictionaryHelper.GetRichTextGallery(currentReport, param, callbackResult);
                }
                #endregion

                #region Get RichText Content
                else if (command == StiDesignerCommand.GetRichTextContent)
                {
                    callbackResult["content"] = StiGalleriesHelper.GetHtmlStringFromRichTextItem(currentReport, param["itemObject"] as Hashtable);
                }
                #endregion

                #region Delete All DataSources
                else if (command == StiDesignerCommand.DeleteAllDataSources)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiDictionaryHelper.DeleteAllDataSources(currentReport, param, callbackResult);
                }
                #endregion

                #region Get Report Cloud Server Mode                
                else if (command == StiDesignerCommand.LoadReportFromCloud)
                {
#if SERVER
                    Hashtable itemObject = param["itemObject"] as Hashtable;
                    try
                    {
                        if (param.Contains("isOnlineVersion"))
                        {
                            StiReport newReport = new StiReport();

                            var connection = new StiServerConnection(CloudServerAdress);
                            connection.Accounts.Users.Login(param["sessionKey"] as string, false);

                            var commandResourceGet = new StiItemResourceCommands.Get
                            {
                                ItemKey = itemObject["Key"] as string,
                                SessionKey = param["sessionKey"] as string
                            };

                            if (itemObject.ContainsKey("VersionKey"))
                            {
                                commandResourceGet.VersionKey = itemObject["VersionKey"] as string;
                            }

                            commandResourceGet = connection.RunCommand(commandResourceGet) as StiItemResourceCommands.Get;

                            if (commandResourceGet.ResultSuccess && commandResourceGet.ResultResource != null && commandResourceGet.ResultResource.Length > 0)
                            {
                                try
                                {
                                    newReport.Load(commandResourceGet.ResultResource);
                                    newReport.Info.Zoom = 1;
                                    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(requestParams.HttpContext), newReport);
                                }
                                catch (Exception e)
                                {
                                    callbackResult["error"] = e.Message;
                                };
                            }
                            else if (commandResourceGet.ResultNotice != null)
                            {
                                callbackResult["errorMessage"] = StiNoticeConverter.Convert(commandResourceGet.ResultNotice);
                            }

                            currentReport = newReport;
                            string reportObjectStr = StiReportEdit.WriteReportInObject(currentReport);

                            if (reportObjectStr != null)
                            {
                                callbackResult["reportObject"] = reportObjectStr;
                                callbackResult["reportGuid"] = requestParams.Cache.ClientGuid;
                            }
                        }
                        else
                        {
                            //var getReportForDesignCommand = new StiReportCommands.Design()
                            //{
                            //    ReportTemplateItemKey = itemObject["Key"] as string,
                            //    SessionKey = param["sessionKey"] as string
                            //};

                            //if (itemObject.ContainsKey("VersionKey"))
                            //{
                            //    getReportForDesignCommand.VersionKey = itemObject["VersionKey"] as string;
                            //}

                            //Hashtable attachedItems = GetReportAttachedItems(itemObject["AttachedItems"] as ArrayList, param["sessionKey"] as string);
                            //getReportForDesignCommand = RunCommand(getReportForDesignCommand) as StiReportCommands.Design;

                            //if (getReportForDesignCommand.ResultSuccess && getReportForDesignCommand.ResultReport != null)
                            //{
                            //    StiReport newReport = new StiReport();
                            //    try
                            //    {
                            //        newReport.Load(getReportForDesignCommand.ResultReport);
                            //        newReport.Info.Zoom = 1;
                            //        StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(Page), newReport);
                            //    }
                            //    catch (Exception e) { callbackResult["error"] = e.Message; };

                            //    currentReport = newReport;
                            //    string reportObjectStr = StiReportEdit.WriteReportInObject(currentReport, attachedItems);
                            //    if (reportObjectStr != null)
                            //    {
                            //        callbackResult["reportObject"] = reportObjectStr;
                            //        callbackResult["reportGuid"] = this.ReportGuid;
                            //    }
                            //}
                            //else
                            //{
                            //    if (getReportForDesignCommand.ResultNotice != null)
                            //        callbackResult["errorMessage"] = getReportForDesignCommand.ResultNotice.CustomMessage;
                            //}
                        }
                    }
                    catch (StiServerException e)
                    {
                        string msg = e.Notice != null ? StiNoticeConverter.Convert(e.Notice) : e.Message;
                        callbackResult["errorMessage"] = msg;
                    }
#endif
                }
                #endregion

                #region CloneBarCodeComponent
                else if (command == StiDesignerCommand.StartEditBarCodeComponent)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null)
                    {
                        StiReportEdit.SaveComponentClone(requestParams, component);
                        callbackResult["barCode"] = StiBarCodeHelper.GetBarCodeJSObject(component as StiBarCode);
                    }
                }
                #endregion

                #region Apply BarCode Properties
                else if (command == StiDesignerCommand.ApplyBarCodeProperties)
                {
                    StiBarCodeHelper.ApplyBarCodeProperties(currentReport, param, callbackResult);
                }
                #endregion

                #region Download Report
                else if (command == StiDesignerCommand.DownloadReport)
                {
                    string reportFile = !string.IsNullOrWhiteSpace(requestParams.Designer.FileName) ? requestParams.Designer.FileName : "Report.mrt";
                    string password = requestParams.Designer.Password;

                    if (currentReport != null)
                    {
                        Stream stream = new MemoryStream();                        
                        if (!String.IsNullOrEmpty(password))
                        {                           
                            currentReport.SaveEncryptedReport(stream, password);

                            if (reportFile.ToLower().EndsWith(".mrt")) reportFile = reportFile.Substring(0, reportFile.Length - 4) + ".mrx";
                            else if (!reportFile.ToLower().EndsWith(".mrx")) reportFile += ".mrx";
                        }
                        else
                        {
                            if (requestParams.Designer.SaveType == "json")
                            {
                                currentReport.Save(new Stimulsoft.Report.SaveLoad.StiJsonReportSLService(), stream);
                            }
                            else if (requestParams.Designer.SaveType == "xml")
                            {
                                currentReport.Save(new Stimulsoft.Report.SaveLoad.StiXmlReportSLService(), stream);
                            }
                            else
                            {
                                currentReport.Save(stream);
                            }
                        }

                        return new StiWebActionResult(stream, "application/octet-stream", reportFile);
                    }
                }
                #endregion

                #region Download Styles
                else if (command == StiDesignerCommand.DownloadStyles)
                {
                    StiReport tempReport = new StiReport();
                    string styles = requestParams.GetString("stylesCollection");

                    if (styles != null)
                    {
                        ArrayList stylesCollection = JSON.Decode(styles) as ArrayList;
                        StiStylesHelper.WriteStylesToReport(tempReport, stylesCollection);
                        Stream stream = new MemoryStream();
                        tempReport.Styles.Save(stream);
                        return new StiWebActionResult(stream, "application/octet-stream", "Styles.sts");
                    }
                }
                #endregion

                #region GetVariableItemsFromDataColumn
                else if (command == StiDesignerCommand.GetVariableItemsFromDataColumn)
                {
                    StiDictionaryHelper.GetVariableItemsFromDataColumn(currentReport, param, callbackResult);
                }
                #endregion

                #region Move Dictionary Item
                else if (command == StiDesignerCommand.MoveDictionaryItem)
                {
                    StiDictionaryHelper.MoveDictionaryItem(currentReport, param, callbackResult);
                }
                #endregion

                #region Convert MetaFile To Png
                else if (command == StiDesignerCommand.ConvertMetaFileToPng)
                {
                    callbackResult["fileContent"] = StiReportResourcesHelper.ConvertBase64MetaFileToBase64Png(param["fileContent"] as string);
                }
                #endregion

                #region GetReportString
                else if (command == StiDesignerCommand.GetReportString)
                {
                    if (currentReport != null)
                        callbackResult["reportString"] = StiEncodingHelper.Encode(currentReport.SaveToString());
                }
                #endregion

                #region UpdateReportAliases
                else if (command == StiDesignerCommand.UpdateReportAliases)
                {
                    StiReportEdit.UpdateReportAliases(requestParams, currentReport, param, callbackResult);
                }
                #endregion

                #region MoveConnectionDataToResource
                else if (command == StiDesignerCommand.MoveConnectionDataToResource)
                {
                    StiDictionaryHelper.MoveConnectionDataToResource(currentReport, param, callbackResult);
                }
                #endregion

                #region GetShapeSampleImage
                else if (command == StiDesignerCommand.GetShapeSampleImage)
                {
                    StiShapeHelper.GetShapeSampleImage(currentReport, param, callbackResult);
                }
                #endregion

                #region SetMapProperties
                else if (command == StiDesignerCommand.SetMapProperties)
                {                    
                    StiMapHelper.SetMapProperties(currentReport, param, callbackResult);
                }
                #endregion

                #region UpdateMapData
                else if (command == StiDesignerCommand.UpdateMapData)
                {
                    StiMapHelper.UpdateMapData(currentReport, param, callbackResult);
                }
                #endregion

                #region Open Page
                else if (command == StiDesignerCommand.OpenPage)
                {
                    StiReportEdit.OpenPage(requestParams, currentReport, callbackResult);
                }
                #endregion

                #region Download Page
                else if (command == StiDesignerCommand.DownloadPage)
                {
                    var pageIndex = Convert.ToInt32(param["pageIndex"]);
                    if (currentReport.Pages.Count > pageIndex)
                    {
                        Stream stream = new MemoryStream();
                        currentReport.Pages[pageIndex].Save(stream);
                        return new StiWebActionResult(stream, "application/octet-stream", "Page.pg");
                    }
                }
                #endregion

                #region UpdateTableElement
                else if (command == StiDesignerCommand.UpdateTableElement)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is IStiTableElement)
                    {
                        var tableElementHelper = new StiTableElementHelper(component as IStiTableElement);
                        tableElementHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        tableElementHelper = null;
                    }
                }
                #endregion

                #region UpdateImageElement
                else if (command == StiDesignerCommand.UpdateImageElement)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is IStiImageElement)
                    {
                        var imageElementHelper = new StiImageElementHelper(component as IStiImageElement);
                        imageElementHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        imageElementHelper = null;
                    }
                }
                #endregion

                #region UpdateTextElement
                else if (command == StiDesignerCommand.UpdateTextElement)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is IStiTextElement)
                    {
                        var textElementHelper = new StiTextElementHelper(component as IStiTextElement);
                        textElementHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        textElementHelper = null;
                    }
                }
                #endregion

                #region UpdateMapElement
                else if (command == StiDesignerCommand.UpdateMapElement)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is IStiMapElement)
                    {
                        var mapElementHelper = new StiMapElementHelper(component as IStiMapElement);
                        mapElementHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        mapElementHelper = null;
                    }
                }
                #endregion

                #region UpdateProgressElement
                else if (command == StiDesignerCommand.UpdateProgressElement)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is IStiProgressElement)
                    {
                        var progressElementHelper = new StiProgressElementHelper(component as IStiProgressElement);
                        progressElementHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        progressElementHelper = null;
                    }
                }
                #endregion

                #region UpdateIndicatorElement
                else if (command == StiDesignerCommand.UpdateIndicatorElement)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is IStiIndicatorElement)
                    {
                        var indicatorElementHelper = new StiIndicatorElementHelper(component as IStiIndicatorElement);
                        indicatorElementHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        indicatorElementHelper = null;
                    }
                }
                #endregion

                #region UpdateChartElement
                else if (command == StiDesignerCommand.UpdateChartElement)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is IStiChartElement)
                    {
                        var chartElementHelper = new StiChartElementHelper(component as IStiChartElement);
                        chartElementHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        chartElementHelper = null;
                    }
                }
                #endregion

                #region UpdateGaugeElement
                else if (command == StiDesignerCommand.UpdateGaugeElement)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is IStiGaugeElement)
                    {
                        var gaugeElementHelper = new StiGaugeElementHelper(component as IStiGaugeElement);
                        gaugeElementHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        gaugeElementHelper = null;
                    }
                }
                #endregion

                #region UpdateShapeElement
                else if (command == StiDesignerCommand.UpdateShapeElement)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is IStiShapeElement)
                    {
                        var shapeElementHelper = new StiShapeElementHelper(component as IStiShapeElement);
                        shapeElementHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        shapeElementHelper = null;
                    }
                }
                #endregion

                #region UpdatePivotElement
                else if (command == StiDesignerCommand.UpdatePivotElement)
                {
                    var component = currentReport.GetComponentByName((string)param["componentName"]);
                    if (component != null && component is IStiPivotElement)
                    {
                        var pivotElementHelper = new StiPivotElementHelper(component as IStiPivotElement);
                        pivotElementHelper.ExecuteJSCommand(param["updateParameters"] as Hashtable, callbackResult);
                        pivotElementHelper = null;
                    }
                }
                #endregion

                #region CreateTextElement
                else if (command == StiDesignerCommand.CreateTextElement)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiTextElementHelper.CreateTextElementFromDictionary(currentReport, param, callbackResult);
                }
                #endregion

                #region CreateTableElement
                else if (command == StiDesignerCommand.CreateTableElement)
                {
                    StiReportEdit.AddReportToUndoArray(requestParams, currentReport);
                    StiTableElementHelper.CreateTableElementFromDictionary(currentReport, param, callbackResult);
                }
                #endregion

                // Update report in cache
                if (currentReport != null) requestParams.Cache.Helper.SaveReportInternal(requestParams, currentReport);
                else requestParams.Cache.Helper.RemoveReportInternal(requestParams);
            }
            catch (Exception e)
            {
#if SERVER
                //StiLog.WriteException(e);
#endif
                callbackResult["error"] = e.Message;
                return StiWebActionResult.JsonResult(requestParams, callbackResult);
            }
            
            return StiWebActionResult.JsonResult(requestParams, callbackResult);
        }

        #endregion
    }
}
