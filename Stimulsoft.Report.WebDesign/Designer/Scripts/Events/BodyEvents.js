
//Document MouseUp
StiMobileDesigner.prototype.DocumentMouseUp = function (evnt) {
    this.options.formInDrag = false;
    this.options.currentComponent = null;
    this.options.startCopyWithCTRL = false;

    if (this.options.itemInDrag) {
        this.options.mainPanel.removeChild(this.options.itemInDrag);
        this.options.itemInDrag = false;
    }

    if (this.options.componentButtonInDrag) {
        this.options.mainPanel.removeChild(this.options.componentButtonInDrag);
        this.options.componentButtonInDrag = false;
    }

    if (this.options.currentPage && this.options.selectingRect && evnt.button != 2) {
        this.MultiSelectComponents(this.options.currentPage);
        this.options.startPoint = false;
    }

    if (this.options.currentPage && this.options.cursorRect) {
        this.options.currentPage.removeChild(this.options.cursorRect);
        this.options.cursorRect = false;
        this.options.startPoint = false;
        if (this.options.insertPanel) this.options.insertPanel.resetChoose();
        if (this.options.toolbox) this.options.toolbox.resetChoose();
        this.options.selectedObject.setSelected();
    }

    if (this.options.controls.zoomScale) this.options.controls.zoomScale.button.ontouchend(); //for Zoom
    if ((this.options.in_resize || this.options.in_drag) && !this.options.drawComponent) {
        var thisComponent;
        if (this.options.in_resize) thisComponent = this.options.in_resize[0];
        if (this.options.in_drag) thisComponent = this.options.in_drag[0];

        var addOrRemovingComponent = this.options.CTRL_pressed || this.options.SHIFT_pressed || this.options.startCopyWithCTRL;
        if (addOrRemovingComponent && !this.options.in_resize) {
            if (!(this.options.mouseMoved &&
                ((this.options.selectedObjects && this.IsContains(this.options.selectedObjects, addOrRemovingComponent)) ||
                 (this.options.selectedObject && this.options.selectedObject == addOrRemovingComponent)))) {

                this.DeleteSelectedLines();
                if (addOrRemovingComponent.typeComponent) {
                    if (this.Is_array(thisComponent)) {
                        if (this.IsContains(thisComponent, addOrRemovingComponent)) {
                            this.RemoveElementFromArray(thisComponent, addOrRemovingComponent);
                            if (thisComponent.length == 1) thisComponent = thisComponent[0];
                        }
                    }
                    else {
                        if (this.options.selectedObjects) {
                            this.options.selectedObjects.push(addOrRemovingComponent);
                            thisComponent = this.options.selectedObjects;
                        }
                        else {
                            if (this.options.selectedObject && this.options.selectedObject.typeComponent != "StiPage" &&
                                this.options.selectedObject.typeComponent != "StiReport" && this.options.selectedObject != addOrRemovingComponent) {
                                thisComponent = [this.options.selectedObject];
                                thisComponent.push(addOrRemovingComponent);
                                this.options.selectedObject = null;
                                this.options.selectedObjects = thisComponent;
                            }
                        }
                    }
                    this.PaintSelectedLines();
                    this.UpdatePropertiesControls();
                }
            }
        }
        var allowUpdateProperties = true;

        if (!this.Is_array(thisComponent)) {
            if (thisComponent == this.options.selectedObject) allowUpdateProperties = false;
            thisComponent.setSelected();
        }

        if (!this.options.mouseMoved) {
            if (allowUpdateProperties) this.UpdatePropertiesControls();
        }
        else {
            var components = this.Is_array(thisComponent) ? thisComponent : [thisComponent];
            var marginsPx;

            for (var i = 0; i < components.length; i++) {
                if (!marginsPx) {
                    var pageName = components[i].properties.pageName;
                    var page = this.options.report.pages[pageName];
                    if (!page) continue;
                    marginsPx = page.marginsPx;
                }
                var marginLeftPx = marginsPx[0];
                var marginTopPx = marginsPx[1];

                var leftPx = this.StrToDouble(components[i].getAttribute("left"));
                var topPx = this.StrToDouble(components[i].getAttribute("top"));

                var leftProperty = leftPx - marginLeftPx;
                var topProperty = topPx - marginTopPx;

                components[i].properties.unitLeft = this.ConvertPixelToUnit(leftProperty / this.options.report.zoom, components[i].isDashboardElement);
                components[i].properties.unitTop = this.ConvertPixelToUnit(topProperty / this.options.report.zoom, components[i].isDashboardElement);

                if (this.options.in_resize) {
                    var widthProperty = this.StrToDouble(components[i].getAttribute("width"));
                    var heightProperty = this.StrToDouble(components[i].getAttribute("height"));

                    components[i].properties.unitWidth = this.ConvertPixelToUnit(widthProperty / this.options.report.zoom, components[i].isDashboardElement);
                    components[i].properties.unitHeight = this.ConvertPixelToUnit(heightProperty / this.options.report.zoom, components[i].isDashboardElement);
                }
            }

            if (this.Is_array(thisComponent) && !this.options.clipboardMode) {
                this.PaintSelectedLines();
                this.UpdatePropertiesControls();
            }

            this.options.clipboardMode = false;

            if (this.options.in_resize) {
                var components = this.Is_array(this.options.in_resize[0]) ? this.options.in_resize[0] : [this.options.in_resize[0]];                
                if (!this.Is_array(this.options.in_resize[0]) && this.IsTableCell(this.options.in_resize[0])) {
                    components = components.concat(this.options.in_resize[3]);
                }
                this.SendCommandChangeRectComponent(components, "ResizeComponent", null, this.options.in_resize[1]);
            }

            if (this.options.in_drag) {
                this.SendCommandChangeRectComponent(this.options.in_drag[0], "MoveComponent");
            }
        }
    }

    this.options.in_resize = false;
    this.PasteCurrentClipboardComponent();
    this.options.in_drag = false;
    this.options.movingCloneComponents = false;
    this.options.gridOffset = false;
    if (this.options.currentPage) this.options.currentPage.style.cursor = "";
}

//Document TouchEnd
StiMobileDesigner.prototype.DocumentTouchEnd = function (event) {
    this.options.currentComponent = null;
    this.options.clipboardMode = false;

    if (this.options.itemInDrag) {
        this.options.mainPanel.removeChild(this.options.itemInDrag);
        this.options.itemInDrag = false;
    }
    if (this.options.componentButtonInDrag) {
        this.options.mainPanel.removeChild(this.options.componentButtonInDrag);
        this.options.componentButtonInDrag = false;
    }
    if (this.options.currentPage && this.options.selectingRect) {
        this.MultiSelectComponents(this.options.currentPage);
        this.options.startPoint = false;
    }
}

//Document Mouse Move
StiMobileDesigner.prototype.DocumentMouseMove = function (evnt) {
    this.DictionaryItemMove(evnt);
    this.ComponentButtonMove(evnt);
    if (this.options.startPosZoomScaleButton) this.options.controls.zoomScale.ontouchmove(evnt, true); //for Zoom
    if (this.options.formInDrag) this.options.formInDrag[4].move(evnt);

    if (this.options.in_resize || this.options.in_drag) {
        this.options.mouseMoved = true;

        mouseCurrentXPos = evnt.clientX || evnt.x;
        mouseCurrentYPos = evnt.clientY || evnt.y;

        if (this.options.startMousePos && mouseCurrentXPos == this.options.startMousePos[0] && mouseCurrentYPos == this.options.startMousePos[1])
            this.options.mouseMoved = false; //fixed bug IE

        var canMoveOrResize = true;

        if (this.options.report.info.alignToGrid && this.options.startMousePos) {
            var gridSize = (this.options.currentPage && this.options.currentPage.isDashboard
                ? this.options.currentPage.properties.gridSize
                : this.options.report.gridSize) * this.options.report.zoom;

            canMoveOrResize = false;

            if (!this.options.gridOffset) {
                this.options.gridOffset = {
                    x: this.options.startMousePos[0],
                    y: this.options.startMousePos[1]
                }
            }

            var xCountGridLines = parseInt((this.options.gridOffset.x - mouseCurrentXPos) / gridSize);
            var yCountGridLines = parseInt((this.options.gridOffset.y - mouseCurrentYPos) / gridSize);
            
            if (Math.abs(xCountGridLines) >= 1 || Math.abs(yCountGridLines) >= 1) {
                canMoveOrResize = true;

                this.options.gridOffset.x = this.options.gridOffset.x - xCountGridLines * gridSize;
                mouseCurrentXPos = this.RoundXY(this.options.gridOffset.x, 0)[0];

                this.options.gridOffset.y = this.options.gridOffset.y - yCountGridLines * gridSize;
                mouseCurrentYPos = this.RoundXY(this.options.gridOffset.y, 0)[0];
            }
        }

        if (this.options.in_drag && canMoveOrResize) {
            if ((this.options.CTRL_pressed || this.options.startCopyWithCTRL) && !this.options.clipboardMode)
                this.MoveCopyComponent(mouseCurrentXPos, mouseCurrentYPos);
            else
                this.MoveComponent(mouseCurrentXPos, mouseCurrentYPos);
        }

        if (this.options.in_resize && canMoveOrResize) {
            if (this.Is_array(this.options.in_resize[0]))
                this.ResizeComponents(mouseCurrentXPos, mouseCurrentYPos);
            else
                this.ResizeComponent(mouseCurrentXPos, mouseCurrentYPos);
        }
    }
}

//Document Touch Move
StiMobileDesigner.prototype.DocumentTouchMove = function (evnt) {
    this.DictionaryItemMove(evnt);
    this.ComponentButtonMove(evnt);
}

StiMobileDesigner.prototype.DictionaryItemMove = function (evnt) {
    if (this.options.itemInDrag) {
        evnt.preventDefault();
        if (this.options.itemInDrag.beginingOffset < 10) {
            this.options.itemInDrag.beginingOffset++;
        }
        else {
            this.options.itemInDrag.move(evnt);
        }
    }
}

StiMobileDesigner.prototype.ComponentButtonMove = function (evnt) {
    if (this.options.componentButtonInDrag) {
        if (evnt) evnt.preventDefault();
        if (this.options.componentButtonInDrag.beginingOffset < 10) {
            this.options.componentButtonInDrag.beginingOffset++;
        }
        else {
            this.options.componentButtonInDrag.move(evnt);
            this.options.eventTouch = evnt;
        }
    }
}