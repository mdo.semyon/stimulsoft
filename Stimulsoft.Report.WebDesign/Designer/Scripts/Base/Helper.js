
StiMobileDesigner.prototype.IsTouchDevice = function() {
    return ('ontouchstart' in document.documentElement) && this.isMobileDevice.any() != null;
}

StiMobileDesigner.prototype.isMobileDevice = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
    }
};

StiMobileDesigner.prototype.SetSelectedObject = function (object) {
    if (this.options.report && this.options.selectedObject && this.options.selectedObject != object) {
        this.options.report.chartStylesContent = null;
        this.options.report.mapStylesContent = null;
        this.options.report.gaugeStylesContent = null;
        this.options.report.dashboardStiMapElementStylesContent = null;
        this.options.report.dashboardStiChartElementStylesContent = null;
        this.options.report.dashboardStiGaugeElementStylesContent = null;
        this.options.report.dashboardStiProgressElementStylesContent = null;
        this.options.report.dashboardStiIndicatorElementStylesContent = null;
    }

    //Hide resizing icons
    if (this.options.selectedObject != null && this.options.selectedObject.typeComponent != "StiPage" && this.options.selectedObject.typeComponent != "StiReport") {
        this.options.selectedObject.changeVisibilityStateResizingIcons(false);
    }

    //Show resizing icons
    if (this.options.selectedObjects == null ||
        (this.options.selectedObjects && this.options.selectedObjects.length == 1) ||
        (this.options.selectedObjects && !this.IsContains(this.options.selectedObjects, object))) {
        
        if (this.options.selectedObjects && this.options.selectedObjects.length == 1) { object = this.options.selectedObjects[0]; }
        this.options.selectedObject = object;
        if (object.typeComponent != "StiPage" && object.typeComponent != "StiReport") {
            object.changeVisibilityStateResizingIcons(true);
        }
        this.DeleteSelectedLines();
        this.options.selectedObjects = null;

        if (this.options.dictionaryPanel) {
            this.options.dictionaryPanel.setFocused(false);
        }
    }

    //Select current component in the report tree
    if (this.options.reportTree) {
        var item = object.typeComponent == "StiReport" ? this.options.reportTree.reportItem : this.options.reportTree.items[object.properties.name];
        if (item) {
            item.setSelected();
            item.openTree();            
        }
    }

    //Show Positions on Status Panel
    if (object != null && object.typeComponent != "StiPage" && object.typeComponent != "StiReport") {
        this.options.statusPanel.showPositions(object.properties.unitLeft, object.properties.unitTop, object.properties.unitWidth, object.properties.unitHeight);
    }
    else {
        this.options.statusPanel.showPositions();
    }

    //Return to main properties
    var propertiesPanel = this.options.propertiesPanel;
    if (object != null && object.typeComponent != "StiCrossField" && propertiesPanel.editCrossTabMode) {
        propertiesPanel.setEditCrossTabMode(false);
        if (propertiesPanel.currentContainerName == "Properties") {
            propertiesPanel.propertiesToolBar.changeVisibleState(true);
        }
    }

    if (object.typeComponent == "StiPage") {
        if (this.options.insertPanel) this.options.insertPanel.setMode();
        if (this.options.toolbox) this.options.toolbox.setMode();
    }
}

StiMobileDesigner.prototype.SetSelectedObjectsByNames = function (page, objectNames) {
    this.DeleteSelectedLines();
    var selectedObjects = this.options.selectedObject ? [this.options.selectedObject] : this.options.selectedObjects;
    if (!selectedObjects) return;

    for (var i = 0; i < selectedObjects.length; i++) {
        if (selectedObjects[i] != null && selectedObjects[i].typeComponent != "StiPage" && selectedObjects[i].typeComponent != "StiReport")
            selectedObjects[i].changeVisibilityStateResizingIcons(false);
    }
    this.options.selectedObject = null;
    this.options.selectedObjects = [];

    for (var i = 0; i < objectNames.length; i++) {
        var component = page.components[objectNames[i]];
        if (component) this.options.selectedObjects.push(component);
    }
    this.PaintSelectedLines();
}

StiMobileDesigner.prototype.PaintSelectedLines = function () {
    if (this.options.multiSelectHelperControls) this.DeleteSelectedLines();

    var selectedObjects = this.options.selectedObjects;
    if (!selectedObjects) return;

    //if multiselect only one object
    if (selectedObjects && selectedObjects.length == 1) {
        selectedObjects[0].setSelected();
        this.UpdatePropertiesControls();
        return;
    }

    //remove single select
    var selectedObject = this.options.selectedObject;
    if (selectedObject != null && selectedObject.typeComponent != "StiPage" && selectedObject.typeComponent != "StiReport") {
        selectedObject.changeVisibilityStateResizingIcons(false);
    }
    this.options.selectedObject = null;

    var lines = { vert: [], hor: [] };
    var jsObject = this;

    var createLine = function (page, x1, y1, x2, y2) {
        var line = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "line") : document.createElement("line");
        line.page = page;
        line.coord = { x1: x1, y1: y1, x2: x2, y2: y2 };
        line.style.strokeWidth = "1";
        line.style.stroke = jsObject.options.themeColors[jsObject.GetThemeColor()];
        line.style.strokeDasharray = "3,2";

        line.repaint = function () {
            var pageMarginsPx = this.page.marginsPx;
            var x1 = (jsObject.ConvertUnitToPixel(jsObject.StrToDouble(this.coord.x1)) * jsObject.options.report.zoom + pageMarginsPx[0]);
            var y1 = (jsObject.ConvertUnitToPixel(jsObject.StrToDouble(this.coord.y1)) * jsObject.options.report.zoom + pageMarginsPx[1]);
            var x2 = (jsObject.ConvertUnitToPixel(jsObject.StrToDouble(this.coord.x2)) * jsObject.options.report.zoom + pageMarginsPx[0]);
            var y2 = (jsObject.ConvertUnitToPixel(jsObject.StrToDouble(this.coord.y2)) * jsObject.options.report.zoom + pageMarginsPx[1]);

            var roundedCoordinates = jsObject.GetRoundedLineCoordinates([x1, y1, x2, y2]);

            this.setAttribute("x1", roundedCoordinates[0] + jsObject.options.xOffset);
            this.setAttribute("y1", roundedCoordinates[1] + jsObject.options.yOffset);
            this.setAttribute("x2", roundedCoordinates[2] + jsObject.options.xOffset);
            this.setAttribute("y2", roundedCoordinates[3] + jsObject.options.yOffset);
        }

        return line;
    }

    var page;
    var linesPoints = [];

    for (var i = 0; i < selectedObjects.length; i++) {
        var component = selectedObjects[i];
        if (!page && this.options.report) page = this.options.report.pages[component.properties.pageName];
        var leftComp = this.StrToDouble(component.properties.unitLeft);
        var topComp = this.StrToDouble(component.properties.unitTop);
        var rightComp = leftComp + this.StrToDouble(component.properties.unitWidth);
        var bottomComp = topComp + this.StrToDouble(component.properties.unitHeight);
        if (!this.IsContains(lines.vert, leftComp)) lines.vert.push(leftComp);
        if (!this.IsContains(lines.vert, rightComp)) lines.vert.push(rightComp);
        if (!this.IsContains(lines.hor, topComp)) lines.hor.push(topComp);
        if (!this.IsContains(lines.hor, bottomComp)) lines.hor.push(bottomComp);

        for (var k = 0; k < component.controls.borders.length; k++) {
            component.controls.borders[k].style.display = "none";
        }
    }

    lines.hor = lines.hor.sort(function (a, b) { return a - b });
    lines.vert = lines.vert.sort(function (a, b) { return a - b });

    var left = jsObject.RoundPlus(lines.vert[0], 5);
    var right = jsObject.RoundPlus(lines.vert[lines.vert.length - 1], 5);
    var top = jsObject.RoundPlus(lines.hor[0], 5);
    var bottom = jsObject.RoundPlus(lines.hor[lines.hor.length - 1], 5);

    var multiSelectHelperControls = { page: page, lines: [], resizingPoints: [] };
    this.options.multiSelectHelperControls = multiSelectHelperControls;

    var createResizingPoint = function (resizingType) {
        var point = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "rect") : document.createElement("rect");
        point.setAttribute("width", 4);
        point.setAttribute("height", 4);
        point.style.fill = "#696969";
        point.style.strokeWidth = "#696969";
        point.style.stroke = "#696969";
        point.resizingType = resizingType;
        point.style.cursor = jsObject.GetCursorType(resizingType);

        point.onmousedown = function (event) {
            event.preventDefault();
            jsObject.options.startMousePos = [event.clientX || event.x, event.clientY || event.y];
            if (jsObject.options.currentPage) jsObject.options.currentPage.style.cursor = this.style.cursor;
            var selectedObjects = jsObject.options.selectedObjects;
            var fixedComponents = { left: [], top: [], right: [], bottom: [] };
            var selectArea = {
                left: jsObject.ConvertUnitToPixel(left) * jsObject.options.report.zoom,
                top: jsObject.ConvertUnitToPixel(top) * jsObject.options.report.zoom,
                right: jsObject.ConvertUnitToPixel(right) * jsObject.options.report.zoom,
                bottom: jsObject.ConvertUnitToPixel(bottom) * jsObject.options.report.zoom                
            }           
            jsObject.options.in_resize = [[], "Multi" + this.resizingType, [], fixedComponents, selectArea];

            for (var i = 0; i < selectedObjects.length; i++) {
                var startValues = {
                    height: parseInt(selectedObjects[i].getAttribute("height")),
                    width: parseInt(selectedObjects[i].getAttribute("width")),
                    left: parseInt(selectedObjects[i].getAttribute("left")),
                    top: parseInt(selectedObjects[i].getAttribute("top"))
                }
                jsObject.options.in_resize[0].push(selectedObjects[i]);
                jsObject.options.in_resize[2].push(startValues);
                selectedObjects[i].startWidth = startValues.width;
                selectedObjects[i].startHeight = startValues.height;

                var compLeft = jsObject.RoundPlus(jsObject.StrToDouble(selectedObjects[i].properties.unitLeft), 5);
                var compRight = jsObject.RoundPlus(jsObject.StrToDouble(selectedObjects[i].properties.unitLeft) + jsObject.StrToDouble(selectedObjects[i].properties.unitWidth), 5);
                var compTop = jsObject.RoundPlus(jsObject.StrToDouble(selectedObjects[i].properties.unitTop), 5);
                var compBottom = jsObject.RoundPlus(jsObject.StrToDouble(selectedObjects[i].properties.unitTop) + jsObject.StrToDouble(selectedObjects[i].properties.unitHeight), 5);

                if (compLeft == left) fixedComponents.left.push(selectedObjects[i]);
                if (compRight == right) fixedComponents.right.push(selectedObjects[i]);
                if (compTop == top) fixedComponents.top.push(selectedObjects[i]);
                if (compBottom == bottom) fixedComponents.bottom.push(selectedObjects[i]);
            } 
        }

        return point;
    }

    //Paint All Lines
    for (var i = 0; i < lines.hor.length; i++) {
        var line = createLine(page, left, lines.hor[i], right, lines.hor[i]);
        line.repaint();
        multiSelectHelperControls.lines.push(line);
        page.appendChild(line);
    }

    for (var i = 0; i < lines.vert.length; i++) {
        var line = createLine(page, lines.vert[i], top, lines.vert[i], bottom);
        line.repaint();
        multiSelectHelperControls.lines.push(line);
        page.appendChild(line);
    }

    //Paint Resizing Icons
    var pageMarginsPx = page.marginsPx;
    var leftPoint = this.ConvertUnitToPixel(left) * this.options.report.zoom + pageMarginsPx[0];
    var topPoint = this.ConvertUnitToPixel(top) * this.options.report.zoom + pageMarginsPx[1];
    var rightPoint = this.ConvertUnitToPixel(right) * this.options.report.zoom + pageMarginsPx[0];
    var bottomPoint = this.ConvertUnitToPixel(bottom) * this.options.report.zoom + pageMarginsPx[1];

    var resizingTypes = ["LeftTop", "Top", "RightTop", "Right", "RightBottom", "Bottom", "LeftBottom", "Left"];
    for (var i = 0; i < resizingTypes.length; i++) {
        var resizingPoint = createResizingPoint(resizingTypes[i]);
        multiSelectHelperControls.resizingPoints.push(resizingPoint);
        page.appendChild(resizingPoint);

        if (i == 0 || i == 6 || i == 7) resizingPoint.setAttribute("x", parseInt(leftPoint - 2) + this.options.xOffset);
        if (i == 2 || i == 3 || i == 4) resizingPoint.setAttribute("x", parseInt(rightPoint - 2) + this.options.xOffset);
        if (i == 1 || i == 5) resizingPoint.setAttribute("x", parseInt((rightPoint + leftPoint) / 2) - 2 + this.options.xOffset);
        if (i == 0 || i == 1 || i == 2) resizingPoint.setAttribute("y", parseInt(topPoint - 2) + this.options.yOffset);
        if (i == 4 || i == 5 || i == 6) resizingPoint.setAttribute("y", parseInt(bottomPoint - 2) + this.options.yOffset);
        if (i == 3 || i == 7) resizingPoint.setAttribute("y", parseInt((bottomPoint + topPoint) / 2 - 2) + this.options.yOffset);
    }
}

StiMobileDesigner.prototype.DeleteSelectedLines = function () {
    var selectedObjects = this.options.selectedObjects;
    if (selectedObjects) {
        for (var i = 0; i < selectedObjects.length; i++) {
            var borders = selectedObjects[i].controls.borders;
            for (var k = 0; k < borders.length; k++) {
                borders[k].style.display = "";
            }
        }
    }

    var multiSelectHelperControls = this.options.multiSelectHelperControls;
    if (multiSelectHelperControls) {
        for (var i = 0; i < multiSelectHelperControls.lines.length; i++) {
            var page = multiSelectHelperControls.page;
            page.removeChild(multiSelectHelperControls.lines[i]);
        }
        for (var i = 0; i < multiSelectHelperControls.resizingPoints.length; i++) {
            var page = multiSelectHelperControls.page;
            page.removeChild(multiSelectHelperControls.resizingPoints[i]);
        }
    }
    this.options.multiSelectHelperControls = null;
}

StiMobileDesigner.prototype.GetAllComponentsHaveImage = function (pageName) {
    components = "";    
    for (var componentName in this.options.report.pages[pageName].components) {        
        component = this.options.report.pages[pageName].components[componentName];
        if (component.controls.imageContent.href.baseVal != "") {
            if (components != "") components += ";";
            components += componentName;            
        }
    }
        
    return (components != "") ? components : false;
}

StiMobileDesigner.prototype.UpdatePropertiesControls = function () {
    if (this.options.layoutPanel) this.options.layoutPanel.updateControls();
    if (this.options.pagePanel && this.options.workPanel.currentPanel == this.options.pagePanel) this.options.pagePanel.updateControls();
    this.options.homePanel.updateControls();    
    this.options.propertiesPanel.updateControls();
    if (this.options.statusPanel && this.options.selectedObject) {
        this.options.statusPanel.componentNameCell.innerHTML = this.options.selectedObject.typeComponent == "StiReport"
            ? Base64.decode(this.options.selectedObject.properties.reportName.replace("Base64Code;", "")) : this.options.selectedObject.properties.name;
    }
}

StiMobileDesigner.prototype.WriteAllProperties = function(object, properties) {
    for (var propertyName in properties) {
        object.properties[propertyName] = properties[propertyName];
        if (propertyName == "gridSize") {
            object.properties.gridSize = this.StrToDouble(properties.gridSize);
        }
    }
}

StiMobileDesigner.prototype.SetObjectToCenter = function(object) {
    var leftPos = (this.options.mobileDesigner.offsetWidth / 2 - object.offsetWidth / 2);
    var topPos = (this.options.mobileDesigner.offsetHeight / 2 - object.offsetHeight / 2);
    object.style.left = leftPos > 0 ? leftPos + "px" : 0;
    object.style.top = topPos > 0 ? topPos + "px" : 0;
}

StiMobileDesigner.prototype.SetObjectToPropertiesPanelCorner = function (object) {
    var leftPos = this.FindPosX(this.options.propertiesPanel, "stiDesignerMainPanel") + this.options.propertiesPanel.offsetWidth + 10;
    var topPos = this.FindPosY(this.options.propertiesPanel, "stiDesignerMainPanel") + 10;
    object.style.left = leftPos + "px";
    object.style.top = topPos + "px";
}

StiMobileDesigner.prototype.Item = function (name, caption, imageName, key, styleProperties, haveSubMenu) {
    var item = {
        "name": name,
        "caption": caption,
        "imageName": imageName,
        "key": key
    }

    item.styleProperties = styleProperties;
    item.haveSubMenu = haveSubMenu;

    return item;
}

StiMobileDesigner.prototype.GetColorFromBrushStr = function (brushStr) {
    if (brushStr == "StiEmptyValue") return "StiEmptyValue";
    if (brushStr == "0") return "255,255,255";
    brushArray = brushStr.split("!");

    return (brushArray.length > 1) ? brushArray[1] : brushArray[0];
}

StiMobileDesigner.prototype.GetRelationBySourceName = function (parent, name) {
    if (!parent) return null;
    var relations = parent.relations;
    
    for (var i = 0; i < relations.length; i++)    {
        if (relations[i].nameInSource == name)
            return relations[i];
    }
    
    return null;
}

StiMobileDesigner.prototype.FindComponentByName = function(name) {
    if (!this.options.report) return false;
    for (var pageName in this.options.report.pages)
        for (var componentName in this.options.report.pages[pageName].components) {
            var component = this.options.report.pages[pageName].components[componentName];
            if (component.properties.name == name) return component;
        }
    
    return false;
}

StiMobileDesigner.prototype.newGuid = (function () {
    var CHARS = '0123456789abcdefghijklmnopqrstuvwxyz'.split('');
    return function (len, radix) {
        var chars = CHARS, uuid = [], rnd = Math.random;
        radix = radix || chars.length;

        if (len) {
            for (var i = 0; i < len; i++) uuid[i] = chars[0 | rnd() * radix];
        } else {
            var r;
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4';

            for (var i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    r = 0 | rnd() * 16;
                    uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
                }
            }
        }

        return uuid.join('');
    };
})();

StiMobileDesigner.prototype.generateKey = function () {
    return this.newGuid().replace(/-/g, '');
}

StiMobileDesigner.prototype.GetCountObjects = function (objectArray) {
    count = 0;
    if (objectArray)
        for (var singleObject in objectArray) { count++ };
    return count;
}

StiMobileDesigner.prototype.SetEnabledAllControls = function (state) {
    for (var name in this.options.buttons)
        if (!this.options.buttons[name].allwaysEnabled && this.options.buttons[name]["setEnabled"])
            this.options.buttons[name].setEnabled(state);

    for (var name in this.options.controls)
        if (!this.options.controls[name].allwaysEnabled && this.options.controls[name]["setEnabled"])
            this.options.controls[name].setEnabled(state);

    if (!state && this.options.statusPanel) this.options.statusPanel.componentNameCell.innerHTML = "";
}

StiMobileDesigner.prototype.CreateMetaTag = function (head) {   
    if (this.options.head) {
        var meta = document.createElement("META");
        meta.setAttribute("content", "width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;");    
        this.options.head.appendChild(meta);
    }
}

StiMobileDesigner.prototype.GetCustomFontsCssText = function (fontData, fontFamilyName) {
    var cssText = "@font-face {\r\n" +
        "font-family: '" + fontFamilyName + "';\r\n" +
        "src: url(" + fontData + ");\r\n }";

    return cssText;
}

StiMobileDesigner.prototype.AddCustomFontsCss = function (cssText) {
    if (this.options.head && cssText) {
        var style = document.createElement("style");
        style.innerHTML = cssText;
        this.options.head.appendChild(style);
        return style;
    }
    return null;
}

StiMobileDesigner.prototype.ShowComponentForm = function (component) {
    if (!component) return;
    var canChange = (component.properties.restrictions && (component.properties.restrictions == "All" || component.properties.restrictions.indexOf("AllowChange") >= 0)) ||
        !component.properties.restrictions;
    if (!canChange) return;

    if (component.typeComponent == "StiText" || component.typeComponent == "StiTextInCells" ||
        component.typeComponent == "StiZipCode" || component.typeComponent == "StiTableCell") {
        this.InitializeTextEditorForm(function (textEditorForm) {
            textEditorForm.propertyName = component.typeComponent == "StiZipCode" ? "code" : "text";
            textEditorForm.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiImage" || component.typeComponent == "StiTableCellImage") {
        this.InitializeImageForm(function (imageForm) {
            imageForm.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiBarCode") {
        this.SendCommandStartEditBarCodeComponent(component.properties.name);
    }
    else if (component.typeComponent == "StiDataBand" || component.typeComponent == "StiCrossDataBand" ||
        component.typeComponent == "StiHierarchicalBand" || component.typeComponent == "StiTable") {
        this.InitializeDataForm(function (dataForm) {
            dataForm.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiChart") {
        this.InitializeEditChartForm(function (editChartForm) {
            editChartForm.currentChartComponent = component;
            editChartForm.jsObject.SendCommandStartEditChartComponent(component.properties.name);
        });
    }
    else if (component.typeComponent == "StiMap") {
        this.InitializeEditMapForm(function (editMapForm) {
            editMapForm.currentMapComponent = component;
            editMapForm.jsObject.SendCommandStartEditMapComponent(component.properties.name);
        });
    }
    //else if (component.typeComponent == "StiGauge") {
    //    this.InitializeEditGaugeForm(function (editGaugeForm) {
    //        editGaugeForm.currentGaugeComponent = component;
    //        editGaugeForm.jsObject.SendCommandStartEditGaugeComponent(component.properties.name);
    //    });
    //}
    else if (component.typeComponent == "StiRichText" || component.typeComponent == "StiTableCellRichText") {
        component.jsObject.InitializeEditRichTextForm(function (editRichTextForm) {
            editRichTextForm.show();
        });
    }
    else if (component.typeComponent == "StiSubReport") {
        component.jsObject.InitializeSubReportForm(function (subReportForm) {
            subReportForm.show();
        });
    }
    else if (component.typeComponent == "StiGroupHeaderBand" || component.typeComponent == "StiCrossGroupHeaderBand") {
        component.jsObject.InitializeGroupHeaderForm(function (groupHeaderForm) {
            groupHeaderForm.show();
        });
    }
    else if (component.typeComponent == "StiCrossTab") {
        this.SendCommandStartEditCrossTabComponent(component.properties.name);
    }
    else if (component.typeComponent == "StiClone") {
        this.InitializeCloneContainerForm(function (cloneContainerForm) {
            cloneContainerForm.show();
        });
    }
    else if (component.typeComponent == "StiShape") {
        component.jsObject.InitializeShapeForm(function (shapeForm) {
            shapeForm.show();
        });
    }
    else if (component.typeComponent == "StiTableElement") {
        this.InitializeEditTableElementForm(function (form) {
            form.currentTableElement = component;
            form.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiImageElement") {
        this.InitializeEditImageElementForm(function (form) {
            form.currentImageElement = component;
            form.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiTextElement") {
        this.InitializeEditTextElementForm(function (form) {
            form.currentTextElement = component;
            form.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiMapElement") {
        this.InitializeEditMapElementForm(function (form) {
            form.currentMapElement = component;
            form.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiProgressElement") {
        this.InitializeEditProgressElementForm(function (form) {
            form.currentProgressElement = component;
            form.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiIndicatorElement") {
        this.InitializeEditIndicatorElementForm(function (form) {
            form.currentIndicatorElement = component;
            form.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiChartElement") {
        this.InitializeEditChartElementForm(function (form) {
            form.currentChartElement = component;
            form.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiGaugeElement") {
        this.InitializeEditGaugeElementForm(function (form) {
            form.currentGaugeElement = component;
            form.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiShapeElement") {
        this.InitializeEditShapeElementForm(function (form) {
            form.currentShapeElement = component;
            form.changeVisibleState(true);
        });
    }
    else if (component.typeComponent == "StiPivotElement") {
        this.InitializeEditPivotElementForm(function (form) {
            form.currentPivotElement = component;
            form.changeVisibleState(true);
        });
    }
}

StiMobileDesigner.prototype.UpdateStateUndoRedoButtons = function () {
    this.options.buttons.undoButton.setEnabled(true);
    this.options.buttons.redoButton.setEnabled(false);
}

StiMobileDesigner.prototype.BackToSelectedComponent = function (componentName) {
    var selectedComponent = this.FindComponentByName(componentName);
    if (!selectedComponent) selectedComponent = this.options.report.pages[componentName];
    if (!selectedComponent) return;

    if (selectedComponent.typeComponent == "StiPage") {
        if (this.options.currentPage.properties.name != selectedComponent.properties.name)
            this.options.paintPanel.showPage(selectedComponent);
    }
    else {
        if (this.options.currentPage.properties.name != selectedComponent.properties.pageName) {
            var selectedPage = this.options.report.pages[selectedComponent.properties.pageName];
            if (selectedPage) this.options.paintPanel.showPage(selectedPage);
        }
        selectedComponent.setSelected();
        this.SetComponentOnTopLevel(selectedComponent);
        this.UpdatePropertiesControls();
    }
}

StiMobileDesigner.prototype.GetConstMargins = function () {
    var constMargins = {
        "cm" : {"marginsNormal" : "1", "marginsNarrow" : "0.5", "marginsWide" : "2"},
        "in" : {"marginsNormal" : "0.4", "marginsNarrow" : "0.2", "marginsWide" : "0.8"},
        "hi" : {"marginsNormal" : "39", "marginsNarrow" : "19.5", "marginsWide" : "78"},
        "mm" : {"marginsNormal" : "9.9", "marginsNarrow" : "4.95", "marginsWide" : "19.8"}
    }
    
    return constMargins;
}

StiMobileDesigner.prototype.GetUnitShortName = function (unit) {
    if (unit == "Centimeters") return "cm";
    if (unit == "HundredthsOfInch") return "hi";
    if (unit == "Inches") return "in";
    if (unit == "Millimeters") return "mm";
    return "cm";
} 

StiMobileDesigner.prototype.GetColorNameByRGB = function (rgb) {
    for (var i = 0; i < this.ConstWebColors.length; i++) {
        if (this.ConstWebColors[i][1] == rgb) return this.ConstWebColors[i][0];
    }
    
    return false;
}

StiMobileDesigner.prototype.SortArrayToSortStr = function (sortArray) {
    var sortStr = this.loc.FormBand.SortBy + " ";
    for (var i = 0; i < sortArray.length; i++) {        
        sortStr += sortArray[i].column;
        if (i != sortArray.length - 1) sortStr += ",";
    }
    
    return sortStr;
}

StiMobileDesigner.prototype.SetWatermarkImagePos = function (page, widthWatermark, heightWatermark) {    
    switch (page.properties.waterMarkImageAlign) {
        case "TopLeft" : {
            page.controls.waterMarkImage.setAttribute("x", "0");
            page.controls.waterMarkImage.setAttribute("y", "0");
            break;
        }
        case "TopCenter" : {
            page.controls.waterMarkImage.setAttribute("x", page.widthPx/2 - widthWatermark/2);
            page.controls.waterMarkImage.setAttribute("y", "0");
            break;
        }
        case "TopRight" : {
            page.controls.waterMarkImage.setAttribute("x", page.widthPx - widthWatermark);
            page.controls.waterMarkImage.setAttribute("y", "0");
            break;
        }
        case "MiddleLeft" : {
                page.controls.waterMarkImage.setAttribute("x", "0");
            page.controls.waterMarkImage.setAttribute("y", page.heightPx/2 - heightWatermark/2);
            break;
        }
        case "MiddleCenter" : {
            page.controls.waterMarkImage.setAttribute("x", page.widthPx/2 - widthWatermark/2);
            page.controls.waterMarkImage.setAttribute("y", page.heightPx/2 - heightWatermark/2);
            break;
        }
        case "MiddleRight" : {
            page.controls.waterMarkImage.setAttribute("x", page.widthPx - widthWatermark);
            page.controls.waterMarkImage.setAttribute("y", page.heightPx/2 - heightWatermark/2);
            break;
        }
        case "BottomLeft" : {
            page.controls.waterMarkImage.setAttribute("x", "0");
            page.controls.waterMarkImage.setAttribute("y", page.heightPx - heightWatermark);
            break;
        }
        case "BottomCenter" : {
            page.controls.waterMarkImage.setAttribute("x", page.widthPx/2 - widthWatermark/2);
            page.controls.waterMarkImage.setAttribute("y", page.heightPx - heightWatermark);
            break;
        }
        case "BottomRight" : {
            page.controls.waterMarkImage.setAttribute("x", page.widthPx - widthWatermark);
            page.controls.waterMarkImage.setAttribute("y", page.heightPx - heightWatermark);
            break;
        }
    };
}

StiMobileDesigner.prototype.GetElementNumberInArray = function (element, array) {
    if (!array || !array.length) return -1
    return array.indexOf(element);
}

StiMobileDesigner.prototype.ShowHelpWindow = function (url) {
    var lang = this.options.helpLanguage == "ru" ? "ru" : "en";
    this.openNewWindow("https://www.stimulsoft.com/" + lang + "/documentation/online/" + url);
}

StiMobileDesigner.prototype.HelpLinks = {
    "clipboard": "user-manual/index.html?reports_designer_ribbon_mode_2013_ribbon_tabs_home_clipboard.htm",
    "font": "user-manual/index.html?reports_designer_ribbon_mode_2013_ribbon_tabs_home_font.htm",
    "alignment": "user-manual/index.html?reports_designer_ribbon_mode_2013_ribbon_tabs_home_alignment.htm",
    "border": "user-manual/index.html?reports_designer_ribbon_mode_2013_ribbon_tabs_home_borders.htm",
    "borderform": "user-manual/index.html?report_internals_appearance_borders_simple_borders.htm",
    "textformat": "user-manual/index.html?report_internals_text_formatting.htm",
    "style": "user-manual/index.html?report_internals_appearance_styles.htm",
    "insertcomponent": "user-manual/index.html?reports_designer_ribbon_mode_2013_ribbon_tabs_insert.htm",
    "page": "user-manual/index.html?reports_designer_ribbon_mode_2013_ribbon_tabs_page_page_setup.htm",
    "preview": "user-manual/index.html?reports_designer_previewing_reports.htm",
    "data": "user-manual/index.html?report_internals_creating_lists_data_source_of_data_band.htm",
    "filter": "user-manual/index.html?report_internals_creating_lists_data_filterting.htm",
    "image": "user-manual/index.html?report_internals_graphic_information_output_resources_of_images.htm",
    "watermark": "user-manual/index.html?report_internals_watermarks_watermark_property.htm",
    "columns": "user-manual/index.html?report_internals_columns_columns_on_page.htm",
    "sort": "user-manual/index.html?report_internals_creating_lists_data_sorting.htm",
    "expression": "user-manual/index.html?report_internals_expressions.htm",
    "connectionNew": "user-manual/index.html?data_data_dictionary_datasources_creating_data_source.htm",
    "connectionEdit": "user-manual/index.html?data_data_dictionary_connection.htm",
    "relationEdit": "user-manual/index.html?data_data_dictionary_relation_creating_relation.htm",
    "columnEdit": "user-manual/index.html?data_data_dictionary_datasources_creating_and_editing_data_columns.htm",
    "dataSourceEdit": "user-manual/data_data_dictionary_datasources.htm",
    "dataSourceFromOtherDatasources": "user-manual/data_data_dictionary_datasources_data_from_other_data_source.htm",
    "variableEdit": "user-manual/index.html?data_data_dictionary_variables.htm",
    "variableItems": "user-manual/index.html?data_data_dictionary_variables_panel_request_from_user_items_dialog.htm",
    "styleDesigner": "user-manual/index.html?report_internals_appearance_style_designer.htm",
    "createStyleCollection": "user-manual/index.html?report_internals_appearance_style_designer_creating_collection_of_styles.htm",
    "conditions": "user-manual/index.html?report_internals_conditional_formatting.htm",
    "reportSetup": "user-manual/index.html?reports_designer_ribbon_mode_2013_main_menu_dialog_report_setup.htm",
    "options": "user-manual/reports_designer_ribbon_mode_2013_main_menu_dialog_options.htm",
    "parameterEdit": "user-manual/index.html?data_data_dictionary_datasources_queries_parameters.htm",
    "wizard": "user-manual/index.html?reports_designer_creating_reports_in_designer_overview_master-detail_report_wizard.htm",
    "layout": "user-manual/reports_designer_ribbon_mode_2013_ribbon_tabs_layout_arrange.htm",
    "textFormat": "user-manual/report_internals_text_formatting.htm",
    "reportCheck": "user-manual/reports_designer_report_checker.htm",
    "globalizationEditor": "user-manual/reports_designer_globalization_editor.htm",
    "viewOptions": "user-manual/index.html?reports_designer_ribbon_mode_2013_ribbon_tabs_page_viewing_options.htm",
    "richtextform": "user-manual/index.html?report_internals_rich_text_output_rich_text_editor.htm",
    "barcodeform": "user-manual/report_internals_barcodes_editor.htm",
    "cloneform": "user-manual/report_internals_panels_cloning.htm",
    "subreportform": "user-manual/report_internals_sub-reports.htm",
    "crosstabform": "user-manual/index.html?report_internals_crosstable_cross_table_editor_2.htm",
    "chartform": "user-manual/reports_internals_charts_editor.htm",
    "onlineOpenReport": "cloud-reports/stimulsoft_cloud_create_and_open_report.htm",
    "onlineSaveReport": "cloud-reports/stimulsoft_cloud_saving_and_download_report.htm",
    "share": "server-manual/context_menu_of_navigator_menu_sharing_settings.htm"
}

StiMobileDesigner.prototype.ResizeDesigner = function () {
    if (this.options.maximizeMode)
        this.MinimizeDesigner();
    else
        this.MaximizeDesigner();
}

StiMobileDesigner.prototype.MaximizeDesigner = function () {
    var designer = this.options.mainPanel.parentElement;
    this.options.maximizeMode = true;

    designer.setAttribute("styleHistory", designer.getAttribute("style"));
    this.options.mainPanel.setAttribute("styleHistory", this.options.mainPanel.getAttribute("style"));
    designer.removeAttribute("style");
    this.options.mainPanel.removeAttribute("style");

    designer.style.position = "absolute";
    designer.style.zIndex = "10000";
    designer.style.top = "0px";
    designer.style.left = "0px";
    designer.style.bottom = "0px";
    designer.style.right = "0px";
}

StiMobileDesigner.prototype.MinimizeDesigner = function () {
    var designer = this.options.mainPanel.parentElement;
    designer.removeAttribute("style");
    this.options.maximizeMode = false;

    designer.setAttribute("style", designer.getAttribute("styleHistory"));
    this.options.mainPanel.setAttribute("style", this.options.mainPanel.getAttribute("styleHistory"));
}

StiMobileDesigner.prototype.ActionExitDesigner = function () {
    if (this.options.haveExitDesignerEvent)
        this.SendCommandExitDesigner();
    else
        history.back();
}

StiMobileDesigner.prototype.GetConnectionNames = function (databaseType, shortName) {
    var databaseConnection = this.loc.Database.Database;
    var connectionTypeNames = {
        "StiMSAccessDatabase": { "name": databaseConnection.replace("{0}", "MS Access"), "shortName": "MS Access" },
        "StiXmlDatabase": { "name": this.loc.Database.DatabaseXml, "shortName": "XML Data" },
        "StiOdbcDatabase": { "name": databaseConnection.replace("{0}", "ODBC"), "shortName": "ODBC" },
        "StiOleDbDatabase": { "name": databaseConnection.replace("{0}", "OLE DB"), "shortName": "OLE DB" },
        "StiSqlDatabase": { "name": databaseConnection.replace("{0}", "MS SQL"), "shortName": "MS SQL" },
        "StiJdbcDatabase": { "name": databaseConnection.replace("{0}", "JDBC"), "shortName": "JDBC" },
        "StiDB2Database": { "name": databaseConnection.replace("{0}", "IBM DB2"), "shortName": "IBM DB2" },
        "StiFirebirdDatabase": { "name": databaseConnection.replace("{0}", "Firebird"), "shortName": "Firebird" },
        "StiInformixDatabase": { "name": databaseConnection.replace("{0}", "Informix"), "shortName": "Informix" },
        "StiMySqlDatabase": { "name": databaseConnection.replace("{0}", "MySql"), "shortName": "MySQL" },
        "StiOracleDatabase": { "name": databaseConnection.replace("{0}", "Oracle"), "shortName": "Oracle" },
        "StiOracleODPDatabase": { "name": databaseConnection.replace("{0}", "Oracle ODP.NET"), "shortName": "Oracle ODP.NET" },
        "StiPostgreSQLDatabase": { "name": databaseConnection.replace("{0}", "PostgreSQL"), "shortName": "PostgreSQL" },
        "StiSqlCeDatabase": { "name": databaseConnection.replace("{0}", "SQLServerCE"), "shortName": "SQL CE" },
        "StiSQLiteDatabase": { "name": databaseConnection.replace("{0}", "SQLite"), "shortName": "SQLite" },
        "StiTeradataDatabase": { "name": databaseConnection.replace("{0}", "Teradata"), "shortName": "Teradata" },
        "StiSybaseAdsDatabase": { "name": databaseConnection.replace("{0}", "SybaseAds"), "shortName": "SybaseAds" },
        "StiSybaseAseDatabase": { "name": databaseConnection.replace("{0}", "SybaseAse"), "shortName": "SybaseAse" },
        "StiUniDirectDatabase": { "name": databaseConnection.replace("{0}", "Uni Direct"), "shortName": "Uni Direct" },
        "StiVistaDBDatabase": { "name": databaseConnection.replace("{0}", "VistaDB"), "shortName": "VistaDB" },
        "StiDotConnectUniversalDatabase": { "name": databaseConnection.replace("{0}", "DotConnectUniversal"), "shortName": "DotConnectUniversal" },
        "StiEffiProzDatabase": { "name": databaseConnection.replace("{0}", "EffiProz"), "shortName": "EffiProz" },
        "StiMongoDBDatabase": { "name": databaseConnection.replace("{0}", "MongoDB"), "shortName": "MongoDB" },
        "StiJsonDatabase": { "name": this.loc.Database.DatabaseJson, "shortName": "JSON Data" },
        "StiODataDatabase": { "name": databaseConnection.replace("{0}", "OData"), "shortName": "OData" }
    }
    
    if (!connectionTypeNames[databaseType]) return "";
    return shortName ? connectionTypeNames[databaseType].shortName : connectionTypeNames[databaseType].name;
}

StiMobileDesigner.prototype.GetLocalizedAdapterName = function (dataAdapterType) {
    switch (dataAdapterType) {
        case "StiDataTableAdapterService": return this.loc.Adapters.AdapterDataTables;
        case "StiDB2AdapterService": return this.loc.Adapters.AdapterDB2Connection;
        case "StiFirebirdAdapterService": return this.loc.Adapters.AdapterFirebirdConnection;
        case "StiInformixAdapterService": return this.loc.Adapters.AdapterInformixConnection;
        case "StiOracleAdapterService": return this.loc.Adapters.AdapterOracleConnection;
        case "StiPostgreSQLAdapterService": return this.loc.Adapters.AdapterPostgreSQLConnection;
        case "StiSqlCeAdapterService": return this.loc.Adapters.AdapterSqlCeConnection;
        case "StiSQLiteAdapterService": return this.loc.Adapters.AdapterSQLiteConnection;
        case "StiTeradataAdapterService": return this.loc.Adapters.AdapterTeradataConnection;
        case "StiVistaDBAdapterService": return this.loc.Adapters.AdapterVistaDBConnection;
        case "StiOleDbAdapterService": return this.loc.Adapters.AdapterOleDbConnection;
        case "StiSqlAdapterService": return this.loc.Adapters.AdapterSqlConnection;
        case "StiOdbcAdapterService": return this.loc.Adapters.AdapterOdbcConnection;
        case "StiMSAccessAdapterService": return this.loc.Adapters.AdapterConnection.replace("{0}", "MS Access");
        case "StiDBaseAdapterService": return this.loc.Adapters.AdapterDBaseFiles;
        case "StiMySqlAdapterService": return this.loc.Adapters.AdapterMySQLConnection;
        case "StiCrossTabAdapterService": return this.loc.Adapters.AdapterCrossTabDataSource;
        case "StiCsvAdapterService": return this.loc.Adapters.AdapterCsvFiles;
        case "StiDataViewAdapterService": return this.loc.Adapters.AdapterDataViews;
        case "StiUserAdapterService": return this.loc.Adapters.AdapterUserSources;
        case "StiBusinessObjectAdapterService": return this.loc.Adapters.AdapterBusinessObjects;
        case "StiVirtualAdapterService": return this.loc.Adapters.AdapterVirtualSource;
        case "StiBusinessObjectAdapterService": return this.loc.Adapters.AdapterBusinessObjects;
    }

    return this.loc.Adapters.AdapterConnection.replace("{0}", dataAdapterType.replace("Sti", "").replace("AdapterService", ""));
}

StiMobileDesigner.prototype.DataBaseObjects = [    
    { dataBaseType : "StiDB2Database", dataAdapterType : "StiDB2AdapterService", dataSourceType : "StiDB2Source" },    
    { dataBaseType : "StiDotConnectUniversalDatabase", dataAdapterType : "StiDotConnectUniversalAdapterService", dataSourceType : "StiDotConnectUniversalSource" },    
    { dataBaseType : "StiFirebirdDatabase", dataAdapterType : "StiFirebirdAdapterService", dataSourceType : "StiFirebirdSource" },
    { dataBaseType : "StiInformixDatabase", dataAdapterType : "StiInformixAdapterService", dataSourceType : "StiInformixSource" },    
    { dataBaseType : "StiMSAccessDatabase", dataAdapterType : "StiMSAccessAdapterService", dataSourceType : "StiMSAccessSource" },
    { dataBaseType : "StiMySqlDatabase", dataAdapterType : "StiMySqlAdapterService", dataSourceType : "StiMySqlSource" },
    { dataBaseType : "StiODataDatabase", dataAdapterType : "StiODataAdapterService", dataSourceType : "StiODataSource" },
    { dataBaseType : "StiOdbcDatabase", dataAdapterType : "StiOdbcAdapterService", dataSourceType : "StiOdbcSource" },
    { dataBaseType : "StiOleDbDatabase", dataAdapterType : "StiOleDbAdapterService", dataSourceType : "StiOleDbSource" },
    { dataBaseType : "StiOracleDatabase", dataAdapterType : "StiOracleAdapterService", dataSourceType : "StiOracleSource" },
    { dataBaseType : "StiPostgreSQLDatabase", dataAdapterType : "StiPostgreSQLAdapterService", dataSourceType : "StiPostgreSQLSource" },
    { dataBaseType : "StiSqlCeDatabase", dataAdapterType : "StiSqlCeAdapterService", dataSourceType : "StiSqlCeSource" },
    { dataBaseType : "StiSqlDatabase", dataAdapterType : "StiSqlAdapterService", dataSourceType : "StiSqlSource" },
    { dataBaseType : "StiSQLiteDatabase", dataAdapterType : "StiSQLiteAdapterService", dataSourceType : "StiSQLiteSource" },
    { dataBaseType : "StiSybaseDatabase", dataAdapterType : "StiSybaseAdapterService", dataSourceType : "StiSybaseSource" },
    { dataBaseType : "StiTeradataDatabase", dataAdapterType : "StiTeradataAdapterService", dataSourceType : "StiTeradataSource" },
    { dataBaseType : "StiVistaDBDatabase", dataAdapterType : "StiVistaDBAdapterService", dataSourceType : "StiVistaDBSource" },
    { dataBaseType: "StiCsvDatabase", dataAdapterType: "StiDataTableAdapterService", dataSourceType: "StiDataTableSource" },
    { dataBaseType: "StiXmlDatabase", dataAdapterType: "StiDataTableAdapterService", dataSourceType: "StiDataTableSource" },
    { dataBaseType: "StiDBaseDatabase", dataAdapterType: "StiDataTableAdapterService", dataSourceType: "StiDataTableSource" },
    { dataBaseType: "StiExcelDatabase", dataAdapterType: "StiDataTableAdapterService", dataSourceType: "StiDataTableSource" },
    { dataBaseType: "StiJsonDatabase", dataAdapterType: "StiDataTableAdapterService", dataSourceType: "StiDataTableSource" },
    { dataBaseType: "StiMongoDbDatabase", dataAdapterType: "StiMongoDbAdapterService", dataSourceType: "StiMongoDbSource" },
    { dataBaseType: null, dataAdapterType: "StiBusinessObjectAdapterService", dataSourceType: "StiBusinessObjectSource" },
    { dataBaseType: null, dataAdapterType: "StiVirtualAdapterService", dataSourceType: "StiVirtualSource" },
    { dataBaseType: null, dataAdapterType: "StiCrossTabAdapterService", dataSourceType: "StiCrossTabDataSource" }
]

StiMobileDesigner.prototype.GetDataSourceTypeFromDataAdapterType = function (dataAdapterType) {
    for (var i = 0; i < this.DataBaseObjects.length; i++) {
        if (this.DataBaseObjects[i].dataAdapterType == dataAdapterType) return this.DataBaseObjects[i].dataSourceType;
    }
    return null;
}

StiMobileDesigner.prototype.GetDataAdapterTypeFromDatabaseType = function (dataBaseType) {
    for (var i = 0; i < this.DataBaseObjects.length; i++) {
        if (this.DataBaseObjects[i].dataBaseType == dataBaseType) return this.DataBaseObjects[i].dataAdapterType;
    }
    return null;
}

StiMobileDesigner.prototype.CopyObject = function (o) {
    if (!o || "object" !== typeof o) {
        return o;
    }
    var c = "function" === typeof o.pop ? [] : {};
    var p, v;
    for (p in o) {
        if (o.hasOwnProperty(p)) {
            v = o[p];
            if (v && "object" === typeof v) {
                c[p] = this.CopyObject(v);
            }
            else c[p] = v;
        }
    }
    return c;
}

StiMobileDesigner.prototype.ClearStyles = function (object) {
    object.className = "stiDesignerClearAllStyles";
}

StiMobileDesigner.prototype.RepaintControlByAttributes = function (control, font, brush, textBrush, border) {
    //Font
    if (font) {
        var fontAttr = font.split("!");
        control.style.fontFamily = fontAttr[0];
        control.style.fontSize = fontAttr[1] + "pt";
        control.style.fontWeight = (fontAttr[2] == "1") ? "bold" : "";
        control.style.fontStyle = (fontAttr[3] == "1") ? "italic" : "";
        control.style.textDecoration = (fontAttr[4] == "1") ? "underline" : "";
    }

    //Brush
    if (brush) {
        var brushColor = this.GetColorFromBrushStr(brush);
        if (brushColor != "transparent") brushColor = "rgb(" + brushColor + ")";
        control.style.background = brushColor;
    }

    //TextBrush
    if (textBrush) {
        var textBrushColor = this.GetColorFromBrushStr(textBrush);
        if (textBrushColor != "transparent") textBrushColor = "rgb(" + textBrushColor + ")";
        control.style.color = textBrushColor;
    }

    //Border    
    control.style.border = "1px dashed #b4b4b4";
    if (border && border != "default") {
        var borderStyles = ["solid", "dashed", "dashed", "dashed", "dotted", "double", "none"];
        var borderAttr = border.split("!");
        var borderSides = borderAttr[0].split(",");
        var borderColor = borderAttr[2];
        if (borderAttr[2] != "transparent") borderColor = "rgb(" + borderAttr[2] + ")";
        var borderStyle = borderStyles[borderAttr[3]];
        var borderSize = borderAttr[1];

        if (borderSides[0] == "1") control.style.borderLeft = borderSize + "px " + borderStyle + " " + borderColor;
        if (borderSides[1] == "1") control.style.borderTop = borderSize + "px " + borderStyle + " " + borderColor;
        if (borderSides[2] == "1") control.style.borderRight = borderSize + "px " + borderStyle + " " + borderColor;
        if (borderSides[3] == "1") control.style.borderBottom = borderSize + "px " + borderStyle + " " + borderColor;
    }
}

StiMobileDesigner.prototype.FilterDataToShortString = function (filterData) {
    return (this.GetCountObjects(filterData) != 0)
        ? "[" + this.loc.PropertyMain.Filters + "]"
        : "[" + this.loc.FormBand.NoFilters + "]";
}

StiMobileDesigner.prototype.SortDataToShortString = function (sortData) {
    if (sortData == "")
        return "[" + this.loc.FormBand.NoSort + "]";
    else {
        var sorts = JSON.parse(Base64.decode(sortData));
        return this.SortArrayToSortStr(sorts);
    }
}

StiMobileDesigner.prototype.GetThemeColor = function () {
    var themeColor = this.options.theme;
    if (themeColor == "Purple") return "Violet";
    if (themeColor == "Blue" || themeColor == "Office2013") return "Blue";
    themeColor = themeColor.replace("Office2013White", "");
    themeColor = themeColor.replace("Office2013LightGray", "");
    themeColor = themeColor.replace("Office2013VeryDarkGray", "");
    themeColor = themeColor.replace("Office2013DarkGray", "");

    return themeColor;
}

StiMobileDesigner.prototype.SortByName = function (a, b) {
    if (a.name && b.name) {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
    }
    return 0
}

StiMobileDesigner.prototype.SortByIndex = function (a, b) {
    if (a.index != null && b.index != null) {
        if (a.index < b.index) return -1;
        if (a.index > b.index) return 1;
    }
    return 0
}

StiMobileDesigner.prototype.SortByCaption = function (a, b) {
    if (a.caption && b.caption) {
        if (a.caption < b.caption) return -1;
        if (a.caption > b.caption) return 1;
    }
    return 0
}

StiMobileDesigner.prototype.dec2hex = function (d) {
    if (d > 15) {
        return d.toString(16)
    } else {
        return "0" + d.toString(16)
    }
}

StiMobileDesigner.prototype.rgb = function (r, g, b) {
    return "#" + this.dec2hex(r) + this.dec2hex(g) + this.dec2hex(b)
};

StiMobileDesigner.prototype.GetNavigatorName = function () {
    var useragent = navigator.userAgent;
    var navigatorname = "Unknown";
    if (this.GetIEVersion() > 0) {
        navigatorname = "MSIE";
    }
    else if (useragent.indexOf('Gecko') != -1) {
        if (useragent.indexOf('Chrome') != -1)
            navigatorname = "Google Chrome";
        else navigatorname = "Mozilla";
    }
    else if (useragent.indexOf('Mozilla') != -1) {
        navigatorname = "old Netscape or Mozilla";
    }
    else if (useragent.indexOf('Opera') != -1) {
        navigatorname = "Opera";
    }

    return navigatorname;
}

StiMobileDesigner.prototype.GetIEVersion = function () {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    // If IE, return version number.
    if (Idx > 0)
        return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));

    // If IE 11 then look for Updated user agent string.
    else if (!!navigator.userAgent.match(/Trident\/7\./))
        return 11;

    else
        return 0; //It is not IE
}

StiMobileDesigner.prototype.RgbColorToHexColor = function (rgbColor) {
    var jsObject = this;
    var colorArrayToHex = function (colorArray) {
        var hex_alphabets = "0123456789ABCDEF";
        var hex = "#";
        var int1, int2;
        for (var i = 0; i < 3; i++) {
            int1 = jsObject.StrToInt(colorArray[i]) / 16;
            int2 = jsObject.StrToInt(colorArray[i]) % 16;

            hex += hex_alphabets.charAt(int1) + hex_alphabets.charAt(int2);
        }
        return (hex);
    }

    rgbColor = rgbColor.toString().toLowerCase().replace("rgb(", "").replace(")", "");
    colorArray = rgbColor.split(",");
    if (colorArray.length >= 3) { return colorArrayToHex(colorArray); }
    else return "#000000";
}

StiMobileDesigner.prototype.IsContains = function (array, item) {
    if (!array) return false;
    for (var index in array)
        if (item == array[index]) return true;

    return false;
}

StiMobileDesigner.prototype.GetSystemVariableDescription = function (name) {
    var text = "<b>" + name + "</b><br><br>";
    text += this.loc.SystemVariables[name];

    return text;
}

StiMobileDesigner.prototype.GetFunctionDescription = function (itemObject) {
    var text = "<b>" + itemObject.descriptionHeader + "</b><br/><br/>";
    text += itemObject.description + "<br/><br/>";
    
    if (itemObject.argumentNames && itemObject.argumentNames.length > 0 &&
        itemObject.argumentDescriptions && itemObject.argumentDescriptions.length > 0 &&
        itemObject.argumentNames.length == itemObject.argumentDescriptions.length) 
    {
        text += "<b>" + this.loc.PropertyMain.Parameters + "</b><br/>";
        for (var i = 0; i < itemObject.argumentNames.length; i++) {
            text += itemObject.argumentNames[i] + " - " + itemObject.argumentDescriptions[i] + "<br/>";
        }
        text += "<br/>";
    }

    text += "<b>" + this.loc.PropertyMain.ReturnValue + "</b><br/>";
    text += itemObject.returnDescription + "<br/><br/>";

    return text;
}

StiMobileDesigner.prototype.EndsWith = function (str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
};

StiMobileDesigner.prototype.ShowReportInTheViewer = function (reportName) {
    var params = {
        sessionKey: this.options.cloudParameters.sessionKey,
        reportName: reportName,
        attachedItems: this.options.report.getAttachedItems()
    };
    this.SendCommandCloneItemResourceSave(params);
};

StiMobileDesigner.prototype.UpperFirstChar = function (text) {
    return text.charAt(0).toUpperCase() + text.substr(1);
};

StiMobileDesigner.prototype.LowerFirstChar = function (text) {
    return text.charAt(0).toLowerCase() + text.substr(1);
};

StiMobileDesigner.prototype.IsBandComponent = function (component) {
    return ((component.typeComponent.indexOf("Band") != -1 && component.typeComponent.indexOf("Cross") == -1) || component.typeComponent == "StiTable");
};

StiMobileDesigner.prototype.IsCrossBandComponent = function (component) {
    return (component.typeComponent.indexOf("Cross") != -1 && component.typeComponent != "StiCrossTab" && component.typeComponent != "StiCrossField");
};

StiMobileDesigner.prototype.addEvent = function (element, eventName, fn) {
    if (element.addEventListener) element.addEventListener(eventName, fn, false);
    else if (element.attachEvent) element.attachEvent('on' + eventName, fn);
}

StiMobileDesigner.prototype.Is_array = function (variable) {
    return (typeof variable == "object") && (variable instanceof Array);
}

StiMobileDesigner.prototype.RemoveElementFromArray = function (array, element) {
    for (var i = 0; i < array.length; i++) {
        if (element == array[i]) {
            array.splice(array.indexOf(element), 1);
        }
    }
}

StiMobileDesigner.prototype.GetCommonObject = function (objects) {
    if (!objects || objects.length == 0) return null;

    var commonObject = {};
    commonObject.jsObject = this;
    commonObject.properties = {};
    commonObject.typeComponent = null;

    for (var i = 0; i < objects.length; i++) {
        for (var propertyName in objects[i].properties) {
            var typeComponent = objects[i].typeComponent;
            if (propertyName in commonObject.properties) continue;
            var propertyValue = objects[i].properties[propertyName];
            var sameValue = true;
            var sameTypeComponent = true;
            for (var k = 0; k < objects.length; k++) {
                var haveThisProperty = true;
                if (!(propertyName in objects[k].properties)) {
                    haveThisProperty = false;
                    break;
                }
                if (propertyValue != objects[k].properties[propertyName]) sameValue = false;
                if (typeComponent != objects[k].typeComponent) sameTypeComponent = false;
            }
            if (haveThisProperty) {
                commonObject.properties[propertyName] = sameValue ? propertyValue : "StiEmptyValue";
                if (propertyName == "font") commonObject.properties[propertyName] = this.GetCommonPropertyValue(objects, "font", "!");
                if (propertyName == "border") {
                    var borderSides = [];
                    for (var j = 0; j < objects.length; j++) {
                        if (!objects[j].properties.border) continue;
                        var borderSidesValue = objects[j].properties.border.split("!");
                        borderSides.push({ properties: { borderSides: borderSidesValue[0]} })
                    }
                    var borderSidesCommonValue = this.GetCommonPropertyValue(borderSides, "borderSides", ",");
                    var borderCommonValue = this.GetCommonPropertyValue(objects, "border", "!");
                    borderCommonValue = borderSidesCommonValue + borderCommonValue.substring(borderCommonValue.indexOf("!"));
                    commonObject.properties[propertyName] = borderCommonValue;
                }
                if (propertyName == "textFormat") {
                    var commonTextFormatType = "StiEmptyValue";
                    for (var j = 0; j < objects.length; j++) {
                        if (commonTextFormatType != "StiEmptyValue" && commonTextFormatType != objects[j].properties.textFormat.type) {
                            commonTextFormatType = "StiEmptyValue";
                            break;
                        }
                        commonTextFormatType = objects[j].properties.textFormat.type;
                    }
                    commonObject.properties.textFormat = { type: commonTextFormatType };
                }
                commonObject.typeComponent = sameTypeComponent ? typeComponent : null;
            }
        }
    }

    return commonObject;
}

StiMobileDesigner.prototype.GetCommonPropertyValue = function (objects, propertyName, separator) {
    var commonArray = null;

    for (var i = 0; i < objects.length; i++) {
        if (!objects[i].properties[propertyName]) continue;
        var propertyArray = objects[i].properties[propertyName].split(separator);
        if (!commonArray) commonArray = this.CopyObject(propertyArray);
        for (var k = 0; k < propertyArray.length; k++) {
            if (commonArray[k] != propertyArray[k]) commonArray[k] = "StiEmptyValue";
        }
    }

    var result = "";
    for (var i = 0; i < commonArray.length; i++) {
        if (i != 0) result += separator;
        result += commonArray[i];
    }

    return result;
}

StiMobileDesigner.prototype.GetCommonPositionsArray = function (objects) {
    var commonArray = null;

    for (var i = 0; i < objects.length; i++) {
        if (!ComponentCollection[objects[i].typeComponent]) continue;
        var positionsArray = ComponentCollection[objects[i].typeComponent][5].split(",");
        if (!commonArray) commonArray = this.CopyObject(positionsArray);
        for (var k = 0; k < positionsArray.length; k++) {
            if (commonArray[k] != positionsArray[k]) commonArray[k] = "0";
        }
    }

    return commonArray;
}

StiMobileDesigner.prototype.ApplyPropertyValue = function (propertyName, propertyValue, updateAllProperties) {
    var selectedObjects = this.options.selectedObjects || [this.options.selectedObject];
    if (!selectedObjects) return;
    var propertyNames = this.Is_array(propertyName) ? propertyName : [propertyName];
    var propertyValues = this.Is_array(propertyValue) ? propertyValue : [propertyValue];

    if (propertyValues.length == propertyNames.length) {
        for (var i = 0; i < selectedObjects.length; i++)
            for (var k = 0; k < propertyNames.length; k++)
                selectedObjects[i].properties[propertyNames[k]] = propertyValues[k];
        this.SendCommandSendProperties(selectedObjects, propertyNames, updateAllProperties);
    }
}

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

StiMobileDesigner.prototype.ConcatColumns = function (columns1, columns2) {
    var commonColumns = this.CopyObject(columns1);
    for (var i = 0; i < columns2.length; i++) {
        var addColumn = true;
        for (var k = 0; k < columns1.length; k++) {
            if (columns1[k].name == columns2[i].name && columns1[k].nameInSource == columns2[i].nameInSource && columns1[k].alias == columns2[i].alias &&
            columns1[k].isCalcColumn == columns2[i].isCalcColumn && columns1[k].type == columns2[i].type) {
                addColumn = false;
            }
        }
        if (addColumn) commonColumns.push(columns2[i]);
    }

    return commonColumns;
}

StiMobileDesigner.prototype.SaveCurrentStylePropertiesToObject = function (object) {
    if (!object) object = {};
    var selectedObject = this.options.selectedObjects ? this.options.selectedObjects[0] : this.options.selectedObject;
    if (selectedObject) {
        var styleProps = this.GetStylePropertiesFromComponent(selectedObject);
        for (var i = 0; i < styleProps.length; i++) {
            object[styleProps[i].name] = styleProps[i].value;
        }
    }
}

StiMobileDesigner.prototype.SaveLastStyleProperties = function (component) {
    if (component && !this.IsBandComponent(component) && component.typeComponent != "StiPage" && component.typeComponent != "StiReport") {
        this.options.lastStyleProperties = this.GetStylePropertiesFromComponent(component, true);
    }
}

StiMobileDesigner.prototype.GetStylePropertiesFromComponent = function (component, ignoreComponentStyle) {
    var properties = [];
    if (component) {
        var propertyNames = ["border", "brush", "backColor", "textBrush", "font", "horAlignment", "vertAlignment"];
        if (!ignoreComponentStyle) propertyNames.push("componentStyle");

        for (var i = 0; i < propertyNames.length; i++) {
            if (component.properties[propertyNames[i]] != null && component.properties[propertyNames[i]] != "StiEmptyValue") {
                properties.push({
                    name: propertyNames[i],
                    value: component.properties[propertyNames[i]]
                });
                if (propertyNames[i] == "brush" && (!component.properties.backColor || component.properties.backColor == "StiEmptyValue")) {
                    properties.push({
                        name: "backColor",
                        value: this.GetColorFromBrushStr(component.properties.brush)
                    });
                }
                if (propertyNames[i] == "backColor" && (!component.properties.brush || component.properties.brush == "StiEmptyValue")) {
                    properties.push({
                        name: "brush",
                        value: "1!" + component.properties.backColor
                    });
                }
            }
        }
    }
    return properties;
}

StiMobileDesigner.prototype.SetStylePropertiesToComponent = function (component, styleProperties) {
    if (styleProperties) {
        var propertyNames = [];
        for (var propertyName in styleProperties) {
            if (component.properties[propertyName] != null) {
                component.properties[propertyName] = styleProperties[propertyName];
                propertyNames.push(propertyName)
            }
        }
        this.SendCommandSendProperties(component, propertyNames, true);
    };
}

StiMobileDesigner.prototype.SetCookie = function (name, value, path, domain, secure, expires) {
    if (value && typeof (value) == "string" && value.length >= 4096) return;
    var pathName = location.pathname;
    var expDate = new Date();
    expDate.setTime(expDate.getTime() + (365 * 24 * 3600 * 1000));
    document.cookie = name + "=" + escape(value) +
        "; expires=" + (expires || expDate.toGMTString()) +
        ((path) ? "; path=" + path : "; path=/") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

StiMobileDesigner.prototype.GetCookie = function (name) {
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset)
            if (end == -1) {
                end = cookie.length;
            }
            setStr = unescape(cookie.substring(offset, end));
        }
    }
    return (setStr);
}

StiMobileDesigner.prototype.AddProgressToControl = function (control) {
    if (!control) return;
    var progress = this.Progress();
    progress.style.display = "none";
    control.appendChild(progress);
    control.progress = progress;
    progress.owner = control;

    progress.show = function (left, top) {
        this.style.display = "";
        this.style.left = (left || (this.owner.offsetWidth / 2 - this.offsetWidth / 2)) + "px";
        this.style.top = (top || (this.owner.offsetHeight / 2 - this.offsetHeight / 2)) + "px";
    }

    progress.hide = function () {
        this.style.display = "none";
    }

    return progress;
}

StiMobileDesigner.prototype.Progress = function () {    
    var progressContainer = document.createElement("div");
    progressContainer.style.position = "absolute";
    progressContainer.style.zIndex = "1000";

    var progress = document.createElement("div");
    progressContainer.appendChild(progress);
    progress.className = "mobile_designer_loader";
    
    return progressContainer;
}

StiMobileDesigner.prototype.ProgressMini = function (color) {
    var progressContainer = document.createElement("div");
    progressContainer.style.width = "18px";
    progressContainer.style.height = "18px";
    progressContainer.style.overflow = "hidden";

    var progress = document.createElement("div");
    progressContainer.appendChild(progress);
    

    progress.className = color == "white" ? "mobile_designer_loader_mini_white" : "mobile_designer_loader_mini_color";

    return progressContainer;
}

StiMobileDesigner.prototype.HatchData = [
    "000000FF00000000",	//HatchStyleHorizontal = 0
    "1010101010101010",	//HatchStyleVertical = 1,			
    "8040201008040201",	//HatchStyleForwardDiagonal = 2,	
    "0102040810204080",	//HatchStyleBackwardDiagonal = 3,	
    "101010FF10101010",	//HatchStyleCross = 4,			
    "8142241818244281",	//HatchStyleDiagonalCross = 5,	
    "8000000008000000",	//HatchStyle05Percent = 6,		
    "0010000100100001",	//HatchStyle10Percent = 7,		
    "2200880022008800",	//HatchStyle20Percent = 8,		
    "2288228822882288",	//HatchStyle25Percent = 9,		
    "2255885522558855",	//HatchStyle30Percent = 10,		
    "AA558A55AA55A855",	//HatchStyle40Percent = 11,		
    "AA55AA55AA55AA55",	//HatchStyle50Percent = 12,		
    "BB55EE55BB55EE55",	//HatchStyle60Percent = 13,		
    "DD77DD77DD77DD77",	//HatchStyle70Percent = 14,		
    "FFDDFF77FFDDFF77",	//HatchStyle75Percent = 15,		
    "FF7FFFF7FF7FFFF7",	//HatchStyle80Percent = 16,		
    "FF7FFFFFFFF7FFFF",	//HatchStyle90Percent = 17,		
    "8844221188442211",	//HatchStyleLightDownwardDiagonal = 18,	
    "1122448811224488",	//HatchStyleLightUpwardDiagonal = 19,	
    "CC663399CC663399",	//HatchStyleDarkDownwardDiagonal = 20,	
    "993366CC993366CC",	//HatchStyleDarkUpwardDiagonal = 21,	
    "E070381C0E0783C1",	//HatchStyleWideDownwardDiagonal = 22,	
    "C183070E1C3870E0",	//HatchStyleWideUpwardDiagonal = 23,	
    "4040404040404040",	//HatchStyleLightVertical = 24,			
    "00FF000000FF0000",	//HatchStyleLightHorizontal = 25,		
    "AAAAAAAAAAAAAAAA",	//HatchStyleNarrowVertical = 26,		
    "FF00FF00FF00FF00",	//HatchStyleNarrowHorizontal = 27,		
    "CCCCCCCCCCCCCCCC",	//HatchStyleDarkVertical = 28,			
    "FFFF0000FFFF0000",	//HatchStyleDarkHorizontal = 29,		
    "8844221100000000",	//HatchStyleDashedDownwardDiagonal = 30,
    "1122448800000000",	//HatchStyleDashedUpwardDiagonal = 311,	
    "F00000000F000000",	//HatchStyleDashedHorizontal = 32,		
    "8080808008080808",	//HatchStyleDashedVertical = 33,		
    "0240088004200110",	//HatchStyleSmallConfetti = 34,			
    "0C8DB130031BD8C0",	//HatchStyleLargeConfetti = 35,		
    "8403304884033048",	//HatchStyleZigZag = 36,			
    "00304A8100304A81",	//HatchStyleWave = 37,				
    "0102040818244281",	//HatchStyleDiagonalBrick = 38,		
    "202020FF020202FF",	//HatchStyleHorizontalBrick = 39,	
    "1422518854224588",	//HatchStyleWeave = 40,				
    "F0F0F0F0AA55AA55",	//HatchStylePlaid = 41,				
    "0100201020000102",	//HatchStyleDivot = 42,				
    "AA00800080008000",	//HatchStyleDottedGrid = 43,		
    "0020008800020088",	//HatchStyleDottedDiamond = 44,		
    "8448300C02010103",	//HatchStyleShingle = 45,			
    "33FFCCFF33FFCCFF",	//HatchStyleTrellis = 46,			
    "98F8F877898F8F77",	//HatchStyleSphere = 47,			
    "111111FF111111FF",	//HatchStyleSmallGrid = 48,			
    "3333CCCC3333CCCC",	//HatchStyleSmallCheckerBoard = 49,	
    "0F0F0F0FF0F0F0F0",	//HatchStyleLargeCheckerBoard = 50,	
    "0502058850205088",	//HatchStyleOutlinedDiamond = 51,	
    "10387CFE7C381000",	//HatchStyleSolidDiamond = 52,
    "0000000000000000"	//HatchStyleTotal = 53
];

StiMobileDesigner.prototype.HexToByteString = function(hex) {
    var result = "0000";
    switch (hex) {
        case "1":
            result = "0001";
            break;
        
        case "2":
            result = "0010";
            break;

        case "3":
            result = "0011";
            break;

        case "4":
            result = "0100";
            break;

        case "5":
            result = "0101";
            break;

        case "6":
            result = "0110";
            break;

        case "7":
            result = "0111";
            break;

        case "8":
            result = "1000";
            break;

        case "9":
            result = "1001";
            break;

         case "A":
            result = "1010";
            break;

         case "B":
            result = "1011";
            break;

         case "C":
            result = "1100";
            break;

         case "D":
            result = "1101";
            break;

         case "E":
            result = "1110";
            break;

         case "F":
            result = "1111";
           break;
    }

    return result;
 }

 StiMobileDesigner.prototype.GetSvgHatchBrush = function (brushProps, width, height) {
     var brushSvg = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "svg") : document.createElement("svg");

     var foreColor = brushProps[1];
     var backColor = brushProps[2];

     var hatchNumber = this.StrToInt(brushProps[3]);
     if (hatchNumber > 53) hatchNumber = 53;

     var brushId = this.newGuid();

     var brushPattern = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "pattern") : document.createElement("pattern");
     brushSvg.appendChild(brushPattern);

     brushPattern.setAttribute("id", brushId);
     brushPattern.setAttribute("x", "0");
     brushPattern.setAttribute("y", "0");
     brushPattern.setAttribute("width", "8");
     brushPattern.setAttribute("height", "8");
     brushPattern.setAttribute("patternUnits", "userSpaceOnUse");

     var sb = "";
     var hatchHex = this.HatchData[hatchNumber];

     for (var index = 0; index < 16; index++) {
         sb += this.HexToByteString(hatchHex.charAt(index));
     }
     
     var brushRect = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "rect") : document.createElement("rect");
     brushPattern.appendChild(brushRect);
     brushRect.setAttribute("x", "0");
     brushRect.setAttribute("y", "0");
     brushRect.setAttribute("width", "8");
     brushRect.setAttribute("height", "8");
     brushRect.setAttribute("fill", "rgb(" + this.ColorToRGBStr(backColor) + ")");


     for (var indexRow = 0; indexRow < 8; indexRow++) {
         for (var indexColumn = 0; indexColumn < 8; indexColumn++) {

             var indexChar = sb.charAt(indexRow * 8 + indexColumn);

             if (indexChar == "1") {
                 var brushRect2 = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "rect") : document.createElement("rect");
                 brushPattern.appendChild(brushRect2);
                 brushRect2.setAttribute("x", indexColumn);
                 brushRect2.setAttribute("y", indexRow.toString());
                 brushRect2.setAttribute("width", "1");
                 brushRect2.setAttribute("height", "1");
                 brushRect2.setAttribute("fill", "rgb(" + this.ColorToRGBStr(foreColor) + ")");

             }
         }
     }

     var brushFinalRect = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "rect") : document.createElement("rect");
     brushSvg.appendChild(brushFinalRect);
     brushFinalRect.setAttribute("fill", "url(#" + brushId + ")");
     brushFinalRect.setAttribute("x", "0");
     brushFinalRect.setAttribute("y", "0");
     brushFinalRect.setAttribute("width", width);
     brushFinalRect.setAttribute("height", height);

     return brushSvg;
 }

 StiMobileDesigner.prototype.GetTextFormatLocalizedName = function (type) {
     switch (type) {
         case "StiGeneralFormatService": return this.loc.FormFormatEditor.General;
         case "StiNumberFormatService": return this.loc.FormFormatEditor.Number;
         case "StiCurrencyFormatService": return this.loc.FormFormatEditor.Currency;
         case "StiDateFormatService": return this.loc.FormFormatEditor.Date;
         case "StiTimeFormatService": return this.loc.FormFormatEditor.Time;
         case "StiPercentageFormatService": return this.loc.FormFormatEditor.Percentage;
         case "StiBooleanFormatService": return this.loc.FormFormatEditor.Boolean;
         case "StiCustomFormatService": return this.loc.FormFormatEditor.Custom;
     }

     return "";
 }

 StiMobileDesigner.prototype.GetUserCrossTabStyles = function () {
     var userStyles = [];

     if (this.options.report) {
         for (var i = 0; i < this.options.report.stylesCollection.length; i++) {
             if (this.options.report.stylesCollection[i].type == "StiCrossTabStyle")
                 userStyles.push(this.options.report.stylesCollection[i]);
         }
     }

     return userStyles;
 }

 StiMobileDesigner.prototype.IsTableCell = function (component) {
     if (!component) return false;

     if (this.Is_array(component)) {
         for (var i = 0; i < component.length; i++) {
             if (!component[i].typeComponent) return false;
             else if (component[i].typeComponent.indexOf("StiTableCell") != 0) return false;
         }
         return true;
     }
     else if (component.typeComponent && component.typeComponent.indexOf("StiTableCell") == 0) return true;

     return false;
 }

 StiMobileDesigner.prototype.GetControlValue = function (control) {
     if (control["setKey"] != null) return control.key;
     else if (control["setChecked"] != null) return control.isChecked;
     else if (control["value"] != null) return control.value;
     else if (control["textBox"] != null && this.textBox["value"] != null) return control.textBox.value;
     return null;
 }

 StiMobileDesigner.prototype.SetControlValue = function (control, value) {
     if (control["setKey"] != null) control.setKey(value);
     else if (control["setChecked"] != null) return control.setChecked(value);
     else if (control["value"] != null) return control.value = value;
     else if (control["textBox"] != null && this.textBox["value"] != null) control.textBox.value = value;
 }

 StiMobileDesigner.prototype.HaveTableCell = function (components) {
     if (!components) return false;
     for (var i = 0; i < components.length; i++) {
         if (components[i].typeComponent.indexOf("StiTableCell") == 0) return true;
     }
     return false;
 }

 StiMobileDesigner.prototype.RebuildTable = function (table, cells, removeOldCells) {
     var page = this.options.report.pages[table.properties.pageName];

     //Remove old cells
     if (removeOldCells) {
         if (table.properties.childs) {
             var childNames = table.properties.childs.split(",");
             for (var indexChild = 0; indexChild < childNames.length; indexChild++) {
                 var child = page.components[childNames[indexChild]];
                 if (child) {
                     page.removeChild(child);
                     delete page.components[childNames[indexChild]];
                 }
             }
         }
     }
     //Add or change new cells
     for (var i = 0; i < cells.length; i++) {
         var compObject = cells[i];

         var component = page.components[compObject.name];
         if (component) {
             this.CreateComponentProperties(component, compObject);
             if (ComponentCollection[component.typeComponent][3] != "none") this.CreateComponentNameContent(component);
         }
         else {
             component = this.CreateComponent(compObject);
             page.appendChild(component);
             page.components[compObject.name] = component;
         }
         component.repaint();
     }
 }

 StiMobileDesigner.prototype.CompareNumbers = function (num1, num2, accuracy) {
     return (Math.abs(num1 - num2) <= accuracy);
 }

 StiMobileDesigner.prototype.GetAllResizingCells = function (component, compResizingType, compStartValues) {
     var page = this.options.report.pages[component.properties.pageName];
     var cells = [];
     if (page) {
         var accuracy = 3;
         var table = component.typeComponent == "StiTable" ? component : page.components[component.properties.parentName];
         if (table && table.typeComponent == "StiTable") {
             var cellNames = table.properties.childs.split(",");
             for (var indexCell = 0; indexCell < cellNames.length; indexCell++) {
                 var cell = page.components[cellNames[indexCell]];
                 if (cell && cell.style.display != "none") {
                     var startValues = {};
                     startValues.height = parseInt(cell.getAttribute("height"));
                     startValues.width = parseInt(cell.getAttribute("width"));
                     startValues.left = parseInt(cell.getAttribute("left"));
                     startValues.top = parseInt(cell.getAttribute("top"));
                     cell.startValues = startValues;
                     if (cell == component) continue;
                     if (component.typeComponent == "StiTable") {
                         cells.push(cell);
                         continue;
                     }

                     switch (compResizingType) {
                         case "LeftTop":
                             {
                                 if (this.CompareNumbers(compStartValues.left, startValues.left + startValues.width, accuracy) &&
                                     this.CompareNumbers(compStartValues.top, startValues.top, accuracy)) {
                                     cell.resizingType = "RightTop";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left, startValues.left + startValues.width, accuracy) &&
                                     this.CompareNumbers(compStartValues.top, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "RightBottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left, startValues.left, accuracy) &&
                                     this.CompareNumbers(compStartValues.top, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "LeftBottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.top, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "Bottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.top, startValues.top, accuracy)) {
                                     cell.resizingType = "Top";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left, startValues.left + startValues.width, accuracy)) {
                                     cell.resizingType = "Right";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left, startValues.left, accuracy)) {
                                     cell.resizingType = "Left";
                                     cells.push(cell);
                                 }
                                 break;
                             }
                         case "Top":
                             {
                                 if (this.CompareNumbers(compStartValues.top, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "Bottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.top, startValues.top, accuracy)) {
                                     cell.resizingType = "Top";
                                     cells.push(cell);
                                 }
                                 break;
                             }
                         case "RightTop":
                             {
                                 if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left + startValues.width, accuracy) &&
                                     this.CompareNumbers(compStartValues.top, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "RightBottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left, accuracy) &&
                                     this.CompareNumbers(compStartValues.top, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "LeftBottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left, accuracy) &&
                                     this.CompareNumbers(compStartValues.top, startValues.top, accuracy)) {
                                     cell.resizingType = "LeftTop";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.top, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "Bottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.top, startValues.top, accuracy)) {
                                     cell.resizingType = "Top";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left + startValues.width, accuracy)) {
                                     cell.resizingType = "Right";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left, accuracy)) {
                                     cell.resizingType = "Left";
                                     cells.push(cell);
                                 }
                                 break;
                             }
                         case "Right":
                         case "ResizeWidth":
                             {
                                 if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left + startValues.width, accuracy)) {
                                     cell.resizingType = "Right";
                                     cells.push(cell);
                                 }
                                 if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left, accuracy)) {
                                     cell.resizingType = "Left";
                                     cells.push(cell);
                                 }
                                 break;
                             }
                         case "RightBottom":
                         case "ResizeDiagonal":
                             {
                                 if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left + startValues.width, accuracy) &&
                                     this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top, accuracy)) {
                                     cell.resizingType = "RightTop";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left, accuracy) &&
                                     this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top, accuracy)) {
                                     cell.resizingType = "LeftTop";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left, accuracy) &&
                                     this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "LeftBottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "Bottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top, accuracy)) {
                                     cell.resizingType = "Top";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left + startValues.width, accuracy)) {
                                     cell.resizingType = "Right";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left + compStartValues.width, startValues.left, accuracy)) {
                                     cell.resizingType = "Left";
                                     cells.push(cell);
                                 }
                                 break;
                             }
                         case "Bottom":
                         case "ResizeHeight":
                             {
                                 if (this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "Bottom";
                                     cells.push(cell);
                                 }
                                 if (this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top, accuracy)) {
                                     cell.resizingType = "Top";
                                     cells.push(cell);
                                 }
                                 break;
                             }
                         case "LeftBottom":
                             {
                                 if (this.CompareNumbers(compStartValues.left, startValues.left, accuracy) &&
                                     this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top, accuracy)) {
                                     cell.resizingType = "LeftTop";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left, startValues.left + startValues.width, accuracy) &&
                                     this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top, accuracy)) {
                                     cell.resizingType = "RightTop";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left, startValues.left + startValues.width, accuracy) &&
                                     this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "RightBottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top + startValues.height, accuracy)) {
                                     cell.resizingType = "Bottom";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.top + compStartValues.height, startValues.top, accuracy)) {
                                     cell.resizingType = "Top";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left, startValues.left + startValues.width, accuracy)) {
                                     cell.resizingType = "Right";
                                     cells.push(cell);
                                 }
                                 else if (this.CompareNumbers(compStartValues.left, startValues.left, accuracy)) {
                                     cell.resizingType = "Left";
                                     cells.push(cell);
                                 }
                                 break;
                             }
                         case "Left":
                             {
                                 if (this.CompareNumbers(compStartValues.left, startValues.left, accuracy)) {
                                     cell.resizingType = "Left";
                                     cells.push(cell);
                                 }
                                 if (this.CompareNumbers(compStartValues.left, startValues.left + startValues.width, accuracy)) {
                                     cell.resizingType = "Right";
                                     cells.push(cell);
                                 }
                                 break;
                             }
                     }
                 }
             }
         }
     }

     return cells;
 }

 StiMobileDesigner.prototype.GetRecentConnectionsFromCookies = function () {
     var connectionsStr = this.GetCookie("StimulsoftMobileDesignerRecentConnections");
     return (connectionsStr ? JSON.parse(connectionsStr) : []);
 }

 StiMobileDesigner.prototype.SaveRecentConnectionToCookies = function (connectionObject) {
     var recentConnections = this.GetRecentConnectionsFromCookies();
     if (recentConnections.length >= 8) recentConnections.splice(0, 1);
     recentConnections.push(connectionObject);
     this.SetCookie("StimulsoftMobileDesignerRecentConnections", JSON.stringify(recentConnections));
 }

 StiMobileDesigner.prototype.GetConnectionNameFromPathData = function (path) {
     if (path.toLowerCase().indexOf("http") == 0 && path.indexOf("?") > 0) {
         path = path.substring(0, path.indexOf("?"));
     }
     var connectionName = path.replace(/^.*[\\\/]/, '');
     if (connectionName.lastIndexOf(".") > 0) connectionName = connectionName.substring(0, connectionName.lastIndexOf("."));
     connectionName = connectionName.replace(/:/g, '').replace(/\\/g, '').replace(/\//g, '');

     return connectionName;
 }

 StiMobileDesigner.prototype.GetOpacityFromColor = function (colorStr) {
     var color = colorStr.split(",")
     if (color.length == 4) {
         return parseInt(color[0]) / 255;
     }

     return 1;
 }

 StiMobileDesigner.prototype.SaveFileToRecentArray = function (name, path, cookieKey) {
     var recentArray = this.GetCookie(cookieKey || "StimulsoftMobileDesignerRecentArray");
     recentArray = recentArray != null ? JSON.parse(recentArray) : [];
     var haveElement = false;

     for (var i = 0; i < recentArray.length; i++) {
         if (recentArray[i].name == name && recentArray[i].path == path) {
             var temp = recentArray[i];
             recentArray.splice(i, 1);
             recentArray.splice(0, 0, temp);
             haveElement = true;
             break;
         }
     }

     if (!haveElement) {
         recentArray.splice(0, 0, { name: name, path: path });
         if (recentArray.length > 10) {
             recentArray.pop();
         }
     }

     this.SetCookie(cookieKey || "StimulsoftMobileDesignerRecentArray", JSON.stringify(recentArray));
 }

 StiMobileDesigner.prototype.GetRecentArray = function (cookieKey) {
     var recentArray = this.GetCookie(cookieKey || "StimulsoftMobileDesignerRecentArray");
     return (recentArray != null ? JSON.parse(recentArray) : []);
 }
 
 StiMobileDesigner.prototype.formatDate = function (formatDate, formatString) {
     var yyyy = formatDate.getFullYear();
     var yy = yyyy.toString().substring(2);
     var m = formatDate.getMonth() + 1;
     var mm = m < 10 ? "0" + m : m;
     var d = formatDate.getDate();
     var dd = d < 10 ? "0" + d : d;

     var h = formatDate.getHours();
     var hh = h < 10 ? "0" + h : h;
     var n = formatDate.getMinutes();
     var nn = n < 10 ? "0" + n : n;
     var s = formatDate.getSeconds();
     var ss = s < 10 ? "0" + s : s;

     formatString = formatString.replace(/yyyy/i, yyyy);
     formatString = formatString.replace(/yy/i, yy);
     formatString = formatString.replace(/mm/i, mm);
     formatString = formatString.replace(/m/i, m);
     formatString = formatString.replace(/dd/i, dd);
     formatString = formatString.replace(/d/i, d);
     formatString = formatString.replace(/hh/i, hh);
     formatString = formatString.replace(/h/i, h);
     formatString = formatString.replace(/nn/i, nn);
     formatString = formatString.replace(/n/i, n);
     formatString = formatString.replace(/ss/i, ss);
     formatString = formatString.replace(/s/i, s);

     return formatString;
 }
 
 StiMobileDesigner.prototype.getT = function (withoutBrackets) {
     var t = String.fromCharCode(84) + "r" + String.fromCharCode(105) + "a" + String.fromCharCode(108);
     if (withoutBrackets) return t;
     return (String.fromCharCode(91) + t + String.fromCharCode(93));
 }

 StiMobileDesigner.prototype.openNewWindow = function (url) {
     var win = window.open(url || "about:blank");
     return win;
 }

 StiMobileDesigner.prototype.stringToTime = function (timeStr) {
     var timeArray = timeStr.split(":");
     var time = { hours: 0, minutes: 0, seconds: 0 };

     time.hours = this.StrToInt(timeArray[0]);
     if (timeArray.length > 1) time.minutes = this.StrToInt(timeArray[1]);
     if (timeArray.length > 2) time.seconds = this.StrToInt(timeArray[2]);

     if (time.hours < 0) time.hours = 0;
     if (time.minutes < 0) time.minutes = 0;
     if (time.seconds < 0) time.seconds = 0;

     if (time.hours > 23) time.hours = 23;
     if (time.minutes > 59) time.minutes = 59;
     if (time.seconds > 59) time.seconds = 59;

     return time;
 }

 StiMobileDesigner.prototype.SetWindowTitle = function (text) {
     if (!this.options.allowChangeWindowTitle) return;
     if (!this.options.jsMode && this.options.productVersion == this.options.productVersion.trim()) text += " " + this.getT();
     document.title = text;
     if (this.options.toolBar && this.options.toolBar.designerTitle) this.options.toolBar.designerTitle.innerHTML = text;
 }

 StiMobileDesigner.prototype.SetWindowIcon = function (doc, imageSrc) {
     var head = doc.head || doc.getElementsByTagName("head")[0];
     var link = doc.createElement("link"),
     oldLink = doc.getElementById("window-icon");
     link.id = "window-icon";
     link.rel = "icon";
     link.href = imageSrc;
     if (oldLink) {
         head.removeChild(oldLink);
     }
     head.appendChild(link);
 }

 StiMobileDesigner.prototype.AddDragAndDropToContainer = function (container, draggableSuccessFunc) {
     if (!this.options.canOpenFiles) return;
     
     var jsObject = this;
     container.draggable = true;

     var stopEvent = function (event) {
         event.stopPropagation();
         event.preventDefault();
     };

     var dropFiles = function (files) {
         var reader = new FileReader();

         reader.onload = function (event) {
             try {
                 draggableSuccessFunc(files, event.target.result);
             }
             catch (error) {
                 var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                 errorMessageForm.show(error.message);
             }
         };

         reader.onerror = function (event) {
             var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
             errorMessageForm.show(event.target.error.code);
         };

         if (files && files.length > 0) {
             reader.readAsDataURL(files[0]);
         }
     };

     container.draggable = false;

     container.addEventListener("dragover", function (event) {
         stopEvent(event);
     }, false);

     container.addEventListener("drop", function (event) {
         stopEvent(event);
         event.preventDefault && event.preventDefault();
         dropFiles(event.dataTransfer.files);
         return false;
     }, false);
 }

 StiMobileDesigner.prototype.GetHumanFileSize = function (size, short) {
     var i = Math.floor(Math.log(size) / Math.log(1024));
     return (short ? parseInt(size / Math.pow(1024, i)) : ((size / Math.pow(1024, i)).toFixed(2) * 1)) + ' ' + ['B', 'KB', 'MB', 'GB', 'TB'][i];
 };

 StiMobileDesigner.prototype.GetFileNameFromPath = function (path) {
     var fileName = path;
     if (path.lastIndexOf("/") >= 0) fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
     if (path.lastIndexOf("\\") >= 0) fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
     if (path.lastIndexOf(".") >= 0) fileName = fileName.substring(0, fileName.lastIndexOf("."));
     return fileName;
 };

 StiMobileDesigner.prototype.CheckImagesInDictionary = function () {
     if (this.options.report) {
         var dictionary = this.options.report.dictionary;
         for (var i = 0; i < dictionary.resources.length; i++) {
             if (dictionary.resources[i].type == "Image") return true;
         }
         var variables = this.GetCollectionFromCategoriesCollection(dictionary.variables);
         for (var i = 0; i < variables.length; i++) {
             if (variables[i].type == "image") return true;
         }
         var dataSources = this.GetDataSourcesFromDictionary(dictionary);
         for (var i = 0; i < dataSources.length; i++) {
             for (var k = 0; k < dataSources[i].columns.length; k++) {
                 if (dataSources[i].columns[k].type == "image" ||
                     dataSources[i].columns[k].type == "byte[]" ||
                     dataSources[i].columns[k].type == "string") return true;
             }
         }
     }

     return false;
 };

 StiMobileDesigner.prototype.CheckRichTextInDictionary = function () {
     if (this.options.report) {
         var dictionary = this.options.report.dictionary;
         for (var i = 0; i < dictionary.resources.length; i++) {
             if (dictionary.resources[i].type == "Rtf" || dictionary.resources[i].type == "Txt") return true;
         }
         var variables = this.GetCollectionFromCategoriesCollection(dictionary.variables);
         for (var i = 0; i < variables.length; i++) {
             if (variables[i].type == "string") return true;
         }
         var dataSources = this.GetDataSourcesFromDictionary(dictionary);
         for (var i = 0; i < dataSources.length; i++) {
             for (var k = 0; k < dataSources[i].columns.length; k++) {
                 if (dataSources[i].columns[k].type == "byte[]" ||
                     dataSources[i].columns[k].type == "string") return true;
             }
         }
     }

     return true;
 };

 StiMobileDesigner.prototype.ClearAllGalleries = function () {
     this.options.imagesGallery = null;
     this.options.richTextGallery = null;
 }

//Fixed bug in JS Designer after runing open dialog
 StiMobileDesigner.prototype.ReturnFocusToDesigner = function () {
     var tempTextBox = this.TextBox(null, 1);
     this.options.mainPanel.appendChild(tempTextBox);
     tempTextBox.focus();
     this.options.mainPanel.removeChild(tempTextBox);
 }

 StiMobileDesigner.prototype.AddResourceFile = function (file, content) {
     if (this.options.dictionaryPanel && this.options.dictionaryPanel.checkResourcesCount()) return;

     if (file.size > this.options.reportResourcesMaximumSize) {
         var errorMessageForm = this.options.forms.errorMessageForm || this.InitializeErrorMessageForm();
         errorMessageForm.show(this.loc.Notices.QuotaMaximumFileSizeExceeded + "<br>" +
             this.loc.PropertyMain.Maximum + ": " +
             this.numberWithSpaces(this.options.reportResourcesMaximumSize), "Warning");
         return;
     }

     var imageExts = ["gif", "png", "jpeg", "jpg", "bmp", "tiff", "ico", "emf", "wmf", "svg"];
     var reportExts = ["mrt", "mrz"];
     var reportSnapshotExts = ["mdc", "mdz"];
     var dataExts = ["xls", "xlsx", "csv", "dbf", "json", "xml"];

     var fileName = file.name || "Resource";
     var fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
     var resourceType;

     if (imageExts.indexOf(fileExt) >= 0) {
         resourceType = "Image";
     }
     else if (reportExts.indexOf(fileExt) >= 0) {
         resourceType = "Report";
     }
     else if (reportSnapshotExts.indexOf(fileExt) >= 0) {
         resourceType = "ReportSnapshot";
     }
     else if (fileExt == "ttf") {
         resourceType = "FontTtf";
     }
     else if (fileExt == "otf") {
         resourceType = "FontOtf";
     }
     else if (fileExt == "ttc") {
         resourceType = "FontTtc";
     }
     //else if (fileExt == "eot") {
     //    resourceType = "FontEot";
     //}
     //else if (fileExt == "woff") {
     //    resourceType = "FontWoff";
     //}
     else if (fileExt == "rtf") {
         resourceType = "Rtf";
     }
     else if (fileExt == "txt") {
         resourceType = "Txt";
     }
     else if (fileExt == "xml") {
         resourceType = "Xml";
     }
     else if (fileExt == "xsd") {
         resourceType = "Xsd";
     }
     else if (fileExt == "xls" || fileExt == "xlsx") {
         resourceType = "Excel";
     }
     else if (fileExt == "csv") {
         resourceType = "Csv";
     }
     else if (fileExt == "dbf") {
         resourceType = "Dbf";
     }
     else if (fileExt == "json") {
         resourceType = "Json";
     }
     else if (fileExt == "doc" || fileExt == "docx") {
         resourceType = "Word";
     }
     else if (fileExt == "pdf") {
         resourceType = "Pdf";
     }
     else {
         return;
     }

     var resourceName = this.GetNewName("Resource", null, fileName.substring(0, fileName.lastIndexOf(".")));

     var resource = {};
     resource.mode = "New";
     resource.name = resourceName;
     resource.alias = resourceName;
     resource.type = resourceType;
     resource.loadedContent = this.options.mvcMode ? encodeURIComponent(content) : content;
     resource.haveContent = true;

     var propertiesPanel = this.options.propertiesPanel;

     if (this.options.showPropertiesGrid &&
         this.options.showDictionary &&
         !propertiesPanel.styleDesignerMode &&
         !propertiesPanel.editChartMode &&
         !propertiesPanel.editCrossTabMode &&
         !propertiesPanel.editGaugeMode &&
         !propertiesPanel.editBarCodeMode)
     {
         this.options.propertiesPanel.showContainer("Dictionary");
     }

     if (dataExts.indexOf(fileExt) >= 0) {
         this.SendCommandCreateDatabaseFromResource(resource);
     }
     else {
         this.SendCommandCreateOrEditResource(resource);
     }
 }

 StiMobileDesigner.prototype.InsertSvgContent = function (svgContainer, svgContent) {
     var temp = document.createElement("div");
     temp.innerHTML = "<svg>" + svgContent + "</svg>";
     while (svgContainer.firstChild != null) svgContainer.removeChild(svgContainer.firstChild);
     var innerObjects = {};

     var writeElementXY = function (element) {
         innerObjects.g = element;
         var xyPosStr = element.getAttribute("transform");
         xyPosStr = xyPosStr.substring(xyPosStr.indexOf("translate(") + "translate(".length);
         xyPosStr = xyPosStr.substring(0, xyPosStr.indexOf(")"));
         var xyPos = xyPosStr.split(" ");
         if (xyPos.length == 1) xyPos = xyPosStr.split(",");
         innerObjects.g.setAttribute("x", xyPos[0]);
         innerObjects.g.setAttribute("y", xyPos.length > 1 ? xyPos[1] : 0);
     }

     Array.prototype.slice.call(temp.childNodes[0].childNodes).forEach(function (el) {
         svgContainer.appendChild(el);
         if (el.tagName == "image") innerObjects.image = el;
         if (el.tagName == "text") innerObjects.text = el;
         if (el.tagName == "rect") innerObjects.rect = el;
         if (el.tagName == "g" && el.getAttribute("transform") && el.getAttribute("transform").indexOf("translate") >= 0) {
             writeElementXY(el);
         }
     });
 }

StiMobileDesigner.prototype.GetCursorType = function (resizingType) {
    switch (resizingType) {
        case "LeftTop":
        case "RightBottom":
            return "nw-resize";
        case "RightTop":
        case "LeftBottom":
            return "sw-resize";
        case "Right":
        case "Left":
            return "e-resize";
        case "Top":
        case "Bottom":
            return "s-resize";
    }
}

StiMobileDesigner.prototype.PasteCurrentClipboardComponent = function () {
    if (this.options.clipboardMode && this.options.in_drag) {
        this.SendCommandChangeRectComponent(this.options.in_drag[0], "MoveComponent");
        this.options.clipboardMode = false;
        this.options.in_drag = false;
    }
}

StiMobileDesigner.prototype.getStyleObject = function (styleName) {
    var styleObject = {};
    if (!this.options.showPropertiesWhichUsedFromStyles && styleName && this.options.report && this.options.report.stylesCollection) {
        for (var i = 0; i < this.options.report.stylesCollection.length; i++) {
            if (this.options.report.stylesCollection[i].properties.name == styleName)
                return this.options.report.stylesCollection[i].properties;
        }
    }

    return styleObject;
}

StiMobileDesigner.prototype.getCloudName = function () {
    if (this.options.cloudParameters && this.options.cloudParameters.localizationName == "ru") {
        return "Stimulsoft " + this.loc.Cloud.Cloud;
    }

    return "Stimulsoft Cloud";
}

StiMobileDesigner.prototype.copyTextToClipboard = function (text) {
    var textArea = document.createElement("textarea");

    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    textArea.style.width = '2em';
    textArea.style.height = '2em';

    textArea.style.padding = 0;

    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    textArea.style.background = 'transparent';

    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();

    try {
        var successful = document.execCommand('copy');
    } catch (err) {
        console.log(err);
    }

    document.body.removeChild(textArea);
}

StiMobileDesigner.prototype.numberWithSpaces = function (x) {
    if (x == null) return "";
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

StiMobileDesigner.prototype.CreateSvgElement = function (name) {
    return ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', name) : document.createElement(name);
}

StiMobileDesigner.prototype.GetAllCrossTabsInReport = function () {
    var crossTabNames = [];
    var report = this.options.report;

    if (report) {
        for (var pageName in report.pages) {
            var page = report.pages[pageName];
            for (var componentName in page.components) {
                var component = page.components[componentName];
                if (component.typeComponent == "StiCrossTab") {
                    crossTabNames.push(component.properties.name);
                }
            }
        }
    }

    return crossTabNames;
}

StiMobileDesigner.prototype.GetReportFileName = function (withExt) {
    var fileName = withExt ? "Report.mrt" : "Report";

    if (this.options.cloudMode && this.options.cloudParameters && this.options.cloudParameters.reportTemplateItemKey) {
        fileName = this.options.cloudParameters.reportName;
        if (withExt) fileName += ".mrt";
    }
    else if (this.options.report.properties.reportFile) {
        fileName = this.options.report.properties.reportFile;
        if (!withExt && (fileName.toLowerCase().endsWith(".mrt") || fileName.toLowerCase().endsWith(".mrz") || fileName.toLowerCase().endsWith(".mrx"))) {
            fileName = fileName.substring(0, fileName.length - 4);
        }
    }
    if (this.options.report.encryptedPassword && withExt) {
        if (fileName.toLowerCase().endsWith(".mrt") || fileName.toLowerCase().endsWith(".mrz")) {
            fileName = fileName.substring(0, fileName.length - 4) + ".mrx";
        }
    }

    return fileName;
}

StiMobileDesigner.prototype.CheckRequestTimeout = function (data) {
    if (data && data.command) {
        if (data.command == "RunQueryScript" || data.command == "ViewData") {
            return data.commandTimeout ? this.StrToInt(data.commandTimeout) : null;
        }
        else if (data.command == "LoadReportToViewer" && this.options.report) {
            var requestTimeout = this.options.requestTimeout;
            var dataSources = this.GetDataSourcesFromDictionary(this.options.report.dictionary);
            for (var i = 0; i < dataSources.length; i++) {
                if (dataSources[i].commandTimeout && dataSources[i].commandTimeout > requestTimeout) {
                    requestTimeout = this.StrToInt(dataSources[i].commandTimeout);
                }
            }
            if (this.options.viewer) {
                this.options.viewer.jsObject.options.server.requestTimeout = requestTimeout;
            }

            return requestTimeout;
        }
    }

    return null;
}

StiMobileDesigner.prototype.CheckStylesForDashboardElement = function (typeComponent) {
    return (typeComponent == "StiChartElement" || typeComponent == "StiGaugeElement" ||
        typeComponent == "StiIndicatorElement" || typeComponent == "StiProgressElement" ||
        typeComponent == "StiMapElement" || typeComponent == "StiTableElement" ||
        typeComponent == "StiPivotElement");
}

StiMobileDesigner.prototype.DashboardElementHaveHeader = function (typeComponent) {
    return this.CheckStylesForDashboardElement(typeComponent);
}

StiMobileDesigner.prototype.CanDropDictionaryItem = function (typeComponent) {
    return this.CheckStylesForDashboardElement(typeComponent);
}

 StiMobileDesigner.prototype.ConstWebColors = [
            ["Transparent", "transparent"],
            ["Black", "0,0,0"],
            ["DimGray", "105,105,105"],
            ["Gray", "128,128,128"],
            ["DarkGray", "169,169,169"],
            ["Silver", "192,192,192"],
            ["LightGray", "211,211,211"],
            ["Gainsboro", "220,220,220"],
            ["WhiteSmoke", "245,245,245"],
            ["White", "255,255,255"],
            ["RosyBrown", "188,143,143"],
            ["IndianRed", "205,92,92"],
            ["Brown", "165,42,42"],
            ["Firebrick", "178,34,34"],
            ["LightCoral", "240,128,12"],
            ["Maroon", "128,0,0"],
            ["DarkRed", "139,0,0"],
            ["Red", "255,0,0"],
            ["Snow", "255,250,250"],
            ["MistyRose", "255,228,225"],
            ["Salmon", "250,128,114"],
            ["Tomato", "255,99,71"],
            ["DarkSalmon", "233,150,122"],
            ["Coral", "255,127,80"],
            ["OrangeRed", "255,69,0"],
            ["LightSalmon", "255,160,122"],
            ["Sienna", "160,82,45"],
            ["SeaShell", "255,245,23"],
            ["Chocolate", "210,105,30"],
            ["SaddleBrown", "139,69,19"],
            ["SandyBrown", "244,164,96"],
            ["PeachPuff", "255,218,185"],
            ["Peru", "205,133,63"],
            ["Linen", "250,240,230"],
            ["Bisque", "255,228,196"],
            ["DarkOrange", "255,140,0"],
            ["BurlyWood", "222,184,135"],
            ["Tan", "210,180,140"],
            ["AntiqueWhite", "250,235,215"],
            ["NavajoWhite", "255,222,173"],
            ["BlanchedAlmond", "255,235,205"],
            ["PapayaWhip", "255,239,213"],
            ["Moccasin", "255,228,181"],
            ["Orange", "255,165,0"],
            ["Wheat", "245,222,179"],
            ["OldLace", "253,245,230"],
            ["FloralWhite", "255,250,240"],
            ["DarkGoldenrod", "184,134,11"],
            ["Goldenrod", "218,165,32"],
            ["Cornsilk", "255,248,220"],
            ["Gold", "255,215,0"],
            ["Khaki", "240,230,140"],
            ["LemonChiffon", "255,250,205"],
            ["PaleGoldenrod", "238,232,170"],
            ["DarkKhaki", "189,183,107"],
            ["Beige", "245,245,220"],
            ["LightGoldenrodYellow", "250,250,210"],
            ["Olive", "128,128,0"],
            ["Yellow", "255,255,0"],
            ["LightYellow", "255,255,224"],
            ["Ivory", "255,255,240"],
            ["OliveDrab", "107,142,35"],
            ["YellowGreen", "154,205,50"],
            ["DarkOliveGreen", "85,107,47"],
            ["GreenYellow", "173,255,47"],
            ["Chartreuse", "127,255,0"],
            ["LawnGreen", "124,252,0"],
            ["DarkSeaGreen", "143,188,139"],
            ["ForestGreen", "34,139,34"],
            ["LimeGreen", "50,205,50"],
            ["LightGreen", "144,238,144"],
            ["PaleGreen", "152,251,152"],
            ["DarkGreen", "0,100,0"],
            ["Green", "0,128,0"],
            ["Lime", "0,255,0"],
            ["Honeydew", "240,255,240"],
            ["SeaGreen", "46,139,87"],
            ["MediumSeaGreen", "60,179,113"],
            ["SpringGreen", "0,255,127"],
            ["MintCream", "245,255,250"],
            ["MediumSpringGreen", "0,250,154"],
            ["MediumAquamarine", "102,205,170"],
            ["Aquamarine", "127,255,212"],
            ["Turquoise", "64,224,20"],
            ["LightSeaGreen", "32,178,170"],
            ["MediumTurquoise", "72,209,204"],
            ["DarkSlateGray", "47,79,79"],
            ["PaleTurquoise", "175,238,23"],
            ["Teal", "0,128,12"],
            ["DarkCyan", "0,139,139"],
            ["Aqua", "0,255,255"],
            ["Cyan", "0,255,255"],
            ["LightCyan", "224,255,255"],
            ["Azure", "240,255,255"],
            ["DarkTurquoise", "0,206,209"],
            ["CadetBlue", "95,158,160"],
            ["PowderBlue", "176,224,230"],
            ["LightBlue", "173,216,230"],
            ["DeepSkyBlue", "0,191,255"],
            ["SkyBlue", "135,206,235"],
            ["LightSkyBlue", "135,206,250"],
            ["SteelBlue", "70,130,180"],
            ["AliceBlue", "240,248,255"],
            ["DodgerBlue", "30,144,255"],
            ["SlateGray", "112,128,144"],
            ["LightSlateGray", "119,136,153"],
            ["LightSteelBlue", "176,196,222"],
            ["CornflowerBlue", "100,149,237"],
            ["RoyalBlue", "65,105,225"],
            ["MidnightBlue", "25,25,112"],
            ["Lavender", "230,230,250"],
            ["Navy", "0,0,12"],
            ["DarkBlue", "0,0,139"],
            ["MediumBlue", "0,0,205"],
            ["Blue", "0,0,255"],
            ["GhostWhite", "248,248,255"],
            ["SlateBlue", "106,90,205"],
            ["DarkSlateBlue", "72,61,139"],
            ["MediumSlateBlue", "123,104,23"],
            ["MediumPurple", "147,112,219"],
            ["BlueViolet", "138,43,226"],
            ["Indigo", "75,0,130"],
            ["DarkOrchid", "153,50,204"],
            ["DarkViolet", "148,0,211"],
            ["MediumOrchid", "186,85,211"],
            ["Thistle", "216,191,216"],
            ["Plum", "221,160,221"],
            ["Violet", "238,130,23"],
            ["Purple", "128,0,12"],
            ["DarkMagenta", "139,0,139"],
            ["Magenta", "255,0,255"],
            ["Fuchsia", "255,0,255"],
            ["Orchid", "218,112,214"],
            ["MediumVioletRed", "199,21,133"],
            ["DeepPink", "255,20,147"],
            ["HotPink", "255,105,180"],
            ["LavenderBlush", "255,240,245"],
            ["PaleVioletRed", "219,112,147"],
            ["Crimson", "220,20,60"],
            ["Pink", "255,192,203"],
            ["LightPink", "255,182,193"]
];