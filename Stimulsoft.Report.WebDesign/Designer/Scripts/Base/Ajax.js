
StiMobileDesigner.prototype.ShowMainLoadProcess = function (command) {
    var commandsWithMainLoadProcess = "CreateReport;OpenReport;SaveReport;CloseReport;WizardResult;GetPreviewPages;" +
        "GetConnectionTypes;CreateOrEditConnection;DeleteConnection;CreateOrEditRelation;DeleteRelation;CreateOrEditColumn;CreateOrEditRelation;" +
        "DeleteRelation;CreateOrEditColumn;DeleteColumn;CreateOrEditDataSource;DeleteDataSource;GetAllConnections;RetrieveColumns;SynchronizeDictionary;" +
        "CreateOrEditBusinessObject;DeleteBusinessObject;UpdateStyles;AddStyle;GetReportFromData;ItemResourceSave;StartEditChartComponent;AddSeries;RemoveSeries;" +
        "SeriesMove;SetLabelsType;AddConstantLineOrStrip;RemoveConstantLineOrStrip;ConstantLineOrStripMove;SendContainerValue;CreateTextComponent;CreateDataComponent;" +
        "RunQueryScript;ViewData;ApplyDesignerOptions;StartEditCrossTabComponent;UpdateCrossTabComponent;Undo;Redo;GetReportForDesigner;" +
        "OpenStyle;SaveStyle;GetGlobalizationStrings;SetCultureSettingsToReport;GetCultureSettingsFromReport;StartEditGaugeComponent;UpdateGaugeComponent;CreateOrEditResource;" +
        "DeleteResource;CreateDatabaseFromResource;LoadReportFromCloud;MoveDictionaryItem;GetVariableItemsFromDataColumn;TestConnection;SendCommandGetDatabaseData;" +
        "ApplySelectedData;MoveConnectionDataToResource;StartEditChartComponent;CreateTableElement;CreateTextElement;UpdateTableElement;UpdatePivotElement;UpdateChartElement" +
        "UpdateGaugeElement;UpdateTextElement;UpdateMapElement;UpdateShapeElement;UpdateProgressElement;UpdateIndicatorElement;UpdateImageElement";

    if (commandsWithMainLoadProcess.indexOf(command) != -1) return true;

    return false;
}

StiMobileDesigner.prototype.CreateXMLHttp = function () {
    if (typeof XMLHttpRequest != "undefined") return new XMLHttpRequest();
    else if (window.ActiveXObject) {
        var allVersions = [
            "MSXML2.XMLHttp.5.0",
            "MSXML2.XMLHttp.4.0",
            "MSXML2.XMLHttp.3.0",
            "MSXML2.XMLHttp",
            "Microsoft.XMLHttp"
        ];
        for (var i = 0; i < allVersions.length; i++) {
            try {
                var xmlHttp = new ActiveXObject(allVersions[i]);
                return xmlHttp;
            }
            catch (oError) {
            }
        }
    }
    throw new Error("Unable to create XMLHttp object.");
}

StiMobileDesigner.prototype.CreatePostParameters = function (data, asObject) {
    var params = {
        "designerId": this.options.mobileDesignerId,
        "reportGuid": this.options.reportGuid,
        "clientGuid": this.options.clientGuid,
        "cloudMode": this.options.cloudMode,
        "cacheMode": this.options.cacheMode,
        "cacheTimeout": this.options.cacheTimeout,
        "cacheItemPriority": this.options.cacheItemPriority,
        "undoMaxLevel": this.options.undoMaxLevel,
        "useRelativeUrls": this.options.useRelativeUrls,
        "passQueryParametersForResources": this.options.passQueryParametersForResources,
        "routes": this.options.routes,
        "formValues": this.options.formValues,
        "version": this.options.shortProductVersion,
        "useAliases": this.options.useAliases,
        "checkReportBeforePreview": this.options.checkReportBeforePreview
    };

    if (this.options.cloudMode && this.options.productVersion != this.options.productVersion.trim()) {
        params.cloudPlan = "Developers";
    }

    if (this.options.report) {
        params.reportFile = this.options.report.properties.reportFile;
    }

    // Add data object fields to params
    if (data) {
        for (var key in data) {
            params[key] = data[key];
        }
    }   

    // Object params
    var postParams = {
        stiweb_component: "Designer",
        stiweb_action: "RunCommand"
    };
    if (params.action) {
        postParams["stiweb_action"] = params.action;
        delete params.action;
    }
    if (params.base64Data) {
        postParams["stiweb_data"] = params.base64Data;
        delete params.base64Data;
    }

    // Params
    var jsonParams = JSON.stringify(params);
    if (this.options.useCompression) postParams["stiweb_packed_parameters"] = StiGZipHelper.pack(jsonParams);
    else postParams["stiweb_parameters"] = Base64.encode(jsonParams);
    if (asObject) return postParams;
    
    // URL params
    var urlParams = "stiweb_component=" + postParams["stiweb_component"] + "&";
    if (postParams["stiweb_action"]) urlParams += "stiweb_action=" + postParams["stiweb_action"] + "&";
    if (postParams["stiweb_data"]) urlParams += "stiweb_data=" + encodeURIComponent(postParams["stiweb_data"]) + "&";
    if (postParams["stiweb_parameters"]) urlParams += "stiweb_parameters=" + encodeURIComponent(postParams["stiweb_parameters"]);
    else urlParams += "stiweb_packed_parameters=" + encodeURIComponent(postParams["stiweb_packed_parameters"]);
    return urlParams;
}

StiMobileDesigner.prototype.GetMvcActionUrl = function (url, data) {
    switch (data.command) {
        case "GetReportForDesigner": return url.replace("{action}", this.options.actions.getReport || this.options.actions.designerEvent);
        case "OpenReport": return url.replace("{action}", this.options.actions.openReport || this.options.actions.designerEvent);
        case "CreateReport":
        case "WizardResult": return url.replace("{action}", this.options.actions.createReport || this.options.actions.designerEvent);
        case "SaveReport": return url.replace("{action}", this.options.actions.saveReport || this.options.actions.designerEvent);
        case "SaveAsReport": return url.replace("{action}", this.options.actions.saveReportAs || this.options.actions.designerEvent);
        case "LoadReportToViewer": return url.replace("{action}", this.options.actions.previewReport || this.options.actions.designerEvent);
        case "ExitDesigner": return url.replace("{action}", this.options.actions.exit || this.options.actions.designerEvent);
        case "SetLocalization": return url.replace("{action}", this.options.routes["action"]);
    }
    return url.replace("{action}", this.options.actions.designerEvent);
}

StiMobileDesigner.prototype.PostAjax = function (url, data, callback) {
    var jsObject = this;
    var xmlHttp = this.CreateXMLHttp();

    if (this.options.actions) url = this.GetMvcActionUrl(url, data);

    if (jsObject.options.requestTimeout != 0) {
        var requestTimeout = this.CheckRequestTimeout(data);

        setTimeout(function () {
            if (xmlHttp.readyState < 4) xmlHttp.abort();
        }, (requestTimeout || jsObject.options.requestTimeout) * 1000);
    }

    xmlHttp.open("POST", url);
    if (this.options.requestHeaderContentType && this.options.requestHeaderContentType != ""){
    	xmlHttp.setRequestHeader("Content-Type", this.options.requestHeaderContentType);
    }else {
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    } 
    xmlHttp.responseType = "text";
    if (data && data.responseType) xmlHttp.responseType = data.responseType;
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4) {
            var status = 0;
            try {
                status = xmlHttp.status;
            }
            catch (e) {
            }

            if (status == 0) {
                callback("ServerError:Timeout response from the server.", jsObject);
            } else if (status == 200) {
                callback(xmlHttp.response ? xmlHttp.response : xmlHttp.responseText, jsObject);
            } else {
                if (jsObject.options.showServerErrorPage && xmlHttp.response) jsObject.controls.reportPanel.innerHTML = xmlHttp.response;
                callback("ServerError:" + status + " - " + xmlHttp.statusText, jsObject);
            }
        }
    };

    var params = this.CreatePostParameters(data, false);
    xmlHttp.send(params);
}

StiMobileDesigner.prototype.PostForm = function (data, doc, url, postOnlyData) {
    if (!doc) doc = document;
    url = url || this.options.requestUrl;
    if (this.options.actions) url = this.GetMvcActionUrl(url, data);

    var form = doc.createElement("FORM");
    form.setAttribute("method", "POST");
    form.setAttribute("action", url);
    
    var params = postOnlyData ? data : this.CreatePostParameters(data, true);
    for (var key in params) {
        var hiddenField = doc.createElement("INPUT");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", params[key]);
        form.appendChild(hiddenField);
    }

    doc.body.appendChild(form);
    form.submit();
    doc.body.removeChild(form);
}

StiMobileDesigner.prototype.AddCommandToStack = function (params) {
    this.options.commands.push(params);
    if (this.options.commands.length == 1) this.ExecuteCommandFromStack();
}

StiMobileDesigner.prototype.ExecuteCommandFromStack = function() {
    var params = this.options.commands[0];

    if (this.ShowMainLoadProcess(params.command)) {
        var processImage = this.options.processImage || this.InitializeProcessImage();
        processImage.show();
    }
    else {
        if (this.options.processImageStatusPanel) this.options.processImageStatusPanel.show();
    }

    this.PostAjax(this.options.requestUrl, params, this.receveFromServer);

    clearTimeout(this.options.timerAjax);

    var jsObject = this;
    this.options.timerAjax = setTimeout(function() {
        jsObject.Synchronization();
    }, params.command != "Synchronization" ? 120000 : 15000);
}

StiMobileDesigner.prototype.Synchronization = function () {
    this.options.commands = [];
    this.SendCommandSynchronization();
}

//Fixed bugs with some symblos
StiMobileDesigner.prototype.EncodeSymbols = function (str) {
    str = str.replace(/'/g, '\\\''); // (')
    str = str.replace(/\\\"/g, '&ldquo;'); // (\")
    return str;
}

//Error
StiMobileDesigner.prototype.errorFromServer = function(args, jsObject) {
}

//Receive
StiMobileDesigner.prototype.receveFromServer = function (args, jsObject) {
    if (!jsObject && this.options) jsObject = this;
    var answer = { command: null };
    try {
        if (jsObject.options.useCompression) args = StiGZipHelper.unpack(args);
        answer = JSON.parse(args);
    }
    catch (e) {
        var errorMessage = "An error occurred while parsing the response from the server.";
        if (typeof args != "string" || args == "" || args == "ServerError:") errorMessage = "An unknown error occurred (the server returned an empty value).";
        else if (args.substr(0, 12) == "ServerError:") errorMessage = args.substr(12);

        answer = { error: errorMessage };
    }
    
    clearTimeout(jsObject.options.timerAjax);

    if (answer.error || answer.infoMessage) {
        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
        var text = answer.error || answer.infoMessage;

        errorMessageForm.show(text, answer.infoMessage != null);
        if (jsObject.options.processImage) jsObject.options.processImage.hide();
        if (jsObject.options.viewer) (jsObject.options.viewer.jsObject.controls || jsObject.options.viewer.jsObject.options).processImage.hide();
        
        if (answer.checkItems && answer.checkItems.length > 0) {
            jsObject.InitializeCheckPopupPanel(answer.checkItems, function (checkPopupPanel) { });
        }
    }
    else {
        switch (answer.command) {
            case "SessionCompleted":
                {
                    var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                    errorMessageForm.show("Session on the server is complete!");
                    break;
                }
            case "SynchronizationError":
                {
                    var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                    errorMessageForm.onAction = function () {
                        jsObject.options.ignoreBeforeUnload = true;
                        location.reload(true);
                    }
                    errorMessageForm.show("The website is temporarily unavailable. Updating is in progress...");
                    break;
                }
            case "Synchronization":
                {
                    jsObject.LoadReport(jsObject.ParseReport(answer.reportObject));
                    break;
                }
            case "CreateReport":
                {                    
                    if (answer.needClearAfterOldReport) jsObject.CloseReport();
                    if (jsObject.options.cloudParameters) jsObject.options.cloudParameters.reportTemplateItemKey = null;
                    jsObject.options.reportGuid = answer.reportGuid;
                    jsObject.LoadReport(jsObject.ParseReport(answer.reportObject));
                    if (jsObject.options.callbackFunctions[answer["callbackFunctionId"]]) {
                        jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer);
                        delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    }
                    break;
                }
            case "GetReportFromData":
            case "OpenReport":
                {
                    jsObject.CloseReport();
                    if (answer["errorMessage"] != null) {
                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        errorMessageForm.show(answer["errorMessage"]);
                    }
                    else {
                        if (answer.reportGuid == null && answer.reportObject == null) {
                            var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                            errorMessageForm.show(jsObject.loc.Errors.Error);
                        }
                        else {
                            if (answer.command == "OpenReport" && jsObject.options.cloudParameters) {
                                jsObject.options.cloudParameters.reportTemplateItemKey = null;
                            }
                            jsObject.options.reportGuid = answer.reportGuid;
                            var reportObject = jsObject.ParseReport(answer.reportObject);
                            reportObject.encryptedPassword = answer.encryptedPassword;
                            jsObject.LoadReport(reportObject);
                        }                        
                    }
                    break;
                }
            case "ItemResourceSave":
                {   
                    if (answer["openSaveAsDialog"]) {
                        var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
                        fileMenu.changeVisibleState(true);
                        jsObject.ExecuteAction("saveAsReport");
                    }
                    else if (answer["errorMessage"] != null) {
                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        errorMessageForm.show(answer["errorMessage"]);
                    }
                    else {
                        jsObject.options.reportIsModified = false;
                        if (jsObject.options.forms.shareForm && jsObject.options.forms.shareForm.visible) {
                            jsObject.options.forms.shareForm.fillShareInfo();
                        };
                    }
                    break;
                }
            case "CloneItemResourceSave":
                {
                    if (answer["errorMessage"] != null) {
                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        errorMessageForm.show(answer["errorMessage"]);
                    } else {
                        if (answer["resultItemKey"]) {
                            var navigatorUrl = jsObject.options.cloudParameters.navigatorUrl;
                            var viewerParameters = jsObject.CopyObject(jsObject.options.cloudParameters);
                            viewerParameters["reportName"] = Base64.encode(answer["reportName"]);
                            viewerParameters["reportTemplateItemKey"] = answer["resultItemKey"];
                            if (viewerParameters["attachedItems"]) delete viewerParameters["attachedItems"];
                            delete viewerParameters["navigatorUrl"];
                            var viewerHref = navigatorUrl.replace("main.aspx", "viewer");

                            if (!jsObject.options.viewerContainer) jsObject.InitializePreviewPanel();
                            jsObject.options.viewerContainer.addFrame();
                            jsObject.options.viewerContainer.changeVisibleState(true);

                            var previewFrame = jsObject.options.viewerContainer.frame;
                            previewFrame.src = "about:blank";
                            var loadFlag = false;

                            previewFrame.onload = function () {
                                if (!loadFlag) {
                                    if (jsObject.options.buttons.previewToolButton) jsObject.options.buttons.previewToolButton.progress.style.visibility = "hidden";
                                    loadFlag = true;
                                    var doc = this.contentDocument || this.document;
                                    jsObject.options.ignoreBeforeUnload = true;
                                    jsObject.PostForm(viewerParameters, doc, viewerHref);
                                    setTimeout(function () { jsObject.options.ignoreBeforeUnload = false; }, 500);
                                }
                            }
                        }
                    }
                    break;
                }
            case "CloseReport":
                {
                    jsObject.CloseReport();
                    break;
                }
            case "SaveReport":
            case "SaveAsReport":
                {
                    if (answer.infoMessage || answer.errorMessage) {
                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        errorMessageForm.show(answer.infoMessage || answer.errorMessage, answer.infoMessage);
                    }
                    break;
                }
            case "MoveComponent":
            case "ResizeComponent":
                {
                    var componentsNames = [];
                    var firstComponent;
                    for (var i = 0; i < answer.components.length; i++) {
                        var component = jsObject.options.report.pages[answer.pageName].components[answer.components[i].componentName];
                        if (i == 0) firstComponent = component;
                        componentsNames.push(answer.components[i].componentName);
                        if (answer.components[i].svgContent != null) component.properties.svgContent = answer.components[i].svgContent;
                        component.repaint();                        
                        if (component.typeComponent == "StiSubReport" && jsObject.options.report.pages[component.properties.subReportPage])
                            jsObject.SendCommandSendProperties(jsObject.options.report.pages[component.properties.subReportPage], []);
                    }
                    jsObject.CheckLargeHeight(jsObject.options.report.pages[answer.pageName], answer.largeHeightAutoFactor);
                    jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                    jsObject.PaintSelectedLines();                    
                    if (answer.components.length == 1 || (answer.command == "ResizeComponent" && !answer.isMultiResize)) {
                        if (firstComponent.typeComponent == "StiTable" && answer.cells) {
                            jsObject.RebuildTable(firstComponent, answer.cells);
                        }

                        firstComponent.setSelected();
                        firstComponent.setOnTopLevel();
                    }
                    else {
                        jsObject.SetSelectedObjectsByNames(jsObject.options.report.pages[answer.pageName], componentsNames);
                    }
                    jsObject.UpdatePropertiesControls();
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "CreateComponent":
                {              
                    var component = answer.properties.isDashboardElement
                        ? jsObject.CreateDashboardElement(answer)
                        : jsObject.CreateComponent(answer);

                    if (component) {
                        component.repaint();
                        jsObject.CheckLargeHeight(jsObject.options.report.pages[component.properties.pageName], answer.largeHeightAutoFactor);
                        jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                        jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;
                        jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                        component.setOnTopLevel();
                        component.setSelected();

                        if (answer.tableCells) {
                            for (var i = 0; i < answer.tableCells.length; i++) {
                                var cell = jsObject.CreateComponent(answer.tableCells[i]);
                                if (cell) {
                                    cell.repaint();
                                    jsObject.options.report.pages[cell.properties.pageName].appendChild(cell);
                                    jsObject.options.report.pages[cell.properties.pageName].components[cell.properties.name] = cell;                                    
                                }
                            }
                        }

                        if (answer.newSubReportPage) jsObject.AddPage(answer.newSubReportPage, true);
                        jsObject.UpdatePropertiesControls();
                        if (jsObject.options.report.info.runDesignerAfterInsert) jsObject.ShowComponentForm(component);
                        jsObject.UpdateStateUndoRedoButtons();
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    }                    
                    break;
                }
            case "RemoveComponent":
                {
                    if (answer.rebuildProps && answer.pageName) {
                        jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.CheckLargeHeight(jsObject.options.report.pages[answer.pageName], answer.largeHeightAutoFactor);
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    }
                    break;
                }
            case "AddPage":
                {
                    jsObject.AddPage(answer);
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "OpenPage":
                {
                    var newPage = jsObject.AddPage(answer.pageProps);
                    jsObject.UpdateStateUndoRedoButtons();

                    var components = answer.pageProps.components;

                    for (var numComponent = 0; numComponent < components.length; numComponent++) {
                        if (ComponentCollection[components[numComponent].typeComponent]) {
                            var component = jsObject.CreateComponent(components[numComponent]);
                            if (component) {
                                component.repaint();
                                jsObject.options.report.pages[newPage.properties.name].components[component.properties.name] = component;
                            }
                        }
                    }

                    newPage.addComponents();

                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "AddDashboard":
                {
                    jsObject.AddDashboard(answer);
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "RemovePage":
                {
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "SendProperties":
                {
                    var checkLargeHeight = false;
                    var components = answer.components;
                    var lastComponent;

                    for (var i = 0; i < components.length; i++) {
                        var object = components[i].typeComponent == "StiPage"
                        ? jsObject.options.report.pages[components[i].componentName]
                        : jsObject.options.report.pages[answer.pageName].components[components[i].componentName];
                        if (!object) continue;

                        jsObject.WriteAllProperties(object, components[i].properties);

                        if (object.typeComponent == "StiPage") {
                            object.repaint(true);
                            object.rebuild(answer.rebuildProps);
                            object.repaintAllComponents();
                        }
                        else {
                            object.properties.svgContent = components[i].svgContent;
                            object.repaint();
                            checkLargeHeight = true;
                            lastComponent = object;
                        }
                    }
                    if (lastComponent && jsObject.options.updateLastStyleProperties) {
                        jsObject.SaveLastStyleProperties(lastComponent);
                    }

                    if (checkLargeHeight && answer.pageName && answer.largeHeightAutoFactor) {
                        jsObject.CheckLargeHeight(jsObject.options.report.pages[answer.pageName], answer.largeHeightAutoFactor);
                    }
                    if (answer.rebuildProps) {
                        jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                        var selectedObjects = jsObject.options.selectedObjects || [jsObject.options.selectedObject];
                        if (selectedObjects) {
                            for (var i = 0; i < selectedObjects.length; i++) {
                                if (selectedObjects[i].typeComponent != "StiPage" && selectedObjects[i].typeComponent != "StiReport") selectedObjects[i].setOnTopLevel();
                            }
                        }
                    }

                    jsObject.UpdatePropertiesControls();
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.options.updateLastStyleProperties = false;
                    break;
                }
            case "ChangeUnit":
                {
                    jsObject.options.report.properties.reportUnit = answer.reportUnit;
                    jsObject.options.buttons.unitButton.updateCaption(answer.reportUnit);
                    jsObject.options.report.gridSize = jsObject.ConvertUnitToPixel(jsObject.StrToDouble(answer.gridSize));
                    jsObject.ConvertAllComponentsToCurrentUnit(answer.pagePositions, answer.compPositions);
                    if (jsObject.options.selectedObject) jsObject.options.selectedObject.setSelected();
                    jsObject.UpdatePropertiesControls();
                    jsObject.UpdateStateUndoRedoButtons();
                    break;
                }
            case "RebuildPage":
                {
                    jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                    break;
                }
            case "LoadReportToViewer":
                {
                    var params = {};
                    params.pageNumber = 0;
                    params.zoom = (jsObject.options.viewer.jsObject.reportParams || jsObject.options.viewer.jsObject.options).zoom || 100;
                    params.viewmode = (jsObject.options.viewer.jsObject.reportParams ? jsObject.options.viewer.jsObject.reportParams.viewMode : jsObject.options.viewer.jsObject.options.menuViewMode) || "OnePage";
                    
                    if (typeof jsObject.options.viewer.jsObject.postAction != 'undefined')
                        jsObject.options.viewer.jsObject.postAction();
                    else
                        jsObject.options.viewer.jsObject.sendToServer("LoadReportFromCache", params);

                    if (answer.checkItems && answer.checkItems.length > 0)
                        jsObject.InitializeCheckPopupPanel(answer.checkItems, function (checkPopupPanel) { });
                    break;
                }
            case "SetToClipboard":
                {
                    jsObject.options.buttons.pasteComponent.setEnabled(true);
                    break;
                }
            case "GetFromClipboard":
                {
                    var components = answer["components"];
                    var countComponents = jsObject.GetCountObjects(components);
                    if (countComponents > 0) {
                        jsObject.options.clipboardMode = true;
                        for (var i = 0; i < countComponents; i++) {
                            var component = jsObject.CreateComponent(components[i]);
                            if (component) {
                                component.repaint();
                                jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                                jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;                                
                                if (!jsObject.isTouchDevice) {
                                    if (i == 0) {
                                        jsObject.options.in_drag = [[], [], [], [], true];
                                        var pagePositions = jsObject.FindPagePositions();
                                        jsObject.options.startMousePos = [
                                            pagePositions.posX + parseInt(component.getAttribute("left")) - 3,
                                            pagePositions.posY + parseInt(component.getAttribute("top")) - 3
                                        ];
                                    }
                                    jsObject.options.in_drag[0].push(component);
                                    jsObject.options.in_drag[1].push(parseInt(component.getAttribute("left")));
                                    jsObject.options.in_drag[2].push(parseInt(component.getAttribute("top")));
                                    jsObject.options.in_drag[3].push(component.getAllChildsComponents());
                                }
                            }
                        }
                        if (jsObject.isTouchDevice && answer.rebuildProps)
                            jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                        jsObject.UpdateStateUndoRedoButtons();
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                        if (!jsObject.options.mouseOverPage) {
                            jsObject.PasteCurrentClipboardComponent();
                        }
                    }
                    break;
                }
            case "Undo":
                {
                    if (answer["reportGuid"] && answer["reportObject"]) {
                        jsObject.options.report = null;
                        jsObject.options.reportGuid = answer.reportGuid;
                        jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                        jsObject.options.reportIsModified = true;
                        jsObject.options.buttons.undoButton.setEnabled(answer.enabledUndoButton);
                        jsObject.options.buttons.redoButton.setEnabled(true);
                    }
                    if (answer.selectedObjectName)
                        jsObject.BackToSelectedComponent(answer.selectedObjectName);
                    break;
                }
            case "Redo":
                {
                    if (answer["reportGuid"] && answer["reportObject"]) {
                        jsObject.options.report = null;
                        jsObject.options.reportGuid = answer.reportGuid;
                        jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                        jsObject.options.reportIsModified = true;
                    }
                    jsObject.options.buttons.redoButton.setEnabled(answer.enabledRedoButton);
                    jsObject.options.buttons.undoButton.setEnabled(true);
                    if (answer.selectedObjectName)
                        jsObject.BackToSelectedComponent(answer.selectedObjectName);
                    break;
                }
            case "RenameComponent":
                {
                    if (answer.newName == answer.oldName) jsObject.UpdatePropertiesControls();
                    else {
                        if (answer.typeComponent == "StiPage") {
                            var page = jsObject.options.report.pages[answer.oldName];
                            if (page) page.rename(answer.newName);
                        }
                        else {
                            var component = jsObject.FindComponentByName(answer.oldName);
                            if (component) component.rename(answer.newName);
                            jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                        }
                    }
                    jsObject.options.statusPanel.componentNameCell.innerHTML = answer.newName;
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "WizardResult":
                {
                    jsObject.options.reportGuid = answer.reportGuid;
                    jsObject.LoadReport(jsObject.ParseReport(answer.reportObject));
                    jsObject.options.reportIsModified = true;
                    if (jsObject.options.cloudParameters){
                        if (!jsObject.options.cloudParameters.thenOpenWizard) {
                            jsObject.options.cloudParameters.reportTemplateItemKey = null;
                        }
                        jsObject.options.cloudParameters.thenOpenWizard = false;
                    }
                    break;
                }
            case "GetConnectionTypes":
                {
                    jsObject.options.forms.selectConnectionForm.fillConnections(answer.connections);
                    break;
                }
            case "NewDictionary":
            case "SynchronizeDictionary":
                {
                    var attachedItems = jsObject.options.report.dictionary.attachedItems;
                    answer.dictionary.attachedItems = attachedItems;
                    jsObject.options.report.dictionary = answer.dictionary;
                    jsObject.options.dictionaryTree.build(answer.dictionary, true);
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "CreateOrEditConnection":
                {
                    jsObject.options.report.dictionary.databases = answer.databases;
                    if (answer.itemObject) jsObject.options.dictionaryTree.createOrEditConnection(answer);
                    jsObject.options.dictionaryPanel.toolBar.updateControls();
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    if (answer.mode == "New") {
                        var processImage = jsObject.options.processImage || jsObject.InitializeProcessImage();
                        processImage.hide();

                        if (answer.skipSchemaWizard) {
                            jsObject.InitializeEditDataSourceForm(function (editDataSourceForm) {
                                editDataSourceForm.datasource = jsObject.GetDataAdapterTypeFromDatabaseType(answer.itemObject.typeConnection);
                                editDataSourceForm.nameInSource = answer.itemObject.name;
                                editDataSourceForm.changeVisibleState(true);
                            });
                        }
                        else {
                            jsObject.InitializeSelectDataForm(function (selectDataForm) {
                                selectDataForm.databaseName = answer.itemObject.name;
                                selectDataForm.typeConnection = answer.itemObject.typeConnection;
                                selectDataForm.connectionObject = answer.itemObject;
                                selectDataForm.changeVisibleState(true);
                            });
                        }
                    }
                    break;
                }
            case "DeleteConnection":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.selectedItem.remove();
                        jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.ClearAllGalleries();
                        jsObject.UpdateStateUndoRedoButtons();
                    }
                    break;
                }
            case "CreateOrEditRelation":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New") jsObject.options.dictionaryTree.addRelation(answer.itemObject);
                        else jsObject.options.dictionaryTree.editRelation(answer);
                        jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                        if (jsObject.options.forms.dataForm && jsObject.options.forms.dataForm.visible) {
                            jsObject.options.forms.dataForm.rebuildTrees(answer.itemObject.nameInSource, "Relation");
                        }
                        if (jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                            jsObject.options.forms.crossTabForm.tabbedPane.tabsPanels.Data.rebuildTrees(answer.itemObject.nameInSource, "Relation");
                        }
                    }
                    break;
                }
            case "DeleteRelation":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.deleteRelation();
                        jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "CreateOrEditColumn":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New")
                            jsObject.options.dictionaryTree.addColumn(answer);
                        else
                            jsObject.options.dictionaryTree.editColumn(answer);
                        if (answer.databases)
                            jsObject.options.report.dictionary.databases = answer.databases;
                        if (answer.businessObjects)
                            jsObject.options.report.dictionary.businessObjects = answer.businessObjects;

                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteColumn":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.deleteColumn(answer);
                        if (answer.databases) jsObject.options.report.dictionary.databases = answer.databases;
                        if (answer.businessObjects) jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "CreateOrEditParameter":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New") jsObject.options.dictionaryTree.addParameter(answer);
                        else jsObject.options.dictionaryTree.editParameter(answer);
                        if (answer.databases) jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteParameter":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.deleteParameter(answer);
                        if (answer.databases) jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "CreateOrEditDataSource":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New")
                            jsObject.options.dictionaryTree.addDataSource(answer.itemObject);
                        else
                            jsObject.options.dictionaryTree.editDataSource(answer.itemObject);
                        jsObject.options.report.dictionary.databases = answer.databases;

                        jsObject.UpdateStateUndoRedoButtons();
                        if (jsObject.options.forms.dataForm && jsObject.options.forms.dataForm.visible) {
                            jsObject.options.forms.dataForm.rebuildTrees(answer.itemObject.name, "DataSource");
                        }
                        if (jsObject.options.forms.editDataSourceFromOtherDatasourcesForm && jsObject.options.forms.editDataSourceFromOtherDatasourcesForm.visible) {
                            jsObject.options.forms.editDataSourceFromOtherDatasourcesForm.rebuildTrees(answer.itemObject.name);
                        }
                        if (jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                            jsObject.options.forms.crossTabForm.tabbedPane.tabsPanels.Data.rebuildTrees(answer.itemObject.name, "DataSource");
                        }
                        if (jsObject.options.forms.wizardForm && jsObject.options.forms.wizardForm.visible) {
                            var dataSources = jsObject.options.report ? jsObject.GetDataSourcesAndBusinessObjectsFromDictionary(jsObject.options.report.dictionary) : null;
                            jsObject.options.forms.wizardForm.dataSourcesFromServer = dataSources;
                            jsObject.options.forms.wizardForm.onshow(true);
                        }
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteDataSource":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.deleteDataSource();
                        jsObject.options.report.dictionary.databases = answer.databases;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "CreateOrEditBusinessObject":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New")
                            jsObject.options.dictionaryTree.addBusinessObject(answer.itemObject, answer.parentBusinessObjectFullName);
                        else
                            jsObject.options.dictionaryTree.editBusinessObject(answer.itemObject);
                        jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                        jsObject.UpdateStateUndoRedoButtons();
                        if (jsObject.options.forms.dataForm && jsObject.options.forms.dataForm.visible) {
                            jsObject.options.forms.dataForm.rebuildTrees(answer.itemObject.name, "BusinessObject");
                        }
                        if (jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                            jsObject.options.forms.crossTabForm.tabbedPane.tabsPanels.Data.rebuildTrees(answer.itemObject.name, "BusinessObject");
                        }
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteBusinessObject":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.deleteBusinessObject();
                        jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteBusinessObjectCategory":
                {
                    jsObject.options.dictionaryTree.selectedItem.remove();
                    jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "EditBusinessObjectCategory":
                {
                    jsObject.options.dictionaryTree.editBusinessObjectCategory(answer);
                    jsObject.options.report.dictionary.businessObjects = answer.businessObjects;
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "DeleteVariablesCategory":
                {
                    jsObject.options.dictionaryTree.selectedItem.remove();
                    jsObject.options.report.dictionary.variables = answer.variables;
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "EditVariablesCategory":
                {


                    jsObject.options.dictionaryTree.selectedItem.itemObject.name = answer.newName;
                    jsObject.options.dictionaryTree.selectedItem.repaint();
                    jsObject.options.report.dictionary.variables = answer.variables;
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "CreateVariablesCategory":
                {
                    jsObject.options.dictionaryTree.createVariablesCategory(answer);
                    jsObject.options.report.dictionary.variables = answer.variables;
                    jsObject.UpdateStateUndoRedoButtons();
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "CreateOrEditVariable":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New")
                            jsObject.options.dictionaryTree.addVariable(answer.itemObject);
                        else
                            jsObject.options.dictionaryTree.editVariable(answer.itemObject);
                        jsObject.options.report.dictionary.variables = answer.variables;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "DeleteVariable":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.selectedItem.remove();
                        jsObject.options.report.dictionary.variables = answer.variables;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "CreateOrEditResource":
                {
                    if (answer.itemObject) {
                        if (answer.mode == "New")
                            jsObject.options.dictionaryTree.addResource(answer.itemObject);
                        else
                            jsObject.options.dictionaryTree.editResource(answer.itemObject);
                        jsObject.options.report.dictionary.resources = answer.resources;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                        jsObject.UpdateResourcesFonts();
                    }
                    break;
                }
            case "DeleteResource":
                {
                    if (answer.deleteResult) {
                        jsObject.options.dictionaryTree.selectedItem.remove();
                        jsObject.options.report.dictionary.resources = answer.resources;
                        jsObject.UpdateStateUndoRedoButtons();
                        jsObject.ClearAllGalleries();
                        jsObject.UpdateResourcesFonts();
                    }
                    else if (answer.usedObjects) {
                        var form = jsObject.MessageFormForDeleteUsedResource();
                        form.show(answer.resourceName, answer.usedObjects);
                        form.action = function (state) {
                            if (state) {
                                var selectedItem = jsObject.options.dictionaryTree.selectedItem;
                                if (selectedItem) jsObject.SendCommandDeleteResource(selectedItem, true);
                            }
                        }
                    }
                    break;
                }
            case "GetAllConnections":
                {
                    jsObject.InitializeNameInSourceForm(function (nameInSourceForm) {
                        nameInSourceForm.connections = answer.connections;
                        nameInSourceForm.changeVisibleState(true);
                    });
                    break;
                }
            case "RetrieveColumns":
                {
                    if (answer.columns) {
                        jsObject.InitializeEditDataSourceForm(function (editDataSourceForm) {
                            var currentColumns = editDataSourceForm.columnsAndParametersTree.getItemObjects("Column");
                            var currentParameters = editDataSourceForm.columnsAndParametersTree.getItemObjects("Parameter");
                            var allColumns = jsObject.ConcatColumns(currentColumns, answer.columns);
                            var allParameters = jsObject.ConcatColumns(currentParameters, answer.parameters);
                            allColumns.sort(jsObject.SortByName);
                            allParameters.sort(jsObject.SortByName);
                            editDataSourceForm.columnsAndParametersTree.addColumnsAndParameters(allColumns, allParameters, true);
                            editDataSourceForm.columnsAndParametersTree.parametersItem.style.display = editDataSourceForm.datasource.parameterTypes ? "" : "none";
                            editDataSourceForm.columnsAndParametersTree.onSelectedItem();
                        });
                        jsObject.ClearAllGalleries();
                    }
                    break;
                }
            case "AddStyle":
                {
                    if (answer.styleObject) {
                        jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
                            styleDesignerForm.stylesTree.addItem(answer.styleObject);
                        });
                    }
                    break;
                }
            case "CreateStylesFromComponents":
                {
                    if (answer.styles) {
                        jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
                            for (var i = 0; i < answer.styles.length; i++) {
                                styleDesignerForm.stylesTree.addItem(answer.styles[i]);
                            }
                        });
                    }
                    break;
                }
            case "UpdateStyles":
                {
                    jsObject.options.report = null;
                    jsObject.options.reportGuid = answer.reportGuid;
                    jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                    jsObject.options.reportIsModified = true;
                    jsObject.UpdateStateUndoRedoButtons();

                    if (answer.selectedObjectName) jsObject.BackToSelectedComponent(answer.selectedObjectName);
                    if (jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                        jsObject.options.forms.crossTabForm.controls.stylesContainer.update();
                    }

                    jsObject.options.report.chartStylesContent = null;
                    jsObject.options.report.mapStylesContent = null;
                    jsObject.options.report.gaugeStylesContent = null;

                    var editChartForm = jsObject.options.forms.editChart;
                    if (editChartForm && editChartForm.visible) {
                        editChartForm.stylesContainer.stylesProgress.style.display = "";
                        editChartForm.stylesContainer.clear();
                        editChartForm.jsObject.SendCommandGetStylesContent({ componentName: editChartForm.chartProperties.name });
                    }
                    break;
                }
            case "CreateStyleCollection":
                {
                    jsObject.InitializeStyleDesignerForm(function (styleDesignerForm) {
                        styleDesignerForm.addStylesCollection(answer.newStylesCollection, answer.collectionName, answer.removeExistingStyles);
                    });
                    break;
                }
            case "StartEditChartComponent":
                {
                    if (answer.properties) {
                        jsObject.InitializeEditChartForm(function (editChartForm) {
                            editChartForm.chartProperties = answer.properties;
                            editChartForm.changeVisibleState(true);
                        });
                    }
                    break;
                }
            case "StartEditMapComponent":
                {
                    if (answer.properties) {
                        jsObject.InitializeEditMapForm(function (editMapForm) {
                            editMapForm.mapProperties = answer.properties;
                            editMapForm.mapSvgContent = answer.svgContent;
                            editMapForm.changeVisibleState(true);
                        });
                    }
                    break;
                }
            case "StartEditGaugeComponent":
                {
                    if (answer.properties) {
                        jsObject.InitializeEditGaugeForm(function (editGaugeForm) {
                            editGaugeForm.gaugeProperties = answer.properties;
                            editGaugeForm.changeVisibleState(true);
                        });
                    }
                    break;
                }
            case "AddSeries":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        var lastSeries = editChartForm.chartProperties.series[editChartForm.chartProperties.series.length - 1];
                        var seriesItem = editChartForm.seriesContainer.addItem(lastSeries.name, lastSeries);
                        editChartForm.chartImage.update();
                    }
                    break;
                }
            case "SeriesMove":
            case "RemoveSeries":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        editChartForm.seriesContainer.update();
                        editChartForm.chartImage.update();
                        if (answer.selectedIndex != null) {
                            editChartForm.seriesContainer.items[answer.selectedIndex].action();
                        }
                    }
                    break;
                }
            case "AddConstantLineOrStrip":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        var itemType = answer.itemType;
                        var container = editChartForm[itemType + "Container"];
                        var collection = itemType == "ConstantLines" ? editChartForm.chartProperties.constantLines : editChartForm.chartProperties.strips;
                        var lastItem = collection[collection.length - 1];
                        var newItem = container.addItem(lastItem.name, lastItem);
                        editChartForm.chartImage.update();
                    }
                    break;
                }
            case "ConstantLineOrStripMove":
            case "RemoveConstantLineOrStrip":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        var itemType = answer.itemType;
                        var container = editChartForm[itemType + "Container"];
                        container.update();
                        editChartForm.chartImage.update();
                        if (answer.selectedIndex != null) {
                            container.items[answer.selectedIndex].action();
                        }
                    }
                    break;
                }
            case "GetLabelsContent":
                {
                    if (answer.labelsContent) {
                        var editChartForm = jsObject.options.forms.editChart;
                        if (answer.isSeriesLables) {
                            editChartForm.seriesLabelsContainer.update(answer.labelsContent);
                        }
                        else {
                            editChartForm.labelsContainer.update(answer.labelsContent);
                        }
                    }
                    break;
                }
            case "GetStylesContent":
                {
                    if (answer.stylesContent) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.stylesContainer.update(answer.stylesContent);
                    }
                    break;
                }
            case "SetLabelsType":
            case "SetChartStyle":
            case "SetChartPropertyValue":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        editChartForm.chartImage.update();
                        if (answer.command == "SetLabelsType") {
                            if (!answer.isSeriesLabels)
                                editChartForm.labelPropertiesContainer.buttons.Common.action();
                            else
                                editChartForm.seriesPropertiesContainer.buttons.SeriesLabels.action();
                        }
                    }
                    break;
                }
            case "SendContainerValue":
                {
                    if (answer.properties) {
                        var editChartForm = jsObject.options.forms.editChart;
                        editChartForm.chartProperties = answer.properties;
                        editChartForm.chartImage.update();
                    }
                    if (answer.closeChartForm) editChartForm.action();
                    break;
                }
            case "GetDatabaseData":
                {
                    jsObject.InitializeSelectDataForm(function (selectDataForm) {
                        selectDataForm.buildTree(answer.data)
                    });
                    break;
                }
            case "ApplySelectedData":
                {
                    if (answer.dictionary) {
                        var attachedItems = jsObject.options.report.dictionary.attachedItems;
                        answer.dictionary.attachedItems = attachedItems;
                        jsObject.options.report.dictionary = answer.dictionary;
                        jsObject.options.dictionaryTree.build(answer.dictionary, true);
                        var selectedDatabaseItem = null;
                        for (var key in jsObject.options.dictionaryTree.mainItems.DataSources.childs) {
                            var databaseItem = jsObject.options.dictionaryTree.mainItems.DataSources.childs[key];
                            if (databaseItem.itemObject.name == answer.databaseName) {
                                selectedDatabaseItem = databaseItem;
                                break;
                            }
                        }
                        if (selectedDatabaseItem) {
                            selectedDatabaseItem.openTree();
                            selectedDatabaseItem.setOpening(true);
                            for (var key in selectedDatabaseItem.childs) {
                                var dataSourceItem = selectedDatabaseItem.childs[key];
                                if (dataSourceItem.itemObject.name.indexOf(answer.selectedDataSource.name) == 0) {
                                    dataSourceItem.setSelected();
                                    break;
                                }
                            }
                        }
                        if (jsObject.options.forms.dataForm && jsObject.options.forms.dataForm.visible) {
                            jsObject.options.forms.dataForm.rebuildTrees(answer.selectedDataSource.name, "DataSource");
                        }
                        if (jsObject.options.forms.editDataSourceFromOtherDatasourcesForm && jsObject.options.forms.editDataSourceFromOtherDatasourcesForm.visible) {
                            jsObject.options.forms.editDataSourceFromOtherDatasourcesForm.rebuildTrees(answer.selectedDataSource.name);
                        }
                        if (jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                            jsObject.options.forms.crossTabForm.tabbedPane.tabsPanels.Data.rebuildTrees(answer.selectedDataSource.name, "DataSource");
                        }
                        if (jsObject.options.forms.wizardForm && jsObject.options.forms.wizardForm.visible) {
                            var dataSources = jsObject.options.report ? jsObject.GetDataSourcesAndBusinessObjectsFromDictionary(jsObject.options.report.dictionary) : null;
                            jsObject.options.forms.wizardForm.dataSourcesFromServer = dataSources;
                            jsObject.options.forms.wizardForm.onshow(true);
                        }
                    }
                    jsObject.ClearAllGalleries();
                    break;
                }
            case "CreateTextComponent":
                {
                    if (answer.newComponents) {
                        for (var i = 0; i < answer.newComponents.length; i++) {
                            var component = jsObject.CreateComponent(answer.newComponents[i]);
                            if (component) {
                                component.repaint();
                                jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                                jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;
                                jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                                component.setOnTopLevel();
                                component.setSelected();
                                jsObject.UpdatePropertiesControls();
                                jsObject.UpdateStateUndoRedoButtons();
                            }
                        }
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    }
                    break;
                }
            case "CreateDataComponent":
                {
                    if (answer.pageComponents) {
                        var pageComponents = JSON.parse(answer.pageComponents);
                        var lastComponent = null;
                        for (var componentName in pageComponents.components) {
                            var componentProps = pageComponents.components[componentName];

                            if (jsObject.options.report.pages[answer.pageName].components[componentProps.name] == null) {
                                var component = jsObject.CreateComponent(componentProps);
                                if (component) {
                                    component.repaint();
                                    lastComponent = component;
                                    jsObject.options.report.pages[answer.pageName].appendChild(component);
                                    jsObject.options.report.pages[answer.pageName].components[componentProps.name] = component;
                                }
                            }
                        }
                        if (lastComponent) {
                            lastComponent.setOnTopLevel();
                            lastComponent.setSelected();
                            jsObject.UpdatePropertiesControls();
                            jsObject.UpdateStateUndoRedoButtons();
                        }
                        jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    }
                    break;
                }
            case "SetReportProperties":
                {
                    if (jsObject.options.report) {
                        jsObject.WriteAllProperties(jsObject.options.report, answer.properties);
                        jsObject.UpdatePropertiesControls();
                        jsObject.UpdateStateUndoRedoButtons();
                    }
                    break;
                }
            case "PageMove":
                {
                    jsObject.ChangePageIndexes(answer.pageIndexes);
                    jsObject.options.pagesPanel.pagesContainer.updatePages();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "TestConnection":
                {
                    jsObject.InitializeEditConnectionForm(function (editConnectionForm) {
                        editConnectionForm.testConnection.setEnabled(true);
                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        if (answer.testResult != null) errorMessageForm.show(answer.testResult, true);
                    });
                    break;
                }
            case "RunQueryScript":
                {
                    var text = answer.resultQueryScript == "successfully"
                        ? jsObject.loc.FormDictionaryDesigner.ExecutedSQLStatementSuccessfully
                        : (answer.resultQueryScript || jsObject.loc.DesignerFx.ConnectionError);
                    var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                    errorMessageForm.show(text, answer.resultQueryScript == "successfully");
                    break;
                }
            case "ViewData":
                {                                        
                    jsObject.InitializeViewDataForm(function (viewDataForm) {
                        viewDataForm.show(answer.resultData, answer.dataSourceName);
                    });
                    break;
                }
            case "ApplyDesignerOptions":
                {
                    jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                    jsObject.options.reportIsModified = true;
                    break;
                }
            case "GetSqlParameterTypes":
                {
                    if (jsObject.options.forms.editDataSourceForm && jsObject.options.forms.editDataSourceForm.visible &&
                        answer.sqlParameterTypes && jsObject.options.forms.editDataSourceForm.datasource) {
                        jsObject.options.forms.editDataSourceForm.datasource.parameterTypes = answer.sqlParameterTypes;
                        jsObject.options.forms.editDataSourceForm.columnToolBar.parameterNew.style.display = "";
                        jsObject.options.forms.editDataSourceForm.columnToolBar.retrieveColumnsAndParameters.style.display = "";
                        jsObject.options.forms.editDataSourceForm.columnsAndParametersTree.parametersItem.style.display = "";
                    }
                    break;
                }
            case "AlignToGridComponents":
            case "ChangeArrangeComponents":
            case "ChangeSizeComponents":
                {
                    jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);
                    jsObject.PaintSelectedLines();
                    jsObject.UpdatePropertiesControls();
                    jsObject.UpdateStateUndoRedoButtons();
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "UpdateSampleTextFormat":
                {
                    var textFormatForm = jsObject.options.forms.textFormatForm;
                    if (textFormatForm && textFormatForm.visible) {
                        textFormatForm.sampleContainer.innerHTML = answer.sampleText;
                    }
                    break;
                }
            case "StartEditCrossTabComponent":
                {
                    jsObject.InitializeCrossTabForm(function (editCrossTabForm) {
                        editCrossTabForm.changeVisibleState(true);
                    });
                    break;
                }
            case "UpdateCrossTabComponent":
                {
                    if (answer.command && answer.callbackFunctionId && jsObject.options.callbackFunctions[answer["callbackFunctionId"]] != null) {
                        jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer);
                        delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    }
                    else {
                        jsObject.InitializeCrossTabForm(function (editCrossTabForm) {
                            editCrossTabForm.recieveCommandResult(answer.updateResult);
                        });
                    }
                    break;
                }
            case "GetCrossTabColorStyles":
                {
                    if (answer.colorStyles && jsObject.options.forms.crossTabForm && jsObject.options.forms.crossTabForm.visible) {
                        jsObject.options.forms.crossTabForm.controls.stylesContainer.fill(answer.colorStyles);
                    }
                    break;
                }
            case "DuplicatePage":
                {
                    if (answer["reportGuid"] && answer["reportObject"]) {
                        jsObject.options.report = null;
                        jsObject.options.reportGuid = answer.reportGuid;
                        jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                        jsObject.options.reportIsModified = true;
                        jsObject.UpdateStateUndoRedoButtons();
                        var pageButton = jsObject.options.pagesPanel.pagesContainer.pages[answer.selectedPageIndex];
                        if (pageButton) pageButton.action();
                    }
                    break;
                }
            case "SetEventValue":
                {
                    break;
                }
            case "GetCrossTabStylesContent":
            case "GetTableStylesContent":
            case "GetGaugeStylesContent":
            case "GetMapStylesContent":
                {
                    if (jsObject.options.callbackFunctions[answer["callbackFunctionId"]]) {
                        jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer["stylesContent"]);
                        delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    }
                    break;
                }
            case "ChangeTableComponent":
                {
                    if (answer.result) {
                        switch (answer.result.command) {
                            case "convertTo":
                            case "insertColumnToLeft":
                            case "insertColumnToRight":
                            case "deleteColumn":
                            case "insertRowAbove":
                            case "insertRowBelow":
                            case "deleteRow":
                            case "joinCells":
                            case "changeColumnsOrRowsCount":
                            case "applyStyle":
                                {
                                    var cells = answer.result.cells;
                                    var page = jsObject.options.report.pages[answer.result.pageName];
                                    var table = page.components[answer.result.tableName];
                                    if (cells && table) {
                                        jsObject.RebuildTable(table, cells, answer.result.command != "convertTo");
                                        if (answer.result.tableProperties) {
                                            jsObject.CreateComponentProperties(table, answer.result.tableProperties);
                                            table.repaint();
                                        }

                                        if (answer.result.selectedCells) {
                                            if (answer.result.selectedCells.length == 0) {
                                                table.setSelected();
                                            }
                                            else if (answer.result.selectedCells.length == 1) {
                                                var cell = page.components[answer.result.selectedCells[0]];
                                                if (cell) cell.setSelected();
                                            }
                                            else {
                                                jsObject.SetSelectedObjectsByNames(page, answer.result.selectedCells);
                                            }
                                        }
                                        jsObject.UpdatePropertiesControls();
                                        jsObject.UpdateStateUndoRedoButtons();
                                    }
                                    if (answer.result.rebuildProps) page.rebuild(answer.result.rebuildProps);
                                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                                    break;
                                }

                        }
                    }
                    break;
                }            
            case "UpdateImagesArray":
                {
                    jsObject.options.images = answer.images;
                    jsObject.InitializeTextEditorForm(function () { });
                    jsObject.InitializeImageForm(function () { });
                    jsObject.InitializeDataForm(function () { });
                    break;
                }
            case "OpenStyle":
                {
                    var styleDesignerForm = jsObject.options.forms.styleDesignerForm
                    if (styleDesignerForm && styleDesignerForm.visible) {
                        styleDesignerForm.stylesCollection = answer.stylesCollection;
                        styleDesignerForm.stylesTree.updateItems(styleDesignerForm.stylesCollection);
                    }
                    break;
                }
            case "CreateFieldOnDblClick":
                {
                    var page = jsObject.options.report.pages[answer.pageName];
                    var newComponents = answer.newComponents;
                    var lastComponent = null;

                    //Add or change new cells
                    for (var i = 0; i < newComponents.length; i++) {
                        var compObject = newComponents[i];

                        var component = jsObject.CreateComponent(compObject);
                        page.appendChild(component);
                        page.components[compObject.name] = component;
                        component.repaint();
                        lastComponent = component;
                    }

                    jsObject.options.report.pages[answer.pageName].rebuild(answer.rebuildProps);

                    if (lastComponent) {
                        lastComponent.setOnTopLevel();
                        lastComponent.setSelected();
                        jsObject.UpdatePropertiesControls();
                        jsObject.UpdateStateUndoRedoButtons();
                    }
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "GetParamsFromQueryString":
                {
                    if (jsObject.options.callbackFunctions[answer["callbackFunctionId"]]) {
                        jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer.params);
                        delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    }
                    break;
                }
            case "CreateMovingCopyComponent":
                {                    
                    var components = answer["components"];
                    var countComponents = jsObject.GetCountObjects(components);
                    if (countComponents > 0) {
                        for (var i = 0; i < countComponents; i++) {
                            var component = jsObject.CreateComponent(components[i]);
                            if (component) {
                                component.repaint();
                                jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                                jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;

                                if (i == 0) {
                                    component.setOnTopLevel();
                                    if (answer["isLastCommand"]) component.setSelected();
                                }
                            }
                        }
                        jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                        jsObject.UpdateStateUndoRedoButtons();
                    }

                    if (jsObject.options.callbackFunctions[answer["callbackFunctionId"]]) {
                        jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer);
                        delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    }
                    if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    break;
                }
            case "UpdateGaugeComponent":
                {
                    jsObject.InitializeEditGaugeForm(function (editGaugeForm) {
                        editGaugeForm.recieveCommandResult(answer.updateResult);
                    });
                    break;
                }
            case "CreateComponentFromResource":
                {
                    if (answer.newComponent) {
                        var component = jsObject.CreateComponent(answer.newComponent);
                        if (component) {
                            component.repaint();
                            jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                            jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;
                            jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                            component.setOnTopLevel();
                            component.setSelected();
                            jsObject.UpdatePropertiesControls();
                            jsObject.UpdateStateUndoRedoButtons();
                        }
                        if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                    }
                    break;
                }
            case "CreateDatabaseFromResource":
                {
                    if (answer.resourceItemObject) {
                        jsObject.options.dictionaryTree.addResource(answer.resourceItemObject);
                        jsObject.options.report.dictionary.resources = answer.resources;
                        jsObject.ClearAllGalleries();
                    }
                    if (answer.newDataBaseName) {
                        answer.itemObject = jsObject.GetObjectByPropertyValueFromCollection(answer.databases, "name", answer.newDataBaseName);
                        if (answer.itemObject) {
                            answer.mode = "New";
                            jsObject.options.report.dictionary.databases = answer.databases;
                            jsObject.options.dictionaryTree.createOrEditConnection(answer);
                            var newConnectionItem = jsObject.options.dictionaryTree.mainItems["DataSources"].getChildByName(answer.itemObject.name);
                            if (newConnectionItem) {
                                for (var key in newConnectionItem.childs) newConnectionItem.childs[key].remove();
                                jsObject.options.dictionaryTree.addTreeItems(answer.itemObject.dataSources, newConnectionItem);
                            }
                            jsObject.options.dictionaryPanel.toolBar.updateControls();
                        }                        
                    }
                    if (jsObject.options.forms.wizardForm && jsObject.options.forms.wizardForm.visible) {
                        var dataSources = jsObject.options.report ? jsObject.GetDataSourcesAndBusinessObjectsFromDictionary(jsObject.options.report.dictionary) : null;
                        jsObject.options.forms.wizardForm.dataSourcesFromServer = dataSources;
                        jsObject.options.forms.wizardForm.onshow(true);
                    }

                    jsObject.UpdateStateUndoRedoButtons();
                    break;
                }
            case "StartEditBarCodeComponent":
                {                    
                    if (answer.barCode) {
                        jsObject.InitializeBarCodeForm(function (editBarCodeForm) {
                            editBarCodeForm.barCode = answer.barCode;
                            editBarCodeForm.changeVisibleState(true);
                        });
                    }
                    break;
                }
            case "MoveDictionaryItem":
                {
                    if (answer.moveCompleted) {
                        var fromItem = jsObject.options.dictionaryTree.selectedItem;
                        var direction = answer.direction;
                        if (direction) {
                            var toItem = direction == "Down"
                                ? (fromItem.nextSibling || (fromItem && fromItem.itemObject.typeItem == "Variable"
                                    ? (fromItem.parent.nextSibling || fromItem.parent.parent) : null))
                                : (fromItem.previousSibling || (fromItem && fromItem.itemObject.typeItem == "Variable"
                                    ? (fromItem.parent.previousSibling || fromItem.parent.parent) : null))

                            if (toItem != null && fromItem) {
                                if (fromItem.itemObject.typeItem == "Variable" && (toItem.itemObject.typeItem == "Category" || toItem.itemObject.typeItem == "VariablesMainItem")) {
                                    fromItem.remove();
                                    var item = toItem.addChild(fromItem);
                                    item.setSelected();
                                    toItem.setOpening(true);
                                }
                                else if (fromItem.itemObject.typeItem == "Variable" && fromItem.parent.itemObject.typeItem == "Category" &&
                                    toItem.itemObject.typeItem == "Variable" && toItem.parent.itemObject.typeItem == "VariablesMainItem") {
                                    fromItem.remove();
                                    var item = toItem.parent.addChild(fromItem);
                                    item.setSelected();
                                }
                                else {
                                    fromItem.move(answer.direction);
                                }
                                jsObject.options.dictionaryPanel.toolBar.updateControls();
                            }
                        }
                        else if (answer.fromObject && answer.toObject &&
                                (answer.fromObject.typeItem == "Variable" || answer.fromObject.typeItem == "Category") &&
                                (answer.toObject.typeItem == "Variable" || answer.toObject.typeItem == "Category")) {

                            //Drag & Drop Variables or Category
                            var dictionaryTree = jsObject.options.dictionaryTree;

                            //Save opening items
                            var openingCategories = {};
                            var allItems = dictionaryTree.mainItems["Variables"].getAllChilds();
                            for (var i = 0; i < allItems.length; i++) {
                                if (allItems[i].itemObject.typeItem == "Category" && allItems[i].isOpening) {
                                    openingCategories[allItems[i].itemObject.name] = true;
                                }
                            }

                            //repaint variables tree
                            while (dictionaryTree.mainItems["Variables"].childsContainer.childNodes[0]) {
                                dictionaryTree.mainItems["Variables"].childsContainer.childNodes[0].remove();
                            }
                            dictionaryTree.addTreeItems(answer.variablesTree, dictionaryTree.mainItems["Variables"]);

                            allItems = dictionaryTree.mainItems["Variables"].getAllChilds();
                            for (var i = 0; i < allItems.length; i++) {
                                if (allItems[i].itemObject.typeItem == "Category" && openingCategories[allItems[i].itemObject.name]) {
                                    allItems[i].setOpening(true);
                                }
                                if (allItems[i].itemObject.name == answer.fromObject.name) {
                                    allItems[i].setSelected();
                                    allItems[i].openTree();
                                }
                            }
                        }
                    }
                    break;
                }
            case "UpdateReportAliases":
                {
                    if (answer["reportGuid"] && answer["reportObject"]) {
                        jsObject.options.report = null;
                        jsObject.options.reportGuid = answer.reportGuid;
                        jsObject.LoadReport(jsObject.ParseReport(answer.reportObject), true);
                        jsObject.options.reportIsModified = true;
                    }
                    if (answer.selectedObjectName)
                        jsObject.BackToSelectedComponent(answer.selectedObjectName);
                    break;
                }
            case "MoveConnectionDataToResource":
                {
                    if (answer["resourcesTree"] && jsObject.options.dictionaryTree && jsObject.options.report) {
                        jsObject.options.report.dictionary.resources = answer["resourcesTree"];
                        var resMainItem = jsObject.options.dictionaryTree.mainItems["Resources"];
                        resMainItem.removeAllChilds();
                        jsObject.options.dictionaryTree.addTreeItems(answer["resourcesTree"], resMainItem);
                        resMainItem.setOpening(true);

                        var editConnectionForm = jsObject.options.forms.editConnectionForm;

                        if (answer["databases"]) {
                            if (editConnectionForm) editConnectionForm.changeVisibleState(false);

                            var dataMainItem = jsObject.options.dictionaryTree.mainItems["DataSources"];
                            dataMainItem.removeAllChilds();
                            jsObject.options.dictionaryTree.addTreeItems(answer["databases"], dataMainItem);
                            dataMainItem.setOpening(true);

                            jsObject.options.report.dictionary.databases = answer.databases;
                            jsObject.ClearAllGalleries();

                            if (answer.connectionObject) {
                                jsObject.InitializeSelectDataForm(function (selectDataForm) {
                                    selectDataForm.databaseName = answer.connectionObject.name;
                                    selectDataForm.typeConnection = answer.connectionObject.typeConnection;
                                    selectDataForm.connectionObject = answer.connectionObject;
                                    selectDataForm.changeVisibleState(true);
                                });
                            }
                        }
                        else {
                            if (editConnectionForm && editConnectionForm.visible) {
                                if (answer.pathSchema != null) editConnectionForm.pathSchemaControl.textBox.value = answer.pathSchema;
                                if (answer.pathData != null) editConnectionForm.pathDataControl.textBox.value = answer.pathData;
                                editConnectionForm.action();
                            }
                        }
                    }
                    break;
                }
            case "SetMapProperties":
            case "UpdateMapData":
                {
                    var mapComp = (jsObject.options.selectedObject && jsObject.options.selectedObject.properties.name == answer.componentName)
                        ? jsObject.options.selectedObject
                        : jsObject.options.report.getComponentByName(answer.componentName);

                    if (mapComp) {
                        mapComp.properties.svgContent = answer.svgContent;
                        mapComp.repaint();
                    }

                    if (answer.mapData && jsObject.options.forms.editMapForm && jsObject.options.forms.editMapForm.visible) {
                        jsObject.options.forms.editMapForm.controls.dataGridView.fillData(answer.mapData);
                    }
                    break;
                }
            case "CreateTextElement":
            case "CreateTableElement":
                {
                    if (answer.newComponent) {
                        var component = jsObject.CreateDashboardElement(answer.newComponent);
                        if (component) {
                            component.repaint();
                            jsObject.options.report.pages[component.properties.pageName].appendChild(component);
                            jsObject.options.report.pages[component.properties.pageName].components[component.properties.name] = component;
                            jsObject.options.report.pages[component.properties.pageName].rebuild(answer.rebuildProps);
                            component.setOnTopLevel();
                            component.setSelected();
                            jsObject.UpdatePropertiesControls();
                            jsObject.UpdateStateUndoRedoButtons();
                            if (jsObject.options.reportTree) jsObject.options.reportTree.build();
                            if (jsObject.options.report.info.runDesignerAfterInsert) jsObject.ShowComponentForm(component);
                        }                        
                    }
                    break;
                }
            default: {
                if (answer.command && answer.callbackFunctionId && jsObject.options.callbackFunctions[answer["callbackFunctionId"]] != null) {
                    jsObject.options.callbackFunctions[answer["callbackFunctionId"]](answer);
                    delete jsObject.options.callbackFunctions[answer["callbackFunctionId"]];
                    break;
                }
            }
        }
    }

    if (jsObject.options.commands.length >= 1) jsObject.options.commands.splice(0, 1);
    if (jsObject.options.commands.length == 0) {
        if (jsObject.options.processImageStatusPanel) jsObject.options.processImageStatusPanel.hide();
        if (jsObject.options.processImage) jsObject.options.processImage.hide();
    }
    else jsObject.ExecuteCommandFromStack();
}

//Send Create Report
StiMobileDesigner.prototype.SendCommandCreateReport = function (callbackFunction, needClearAfterOldReport) {
    var params = {
        command: "CreateReport",
        defaultUnit: this.options.defaultUnit,
        callbackFunctionId: this.generateKey()
    };
    if (needClearAfterOldReport) params.needClearAfterOldReport = true;
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Send Open Report
StiMobileDesigner.prototype.SendCommandOpenReport = function (fileContent, fileName, reportParams, filePath) {
    var params = {
        command: "OpenReport",
        openReportFile: fileName,
        filePath: filePath,
        password: reportParams.password,
        isPacked: reportParams.isPacked
    };
    if (fileContent) params.base64Data = fileContent.indexOf("base64,") >= 0 ? fileContent.substr(fileContent.indexOf("base64,") + 7) : fileContent;
    this.AddCommandToStack(params);
}

//Send Save Report
StiMobileDesigner.prototype.SendCommandSaveReport = function () {
    this.options.reportIsModified = false;
    if (this.options.haveSaveEvent) {
        var params = {};
        params.command = "SaveReport";
        params.reportFile = this.options.report.properties.reportFile;
        if (this.options.report.encryptedPassword) params.encryptedPassword = this.options.report.encryptedPassword;

        if (this.options.saveReportMode == "Hidden") {
            this.AddCommandToStack(params);
        }
        else if (this.options.saveReportMode == "Visible") {
            this.options.ignoreBeforeUnload = true;
            this.PostForm(params);
            this.options.ignoreBeforeUnload = false;
        }
        else if (this.options.saveReportMode == "NewWindow") {
            this.options.ignoreBeforeUnload = true;
            var win = window.open("about:blank", '_blank');
            this.PostForm(params, win ? win.document : document);
            this.options.ignoreBeforeUnload = false;
        }
    }
    else {
        var jsObject = this;
        this.options.ignoreBeforeUnload = true;
        var params = {
            command: "DownloadReport",
            reportFile: this.options.report.properties.reportFile,
            reportGuid: this.options.reportGuid
        }
        if (this.options.report.encryptedPassword) params.encryptedPassword = this.options.report.encryptedPassword;
        this.PostForm(params);
        setTimeout(function () { jsObject.options.ignoreBeforeUnload = false; }, 500);
    }
}

//Send Save As Report
StiMobileDesigner.prototype.SendCommandSaveAsReport = function (saveType) {    
    this.options.reportIsModified = false;

    if (this.options.haveSaveAsEvent) {        
        var params = {};
        params.command = "SaveAsReport";
        params.reportFile = this.options.report.properties.reportFile;
        if (this.options.report.encryptedPassword) params.encryptedPassword = this.options.report.encryptedPassword;
        if (saveType) params.saveType = saveType;

        if (this.options.saveReportAsMode == "Hidden") {
            this.AddCommandToStack(params);
        }
        else if (this.options.saveReportAsMode == "Visible") {
            this.options.ignoreBeforeUnload = true;
            this.PostForm(params);
            this.options.ignoreBeforeUnload = false;
        }
        else if (this.options.saveReportAsMode == "NewWindow") {
            this.options.ignoreBeforeUnload = true;
            var win = window.open("about:blank", '_blank');
            this.PostForm(params, win ? win.document : document);
            this.options.ignoreBeforeUnload = false;
        }
    }
    else {
        var jsObject = this;
        this.options.ignoreBeforeUnload = true;
        var params = {
            command: "DownloadReport",
            reportFile: this.options.report.properties.reportFile,
            reportGuid: this.options.reportGuid
        }
        if (this.options.report.encryptedPassword) params.encryptedPassword = this.options.report.encryptedPassword;
        if (saveType) params.saveType = saveType;
        this.PostForm(params);
        setTimeout(function () { jsObject.options.ignoreBeforeUnload = false; }, 500);
    }
}

//Send Close Report
StiMobileDesigner.prototype.SendCommandCloseReport = function () {
    var params = {};
    params.command = "CloseReport";
    this.AddCommandToStack(params);
}

//Send Change Rect Component
StiMobileDesigner.prototype.SendCommandChangeRectComponent = function (component, command, runFromProperty, resizeType) {
    if (!component) return;
    var params = {};
    params.command = command;
    params.zoom = this.options.report.zoom.toString();
    params.runFromProperty = runFromProperty;
    params.resizeType = resizeType;
    params.components = [];
    if (this.options.in_drag && this.options.in_drag.length > 4) params.moveAfterCopyPaste = true;

    var components = this.Is_array(component) ? component : [component];
    for (var i = 0; i < components.length; i++) {
        if (command == "MoveComponent" && this.IsTableCell(components[i])) continue;
        if (components[i].properties.unitLeft == null || components[i].properties.unitTop == null ||
            components[i].properties.unitWidth == null || components[i].properties.unitHeight == null)
            continue;
        if (!params.pageName) params.pageName = components[i].properties.pageName;
        var compParams = {
            componentName: components[i].properties.name,
            invertWidth: components[i].properties.invertWidth,
            invertHeight: components[i].properties.invertHeight,
            componentRect: components[i].properties.unitLeft + "!" + components[i].properties.unitTop + "!" +
                components[i].properties.unitWidth + "!" + components[i].properties.unitHeight
        }
        params.components.push(compParams);
    }
    if (params.components.length == 0) return;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Component
StiMobileDesigner.prototype.SendCommandCreateComponent = function (pageName, typeComponent, componentRect, additionalParams) {
    var params = {};
    params.command = "CreateComponent";
    params.pageName = pageName;
    params.typeComponent = typeComponent;
    params.componentRect = componentRect;
    params.zoom = this.options.report.zoom.toString();
    if (additionalParams) params.additionalParams = additionalParams;

    if (this.options.report.info.useLastFormat && this.options.lastStyleProperties && !this.IsBandComponent(params)) {
        params.lastStyleProperties = this.options.lastStyleProperties;
    }
    
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Remove Component
StiMobileDesigner.prototype.SendCommandRemoveComponent = function (component) {
    var params = {};
    params.command = "RemoveComponent";
    params.components = [];

    var components = this.Is_array(component) ? component : [component];
    for (var i = 0; i < components.length; i++) {
        params.components.push(components[i].properties.name);
    }
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Add Page
StiMobileDesigner.prototype.SendCommandAddPage = function (pageIndex) {
    var params = {};
    params.command = "AddPage";
    params.pageIndex = pageIndex.toString();
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Add Dashboard
StiMobileDesigner.prototype.SendCommandAddDashboard = function (pageIndex) {
    var params = {};
    params.command = "AddDashboard";
    params.pageIndex = pageIndex.toString();
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Remove Page
StiMobileDesigner.prototype.SendCommandRemovePage = function (page) {
    var params = {};
    params.command = "RemovePage";
    params.pageName = page.properties.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Rebuild Page
StiMobileDesigner.prototype.SendCommandRebuildPage = function (page) {
    var params = {};
    params.command = "RebuildPage";
    params.pageName = page.properties.name;
    this.AddCommandToStack(params);
}

//Send Change Unit
StiMobileDesigner.prototype.SendCommandChangeUnit = function (reportUnit) {
    var params = {};
    params.command = "ChangeUnit";
    params.reportUnit = reportUnit;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Properties
StiMobileDesigner.prototype.SendCommandSendProperties = function (object, propertiesNames, updateAllControls) {
    var objects = this.Is_array(object) ? object : [object];

    if (objects.length > 0 && objects[0].typeComponent == "StiCrossField" && propertiesNames) {
        for (var i = 0; i < propertiesNames.length; i++) {
            this.ApplyCrossTabFieldProperty(objects[0], propertiesNames[i], objects[0].properties[propertiesNames[i]]);
        }
    }
    else {
        var params = {};
        params.command = "SendProperties";
        params.updateAllControls = updateAllControls ? true : false;
        params.zoom = this.options.report.zoom.toString();
        params.components = [];
        this.options.updateLastStyleProperties = false;

        var stylePropertyNames = ["border", "brush", "backColor", "textBrush", "font", "horAlignment", "vertAlignment"];
        
        for (var i = 0; i < objects.length; i++) {
            var cannotChange = objects[i].properties.restrictions && !(objects[i].properties.restrictions == "All" || objects[i].properties.restrictions.indexOf("AllowChange") >= 0);
            var properties = [];
            var component = {};

            for (var num = 0; num < propertiesNames.length; num++) {
                if (typeof (objects[i].properties[propertiesNames[num]]) != "undefined") {
                    properties.push({
                        name: propertiesNames[num],
                        value: objects[i].properties[propertiesNames[num]]
                    });

                    if (propertiesNames[num] == "restrictions") cannotChange = false;
                    if (stylePropertyNames.indexOf(propertiesNames[num]) >= 0) this.options.updateLastStyleProperties = true;
                }
            }

            component.typeComponent = objects[i].typeComponent;
            component.componentName = objects[i].properties.name;
            component.properties = properties;
            if (cannotChange) component.cannotChange = true;
            params.components.push(component);
        }

        this.options.reportIsModified = true;
        this.AddCommandToStack(params);
    }
}

//Send Get Preview Pages
StiMobileDesigner.prototype.SendCommandLoadReportToViewer = function () {
    if (this.options.report == null) return;
    (this.options.viewer.jsObject.controls || this.options.viewer.jsObject.options).reportPanel.clear();
    (this.options.viewer.jsObject.controls || this.options.viewer.jsObject.options).processImage.show();
    
    var params = {
        command: "LoadReportToViewer",
        viewerClientGuid: this.options.viewer.jsObject.options.clientGuid,
        checkReportBeforePreview: this.options.checkReportBeforePreview
    };
    this.AddCommandToStack(params);
}

//Set To Clipboard
StiMobileDesigner.prototype.SendCommandSetToClipboard = function (component) {
    if (this.options.movingCloneComponents) return;
    this.options.clipboard = true;
    var params = {};
    params.command = "SetToClipboard";
    params.pageName = this.options.currentPage.properties.name;
    params.components = [];
    var components = this.Is_array(component) ? component : [component];
    for (var i = 0; i < components.length; i++) {
        params.components.push(components[i].properties.name);
    }
    this.AddCommandToStack(params);
}

//Get From Clipboard
StiMobileDesigner.prototype.SendCommandGetFromClipboard = function () {
    if (this.options.movingCloneComponents) return;
    var params = {};
    params.command = "GetFromClipboard";
    params.pageName = this.options.currentPage.properties.name;
    params.zoom = this.options.report.zoom.toString();    
    this.AddCommandToStack(params); 
}

//Send Synchronize
StiMobileDesigner.prototype.SendCommandSynchronization = function () {
    var params = {};
    params.command = "Synchronization";
    this.AddCommandToStack(params);
}

//Send Undo
StiMobileDesigner.prototype.SendCommandUndo = function () {
    var params = {};
    params.command = "Undo";
    params.zoom = this.options.report.zoom.toString();
    params.selectedObjectName = this.options.selectedObject ? this.options.selectedObject.properties.name : null;
    this.AddCommandToStack(params); 
}

//Send Redo
StiMobileDesigner.prototype.SendCommandRedo = function () {
    var params = {};
    params.command = "Redo";
    params.zoom = this.options.report.zoom.toString();
    params.selectedObjectName = this.options.selectedObject ? this.options.selectedObject.properties.name : null;
    this.AddCommandToStack(params); 
}

//Send Rename Component
StiMobileDesigner.prototype.SendCommandRenameComponent = function (component, newName) {
    var params = {};
    params.command = "RenameComponent";
    params.typeComponent = component.typeComponent;
    params.oldName = component.properties.name;
    params.newName = newName;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Wizard Result
StiMobileDesigner.prototype.SendCommandWizardResult = function (wizardResult) {
    var params = {
        command: "WizardResult",
        defaultUnit: this.options.defaultUnit,
        wizardResult: wizardResult
    };
    if (this.options.cloudParameters && this.options.report && this.options.report.dictionary.attachedItems) {
        params.attachedItems = this.options.report.getAttachedItems();
        params.sessionKey = this.options.cloudParameters.sessionKey;
    }
    var fileMenu = this.options.menus.fileMenu || this.InitializeFileMenu();
    fileMenu.changeVisibleState(false);
    this.AddCommandToStack(params);
}

//Send ExitDesigner
StiMobileDesigner.prototype.SendCommandExitDesigner = function () {
    this.PostForm({ command: "ExitDesigner" });
}

//Send Synchronize Dictionary
StiMobileDesigner.prototype.SendCommandSynchronizeDictionary = function () {
    var params = {};
    params.command = "SynchronizeDictionary";
    this.AddCommandToStack(params);
}

//Send New Dictionary
StiMobileDesigner.prototype.SendCommandNewDictionary = function () {
    var params = {};
    params.command = "NewDictionary";
    this.AddCommandToStack(params);
}

//Send Get Connection Types
StiMobileDesigner.prototype.SendCommandGetConnectionTypes = function () {
    var params = {};
    params.command = "GetConnectionTypes";
    this.AddCommandToStack(params);
}

//Send Create Or Edit Connection
StiMobileDesigner.prototype.SendCommandCreateOrEditConnection = function (connectionFormResult) {
    var params = {};
    params.command = "CreateOrEditConnection";
    params.connectionFormResult = connectionFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Connection
StiMobileDesigner.prototype.SendCommandDeleteConnection = function (selectedItem) {
    var params = {};
    params.command = "DeleteConnection";
    params.connectionName = selectedItem.itemObject.name;
    params.dataSourceNames = selectedItem.getChildNames();
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Or Edit Relation
StiMobileDesigner.prototype.SendCommandCreateOrEditRelation = function (relationFormResult) {
    var params = {};
    params.command = "CreateOrEditRelation";
    params.relationFormResult = relationFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Relation
StiMobileDesigner.prototype.SendCommandDeleteRelation = function (selectedItem) {
    var params = {};
    params.command = "DeleteRelation";
    params.relationNameInSource = selectedItem.itemObject.nameInSource;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Or Edit Column
StiMobileDesigner.prototype.SendCommandCreateOrEditColumn = function (columnFormResult) {
    var params = {};
    params.command = "CreateOrEditColumn";
    params.columnFormResult = columnFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Column
StiMobileDesigner.prototype.SendCommandDeleteColumn = function (selectedItem) {
    var params = {};
    params.command = "DeleteColumn";
    params.columnName = selectedItem.itemObject.name;
    var columnParent = this.options.dictionaryTree.getCurrentColumnParent();
    params.currentParentType = columnParent.type;
    params.currentParentName = (columnParent.type == "BusinessObject") ? selectedItem.getBusinessObjectFullName() : columnParent.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Or Edit Parameter
StiMobileDesigner.prototype.SendCommandCreateOrEditParameter = function (parameterFormResult) {
    var params = {};
    params.command = "CreateOrEditParameter";
    params.parameterFormResult = parameterFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Parameter
StiMobileDesigner.prototype.SendCommandDeleteParameter = function (selectedItem) {
    var params = {};
    params.command = "DeleteParameter";
    params.parameterName = selectedItem.itemObject.name;
    var parameterParent = this.options.dictionaryTree.getCurrentColumnParent();
    params.currentParentName = parameterParent.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Or Edit Data Source
StiMobileDesigner.prototype.SendCommandCreateOrEditDataSource = function (dataSourceFormResult) {
    var params = {};
    params.command = "CreateOrEditDataSource";
    params.dataSourceFormResult = dataSourceFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete DataSource
StiMobileDesigner.prototype.SendCommandDeleteDataSource = function (selectedItem) {
    var params = {};
    params.command = "DeleteDataSource";
    params.dataSourceName = selectedItem.itemObject.name;
    params.dataSourceNameInSource = selectedItem.itemObject.nameInSource;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Or Edit BusinessObject
StiMobileDesigner.prototype.SendCommandCreateOrEditBusinessObject = function (businessObjectFormResult) {
    var params = {};
    params.command = "CreateOrEditBusinessObject";
    params.businessObjectFormResult = businessObjectFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete BusinessObject
StiMobileDesigner.prototype.SendCommandDeleteBusinessObject = function (selectedItem) {
    var params = {};
    params.command = "DeleteBusinessObject";
    params.businessObjectFullName = selectedItem.getBusinessObjectFullName();
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Get All Connections
StiMobileDesigner.prototype.SendCommandGetAllConnections = function (typeDataAdapter) {
    var params = {};
    params.command = "GetAllConnections";
    params.typeDataAdapter = typeDataAdapter;
    this.AddCommandToStack(params);
}

//Send RetrieveColumns
StiMobileDesigner.prototype.SendCommandRetrieveColumns = function (params) {
    params.command = "RetrieveColumns";
    this.AddCommandToStack(params);
}

//Send Delete Category
StiMobileDesigner.prototype.SendCommandDeleteCategory = function (selectedItem) {
    var params = {};
    params.command = selectedItem.parent.itemObject.typeItem == "VariablesMainItem"
        ? "DeleteVariablesCategory"
        : "DeleteBusinessObjectCategory";
    params.categoryName = selectedItem.itemObject.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Edit Category
StiMobileDesigner.prototype.SendCommandEditCategory = function (categoryFormResult) {
    var params = {};
    params.command = this.options.dictionaryTree.selectedItem.parent.itemObject.typeItem == "VariablesMainItem"
        ? "EditVariablesCategory"
        : "EditBusinessObjectCategory";
    params.categoryFormResult = categoryFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Update Cache
StiMobileDesigner.prototype.SendCommandUpdateCache = function () {
    var jsObject = this;
    if (this.options.allowAutoUpdateCache) {
        this.options.timerUpdateCache = setTimeout(function () {
            jsObject.SendCommandUpdateCache();
        }, this.options.timeUpdateCache);

        var params = {};
        params.command = "UpdateCache";
        this.AddCommandToStack(params);
    }
}

//Send Create Or Edit Variable
StiMobileDesigner.prototype.SendCommandCreateOrEditVariable = function (variableFormResult) {
    var params = {};
    params.command = "CreateOrEditVariable";
    params.variableFormResult = variableFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Variable
StiMobileDesigner.prototype.SendCommandDeleteVariable = function (selectedItem) {
    var params = {};
    params.command = "DeleteVariable";
    params.variableName = selectedItem.itemObject.name;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Create Variables Category
StiMobileDesigner.prototype.SendCommandCreateVariablesCategory = function (categoryFormResult) {
    var params = {};
    params.command = "CreateVariablesCategory";
    params.categoryFormResult = categoryFormResult;
    this.options.reportIsModified = true;    
    this.AddCommandToStack(params);
}

//Send Create Or Edit Resource
StiMobileDesigner.prototype.SendCommandCreateOrEditResource = function (resourceFormResult) {
    var params = {};
    params.command = "CreateOrEditResource";
    params.resourceFormResult = resourceFormResult;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Delete Resource
StiMobileDesigner.prototype.SendCommandDeleteResource = function (selectedItem, ignoreCheckUsed) {
    var params = {};
    params.command = "DeleteResource";
    params.resourceName = selectedItem.itemObject.name;
    if (ignoreCheckUsed) params.ignoreCheckUsed = true;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Update Styles
StiMobileDesigner.prototype.SendCommandUpdateStyles = function (stylesCollection, collectionName) {
    this.options.reportIsModified = true;
    this.options.stylesIsModified = true;
    var params = {};    
    params.command = "UpdateStyles";
    if (stylesCollection) params.stylesCollection = stylesCollection;
    if (collectionName) params.collectionName = collectionName;
    params.zoom = this.options.report.zoom.toString();
    if (this.options.selectedObject) params.selectedObjectName = this.options.selectedObject.properties.name;
    this.AddCommandToStack(params);
}

//Send Add Style
StiMobileDesigner.prototype.SendCommandAddStyle = function (type) {
    this.options.reportIsModified = true;
    var params = {};
    params.command = "AddStyle";
    params.type = type;
    this.AddCommandToStack(params);
}

//Send Create Style Collection
StiMobileDesigner.prototype.SendCommandCreateStyleCollection = function (styleCollectionProperties) {
    this.options.reportIsModified = true;
    var params = {};
    params.command = "CreateStyleCollection";
    params.styleCollectionProperties = styleCollectionProperties;
    this.AddCommandToStack(params);
}

//Send Get Report From Data
StiMobileDesigner.prototype.SendCommandGetReportFromData = function (params) {    
    params.command = "GetReportFromData";
    this.AddCommandToStack(params);
}

//Send Item Resource Save
StiMobileDesigner.prototype.SendCommandItemResourceSave = function (itemKey, customMessage) {
    var params = {
        command: "ItemResourceSave",
        reportTemplateItemKey: itemKey,
        sessionKey: this.options.cloudParameters.sessionKey
    }
    if (customMessage) params.customMessage = customMessage;
    if (this.options.isOnlineVersion) params.isOnlineVersion = true;
    this.AddCommandToStack(params);
}

//Send Clone Item Resource Save
StiMobileDesigner.prototype.SendCommandCloneItemResourceSave = function (params) {
    params.command = "CloneItemResourceSave";
    
    this.AddCommandToStack(params);
}

//Send Clone Chart Component
StiMobileDesigner.prototype.SendCommandStartEditChartComponent = function (componentName) {    
    var params = {};
    params.componentName = componentName;
    params.command = "StartEditChartComponent";
    this.AddCommandToStack(params);
}

//Send Clone Map Component
StiMobileDesigner.prototype.SendCommandStartEditMapComponent = function (componentName) {
    var params = {
        command: "StartEditMapComponent",
        componentName: componentName,
        zoom: this.options.report.zoom.toString()
    }
    this.AddCommandToStack(params);
}

// Send Canceled Edit Component
StiMobileDesigner.prototype.SendCommandCanceledEditComponent = function (componentName) {
    var params = {};
    params.componentName = componentName;
    params.command = "CanceledEditComponent";
    this.AddCommandToStack(params);
}

// Send Add Series
StiMobileDesigner.prototype.SendCommandAddSeries = function (params) {
    params.command = "AddSeries";
    this.AddCommandToStack(params);
}

// Send Remove Series
StiMobileDesigner.prototype.SendCommandRemoveSeries = function (params) {
    params.command = "RemoveSeries";
    this.AddCommandToStack(params);
}

// Send Series Move
StiMobileDesigner.prototype.SendCommandSeriesMove = function (params) {
    params.command = "SeriesMove";
    this.AddCommandToStack(params);
}

// Get Labels Content
StiMobileDesigner.prototype.SendCommandGetLabelsContent = function (params) {
    params.command = "GetLabelsContent";
    this.AddCommandToStack(params);
}

// Get Styles Content
StiMobileDesigner.prototype.SendCommandGetStylesContent = function (params) {
    params.command = "GetStylesContent";
    this.AddCommandToStack(params);
}

// Set Labels Type
StiMobileDesigner.prototype.SendCommandSetLabelsType = function (params) {
    params.command = "SetLabelsType";
    this.AddCommandToStack(params);
}

// Set Chart Style
StiMobileDesigner.prototype.SendCommandSetChartStyle = function (params) {
    params.command = "SetChartStyle";
    this.AddCommandToStack(params);
}

// Set Chart Property Value
StiMobileDesigner.prototype.SendCommandSetChartPropertyValue = function (params) {
    params.command = "SetChartPropertyValue";
    this.AddCommandToStack(params);
}

// Send Add ConstantLine Or Strip
StiMobileDesigner.prototype.SendCommandAddConstantLineOrStrip = function (params) {
    params.command = "AddConstantLineOrStrip";
    this.AddCommandToStack(params);
}

// Send Remove ConstantLine Or Strip
StiMobileDesigner.prototype.SendCommandRemoveConstantLineOrStrip = function (params) {
    params.command = "RemoveConstantLineOrStrip";
    this.AddCommandToStack(params);
}

// Send ConstantLine Or Strip Move
StiMobileDesigner.prototype.SendCommandConstantLineOrStripMove = function (params) {
    params.command = "ConstantLineOrStripMove";
    this.AddCommandToStack(params);
}

// Send Container Value
StiMobileDesigner.prototype.SendCommandSendContainerValue = function (params) {
    params.command = "SendContainerValue";
    this.AddCommandToStack(params);
}

// Get Database Data
StiMobileDesigner.prototype.SendCommandGetDatabaseData = function (databaseName) {
    var params = {
        command : "GetDatabaseData",
        databaseName : databaseName
    }
    this.AddCommandToStack(params);
} 

// Apply Selected Data
StiMobileDesigner.prototype.SendCommandApplySelectedData = function (data, databaseName) {
    var params = {
        command: "ApplySelectedData",
        data: data,
        databaseName: databaseName
    }
    this.AddCommandToStack(params);
}

// Create Text Component
StiMobileDesigner.prototype.SendCommandCreateTextComponent = function (itemObject, point, pageName) {
    this.options.reportIsModified = true;
    var params = {
        command: "CreateTextComponent",
        itemObject: itemObject,
        pageName: pageName,
        zoom: this.options.report.zoom.toString(),
        point: point
    }

    if (this.options.dictionaryPanel) {
        params.createLabel = this.options.menus.dictionarySettingsMenu.controls.createLabel.isChecked;
    }

    if (this.options.report.info.useLastFormat && this.options.lastStyleProperties) {
        params.lastStyleProperties = this.options.lastStyleProperties;
    }

    this.AddCommandToStack(params);
}

// Create Data Component
StiMobileDesigner.prototype.SendCommandCreateDataComponent = function (params) {
    this.options.reportIsModified = true;
    params.command = "CreateDataComponent";
    params.zoom = this.options.report.zoom.toString();
    this.AddCommandToStack(params);
}

//Send Report Properties
StiMobileDesigner.prototype.SendCommandSetReportProperties = function (propertiesNames) {
    var properties = {};
    var report = this.options.report;
    if (!report) return;

    for (var i = 0; i < propertiesNames.length; i++) {
        if (report.properties[propertiesNames[i]] != null) {
            properties[propertiesNames[i]] = report.properties[propertiesNames[i]];
        }
    }

    var params = {};
    params.command = "SetReportProperties";
    params.properties = properties;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Page Move
StiMobileDesigner.prototype.SendCommandPageMove = function (direction, pageIndex) {
    var params = {};
    params.command = "PageMove";
    params.direction = direction;
    params.pageIndex = pageIndex;
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Command Test Connection
StiMobileDesigner.prototype.SendCommandTestConnection = function (typeConnection, connectionString) {
    var params = {};
    params.command = "TestConnection";
    params.typeConnection = typeConnection;
    params.connectionString = connectionString;
    this.AddCommandToStack(params);
}

//Send Command Run Query Script
StiMobileDesigner.prototype.SendCommandRunQueryScript = function (params) {
    params.command = "RunQueryScript";
    this.AddCommandToStack(params);
}

//Send Command View Data
StiMobileDesigner.prototype.SendCommandViewData = function (params) {
    params.command = "ViewData";
    this.AddCommandToStack(params);
}

//Send Command View Data
StiMobileDesigner.prototype.SendCommandApplyDesignerOptions = function (designerOptions) {
    var params = {
        command: "ApplyDesignerOptions",
        designerOptions: designerOptions,
        zoom: this.options.report.zoom.toString()
    }
    this.SetCookie("StimulsoftMobileDesignerOptions", Base64.encode(JSON.stringify(designerOptions)));
    this.AddCommandToStack(params);
}

//Send Command Get Sql Parameter Types
StiMobileDesigner.prototype.SendCommandGetSqlParameterTypes = function (dataSource) {
    var params = {
        command: "GetSqlParameterTypes",
        dataSource: dataSource
    }
    this.AddCommandToStack(params);
}

//Send Command Align To Grid
StiMobileDesigner.prototype.SendCommandAlignToGridComponents = function () {
    var params = {};
    params.command = "AlignToGridComponents";
    params.components = [];
    params.pageName = this.options.currentPage.properties.name;

    var components = [];
    if (this.options.selectedObjects) components = this.options.selectedObjects;
    else if (this.options.selectedObject) components.push(this.options.selectedObject);

    for (var i = 0; i < components.length; i++) {
        params.components.push(components[i].properties.name);
    }
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Send Command Change Arrange Components
StiMobileDesigner.prototype.SendCommandChangeArrangeComponents = function (arrangeCommand) {
    var params = {};
    params.command = "ChangeArrangeComponents";
    params.arrangeCommand = arrangeCommand;
    params.components = [];
    params.pageName = this.options.currentPage.properties.name;

    var components = [];
    if (this.options.selectedObjects) components = this.options.selectedObjects;
    else if (this.options.selectedObject) components.push(this.options.selectedObject);

    for (var i = 0; i < components.length; i++) {
        params.components.push(components[i].properties.name);
    }
    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Update Sample Text Format
StiMobileDesigner.prototype.SendCommandUpdateSampleTextFormat = function (textFormat) {
    var params = {};
    params.command = "UpdateSampleTextFormat";
    params.textFormat = textFormat;

    this.AddCommandToStack(params);
}

//Send Clone CrossTab Component
StiMobileDesigner.prototype.SendCommandStartEditCrossTabComponent = function (componentName) {
    var params = {};
    params.componentName = componentName;
    params.command = "StartEditCrossTabComponent";
    this.AddCommandToStack(params);
}

//Send Update CrossTab Component
StiMobileDesigner.prototype.SendCommandUpdateCrossTabComponent = function (componentName, updateParameters) {
    var params = {};
    params.componentName = componentName;
    params.updateParameters = updateParameters;
    params.command = "UpdateCrossTabComponent";
    this.AddCommandToStack(params);
}

//Send Get CrossTab Color Styles
StiMobileDesigner.prototype.SendCommandGetCrossTabColorStyles = function () {
    var params = {};
    params.command = "GetCrossTabColorStyles";
    this.AddCommandToStack(params);
}

//Send Duplicate Page
StiMobileDesigner.prototype.SendCommandDuplicatePage = function (pageIndex) {
    var params = {};
    params.command = "DuplicatePage";
    params.pageIndex = pageIndex;
    this.AddCommandToStack(params);
}

//Send Set Event Value
StiMobileDesigner.prototype.SendCommandSetEventValue = function (components, eventName, eventValue) {
    var params = {};
    params.command = "SetEventValue";
    params.components = components;
    params.eventValue = eventValue;
    params.eventName = eventName;
    this.AddCommandToStack(params);
}

//Get CrossTab Styles Content
StiMobileDesigner.prototype.SendCommandGetCrossTabStylesContent = function (callbackFunction) {
    var params = {};
    params.command = "GetCrossTabStylesContent";
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Get Gauge Styles Content
StiMobileDesigner.prototype.SendCommandGetGaugeStylesContent = function (callbackFunction) {
    var params = {};
    params.command = "GetGaugeStylesContent";
    params.componentName = this.options.selectedObject ? this.options.selectedObject.properties.name : this.options.selectedObjects[0].properties.name;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Get Map Styles Content
StiMobileDesigner.prototype.SendCommandGetMapStylesContent = function (callbackFunction) {
    var params = {};
    params.command = "GetMapStylesContent";
    params.componentName = this.options.selectedObject ? this.options.selectedObject.properties.name : this.options.selectedObjects[0].properties.name;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Get Table Styles Content
StiMobileDesigner.prototype.SendCommandGetTableStylesContent = function (callbackFunction) {
    var params = {};
    params.command = "GetTableStylesContent";
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Get Dashboard Styles Content
StiMobileDesigner.prototype.SendCommandGetDashboardStylesContent = function (callbackFunction) {
    var params = {};
    params.command = "GetDashboardStylesContent";
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Change Table
StiMobileDesigner.prototype.SendCommandChangeTableComponent = function (changeParameters) {
    var selectedObjects = this.options.selectedObject ? [this.options.selectedObject] : this.options.selectedObjects;
    if (!selectedObjects) return;

    changeParameters.cells = [];
    for (var i = 0; i < selectedObjects.length; i++) {
        changeParameters.cells.push(selectedObjects[i].properties.name);
    }

    var params = {
        command: "ChangeTableComponent",
        tableName: selectedObjects[0].typeComponent == "StiTable" ? selectedObjects[0].properties.name : selectedObjects[0].properties.parentName,
        zoom: this.options.report.zoom.toString(),
        changeParameters: changeParameters
    };

    this.AddCommandToStack(params);
}

//Send Open Style
StiMobileDesigner.prototype.SendCommandOpenStyle = function (fileContent, fileName) {
    var params = {
        command: "OpenStyle"
    };
    if (fileContent) params.base64Data = fileContent.substr(fileContent.indexOf("base64,") + 7);
    this.AddCommandToStack(params);
}

//Send Save Style
StiMobileDesigner.prototype.SendCommandSaveStyle = function (stylesCollection) {
    this.options.ignoreBeforeUnload = true;
    if (this.options.mvcMode) {
        var params = {
            command: "DownloadStyles",
            stylesCollection: JSON.stringify(stylesCollection)
        };
        this.AddMainParameters(params);
        this.PostForm(this.options.requestUrl.replace("{action}", this.options.actionDesignerEvent), params);
    }
    else {
        this.PostForm({ command: "DownloadStyles", reportGuid: this.options.reportGuid, stylesCollection: JSON.stringify(stylesCollection) });
    }
    var jsObject = this;
    setTimeout(function () { jsObject.options.ignoreBeforeUnload = false; }, 500);
}

//Send Create Style From Component
StiMobileDesigner.prototype.SendCommandCreateStylesFromComponents = function (componentsNames) {
    var params = {};
    params.command = "CreateStylesFromComponents";
    params.componentsNames = componentsNames;
    this.AddCommandToStack(params);
}

//Send Change Size Components
StiMobileDesigner.prototype.SendCommandChangeSizeComponents = function (actionName) {
    var params = {};
    params.command = "ChangeSizeComponents";
    params.actionName = actionName;    
    params.pageName = this.options.currentPage.properties.name;
    params.zoom = this.options.report.zoom.toString();
    params.components = [];

    var components = [];
    if (this.options.selectedObjects) components = this.options.selectedObjects;
    else if (this.options.selectedObject) components.push(this.options.selectedObject);

    for (var i = 0; i < components.length; i++) {
        params.components.push(components[i].properties.name);
    }

    this.AddCommandToStack(params);
}

//Send Delete Column
StiMobileDesigner.prototype.SendCommandCreateFieldOnDblClick = function (selectedItem) {
    var params = {};
    params.command = "CreateFieldOnDblClick";
    params.pageName = this.options.currentPage.properties.name;
    params.fullName = selectedItem.getResultForEditForm();
    params.selectedComponents = [];
    params.zoom = this.options.report.zoom.toString();

    var selectedComponents = [];
    if (this.options.selectedObjects) selectedComponents = this.options.selectedObjects;
    else if (this.options.selectedObject) selectedComponents.push(this.options.selectedObject);

    for (var i = 0; i < selectedComponents.length; i++) {
        params.selectedComponents.push(selectedComponents[i].properties.name);
    }

    if (this.options.report.info.useLastFormat && this.options.lastStyleProperties) {
        params.lastStyleProperties = this.options.lastStyleProperties;
    }

    if (selectedItem.itemObject.typeItem == "Column") {
        params.columnName = selectedItem.itemObject.name;
        var columnParent = this.options.dictionaryTree.getCurrentColumnParent();
        params.currentParentType = columnParent.type;
        params.currentParentName = (columnParent.type == "BusinessObject") ? selectedItem.getBusinessObjectFullName() : columnParent.name;
    }
    else if (selectedItem.itemObject.typeItem == "Parameter") {
        params.parameterName = selectedItem.itemObject.name;
        var parameterParent = this.options.dictionaryTree.getCurrentColumnParent();
        params.currentParentName = parameterParent.name;
    }
    else if (selectedItem.itemObject.typeItem == "Resource") {
        params.resourceName = selectedItem.itemObject.name;
        params.resourceType = selectedItem.itemObject.type;
    }

    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Get Params From QueryString
StiMobileDesigner.prototype.SendCommandGetParamsFromQueryString = function (queryString, dataSourceName, callbackFunction) {
    var params = {};
    params.command = "GetParamsFromQueryString";
    params.queryString = queryString;
    params.dataSourceName = dataSourceName;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Create Moving Copy Component
StiMobileDesigner.prototype.SendCommandCreateMovingCopyComponent = function (componentName, componentRect, isLastCommand, callbackFunction) {
    var params = {};
    params.command = "CreateMovingCopyComponent";
    params.components = [componentName];
    params.pageName = this.options.currentPage.properties.name;
    params.componentRect = componentRect;
    params.zoom = this.options.report.zoom.toString();
    params.isLastCommand = isLastCommand;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.options.reportIsModified = true;
    this.AddCommandToStack(params);
}

//Get Report Check Items
StiMobileDesigner.prototype.SendCommandGetReportCheckItems = function (callbackFunction) {
    var params = {};
    params.command = "GetReportCheckItems";
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Get Check Preview
StiMobileDesigner.prototype.SendCommandGetCheckPreview = function (checkIndex, callbackFunction) {
    var params = {};
    params.command = "GetCheckPreview";
    params.checkIndex = checkIndex;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Action Check
StiMobileDesigner.prototype.SendCommandActionCheck = function (checkIndex, actionIndex, callbackFunction) {
    var params = {};
    params.command = "ActionCheck";
    params.checkIndex = checkIndex;
    params.actionIndex = actionIndex;
    params.zoom = this.options.report.zoom.toString();
    params.selectedObjectName = this.options.selectedObject.properties.name;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Check Expression
StiMobileDesigner.prototype.SendCommandCheckExpression = function (expressionText, callbackFunction) {
    var params = {};
    params.command = "CheckExpression";
    params.expressionText = expressionText;
    params.componentName = this.options.selectedObject ? this.options.selectedObject.properties.name : null;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Get Globalization Strings
StiMobileDesigner.prototype.SendCommandGetGlobalizationStrings = function (callbackFunction) {
    var params = {};
    params.command = "GetGlobalizationStrings";    
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Add Globalization Strings
StiMobileDesigner.prototype.SendCommandAddGlobalizationStrings = function (cultureName, callbackFunction) {
    var params = {};
    params.command = "AddGlobalizationStrings";
    params.callbackFunctionId = this.generateKey();
    params.cultureName = cultureName;
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Remove Globalization Strings
StiMobileDesigner.prototype.SendCommandRemoveGlobalizationStrings = function (index, callbackFunction) {
    var params = {};
    params.command = "RemoveGlobalizationStrings";
    params.callbackFunctionId = this.generateKey();
    params.index = index;
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Get Culture Settings From Report
StiMobileDesigner.prototype.GetCultureSettingsFromReport = function (index, callbackFunction) {
    var params = {};
    params.command = "GetCultureSettingsFromReport";
    params.callbackFunctionId = this.generateKey();
    params.index = index;
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Set Culture Settings To Report
StiMobileDesigner.prototype.SetCultureSettingsToReport = function (cultureName, callbackFunction) {
    var params = {};
    params.command = "SetCultureSettingsToReport";    
    params.callbackFunctionId = this.generateKey();
    params.cultureName = cultureName;
    params.zoom = this.options.report.zoom.toString();
    params.selectedObjectName = this.options.selectedObject.properties.name;
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Apply Globalization Strings
StiMobileDesigner.prototype.SendCommandApplyGlobalizationStrings = function (index, propertyName, propertyValue) {
    var params = {};
    params.command = "ApplyGlobalizationStrings";
    params.index = index;
    params.propertyName = propertyName;
    params.propertyValue = propertyValue;
    this.AddCommandToStack(params);
}

//Send Clone Chart Component
StiMobileDesigner.prototype.SendCommandStartEditGaugeComponent = function (componentName) {
    var params = {};
    params.componentName = componentName;
    params.command = "StartEditGaugeComponent";
    this.AddCommandToStack(params);
}

//Get Resource Content
StiMobileDesigner.prototype.SendCommandGetResourceContent = function (resourceName, callbackFunction) {
    var params = {};
    params.command = "GetResourceContent";
    params.resourceName = resourceName;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Convert Resource Content
StiMobileDesigner.prototype.SendCommandConvertResourceContent = function (content, type, callbackFunction) {
    var params = {
        command: "ConvertResourceContent",
        type: type
    };
    params.callbackFunctionId = this.generateKey();
    if (content) params.base64Data = content.substr(content.indexOf("base64,") + 7);
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Get Resource Text
StiMobileDesigner.prototype.SendCommandGetResourceText = function (resourceName, callbackFunction) {
    var params = {};
    params.command = "GetResourceText";
    params.resourceName = resourceName;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Set Resource Text
StiMobileDesigner.prototype.SendCommandSetResourceText = function (resourceName, resourceText) {
    var params = {};
    params.command = "SetResourceText";
    params.resourceName = resourceName;
    params.resourceText = resourceText;

    this.AddCommandToStack(params);
}

//Get Resource View Data
StiMobileDesigner.prototype.SendCommandGetResourceViewData = function (resourceName, resourceType, resourceContent, callbackFunction) {
    var params = {};
    params.command = "GetResourceViewData";
    params.resourceName = resourceName;
    params.resourceType = resourceType;
    if (resourceContent) params.resourceContent = resourceContent;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    this.AddCommandToStack(params);
}

//Send Update Gauge Component
StiMobileDesigner.prototype.SendCommandUpdateGaugeComponent = function (componentName, updateParameters) {
    var params = {};
    params.componentName = componentName;
    params.updateParameters = updateParameters;
    params.command = "UpdateGaugeComponent";
    this.AddCommandToStack(params);
}

//Create Component From Resource
StiMobileDesigner.prototype.SendCommandCreateComponentFromResource = function (itemObject, point, pageName) {
    this.options.reportIsModified = true;
    var params = {
        command: "CreateComponentFromResource",
        itemObject: itemObject,
        pageName: pageName,
        zoom: this.options.report.zoom.toString(),
        point: point
    }

    if (this.options.report.info.useLastFormat && this.options.lastStyleProperties) {
        params.lastStyleProperties = this.options.lastStyleProperties;
    }

    this.AddCommandToStack(params);
}

//Get Sample Connection String
StiMobileDesigner.prototype.SendCommandGetSampleConnectionString = function (typeConnection, callbackFunction) {
    var params = {};
    params.command = "GetSampleConnectionString";
    params.typeConnection = typeConnection;
    params.callbackFunctionId = this.generateKey();
    this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;

    this.AddCommandToStack(params);
}

//Create Data Item From Resource
StiMobileDesigner.prototype.SendCommandCreateDatabaseFromResource = function (resourceObject) {
    var params = {};
    params.command = "CreateDatabaseFromResource";
    params.resourceObject = resourceObject;

    this.AddCommandToStack(params);
}

//Send Clone BarCode Component
StiMobileDesigner.prototype.SendCommandStartEditBarCodeComponent = function (componentName) {
    var params = {};
    params.componentName = componentName;
    params.command = "StartEditBarCodeComponent";
    this.AddCommandToStack(params);
}

//Move Dictionary Item
StiMobileDesigner.prototype.SendCommandMoveDictionaryItem = function (fromObject, toObject, direction) {
    var params = {};
    params.command = "MoveDictionaryItem";
    params.direction = direction;
    params.fromObject = fromObject;
    params.toObject = toObject;

    if (fromObject.typeItem == "Column") {
        var columnParent = this.options.dictionaryTree.getCurrentColumnParent();
        params.currentParentType = columnParent.type;
        params.currentParentName = (columnParent.type == "BusinessObject") ? this.options.dictionaryTree.selectedItem.getBusinessObjectFullName() : columnParent.name;
    }

    this.AddCommandToStack(params);
}


StiMobileDesigner.prototype.SendCommandUpdateReportAliases = function () {
    var params = {};
    params.command = "UpdateReportAliases";
    params.zoom = this.options.report.zoom.toString();
    params.selectedObjectName = this.options.selectedObject ? this.options.selectedObject.properties.name : null;

    this.AddCommandToStack(params);
}

//Send Any Command
StiMobileDesigner.prototype.SendCommandToDesignerServer = function (commandName, parameters, callbackFunction) {
    var params = {};
    params.command = commandName;

    if (callbackFunction) {
        params.callbackFunctionId = this.generateKey();
        this.options.callbackFunctions[params.callbackFunctionId] = callbackFunction;
    }

    if (parameters) {
        for (var key in parameters) {
            params[key] = parameters[key];
        }
    }

    this.AddCommandToStack(params);
}

//Move Connection Data To Resource
StiMobileDesigner.prototype.SendCommandMoveConnectionDataToResource = function (editConnectionForm) {
    var params = {};
    params.command = "MoveConnectionDataToResource";
    params.pathSchema = Base64.encode(editConnectionForm.pathSchemaControl.textBox.value);
    params.pathData = Base64.encode(editConnectionForm.pathDataControl.textBox.value);
    params.typeConnection = editConnectionForm.connection.typeConnection;
    params.name = editConnectionForm.nameControl.value;
    params.alias = editConnectionForm.aliasControl.value;
    params.codePageCsv = editConnectionForm.codePageCsvControl.key;
    params.codePageDbase = editConnectionForm.codePageDbaseControl.key;
    params.separator = editConnectionForm.getCsvSeparatorText(editConnectionForm.csvSeparatorControl.key);
    if (editConnectionForm.mode == "Edit") params.oldName = editConnectionForm.connection.name;

    this.AddCommandToStack(params);
}

//Move Connection Data To Resource
StiMobileDesigner.prototype.SendCommandMoveConnectionDataToResource = function (editConnectionForm) {
    var params = {};
    params.command = "MoveConnectionDataToResource";
    params.pathSchema = Base64.encode(editConnectionForm.pathSchemaControl.textBox.value);
    params.pathData = Base64.encode(editConnectionForm.pathDataControl.textBox.value);
    params.typeConnection = editConnectionForm.connection.typeConnection;
    params.name = editConnectionForm.nameControl.value;
    params.alias = editConnectionForm.aliasControl.value;
    params.codePageCsv = editConnectionForm.codePageCsvControl.key;
    params.codePageDbase = editConnectionForm.codePageDbaseControl.key;
    params.separator = editConnectionForm.getCsvSeparatorText(editConnectionForm.csvSeparatorControl.key);
    if (editConnectionForm.mode == "Edit") params.oldName = editConnectionForm.connection.name;

    this.AddCommandToStack(params);
}

//Set Map Properties
StiMobileDesigner.prototype.SendCommandSetMapProperties = function (componentName, properties, updateMapData) {
    var params = {
        command: "SetMapProperties",
        componentName: componentName,
        properties: properties,
        zoom: this.options.report.zoom.toString()
    }

    if (updateMapData) params.updateMapData = true;

    this.AddCommandToStack(params);
}

//Update Map Data
StiMobileDesigner.prototype.SendCommandUpdateMapData = function (componentName, rowIndex, columnName, textValue) {
    var params = {
        command: "UpdateMapData",
        componentName: componentName,
        rowIndex: rowIndex,
        columnName: columnName,
        textValue: textValue,
        zoom: this.options.report.zoom.toString()
    }

    this.AddCommandToStack(params);
}

//Send Open Page
StiMobileDesigner.prototype.SendCommandOpenPage = function (fileContent, fileName) {
    var params = {
        command: "OpenPage"
    };
    if (fileContent) params.base64Data = fileContent.substr(fileContent.indexOf("base64,") + 7);
    this.AddCommandToStack(params);
}

//Send Save Page
StiMobileDesigner.prototype.SendCommandSavePage = function (pageIndex) {
    this.options.ignoreBeforeUnload = true;
    if (this.options.mvcMode) {
        var params = {
            command: "DownloadPage",
            pageIndex: pageIndex
        };
        this.AddMainParameters(params);
        this.PostForm(this.options.requestUrl.replace("{action}", this.options.actionDesignerEvent), params);
    }
    else {
        this.PostForm({ command: "DownloadPage", reportGuid: this.options.reportGuid, pageIndex: pageIndex });
    }
    var jsObject = this;
    setTimeout(function () { jsObject.options.ignoreBeforeUnload = false; }, 500);
}

// Create Text Element
StiMobileDesigner.prototype.SendCommandCreateTextElement = function (itemObject, point, pageName) {
    this.options.reportIsModified = true;
    var params = {
        command: "CreateTextElement",
        itemObject: itemObject,
        pageName: pageName,
        zoom: this.options.report.zoom.toString(),
        point: point
    }

    if (this.options.report.info.useLastFormat && this.options.lastStyleProperties) {
        params.lastStyleProperties = this.options.lastStyleProperties;
    }

    this.AddCommandToStack(params);
}

// Create Table Element
StiMobileDesigner.prototype.SendCommandCreateTableElement = function (draggedItem, point, pageName) {
    this.options.reportIsModified = true;
    var params = {
        command: "CreateTableElement",
        draggedItem: draggedItem,
        pageName: pageName,
        zoom: this.options.report.zoom.toString(),
        point: point
    }

    if (this.options.report.info.useLastFormat && this.options.lastStyleProperties) {
        params.lastStyleProperties = this.options.lastStyleProperties;
    }

    this.AddCommandToStack(params);
}