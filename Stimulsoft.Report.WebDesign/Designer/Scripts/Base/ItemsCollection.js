
StiMobileDesigner.prototype.GetPageOrientationItems = function (showImage) {    
    var items = [];
    items.push(this.Item("portrait", this.loc.FormPageSetup.PageOrientationPortrait, showImage ? "Portrait.png" : null, "Portrait"));
    items.push(this.Item("landscape", this.loc.FormPageSetup.PageOrientationLandscape, showImage ? "Landscape.png" : null, "Landscape"));
    
    return items;
}

StiMobileDesigner.prototype.GetUnitItems = function (showImage) {
    var items = [];
    items.push(this.Item("cm", this.loc.PropertyEnum.StiReportUnitTypeCentimeters, null, "cm"));
    items.push(this.Item("hi", this.loc.PropertyEnum.StiReportUnitTypeHundredthsOfInch, null, "hi"));
    items.push(this.Item("in", this.loc.PropertyEnum.StiReportUnitTypeInches, null, "in"));
    items.push(this.Item("mm", this.loc.PropertyEnum.StiReportUnitTypeMillimeters, null, "mm"));

    return items;
}

StiMobileDesigner.prototype.GetFilterIngineItems = function () {    
    var items = [];
    items.push(this.Item("ReportEngine", this.loc.PropertyEnum.StiFilterEngineReportEngine, null, "ReportEngine"));
    items.push(this.Item("SQLQuery", this.loc.PropertyEnum.StiFilterEngineSQLQuery, null, "SQLQuery"));
    
    return items;
}  

StiMobileDesigner.prototype.GetBorderStyleItems = function () {    
    var items = [];
    items.push(this.Item("borderStyleSolid", this.loc.PropertyEnum.StiPenStyleSolid, "BorderStyleSolid.png", "0"));
    items.push(this.Item("borderStyleDash", this.loc.PropertyEnum.StiPenStyleDash, "BorderStyleDash.png", "1"));
    items.push(this.Item("borderStyleDashDot", this.loc.PropertyEnum.StiPenStyleDashDot, "BorderStyleDashDot.png", "2"));
    items.push(this.Item("borderStyleDashDotDot", this.loc.PropertyEnum.StiPenStyleDashDotDot, "BorderStyleDashDotDot.png", "3"));
    items.push(this.Item("borderStyleDot", this.loc.PropertyEnum.StiPenStyleDot, "BorderStyleDot.png", "4"));
    items.push(this.Item("borderStyleDouble", this.loc.PropertyEnum.StiPenStyleDouble, "BorderStyleDouble.png", "5"));
    items.push(this.Item("borderStyleNone", this.loc.PropertyEnum.StiPenStyleNone, "BorderStyleNone.png", "6"));
    
    return items;
}

StiMobileDesigner.prototype.GetImageRotationItems = function () {    
    var items = [];
    items.push(this.Item("itemNone", this.loc.PropertyEnum.StiImageRotationNone, null, "None"));
    items.push(this.Item("itemRotate90CW", this.loc.PropertyEnum.StiImageRotationRotate90CW, null, "Rotate90CW"));
    items.push(this.Item("itemRotate90CCW", this.loc.PropertyEnum.StiImageRotationRotate90CCW, null, "Rotate90CCW"));
    items.push(this.Item("itemRotate180", this.loc.PropertyEnum.StiImageRotationRotate180, null, "Rotate180"));
    items.push(this.Item("itemFlipHorizontal", this.loc.PropertyEnum.StiImageRotationFlipHorizontal, null, "FlipHorizontal"));
    items.push(this.Item("itemFlipVertical", this.loc.PropertyEnum.StiImageRotationFlipVertical, null, "FlipVertical"));
    
    return items;
}

StiMobileDesigner.prototype.GetBarCodeAngleItems = function () {
    var items = [];
    
    items.push(this.Item("angle0", this.loc.PropertyEnum.StiAngleAngle0, null, "Angle0"));
    items.push(this.Item("angle90", this.loc.PropertyEnum.StiAngleAngle90, null, "Angle90"));
    items.push(this.Item("angle180", this.loc.PropertyEnum.StiAngleAngle180, null, "Angle180"));
    items.push(this.Item("angle270", this.loc.PropertyEnum.StiAngleAngle270, null, "Angle270"));
    
    return items;
}

StiMobileDesigner.prototype.GetTextAngleItems = function () {
    var items = [];
    
    items.push(this.Item("angle0", this.loc.PropertyEnum.StiAngleAngle0, null, "0"));
    items.push(this.Item("angle45", this.loc.PropertyEnum.StiAngleAngle45, null, "45"));
    items.push(this.Item("angle90", this.loc.PropertyEnum.StiAngleAngle90, null, "90"));
    items.push(this.Item("angle180", this.loc.PropertyEnum.StiAngleAngle180, null, "180"));
    items.push(this.Item("angle270", this.loc.PropertyEnum.StiAngleAngle270, null, "270"));
    
    return items;
}

StiMobileDesigner.prototype.GetSortDirectionItems = function () {    
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiSortDirectionAsc, null, "0"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiSortDirectionDesc, null, "1"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiSortDirectionNone, null, "2"));
    
    return items;
}

StiMobileDesigner.prototype.GetPrintOnItems = function () {    
    var items = [];
    items.push(this.Item("itemAllPagesItem", this.loc.PropertyEnum.StiPrintOnTypeAllPages, null, "AllPages"));
    items.push(this.Item("itemExceptFirstPage", this.loc.PropertyEnum.StiPrintOnTypeExceptFirstPage, null, "ExceptFirstPage"));
    items.push(this.Item("itemExceptLastPage", this.loc.PropertyEnum.StiPrintOnTypeExceptLastPage, null, "ExceptLastPage"));
    items.push(this.Item("itemExceptFirstAndLastPage", this.loc.PropertyEnum.StiPrintOnTypeExceptFirstAndLastPage, null, "ExceptFirstAndLastPage"));
    items.push(this.Item("itemOnlyFirstPage", this.loc.PropertyEnum.StiPrintOnTypeOnlyFirstPage, null, "OnlyFirstPage"));
    items.push(this.Item("itemOnlyLastPage", this.loc.PropertyEnum.StiPrintOnTypeOnlyLastPage, null, "OnlyLastPage"));
    items.push(this.Item("itemOnlyFirstAndLastPage", this.loc.PropertyEnum.StiPrintOnTypeOnlyFirstAndLastPage, null, "OnlyFirstAndLastPage"));
    
    return items;
}

StiMobileDesigner.prototype.GetSortDirectionItemsForSortForm = function () {    
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiSortDirectionAsc, null, "ASC"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiSortDirectionDesc, null, "DESC"));
    
    return items;
}

StiMobileDesigner.prototype.GetFilterTypeItems = function () {    
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterModeAnd, null, "And"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterModeOr, null, "Or"));
    
    return items;
}

StiMobileDesigner.prototype.GetFilterFieldIsItems = function () {    
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterItemValue, null, "Value"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterItemExpression, null, "Expression"));
    
    return items;
}

StiMobileDesigner.prototype.GetFilterDataTypeItems = function () {    
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterDataTypeString, null, "String"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterDataTypeNumeric, null, "Numeric"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiFilterDataTypeDateTime, null, "DateTime"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiFilterDataTypeBoolean, null, "Boolean"));
    items.push(this.Item("item4", this.loc.PropertyEnum.StiFilterDataTypeExpression, null, "Expression"));
    
    return items;
}

StiMobileDesigner.prototype.GetFilterConditionItems = function (dataType, isChartFilter) {
    var items = [];
    switch (dataType) {
        case "String":
            {
                items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterConditionEqualTo, null, "EqualTo"));
                items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterConditionNotEqualTo, null, "NotEqualTo"));
                items.push(this.Item("item2", this.loc.PropertyEnum.StiFilterConditionContaining, null, "Containing"));
                items.push(this.Item("item3", this.loc.PropertyEnum.StiFilterConditionNotContaining, null, "NotContaining"));
                items.push(this.Item("item4", this.loc.PropertyEnum.StiFilterConditionBeginningWith, null, "BeginningWith"));
                items.push(this.Item("item5", this.loc.PropertyEnum.StiFilterConditionEndingWith, null, "EndingWith"));
                if (!isChartFilter) {
                    items.push(this.Item("item8", this.loc.PropertyEnum.StiFilterConditionIsNull, null, "IsNull"));
                    items.push(this.Item("item9", this.loc.PropertyEnum.StiFilterConditionIsNotNull, null, "IsNotNull"));
                }
                break;
            }
        case "Numeric":
        case "DateTime":
            {
                items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterConditionEqualTo, null, "EqualTo"));
                items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterConditionNotEqualTo, null, "NotEqualTo"));
                if (!isChartFilter) {
                    items.push(this.Item("item2", this.loc.PropertyEnum.StiFilterConditionBetween, null, "Between"));
                    items.push(this.Item("item3", this.loc.PropertyEnum.StiFilterConditionNotBetween, null, "NotBetween"));
                }
                items.push(this.Item("item4", this.loc.PropertyEnum.StiFilterConditionGreaterThan, null, "GreaterThan"));
                items.push(this.Item("item5", this.loc.PropertyEnum.StiFilterConditionGreaterThanOrEqualTo, null, "GreaterThanOrEqualTo"));
                items.push(this.Item("item6", this.loc.PropertyEnum.StiFilterConditionLessThan, null, "LessThan"));
                items.push(this.Item("item7", this.loc.PropertyEnum.StiFilterConditionLessThanOrEqualTo, null, "LessThanOrEqualTo"));
                if (!isChartFilter) {
                    items.push(this.Item("item8", this.loc.PropertyEnum.StiFilterConditionIsNull, null, "IsNull"));
                    items.push(this.Item("item9", this.loc.PropertyEnum.StiFilterConditionIsNotNull, null, "IsNotNull"));
                }
                break;
            }
        case "Boolean":
            {
                items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterConditionEqualTo, null, "EqualTo"));
                items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterConditionNotEqualTo, null, "NotEqualTo"));
                break;
            }
        case "Expression":
            {
                items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterConditionEqualTo, null, "EqualTo"));
                items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterConditionNotEqualTo, null, "NotEqualTo"));
                items.push(this.Item("item2", this.loc.PropertyEnum.StiFilterConditionBetween, null, "Between"));
                items.push(this.Item("item3", this.loc.PropertyEnum.StiFilterConditionNotBetween, null, "NotBetween"));
                items.push(this.Item("item4", this.loc.PropertyEnum.StiFilterConditionGreaterThan, null, "GreaterThan"));
                items.push(this.Item("item5", this.loc.PropertyEnum.StiFilterConditionGreaterThanOrEqualTo, null, "GreaterThanOrEqualTo"));
                items.push(this.Item("item6", this.loc.PropertyEnum.StiFilterConditionLessThan, null, "LessThan"));
                items.push(this.Item("item7", this.loc.PropertyEnum.StiFilterConditionLessThanOrEqualTo, null, "LessThanOrEqualTo"));
                items.push(this.Item("item8", this.loc.PropertyEnum.StiFilterConditionContaining, null, "Containing"));
                items.push(this.Item("item9", this.loc.PropertyEnum.StiFilterConditionNotContaining, null, "NotContaining"));
                items.push(this.Item("item10", this.loc.PropertyEnum.StiFilterConditionBeginningWith, null, "BeginningWith"));
                items.push(this.Item("item11", this.loc.PropertyEnum.StiFilterConditionEndingWith, null, "EndingWith"));
                items.push(this.Item("item12", this.loc.PropertyEnum.StiFilterConditionIsNull, null, "IsNull"));
                items.push(this.Item("item13", this.loc.PropertyEnum.StiFilterConditionIsNotNull, null, "IsNotNull"));
                break;
            }
    }

    return items;
}

StiMobileDesigner.prototype.GetSummaryTypeItems = function () {    
    var items = [];
    for (var i = 0; i < this.options.summaryTypes.length; i++)
        items.push(this.Item("item" + i, this.options.summaryTypes[i].value, null, this.options.summaryTypes[i].key));
    
    return items;
}

StiMobileDesigner.prototype.GetShapeTypeItems = function () {
    var items = [];
    
    items.push(this.Item("shape0", this.loc.Shapes.Arrow, null, "StiArrowShapeType"));
    items.push(this.Item("shape1", this.loc.Shapes.DiagonalDownLine, null, "StiDiagonalDownLineShapeType"));
    items.push(this.Item("shape2", this.loc.Shapes.DiagonalUpLine, null, "StiDiagonalUpLineShapeType"));
    items.push(this.Item("shape3", this.loc.Shapes.HorizontalLine, null, "StiHorizontalLineShapeType"));
    items.push(this.Item("shape4", this.loc.Shapes.LeftAndRightLine, null, "StiLeftAndRightLineShapeType"));
    items.push(this.Item("shape5", this.loc.Shapes.Oval, null, "StiOvalShapeType"));
    items.push(this.Item("shape6", this.loc.Shapes.Rectangle, null, "StiRectangleShapeType"));
    items.push(this.Item("shape7", this.loc.Shapes.RoundedRectangle, null, "StiRoundedRectangleShapeType"));
    items.push(this.Item("shape8", this.loc.Shapes.TopAndBottomLine, null, "StiTopAndBottomLineShapeType"));
    items.push(this.Item("shape9", this.loc.Shapes.Triangle, null, "StiTriangleShapeType"));
    items.push(this.Item("shape10", this.loc.Shapes.VerticalLine, null, "StiVerticalLineShapeType"));
    items.push(this.Item("shape11", this.loc.Shapes.ComplexArrow, null, "StiComplexArrowShapeType"));
    items.push(this.Item("shape12", this.loc.Shapes.BentArrow, null, "StiBentArrowShapeType"));
    items.push(this.Item("shape13", this.loc.Shapes.Chevron, null, "StiChevronShapeType"));
    items.push(this.Item("shape14", this.loc.Shapes.Division, null, "StiDivisionShapeType"));
    items.push(this.Item("shape15", this.loc.Shapes.Equal, null, "StiEqualShapeType"));
    items.push(this.Item("shape16", this.loc.Shapes.FlowchartCard, null, "StiFlowchartCardShapeType"));
    items.push(this.Item("shape17", this.loc.Shapes.FlowchartCollate, null, "StiFlowchartCollateShapeType"));
    items.push(this.Item("shape18", this.loc.Shapes.FlowchartDecision, null, "StiFlowchartDecisionShapeType"));
    items.push(this.Item("shape19", this.loc.Shapes.FlowchartManualInput, null, "StiFlowchartManualInputShapeType"));
    items.push(this.Item("shape20", this.loc.Shapes.FlowchartOffPageConnector, null, "StiFlowchartOffPageConnectorShapeType"));
    items.push(this.Item("shape21", this.loc.Shapes.FlowchartPreparation, null, "StiFlowchartPreparationShapeType"));
    items.push(this.Item("shape22", this.loc.Shapes.FlowchartSort, null, "StiFlowchartSortShapeType"));
    items.push(this.Item("shape23", this.loc.Shapes.Frame, null, "StiFrameShapeType"));
    items.push(this.Item("shape24", this.loc.Shapes.Minus, null, "StiMinusShapeType"));
    items.push(this.Item("shape25", this.loc.Shapes.Multiply, null, "StiMultiplyShapeType"));
    items.push(this.Item("shape26", this.loc.Shapes.Parallelogram, null, "StiParallelogramShapeType"));
    items.push(this.Item("shape27", this.loc.Shapes.Plus, null, "StiPlusShapeType"));
    items.push(this.Item("shape28", this.loc.Shapes.RegularPentagon, null, "StiRegularPentagonShapeType"));
    items.push(this.Item("shape29", this.loc.Shapes.Octagon, null, "StiOctagonShapeType"));    
    items.push(this.Item("shape30", this.loc.Shapes.Trapezoid, null, "StiTrapezoidShapeType"));
    items.push(this.Item("shape31", this.loc.Shapes.SnipSameSideCornerRectangle, null, "StiSnipSameSideCornerRectangleShapeType"));
    items.push(this.Item("shape32", this.loc.Shapes.SnipDiagonalSideCornerRectangle, null, "StiSnipDiagonalSideCornerRectangleShapeType"));
    
    return items;
}

StiMobileDesigner.prototype.GetCheckBoxStyleItems = function () {
    var items = [];
    
    items.push(this.Item("style0", this.loc.PropertyEnum.StiCheckStyleCross, "CheckBoxCross.png", "Cross"));
    items.push(this.Item("style1", this.loc.PropertyEnum.StiCheckStyleCheck, "CheckBoxCheck.png", "Check"));
    items.push(this.Item("style2", this.loc.PropertyEnum.StiCheckStyleCrossRectangle, "CheckBoxCrossRectangle.png", "CrossRectangle"));
    items.push(this.Item("style3", this.loc.PropertyEnum.StiCheckStyleCheckRectangle, "CheckBoxCheckRectangle.png", "CheckRectangle"));
    items.push(this.Item("style4", this.loc.PropertyEnum.StiCheckStyleCrossCircle, "CheckBoxCrossCircle.png", "CrossCircle"));
    items.push(this.Item("style5", this.loc.PropertyEnum.StiCheckStyleDotCircle, "CheckBoxDotCircle.png", "DotCircle"));
    items.push(this.Item("style6", this.loc.PropertyEnum.StiCheckStyleDotRectangle, "CheckBoxDotRectangle.png", "DotRectangle"));
    items.push(this.Item("style7", this.loc.PropertyEnum.StiCheckStyleNoneCircle, "CheckBoxNoneCircle.png", "NoneCircle"));
    items.push(this.Item("style8", this.loc.PropertyEnum.StiCheckStyleNoneRectangle, "CheckBoxNoneRectangle.png", "NoneRectangle"));
    items.push(this.Item("style9", this.loc.PropertyEnum.StiCheckStyleNone, "CheckBoxNone.png", "None"));
    
    return items;
}

StiMobileDesigner.prototype.GetCheckBoxValuesItems = function () {
    var items = [];
   
    items.push(this.Item("value0", this.loc.FormFormatEditor.nameTrue + "/" + this.loc.FormFormatEditor.nameFalse, null, "true/false"));
    items.push(this.Item("value1", this.loc.FormFormatEditor.nameYes + "/" + this.loc.FormFormatEditor.nameNo, null, "yes/no"));
    items.push(this.Item("value2", this.loc.FormFormatEditor.nameOn + "/" + this.loc.FormFormatEditor.nameOff, null, "on/off"));
    items.push(this.Item("value3", "1/0", null, "1/0"));    
    
    return items;
}

StiMobileDesigner.prototype.GetCloneContainerItems = function (currentComponentName) {
    var items = [];
    if (this.options.report != null) {
        items.push(this.Item("NotAssigned", this.loc.Report.NotAssigned, null, "[Not Assigned]"));

        for (var pageName in this.options.report.pages)
            for (var componentName in this.options.report.pages[pageName].components) {
                var component = this.options.report.pages[pageName].components[componentName];
                if (component.typeComponent == "StiPanel" ||
                    component.typeComponent == "StiSubReport" ||
                    component.typeComponent == "StiCrossTab" ||
                    (component.typeComponent == "StiClone" && currentComponentName != component.properties.name)) {
                    items.push(this.Item(componentName, componentName, null, componentName));
                }
            }

    }
    return items;
}

StiMobileDesigner.prototype.GetMasterComponentItems = function (resultForDataForm) {
    if (!this.options.report) return null;
    var selectedObjects = this.options.selectedObjects || [this.options.selectedObject];
    if (!selectedObjects) return null;

    var currPageName = selectedObjects[0].properties.pageName;
    var selectedNames = [];
    for (var i = 0; i < selectedObjects.length; i++) selectedNames.push(selectedObjects[i].properties.name)

    var items = [];
    for (var componentName in this.options.report.pages[currPageName].components) {
        var component = this.options.report.pages[currPageName].components[componentName];
        if ((component.typeComponent == "StiTable" || component.typeComponent == "StiDataBand" || component.typeComponent == "StiHierarchicalBand" ||
            component.typeComponent == "StiCrossDataBand" || component.typeComponent == "StiChart") && !this.IsContains(selectedNames, componentName))
            if (resultForDataForm)
                items.push({ name: componentName, type: component.typeComponent });
            else
                items.push(this.Item(componentName, componentName, null, componentName));
    }

    //subreport
    for (var pageName in this.options.report.pages) {
        for (var componentName in this.options.report.pages[pageName].components) {
            var component = this.options.report.pages[pageName].components[componentName];
            if (component.typeComponent == "StiSubReport" && component.properties.subReportPage == currPageName) {
                var parentName = component.properties.parentName;
                var parentComponent = this.options.report.pages[pageName].components[parentName];

                while (parentComponent) {
                    if (parentComponent &&
                        (parentComponent.typeComponent == "StiDataBand" ||
                        parentComponent.typeComponent == "StiHierarchicalBand" ||
                        parentComponent.typeComponent == "StiCrossDataBand")) {
                        if (resultForDataForm)
                            items.push({ name: parentComponent.properties.name, type: parentComponent.typeComponent });
                        else
                            items.push(this.Item(parentComponent.properties.name, parentComponent.properties.name, null, parentComponent.properties.name));
                    }
                    parentComponent = this.options.report.pages[pageName].components[parentComponent.properties.parentName];
                }
            }
        }
    }

    if (items.length == 0) return null;

    return items;
}

StiMobileDesigner.prototype.GetRelationsInSourceItems = function (object) {    
    if (!object) return null;
    var items = [];

    for (var i = 0; i < object.relations.length; i++) {
        items.push(this.Item("relation" + i, object.relations[i].name, null, object.relations[i].nameInSource));
    }
    
    return items.length == 0 ? null : items;
}

StiMobileDesigner.prototype.GetSubReportItems = function () {
    var items = [];
    var selectedObjects = this.options.selectedObjects || [this.options.selectedObject];
    if (this.options.report && selectedObjects) {
        items.push(this.Item("NotAssigned", this.loc.Report.NotAssigned, null, "[Not Assigned]"));
        var currentPageName = selectedObjects[0].properties.pageName || selectedObjects[0].properties.name;
        
        for (var pageName in this.options.report.pages) {
            if (pageName != currentPageName)
                items.push(this.Item(pageName, pageName, null, pageName)); 
        }
    }
    
    return items;
}

StiMobileDesigner.prototype.GetImageAlignItems = function () {
    var imgAlignArray = ["TopLeft", "TopCenter", "TopRight", "MiddleLeft", "MiddleCenter", "MiddleRight", "BottomLeft", "BottomCenter", "BottomRight"];
    var imgAlignItems = [];
    for (var i = 0; i < imgAlignArray.length; i++) 
        imgAlignItems.push(this.Item("AlignItem" + i, this.loc.PropertyEnum["ContentAlignment" + imgAlignArray[i]],
            null, imgAlignArray[i]));

    return imgAlignItems;
}

StiMobileDesigner.prototype.GetComponentStyleItems = function (includeCollections) {
    var jsObject = this;
    var defaultFont = "Arial!8!0!0!0!0";
    var defaultBrush = "1!transparent";
    var defaultTextBrush = "1!0,0,0";
    var defaultBorder = "default";
    var items = [];
    var collectionNames = {};

    var commonSelectedObject = this.options.selectedObject || this.GetCommonObject(this.options.selectedObjects);
    var typeComponent = (commonSelectedObject && commonSelectedObject.typeComponent) ? commonSelectedObject.typeComponent : "Any";

    items.push(this.Item("styleNone", this.loc.Report.No, null, "[None]", { type: "StiStyle", font: defaultFont, brush: defaultBrush, textBrush: defaultTextBrush, border: defaultBorder }));
    if (this.options.report) {
        for (var i = 0; i < this.options.report.stylesCollection.length; i++) {
            var properties = this.options.report.stylesCollection[i].properties;
            var styleType = this.options.report.stylesCollection[i].type;
            if (includeCollections && properties.collectionName) {
                collectionNames[properties.collectionName] = true;
            }

            //Chart
            if (styleType == "StiChartStyle" && (typeComponent == "StiChart" || typeComponent == "Any")) {
                var styleProperties = {
                    type: "StiChartStyle",
                    properties: properties
                }
                items.push(this.Item("style" + i, properties.name, null, properties.name, styleProperties));
            }

            //CrossTab
            else if (styleType == "StiCrossTabStyle" && (typeComponent == "StiCrossTab" || typeComponent == "Any")) {
                var styleProperties = {
                    type: "StiCrossTabStyle",
                    properties: properties
                }
                items.push(this.Item("style" + i, properties.name, null, properties.name, styleProperties));
            }

            //Other
            else if (styleType == "StiStyle" && typeComponent != "StiChart" && typeComponent != "StiCrossTab") {
                var styleProperties = {
                    type: "StiStyle",
                    font: (properties["font"] != null && properties["allowUseFont"]) ? properties["font"] : defaultFont,
                    brush: (properties["brush"] != null && properties["allowUseBrush"]) ? properties["brush"] : defaultBrush,
                    textBrush: (properties["textBrush"] != null && properties["allowUseTextBrush"]) ? properties["textBrush"] : defaultTextBrush,
                    border: (properties["border"] != null && properties["allowUseBorderSides"]) ? properties["border"] : defaultBorder
                }
                items.push(this.Item("style" + i, properties.name, null, properties.name, styleProperties));
            }
        }

        if (includeCollections) {
            var styleCollections = [];
            for (var collectionName in collectionNames) {
                styleCollections.push(collectionName);
            }
            return {
                styleCollections: styleCollections,
                styleItems: items
            };
        }
    }

    return items;
}

StiMobileDesigner.prototype.GetHorizontalAlignmentItems = function (shotType) {    
    var items = [];
    items.push(this.Item("horAlignLeft", this.loc.PropertyEnum.StiTextHorAlignmentLeft, null, "Left"));
    items.push(this.Item("horAlignCenter", this.loc.PropertyEnum.StiTextHorAlignmentCenter, null, "Center"));
    items.push(this.Item("horAlignRight", this.loc.PropertyEnum.StiTextHorAlignmentRight, null, "Right"));
    if (!shotType) items.push(this.Item("horAlignWidth", this.loc.PropertyEnum.StiTextHorAlignmentWidth, null, "Width"));
    
    return items;
}

StiMobileDesigner.prototype.GetVerticalAlignmentItems = function (shotType) {    
    var items = [];
    items.push(this.Item("vertAlignTop", this.loc.PropertyEnum.StiVertAlignmentTop, null, "Top"));
    items.push(this.Item("vertAlignCenter", this.loc.PropertyEnum.StiVertAlignmentCenter, null, "Center"));
    items.push(this.Item("vertAlignBottom", this.loc.PropertyEnum.StiVertAlignmentBottom, null, "Bottom"));
    
    return items;
}

StiMobileDesigner.prototype.GetCrossTabHorAlignItems = function (shotType) {    
    var items = [];
    items.push(this.Item("crossTabAlignNone", this.loc.PropertyEnum.StiCrossHorAlignmentNone, null, "None"));
    items.push(this.Item("crossTabAlignLeft", this.loc.PropertyEnum.StiTextHorAlignmentLeft, null, "Left"));
    items.push(this.Item("crossTabAlignCenter", this.loc.PropertyEnum.StiTextHorAlignmentCenter, null, "Center"));
    items.push(this.Item("crossTabAlignRight", this.loc.PropertyEnum.StiTextHorAlignmentRight, null, "Right"));
    items.push(this.Item("crossTabAlignWidth", this.loc.PropertyEnum.StiTextHorAlignmentWidth, null, "Width"));
    
    return items;
}

StiMobileDesigner.prototype.GetTextFormatItems = function (showImage) {    
    var items = [];
    items.push(this.Item("StiGeneralFormatService", this.loc.FormFormatEditor.General, showImage ? "FormatGeneral.png" : null, "StiGeneralFormatService"));
    items.push(this.Item("StiNumberFormatService", this.loc.FormFormatEditor.Number, showImage ? "FormatNumber.png" : null, "StiNumberFormatService"));
    items.push(this.Item("StiCurrencyFormatService", this.loc.FormFormatEditor.Currency, showImage ? "FormatCurrency.png" : null, "StiCurrencyFormatService"));
    items.push(this.Item("StiDateFormatService", this.loc.FormFormatEditor.Date, showImage ? "FormatDate.png" : null, "StiDateFormatService"));
    items.push(this.Item("StiTimeFormatService", this.loc.FormFormatEditor.Time, showImage ? "FormatTime.png" : null, "StiTimeFormatService"));
    items.push(this.Item("StiPercentageFormatService", this.loc.FormFormatEditor.Percentage, showImage ? "FormatPercentage.png" : null, "StiPercentageFormatService"));
    items.push(this.Item("StiBooleanFormatService", this.loc.FormFormatEditor.Boolean, showImage ? "FormatBoolean.png" : null, "StiBooleanFormatService"));
    
    return items;
}

StiMobileDesigner.prototype.GetRelationsItemsFromDataSource = function (dataSource) {
    var relationsArray = [];
    this.GetAllRelationsFromDataSource(dataSource, "", null, relationsArray);

    var items = [];
    items.push(this.Item("NotAssigned", this.loc.Report.NotAssigned, null, "NotAssigned"));

    for (var i = 0; i < relationsArray.length; i++) {
        items.push(this.Item("relationItem" + relationsArray[i], relationsArray[i], null, relationsArray[i]));
    }
    
    return items;
}

StiMobileDesigner.prototype.GetTotalFuntionItems = function (notNone) {
    var items = [];
    if (!notNone) items.push(this.Item("TotalFuntionNone", this.loc.PropertyEnum.StiBorderSidesNone, null, "none"));

    for (var i = 0; i < this.options.aggrigateFunctions.length; i++) {
        items.push(this.Item("TotalFuntion" + i, this.options.aggrigateFunctions[i].serviceName, null, this.options.aggrigateFunctions[i].serviceName));
    }

    return items;
}

StiMobileDesigner.prototype.GetLanguagesItems = function () {
    var items = [];
    items.push(this.Item("languageC", "C#", null, "C"));
    items.push(this.Item("languageVB", "VB.Net", null, "VB"));

    return items;
}

StiMobileDesigner.prototype.GetComponentTypeItems = function () {
    var items = [];
    items.push(this.Item("componentType0", this.loc.Components.StiDataBand, null, "Data"));
    items.push(this.Item("componentType1", this.loc.Components.StiTable, null, "Table"));

    return items;
}

StiMobileDesigner.prototype.GetFontSizes = function () {
    return ["5", "6", "7", "8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24", "26", "28", "36", "48", "72"];
}

StiMobileDesigner.prototype.GetDictionaryNewItems = function () {
    var items = [];   
    items.push(this.Item("dataSourceNew", this.loc.MainMenu.menuEditDataSourceNew.replace("...", ""), "DataSourceNew.png", "dataSourceNew"));
    items.push("separator1");
    if (!this.options.isJava && !this.options.jsMode) {
        items.push(this.Item("businessObjectNew", this.loc.MainMenu.menuEditBusinessObjectNew.replace("...", ""), "BusinessObjectNew.png", "businessObjectNew"));
    }
    items.push(this.Item("columnNew", this.loc.MainMenu.menuEditColumnNew.replace("...", ""), "ColumnNew.png", "columnNew"));
    items.push(this.Item("calcColumnNew", this.loc.MainMenu.menuEditCalcColumnNew.replace("...", ""), "CalcColumnNew.png", "calcColumnNew"));
    items.push(this.Item("parameterNew", this.loc.MainMenu.menuEditDataParameterNew.replace("...", ""), "ParameterNew.png", "parameterNew"));
    items.push(this.Item("relationNew", this.loc.MainMenu.menuEditRelationNew.replace("...", ""), "RelationNew.png", "relationNew"));
    items.push("separator2");
    items.push(this.Item("categoryNew", this.loc.MainMenu.menuEditCategoryNew.replace("...", ""), "CategoryNew.png", "categoryNew"));
    items.push(this.Item("variableNew", this.loc.MainMenu.menuEditVariableNew.replace("...", ""), "VariableNew.png", "variableNew"));
    items.push(this.Item("resourceNew", this.loc.MainMenu.menuEditResourceNew.replace("...", ""), "Resources.ResourceNew.png", "resourceNew"));
    return items;
}

StiMobileDesigner.prototype.GetDictionaryActionsItems = function () {
    var items = [];
    items.push(this.Item("newDictionary", this.loc.FormDictionaryDesigner.DictionaryNew.replace("...", ""), "New.png", "newDictionary"));
    items.push(this.Item("synchronize", this.loc.FormDictionaryDesigner.Synchronize, "Synchronize.png", "synchronize"));
    
    return items;
}

StiMobileDesigner.prototype.GetColumnTypesItems = function () {
    var items = [];
    items.push(this.Item("bool", "bool", null, "bool"));
    items.push(this.Item("byte", "byte", null, "byte"));
    items.push(this.Item("byte[]", "byte[]", null, "byte[]"));
    items.push(this.Item("char", "char", null, "char"));
    items.push(this.Item("datetime", "datetime", null, "datetime"));
    items.push(this.Item("decimal", "decimal", null, "decimal"));
    items.push(this.Item("double", "double", null, "double"));
    items.push(this.Item("guid", "guid", null, "guid"));
    items.push(this.Item("short", "short", null, "short"));
    items.push(this.Item("int", "int", null, "int"));
    items.push(this.Item("long", "long", null, "long"));
    items.push(this.Item("sbyte", "sbyte", null, "sbyte"));
    items.push(this.Item("float", "float", null, "float"));
    items.push(this.Item("string", "string", null, "string"));
    items.push(this.Item("timespan", "timespan", null, "timespan"));
    items.push(this.Item("ushort", "ushort", null, "ushort"));
    items.push(this.Item("uint", "uint", null, "uint"));
    items.push(this.Item("ulong", "ulong", null, "ulong"));
    items.push(this.Item("image", "image", null, "image"));
    items.push(this.Item("bool (Nullable)", "bool (Nullable)", null, "bool (Nullable)"));
    items.push(this.Item("byte (Nullable)", "byte (Nullable)", null, "byte (Nullable)"));
    items.push(this.Item("char (Nullable)", "char (Nullable)", null, "char (Nullable)"));
    items.push(this.Item("datetime (Nullable)", "datetime (Nullable)", null, "datetime (Nullable)"));
    items.push(this.Item("decimal (Nullable)", "decimal (Nullable)", null, "decimal (Nullable)"));
    items.push(this.Item("double (Nullable)", "double (Nullable)", null, "double (Nullable)"));
    items.push(this.Item("guid (Nullable)", "guid (Nullable)", null, "guid (Nullable)"));
    items.push(this.Item("short (Nullable)", "short (Nullable)", null, "short (Nullable)"));
    items.push(this.Item("int (Nullable)", "int (Nullable)", null, "int (Nullable)"));
    items.push(this.Item("long (Nullable)", "long (Nullable)", null, "long (Nullable)"));
    items.push(this.Item("byte (Nullable)", "byte (Nullable)", null, "byte (Nullable)"));
    items.push(this.Item("sbyte (Nullable)", "sbyte (Nullable)", null, "sbyte (Nullable)"));
    items.push(this.Item("float (Nullable)", "float (Nullable)", null, "float (Nullable)"));
    items.push(this.Item("timespan (Nullable)", "timespan (Nullable)", null, "timespan (Nullable)"));
    items.push(this.Item("ushort (Nullable)", "ushort (Nullable)", null, "ushort (Nullable)"));
    items.push(this.Item("uint (Nullable)", "uint (Nullable)", null, "uint (Nullable)"));
    items.push(this.Item("ulong (Nullable)", "ulong (Nullable)", null, "ulong (Nullable)"));
    items.push(this.Item("object", "object", null, "object"));
    items.push(this.Item("refcursor", "refcursor", null, "object"));

    return items;
}

StiMobileDesigner.prototype.GetQueryTextTypeItems = function () {
    var items = [];
    items.push(this.Item("table", this.loc.PropertyEnum.StiSqlSourceTypeTable, null, "Table"));
    items.push(this.Item("storedProcedure", this.loc.PropertyEnum.StiSqlSourceTypeStoredProcedure, null, "StoredProcedure"));

    return items;
}

StiMobileDesigner.prototype.GetVariableTypesItems = function () {
    var items = [];
    items.push(this.Item("string", "string", "DataColumnString.png", "string"));
    items.push(this.Item("float", "float", "DataColumnFloat.png", "float"));
    items.push(this.Item("double", "double", "DataColumnFloat.png", "double"));
    items.push(this.Item("decimal", "decimal", "DataColumnDecimal.png", "decimal"));
    items.push("separator1");
    items.push(this.Item("datetime", "datetime", "DataColumnDateTime.png", "datetime"));
    items.push(this.Item("timespan", "timespan", "DataColumnDateTime.png", "timespan"));
    items.push("separator2");
    items.push(this.Item("sbyte", "sbyte", "DataColumnInt.png", "sbyte"));
    items.push(this.Item("byte", "byte", "DataColumnInt.png", "byte"));
    items.push(this.Item("short", "short", "DataColumnInt.png", "short"));
    items.push(this.Item("ushort", "ushort", "DataColumnInt.png", "ushort"));
    items.push(this.Item("int", "int", "DataColumnInt.png", "int"));
    items.push(this.Item("uint", "uint", "DataColumnInt.png", "uint"));
    items.push(this.Item("long", "long", "DataColumnInt.png", "long"));
    items.push(this.Item("ulong", "ulong", "DataColumnInt.png", "ulong"));
    items.push("separator3");
    items.push(this.Item("bool", "bool", "DataColumnBool.png", "bool"));
    items.push(this.Item("char", "char", "DataColumnChar.png", "char"));
    items.push(this.Item("guid", "guid", "DataColumnString.png", "guid"));
    items.push(this.Item("object", "object", "DataColumnString.png", "object"));
    items.push("separator3");
    items.push(this.Item("image", "image", "DataColumnImage.png", "image"));

    return items;
}

StiMobileDesigner.prototype.GetVariableBasicTypesItems = function () {
    var items = [];
    items.push(this.Item("value", this.loc.PropertyEnum.StiTypeModeValue, null, "Value"));
    items.push(this.Item("nullablevalue", this.loc.PropertyEnum.StiTypeModeNullableValue, null, "NullableValue"));
    items.push(this.Item("range", this.loc.PropertyEnum.StiTypeModeRange, null, "Range"));
    items.push(this.Item("list", this.loc.PropertyEnum.StiTypeModeList, null, "List"));

    return items;
}

StiMobileDesigner.prototype.GetMonthesForDatePickerItems = function () {
    var items = [];
    for (i = 0; i < this.options.monthesCollection.length; i++)
        items.push(this.Item("Month" + i, this.loc.A_WebViewer["Month" + this.options.monthesCollection[i]], null, i));

    return items;
}

StiMobileDesigner.prototype.GetVariableDataSourceItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyMain.Items, null, "Items"));
    items.push(this.Item("item1", this.loc.PropertyMain.DataColumns, null, "Columns"));

    return items;
}

StiMobileDesigner.prototype.GetVariableDateTimeTypesItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiDateTimeTypeDateAndTime, null, "DateAndTime"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiDateTimeTypeDate, null, "Date"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiDateTimeTypeTime, null, "Time"));

    return items;
}

StiMobileDesigner.prototype.GetBoolItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.FormFormatEditor.nameTrue, null, "True"));
    items.push(this.Item("item1", this.loc.FormFormatEditor.nameFalse, null, "False"));

    return items;
}

StiMobileDesigner.prototype.GetAddStyleMenuItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.Components.StiComponent, "Styles.StiStyle32.png", "StiStyle"));
    items.push(this.Item("item1", this.loc.Components.StiChart, "Styles.StiChartStyle32.png", "StiChartStyle"));
    items.push(this.Item("item2", this.loc.Components.StiCrossTab, "Styles.StiCrossTabStyle32.png", "StiCrossTabStyle"));
    items.push(this.Item("item3", this.loc.Components.StiGauge, "Styles.StiGaugeStyle32.png", "StiGaugeStyle"));
    items.push(this.Item("item4", this.loc.Components.StiMap, "Styles.StiMapStyle32.png", "StiMapStyle"));
    items.push(this.Item("item5", this.loc.Components.StiTable, "Styles.StiTableStyle32.png", "StiTableStyle"));

    return items;
}

StiMobileDesigner.prototype.GetNestedLevelsItems = function () {
    var items = [];
    for (i = 1; i <= 10; i++)
        items.push(this.Item("item" + i, i, null, i.toString()));    

    return items;
}

StiMobileDesigner.prototype.GetNestedFactorItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiNestedFactorHigh, null, "High"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiNestedFactorNormal, null, "Normal"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiNestedFactorLow, null, "Low"));

    return items;
}

StiMobileDesigner.prototype.GetPageMarginsItems = function () {
    var items = [];
    items.push(this.Item("marginsNormal", "<b>" + this.loc.FormDesigner.MarginsNormal + "</b>", "MarginsNormal.png", "marginsNormal"));
    items.push(this.Item("marginsNarrow", "<b>" + this.loc.FormDesigner.MarginsNarrow + "</b>", "MarginsNarrow.png", "marginsNarrow"));
    items.push(this.Item("marginsWide", "<b>" + this.loc.FormDesigner.MarginsWide + "</b>", "MarginsWide.png", "marginsWide"));

    return items;
}

StiMobileDesigner.prototype.GetPageColumnsItems = function () {
    var items = [];
    items.push(this.Item("oneColumn", this.loc.FormDesigner.ColumnsOne, "OneColumn.png", "1"));
    items.push(this.Item("twoColumn", this.loc.FormDesigner.ColumnsTwo, "TwoColumn.png", "2"));
    items.push(this.Item("threeColumn", this.loc.FormDesigner.ColumnsThree, "ThreeColumn.png", "3"));

    return items;
}

StiMobileDesigner.prototype.GetAddSeriesItems = function () {
    var items = [];
    items.push(this.Item("ClusteredColumn", this.loc.Chart.ClusteredColumn, "ChartCategoryClusteredColumn.png", "ClusteredColumn", null, true));
    items.push(this.Item("Line", this.loc.Chart.Line, "ChartCategoryLine.png", "Line", null, true));
    items.push(this.Item("Area", this.loc.Chart.Area, "ChartCategoryArea.png", "Area", null, true));
    items.push(this.Item("Range", this.loc.Chart.Range, "ChartCategoryRange.png", "Range", null, true));
    items.push(this.Item("ClusteredBar", this.loc.Chart.ClusteredBar, "ChartCategoryClusteredBar.png", "ClusteredBar", null, true));
    items.push(this.Item("Scatter", this.loc.Chart.Scatter, "ChartCategoryScatter.png", "Scatter", null, true));
    items.push(this.Item("Pie", this.loc.Chart.Pie, "ChartCategoryPie.png", "Pie", null, true));
    items.push(this.Item("Radar", this.loc.Chart.Radar, "ChartCategoryRadar.png", "Radar", null, true));
    items.push(this.Item("Funnel", this.loc.Chart.Funnel, "ChartCategoryFunnel.png", "Funnel", null, true));
    items.push(this.Item("Financial", this.loc.Chart.Financial, "ChartCategoryFinancial.png", "Financial", null, true));
    items.push(this.Item("Others", this.loc.FormColorBoxPopup ? this.loc.FormColorBoxPopup.Others.replace("...", "") : "Others", "ChartCategoryOthers.png", "Others", null, true));

    return items;
}

StiMobileDesigner.prototype.GetChartClusteredColumnItems = function () {
    var items = [];
    items.push(this.Item("StiClusteredColumnSeries", this.loc.Chart.ClusteredColumn, "ChartClusteredColumn.png", "StiClusteredColumnSeries"));
    items.push(this.Item("StiStackedColumnSeries", this.loc.Chart.StackedColumn, "ChartStackedColumn.png", "StiStackedColumnSeries"));
    items.push(this.Item("StiFullStackedColumnSeries", this.loc.Chart.FullStackedColumn, "ChartFullStackedColumn.png", "StiFullStackedColumnSeries"));
    
    return items;
}

StiMobileDesigner.prototype.GetChartLineItems = function () {
    var items = [];
    items.push(this.Item("StiLineSeries", this.loc.Chart.Line, "ChartLine.png", "StiLineSeries"));
    items.push(this.Item("StiStackedLineSeries", this.loc.Chart.StackedLine, "ChartStackedLine.png", "StiStackedLineSeries"));
    items.push(this.Item("StiFullStackedLineSeries", this.loc.Chart.FullStackedLine, "ChartFullStackedLine.png", "StiFullStackedLineSeries"));
    items.push("separator");
    items.push(this.Item("StiSplineSeries", this.loc.Chart.Spline, "ChartSpline.png", "StiSplineSeries"));
    items.push(this.Item("StiStackedSplineSeries", this.loc.Chart.StackedSpline, "ChartStackedSpline.png", "StiStackedSplineSeries"));
    items.push(this.Item("StiFullStackedSplineSeries", this.loc.Chart.FullStackedSpline, "ChartFullStackedSpline.png", "StiFullStackedSplineSeries"));
    items.push("separator");
    items.push(this.Item("StiSteppedLineSeries", this.loc.Chart.SteppedLine, "ChartSteppedLine.png", "StiSteppedLineSeries"));

    return items;
}

StiMobileDesigner.prototype.GetChartAreaItems = function () {
    var items = [];
    items.push(this.Item("StiAreaSeries", this.loc.Chart.Area, "ChartArea.png", "StiAreaSeries"));
    items.push(this.Item("StiStackedAreaSeries", this.loc.Chart.StackedArea, "ChartStackedArea.png", "StiStackedAreaSeries"));
    items.push(this.Item("StiFullStackedAreaSeries", this.loc.Chart.FullStackedArea, "ChartFullStackedArea.png", "StiFullStackedAreaSeries"));
    items.push("separator");
    items.push(this.Item("StiSplineAreaSeries", this.loc.Chart.SplineArea, "ChartSplineArea.png", "StiSplineAreaSeries"));
    items.push(this.Item("StiStackedSplineAreaSeries", this.loc.Chart.StackedSplineArea, "ChartStackedSplineArea.png", "StiStackedSplineAreaSeries"));
    items.push(this.Item("StiFullStackedSplineAreaSeries", this.loc.Chart.FullStackedSplineArea, "ChartFullStackedSplineArea.png", "StiFullStackedSplineAreaSeries"));
    items.push("separator");
    items.push(this.Item("StiSteppedAreaSeries", this.loc.Chart.SteppedArea, "ChartSteppedArea.png", "StiSteppedAreaSeries"));

    return items;
}

StiMobileDesigner.prototype.GetChartRangeItems = function () {
    var items = [];
    items.push(this.Item("StiRangeSeries", this.loc.Chart.Range, "ChartRange.png", "StiRangeSeries"));
    items.push(this.Item("StiSplineRangeSeries", this.loc.Chart.SplineRange, "ChartSplineRange.png", "StiSplineRangeSeries"));
    items.push(this.Item("StiSteppedRangeSeries", this.loc.Chart.SteppedRange, "ChartSteppedRange.png", "StiSteppedRangeSeries"));
    items.push("separator");
    items.push(this.Item("StiRangeBarSeries", this.loc.Chart.RangeBar, "ChartRangeBar.png", "StiRangeBarSeries"));

    return items;
}

StiMobileDesigner.prototype.GetChartClusteredBarItems = function () {
    var items = [];
    items.push(this.Item("StiClusteredBarSeries", this.loc.Chart.ClusteredBar, "ChartClusteredBar.png", "StiClusteredBarSeries"));
    items.push(this.Item("StiStackedBarSeries", this.loc.Chart.StackedBar, "ChartStackedBar.png", "StiStackedBarSeries"));
    items.push(this.Item("StiFullStackedBarSeries", this.loc.Chart.FullStackedBar, "ChartFullStackedBar.png", "StiFullStackedBarSeries"));

    return items;
}

StiMobileDesigner.prototype.GetChartScatterItems = function () {
    var items = [];
    items.push(this.Item("StiScatterSeries", this.loc.Chart.Scatter, "ChartScatter.png", "StiScatterSeries"));
    items.push(this.Item("StiScatterLineSeries", this.loc.Chart.ScatterLine, "ChartScatterLine.png", "StiScatterLineSeries"));
    items.push(this.Item("StiScatterSplineSeries", this.loc.Chart.ScatterSpline, "ChartScatterSpline.png", "StiScatterSplineSeries"));

    return items;
}

StiMobileDesigner.prototype.GetChartPieItems = function () {
    var items = [];
    items.push(this.Item("StiPieSeries", this.loc.Chart.Pie, "ChartCategoryPie.png", "StiPieSeries"));

    return items;
}

StiMobileDesigner.prototype.GetChartRadarItems = function () {
    var items = [];
    items.push(this.Item("StiRadarPointSeries", this.loc.Chart.RadarPoint, "ChartRadarPoint.png", "StiRadarPointSeries"));
    items.push(this.Item("StiRadarLineSeries", this.loc.Chart.RadarLine, "ChartRadarLine.png", "StiRadarLineSeries"));
    items.push(this.Item("StiRadarAreaSeries", this.loc.Chart.RadarArea, "ChartRadarArea.png", "StiRadarAreaSeries"));

    return items;
}

StiMobileDesigner.prototype.GetChartFunnelItems = function () {
    var items = [];
    items.push(this.Item("StiFunnelSeries", this.loc.Chart.Funnel, "ChartCategoryFunnel.png", "StiFunnelSeries"));
    items.push(this.Item("StiFunnelWeightedSlicesSeries", this.loc.Chart.FunnelWeightedSlices, "ChartFunnelWeightedSlices.png", "StiFunnelWeightedSlicesSeries"));

    return items;
}

StiMobileDesigner.prototype.GetChartFinancialItems = function () {
    var items = [];
    items.push(this.Item("StiCandlestickSeries", this.loc.Chart.Candlestick, "ChartCandlestick.png", "StiCandlestickSeries"));
    items.push(this.Item("StiStockSeries", this.loc.Chart.Stock, "ChartStock.png", "StiStockSeries"));

    return items;
}

StiMobileDesigner.prototype.GetChartOthersItems = function () {
    var items = [];
    items.push(this.Item("StiGanttSeries", this.loc.Chart.Gantt, "ChartGantt.png", "StiGanttSeries"));
    items.push(this.Item("StiDoughnutSeries", this.loc.Chart.Doughnut, "ChartCategoryOthers.png", "StiDoughnutSeries"));
    items.push(this.Item("StiBubbleSeries", this.loc.Chart.Bubble, "ChartBubble.png", "StiBubbleSeries"));
    items.push(this.Item("StiTreemapSeries", this.loc.Chart.Treemap, "ChartTreemap.png", "StiTreemapSeries"));

    return items;
}

StiMobileDesigner.prototype.GetSortByItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyMain.Value, null, "Value"));
    items.push(this.Item("item1", this.loc.PropertyMain.Argument, null, "Argument"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiCheckStyleNone, null, "None"));

    return items;
}

StiMobileDesigner.prototype.GetChartSortDirectionItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiSortDirectionAsc, null, "Ascending"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiSortDirectionDesc, null, "Descending"));

    return items;
}

StiMobileDesigner.prototype.GetShowSeriesLabelsItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiShowSeriesLabelsFromChart, null, "FromChart"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiShowSeriesLabelsFromSeries, null, "FromSeries"));

    return items;
}

StiMobileDesigner.prototype.GetYAxisItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiSeriesYAxisLeftYAxis, null, "LeftYAxis"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiSeriesYAxisRightYAxis, null, "RightYAxis"));

    return items;
}

StiMobileDesigner.prototype.GetMarkerTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiMarkerTypeRectangle, null, "Rectangle"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiMarkerTypeTriangle, null, "Triangle"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiMarkerTypeCircle, null, "Circle"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiMarkerTypeStar5, null, "Star5"));
    items.push(this.Item("item4", this.loc.PropertyEnum.StiMarkerTypeStar6, null, "Star6"));
    items.push(this.Item("item5", this.loc.PropertyEnum.StiMarkerTypeStar7, null, "Star7"));
    items.push(this.Item("item6", this.loc.PropertyEnum.StiMarkerTypeStar8, null, "Star8"));
    items.push(this.Item("item7", this.loc.PropertyEnum.StiMarkerTypeHexagon, null, "Hexagon"));

    return items;
}

StiMobileDesigner.prototype.GetTopNModeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiCheckStyleNone, null, "None"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiBorderSidesTop, null, "Top"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiBorderSidesBottom, null, "Bottom"));

    return items;
}

StiMobileDesigner.prototype.GetRadarStyleItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiRadarStyleXFPolygon, null, "Polygon"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiRadarStyleXFCircle, null, "Circle"));

    return items;
}

StiMobileDesigner.prototype.GetXAxisArrowStyleItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiCheckStyleNone, null, "None"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiArrowStyleTriangle, null, "Triangle"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiArrowStyleLines, null, "Lines"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiArrowStyleCircle, null, "Circle"));
    items.push(this.Item("item4", this.loc.PropertyEnum.StiArrowStyleArc , null, "Arc"));
    items.push(this.Item("item5", this.loc.PropertyEnum.StiArrowStyleArcAndCircle, null, "ArcAndCircle"));

    return items;
}

StiMobileDesigner.prototype.GetXAxisStepItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiCheckStyleNone, null, "None"));
    items.push(this.Item("item1", "Second", null, "Second"));
    items.push(this.Item("item2", "Minute", null, "Minute"));
    items.push(this.Item("item3", "Hour", null, "Hour"));
    items.push(this.Item("item4", "Day", null, "Day"));
    items.push(this.Item("item5", "Month", null, "Month"));
    items.push(this.Item("item6", "Year", null, "Year"));

    return items;
}

StiMobileDesigner.prototype.GetXAxisPlacementItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiCheckStyleNone, null, "None"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiLabelsPlacementOneLine, null, "OneLine"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiLabelsPlacementTwoLines, null, "TwoLines"));

    return items;
}

StiMobileDesigner.prototype.GetDockItems = function () {
    var items = [];

    items.push(this.Item("item0", this.loc.PropertyEnum.StiVertAlignmentTop, null, "Top"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiTextHorAlignmentRight, null, "Right"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiVertAlignmentBottom, null, "Bottom"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiTextHorAlignmentLeft, null, "Left"));

    return items;
}

StiMobileDesigner.prototype.GetXAxisTextAlignmentItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiTextHorAlignmentLeft, null, "Left"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiTextHorAlignmentRight, null, "Right"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiTextHorAlignmentCenter, null, "Center"));

    return items;
}

StiMobileDesigner.prototype.GetXAxisTitleAlignmentItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StringAlignmentNear, null, "Near"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StringAlignmentCenter, null, "Center"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StringAlignmentFar, null, "Far"));
    
    return items;
}

StiMobileDesigner.prototype.GetXAxisTitleDirectionItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiLegendDirectionLeftToRight, null, "LeftToRight"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiLegendDirectionRightToLeft, null, "RightToLeft"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiLegendDirectionTopToBottom, null, "TopToBottom"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiLegendDirectionBottomToTop, null, "BottomToTop"));

    return items;
}

StiMobileDesigner.prototype.GetShowXAxisItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiShowXAxisBottom, null, "Bottom"));    
    items.push(this.Item("item1", this.loc.PropertyEnum.StiShowYAxisCenter, null, "Center"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiShowYAxisBoth, null, "Both"));

    return items;
}

StiMobileDesigner.prototype.GetLegendValueTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiSeriesLabelsValueTypeValue, null, "Value"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiSeriesLabelsValueTypeSeriesTitle, null, "SeriesTitle"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiSeriesLabelsValueTypeArgument, null, "Argument"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiSeriesLabelsValueTypeTag, null, "Tag"));
    items.push(this.Item("item4", this.loc.PropertyEnum.StiSeriesLabelsValueTypeWeight, null, "Weight"));
    items.push(this.Item("item5", this.loc.PropertyEnum.StiSeriesLabelsValueTypeValueArgument, null, "ValueArgument"));
    items.push(this.Item("item6", this.loc.PropertyEnum.StiSeriesLabelsValueTypeArgumentValue, null, "ArgumentValue"));
    items.push(this.Item("item7", this.loc.PropertyEnum.StiSeriesLabelsValueTypeSeriesTitleValue, null, "SeriesTitleValue"));
    items.push(this.Item("item8", this.loc.PropertyEnum.StiSeriesLabelsValueTypeSeriesTitleArgument, null, "SeriesTitleArgument"));    

    return items;
}

StiMobileDesigner.prototype.GetDrillDownPageItems = function () {
    var items = [];
    if (this.options.report != null) {
        items.push(this.Item("NotAssigned", this.loc.Report.NotAssigned, null, ""));
        for (var pageName in this.options.report.pages) {
            if (pageName != this.options.selectedObject.properties.pageName)
                items.push(this.Item(pageName, pageName, null, pageName));
        }
    }

    return items;
}

StiMobileDesigner.prototype.GetLegendHorAlignmentItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiLegendHorAlignmentLeftOutside, null, "LeftOutside"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiLegendHorAlignmentLeft, null, "Left"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiLegendHorAlignmentCenter, null, "Center"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiLegendHorAlignmentRight, null, "Right"));
    items.push(this.Item("item4", this.loc.PropertyEnum.StiLegendHorAlignmentRightOutside, null, "RightOutside"));

    return items;
}

StiMobileDesigner.prototype.GetLegendVertAlignmentItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiLegendVertAlignmentTopOutside, null, "TopOutside"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiLegendVertAlignmentTop, null, "Top"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiLegendVertAlignmentCenter, null, "Center"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiLegendVertAlignmentBottom, null, "Bottom"));
    items.push(this.Item("item4", this.loc.PropertyEnum.StiLegendVertAlignmentBottomOutside, null, "BottomOutside"));

    return items;
}

StiMobileDesigner.prototype.GetConstantLinesOrientationItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiOrientationHorizontal, null, "Horizontal"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiOrientationVertical, null, "Vertical"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiOrientationHorizontalRight, null, "HorizontalRight"));

    return items;
}

StiMobileDesigner.prototype.GetConstantLinesPositionItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiTextPositionLeftTop, null, "LeftTop"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiTextPositionLeftBottom, null, "LeftBottom"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiTextPositionCenterTop, null, "CenterTop"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiTextPositionCenterBottom, null, "CenterBottom"));
    items.push(this.Item("item4", this.loc.PropertyEnum.StiTextPositionRightTop, null, "RightTop"));
    items.push(this.Item("item5", this.loc.PropertyEnum.StiTextPositionRightBottom, null, "RightBottom"));

    return items;
}

StiMobileDesigner.prototype.GetConditionsFieldIsItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyMain.Value, null, "Value"));
    items.push(this.Item("item1", this.loc.PropertyMain.Argument, null, "Argument"));

    return items;
}

StiMobileDesigner.prototype.GetConditionsDataTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterDataTypeString, null, "String"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterDataTypeNumeric, null, "Numeric"));    
    items.push(this.Item("item2", this.loc.PropertyEnum.StiFilterDataTypeDateTime, null, "DateTime"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiFilterDataTypeBoolean, null, "Boolean"));

    return items;
}

StiMobileDesigner.prototype.GetFiltersFieldIsItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterItemValue, null, "Value"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterItemArgument, null, "Argument"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiFilterItemExpression, null, "Expression"));

    return items;
}

StiMobileDesigner.prototype.GetSeriesGanttFiltersFieldIsItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterItemValue, null, "Value"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterItemValueEnd, null, "ValueEnd"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiFilterItemArgument, null, "Argument"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiFilterItemExpression, null, "Expression"));

    return items;
}

StiMobileDesigner.prototype.GetSeriesFinancialFiltersFieldIsItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiFilterItemValueOpen, null, "ValueOpen"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiFilterItemValueClose, null, "ValueClose"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiFilterItemValueLow, null, "ValueLow"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiFilterItemValueHigh, null, "ValueHigh"));
    items.push(this.Item("item4", this.loc.PropertyEnum.StiFilterItemArgument, null, "Argument"));
    items.push(this.Item("item5", this.loc.PropertyEnum.StiFilterItemExpression, null, "Expression"));

    return items;
}

StiMobileDesigner.prototype.GetChartStyleBrushTypeItems = function () {
    var items = [];
    items.push(this.Item("StiBrushTypeGlare", this.loc.PropertyEnum.StiBrushTypeGlare, null, "Glare"));
    items.push(this.Item("StiBrushTypeGradient0", this.loc.PropertyEnum.StiBrushTypeGradient0, null, "Gradient0"));
    items.push(this.Item("StiBrushTypeGradient180", this.loc.PropertyEnum.StiBrushTypeGradient180, null, "Gradient180"));
    items.push(this.Item("StiBrushTypeGradient270", this.loc.PropertyEnum.StiBrushTypeGradient270, null, "Gradient270"));
    items.push(this.Item("StiBrushTypeGradient45", this.loc.PropertyEnum.StiBrushTypeGradient45, null, "Gradient45"));
    items.push(this.Item("StiBrushTypeGradient90", this.loc.PropertyEnum.StiBrushTypeGradient90, null, "Gradient90"));
    items.push(this.Item("StiBrushTypeSolid", this.loc.PropertyEnum.StiBrushTypeSolid, null, "Solid"));

    return items;
}

StiMobileDesigner.prototype.GetDataBandItems = function () {
    if (this.options.report == null || (!this.options.selectedObjects && !this.options.selectedObject)) return null;
    var items = [];
    items.push(this.Item("NotAssigned", this.loc.Report.NotAssigned, null, "NotAssigned"));
    var pageName = this.options.selectedObjects ? this.options.selectedObjects[0].properties.pageName : this.options.selectedObject.properties.pageName;
    var page = pageName ? this.options.report.pages[pageName] : this.options.currentPage;
    if (!page) return;

    for (var componentName in page.components) {
        var component = page.components[componentName];
        if (component.typeComponent == "StiDataBand" || component.typeComponent == "StiCrossDataBand" ||
            component.typeComponent == "StiHierarchicalBand" || component.typeComponent == "StiTable" || component.typeComponent == "StiCrossTab")
            items.push(this.Item(componentName, componentName, null, componentName));
    }

    return items;
}

StiMobileDesigner.prototype.GetAddConditionMenuItems = function () {
    var items = [];
    items.push(this.Item("StiHighlightCondition", this.loc.PropertyMain.HighlightCondition, "Highlight.png", "StiHighlightCondition"));
    items.push(this.Item("StiDataBarCondition", this.loc.PropertyMain.DataBarCondition, "DataBar.png", "StiDataBarCondition"));
    items.push(this.Item("StiColorScaleCondition", this.loc.PropertyMain.ColorScaleCondition, "ColorScale.png", "StiColorScaleCondition"));
    items.push(this.Item("StiIconSetCondition", this.loc.PropertyMain.IconSetCondition, "IconSet.png", "StiIconSetCondition"));
    
    return items;
}

StiMobileDesigner.prototype.GetFontSizeItems = function () {
    var sizeItems = [];
    for (var i = 0; i < this.options.fontSizes.length; i++) {
        sizeItems.push(this.Item("homeSizesFont" + i, this.options.fontSizes[i], null, this.options.fontSizes[i]));
    }

    return sizeItems;
}

StiMobileDesigner.prototype.GetConditionsMinimumTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyMain.Auto, null, "Auto"));
    items.push(this.Item("item1", this.loc.FormFormatEditor.Percentage, null, "Percent"));
    items.push(this.Item("item2", this.loc.PropertyMain.Value, null, "Value"));
    items.push(this.Item("item3", this.loc.PropertyMain.Minimum, null, "Minimum"));

    return items;
}

StiMobileDesigner.prototype.GetConditionsMaximumTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyMain.Auto, null, "Auto"));
    items.push(this.Item("item1", this.loc.FormFormatEditor.Percentage, null, "Percent"));
    items.push(this.Item("item2", this.loc.PropertyMain.Value, null, "Value"));
    items.push(this.Item("item3", this.loc.PropertyMain.Maximum, null, "Maximum"));

    return items;
}

StiMobileDesigner.prototype.GetConditionsDirectionItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyMain.Default, null, "Default"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiLegendDirectionLeftToRight, null, "LeftToRight"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiLegendDirectionRightToLeft, null, "RighToLeft"));

    return items;
}

StiMobileDesigner.prototype.GetConditionsBrushTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.Report.StiSolidBrush, null, "Solid"));
    items.push(this.Item("item1", this.loc.Report.StiGradientBrush, null, "Gradient"));

    return items;
}

StiMobileDesigner.prototype.GetConditionsShowBordersItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiBorderSidesNone, null, "None"));
    items.push(this.Item("item1", this.loc.Report.StiSolidBrush, null, "Solid"));

    return items;
}

StiMobileDesigner.prototype.GetConditionsColorScaleTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiColorScaleTypeColor2, null, "Color2"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiColorScaleTypeColor3, null, "Color3"));

    return items;
}

StiMobileDesigner.prototype.GetConditionsValueTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyMain.Auto, null, "Auto"));
    items.push(this.Item("item1", this.loc.FormFormatEditor.Percentage, null, "Percent"));
    items.push(this.Item("item2", this.loc.PropertyMain.Value, null, "Value"));

    return items;
}

StiMobileDesigner.prototype.GetConditionsAlignmentItems = function () {
    var imgAlignArray = ["TopLeft", "TopCenter", "TopRight", "MiddleLeft", "MiddleCenter", "MiddleRight", "BottomLeft", "BottomCenter", "BottomRight"];
    var imgAlignItems = [];
    for (var i = 0; i < imgAlignArray.length; i++) {
        imgAlignItems.push(this.Item("AlignItem" + i, this.loc.PropertyEnum["ContentAlignment" + imgAlignArray[i]],
            null, imgAlignArray[i]));
        if (i == 2 || i == 5) imgAlignItems.push("separator" + i);
    }

    return imgAlignItems;
}

StiMobileDesigner.prototype.GetConditionsOperationItems = function () {
    var items = [];
    items.push(this.Item("item0", ">=", null, "MoreThanOrEqual"));
    items.push(this.Item("item1", ">", null, "MoreThan"));

    return items;
}

StiMobileDesigner.prototype.GetConditionsIconSetValueTypeItems = function () {
    var items = [];
    items.push(this.Item("item1", this.loc.FormFormatEditor.Percentage, null, "Percent"));
    items.push(this.Item("item2", this.loc.PropertyMain.Value, null, "Value"));

    return items;
}

StiMobileDesigner.prototype.GetCalculationModeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiCalculationModeCompilation, null, "Compilation"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiCalculationModeInterpretation, null, "Interpretation"));

    return items;
}

StiMobileDesigner.prototype.GetEngineVersionItems = function () {
    var items = [];
    items.push(this.Item("item0", "EngineV1", null, "EngineV1"));
    items.push(this.Item("item1", "EngineV2", null, "EngineV2"));

    return items;
}

StiMobileDesigner.prototype.GetNumberOfPassItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiNumberOfPassSinglePass, null, "SinglePass"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiNumberOfPassDoublePass, null, "DoublePass"));

    return items;
}

StiMobileDesigner.prototype.GetReportCacheModeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiReportCacheModeOff, null, "Off"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiReportCacheModeOn, null, "On"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiReportCacheModeAuto, null, "Auto"));

    return items;
}

StiMobileDesigner.prototype.GetPreviewModeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiPreviewModeStandard, null, "Standard"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiPreviewModeStandardAndDotMatrix, null, "StandardAndDotMatrix"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiPreviewModeDotMatrix, null, "DotMatrix"));

    return items;
}

StiMobileDesigner.prototype.GetParametersOrientationItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiOrientationHorizontal, null, "Horizontal"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiOrientationVertical, null, "Vertical"));

    return items;
}

StiMobileDesigner.prototype.GetScriptLanguageItems = function () {
    var items = [];
    items.push(this.Item("item0", "CSharp", null, "CSharp"));
    items.push(this.Item("item1", "VB", null, "VB"));

    return items;
}

StiMobileDesigner.prototype.GetTrimmingItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StringTrimmingNone, null, "None"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StringTrimmingCharacter, null, "Character"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StringTrimmingEllipsisCharacter, null, "EllipsisCharacter"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StringTrimmingEllipsisPath, null, "EllipsisPath"));
    items.push(this.Item("item4", this.loc.PropertyEnum.StringTrimmingEllipsisWord, null, "EllipsisWord"));
    items.push(this.Item("item5", this.loc.PropertyEnum.StringTrimmingWord, null, "Word"));

    return items;
}

StiMobileDesigner.prototype.GetProcessAtItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiProcessAtNone, null, "None"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiProcessAtEndOfReport, null, "EndOfReport"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiProcessAtEndOfPage, null, "EndOfPage"));
    
    return items;
}

StiMobileDesigner.prototype.GetProcessingDuplicatesItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiProcessingDuplicatesTypeNone, null, "None"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiProcessingDuplicatesTypeMerge, null, "Merge"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiProcessingDuplicatesTypeHide, null, "Hide"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiProcessingDuplicatesTypeRemoveText, null, "RemoveText"));
    items.push(this.Item("item4", this.loc.PropertyEnum.StiProcessingDuplicatesTypeBasedOnTagMerge, null, "BasedOnTagMerge"));
    items.push(this.Item("item5", this.loc.PropertyEnum.StiProcessingDuplicatesTypeBasedOnTagHide, null, "BasedOnTagHide"));
    items.push(this.Item("item6", this.loc.PropertyEnum.StiProcessingDuplicatesTypeBasedOnTagRemoveText, null, "BasedOnTagRemoveText"));
    items.push(this.Item("item7", this.loc.PropertyEnum.StiProcessingDuplicatesTypeGlobalRemoveText, null, "GlobalRemoveText"));
    items.push(this.Item("item8", this.loc.PropertyEnum.StiProcessingDuplicatesTypeGlobalMerge, null, "GlobalMerge"));
    items.push(this.Item("item9", this.loc.PropertyEnum.StiProcessingDuplicatesTypeGlobalHide, null, "GlobalHide"));
    items.push(this.Item("item10", this.loc.PropertyEnum.StiProcessingDuplicatesTypeBasedOnValueRemoveText, null, "BasedOnValueRemoveText"));
    items.push(this.Item("item11", this.loc.PropertyEnum.StiProcessingDuplicatesTypeBasedOnValueAndTagMerge, null, "BasedOnValueAndTagMerge"));
    items.push(this.Item("item12", this.loc.PropertyEnum.StiProcessingDuplicatesTypeBasedOnValueAndTagHide, null, "BasedOnValueAndTagHide"));
    items.push(this.Item("item13", this.loc.PropertyEnum.StiProcessingDuplicatesTypeGlobalBasedOnValueAndTagMerge, null, "GlobalBasedOnValueAndTagMerge"));
    items.push(this.Item("item14", this.loc.PropertyEnum.StiProcessingDuplicatesTypeGlobalBasedOnValueAndTagHide, null, "GlobalBasedOnValueAndTagHide"));
    items.push(this.Item("item15", this.loc.PropertyEnum.StiProcessingDuplicatesTypeGlobalBasedOnValueRemoveText, null, "GlobalBasedOnValueRemoveText"));

    return items;
}

StiMobileDesigner.prototype.GetXmlTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", "ADO.NET XML", null, "AdoNetXml"));
    items.push(this.Item("item1", "XML", null, "Xml"));

    return items;
}

StiMobileDesigner.prototype.GetAutoSaveItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.FormOptions.Minutes.replace("{0}", "5"), null, "5"));
    items.push(this.Item("item1", this.loc.FormOptions.Minutes.replace("{0}", "10"), null, "10"));
    items.push(this.Item("item2", this.loc.FormOptions.Minutes.replace("{0}", "15"), null, "15"));
    items.push(this.Item("item3", this.loc.FormOptions.Minutes.replace("{0}", "20"), null, "20"));
    items.push(this.Item("item4", this.loc.FormOptions.Minutes.replace("{0}", "30"), null, "30"));
    items.push(this.Item("item5", this.loc.FormOptions.Minutes.replace("{0}", "60"), null, "60"));

    return items;
}

StiMobileDesigner.prototype.GetDBaseCodePageItems = function () {
    var items = [];
    for (var i = 0; i < this.options.dBaseCodePages.length; i++) {
        items.push(this.Item("item" + i, this.options.dBaseCodePages[i].name, null, this.options.dBaseCodePages[i].key.toString()));
    }

    return items;
}

StiMobileDesigner.prototype.GetCsvCodePageItems = function () {
    var items = [];
    for (var i = 0; i < this.options.csvCodePages.length; i++) {
        items.push(this.Item("item" + i, this.options.csvCodePages[i].name, null, this.options.csvCodePages[i].key.toString()));
    }

    return items;
}

StiMobileDesigner.prototype.GetCsvSeparatorItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.FormDictionaryDesigner.CsvSeparatorSystem, null, "System"));
    items.push(this.Item("item1", this.loc.FormDictionaryDesigner.CsvSeparatorTab, null, "Tab"));
    items.push(this.Item("item2", this.loc.FormDictionaryDesigner.CsvSeparatorSemicolon, null, "Semicolon"));
    items.push(this.Item("item3", this.loc.FormDictionaryDesigner.CsvSeparatorComma, null, "Comma"));
    items.push(this.Item("item4", this.loc.FormDictionaryDesigner.CsvSeparatorSpace, null, "Space"));
    items.push(this.Item("item5", this.loc.FormDictionaryDesigner.CsvSeparatorOther, null, "Other"));

    return items;
}

StiMobileDesigner.prototype.GetParameterTypeItems = function (parameterTypes) {    
    if (!parameterTypes) return [];
    var items = [];

    for (var i = 0; i < parameterTypes.length; i++) {
        items.push(this.Item("item" + i, parameterTypes[i].typeName, null, parameterTypes[i].typeValue.toString()));
    }
    return items;
}

StiMobileDesigner.prototype.GetLayoutAlignItems = function (isContextMenu) {
    var items = [];
    if (isContextMenu) {
        items.push(this.Item("AlignToGrid", this.loc.Toolbars.AlignToGrid, "ContextMenu.AlignToGrid.png", "AlignToGrid"));
        items.push("separator0");
    }
    items.push(this.Item("AlignLeft", this.loc.Toolbars.AlignLeft, "Layout.AlignLeft.png", "AlignLeft"));
    items.push(this.Item("AlignCenter", this.loc.Toolbars.AlignCenter, "Layout.AlignCenter.png", "AlignCenter"));
    items.push(this.Item("AlignRight", this.loc.Toolbars.AlignRight, "Layout.AlignRight.png", "AlignRight"));
    items.push("separator1");
    items.push(this.Item("AlignTop", this.loc.Toolbars.AlignTop, "Layout.AlignTop.png", "AlignTop"));
    items.push(this.Item("AlignMiddle", this.loc.Toolbars.AlignMiddle, "Layout.AlignMiddle.png", "AlignMiddle"));
    items.push(this.Item("AlignBottom", this.loc.Toolbars.AlignBottom, "Layout.AlignBottom.png", "AlignBottom"));
    items.push("separator2");
    items.push(this.Item("MakeHorizontalSpacingEqual", this.loc.Toolbars.MakeHorizontalSpacingEqual, "Layout.MakeHorizontalSpacingEqual.png", "MakeHorizontalSpacingEqual"));
    items.push(this.Item("MakeVerticalSpacingEqual", this.loc.Toolbars.MakeVerticalSpacingEqual, "Layout.MakeVerticalSpacingEqual.png", "MakeVerticalSpacingEqual"));
    items.push("separator3");
    items.push(this.Item("CenterHorizontally", this.loc.Toolbars.CenterHorizontally, "Layout.CenterHorizontally.png", "CenterHorizontally"));
    items.push(this.Item("CenterVertically", this.loc.Toolbars.CenterVertically, "Layout.CenterVertically.png", "CenterVertically"));

    return items;
}

StiMobileDesigner.prototype.GetRetrieveColumnsAndParametersItems = function () {
    var items = [];
    items.push(this.Item("retrieveColumnsAndParameters", this.loc.FormDictionaryDesigner.RetrieveColumnsAndParameters, null, "retrieveColumnsAndParameters"));
    items.push(this.Item("retrieveParameters", this.loc.FormDictionaryDesigner.RetrieveParameters, null, "retrieveParameters"));
    items.push("separator");    

    return items;
}

StiMobileDesigner.prototype.GetDecimalSeparatorItems = function () {
    var items = [];
    items.push(this.Item("item0", ".", null, "."));
    items.push(this.Item("item1", ",", null, ","));

    return items;
}

StiMobileDesigner.prototype.GetGroupSeparatorItems = function () {
    var items = [];
    items.push(this.Item("item0", "", null, ""));
    items.push(this.Item("item1", ".", null, "."));
    items.push(this.Item("item2", ",", null, ","));

    return items;
}

StiMobileDesigner.prototype.GetNumberNegativePatternItems = function () {
    var items = [];
    items.push(this.Item("item0", "(n)", null, 0));
    items.push(this.Item("item1", "-n", null, 1));
    items.push(this.Item("item2", "- n", null, 2));
    items.push(this.Item("item3", "n-", null, 3));
    items.push(this.Item("item4", "n -", null, 4));

    return items;
}

StiMobileDesigner.prototype.GetCurrencyPositivePatternItems = function () {
    var items = [];
    items.push(this.Item("item0", "$n", null, 0));
    items.push(this.Item("item1", "n$", null, 1));
    items.push(this.Item("item2", "$ n", null, 2));
    items.push(this.Item("item3", "n $", null, 3));

    return items;
}

StiMobileDesigner.prototype.GetCurrencyNegativePatternItems = function () {
    var items = [];
    items.push(this.Item("item0", "($n)", null, 0));
    items.push(this.Item("item1", "-$n", null, 1));
    items.push(this.Item("item2", "$-n", null, 2));
    items.push(this.Item("item3", "$n-", null, 3));
    items.push(this.Item("item4", "(n$)", null, 4));
    items.push(this.Item("item5", "-n$", null, 5));
    items.push(this.Item("item6", "n-$", null, 6));
    items.push(this.Item("item7", "n$-", null, 7));
    items.push(this.Item("item8", "-n $", null, 8));
    items.push(this.Item("item9", "n $-", null, 9));
    items.push(this.Item("item10", "$ n-", null, 10));
    items.push(this.Item("item11", "$ -n", null, 11));
    items.push(this.Item("item12", "n- $", null, 12));
    items.push(this.Item("item13", "($ n)", null, 13));
    items.push(this.Item("item14", "(n $)", null, 14));

    return items;
}

StiMobileDesigner.prototype.GetPercentagePositivePatternItems = function () {
    var items = [];
    items.push(this.Item("item0", "n %", null, 0));
    items.push(this.Item("item1", "n%", null, 1));
    items.push(this.Item("item2", "%n", null, 2));

    return items;
}

StiMobileDesigner.prototype.GetPercentageNegativePatternItems = function () {
    var items = [];
    items.push(this.Item("item0", "-n %", null, 0));
    items.push(this.Item("item1", "-n%", null, 1));
    items.push(this.Item("item2", "-%n", null, 2));

    return items;
}

StiMobileDesigner.prototype.GetPercentageSymbolItems = function () {
    var items = [];
    items.push(this.Item("item0", "", null, ""));
    items.push(this.Item("item1", "%", null, "%"));

    return items;
}

StiMobileDesigner.prototype.GetCurrencySymbolItems = function () {
    var items = [];
    for (var i = 0; i < this.options.currencySymbols.length; i++) {
        items.push(this.Item("item" + i, this.options.currencySymbols[i], null, this.options.currencySymbols[i]));
    }

    return items;
}

StiMobileDesigner.prototype.GetBooleanFormatItems = function () {
    var items = [];
    items.push(this.Item("item0", "False", null, "False"));
    items.push(this.Item("item1", "True", null, "True"));
    items.push(this.Item("item2", "No", null, "No"));
    items.push(this.Item("item3", "Yes", null, "Yes"));
    items.push(this.Item("item4", "Off", null, "Off"));
    items.push(this.Item("item5", "On", null, "On"));

    return items;
}

StiMobileDesigner.prototype.GetSortDirectionItemsForGroupsControl = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiSortDirectionAsc, null, "ASC"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiSortDirectionDesc, null, "DESC"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiSortDirectionNone, null, "NONE"));

    return items;
}

StiMobileDesigner.prototype.GetResultFunctionItems = function () {
    var items = [];
    items.push(this.Item("No", "No", null, "No"));
    items.push(this.Item("Sum", "Sum", null,"Sum"));
    items.push(this.Item("SumDistinct", "SumDistinct", null, "SumDistinct"));
    items.push(this.Item("Count", "Count", null, "Count"));
    items.push(this.Item("CountDistinct", "CountDistinct", null, "CountDistinct"));
    items.push(this.Item("Min", "Min", null, "Min"));
    items.push(this.Item("Max", "Max", null, "Max"));
    items.push(this.Item("MinDate", "MinDate", null, "MinDate"));
    items.push(this.Item("MaxDate", "MaxDate", null, "MaxDate"));
    items.push(this.Item("MinTime", "MinTime", null, "MinTime"));
    items.push(this.Item("MaxTime", "MaxTime", null, "MaxTime"));
    items.push(this.Item("MinStr", "MinStr", null, "MinStr"));
    items.push(this.Item("MaxStr", "MaxStr", null, "MaxStr"));
    items.push(this.Item("Median", "Median", null, "Median"));
    items.push(this.Item("Mode", "Mode", null, "Mode"));
    items.push(this.Item("Avg", "Avg", null, "Avg"));
    items.push(this.Item("First", "First", null, "First"));
    items.push(this.Item("Last", "Last", null, "Last"));

    return items;
}

StiMobileDesigner.prototype.GetDrillDownModeItems = function () {
    var items = [];
    items.push(this.Item("SinglePage", this.loc.PropertyEnum.StiDrillDownModeSinglePage, null, "SinglePage"));
    items.push(this.Item("MultiPage", this.loc.PropertyEnum.StiDrillDownModeMultiPage, null, "MultiPage"));

    return items;
}

StiMobileDesigner.prototype.GetHyperlinkTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.FormInteraction.HyperlinkUsingInteractionBookmark, null, "HyperlinkUsingInteractionBookmark"));
    items.push(this.Item("item1", this.loc.FormInteraction.HyperlinkUsingInteractionTag, null, "HyperlinkUsingInteractionTag"));
    items.push(this.Item("item2", this.loc.FormInteraction.HyperlinkExternalDocuments, null, "HyperlinkExternalDocuments"));

    return items;
}

StiMobileDesigner.prototype.GetDataBandsForInteractionSort = function () {
    if (!this.options.report) return null;
        
    var items = [];
    for (var componentName in this.options.currentPage.components) {
        var component = this.options.currentPage.components[componentName];
        if ((component.typeComponent == "StiTable" || component.typeComponent == "StiDataBand" || component.typeComponent == "StiHierarchicalBand" ||
                component.typeComponent == "StiCrossDataBand") && component.properties.dataSource  && component.properties.dataSource != "[Not Assigned]")
            items.push({ componentName: componentName, dataSourceName: component.properties.dataSource });
    }

    if (items.length == 0) return null;

    return items;
}

StiMobileDesigner.prototype.GetHotkeyPrefixItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.HotkeyPrefixNone, null, "None"));
    items.push(this.Item("item1", this.loc.PropertyEnum.HotkeyPrefixShow, null, "Show"));
    items.push(this.Item("item2", this.loc.PropertyEnum.HotkeyPrefixHide, null, "Hide"));

    return items;
}

StiMobileDesigner.prototype.GetTextQualityItems = function () {
    var items = [];
    items.push(this.Item("item0", "Standard", null, "Standard"));
    items.push(this.Item("item1", "Typographic", null, "Typographic"));
    items.push(this.Item("item2", "Wysiwyg", null, "Wysiwyg"));

    return items;
}

StiMobileDesigner.prototype.GetSortDirectionForCrossTabField = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiSortDirectionAsc, null, "Asc"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiSortDirectionDesc, null, "Desc"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiSortDirectionNone, null, "None"));

    return items;
}

StiMobileDesigner.prototype.GetSortType = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiSortTypeByValue, null, "ByValue"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiSortTypeByDisplayValue, null, "ByDisplayValue"));

    return items;
}

StiMobileDesigner.prototype.GetEnumeratorTypeItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiEnumeratorTypeNone, null, "None"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiEnumeratorTypeArabic, null, "Arabic"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiEnumeratorTypeRoman, null, "Roman"));
    items.push(this.Item("item3", this.loc.PropertyEnum.StiEnumeratorTypeABC, null, "ABC"));

    return items;
}

StiMobileDesigner.prototype.GetSummaryValuesItems = function () {
    var items = [];
    items.push(this.Item("item0", this.loc.PropertyEnum.StiSummaryValuesAllValues, null, "AllValues"));
    items.push(this.Item("item1", this.loc.PropertyEnum.StiSummaryValuesSkipZerosAndNulls, null, "SkipZerosAndNulls"));
    items.push(this.Item("item2", this.loc.PropertyEnum.StiSummaryValuesSkipNulls, null, "SkipNulls"));
    
    return items;
}

StiMobileDesigner.prototype.GetSummaryTypeForCrossTabFiledItems = function () {
    var items = [];
    items.push(this.Item("item0", "None", null, "None"));
    items.push(this.Item("item1", "Sum", null, "Sum"));
    items.push(this.Item("item3", "Average", null, "Average"));
    items.push(this.Item("item4", "Min", null, "Min"));
    items.push(this.Item("item5", "Max", null, "Max"));
    items.push(this.Item("item6", "Count", null, "Count"));
    items.push(this.Item("item7", "CountDistinct", null, "CountDistinct"));
    items.push(this.Item("item8", "Image", null, "Image"));

    return items;
}

StiMobileDesigner.prototype.GetGaugeTypeItems = function () {
    var items = [];
    items.push(this.Item("StiRadialScale", this.loc.Gauge.RadialScale, "Gauge.Big.RadialScale.png", "StiRadialScale"));
    items.push(this.Item("StiLinearScale", this.loc.Gauge.LinearScale, "Gauge.Big.LinearScale.png", "StiLinearScale"));

    return items;
}

StiMobileDesigner.prototype.GetCellTypeItems = function () {
    var items = [];
    items.push(this.Item("Text", this.loc.PropertyEnum.StiTablceCellTypeText, null, "Text"));
    items.push(this.Item("Image", this.loc.PropertyEnum.StiTablceCellTypeImage, null, "Image"));
    items.push(this.Item("CheckBox", this.loc.PropertyEnum.StiTablceCellTypeCheckBox, null, "CheckBox"));
    if (!this.options.isJava && !this.options.jsMode){
       items.push(this.Item("RichText", this.loc.PropertyEnum.StiTablceCellTypeRichText, null, "RichText"));
    }
    
    return items;
}

StiMobileDesigner.prototype.GetCellTypeItems = function () {
    var items = [];
    items.push(this.Item("Text", this.loc.PropertyEnum.StiTablceCellTypeText, null, "Text"));
    items.push(this.Item("Image", this.loc.PropertyEnum.StiTablceCellTypeImage, null, "Image"));
    items.push(this.Item("CheckBox", this.loc.PropertyEnum.StiTablceCellTypeCheckBox, null, "CheckBox"));
    if (!this.options.isJava){
       items.push(this.Item("RichText", this.loc.PropertyEnum.StiTablceCellTypeRichText, null, "RichText")); 
    }

    return items;
}

StiMobileDesigner.prototype.GetCellDockStyleItems = function () {
    var items = [];
    items.push(this.Item("Left", this.loc.PropertyEnum.StiDockStyleLeft, null, "Left"));
    items.push(this.Item("Right", this.loc.PropertyEnum.StiDockStyleRight, null, "Right"));
    items.push(this.Item("Top", this.loc.PropertyEnum.StiDockStyleTop, null, "Top"));
    items.push(this.Item("Bottom", this.loc.PropertyEnum.StiDockStyleBottom, null, "Bottom"));
    items.push(this.Item("None", this.loc.PropertyEnum.StiDockStyleNone, null, "None"));
    items.push(this.Item("Fill", this.loc.PropertyEnum.StiDockStyleFill, null, "Fill"));
    
    return items;
}

StiMobileDesigner.prototype.GetTableContextSubMenuItems = function () {
    var items = [];
    items.push(this.Item("joinCells", this.loc.MainMenu.menuJoinCells, "ContextMenu.JoinCells.png", "joinCells"));
    items.push("separator0");
    items.push(this.Item("insertColumnToLeft", this.loc.MainMenu.menuInsertColumnToLeft, "ContextMenu.InsertColumnToLeft.png", "insertColumnToLeft"));    
    items.push(this.Item("insertColumnToRight", this.loc.MainMenu.menuInsertColumnToRight, "ContextMenu.InsertColumnToRight.png", "insertColumnToRight"));
    items.push(this.Item("deleteColumn", this.loc.MainMenu.menuDeleteColumn, "ContextMenu.DeleteColumn.png", "deleteColumn"));
    //items.push(this.Item("selectColumn", this.loc.MainMenu.menuSelectColumn, "ContextMenu.SelectColumn.png", "selectColumn"));
    items.push("separator1");
    items.push(this.Item("insertRowAbove", this.loc.MainMenu.menuInsertRowAbove, "ContextMenu.InsertRowAbove.png", "insertRowAbove"));
    items.push(this.Item("insertRowBelow", this.loc.MainMenu.menuInsertRowBelow, "ContextMenu.InsertRowBelow.png", "insertRowBelow"));
    items.push(this.Item("deleteRow", this.loc.MainMenu.menuDeleteRow, "ContextMenu.DeleteRow.png", "deleteRow"));
    //items.push(this.Item("selectRow", this.loc.MainMenu.menuSelectRow, "ContextMenu.SelectRow.png", "selectRow"));
    items.push("separator2");
    items.push(this.Item("convertToText", this.loc.MainMenu.menuConvertToText, "ContextMenu.StiText.png", "convertToText"));
    items.push(this.Item("convertToImage", this.loc.MainMenu.menuConvertToImage, "ContextMenu.StiImage.png", "convertToImage"));
    items.push(this.Item("convertToCheckBox", this.loc.MainMenu.menuConvertToCheckBox, "ContextMenu.StiCheckBox.png", "convertToCheckBox"));
    if (!this.options.isJava){ 
	    items.push(this.Item("convertToRichText", this.loc.MainMenu.MenuConvertToRichText, "ContextMenu.StiRichText.png", "convertToRichText"));
    }

    return items;
}

StiMobileDesigner.prototype.GetOrderContextSubMenuItems = function () {
    var items = [];
    items.push(this.Item("BringToFront", this.loc.Toolbars.BringToFront, "ContextMenu.BringToFront.png", "BringToFront"));
    items.push(this.Item("SendToBack", this.loc.Toolbars.SendToBack, "ContextMenu.SendToBack.png", "SendToBack"));
    items.push(this.Item("MoveForward", this.loc.Toolbars.MoveForward, "ContextMenu.MoveForward.png", "MoveForward"));
    items.push(this.Item("MoveBackward", this.loc.Toolbars.MoveBackward, "ContextMenu.MoveBackward.png", "MoveBackward"));

    return items;
}

StiMobileDesigner.prototype.GetTableAutoWidthItems = function () {
    var items = [];
    items.push(this.Item("None", this.loc.PropertyEnum.StiTableAutoWidthNone, null, "None"));
    items.push(this.Item("Page", this.loc.PropertyEnum.StiTableAutoWidthPage, null, "Page"));
    items.push(this.Item("Table", this.loc.PropertyEnum.StiTableAutoWidthTable, null, "Table"));

    return items;
}

StiMobileDesigner.prototype.GetTableAutoWidthTypeItems = function () {
    var items = [];
    items.push(this.Item("None", this.loc.PropertyEnum.StiTableAutoWidthTypeNone, null, "None"));
    items.push(this.Item("LastColumns", this.loc.PropertyEnum.StiTableAutoWidthTypeLastColumns, null, "LastColumns"));
    items.push(this.Item("FullTable", this.loc.PropertyEnum.StiTableAutoWidthTypeFullTable, null, "FullTable"));

    return items;
}

StiMobileDesigner.prototype.GetPrintOnEvenOddPagesItems = function () {
    var items = [];
    items.push(this.Item("Ignore", this.loc.PropertyEnum.StiPrintOnEvenOddPagesTypeIgnore, null, "Ignore"));
    items.push(this.Item("PrintOnEvenPages", this.loc.PropertyEnum.StiPrintOnEvenOddPagesTypePrintOnEvenPages, null, "PrintOnEvenPages"));
    items.push(this.Item("PrintOnOddPages", this.loc.PropertyEnum.StiPrintOnEvenOddPagesTypePrintOnOddPages, null, "PrintOnOddPages"));

    return items;
}

StiMobileDesigner.prototype.GetVariableSelectionItems = function () {
    var items = [];
    items.push(this.Item("FromVariable", this.loc.PropertyEnum.StiSelectionModeFromVariable, null, "FromVariable"));
    items.push(this.Item("Nothing", this.loc.PropertyEnum.StiSelectionModeNothing, null, "Nothing"));
    items.push(this.Item("First", this.loc.PropertyEnum.StiSelectionModeFirst, null, "First"));

    return items;
}

StiMobileDesigner.prototype.GetWatermarkEnabledItems = function () {
    var items = [];
    items.push(this.Item("True", this.loc.PropertyEnum.boolTrue, null, "True"));
    items.push(this.Item("False", this.loc.PropertyEnum.boolFalse, null, "False"));
    items.push(this.Item("Expression", this.loc.PropertyMain.Expression, null, "Expression"));

    return items;
}

StiMobileDesigner.prototype.GetColumnDirectionItems = function () {
    var items = [];
    items.push(this.Item("DownThenAcross", this.loc.PropertyEnum.StiColumnDirectionDownThenAcross, null, "DownThenAcross"));
    items.push(this.Item("AcrossThenDown", this.loc.PropertyEnum.StiColumnDirectionAcrossThenDown, null, "AcrossThenDown"));

    return items;
}

StiMobileDesigner.prototype.GetLayoutSizeItems = function () {
    var items = [];
    items.push(this.Item("MakeSameSize", this.loc.Toolbars.MakeSameSize, "Layout.MakeSameSize.png", "MakeSameSize"));
    items.push(this.Item("MakeSameWidth", this.loc.Toolbars.MakeSameWidth, "Layout.MakeSameWidth.png", "MakeSameWidth"));
    items.push(this.Item("MakeSameHeight", this.loc.Toolbars.MakeSameHeight, "Layout.MakeSameHeight.png", "MakeSameHeight"));

    return items;
}

StiMobileDesigner.prototype.GetDockStyleItems = function () {
    var items = [];
    items.push(this.Item("Left", this.loc.PropertyEnum.StiDockStyleLeft, null, "Left"));
    items.push(this.Item("Right", this.loc.PropertyEnum.StiDockStyleRight, null, "Right"));
    items.push(this.Item("Top", this.loc.PropertyEnum.StiDockStyleTop, null, "Top"));
    items.push(this.Item("Bottom", this.loc.PropertyEnum.StiDockStyleBottom, null, "Bottom"));
    items.push(this.Item("Fill", this.loc.PropertyEnum.StiDockStyleFill, null, "Fill"));
    items.push(this.Item("None", this.loc.PropertyEnum.StiDockStyleNone, null, "None"));

    return items;
}

StiMobileDesigner.prototype.GetCultureItems = function () {
    var items = [];
    items.push(this.Item("none", " ", null, ""));

    for (var i = 0; i < this.options.cultures.length; i++)
        items.push(this.Item(i.toString(), this.options.cultures[i].displayName, null, this.options.cultures[i].name));

    return items;
}

StiMobileDesigner.prototype.GetAddGaugeItems = function () {
    var items = [];
    var radialScaleItem = this.Item("ScaleRadial", this.loc.Gauge.RadialScale, "Gauge.Big.RadialScale.png", "StiRadialScale")
    radialScaleItem.haveSubMenu = true;
    items.push(radialScaleItem);
    var linearScaleItem = this.Item("ScaleLinear", this.loc.Gauge.LinearScale, "Gauge.Big.LinearScale.png", "StiLinearScale")
    linearScaleItem.haveSubMenu = true;
    items.push(linearScaleItem);
    items.push("separator1");
    items.push(this.Item("ChildElementRadialRange", this.loc.Gauge.RadialRange, "Gauge.Big.RadialRangeList.png", "StiRadialRange"));
    items.push(this.Item("ChildElementLinearRange", this.loc.Gauge.LinearRange, "Gauge.Big.LinearRangeList.png", "StiLinearRange"));
    items.push("separator2");
    items.push(this.Item("ChildElementTickCustomValue", this.loc.Gauge.TickCustomValue, "Gauge.Big.TickCustomValue.png", "StiTickCustomValue"));
    items.push("separator3");
    items.push(this.Item("ChildElementStateIndicatorFilter", this.loc.Gauge.StateIndicatorFilter, "Gauge.Big.StateIndicatorFilter.png", "StiStateIndicatorFilter"));
    items.push("separator4");
    items.push(this.Item("ChildElementBarRangeList", this.loc.Gauge.BarRangeList, "Gauge.Big.BarRangeList.png", "StiBarRangeList"));

    return items;
}

StiMobileDesigner.prototype.GetGaugeRadialScaleItems = function () {
    var items = [];

    items.push(this.Item("ScaleRadial", this.loc.Gauge.RadialScale, "Gauge.Big.RadialScale.png", "StiRadialScale"));
    items.push("separator1");
    items.push(this.Item("ElementRadialTickMarkMajor", this.loc.Gauge.RadialTickMarkMajor, "Gauge.Big.RadialTickMarkMajor.png", "StiRadialTickMarkMajor"));
    items.push(this.Item("ElementRadialTickMarkMinor", this.loc.Gauge.RadialTickMarkMinor, "Gauge.Big.RadialTickMarkMinor.png", "StiRadialTickMarkMinor"));
    items.push(this.Item("ElementRadialTickMarkCustom", this.loc.Gauge.RadialTickMarkCustom, "Gauge.Big.RadialTickMarkCustom.png", "StiRadialTickMarkCustom"));
    items.push(this.Item("ElementRadialTickLabelMajor", this.loc.Gauge.RadialTickLabelMajor, "Gauge.Big.RadialTickLabelMajor.png", "StiRadialTickLabelMajor"));
    items.push(this.Item("ElementRadialTickLabelMinor", this.loc.Gauge.RadialTickLabelMinor, "Gauge.Big.RadialTickLabelMinor.png", "StiRadialTickLabelMinor"));
    items.push(this.Item("ElementRadialTickLabelCustom", this.loc.Gauge.RadialTickLabelCustom, "Gauge.Big.RadialTickLabelCustom.png", "StiRadialTickLabelCustom"));
    items.push("separator2");
    items.push(this.Item("ElementRadialRangeList", this.loc.Gauge.RadialRangeList, "Gauge.Big.RadialRangeList.png", "StiRadialRangeList"));
    items.push("separator3")
    items.push(this.Item("ElementRadialBar", this.loc.Gauge.RadialBar, "Gauge.Big.RadialBar.png", "StiRadialBar"));
    items.push(this.Item("ElementRadialMarker", this.loc.Gauge.RadialMarker, "Gauge.Big.RadialMarker.png", "StiRadialMarker"));
    items.push(this.Item("ElementNeedle", this.loc.Gauge.Needle, "Gauge.Big.Needle.png", "StiNeedle"));
    items.push("separator4");
    items.push(this.Item("ElementRadialStateIndicator", this.loc.Gauge.StateIndicator, "Gauge.Big.RadialStateIndicator.png", "StiRadialStateIndicator"));

    return items;
}

StiMobileDesigner.prototype.GetGaugeLinearScaleItems = function () {
    var items = [];

    items.push(this.Item("ScaleLinear", this.loc.Gauge.LinearScale, "Gauge.Big.LinearScale.png", "StiLinearScale"));
    items.push("separator1");
    items.push(this.Item("ElementLinearTickMarkMajor", this.loc.Gauge.LinearTickMarkMajor, "Gauge.Big.LinearTickMarkMajor.png", "StiLinearTickMarkMajor"));
    items.push(this.Item("ElementLinearTickMarkMinor", this.loc.Gauge.LinearTickMarkMinor, "Gauge.Big.LinearTickMarkMinor.png", "StiLinearTickMarkMinor"));
    items.push(this.Item("ElementLinearTickMarkCustom", this.loc.Gauge.LinearTickMarkCustom, "Gauge.Big.LinearTickMarkCustom.png", "StiLinearTickMarkCustom"));
    items.push(this.Item("ElementLinearTickLabelMajor", this.loc.Gauge.LinearTickLabelMajor, "Gauge.Big.LinearTickLabelMajor.png", "StiLinearTickLabelMajor"));
    items.push(this.Item("ElementLinearTickLabelMinor", this.loc.Gauge.LinearTickLabelMinor, "Gauge.Big.LinearTickLabelMinor.png", "StiLinearTickLabelMinor"));
    items.push(this.Item("ElementLinearTickLabelCustom", this.loc.Gauge.LinearTickLabelCustom, "Gauge.Big.LinearTickLabelCustom.png", "StiLinearTickLabelCustom"));
    items.push("separator2");
    items.push(this.Item("ElementLinearRangeList", this.loc.Gauge.LinearRangeList, "Gauge.Big.LinearRangeList.png", "StiLinearRangeList"));
    items.push("separator3");
    items.push(this.Item("ElementLinearBar", this.loc.Gauge.LinearBar, "Gauge.Big.LinearBar.png", "StiLinearBar"));
    items.push(this.Item("ElementLinearMarker", this.loc.Gauge.LinearMarker, "Gauge.Big.LinearMarker.png", "StiLinearMarker"));
    items.push("separator4");
    items.push(this.Item("ElementLinearStateIndicator", this.loc.Gauge.StateIndicator, "Gauge.Big.LinearStateIndicator.png", "StiLinearStateIndicator"));

    return items;
}

StiMobileDesigner.prototype.GetFontNamesItems = function () {
    var items = [];
    for (var i = 0; i < this.options.fontNames.length; i++)
        items.push(this.Item("fontItem" + i, this.options.fontNames[i].value, null, this.options.fontNames[i].value));

    return items;
}

StiMobileDesigner.prototype.GetBorderPrimitiveCapStyleItems = function () {
    var items = [];
    items.push(this.Item("None", this.loc.PropertyEnum.StiCapStyleDiamond, null, "None"));
    items.push(this.Item("Arrow", this.loc.PropertyEnum.StiCapStyleArrow, null, "Arrow"));
    items.push(this.Item("Open", this.loc.PropertyEnum.StiCapStyleOpen, null, "Open"));
    items.push(this.Item("Stealth", this.loc.PropertyEnum.StiCapStyleStealth, null, "Stealth"));
    items.push(this.Item("Diamond", this.loc.PropertyEnum.StiCapStyleDiamond, null, "Diamond"));
    items.push(this.Item("Square", this.loc.PropertyEnum.StiCapStyleSquare, null, "Square"));
    items.push(this.Item("Oval", this.loc.PropertyEnum.StiCapStyleOval, null, "Oval"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeChecksumItems = function () {
    var items = [];
    items.push(this.Item("None", this.loc.PropertyEnum.StiCode11CheckSumNone, null, "None"));
    items.push(this.Item("OneDigit", this.loc.PropertyEnum.StiCode11CheckSumOneDigit, null, "OneDigit"));
    items.push(this.Item("TwoDigits", this.loc.PropertyEnum.StiCode11CheckSumTwoDigits, null, "TwoDigits"));
    items.push(this.Item("Auto", this.loc.PropertyEnum.StiCode11CheckSumAuto, null, "Auto"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeCheckSumItems = function () {
    var items = [];
    items.push(this.Item("Yes", this.loc.PropertyEnum.StiCheckSumYes, null, "Yes"));
    items.push(this.Item("No", this.loc.PropertyEnum.StiCheckSumNo, null, "No"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodePlesseyCheckSumItems = function () {
    var items = [];
    items.push(this.Item("None", this.loc.PropertyEnum.StiPlesseyCheckSumNone, null, "None"));
    items.push(this.Item("Modulo10", this.loc.PropertyEnum.StiPlesseyCheckSumModulo10, null, "Modulo10"));
    items.push(this.Item("Modulo11", this.loc.PropertyEnum.StiPlesseyCheckSumModulo11, null, "Modulo11"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeEncodingTypeItems = function () {
    var items = [];
    items.push(this.Item("Ascii", "Ascii", null, "Ascii"));
    items.push(this.Item("C40", "C40", null, "C40"));
    items.push(this.Item("Text", "Text", null, "Text"));
    items.push(this.Item("X12", "X12", null, "X12"));
    items.push(this.Item("Edifact", "Edifact", null, "Edifact"));
    items.push(this.Item("Binary", "Binary", null, "Binary"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeMatrixSizeDataMatrixItems = function () {
    var items = [];
    items.push(this.Item("Automatic", "Automatic", null, "Automatic"));
    items.push(this.Item("s10x10", "s10x10", null, "s10x10"));
    items.push(this.Item("s12x12", "s12x12", null, "s12x12"));
    items.push(this.Item("s8x18", "s8x18", null, "s8x18"));
    items.push(this.Item("s14x14", "s14x14", null, "s14x14"));
    items.push(this.Item("s8x32", "s8x32", null, "s8x32"));
    items.push(this.Item("s16x16", "s16x16", null, "s16x16"));
    items.push(this.Item("s12x26", "s12x26", null, "s12x26"));
    items.push(this.Item("s18x18", "s18x18", null, "s18x18"));
    items.push(this.Item("s20x20", "s20x20", null, "s20x20"));
    items.push(this.Item("s12x36", "s12x36", null, "s12x36"));
    items.push(this.Item("s22x22", "s22x22", null, "s22x22"));
    items.push(this.Item("s16x36", "s16x36", null, "s16x36"));
    items.push(this.Item("s24x24", "s24x24", null, "s24x24"));
    items.push(this.Item("s26x26", "s26x26", null, "s26x26"));
    items.push(this.Item("s16x48", "s16x48", null, "s16x48"));
    items.push(this.Item("s32x32", "s32x32", null, "s32x32"));
    items.push(this.Item("s36x36", "s36x36", null, "s36x36"));
    items.push(this.Item("s40x40", "s40x40", null, "s40x40"));
    items.push(this.Item("s44x44", "s44x44", null, "s44x44"));
    items.push(this.Item("s48x48", "s48x48", null, "s48x48"));
    items.push(this.Item("s52x52", "s52x52", null, "s52x52"));
    items.push(this.Item("s64x64", "s64x64", null, "s64x64"));
    items.push(this.Item("s72x72", "s72x72", null, "s72x72"));
    items.push(this.Item("s80x80", "s80x80", null, "s80x80"));
    items.push(this.Item("s88x88", "s88x88", null, "s88x88"));
    items.push(this.Item("s96x96", "s96x96", null, "s96x96"));
    items.push(this.Item("s104x104", "s104x104", null, "s104x104"));
    items.push(this.Item("s120x120", "s120x120", null, "s120x120"));
    items.push(this.Item("s132x132", "s132x132", null, "s132x132"));
    items.push(this.Item("s144x144", "s144x144", null, "s144x144"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeSupplementTypeItems = function () {
    var items = [];
    items.push(this.Item("None", this.loc.PropertyEnum.StiEanSupplementTypeNone, null, "None"));
    items.push(this.Item("TwoDigit", this.loc.PropertyEnum.StiEanSupplementTypeTwoDigit, null, "TwoDigit"));
    items.push(this.Item("FiveDigit", this.loc.PropertyEnum.StiEanSupplementTypeFiveDigit, null, "FiveDigit"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeEncodingModeItems = function () {
    var items = [];
    items.push(this.Item("Text", "Text", null, "Text"));
    items.push(this.Item("Numeric", "Numeric", null, "Numeric"));
    items.push(this.Item("Byte", "Byte", null, "Byte"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeErrorsCorrectionLevelItems = function () {
    var items = [];
    items.push(this.Item("Automatic", "Automatic", null, "Automatic"));
    for (var i = 0; i <= 8; i++) {
        items.push(this.Item("Level" + i, "Level" + i, null, "Level" + i));
    }

    return items;
}

StiMobileDesigner.prototype.GetBarCodeErrorCorrectionLevelItems = function () {
    var items = [];
    for (var i = 1; i <= 4; i++) {
        items.push(this.Item("Level" + i, "Level" + i, null, "Level" + i));
    }

    return items;
}

StiMobileDesigner.prototype.GetBarCodeMatrixSizeQRCodeItems = function () {
    var items = [];
    items.push(this.Item("Automatic", "Automatic", null, "Automatic"));
    for (var i = 1; i <= 40; i++) {
        items.push(this.Item("v" + i, "v" + i, null, "v" + i));
    }

    return items;
}

StiMobileDesigner.prototype.GetInterfaceTypeItems = function () {
    var items = [];
    items.push(this.Item("mouse", "<b>" + this.loc.Interface.Mouse + "</b><br>" + this.loc.Interface.MouseDescription, "InterfaceMenu.MouseInterface.png", "Mouse"));
    items.push(this.Item("touch", "<b>" + this.loc.Interface.Touch + "</b><br>" + this.loc.Interface.TouchDescription, "InterfaceMenu.TouchInterface.png", "Touch"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeModeItems = function () {
    var items = [];
    items.push(this.Item("Mode2", "Mode2", null, "Mode2"));
    items.push(this.Item("Mode3", "Mode3", null, "Mode3"));
    items.push(this.Item("Mode4", "Mode4", null, "Mode4"));
    items.push(this.Item("Mode5", "Mode5", null, "Mode5"));
    items.push(this.Item("Mode6", "Mode6", null, "Mode6"));

    return items;
}

StiMobileDesigner.prototype.GetAllComponentsItems = function (sort) {
    var items = [];
    if (this.options.report) {
        var repName = Base64.decode(this.options.report.properties.reportName.replace("Base64Code;", ""));
        var repText = repName + " : " + this.loc.Components.StiReport;
        items.push(this.Item(repName, repText, null, repText));
        
        for (var pageName in this.options.report.pages) {
            var pageText = pageName + " : " + this.loc.Components.StiPage;
            items.push(this.Item(pageName, pageText, null, pageText));

            for (var componentName in this.options.report.pages[pageName].components) {
                var component = this.options.report.pages[pageName].components[componentName];
                var compText = componentName;
                var componentType = "";

                if (component.properties.cellType) {
                    componentType = this.loc.Components["Sti" + component.properties.cellType];
                }
                else {
                    componentType = this.loc.Components[component.typeComponent];
                }

                if (componentType) compText += " : " + componentType;
                items.push(this.Item(componentName, compText, null, compText));

                if (component.typeComponent == "StiCrossTab") {
                    var crossTabChilds = component.controls.crossTabContainer.childNodes;
                    for (var i = 0; i < crossTabChilds.length; i++) {
                        var fieldType = this.loc.Components[crossTabChilds[i].properties.typeCrossField];
                        var fieldName = crossTabChilds[i].properties.name;
                        var fieldText = crossTabChilds[i].properties.name;
                        if (fieldType) fieldText += " : " + fieldType;
                        items.push(this.Item(fieldName, fieldText, null, ["StiCrossField", componentName, fieldName]));
                    }
                }
            }
        }
    }

    if (sort) {
        items.sort(this.SortByCaption);
    }

    return items;
}

StiMobileDesigner.prototype.GetBarCodeCategoriesItems = function () {
    var items = [];
    items.push(this.Item("TwoDimensional", this.loc.BarCode.TwoDimensional, "BarCodes.StiQRCodeBarCodeType.png", "TwoDimensional", null, true));
    items.push(this.Item("EANUPC", "EAN\UPC", "BarCodes.StiCodabarBarCodeType.png", "EANUPC", null, true));
    items.push(this.Item("GS1", "GS1", "BarCodes.StiCodabarBarCodeType.png", "GS1", null, true));
    items.push(this.Item("Post", this.loc.BarCode.Post, "BarCodes.StiAustraliaPost4StateBarCodeType.png", "Post", null, true));
    items.push(this.Item("Others", this.loc.FormDesigner.Others, "BarCodes.StiCodabarBarCodeType.png", "Others", null, true));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeTwoDimensionalItems = function () {
    var items = [];
    items.push(this.Item("StiQRCodeBarCodeType", "QR Code", "BarCodes.StiQRCodeBarCodeType.png", "StiQRCodeBarCodeType"));
    items.push(this.Item("StiDataMatrixBarCodeType", "DataMatrix", "BarCodes.StiDataMatrixBarCodeType.png", "StiDataMatrixBarCodeType"));
    items.push(this.Item("StiMaxicodeBarCodeType", "Maxicode", "BarCodes.StiMaxicodeBarCodeType.png", "StiMaxicodeBarCodeType"));
    items.push(this.Item("StiPdf417BarCodeType", "Pdf417", "BarCodes.StiPdf417BarCodeType.png", "StiPdf417BarCodeType"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeEANUPCItems = function () {
    var items = [];
    items.push(this.Item("StiEAN128aBarCodeType", "EAN-128a", "BarCodes.StiCodabarBarCodeType.png", "StiEAN128aBarCodeType"));
    items.push(this.Item("StiEAN128bBarCodeType", "EAN-128b", "BarCodes.StiCodabarBarCodeType.png", "StiEAN128bBarCodeType"));
    items.push(this.Item("StiEAN128cBarCodeType", "EAN-128c", "BarCodes.StiCodabarBarCodeType.png", "StiEAN128cBarCodeType"));
    items.push(this.Item("StiEAN128AutoBarCodeType", "EAN-128 Auto", "BarCodes.StiCodabarBarCodeType.png", "StiEAN128AutoBarCodeType"));
    items.push(this.Item("StiEAN13BarCodeType", "EAN-13", "BarCodes.StiEANBarCodeType.png", "StiEAN13BarCodeType"));
    items.push(this.Item("StiEAN8BarCodeType", "EAN-8", "BarCodes.StiEANBarCodeType.png", "StiEAN8BarCodeType"));
    items.push(this.Item("StiUpcABarCodeType", "UPC-A", "BarCodes.StiCodabarBarCodeType.png", "StiUpcABarCodeType"));
    items.push(this.Item("StiUpcEBarCodeType", "UPC-E", "BarCodes.StiCodabarBarCodeType.png", "StiUpcEBarCodeType"));
    items.push(this.Item("StiUpcSup2BarCodeType", "UPC-Supp2", "BarCodes.StiCodabarBarCodeType.png", "StiUpcSup2BarCodeType"));
    items.push(this.Item("StiUpcSup5BarCodeType", "UPC-Supp5", "BarCodes.StiCodabarBarCodeType.png", "StiUpcSup5BarCodeType"));
    items.push(this.Item("StiJan13BarCodeType", "JAN-13", "BarCodes.StiEANBarCodeType.png", "StiJan13BarCodeType"));
    items.push(this.Item("StiJan8BarCodeType", "JAN-8", "BarCodes.StiEANBarCodeType.png", "StiJan8BarCodeType"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeGS1Items = function () {
    var items = [];
    if (!this.options.jsMode) {
        items.push(this.Item("StiGS1_128BarCodeType", "GS1-128", "BarCodes.StiCodabarBarCodeType.png", "StiGS1_128BarCodeType"));
    }
    items.push(this.Item("StiSSCC18BarCodeType", "SSCC", "BarCodes.StiCodabarBarCodeType.png", "StiSSCC18BarCodeType"));
    items.push(this.Item("StiITF14BarCodeType", "ITF-14", "BarCodes.StiITF14BarCodeType.png", "StiITF14BarCodeType"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodePostItems = function () {
    var items = [];
    items.push(this.Item("StiAustraliaPost4StateBarCodeType", "Australia Post 4-state", "BarCodes.StiAustraliaPost4StateBarCodeType.png", "StiAustraliaPost4StateBarCodeType"));
    items.push(this.Item("StiPostnetBarCodeType", "Postnet", "BarCodes.StiPostnetBarCodeType.png", "StiPostnetBarCodeType"));
    items.push(this.Item("StiDutchKIXBarCodeType", "Royal TPG Post KIX 4-State", "BarCodes.StiAustraliaPost4StateBarCodeType.png", "StiDutchKIXBarCodeType"));
    items.push(this.Item("StiRoyalMail4StateBarCodeType", "Royal Mail 4-state", "BarCodes.StiAustraliaPost4StateBarCodeType.png", "StiRoyalMail4StateBarCodeType"));
    items.push(this.Item("StiFIMBarCodeType", "FIM", "BarCodes.StiFIMBarCodeType.png", "StiFIMBarCodeType"));

    return items;
}

StiMobileDesigner.prototype.GetBarCodeOthersItems = function () {
    var items = [];
    items.push(this.Item("StiPharmacodeBarCodeType", "Pharmacode", "BarCodes.StiCodabarBarCodeType.png", "StiPharmacodeBarCodeType"));
    items.push(this.Item("StiCode11BarCodeType", "Code11", "BarCodes.StiCodabarBarCodeType.png", "StiCode11BarCodeType"));
    items.push(this.Item("StiCode128aBarCodeType", "Code128a", "BarCodes.StiCodabarBarCodeType.png", "StiCode128aBarCodeType"));
    items.push(this.Item("StiCode128bBarCodeType", "Code128b", "BarCodes.StiCodabarBarCodeType.png", "StiCode128bBarCodeType"));
    items.push(this.Item("StiCode128cBarCodeType", "Code128c", "BarCodes.StiCodabarBarCodeType.png", "StiCode128cBarCodeType"));
    items.push(this.Item("StiCode128AutoBarCodeType", "Code128 Auto", "BarCodes.StiCodabarBarCodeType.png", "StiCode128AutoBarCodeType"));
    items.push(this.Item("StiCode39BarCodeType", "Code39", "BarCodes.StiCodabarBarCodeType.png", "StiCode39BarCodeType"));
    items.push(this.Item("StiCode39ExtBarCodeType", "Code39 Extended", "BarCodes.StiCodabarBarCodeType.png", "StiCode39ExtBarCodeType"));
    items.push(this.Item("StiCode93BarCodeType", "Code93", "BarCodes.StiCodabarBarCodeType.png", "StiCode93BarCodeType"));
    items.push(this.Item("StiCode93ExtBarCodeType", "Code93 Extended", "BarCodes.StiCodabarBarCodeType.png", "StiCode93ExtBarCodeType"));
    items.push(this.Item("StiCodabarBarCodeType", "Codabar", "BarCodes.StiCodabarBarCodeType.png", "StiCodabarBarCodeType"));
    items.push(this.Item("StiIsbn10BarCodeType", "ISBN-10", "BarCodes.StiEANBarCodeType.png", "StiIsbn10BarCodeType"));
    items.push(this.Item("StiIsbn13BarCodeType", "ISBN-13", "BarCodes.StiEANBarCodeType.png", "StiIsbn13BarCodeType"));
    items.push(this.Item("StiMsiBarCodeType", "Msi", "BarCodes.StiCodabarBarCodeType.png", "StiMsiBarCodeType"));
    items.push(this.Item("StiPlesseyBarCodeType", "Plessey", "BarCodes.StiCodabarBarCodeType.png", "StiPlesseyBarCodeType"));
    items.push(this.Item("StiInterleaved2of5BarCodeType", "2of5 Interleaved", "BarCodes.StiCodabarBarCodeType.png", "StiInterleaved2of5BarCodeType"));
    items.push(this.Item("StiStandard2of5BarCodeType", "2of5 Standard", "BarCodes.StiCodabarBarCodeType.png", "StiStandard2of5BarCodeType"));

    return items;
}

StiMobileDesigner.prototype.GetStylesActionsMenuItems = function () {
    var items = [];
    items.push(this.Item("openStyle", this.loc.MainMenu.menuFileOpen.replace("&", ""), "Open.png", "openStyle"));
    items.push(this.Item("saveStyle", this.loc.MainMenu.menuFileSaveAs, "Save.png", "saveStyle"));
    items.push("separator");
    items.push(this.Item("createStyleCollection", this.loc.FormStyleDesigner.CreateStyleCollection, "StylesCreate.png", "createStyleCollection"));

    return items;
}

StiMobileDesigner.prototype.GetAllStyleCollectionNames = function () {
    var styles = {};
    var report = this.options.report;
    if (report) {
        for (var pageName in report.pages) {
            var page = report.pages[pageName];
            if (page.properties.componentStyle) {
                styles[page.properties.componentStyle] = true;
            }
            for (var cName in page.components) {
                var component = page.components[cName];
                if (component.properties.componentStyle) {
                    styles[component.properties.componentStyle] = true;
                }
            }
        }
    }

    return styles;
}

StiMobileDesigner.prototype.GetRenderToItems = function (currentComponent) {
    var items = [];
    var report = this.options.report;
    items.push(this.Item("empty", "", null, ""));

    if (currentComponent && currentComponent.typeComponent == "StiText") {
        var page = report.pages[currentComponent.properties.pageName];
        var parentName = currentComponent.properties.parentName;

        for (var componentName in page.components) {
            var component = page.components[componentName];
            if (currentComponent != component && parentName == component.properties.parentName &&
                    (component.typeComponent == "StiText" || component.typeComponent == "StiTextInCells")) {
                var itemText = component.properties.name;
                items.push(this.Item(itemText, itemText, null, itemText));
            }
        }
    }

    return items;
}

StiMobileDesigner.prototype.GetSaveTypeItems = function () {
    var items = [];
    items.push(this.Item("XML", "XML", null, "xml"));
    items.push(this.Item("JSON", "JSON", null, "json"));

    return items;
}

StiMobileDesigner.prototype.GetRefreshTimeItems = function () {
    var items = [];
    items.push(this.Item("0", this.loc.PropertyEnum.DialogResultNone, null, "0"));
    items.push(this.Item("10", "10 Seconds", null, "10"));
    items.push(this.Item("20", "20 Seconds", null, "20"));
    items.push(this.Item("30", "30 Seconds", null, "30"));
    items.push(this.Item("60", "1 Minute", null, "60"));
    items.push(this.Item("120", "2 Minute", null, "120"));
    items.push(this.Item("300", "5 Minutes", null, "300"));
    items.push(this.Item("600", "10 Minutes", null, "600"));
    items.push(this.Item("1800", "30 Minutes", null, "1800"));
    items.push(this.Item("3600", "1 Hour", null, "3600"));

    return items;
}

StiMobileDesigner.prototype.GetExportFormatTypesItems = function () {
    var items = [];
    items.push(this.Item("ReportSnapshot", this.loc.FormViewer.DocumentFile, "Resources.ResourceReportSnapshot.png", "ReportSnapshot"));
    items.push(this.Item("Pdf", this.loc.Export.ExportTypePdfFile, "Resources.ResourcePdf.png", "Pdf"));
    items.push(this.Item("Xps", this.loc.Export.ExportTypeXpsFile, "Resources.ResourceXps.png", "Xps"));
    items.push(this.Item("PowerPoint", this.loc.Export.ExportTypePpt2007File, "Resources.ResourcePowerPoint.png", "PowerPoint"));
    items.push(this.Item("Html", this.loc.Export.ExportTypeHtmlFile, "Resources.ResourceHtml.png", "Html"));
    items.push(this.Item("Text", this.loc.Export.ExportTypeTxtFile, "Resources.ResourceTxt.png", "Text"));
    items.push(this.Item("RichText", this.loc.Export.ExportTypeRtfFile, "Resources.ResourceRtf.png", "RichText"));
    items.push(this.Item("Word", this.loc.Export.ExportTypeWord2007File, "Resources.ResourceWord.png", "Word"));
    items.push(this.Item("OpenDocumentWriter", this.loc.Export.ExportTypeWriterFile, "Resources.ResourceOpenDocumentWriter.png", "OpenDocumentWriter"));
    items.push(this.Item("Excel", this.loc.Export.ExportTypeExcelFile, "Resources.ResourceExcel.png", "Excel"));
    items.push(this.Item("OpenDocumentCalc", this.loc.Export.ExportTypeCalcFile, "Resources.ResourceOpenDocumentCalc.png", "OpenDocumentCalc"));
    items.push(this.Item("Data", this.loc.Export.ExportTypeDataFile, "Resources.ResourceData.png", "Data"));
    items.push(this.Item("Image", this.loc.Export.ExportTypeImageFile, "Resources.ResourceImage.png", "Image"));

    return items;
}

StiMobileDesigner.prototype.GetMapsCategoriesItems = function () {
    var items = [];
    items.push(this.Item("World", "World", "Maps.World.png", "World"));
    items.push("separator");
    items.push(this.Item("Europe", "Europe", "Maps.EU.png", "Europe", null, true));
    items.push(this.Item("NorthAmerica", "NorthAmerica", "Maps.NorthAmerica.png", "NorthAmerica", null, true));
    items.push(this.Item("SouthAmerica", "SouthAmerica", "Maps.SouthAmerica.png", "SouthAmerica", null, true));
    items.push(this.Item("Asia", "Asia", "Maps.Asia.png", "Asia", null, true));
    items.push(this.Item("Oceania", "Oceania", "Maps.Oceania.png", "Oceania", null, true));
    items.push(this.Item("Africa", "Africa", "Maps.Africa.png", "Africa", null, true));

    return items;
}

StiMobileDesigner.prototype.GetEuropeMapsItems = function () {
    var items = [];
    items.push(this.Item("EU", "EU", "Maps.EU.png", "EU"));
    items.push("separator");
    items.push(this.Item("France", "France", "Maps.France.png", "France"));
    items.push(this.Item("Germany", "Germany", "Maps.Germany.png", "Germany"));
    items.push(this.Item("Italy", "Italy", "Maps.Italy.png", "Italy"));
    items.push(this.Item("Netherlands", "Netherlands", "Maps.Netherlands.png", "Netherlands"));
    items.push(this.Item("Russia", "Russia", "Maps.Russia.png", "Russia"));
    items.push(this.Item("Switzerland", "Switzerland", "Maps.Switzerland.png", "Switzerland"));
    items.push(this.Item("UK", "UK", "Maps.UK.png", "UK"));
    items.push("separator");
    items.push(this.Item("Albania", "Albania", "Maps.Albania.png", "Albania"));
    items.push(this.Item("Andorra", "Andorra", "Maps.Andorra.png", "Andorra"));
    items.push(this.Item("Austria", "Austria", "Maps.Austria.png", "Austria"));
    items.push(this.Item("Belarus", "Belarus", "Maps.Belarus.png", "Belarus"));
    items.push(this.Item("Belgium", "Belgium", "Maps.Belgium.png", "Belgium"));
    items.push(this.Item("BosniaAndHerzegovina", "Bosnia and Herzegovina", "Maps.BosniaAndHerzegovina.png", "BosniaAndHerzegovina"));
    items.push(this.Item("Bulgaria", "Bulgaria", "Maps.Bulgaria.png", "Bulgaria"));
    items.push(this.Item("Croatia", "Croatia", "Maps.Croatia.png", "Croatia"));
    items.push(this.Item("CzechRepublic", "Czech Republic", "Maps.CzechRepublic.png", "CzechRepublic"));
    items.push(this.Item("Denmark", "Denmark", "Maps.Denmark.png", "Denmark"));
    items.push(this.Item("Estonia", "Estonia", "Maps.Estonia.png", "Estonia"));
    items.push(this.Item("Finland", "Finland", "Maps.Finland.png", "Finland"));
    items.push(this.Item("Georgia", "Georgia", "Maps.Georgia.png", "Georgia"));
    items.push(this.Item("Greece", "Greece", "Maps.Greece.png", "Greece"));
    items.push(this.Item("Hungary", "Hungary", "Maps.Hungary.png", "Hungary"));
    items.push(this.Item("Iceland", "Iceland", "Maps.Iceland.png", "Iceland"));
    items.push(this.Item("Ireland", "Ireland", "Maps.Ireland.png", "Ireland"));
    items.push(this.Item("Latvia", "Latvia", "Maps.Latvia.png", "Latvia"));
    items.push(this.Item("Liechtenstein", "Liechtenstein", "Maps.Liechtenstein.png", "Liechtenstein"));
    items.push(this.Item("Lithuania", "Lithuania", "Maps.Lithuania.png", "Lithuania"));
    items.push(this.Item("Luxembourg", "Luxembourg", "Maps.Luxembourg.png", "Luxembourg"));
    items.push(this.Item("Macedonia", "Macedonia", "Maps.Macedonia.png", "Macedonia"));
    items.push(this.Item("Malta", "Malta", "Maps.Malta.png", "Malta"));
    items.push(this.Item("Moldova", "Moldova", "Maps.Moldova.png", "Moldova"));
    items.push(this.Item("Monaco", "Monaco", "Maps.Monaco.png", "Monaco"));
    items.push(this.Item("Montenegro", "Montenegro", "Maps.Montenegro.png", "Montenegro"));
    items.push(this.Item("Norway", "Norway", "Maps.Norway.png", "Norway"));
    items.push(this.Item("Poland", "Poland", "Maps.Poland.png", "Poland"));
    items.push(this.Item("Portugal", "Portugal", "Maps.Portugal.png", "Portugal"));
    items.push(this.Item("Romania", "Romania", "Maps.Romania.png", "Romania"));
    items.push(this.Item("SanMarino", "SanMarino", "Maps.SanMarino.png", "SanMarino"));
    items.push(this.Item("Serbia", "Serbia", "Maps.Serbia.png", "Serbia"));
    items.push(this.Item("Slovakia", "Slovakia", "Maps.Slovakia.png", "Slovakia"));
    items.push(this.Item("Slovenia", "Slovenia", "Maps.Slovenia.png", "Slovenia"));
    items.push(this.Item("Spain", "Spain", "Maps.Spain.png", "Spain"));
    items.push(this.Item("Sweden", "Sweden", "Maps.Sweden.png", "Sweden"));
    items.push(this.Item("Turkey", "Turkey", "Maps.Turkey.png", "Turkey"));
    items.push(this.Item("Ukraine", "Ukraine", "Maps.Ukraine.png", "Ukraine"));
    items.push(this.Item("Vatican", "Vatican", "Maps.Vatican.png", "Vatican"));

    return items;
}

StiMobileDesigner.prototype.GetNorthAmericaMapsItems = function () {
    var items = [];
    items.push(this.Item("USA", "USA", "Maps.USA.png", "USA"));
    items.push(this.Item("Canada", "Canada", "Maps.Canada.png", "Canada"));
    items.push(this.Item("Mexico", "Mexico", "Maps.Mexico.png", "Mexico"));

    return items;
}

StiMobileDesigner.prototype.GetSouthAmericaMapsItems = function () {
    var items = [];
    items.push(this.Item("Argentina", "Argentina", "Maps.Argentina.png", "Argentina"));
    items.push(this.Item("Bolivia", "Bolivia", "Maps.Bolivia.png", "Bolivia"));
    items.push(this.Item("Brazil", "Brazil", "Maps.Brazil.png", "Brazil"));
    items.push(this.Item("Chile", "Chile", "Maps.Chile.png", "Chile"));
    items.push(this.Item("Colombia", "Colombia", "Maps.Colombia.png", "Colombia"));
    items.push(this.Item("Ecuador", "Ecuador", "Maps.Ecuador.png", "Ecuador"));
    items.push(this.Item("FalklandIslands", "Falkland Islands", "Maps.FalklandIslands.png", "FalklandIslands"));
    items.push(this.Item("Guyana", "Guyana", "Maps.Guyana.png", "Guyana"));
    items.push(this.Item("Paraguay", "Paraguay", "Maps.Paraguay.png", "Paraguay"));
    items.push(this.Item("Peru", "Peru", "Maps.Peru.png", "Peru"));
    items.push(this.Item("Suriname", "Suriname", "Maps.Suriname.png", "Suriname"));
    items.push(this.Item("Uruguay", "Uruguay", "Maps.Uruguay.png", "Uruguay"));
    items.push(this.Item("Venezuela", "Venezuela", "Maps.Venezuela.png", "Venezuela"));

    return items;
}

StiMobileDesigner.prototype.GetAsiaMapsItems = function () {
    var items = [];
    items.push(this.Item("Armenia", "Armenia", "Maps.Armenia.png", "Armenia"));
    items.push(this.Item("Azerbaijan", "Azerbaijan", "Maps.Azerbaijan.png", "Azerbaijan"));
    items.push(this.Item("China", "China", "Maps.China.png", "China"));
    items.push(this.Item("India", "India", "Maps.India.png", "India"));
    items.push(this.Item("Israel", "Israel", "Maps.Israel.png", "Israel"));
    items.push(this.Item("Japan", "Japan", "Maps.Japan.png", "Japan"));
    items.push(this.Item("Kazakhstan", "Kazakhstan", "Maps.Kazakhstan.png", "Kazakhstan"));
    items.push(this.Item("Malaysia", "Malaysia", "Maps.Malaysia.png", "Malaysia"));
    items.push(this.Item("Philippines", "Philippines", "Maps.Philippines.png", "Philippines"));
    items.push(this.Item("SaudiArabia", "Saudi Arabia", "Maps.SaudiArabia.png", "SaudiArabia"));
    items.push(this.Item("SouthKorea", "South Korea", "Maps.SouthKorea.png", "SouthKorea"));
    items.push(this.Item("Thailand", "Thailand", "Maps.Thailand.png", "Thailand"));
    items.push(this.Item("Vietnam", "Vietnam", "Maps.Vietnam.png", "Vietnam"));

    return items;
}

StiMobileDesigner.prototype.GetOceaniaMapsItems = function () {
    var items = [];
    items.push(this.Item("Australia", "Australia", "Maps.Australia.png", "Australia"));
    items.push(this.Item("Indonesia", "Indonesia", "Maps.Indonesia.png", "Indonesia"));
    items.push(this.Item("NewZealand", "New Zealand", "Maps.NewZealand.png", "NewZealand"));

    return items;
}

StiMobileDesigner.prototype.GetAfricaMapsItems = function () {
    var items = [];
    items.push(this.Item("SouthAfrica", "South Africa", "Maps.SouthAfrica.png", "SouthAfrica"));

    return items;
}

StiMobileDesigner.prototype.GetChoroplethDataTypesItems = function () {
    var items = [];
    items.push(this.Item("dataColumns", this.loc.PropertyMain.DataColumns, null, "DataColumns"));
    items.push(this.Item("manual", this.loc.PropertyEnum.FormStartPositionManual, null, "Manual"));

    return items;
}

StiMobileDesigner.prototype.GetChoroplethMapTypesItems = function () {
    var items = [];
    items.push(this.Item("Individual", this.loc.PropertyEnum.StiMapTypeIndividual, null, "Individual"));
    items.push(this.Item("Group", this.loc.PropertyEnum.StiMapTypeGroup, null, "Group"));
    items.push(this.Item("Heatmap", this.loc.PropertyEnum.StiMapTypeHeatmap, null, "Heatmap"));
    items.push(this.Item("HeatmapWithGroup", this.loc.PropertyEnum.StiMapTypeHeatmapWithGroup, null, "HeatmapWithGroup"));

    return items;
}

StiMobileDesigner.prototype.GetMapDisplayNameTypeItems = function () {
    var items = [];
    items.push(this.Item("None", this.loc.PropertyEnum.StiDisplayNameTypeNone, null, "None"));
    items.push(this.Item("Full", this.loc.PropertyEnum.StiDisplayNameTypeFull, null, "Full"));
    items.push(this.Item("Short", this.loc.PropertyEnum.StiDisplayNameTypeShort, null, "Short"));

    return items;
}

StiMobileDesigner.prototype.GetLineSpacingItems = function () {
    var items = [];
    items.push(this.Item("item_1", "1", null, "1"));
    items.push(this.Item("item_1.15", "1.15", null, "1.15"));
    items.push(this.Item("item_1.5", "1.5", null, "1.5"));
    items.push(this.Item("item_2", "2", null, "2"));
    items.push(this.Item("item_2.5", "2.5", null, "2.5"));
    items.push(this.Item("item_3", "3", null, "3"));

    return items;
}

StiMobileDesigner.prototype.GetGaugeCalculationModeItems = function () {
    var items = [];
    items.push(this.Item("Auto", this.loc.PropertyEnum.StiGaugeCalculationModeAuto, null, "Auto"));
    items.push(this.Item("Custom", this.loc.PropertyEnum.StiGaugeCalculationModeCustom, null, "Custom"));

    return items;
}