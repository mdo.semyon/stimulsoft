
StiMobileDesigner.prototype.CreatePage = function (pageObject, isDashboard) {
    var page = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "svg") : document.createElement("svg");
    page.jsObject = this;
    page.typeComponent = "StiPage";
    page.style.margin = this.options.paintPanelPadding + "px";
    page.style.display = "none";
    page.className = "stiPageSvg";
    page.style.border = "1px solid #c6c6c6";
    page.style.overflow = "hidden";
    page.isDashboard = isDashboard;

    //Set Properties
    page.properties = {};
    page.properties.name = pageObject.name;
    page.properties.pageIndex = pageObject.pageIndex;
    page.properties.largeHeightAutoFactor = "1";
    this.WriteAllProperties(page, pageObject.properties);    

    //Create Controls
    page.controls = {};
    this.CreatePageBackgroundGradient(page);
    this.CreatePageBackgroundHatch(page);
    this.CreatePageWaterMark(page);
    this.CreatePageWaterMarkGradient(page);
    this.CreatePageWaterMarkImage(page);
    this.CreatePageBorders(page);

    if (this.options.report && this.options.report.info.showGrid) {
        this.CreatePageGridLines(page);
    }

    if ((!this.options.jsMode || this.options.nodeJsMode) && this.options.productVersion == this.options.productVersion.trim()) {
        this.CreatePageWaterMarkChars(page);
    }

    //Create Methods
    this.CreatePageEvents(page);
    page.repaint = function (rebuildGrigLines) { this.jsObject.RepaintPage(this, rebuildGrigLines); }
    page.remove = function () { this.jsObject.RemovePage(this); }
    page.rebuild = function (componentsProps) { this.jsObject.RebuildPage(this, componentsProps); }
    page.setSelected = function () { this.jsObject.SetSelectedObject(this); }
    page.rename = function (newName) { this.jsObject.RenamePage(this, newName); }
    page.addComponents = function () { this.jsObject.AddComponents(this); }
    page.removeComponents = function () { this.jsObject.RemoveComponents(this); }
    page.updateComponentsLevels = function () { this.jsObject.UpdateComponentsLevels(this); }
    page.repaintAllComponents = function () { this.jsObject.RepaintAllComponentsOnPage(this); }

    return page;
}

//GridLines
StiMobileDesigner.prototype.CreatePageGridLines = function (page) {
    var jsObject = this;
    var gridSize = page.isDashboard ? page.properties.gridSize : this.options.report.gridSize;
    var pageWidth = this.ConvertUnitToPixel(this.StrToDouble(page.properties.unitWidth), page.isDashboard);
    var pageHeight = this.ConvertUnitToPixel(this.StrToDouble(page.properties.unitHeight), page.isDashboard);
    var marginsStr = page.properties.unitMargins.split("!");
    var verticalMarginsPx = parseInt(this.ConvertUnitToPixel(this.StrToDouble(marginsStr[1]) + this.StrToDouble(marginsStr[3]), page.isDashboard));
    var horizontalMarginsPx = parseInt(this.ConvertUnitToPixel(this.StrToDouble(marginsStr[0]) + this.StrToDouble(marginsStr[2]), page.isDashboard));
    var segmentPerHeight = this.StrToDouble(page.properties.segmentPerHeight);
    var segmentPerWidth = this.StrToDouble(page.properties.segmentPerWidth);
    if (segmentPerWidth > 1) pageWidth = ((pageWidth - horizontalMarginsPx) * segmentPerWidth) + horizontalMarginsPx;
    if (segmentPerHeight > 1) pageHeight = ((pageHeight - verticalMarginsPx) * segmentPerHeight) + verticalMarginsPx;
    var largeHeightFactor = page.properties.largeHeight ? this.StrToInt(page.properties.largeHeightFactor) : this.StrToDouble(page.properties.largeHeightAutoFactor);

    if (page.controls.gridLines) {
        for (i = 0; i < page.controls.gridLines.length; i++)
            page.removeChild(page.controls.gridLines[i]);
    }

    var margins = page.properties.unitMargins.split("!");
    var marginsPx = [];
    for (i = 0; i < 4; i++) {
        marginsPx[i] = this.ConvertUnitToPixel(this.StrToDouble(margins[i]), page.isDashboard);
    }

    var startXPos = marginsPx[0];
    var endXPos = pageWidth - marginsPx[2];
    var startYPos = marginsPx[1];
    var endYPos = largeHeightFactor > 1 ? (pageHeight - marginsPx[3] - marginsPx[1]) * largeHeightFactor + marginsPx[1] : pageHeight - marginsPx[3];

    var createGridLine = function (x1, y1, x2, y2) {
        var line = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "line") : document.createElement("line");
        line.style.stroke = "#dcdcdc";
        line.positions = { x1: x1, y1: y1, x2: x2, y2: y2 };
        page.controls.gridLines.push(line);
        page.insertBefore(line, page.controls.borders[0]);

        line.repaint = function () {
            line.setAttribute("x1", line.positions.x1 * jsObject.options.report.zoom);
            line.setAttribute("y1", line.positions.y1 * jsObject.options.report.zoom);
            line.setAttribute("x2", line.positions.x2 * jsObject.options.report.zoom);
            line.setAttribute("y2", line.positions.y2 * jsObject.options.report.zoom);
            line.style.strokeDasharray = jsObject.options.report.info.gridMode == "Lines" ? "1" : ("1," + (gridSize * jsObject.options.report.zoom - 1));
        }

        return line;
    }

    page.controls.gridLines = [];

    var x = startXPos;
    var i = 0;
    while (x <= endXPos) {
        var line = createGridLine(x, startYPos, x, endYPos);
        x += gridSize;
        if (i % 2 == 0) line.style.stroke = "#bebebe";
        i++;
    }

    if (jsObject.options.report.info.gridMode == "Lines") {
        var y = startYPos;
        i = 0;
        while (y <= endYPos) {
            var line = createGridLine(startXPos, y, endXPos, y);
            y += gridSize;
            if (i % 2 == 0) line.style.stroke = "#bebebe";
            i++;
        }
    }
}

// Borders
StiMobileDesigner.prototype.CreatePageBorders = function (page) {
    page.controls.borders = [];
    for (i = 0; i < 8; i ++) {
        page.controls.borders[i] = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "line") : document.createElement("line");
        page.appendChild(page.controls.borders[i]);
    }
}

// Watermark
StiMobileDesigner.prototype.CreatePageWaterMark = function (page) {
    page.controls.waterMarkParent = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "g") : document.createElement("g");
    page.appendChild(page.controls.waterMarkParent);
    page.controls.waterMarkChild = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "g") : document.createElement("g");
    page.controls.waterMarkParent.appendChild(page.controls.waterMarkChild);
    page.controls.waterMarkText = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "text") : document.createElement("text");
    page.controls.waterMarkChild.appendChild(page.controls.waterMarkText);
}

// Watermark Chars
StiMobileDesigner.prototype.CreatePageWaterMarkChars = function (page) {
    page.controls.waterMarkCharsParent = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "g") : document.createElement("g");
    page.appendChild(page.controls.waterMarkCharsParent);        
    page.controls.waterMarkCharsChild = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "g") : document.createElement("g");
    page.controls.waterMarkCharsParent.appendChild(page.controls.waterMarkCharsChild);
    page.controls.waterMarkCharsText = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "text") : document.createElement("text");
    page.controls.waterMarkCharsChild.appendChild(page.controls.waterMarkCharsText);
}

// Watermark Image
StiMobileDesigner.prototype.CreatePageWaterMarkImage = function (page) {
    page.controls.waterMarkImage = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "image") : document.createElement("image");
    page.appendChild(page.controls.waterMarkImage);
}

// Background Gradient
StiMobileDesigner.prototype.CreatePageBackgroundGradient = function (page) {
    page.controls.gradient = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "linearGradient") : document.createElement("linearGradient");
    page.appendChild(page.controls.gradient);
    var gradientId = "gradient" + this.newGuid().replace(/-/g, '');
    page.controls.gradient.setAttribute("id", gradientId);
    page.controls.gradient.setAttribute("x1", "0%");
    page.controls.gradient.setAttribute("y1", "0%");
    page.controls.gradient.setAttribute("x2", "100%");
    page.controls.gradient.setAttribute("y2", "0%");
    page.controls.gradient.stop1 = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "stop") : document.createElement("stop");
    page.controls.gradient.appendChild(page.controls.gradient.stop1);
    page.controls.gradient.stop2 = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "stop") : document.createElement("stop");
    page.controls.gradient.stop3 = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "stop") : document.createElement("stop");
    page.controls.gradient.appendChild(page.controls.gradient.stop3);
    page.controls.gradient.stop1.setAttribute("offset", "0");
    page.controls.gradient.stop2.setAttribute("offset", "50%");
    page.controls.gradient.stop3.setAttribute("offset", "100%");
    page.controls.gradient.rect = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "rect") : document.createElement("rect");
    page.controls.gradient.rect.setAttribute("x", "0");
    page.controls.gradient.rect.setAttribute("y", "0");
    page.controls.gradient.rect.setAttribute("width", "100%");
    page.controls.gradient.rect.setAttribute("height", "100%");
    page.controls.gradient.rect.setAttribute("fill", "url(#" + gradientId + ")");
    page.controls.gradient.rect.style.display = "none";
    page.appendChild(page.controls.gradient.rect);
}

// WaterMark Gradient
StiMobileDesigner.prototype.CreatePageWaterMarkGradient = function (page) {
    page.controls.waterMarkGradient = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "linearGradient") : document.createElement("linearGradient");
    page.appendChild(page.controls.waterMarkGradient);
    page.controls.waterMarkGradient.setAttribute("id", "waterMarkGradient" + this.newGuid().replace(/-/g, ''));
    page.controls.waterMarkGradient.setAttribute("x1", "0%");
    page.controls.waterMarkGradient.setAttribute("y1", "0%");
    page.controls.waterMarkGradient.setAttribute("x2", "100%");
    page.controls.waterMarkGradient.setAttribute("y2", "0%");
    page.controls.waterMarkGradient.stop1 = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "stop") : document.createElement("stop");
    page.controls.waterMarkGradient.appendChild(page.controls.waterMarkGradient.stop1);
    page.controls.waterMarkGradient.stop2 = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "stop") : document.createElement("stop");
    page.controls.waterMarkGradient.appendChild(page.controls.waterMarkGradient.stop2);
    page.controls.waterMarkGradient.stop3 = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "stop") : document.createElement("stop");
    page.controls.waterMarkGradient.appendChild(page.controls.waterMarkGradient.stop3);
    page.controls.waterMarkGradient.stop1.setAttribute("offset", "0");
    page.controls.waterMarkGradient.stop2.setAttribute("offset", "50%");
    page.controls.waterMarkGradient.stop3.setAttribute("offset", "100%");
}

// Background Hatch
StiMobileDesigner.prototype.CreatePageBackgroundHatch = function (page) {
    var svgHatchBrush = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "svg") : document.createElement("svg");
    page.controls.svgHatchBrush = svgHatchBrush;
    page.appendChild(svgHatchBrush);
}