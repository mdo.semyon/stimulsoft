
StiMobileDesigner.prototype.ChangeVisibilityStateResizingIcons = function (component, state) {
    if ((!this.options.isTouchDevice || component.isCrossTabField) && component.controls.resizingPoints) {
        for (i = 0; i <= 7; i++) {
            var resizingPoint = component.controls.resizingPoints[i];
            if (resizingPoint) {
                resizingPoint.style.display = state ? "" : "none";
            }
        }
    }
    else {
        var iconNumbers = ComponentCollection[component.typeComponent][4].split(",");

        for (i = 0; i < iconNumbers.length; i++) {
            component.controls.resizingIcons[iconNumbers[i]].style.visibility = state ? "visible" : "hidden";
        }

        if (component.controls.editIcon) {
            component.controls.editIcon.style.visibility = state ? "visible" : "hidden";
        }
    }

    if (component.controls.editDbsIcon) {
        component.controls.editDbsIcon.style.visibility = state ? "visible" : "hidden";
        if (this.options.currentForm && this.options.currentForm.visible && this.options.currentForm.dockingComponent == component) {
            component.controls.editDbsIcon.style.visibility = "hidden";
        }
    }
}

StiMobileDesigner.prototype.FindAllChilds = function(component, allChilds) {    
    var childsStr = component.properties.childs;
    if (childsStr) {
        var childs = childsStr.split(",");
        for (var indexChild = 0; indexChild < childs.length; indexChild++) {           
            var child = this.options.report.pages[component.properties.pageName].components[childs[indexChild]];
            if (child) {
                child.startPosX = child.getAttribute("left");
                child.startPosY = child.getAttribute("top");
                allChilds[childs[indexChild]] = child;
                this.FindAllChilds(child, allChilds);
            }
        }
    }   
    return allChilds;
}

StiMobileDesigner.prototype.GetAllChildsComponents = function (component) {
    var childs = {};    
    this.FindAllChilds(component, childs);    
    return childs;
}

StiMobileDesigner.prototype.SetComponentOnTopLevel = function (component) {
    page = this.options.report.pages[component.properties.pageName];
    page.removeChild(component);
    page.appendChild(component);
   
    var allChilds = component.getAllChildsComponents();
    parentIndex = parseInt(component.properties.parentIndex) + 1;

    do
    {        
        flag = false;
        for (var childName in allChilds) {  
            var child = allChilds[childName];
            if (child.properties.parentIndex == parentIndex) {                
                page.removeChild(child);
                page.appendChild(child);                
                flag = true;        
            }    
        }
        parentIndex++;
    }
    while (flag)
}

StiMobileDesigner.prototype.ResizeComponents = function (mouseCurrentXPos, mouseCurrentYPos) {
    var resizingType = this.options.in_resize[1];
    var deltaX = mouseCurrentXPos - this.options.startMousePos[0];
    var deltaY = mouseCurrentYPos - this.options.startMousePos[1];
    var fixedComponents = this.options.in_resize[3];
    var selectArea = this.options.in_resize[4];
    var selectAreaWidth = selectArea.right - selectArea.left;
    var selectAreaHeight = selectArea.bottom - selectArea.top;    
    var pageMarginsPx = this.options.currentPage.marginsPx;

    for (var i = 0; i < this.options.in_resize[0].length; i++) {
        var component = this.options.in_resize[0][i];
        var startValues = this.options.in_resize[2][i];
        
        var canResize = !component.properties.locked && (
            (component.properties.restrictions && (component.properties.restrictions == "All" || component.properties.restrictions.indexOf("AllowResize") >= 0)) ||
             !component.properties.restrictions);

        if (this.IsTableCell(component) || component.typeComponent == "StiTable" || !canResize) continue;

        var newWidth = startValues.width;
        var newHeight = startValues.height;
        var newLeft = startValues.left;
        var newTop = startValues.top;

        switch (resizingType) {
            case "MultiLeftTop":
                {
                    var scaleX = deltaX < 0 ? (selectAreaWidth + Math.abs(deltaX)) / selectAreaWidth : (selectAreaWidth - Math.abs(deltaX)) / selectAreaWidth;
                    newWidth = startValues.width * scaleX;
                    newLeft = selectArea.right - ((selectArea.right - ((startValues.left - pageMarginsPx[0]) + startValues.width)) * scaleX) - newWidth + pageMarginsPx[0];

                    var scaleY = deltaY < 0 ? (selectAreaHeight + Math.abs(deltaY)) / selectAreaHeight : (selectAreaHeight - Math.abs(deltaY)) / selectAreaHeight;
                    newHeight = startValues.height * scaleY;
                    newTop = selectArea.bottom - ((selectArea.bottom - ((startValues.top - pageMarginsPx[1]) + startValues.height)) * scaleY) - newHeight + pageMarginsPx[1];
                    break;
                }
            case "MultiTop":
                {
                    var scaleY = deltaY < 0 ? (selectAreaHeight + Math.abs(deltaY)) / selectAreaHeight : (selectAreaHeight - Math.abs(deltaY)) / selectAreaHeight;
                    newHeight = startValues.height * scaleY;
                    newTop = selectArea.bottom - ((selectArea.bottom - ((startValues.top - pageMarginsPx[1]) + startValues.height)) * scaleY) - newHeight + pageMarginsPx[1];
                    break;
                }
            case "MultiRightTop":
                {
                    var scaleX = (selectAreaWidth + deltaX) / selectAreaWidth;
                    newWidth = startValues.width * scaleX;
                    if (!this.IsContains(fixedComponents.left, component)) {
                        newLeft = ((startValues.left - pageMarginsPx[0] - selectArea.left) * scaleX) + pageMarginsPx[0] + selectArea.left;
                    }
                    var scaleY = deltaY < 0 ? (selectAreaHeight + Math.abs(deltaY)) / selectAreaHeight : (selectAreaHeight - Math.abs(deltaY)) / selectAreaHeight;
                    newHeight = startValues.height * scaleY;
                    newTop = selectArea.bottom - ((selectArea.bottom - ((startValues.top - pageMarginsPx[1]) + startValues.height)) * scaleY) - newHeight + pageMarginsPx[1];
                    break;
                }
            case "MultiRight":
                {
                    var scaleX = (selectAreaWidth + deltaX) / selectAreaWidth;                    
                    newWidth = startValues.width * scaleX;
                    if (!this.IsContains(fixedComponents.left, component)) {
                        newLeft = ((startValues.left - pageMarginsPx[0] - selectArea.left) * scaleX) + pageMarginsPx[0] + selectArea.left;
                    }
                    break;
                }
            case "MultiRightBottom":
                {
                    var scaleX = (selectAreaWidth + deltaX) / selectAreaWidth;
                    var scaleY = (selectAreaHeight + deltaY) / selectAreaHeight;
                    newWidth = startValues.width * scaleX;
                    newHeight = startValues.height * scaleY;
                    if (!this.IsContains(fixedComponents.left, component)) {
                        newLeft = ((startValues.left - pageMarginsPx[0] - selectArea.left) * scaleX) + pageMarginsPx[0] + selectArea.left;
                    }
                    if (!this.IsContains(fixedComponents.top, component)) {
                        newTop = ((startValues.top - pageMarginsPx[1] - selectArea.top) * scaleY) + pageMarginsPx[1] + selectArea.top;
                    }
                    break;
                }
            case "MultiBottom":
                {
                    var scaleY = (selectAreaHeight + deltaY) / selectAreaHeight;
                    newHeight = startValues.height * scaleY;
                    if (!this.IsContains(fixedComponents.top, component)) {
                        newTop = ((startValues.top - pageMarginsPx[1] - selectArea.top) * scaleY) + pageMarginsPx[1] + selectArea.top;
                    }
                    break;
                }
            case "MultiLeftBottom":
                {
                    var scaleX = deltaX < 0 ? (selectAreaWidth + Math.abs(deltaX)) / selectAreaWidth : (selectAreaWidth - Math.abs(deltaX)) / selectAreaWidth;
                    newWidth = startValues.width * scaleX;
                    newLeft = selectArea.right - ((selectArea.right - ((startValues.left - pageMarginsPx[0]) + startValues.width)) * scaleX) - newWidth + pageMarginsPx[0];

                    var scaleY = (selectAreaHeight + deltaY) / selectAreaHeight;
                    newHeight = startValues.height * scaleY;
                    if (!this.IsContains(fixedComponents.top, component)) {
                        newTop = ((startValues.top - pageMarginsPx[1] - selectArea.top) * scaleY) + pageMarginsPx[1] + selectArea.top;
                    }
                    break;
                }
            case "MultiLeft":
                {
                    var scaleX = deltaX < 0 ? (selectAreaWidth + Math.abs(deltaX)) / selectAreaWidth : (selectAreaWidth - Math.abs(deltaX)) / selectAreaWidth;
                    newWidth = startValues.width * scaleX;
                    newLeft = selectArea.right - ((selectArea.right - ((startValues.left - pageMarginsPx[0]) + startValues.width)) * scaleX) - newWidth + pageMarginsPx[0];
                    break;
                }
        }

        if (newWidth > 0 && newHeight > 0) {
            var pageMarginsPx = this.options.report.pages[component.properties.pageName].marginsPx;
            component.properties.unitLeft = this.ConvertPixelToUnit((newLeft - pageMarginsPx[0]) / this.options.report.zoom, component.isDashboardElement);
            component.properties.unitTop = this.ConvertPixelToUnit((newTop - pageMarginsPx[1]) / this.options.report.zoom, component.isDashboardElement);
            component.properties.unitWidth = this.ConvertPixelToUnit(newWidth / this.options.report.zoom, component.isDashboardElement);
            component.properties.unitHeight = this.ConvertPixelToUnit(newHeight / this.options.report.zoom, component.isDashboardElement);
            component.repaint();            
        }
    }

    this.PaintSelectedLines();
}

StiMobileDesigner.prototype.ResizeComponent = function (mouseCurrentXPos, mouseCurrentYPos, tableCell) {
    var component = tableCell || this.options.in_resize[0];
    var resizingType = tableCell ? tableCell.resizingType : this.options.in_resize[1];
    var startValues = tableCell ? tableCell.startValues : this.options.in_resize[2];
    var canResize = !component.properties.locked && 
        ((component.properties.restrictions && (component.properties.restrictions == "All" || component.properties.restrictions.indexOf("AllowResize") >= 0)) ||
            !component.properties.restrictions);
    component.properties.invertWidth = false;
    component.properties.invertHeight = false;

    var isTableCell = this.IsTableCell(component);
    if (!tableCell && !isTableCell && !canResize) return;

    var deltaX = this.options.startMousePos[0] - mouseCurrentXPos;
    var deltaY = this.options.startMousePos[1] - mouseCurrentYPos;

    var newWidth = startValues.width;
    var newHeight = startValues.height;
    var newLeft = startValues.left;
    var newTop = startValues.top;
    
    if (resizingType == "ResizeDiagonal" || resizingType == "ResizeWidth" || resizingType == "ResizeHeight") {
        var directWidth = resizingType == "ResizeDiagonal" || resizingType == "ResizeWidth" ? -1 : 0;
        var directHeight = resizingType == "ResizeDiagonal" || resizingType == "ResizeHeight" ? -1 : 0;

        newWidth = startValues.width + directWidth * deltaX;
        newHeight = startValues.height + directHeight * deltaY;
    }
    else if (resizingType == "ResizeHeightUp") {
        newTop = startValues.top - deltaY;
        newHeight = startValues.height + deltaY;
    }
    else {
        switch (resizingType) {
            case "LeftTop":
                {
                    newLeft = startValues.left - deltaX;
                    newTop = startValues.top - deltaY;
                    newWidth = startValues.width + deltaX;
                    newHeight = startValues.height + deltaY;
                    break;
                }
            case "Top":
                {
                    newTop = startValues.top - deltaY;
                    newHeight = startValues.height + deltaY;
                    break;
                }
            case "RightTop":
                {
                    newTop = startValues.top - deltaY;
                    newWidth = startValues.width - deltaX;
                    newHeight = startValues.height + deltaY;
                    break;
                }
            case "Right":
                {
                    newWidth = startValues.width - deltaX;
                    break;
                }
            case "RightBottom":
                {
                    newWidth = startValues.width - deltaX;
                    newHeight = startValues.height - deltaY;
                    break;
                }
            case "Bottom":
                {
                    newHeight = startValues.height - deltaY;
                    break;
                }
            case "LeftBottom":
                {
                    newLeft = startValues.left - deltaX;
                    newWidth = startValues.width + deltaX;
                    newHeight = startValues.height - deltaY;
                    break;
                }
            case "Left":
                {
                    newLeft = startValues.left - deltaX;
                    newWidth = startValues.width + deltaX;
                    break;
                }
        }
    }

    if (component.typeComponent == "StiTable" && resizingType == "Bottom") {
        this.ResizeAllTableCells(component, startValues.height != 0 ? newHeight / startValues.height : 1);
    }

    var jsObject = this;
    var checkStopResizing = function () {
        if (jsObject.options.in_resize[0].stopResizing) return true;
        else if (jsObject.options.in_resize.length > 3) {
            for (var i = 0; i < jsObject.options.in_resize[3].length; i++) {
                if (jsObject.options.in_resize[3][i].stopResizing) return true;
            }
        }
        return false;
    }

    var stopResizingAllComponents = false;
    if (isTableCell && !tableCell) this.options.oldPositions = [];

    if (newWidth > 0 && newHeight > 0) {
        if (isTableCell) {
            this.options.oldPositions.push([component, component.properties.unitLeft, component.properties.unitTop, component.properties.unitWidth, component.properties.unitHeight]);
        }

        component.stopResizing = false;
        stopResizingAllComponents = isTableCell ? checkStopResizing() : false;

        if (!stopResizingAllComponents) {
            var pageMarginsPx = this.options.report.pages[component.properties.pageName].marginsPx;
            component.properties.unitLeft = this.ConvertPixelToUnit((newLeft - pageMarginsPx[0]) / this.options.report.zoom, component.isDashboardElement);
            component.properties.unitTop = this.ConvertPixelToUnit((newTop - pageMarginsPx[1]) / this.options.report.zoom, component.isDashboardElement);
            component.properties.unitWidth = this.ConvertPixelToUnit(newWidth / this.options.report.zoom, component.isDashboardElement);
            component.properties.unitHeight = this.ConvertPixelToUnit(newHeight / this.options.report.zoom, component.isDashboardElement);
        }
    }
    else {
        if ((this.options.isTouchDevice && !isTableCell) || this.IsBandComponent(component) || this.IsCrossBandComponent(component)) {
            if (newWidth < 0) component.properties.unitWidth = this.ConvertPixelToUnit(1 / this.options.report.zoom, component.isDashboardElement);
            if (newHeight < 0) component.properties.unitHeight = this.ConvertPixelToUnit(1 / this.options.report.zoom, component.isDashboardElement);
        }
        else {
            if ((newWidth <= 0 || newHeight <= 0) && isTableCell) {
                component.stopResizing = true;
            }                       
            
            if (newWidth < 0) {                
                component.properties.invertWidth = true;
                newLeft += newWidth;
                newWidth = Math.abs(newWidth);
            }
            if (newHeight < 0) {
                component.properties.invertHeight = true;
                newTop += newHeight;
                newHeight = Math.abs(newHeight);
            }

            stopResizingAllComponents = component.stopResizing;

            if (!stopResizingAllComponents) {
                var pageMarginsPx = this.options.report.pages[component.properties.pageName].marginsPx;
                component.properties.unitLeft = this.ConvertPixelToUnit((newLeft - pageMarginsPx[0]) / this.options.report.zoom, component.isDashboardElement);
                component.properties.unitTop = this.ConvertPixelToUnit((newTop - pageMarginsPx[1]) / this.options.report.zoom, component.isDashboardElement);
                component.properties.unitWidth = this.ConvertPixelToUnit(newWidth / this.options.report.zoom, component.isDashboardElement);
                component.properties.unitHeight = this.ConvertPixelToUnit(newHeight / this.options.report.zoom, component.isDashboardElement);
            }
        }
    }

    if (!stopResizingAllComponents) component.repaint();

    clearTimeout(component.posTimer);
    component.posTimer = setTimeout(function () {
        jsObject.options.statusPanel.showPositions(
            component.properties.unitLeft,
            component.properties.unitTop,
            component.properties.unitWidth,
            component.properties.unitHeight
            );
    }, 20);

    if (!tableCell && isTableCell) {
        this.ResizeTableCells(mouseCurrentXPos, mouseCurrentYPos);

        if (checkStopResizing() && this.options.oldPositions) {
            for (var i = 0; i < this.options.oldPositions.length; i++) {
                var comp = this.options.oldPositions[i][0];
                comp.properties.unitLeft = this.options.oldPositions[i][1];
                comp.properties.unitTop = this.options.oldPositions[i][2];
                comp.properties.unitWidth = this.options.oldPositions[i][3];
                comp.properties.unitHeight = this.options.oldPositions[i][4];
                comp.repaint();
            }
            this.options.oldPositions = [];
        }
    }
}

StiMobileDesigner.prototype.SetComponentToNewPos = function (component, page, startPosX, startPosY, allChilds) {    
    var jsObject = this;
    var marginLeftPx = page.marginsPx[0];
    var marginTopPx = page.marginsPx[1];

    var canMove = !component.properties.locked &&
        ((component.properties.restrictions && (component.properties.restrictions == "All" || component.properties.restrictions.indexOf("AllowMove") >= 0)) ||
         !component.properties.restrictions);
    if (!canMove) return;

    var deltaX = jsObject.options.startMousePos[0] - mouseCurrentXPos;
    var deltaY = jsObject.options.startMousePos[1] - mouseCurrentYPos;
    
    var newPosX = startPosX - deltaX;
    var newPosY = startPosY - deltaY;

    if (newPosX < -component.realWidth) { deltaX = deltaX + newPosX; newPosX = -component.realWidth + 10 }
    if (newPosY < -component.realHeight) { deltaY = deltaY + newPosY; newPosY = -component.realHeight + 10; }
    if (newPosX > page.widthPx - 10) { deltaX = deltaX - newPosX; newPosX = page.widthPx - 10; }
    if (newPosY > page.heightPx - 10) { deltaY = deltaY - newPosY; newPosY = page.heightPx - 10; }

    component.setAttribute("transform", "translate(" + newPosX + ", " + newPosY + ")");
    component.setAttribute("left", newPosX);
    component.setAttribute("top", newPosY);
    jsObject.MoveAllChildsComponents(allChilds, deltaX, deltaY, marginLeftPx, marginTopPx);

    clearTimeout(component.posTimer);
    component.posTimer = setTimeout(function () {
        var pageMarginsPx = jsObject.options.report.pages[component.properties.pageName].marginsPx;
        component.properties.unitLeft = jsObject.ConvertPixelToUnit((newPosX - pageMarginsPx[0]) / jsObject.options.report.zoom, component.isDashboardElement);
        component.properties.unitTop = jsObject.ConvertPixelToUnit((newPosY - pageMarginsPx[1]) / jsObject.options.report.zoom, component.isDashboardElement);

        jsObject.options.statusPanel.showPositions(
            component.properties.unitLeft,
            component.properties.unitTop,
            component.properties.unitWidth,
            component.properties.unitHeight
            );
    }, 20);
}

StiMobileDesigner.prototype.MoveCopyComponent = function (mouseCurrentXPos, mouseCurrentYPos) {
    var jsObject = this;    
    var in_drag = this.options.in_drag;
    if (!this.options.in_drag) return;

    if (!this.options.movingCloneComponents) {
        var components = this.Is_array(in_drag[0]) ? in_drag[0] : [in_drag[0]];
        var componentChilds = this.Is_array(in_drag[3]) ? in_drag[3] : [in_drag[3]];
        var movingCloneComponents = [];

        for (var i = 0; i < components.length; i++) {
            var component = components[i];
            var cloneComponent = component.clone();

            movingCloneComponents.push(cloneComponent);
            cloneComponent.repaint();
            cloneComponent.page = this.options.report.pages[cloneComponent.properties.pageName];
            cloneComponent.page.appendChild(cloneComponent);

            cloneComponent.cloneChilds = [];
            var childs = componentChilds[i];
            for (var childName in childs) {
                var cloneChild = childs[childName].clone();
                cloneChild.startPosX = childs[childName].startPosX;
                cloneChild.startPosY = childs[childName].startPosY;

                cloneChild.repaint();
                cloneComponent.cloneChilds.push(cloneChild);
                cloneComponent.page.appendChild(cloneChild);
            }
        }

        this.options.movingCloneComponents = movingCloneComponents;
    }

    var startPosX = this.Is_array(in_drag[1]) ? in_drag[1] : [in_drag[1]];
    var startPosY = this.Is_array(in_drag[2]) ? in_drag[2] : [in_drag[2]];

    for (var i = 0; i < this.options.movingCloneComponents.length; i++) {
        this.SetComponentToNewPos(
            this.options.movingCloneComponents[i],
            this.options.movingCloneComponents[i].page,
            startPosX[i],
            startPosY[i],
            this.options.movingCloneComponents[i].cloneChilds
        );
    }
}

StiMobileDesigner.prototype.MoveComponent = function (mouseCurrentXPos, mouseCurrentYPos) {

    if (this.options.multiSelectHelperControls && this.options.mouseMoved) {
        if (this.options.selectedObjects != null && !this.options.multiSelectHelperControls.setOnTopLevel) {
            for (var i = 0; i < this.options.selectedObjects.length; i++) this.options.selectedObjects[i].setOnTopLevel();
            this.options.multiSelectHelperControls.setOnTopLevel = true;
        }
        this.DeleteSelectedLines();
    }

    var jsObject = this;
    var in_drag = this.options.in_drag;

    if (this.Is_array(in_drag[0])) {
        for (var i = 0; i < in_drag[0].length; i++) {
            if (this.IsTableCell(in_drag[0][i])) continue;
            this.SetComponentToNewPos(in_drag[0][i], this.options.report.pages[in_drag[0][i].properties.pageName], in_drag[1][i], in_drag[2][i], in_drag[3][i]);
        }
    }
    else {
        if (this.IsTableCell(in_drag[0])) return;
        this.SetComponentToNewPos(in_drag[0], this.options.report.pages[in_drag[0].properties.pageName], in_drag[1], in_drag[2], in_drag[3]);
    }
}

StiMobileDesigner.prototype.MoveAllChildsComponents = function (allChilds, moveX, moveY, marginLeftPx, marginTopPx) {
    for (var childName in allChilds) {
        var child = allChilds[childName];
        var startX = parseInt(child.startPosX);
        var startY = parseInt(child.startPosY);

        var newLeft = startX - moveX;
        var newTop = startY - moveY;

        var newXPos = (newLeft - marginLeftPx) / this.options.report.zoom;
        var newYPos = (newTop - marginTopPx) / this.options.report.zoom;

        var oldLeft = child.getAttribute("left");
        var oldTop = child.getAttribute("top");

        child.setAttribute("transform", "translate(" + newLeft + ", " + newTop + ")");
        child.setAttribute("left", newLeft);
        child.setAttribute("top", newTop);
        child.properties.unitLeft = this.ConvertPixelToUnit(newXPos, child.isDashboardElement);
        child.properties.unitTop = this.ConvertPixelToUnit(newYPos, child.isDashboardElement);
    }
}

StiMobileDesigner.prototype.RemoveComponent = function (component) {
    var components = this.Is_array(component) ? component : [component];
    var page = this.options.report.pages[components[0].properties.pageName];
    if (!page) return;

    this.SendCommandRemoveComponent(component);

    for (var i = 0; i < components.length; i++) {
        var childs = components[i].getAllChildsComponents();
        for (var indexChild in childs) {
            page.removeChild(childs[indexChild]);
            delete page.components[childs[indexChild].properties.name];
            delete childs[indexChild];
        }
        if (components[i].isDashboardElement && this.options.currentForm && this.options.currentForm.dockingComponent == components[i]) {
            this.options.currentForm.changeVisibleState(false);
        }
        if (page.components[components[i].properties.name]) {
            page.removeChild(components[i]);
            delete page.components[components[i].properties.name];
        }
        delete components[i];
    }
    delete components;
    this.options.selectedObjects = null;
    page.setSelected();
    this.UpdatePropertiesControls();
}

StiMobileDesigner.prototype.CopyComponent = function (component) {
    this.SendCommandSetToClipboard(component);
}

StiMobileDesigner.prototype.CutComponent = function (component) {
    this.SendCommandSetToClipboard(component);
    this.RemoveComponent(component);
}

StiMobileDesigner.prototype.RenameComponent = function(component, newName) {
    var page = this.options.report.pages[component.properties.pageName];
    page.components[newName] = component;
    delete page.components[component.properties.name];
    component.properties.name = newName;
    component.repaint();
}

StiMobileDesigner.prototype.RepaintColumnsLines = function (component) {
    var jsObject = this;
    var columnsCount = component.properties.columns ? jsObject.StrToInt(component.properties.columns) : 0;

    //remove old lines
    if (component.controls.columnLines) {
        for (var i = 0; i < component.controls.columnLines.length; i++) {
            component.removeChild(component.controls.columnLines[i]);
        }
        component.controls.columnLines = null;
    }

    //add new lines
    if (columnsCount > 1 && component.controls) {
        var topMargin = component.marginsPx ? component.marginsPx[1] : 0;
        var bottomMargin = component.marginsPx ? component.marginsPx[3] : 0;
        var leftMargin = component.marginsPx ? component.marginsPx[0] : 0;
        var rightMargin = component.marginsPx ? component.marginsPx[2] : 0;
        var componentWidthPx = parseInt(component.getAttribute("width")) - leftMargin - rightMargin;
        var componentHeightPx = parseInt(component.getAttribute("height")) - topMargin - bottomMargin;
        var columnGapsPx = jsObject.ConvertUnitToPixel(jsObject.StrToDouble(component.properties.columnGaps), component.isDashboardElement) * jsObject.options.report.zoom;

        var getColumnWidthPx = function () {
            var panelColumnWidthPx = jsObject.ConvertUnitToPixel(jsObject.StrToDouble(component.properties.columnWidth), component.isDashboardElement) * jsObject.options.report.zoom;
            if (panelColumnWidthPx == 0) {
                if (columnsCount == 0) return componentWidthPx;
                panelColumnWidthPx = (componentWidthPx / columnsCount) - columnGapsPx;
            }
            return panelColumnWidthPx;
        }

        var addRedLine = function (x1, y1, x2, y2) {
            var redLine = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "line") : document.createElement("line");
            component.appendChild(redLine);
            if (!component.controls.columnLines) component.controls.columnLines = [];
            component.controls.columnLines.push(redLine);

            redLine.style.strokeDasharray = "2,2";
            redLine.style.stroke = "#ff0000";
            var roundedCoordinates = jsObject.GetRoundedLineCoordinates([x1, y1, x2, y2]);

            redLine.setAttribute("x1", roundedCoordinates[0] + jsObject.options.xOffset);
            redLine.setAttribute("y1", roundedCoordinates[1] + jsObject.options.yOffset);
            redLine.setAttribute("x2", roundedCoordinates[2] + jsObject.options.xOffset);
            redLine.setAttribute("y2", roundedCoordinates[3] + jsObject.options.yOffset);

            return redLine;
        }

        var columnWidthPx = getColumnWidthPx();
        var pos = columnWidthPx;

        for (var index = 1; index < columnsCount; index++) {
            addRedLine(pos + leftMargin, 0 + topMargin, pos + leftMargin, componentHeightPx + topMargin);
            addRedLine(pos + leftMargin + columnGapsPx, 0 + topMargin, pos + leftMargin + columnGapsPx, componentHeightPx + topMargin);
            pos += columnWidthPx + columnGapsPx;
        }
        if (pos + leftMargin < componentWidthPx)
            addRedLine(pos + leftMargin, 0 + topMargin, pos + leftMargin, componentHeightPx + topMargin);
    }
}

StiMobileDesigner.prototype.ResizeTableCells = function (mouseCurrentXPos, mouseCurrentYPos) {
    var resizingCells = this.options.in_resize[3];
    if (resizingCells) {
        for (var i = 0; i < resizingCells.length; i++) {
            this.ResizeComponent(mouseCurrentXPos, mouseCurrentYPos, resizingCells[i]);
        }
    }
}

StiMobileDesigner.prototype.ResizeAllTableCells = function (table, cellsZoom) {
    var resizingCells = this.options.in_resize[3];
    var tableTop = parseInt(table.getAttribute("top"));
    var deltaY = cellsZoom * tableTop - tableTop;
    var pageMarginsPx = this.options.report.pages[table.properties.pageName].marginsPx;

    if (resizingCells) {
        for (var i = 0; i < resizingCells.length; i++) {
            var cell = resizingCells[i];

            var newHeight = cell.startValues.height * cellsZoom;
            var newTop = cell.startValues.top * cellsZoom - deltaY;

            if (cell.startValues.top != newTop) cell.properties.unitTop = this.ConvertPixelToUnit((newTop - pageMarginsPx[1]) / this.options.report.zoom);
            if (cell.startValues.height != newHeight) cell.properties.unitHeight = this.ConvertPixelToUnit(newHeight / this.options.report.zoom);
            cell.repaint();
        }
    }
}

StiMobileDesigner.prototype.ApplyComponentSizes = function (component) {
    var pageName = component.properties.pageName;
    var marginsPx = this.options.report.pages[pageName].marginsPx;

    var marginLeftPx = marginsPx[0];
    var marginTopPx = marginsPx[1];

    var leftPx = this.StrToDouble(component.getAttribute("left"));
    var topPx = this.StrToDouble(component.getAttribute("top"));

    var leftProperty = leftPx - marginLeftPx;
    var topProperty = topPx - marginTopPx;

    component.properties.unitLeft = this.ConvertPixelToUnit(leftProperty / this.options.report.zoom, component.isDashboardElement);
    component.properties.unitTop = this.ConvertPixelToUnit(topProperty / this.options.report.zoom, component.isDashboardElement);

    if (this.options.in_resize) {
        //debugger;
        var widthProperty = this.StrToDouble(component.getAttribute("width"));
        var heightProperty = this.StrToDouble(component.getAttribute("height"));

        component.properties.unitWidth = this.ConvertPixelToUnit(widthProperty / this.options.report.zoom, component.isDashboardElement);
        component.properties.unitHeight = this.ConvertPixelToUnit(heightProperty / this.options.report.zoom, component.isDashboardElement);
    }
}

StiMobileDesigner.prototype.CloneComponent = function (component) {
    var compObject = {
        properties: this.CopyObject(component.properties)
    }

    compObject.typeComponent = component.typeComponent;
    compObject.name = component.properties.name;    
    compObject.parentName = component.properties.parentName;
    compObject.parentIndex = component.properties.parentIndex;
    compObject.componentIndex = component.properties.componentIndex;
    compObject.childs = component.properties.childs;
    compObject.svgContent = component.properties.svgContent;
    compObject.pageName = component.properties.pageName;
    compObject.componentRect = component.properties.unitLeft + "!" + component.properties.unitTop + "!" +
        component.properties.unitWidth + "!" + component.properties.unitHeight;
   
    return this.CreateComponent(compObject);
}