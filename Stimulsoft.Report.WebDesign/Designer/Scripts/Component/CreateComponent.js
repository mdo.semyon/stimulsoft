
StiMobileDesigner.prototype.CreateComponent = function (compObject, isCrossTabField, isDashboardElement) {
    var component = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'g') : document.createElement("g");
    component.jsObject = this;
    component.isCrossTabField = isCrossTabField;
    component.isDashboardElement = isDashboardElement;
    if (!ComponentCollection[compObject.typeComponent]) return null;

    //Set Properties  
    this.CreateComponentProperties(component, compObject);

    //Create Controls   
    component.controls = {};
    this.CreateComponentShadow(component);
    this.CreateComponentBackGround(component);
    this.CreateComponentImageContent(component);
    this.CreateComponentSvgContent(component);
    if (ComponentCollection[component.typeComponent][2] != "none") this.CreateComponentHeader(component);
    if (ComponentCollection[component.typeComponent][3] != "none") this.CreateComponentNameContent(component);
    if (ComponentCollection[component.typeComponent][6] != "0") this.CreateComponentCorners(component);
    if (component.typeComponent == "StiCrossTab") this.CreateCrossTabContainer(component);
    this.CreateComponentBorder(component);    

    if (component.isDashboardElement) {
        this.CreateComponentDashboardEditIcon(component);
        if (this.DashboardElementHaveHeader(component.typeComponent)) {
            this.CreateComponentDragDropLabel(component);
        }
    }

    if (this.options.isTouchDevice && !isCrossTabField)
        this.CreateComponentResizingIcons(component);
    else
        this.CreateComponentResizingPoints(component);

    //Create Methods
    this.CreateComponentEvents(component);
    component.repaint = function () { this.jsObject.RepaintComponent(this); }
    component.remove = function () { this.jsObject.RemoveComponent(this); }
    component.copy = function () { this.jsObject.CopyComponent(this); }
    component.clone = function () { return this.jsObject.CloneComponent(this); }
    component.cut = function () { this.jsObject.CutComponent(this); }
    component.setSelected = function () { this.jsObject.SetSelectedObject(this); }
    component.rename = function (newName) { this.jsObject.RenameComponent(this, newName); }
    component.changeVisibilityStateResizingIcons = function (state) { this.jsObject.ChangeVisibilityStateResizingIcons(this, state); }
    component.setOnTopLevel = function () { this.jsObject.SetComponentOnTopLevel(this); }
    component.getAllChildsComponents = function () { return this.jsObject.GetAllChildsComponents(this); }
    
    return component;
}

StiMobileDesigner.prototype.CreateComponentProperties = function (component, compObject) {    
    component.properties = {};
    component.properties.name = compObject.name;
    component.typeComponent = compObject.typeComponent;
    var rect = compObject.componentRect.split("!");
    component.properties.unitLeft = rect[0];
    component.properties.unitTop = rect[1];
    component.properties.unitWidth = rect[2];
    component.properties.unitHeight = rect[3];
    component.properties.parentName = compObject.parentName;
    component.properties.parentIndex = compObject.parentIndex;
    component.properties.componentIndex = compObject.componentIndex;
    component.properties.childs = compObject.childs;
    component.properties.svgContent = compObject.svgContent;
    component.properties.pageName = compObject.pageName;
    this.WriteAllProperties(component, compObject.properties);
}

StiMobileDesigner.prototype.CreateComponentResizingPoints = function (component) {
    var jsObject = this;
        
    var createResizingPoint = function (isBand, resizingType) {
        var point = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "rect") : document.createElement("rect");
        point.setAttribute("width", isBand ? 5 : 4);
        point.setAttribute("height", isBand ? 5 : 4);
        point.style.fill = isBand ? "#ffffff" : "#696969";
        point.style.stroke = "#696969";
        point.resizingType = resizingType;
        point.style.display = "none";
        point.component = component;

        if (!isBand) {
            point.style.cursor = jsObject.GetCursorType(resizingType);
            point.onmousedown = function (event) {
                if (jsObject.options.drawComponent) return;
                event.preventDefault();
                jsObject.options.startMousePos = [event.clientX || event.x, event.clientY || event.y];
                if (jsObject.options.currentPage) jsObject.options.currentPage.style.cursor = this.style.cursor;

                var startValues = {};
                startValues.height = parseInt(this.component.getAttribute("height"));
                startValues.width = parseInt(this.component.getAttribute("width"));
                startValues.left = parseInt(this.component.getAttribute("left"));
                startValues.top = parseInt(this.component.getAttribute("top"));
                jsObject.options.in_resize = [this.component, this.resizingType, startValues];

                if (jsObject.IsTableCell(this.component))
                    jsObject.options.in_resize.push(jsObject.GetAllResizingCells(this.component, this.resizingType, startValues));
                else if (this.component.typeComponent == "StiTable")
                    jsObject.options.in_resize.push(jsObject.GetAllResizingCells(this.component));
            }
        }

        return point;
    }

    var createResizingArrow = function (resizingType) {
        var resizingArrow = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'image') : document.createElement("image");
        resizingArrow.setAttribute("height", resizingType == "Bottom" || resizingType == "Top" ? 11 : 7);
        resizingArrow.setAttribute("width", resizingType == "Bottom" || resizingType == "Top" ? 7 : 11);
        resizingArrow.resizingType = resizingType;
        resizingArrow.href.baseVal = jsObject.options.images[resizingType == "Bottom" || resizingType == "Top" ? "ResizeIcons.ResizeVert.png" : "ResizeIcons.ResizeHor.png"];
        resizingArrow.style.display = "none";
        resizingArrow.component = component;
        resizingArrow.style.cursor = jsObject.GetCursorType(resizingType);

        resizingArrow.onmousedown = function (event) {
            if (jsObject.options.drawComponent) return;
            event.preventDefault();
            jsObject.options.startMousePos = [event.clientX || event.x, event.clientY || event.y];
            var startValues = {};
            startValues.height = parseInt(this.component.getAttribute("height"));
            startValues.width = parseInt(this.component.getAttribute("width"));
            startValues.left = parseInt(this.component.getAttribute("left"));
            startValues.top = parseInt(this.component.getAttribute("top"));
            jsObject.options.in_resize = [this.component, this.resizingType, startValues];

            if (jsObject.IsTableCell(this.component))
                jsObject.options.in_resize.push(jsObject.GetAllResizingCells(this.component, this.resizingType, startValues));
            else if (this.component.typeComponent == "StiTable")
                jsObject.options.in_resize.push(jsObject.GetAllResizingCells(this.component));
        }

        return resizingArrow;
    }

    component.controls.resizingPoints = [];
    var resizingType = ["LeftTop", "Top", "RightTop", "Right", "RightBottom", "Bottom", "LeftBottom", "Left"];

    for (i = 0; i <= 7; i++) {
        if (((!jsObject.IsBandComponent(component) && !jsObject.IsCrossBandComponent(component)) ||
            (i % 2 == 0 && (jsObject.IsBandComponent(component) || jsObject.IsCrossBandComponent(component)))) &&
                (!((i != 3 && i != 7 && component.typeComponent == "StiHorizontalLinePrimitive") ||
                (i != 1 && i != 5 && component.typeComponent == "StiVerticalLinePrimitive"))))
        {
            var resizingPoint = createResizingPoint(jsObject.IsBandComponent(component) || jsObject.IsCrossBandComponent(component), resizingType[i]);
            component.controls.resizingPoints[i] = resizingPoint;
            component.appendChild(resizingPoint);
        }
        else
            component.controls.resizingPoints[i] = null;
    }

    if (jsObject.IsBandComponent(component)) {
        var pointIndex = component.typeComponent == "StiPageFooterBand" ? 1 : 5;
        component.controls.resizingPoints[pointIndex] = createResizingArrow(resizingType[pointIndex]);
        component.appendChild(component.controls.resizingPoints[pointIndex]);
    }

    if (jsObject.IsCrossBandComponent(component)) {
        component.controls.resizingPoints[3] = createResizingArrow(resizingType[3]);
        component.appendChild(component.controls.resizingPoints[3]);
    }
}

StiMobileDesigner.prototype.CreateComponentResizingIcons = function (component) {
    var resizingType = ["Move", "ResizeWidth", "ResizeDiagonal", "ResizeHeight"];

    var images = [
        this.options.images[this.options.isTouchDevice ? "Arrow_cross_touch.png" : "Arrow_cross.png"],
        this.options.images[this.options.isTouchDevice ? "Arrow_hor_touch.png" : "Arrow_hor.png"],
        this.options.images[this.options.isTouchDevice ? "Arrow_diag_touch.png" : "Arrow_diag.png"],
        this.options.images[this.options.isTouchDevice ? "Arrow_vert_touch.png" : "Arrow_vert.png"]
    ];

    if (component.typeComponent == "StiPageFooterBand") {
        images[0] = this.options.images[this.options.isTouchDevice ? "Arrow_vert_up_touch.png" : "Arrow_vert_up.png"];
        images[3] = this.options.images[this.options.isTouchDevice ? "Arrow_cross_touch.png" : "Arrow_cross.png"];
        resizingType = ["ResizeHeightUp", "ResizeWidth", "ResizeDiagonal", "Move"];
    }

    var iconNumbers = ComponentCollection[component.typeComponent][4].split(",");
    component.controls.resizingIcons = [];

    for (i = 0; i < iconNumbers.length; i++) {
        var numIcon = iconNumbers[i];
        var resizingIcon = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'image') : document.createElement("image");
        resizingIcon.jsObject = this;
        resizingIcon.thisComponent = component;
        resizingIcon.style.opacity = 0.8;
        resizingIcon.href.baseVal = images[numIcon];
        resizingIcon.resizingType = resizingType[numIcon];
        resizingIcon.setAttribute("height", this.options.isTouchDevice ? 50 : 24);
        resizingIcon.setAttribute("width", this.options.isTouchDevice ? 50 : 24);
        if ((numIcon != 0 && component.typeComponent != "StiPageFooterBand") || (numIcon != 3 && component.typeComponent == "StiPageFooterBand")) {
            resizingIcon.ontouchstart = function (event, mouseProcess) {
                var this_ = this;
                this.isTouchProcessFlag = mouseProcess ? false : true;

                if (this.jsObject.options.drawComponent) return;
                if (event && this.jsObject.options.isTouchDevice) {
                    event.preventDefault();
                    this.jsObject.options.startMousePos = [event.touches[0].pageX, event.touches[0].pageY];
                }
                var startValues = {};
                startValues.height = parseInt(this.thisComponent.getAttribute("height"));
                startValues.width = parseInt(this.thisComponent.getAttribute("width"));
                startValues.left = parseInt(this.thisComponent.getAttribute("left"));
                startValues.top = parseInt(this.thisComponent.getAttribute("top"));
                this.jsObject.options.in_resize = [this.thisComponent, this.resizingType, startValues];
                if (this.jsObject.IsTableCell(this.thisComponent))
                    this.jsObject.options.in_resize.push(this.jsObject.GetAllResizingCells(this.thisComponent, this.resizingType, startValues));
                else if (this.thisComponent.typeComponent == "StiTable")
                    this.jsObject.options.in_resize.push(this.jsObject.GetAllResizingCells(this.thisComponent));

                setTimeout(function () {
                    this_.isTouchProcessFlag = false;
                }, 1000);
            }
            resizingIcon.onmousedown = function (event) {
                if (this.isTouchProcessFlag || this.jsObject.options.drawComponent) return;
                event.preventDefault();
                this.jsObject.options.startMousePos = [event.clientX || event.x, event.clientY || event.y];
                this.ontouchstart(null, true);
            }
        }

        resizingIcon.style.visibility = "hidden";
        component.controls.resizingIcons[numIcon] = resizingIcon;
        component.appendChild(resizingIcon);
    }

    if (ComponentCollection[component.typeComponent][7]) {
        var editIcon = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'image') : document.createElement("image");
        component.appendChild(editIcon);
        component.controls.editIcon = editIcon;
        editIcon.style.opacity = 0.8;
        editIcon.jsObject = this;
        editIcon.href.baseVal = this.options.images["IconEdit.png"];
        editIcon.setAttribute("height", 24);
        editIcon.setAttribute("width", 24);
        editIcon.style.visibility = "hidden";
        editIcon.action = function () {
            this.jsObject.options.propertiesPanelPressed = true;
            this.jsObject.ShowComponentForm(component);
        }
        if (!this.options.isTouchDevice) {
            editIcon.onmouseover = function () { this.style.opacity = 1; };
            editIcon.onmouseout = function () { this.style.opacity = 0.8; };
        }
        editIcon.onmousedown = function () { editIcon.action(); };
        editIcon.ontouchstart = function () { editIcon.action(); };
    }
}

StiMobileDesigner.prototype.CreateComponentDashboardEditIcon = function (component) {
    var editDbsIcon = this.CreateSvgButton("Dashboards.BigEdit.png", 28, 22);
    editDbsIcon.style.visibility = "hidden";
    component.appendChild(editDbsIcon);
    component.controls.editDbsIcon = editDbsIcon;
    
    editDbsIcon.action = function () {
        this.jsObject.ShowComponentForm(component);
    }
}

StiMobileDesigner.prototype.CreateComponentDragDropLabel = function (component) {
    var container = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'svg') : document.createElement("svg");
    component.controls.dragDropHintLabel = container;
    component.appendChild(container);
    
    var img = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'image') : document.createElement("image");
    if (this.options.images["Dashboards.HintLabels." + component.typeComponent + ".png"]) {
        img.href.baseVal = this.options.images["Dashboards.HintLabels." + component.typeComponent + ".png"];
    }
    container.appendChild(img);
    component.controls.dragDropHintLabel.image = img;

    var text = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'text') : document.createElement("text");
    text.setAttribute("font-family", "Arial");
    text.setAttribute("fill", "#c4c4c4");        
    text.textContent = this.loc.Dashboard.DragDropDataFromDictionary;
    container.appendChild(text);
    component.controls.dragDropHintLabel.text = text;
}

StiMobileDesigner.prototype.CreateComponentCorners = function (component) {   
    component.controls.corners = [];
    for (i = 0; i < 4; i++) {
        var corner  = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'polyline') : document.createElement("polyline");
        corner.setAttribute("fill", "none");
        corner.setAttribute("stroke-width", "0,1px");
        corner.setAttribute("stroke", "black");        
        component.controls.corners[i] = corner;        
        component.appendChild(corner);
    }
}

StiMobileDesigner.prototype.CreateComponentBorder = function (component) {  
    component.controls.borders = [];
    for (i = 0; i < 8; i ++) {
        component.controls.borders[i] = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "line") : document.createElement("line");
        component.appendChild(component.controls.borders[i]);
    }    
}

StiMobileDesigner.prototype.CreateComponentBackGround = function (component) {    
    var backGround = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "rect") : document.createElement("rect");
    backGround.style.stroke = "Transparent";    
    component.controls.background = backGround;
    component.appendChild(backGround);
}

StiMobileDesigner.prototype.CreateComponentHeader = function (component) {  
    var header = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'rect') : document.createElement("rect");
    component.controls.header = header;
    component.appendChild(header);
}

StiMobileDesigner.prototype.CreateComponentNameContent = function (component) {  
    var nameContent = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'svg') : document.createElement("svg");
    component.controls.nameContent = nameContent;
    component.appendChild(nameContent);

    if (this.DashboardElementHaveHeader(component.typeComponent)) {
        var nameBar = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'rect') : document.createElement("rect");
        component.controls.nameBar = nameBar;
        nameContent.appendChild(nameBar);
    }

    var nameText = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'text') : document.createElement("text");
    nameText.setAttribute("font-family", "Arial");
    nameText.setAttribute("fill", "Black");        
    component.controls.nameText = nameText;
    nameContent.appendChild(nameText);
}

StiMobileDesigner.prototype.CreateComponentImageContent = function (component) {
    var parentImageContent = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'svg') : document.createElement("svg");
    var imageContent = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'image') : document.createElement("image");
    parentImageContent.appendChild(imageContent);
    component.controls.imageContent = imageContent;
    component.controls.parentImageContent = parentImageContent;
    component.appendChild(parentImageContent);
}

StiMobileDesigner.prototype.CreateComponentSvgContent = function (component) {
    var svgContent = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'svg') : document.createElement("svg");
    component.controls.svgContent = svgContent;
    component.appendChild(svgContent);

    var svgContentChild = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "g") : document.createElement("g");
    component.controls.svgContentChild = svgContentChild;
    svgContent.appendChild(svgContentChild);

    var svgContentInnerChild = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "g") : document.createElement("g");
    component.controls.svgContentInnerChild = svgContentInnerChild;
    svgContentChild.appendChild(svgContentInnerChild);

    var svgContentText = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "text") : document.createElement("text");
    component.controls.svgContentText = svgContentText;
    svgContentInnerChild.appendChild(svgContentText);
    svgContentText.clear = function () { while (this.childNodes[0]) this.removeChild(this.childNodes[0]); }
}

StiMobileDesigner.prototype.CreateComponentShadow = function (component) {    
    var shadow = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "rect") : document.createElement("rect");
    component.controls.shadow = shadow;
    component.appendChild(shadow);
}

StiMobileDesigner.prototype.CreateCrossTabContainer = function (component) {
    var crossTabContainer = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "svg") : document.createElement("svg");
    component.controls.crossTabContainer = crossTabContainer;
    component.appendChild(crossTabContainer);
}

StiMobileDesigner.prototype.CreateCrossTabFieldComponent = function (compObject, inCrossTabForm) {
    var component = this.CreateComponent(compObject, true);
    component.inCrossTabForm = inCrossTabForm;

    component.ontouchmove = null;    
    component.onmouseup = null;
    component.ondblclick = null;

    //Override
    for (i = 0; i <= 7; i++) {
        var resizingPoint = component.controls.resizingPoints[i];
        if (resizingPoint) {
            resizingPoint.onmousedown = null;
            resizingPoint.style.fill = "red";
            resizingPoint.style.strokeWidth = "red";
            resizingPoint.style.stroke = "red";
            resizingPoint.style.cursor = "default";
        }
    }

    component.onmousedown = function (event) {
        if (this.isTouchStartFlag) return;
        if (event) {
            event.preventDefault();
            event.stopPropagation();
            component.jsObject.options.mobileDesigner.pressedDown();
        }
        this.action();
    }

    component.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        this.action();
        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }
        clearTimeout(this.isTouchStartTimer);
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    if (inCrossTabForm) {
        component.setSelected = function (state) {
            if (!state) {
                this.jsObject.ChangeVisibilityStateResizingIcons(this, false);
                if (this == this.jsObject.options.selectedCrossTabField) this.jsObject.options.selectedCrossTabField = null;
                return;
            }
            if (this.jsObject.options.selectedCrossTabField) this.jsObject.options.selectedCrossTabField.setSelected(false);
            this.jsObject.options.selectedCrossTabField = this;
            this.jsObject.ChangeVisibilityStateResizingIcons(this, true);
            this.parentContainer.removeChild(this);
            this.parentContainer.appendChild(this);
        }

        component.action = function () {
            this.setSelected(true);
            if (!this.jsObject.options.propertiesPanel.editCrossTabMode) {
                this.jsObject.options.propertiesPanel.setEditCrossTabMode(true);
            }
            if (this.jsObject.options.propertiesPanel.editCrossTabPropertiesPanel) {
                this.jsObject.options.propertiesPanel.editCrossTabPropertiesPanel.updateProperties(this.properties);
            }
        }
    }
    else {
        component.action = function () {
            this.setSelected(true);

            var propertiesPanel = this.jsObject.options.propertiesPanel;
            propertiesPanel.returnToPanel = null;
            propertiesPanel.editCrossTabMode = true;
            propertiesPanel.mainPropertiesPanel.style.display = "none";
            propertiesPanel.propertiesToolBar.changeVisibleState(false);

            if (propertiesPanel.eventsMode) propertiesPanel.setEventsMode(false);

            if (!propertiesPanel.editCrossTabPropertiesPanel) {
                    propertiesPanel.editCrossTabPropertiesPanel = propertiesPanel.jsObject.CrossTabPropertiesPanel();
                    propertiesPanel.containers["Properties"].appendChild(propertiesPanel.editCrossTabPropertiesPanel);
                }
                propertiesPanel.editCrossTabPropertiesPanel.style.display = "";

            if (this.jsObject.options.propertiesPanel.editCrossTabPropertiesPanel) {
                this.jsObject.options.propertiesPanel.editCrossTabPropertiesPanel.updateProperties(this.properties);
            }
            this.jsObject.UpdatePropertiesControls();
        }
    }

    

    return component;
}

StiMobileDesigner.prototype.CreateSvgButton = function (imageName, buttonSize, imageSize) {
    var button = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'svg') : document.createElement("svg");
    var width = buttonSize || 24;
    var height = buttonSize || 24;
    button.setAttribute("height", width);
    button.setAttribute("width", height);
    button.jsObject = this;

    var rect = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'rect') : document.createElement("rect");
    rect.style.fill = "#ffffff";
    rect.style.stroke = "#ababab";
    rect.setAttribute("height", width);
    rect.setAttribute("width", height);
    button.appendChild(rect);

    var img = ("createElementNS" in document) ? document.createElementNS('http://www.w3.org/2000/svg', 'image') : document.createElement("image");
    img.setAttribute("height", imageSize || 16);
    img.setAttribute("width", imageSize || 16);
    img.setAttribute("x", imageSize ? (width - imageSize) / 2 : 6);
    img.setAttribute("y", imageSize ? (width - imageSize) / 2 : 6);
    button.appendChild(img);
    img.href.baseVal = this.options.images[imageName];
        
    if (!this.options.isTouchDevice) {
        button.onmouseover = function () { rect.style.fill = "#d3d3d3"; };
        button.onmouseout = function () { rect.style.fill = "#ffffff"; };
    }
    button.onmousedown = function () { button.clicked(); };
    button.ontouchstart = function () { button.clicked(); };

    button.clicked = function () {
        rect.style.fill = "#ffffff";
        button.action();
    };

    button.action = function () { }; 

    return button;
}