
StiMobileDesigner.prototype.DropDictionaryItemToDashboardElement = function (component, itemInDrag, event) {
    var form = this.options.currentForm && this.options.currentForm.visible && this.options.currentForm.dockingComponent == component ? this.options.currentForm : null;

    switch (component.typeComponent) {
        case "StiTableElement": {
            if (form) {
                form.controls.dataContainer.onmouseup(event);
            }
            else {
                this.InitializeEditTableElementForm(function (form) {
                    form.currentTableElement = component;
                    form.controls.dataContainer.onmouseup(event);
                });
            }
            break;
        }
        case "StiPivotElement": {
            if (form) {
                form.controls.columnsBlock.container.onmouseup(event);
            }
            else {
                this.InitializeEditPivotElementForm(function (form) {
                    form.currentPivotElement = component;
                    form.controls.columnsBlock.container.onmouseup(event);
                });
            }
            break;
        }
        case "StiChartElement": {
            if (form) {
                form.controls.valuesBlock.container.onmouseup(event);
            }
            else {
                this.InitializeEditChartElementForm(function (form) {
                    form.currentChartElement = component;
                    form.controls.valuesBlock.container.onmouseup(event);
                });
            }
            break;
        }
        case "StiGaugeElement": {
            if (form) {
                form.controls.valueDataColumn.innerContainer.onmouseup(event);
            }
            else {
                this.InitializeEditGaugeElementForm(function (form) {
                    form.currentGaugeElement = component;
                    form.controls.valueDataColumn.innerContainer.onmouseup(event);
                });
            }
            break;
        }
        case "StiIndicatorElement": {
            if (form) {
                form.controls.valueDataColumn.innerContainer.onmouseup(event);
            }
            else {
                this.InitializeEditIndicatorElementForm(function (form) {
                    form.currentIndicatorElement = component;
                    form.controls.valueDataColumn.innerContainer.onmouseup(event);
                });
            }
            break;
        }
        case "StiProgressElement": {
            if (form) {
                form.controls.valueDataColumn.innerContainer.onmouseup(event);
            }
            else {
                this.InitializeEditProgressElementForm(function (form) {
                    form.currentProgressElement = component;
                    form.controls.valueDataColumn.innerContainer.onmouseup(event);
                });
            }
            break;
        }
        case "StiMapElement": {
            var applyDropEvent = function (form, component, event) {
                if (component.properties.mapMode == "Choropleth" && component.properties.dataFrom == "DataColumns") {
                    form.controls.keyDataColumn.innerContainer.onmouseup(event);
                }
                else if (component.properties.mapMode == "Online") {
                    form.controls.latitudeDataColumn.innerContainer.onmouseup(event);
                }
            }

            if (form) {
                applyDropEvent(form, component, event);
            }
            else {
                this.InitializeEditMapElementForm(function (form) {
                    form.currentMapElement = component;
                    applyDropEvent(form, component, event);
                });
            }
            break;
        }
    }
    
}