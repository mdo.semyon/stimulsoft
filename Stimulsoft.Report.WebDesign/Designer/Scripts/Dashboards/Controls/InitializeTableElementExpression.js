﻿
StiMobileDesigner.prototype.TableElementExpression = function (name, width, toolTip, items) {
    var control = this.DropDownList(name, width, toolTip, items, false, false, 24, false);
    control.button.imageCell.style.padding = "0 7px 0 7px";

    return control;
}