﻿
StiMobileDesigner.prototype.ChartSeriesTypeControl = function (name, width) {
    var control = this.CreateHTMLTable();
    control.isEnabled = true;

    var button = this.SmallButton(null, null, "Chart", "ChartClusteredColumn.png", null, "Down", this.GetStyles("FormButton"), true);
    if (width) button.style.width = width + "px";
    button.style.height = "38px";
    button.innerTable.style.width = "100%";
    button.imageCell.style.width = "1px";
    button.arrowCell.style.width = "1px";
    control.addCell(button);

    var allItems = {
        mainMenu: this.GetAddSeriesItems(),
        clusteredColumn: this.GetChartClusteredColumnItems(),
        line: this.GetChartLineItems(),
        area: this.GetChartAreaItems(),
        range: this.GetChartRangeItems(),
        clusteredBar: this.GetChartClusteredBarItems(),
        scatter: this.GetChartScatterItems(),
        pie: this.GetChartPieItems(),
        radar: this.GetChartRadarItems(),
        funnel: this.GetChartFunnelItems(),
        financial: this.GetChartFinancialItems(),
        others: this.GetChartOthersItems()
    };

    var menu = this.VerticalMenu(name + "Menu", button, "Down", allItems.mainMenu, this.GetStyles("MenuMiddleItem"))
    menu.firstChild.style.maxHeight = "1000px";

    this.InitializeSubMenu(name + "ClusteredColumnMenu", allItems.clusteredColumn, menu.items["ClusteredColumn"], menu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "LineMenu", allItems.line, menu.items["Line"], menu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "AreaMenu", allItems.area, menu.items["Area"], menu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "RangeMenu", allItems.range, menu.items["Range"], menu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "ClusteredBarMenu", allItems.clusteredBar, menu.items["ClusteredBar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "ScatterMenu", allItems.scatter, menu.items["Scatter"], menu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "PieMenu", allItems.pie, menu.items["Pie"], menu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "RadarMenu", allItems.radar, menu.items["Radar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "FunnelMenu", allItems.funnel, menu.items["Funnel"], menu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "FinancialMenu", allItems.financial, menu.items["Financial"], menu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "OthersMenu", allItems.others, menu.items["Others"], menu, "MenuMiddleItem");

    button.action = function () {
        menu.changeVisibleState(!menu.visible);
    }

    control.action = function () { }

    control.setKey = function (key) {
        this.key = key;
        var seriesType = "Sti" + key + "Series";

        for (var items in allItems) {
            if (allItems[items].length) {
                for (var i = 0; i < allItems[items].length; i++) {
                    if (seriesType == allItems[items][i].key) {
                        button.image.src = this.jsObject.options.images[allItems[items][i].imageName];
                        button.caption.innerHTML = allItems[items][i].caption;
                    }
                }
            }
        }
    }

    control.setEnabled = function (state) {
        control.isEnabled = state;
        button.setEnabled(state);
    }

    menu.update = function () {
        var types = control.seriesTypes;
        if (!types) return;

        for (var mainItemName in menu.items) {
            if (mainItemName == "separator") continue;
            var mainItem = menu.items[mainItemName];
            var categoryEnabled = false;
            for (subItemName in mainItem.menu.items) {
                if (subItemName == "separator") continue;
                var subItem = mainItem.menu.items[subItemName];
                var seriesType = subItem.key;
                var finded = false;

                for (var i = 0; i < types.length; i++) {
                    var type = "Sti" + types[i] + "Series";
                    if (type == seriesType) {
                        finded = true;
                        categoryEnabled = true;
                        break;
                    }
                }

                if (types.length == 0) {
                    subItem.setEnabled(true);
                    categoryEnabled = true;
                }
                else
                    subItem.setEnabled(finded);

            }
            mainItem.setEnabled(categoryEnabled);
        }
    }

    menu.onshow = function () {
        menu.update();
    }

    menu.action = function (menuItem) {
        if (menuItem.haveSubMenu) return;
        menu.changeVisibleState(false);
        control.setKey(menuItem.key.replace("Sti", "").replace("Series", ""));
        control.action();
    }

    return control;
}