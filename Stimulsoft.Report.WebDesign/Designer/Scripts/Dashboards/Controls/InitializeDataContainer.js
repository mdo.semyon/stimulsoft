﻿
StiMobileDesigner.prototype.DataContainer = function (width, height, showItemImage) {
    var container = document.createElement("div");
    container.jsObject = this;
    container.className = "stiDataContainer";
    if (width) container.style.width = width + "px";
    if (height) container.style.height = height + "px";
    container.selectedItem = null;

    container.updateHintText = function () {
        if (this.childNodes.length == 0 && !this.hintText) {
            this.hintText = document.createElement("div");
            this.hintText.className = "wizardFormHintText";
            this.hintText.innerHTML = this.jsObject.loc.Dashboard.DragDropDataFromDictionary;
            this.appendChild(this.hintText);
            this.hintText.style.width = "100%";
            this.hintText.style.textAlign = "center";
            this.hintText.style.top = "calc(50% - 6px)";
            this.style.borderStyle = "dashed";
        }
        else {            
            if (this.hintText && this.childNodes.length > 1) {
                this.removeChild(this.hintText);
                this.hintText = null;
            }
            this.style.borderStyle = "solid";
        }
    }

    container.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        this.selectedItem = null;
        this.style.paddingBottom = "0px";
        this.hintText = null;
        this.onAction();
        this.updateHintText();
    }

    container.addItem = function (caption, imageName, itemObject) {
        var item = this.jsObject.DataContainerItem(caption, showItemImage ? imageName : null, itemObject, this);
        this.appendChild(item);
        this.updateHintText();
        this.style.borderColor = "";

        return item;
    }

    container.getItemIndex = function (item) {
        for (var i = 0; i < this.childNodes.length; i++)
            if (this.childNodes[i] == item) return i;

        return null;
    }

    container.getItemByIndex = function (index) {
        if (!this.hintText && index < this.childNodes.length) {
            return this.childNodes[index];
        }

        return null;
    }

    container.getSelectedItemIndex = function () {
        return this.selectedItem ? this.getItemIndex(this.selectedItem) : null;
    }

    container.getOverItemIndex = function () {
        for (var i = 0; i < this.childNodes.length; i++)
            if (this.childNodes[i].isOver) return i;

        return null;
    }

    container.getCountItems = function () {
        return (!this.hintText ? this.childNodes.length : 0);
    }

    container.onmouseover = function () {
        if (this.jsObject.options.itemInDrag && this.jsObject.options.itemInDrag.originalItem.itemObject) {
            var typeItem = this.jsObject.options.itemInDrag.originalItem.itemObject.typeItem;
            if (typeItem == "Column" || typeItem == "DataSource" || typeItem == "BusinessObject" || typeItem == "Meter") {
                container.style.borderStyle = "dashed";
                container.style.borderColor = this.jsObject.options.themeColors[this.jsObject.GetThemeColor()];
            }
        }
    }

    container.onmouseout = function () {
        container.style.borderStyle = this.hintText ? "dashed" : "solid";
        container.style.borderColor = "";
    }

    container.onAction = function (item) { }
    container.onRemove = function () { }

    return container;
}

StiMobileDesigner.prototype.DataContainerItem = function (caption, imageName, itemObject, container) {
    var item = this.StandartSmallButton(null, null, caption, imageName);
    item.container = container;
    item.itemObject = itemObject;
    if (item.caption) item.caption.style.width = "100%";
    item.style.height = "30px";
    item.innerTable.style.width = "100%";
    
    var closeButton = this.StandartSmallButton(null, null, null, "CloseForm.png");
    closeButton.style.background = "white";
    closeButton.style.height = closeButton.style.width = "26px";
    closeButton.style.marginRight = "2px";
    closeButton.style.display = "none";
    closeButton.innerTable.style.width = "100%";
    item.innerTable.addCell(closeButton);

    closeButton.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = true;
        closeButton.style.background = "lightgray";
    }

    closeButton.onmouseleave = function () {
        this.isOver = false;
        if (!this.isEnabled) return;
        this.className = (this.isSelected ? this.styles["selected"] : this.styles["default"]) + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.style.background = "white";
    }

    closeButton.action = function () {
        item.closeButtonAction = true;
        item.remove();
    }

    item.onmousedown = function (event) {
        if (this.isTouchStartFlag) return;
        if (event) event.preventDefault();

        if (this.itemObject && event.button != 2 && !this.jsObject.options.controlsIsFocused) {
            var itemInDragObject = this.jsObject.TreeItemForDragDrop({ name: caption, typeIcon: imageName ? imageName.replace(".png", "") : "" }, null, !imageName);
            if (itemInDragObject.button.captionCell) itemInDragObject.button.captionCell.style.padding = "3px 15px 3px 5px";
            if (itemInDragObject.button.imageCell) itemInDragObject.button.imageCell.style.padding = "2px 5px 2px 5px";
            itemInDragObject.originalItem = item;
            itemInDragObject.beginingOffset = 0;
            this.jsObject.options.itemInDrag = itemInDragObject;
        }
    }

    item.onmouseup = function (event) {
        if (event.button == 2) {
            item.action();
        }
        if (this.jsObject.options.controlsIsFocused && this.jsObject.options.controlsIsFocused.action) {
            this.jsObject.options.controlsIsFocused.action();
        }
    }

    item.action = function () {
        if (!item.closeButtonAction) item.select();
        item.closeButtonAction = false;
    }

    item.remove = function () {
        if (container.selectedItem == this) {
            var prevItem = this.previousSibling;
            var nextItem = this.nextSibling;
            container.selectedItem = null;
            if (container.childNodes.length > 1) {
                if (nextItem) {
                    nextItem.setSelected(true);
                    container.selectedItem = nextItem;
                }
                else if (prevItem) {
                    prevItem.setSelected(true);
                    container.selectedItem = prevItem;
                }
            }
        }
        container.onRemove(container.getItemIndex(this));
        container.removeChild(this);        
        container.onAction();
        container.updateHintText();
    }

    item.select = function () {
        if (container.selectedItem) container.selectedItem.setSelected(false);
        this.setSelected(true);
        container.selectedItem = this;
        container.onAction();
    }

    item.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        this.isOver = true;
        closeButton.style.display = "";
    }

    item.onmouseleave = function () {
        this.isOver = false;
        if (!this.isEnabled) return;
        this.className = (this.isSelected ? this.styles["selected"] : this.styles["default"]) + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        closeButton.style.display = "none";
    }

    //item.setSelected = function (state) {
    //    this.isSelected = state;
    //    this.className = (state ? this.styles["selected"] : (this.isEnabled ? (this.isOver ? this.styles["over"] : this.styles["default"]) : this.styles["disabled"])) +
    //        (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");

    //    if (item.image) {
    //        if (state && this.jsObject.options.images[selImageName]) {
    //            item.image.src = this.jsObject.options.images[selImageName];
    //        }
    //        else if (this.jsObject.options.images[imageName]) {
    //            item.image.src = this.jsObject.options.images[imageName];
    //        }
    //    }
    //}

    return item;
}
