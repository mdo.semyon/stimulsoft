﻿
StiMobileDesigner.prototype.TextElementEditor = function (name, width, height, hintText) {

    var richTextControl = document.createElement("div");
    if (name != null) this.options.controls[name] = richTextControl;
    richTextControl.controls = {};
    richTextControl.jsObject = this;
    richTextControl.name = name != null ? name : this.generateKey();
    richTextControl.id = richTextControl.name;
    richTextControl.isEnabled = true;
    richTextControl.isSelected = false;
    richTextControl.isOver = false;
    richTextControl.isFocused = false;
    richTextControl.width = width;
    richTextControl.height = height;
    richTextControl.style.position = "relative";
    var defaultFontName = "Arial";
    var defaultFontSize = "8";

    /* Tool Bar 1 */
    var toolBar1 = this.CreateHTMLTable();
    toolBar1.style.borderSpacing = "3px";
    toolBar1.addRow();
    richTextControl.toolBar1 = toolBar1;
    richTextControl.appendChild(toolBar1);

    var fontList = this.FontList(name + "FontList", 180, null, true);
    richTextControl.controls.fontList = fontList;
    fontList.setKey(defaultFontName);
    toolBar1.addCell(fontList).style.paddingLeft = "4px";

    var sizes = [5, 6, 7, 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 76];
    //var sizes = ["8pt", "10pt", "12pt", "14pt", "18pt", "24pt", "36pt"];
    var items = [];
    for (var size in sizes) items.push(this.Item("sizeItem" + size, sizes[size], null, sizes[size]));
    //for (var i = 0; i < sizes.length; i++) items.push(this.Item("sizeItem" + i, sizes[i], null, i + 1));
    var sizeList = this.DropDownList(name + "FontSize", 55, null, items, true, false, null, true);
    richTextControl.controls.sizeList = sizeList;
    sizeList.setKey(defaultFontSize);
    toolBar1.addCell(sizeList);    

    var sep1 = this.HomePanelSeparator();
    sep1.style.height = "26px";
    toolBar1.addCell(sep1);

    var buttonStyleBold = this.StandartSmallButton(null, null, null, "Bold.png");
    richTextControl.controls.buttonStyleBold = buttonStyleBold;
    toolBar1.addCell(buttonStyleBold);
    buttonStyleBold.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("bold", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var buttonStyleItalic = this.StandartSmallButton(null, null, null, "Italic.png");
    richTextControl.controls.buttonStyleItalic = buttonStyleItalic;
    toolBar1.addCell(buttonStyleItalic);
    buttonStyleItalic.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("italic", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var buttonStyleUnderline = this.StandartSmallButton(null, null, null, "Underline.png");
    richTextControl.controls.buttonStyleUnderline = buttonStyleUnderline;
    toolBar1.addCell(buttonStyleUnderline);
    buttonStyleUnderline.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("underline", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var sep2 = this.HomePanelSeparator();
    sep2.style.height = "26px";
    toolBar1.addCell(sep2);

    var colorControl = this.ColorControlWithImage(null, "TextColor.png");
    richTextControl.controls.colorControl = colorControl;
    colorControl.setKey("255,0,0");
    toolBar1.addCell(colorControl);
    colorControl.action = function () {
        var colors = this.key.split(",");
        var hexColor = this.jsObject.rgb(Number(colors[0]), Number(colors[1]), Number(colors[2]));
        richText.restoreSelection();
        richText.win.document.execCommand("foreColor", null, hexColor);
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var sep3 = this.HomePanelSeparator();
    sep3.style.height = "26px";
    toolBar1.addCell(sep3);
    
    var buttonAlignLeft = this.StandartSmallButton(null, null, null, "AlignLeft.png");
    richTextControl.controls.buttonAlignLeft = buttonAlignLeft;
    toolBar1.addCell(buttonAlignLeft).style.paddingLeft = "4px";    

    var buttonAlignCenter = this.StandartSmallButton(null, null, null, "AlignCenter.png");
    richTextControl.controls.buttonAlignCenter = buttonAlignCenter;
    toolBar1.addCell(buttonAlignCenter);

    var buttonAlignRight = this.StandartSmallButton(null, null, null, "AlignRight.png");
    richTextControl.controls.buttonAlignRight = buttonAlignRight;
    toolBar1.addCell(buttonAlignRight);

    var buttonAlignWidth = this.StandartSmallButton(null, null, null, "AlignWidth.png");
    richTextControl.controls.buttonAlignWidth = buttonAlignWidth;
    toolBar1.addCell(buttonAlignWidth);
        
    var separator = document.createElement("div");
    separator.className = "stiDesignerFormSeparator";
    separator.style.margin = "0 0 4px 0";
    richTextControl.toolBarsSeparator = separator;
    richTextControl.appendChild(separator);

    /* Rich Text */
    var richText = document.createElement("iframe");
    richText.setAttribute("frameborder", "no");
    richText.setAttribute("src", "about:blank");
    richText.style.width = width + "px";
    richText.style.height = height + "px";
    richText.style.minWidth = width + "px";
    richText.style.minHeight = height + "px";
    richText.style.padding = "0";
    richText.style.margin = "2px 0px 0px 6px";    
    richText.className = this.options.isTouchDevice ? "stiDesignerTextArea_Touch" : "stiDesignerTextArea_Mouse";
    if (hintText) richText.setAttribute(this.GetNavigatorName() == "MSIE" ? "title" : "placeholder", hintText);
    richTextControl.richText = richText;
    richTextControl.appendChild(richText);

    richText.onload = function () {
        this.win = this.contentWindow || this.window;
        this.doc = this.contentDocument || this.document;
        this.doc.designMode = "on";
        this.ranges = [];

        var body = this.doc.getElementsByTagName("body")[0];
        if (body) {
            body.style.margin = "0px";

            if (richTextControl.isEnabled) {
                body.click();
                body.focus();

                body.onfocus = function () {
                    richTextControl.isFocused = true;
                    richTextControl.setSelected(true);
                }

                body.onblur = function () {
                    richTextControl.isFocused = false;
                    richTextControl.setSelected(false);
                }
            }
            else {
                fontList.textBox.focus();
            }
        }

        this.doc.onclick = function () {
            richTextControl.updateState();
        }
                
        this.doc.onmousedown = function () {
            richText.ranges = [];
            richTextControl.jsObject.options.mobileDesigner.pressedDown();
        }

        this.doc.onselect = function () {
            richTextControl.updateState();

            if (richText.win.getSelection) {
                richText.ranges = [];
                var sel = richText.win.getSelection();
                if (sel.rangeCount) {
                    for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                        richText.ranges.push(sel.getRangeAt(i));
                    }
                }
            }
        }

        //Events
        if (this.win) {
            var keyTimer;

            this.win.onkeyup = function (event) {
                clearTimeout(keyTimer);
                keyTimer = setTimeout(function () {
                    richTextControl.onchangetext();
                }, 1000);
            };
        }
    }

    richText.restoreSelection = function () {
        this.win.focus();
        if (this.win.getSelection && this.ranges.length > 0) {
            var sel = this.win.getSelection();
            sel.removeAllRanges();
            for (var i = 0, len = this.ranges.length; i < len; ++i) {
                sel.addRange(this.ranges[i]);
            }
        }
    }

    richText.selectAllText = function () {
        var body = this.doc.getElementsByTagName("body")[0];

        if (body.createTextRange) {
            var range = body.createTextRange();
            range.moveToElementText(body);
            range.select();
        }
        else if (window.getSelection) {
            var selection = richText.win.getSelection();
            var range = this.doc.createRange();
            range.selectNodeContents(body);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    }

    richText.removeAllSelection = function () {
        this.win.focus();
        if (this.win.getSelection) {
            var sel = this.win.getSelection();
            sel.removeAllRanges();
        }
    }

    richTextControl.resize = function (newWidth, newHeight) {
        richTextControl.width = newWidth;
        richTextControl.height = newHeight;
        richText.style.width = newWidth + "px";
        richText.style.height = newHeight + "px";
        richText.style.minWidth = newWidth + "px";
        richText.style.minHeight = newHeight + "px";
    }

    richTextControl.updateState = function () {
        if (!this.isEnabled) return;
        buttonStyleBold.setSelected(richText.win.document.queryCommandState("bold"));
        buttonStyleItalic.setSelected(richText.win.document.queryCommandState("italic"));
        buttonStyleUnderline.setSelected(richText.win.document.queryCommandState("underline"));

        var rgbColor = "0,0,0";
        var fontColor = richText.win.document.queryCommandValue("foreColor");
        if (fontColor) {
            if (fontColor.toString().toLowerCase().indexOf("rgb") == 0) {
                rgbColor = fontColor.substring(4, fontColor.indexOf(")"));
            }
            else {
                var b = (fontColor >> 16) & 255;
                var g = (fontColor >> 8) & 255;
                var r = fontColor & 255;
                rgbColor = r + "," + g + "," + b;
            }
        }
        colorControl.setKey(rgbColor);
    }

    richTextControl.setText = function (text, currentFont) {
        var win = this.richText.contentWindow || this.richText.window;
        var doc = win.document || win.contentDocument;

        if (doc) {
            doc.open();
            doc.write(text);
            doc.close();

            if (currentFont) {
                var body = doc.getElementsByTagName("body")[0];
                body.style.fontFamily = currentFont.name;
                defaultFontName = currentFont.name;
                body.style.fontSize = currentFont.size;
                defaultFontSize = currentFont.size;
                buttonStyleBold.setSelected(currentFont.bold == "1");
                buttonStyleItalic.setSelected(currentFont.italic == "1");
                buttonStyleUnderline.setSelected(currentFont.underline == "1");
            }
        }
        this.updateState();
    }

    richTextControl.getText = function (text) {
        var win = this.richText.contentWindow || this.richText.window;
        var doc = win.document || win.contentDocument;

        var htmlText = doc ? doc.body.innerHTML : "";

        startIndex = 0;
        while (startIndex >= 0) {
            var startIndex = htmlText.indexOf("rgb(");
            if (startIndex == -1) startIndex = htmlText.indexOf("RGB(");
            if (startIndex >= 0) {
                var endIndex = htmlText.indexOf(")", startIndex);
                var hexColor = richTextControl.jsObject.RgbColorToHexColor(htmlText.substr(startIndex, endIndex - startIndex + 1));
                htmlText = htmlText.substring(0, startIndex) + hexColor + htmlText.substring(endIndex + 1);
            }
        }

        return htmlText;
    }

    richTextControl.setRichTextContent = function (itemObject) {
        if (!itemObject) {
            richTextControl.clearResourceContent();
        }
        else if (itemObject.content) {
            richTextControl.loadResourceContent(itemObject.content);
        }
        else {
            this.jsObject.SendCommandToDesignerServer("GetRichTextContent", { itemObject: itemObject }, function (answer) {
                itemObject.content = Base64.decode(answer.content);
                richTextControl.loadResourceContent(itemObject.content);
            });
        }
    }

    richTextControl.loadResourceContent = function (content) {
        this.setText(content || "");
        richTextControl.setEnabled(false);
        removeButton.style.display = "";
        richTextControl.onLoadResourceContent(content);
    }

    richTextControl.clearResourceContent = function () {
        richTextControl.setEnabled(true);
        removeButton.style.display = "none";
        this.setText("");
        richTextControl.onchange();
    }

    richTextControl.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        if (!this.isSelected && !this.isFocused)
            this.richText.className = "stiDesignerTextAreaOver" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    richTextControl.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.isOver = false;
        if (!this.isSelected && !this.isFocused)
            this.richText.className = "stiDesignerTextArea" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    richTextControl.setEnabled = function (state) {
        sizeList.setEnabled(state);
        fontList.setEnabled(state);
        colorControl.setEnabled(state);
        buttonStyleBold.setEnabled(state);
        buttonStyleItalic.setEnabled(state);
        buttonStyleUnderline.setEnabled(state);
        buttonAlignLeft.setEnabled(state);
        buttonAlignCenter.setEnabled(state);
        buttonAlignRight.setEnabled(state);
        buttonAlignWidth.setEnabled(state);
        if (richText.doc && !state) {
            richText.doc.designMode = "off";
            var body = richText.doc.getElementsByTagName("body")[0];
            if (body) body.onfocus = null;
        }

        this.isEnabled = state;
        this.disabled = !state;
        this.richText.className =
            (state ? "stiDesignerTextArea" : "stiDesignerTextAreaDisabled") +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    richTextControl.setSelected = function (state) {
        this.isSelected = state;
        this.richText.className = (state
            ? "stiDesignerTextAreaOver"
            : (this.isEnabled ? (this.isOver ? "stiDesignerTextAreaOver" : "stiDesignerTextArea") : "stiDesignerTextAreaDisabled")) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    richTextControl.action = function () { };
    richTextControl.onchange = function () { };
    richTextControl.onchangetext = function () { };
    richTextControl.onLoadResourceContent = function (content) { };

    return richTextControl;
}