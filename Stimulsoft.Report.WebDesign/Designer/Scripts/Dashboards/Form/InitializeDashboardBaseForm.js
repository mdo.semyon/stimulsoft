﻿
StiMobileDesigner.prototype.DashboardBaseForm = function (name, caption, level, helpUrl) {
    var form = this.BaseForm(name, caption, level, helpUrl);
    form.isNotModal = true;

    form.changeVisibleState = function (state) {
        if (state) {
            this.style.display = "";
            
            if (this.jsObject.options.currentForm && this.parentElement == this.jsObject.options.mainPanel) {
                this.jsObject.options.mainPanel.removeChild(this);
                this.jsObject.options.mainPanel.appendChild(this);
            }
            
            this.onshow();

            if (this.isDockableToComponent)
                this.dockToComponent();
            else
                this.jsObject.SetObjectToCenter(this);

            this.visible = true;            
            this.currentFormDownLevel = this.jsObject.options.currentForm && this.jsObject.options.currentForm.visible ? this.jsObject.options.currentForm : null;
            this.jsObject.options.currentForm = this;

            d = new Date();
            var endTime = d.getTime() + this.jsObject.options.formAnimDuration;
            this.flag = false;
            this.jsObject.ShowAnimationForm(this, endTime);
            this.movedByUser = false;
        }
        else {
            clearTimeout(this.animationTimer);
            this.visible = false;

            var selectedObject = this.jsObject.options.selectedObject;
            if (selectedObject && selectedObject.controls.editDbsIcon && this.dockingComponent == selectedObject) {
                selectedObject.controls.editDbsIcon.style.visibility = "visible";
            }

            this.dockingComponent = null;
            this.jsObject.options.currentForm = this.currentFormDownLevel || null;
            this.style.display = "none";
            if (!this.jsObject.options.forms[this.name]) {
                this.jsObject.options.mainPanel.removeChild(this);
            }
            this.onhide();
        }
    }

    form.dockToComponent = function () {
        var component = this.jsObject.options.selectedObject;
        if (component) {
            var top = parseInt(component.getAttribute("top"));
            var left = parseInt(component.getAttribute("left"));
            var right = left + parseInt(component.getAttribute("width"));
            var pagePos = this.jsObject.FindPagePositions();
            var compAbsPosLeft = pagePos.posX + left;
            var compAbsPosRight = pagePos.posX + right;
            var compAbsPosTop = pagePos.posY + top;
                        
            this.style.left = (compAbsPosRight + 10) + "px";
            this.style.top = compAbsPosTop + "px";

            if (compAbsPosRight + this.offsetWidth > this.jsObject.options.mainPanel.offsetWidth && compAbsPosLeft - this.offsetWidth > 0) {
                this.style.left = (compAbsPosLeft - this.offsetWidth - 10) + "px";
            }

            if (compAbsPosTop + this.offsetHeight > this.jsObject.options.mainPanel.offsetHeight) {
                this.style.top = (this.jsObject.options.mainPanel.offsetHeight - this.offsetHeight - 5) + "px";
            }

            this.dockingComponent = component;

            if (component.controls.editDbsIcon) {
                component.controls.editDbsIcon.style.visibility = "hidden";
            }
        }
    }

    form.addControlRow = function (table, textControl, controlName, control, margin) {
        if (!this.controls) this.controls = {};
        this.controls[controlName] = control;
        this.controls[controlName + "Row"] = table.addRow();

        if (textControl != null) {
            var text = table.addCellInLastRow();
            this.controls[controlName + "Text"] = text;
            text.innerHTML = textControl;
            text.className = "stiDesignerCaptionControls";
            text.style.paddingLeft = "12px";
            text.style.minWidth = "70px";
        }

        if (control) {
            control.form = this;
            control.style.margin = margin;
            var controlCell = table.addCellInLastRow(control);
            if (textControl == null) controlCell.setAttribute("colspan", 2);
        }

        return controlCell;
    }

    return form;
}
