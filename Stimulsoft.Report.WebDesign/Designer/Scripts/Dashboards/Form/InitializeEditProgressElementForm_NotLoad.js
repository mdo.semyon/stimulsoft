﻿
StiMobileDesigner.prototype.InitializeEditProgressElementForm_ = function () {
    var form = this.DashboardBaseForm("editProgressElementForm", this.loc.Components.StiProgress, 1);
    form.isDockableToComponent = true;
    form.buttonsSeparator.style.display = "none";
    form.buttonsPanel.style.display = "none";
    form.container.style.borderTop = "0px";
    form.caption.style.padding = "0px 10px 0 12px";
    var jsObject = this;

    var controlsTable = this.CreateHTMLTable();
    form.container.appendChild(controlsTable);
    form.container.style.padding = "0 0 6px 0";

    //DataColumns
    var dataColumns = [
        ["value", this.loc.PropertyMain.Value],
        ["target", this.loc.PropertyMain.Target],
        ["series", this.loc.PropertyMain.Series]
    ];

    for (var i = 0; i < dataColumns.length; i++) {
        var container = this.ProgressDataColumnContainer(form, dataColumns[i][0], dataColumns[i][1]);
        container.allowSelected = true;
        form.addControlRow(controlsTable, null, dataColumns[i][0] + "DataColumn", container, "0px 12px 0px 12px");

        container.action = function () {
            if (!this.dataColumnObject) {
                form.controls.expression.currentContainer = null;
                form.controls.expression.setEnabled(false);
            }
            form.applyDataColumnPropertyToProgressElement(this);
        }

        container.onSelected = function () {
            for (var i = 0; i < dataColumns.length; i++) {
                if (form.controls[dataColumns[i][0] + "DataColumn"] != this)
                    form.controls[dataColumns[i][0] + "DataColumn"].setSelected(false);
            }

            if (this.dataColumnObject && this.dataColumnObject.expression != null) {
                form.controls.expression.currentContainer = this;
                form.controls.expression.setEnabled(true);
                form.controls.expression.textBox.value = Base64.decode(this.dataColumnObject.expression);
            }
        }
    }

    var parentValueContainer = form.controls.valueDataColumn.innerContainer.parentElement;

    var contextMenu = this.options.menus.progressElementFormContextMenu || this.BaseContextMenu("progressElementFormContextMenu", "Down", null, this.GetStyles("MenuStandartItem"));
        
    var expressionControl = this.TableElementExpression(null, 160, null, null);
    form.addControlRow(controlsTable, this.loc.PropertyMain.Expression, "expression", expressionControl, "12px 12px 6px 12px");
    form.controls.expressionText.style.paddingTop = "6px";
    expressionControl.menu = contextMenu;
    expressionControl.menu.parentButton = expressionControl.button;

    expressionControl.action = function () {
        if (this.currentContainer) {
            form.applyExpressionPropertyToProgressElement(this.currentContainer, this.textBox.value);
        }
    }

    contextMenu.action = function (menuItem) {
        this.changeVisibleState(false);
        var currentContainer = expressionControl.currentContainer;

        if (menuItem.key.indexOf("Function_") == 0) {
            form.sendCommandToProgressElement(
                {
                    command: "SetFunction",
                    containerName: jsObject.UpperFirstChar(currentContainer.name),
                    function: menuItem.key.replace("Function_", "")
                },
                function (answer) {
                    if (answer.progressElement) {
                        form.updateControls(answer.progressElement);
                        form.updateProgressSvgContent(answer.progressElement.svgContent);
                    }
                }
            );
            return;
        }

        switch (menuItem.key) {
            case "removeField": {
                currentContainer.clear();
                currentContainer.action();
                break;
            }
            case "editExpression": {
                contextMenu.jsObject.InitializeExpressionEditorForm(function (expressionEditorForm) {
                    var propertiesPanel = expressionEditorForm.jsObject.options.propertiesPanel;
                    expressionEditorForm.propertiesPanelZIndex = propertiesPanel.style.zIndex;
                    expressionEditorForm.propertiesPanelIsEnabled = propertiesPanel.isEnabled;
                    expressionEditorForm.resultControl = expressionControl;
                    expressionEditorForm.changeVisibleState(true);
                });
                break;
            }
            case "newItem": {
                form.sendCommandToProgressElement(
                    {
                        command: "NewItem",
                        containerName: jsObject.UpperFirstChar(currentContainer.name)
                    },
                    function (answer) {
                        if (answer.progressElement) {
                            form.updateControls(answer.progressElement);
                            form.updateProgressSvgContent(answer.progressElement.svgContent);
                            form.checkStartMode();
                        }
                    }
                );
                break;
            }
        }
    }

    contextMenu.onshow = function () {
        var currentContainer = expressionControl.currentContainer;
        var items = [];
        items.push(this.jsObject.Item("newItem", this.jsObject.loc.FormDictionaryDesigner.NewItem, "Empty16.png", "newItem"));

        if (expressionControl.isEnabled && currentContainer && currentContainer.isSelected) {
            items.push("separator1");
            items.push(this.jsObject.Item("editExpression", this.jsObject.loc.Dashboard.EditExpression, "EditButton.png", "editExpression"));
            items.push(this.jsObject.Item("renameField", this.jsObject.loc.Buttons.Rename, "DataColumn.png", "renameField"));
            items.push(this.jsObject.Item("removeField", this.jsObject.loc.Dashboard.RemoveField, "Remove.png", "removeField"));

            if (currentContainer.dataColumnObject) {
                var functions = currentContainer.dataColumnObject.functions;
                if (functions && functions.length > 0) {
                    items.push("separator2");
                    for (var i = 0; i < functions.length; i++) {
                        items.push(this.jsObject.Item("Function_" + functions[i], functions[i], "CheckBox.png", "Function_" + functions[i]));
                    }
                }
            }
        }

        this.addItems(items);

        for (var itemName in this.items) {
            if (itemName.indexOf("Function_") == 0) {
                var funcItem = this.items[itemName];
                var isSelected = currentContainer.dataColumnObject && currentContainer.dataColumnObject.currentFunction &&
                    itemName.toLowerCase() == ("Function_" + currentContainer.dataColumnObject.currentFunction).toLowerCase();

                funcItem.caption.style.fontWeight = isSelected ? "bold" : "normal";
                funcItem.image.style.visibility = isSelected ? "visible" : "hidden";
            }
        }
    }

    //Mode Types
    var modeTypes = ["Circle", "Pie", "DataBars"];
    var modeTypesTable = this.CreateHTMLTable();
    modeTypesTable.buttons = {};
    for (var i = 0; i < modeTypes.length; i++) {
        var button = this.SmallImageButtonWithBorder(null, null, "Dashboards.Progress." + modeTypes[i] + ".png", this.loc.PropertyEnum["StiProgressElementMode" + modeTypes[i]]);
        button.modeType = modeTypes[i];
        button.style.marginRight = "5px";
        modeTypesTable.addCell(button);
        modeTypesTable.buttons[modeTypes[i]] = button;

        button.action = function () {
            this.select();
            form.sendCommandToProgressElement({ command: "SetPropertyValue", propertyName: "Mode", propertyValue: this.modeType },
                function (answer) {
                    if (answer.progressElement) {
                        form.updateControls(answer.progressElement);
                        form.updateProgressSvgContent(answer.progressElement.svgContent);
                    }
                }
            );
        }

        button.select = function () {
            for (var name in modeTypesTable.buttons) {
                modeTypesTable.buttons[name].setSelected(false);
            }
            this.setSelected(true);
        }
    }
    form.addControlRow(controlsTable, this.loc.PropertyMain.Mode, "modeTypesTable", modeTypesTable, "6px 12px 6px 12px");

    form.setValues = function () {
        expressionControl.textBox.value = "";
        expressionControl.setEnabled(false);
        if (modeTypesTable.buttons[this.progressProperties.mode]) {
            modeTypesTable.buttons[this.progressProperties.mode].select();
        }

        var meters = this.progressProperties.meters;
        
        var meterTypes = ["Value", "Target", "Series"];
        for (var i = 0; i < meterTypes.length; i++) {
            var meter = meters["Sti" + meterTypes[i] + "ProgressMeter"];
            var container = this.controls[jsObject.LowerFirstChar(meterTypes[i]) + "DataColumn"];

            if (meter) {
                container.addColumn(meter.label, meter);
                if (container.isSelected && container.item) container.item.action();
            }
            else
                container.clear();
        }
    }

    form.checkStartMode = function () {
        var itemsCount = 0;
        for (var i = 0; i < dataColumns.length; i++) {
            var container = form.controls[dataColumns[i][0] + "DataColumn"];
            if (container.dataColumnObject) itemsCount++;
        }

        var valueContainer = form.controls.valueDataColumn.innerContainer;

        if (itemsCount == 0) {
            form.container.appendChild(valueContainer);
            controlsTable.style.display = "none";
            valueContainer.style.height = valueContainer.style.maxHeight = "260px";
            valueContainer.style.width = "267px";
            valueContainer.style.margin = "6px 12px 6px 12px";
        }
        else {
            parentValueContainer.appendChild(valueContainer);
            controlsTable.style.display = "";
            valueContainer.style.height = "30px";
            valueContainer.style.width = "auto";
            valueContainer.style.margin = "0";
        }
    }

    form.onshow = function () {
        form.currentPanelName = form.jsObject.options.propertiesPanel.getCurrentPanelName();
        form.jsObject.options.propertiesPanel.showContainer("Dictionary");
        expressionControl.textBox.value = "";
        expressionControl.setEnabled(false);
        var meterTypes = ["Value", "Target", "Series"];
        for (var i = 0; i < meterTypes.length; i++) {
            var container = this.controls[jsObject.LowerFirstChar(meterTypes[i]) + "DataColumn"];
            container.clear();
        }
        form.checkStartMode();

        form.sendCommandToProgressElement({ command: "GetProgressElementProperties" },
            function (answer) {
                if (answer.progressElement) {
                    form.updateControls(answer.progressElement);
                    form.checkStartMode();
                }
            }
        );
    }

    form.onhide = function () {
        form.jsObject.options.propertiesPanel.showContainer(form.currentPanelName);
    }

    form.updateControls = function (progressElement) {
        form.progressProperties = progressElement;
        form.setValues();
    }

    form.applyExpressionPropertyToProgressElement = function (container, expressionValue) {
        if (container) {
            form.sendCommandToProgressElement(
                {
                    command: "SetExpression",
                    containerName: form.jsObject.UpperFirstChar(container.name),
                    expressionValue: Base64.encode(expressionValue)
                },
                function (answer) {
                    if (answer.progressElement) {
                        form.updateControls(answer.progressElement);
                        form.updateProgressSvgContent(answer.progressElement.svgContent);
                    }
                }
            );
        }
    }

    form.applyDataColumnPropertyToProgressElement = function (container) {
        form.sendCommandToProgressElement(
            {
                command: "SetDataColumn",
                containerName: form.jsObject.UpperFirstChar(container.name),
                dataColumnObject: container.dataColumnObject
            },
            function (answer) {
                if (answer.progressElement) {
                    form.updateControls(answer.progressElement);
                    form.updateProgressSvgContent(answer.progressElement.svgContent);
                    if (container.item) container.item.action();
                    form.checkStartMode();
                }
            }
        );
    }

    form.sendCommandToProgressElement = function (updateParameters, callbackFunction) {
        updateParameters.zoom = form.jsObject.options.report.zoom.toString();

        form.jsObject.SendCommandToDesignerServer("UpdateProgressElement",
            {
                componentName: form.currentProgressElement.properties.name,
                updateParameters: updateParameters
            },
            function (answer) {
                callbackFunction(answer);
            });
    }

    form.updateProgressSvgContent = function (svgContent) {
        this.currentProgressElement.properties.svgContent = svgContent;
        this.currentProgressElement.repaint();
    }

    return form;
}

StiMobileDesigner.prototype.ProgressDataColumnContainer = function (form, name, headerText, width, showItemImage) {
    var container = this.DataColumnContainer(name, headerText, width, showItemImage);
    var innerContainer = container.innerContainer;
    var jsObject = this;

    innerContainer.oldonmouseup = innerContainer.onmouseup;

    innerContainer.onmouseup = function (event) {
        if (innerContainer.canInsert()) {
            var originalItem = jsObject.options.itemInDrag.originalItem;
            var itemObject = jsObject.CopyObject(originalItem.itemObject);
            if (originalItem.closeButton && originalItem.closeButton.clicked) return;

            if (itemObject.typeItem == "Meter") {
                var fromContainerName = jsObject.UpperFirstChar(originalItem.container.name);
                var toContainerName = jsObject.UpperFirstChar(container.name);

                if (toContainerName != fromContainerName) {
                    form.sendCommandToProgressElement(
                        {
                            command: "MoveMeter",
                            toContainerName: toContainerName,
                            fromContainerName: fromContainerName
                        },
                        function (answer) {
                            form.updateControls(answer.progressElement);
                            form.updateProgressSvgContent(answer.progressElement.svgContent);
                            if (container.item) container.item.action();
                        });
                }
            }
            else {
                innerContainer.oldonmouseup(event);
            }
        }
    }

    return container;
}
