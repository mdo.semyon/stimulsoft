﻿
StiMobileDesigner.prototype.InitializeEditMapElementForm_ = function () {
    var form = this.DashboardBaseForm("editMapElementForm", this.loc.Components.StiMap, 1);
    form.isDockableToComponent = true;
    form.buttonsSeparator.style.display = "none";
    form.buttonsPanel.style.display = "none";
    form.container.style.borderTop = "0px";
    form.caption.style.padding = "0px 10px 0 12px";
    var jsObject = this;

    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerImageFormMainPanel";
    form.container.appendChild(mainTable);
    form.container.style.padding = "0px";

    //Buttons
    var buttonProps = [
        ["Choropleth", "Maps.MapChoropleth.png", this.loc.PropertyEnum.StiMapModeChoropleth],
        ["Online", "Maps.MapOnline.png", this.loc.PropertyEnum.StiMapModeOnline]
    ];

    //Add Panels && Buttons
    var panelsContainer = mainTable.addCell();
    var buttonsPanel = mainTable.addCell();
    buttonsPanel.style.verticalAlign = "top";
    form.mainButtons = {};
    form.panels = {};

    for (var i = 0; i < buttonProps.length; i++) {
        var panel = document.createElement("Div");
        panel.className = "stiDesignerMapsEditorFormPanel";
        if (i != 0) panel.style.display = "none";
        panelsContainer.appendChild(panel);
        form.panels[buttonProps[i][0]] = panel;
        var button = this.StandartFormBigButton("editMapForm" + buttonProps[i][0] + "Button", null, buttonProps[i][2], buttonProps[i][1], buttonProps[i][2], 80);
        button.style.margin = "2px";
        form.mainButtons[buttonProps[i][0]] = button;
        buttonsPanel.appendChild(button);
        button.panelName = buttonProps[i][0];
        button.action = function () {
            form.setMode(this.panelName);
            form.applyPropertiesToMapElement();
        }
    }

    var choroplethTable = this.CreateHTMLTable();
    choroplethTable.style.width = "100%";
    form.panels.Choropleth.appendChild(choroplethTable);

    //Data From
    var dataFromControl = this.DropDownList(null, 200, null, this.GetChoroplethDataTypesItems(), true);
    form.addControlRow(choroplethTable, this.loc.Adapters.AdapterConnection.replace("{0}", ""), "dataFrom", dataFromControl, "12px");

    dataFromControl.action = function () {
        form.updateControlsVisibleStates();
        form.applyPropertiesToMapElement();
    }

    //DataColumns
    var dataColumns = [
        ["key", this.loc.PropertyMain.Key, 100],
        ["name", this.loc.PropertyMain.Name, 140],
        ["value", this.loc.PropertyMain.Value, 100],
        ["group", this.loc.PropertyEnum.StiMapTypeGroup, 90],
        ["color", this.loc.PropertyMain.Color, 90]
    ];

    for (var i = 0; i < dataColumns.length; i++) {
        var container = this.MapDataColumnContainer(form, dataColumns[i][0], dataColumns[i][1]);
        container.allowSelected = true;
        form.addControlRow(choroplethTable, null, dataColumns[i][0] + "DataColumn", container, "0px 12px " + (i == dataColumns.length - 1 ? "12px" : "0px") + " 12px");

        container.action = function () {
            if (!this.dataColumnObject) {
                form.controls.expressionChoropleth.currentContainer = null;
                form.controls.expressionChoropleth.setEnabled(false);
            }
            form.applyDataColumnPropertyToMapElement(this);
        }

        container.onSelected = function () {
            for (var i = 0; i < dataColumns.length; i++) {
                if (form.controls[dataColumns[i][0] + "DataColumn"] != this)
                    form.controls[dataColumns[i][0] + "DataColumn"].setSelected(false);
            }

            if (this.dataColumnObject && this.dataColumnObject.expression != null) {
                form.controls.expressionChoropleth.currentContainer = this;
                form.controls.expressionChoropleth.setEnabled(true);
                form.controls.expressionChoropleth.textBox.value = Base64.decode(this.dataColumnObject.expression);
            }
        }
    }

    //DataGrid
    var dataGridView = document.createElement("div");
    dataGridView.headers = {};
    dataGridView.columns = {};
    dataGridView.className = "stiMapManualContainer";
    dataGridView.style.height = "254px";
    form.addControlRow(choroplethTable, null, "dataGridView", dataGridView, "0 12px 12px 12px");

    var dataGridHeader = this.CreateHTMLTable();
    dataGridHeader.className = "stiMapDataGridHeader";
    var dataGridTable = this.CreateHTMLTable();
    dataGridTable.className = "stiMapDataGrid";
    dataGridView.appendChild(dataGridHeader);
    var scrollContainer = document.createElement("div");
    scrollContainer.style.height = "232px";
    scrollContainer.className = "stiMapScrollContainer";
    dataGridView.appendChild(scrollContainer);
    scrollContainer.appendChild(dataGridTable);

    for (var i = 0; i < dataColumns.length; i++) {
        var headerCell = dataGridHeader.addTextCell(dataColumns[i][1]);
        headerCell.style.width = (dataColumns[i][2] + 4) + "px";
        dataGridView.headers[dataColumns[i][0]] = headerCell;

        var columnCell = dataGridTable.addCell();
        columnCell.style.width = "1px";
        dataGridView.columns[dataColumns[i][0]] = columnCell;
    }

    dataGridView.fillData = function (data) {
        //clear columns
        for (var i = 0; i < dataColumns.length; i++) {
            while (dataGridView.columns[dataColumns[i][0]].childNodes[0])
                dataGridView.columns[dataColumns[i][0]].removeChild(dataGridView.columns[dataColumns[i][0]].childNodes[0]);
        }

        //add new data
        if (data) {
            for (var i = 0; i < data.length; i++) {
                for (var k = 0; k < dataColumns.length; k++) {
                    var textBox = jsObject.TextBox(null, dataColumns[k][2]);
                    if (dataColumns[k][0] == "key") textBox.readOnly = true;
                    dataGridView.columns[dataColumns[k][0]].appendChild(textBox);
                    textBox.style.borderLeft = "0";
                    textBox.style.borderTop = "0";
                    if (data[i][dataColumns[k][0]]) textBox.value = data[i][dataColumns[k][0]];
                    textBox.rowIndex = i;
                    textBox.columnName = dataColumns[k][0];

                    textBox.action = function () {
                        form.sendCommandToMapElement(
                            {
                                command: "UpdateMapData",
                                rowIndex: this.rowIndex,
                                columnName: this.columnName,
                                textValue: this.value
                            },
                            function (answer) {
                                if (answer.mapElement) {
                                    form.updateControls(answer.mapElement);
                                }
                            }
                        );
                    }
                }
            }
        }
    }

    //Separator
    form.addControlRow(choroplethTable, null, "separator1", this.FormSeparator(), "0 0 12px 0");

    //Map ID
    var mapIDControl = this.MapIDControl("editMapFormMapID", 574);
    form.addControlRow(choroplethTable, null, "mapID", mapIDControl, "0 0 6px 12px");

    mapIDControl.action = function () {
        form.applyPropertiesToMapElement(true);
    }
    
    var expressionChoropleth = this.MapElementExpression(null, 200, null, null);
    var choroplethExpressionMenu = this.options.menus.choroplethExpressionMenu || this.InitializeMapExpressionMenu("choroplethExpressionMenu", expressionChoropleth, form);
    form.addControlRow(choroplethTable, this.loc.PropertyMain.Expression, "expressionChoropleth", expressionChoropleth, "6px 12px 6px 12px");
    expressionChoropleth.menu = choroplethExpressionMenu;
    expressionChoropleth.menu.parentButton = expressionChoropleth.button;
    
    expressionChoropleth.action = function () {
        if (this.currentContainer) {
            form.applyExpressionPropertyToMapElement(this.currentContainer, this.textBox.value);
        }
    }

    //Map Type
    var mapTypeControl = this.DropDownList(null, 200, null, this.GetChoroplethMapTypesItems(), true);
    form.addControlRow(choroplethTable, this.loc.PropertyMain.MapType, "mapType", mapTypeControl, "6px 12px 6px 12px");

    mapTypeControl.action = function () {
        form.updateControlsVisibleStates();
        form.applyPropertiesToMapElement();
    }

    dataGridView.updateColumnsState = function () {
        dataGridView.headers.color.style.display = dataGridView.columns.color.style.display = mapTypeControl.key == "Individual" ? "" : "none";
        dataGridView.headers.group.style.display = dataGridView.columns.group.style.display = mapTypeControl.key == "Group" || mapTypeControl.key == "HeatmapWithGroup" ? "" : "none";
    }

    //Display Name Type
    var displayNameTypeControl = this.DropDownList(null, 200, null, this.GetMapDisplayNameTypeItems(), true);
    form.addControlRow(choroplethTable, this.loc.PropertyMain.DisplayNameType, "displayNameType", displayNameTypeControl, "6px 12px 6px 12px");

    displayNameTypeControl.action = function () {
        form.applyPropertiesToMapElement();
    }

    //Show Value
    var showValueCheckBox = this.CheckBox(null, this.loc.PropertyMain.ShowValue);
    form.addControlRow(choroplethTable, " ", "showValue", showValueCheckBox, "8px 12px 8px 12px");

    showValueCheckBox.action = function () {
        form.applyPropertiesToMapElement();
    }

    //Color Each
    var colorEachCheckBox = this.CheckBox(null, this.loc.PropertyMain.ColorEach);
    form.addControlRow(choroplethTable, " ", "colorEach", colorEachCheckBox, "8px 12px 0px 12px");

    colorEachCheckBox.action = function () {
        form.applyPropertiesToMapElement();
    }

    //OnlineDataColumns
    var onlineDataColumns = [
        ["latitude", this.loc.PropertyMain.Latitude],
        ["longitude", this.loc.PropertyMain.Longitude]
    ];

    for (var i = 0; i < onlineDataColumns.length; i++) {
        var container = this.MapDataColumnContainer(form, onlineDataColumns[i][0], onlineDataColumns[i][1]);
        container.allowSelected = true;
        container.style.margin = "0px 12px " + (i == onlineDataColumns.length - 1 ? "12px" : "0px") + " 12px";
        form.panels.Online.appendChild(container);
        form.controls[onlineDataColumns[i][0] + "DataColumn"] = container;

        container.action = function () {
            if (!this.dataColumnObject) {
                form.controls.expressionOnline.currentContainer = null;
                form.controls.expressionOnline.setEnabled(false);
            }
            form.applyDataColumnPropertyToMapElement(this);
        }

        container.onSelected = function () {
            for (var i = 0; i < onlineDataColumns.length; i++) {
                if (form.controls[onlineDataColumns[i][0] + "DataColumn"] != this)
                    form.controls[onlineDataColumns[i][0] + "DataColumn"].setSelected(false);
            }

            if (this.dataColumnObject && this.dataColumnObject.expression != null) {
                form.controls.expressionOnline.currentContainer = this;
                form.controls.expressionOnline.setEnabled(true);
                form.controls.expressionOnline.textBox.value = Base64.decode(this.dataColumnObject.expression);
            }
        }
    }

    var onlineTable = this.CreateHTMLTable();
    onlineTable.style.width = "100%";
    form.panels.Online.appendChild(onlineTable);
    
    var expressionOnline = this.MapElementExpression(null, 200, null, null);
    var onlineExpressionMenu = this.options.menus.onlineExpressionMenu || this.InitializeMapExpressionMenu("onlineExpressionMenu", expressionOnline, form);
    form.addControlRow(onlineTable, this.loc.PropertyMain.Expression, "expressionOnline", expressionOnline, "6px 12px 6px 12px");
    expressionOnline.menu = onlineExpressionMenu;
    expressionOnline.menu.parentButton = expressionOnline.button;

    expressionOnline.action = function () {
        if (this.currentContainer) {
            form.applyExpressionPropertyToMapElement(this.currentContainer, this.textBox.value);
        }
    }

    form.setMode = function (mode) {
        form.mode = mode;
        for (var panelName in form.panels) {
            form.panels[panelName].style.display = mode == panelName ? "" : "none";
            form.mainButtons[panelName].setSelected(mode == panelName);
        }
    }

    form.setValues = function () {
        form.setMode(this.mapProperties.mapMode);
        mapTypeControl.setKey(this.mapProperties.mapType);
        mapIDControl.setKey(this.mapProperties.mapID);
        displayNameTypeControl.setKey(this.mapProperties.displayNameType);
        showValueCheckBox.setChecked(this.mapProperties.showValue);
        colorEachCheckBox.setChecked(this.mapProperties.colorEach);
        dataFromControl.setKey(this.mapProperties.dataFrom);
        dataGridView.fillData(this.mapProperties.mapData);
        expressionChoropleth.textBox.value = "";
        expressionChoropleth.setEnabled(false);
        expressionOnline.textBox.value = "";
        expressionOnline.setEnabled(false);

        var meters = this.mapProperties.meters;

        var meterTypes = ["Key", "Name", "Value", "Color", "Group", "Latitude", "Longitude"];
        for (var i = 0; i < meterTypes.length; i++) {
            var meter = meters["Sti" + meterTypes[i] + "MapMeter"];
            var container = this.controls[jsObject.LowerFirstChar(meterTypes[i]) + "DataColumn"];

            if (meter) {
                container.addColumn(meter.label, meter);
                if (container.isSelected && container.item) container.item.action();
            }
            else
                container.clear();
        }
    }

    form.getValues = function () {
        props = {
            mapMode: form.mode,
            mapType: mapTypeControl.key,
            mapID: mapIDControl.key,
            showValue: showValueCheckBox.isChecked,
            colorEach: colorEachCheckBox.isChecked,
            displayNameType: displayNameTypeControl.key,
            mapData: {},
            dataFrom: dataFromControl.key
        }

        return props;
    }

    form.updateControlsVisibleStates = function () {
        this.controls.keyDataColumnRow.style.display = dataFromControl.key == "DataColumns" ? "" : "none";
        this.controls.nameDataColumnRow.style.display = dataFromControl.key == "DataColumns" ? "" : "none";
        this.controls.valueDataColumnRow.style.display = dataFromControl.key == "DataColumns" ? "" : "none";
        this.controls.groupDataColumnRow.style.display = dataFromControl.key == "DataColumns" && (mapTypeControl.key == "Group" || mapTypeControl.key == "HeatmapWithGroup") ? "" : "none";
        this.controls.colorDataColumnRow.style.display = dataFromControl.key == "DataColumns" && mapTypeControl.key == "Individual" ? "" : "none";
        this.controls.expressionChoroplethRow.style.display = dataFromControl.key == "DataColumns" ? "" : "none";
        dataGridView.style.display = dataFromControl.key == "Manual" ? "" : "none";
        dataGridView.updateColumnsState();
    }

    form.onshow = function () {
        form.currentPanelName = form.jsObject.options.propertiesPanel.getCurrentPanelName();
        form.jsObject.options.propertiesPanel.showContainer("Dictionary");
        form.controls.expressionChoropleth.textBox.value = "";
        form.controls.expressionChoropleth.setEnabled(false);
        form.controls.expressionOnline.textBox.value = "";
        form.controls.expressionOnline.setEnabled(false);
        var meterTypes = ["Key", "Name", "Value", "Color", "Group", "Latitude", "Longitude"];
        for (var i = 0; i < meterTypes.length; i++) {
            var container = this.controls[jsObject.LowerFirstChar(meterTypes[i]) + "DataColumn"];
            container.clear();
        }

        form.sendCommandToMapElement({ command: "GetMapElementProperties" },
            function (answer) {
                if (answer.mapElement) {
                    form.updateControls(answer.mapElement, true);
                }
            }
        );
    }

    form.onhide = function () {
        form.jsObject.options.propertiesPanel.showContainer(form.currentPanelName);
    }

    form.updateControls = function (mapProperties, notRepaintElement) {
        form.mapProperties = mapProperties;
        form.setValues();
        form.updateControlsVisibleStates();        
        if (!notRepaintElement) {
            form.currentMapElement.properties.svgContent = mapProperties.svgContent;
            form.currentMapElement.repaint();
        }
        for (var propName in mapProperties) {
            form.currentMapElement.properties[propName] = mapProperties[propName];
        }
    }

    form.applyPropertiesToMapElement = function (updateMapData) {
        form.sendCommandToMapElement(
            {
                command: "SetProperties",
                properties: form.getValues(),
                updateMapData: updateMapData
            },
            function (answer) {
                if (answer.mapElement) {
                    form.updateControls(answer.mapElement);
                    if (updateMapData) {
                        jsObject.options.report.dashboardStiMapElementStylesContent = null;
                        jsObject.options.homePanel.updateControls();
                    }
                }
            }
        );
    }

    form.applyDataColumnPropertyToMapElement = function (container) {
        form.sendCommandToMapElement(
            {
                command: "SetDataColumn",
                containerName: form.jsObject.UpperFirstChar(container.name),
                dataColumnObject: container.dataColumnObject
            },
            function (answer) {
                if (answer.mapElement) {
                    form.updateControls(answer.mapElement);
                    if (container.item) container.item.action();
                }
            }
        );
    }

    form.applyExpressionPropertyToMapElement = function (container, expressionValue) {
        if (container) {
            form.sendCommandToMapElement(
                {
                    command: "SetExpression",
                    containerName: form.jsObject.UpperFirstChar(container.name),
                    expressionValue: Base64.encode(expressionValue)
                },
                function (answer) {
                    if (answer.mapElement) {
                        form.updateControls(answer.mapElement);
                    }
                }
            );
        }
    }

    form.sendCommandToMapElement = function (updateParameters, callbackFunction) {
        updateParameters.zoom = form.jsObject.options.report.zoom.toString();

        form.jsObject.SendCommandToDesignerServer("UpdateMapElement",
            {
                componentName: form.currentMapElement.properties.name,
                updateParameters: updateParameters
            },
            function (answer) {
                callbackFunction(answer);
            });
    }

    return form;
}

StiMobileDesigner.prototype.InitializeMapExpressionMenu = function (name, expressionControl, form) {
    var menu = this.BaseContextMenu(name, "Down", null, this.GetStyles("MenuStandartItem"));
    var jsObject = this;

    menu.onshow = function () {
        var currentContainer = expressionControl.currentContainer;
        var items = [];
        items.push(jsObject.Item("newItem", jsObject.loc.FormDictionaryDesigner.NewItem, "Empty16.png", "newItem"));

        if (expressionControl.isEnabled && currentContainer && currentContainer.isSelected) {
            items.push("separator1");
            items.push(jsObject.Item("editExpression", jsObject.loc.Dashboard.EditExpression, "EditButton.png", "editExpression"));
            items.push(jsObject.Item("renameField", jsObject.loc.Buttons.Rename, "DataColumn.png", "renameField"));
            items.push(jsObject.Item("removeField", jsObject.loc.Dashboard.RemoveField, "Remove.png", "removeField"));

            if (currentContainer.dataColumnObject) {
                var functions = currentContainer.dataColumnObject.functions;
                if (functions && functions.length > 0) {
                    items.push("separator2");
                    for (var i = 0; i < functions.length; i++) {
                        items.push(jsObject.Item("Function_" + functions[i], functions[i], "CheckBox.png", "Function_" + functions[i]));
                    }
                }
            }
        }

        this.addItems(items);

        for (var itemName in this.items) {
            if (itemName.indexOf("Function_") == 0) {
                var funcItem = this.items[itemName];
                var isSelected = currentContainer.dataColumnObject && currentContainer.dataColumnObject.currentFunction &&
                    itemName.toLowerCase() == ("Function_" + currentContainer.dataColumnObject.currentFunction).toLowerCase();

                funcItem.caption.style.fontWeight = isSelected ? "bold" : "normal";
                funcItem.image.style.visibility = isSelected ? "visible" : "hidden";
            }
        }
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        var currentContainer = expressionControl.currentContainer;

        if (menuItem.key.indexOf("Function_") == 0) {
            form.sendCommandToMapElement(
                {
                    command: "SetFunction",
                    containerName: jsObject.UpperFirstChar(currentContainer.name),
                    function: menuItem.key.replace("Function_", "")
                },
                function (answer) {
                    if (answer.mapElement) {
                        form.updateControls(answer.mapElement);
                    }
                }
            );
            return;
        }

        switch (menuItem.key) {
            case "removeField": {
                currentContainer.clear();
                currentContainer.action();
                break;
            }
            case "editExpression": {
                menu.jsObject.InitializeExpressionEditorForm(function (expressionEditorForm) {
                    var propertiesPanel = expressionEditorForm.jsObject.options.propertiesPanel;
                    expressionEditorForm.propertiesPanelZIndex = propertiesPanel.style.zIndex;
                    expressionEditorForm.propertiesPanelIsEnabled = propertiesPanel.isEnabled;
                    expressionEditorForm.resultControl = expressionControl;
                    expressionEditorForm.changeVisibleState(true);
                });
                break;
            }
            case "newItem": {
                form.sendCommandToMapElement(
                    {
                        command: "NewItem",
                        containerName: jsObject.UpperFirstChar(currentContainer.name)
                    },
                    function (answer) {
                        if (answer.mapElement) {
                            form.updateControls(answer.mapElement);
                        }
                    }
                );
                break;
            }
        }
    }

    return menu;
}

StiMobileDesigner.prototype.MapDataColumnContainer = function (form, name, headerText, width, showItemImage) {
    var container = this.DataColumnContainer(name, headerText, width, showItemImage);
    var innerContainer = container.innerContainer;
    var jsObject = this;

    innerContainer.oldonmouseup = innerContainer.onmouseup;

    innerContainer.onmouseup = function (event) {
        if (innerContainer.canInsert()) {
            var originalItem = jsObject.options.itemInDrag.originalItem;
            var itemObject = jsObject.CopyObject(originalItem.itemObject);
            if (originalItem.closeButton && originalItem.closeButton.clicked) return;

            if (itemObject.typeItem == "Meter") {
                var fromContainerName = jsObject.UpperFirstChar(originalItem.container.name);
                var toContainerName = jsObject.UpperFirstChar(container.name);

                if (toContainerName != fromContainerName) {
                    form.sendCommandToMapElement(
                        {
                            command: "MoveMeter",
                            toContainerName: toContainerName,
                            fromContainerName: fromContainerName
                        },
                        function (answer) {
                            form.updateControls(answer.mapElement);
                            if (container.item) container.item.action();
                        });
                }
            }
            else {
                innerContainer.oldonmouseup(event);
            }
        }
    }

    return container;
}

StiMobileDesigner.prototype.MapElementExpression = function (name, width, toolTip, items) {
   return this.DropDownList(name, width, toolTip, items, false, false, null, false);
}
