﻿
StiMobileDesigner.prototype.InitializeEditPivotElementForm_ = function () {
    var form = this.DashboardBaseForm("editPivotElementForm", this.loc.Components.StiPivot, 1);
    form.isDockableToComponent = true;
    form.buttonsSeparator.style.display = "none";
    form.buttonsPanel.style.display = "none";
    form.container.style.borderTop = "0px";
    form.caption.style.padding = "0px 10px 0 12px";
    var jsObject = this;

    var controlsTable = this.CreateHTMLTable();
    form.container.appendChild(controlsTable);
    form.container.style.padding = "0 0 6px 0";

    var contextMenu = this.options.menus.pivotElementFormContextMenu || this.BaseContextMenu("pivotElementFormContextMenu", "Down", null, this.GetStyles("MenuStandartItem"));

    var columnsBlock = this.PivotDataColumnsContainerBlock(form, contextMenu, "columns", this.loc.FormCrossTabDesigner.Columns.replace(":", ""), true);
    columnsBlock.style.width = "calc(100% - 24px)";
    form.addControlRow(controlsTable, null, "columnsBlock", columnsBlock, "0px 12px 0px 12px");
    var parentColumnsContainer = columnsBlock.container.parentElement;

    var rowsBlock = this.PivotDataColumnsContainerBlock(form, contextMenu, "rows", this.loc.FormCrossTabDesigner.Rows.replace(":", ""), true);
    rowsBlock.style.width = "calc(100% - 24px)";
    form.addControlRow(controlsTable, null, "rowsBlock", rowsBlock, "0px 12px 0px 12px");

    var summariesBlock = this.PivotDataColumnsContainerBlock(form, contextMenu, "summaries", this.loc.FormCrossTabDesigner.Summary.replace(":", ""), true);
    summariesBlock.style.width = "calc(100% - 24px)";
    form.addControlRow(controlsTable, null, "summariesBlock", summariesBlock, "0px 12px 12px 12px");

    var expressionControl = this.TableElementExpression(null, 160, null, null);    
    form.addControlRow(controlsTable, this.loc.PropertyMain.Expression, "expression", expressionControl, "6px 12px 6px 12px");
    expressionControl.style.display = "inline-block";
    expressionControl.parentNode.style.textAlign = "right";
    expressionControl.menu = contextMenu;
    expressionControl.menu.parentButton = expressionControl.button;

    expressionControl.action = function () {
        var selectedItem = form.getSelectedItem();
        if (selectedItem) {
            var containerName = selectedItem.container.name;
            var itemIndex = selectedItem.container.getItemIndex(selectedItem);
            form.sendCommandToPivotElement(
                {
                    command: "SetExpression",
                    itemIndex: itemIndex,
                    containerName: containerName,
                    expressionValue: Base64.encode(expressionControl.textBox.value)
                },
                function (answer) {
                    var container = form.controls[containerName + "Block"].container;
                    container.updateMeters(answer.pivotElement.meters[containerName], itemIndex);
                    form.updatePivotSvgContent(answer.pivotElement.svgContent);
                }
            );
        }
    }

    contextMenu.action = function (menuItem) {
        var selectedItem = form.getSelectedItem();
        var container = contextMenu.container;
        if (!container && selectedItem) container = selectedItem.container;        

        this.changeVisibleState(false);
        if (!container) return;

        if (menuItem.key == "newItem") {
            form.sendCommandToPivotElement({ command: "NewItem", containerName: container.name },
                function (answer) {
                    var resultMeters = answer.pivotElement.meters[container.name];
                    container.updateMeters(resultMeters, resultMeters.length - 1);
                    form.updatePivotSvgContent(answer.pivotElement.svgContent);
                    form.checkStartMode();
                }
            );
            return;
        }
        
        if (!selectedItem) return;
        var itemIndex = container.getItemIndex(selectedItem);

        if (menuItem.key.indexOf("Function_") == 0) {
            form.sendCommandToPivotElement(
                {
                    command: "SetFunction",
                    containerName: container.name,
                    itemIndex: itemIndex,
                    function: menuItem.key.replace("Function_", "")
                },
                function (answer) {
                    container.updateMeters(answer.pivotElement.meters[container.name], itemIndex);
                    form.updatePivotSvgContent(answer.pivotElement.svgContent);
                }
            );
            return;
        }

        switch (menuItem.key) {
            case "removeField": {
                selectedItem.remove();
                break;
            }
            case "editExpression": {
                contextMenu.jsObject.InitializeExpressionEditorForm(function (expressionEditorForm) {
                    var propertiesPanel = expressionEditorForm.jsObject.options.propertiesPanel;
                    expressionEditorForm.propertiesPanelZIndex = propertiesPanel.style.zIndex;
                    expressionEditorForm.propertiesPanelIsEnabled = propertiesPanel.isEnabled;
                    expressionEditorForm.resultControl = expressionControl;
                    expressionEditorForm.changeVisibleState(true);
                });
                break;
            }
            case "duplicateField": {
                form.sendCommandToPivotElement({ command: "DuplicateMeter", containerName: container.name, itemIndex: itemIndex },
                    function (answer) {
                        var resultMeters = answer.pivotElement.meters[container.name];
                        container.updateMeters(resultMeters, answer.insertIndex);
                        form.updatePivotSvgContent(answer.pivotElement.svgContent);
                        form.checkStartMode();
                    }
                );
                break;
            }
        }
    }

    contextMenu.onshow = function () {
        var selectedItem = form.getSelectedItem();
        var items = [];

        items.push(this.jsObject.Item("newItem", this.jsObject.loc.FormDictionaryDesigner.NewItem, "Empty16.png", "newItem"));

        if (expressionControl.isEnabled && selectedItem) {            
            var container = contextMenu.container || selectedItem.container;

            items.push("separator1");
            if (container && container.multiItems) {
                items.push(this.jsObject.Item("duplicateField", this.jsObject.loc.Dashboard.DuplicateField, "Duplicate.png", "duplicateField"));
            }
            items.push(this.jsObject.Item("editExpression", this.jsObject.loc.Dashboard.EditExpression, "EditButton.png", "editExpression"));
            items.push(this.jsObject.Item("renameField", this.jsObject.loc.Buttons.Rename, "DataColumn.png", "renameField"));
            items.push(this.jsObject.Item("removeField", this.jsObject.loc.Dashboard.RemoveField, "Remove.png", "removeField"));

            if (selectedItem.itemObject) {
                var functions = selectedItem.itemObject.functions;
                if (functions && functions.length > 0) {
                    items.push("separator2");
                    for (var i = 0; i < functions.length; i++) {
                        items.push(this.jsObject.Item("Function_" + functions[i], functions[i], "CheckBox.png", "Function_" + functions[i]));
                    }
                }
            }
        }

        this.addItems(items);

        for (var itemName in this.items) {
            if (itemName.indexOf("Function_") == 0) {
                var funcItem = this.items[itemName];
                var isSelected = selectedItem && selectedItem.itemObject.currentFunction &&
                    itemName.toLowerCase() == ("Function_" + selectedItem.itemObject.currentFunction).toLowerCase();
                funcItem.caption.style.fontWeight = isSelected ? "bold" : "normal";
                funcItem.image.style.visibility = isSelected ? "visible" : "hidden";
            }
        }
    }

    contextMenu.onhide = function () {
        contextMenu.container = null;
    }

    form.setValues = function () {        
        var selectedItem = form.getSelectedItem();
        var meters = this.pivotProperties.meters;

        columnsBlock.container.updateMeters(meters.columns);
        rowsBlock.container.updateMeters(meters.rows);
        summariesBlock.container.updateMeters(meters.summaries);
    }

    form.checkStartMode = function () {
        var itemsCount = 0;
        var containers = ["columns", "rows", "summaries"];
        for (var i = 0; i < containers.length; i++) {
            var container = form.controls[containers[i] + "Block"].container;
            itemsCount += container.getCountItems();
        }

        if (itemsCount == 0) {
            form.container.appendChild(columnsBlock.container);
            controlsTable.style.display = "none";
            columnsBlock.container.style.height = columnsBlock.container.style.maxHeight = "260px";
            columnsBlock.container.style.width = "267px";
            columnsBlock.container.style.margin = "6px 12px 6px 12px";
        }
        else {
            parentColumnsContainer.appendChild(columnsBlock.container);
            controlsTable.style.display = "";
            columnsBlock.container.style.height = "auto";
            columnsBlock.container.style.width = "auto";
            columnsBlock.container.style.margin = "0";
            columnsBlock.container.style.maxHeight = "100px";
        }
    }

    form.onshow = function () {
        form.currentPanelName = form.jsObject.options.propertiesPanel.getCurrentPanelName();
        form.jsObject.options.propertiesPanel.showContainer("Dictionary");
        expressionControl.textBox.value = "";
        expressionControl.setEnabled(false);
        columnsBlock.container.clear();
        rowsBlock.container.clear();
        summariesBlock.container.clear();
        form.checkStartMode();

        form.sendCommandToPivotElement({ command: "GetPivotElementProperties" },
            function (answer) {
                form.pivotProperties = answer.pivotElement;
                form.setValues();
                form.checkStartMode();
            }
        );
    }

    form.onhide = function () {
        form.jsObject.options.propertiesPanel.showContainer(form.currentPanelName);
    }

    form.getSelectedItem = function () {
        var containers = ["columns", "rows", "summaries"];
        for (var i = 0; i < containers.length; i++) {
            var container = form.controls[containers[i] + "Block"].container;
            if (container.selectedItem) {
                return container.selectedItem;
            }
        }
        return null;
    }
        
    form.applyExpressionPropertyToPivotElement = function (containerName, itemIndex, expressionValue) {
        form.sendCommandToPivotElement(
            {
                command: "SetExpression",
                itemIndex: itemIndex,
                containerName: containerName,
                expressionValue: Base64.encode(expressionValue)
            },
            function (answer) {
                if (answer.pivotElement) {
                    var container = form.controls[containerName + "Block"].container;
                    container.updateMeters(answer.pivotElement.meters[containerName], itemIndex);
                    form.updatePivotSvgContent(answer.pivotElement.svgContent);
                }
            }
        );
    }

    form.sendCommandToPivotElement = function (updateParameters, callbackFunction) {
        updateParameters.zoom = form.jsObject.options.report.zoom.toString();

        form.jsObject.SendCommandToDesignerServer("UpdatePivotElement",
            {
                componentName: form.currentPivotElement.properties.name,
                updateParameters: updateParameters
            },
            function (answer) {
                callbackFunction(answer);
            });
    }

    form.updatePivotSvgContent = function (svgContent) {
        this.currentPivotElement.properties.svgContent = svgContent;
        this.currentPivotElement.repaint();
    }

    return form;
}

StiMobileDesigner.prototype.PivotDataColumnsContainerBlock = function (form, contextMenu, containerName, headerText, multiItems) {
    var table = this.CreateHTMLTable();

    var header = table.addTextCell(headerText);
    header.className = "stiDesignerTextContainer";
    header.style.padding = "12px 0 12px 0";

    var container = this.DataContainer();
    container.multiItems = multiItems;
    container.name = containerName;
    table.container = container;
    container.style.minHeight = "31px";
    container.style.maxHeight = "100px";
    table.addCellInNextRow(container);

    container.updateMeters = function (meters, selectedIndex) {
        var oldScrollTop = this.scrollTop;
        this.style.height = this.offsetHeight + "px";

        this.clear();
        for (var i = 0; i < meters.length; i++) {
            this.addItem(meters[i].label, "Dashboards.Meters." + meters[i].typeIcon + ".png", meters[i]);
        }
        if (selectedIndex != null && selectedIndex < meters.length && selectedIndex >= 0) {
            this.childNodes[selectedIndex].select();
        }
        this.scrollTop = oldScrollTop;
        this.style.height = "auto";
        this.style.paddingBottom = (multiItems && this.getCountItems() > 0) ? "30px" : "0px";
    }

    container.onmouseup = function (event) {
        if (event.button == 2) {
            event.stopPropagation();
            var point = this.jsObject.FindMousePosOnMainPanel(event);
            contextMenu.show(point.xPixels + 3, point.yPixels + 3, "Down", "Right");
            contextMenu.container = this;
        }
        else if (this.jsObject.options.itemInDrag) {
            var itemObject = this.jsObject.CopyObject(this.jsObject.options.itemInDrag.originalItem.itemObject);
            if (!itemObject) return;
            var typeItem = itemObject.typeItem;

            if (typeItem == "Meter") {
                var toIndex = this.getOverItemIndex();
                var fromContainerName = this.jsObject.options.itemInDrag.originalItem.container.name;
                var fromContainer = form.controls[fromContainerName + "Block"].container;
                var fromIndex = fromContainer.getItemIndex(this.jsObject.options.itemInDrag.originalItem);

                if (containerName != fromContainerName || (toIndex != null && fromIndex != null && fromIndex != toIndex)) {
                    form.sendCommandToPivotElement(
                        {
                            command: "MoveMeter",
                            toContainerName: containerName,
                            fromContainerName: fromContainerName,
                            toIndex: toIndex,
                            fromIndex: fromIndex
                        },
                        function (answer) {
                            if (containerName != fromContainerName) {
                                var fromContainer = form.controls[fromContainerName + "Block"].container;
                                fromContainer.updateMeters(answer.pivotElement.meters[fromContainerName]);
                            }
                            container.updateMeters(answer.pivotElement.meters[containerName], toIndex);
                            form.updatePivotSvgContent(answer.pivotElement.svgContent);
                            form.checkStartMode();
                        });
                }
            }
            else if (typeItem == "Column" || typeItem == "DataSource" || typeItem == "BusinessObject") {
                var draggedItem = {
                    itemObject: itemObject
                };

                if (typeItem == "Column") {
                    var columnParent = this.jsObject.options.dictionaryTree.getCurrentColumnParent();
                    if (columnParent) {
                        draggedItem.currentParentType = columnParent.type;
                        draggedItem.currentParentName = (columnParent.type == "BusinessObject") ? this.jsObject.options.itemInDrag.originalItem.getBusinessObjectFullName() : columnParent.name;
                    }
                }
                else {
                    draggedItem.currentParentType = typeItem;
                    draggedItem.currentParentName = itemObject.name;
                }

                var params = {
                    command: "InsertMeters",
                    containerName: containerName,
                    draggedItem: draggedItem
                }

                if (typeItem == "Column") {
                    params.insertIndex = container.getOverItemIndex();
                }

                form.sendCommandToPivotElement(params,
                    function (answer) {
                        var insertIndex = params.insertIndex != null ? params.insertIndex : answer.pivotElement.meters[containerName].length - 1;
                        container.updateMeters(answer.pivotElement.meters[containerName], insertIndex);
                        form.updatePivotSvgContent(answer.pivotElement.svgContent);
                        form.checkStartMode();
                    }
                );
            }
        }

        return false;
    }

    container.onAction = function () {
        var selectedItem = null;
        var containers = ["columns", "rows", "summaries"];
        for (var i = 0; i < containers.length; i++) {
            var container = form.controls[containers[i] + "Block"].container;
            if (this != container && container.selectedItem) {
                container.selectedItem.setSelected(false);
                container.selectedItem = null;
            }
            if (container.selectedItem) {
                selectedItem = container.selectedItem;
            }
        }
        form.controls.expression.setEnabled(selectedItem != null);
        form.controls.expression.textBox.value = selectedItem != null ? Base64.decode(selectedItem.itemObject.expression) : "";
    }

    container.onRemove = function (itemIndex) {
        form.sendCommandToPivotElement({ command: "RemoveMeter", containerName: containerName, itemIndex: itemIndex },
            function (answer) {
                container.updateMeters(answer.pivotElement.meters[containerName], container.getSelectedItemIndex());
                form.updatePivotSvgContent(answer.pivotElement.svgContent);
                form.checkStartMode();
            }
        );
    }

    container.oncontextmenu = function (event) {
        return false;
    }

    return table;
}