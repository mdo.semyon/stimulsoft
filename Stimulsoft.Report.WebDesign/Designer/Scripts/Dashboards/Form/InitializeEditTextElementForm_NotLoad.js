﻿
StiMobileDesigner.prototype.InitializeEditTextElementForm_ = function () {
    var form = this.DashboardBaseForm("editTextElementForm", this.loc.Components.StiText, 1);    
    form.isDockableToComponent = true;
    form.buttonsSeparator.style.display = "none";
    form.buttonsPanel.style.display = "none";
    form.container.style.borderTop = "0px";
    form.caption.style.padding = "0px 10px 0 12px";

    var richTextControl = this.TextElementEditor("editTextElementFormEditor", 600, 300);
    richTextControl.richText.style.margin = "0px";
    richTextControl.richText.style.border = "0px";
    richTextControl.style.margin = "12px";
    form.container.appendChild(richTextControl);
        
    richTextControl.controls.fontList.action = function () {
        form.setProperty("fontName", this.key);
    }

    richTextControl.controls.sizeList.action = function () {
        form.setProperty("fontSize", this.key);
    }

    richTextControl.controls.buttonAlignLeft.action = function () {
        form.setProperty("horAlignment", "Left");
    }

    richTextControl.controls.buttonAlignCenter.action = function () {
        form.setProperty("horAlignment", "Center");
    }

    richTextControl.controls.buttonAlignRight.action = function () {
        form.setProperty("horAlignment", "Right");
    }

    richTextControl.controls.buttonAlignWidth.action = function () {
        form.setProperty("horAlignment", "Width");
    }

    richTextControl.onchange = function () {
        form.setProperty("text", Base64.encode(richTextControl.getText()), true);
    };

    richTextControl.onchangetext = function () {
        form.setProperty("text", Base64.encode(richTextControl.getText()), true);
    };

    form.updateControls = function () {
        var font = this.jsObject.FontStrToObject(form.currentTextElement.properties.font);
        richTextControl.setText(Base64.decode(form.currentTextElement.properties.text), font);
        richTextControl.controls.fontList.setKey(font.name);
        richTextControl.controls.sizeList.setKey(font.size);
        richTextControl.controls.buttonAlignLeft.setSelected(form.currentTextElement.properties.horAlignment == "Left");
        richTextControl.controls.buttonAlignCenter.setSelected(form.currentTextElement.properties.horAlignment == "Center");
        richTextControl.controls.buttonAlignRight.setSelected(form.currentTextElement.properties.horAlignment == "Right");
        richTextControl.controls.buttonAlignWidth.setSelected(form.currentTextElement.properties.horAlignment == "Width");
    }

    form.onshow = function () {
        form.updateControls();
    }

    form.setProperty = function (propertyName, propertyValue, notUpdateControls) {
        form.sendCommandToTextElement(
            {
                command: "SetProperty",
                propertyName: propertyName,
                propertyValue: propertyValue,
            },
            function (answer) {
                for (var propName in answer.textElement) {
                    form.currentTextElement.properties[propName] = answer.textElement[propName];
                }
                form.currentTextElement.repaint();
                form.jsObject.options.homePanel.updateControls();
                if (!notUpdateControls) form.updateControls();
            }
        );
    }

    form.sendCommandToTextElement = function (updateParameters, callbackFunction) {
        form.jsObject.SendCommandToDesignerServer("UpdateTextElement",
            {
                componentName: form.currentTextElement.properties.name,
                updateParameters: updateParameters
            },
            function (answer) {
                callbackFunction(answer);
            });
    }

    return form;
}
