﻿
StiMobileDesigner.prototype.InitializeEditChartElementForm_ = function () {
    var form = this.DashboardBaseForm("editChartElementForm", this.loc.Components.StiChart, 1);
    form.isDockableToComponent = true;
    form.buttonsSeparator.style.display = "none";
    form.buttonsPanel.style.display = "none";
    form.container.style.borderTop = "0px";
    form.caption.style.padding = "0px 10px 0 12px";
    var jsObject = this;

    var controlsTable = this.CreateHTMLTable();
    form.container.appendChild(controlsTable);
    form.container.style.padding = "0 0 6px 0";

    var contextMenu = this.options.menus.chartElementFormContextMenu || this.BaseContextMenu("chartElementFormContextMenu", "Down", null, this.GetStyles("MenuStandartItem"));

    var valuesBlock = this.ChartDataColumnsContainerBlock(form, contextMenu, "values", this.loc.PropertyMain.Values , true);
    valuesBlock.style.width = "calc(100% - 24px)";
    form.addControlRow(controlsTable, null, "valuesBlock", valuesBlock, "0px 12px 0px 12px");
    var parentValuesContainer = valuesBlock.container.parentElement;

    var argumentsBlock = this.ChartDataColumnsContainerBlock(form, contextMenu, "arguments", this.loc.PropertyMain.Arguments, true);
    argumentsBlock.style.width = "calc(100% - 24px)";
    form.addControlRow(controlsTable, null, "argumentsBlock", argumentsBlock, "0px 12px 0px 12px");

    var weightsBlock = this.ChartDataColumnsContainerBlock(form, contextMenu, "weights", this.loc.PropertyMain.Weights, true);
    weightsBlock.style.width = "calc(100% - 24px)";
    form.addControlRow(controlsTable, null, "weightsBlock", weightsBlock, "0px 12px 0px 12px");

    var seriesBlock = this.ChartDataColumnsContainerBlock(form, contextMenu, "series", this.loc.PropertyMain.Series);
    seriesBlock.style.width = "calc(100% - 24px)";
    form.addControlRow(controlsTable, null, "seriesBlock", seriesBlock, "0px 12px 12px 12px");

    var seriesTypeControl = this.ChartSeriesTypeControl("chartElementFormSeriesType");
    seriesTypeControl.style.width = "calc(100% - 24px)";
    form.addControlRow(controlsTable, null, "seriesType", seriesTypeControl, "6px 12px 6px 12px");

    seriesTypeControl.action = function () {
        var selectedItem = form.getSelectedItem();
        if (selectedItem && selectedItem.container.name == "values") {
            var itemIndex = selectedItem.container.getItemIndex(selectedItem);
            var oldIsBubble = form.oldSeriesType != null && form.oldSeriesType == "Bubble";
            var seriesType = this.key;
            form.oldSeriesType = seriesType;
            form.setBubbleMode(this.key == "Bubble");
            form.sendCommandToChartElement(
                {
                    command: "SetSeriesType",
                    itemIndex: itemIndex,
                    seriesType: seriesType,
                    oldIsBubble: oldIsBubble
                },
                function (answer) {
                    if (oldIsBubble && seriesType != "Bubble" || !oldIsBubble && seriesType == "Bubble") {
                        argumentsBlock.container.updateMeters(answer.chartElement.meters.arguments);
                        if (seriesType == "Bubble") {
                            weightsBlock.container.clear();
                        }
                    }
                    selectedItem.container.updateMeters(answer.chartElement.meters[selectedItem.container.name], itemIndex);
                    form.updateChartSvgContent(answer.chartElement.svgContent);
                    form.checkStartMode();
                }
            );
        }
    }

    var expressionControl = this.TableElementExpression(null, 150, null, null);    
    form.addControlRow(controlsTable, this.loc.PropertyMain.Expression, "expression", expressionControl, "6px 12px 6px 12px");
    expressionControl.style.display = "inline-block";
    expressionControl.parentNode.style.textAlign = "right";
    expressionControl.menu = contextMenu;
    expressionControl.menu.parentButton = expressionControl.button;

    expressionControl.action = function () {
        var selectedItem = form.getSelectedItem();
        if (selectedItem) {
            var containerName = selectedItem.container.name;
            var itemIndex = selectedItem.container.getItemIndex(selectedItem);
            form.applyExpressionPropertyToChartElement(containerName, itemIndex, expressionControl.textBox.value);
        }
    }

    contextMenu.action = function (menuItem) {
        var selectedItem = form.getSelectedItem();
        var container = contextMenu.container;
        if (!container && selectedItem) container = selectedItem.container;        

        this.changeVisibleState(false);
        if (!container) return;

        if (menuItem.key == "newItem") {
            form.sendCommandToChartElement({ command: "NewItem", containerName: container.name, oldSeriesType: form.oldSeriesType },
                function (answer) {
                    var resultMeters = answer.chartElement.meters[container.name];
                    container.updateMeters(resultMeters, resultMeters.length - 1);
                    form.updateChartSvgContent(answer.chartElement.svgContent);
                    form.checkStartMode();
                }
            );
            return;
        }
        
        if (!selectedItem) return;
        var itemIndex = container.getItemIndex(selectedItem);

        if (menuItem.key.indexOf("Function_") == 0) {
            form.sendCommandToChartElement(
                {
                    command: "SetFunction",
                    containerName: container.name,
                    itemIndex: itemIndex,
                    function: menuItem.key.replace("Function_", "")
                },
                function (answer) {
                    container.updateMeters(answer.chartElement.meters[container.name], itemIndex);
                    form.updateChartSvgContent(answer.chartElement.svgContent);
                }
            );
            return;
        }

        switch (menuItem.key) {
            case "removeField": {
                selectedItem.remove();
                break;
            }
            case "editExpression": {
                contextMenu.jsObject.InitializeExpressionEditorForm(function (expressionEditorForm) {
                    var propertiesPanel = expressionEditorForm.jsObject.options.propertiesPanel;
                    expressionEditorForm.propertiesPanelZIndex = propertiesPanel.style.zIndex;
                    expressionEditorForm.propertiesPanelIsEnabled = propertiesPanel.isEnabled;
                    expressionEditorForm.resultControl = expressionControl;
                    expressionEditorForm.changeVisibleState(true);
                });
                break;
            }
            case "duplicateField": {
                form.sendCommandToChartElement({ command: "DuplicateMeter", containerName: container.name, itemIndex: itemIndex },
                    function (answer) {
                        var resultMeters = answer.chartElement.meters[container.name];
                        container.updateMeters(resultMeters, answer.insertIndex);
                        form.updateChartSvgContent(answer.chartElement.svgContent);
                        form.checkStartMode();
                    }
                );
                break;
            }
        }
    }

    contextMenu.onshow = function () {
        var selectedItem = form.getSelectedItem();
        var items = [];

        items.push(this.jsObject.Item("newItem", this.jsObject.loc.FormDictionaryDesigner.NewItem, "Empty16.png", "newItem"));

        if (expressionControl.isEnabled && selectedItem) {            
            var container = contextMenu.container || selectedItem.container;

            items.push("separator1");
            if (container && container.multiItems) {
                items.push(this.jsObject.Item("duplicateField", this.jsObject.loc.Dashboard.DuplicateField, "Duplicate.png", "duplicateField"));
            }
            items.push(this.jsObject.Item("editExpression", this.jsObject.loc.Dashboard.EditExpression, "EditButton.png", "editExpression"));
            items.push(this.jsObject.Item("renameField", this.jsObject.loc.Buttons.Rename, "DataColumn.png", "renameField"));
            items.push(this.jsObject.Item("removeField", this.jsObject.loc.Dashboard.RemoveField, "Remove.png", "removeField"));

            if (selectedItem.itemObject) {
                var functions = selectedItem.itemObject.functions;
                if (functions && functions.length > 0) {
                    items.push("separator2");
                    for (var i = 0; i < functions.length; i++) {
                        items.push(this.jsObject.Item("Function_" + functions[i], functions[i], "CheckBox.png", "Function_" + functions[i]));
                    }
                }
            }
        }

        this.addItems(items);

        for (var itemName in this.items) {
            if (itemName.indexOf("Function_") == 0) {
                var funcItem = this.items[itemName];
                var isSelected = selectedItem && selectedItem.itemObject.currentFunction &&
                    itemName.toLowerCase() == ("Function_" + selectedItem.itemObject.currentFunction).toLowerCase();
                funcItem.caption.style.fontWeight = isSelected ? "bold" : "normal";
                funcItem.image.style.visibility = isSelected ? "visible" : "hidden";
            }
        }
    }

    contextMenu.onhide = function () {
        contextMenu.container = null;
    }

    form.setValues = function () {
        var selectedItem = form.getSelectedItem();
        var meters = this.chartProperties.meters;
                
        if (meters.values.length > 0) {
            form.oldSeriesType = meters.values[0].seriesType;
            seriesTypeControl.setKey(form.oldSeriesType);
        }

        form.setBubbleMode(seriesTypeControl.key == "Bubble");

        argumentsBlock.container.updateMeters(meters.arguments);        
        seriesBlock.container.updateMeters(meters.series);
        weightsBlock.container.updateMeters(meters.weights);
        valuesBlock.container.updateMeters(meters.values, 0);
    }

    form.checkStartMode = function () {
        var itemsCount = 0;
        var containers = ["values", "arguments", "weights", "series"];
        for (var i = 0; i < containers.length; i++) {
            var container = form.controls[containers[i] + "Block"].container;
            itemsCount += container.getCountItems();
        }

        if (itemsCount == 0) {
            form.container.appendChild(valuesBlock.container);
            controlsTable.style.display = "none";
            valuesBlock.container.style.height = valuesBlock.container.style.maxHeight = "260px";
            valuesBlock.container.style.width = "257px";
            valuesBlock.container.style.margin = "6px 12px 6px 12px";
        }
        else {
            parentValuesContainer.appendChild(valuesBlock.container);
            controlsTable.style.display = "";
            valuesBlock.container.style.height = "auto";
            valuesBlock.container.style.width = "auto";
            valuesBlock.container.style.margin = "0";
            valuesBlock.container.style.maxHeight = "100px";
        }
    }

    form.onshow = function () {
        form.currentPanelName = form.jsObject.options.propertiesPanel.getCurrentPanelName();
        form.jsObject.options.propertiesPanel.showContainer("Dictionary");
        form.oldSeriesType = null;
        valuesBlock.container.clear();
        argumentsBlock.container.clear();
        weightsBlock.container.clear();
        seriesBlock.container.clear();
        valuesBlock.header.innerHTML = this.jsObject.loc.PropertyMain.Values;
        argumentsBlock.header.innerHTML = this.jsObject.loc.PropertyMain.Arguments;
        weightsBlock.style.display = "none";
        expressionControl.textBox.value = "";
        expressionControl.setEnabled(false);
        seriesTypeControl.setKey("ClusteredColumn");
        seriesTypeControl.setEnabled(false);
        form.checkStartMode();

        form.sendCommandToChartElement({ command: "GetChartElementProperties" },
            function (answer) {
                form.chartProperties = answer.chartElement;
                form.setValues();
                form.checkStartMode();
            }
        );
    }

    form.onhide = function () {
        form.jsObject.options.propertiesPanel.showContainer(form.currentPanelName);
    }

    form.setBubbleMode = function (state) {
        weightsBlock.style.display = state ? "" : "none";
        valuesBlock.header.innerHTML = state ? "X" : this.jsObject.loc.PropertyMain.Values;
        argumentsBlock.header.innerHTML = state ? "Y" : this.jsObject.loc.PropertyMain.Arguments;
    }

    form.getSelectedItem = function () {
        var containers = ["values", "arguments", "weights", "series"];
        for (var i = 0; i < containers.length; i++) {
            var container = form.controls[containers[i] + "Block"].container;
            if (container.selectedItem) {
                return container.selectedItem;
            }
        }
        return null;
    }
        
    form.applyExpressionPropertyToChartElement = function (containerName, itemIndex, expressionValue) {
        form.sendCommandToChartElement(
            {
                command: "SetExpression",
                itemIndex: itemIndex,
                containerName: containerName,
                expressionValue: Base64.encode(expressionValue)
            },
            function (answer) {
                if (answer.chartElement) {
                    var container = form.controls[containerName + "Block"].container;
                    container.updateMeters(answer.chartElement.meters[containerName], itemIndex);
                    form.updateChartSvgContent(answer.chartElement.svgContent);
                }
            }
        );
    }

    form.sendCommandToChartElement = function (updateParameters, callbackFunction) {
        updateParameters.zoom = form.jsObject.options.report.zoom.toString();

        form.jsObject.SendCommandToDesignerServer("UpdateChartElement",
            {
                componentName: form.currentChartElement.properties.name,
                updateParameters: updateParameters
            },
            function (answer) {
                if (answer.chartElement) {
                    seriesTypeControl.seriesTypes = answer.chartElement.seriesTypes;
                }
                callbackFunction(answer);
            });
    }

    form.updateChartSvgContent = function (svgContent) {
        this.currentChartElement.properties.svgContent = svgContent;
        this.currentChartElement.repaint();
    }

    return form;
}


StiMobileDesigner.prototype.ChartDataColumnsContainerBlock = function (form, contextMenu, containerName, headerText, multiItems) {
    var table = this.CreateHTMLTable();

    var header = table.addTextCell(headerText);
    table.header = header;
    header.className = "stiDesignerTextContainer";
    header.style.padding = "12px 0 12px 0";

    var container = this.DataContainer();
    container.multiItems = multiItems;
    container.name = containerName;
    table.container = container;
    container.style.minHeight = "31px";
    container.style.maxHeight = "100px";
    table.addCellInNextRow(container);

    container.updateMeters = function (meters, selectedIndex) {
        var oldScrollTop = this.scrollTop;
        this.style.height = this.offsetHeight + "px";

        this.clear();
        for (var i = 0; i < meters.length; i++) {
            this.addItem(meters[i].label, "Dashboards.Meters." + meters[i].typeIcon + ".png", meters[i]);
        }
        if (selectedIndex != null && selectedIndex < meters.length && selectedIndex >= 0) {
            this.childNodes[selectedIndex].select();
        }
        this.scrollTop = oldScrollTop;
        this.style.height = "auto";
        this.style.paddingBottom = (multiItems && this.getCountItems() > 0) ? "30px" : "0px";

        if (this.name == "values" && this.getCountItems() == 0) {
            form.oldSeriesType = null;
            form.setBubbleMode(false);
            form.controls.seriesType.setKey("ClusteredColumn");

            if (form.controls.weightsBlock.container.getCountItems() > 0) {
                form.sendCommandToChartElement(
                    {
                        command: "SetSeriesType",
                        itemIndex: 0,
                        seriesType: "ClusteredColumn",
                        oldIsBubble: true
                    },
                    function () {
                        form.controls.weightsBlock.container.clear();
                    }
                );
            }
        }
    }

    container.onmouseup = function (event) {
        if (event.button == 2) {
            event.stopPropagation();
            var point = this.jsObject.FindMousePosOnMainPanel(event);
            contextMenu.show(point.xPixels + 3, point.yPixels + 3, "Down", "Right");
            contextMenu.container = this;
        }
        else if (this.jsObject.options.itemInDrag) {
            var itemObject = this.jsObject.CopyObject(this.jsObject.options.itemInDrag.originalItem.itemObject);
            if (!itemObject) return;
            var typeItem = itemObject.typeItem;

            if (typeItem == "Meter") {
                var toIndex = this.getOverItemIndex();
                var fromContainerName = this.jsObject.options.itemInDrag.originalItem.container.name;
                var fromContainer = form.controls[fromContainerName + "Block"].container;
                var fromIndex = fromContainer.getItemIndex(this.jsObject.options.itemInDrag.originalItem);

                if (containerName != fromContainerName || (toIndex != null && fromIndex != null && fromIndex != toIndex)) {
                    form.sendCommandToChartElement(
                        {
                            command: "MoveMeter",
                            toContainerName: containerName,
                            fromContainerName: fromContainerName,
                            toIndex: toIndex,
                            fromIndex: fromIndex,
                            oldSeriesType: form.oldSeriesType
                        },
                        function (answer) {
                            if (containerName != fromContainerName) {
                                var fromContainer = form.controls[fromContainerName + "Block"].container;
                                fromContainer.updateMeters(answer.chartElement.meters[fromContainerName]);
                            }
                            container.updateMeters(answer.chartElement.meters[containerName], toIndex);
                            form.updateChartSvgContent(answer.chartElement.svgContent);
                            form.checkStartMode();
                        });
                }
            }
            else if (typeItem == "Column" || typeItem == "DataSource" || typeItem == "BusinessObject") {
                var draggedItem = {
                    itemObject: itemObject
                };

                if (typeItem == "Column") {
                    var columnParent = this.jsObject.options.dictionaryTree.getCurrentColumnParent();
                    if (columnParent) {
                        draggedItem.currentParentType = columnParent.type;
                        draggedItem.currentParentName = (columnParent.type == "BusinessObject") ? this.jsObject.options.itemInDrag.originalItem.getBusinessObjectFullName() : columnParent.name;
                    }
                }
                else {
                    draggedItem.currentParentType = typeItem;
                    draggedItem.currentParentName = itemObject.name;
                }

                var params = {
                    command: "InsertMeters",
                    containerName: containerName,
                    draggedItem: draggedItem
                }

                if (typeItem == "Column") {
                    params.insertIndex = container.getOverItemIndex();
                }

                form.sendCommandToChartElement(params,
                    function (answer) {
                        var insertIndex = params.insertIndex != null ? params.insertIndex : answer.chartElement.meters[containerName].length - 1;
                        container.updateMeters(answer.chartElement.meters[containerName], insertIndex);
                        form.updateChartSvgContent(answer.chartElement.svgContent);
                        form.checkStartMode();
                    }
                );
            }
        }

        return false;
    }

    container.onAction = function () {
        var selectedItem = null;
        var containers = ["values", "arguments", "weights", "series"];
        for (var i = 0; i < containers.length; i++) {
            var container = form.controls[containers[i] + "Block"].container;
            if (this != container && container.selectedItem) {
                container.selectedItem.setSelected(false);
                container.selectedItem = null;
            }
            if (container.selectedItem) {
                selectedItem = container.selectedItem;
            }
        }
        form.controls.expression.setEnabled(selectedItem != null);
        form.controls.expression.textBox.value = selectedItem != null ? Base64.decode(selectedItem.itemObject.expression) : "";
        form.controls.seriesType.setEnabled(selectedItem != null && selectedItem.itemObject.seriesType);
        if (selectedItem != null && selectedItem.itemObject.seriesType) {
            form.controls.seriesType.setKey(selectedItem.itemObject.seriesType);
        }
    }

    container.onRemove = function (itemIndex) {
        form.sendCommandToChartElement({ command: "RemoveMeter", containerName: containerName, itemIndex: itemIndex },
            function (answer) {
                container.updateMeters(answer.chartElement.meters[containerName], container.getSelectedItemIndex());
                form.updateChartSvgContent(answer.chartElement.svgContent);
                form.checkStartMode();
            }
        );
    }

    container.oncontextmenu = function (event) {
        return false;
    }

    return table;
}