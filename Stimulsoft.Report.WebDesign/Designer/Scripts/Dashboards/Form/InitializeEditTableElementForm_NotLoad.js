﻿
StiMobileDesigner.prototype.InitializeEditTableElementForm_ = function () {
    var form = this.DashboardBaseForm("editTableElementForm", this.loc.Components.StiTable, 1);
    form.isDockableToComponent = true;
    form.buttonsSeparator.style.display = "none";
    form.buttonsPanel.style.display = "none";
    form.container.style.borderTop = "0px";
    form.caption.style.padding = "0px 10px 0 12px";
    var jsObject = this;

    var controlsTable = this.CreateHTMLTable();
    form.container.appendChild(controlsTable);
    form.container.style.padding = "0 0 6px 0";
    
    //Data Container
    var dataContainer = this.DataContainer(null, 280, true);
    dataContainer.style.minWidth = "280px";
    form.addControlRow(controlsTable, null, "dataContainer", dataContainer, "6px 12px 6px 12px");

    dataContainer.updateMeters = function (meters, selectedIndex) {
        var oldScrollTop = this.scrollTop;
        this.clear();
        for (var i = 0; i < meters.length; i++) {
            this.addItem(meters[i].label, "Dashboards.Meters." + meters[i].typeIcon + ".png", meters[i]);
        }
        if (selectedIndex != null && selectedIndex >= 0 && selectedIndex < meters.length) {
            this.childNodes[selectedIndex].select();
        }
        this.scrollTop = oldScrollTop;
    }

    var contextMenu = this.options.menus.tableElementFormContextMenu || this.BaseContextMenu("tableElementFormContextMenu", "Down", null, this.GetStyles("MenuStandartItem"));

    dataContainer.onmouseup = function (event) {
        if (event.button == 2) {
            event.stopPropagation();
            var point = this.jsObject.FindMousePosOnMainPanel(event);
            contextMenu.show(point.xPixels + 3, point.yPixels + 3, "Down", "Right");
        }
        else if (this.jsObject.options.itemInDrag) {
            var itemObject = this.jsObject.CopyObject(this.jsObject.options.itemInDrag.originalItem.itemObject);
            if (!itemObject) return;
            var typeItem = itemObject.typeItem;

            if (typeItem == "Meter") {
                var toIndex = this.getOverItemIndex();
                var fromIndex = this.getItemIndex(this.jsObject.options.itemInDrag.originalItem);
                if (toIndex != null && fromIndex != null && fromIndex != toIndex) {
                    form.sendCommandToTableElement({ command: "MoveMeter", toIndex: toIndex, fromIndex: fromIndex },
                        function (answer) {
                            if (answer.tableElement) {
                                dataContainer.updateMeters(answer.tableElement.meters, toIndex);
                                form.updateTableSvgContent(answer.tableElement.svgContent);
                            }
                        }
                    );

                    var fromIndex = this.getItemIndex(this.jsObject.options.itemInDrag.originalItem);
                }
            }
            else if (typeItem == "Column" || typeItem == "DataSource" || typeItem == "BusinessObject") {
                var draggedItem = {
                    itemObject: itemObject
                };

                if (typeItem == "Column") {
                    var columnParent = this.jsObject.options.dictionaryTree.getCurrentColumnParent();
                    if (columnParent) {
                        draggedItem.currentParentType = columnParent.type;
                        draggedItem.currentParentName = (columnParent.type == "BusinessObject") ? this.jsObject.options.itemInDrag.originalItem.getBusinessObjectFullName() : columnParent.name;
                    }
                }
                else {
                    draggedItem.currentParentType = typeItem;
                    draggedItem.currentParentName = itemObject.name;
                }

                var params = {
                    command: "InsertMeters",
                    draggedItem: draggedItem
                }

                if (typeItem == "Column") {
                    params.insertIndex = dataContainer.getOverItemIndex();
                }

                form.sendCommandToTableElement(params,
                    function (answer) {
                        if (answer.tableElement) {
                            dataContainer.updateMeters(answer.tableElement.meters, params.insertIndex != null ? params.insertIndex : answer.tableElement.meters.length - 1);
                            form.updateTableSvgContent(answer.tableElement.svgContent);
                        }
                    }
                );
            }
        }

        return false;
    }

    dataContainer.oncontextmenu = function (event) {
        return false;
    }

    //Column Types
    var meterTypes = ["Dimension", "Measure", "DataBars", "ColorScale", "Indicator", "Sparklines"];
    var meterTypesTable = this.CreateHTMLTable();
    meterTypesTable.buttons = {};
    for (var i = 0; i < meterTypes.length; i++) {
        var button = this.SmallImageButtonWithBorder(null, null, "Dashboards.Meters." + meterTypes[i] + ".png", this.loc.Dashboard[meterTypes[i]]);
        button.meterType = meterTypes[i];
        button.style.marginRight = "5px";
        meterTypesTable.addCell(button);
        meterTypesTable.buttons[meterTypes[i]] = button;

        button.action = function () {
            this.select();
            var itemIndex = dataContainer.getSelectedItemIndex()

            form.sendCommandToTableElement({ command: "ConvertMeter", itemIndex: itemIndex, meterType: this.meterType},
                function (answer) {
                    if (answer.tableElement) {
                        dataContainer.updateMeters(answer.tableElement.meters, itemIndex);
                        form.updateTableSvgContent(answer.tableElement.svgContent);
                    }
                }
            );
        }

        button.select = function () {
            for (var name in meterTypesTable.buttons) {
                meterTypesTable.buttons[name].setSelected(false);
            }
            this.setSelected(true);
        }
    }
    form.addControlRow(controlsTable, this.loc.PropertyMain.Type, "meterTypesTable", meterTypesTable, "6px 12px 6px 0");

    var expressionControl = this.TableElementExpression(null, 150, null, null);
    form.addControlRow(controlsTable, this.loc.PropertyMain.Expression, "expressionControl", expressionControl, "6px 12px 6px 0");
    expressionControl.menu = contextMenu;
    expressionControl.menu.parentButton = expressionControl.button;

    expressionControl.action = function () {
        form.setPropertyValue("Exspression", Base64.encode(this.textBox.value));
    }
    
    //SparkLines Types
    var sparklinesTypes = ["Line", "Area", "Column", "WinLoss"];
    var sparklinesTable = this.CreateHTMLTable();
    sparklinesTable.buttons = {};
    for (var i = 0; i < sparklinesTypes.length; i++) {
        var button = this.SmallImageButtonWithBorder(null, null, "Dashboards.Meters.Sparklines" + sparklinesTypes[i] + ".png", this.loc.Chart["Sparklines" + sparklinesTypes[i]]);
        button.sparklinesType = sparklinesTypes[i];
        button.style.marginRight = "5px";
        sparklinesTable.addCell(button);
        sparklinesTable.buttons[sparklinesTypes[i]] = button;

        button.action = function () {
            this.select();
            var itemIndex = dataContainer.getSelectedItemIndex()

            form.sendCommandToTableElement({ command: "ChangeSparklinesType", itemIndex: itemIndex, sparklinesType: this.sparklinesType },
                function (answer) {
                    if (answer.tableElement) {
                        dataContainer.updateMeters(answer.tableElement.meters, itemIndex);
                        form.updateTableSvgContent(answer.tableElement.svgContent);
                    }
                }
            );
        }

        button.select = function () {
            for (var name in sparklinesTable.buttons) {
                sparklinesTable.buttons[name].setSelected(false);
            }
            this.setSelected(true);
        }
    }
    form.addControlRow(controlsTable, this.loc.Dashboard.Sparklines, "sparklinesTable", sparklinesTable, "6px 12px 6px 0");

    //HighLowPoints
    var highLowPoints = this.CheckBox(null, this.loc.Dashboard.HighLowPoints);
    form.addControlRow(controlsTable, this.loc.Dashboard.HighLowPoints, "highLowPoints", highLowPoints, "10px 12px 10px 0");

    highLowPoints.action = function () {
        form.setPropertyValue("ShowHighLowPoints", this.isChecked);
    }

    //FirstLastPoints
    var firstLastPoints = this.CheckBox(null, this.loc.Dashboard.FirstLastPoints);
    form.addControlRow(controlsTable, this.loc.Dashboard.FirstLastPoints, "firstLastPoints", firstLastPoints, "10px 12px 10px 0");

    firstLastPoints.action = function () {
        form.setPropertyValue("ShowFirstLastPoints", this.isChecked);
    }

    contextMenu.action = function (menuItem) {
        this.changeVisibleState(false);
        var itemIndex = dataContainer.getSelectedItemIndex();

        if (menuItem.key.indexOf("Function_") == 0) {
            form.sendCommandToTableElement({ command: "SetFunction", itemIndex: itemIndex, function: menuItem.key.replace("Function_", "") },
                function (answer) {
                    if (answer.tableElement) {
                        dataContainer.updateMeters(answer.tableElement.meters, itemIndex);
                        form.updateTableSvgContent(answer.tableElement.svgContent);
                    }
                }
            );
            return;
        }

        switch (menuItem.key) {
            case "removeField": {
                if (dataContainer.selectedItem) dataContainer.selectedItem.remove();
                break;
            }
            case "editExpression": {
                contextMenu.jsObject.InitializeExpressionEditorForm(function (expressionEditorForm) {
                    var propertiesPanel = expressionEditorForm.jsObject.options.propertiesPanel;
                    expressionEditorForm.propertiesPanelZIndex = propertiesPanel.style.zIndex;
                    expressionEditorForm.propertiesPanelIsEnabled = propertiesPanel.isEnabled;
                    expressionEditorForm.resultControl = expressionControl;
                    expressionEditorForm.changeVisibleState(true);
                });
                break;
            }
            case "newDimension":
            case "newMeasure": {
                var insertIndex = itemIndex + 1;
                if (dataContainer.hintText || insertIndex >= dataContainer.childNodes.length) insertIndex = -1;

                form.sendCommandToTableElement({ command: "NewMeter", insertIndex: insertIndex, itemType: menuItem.key },
                    function (answer) {
                        if (answer.tableElement) {
                            dataContainer.updateMeters(answer.tableElement.meters, insertIndex != -1 ? insertIndex : answer.tableElement.meters.length - 1);
                            form.updateTableSvgContent(answer.tableElement.svgContent);
                        }
                    }
                );
                break;
            }
            case "duplicateField": {
                form.sendCommandToTableElement({ command: "DuplicateMeter", itemIndex: itemIndex },
                    function (answer) {
                        if (answer.tableElement) {
                            dataContainer.updateMeters(answer.tableElement.meters, answer.insertIndex);
                            form.updateTableSvgContent(answer.tableElement.svgContent);
                        }
                    }
                );
                break;
            }            
        }
    }

    contextMenu.onshow = function () {
        var items = [];
        items.push(this.jsObject.Item("newDimension", this.jsObject.loc.Dashboard.NewDimension, "Dashboards.Meters.Dimension.png", "newDimension"));
        items.push(this.jsObject.Item("newMeasure", this.jsObject.loc.Dashboard.NewMeasure, "Dashboards.Meters.Measure.png", "newMeasure"));
                        
        if (dataContainer.selectedItem) {
            items.push("separator1");
            items.push(this.jsObject.Item("duplicateField", this.jsObject.loc.Dashboard.DuplicateField, "Duplicate.png", "duplicateField"));
            items.push(this.jsObject.Item("editExpression", this.jsObject.loc.Dashboard.EditExpression, "EditButton.png", "editExpression"));
            items.push(this.jsObject.Item("renameField", this.jsObject.loc.Buttons.Rename, "DataColumn.png", "renameField"));
            items.push(this.jsObject.Item("removeField", this.jsObject.loc.Dashboard.RemoveField, "Remove.png", "removeField"));

            var functions = dataContainer.selectedItem.itemObject.functions;            
            if (functions && functions.length > 0) {
                items.push("separator2");
                for (var i = 0; i < functions.length; i++) {
                    items.push(this.jsObject.Item("Function_" + functions[i], functions[i], "CheckBox.png", "Function_" + functions[i]));
                }
            }
        }

        this.addItems(items);
        
        for (var itemName in this.items) {
            if (itemName.indexOf("Function_") == 0) {
                var funcItem = this.items[itemName];
                var isSelected = dataContainer.selectedItem && dataContainer.selectedItem.itemObject.currentFunction &&
                    itemName.toLowerCase() == ("Function_" + dataContainer.selectedItem.itemObject.currentFunction).toLowerCase();

                funcItem.caption.style.fontWeight = isSelected ? "bold" : "normal";
                funcItem.image.style.visibility = isSelected ? "visible" : "hidden";
            }
        }
    }

    expressionControl.refreshExpressionHint = function () {
        if (meterTypesTable.buttons.Dimension.isSelected)
            expressionControl.textBox.setAttribute("placeholder", "Field");

        else if (meterTypesTable.buttons.Measure.isSelected || meterTypesTable.buttons.DataBars.isSelected || meterTypesTable.buttons.ColorScale.isSelected)
            expressionControl.textBox.setAttribute("placeholder", "Sum(Field)");

        else if (meterTypesTable.buttons.Sparklines.isSelected)
            expressionControl.textBox.setAttribute("placeholder", "Field");

        else if (meterTypesTable.buttons.Indicator.isSelected)
            expressionControl.textBox.setAttribute("placeholder", "Sum(Field) / Sum(Target)");
        else
            expressionControl.textBox.setAttribute("placeholder", "");
    }

    dataContainer.onAction = function () {
        if (dataContainer.selectedItem) {
            var itemObject = dataContainer.selectedItem.itemObject;
            if (meterTypesTable.buttons[itemObject.type]) meterTypesTable.buttons[itemObject.type].select();
            if (itemObject.type == "Sparklines") {
                if (sparklinesTable.buttons[itemObject.sparklinesType]) sparklinesTable.buttons[itemObject.sparklinesType].select();
                highLowPoints.setChecked(itemObject.showHighLowPoints);
                firstLastPoints.setChecked(itemObject.showFirstLastPoints);
            }
            expressionControl.textBox.value = Base64.decode(itemObject.expression);
        }

        form.controls.meterTypesTableRow.style.display = form.controls.expressionControlRow.style.display = dataContainer.selectedItem != null ? "" : "none";
        form.controls.sparklinesTableRow.style.display = dataContainer.selectedItem != null && meterTypesTable.buttons.Sparklines.isSelected ? "" : "none";
        form.controls.highLowPointsRow.style.display = form.controls.firstLastPointsRow.style.display =
            dataContainer.selectedItem != null && meterTypesTable.buttons.Sparklines.isSelected &&
                (sparklinesTable.buttons.Line.isSelected || sparklinesTable.buttons.Area.isSelected) ? "" : "none";

        expressionControl.refreshExpressionHint();
    }

    dataContainer.onRemove = function (itemIndex) {
        form.sendCommandToTableElement({ command: "RemoveMeter", itemIndex: itemIndex },
            function (answer) {
                if (answer.tableElement) {
                    dataContainer.updateMeters(answer.tableElement.meters, dataContainer.getSelectedItemIndex());
                    form.updateTableSvgContent(answer.tableElement.svgContent);
                }
            }
        );
    }

    form.setPropertyValue = function (propertyName, propertyValue) {
        form.sendCommandToTableElement(
            {
                command: "SetPropertyValue",
                propertyName: propertyName,
                propertyValue: propertyValue,
                itemIndex: dataContainer.getSelectedItemIndex()
            },
            function (answer) {
                if (answer.tableElement) {
                    dataContainer.updateMeters(answer.tableElement.meters, dataContainer.getSelectedItemIndex());
                    form.updateTableSvgContent(answer.tableElement.svgContent);
                }
            }
        );
    }

    form.onshow = function () {
        form.currentPanelName = form.jsObject.options.propertiesPanel.getCurrentPanelName();
        form.jsObject.options.propertiesPanel.showContainer("Dictionary");
        dataContainer.clear();
        form.sendCommandToTableElement({ command: "GetTableElementProperties" },
            function (answer) {
                if (answer.tableElement) {
                    dataContainer.updateMeters(answer.tableElement.meters, 0);
                }
            }
        );
    }

    form.onhide = function () {
        form.jsObject.options.propertiesPanel.showContainer(form.currentPanelName);
    }

    form.sendCommandToTableElement = function (updateParameters, callbackFunction) {
        updateParameters.zoom = form.jsObject.options.report.zoom.toString();

        form.jsObject.SendCommandToDesignerServer("UpdateTableElement",
            {
                componentName: form.currentTableElement.properties.name,
                updateParameters: updateParameters
            },
            function (answer) {
                callbackFunction(answer);
            });
    }

    form.updateTableSvgContent = function (svgContent) {
        this.currentTableElement.properties.svgContent = svgContent;
        this.currentTableElement.repaint();
    }

    return form;
}