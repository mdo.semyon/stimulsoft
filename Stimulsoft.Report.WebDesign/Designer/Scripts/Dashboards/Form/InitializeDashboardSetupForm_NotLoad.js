﻿
StiMobileDesigner.prototype.InitializeDashboardSetupForm_ = function () {
    var form = this.DashboardBaseForm("dashboardSetup", this.loc.Components.StiDashboard, 1);
    form.buttonsSeparator.style.display = "none";
    form.buttonsPanel.style.display = "none";
    form.container.style.borderTop = "0px";
    form.caption.style.padding = "0px 10px 0 12px";
    var jsObject = this;

    var controlsTable = this.CreateHTMLTable();
    form.container.appendChild(controlsTable);
    form.container.style.padding = "0 0 6px 0";

    var widthControl = this.TextBox(null, 100);
    form.addControlRow(controlsTable, this.loc.PropertyMain.Width, "width", widthControl, "6px 30px 6px 12px");
    widthControl.action = function () {
        this.value = Math.abs(this.jsObject.StrToDouble(this.value));
        this.jsObject.options.currentPage.properties.unitWidth = this.value;
        this.jsObject.SendCommandSendProperties(this.jsObject.options.currentPage, ["unitWidth"]);
    }

    var heightControl = this.TextBox(null, 100);
    form.addControlRow(controlsTable, this.loc.PropertyMain.Height, "height", heightControl, "6px 30px 6px 12px");
    heightControl.action = function () {
        this.value = Math.abs(this.jsObject.StrToDouble(this.value));
        this.jsObject.options.currentPage.properties.unitHeight = this.value;
        this.jsObject.SendCommandSendProperties(this.jsObject.options.currentPage, ["unitHeight"]);
    }

    form.onshow = function () {
        var currentPage = this.jsObject.options.currentPage;
        widthControl.value = currentPage.properties.unitWidth;
        heightControl.value = currentPage.properties.unitHeight;
        widthControl.focus();
    }

    return form
}