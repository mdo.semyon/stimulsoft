﻿
StiMobileDesigner.prototype.InitializeEditImageElementForm_ = function () {
    var form = this.DashboardBaseForm("editImageElementForm", this.loc.Components.StiImage, 1);
    form.isDockableToComponent = true;
    form.buttonsSeparator.style.display = "none";
    form.buttonsPanel.style.display = "none";
    form.caption.style.padding = "0px 10px 0 12px";
    form.container.style.borderTop = "0px";

    var imageSrcContainer = this.ImageControl(null, 203, 200);
    imageSrcContainer.style.margin = "12px";
    form.container.appendChild(imageSrcContainer);

    var hLinkText = document.createElement("div")
    hLinkText.innerHTML = this.loc.PropertyMain.Hyperlink;
    hLinkText.className = "stiDesignerTextContainer";
    hLinkText.style.padding = "4px 0 4px 12px";
    form.container.appendChild(hLinkText);

    var hLinkControl = this.ExpressionControl(null, 200, null, false);
    hLinkControl.style.margin = "0 12px 12px 12px";
    form.container.appendChild(hLinkControl);

    hLinkControl.action = function () {
        form.setPropertyValue("imageUrl", Base64.encode(this.textBox.value));
    }

    imageSrcContainer.action = function () {
        form.setPropertyValue("imageSrc", this.src);
    }

    form.onshow = function () {
        imageSrcContainer.setImage(form.currentImageElement.properties.imageSrc);
        hLinkControl.textBox.value = Base64.decode(form.currentImageElement.properties.imageUrl);
    }

    form.setPropertyValue = function (propertyName, propertyValue) {
        form.sendCommandToImageElement(
            {
                command: "SetPropertyValue",
                propertyName: propertyName,
                propertyValue: propertyValue
            },
            function (answer) {
                if (answer.imageElement) {
                    for (var propertyName in answer.imageElement) {
                        form.currentImageElement.properties[propertyName] = answer.imageElement[propertyName];
                    }
                    form.currentImageElement.repaint();
                }
            }
        );
    }

    form.sendCommandToImageElement = function (updateParameters, callbackFunction) {
        form.jsObject.SendCommandToDesignerServer("UpdateImageElement",
            {
                componentName: form.currentImageElement.properties.name,
                updateParameters: updateParameters
            },
            function (answer) {
                callbackFunction(answer);
            });
    }

    return form;
}
