﻿
StiMobileDesigner.prototype.InitializeEditIndicatorElementForm_ = function () {
    var form = this.DashboardBaseForm("editIndicatorElementForm", this.loc.Components.StiIndicator, 1);
    form.isDockableToComponent = true;
    form.buttonsSeparator.style.display = "none";
    form.buttonsPanel.style.display = "none";
    form.container.style.borderTop = "0px";
    form.caption.style.padding = "0px 10px 0 12px";
    var jsObject = this;

    var controlsTable = this.CreateHTMLTable();
    form.container.appendChild(controlsTable);
    form.container.style.padding = "0 0 6px 0";

    //DataColumns
    var dataColumns = [
        ["value", this.loc.PropertyMain.Value],
        ["target", this.loc.PropertyMain.Target],
        ["series", this.loc.PropertyMain.Series]
    ];

    for (var i = 0; i < dataColumns.length; i++) {
        var container = this.IndicatorDataColumnContainer(form, dataColumns[i][0], dataColumns[i][1]);
        container.allowSelected = true;
        form.addControlRow(controlsTable, null, dataColumns[i][0] + "DataColumn", container, "0px 12px " + (i == dataColumns.length - 1 ? "6px" : "0px" + " 12px"));

        container.action = function () {
            if (!this.dataColumnObject) {
                form.controls.expression.currentContainer = null;
                form.controls.expression.setEnabled(false);
            }
            form.applyDataColumnPropertyToIndicatorElement(this);
        }

        container.onSelected = function () {
            for (var i = 0; i < dataColumns.length; i++) {
                if (form.controls[dataColumns[i][0] + "DataColumn"] != this)
                    form.controls[dataColumns[i][0] + "DataColumn"].setSelected(false);
            }

            if (this.dataColumnObject && this.dataColumnObject.expression != null) {
                form.controls.expression.currentContainer = this;
                form.controls.expression.setEnabled(true);
                form.controls.expression.textBox.value = Base64.decode(this.dataColumnObject.expression);
            }
        }
    }

    var parentValueContainer = form.controls.valueDataColumn.innerContainer.parentElement;

    var contextMenu = this.options.menus.indicatorElementFormContextMenu || this.BaseContextMenu("indicatorElementFormContextMenu", "Down", null, this.GetStyles("MenuStandartItem"));
        
    var expressionControl = this.TableElementExpression(null, 160, null, null);
    form.addControlRow(controlsTable, this.loc.PropertyMain.Expression, "expression", expressionControl, "12px 12px 6px 12px");
    form.controls.expressionText.style.paddingTop = "6px";
    expressionControl.menu = contextMenu;
    expressionControl.menu.parentButton = expressionControl.button;

    expressionControl.action = function () {
        if (this.currentContainer) {
            form.applyExpressionPropertyToIndicatorElement(this.currentContainer, this.textBox.value);
        }
    }

    contextMenu.action = function (menuItem) {
        this.changeVisibleState(false);
        var currentContainer = expressionControl.currentContainer;

        if (menuItem.key.indexOf("Function_") == 0) {
            form.sendCommandToIndicatorElement(
                {
                    command: "SetFunction",
                    containerName: jsObject.UpperFirstChar(currentContainer.name),
                    function: menuItem.key.replace("Function_", "")
                },
                function (answer) {
                    if (answer.indicatorElement) {
                        form.updateControls(answer.indicatorElement);
                        form.updateIndicatorSvgContent(answer.indicatorElement.svgContent);
                    }
                }
            );
            return;
        }

        switch (menuItem.key) {
            case "removeField": {
                currentContainer.clear();
                currentContainer.action();
                break;
            }
            case "editExpression": {
                contextMenu.jsObject.InitializeExpressionEditorForm(function (expressionEditorForm) {
                    var propertiesPanel = expressionEditorForm.jsObject.options.propertiesPanel;
                    expressionEditorForm.propertiesPanelZIndex = propertiesPanel.style.zIndex;
                    expressionEditorForm.propertiesPanelIsEnabled = propertiesPanel.isEnabled;
                    expressionEditorForm.resultControl = expressionControl;
                    expressionEditorForm.changeVisibleState(true);
                });
                break;
            }
            case "newItem": {
                form.sendCommandToIndicatorElement(
                    {
                        command: "NewItem",
                        containerName: jsObject.UpperFirstChar(currentContainer.name)
                    },
                    function (answer) {
                        if (answer.indicatorElement) {
                            form.updateControls(answer.indicatorElement);
                            form.updateIndicatorSvgContent(answer.indicatorElement.svgContent);
                            form.checkStartMode();
                        }
                    }
                );
                break;
            }
        }
    }

    contextMenu.onshow = function () {
        var currentContainer = expressionControl.currentContainer;
        var items = [];
        items.push(this.jsObject.Item("newItem", this.jsObject.loc.FormDictionaryDesigner.NewItem, "Empty16.png", "newItem"));

        if (expressionControl.isEnabled && currentContainer && currentContainer.isSelected) {
            items.push("separator1");
            items.push(this.jsObject.Item("editExpression", this.jsObject.loc.Dashboard.EditExpression, "EditButton.png", "editExpression"));
            items.push(this.jsObject.Item("renameField", this.jsObject.loc.Buttons.Rename, "DataColumn.png", "renameField"));
            items.push(this.jsObject.Item("removeField", this.jsObject.loc.Dashboard.RemoveField, "Remove.png", "removeField"));

            if (currentContainer.dataColumnObject) {
                var functions = currentContainer.dataColumnObject.functions;
                if (functions && functions.length > 0) {
                    items.push("separator2");
                    for (var i = 0; i < functions.length; i++) {
                        items.push(this.jsObject.Item("Function_" + functions[i], functions[i], "CheckBox.png", "Function_" + functions[i]));
                    }
                }
            }
        }

        this.addItems(items);

        for (var itemName in this.items) {
            if (itemName.indexOf("Function_") == 0) {
                var funcItem = this.items[itemName];
                var isSelected = currentContainer.dataColumnObject && currentContainer.dataColumnObject.currentFunction &&
                    itemName.toLowerCase() == ("Function_" + currentContainer.dataColumnObject.currentFunction).toLowerCase();

                funcItem.caption.style.fontWeight = isSelected ? "bold" : "normal";
                funcItem.image.style.visibility = isSelected ? "visible" : "hidden";
            }
        }
    }

    form.setValues = function () {
        expressionControl.textBox.value = "";
        expressionControl.setEnabled(false);

        var meters = this.indicatorProperties.meters;
        
        var meterTypes = ["Value", "Target", "Series"];
        for (var i = 0; i < meterTypes.length; i++) {
            var meter = meters["Sti" + meterTypes[i] + "IndicatorMeter"];
            var container = this.controls[jsObject.LowerFirstChar(meterTypes[i]) + "DataColumn"];

            if (meter) {
                container.addColumn(meter.label, meter);
                if (container.isSelected && container.item) container.item.action();
            }
            else
                container.clear();
        }
    }

    form.checkStartMode = function () {
        var itemsCount = 0;
        for (var i = 0; i < dataColumns.length; i++) {
            var container = form.controls[dataColumns[i][0] + "DataColumn"];
            if (container.dataColumnObject) itemsCount++;
        }

        var valueContainer = form.controls.valueDataColumn.innerContainer;

        if (itemsCount == 0) {
            form.container.appendChild(valueContainer);
            controlsTable.style.display = "none";
            valueContainer.style.height = valueContainer.style.maxHeight = "260px";
            valueContainer.style.width = "267px";
            valueContainer.style.margin = "6px 12px 6px 12px";
        }
        else {
            parentValueContainer.appendChild(valueContainer);
            controlsTable.style.display = "";
            valueContainer.style.height = "30px";
            valueContainer.style.width = "auto";
            valueContainer.style.margin = "0";
        }
    }

    form.onshow = function () {
        form.currentPanelName = form.jsObject.options.propertiesPanel.getCurrentPanelName();
        form.jsObject.options.propertiesPanel.showContainer("Dictionary");
        expressionControl.textBox.value = "";
        expressionControl.setEnabled(false);
        var meterTypes = ["Value", "Target", "Series"];
        for (var i = 0; i < meterTypes.length; i++) {
            var container = this.controls[jsObject.LowerFirstChar(meterTypes[i]) + "DataColumn"];
            container.clear();
        }
        form.checkStartMode();

        form.sendCommandToIndicatorElement({ command: "GetIndicatorElementProperties" },
            function (answer) {
                if (answer.indicatorElement) {
                    form.updateControls(answer.indicatorElement);
                    form.checkStartMode();
                }
            }
        );
    }

    form.onhide = function () {
        form.jsObject.options.propertiesPanel.showContainer(form.currentPanelName);
    }

    form.updateControls = function (indicatorElement) {
        form.indicatorProperties = indicatorElement;
        form.setValues();
    }

    form.applyExpressionPropertyToIndicatorElement = function (container, expressionValue) {
        if (container) {
            form.sendCommandToIndicatorElement(
                {
                    command: "SetExpression",
                    containerName: form.jsObject.UpperFirstChar(container.name),
                    expressionValue: Base64.encode(expressionValue)
                },
                function (answer) {
                    if (answer.indicatorElement) {
                        form.updateControls(answer.indicatorElement);
                        form.updateIndicatorSvgContent(answer.indicatorElement.svgContent);
                    }
                }
            );
        }
    }

    form.applyDataColumnPropertyToIndicatorElement = function (container) {
        form.sendCommandToIndicatorElement(
            {
                command: "SetDataColumn",
                containerName: form.jsObject.UpperFirstChar(container.name),
                dataColumnObject: container.dataColumnObject
            },
            function (answer) {
                if (answer.indicatorElement) {
                    form.updateControls(answer.indicatorElement);
                    form.updateIndicatorSvgContent(answer.indicatorElement.svgContent);
                    if (container.item) container.item.action();
                    form.checkStartMode();
                }
            }
        );
    }

    form.sendCommandToIndicatorElement = function (updateParameters, callbackFunction) {
        updateParameters.zoom = form.jsObject.options.report.zoom.toString();

        form.jsObject.SendCommandToDesignerServer("UpdateIndicatorElement",
            {
                componentName: form.currentIndicatorElement.properties.name,
                updateParameters: updateParameters
            },
            function (answer) {
                callbackFunction(answer);
            });
    }

    form.updateIndicatorSvgContent = function (svgContent) {
        this.currentIndicatorElement.properties.svgContent = svgContent;
        this.currentIndicatorElement.repaint();
    }

    return form;
}

StiMobileDesigner.prototype.IndicatorDataColumnContainer = function (form, name, headerText, width, showItemImage) {
    var container = this.DataColumnContainer(name, headerText, width, showItemImage);
    var innerContainer = container.innerContainer;
    var jsObject = this;

    innerContainer.oldonmouseup = innerContainer.onmouseup;

    innerContainer.onmouseup = function (event) {
        if (innerContainer.canInsert()) {
            var originalItem = jsObject.options.itemInDrag.originalItem;
            var itemObject = jsObject.CopyObject(originalItem.itemObject);
            if (originalItem.closeButton && originalItem.closeButton.clicked) return;

            if (itemObject.typeItem == "Meter") {
                var fromContainerName = jsObject.UpperFirstChar(originalItem.container.name);
                var toContainerName = jsObject.UpperFirstChar(container.name);

                if (toContainerName != fromContainerName) {
                    form.sendCommandToIndicatorElement(
                        {
                            command: "MoveMeter",
                            toContainerName: toContainerName,
                            fromContainerName: fromContainerName
                        },
                        function (answer) {
                            form.updateControls(answer.indicatorElement);
                            form.updateIndicatorSvgContent(answer.indicatorElement.svgContent);
                            if (container.item) container.item.action();
                        });
                }
            }
            else {
                innerContainer.oldonmouseup(event);
            }
        }
    }

    return container;
}
