﻿
StiMobileDesigner.prototype.VerticalMenuItemForDashboardStyles = function (menu, styleObject) {
    var item = this.VerticalMenuItem(menu, styleObject.ident, styleObject.ident, null, styleObject.ident, this.GetStyles("MenuBigItem"));

    item.style.height = "auto";
    item.style.overflow = "hidden";
    item.caption.style.padding = "8px";
    item.caption.innerHTML = "";

    item.styleContainer = document.createElement("div");
    item.caption.appendChild(item.styleContainer);

    var styleTable = this.CreateHTMLTable();
    styleTable.style.width = "120px";
    item.styleContainer.appendChild(styleTable);

    var sampleText = styleTable.addTextCell("Aa");
    sampleText.style.height = "22px";
    sampleText.style.fontSize = "18px";
    sampleText.style.textAlign = "center";
    sampleText.style.color = styleObject.brush;
    sampleText.style.border = "1px solid " + styleObject.brush;

    var identText = styleTable.addTextCellInNextRow(styleObject.ident);
    identText.style.height = "18px";
    identText.style.fontSize = "12px";
    identText.style.textAlign = "center";
    identText.style.color = "#ffffff";
    identText.style.background = styleObject.brush;

    return item;
}

StiMobileDesigner.prototype.VerticalMenuItemForTableOrPivotElementsStyles = function (menu, styleObject, isPivotElement) {
    var item = this.VerticalMenuItem(menu, styleObject.ident, styleObject.ident, null, styleObject.ident, this.GetStyles("MenuBigItem"));

    item.style.height = "auto";
    item.style.overflow = "hidden";
    item.caption.style.padding = "8px";
    item.caption.innerHTML = "";

    item.styleContainer = document.createElement("div");
    item.caption.appendChild(item.styleContainer);

    if (styleObject.ident == "Auto") {
        item.styleContainer.innerHTML = "Auto"
        item.styleContainer.style.fontSize = "12px";
        item.styleContainer.style.border = "1px dashed #c6c6c6";
        item.styleContainer.style.padding = "10px";
        item.styleContainer.style.textAlign = "center";
    }
    else {
        var styleTable = this.CreateHTMLTable();        
        styleTable.style.width = "119px";
        styleTable.style.border = "1px solid #c0c0c0";
        styleTable.style.borderRight = "0px";
        item.styleContainer.appendChild(styleTable);

        for (var i = 0; i < 5; i++) {            
            for (var k = 0; k < 3; k++) {
                var cell = styleTable.addCellInLastRow();
                cell.style.height = "10px";
                cell.style.borderRight = "1px solid #c0c0c0";
                cell.style.background = i == 0 || (isPivotElement && k == 0)
                    ? "#ededed"
                    : (i == 1 || i == 3)
                        ? "#ffffff"
                        : styleObject.brushLight;

                if (isPivotElement && k == 0 && i < 4) {
                    cell.style.borderBottom = "1px solid #c0c0c0";
                }
            }
            if (i < 4) styleTable.addRow();
        }
    }

    return item;
}

StiMobileDesigner.prototype.VerticalMenuItemForMapElementStyles = function (menu, styleObject) {
    var item = this.VerticalMenuItem(menu, styleObject.ident, styleObject.ident, null, styleObject.ident, this.GetStyles("MenuBigItem"));
    item.style.height = "auto";
    item.style.overflow = "hidden";
    item.caption.style.padding = "4px";
    item.caption.innerHTML = "";

    item.styleContainer = document.createElement("div");
    item.caption.appendChild(item.styleContainer);

    if (styleObject.ident == "Auto") {
        item.styleContainer.innerHTML = "Auto"
        item.styleContainer.style.fontSize = "12px";
        item.styleContainer.style.border = "1px dashed #c6c6c6";
        item.styleContainer.style.padding = "10px";
        item.styleContainer.style.textAlign = "center";
    }
    else {
        item.styleContainer.innerHTML = styleObject.image;
    }

    return item;
}

StiMobileDesigner.prototype.VerticalMenuItemForChartElementStyles = function (menu, styleObject) {
    var item = this.VerticalMenuItemForMapElementStyles(menu, styleObject);
    if (item.caption && styleObject.ident != "Auto") {
        item.caption.style.padding = "0";
    }

    return item;
}

StiMobileDesigner.prototype.VerticalMenuItemForGaugeElementStyles = function (menu, styleObject) {
    var item = this.VerticalMenuItemForMapElementStyles(menu, styleObject);

    return item;
}

StiMobileDesigner.prototype.VerticalMenuItemForProgressElementStyles = function (menu, styleObject) {
    var item = this.VerticalMenuItemForMapElementStyles(menu, styleObject);

    return item;
}

StiMobileDesigner.prototype.VerticalMenuItemForIndicatorElementStyles = function (menu, styleObject) {
    var item = this.VerticalMenuItemForMapElementStyles(menu, styleObject);

    return item;
}