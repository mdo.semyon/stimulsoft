
function StiMobileDesigner(parameters) {    
    this.defaultParameters = {};
    this.options = parameters;
    this.options.buttons = {};
    this.options.controls = {};
    this.options.menus = {};
    this.options.forms = {};
    this.options.radioButtons = {};
    this.options.callbackFunctions = {};
    this.options.openDialogs = {};
    this.options.properties = {};
    this.options.propertiesGroups = {};
    this.options.dataBasesTreeOpeningArray = {};
    this.options.dataBasesTreeOpeningArrayTemp = {};
    this.options.paintPanelPadding = 15;
    this.options.previewPageNumber = 0;
    this.options.previewCountPages = 0;
    this.options.commands = [];
    this.options.touchZoom = {};
    this.options.startZoom = 0;
    this.options.oldDeltaPos = 0;
    this.options.timeUpdateCache = 180000;
    this.options.modifyRestrictions = true;
    this.options.mobileDesigner = document.getElementById(parameters.mobileDesignerId);
    this.options.head = document.getElementsByTagName("head")[0];
    this.options.supportTouchInterface = this.IsTouchDevice();
    this.options.isTouchDevice = parameters.interfaceType == "Auto"
        ? (this.GetCookie("StimulsoftMobileDesignerInterfaceType")
            ? this.options.supportTouchInterface && this.GetCookie("StimulsoftMobileDesignerInterfaceType") == "Touch"
            : this.options.supportTouchInterface)
        : (parameters.interfaceType == "Touch" || parameters.interfaceType == "Mobile");
    this.options.canOpenFiles = window.File && window.FileReader && window.FileList && window.Blob;
    this.options.menuAnimDuration = parameters.showAnimation ? 150 : 0;
    this.options.formAnimDuration = parameters.showAnimation ? 200 : 0;
    this.options.xOffset = parameters.focusingX ? 0 : 0.5;
    this.options.yOffset = parameters.focusingY ? 0 : 0.5;
    this.options.containers = {};
    this.options.designerIsFocused = true;
    this.options.fontSizes = this.GetFontSizes();
    this.options.monthesCollection = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    this.options.dayOfWeekCollection = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    this.options.themeColors = { Blue: "#19478a", Carmine: "#912c2f", Green: "#0b6433", Orange: "#b73a1c", Purple: "#8653a5", Teal: "#23645c", Violet: "#6d3069" };
    this.options.propertyControlWidth = this.options.propertiesGridWidth - this.options.propertiesGridLabelWidth - 35;
    this.options.propertyNumbersControlWidth = Math.max(this.options.propertyControlWidth - 100, 40);
    this.options.showPanelPropertiesAndDictionary = this.options.showDictionary || this.options.showPropertiesGrid || this.options.showReportTree;    
    if (!this.options.requestTimeout) this.options.requestTimeout = 20;
    if (this.options.fullScreenMode) this.options.mobileDesigner.style.zIndex = "10000";

    var setupToolboxCookie = this.GetCookie("StimulsoftMobileDesignerSetupToolbox");
    var setupToolbox = setupToolboxCookie ? JSON.parse(setupToolboxCookie) : null;
    this.options.showToolbox = setupToolbox ? setupToolbox.showToolbox : true;
    this.options.showInsertTab = setupToolbox ? setupToolbox.showInsertTab : true;
    this.options.publishUrl = "https://publish.stimulsoft.com/";

    //Dashboards options
    this.options.dashboardElementTitleHeight = 30;

    if (parameters.loc) {
        var loc = JSON.parse(parameters.loc);
        this.loc = loc.Localization || loc;
        delete this.options.loc;
    }
    else {
        alert("js scripts complete!");
        return;
    }

    if (this.options.cloudMode && this.options.cloudParameters) {
        if (this.options.cloudParameters.isTouchDevice != null) {
            this.options.isTouchDevice = this.options.cloudParameters.isTouchDevice == "true";
        }

        var title = this.loc.FormDesigner.title;
        if (this.options.cloudParameters.reportName) title = this.options.cloudParameters.reportName + " - " + title;
        this.SetWindowTitle(title);

        if (this.options.cloudParameters.isOnlineVersion) {
            this.options.isOnlineVersion = true;
            this.options.reportResourcesMaximumSize = this.options.productVersion == this.options.productVersion.trim()
                ? this.options.cloudParameters.maxResourceSizeTr
                : this.options.cloudParameters.maxResourceSizeDeveloper;
            this.options.reportResourcesMaximumCount = 5;
        }

        var requestChangesCookie = this.GetCookie("StimulsoftMobileDesignerRequestChangesWhenSaving");
        this.options.requestChangesWhenSaving = requestChangesCookie == null || requestChangesCookie == "true";
    }

    // Load designer styles
    if (!this.options.jsMode) this.LoadStyle(this.options.stylesUrl);

    // Data Tree
    this.options.dataTree = this.DataTree();
    this.options.mobileDesigner.jsObject = this;
    this.options.mainPanel = document.getElementById(this.options.mobileDesigner.id + "_MainPanel");

    if (parameters.scrollbiParameters && parameters.scrollbiParameters.errorMessage) {
        var errorMessageForm = this.options.forms.errorMessageForm || this.InitializeErrorMessageForm();
        errorMessageForm.show(parameters.scrollbiParameters.errorMessage);
    }
                    
    var jsObject = this;
    this.CreateMetaTag();
    this.InitializeDesigner();        
    this.InitializeToolBar();
    this.InitializeWorkPanel();
    this.InitializeHomePanel();
    this.InitializeStatusPanel();    
    this.InitializePropertiesPanel();
    this.InitializePagesPanel();
    this.InitializePaintPanel();
    this.InitializeToolbox();
    this.InitializeToolTip();
    if (this.options.jsMode) this.InitializePreviewPanel();    
    if (this.options.cloudMode) this.InitializeLoginControls();

    this.SetEnabledAllControls(false);

    this.addEvent(document, 'mousemove', function (event) {
        jsObject.DocumentMouseMove(event);
    });

    this.addEvent(document, 'touchmove', function (event) {
        jsObject.DocumentTouchMove(event);
    });
        
    this.addEvent(document, 'touchend', function (event) {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);

        jsObject.DocumentTouchEnd(event);

        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    });

    this.addEvent(document, 'mouseup', function (event) {
        if (this.isTouchEndTimer) return;
        jsObject.DocumentMouseUp(event);
    });
        
    //Load Report
    if (this.options.jsMode) {
        jsObject.CloseReport();
    }
    else {
        var processImage = this.InitializeProcessImage();
        processImage.show();
        
        if (document.readyState == 'complete') {
            jsObject.BuildDesignerComplete();
        }
        else if (window.addEventListener) {
            window.addEventListener('load', function () { jsObject.BuildDesignerComplete(); });
        }
        else {
            window.attachEvent('onload', function () { jsObject.BuildDesignerComplete(); });
        }
    }
}

StiMobileDesigner.prototype.mergeOptions = function (fromObject, toObject) {
    for (var value in fromObject) {
        if (toObject[value] === undefined || typeof toObject[value] !== "object") toObject[value] = fromObject[value];
        else this.mergeOptions(fromObject[value], toObject[value]);
    }
}

StiMobileDesigner.prototype.BuildDesignerComplete = function () {
    var jsObject = this;    
    var params = { defaultUnit: this.options.defaultUnit };

    if (this.options.cloudParameters) {
        params.attachedItems = this.options.cloudParameters.attachedItems || [];
        params.resourceItems = this.options.cloudParameters.resourceItems || [];
        params.sessionKey = this.options.cloudParameters.sessionKey;
        params.reportTemplateItemKey = this.options.cloudParameters.reportTemplateItemKey;  
    }

    this.SendCommandToDesignerServer("GetReportForDesigner", params, function (answer) {
        if (answer.reportObject) jsObject.LoadReport(jsObject.ParseReport(answer.reportObject));

        if ((jsObject.options.cloudParameters && jsObject.options.cloudParameters.thenOpenWizard) || jsObject.options.runWizardAfterLoad) {
            var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
            fileMenu.changeVisibleState(true);
        }
    });
    
    // Update images array
    this.PostAjax(this.options.requestUrl, { command: "UpdateImagesArray" }, this.receveFromServer);
    
    // Load all scripts
    this.LoadScript(this.options.scriptsUrl + "AllNotLoadedScripts");

    //append font for pictorial chart
    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
                    @font-face {\
                                font-family: 'Stimulsoft';\
                                src: url(data:font/ttf;base64,AAEAAAALAIAAAwAwT1MvMg8SB94AAAC8AAAAYGNtYXB84HyCAAABHAAAAKxnYXNwAAAAEAAAAcgAAAAIZ2x5ZnsqufkAAAHQAAAcDGhlYWQPpKc5AAAd3AAAADZoaGVhB8UD/AAAHhQAAAAkaG10eOjAB1QAAB44AAAA9GxvY2G5XMAMAAAfLAAAAHxtYXhwAEoAgQAAH6gAAAAgbmFtZSWAbToAAB/IAAABqnBvc3QAAwAAAAAhdAAAACAAAwPyAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADqwgPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAkAAAACAAIAAEAAAAAQAg6SrpOulC6Vjpdumi6bDpyune6r7qwOrC//3//wAAAAAAIOkA6TrpQulY6Xbpoumv6crp3Oq+6sDqwv/9//8AAf/jFwQW9RbuFtkWvBaRFoUWbBZbFXwVexV6AAMAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAf//AA8AAQAAAAAAAAAAAAIAADc5AQAAAAABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAA/8AEAAPAAAYAAAERIQkBIREBIP7gAgACAP7gA8D+AP4AAgACAAAAAAABAAD/wAQAA8AABgAAEyERCQERIQACAAIA/gD+AAKgASD+AP4AASAAAQAA/8AEAAPAAAcAABMBByERBwkBAAGk7ANI7f5d/pACUP5d7QNI7AGk/pAAAAAAAQAA/8AEAAPAAAcAABMBJyERJwkBAAGk7ANI7f5d/pABMAGk7Py47P5cAXAAAAAAAQAA/8AEAAPAAAYAABMhESERIQEAASABwAEg/gABwP4AAgACAAAAAAEABwACBAIDcwAeAAA3JzcXHgEfAQE2Nz4BNzY3NhYfAQEGBw4BBwYxIiYnvrdXJA9UK2oBEzkyM0wWFwEDER0s/rxDOztZGhoBbUy/vlwlD1YubQFcSEBAYB0dAQUNHi7+cVJISWwgH29OAAEAAP/ABAADwAAbAAABFAcOAQcGIyInLgEnJjU0Nz4BNzYzMhceARcWBAAoKIteXWpqXV6LKCgoKIteXWpqXV6LKCgBwGpdXosoKCgoi15dampdXosoKCgoi15dAAAAAAIAAP/ABAADwAAdACMAAAEiBw4BBwYVFBceARcWMzI3PgE3NjUxNCcuAScmIwMnNxcBFwIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qPudGoQFCRQPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/NvlRaABhEQAAAIAAP/ABAADwAAdACkAAAEiBw4BBwYVFBceARcWMzI3PgE3NjUxNCcuAScmIwEHJwcnNyc3FzcXBwIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qASVK29tK3NxK29tK3APAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/SVK3NxK29tK3NxK2wADAAD/wAQAA8AAHQArAEEAAAEiBw4BBwYVFBceARcWMzI3PgE3NjUxNCcuAScmIxEiJjU0NjMyFhUxFAYjNxQGIyImNTE0Jy4BJyYxNDYzMhYVMQIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qHisrHh4rKx4lFg8PFgUGDgUGKx4eKwPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/LcrHh4rKx4eK9sPFRUPDjs7gjQ0HisrHgAAAQAA/8AEAAPAAAwAABM3CQEXCQEHCQEnCQEAkgFuAW6S/pIBbpL+kv6SkgFu/pIDLpL+kgFukv6S/pKSAW7+kpIBbgFuAAAAAgGO/8ACcgPAAAsAIgAAJRQGIyImNTQ2MzIWJyImNTE0Jy4BJyYxNDYzMhYVMQMUBiMCckMvL0NDLy9DchAWDAwcDAxDLy9DTBYQMi9DQy8vQ0OOFw8PWlvOVFMvQ0Mv/ccPFwAAAAABAFP/wAOtA8AAIAAAARMHAT4BNzMyFx4BFxYXFhceARcWOwEwBw4BBwYjIiYjATrnY/6VLXZDATEqK0geHxoaHyBKKysyIwUFPkNDfGNjYwIT/c4hA50rNAQODisZGBYVGxswEBAfH0sfHkIAAAAAAQEU/8AC7APAACMAAAE1IRUwBw4BBwYVETAXHgEXFjMVITUwNz4BNzY1ETAnLgEnJgEUAdgYGTsZGAIDHSAgO/4oGBk7GRgCAx0gIANxT08CAx0gIDv92BgZOxkYT08CAx0gIDsCKBgZOxkYAAAAAAIAdv/AA4oDwAAjAEcAAAE1IRUwBw4BBwYVETAXHgEXFjMVITUwNz4BNzY1ETAnLgEnJiE1IRUwBw4BBwYVETAXHgEXFjMVITUwNz4BNzY1ETAnLgEnJgGxAdkZGDsZGQMCHiAgO/4nGRg8GBkDAh4gIP6KAdkZGDwYGQMCHiAgO/4nGRg7GRkDAh4gIANxT08CAx0gIDv92BgZOxkYT08CAx0gIDsCKBgZOxkYT08CAx0gIDv92BgZOxkYT08CAx0gIDsCKBgZOxkYAAMAAP/lBAADmwATACgAPAAAATUhFTAGFREwFjMVITUwNjURMCYhNSEVMAYVETAWMxUhNTA2NREwJiMhNSEVMAYVETAWMxUhNTA2NREwJgJJAbeSJG7+SZIk/m4BtpIlbf5KkiVt/tsBt5Ikbv5JkiQDUklJJG7+AJJJSSRuAgCSSUkkbv4AkklJJG4CAJJJSSRu/gCSSUkkbgIAkgAAAAADAAAAIAQAA2AAEwAuADIAABM1IRUwBhURMBYzFSE1MDY1ETAmITUhFQ4BBxUDIwM1MxUwBhcWFx4BFxYxEzA0JRUzNQABgIAgYP6AgCACYAFAOlgOoIDA4DgYDBARHgoLgP4ggAMgQEAgYP5AgEBAIGABwIBAQAFINgH9gAMAQEAoeDxXWKI5OgIggEBAQAABAAABLgQAAlIABAAAExEhESEABAD8AAJS/twBJAAAAAEAAP/ABAADwAAbAAABFAcOAQcGIyInLgEnJjU0Nz4BNzYzMhceARcWBAAoKIteXWpqXV6LKCgoKIteXWpqXV6LKCgBwGpdXosoKCgoi15dampdXosoKCgoi15dAAAAAAMAAP/ABAADwAAdADsASwAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTE0Jy4BJyYjESInLgEnJjU0Nz4BNzYzMhceARcWFTEUBw4BBwYjGQEyNz4BNzY1NCcuAScmIwIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qY1hXgyUmJiWDV1hjY1hXgyUmJiWDV1hjal1eiygoKCiLXl1qA8AoKIteXWpqXV6LKCgoKIteXWpqXV6LKCj8ICYlg1dYY2NYV4MlJiYlg1dYY2NYV4MlJgPg/AAoKIteXWpqXV6LKCgAAgAA/8AEAAPAAB0AOwAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTE0Jy4BJyYjESInLgEnJjU0Nz4BNzYzMhceARcWFTEUBw4BBwYjAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXWpjWFeDJSYmJYNXWGNjWFeDJSYmJYNXWGMDwCgoi15dampdXosoKCgoi15dampdXosoKPwgJiWDV1hjY1hXgyUmJiWDV1hjY1hXgyUmAAAAAwAA/8AEAAPAAB0AOwBFAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1MTQnLgEnJiMRIicuAScmNTQ3PgE3NjMyFx4BFxYVMRQHDgEHBiMZASE0Jy4BJyYjAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXWpjWFeDJSYmJYNXWGNjWFeDJSYmJYNXWGMCACgoi15dagPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/CAmJYNXWGNjWFeDJSYmJYNXWGNjWFeDJSYD4P4Aal1eiygoAAMAAP/ABAADwAAdADsAUwAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTE0Jy4BJyYjESInLgEnJjU0Nz4BNzYzMhceARcWFTEUBw4BBwYjGQEhFBceARcWMzI3PgE3NjU0Jy4BJyYjAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXWpjWFeDJSYmJYNXWGNjWFeDJSYmJYNXWGP+ICYlg1dYY2NYV4MlJiYlg1dYYwPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/CAmJYNXWGNjWFeDJSYmJYNXWGNjWFeDJSYDwP4gY1hXgyUmJiWDV1hjY1hXgyUmAAAACgAA/8ADwAPAAAMABwALAA8AEQAVABkAGwAfACMAAAERMxEDIxEzBREzEQMjETMFMTcjETMnETMRATE3IxEzJxEzEQMAwCCAgP5gwCCAgP5wsMDAoID+cLDAwKCAA8D8AAQA/CADwKD8wANA/OADALAQ/YAgAkD9wAGQEP5AIAGA/oAACgAA/8ADwAPAAAMABwALAA8AEwAXABsAHQAhACUAABMzESMTESMRNyMRMwERMxEDIxEzBREzEQMjETMFMTcjETMnETMREKCgkICgwMACQMAggID+YMAggID+cLDAwKCAAXD+YAGQ/oABgCD+QAQA/AAEAPwgA8Cg/MADQPzgAwCwEP2AIAJA/cAAAAAKAAD/wAPAA8AAAwAHAAsADwATABcAGwAfACMAJwAAEzMRIxMRIxE3IxEzEzMRIxMRIxE3IxEzAREzEQMjETMFETMRAyMRMxCgoJCAoMDAUKCgkICgwMABQMAggID+YMAggIABcP5gAZD+gAGAIP5AAnD9oAJQ/cACQCD9gAQA/AAEAPwgA8Cg/MADQPzgAwAAAAAACgAA/8ADwAPAAAMABwALAA8AEwAXABsAHwAjACcAABMzESMTESMRNyMRMxMzESMTESMRNyMRMxMzESMTMxEjExEzEQMjETMQoKCQgKDAwFCgoJCAoMDAQMDAIICA4MAggIABcP5gAZD+gAGAIP5AAnD9oAJQ/cACQCD9gANA/MADIP0AA+D8AAQA/CADwAAACQAA/8ADwAPAAAMABwALAA8AEwAXABsAHwAjAAATMxEjExEjETcjETMTMxEjExEjETcjETMTMxEjATMRIwMzESMQoKCQgKDAwFCgoJCAoMDAQMDAAQDAwOCAgAFw/mABkP6AAYAg/kACcP2gAlD9wAJAIP2AA0D8wAQA/AADIP0AAAAAAQAA/8AEAAPAAAMAAAkDAgD+AAIAAgADwP4A/gACAAAMAAD/wAQAA8AAAwAHAAsADwATABcAGwAfACMAJwArAC8AABMhESEBESERJSERIRMhESEBESERJSERIQUhESEBESERJSERIRMhESEBESERJSERIRABwP5AAbD+YAHA/iAB4FABwP5AAbD+YAHA/iAB4PwQAcD+QAGw/mABwP4gAeBQAcD+QAGw/mABwP4gAeADsP5AAbD+YAGgIP4gAdD+QAGw/mABoCD+IFD+QAGw/mABoCD+IAHQ/kABsP5gAaAg/iAABQAA/8AEAAPAAAMABwALAA8AEwAAAREhEQMhESEBIREhASERIQEhESECIAHgIP5gAaD8IAHg/iACIAHg/iD94AHg/iADwP4gAeD+QAGg/gD+IAHg/iAEAP4gAAAGAAD/wAQAA8AAAwAHAAsADwATABcAABMRIREDIREhNxEhEQMhESEBIREhASERIQAB4CD+YAGgYAHgIP5gAaD8IAHg/iACIAHg/iADwP4gAeD+QAGgIP4gAeD+QAGg/gD+IAHg/iAAAAcAAP/ABAADwAADAAcACwAPABMAFwAbAAATESERAyERIRMRIREDIREhAREhEQMhESEBIREhAAHgIP5gAaBgAeAg/mABoP5AAeAg/mABoPwgAeD+IAPA/iAB4P5AAaD+AP4gAeD+QAGgAkD+IAHg/kABoP4A/iAAAAgAAP/ABAADwAADAAcACwAPABMAFwAbAB8AABMRIREDIREhExEhEQMhESElESERAyERIRMRIREDIREhAAHgIP5gAaBgAeAg/mABoPwgAeAg/mABoGAB4CD+YAGgA8D+IAHg/kABoP4A/iAB4P5AAaAg/iAB4P5AAaACQP4gAeD+QAGgAAEAAP/QBAADsAAJAAABGwEFARMlBRMBAWCgoAFg/wBA/sD+wED/AAKQASD+4ED/AP6AwMABgAEAAAAAAAMAAP/QBAADsAAJABMAGQAAASULAQUBAyUFAxMlBRMnJRsBBQcBBQEDJREEAP6goKD+oAEAQAFAAUBALP7U/tU88QFLlZYBSvD+cP6gAQBAAUACUEABIP7gQP8A/oDAwAGA/qC0tAFo7zwBDf7zPO8BOED/AP6AwAMgAAACAAD/0AQAA7AACQATAAABJQsBBQEDJQUDEyUFEyclGwEFBwQA/qCgoP6gAQBAAUABQEAs/tT+1TzxAUuVlgFK8AJQQAEg/uBA/wD+gMDAAYD+oLS0AWjvPAEN/vM87wADAAD/0AQAA7AACQATABgAAAElCwEFAQMlBQMTJQUTJyUbAQUHAREFAQMEAP6goKD+oAEAQAFAAUBALP7U/tU88QFLlZYBSvD+cP6gAQBAAlBAASD+4ED/AP6AwMABgP6gtLQBaO88AQ3+8zzv/tgCYED/AP6AAAADAAD/0AQAA7AACQATABsAAAElCwEFAQMlBQMTJQUTJyUbAQUHCwIFAQMlFwQA/qCgoP6gAQBAAUABQEAs/tT+1TzxAUuVlgFK8FCgoP6gAQBAAUCgAlBAASD+4ED/AP6AwMABgP6gtLQBaO88AQ3+8zzvATgBIP7gQP8A/oDAYAAAAAABAAAAIAQAA2AAAgAANyEBAAQA/gAgA0AAAAAAAQAAAJsEAALlAAIAABMhAQAEAP4AAuX9tgAAAAEAAACbBAAC5QACAAA3IQEABAD+AJsCSgAAAAABAAAAAAQAA2AAEAAAAScRIxUnARUzESE1MxUhETMEAMCAwP4AgAFAgAFAgAFgwAEgoMD+ACD+wMDAAUAAAAAAAwAA/8AEAAOAAAsAFwAwAAAlFAYjIiY1NDYzMhYFFAYjIiY1NDYzMhYZASE0JisBFTMTDgEVFBYzITUhIiY1OAE1AYA4KCg4OCgoOAKAOCgoODgoKDj9ACUbwIAwFhpLNQMA/QAbJSAoODgoKDg4KCg4OCgoODgBeAGAGyVA/mQSNB41S0AlGwEAAAABAAD/wAPAA4AANQAAAQ4BIyImJy4BNTQ2NzYnLgEnJiMiBw4BBwYxFBceARcWFxYXHgEXFjMwNz4BNzY1NCcuAScmAsAwIDAwYDAwUFAwGBISSCoqGBghITwVFRYXSS0uLy9EQ5FFRDAeHkgeHh8fVCsrAUAwUFAwMGAwMCAwGCsrVB8fHh5IHh4wREWRQ0QvLy4tSRcWFRU8ISEYGCoqSBISAAQAwP/AA0ADwAAPABMAHwAjAAABISIGFREUFjMhMjY1ETQmBSEVIRMiJjU0NjMyFhUUBjchESEC4P5AKDg4KAHAKDg4/ngBAP8AgBslJRsbJSXl/gACAAPAOCj8wCg4OCgDQCg4MCD8kCUbGyUlGxslwAKAAAAAAAIAgAAAA4ADwAALABwAAAE0NjMyFhUUBiMiJgUjAxMnBxMDIyIGFREhETQmAUBwUFBwcFBQcAHAI8dKYGBKxyNgIAMAIAMAUHBwUFBwcLD+bAF0YGD+jAGUcFD+wAFAUHAAAAADAAD/wAQAA4AAJwA/AEMAAAEjNTQnLgEnJiMiBw4BBwYVERQXHgEXFjMyNz4BNzY9ATMyNjURNCYlLgEnPgE3PgEzMhYXHgEXDgEHDgEjIiYBIzUzA8DAHh5pRkVQUEVGaR4eHh5pRkVQUEVGaR4ewBslJfzVHCIJCSIcLGs5OWssHCIJCSIcLGs5OWsCpICAAoBgIR0dLAwNDQwsHR0h/YAhHR0sDA0NDCwdHSFgJRsBQBslPgkSBwcSCQ8PDw8JEgcHEgkPDw/+kcAAAAAAAQAA/8AEAAPAABYAAAEnAScFJy4BBwYWHwEDFwEXETM/ATUhAwC3AbeA/dusJlobGg8mrNuAAUm3gEDA/wABQLcBSYDbrCYPGhtaJqz924ABt7f/AMBAgAAAAAACAAAAAAQAA0AAJwArAAABAyM1NCYjISIGFREXMw4BFRQWMzI2NTQmJyEOARUUFjMyNjU0JiczJTUzFwQAgMAmGv3AGiZAUQgJSzU1SwkIAWIICUs1NUsJCFH+wIVgAYABAIAaJiYa/gBADiERNUtLNREhDg4hETVLSzURIQ7AwMAAAAACAAD/wAQAA8AAGwBzAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1NCcuAScmAyImJxM+AT0BNCYjIicuAScmNS4BKwEiBh0BFBYfARUmJy4BJyY1NDY3MzI2PwE+AT0BPgEzMhYXDgEHDgEVFBYXHgEzOgEzFhceAQcGBxQGFQYHDgEHBgIAal1eiygoKCiLXl1qal1eiygoKCiLXl1qL1kp6QQEEw0qKipCFBUFDAaADRMKCG4sIyMzDQ4WFXUGDAWABAUeQSE1YywDBgMbHR0bHEYmAgUCBgYFBAYFEgEeJSRRLC0DwCgoi15dampdXosoKCgoi15dampdXosoKPxAExEBBwQLBmANExITLBMSAQQFEw3ACRAEN7wfKCdfNTU5NGAsBQSABQwGTQkKFxYDBQMbRyYmRxsbHRMjIl88O0YBAwEgGBkjCgkAAAIBQP/AAoADwAALAB0AAAEUBiMiJjU0NjMyFhUjIgYVETMRMxEzETMRMxE0JgJAOCgoODgoKDjAGyVAUCBQQCUDYCg4OCgoODjIJRv+wP6AAYD+gAGAAUAbJQAAAgDA/8ADAAPAAAsAJgAAARQGIyImNTQ2MzIWEzcnLgEjISIGDwEXNxcHMxMzETMRMxMzJzcXAkA4KCg4OCgoOI8xhQUOCP8ACA4FhTFvJoZ7FUAgQBV7hiZvA2AoODgoKDg4/jgjzwYICAbOJJBa9v7AAUD+wAFA9lqQAAQAAP/ABAADwAALABcAKQBEAAABFAYjIiY1NDYzMhYFFAYjIiY1NDYzMhYFIyIGFREzETMRMxEzETMRNCYBNycuASMhIgYPARc3FwczEzMRMxEzEzMnNxcBADgoKDg4KCg4AkA4KCg4OCgoOP3AwBslQFAgUEAlArQxhQUOCP8ACA4FhTFvJoZ7FUAgQBV7hiZvA2AoODgoKDg4KCg4OCgoODjIJRv+wP6AAYD+gAGAAUAbJf8AI88GCAgGziSQWvb+wAFA/sABQPZakAAAAAACAGL/wAOhA8AALAA5AAABNDY3LgEnJgYjIiYHDgEHBgcGFhcWFx4BNz4BMzIWNz4BNz4BNyInLgEnJicDPgEnDgEHDgEXFjY3AxdrBC11GTxqHh9ZMUFxIiIGBxkbGyEgTzIxPDs7OzM1SCAlIQEBFRUzFhUBgBogBSdUHBkjBitSGwGgYWACQiIBBjUuAQFFOjtGRo1AQS8vVQICKCoBAU4vNlkDCwoxJyg6AXwhVi0CKyEcViwDKyAAAAYAQP/AA8ADvQANABsANgBZAGsAfgAAASIGFREUFjMyNjURNCYhIgYVERQWMzI2NRE0JhMUFjMxFRQWMzI2PQEzFRQWMzI2PQEyNjURISUuASc3NiYnJgYPAScuASMiBg8BJy4BBw4BHwEOAQcVITUjJSImNTQ2MzgBMTgBMTIWFRQGMyImNTQ2MzgBMTgBMTIWFRQGIwOAGiYmGhomJvzmGiYmGhomJkY4KCYaGiaAJhoaJig4/cACPgdFNSAGCQwMGQYgCBYtGBgtFgggBhkMDAkGIDVFBwI+Av6CDRMTDQ0TE7MNExMNDRMTDQJAJhr/ABomJhoBABomJhr/ABomJhoBABom/qAoOIAaJiYagIAaJiYagDgoAWBAQm0jQAwZBgYJDEADBwgIBwNADAkGBhkMQCNtQiAgQBMNDRMTDQ0TEw0NExMNDRMABAAA/8ADwAOAAAMABwALAA8AABMRJRETJREhBRElEQMlESEAAYBAAgD+AAIA/gBA/oABgAHAATg0/pQBdkr+QED+QEgBeP6QNQE7AAEAAAABAABxUvu9Xw889QALBAAAAAAA1kGxXgAAAADWQbFeAAD/wAQCA8AAAAAIAAIAAAAAAAAAAQAAA8D/wAAABAAAAP/+BAIAAQAAAAAAAAAAAAAAAAAAAD0EAAAAAAAAAAAAAAACAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAHBAAAAAQAAAAEAAAABAAAAAQAAAAEAAGOBAAAUwQAARQEAAB2BAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAADwAAAA8AAAAPAAAADwAAAA8AAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAwAQAAIAEAAAABAAAAAQAAAAEAAAABAABQAQAAMAEAAAABAAAYgQAAEAEAAAAAAAAAAAKABQAHgA0AEgAYAB4AIwAwgDyAS4BcgHOAfICJgJeApYC/ANMA5QDpAPUBEQEngUGBYIFxAYKBlQGnAbeBu4HUAd+B7IH7ggwCE4IjAi8CPgJOglICVYJZAmECcoKHApYCooK8gsgC2IMCAw2DHQM3A06DeAOBgABAAAAPQB/AAwAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAADgCuAAEAAAAAAAEACgAAAAEAAAAAAAIABwB7AAEAAAAAAAMACgA/AAEAAAAAAAQACgCQAAEAAAAAAAUACwAeAAEAAAAAAAYACgBdAAEAAAAAAAoAGgCuAAMAAQQJAAEAFAAKAAMAAQQJAAIADgCCAAMAAQQJAAMAFABJAAMAAQQJAAQAFACaAAMAAQQJAAUAFgApAAMAAQQJAAYAFABnAAMAAQQJAAoANADIU3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0VmVyc2lvbiAxLjAAVgBlAHIAcwBpAG8AbgAgADEALgAwU3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0U3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0UmVndWxhcgBSAGUAZwB1AGwAYQByU3RpbXVsc29mdABTAHQAaQBtAHUAbABzAG8AZgB0Rm9udCBnZW5lcmF0ZWQgYnkgSWNvTW9vbi4ARgBvAG4AdAAgAGcAZQBuAGUAcgBhAHQAZQBkACAAYgB5ACAASQBjAG8ATQBvAG8AbgAuAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==) format('truetype');\
                                font-weight: normal;\
                                font- style: normal;\
                                }\
                    "));

    document.head.appendChild(newStyle);
}