﻿
StiMobileDesigner.prototype.ProcessImageStatusPanel = function () {
    var processImage = this.CreateHTMLTable(); ;
    processImage.jsObject = this;
    processImage.style.display = "none";
    processImage.className = "stiDesignerProcessImageStatusPanel stiDesignerClearAllStyles";
    this.options.processImageStatusPanel = processImage;        

    var img = this.ProgressMini("white");
    processImage.addCell(img).style.padding = "0 3px 0 3px";

    var text = processImage.addCell();
    text.innerHTML = this.loc.A_WebViewer.Loading.replace("...", "");
    text.style.fontSize = "12px";
    text.style.padding = "0 5px 0 0";

    processImage.show = function () {
        this.style.display = "";
    }

    processImage.hide = function () {
        this.style.display = "none";
    }

    return processImage;
}

StiMobileDesigner.prototype.InitializeProcessImage = function () { 
    var processImage = this.Progress();
    processImage.jsObject = this;
    processImage.style.display = "none";
    this.options.processImage = processImage;
    this.options.mainPanel.appendChild(processImage);
    processImage.style.top = "50%"
    processImage.style.left = "50%"
    processImage.style.marginLeft = "-32px";
    processImage.style.marginTop = "-100px";

    var buttonCancel = this.FormButton(null, null, this.loc.Buttons.Cancel.replace("&", ""), null);
    buttonCancel.style.position = "absolute";
    buttonCancel.style.display = "none";
    buttonCancel.style.top = "145px";
    buttonCancel.style.borderRadius = "7px";
    buttonCancel.style.border = "1px solid #c6c6c6";
    buttonCancel.style.left = "calc(50% - 40px)";
    buttonCancel.style.height = "20px";
    processImage.appendChild(buttonCancel);
    processImage.buttonCancel = buttonCancel;

    processImage.show = function () {
        this.style.display = "";
        if (!this.jsObject.options.disabledPanels) this.jsObject.InitializeDisabledPanels();
        this.jsObject.options.disabledPanels[6].style.display = "";
    }

    processImage.hide = function () {
        this.style.display = "none";
        this.hideCancelButton();
        if (!this.jsObject.options.disabledPanels) this.jsObject.InitializeDisabledPanels();
        this.jsObject.options.disabledPanels[6].style.display = "none";
    }

    processImage.showCancelButton = function () {
        this.cancelTimer = setTimeout(function () {
            buttonCancel.style.display = "";
            buttonCancel.style.opacity = 1 / 100;
            var d = new Date();
            var endTime = d.getTime() + 300;
            processImage.jsObject.ShowAnimationForm(buttonCancel, endTime);
        }, 5000);
    }

    processImage.hideCancelButton = function () {
        buttonCancel.style.display = "none";
        clearTimeout(buttonCancel.animationTimer);
        clearTimeout(processImage.cancelTimer);
    }

    return processImage;
}