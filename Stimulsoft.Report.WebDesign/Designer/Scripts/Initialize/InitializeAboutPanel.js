﻿
StiMobileDesigner.prototype.InitializeAboutPanel = function () {
    var aboutPanel = document.createElement("div");
    this.options.aboutPanel = aboutPanel;
    this.options.mainPanel.appendChild(aboutPanel);
    aboutPanel.jsObject = this;
    aboutPanel.className = "stiDesignerAboutPanel";
    aboutPanel.style.background = "url(" + this.options.images["About.png"] + ")";
    aboutPanel.style.display = "none";

    var header = document.createElement("div");
    header.innerHTML = "Stimulsoft Reports";
    header.className = "stiDesignerAboutPanelHeader";
    aboutPanel.appendChild(header);

    var copyRight = document.createElement("div");
    copyRight.innerHTML = "Copyright 2003-" + new Date().getFullYear() + " Stimulsoft";
    copyRight.className = "stiDesignerAboutPanelCopyright";
    aboutPanel.appendChild(copyRight);
        
    var version = document.createElement("div");
    version.innerHTML = "Version " + this.options.productVersion.trim();
    if (!this.options.jsMode) version.innerHTML += ", " + this.options.frameworkType;
    version.innerHTML += ", JS";
    version.className = "stiDesignerAboutPanelVersion";
    aboutPanel.appendChild(version);

    var allRight = document.createElement("div");
    allRight.innerHTML = "All rights reserved";
    allRight.className = "stiDesignerAboutPanelVersion";
    aboutPanel.appendChild(allRight);

    var stiLink = document.createElement("div");    
    stiLink.innerHTML = "www.stimulsoft.com";
    stiLink.className = "stiDesignerAboutPanelStiLink";
    aboutPanel.appendChild(stiLink);

    stiLink.onclick = function (event) {
        if (event) {
            event.stopPropagation();
            event.preventDefault();
        }
        aboutPanel.jsObject.openNewWindow("https://www.stimulsoft.com");
    };
    
    aboutPanel.ontouchend = function () { this.changeVisibleState(false); }
    aboutPanel.onclick = function () { this.changeVisibleState(false); }

    aboutPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        this.jsObject.SetObjectToCenter(this);
        if (!this.jsObject.options.disabledPanels) this.jsObject.InitializeDisabledPanels();
        this.jsObject.options.disabledPanels[2].changeVisibleState(state);
        if (this.jsObject.options.previewMode && this.jsObject.options.viewer) {
            (this.jsObject.options.viewer.jsObject.controls || this.jsObject.options.viewer.jsObject.options).disabledPanels[1].style.display = state ? "" : "none";
        }
        if (this.jsObject.options.buttons["About"]) this.jsObject.options.buttons["About"].setSelected(state);
        this.visible = state;
        this.jsObject.options.currentForm = state ? this : null;
    }

    return aboutPanel;
}