﻿
StiMobileDesigner.prototype.FileButton = function () {
    var caption = this.loc.MainMenu.menuFile;
    if (caption) caption = caption.replace("&", "");
    var button = this.SmallButton("fileButton", null, caption, null, null, null, this.GetStyles("ToolButton"), true);
    button.style.height = "30px";
    button.style.minWidth = "30px";
    button.innerTable.style.width = "100%";

    return button;
}