﻿
StiMobileDesigner.prototype.InitializeToolBar = function () {
    var toolBar = document.createElement("div");
    this.options.toolBar = toolBar;
    this.options.mainPanel.appendChild(toolBar);
    toolBar.className = "stiDesignerToolBar";
    toolBar.jsObject = this;
    if (this.options.isTouchDevice) toolBar.style.overflowX = "auto"; else toolBar.style.overflow = "hidden";
    var toolButoonCellClass = "stiDesignerToolButtonCell";

    var toolBarTable = this.CreateHTMLTable();
    toolBarTable.style.width = "100%";
    toolBarTable.style.height = "100%";
    toolBar.appendChild(toolBarTable);

    toolBar.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        this.jsObject.options.paintPanel.style.top = (this.jsObject.options.workPanel.offsetHeight + this.jsObject.options.pagesPanel.offsetHeight + this.offsetHeight) + "px";
    }

    if (this.options.showSaveButton) {
        var saveReportHotButton = this.StatusPanelButton("saveReportHotButton", null, "SaveWhite.png", null, null, 30, 30);
        saveReportHotButton.style.marginLeft = "3px";
        toolBarTable.addCell(saveReportHotButton).className = toolButoonCellClass;
        saveReportHotButton.action = function () { this.jsObject.ActionSaveReport(); }
    }

    //Undo Button
    var undoButton = this.StatusPanelButton("undoButton", null, "Undo.png", this.loc.MainMenu.menuEditUndo.replace("&", ""), null, 30, 30);
    toolBarTable.addCell(undoButton).className = toolButoonCellClass;
    undoButton.style.marginLeft = "3px";

    //Redo Button
    var redoButton = this.StatusPanelButton("redoButton", null, "Redo.png", this.loc.MainMenu.menuEditRedo.replace("&", ""), null, 30, 30);
    toolBarTable.addCell(redoButton).className = toolButoonCellClass;
    redoButton.style.marginLeft = "3px";

    //Interface Button
    var interfaceButton = this.StatusPanelButton("interfaceButton", null, "InterfaceMenu.InterfaceType.png", null, "Down", 30, 30);
    interfaceButton.arrow.src = this.options.images["ButtonArrowDownWhite.png"];
    interfaceButton.style.margin = "0 2px 0 2px";
    interfaceButton.allwaysEnabled = true;
    toolBarTable.addCell(interfaceButton).className = toolButoonCellClass;
    interfaceButton.style.display = this.options.interfaceType == "Auto" && this.options.supportTouchInterface && !this.options.cloudMode ? "" : "none";

    var interfaceMenu = this.InterfaceTypeMenu();
    interfaceButton.action = function () {
        interfaceMenu.changeVisibleState(!interfaceMenu.visible);
    }

    var fileButtonCell = toolBarTable.addCell();
    fileButtonCell.className = toolButoonCellClass;
    var fileButton = this.FileButton();
    fileButton.style.margin = "2px 2px 0 3px";
    fileButtonCell.appendChild(fileButton);
    fileButton.style.display = this.options.showFileMenu ? "" : "none";

    var buttons = [
        ["homeToolButton", this.loc.Toolbars.TabHome, true],
        ["insertToolButton", this.loc.PropertyMain.Insert, !this.options.showInsertButton ? false : this.options.showInsertTab],
        ["pageToolButton", this.loc.Toolbars.TabPage, this.options.showPageButton],
        ["layoutToolButton", this.loc.Toolbars.TabLayout, this.options.showLayoutButton],
        ["previewToolButton", this.loc.Wizards.Preview, this.options.showPreviewButton],
    ]

    for (var i = 0; i < buttons.length; i++) {
        var toolButtonCell = toolBarTable.addCell(this.ToolButton(buttons[i][0], buttons[i][1],
            (this.options.cloudMode && buttons[i][0] == "previewToolButton") ? true : false));
        toolButtonCell.className = toolButoonCellClass;
        toolButtonCell.style.display = buttons[i][2] ? "" : "none";
    }

    if (this.options.cloudMode && this.options.buttons.previewToolButton) {
        var previewToolButton = this.options.buttons.previewToolButton;
        previewToolButton.progress = this.ProgressMini();
        previewToolButton.progress.style.visibility = "hidden";
        previewToolButton.caption.style.padding = "0 2px 0 7px";
        previewToolButton.style.padding = "0 3px 0 12px";
        previewToolButton.innerTable.addCell(this.options.buttons.previewToolButton.progress);
    }

    var freeCell = toolBarTable.addCell();
    freeCell.style.width = "100%";
    freeCell.className = toolButoonCellClass;

    if (!this.options.fullScreenMode) {
        var resizeButton = this.StatusPanelButton("resizeDesigner", null, "ResizeWindow.png", this.loc.PropertyMain.Resize, null, 30, 30);
        toolBarTable.addCell(resizeButton).className = toolButoonCellClass;
        resizeButton.allwaysEnabled = true;
    }

    //Publish
    var publishButton = this.SmallButton("buttonPublish", null, this.loc.Cloud.ButtonPublish, null, null, null, this.GetStyles("ToolButtonDinamic"));
    publishButton.style.margin = "2px 2px 0 2px";
    publishButton.style.height = "30px";
    toolBarTable.addCell(publishButton).style.verticalAlign = "bottom";
    publishButton.style.display = this.options.cloudMode ? "" : "none";

    publishButton.action = function () {
        var jsObject = this.jsObject;
        var win = jsObject.openNewWindow();

        if (jsObject.options.report) {
            jsObject.SendCommandToDesignerServer("GetReportString", {}, function (answer) {
                jsObject.InitializePublishForm(function (publishForm) {
                    publishForm.show(answer.reportString, win);
                });
            });
        }
        else {
            jsObject.InitializePublishForm(function (publishForm) {
                publishForm.show(null, win);
            });
        }
    }

    //Localization
    if ((this.options.jsMode && this.options.showLocalization) ||
        (this.options.locFiles && this.options.locFiles.length > 0)) {        
        var locControl = document.createElement("div");
        locControl.jsObject = this;
        this.localizationControl = locControl;
        var locCell = toolBarTable.addCell(locControl);
        locCell.className = toolButoonCellClass;
        locControl.style.margin = "0 2px 0 2px";
        locControl.locName = "en";

        var locButton = this.StatusPanelButton("localizationButton", "EN", null, null, null, 30, 30);
        if (locButton.caption) locButton.caption.style.textAlign = "center";
        locControl.appendChild(locButton);
        locButton.allwaysEnabled = true;

        var locMenu = this.VerticalMenu("localizationMenu", locButton, "Down", null, this.GetStyles("MenuMiddleItem"));
        locMenu.rightToLeft = true;
        locMenu.innerContent.style.maxHeight = "550px";

        locButton.action = function () { locMenu.changeVisibleState(!locMenu.visible); }

        locControl.action = function () { }

        locControl.setLoc = function (locName, locShortName) {
            this.locName = locName;
            locButton.caption.innerHTML = locShortName ? locShortName.toUpperCase() : locName.toUpperCase();
        }

        locControl.addItems = function (items) {
            locMenu.addItems(items);
        }

        locMenu.action = function (menuItem) {
            locMenu.changeVisibleState(false)
            locControl.setLoc(menuItem.key, menuItem.name);
            locControl.action();
        }

        locMenu.onshow = function () {
            for (var itemKey in this.items) {
                if (this.items[itemKey].caption)
                    this.items[itemKey].setSelected(locControl.locName == this.items[itemKey].key);
            }
        }

        if (this.options.locFiles && this.options.locFiles.length > 0) {
            if (this.options.cultureName) {
                locControl.locName = this.options.cultureName;
                if (locButton.caption) locButton.caption.innerHTML = this.options.cultureName.toUpperCase();
            }

            //Add loc items
            var locItems = [];
            for (var i = 0; i < this.options.locFiles.length; i++) {
                locItems.push(this.Item(this.options.locFiles[i].FileName, this.options.locFiles[i].Description, null, this.options.locFiles[i].CultureName));
            }
            locControl.addItems(locItems);

            //Override methods
            locControl.setLoc = function (locName, locFileName) {
                this.locName = locName;
                this.locFileName = locFileName;
                locButton.caption.innerHTML = locName.toUpperCase();
            }

            locMenu.action = function (menuItem) {
                locMenu.changeVisibleState(false)
                locControl.setLoc(menuItem.key, menuItem.name);
                locControl.action();
            }

            locControl.action = function () {
                var params = {
                    command: "SetLocalization",
                    localization: this.locFileName
                };
                this.jsObject.PostForm(params);
            }
        }
    }

    //About Button
    var aboutButton = this.StatusPanelButton("aboutButton", null, "AboutIcon.png", this.loc.MainMenu.menuHelpAboutProgramm.replace("&", "").replace("...", ""), null, 30, 30)
    aboutButton.style.margin = "0 2px 0 2px";
    var aboutCell = toolBarTable.addCell(aboutButton);
    aboutCell.className = toolButoonCellClass;
    aboutButton.allwaysEnabled = true;
    aboutButton.style.display = this.options.showAboutButton ? "" : "none";

    //Show Toolbar button
    var showToolBarButton = this.StatusPanelButton("showToolBarButton", null, "ShowToolbar.png", null, null, 30, 30);
    showToolBarButton.allwaysEnabled = true;
    showToolBarButton.style.display = "none";
    showToolBarButton.style.margin = "0 2px 0 2px";
    toolBarTable.addCell(showToolBarButton).className = toolButoonCellClass;

    this.options.buttons["homeToolButton"].setSelected(true);
}