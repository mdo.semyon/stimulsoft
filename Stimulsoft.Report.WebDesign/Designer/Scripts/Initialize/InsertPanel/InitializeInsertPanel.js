﻿
StiMobileDesigner.prototype.InitializeInsertPanel = function () {
    var insertPanel = this.ChildWorkPanel("insertPanel", "stiDesignerInsertPanel");
    insertPanel.style.display = "none";
    insertPanel.selectedComponent = null;
    insertPanel.buttons = {};

    insertPanel.resetChoose = function () {
        this.jsObject.options.drawComponent = false;
        this.jsObject.options.paintPanel.changeCursorType(false);

        if (this.selectedComponent) {
            this.selectedComponent.setSelected(false);
            this.selectedComponent = null;
        }

        if (!this.jsObject.options.componentsIntoInsertTab) {
            this.jsObject.options.buttons.insertBands.setSelected(false);
            this.jsObject.options.buttons.insertCrossBands.setSelected(false);
            this.jsObject.options.buttons.insertComponents.setSelected(false);
            this.jsObject.options.buttons.insertBarCodes.setSelected(false);
            this.jsObject.options.buttons.insertInfographics.setSelected(false);
            this.jsObject.options.buttons.insertShapes.setSelected(false);
        }
    }

    insertPanel.setChoose = function (selectedElement) {
        this.jsObject.options.drawComponent = true;
        this.jsObject.options.paintPanel.setCopyStyleMode(false);
        this.jsObject.options.paintPanel.changeCursorType(true);
        this.selectedComponent = selectedElement;

        if (selectedElement.menu && selectedElement.menu.parentButton) {
            selectedElement.menu.parentButton.setSelected(true);
        }
        else {
            selectedElement.setSelected(true);
        }
    }

    var innerTable = this.CreateHTMLTable();
    insertPanel.appendChild(innerTable);

    innerTable.addCell(this.GroupBlock("groupBlockInsertPages", this.loc.FormDictionaryDesigner.NewItem, false, null));
    innerTable.addCell(this.GroupBlockSeparator());

    if (!this.options.componentsIntoInsertTab) {
        innerTable.addCell(this.GroupBlock("groupBlockGroupsComponents", this.loc.PropertyMain.Categories, false, null));
        this.options.controls.groupsComponentsSep = this.GroupBlockSeparator();
        innerTable.addCell(this.options.controls.groupsComponentsSep);
    }

    innerTable.addCell(this.GroupBlock("groupBlockMainComponents", this.loc.Report.Components, false, null));
    innerTable.addCell(this.GroupBlockSeparator());

    //Pages
    var pagesTable = this.GroupBlockInnerTable();
    pagesTable.style.width = "100%";
    this.options.controls.groupBlockInsertPages.container.appendChild(pagesTable);

    var pageButton = this.BigButton("insertPanelAddPage", null, this.loc.A_WebViewer.Page, "BlankPage.png",
        [this.loc.HelpDesigner.PageNew, this.HelpLinks["insertcomponent"]], false, this.GetStyles("StandartBigButton"), false, 70);
    pageButton.style.display = "inline-block";
    pagesTable.addCell(pageButton).style.textAlign = "center";

    if (this.options.dashboardAssemblyLoaded) {
        var dashboardButton = this.BigButton("insertPanelAddDashboard", null, this.loc.Components.StiDashboard, "StiDashboard.png",
            [this.loc.Wizards.groupCreateNewDashboard, this.HelpLinks["insertcomponent"]], false, this.GetStyles("StandartBigButton"), false, 70);
        dashboardButton.style.display = "inline-block";
        pagesTable.addCell(dashboardButton).style.textAlign = "center";
    }

    //Groups
    if (!this.options.componentsIntoInsertTab) {
        var groupsTable = this.GroupBlockInnerTable();
        this.options.controls.groupBlockGroupsComponents.container.appendChild(groupsTable);

        groupsTable.addCell(this.BigButton("insertBands", null, this.loc.Report.Bands, "Bands.png",
            [this.loc.Report.Bands, this.HelpLinks["insertcomponent"]], true, this.GetStyles("StandartBigButton")));
        this.BandsMenu();

        groupsTable.addCell(this.BigButton("insertCrossBands", null, this.loc.Report.CrossBands, "CrossBands.png",
            [this.loc.Report.CrossBands, this.HelpLinks["insertcomponent"]], true, this.GetStyles("StandartBigButton")));
        this.CrossBandsMenu();

        groupsTable.addCell(this.BigButton("insertComponents", null, this.loc.Report.Components, "Components.png",
            [this.loc.Report.Components, this.HelpLinks["insertcomponent"]], true, this.GetStyles("StandartBigButton")));
        this.ComponentsMenu();

        groupsTable.addCell(this.BigButton("insertBarCodes", null, this.loc.Components.StiBarCode, "StiBarCode.png",
            [this.loc.Components.StiBarCode, this.HelpLinks["insertcomponent"]], true, this.GetStyles("StandartBigButton")));
        this.BarCodesMenu();

        var insertShapesButton = this.BigButton("insertShapes", null, this.loc.Report.Shapes, "StiShape.png",
            [this.loc.Report.Shapes, this.HelpLinks["insertcomponent"]], true, this.GetStyles("StandartBigButton"));
        groupsTable.addCell(insertShapesButton);
        this.ShapesMenu("shapesMenu", insertShapesButton);

        groupsTable.addCell(this.BigButton("insertInfographics", null, this.loc.Report.Infographics, "Infographics.png",
            [this.loc.Report.Infographics, this.HelpLinks["insertcomponent"]], true, this.GetStyles("StandartBigButton")));
        this.InfographicsMenu();
    }

    //Main
    insertPanel.update = function (components) {
        var jsObject = this.jsObject;

        if (insertPanel.mainTable) {
            this.jsObject.options.controls.groupBlockMainComponents.container.removeChild(insertPanel.mainTable);
            insertPanel.buttons = {};
        }

        insertPanel.mainTable = jsObject.GroupBlockInnerTable();
        jsObject.options.controls.groupBlockMainComponents.container.appendChild(insertPanel.mainTable);

        var addComponentButtons = function (componentTypes) {
            for (var i = 0; i < componentTypes.length; i++) {
                if (jsObject.options.visibilityComponents[componentTypes[i]] ||
                    jsObject.options.visibilityBands[componentTypes[i]] ||
                    jsObject.options.visibilityCrossBands[componentTypes[i]] ||
                    (jsObject.options.dashboardAssemblyLoaded && jsObject.options.visibilityDashboardElements[componentTypes[i]]))
                {
                    var isDashboardElement = jsObject.options.dashboardAssemblyLoaded && jsObject.options.visibilityDashboardElements[componentTypes[i]];
                    var text = isDashboardElement ? jsObject.loc.Components[componentTypes[i].replace("Element", "")] : jsObject.loc.Components[componentTypes[i]];
                    var image = (isDashboardElement ? "Dashboards.BigComponents." : "") + componentTypes[i] + ".png";
                    var tooltip = ["<b>" + text + "</b><br><br>" + (!isDashboardElement ? jsObject.loc.HelpComponents[componentTypes[i]] : ""), jsObject.HelpLinks["insertcomponent"]];
                    var button = jsObject.ComponentButton(componentTypes[i], text, image, "StandartBigButton", tooltip);

                    if (isDashboardElement && componentTypes[i] == "StiShapeElement") {
                        button = jsObject.BigButton("insertShapesElements", null, jsObject.loc.Report.Shapes, "StiShape.png", tooltip, true, jsObject.GetStyles("StandartBigButton"));
                        var shapesMenu = jsObject.ShapesMenu("shapesElementsMenu", button, true, true);
                        button.action = function () {
                            shapesMenu.changeVisibleState(!shapesMenu.visible);
                        }
                    }

                    button.isDashboardElement = isDashboardElement;
                    button.caption.style.maxWidth = "60px";
                    button.allwaysEnabled = false;
                    insertPanel.mainTable.addCell(button);
                    insertPanel.buttons[componentTypes[i]] = button;
                    button.setEnabled(jsObject.options.report != null);
                }
            }
        }

        var componentTypes = jsObject.options.componentsIntoInsertTab || components || jsObject.GetComponentsIntoInsertTab();
        var addSetupButtonSeparator = false;

        if (jsObject.Is_array(componentTypes)) {
            addComponentButtons(componentTypes);
        }
        else {
            var addSep = false;
            for (var groupName in componentTypes) {
                if (componentTypes[groupName].length > 0) {
                    var componentsInGroup = componentTypes[groupName];
                    var visibleComponents = [];
                    for (var i = 0; i < componentsInGroup.length; i++) {
                        if (jsObject.options.visibilityComponents[componentsInGroup[i]] ||
                            jsObject.options.visibilityBands[componentsInGroup[i]] ||
                            jsObject.options.visibilityCrossBands[componentsInGroup[i]] ||
                            (jsObject.options.dashboardAssemblyLoaded && jsObject.options.visibilityDashboardElements[componentsInGroup[i]])) {
                            visibleComponents.push(componentsInGroup[i]);
                        }
                    }
                    if (visibleComponents.length > 0) {
                        if (addSep && groupName != "dashboards") {
                            insertPanel.buttons["separator" + groupName] = jsObject.InsertPanelSeparator();
                            insertPanel.mainTable.addCell(insertPanel.buttons["separator" + groupName]);
                        }
                        addSep = true;
                        addSetupButtonSeparator = true;
                    }
                }
                addComponentButtons(componentTypes[groupName]);
            }
        }

        if (jsObject.options.showSetupToolboxButton) {
            if (addSetupButtonSeparator) {
                insertPanel.setupButtonSeparator = insertPanel.mainTable.addCell(jsObject.InsertPanelSeparator());
            }
            var setupToolboxButton = jsObject.BigButton("insertPanelSetupToolbox", null, jsObject.loc.FormDesigner.SetupToolbox, "SetupToolbox.png",
                null, null, jsObject.GetStyles("StandartBigButton"), false, 70);
            insertPanel.mainTable.addCell(setupToolboxButton);

            setupToolboxButton.action = function () {
                this.jsObject.InitializeSetupToolboxForm(function (form) {
                    form.changeVisibleState(true);
                });
            }
        }

        var buttonNames = ["insertBands", "insertCrossBands", "insertComponents", "insertBarCodes", "insertShapes", "insertInfographics",
            "insertPanelAddPage", "insertPanelAddDashboard"];

        for (var i = 0; i < buttonNames.length; i++) {
            var button = jsObject.options.buttons[buttonNames[i]];
            if (button && button.style.display != "none")
                button.setEnabled(jsObject.options.report != null);
        }

        insertPanel.setMode(true);
    }

    insertPanel.setMode = function (manually) {
        var mode = this.jsObject.options.report && this.jsObject.options.currentPage && this.jsObject.options.currentPage.isDashboard ? "Dashboard" : "Page";
        if (this.mode == mode && !manually) return;
        this.mode = mode;

        if (!this.jsObject.options.componentsIntoInsertTab) {
            this.jsObject.options.controls.groupBlockGroupsComponents.style.display =
                this.jsObject.options.controls.groupsComponentsSep.style.display = mode == "Page" ? "" : "none";
        }

        insertPanel.setupButtonSeparator.style.display = "none";
        
        for (var name in this.buttons) {
            var button = this.buttons[name];
            button.parentNode.style.display = (mode == "Dashboard" && button.isDashboardElement && name.indexOf("separator") < 0) ||
                (mode == "Page" && !button.isDashboardElement) ? "" : "none";

            if (button.parentNode.style.display == "") {
                insertPanel.setupButtonSeparator.style.display = "";
            }
        }
    }

    insertPanel.update();
    insertPanel.setMode();
}

StiMobileDesigner.prototype.InsertPanelSeparator = function () {
    var sep = this.HomePanelSeparator();
    sep.style.height = this.options.isTouchDevice ? "90px" : "70px";

    return sep;
}

StiMobileDesigner.prototype.GetComponentsIntoInsertTab = function () {
    var componentsStr = this.GetCookie("StimulsoftMobileDesignerComponentsIntoInsertTab");
    if (componentsStr) {
        var components = JSON.parse(componentsStr);
        return components;
    }
    else {
        var components = {
            bands: ["StiPageHeaderBand", "StiPageFooterBand", "StiGroupHeaderBand", "StiGroupFooterBand", "StiHeaderBand", "StiFooterBand", "StiDataBand"],
            crossBands: [],
            components: ["StiText", "StiImage"],
            shapes: [],
            infographics: ["StiChart"]
        }

        if (this.options.dashboardAssemblyLoaded) {
            components.dashboards = ["StiTableElement", "StiChartElement", "StiGaugeElement", "StiPivotElement", "StiIndicatorElement", "StiProgressElement", "StiMapElement",
                "StiImageElement", "StiTextElement", "StiPanelElement", "StiShapeElement"];
        }

        return components;
    }
}
