﻿
StiMobileDesigner.prototype.InitializeCheckPanel_ = function () {
    var checkPanel = document.createElement("div");
    this.options.checkPanel = checkPanel;
    this.options.mainPanel.appendChild(checkPanel);
    checkPanel.className = "stiDesignerCheckPanel";
    checkPanel.jsObject = this;
    checkPanel.controls = {};
    checkPanel.isVisible = false;
    checkPanel.style.display = "none";
    checkPanel.style.boxShadow = "0 0 7px rgba(0,0,0,0.3)";

    //Toolbar
    var buttons = [
        ["Error", this.loc.Report.Errors, "ReportChecker.Error.png"],
        ["Warning", this.loc.Report.Warnings, "ReportChecker.Warning.png"],
        ["Information", this.loc.Report.InformationMessages, "ReportChecker.Information.png"],
        ["ReportRenderingMessage", this.loc.Report.ReportRenderingMessages, "ReportChecker.ReportRenderingMessage.png"]
    ]

    var toolBar = this.CreateHTMLTable();
    checkPanel.appendChild(toolBar);
    checkPanel.appendChild(this.FormSeparator());

    for (var i = 0; i < buttons.length; i++) {        
        var button = this.StandartSmallButton(null, null, buttons[i][1], buttons[i][2], null, null, true);
        button.name = buttons[i][0];
        button.style.margin = "4px";
        checkPanel.controls[buttons[i][0] + "Button"] = button;
        toolBar.addCell(button);
        if (i != buttons.length - 1) {
            toolBar.addCell(this.HomePanelSeparator());
        }
        button.setSelected(true);

        button.action = function () {
            this.setSelected(!this.isSelected);
            checkPanel.container.fill(checkPanel.checkItems);
        }
    }

    //Container
    var container = this.ReportCheckContainer(checkPanel);
    checkPanel.container = container;
    checkPanel.appendChild(container);
    
    checkPanel.show = function (checkItems) {
        this.checkItems = checkItems;
        this.changeVisibleState(true);

        if (checkItems) {
            container.progress.hide();
            container.fill(checkItems);
        }
        else {
            this.jsObject.SendCommandGetReportCheckItems(function (answer) {
                checkPanel.checkItems = answer.checkItems;
                container.progress.hide();                
                container.fill(answer.checkItems);
            });
        }
    }

    checkPanel.changeVisibleState = function (state) {
        this.isVisible = state;
        this.style.display = state ? "inline-block" : "none";

        if (state) {
            container.progress.show();
            container.progress.style.left = "265px";
            container.progress.style.top = "100px";
            this.style.left = (2 + (this.jsObject.options.propertiesPanel ? this.jsObject.options.propertiesPanel.offsetWidth : 0)) + "px";
            this.style.bottom = (2 + (this.jsObject.options.statusPanel ? this.jsObject.options.statusPanel.offsetHeight : 0)) + "px";
        }
    }
    
    checkPanel.onmousedown = function () {
        if (!this.isTouchStartFlag) this.ontouchstart(true);
    }

    checkPanel.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.checkPanelPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    return checkPanel;
}

StiMobileDesigner.prototype.ReportCheckContainer = function (checkPanel) {
    var container = document.createElement("div");
    container.jsObject = this;
    container.className = "stiDesignerCheckContainer";
    this.AddProgressToControl(container);
    
    container.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
    }
    
    container.fill = function (checkItems) {
        this.clear();
        if (!checkItems) return;
        var errorCount = 0;
        var warningCount = 0;
        var informationCount = 0;
        var reportRenderingMessageCount = 0;
        var num = 1;

        for (var i = 0; i < checkItems.length; i++) {
            switch (checkItems[i].status) {
                case "Error": errorCount++; break;
                case "Warning": warningCount++; break;
                case "Information": informationCount++; break;
                case "ReportRenderingMessage": reportRenderingMessageCount++; break;
            }

            if (checkPanel.controls[checkItems[i].status + "Button"] && checkPanel.controls[checkItems[i].status + "Button"].isSelected) {
                var checkItem = this.jsObject.ReportCheckContainerItem(num, checkItems[i], checkPanel);
                checkItems[i].index = i;
                num++;
                this.appendChild(checkItem);
            }            
        }

        checkPanel.controls.ErrorButton.caption.innerHTML = this.jsObject.loc.Report.Errors + " - " + errorCount;
        checkPanel.controls.WarningButton.caption.innerHTML = this.jsObject.loc.Report.Warnings + " - " + warningCount;
        checkPanel.controls.InformationButton.caption.innerHTML = this.jsObject.loc.Report.InformationMessages + " - " + informationCount;
        checkPanel.controls.ReportRenderingMessageButton.caption.innerHTML = this.jsObject.loc.Report.ReportRenderingMessages + " - " + reportRenderingMessageCount;

        if (this.childNodes.length == 0) {
            var noIssues = document.createElement("div");
            noIssues.innerHTML = this.jsObject.loc.Report.NoIssues;
            noIssues.style.textAlign = "center";
            noIssues.style.marginTop = "200px";
            this.appendChild(noIssues);
        }
    }
        
    return container;
}

StiMobileDesigner.prototype.ReportCheckContainerItem = function (num, checkObject, checkPanel) {
    var checkItem = document.createElement("div");
    checkItem.className = "stiDesignerCheckContainerItem";

    var mainTable = this.CreateHTMLTable();
    mainTable.style.width = "100%";
    checkItem.appendChild(mainTable);
    var upTable = this.CreateHTMLTable();
    mainTable.addCell(upTable);

    //Image
    var img = document.createElement("img");
    img.style.marginRight = "8px";
    img.src = this.options.images["ReportChecker." + checkObject.status + "32.png"];
    upTable.addCell(img);

    var shortMessage = checkObject.shortMessage;
    if (!shortMessage) {
        switch (checkObject.status) {
            case "Error": shortMessage = this.loc.Report.Errors; break;
            case "Warning": shortMessage = this.loc.Report.Warnings; break;
            case "Information": shortMessage = this.loc.Report.InformationMessages; break;
            case "ReportRenderingMessage": shortMessage = this.loc.Report.ReportRenderingMessages; break;
        }
    }

    var messageText = num + ". <b>" + shortMessage + "</b><br>" + checkObject.longMessage;
    var textCell = upTable.addTextCell(messageText);
        
    var downTable = this.CreateHTMLTable();
    downTable.style.float = "right";
    mainTable.addCellInNextRow(downTable);

    //Actions
    for (var i = 0; i < checkObject.actions.length; i++) {
        if (checkObject.actions[i].name == "Edit") continue; //Temporary
        var actionButton = this.SmallButton(null, null, checkObject.actions[i].name, null, checkObject.actions[i].description, null, this.GetStyles("FormButton"), true);
        actionButton.actionIndex = i;
        actionButton.style.marginLeft = "5px";
        downTable.addCell(actionButton);

        actionButton.action = function () {
            var this_ = this;
            this.setEnabled(false);
            this.jsObject.SendCommandActionCheck(checkObject.index, this.actionIndex, function (answer) {
                this_.setEnabled(true);
                if (answer.checkItems) {
                    checkPanel.container.fill(answer.checkItems);
                }

                if (answer.reportGuid && answer.reportObject) {
                    actionButton.jsObject.options.report = null;
                    actionButton.jsObject.options.reportGuid = answer.reportGuid;
                    actionButton.jsObject.LoadReport(actionButton.jsObject.ParseReport(answer.reportObject), true);
                    actionButton.jsObject.options.reportIsModified = true;
                }
                actionButton.jsObject.BackToSelectedComponent(answer.selectedObjectName);
            });
        }
    }

    //Clipboard Button    
    var clipboardButton = this.SmallButton(null, null, null, "Copy.png", this.loc.HelpDesigner.CopyToClipboard, null, this.GetStyles("FormButton"), true);
    clipboardButton.style.marginLeft = "5px";
    clipboardButton.action = function () {
        this.jsObject.copyTextToClipboard(num + "." + shortMessage + "\r\n" + checkObject.longMessage);
    }
    downTable.addCell(clipboardButton);

    //View Button
    if (checkObject.previewVisible) {
        var viewButton = this.SmallButton(null, null, null, "View.png", this.loc.Toolbars.TabView, null, this.GetStyles("FormButton"), true);
        viewButton.style.marginLeft = "5px";
        downTable.addCell(viewButton);
        
        viewButton.action = function () {
            viewButton.setEnabled(false);
            if (this.jsObject.options.jsMode) {
                viewButton.setEnabled(true);
                if (checkPanel.isVisible) {
                    var pageIndex = 0;
                    if (checkObject.pageIndex != null) pageIndex = checkObject.pageIndex; 
                    var pageSvg = this.jsObject.GetSvgPageForCheckPreview(pageIndex, checkObject.elementName);
                    var checkPreviewPanel = viewButton.jsObject.InitializeCheckPreviewPanel(viewButton, null);                    
                    checkPreviewPanel.appendChild(pageSvg);
                    checkPreviewPanel.show();
                }
            }
            else {
                this.jsObject.SendCommandGetCheckPreview(checkObject.index, function (answer) {
                    viewButton.setEnabled(true);
                    if (checkPanel.isVisible) {
                        var checkPreviewPanel = viewButton.jsObject.InitializeCheckPreviewPanel(viewButton, answer.previewImage);
                        checkPreviewPanel.show();
                    }
                });
            }
        }
    }

    return checkItem;
}

StiMobileDesigner.prototype.InitializeCheckPreviewPanel = function (parentButton, previewImageSrc) {
    var checkPreviewPanel = document.createElement("div");
    if (this.options.checkPreviewPanel) {
        this.options.checkPreviewPanel.hide();
    }

    this.options.checkPreviewPanel = checkPreviewPanel;
    this.options.mainPanel.appendChild(checkPreviewPanel);
    checkPreviewPanel.className = "stiDesignerCheckPreviewPanel";
    checkPreviewPanel.jsObject = this;

    checkPreviewPanel.hide = function (){
        this.jsObject.options.mainPanel.removeChild(this);
        this.jsObject.options.checkPreviewPanel = null;
        parentButton.setSelected(false);
    }

    checkPreviewPanel.show = function () {
        checkPreviewPanel.style.left = (this.jsObject.FindPosX(parentButton, "stiDesignerMainPanel") + parentButton.offsetWidth + 5) + "px";
        var top = (this.jsObject.FindPosY(parentButton, "stiDesignerMainPanel") - checkPreviewPanel.offsetHeight - 5);
        checkPreviewPanel.style.top = (top < 0 ? 0 : top) + "px";
        parentButton.setSelected(true);
    }

    if (previewImageSrc) {
        var previewImage = document.createElement("img");
        previewImage.src = previewImageSrc;
        checkPreviewPanel.appendChild(previewImage);
    }           

    return checkPreviewPanel;
}

StiMobileDesigner.prototype.GetSvgPageForCheckPreview = function (pageIndex, elementName, zoom, onlyPage, pageProperties) {
    var oldPaintPanelPadding = this.options.paintPanelPadding;
    this.options.paintPanelPadding = 0;
    var previewZoom = zoom || 0.4;

    var page = this.options.paintPanel.findPageByIndex(pageIndex);

    var pageObject = {
        name: page.properties.name,
        pageIndex: page.properties.pageIndex,
        properties: pageProperties || this.CopyObject(page.properties),
        components: page.components
    }

    var showGrid = this.options.report.info.showGrid;
    this.options.report.info.showGrid = false;

    var pageSvg = this.CreatePage(pageObject);
    pageSvg.onmousedown = null;
    pageSvg.ondblclick = null;
    pageSvg.onmousedown = null;
    pageSvg.onmousemove = null;
    pageSvg.onmouseup = null;
    pageSvg.style.display = "";
    pageSvg.className = "";
    pageSvg.style.boxShadow = "";
    pageSvg.repaint();

    this.options.report.info.showGrid = showGrid;

    if (!onlyPage) {
        for (var compName in page.components) {
            var cloneComponent = this.CloneComponent(page.components[compName]);
            cloneComponent.onmousedown = null;
            cloneComponent.ondblclick = null;
            cloneComponent.onmousedown = null;
            cloneComponent.onmousemove = null;
            cloneComponent.onmouseup = null;
            if (compName == elementName) {
                cloneComponent.properties.border = "1,1,1,1!3!255,0,0!0!0!4";
            }
            pageSvg.appendChild(cloneComponent);
            cloneComponent.repaint();
        }
    }

    var width = parseInt(pageSvg.getAttribute("width"));
    var height = parseInt(pageSvg.getAttribute("height"));
    pageSvg.setAttribute("viewBox", "0,0," + width + "," + height);
    pageSvg.setAttribute("width", (previewZoom * width) / this.options.report.zoom);
    pageSvg.setAttribute("height", (previewZoom * height) / this.options.report.zoom);

    this.options.paintPanelPadding = oldPaintPanelPadding;

    return pageSvg
}