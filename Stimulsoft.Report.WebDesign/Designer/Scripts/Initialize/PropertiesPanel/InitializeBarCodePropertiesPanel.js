﻿
StiMobileDesigner.prototype.BarCodePropertiesPanel = function () {
    var panel = document.createElement("div");
    panel.jsObject = this;
    panel.style.display = "none";

    var groupNames = [
        ["BarCode", this.loc.PropertyCategory.BarCodeCategory],
        ["Misc", this.loc.PropertyCategory.MiscCategory]
    ]

    this.AddGroupsToPropertiesPanel(groupNames, panel);
    panel.groups.Misc.changeOpenedState(true);

    var properties = [
        ["aspectRatio", this.loc.PropertyMain.AspectRatio, this.PropertyTextBox(null, this.options.propertyNumbersControlWidth), panel.groups["Misc"], "Textbox"],
        ["autoDataColumns", this.loc.PropertyMain.AutoDataColumns, this.CheckBox(null), panel.groups["Misc"], "Checkbox"],
        ["autoDataRows", this.loc.PropertyMain.AutoDataRows, this.CheckBox(null), panel.groups["Misc"], "Checkbox"],
        ["dataColumns", this.loc.PropertyMain.DataColumns, this.PropertyTextBox(null, this.options.propertyNumbersControlWidth), panel.groups["Misc"], "Textbox"],
        ["dataRows", this.loc.PropertyMain.DataRows, this.PropertyTextBox(null, this.options.propertyNumbersControlWidth), panel.groups["Misc"], "Textbox"],
        ["encodingMode", this.loc.PropertyMain.EncodingMode,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodeEncodingModeItems(), true, false), panel.groups["Misc"], "DropdownList"],
        ["errorsCorrectionLevel", this.loc.PropertyMain.ErrorsCorrectionLevel,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodeErrorsCorrectionLevelItems(), true, false), panel.groups["Misc"], "DropdownList"],
        ["errorCorrectionLevel", this.loc.PropertyMain.ErrorCorrectionLevel,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodeErrorCorrectionLevelItems(), true, false), panel.groups["Misc"], "DropdownList"],
        ["ratioY", this.loc.PropertyMain.RatioY, this.PropertyTextBox(null, this.options.propertyNumbersControlWidth), panel.groups["Misc"], "Textbox"],
        ["checksum", this.loc.PropertyMain.Checksum,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodeChecksumItems(), true, false), panel.groups["Misc"], "DropdownList"],
        ["checkSum", this.loc.PropertyMain.Checksum,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodeCheckSumItems(), true, false), panel.groups["Misc"], "DropdownList"],
        ["checkSum1", this.loc.PropertyMain.CheckSum1,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodePlesseyCheckSumItems(), true, false), panel.groups["Misc"], "DropdownList"],
        ["checkSum2", this.loc.PropertyMain.CheckSum2,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodePlesseyCheckSumItems(), true, false), panel.groups["Misc"], "DropdownList"],
        ["supplementCode", this.loc.PropertyMain.SupplementCode, this.PropertyTextBox(null, this.options.propertyControlWidth), panel.groups["BarCode"], "Textbox"],
        ["encodingType", this.loc.PropertyMain.EncodingType,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodeEncodingTypeItems(), true, false), panel.groups["Misc"], "DropdownList"],
        ["matrixSize", this.loc.PropertyMain.MatrixSize,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodeMatrixSizeDataMatrixItems(), true, false), panel.groups["Misc"], "DropdownList"],        
        ["height", this.loc.PropertyMain.Height, this.PropertyTextBox(null, this.options.propertyNumbersControlWidth), panel.groups["Misc"], "Textbox"],
        ["module", this.loc.PropertyMain.Module, this.PropertyTextBox(null, this.options.propertyNumbersControlWidth), panel.groups["Misc"], "Textbox"],
        ["printVerticalBars", this.loc.PropertyMain.PrintVerticalBars, this.CheckBox(null), panel.groups["Misc"], "Checkbox"],
        ["ratio", this.loc.PropertyMain.Ratio, this.PropertyTextBox(null, this.options.propertyNumbersControlWidth), panel.groups["Misc"], "Textbox"],
        ["useRectangularSymbols", this.loc.PropertyMain.UseRectangularSymbols, this.CheckBox(null), panel.groups["Misc"], "Checkbox"],
        ["showQuietZoneIndicator", this.loc.PropertyMain.ShowQuietZoneIndicator, this.CheckBox(null), panel.groups["Misc"], "Checkbox"],
        ["supplementType", this.loc.PropertyMain.SupplementType,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodeSupplementTypeItems(), true, false), panel.groups["Misc"], "DropdownList"],
        ["addClearZone", this.loc.PropertyMain.AddClearZone, this.CheckBox(null), panel.groups["Misc"], "Checkbox"],
        ["space", this.loc.PropertyMain.Space, this.PropertyTextBox(null, this.options.propertyNumbersControlWidth), panel.groups["Misc"], "Textbox"],
        ["companyPrefix", this.loc.PropertyMain.CompanyPrefix, this.PropertyTextBox(null, this.options.propertyControlWidth), panel.groups["Misc"], "Textbox"],
        ["extensionDigit", this.loc.PropertyMain.ExtensionDigit, this.PropertyTextBox(null, this.options.propertyControlWidth), panel.groups["Misc"], "Textbox"],
        ["serialNumber", this.loc.PropertyMain.SerialNumber, this.PropertyTextBox(null, this.options.propertyControlWidth), panel.groups["Misc"], "Textbox"],
        ["mode", this.loc.PropertyMain.Mode,
            this.PropertyDropDownList(null, this.options.propertyControlWidth, this.GetBarCodeModeItems(), true, false), panel.groups["Misc"], "DropdownList"],
        ["processTilde", this.loc.PropertyMain.ProcessTilde, this.CheckBox(null), panel.groups["Misc"], "Checkbox"],
        ["structuredAppendPosition", this.loc.PropertyMain.StructuredAppendPosition, this.PropertyTextBox(null, this.options.propertyNumbersControlWidth), panel.groups["Misc"], "Textbox"],
        ["structuredAppendTotal", this.loc.PropertyMain.StructuredAppendTotal, this.PropertyTextBox(null, this.options.propertyNumbersControlWidth), panel.groups["Misc"], "Textbox"],
        ["trimExcessData", this.loc.PropertyMain.TrimExcessData, this.CheckBox(null), panel.groups["Misc"], "Checkbox"]
    ]

    for (var i = 0; i < properties.length; i++) {
        var property = this.AddBarCodePropertyToPropertiesGroup(properties[i][0], properties[i][1], properties[i][2], properties[i][3], properties[i][4]);
    }

    panel.updateProperties = function (propertiesValues) {
        if (propertiesValues == null) propertiesValues = {};
        this.propertiesValues = propertiesValues;
        var propertiesGroups = this.groups;
        var barCodeForm = this.jsObject.options.forms.barCodeForm;

        for (var groupName in propertiesGroups) {
            var propertyGroup = propertiesGroups[groupName];
            var showGroup = false;
            for (var propertyName in propertyGroup.properties) {
                var property = propertyGroup.properties[propertyName];
                if (property.isUserProperty) {
                    var showProperty = propertiesValues[property.name] != null;

                    //Exceptions
                    if (property.name == "height" && (
                        barCodeForm.barCode.codeType == "StiQRCodeBarCodeType" ||
                        barCodeForm.barCode.codeType == "StiMaxicodeBarCodeType" ||
                        barCodeForm.barCode.codeType == "StiPdf417BarCodeType" ||
                        barCodeForm.barCode.codeType == "StiDataMatrixBarCodeType" ||
                        barCodeForm.barCode.codeType == "StiAustraliaPost4StateBarCodeType" ||
                        barCodeForm.barCode.codeType == "StiFIMBarCodeType"))
                        showProperty = false;

                    if (property.name == "module" && (
                        barCodeForm.barCode.codeType == "StiMaxicodeBarCodeType" ||
                        barCodeForm.barCode.codeType == "StiFIMBarCodeType" ||
                        barCodeForm.barCode.codeType == "StiPharmacodeBarCodeType"))
                        showProperty = false;

                    property.style.display = showProperty ? "" : "none";
                    if (showProperty) {
                        showGroup = true;
                        if (property.name == "matrixSize") {
                            property.control.addItems(barCodeForm && barCodeForm.barCode.codeType == "StiQRCodeBarCodeType" 
                                ? this.jsObject.GetBarCodeMatrixSizeQRCodeItems() : this.jsObject.GetBarCodeMatrixSizeDataMatrixItems());
                        }
                        this.jsObject.SetPropertyValue(property, propertiesValues[property.name]);
                    }
                }
            }
            propertyGroup.style.display = showGroup ? "" : "none";
        }
    }

    return panel;
}

StiMobileDesigner.prototype.AddBarCodePropertyToPropertiesGroup = function (propertyName, propertyCaption, propertyControl, propertyGroup, controlType) {
    var property = this.AddPropertyToPropertiesGroup(propertyName, propertyCaption, propertyControl, propertyGroup, controlType);
    
    propertyControl.action = function () {
        var barCodeForm = property.jsObject.options.forms.barCodeForm;
        var editBarCodePropertiesPanel = property.jsObject.options.propertiesPanel.editBarCodePropertiesPanel;
        
        if (barCodeForm && barCodeForm.visible && editBarCodePropertiesPanel && editBarCodePropertiesPanel.propertiesValues) {
            barCodeForm.applyBarCodeProperties([{ name: "BarCodeType." + property.jsObject.UpperFirstChar(this.property.name), value: this.property.getValue() }]);
        }
    }

    return property;
}