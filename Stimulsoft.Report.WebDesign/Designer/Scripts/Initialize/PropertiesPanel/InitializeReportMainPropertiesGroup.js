﻿
StiMobileDesigner.prototype.ReportMainPropertiesGroup = function () {
    var mainPropertiesGroup = this.PropertiesGroup("reportMainPropertiesGroup", this.loc.PropertyCategory.MainCategory);
    mainPropertiesGroup.style.margin = "5px 0 5px 0";
    mainPropertiesGroup.style.display = "none";

    var properties = [];
    properties.push(["AutoLocalizeReportOnRun", this.loc.PropertyMain.AutoLocalizeReportOnRun, this.CheckBox("controlReportPropertyAutoLocalizeReportOnRun"), "Checkbox"]);
    properties.push(["CacheAllData", this.loc.PropertyMain.CacheAllData, this.CheckBox("controlReportPropertyCacheAllData"), "Checkbox"]);
    properties.push(["CacheTotals", "Cache Totals", this.CheckBox("controlReportPropertyCacheTotals"), "Checkbox"]);
    if (!this.options.jsMode) {
        //if (!this.options.cloudMode) {
            properties.push(["CalculationMode", this.loc.PropertyMain.CalculationMode, this.PropertyDropDownList("controlReportPropertyCalculationMode", this.options.propertyControlWidth, this.GetCalculationModeItems(), true), "DropdownList"]);
        //}
        properties.push(["ConvertNulls", this.loc.PropertyMain.ConvertNulls, this.CheckBox("controlReportPropertyConvertNulls"), "Checkbox"]);
    }    
    properties.push(["Collate", this.loc.PropertyMain.Collate, this.PropertyTextBox("controlReportPropertyCollate", this.options.propertyNumbersControlWidth), "Textbox"]);
    properties.push(["Culture", this.loc.PropertyMain.Culture, this.PropertyDropDownList("controlReportPropertyCulture", this.options.propertyControlWidth, this.GetCultureItems(), true), "DropdownList"]);
    properties.push(["GlobalizationStrings", this.loc.PropertyMain.GlobalizationStrings, this.PropertyTextBoxWithEditButton("controlReportPropertyGlobalizationStrings", this.options.propertyControlWidth, true), "TextBoxWithEditButton"]);
    if (!this.options.cloudMode)
        properties.push(["EngineVersion", this.loc.PropertyMain.EngineVersion, this.PropertyDropDownList("controlReportPropertyEngineVersion", this.options.propertyControlWidth, this.GetEngineVersionItems(), true), "DropdownList"]);
    properties.push(["NumberOfPass", this.loc.PropertyMain.NumberOfPass, this.PropertyDropDownList("controlReportPropertyNumberOfPass", this.options.propertyControlWidth, this.GetNumberOfPassItems(), true), "DropdownList"]);
    //properties.push(["PreviewMode", this.loc.PropertyMain.PreviewMode, this.PropertyDropDownList("controlReportPropertyPreviewMode", this.options.propertyControlWidth, this.GetPreviewModeItems(), true), "DropdownList"]);
    if (!this.options.cloudMode)
        properties.push(["refreshTime", this.loc.PropertyMain.RefreshTime, this.PropertyDropDownList("controlReportPropertyRefreshTime", this.options.propertyControlWidth, this.GetRefreshTimeItems()), "DropdownList"]);
    if (!this.options.jsMode)
        properties.push(["ReportCacheMode", this.loc.PropertyMain.ReportCacheMode, this.PropertyDropDownList("controlReportPropertyReportCacheMode", this.options.propertyControlWidth, this.GetReportCacheModeItems(), true), "DropdownList"]);
    properties.push(["ReportUnit", this.loc.PropertyMain.ReportUnit, this.PropertyDropDownList("controlReportPropertyReportUnit", this.options.propertyControlWidth, this.GetUnitItems(), true), "DropdownList"]);
    properties.push(["RetrieveOnlyUsedData", this.loc.PropertyMain.RetrieveOnlyUsedData, this.CheckBox("controlReportPropertyRetrieveOnlyUsedData"), "Checkbox"]);
    if (!this.options.jsMode)
        properties.push(["ParametersOrientation", this.loc.PropertyMain.ParametersOrientation, this.PropertyDropDownList("controlReportPropertyParametersOrientation", this.options.propertyControlWidth, this.GetParametersOrientationItems(), true), "DropdownList"]);
    properties.push(["RequestParameters", this.loc.PropertyMain.RequestParameters, this.CheckBox("controlReportPropertyRequestParameters"), "Checkbox"]);
    if (!this.options.cloudMode && !this.options.jsMode)
        properties.push(["ScriptLanguage", this.loc.PropertyMain.ScriptLanguage, this.PropertyDropDownList("controlReportPropertyScriptLanguage", this.options.propertyControlWidth, this.GetScriptLanguageItems(), true), "DropdownList"]);
    properties.push(["StopBeforePage", this.loc.PropertyMain.StopBeforePage, this.PropertyTextBox("controlReportPropertyStopBeforePage", this.options.propertyNumbersControlWidth), "Textbox"]);
    //if (!this.options.cloudMode && !this.options.jsMode)
    //    properties.push(["StoreImagesInResources", this.loc.PropertyMain.StoreImagesInResources, this.CheckBox("controlReportPropertyStoreImagesInResources"), "Checkbox"]);

    var jsObject = this;

    for (var i = 0; i < properties.length; i++) {
        var control = properties[i][2];
        control.propertyName = this.LowerFirstChar(properties[i][0]);
        control.controlType = properties[i][3];
        mainPropertiesGroup.container.appendChild(this.Property(control.propertyName, properties[i][1], control));

        mainPropertiesGroup.jsObject.AddMainMethodsToPropertyControl(control);
                
        control.action = function () {
            if (this.propertyName == "reportUnit") {
                jsObject.SendCommandChangeUnit(this.key);
            }
            else {
                if (this.propertyName == "refreshTime") this.textBox.value = this.key;    
                jsObject.options.selectedObject.properties[this.propertyName] = this.getValue();
                jsObject.SendCommandSetReportProperties([this.propertyName]);
            }
        }

        if (control.propertyName == "globalizationStrings") {
            control.textBox.value = "(" + jsObject.loc.PropertyMain.GlobalizationStrings + ")";
            control.button.action = function () {
                jsObject.SendCommandGetGlobalizationStrings(function (answer) {
                    jsObject.InitializeGlobalizationEditorForm(function (globalizationEditorForm) {
                        globalizationEditorForm.show(answer);
                    })
                });
            }
        }
    }

    return mainPropertiesGroup;
}