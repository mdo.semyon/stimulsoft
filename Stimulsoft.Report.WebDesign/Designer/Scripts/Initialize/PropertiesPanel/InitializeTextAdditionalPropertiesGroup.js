﻿
StiMobileDesigner.prototype.TextAdditionalPropertiesGroup = function () {
    var textAdditionalPropertiesGroup = this.PropertiesGroup("textAdditionalPropertiesGroup", this.loc.PropertyCategory.TextAdditionalCategory);
    textAdditionalPropertiesGroup.style.margin = "5px 0 5px 0";
    textAdditionalPropertiesGroup.style.display = "none";
        
    //AllowHtmlTags
    var controlPropertyAllowHtmlTags = this.CheckBox("controlPropertyAllowHtmlTags");
    controlPropertyAllowHtmlTags.action = function() {
        this.jsObject.ApplyPropertyValue("allowHtmlTags", this.isChecked);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("allowHtmlTags", this.loc.PropertyMain.AllowHtmlTags, controlPropertyAllowHtmlTags));
    
    //Text Angle
    var controlPropertyTextAngle = this.PropertyTextBox("controlPropertyTextAngle", this.options.propertyNumbersControlWidth);
    controlPropertyTextAngle.action = function() {        
        this.value = Math.abs(this.jsObject.StrToInt(this.value));
        this.jsObject.ApplyPropertyValue("textAngle", this.value);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("textAngle", this.loc.PropertyMain.Angle, controlPropertyTextAngle, "Angle"));
        
    //EditableText
    var controlPropertyEditableText = this.CheckBox("controlPropertyEditableText");
    controlPropertyEditableText.action = function() {
        this.jsObject.ApplyPropertyValue("editableText", this.isChecked);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("editableText", this.loc.PropertyMain.Editable, controlPropertyEditableText, "Editable"));
    
    //Hide Zeros
    var controlPropertyHideZeros = this.CheckBox("controlPropertyHideZeros");
    controlPropertyHideZeros.action = function() {
        this.jsObject.ApplyPropertyValue("hideZeros", this.isChecked);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("hideZeros", this.loc.PropertyMain.HideZeros, controlPropertyHideZeros));

    //Line Spacing
    var controlPropertyLineSpacing = this.PropertyTextBox("controlPropertyLineSpacing", this.options.propertyNumbersControlWidth);
    controlPropertyLineSpacing.action = function () {
        this.value = Math.abs(this.jsObject.StrToDouble(this.value));
        this.jsObject.ApplyPropertyValue("lineSpacing", this.value);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("lineSpacing", this.loc.PropertyMain.LineSpacing, controlPropertyLineSpacing));

    //Text Margins
    var controlPropertyTextMargins = this.PropertyMarginsControl("controlPropertyTextMargins", this.options.propertyControlWidth + 61);
    controlPropertyTextMargins.action = function() {
        this.jsObject.ApplyPropertyValue("textMargins", this.getValue());
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("textMargins", this.loc.PropertyMain.Margins, controlPropertyTextMargins, "Margins"));
    
    //MaxNumberOfLines
    var controlPropertyMaxNumberOfLines = this.PropertyTextBox("controlPropertyMaxNumberOfLines", this.options.propertyNumbersControlWidth);
    controlPropertyMaxNumberOfLines.action = function() {        
        this.value = Math.abs(this.jsObject.StrToInt(this.value));
        this.jsObject.ApplyPropertyValue("maxNumberOfLines", this.value);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("maxNumberOfLines", this.loc.PropertyMain.MaxNumberOfLines, controlPropertyMaxNumberOfLines));
    
    //Only Text
    var controlPropertyOnlyText = this.CheckBox("controlPropertyOnlyText");
    controlPropertyOnlyText.action = function() {
        this.jsObject.ApplyPropertyValue("onlyText", this.isChecked);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("onlyText", this.loc.PropertyMain.OnlyText, controlPropertyOnlyText));

    //ProcessAt
    var controlPropertyProcessAt = this.PropertyDropDownList("controlPropertyProcessAt", this.options.propertyControlWidth, this.GetProcessAtItems(), true, false);
    controlPropertyProcessAt.action = function () {
        this.jsObject.ApplyPropertyValue("processAt", this.key);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("processAt", this.loc.PropertyMain.ProcessAt, controlPropertyProcessAt));

    //ProcessingDuplicates
    var controlPropertyProcessingDuplicates = this.PropertyDropDownList("controlPropertyProcessingDuplicates", this.options.propertyControlWidth, this.GetProcessingDuplicatesItems(), true, false);
    controlPropertyProcessingDuplicates.action = function () {
        this.jsObject.ApplyPropertyValue("processingDuplicates", this.key);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("processingDuplicates", this.loc.PropertyMain.ProcessingDuplicates, controlPropertyProcessingDuplicates));

    //RenderTo
    var controlPropertyRenderTo = this.PropertyDropDownList("controlPropertyRenderTo", this.options.propertyControlWidth, null, true, false);
    controlPropertyRenderTo.action = function () {
        this.jsObject.ApplyPropertyValue("renderTo", this.key);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("renderTo", this.loc.PropertyMain.RenderTo, controlPropertyRenderTo));

    //ShrinkFontToFit
    var controlPropertyShrinkFontToFit = this.CheckBox("controlPropertyShrinkFontToFit");
    controlPropertyShrinkFontToFit.action = function () {
        this.jsObject.ApplyPropertyValue("shrinkFontToFit", this.isChecked);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("shrinkFontToFit", this.loc.PropertyMain.ShrinkFontToFit, controlPropertyShrinkFontToFit));

    //ShrinkFontToFitMinimumSize
    var controlPropertyShrinkFontToFitMinimumSize = this.PropertyTextBox("controlPropertyShrinkFontToFitMinimumSize", this.options.propertyNumbersControlWidth);
    controlPropertyShrinkFontToFitMinimumSize.action = function () {
        this.value = Math.abs(this.jsObject.StrToDouble(this.value));
        this.jsObject.ApplyPropertyValue("shrinkFontToFitMinimumSize", this.value);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("shrinkFontToFitMinimumSize", this.loc.PropertyMain.ShrinkFontToFitMinimumSize, controlPropertyShrinkFontToFitMinimumSize));

    //RightToLeftText
    var controlPropertyRightToLeftText = this.CheckBox("controlPropertyRightToLeftText");
    controlPropertyRightToLeftText.action = function () {
        this.jsObject.ApplyPropertyValue("rightToLeft", this.isChecked);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("rightToLeftText", this.loc.PropertyMain.RightToLeft, controlPropertyRightToLeftText, "RightToLeft"));
            
    //Continuous Text
    var controlPropertyContinuousText = this.CheckBox("controlPropertyContinuousText");
    controlPropertyContinuousText.action = function() {
        this.jsObject.ApplyPropertyValue("continuousText", this.isChecked);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("continuousText", this.loc.PropertyMain.ContinuousText, controlPropertyContinuousText));
    
    //Word Wrap
    var controlPropertyWordWrap = this.CheckBox("controlPropertyWordWrap");
    controlPropertyWordWrap.action = function () {
        this.jsObject.ApplyPropertyValue("wordWrap", this.isChecked);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("wordWrap", this.loc.PropertyMain.WordWrap, controlPropertyWordWrap));
        
    //Trimming
    var controlPropertyTrimming = this.PropertyDropDownList("controlPropertyTrimming", this.options.propertyControlWidth, this.GetTrimmingItems(), true, false);
    controlPropertyTrimming.action = function () {
        this.jsObject.ApplyPropertyValue("trimming", this.key);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("trimming", this.loc.PropertyMain.Trimming, controlPropertyTrimming));

    //TextOptionsRightToLeft
    var controlPropertyTextOptionsRightToLeft = this.CheckBox("controlPropertyTextOptionsRightToLeft");
    controlPropertyTextOptionsRightToLeft.action = function () {
        this.jsObject.ApplyPropertyValue("textOptionsRightToLeft", this.isChecked);
    }
    textAdditionalPropertiesGroup.container.appendChild(this.Property("textOptionsRightToLeft", this.loc.PropertyMain.RightToLeft, controlPropertyTextOptionsRightToLeft, "RightToLeft"));

    return textAdditionalPropertiesGroup;
}