﻿
StiMobileDesigner.prototype.ReportDescriptionPropertiesGroup = function () {
    var descriptionPropertiesGroup = this.PropertiesGroup("reportDescriptionPropertiesGroup", this.loc.PropertyCategory.DescriptionCategory);
    descriptionPropertiesGroup.style.margin = "5px 0 5px 0";
    descriptionPropertiesGroup.style.display = "none";

    var properties = [
        ["ReportName", this.loc.PropertyMain.ReportName, this.PropertyTextBox("controlReportPropertyReportName", this.options.propertyControlWidth), "Textbox"],
        ["ReportAlias", this.loc.PropertyMain.ReportAlias, this.PropertyTextBox("controlReportPropertyReportAlias", this.options.propertyControlWidth), "Textbox"],
        ["ReportAuthor", this.loc.PropertyMain.ReportAuthor, this.PropertyTextBox("controlReportPropertyReportAuthor", this.options.propertyControlWidth), "Textbox"],
        ["ReportDescription", this.loc.PropertyMain.ReportDescription, this.PropertyTextBox("controlReportPropertyReportDescription", this.options.propertyControlWidth), "Textbox"]
    ]

    for (var i = 0; i < properties.length; i++) {
        var control = properties[i][2];
        control.propertyName = this.LowerFirstChar(properties[i][0]);
        control.controlType = properties[i][3];
        descriptionPropertiesGroup.container.appendChild(this.Property(control.propertyName, properties[i][1], control));

        descriptionPropertiesGroup.jsObject.AddMainMethodsToPropertyControl(control);

        control.action = function () {
            this.jsObject.options.selectedObject.properties[this.propertyName] = this.getValue();
            this.jsObject.SendCommandSetReportProperties([this.propertyName]);
        }
    }

    return descriptionPropertiesGroup;
}