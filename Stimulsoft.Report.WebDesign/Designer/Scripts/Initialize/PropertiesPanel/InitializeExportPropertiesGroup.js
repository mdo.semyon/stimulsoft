﻿
StiMobileDesigner.prototype.ExportPropertiesGroup = function () {
    var exportPropertiesGroup = this.PropertiesGroup("exportPropertiesGroup", this.loc.PropertyCategory.ExportCategory);
    exportPropertiesGroup.style.margin = "5px 0 5px 0";
    exportPropertiesGroup.style.display = "none";

    //ExcelValue
    var controlPropertyExcelValue = this.PropertyExpressionControl("controlPropertyExcelValue", this.options.propertyControlWidth, false);
    controlPropertyExcelValue.action = function () {
        this.jsObject.ApplyPropertyValue("excelValue", Base64.encode(this.textBox.value));
    }
    exportPropertiesGroup.container.appendChild(this.Property("excelValue", this.loc.PropertyMain.ExcelValue, controlPropertyExcelValue));
    

    //ExportAsImage
    var controlPropertyExportAsImage = this.CheckBox("controlPropertyExportAsImage");
    controlPropertyExportAsImage.action = function () {
        this.jsObject.ApplyPropertyValue("exportAsImage", this.isChecked);
    }
    exportPropertiesGroup.container.appendChild(this.Property("exportAsImage", this.loc.PropertyMain.ExportAsImage, controlPropertyExportAsImage));

    return exportPropertiesGroup;
}