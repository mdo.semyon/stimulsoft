﻿
StiMobileDesigner.prototype.PropertyBrushControl = function (name, toolTip, width) {
    var brushControl = this.CreateHTMLTable();
    brushControl.jsObject = this;
    brushControl.name = name;
    brushControl.key = null;
    brushControl.isEnabled = true;
    this.options.controls[name] = brushControl;

    brushControl.button = this.SmallButton(name + "Button", null, this.loc.Report.StiSolidBrush,
        "BrushSolid.png", toolTip, "Down", this.GetStyles("PropertiesBrushControlButton"));
    brushControl.button.style.width = (width + 4) + "px";
    brushControl.button.caption.style.width = "100%";
    brushControl.addCell(brushControl.button);
    brushControl.menu = this.BrushMenu(name + "BrushMenu", brushControl.button);
    brushControl.menu.brushControl = brushControl;
    brushControl.button.brushMenu = brushControl.menu;

    //Override image
    var colorBar = document.createElement("div");
    colorBar.className = "stiImageControlBrushPropertyForSolid";

    var imageCell = brushControl.button.image.parentElement;
    imageCell.removeChild(brushControl.button.image);
    imageCell.appendChild(colorBar);
    brushControl.button.image = colorBar;

    brushControl.setEnabled = function (state) {
        if (this.button.image) this.button.image.style.opacity = state ? "1" : "0.3";
        if (this.button.arrow) this.button.arrow.style.opacity = state ? "1" : "0.3";
        this.isEnabled = state;
        this.button.isEnabled = state;
        this.button.className = (state ? this.button.styles["default"] : this.button.styles["disabled"]) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    brushControl.button.action = function () {
        this.brushMenu.changeVisibleState(!this.brushMenu.visible);
    }

    brushControl.action = function () { }

    brushControl.setKey = function (key) {
        this.key = key;
        this.button.key = key;
        this.button.image.style.opacity = 1;
        if (key == "StiEmptyValue") {
            this.button.image.style.background = "#ffffff";
            this.button.caption.innerHTML = "";
            return;
        }
        if (key.indexOf("1!") == 0) {
            var brushColor = this.jsObject.GetColorFromBrushStr(key);
            var color;
            if (brushColor == "transparent")
                color = "255,255,255";
            else {
                var colors = brushColor.split(",");
                if (colors.length == 4) {
                    this.button.image.style.opacity = this.jsObject.StrToInt(colors[0]) / 255;
                    colors.splice(0, 1);
                }
                color = colors[0] + "," + colors[1] + "," + colors[2];
            }
            this.button.caption.innerHTML = this.jsObject.loc.Report.StiSolidBrush;
            this.button.image.className = "stiImageControlBrushPropertyForSolid";
            this.button.image.style.background = "rgb(" + color + ")";
        }
        else {
            var brushArray = key.split("!");
            var brushType = "Solid";
            switch (brushArray[0]) {
                case "0": brushType = "Empty"; break;
                case "2": brushType = "Hatch"; break;
                case "3": brushType = "Gradient"; break;
                case "4": brushType = "Glare"; break;
                case "5": brushType = "Glass"; break;
            }
            this.button.image.className = "stiImageControlBrushPropertyForNotSolid";
            this.button.image.style.background = "url(" + this.jsObject.options.images["Brush" + brushType + ".png"] + ")";
            this.button.caption.innerHTML = this.jsObject.loc.Report["Sti" + brushType + "Brush"];
        }
    }

    brushControl.menu.action = function () {
        this.brushControl.setKey(this.parentButton.key);
        this.brushControl.action();
    }

    return brushControl;
}