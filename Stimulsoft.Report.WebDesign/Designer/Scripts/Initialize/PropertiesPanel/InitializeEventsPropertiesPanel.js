﻿
StiMobileDesigner.prototype.EventsPropertiesPanel = function () {
    var eventsPropertiesPanel = document.createElement("div");
    eventsPropertiesPanel.style.display = "none";
    eventsPropertiesPanel.jsObject = this;

    eventsPropertiesPanel.show = function () {
        this.style.display = "";
        this.update();
    }

    eventsPropertiesPanel.hide = function () {
        this.style.display = "none";
    }

    eventsPropertiesPanel.selectedComponentParams = function () {
        var selectedObjects = this.jsObject.options.selectedObject ? [this.jsObject.options.selectedObject] : this.jsObject.options.selectedObjects;
        var selectedComponentParams = [];

        if (selectedObjects) {
            for (var i = 0; i < selectedObjects.length; i++) {
                selectedComponentParams.push({
                    name: selectedObjects[i].properties.name,
                    typeComponent: selectedObjects[i].typeComponent
                });
            }
        }

        return selectedComponentParams;
    }

    eventsPropertiesPanel.update = function () {
        if (this.groups) {
            for (var groupName in this.groups)
                this.groups[groupName].style.display = "none";
        }

        var selectedObjects = this.jsObject.options.selectedObject ? [this.jsObject.options.selectedObject] : this.jsObject.options.selectedObjects;
        var commonObject = this.jsObject.GetCommonObject(selectedObjects);
        if (commonObject) eventsPropertiesPanel.fillEvents(commonObject.properties.events);
    }

    eventsPropertiesPanel.fillEvents = function (eventValues) {
        if (!eventValues) return;
        if (!this.properties) this.properties = {};
        if (!this.groups) this.groups = {};

        var eventGroups = [
            ["ExportEvents", this.jsObject.loc.PropertyCategory.ExportEventsCategory, ["ExportedEvent", "ExportingEvent"]],
            ["MouseEvents", this.jsObject.loc.PropertyCategory.MouseEventsCategory, ["ClickEvent", "DoubleClickEvent", "GetDrillDownReportEvent", "MouseEnterEvent", "MouseLeaveEvent"]],
            ["NavigationEvents", this.jsObject.loc.PropertyCategory.NavigationEventsCategory, ["GetBookmarkEvent", "GetHyperlinkEvent"]],
            ["PrintEvents", this.jsObject.loc.PropertyCategory.PrintEventsCategory, ["AfterPrintEvent", "BeforePrintEvent", "PrintedEvent", "PrintingEvent"]],
            ["RenderEvents", this.jsObject.loc.PropertyCategory.RenderEventsCategory, ["BeginRenderEvent", "ColumnBeginRenderEvent", "ColumnEndRenderEvent", "EndRenderEvent",
                "RenderingEvent", "ReportCacheProcessingEvent"]],
            ["ValueEvents", this.jsObject.loc.PropertyCategory.ValueEventsCategory, ["GetExcelValueEvent", "GetCollapsedEvent", "GetTagEvent", "GetToolTipEvent", "GetValueEvent",
                "GetDataUrlEvent", "GetImageDataEvent", "GetImageURLEvent", "GetBarCodeEvent", "GetCheckedEvent", "FillParametersEvent", "GetZipCodeEvent", "GetSummaryExpressionEvent"]]
        ]

        for (var i = 0; i < eventGroups.length; i++) {
            var group = this.groups[eventGroups[i][0]];

            if (!group) {
                group = this.jsObject.PropertiesGroup(eventGroups[i][0], eventGroups[i][1]);
                group.style.margin = "5px 0 5px 0";
                this.groups[eventGroups[i][0]] = group;
                this.appendChild(group);
            }

            group.style.display = "none";
            var eventNames = eventGroups[i][2];
            var propsCount = 0;

            for (var k = 0; k < eventNames.length; k++) {
                var property = this.properties[eventNames[k]];

                if (!property) {
                    var propertyControl = this.jsObject.PropertyExpressionControl("eventProperty" + eventNames[k], this.jsObject.options.propertyControlWidth, true, true);
                    propertyControl.textBox.useHiddenValue = true;
                    propertyControl.eventName = eventNames[k];
                    property = this.jsObject.Property(null, this.jsObject.loc.PropertyEvents[eventNames[k]] || eventNames[k], propertyControl);
                    property.name = eventNames[k];
                    property.updateCaption();
                    this.properties[eventNames[k]] = property;
                    group.container.appendChild(property);

                    propertyControl.action = function () {
                        var selectedObjects = this.jsObject.options.selectedObject ? [this.jsObject.options.selectedObject] : this.jsObject.options.selectedObjects;
                        var value = Base64.encode(this.textBox.hiddenValue || this.textBox.value);
                        for (var i = 0; i < selectedObjects.length; i++) {
                            selectedObjects[i].properties.events[this.eventName] = value;
                        }
                        this.jsObject.SendCommandSetEventValue(eventsPropertiesPanel.selectedComponentParams(), this.eventName, value);
                    }
                }

                property.style.display = "none";

                if (eventValues[eventNames[k]] != null) {
                    property.style.display = "";
                    propsCount++;
                    var value = Base64.decode(eventValues[eventNames[k]]);
                    property.propertyControl.textBox.hiddenValue = value != "StiEmptyValue" ? value : "";
                    property.propertyControl.textBox.value = property.propertyControl.textBox.hiddenValue;
                }
            }

            if (propsCount > 0) group.style.display = "";
        }
    }

    eventsPropertiesPanel.updatePropertiesCaptions = function () {
        for (var propertyName in this.properties) {
            this.properties[propertyName].updateCaption();
        }
    }

    return eventsPropertiesPanel;
}