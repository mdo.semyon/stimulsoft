﻿
StiMobileDesigner.prototype.PropertyTextFormatControl = function (name, width) {
    var textFormatControl = this.PropertyTextBoxWithEditButton(name, width, true);
    textFormatControl.key = null;

    textFormatControl.button.action = function () {
        this.jsObject.InitializeTextFormatForm(function (textFormatForm) {

            textFormatForm.action = function () {
                if (textFormatForm.controls.formatsContainer.selectedItem) {
                    var resultTextFormat = textFormatForm.controls.formatsContainer.selectedItem.itemObject;
                    textFormatControl.setKey(resultTextFormat);
                }
                textFormatForm.changeVisibleState(false);
                textFormatControl.action();
            }

            textFormatForm.show(textFormatControl.key);
        });
    }

    textFormatControl.setKey = function (key) {
        this.key = key;
        this.textBox.value = this.jsObject.GetTextFormatLocalizedName(key.type);
    }

    textFormatControl.action = function () { }

    return textFormatControl;
}