﻿
StiMobileDesigner.prototype.BarCodeAdditionalPropertiesGroup = function () {
    var barCodeAdditionalPropertiesGroup = this.PropertiesGroup("barCodeAdditionalPropertiesGroup", this.loc.PropertyCategory.BarCodeAdditionalCategory);
    barCodeAdditionalPropertiesGroup.style.margin = "5px 0 5px 0";
    barCodeAdditionalPropertiesGroup.style.display = "none";

    //Horizontal Alignment
    var controlPropertyHorAlign = this.PropertyDropDownList("controlPropertyBarCodeHorizontalAlignment", this.options.propertyControlWidth, this.GetHorizontalAlignmentItems(true), true, false);
    controlPropertyHorAlign.action = function () {
        this.jsObject.ApplyPropertyValue("horAlignment", this.key);
    }
    barCodeAdditionalPropertiesGroup.container.appendChild(this.Property("barCodeHorizontalAlignment", this.loc.PropertyMain.HorAlignment, controlPropertyHorAlign, "HorAlignment"));

    //Vertical Alignment
    var controlPropertyVertAlign = this.PropertyDropDownList("controlPropertyBarCodeVerticalAlignment", this.options.propertyControlWidth, this.GetVerticalAlignmentItems(), true, false);
    controlPropertyVertAlign.action = function () {
        this.jsObject.ApplyPropertyValue("vertAlignment", this.key);
    }
    barCodeAdditionalPropertiesGroup.container.appendChild(this.Property("barCodeVerticalAlignment", this.loc.PropertyMain.VertAlignment, controlPropertyVertAlign, "VertAlignment"));
                  
    return barCodeAdditionalPropertiesGroup;
}