﻿
//Font Control
StiMobileDesigner.prototype.PropertyFontControl = function (controlName, propertyName, enableActionEvent) {
    var fontTable = this.CreateHTMLTable();
    fontTable.style.margin = "2px 0 2px 0"
    fontTable.controlName = controlName;
    fontTable.propertyName = propertyName || controlName;
    fontTable.key = "Arial!8!0!0!0!0";
    fontTable.multiRows = true;

    fontTable.applyFontPropertyValue = function (fontPropertyName, fontPropertyValue) {
        var selectedObjects = this.jsObject.options.selectedObjects;
        for (var i = 0; i < selectedObjects.length; i++) {
            var font = this.jsObject.FontStrToObject(selectedObjects[i].properties[this.propertyName]);
            font[fontPropertyName] = fontPropertyValue;
            var fontResult = this.jsObject.FontObjectToStr(font);
            selectedObjects[i].properties[this.propertyName] = fontResult;
        }
        this.jsObject.SendCommandSendProperties(selectedObjects, [this.propertyName]);
    }

    fontTable.actionFontChildControl = function (fontPropertyName, fontPropertyValue) {
        if (enableActionEvent) {
            var font = this.jsObject.FontStrToObject(this.key);
            font[fontPropertyName] = fontPropertyValue;
            this.key = this.jsObject.FontObjectToStr(font);
            this.action();
        }
        else if (this.jsObject.options.selectedObjects) {
            this.applyFontPropertyValue(fontPropertyName, fontPropertyValue);
        }
        else if (this.jsObject.options.selectedObject) {
            var font = this.jsObject.FontStrToObject(this.jsObject.options.selectedObject.properties[this.propertyName]);
            font[fontPropertyName] = fontPropertyValue;
            var fontResult = this.jsObject.FontObjectToStr(font);
            this.jsObject.options.selectedObject.properties[this.propertyName] = fontResult;
            this.jsObject.SendCommandSendProperties(this.jsObject.options.selectedObject, [this.propertyName]);
        }
    }

    //Name
    var fontName = this.PropertyFontList("controlProperty" + controlName + "Name", this.options.propertyControlWidth - 56, false);
    fontTable.addCell(fontName);
    fontName.fontControl = fontTable;
    fontName.action = function () {
        if (this.key == "Aharoni") { this.jsObject.options.buttons["controlProperty" + this.fontControl.controlName + "Bold"].setSelected(true); }
        this.jsObject.options.buttons["controlProperty" + this.fontControl.controlName + "Bold"].isEnabled = !(this.key == "Aharoni");
        this.fontControl.actionFontChildControl("name", this.key);
    };

    //Size
    var sizeItems = [];
    for (var i = 0; i < this.options.fontSizes.length; i++) {
        sizeItems.push(this.Item(controlName + "SizesFont" + i, this.options.fontSizes[i], null, this.options.fontSizes[i]));
    }
    var fontSize = this.PropertyDropDownList("controlProperty" + controlName + "Size", 45, sizeItems, false, false, this.loc.HelpDesigner.FontSize);
    fontTable.addCell(fontSize).style.paddingLeft = "5px";
    fontSize.fontControl = fontTable;
    fontSize.action = function () {
        var sizeValue = Math.abs(this.jsObject.StrToDouble(this.key));
        if (sizeValue == 0) sizeValue = 1;
        this.setKey(sizeValue.toString());
        this.fontControl.actionFontChildControl("size", this.key);
    }

    var fontDownTable = this.CreateHTMLTable();
    fontTable.addCellInNextRow(fontDownTable).setAttribute("colspan", "2");

    //Bold
    var boldButton = this.StandartSmallButton("controlProperty" + controlName + "Bold", null, null, "Bold.png", this.loc.PropertyMain.Bold, null);
    fontDownTable.addCell(boldButton).style.padding = "3px 2px 0 0";
    boldButton.fontControl = fontTable;
    boldButton.action = function () {
        this.setSelected(!this.isSelected);
        this.fontControl.actionFontChildControl("bold", this.isSelected ? "1" : "0");
    }

    //Italic
    var italicButton = this.StandartSmallButton("controlProperty" + controlName + "Italic", null, null, "Italic.png", this.loc.PropertyMain.Italic, null);
    fontDownTable.addCell(italicButton).style.padding = "3px 2px 0 2px";
    italicButton.fontControl = fontTable;
    italicButton.action = function () {
        this.setSelected(!this.isSelected);
        this.fontControl.actionFontChildControl("italic", this.isSelected ? "1" : "0");
    }

    //Underline
    var underlineButton = this.StandartSmallButton("controlProperty" + controlName + "Underline", null, null, "Underline.png", this.loc.PropertyMain.Underline, null);
    fontDownTable.addCell(underlineButton).style.padding = "3px 2px 0 2px";
    underlineButton.fontControl = fontTable;
    underlineButton.action = function () {
        this.setSelected(!this.isSelected);
        this.fontControl.actionFontChildControl("underline", this.isSelected ? "1" : "0");
    }

    //Strikeout
    var strikeoutButton = this.StandartSmallButton("controlProperty" + controlName + "Strikeout", null, null, "Strikeout.png", this.loc.PropertyMain.FontStrikeout, null);
    fontDownTable.addCell(strikeoutButton).style.padding = "3px 2px 0 2px";
    strikeoutButton.fontControl = fontTable;
    strikeoutButton.action = function () {
        this.setSelected(!this.isSelected);
        this.fontControl.actionFontChildControl("strikeout", this.isSelected ? "1" : "0");
    }

    fontTable.action = function () { }

    fontTable.setKey = function (key) {
        this.key = key;
        var font = key.split("!");
        var controls = this.jsObject.options.controls;
        var buttons = this.jsObject.options.buttons;
        controls["controlProperty" + this.controlName + "Name"].setKey(font[0]);
        controls["controlProperty" + this.controlName + "Size"].setKey(font[1]);
        buttons["controlProperty" + this.controlName + "Bold"].setSelected(font[2] == "1");
        buttons["controlProperty" + this.controlName + "Italic"].setSelected(font[3] == "1");
        buttons["controlProperty" + this.controlName + "Underline"].setSelected(font[4] == "1");
        buttons["controlProperty" + this.controlName + "Strikeout"].setSelected(font[5] == "1");
    }

    return fontTable;
}