﻿
StiMobileDesigner.prototype.PropertiesGroup = function (name, caption, width) {
    var propertiesGroup = document.createElement("div");    
    propertiesGroup.jsObject = this;    
    if (name != null) this.options.propertiesGroups[name] = propertiesGroup;
    propertiesGroup.name = name;
    propertiesGroup.className = "stiDesignerPropertiesGroup";
    propertiesGroup.isOpened = false;
    if (width) propertiesGroup.style.width = width + "px";
    
    //Header
    propertiesGroup.header = document.createElement("div");
    propertiesGroup.appendChild(propertiesGroup.header);
    propertiesGroup.headerButton = this.SmallButton(name != null ? name + "HeaderButton" : null, null, caption, "PropertiesGroupArrowClose.png", null, null, this.GetStyles("PropertiesGroupHeaderButton"));
    propertiesGroup.header.appendChild(propertiesGroup.headerButton);    
    propertiesGroup.headerButton.propertiesGroup = propertiesGroup;
    propertiesGroup.headerButton.action = function () { 
        this.propertiesGroup.changeOpenedState(!this.propertiesGroup.isOpened); 
    }
    
    propertiesGroup.changeOpenedState = function (state) {
        this.container.style.display = state ?  "" : "none";
        this.isOpened = state;
        this.headerButton.setSelected(state);
        this.headerButton.image.src = this.jsObject.options.images[state ?  "PropertiesGroupArrowOpen.png" : "PropertiesGroupArrowClose.png"];
    }    
    
    propertiesGroup.container = document.createElement("div");
    propertiesGroup.appendChild(propertiesGroup.container);
    propertiesGroup.container.className = "stiDesignerPropertiesGroupContainer";
    propertiesGroup.container.style.display = "none";
    
    return propertiesGroup;
}
