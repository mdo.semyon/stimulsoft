﻿
StiMobileDesigner.prototype.DictionaryPanel = function () {
    var dictionaryPanel = document.createElement("div");
    dictionaryPanel.jsObject = this;
    dictionaryPanel.className = "stiDesignerPropertiesPanelInnerContent";
    this.options.dictionaryPanel = dictionaryPanel;
    dictionaryPanel.style.top = "35px";

    //Hint Text
    var createDataHintItem = this.CreateDataHintItem();
    dictionaryPanel.appendChild(createDataHintItem);
    dictionaryPanel.createDataHintItem = createDataHintItem;    
    
    createDataHintItem.onclick = function () {
        if (this.jsObject.options.dictionaryShowMoreActivated) {
            return;
        }

        this.jsObject.InitializeSelectConnectionForm(function (selectConnectionForm) {
            selectConnectionForm.changeVisibleState(true);
        });
    }

    //Show More
    var showMoreButton = this.SmallButton("dictionaryShowMore", null, this.loc.Buttons.ShowMore, null, null, null, this.GetStyles("HyperlinkButton"));
    showMoreButton.caption.className = "stiCreateDataHintHeaderText";
    showMoreButton.style.marginTop = "10px";
    showMoreButton.style.display = "inline-block";
    createDataHintItem.addCellInNextRow(showMoreButton).style.textAlign = "center";

    showMoreButton.action = function () {
        this.jsObject.options.dictionaryShowMoreActivated = true;
        this.jsObject.options.dictionaryPanel.toolBar.updateControls();     
    }

    //Main Dictionary
    dictionaryPanel.mainDictionary = document.createElement("div");
    dictionaryPanel.appendChild(dictionaryPanel.mainDictionary);

    //Dictionary Toolbar
    dictionaryPanel.toolBar = this.CreateHTMLTable();
    dictionaryPanel.toolBar.jsObject = this;
    dictionaryPanel.toolBar.controls = {};
    dictionaryPanel.mainDictionary.appendChild(dictionaryPanel.toolBar);

    //Dictionary Separator
    var dictionarySeparator = document.createElement("div");
    dictionarySeparator.className = "stiDesignerDictionarySeparator";
    dictionaryPanel.mainDictionary.appendChild(dictionarySeparator);
        
    //Items Container
    var dictionaryItemsContainer = document.createElement("div");
    dictionaryItemsContainer.className = "stiDesignerDictionaryItemsContainer";
    dictionaryItemsContainer.style.top = this.options.isTouchDevice ? "35px" : "34px";
    dictionaryPanel.mainDictionary.appendChild(dictionaryItemsContainer);
    dictionaryItemsContainer.appendChild(this.DictionaryTree());

    //Description Panel
    var descriptionPanel = document.createElement("div");
    descriptionPanel.style.display = "none";
    descriptionPanel.className = "stiDesignerDictionaryDescriptionPanel";
    dictionaryPanel.mainDictionary.appendChild(descriptionPanel);
    dictionaryPanel.descriptionPanel = descriptionPanel;

    descriptionPanel.show = function (text) {
        descriptionPanel.style.display = "";
        dictionaryItemsContainer.style.bottom = "121px";
        descriptionPanel.innerHTML = text;
    }

    descriptionPanel.hide = function () {
        descriptionPanel.style.display = "none";
        dictionaryItemsContainer.style.bottom = "0px";
    }

    dictionaryPanel.toolBar.updateControls = function (currentMenu) {
        var selectedItem = this.jsObject.options.dictionaryTree.selectedItem;
        if (selectedItem == null) return;
        var selectedItemObject = selectedItem.itemObject;
        this.controls["MoveUp"].setEnabled(selectedItem.canMove("Up"));
        this.controls["MoveDown"].setEnabled(selectedItem.canMove("Down"));

        //Permissions
        var permissionDataSources = this.jsObject.options.permissionDataSources;
        var permissionDataConnections = this.jsObject.options.permissionDataConnections;
        var permissionBusinessObjects = this.jsObject.options.permissionBusinessObjects;
        var permissionVariables = this.jsObject.options.permissionVariables;
        var permissionDataRelations = this.jsObject.options.permissionDataRelations;
        var permissionDataColumns = this.jsObject.options.permissionDataColumns;
        var permissionResources = this.jsObject.options.permissionResources;
        var permissionSqlParameters = this.jsObject.options.permissionSqlParameters;

        var getPermissionResult = function (selectedItemObject, permissionType) {
            return (selectedItemObject.typeItem == "DataBase" && !(permissionDataConnections.indexOf("All") >= 0 || permissionDataConnections.indexOf(permissionType) >= 0)) ||
            (selectedItemObject.typeItem == "DataSource" && !(permissionDataSources.indexOf("All") >= 0 || permissionDataSources.indexOf(permissionType) >= 0)) ||
            (selectedItemObject.typeItem == "BusinessObject" && !(permissionBusinessObjects.indexOf("All") >= 0 || permissionBusinessObjects.indexOf(permissionType) >= 0)) ||
            (selectedItemObject.typeItem == "Variable" && !(permissionVariables.indexOf("All") >= 0 || permissionVariables.indexOf(permissionType) >= 0)) ||
            (selectedItemObject.typeItem == "Relation" && !(permissionDataRelations.indexOf("All") >= 0 || permissionDataRelations.indexOf(permissionType) >= 0)) ||
            (selectedItemObject.typeItem == "Column" && !(permissionDataColumns.indexOf("All") >= 0 || permissionDataColumns.indexOf(permissionType) >= 0)) ||
            (selectedItemObject.typeItem == "Resource" && !(permissionResources.indexOf("All") >= 0 || permissionResources.indexOf(permissionType) >= 0)) ||
            (selectedItemObject.typeItem == "Parameter" && !(permissionSqlParameters.indexOf("All") >= 0 || permissionSqlParameters.indexOf(permissionType) >= 0))
        }

        var cannotEdit = getPermissionResult(selectedItemObject, "Modify");
        var cannotDelete = getPermissionResult(selectedItemObject, "Delete");

        //Enabled or Disabled Buttons
        var enableEdit = !(selectedItemObject.typeItem.indexOf("MainItem") != -1 || /*selectedItemObject.isCloud ||*/ selectedItemObject.isCloudAttachedItem ||
            (selectedItemObject.typeItem == "DataBase" && selectedItemObject.typeIcon == "ConnectionFail"))
            && selectedItemObject.typeItem != "SystemVariable" && selectedItemObject.typeItem != "Function" &&
            selectedItemObject.typeItem != "FunctionsCategory" && selectedItemObject.typeItem != "Parameters";

        var enableDelete = enableEdit || selectedItemObject.typeIcon == "ConnectionFail";

        this.controls["EditItem"].setEnabled(enableEdit && !cannotEdit);
        this.controls["DeleteItem"].setEnabled(enableDelete && !cannotDelete);

        var showColumnItem = (selectedItemObject.typeItem == "DataSource" || selectedItemObject.typeItem == "BusinessObject"
            || selectedItemObject.typeItem == "Column") && !selectedItemObject.isCloud &&
            (permissionDataColumns.indexOf("All") >= 0 || permissionDataColumns.indexOf("Create") >= 0);

        var showParameterItem = ((selectedItemObject.typeItem == "DataSource" && selectedItemObject.parameterTypes) ||
            selectedItemObject.typeItem == "Parameter" || selectedItemObject.typeItem == "Parameters" ||
            (selectedItemObject.typeItem == "Column" || this.jsObject.options.dictionaryTree.getCurrentColumnParent().parameterTypes) &&
            !selectedItemObject.isCloud && (permissionDataColumns.indexOf("All") >= 0 || permissionDataColumns.indexOf("Create") >= 0)) &&
            (permissionSqlParameters.indexOf("All") >= 0 || permissionSqlParameters.indexOf("Create") >= 0);

        if (currentMenu) {
            if (currentMenu.items["editItem"]) currentMenu.items["editItem"].setEnabled(enableEdit && !cannotEdit);
            if (currentMenu.items["deleteItem"]) currentMenu.items["deleteItem"].setEnabled(enableDelete && !cannotDelete);

            currentMenu.items["columnNew"].style.display = showColumnItem ? "" : "none";
            currentMenu.items["calcColumnNew"].style.display = showColumnItem ? "" : "none";
            currentMenu.items["parameterNew"].style.display = showParameterItem && !this.jsObject.options.jsMode ? "" : "none";
            currentMenu.items["dataSourceNew"].style.display = permissionDataSources.indexOf("All") >= 0 || permissionDataSources.indexOf("Create") >= 0 ? "" : "none";
            currentMenu.items["separator1"].style.display = currentMenu.items["dataSourceNew"].style.display;
            if (!this.jsObject.options.isJava && !this.jsObject.options.jsMode) {
                currentMenu.items["businessObjectNew"].style.display = (permissionBusinessObjects.indexOf("All") >= 0 || permissionBusinessObjects.indexOf("Create") >= 0) ? "" : "none";
            }
            currentMenu.items["relationNew"].style.display = (permissionDataRelations.indexOf("All") >= 0 || permissionDataRelations.indexOf("Create") >= 0) ? "" : "none";
            currentMenu.items["variableNew"].style.display = (permissionVariables.indexOf("All") >= 0 || permissionVariables.indexOf("Create") >= 0) ? "" : "none";
            currentMenu.items["resourceNew"].style.display = (permissionResources.indexOf("All") >= 0 || permissionResources.indexOf("Create") >= 0) ? "" : "none";
            currentMenu.items["categoryNew"].style.display = currentMenu.items["variableNew"].style.display;
            currentMenu.items["separator2"].style.display = currentMenu.items["variableNew"].style.display;

            if (currentMenu.items["separator3_0"] && currentMenu.items["viewData"]) {
                currentMenu.items["separator3_0"].style.display = selectedItemObject.typeItem == "DataSource" ? "" : "none";
                currentMenu.items["viewData"].style.display = selectedItemObject.typeItem == "DataSource" ? "" : "none";
            }
        }
        if (currentMenu && currentMenu == this.jsObject.options.menus.dictionaryNewItemMenu) {
            var hideParentButton = true;
            for (var itemName in currentMenu.items) {
                if (currentMenu.items[itemName].style.display == "") hideParentButton = false;
            }
            this.controls["NewItem"].style.display = hideParentButton ? "none" : "";
        }

        var showCreateDataHint = true;
        if (this.jsObject.options.dictionaryShowMoreActivated) showCreateDataHint = false;

        if (this.jsObject.options.dictionaryTree && showCreateDataHint) {
            var mainItems = this.jsObject.options.dictionaryTree.mainItems;
            var mainItemsNames = ["DataSources", "BusinessObjects", "Variables", "Resources", "SystemVariables", "Functions", "Images", "RichTexts", "SubReports"];
            for (var i = 0; i < mainItemsNames.length; i++) {
                if (mainItems[mainItemsNames[i]]) {
                    if (mainItems[mainItemsNames[i]].isOpening ||
                        ((mainItemsNames[i] == "DataSources" ||
                            mainItemsNames[i] == "BusinessObjects" ||
                            mainItemsNames[i] == "Variables" ||
                            mainItemsNames[i] == "Resources") &&
                        mainItems[mainItemsNames[i]].childsContainer.childNodes.length > 0))
                    {
                        showCreateDataHint = false;
                        break;
                    }
                }
            }
        }

        createDataHintItem.style.display = showCreateDataHint ? "" : "none";
        showMoreButton.style.display = showCreateDataHint ? "inline-block" : "none";
        dictionaryItemsContainer.style.display = !showCreateDataHint ? "" : "none";
    }

    var buttons = [
        ["Actions", this.loc.FormDictionaryDesigner.Actions, null, null, "Down"],
        ["NewItem", this.loc.FormDictionaryDesigner.NewItem, "NewItem.png", null, "Down"],
        ["EditItem", null, "Edit.png", this.loc.QueryBuilder.Edit, null],
        ["DeleteItem", null, "Remove.png", this.loc.MainMenu.menuEditDelete.replace("&", ""), null],
        ["separator"],
        ["MoveUp", null, "MoveUp.png", this.loc.QueryBuilder.MoveUp],
        ["MoveDown", null, "MoveDown.png", this.loc.QueryBuilder.MoveDown]
    ]

    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i][0] == "separator") {
            dictionaryPanel.toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var dictionaryToolbarButton =
            this.SmallButton("dictionary" + buttons[i][0], null, buttons[i][1], buttons[i][2], buttons[i][1] || buttons[i][3], buttons[i][4], this.GetStyles("StandartSmallButton"));
        dictionaryPanel.toolBar.controls[buttons[i][0]] = dictionaryToolbarButton;
        dictionaryToolbarButton.style.margin = this.options.isTouchDevice ? "3px 0px 3px 3px" : "5px 0px 5px 5px";
        dictionaryPanel.toolBar.addCell(dictionaryToolbarButton);
    }

    dictionaryPanel.toolBar.controls["Actions"].action = function () {
        var dictionaryActionsMenu = this.jsObject.options.menus.dictionaryActionsMenu || this.jsObject.InitializeDictionaryActionsMenu();        
        dictionaryActionsMenu.changeVisibleState(!dictionaryActionsMenu.visible);
    }
    dictionaryPanel.toolBar.controls["NewItem"].action = function () {
        var dictionaryNewItemMenu = this.jsObject.options.menus.dictionaryNewItemMenu || this.jsObject.InitializeDictionaryNewItemMenu();
        if (!dictionaryNewItemMenu.visible) dictionaryPanel.toolBar.updateControls(dictionaryNewItemMenu);
        dictionaryNewItemMenu.changeVisibleState(!dictionaryNewItemMenu.visible);
    }
    dictionaryPanel.toolBar.controls["EditItem"].action = function () { this.jsObject.EditItemDictionaryTree(); }
    dictionaryPanel.toolBar.controls["DeleteItem"].action = function () { this.jsObject.DeleteItemDictionaryTree(); }
    dictionaryPanel.toolBar.controls["MoveUp"].action = dictionaryPanel.toolBar.controls["MoveDown"].action = function () {
        var selectedItem = this.jsObject.options.dictionaryTree.selectedItem;
        if (selectedItem) {
            var direction = this.name == "dictionaryMoveUp" ? "Up" : "Down";
            var fromObject = selectedItem.itemObject;
            var toItem = direction == "Down"
                ? (selectedItem.nextSibling || (fromObject.typeItem == "Variable" ? (selectedItem.parent.nextSibling || selectedItem.parent.parent) : null))
                : (selectedItem.previousSibling || (fromObject.typeItem == "Variable" ? (selectedItem.parent.previousSibling || selectedItem.parent.parent) : null));
            var toObject = toItem ? toItem.itemObject : null;
            this.setEnabled(false);
            this.jsObject.SendCommandMoveDictionaryItem(fromObject, toObject, direction);
        }
    }

    dictionaryPanel.onmouseup = function (event) {
        if (event.button == 2) {
            event.stopPropagation();
            if (!this.jsObject.options.report) return;
            var dictionaryContextMenu = this.jsObject.options.menus.dictionaryContextMenu || this.jsObject.InitializeDictionaryContextMenu();
            dictionaryPanel.toolBar.updateControls(dictionaryContextMenu);
            var point = this.jsObject.FindMousePosOnMainPanel(event);
            dictionaryContextMenu.show(point.xPixels + 3, point.yPixels + 3, "Down", "Right");
        }
        return false;
    }

    dictionaryPanel.oncontextmenu = function (event) {
        return false;
    }

    dictionaryPanel.checkResourcesCount = function () {
        if (this.jsObject.options.reportResourcesMaximumCount && this.jsObject.options.report) {
            var resourcesCount = this.jsObject.options.report.dictionary.resources.length;
            if (resourcesCount >= this.jsObject.options.reportResourcesMaximumCount) {
                var errorMessageForm = this.jsObject.options.forms.errorMessageForm || this.jsObject.InitializeErrorMessageForm();
                errorMessageForm.show(this.jsObject.loc.Notices.QuotaMaximumResourcesCountExceeded + "<br>" +
                    this.jsObject.loc.PropertyMain.Maximum + ": " + this.jsObject.options.reportResourcesMaximumCount);
                return true;
            }
        }
        return false;
    }

    dictionaryPanel.onmousedown = function () {
        dictionaryPanel.setFocused(true);
        dictionaryPanel.jsObject.options.dictionaryPanelPressed = true;
    }

    dictionaryPanel.ontouchstart = function () {
        dictionaryItemsContainer.onmousedown();
    }

    dictionaryPanel.setFocused = function (state) {
        this.isFocused = state;
        var selectedItem = this.jsObject.options.dictionaryTree.selectedItem;

        if (selectedItem) {
            if (state)
                selectedItem.button.className = selectedItem.button.className.replace(" stiDesignerTreeItemButtonSelectedNotActive", "");
            else if (selectedItem.button.className.indexOf("stiDesignerTreeItemButtonSelectedNotActive") < 0)
                selectedItem.button.className += " stiDesignerTreeItemButtonSelectedNotActive";
        }
    }

    return dictionaryPanel;
}

StiMobileDesigner.prototype.CreateDataHintItem = function () {
    var createDataHintItem = this.CreateHTMLTable();
    var widthHintItem = this.options.propertiesGridWidth - 120;
    createDataHintItem.className = "stiCreateDataHintItem";
    createDataHintItem.style.top = "calc(50% - 60px)";
    createDataHintItem.style.left = "calc(50% - " + widthHintItem / 2 + "px)";
    var img = document.createElement("img");
    img.src = this.options.images["ItemCreateData.png"];
    createDataHintItem.addCell(img).style.textAlign = "center";
    createDataHintItem.addTextCellInNextRow(this.loc.FormDictionaryDesigner.ClickHere).className = "stiCreateDataHintHeaderText";

    createDataHintItem.onmouseup = function(event){
        this.jsObject.options.dictionaryPanel.onmouseup(event);
    }

    //text create
    var text = document.createElement("div");
    text.className = "stiCreateDataHintText";
    text.innerHTML = this.loc.FormDictionaryDesigner.CreateNewDataSource;
    text.style.width = widthHintItem + "px";
    createDataHintItem.addCellInNextRow(text).style.textAlign = "center";

    //separator
    var separator = document.createElement("div");
    separator.style.display = "inline-block";
    createDataHintItem.addCellInNextRow(separator).style.textAlign = "center";
    var sepTable = this.CreateHTMLTable();
    separator.appendChild(sepTable);

    var line1 = document.createElement("div");
    line1.className = "stiCreateDataSeparatorLine";
    line1.style.width = (widthHintItem / 2 - 40) + "px";
    sepTable.addCell(line1);

    var textOr = document.createElement("div");
    textOr.className = "stiCreateDataHintText";
    textOr.innerHTML = this.loc.Report.FilterOr.toString().toLowerCase();
    textOr.style.padding = "0 8px 0 8px";
    sepTable.addCell(textOr).style.width = "1px";

    var line2 = document.createElement("div");
    line2.style.width = (widthHintItem / 2 - 40) + "px";
    line2.className = "stiCreateDataSeparatorLine";
    sepTable.addCell(line2);

    //text drop
    var text2 = document.createElement("div");
    text2.className = "stiCreateDataHintText";
    text2.innerHTML = this.loc.FormDictionaryDesigner.DragNewDataSource;
    text2.style.width = widthHintItem + "px";
    createDataHintItem.addCellInNextRow(text2).style.textAlign = "center";

    return createDataHintItem;
}