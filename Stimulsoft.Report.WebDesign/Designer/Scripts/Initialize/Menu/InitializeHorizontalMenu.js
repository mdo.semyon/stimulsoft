﻿
StiMobileDesigner.prototype.HorizontalMenu = function (name, parentButton, animDirection, items, itemsStyles) {
    var menu = this.BaseMenu(name, parentButton, animDirection);
    menu.itemsStyles = itemsStyles;

    menu.addItems = function (items) {
        while (this.innerContent.childNodes[0]) {
            this.innerContent.removeChild(this.innerContent.childNodes[0]);
        }
        if (items && items.length) {
            for (var i = 0; i < items.length; i++) {
                if (typeof (items[i]) != "string") {
                    var item = this.jsObject.VerticalMenuItem(this, items[i].name, items[i].caption, items[i].imageName, items[i].key, this.itemsStyles, items[i].haveSubMenu);
                    this.innerContent.appendChild(item);
                }
                else
                    this.innerContent.appendChild(this.jsObject.VerticalMenuSeparator(this, items[i]));
            }
        }
    }

    menu.onmousedown = function () {
        if (!this.isTouchStartFlag) this.ontouchstart(true);
    }

    menu.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.horMenuPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }
    
    menu.addItems(items);

    return menu;
}