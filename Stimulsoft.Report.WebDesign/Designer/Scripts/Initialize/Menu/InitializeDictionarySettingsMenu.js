﻿
StiMobileDesigner.prototype.InitializeDictionarySettingsMenu = function () {
    var dictionarySettingsMenu = this.VerticalMenu("dictionarySettingsMenu", this.options.propertiesPanel.dictionaryToolBar.controls["Settings"], "Down", null, this.GetStyles("MenuStandartItem"));
    dictionarySettingsMenu.controls = {};

    var settingsControls = [
        ["createFieldOnDoubleClick", this.CheckBox(null, this.loc.PropertyMain.CreateFieldOnDoubleClick), "8px"],
        ["createLabel", this.CheckBox(null, this.loc.PropertyMain.CreateLabel), "0px 8px 8px 8px"],
        ["useAliases", this.CheckBox(null, this.loc.PropertyMain.UseAliases), "0px 8px 8px 8px"]
    ]

    var setSettingsToCookie = function () {
        var settings = {};
        for (var i = 0; i < settingsControls.length; i++) {
            settings[settingsControls[i][0]] = dictionarySettingsMenu.controls[settingsControls[i][0]].isChecked;
            dictionarySettingsMenu.jsObject.options[settingsControls[i][0]] = settings[settingsControls[i][0]];
        }
        dictionarySettingsMenu.jsObject.SetCookie("StiMobileDesignerDictionarySettings", JSON.stringify(settings));
    }

    for (var i = 0; i < settingsControls.length; i++) {
        var control = settingsControls[i][1];
        control.name = settingsControls[i][0];
        dictionarySettingsMenu.controls[control.name] = control;
        control.style.margin = settingsControls[i][2];
        dictionarySettingsMenu.innerContent.appendChild(control);
        control.action = function () {
            setSettingsToCookie();

            if (this.name == "useAliases") {
                this.jsObject.SendCommandUpdateReportAliases();
            }
        }
    }

    var settings = {
        createFieldOnDoubleClick: false,
        createLabel: false,
        useAliases: false
    };

    var jsonSettings = this.GetCookie("StiMobileDesignerDictionarySettings");
    if (jsonSettings) settings = JSON.parse(jsonSettings);

    for (var i = 0; i < settingsControls.length; i++) {
        dictionarySettingsMenu.controls[settingsControls[i][0]].setChecked(settings[settingsControls[i][0]]);
    }

    //if (this.options.jsMode) {
    //    dictionarySettingsMenu.controls.useAliases.setChecked(false);
    //    dictionarySettingsMenu.controls.useAliases.style.display = "none";
    //}

    setSettingsToCookie();    

    return dictionarySettingsMenu;
}