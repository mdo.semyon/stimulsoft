﻿
StiMobileDesigner.prototype.InterfaceTypeMenu = function () {
    
    var menu = this.VerticalMenu("interfaceTypeMenu", this.options.buttons.interfaceButton, "Down", this.GetInterfaceTypeItems(), this.GetStyles("MenuMiddleItem"));

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.SetCookie("StimulsoftMobileDesignerInterfaceType", menuItem.key);
        location.reload();
    }

    menu.onshow = function (menuItem) {        
        this.items.touch.setSelected(this.jsObject.options.isTouchDevice);
        this.items.mouse.setSelected(!this.jsObject.options.isTouchDevice);
    }

    return menu;
}