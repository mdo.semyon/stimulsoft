﻿
StiMobileDesigner.prototype.InitializeToolboxComponentsMenu = function (parentButton) {    

    var componentTypes = this.options.isJava
        ? ["StiText", "StiImage", "StiPanel", "StiClone", "StiCheckBox", "StiSubReport", "StiZipCode", "StiTable", "StiCrossTab"]
        : ["StiText", "StiTextInCells", "StiRichText", "StiImage", "StiPanel", "StiClone", "StiCheckBox", "StiSubReport", "StiZipCode", "StiTable", "StiCrossTab"];

    var items = [];

    for (var i = 0; i < componentTypes.length; i++) {
        if (this.options.visibilityComponents[componentTypes[i]] ||
            this.options.visibilityBands[componentTypes[i]] ||
            this.options.visibilityCrossBands[componentTypes[i]])
        {
            items.push(this.Item(componentTypes[i], this.loc.Components[componentTypes[i]], "SmallComponents." + componentTypes[i] + ".png", componentTypes[i]));
        }
    }

    if (items.length == 0) parentButton.style.display = "none";

    var menu = this.HorizontalMenu("toolboxComponentsMenu", parentButton, "Right", items, this.GetStyles("MenuStandartItem"));

    for (var itemKey in menu.items) {
        this.AddDragEventsToComponentButton(menu.items[itemKey]);
        menu.items[itemKey].setAttribute("title", this.loc.HelpComponents[itemKey]);
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.options.toolbox.resetChoose();
        this.jsObject.options.toolbox.setChoose(menuItem);
    }

    return menu;
}