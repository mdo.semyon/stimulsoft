﻿
StiMobileDesigner.prototype.CrossBandsMenu = function () {
    var componentTypes = ["StiCrossGroupHeaderBand", "StiCrossGroupFooterBand", "StiCrossHeaderBand", "StiCrossFooterBand", "StiCrossDataBand"];;

    var items = [];

    for (var i = 0; i < componentTypes.length; i++) {
        if (this.options.visibilityCrossBands[componentTypes[i]]) {
            items.push(this.Item(componentTypes[i], this.loc.Components[componentTypes[i]], "SmallComponents." + componentTypes[i] + ".png", componentTypes[i]));
        }
    }

    if (items.length == 0 && this.options.buttons.insertCrossBands)
        this.options.buttons.insertCrossBands.style.display = "none";

    var menu = this.VerticalMenu("crossBandsMenu", this.options.buttons.insertCrossBands, "Down", items, this.GetStyles("MenuStandartItem"));

    for (var itemKey in menu.items) {
        this.AddDragEventsToComponentButton(menu.items[itemKey]);
        menu.items[itemKey].setAttribute("title", this.loc.HelpComponents[itemKey]);
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.options.insertPanel.resetChoose();
        this.jsObject.options.insertPanel.setChoose(menuItem);
    }

    return menu;
}