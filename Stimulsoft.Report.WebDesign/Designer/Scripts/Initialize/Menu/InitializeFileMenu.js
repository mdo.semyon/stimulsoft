﻿
StiMobileDesigner.prototype.InitializeFileMenu = function () {
    var fileMenu = document.createElement("div");
    fileMenu.id = this.options.mobileDesigner.id + "fileMenu";
    fileMenu.className = "stiDesignerFileMenu";
    fileMenu.jsObject = this;
    fileMenu.items = {};
    fileMenu.style.display = "none";
    fileMenu.style.width = "0px";
    fileMenu.style.minHeight = this.options.cloudMode ? "400px" : "500px";
    this.options.menus["fileMenu"] = fileMenu;
    this.options.mainPanel.appendChild(fileMenu);

    fileMenu.appendChild(this.FileMenuCloseButton());
    if (this.options.showFileMenuInfo) fileMenu.appendChild(this.FileMenuItem("infoReport", this.loc.ReportInfo.Info));
    if (this.options.showFileMenuNew) fileMenu.appendChild(this.FileMenuItem("newReport", this.loc.MainMenu.menuFileNew.replace("&", "")));
    if (this.options.showFileMenuOpen && this.options.canOpenFiles) {
        fileMenu.appendChild(this.FileMenuItem("openReport", this.loc.MainMenu.menuFileOpen.replace("&", "").replace("...", "")));
    }
    if (this.options.showFileMenuSave) fileMenu.appendChild(this.FileMenuItem("saveReport", this.loc.A_WebViewer.SaveReport));
    if (this.options.showFileMenuSaveAs) fileMenu.appendChild(this.FileMenuItem("saveAsReport", this.loc.MainMenu.menuFileSaveAs.replace("...", "")));
    fileMenu.appendChild(this.FileMenuItem("share", this.loc.Cloud.ButtonShare));
    fileMenu.appendChild(this.FileMenuItem("publish", this.loc.Cloud.ButtonPublish));
    if (this.options.showFileMenuClose) fileMenu.appendChild(this.FileMenuItem("closeReport", this.loc.MainMenu.menuFileClose.replace("&", "")));
    if (this.options.showFileMenuExit) fileMenu.appendChild(this.FileMenuItem("exitDesigner", this.loc.MainMenu.menuFileExit.replace("&", "")));
    if (this.options.showFileMenuOptions) {
        fileMenu.appendChild(this.FileMenuSeparator());
        fileMenu.appendChild(this.FileMenuItem("optionsDesigner", this.loc.FormOptions.title));
    }
    if (this.options.showFileMenuAbout) fileMenu.appendChild(this.FileMenuItem("aboutDesigner", this.loc.MainMenu.menuHelpAboutProgramm.replace("&", "").replace("...", "")));
    if (this.options.showFileMenuHelp) {
        var helpItem = this.FileMenuItem("help", this.loc.Buttons.Help);
        helpItem.style.position = "absolute";
        helpItem.style.bottom = "0px";
        helpItem.style.width = "100%";
        fileMenu.appendChild(helpItem);
    }

    fileMenu.changeVisibleState = function (state) {
        var options = this.jsObject.options;
        if (options.viewerContainer)
            options.viewerContainer.changeVisibleState(options.previewMode && !options.mvcMode ? !state : false);
        options.toolBar.changeVisibleState(!state);
        options.workPanel.changeVisibleState(options.workPanel.visibleState ? !state : false);
        options.statusPanel.changeVisibleState(!state);
        options.pagesPanel.changeVisibleState((options.previewMode && options.mvcMode) ? false : !state);
        if (options.showPanelPropertiesAndDictionary) {
            options.propertiesPanel.changeVisibleState(!options.propertiesPanel.fixedViewMode ? (options.previewMode && options.mvcMode ? false : !state) : false);
            options.propertiesPanel.showButtonsPanel.changeVisibleState((options.previewMode && options.mvcMode) || !options.propertiesPanel.fixedViewMode ? false : (state ? false : true));
        }
        options.paintPanel.changeVisibleState(!state);
        
        if (state) {
            if (options.showFileMenuSave) this.items.saveReport.setEnabled(options.report);
            if (options.showFileMenuSaveAs) this.items.saveAsReport.setEnabled(options.report);
            if (options.showFileMenuClose) this.items.closeReport.setEnabled(options.report);
            if (options.showFileMenuInfo) this.items.infoReport.setEnabled(options.report);
            if (options.showFileMenuOptions) this.items.optionsDesigner.setEnabled(options.report);

            if (this.items.share) {
                this.items.share.setEnabled(options.report);
                this.items.share.style.display = (options.cloudMode || (options.jsMode && options.nodeJsMode)) ? "" : "none";
            }
            if (this.items.publish) {
                this.items.publish.style.display = (options.cloudMode || (options.jsMode && options.nodeJsMode)) ? "" : "none";
                this.items.publish.setEnabled(options.report);
            }

            if (options.cloudMode) {
                if (options.cloudParameters && options.cloudParameters.thenOpenWizard) {
                    this.items.newReport.action();
                }
                else {
                    this.items.openReport.action();
                }
            }
            else if (options.showFileMenuNew) {
                this.items.newReport.action();
            }
        }

        if (state)
            this.style.display = "";
        else {
            this.closeAllPanels();
            if (options.previewMode) {
                options.buttons.homeToolButton.action();
            }
        }

        this.visible = state;

        d = new Date();
        endTime = d.getTime() + options.menuAnimDuration;
        this.jsObject.ShowAnimationFileMenu(this, (state ? 150 : 0), endTime);
    }

    fileMenu.action = function (menuItem) {
        this.closeAllPanels(menuItem.name);
        this.jsObject.ExecuteAction(menuItem.name);
    }

    fileMenu.closeAllPanels = function (openingPanelName) {
        if (this.jsObject.options.newReportPanel && openingPanelName != "newReport") this.jsObject.options.newReportPanel.changeVisibleState(false);
        if (this.jsObject.options.infoReportPanel && openingPanelName != "infoReport") this.jsObject.options.infoReportPanel.changeVisibleState(false);
        if (this.jsObject.options.openPanel && openingPanelName != "openReport") this.jsObject.options.openPanel.changeVisibleState(false);
        if (this.jsObject.options.saveAsPanel && openingPanelName != "saveAsReport") this.jsObject.options.saveAsPanel.changeVisibleState(false);
    }

    return fileMenu;
}

StiMobileDesigner.prototype.FileMenuItem = function (name, caption) {
    var menuItem = document.createElement("div");
    menuItem.jsObject = this;
    menuItem.isEnabled = true;
    menuItem.isSelected = false;
    menuItem.name = name;
    menuItem.id = this.options.mobileDesigner.id + "FileMenuItem" + name;
    menuItem.innerHTML = caption;
    menuItem.className = "stiDesignerFileMenuItem";
    this.options.menus["fileMenu"].items[name] = menuItem;

    menuItem.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    menuItem.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.className = "stiDesignerFileMenuItemOver";
    }

    menuItem.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.className = "stiDesignerFileMenuItem";
        this.className = this.isSelected ? "stiDesignerFileMenuItemSelected" : "stiDesignerFileMenuItem";
    }

    menuItem.onclick = function () {
        if (this.isTouchEndFlag || !this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.action();
    }

    menuItem.ontouchend = function () {
        if (!this.isEnabled) return;
        this.className = "stiDesignerFileMenuItemOver";
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);

        setTimeout(function () {
            this_.className = "stiDesignerFileMenuItem";
            this_.action();
        }, 500);

        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    menuItem.action = function () {
        this.setSelected(true);
        this.jsObject.options.menus.fileMenu.action(this);
    }

    menuItem.setEnabled = function (state) {
        this.isEnabled = state;
        this.className = state ? "stiDesignerFileMenuItem" : "stiDesignerFileMenuItemDisabled";
    }

    menuItem.setSelected = function (state) {
        if (state) {
            var items = this.jsObject.options.menus.fileMenu.items;
            for (var name in items) {
                if ("setSelected" in items[name]) items[name].setSelected(false);
            }
        }
        this.isSelected = state;
        this.className = state ? "stiDesignerFileMenuItemSelected" : (this.isEnabled ? "stiDesignerFileMenuItem" : "stiDesignerFileMenuItemDisabled");
    }

    return menuItem;
}

StiMobileDesigner.prototype.FileMenuSeparator = function () {
    var menuSeparator = document.createElement("div");
    menuSeparator.style.padding = "5px 25px 5px 25px"
    menuSeparatorInner = document.createElement("div");
    menuSeparatorInner.className = "stiDesignerFileMenuSeparator";
    menuSeparator.appendChild(menuSeparatorInner);
    
    return menuSeparator;
}

StiMobileDesigner.prototype.FileMenuCloseButton = function () {
    var closeButtonParent = document.createElement("div");
    closeButtonParent.style.padding = "15px 0px 15px 25px";

    var closeButton = document.createElement("img");
    closeButton.src = this.options.images["CloseFileMenu.png"];
    closeButton.jsObject = this;
    closeButton.name = "closeFileMenu";
    closeButton.id = this.options.mobileDesigner.id + closeButton.name;
    closeButtonParent.appendChild(closeButton);

    closeButton.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    closeButton.onmouseenter = function () {
        if (this.jsObject.options.isTouchClick) return;
        this.style.opacity = "0.7";
    }

    closeButton.onmouseleave = function () {
        this.style.opacity = "1";
    }

    closeButton.onclick = function () {
        if (this.isTouchEndFlag) return;
        this.action();
    }

    closeButton.ontouchend = function () {
        this.style.opacity = "0.7";
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        setTimeout(function () {
            this_.style.opacity = "1";
            this_.action();
        }, 150);
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    closeButton.action = function () {
        this.jsObject.ExecuteAction(this.name);
    }

    return closeButtonParent;
}