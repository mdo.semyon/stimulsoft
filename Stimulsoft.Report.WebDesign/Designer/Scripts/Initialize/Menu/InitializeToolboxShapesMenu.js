﻿
StiMobileDesigner.prototype.InitializeToolboxShapesMenu = function (menuName, parentButton, withoutPrimitives, isDashboardElement) {
    var menu = this.HorizontalMenu(menuName, parentButton, "Right", null, this.GetStyles("MenuStandartItem"));
    menu.innerContent.style.width = "200px";
    menu.type = "Menu";

    if (!withoutPrimitives) {
        var basicShapesHeader = this.ShapesMenuHeader(this.loc.Shapes.BasicShapes);
        basicShapesHeader.style.display = "none";
        menu.innerContent.appendChild(basicShapesHeader);

        var basicShapes = ["StiHorizontalLinePrimitive", "StiVerticalLinePrimitive", "StiRectanglePrimitive", "StiRoundedRectanglePrimitive"];

        for (var i = 0; i < basicShapes.length; i++) {
            if (this.options.visibilityComponents[basicShapes[i]]) {
                basicShapesHeader.style.display = "";
                var button = this.ShapesMenuButton(menu, basicShapes[i], "Shapes." + basicShapes[i] + ".png", this.loc.HelpComponents[basicShapes[i]]);
                menu.innerContent.appendChild(button);
            }
        }
    }

    if (this.options.visibilityComponents.StiShape) {
        var otherShapes = [
            {
                category: this.loc.Shapes.EquationShapes,
                items: ["StiPlusShapeType", "StiMinusShapeType", "StiMultiplyShapeType", "StiDivisionShapeType", "StiEqualShapeType"]
            },
            {
                category: this.loc.Shapes.BlockArrows,
                items: ["StiArrowShapeTypeRight", "StiArrowShapeTypeLeft", "StiArrowShapeTypeUp", "StiArrowShapeTypeDown", "StiComplexArrowShapeType",
                    "StiFlowchartSortShapeType", "StiBentArrowShapeType", "StiChevronShapeType"]
            },
            {
                category: this.loc.Shapes.Lines,
                items: ["StiDiagonalUpLineShapeType", "StiDiagonalDownLineShapeType", "StiHorizontalLineShapeType", "StiLeftAndRightLineShapeType",
                    "StiTopAndBottomLineShapeType", "StiVerticalLineShapeType"]
            },
            {
                category: this.loc.Shapes.Flowchart,
                items: ["StiOvalShapeType", "StiRectangleShapeType", "StiRoundedRectangleShapeType", "StiTriangleShapeType", "StiFlowchartCardShapeType",
                    "StiFlowchartCollateShapeType", "StiFlowchartDecisionShapeType", "StiFlowchartManualInputShapeType", "StiFlowchartOffPageConnectorShapeType",
                    "StiFlowchartPreparationShapeType", "StiFrameShapeType", "StiParallelogramShapeType", "StiRegularPentagonShapeType", "StiTrapezoidShapeType",
                    "StiOctagonShapeType", "StiSnipSameSideCornerRectangleShapeType", "StiSnipDiagonalSideCornerRectangleShapeType"]
            }
        ]

        for (var i = 0; i < otherShapes.length; i++) {
            var header = this.ShapesMenuHeader(otherShapes[i].category);
            menu.innerContent.appendChild(header);

            for (var k = 0; k < otherShapes[i].items.length; k++) {
                var toolTip = otherShapes[i].items[k].replace("ShapeType", "").replace("Sti", "");
                var button = this.ShapesMenuButton(menu, (isDashboardElement ? "StiShapeElement;" : "StiShape;") + otherShapes[i].items[k], "Shapes." + otherShapes[i].items[k] + ".png", toolTip);
                menu.innerContent.appendChild(button);
            }
        }
    }
    
    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.options.toolbox.resetChoose();
        this.jsObject.options.toolbox.setChoose(menuItem);
    }

    return menu;
}