﻿
StiMobileDesigner.prototype.BandsMenu = function () {

    var componentTypes = ["StiReportTitleBand", "StiReportSummaryBand", "StiPageHeaderBand", "StiPageFooterBand", "StiGroupHeaderBand",
        "StiGroupFooterBand", "StiHeaderBand", "StiFooterBand", "StiColumnHeaderBand", "StiColumnFooterBand", "StiDataBand", "StiHierarchicalBand",
        "StiChildBand", "StiEmptyBand", "StiOverlayBand"];

    var items = [];

    for (var i = 0; i < componentTypes.length; i++) {
        if (this.options.visibilityBands[componentTypes[i]]) {
            items.push(this.Item(componentTypes[i], this.loc.Components[componentTypes[i]], "SmallComponents." + componentTypes[i] + ".png", componentTypes[i]));
        }
    }

    if (items.length == 0 && this.options.buttons.insertComponents)
        this.options.buttons.insertComponents.style.display = "none";

    var menu = this.VerticalMenu("bandsMenu", this.options.buttons.insertBands, "Down", items, this.GetStyles("MenuStandartItem"));

    for (var itemKey in menu.items) {
        this.AddDragEventsToComponentButton(menu.items[itemKey]);
        menu.items[itemKey].setAttribute("title", this.loc.HelpComponents[itemKey]);
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.options.toolbox.resetChoose();
        this.jsObject.options.toolbox.setChoose(menuItem);
    }

    return menu;
}