﻿
StiMobileDesigner.prototype.InitializeDictionaryActionsMenu = function () {
    var dictionaryActionsMenu = this.VerticalMenu("dictionaryActionsMenu", this.options.dictionaryPanel.toolBar.controls["Actions"], "Down", this.GetDictionaryActionsItems(), this.GetStyles("MenuStandartItem"));

    dictionaryActionsMenu.action = function (menuItem) {
        this.changeVisibleState(false);
        switch (menuItem.key) {
            case "newDictionary":
                {
                    this.jsObject.SendCommandNewDictionary();
                    break;
                }
            case "synchronize":
                {
                    this.jsObject.SendCommandSynchronizeDictionary();
                    break;
                }
        }
    }

    return dictionaryActionsMenu;
}