﻿StiMobileDesigner.prototype.InitializeToolboxBarCodesMenu = function (parentButton) {
    var menu = this.HorizontalMenu("toolboxBarCodesMenu", parentButton, "Right", this.GetBarCodeCategoriesItems(), this.GetStyles("MenuStandartItem"));
    menu.type = "Menu";

    this.InitializeSubMenu("toolboxBarCodesTwoDimensionalMenu", this.GetBarCodeTwoDimensionalItems(), menu.items["TwoDimensional"], menu, "MenuStandartItem");
    this.InitializeSubMenu("toolboxBarCodesEANUPCMenu", this.GetBarCodeEANUPCItems(), menu.items["EANUPC"], menu, "MenuStandartItem");
    this.InitializeSubMenu("toolboxBarCodesGS1Menu", this.GetBarCodeGS1Items(), menu.items["GS1"], menu, "MenuStandartItem");
    this.InitializeSubMenu("toolboxBarCodesPostMenu", this.GetBarCodePostItems(), menu.items["Post"], menu, "MenuStandartItem");
    this.InitializeSubMenu("toolboxBarCodesOthersMenu", this.GetBarCodeOthersItems(), menu.items["Others"], menu, "MenuStandartItem").firstChild.style.maxHeight = "700px";

    if (this.options.toolbox.buttons.insertBarCodes)
        this.options.toolbox.buttons.insertBarCodes.style.display = this.options.visibilityComponents.StiBarCode ? "" : "none";

    menu.action = function (menuItem) {
        if (menuItem.haveSubMenu) return;
        menuItem.name = "StiBarCode;" + menuItem.key;
        menu.changeVisibleState(false);

        if (this.jsObject.options.insertPanel) {
            this.jsObject.options.insertPanel.resetChoose();
        }

        this.jsObject.options.drawComponent = true;
        this.jsObject.options.paintPanel.setCopyStyleMode(false);
        this.jsObject.options.paintPanel.changeCursorType(true);

        if (this.jsObject.options.toolbox)
            this.jsObject.options.toolbox.selectedComponent = menuItem;
    }

    return menu;
}