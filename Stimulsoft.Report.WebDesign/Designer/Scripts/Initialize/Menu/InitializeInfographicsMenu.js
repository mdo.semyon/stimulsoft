﻿
StiMobileDesigner.prototype.InfographicsMenu = function () {
    var items = this.GetAddSeriesItems();
    items.push("separator");
    items.push(this.Item("Maps", this.loc.Components.StiMap, "Styles.StiMapStyle32.png", "Maps", null, true));

    var menu = this.VerticalMenu("infographicsMenu", this.options.buttons.insertInfographics, "Down", items, this.GetStyles("MenuMiddleItem"));
    menu.firstChild.style.maxHeight = "1000px";

    this.InitializeSubMenu("infographicsClusteredColumnMenu", this.GetChartClusteredColumnItems(), menu.items["ClusteredColumn"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsLineMenu", this.GetChartLineItems(), menu.items["Line"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsAreaMenu", this.GetChartAreaItems(), menu.items["Area"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsRangeMenu", this.GetChartRangeItems(), menu.items["Range"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsClusteredBarMenu", this.GetChartClusteredBarItems(), menu.items["ClusteredBar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsScatterMenu", this.GetChartScatterItems(), menu.items["Scatter"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsPieMenu", this.GetChartPieItems(), menu.items["Pie"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsRadarMenu", this.GetChartRadarItems(), menu.items["Radar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsFunnelMenu", this.GetChartFunnelItems(), menu.items["Funnel"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsFinancialMenu", this.GetChartFinancialItems(), menu.items["Financial"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsOthersMenu", this.GetChartOthersItems(), menu.items["Others"], menu, "MenuMiddleItem");

    var mapsMenu = this.InitializeSubMenu("infographicsMapsMenu", this.GetMapsCategoriesItems(), menu.items["Maps"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsEuropeMapsMenu", this.GetEuropeMapsItems(), mapsMenu.items["Europe"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsNorthAmericaMapsMenu", this.GetNorthAmericaMapsItems(), mapsMenu.items["NorthAmerica"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsSouthAmericaMapsMenu", this.GetSouthAmericaMapsItems(), mapsMenu.items["SouthAmerica"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsAsiaMapsMenu", this.GetAsiaMapsItems(), mapsMenu.items["Asia"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsOceaniaMapsMenu", this.GetOceaniaMapsItems(), mapsMenu.items["Oceania"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu("infographicsAfricaMapsMenu", this.GetAfricaMapsItems(), mapsMenu.items["Africa"], mapsMenu, "MenuMiddleItem");

    if (this.options.buttons["insertInfographics"])
        this.options.buttons["insertInfographics"].style.display = this.options.visibilityComponents.StiChart ? "" : "none";

    menu.action = function (menuItem) {
        if (!menuItem.haveSubMenu) {
            var infographicType = menuItem.menu.name.indexOf("MapsMenu") >= 0
                ? "Infographic;StiMap;"
                : "Infographic;StiChart;";

            menuItem.name = infographicType + menuItem.key;
            menu.changeVisibleState(false);

            if (this.jsObject.options.insertPanel) {
                this.jsObject.options.insertPanel.resetChoose();
            }

            this.jsObject.options.drawComponent = true;
            this.jsObject.options.paintPanel.setCopyStyleMode(false);
            this.jsObject.options.paintPanel.changeCursorType(true);

            if (this.jsObject.options.insertPanel)
                this.jsObject.options.insertPanel.selectedComponent = menuItem;
        }
    }

    return menu;
}
