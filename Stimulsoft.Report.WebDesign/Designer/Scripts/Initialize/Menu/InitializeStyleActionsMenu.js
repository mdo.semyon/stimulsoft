﻿
StiMobileDesigner.prototype.InitializeStyleActionsMenu = function (styleDesignerForm) {
    var menu = this.VerticalMenu("styleActionsMenu", styleDesignerForm.toolBar.actions, "Down", this.GetStylesActionsMenuItems(), this.GetStyles("MenuStandartItem"));

    styleDesignerForm.toolBar.actions.action = function () {
        menu.changeVisibleState(!menu.visible);
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        switch (menuItem.key) {
            case "openStyle": {
                if (this.jsObject.options.canOpenFiles) {
                    this.jsObject.InitializeOpenDialog("loadStyleFromFile", this.jsObject.StiHandleOpenStyle, ".sts");
                    this.jsObject.options.openDialogs.loadStyleFromFile.action();
                }
                break;
            }
            case "saveStyle": {
                this.jsObject.SendCommandSaveStyle(styleDesignerForm.stylesCollection);
                break;
            }
            case "createStyleCollection": {
                this.jsObject.InitializeCreateStyleCollectionForm(function (createStyleCollectionForm) {
                    createStyleCollectionForm.changeVisibleState(true);
                });
                break;
            }
        }
    }
        
    return menu;
}