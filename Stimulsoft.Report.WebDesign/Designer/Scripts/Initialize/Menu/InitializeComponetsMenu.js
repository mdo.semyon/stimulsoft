﻿
StiMobileDesigner.prototype.ComponentsMenu = function () {
    var componentTypes = this.options.isJava
        ? ["StiText", "StiImage", "StiPanel", "StiClone", "StiCheckBox", "StiSubReport", "StiZipCode", "StiTable", "StiCrossTab"]
        : ["StiText", "StiTextInCells", "StiRichText", "StiImage", "StiPanel", "StiClone", "StiCheckBox", "StiSubReport", "StiZipCode", "StiTable", "StiCrossTab"];

    var items = [];

    for (var i = 0; i < componentTypes.length; i++) {
        if (this.options.visibilityComponents[componentTypes[i]] ||
            this.options.visibilityBands[componentTypes[i]] ||
            this.options.visibilityCrossBands[componentTypes[i]]) {
            items.push(this.Item(componentTypes[i], this.loc.Components[componentTypes[i]], "SmallComponents." + componentTypes[i] + ".png", componentTypes[i]));
        }
    }

    if (items.length == 0 && this.options.buttons.insertCrossBands)
        this.options.buttons.insertCrossBands.style.display = "none";

    var menu = this.VerticalMenu("componentsMenu", this.options.buttons.insertComponents, "Down", items, this.GetStyles("MenuStandartItem"));

    for (var itemKey in menu.items) {
        this.AddDragEventsToComponentButton(menu.items[itemKey]);
        menu.items[itemKey].setAttribute("title", this.loc.HelpComponents[itemKey]);
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.options.insertPanel.resetChoose();
        this.jsObject.options.insertPanel.setChoose(menuItem);
    }

    return menu;
}