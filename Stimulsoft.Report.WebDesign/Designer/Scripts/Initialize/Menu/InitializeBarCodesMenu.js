﻿
StiMobileDesigner.prototype.BarCodesMenu = function () {
    var menu = this.VerticalMenu("barCodesMenu", this.options.buttons.insertBarCodes, "Down", this.GetBarCodeCategoriesItems(), this.GetStyles("MenuStandartItem"));

    this.InitializeSubMenu("barCodesTwoDimensionalMenu", this.GetBarCodeTwoDimensionalItems(), menu.items["TwoDimensional"], menu, "MenuStandartItem");
    this.InitializeSubMenu("barCodesEANUPCMenu", this.GetBarCodeEANUPCItems(), menu.items["EANUPC"], menu, "MenuStandartItem");
    this.InitializeSubMenu("barCodesGS1Menu", this.GetBarCodeGS1Items(), menu.items["GS1"], menu, "MenuStandartItem");
    this.InitializeSubMenu("barCodesPostMenu", this.GetBarCodePostItems(), menu.items["Post"], menu, "MenuStandartItem");
    this.InitializeSubMenu("barCodesOthersMenu", this.GetBarCodeOthersItems(), menu.items["Others"], menu, "MenuStandartItem").firstChild.style.maxHeight = "700px";

    if (this.options.buttons.insertBarCodes)
        this.options.buttons.insertBarCodes.style.display = this.options.visibilityComponents.StiBarCode ? "" : "none";

    menu.action = function (menuItem) {
        if (menuItem.haveSubMenu) return;
        menuItem.name = "StiBarCode;" + menuItem.key;
        menu.changeVisibleState(false);

        if (this.jsObject.options.insertPanel) {
            this.jsObject.options.insertPanel.resetChoose();
        }

        this.jsObject.options.drawComponent = true;
        this.jsObject.options.paintPanel.setCopyStyleMode(false);
        this.jsObject.options.paintPanel.changeCursorType(true);

        if (this.jsObject.options.insertPanel)
            this.jsObject.options.insertPanel.selectedComponent = menuItem;
    }

    return menu;
}
