﻿
StiMobileDesigner.prototype.InitializeDictionaryNewItemMenu = function () {
    var dictionaryNewItemMenu = this.VerticalMenu("dictionaryNewItemMenu", this.options.dictionaryPanel.toolBar.controls["NewItem"], "Down", this.GetDictionaryNewItems(), this.GetStyles("MenuStandartItem"));

    dictionaryNewItemMenu.action = function (menuItem) {
        this.changeVisibleState(false);
        switch (menuItem.key) {
            case "dataSourceNew":
                {
                    this.jsObject.InitializeSelectConnectionForm(function (selectConnectionForm) {
                        selectConnectionForm.changeVisibleState(true);
                    });
                    break;
                }
            case "businessObjectNew":
                {
                    this.jsObject.InitializeEditDataSourceForm(function (editDataSourceForm) {
                        editDataSourceForm.datasource = "BusinessObject";
                        editDataSourceForm.changeVisibleState(true);
                    });
                    break;
                }
            case "relationNew":
                {
                    this.jsObject.InitializeEditRelationForm(function (editRelationForm) {
                        editRelationForm.relation = null;
                        editRelationForm.changeVisibleState(true);
                    });
                    break;
                }
            case "columnNew":
            case "calcColumnNew":
                {
                    this.jsObject.InitializeEditColumnForm(function (editColumnForm) {
                        editColumnForm.column = menuItem.key == "columnNew" ? "column" : "calcColumn";
                        editColumnForm.changeVisibleState(true);
                    });
                    break;
                }
            case "parameterNew":
                {
                    this.jsObject.InitializeEditParameterForm(function (editParameterForm) {
                        editParameterForm.parameter = null;
                        editParameterForm.changeVisibleState(true);
                    });
                    break;
                }
            case "variableNew":
                {
                    this.jsObject.InitializeEditVariableForm(function (editVariableForm) {
                        editVariableForm.variable = null;
                        editVariableForm.changeVisibleState(true);
                    });
                    break;
                }
            case "categoryNew":
                {
                    this.jsObject.InitializeEditCategoryForm(function (editCategoryForm) {
                        editCategoryForm.category = null;
                        editCategoryForm.changeVisibleState(true);
                    });
                    break;
                }
            case "resourceNew":
                {
                    if (this.jsObject.options.dictionaryPanel.checkResourcesCount()) return;
                    this.jsObject.InitializeEditResourceForm(function (editResourceForm) {
                        editResourceForm.resource = null;
                        editResourceForm.changeVisibleState(true);
                    });
                    break;
                }
        }
    }

    return dictionaryNewItemMenu;
}