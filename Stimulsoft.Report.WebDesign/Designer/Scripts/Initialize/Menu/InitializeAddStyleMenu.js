﻿
StiMobileDesigner.prototype.InitializeAddStyleMenu = function (styleDesignerForm) {
    var menu = this.VerticalMenu("addStyleMenu", styleDesignerForm.toolBar.addStyle, "Down", this.GetAddStyleMenuItems(), this.GetStyles("MenuMiddleItem"))

    styleDesignerForm.toolBar.addStyle.action = function () {
        menu.changeVisibleState(!menu.visible);
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        this.jsObject.SendCommandAddStyle(menuItem.key);
    }

    return menu;
}