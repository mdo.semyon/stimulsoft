﻿
StiMobileDesigner.prototype.InitializeToolboxInfographicsMenu = function () {
    var items = this.GetAddSeriesItems();
    items.push("separator");
    items.push(this.Item("Maps", this.loc.Components.StiMap, "Styles.StiMapStyle32.png", "Maps", null, true));

    var menu = this.HorizontalMenu("toolboxInfographicsMenu", this.options.toolbox.buttons.infographics, "Right", items, this.GetStyles("MenuMiddleItem"));

    menu.type = "Menu";
    menu.firstChild.style.maxHeight = "1000px";

    this.InitializeSubMenu("toolboxInfographicsClusteredColumnMenu", this.GetChartClusteredColumnItems(), menu.items["ClusteredColumn"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsLineMenu", this.GetChartLineItems(), menu.items["Line"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsAreaMenu", this.GetChartAreaItems(), menu.items["Area"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsRangeMenu", this.GetChartRangeItems(), menu.items["Range"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsClusteredBarMenu", this.GetChartClusteredBarItems(), menu.items["ClusteredBar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsScatterMenu", this.GetChartScatterItems(), menu.items["Scatter"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsPieMenu", this.GetChartPieItems(), menu.items["Pie"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsRadarMenu", this.GetChartRadarItems(), menu.items["Radar"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsFunnelMenu", this.GetChartFunnelItems(), menu.items["Funnel"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsFinancialMenu", this.GetChartFinancialItems(), menu.items["Financial"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxInfographicsOthersMenu", this.GetChartOthersItems(), menu.items["Others"], menu, "MenuMiddleItem");

    var mapsMenu = this.InitializeSubMenu("toolboxMapsMenu", this.GetMapsCategoriesItems(), menu.items["Maps"], menu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxEuropeMapsMenu", this.GetEuropeMapsItems(), mapsMenu.items["Europe"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxNorthAmericaMapsMenu", this.GetNorthAmericaMapsItems(), mapsMenu.items["NorthAmerica"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxSouthAmericaMapsMenu", this.GetSouthAmericaMapsItems(), mapsMenu.items["SouthAmerica"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxAsiaMapsMenu", this.GetAsiaMapsItems(), mapsMenu.items["Asia"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxOceaniaMapsMenu", this.GetOceaniaMapsItems(), mapsMenu.items["Oceania"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu("toolboxAfricaMapsMenu", this.GetAfricaMapsItems(), mapsMenu.items["Africa"], mapsMenu, "MenuMiddleItem");

    menu.action = function (menuItem) {
        if (!menuItem.haveSubMenu) {
            var infographicType = menuItem.menu.name.indexOf("MapsMenu") >= 0
                ? "Infographic;StiMap;"
                : "Infographic;StiChart;";

            menuItem.name = infographicType + menuItem.key;
            menu.changeVisibleState(false);

            if (this.jsObject.options.insertPanel) {
                this.jsObject.options.insertPanel.resetChoose();
            }

            this.jsObject.options.drawComponent = true;
            this.jsObject.options.paintPanel.setCopyStyleMode(false);
            this.jsObject.options.paintPanel.changeCursorType(true);

            if (this.jsObject.options.toolbox)
                this.jsObject.options.toolbox.selectedComponent = menuItem;
        }
    }

    return menu;
}
