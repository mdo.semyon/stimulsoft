﻿
StiMobileDesigner.prototype.GridModeMenu = function () {

    var items = [];
    items.push(this.Item("Lines", this.loc.FormOptions.GridLines, "ViewOptions.GridLines.png", "Lines"));
    items.push(this.Item("Dots", this.loc.FormOptions.GridDots, "ViewOptions.GridDots.png", "Dots"));

    var menu = this.VerticalMenu("gridModeMenu", this.options.buttons.pagePanelGridMode, "Down", items, this.GetStyles("MenuStandartItem"));

    menu.action = function (menuItem) {
        this.changeVisibleState(false);
        if (!this.jsObject.options.report) return;
        var designerOptions = this.jsObject.options.report.info;
        designerOptions.gridMode = menuItem.key;
        this.jsObject.options.buttons.pagePanelGridMode.image.src = this.jsObject.options.images["ViewOptions.Grid" + designerOptions.gridMode + ".png"];
        this.jsObject.SendCommandApplyDesignerOptions(designerOptions);
    }

    menu.onshow = function (menuItem) {
        var designerOptions = this.jsObject.options.report ? this.jsObject.options.report.info : null;
        if (designerOptions) {
            for (var name in this.items) {
                this.items[name].setSelected(name == designerOptions.gridMode);
            }
        }
    }

    return menu;
}