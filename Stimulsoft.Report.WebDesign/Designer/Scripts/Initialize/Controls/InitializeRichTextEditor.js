﻿
StiMobileDesigner.prototype.RichTextEditor = function (name, width, height, hintText) {

    var richTextControl = document.createElement("div");
    if (name != null) this.options.controls[name] = richTextControl;
    richTextControl.jsObject = this;
    richTextControl.name = name != null ? name : this.generateKey();
    richTextControl.id = richTextControl.name;
    richTextControl.isEnabled = true;
    richTextControl.isSelected = false;
    richTextControl.isOver = false;
    richTextControl.isFocused = false;
    richTextControl.width = width;
    richTextControl.height = height;
    richTextControl.style.position = "relative";
    var defaultFontName = "Arial";
    var defaultFontSize = "8";
    
    var removeButton = this.SmallButton(null, null, null, "Remove.png", this.loc.MainMenu.menuEditDelete.replace("&", ""), null, this.GetStyles("FormButton"));
    removeButton.style.position = "absolute";
    richTextControl.appendChild(removeButton);
    removeButton.style.top = "55px";
    removeButton.style.right = "30px";
    removeButton.style.display = "none";

    removeButton.action = function () {
        richTextControl.clearResourceContent();
    }

    /* Tool Bar 1 */
    var toolBar1 = this.CreateHTMLTable();
    toolBar1.style.borderSpacing = "3px";
    toolBar1.addRow();
    richTextControl.toolBar1 = toolBar1;
    richTextControl.appendChild(toolBar1);

    var fontList = this.FontList(name + "FontList", 180, null, true);
    fontList.setKey(defaultFontName);
    toolBar1.addCell(fontList).style.paddingLeft = "4px";
    fontList.action = function () {
        var key = this.key;
        richText.restoreSelection();
        richText.win.document.execCommand("fontName", null, key);
        richTextControl.updateState();
        richTextControl.onchange();
    }


    var sizes = [5, 6, 7, 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 76];
    //var sizes = ["8pt", "10pt", "12pt", "14pt", "18pt", "24pt", "36pt"];
    var items = [];
    for (var size in sizes) items.push(this.Item("sizeItem" + size, sizes[size], null, sizes[size]));
    //for (var i = 0; i < sizes.length; i++) items.push(this.Item("sizeItem" + i, sizes[i], null, i + 1));
    var sizeList = this.DropDownList(name + "FontSize", 55, null, items, true, false, null, true);
    sizeList.setKey(defaultFontSize);
    toolBar1.addCell(sizeList);

    sizeList.action = function () {
        var key = this.key;
        richText.restoreSelection();        
        //richText.win.document.execCommand("fontSize", null, this.key);

        richText.win.document.execCommand("fontSize", null, "7");
        var fontElements = richText.win.document.getElementsByTagName("font");
        for (var i = 0, len = fontElements.length; i < len; ++i) {
            if (fontElements[i].size == "7") {
                fontElements[i].removeAttribute("size");
                fontElements[i].style.fontSize = key + "px";
            }
        }
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var sep1 = this.HomePanelSeparator();
    sep1.style.height = "26px";
    toolBar1.addCell(sep1);

    var buttonStyleBold = this.StandartSmallButton(null, null, null, "Bold.png");
    toolBar1.addCell(buttonStyleBold);
    buttonStyleBold.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("bold", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var buttonStyleItalic = this.StandartSmallButton(null, null, null, "Italic.png");
    toolBar1.addCell(buttonStyleItalic);
    buttonStyleItalic.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("italic", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var buttonStyleUnderline = this.StandartSmallButton(null, null, null, "Underline.png");
    toolBar1.addCell(buttonStyleUnderline);
    buttonStyleUnderline.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("underline", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var sep2 = this.HomePanelSeparator();
    sep2.style.height = "26px";
    toolBar1.addCell(sep2);

    var colorControl = this.ColorControlWithImage(null, "TextColor.png");
    colorControl.setKey("255,0,0");
    toolBar1.addCell(colorControl);
    colorControl.action = function () {
        var colors = this.key.split(",");
        var hexColor = this.jsObject.rgb(Number(colors[0]), Number(colors[1]), Number(colors[2]));
        richText.restoreSelection();
        richText.win.document.execCommand("foreColor", null, hexColor);
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var sep3 = this.HomePanelSeparator();
    sep3.style.height = "26px";
    toolBar1.addCell(sep3);
    
    var buttonAlignLeft = this.StandartSmallButton(null, null, null, "AlignLeft.png");
    toolBar1.addCell(buttonAlignLeft).style.paddingLeft = "4px";
    buttonAlignLeft.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("justifyLeft", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var buttonAlignCenter = this.StandartSmallButton(null, null, null, "AlignCenter.png");
    toolBar1.addCell(buttonAlignCenter);
    buttonAlignCenter.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("justifyCenter", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var buttonAlignRight = this.StandartSmallButton(null, null, null, "AlignRight.png");
    toolBar1.addCell(buttonAlignRight);
    buttonAlignRight.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("justifyRight", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var buttonAlignWidth = this.StandartSmallButton(null, null, null, "AlignWidth.png");
    toolBar1.addCell(buttonAlignWidth);
    buttonAlignWidth.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("justifyFull", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var sep4 = this.HomePanelSeparator();
    sep4.style.height = "26px";
    toolBar1.addCell(sep4);

    var buttonUnorderedList = this.StandartSmallButton(null, null, null, "UnorderedList.png");
    toolBar1.addCell(buttonUnorderedList);
    buttonUnorderedList.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("insertUnorderedList", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var buttonOrderedList = this.StandartSmallButton(null, null, null, "OrderedList.png");
    toolBar1.addCell(buttonOrderedList);
    buttonOrderedList.action = function () {
        richText.restoreSelection();
        richText.win.document.execCommand("insertOrderedList", null, "");
        richTextControl.updateState();
        richTextControl.onchange();
    }

    var separator = document.createElement("div");
    separator.className = "stiDesignerFormSeparator";
    separator.style.margin = "0 0 4px 0";
    richTextControl.toolBarsSeparator = separator;
    richTextControl.appendChild(separator);

    /* Rich Text */

    var richText = document.createElement("iframe");
    richText.setAttribute("frameborder", "no");
    richText.setAttribute("src", "about:blank");
    richText.style.width = width + "px";
    richText.style.height = height + "px";
    richText.style.minWidth = width + "px";
    richText.style.minHeight = height + "px";
    richText.style.padding = "0";
    richText.style.margin = "2px 0px 0px 6px";    
    richText.className = this.options.isTouchDevice ? "stiDesignerTextArea_Touch" : "stiDesignerTextArea_Mouse";
    if (hintText) richText.setAttribute(this.GetNavigatorName() == "MSIE" ? "title" : "placeholder", hintText);
    richTextControl.richText = richText;
    richTextControl.appendChild(richText);

    richTextControl.insertText = function (text) {
        if (text || richTextControl.jsObject.options.itemInDrag) {
            var dictionaryTree = richTextControl.jsObject.options.dictionaryTree;
            var newText = text || (dictionaryTree.selectedItem ? dictionaryTree.selectedItem.getResultForEditForm() : null);

            if (newText) {
                var currText = richTextControl.getText();
                var resultText = currText + newText;

                try {
                    var sel = richText.win.getSelection();
                    var selectionPos = sel.focusOffset;
                    var mainPos = currText.indexOf(sel.focusNode.textContent);
                    if (mainPos == -1) mainPos = 0;
                    selectionPos += mainPos;
                    resultText = currText.substr(0, selectionPos) + newText + currText.substr(selectionPos);
                }
                catch (e) { }

                richTextControl.setText(resultText);
            }
        }
    }

    richText.onload = function () {
        this.win = this.contentWindow || this.window;
        this.doc = this.contentDocument || this.document;
        this.doc.designMode = "on";
        this.ranges = [];

        richText.win.document.execCommand("fontName", null, fontList.key);
        richText.win.document.execCommand("fontSize", null, sizeList.key);

        var body = this.doc.getElementsByTagName("body")[0];
        if (body) {
            body.style.margin = "0px";
            //body.style.zoom = "1";//"1.5";
            //body.style.width = (richTextControl.width / 1.5) + "px";
            //body.style.height = (richTextControl.height / 1.5) + "px";

            //body.style.fontFamily = fontList.key;
            //body.style.fontSize = sizeList.key + "px";

            if (richTextControl.isEnabled) {
                body.click();
                body.focus();

                body.onfocus = function () { richTextControl.isFocused = true; richTextControl.setSelected(true); }
                body.onblur = function () { richTextControl.isFocused = false; richTextControl.setSelected(false); richTextControl.action(); richTextControl.onchange(); }
            }
            else {
                fontList.textBox.focus();
            }
        }

        this.doc.onclick = function () {
            richTextControl.updateState();
        }
                
        this.doc.onmousedown = function () {
            richText.ranges = [];
            richTextControl.jsObject.options.mobileDesigner.pressedDown();
        }

        this.doc.onselect = function () {
            richTextControl.updateState();

            if (richText.win.getSelection) {
                richText.ranges = [];
                var sel = richText.win.getSelection();
                if (sel.rangeCount) {
                    for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                        richText.ranges.push(sel.getRangeAt(i));
                    }
                }
            }
        }

        //Events
        if (this.win) {
            var itemInDragMove = function (event) {
                if (richTextControl.jsObject.options.itemInDrag) {
                    richTextControl.jsObject.options.itemInDrag.move(event, richTextControl.jsObject.FindPosX(richText, null, true), richTextControl.jsObject.FindPosY(richText, null, true));
                }
            }

            this.win.onmousemove = function (event) { itemInDragMove(event); };
            this.win.onmouseup = function (event) { richTextControl.insertText(); richTextControl.jsObject.DocumentMouseUp(event); }
            this.win.ontouchmove = function (event) { itemInDragMove(event); };
            this.win.ontouchend = function (event) { richTextControl.insertText(); richTextControl.jsObject.DocumentTouchEnd(event); }
        }
    }

    richText.restoreSelection = function () {
        this.win.focus();
        if (this.win.getSelection && this.ranges.length > 0) {
            var sel = this.win.getSelection();
            sel.removeAllRanges();
            for (var i = 0, len = this.ranges.length; i < len; ++i) {
                sel.addRange(this.ranges[i]);
            }
        }
    }

    richTextControl.resize = function (newWidth, newHeight) {
        richTextControl.width = newWidth;
        richTextControl.height = newHeight;
        richText.style.width = newWidth + "px";
        richText.style.height = newHeight + "px";
        richText.style.minWidth = newWidth + "px";
        richText.style.minHeight = newHeight + "px";
    }

    richTextControl.updateState = function () {
        if (!this.isEnabled) return;
        buttonStyleBold.setSelected(richText.win.document.queryCommandState("bold"));
        buttonStyleItalic.setSelected(richText.win.document.queryCommandState("italic"));
        buttonStyleUnderline.setSelected(richText.win.document.queryCommandState("underline"));
        buttonAlignLeft.setSelected(richText.win.document.queryCommandState("justifyLeft"));
        buttonAlignCenter.setSelected(richText.win.document.queryCommandState("justifyCenter"));
        buttonAlignRight.setSelected(richText.win.document.queryCommandState("justifyRight"));
        buttonAlignWidth.setSelected(richText.win.document.queryCommandState("justifyFull"));
        buttonUnorderedList.setSelected(richText.win.document.queryCommandState("insertUnorderedList"));
        buttonOrderedList.setSelected(richText.win.document.queryCommandState("insertOrderedList"));
        
        var fontName = richText.win.document.queryCommandValue("fontName");
        fontList.setKey(fontName ? fontName : defaultFontName);

        //var fontSize = richText.win.document.queryCommandValue("fontSize");
        //sizeList.setKey(fontSize? fontSize : defaultFontSize);

        var rgbColor = "0,0,0";
        var fontColor = richText.win.document.queryCommandValue("foreColor");
        if (fontColor) {
            if (fontColor.toString().toLowerCase().indexOf("rgb") == 0) {
                rgbColor = fontColor.substring(4, fontColor.indexOf(")"));
            }
            else {
                var b = (fontColor >> 16) & 255;
                var g = (fontColor >> 8) & 255;
                var r = fontColor & 255;
                rgbColor = r + "," + g + "," + b;
            }
        }
        colorControl.setKey(rgbColor);
    }

    richTextControl.setText = function (text, currentFont) {
        var win = this.richText.contentWindow || this.richText.window;
        var doc = win.document || win.contentDocument;

        if (doc) {
            doc.open();
            doc.write(text);
            doc.close();

            if (currentFont) {
                var body = doc.getElementsByTagName("body")[0];
                body.style.fontFamily = currentFont.name;
                defaultFontName = currentFont.name;
                body.style.fontSize = currentFont.size;
                defaultFontSize = currentFont.size;
                //debugger;
                //body.style.fontWeight = currentFont.bold == "1" ? "bold" : "normal";
                buttonStyleBold.setSelected(currentFont.bold == "1");
                //body.style.fontStyle = currentFont.italic == "1" ? "bold" : "normal";
                buttonStyleItalic.setSelected(currentFont.italic == "1");
            }
        }

        sizeList.setKey(defaultFontSize);
        this.updateState();
    }

    richTextControl.getText = function (text) {
        var win = this.richText.contentWindow || this.richText.window;
        var doc = win.document || win.contentDocument;

        var htmlText = doc ? doc.body.innerHTML : "";

        startIndex = 0;
        while (startIndex >= 0) {
            var startIndex = htmlText.indexOf("rgb(");
            if (startIndex == -1) startIndex = htmlText.indexOf("RGB(");
            if (startIndex >= 0) {
                var endIndex = htmlText.indexOf(")", startIndex);
                var hexColor = richTextControl.jsObject.RgbColorToHexColor(htmlText.substr(startIndex, endIndex - startIndex + 1));
                htmlText = htmlText.substring(0, startIndex) + hexColor + htmlText.substring(endIndex + 1);
            }
        }

        return htmlText;
    }

    richTextControl.setRichTextContent = function (itemObject) {
        if (!itemObject) {
            richTextControl.clearResourceContent();
        }
        else if (itemObject.content) {
            richTextControl.loadResourceContent(itemObject.content);
        }
        else {
            this.jsObject.SendCommandToDesignerServer("GetRichTextContent", { itemObject: itemObject }, function (answer) {
                itemObject.content = Base64.decode(answer.content);
                richTextControl.loadResourceContent(itemObject.content);
            });
        }
    }

    richTextControl.loadResourceContent = function (content) {
        this.setText(content || "");
        richTextControl.setEnabled(false);
        removeButton.style.display = "";
        richTextControl.onLoadResourceContent(content);
    }

    richTextControl.clearResourceContent = function () {
        richTextControl.setEnabled(true);
        removeButton.style.display = "none";
        this.setText("");
        richTextControl.onchange();
    }

    richTextControl.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        if (!this.isSelected && !this.isFocused)
            this.richText.className = "stiDesignerTextAreaOver" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    richTextControl.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.isOver = false;
        if (!this.isSelected && !this.isFocused)
            this.richText.className = "stiDesignerTextArea" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    richTextControl.setEnabled = function (state) {
        sizeList.setEnabled(state);
        fontList.setEnabled(state);
        colorControl.setEnabled(state);
        buttonStyleBold.setEnabled(state);
        buttonStyleItalic.setEnabled(state);
        buttonStyleUnderline.setEnabled(state);
        buttonAlignLeft.setEnabled(state);
        buttonAlignCenter.setEnabled(state);
        buttonAlignRight.setEnabled(state);
        buttonAlignWidth.setEnabled(state);
        buttonUnorderedList.setEnabled(state);
        buttonOrderedList.setEnabled(state);
        if (richText.doc && !state) {
            richText.doc.designMode = "off";
            var body = richText.doc.getElementsByTagName("body")[0];
            if (body) body.onfocus = null;
        }

        this.isEnabled = state;
        this.disabled = !state;
        this.richText.className =
            (state ? "stiDesignerTextArea" : "stiDesignerTextAreaDisabled") +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    richTextControl.setSelected = function (state) {
        this.isSelected = state;
        this.richText.className = (state
            ? "stiDesignerTextAreaOver"
            : (this.isEnabled ? (this.isOver ? "stiDesignerTextAreaOver" : "stiDesignerTextArea") : "stiDesignerTextAreaDisabled")) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    richTextControl.action = function () { };
    richTextControl.onchange = function () { };
    richTextControl.onLoadResourceContent = function (content) { };

    return richTextControl;
}

StiMobileDesigner.prototype.RichTextContainer = function (width, height) {
    var container = document.createElement("div");
    container.jsObject = this;
    container.className = "stiImageContainerWithBorder";
    container.style.overflow = "auto";
    container.style.width = width + "px";
    container.style.height = height + "px";
    this.AddProgressToControl(container);
    var innerContainer = document.createElement("div");
    container.appendChild(innerContainer);

    container.resize = function (newWidth, newHeight) {
        container.style.width = newWidth + "px";
        container.style.height = newHeight + "px";
    }

    container.setRichTextContent = function (itemObject) {
        container.progress.hide();
        if (itemObject) {
            if (itemObject.content) {
                innerContainer.innerHTML = itemObject.content;
            }
            else if (itemObject.url) {
                container.progress.show();
                this.jsObject.SendCommandToDesignerServer("GetRichTextContent", { itemObject: itemObject }, function (answer) {
                    container.progress.hide();
                    itemObject.content = Base64.decode(answer.content);
                    innerContainer.innerHTML = itemObject.content;
                });
            }
        }
        else {
            this.innerHTML = "";
        }
    }

    return container;
}