﻿
StiMobileDesigner.prototype.ColorControlWithImage = function (name, imageName, toolTip) {
    var colorControl = this.ColorControl(name, toolTip);
    
    //Override image
    var icon = document.createElement("img");
    icon.src = this.options.images[imageName];
   
    var colorBar = colorControl.button.image;
    var colorBarParent = colorBar.parentElement;
    var imageCell = colorBarParent.parentElement;
    colorBarParent.className = "stiColorControlWithImage_ColorBar";    
    imageCell.removeChild(colorBarParent);
    imageCell.appendChild(icon);
    imageCell.appendChild(colorBarParent);
    colorControl.button.image = colorBar;
    colorControl.button.icon = icon;
    
    //Override methods
    colorControl.setEnabled = function (state) {
        if (this.button.image) this.button.image.style.opacity = state ? "1" : "0.3";
        if (this.button.icon) this.button.icon.style.opacity = state ? "1" : "0.3";
        if (this.button.arrow) this.button.arrow.style.opacity = state ? "1" : "0.3";
        this.isEnabled = state;
        this.button.isEnabled = state;
        this.button.className = (state ? this.button.styles["default"] : this.button.styles["disabled"]) + 
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }
    
    return colorControl;
}