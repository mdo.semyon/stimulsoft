﻿
StiMobileDesigner.prototype.DateControlWithCheckBox = function (name, width, toolTip) {
    var dateControlWithCheckBox = this.CreateHTMLTable();
    dateControlWithCheckBox.key = new Date();
    dateControlWithCheckBox.isChecked = true;
    dateControlWithCheckBox.isEnabled = true;

    dateControlWithCheckBox.dateControl = this.DateControl(name, width, toolTip);
    dateControlWithCheckBox.dateControl.parentControl = dateControlWithCheckBox;
    dateControlWithCheckBox.addCell(dateControlWithCheckBox.dateControl);
    dateControlWithCheckBox.dateControl.action = function () { this.parentControl.action(); }
    dateControlWithCheckBox.dateControl.setKey = function (key) {
        this.key = key;
        this.parentControl.key = key;
        this.textBox.value = key.toLocaleString();
    }

    dateControlWithCheckBox.checkBox = this.CheckBox(name + "CheckBox", this.loc.Report.NotAssigned);
    dateControlWithCheckBox.checkBox.style.marginLeft = "5px";
    dateControlWithCheckBox.checkBox.parentControl = dateControlWithCheckBox;
    dateControlWithCheckBox.addCell(dateControlWithCheckBox.checkBox);
    dateControlWithCheckBox.checkBox.action = function () {
        this.parentControl.dateControl.setEnabled(!this.isChecked);
    }

    dateControlWithCheckBox.checkBox.setChecked = function (state) {
        this.image.style.visibility = (state) ? "visible" : "hidden";
        this.isChecked = state;
        this.parentControl.isChecked = state;
    }

    dateControlWithCheckBox.setEnabled = function (state) {
        this.checkBox.setEnabled(state);
        this.dateControl.setEnabled(state);
        this.isEnabled = state;
    }

    dateControlWithCheckBox.setKey = function (key) {
        this.key = key;
        this.dateControl.setKey(key);
    }

    dateControlWithCheckBox.setChecked = function (state) {
        this.checkBox.setChecked(state)
        this.isChecked = state;
        this.dateControl.setEnabled(!this.isChecked);
    }

    dateControlWithCheckBox.setChecked(false);

    dateControlWithCheckBox.action = function () { }

    return dateControlWithCheckBox;
}

StiMobileDesigner.prototype.DateControlWithCheckBox2 = function (name, width, toolTip) {
    var dateControlWithCheckBox = this.CreateHTMLTable();
    dateControlWithCheckBox.key = new Date();
    dateControlWithCheckBox.isChecked = true;
    dateControlWithCheckBox.isEnabled = true;
    if (name != null) this.options.controls[name] = dateControlWithCheckBox;

    dateControlWithCheckBox.checkBox = this.CheckBox(name + "CheckBox");
    dateControlWithCheckBox.checkBox.parentControl = dateControlWithCheckBox;
    dateControlWithCheckBox.addCell(dateControlWithCheckBox.checkBox);

    dateControlWithCheckBox.checkBox.action = function () {
        this.parentControl.dateControl.setEnabled(this.isChecked);
    }

    dateControlWithCheckBox.checkBox.setChecked = function (state) {
        this.image.style.visibility = (state) ? "visible" : "hidden";
        this.isChecked = state;
        this.parentControl.isChecked = state;
    }

    dateControlWithCheckBox.dateControl = this.DateControl(name, width, toolTip);
    dateControlWithCheckBox.dateControl.parentControl = dateControlWithCheckBox;
    dateControlWithCheckBox.addCell(dateControlWithCheckBox.dateControl).style.paddingLeft = "4px";

    dateControlWithCheckBox.dateControl.action = function () {
        this.parentControl.action();
    }

    dateControlWithCheckBox.dateControl.setKey = function (key) {
        this.key = key;
        this.parentControl.key = key;
        this.textBox.value = key.toLocaleString();
    }

    dateControlWithCheckBox.setEnabled = function (state) {
        this.checkBox.setEnabled(state);
        this.dateControl.setEnabled(state && this.isChecked);
        this.isEnabled = state;
    }

    dateControlWithCheckBox.setKey = function (key) {
        this.key = key;
        this.dateControl.setKey(key);
    }

    dateControlWithCheckBox.setChecked = function (state) {
        this.checkBox.setChecked(state)
        this.isChecked = state;
        this.dateControl.setEnabled(this.isChecked);
    }

    dateControlWithCheckBox.setChecked(true);

    dateControlWithCheckBox.action = function () { }

    return dateControlWithCheckBox;
}