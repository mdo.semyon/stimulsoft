﻿
StiMobileDesigner.prototype.BrushControl = function (name, caption, imageName, toolTip) {
    var brushControl = this.CreateHTMLTable();
    brushControl.jsObject = this;
    brushControl.name = name;
    brushControl.key = null;
    brushControl.isEnabled = true;
    this.options.controls[name] = brushControl;

    brushControl.button = this.StandartSmallButton(name + "Button", null, caption, imageName, toolTip, null);
    brushControl.addCell(brushControl.button);
    brushControl.menu = this.BrushMenu(name + "BrushMenu", brushControl.button);
    brushControl.menu.brushControl = brushControl;
    brushControl.button.brushMenu = brushControl.menu;

    //Override image
    var colorBar = document.createElement("div");
    colorBar.className = "stiColorControlWithImage_ColorBar";

    var icon = brushControl.button.image;
    var imageCell = brushControl.button.image.parentElement;
    imageCell.appendChild(colorBar);
    brushControl.button.image = colorBar;
    brushControl.button.icon = icon;

    //Override methods
    brushControl.setEnabled = function (state) {
        if (this.button.image) this.button.image.style.opacity = state ? "1" : "0.3";
        if (this.button.icon) this.button.icon.style.opacity = state ? "1" : "0.3";
        if (this.button.arrow) this.button.arrow.style.opacity = state ? "1" : "0.3";
        this.isEnabled = state;
        this.button.isEnabled = state;
        this.button.className = (state ? this.button.styles["default"] : this.button.styles["disabled"]) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    brushControl.button.action = function () {
        this.brushMenu.changeVisibleState(!this.brushMenu.visible);
    }

    brushControl.action = function () { }

    brushControl.setKey = function (key) {
        this.key = key;
        this.button.key = key;
        if (key == "StiEmptyValue") {
            this.button.image.style.background = "#ffffff";
            this.button.caption.innerHTML = "";
            return;
        }
        var brushColor = this.jsObject.GetColorFromBrushStr(key);
        this.button.image.style.opacity = 1;
        var color;
        if (brushColor == "transparent")
            color = "255,255,255";
        else {
            var colors = brushColor.split(",");
            if (colors.length == 4) {
                this.button.image.style.opacity = this.jsObject.StrToInt(colors[0]) / 255;
                colors.splice(0, 1);
            }
            color = colors[0] + "," + colors[1] + "," + colors[2];
        }

        this.button.image.style.background = "rgb(" + color + ")";
    }

    brushControl.menu.action = function () {
        this.brushControl.setKey(this.parentButton.key);
        this.brushControl.action();
    }

    return brushControl;
}
