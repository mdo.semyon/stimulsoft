﻿
StiMobileDesigner.prototype.FontList = function (name, width, height, cutMenu, toolTip) {
    var fontList = this.DropDownList(name, width, toolTip ? toolTip : this.loc.HelpDesigner.FontName, this.GetFontNamesItems(), true, false, height, cutMenu);
    fontList.isFontList = true;

    var innerContent = fontList.menu.innerContent;    
    var sysFHeader = this.StandartMenuHeader(this.loc.PropertyMain.SystemFonts);

    if (innerContent.firstChild)
        innerContent.insertBefore(sysFHeader, innerContent.firstChild);
    else
        innerContent.appendChild(sysFHeader);

    var customFContainer = document.createElement("div");
    innerContent.insertBefore(customFContainer, sysFHeader);

    fontList.menu.onshow = function () {
        while (customFContainer.childNodes[0])
            customFContainer.removeChild(customFContainer.childNodes[0]);

        //Add custom fonts
        var resourcesFonts = this.jsObject.options.resourcesFonts;

        if (resourcesFonts && this.jsObject.GetCountObjects(resourcesFonts) > 0) {
            var customFHeader = this.jsObject.StandartMenuHeader(this.jsObject.loc.PropertyMain.CustomFonts);
            customFContainer.appendChild(customFHeader);

            for (var resourceName in resourcesFonts) {
                var menuItem = this.jsObject.VerticalMenuItem(fontList.menu, "fontItemCustom" + resourceName, resourcesFonts[resourceName].originalFontFamily, null,
                    resourcesFonts[resourceName].originalFontFamily, this.jsObject.GetStyles("MenuStandartItem"));

                customFContainer.appendChild(menuItem);
            }
        }
    }

    if (this.options.jsMode) {
        fontList.menu.addItems = function (items) {
            while (this.innerContent.childNodes[0]) {
                this.innerContent.removeChild(this.innerContent.childNodes[0]);
            }

            innerContent = fontList.menu.innerContent;
            sysFHeader = this.jsObject.StandartMenuHeader(this.jsObject.loc.PropertyMain.SystemFonts);

            if (innerContent.firstChild)
                innerContent.insertBefore(sysFHeader, innerContent.firstChild);
            else
                innerContent.appendChild(sysFHeader);

            customFContainer = document.createElement("div");
            innerContent.insertBefore(customFContainer, sysFHeader);

            if (items && items.length) {
                for (var i = 0; i < items.length; i++) {
                    var item = this.jsObject.VerticalMenuItem(this, items[i].name, items[i].caption, items[i].imageName, items[i].key, this.itemsStyles, items[i].haveSubMenu);
                    this.innerContent.appendChild(item);
                }
            }
        }

        fontList.addItems = function (items) {
            fontList.items = items;
            fontList.menu.addItems(items);
        }
    }

    //Override
    fontList.setKey = function (key) {
        this.key = key;
        if (key == null) return;
        if (key == "StiEmptyValue") {
            this.textBox.value = "";
            return;
        }
        if (this.menu && this.menu.items) {
            for (var itemName in this.menu.items)
                if (key == this.menu.items[itemName].key) {
                    this.textBox.value = this.menu.items[itemName].caption_;
                    if (this.image) this.image.src = this.jsObject.options.images[this.menu.items[itemName].imageName];
                    return;
                }
        }
        this.textBox.value = key.toString();
    }

    return fontList;
}