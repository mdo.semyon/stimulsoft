﻿
StiMobileDesigner.prototype.ImageControl = function (name, width, height, hideButtons, easyType) {
    var imageControl = document.createElement("div");
    imageControl.src = null;
    imageControl.jsObject = this;
    if (name != null) this.options.controls[name] = imageControl;
    var innerTable = this.CreateHTMLTable();
    innerTable.style.width = "100%";
    imageControl.appendChild(innerTable);
    imageControl.className = "stiImageContainerWithBorder";
    if (!width) width = 250;
    if (!height) height = 100;
    imageControl.style.width = width + "px";
    imageControl.style.height = height + "px";
        
    var imageContainer = document.createElement("img");
    imageContainer.style.display = "none";
    imageContainer.style.maxWidth = (easyType || hideButtons ? width : (width - 40)) + "px";
    imageContainer.style.maxHeight = height + "px";
    imageControl.imageContainer = imageContainer;

    imageContainer.onerror = function () {
        imageContainer.style.display = "none";
        if (imageContainer.src && imageContainer.src.indexOf("data:image/x-wmf;") >= 0) {
            imageControl.jsObject.SendCommandToDesignerServer("ConvertMetaFileToPng", { fileContent: imageContainer.src }, function (answer) {
                if (answer.fileContent) {
                    imageContainer.src = answer.fileContent;
                    imageContainer.style.display = "";
                }
            });
        }
    }

    imageContainer.isValidSrc = function (src) {
        return (src && src.indexOf("{") != 0)
    }
        
    var imageCell = innerTable.addCell();
    imageCell.style.width = (easyType || hideButtons ? width : (width - 40)) + "px";
    imageCell.style.height = height + "px";
    imageCell.style.textAlign = "center";
    imageCell.appendChild(imageContainer);

    var filesMask = ".bmp,.gif,.jpeg,.jpg,.png,.tiff,.ico,.emf,.wmf,.svg";

    if (!easyType) {
        var hintText = document.createElement("div");
        hintText.className = "stiDesignerTextContainer stiDragAndDropHintText";
        hintText.innerHTML = this.loc.FormDictionaryDesigner.TextDropImageHere;
        imageCell.appendChild(hintText);

        if (!hideButtons) {
            var openButton = this.SmallButton(null, null, null, "Open.png", this.loc.MainMenu.menuFileOpen.replace("&", "").replace("...", ""), null, this.GetStyles("FormButton"));
            var removeButton = this.SmallButton(null, null, null, "Remove.png", this.loc.MainMenu.menuEditDelete.replace("&", ""), null, this.GetStyles("FormButton"));
            imageContainer.removeButton = removeButton;
            openButton.style.margin = "4px";
            removeButton.style.margin = "0 4px 4px 4px";
            removeButton.setEnabled(false);

            var buttonsCell = innerTable.addCell();
            buttonsCell.style.width = "1px";
            buttonsCell.style.verticalAlign = "top";
            buttonsCell.appendChild(openButton);
            buttonsCell.appendChild(removeButton);

            removeButton.action = function () {
                imageControl.setImage(null);
                imageControl.action();
            }
                        
            openButton.action = function () {
                if (this.jsObject.options.canOpenFiles) {
                    var openDialog = this.jsObject.InitializeOpenDialog("imageControlImageDialog", function (evt) {
                        var files = evt.target.files;

                        for (var i = 0, f; f = files[i]; i++) {
                            var reader = new FileReader();
                            reader.jsObject = this.jsObject;

                            reader.onload = (function (theFile) {
                                return function (e) {
                                    if (theFile.size > reader.jsObject.options.reportResourcesMaximumSize) {
                                        var errorMessageForm = reader.jsObject.options.forms.errorMessageForm || reader.jsObject.InitializeErrorMessageForm();
                                        errorMessageForm.show(reader.jsObject.loc.Notices.QuotaMaximumFileSizeExceeded + "<br>" +
                                            reader.jsObject.loc.PropertyMain.Maximum + ": " +
                                            reader.jsObject.numberWithSpaces(reader.jsObject.options.reportResourcesMaximumSize), "Warning");
                                        return;
                                    }
                                    reader.jsObject.ResetOpenDialogs();
                                    imageControl.setImage(e.target.result);
                                    imageControl.action(files, e.target.result, evt.target.value);
                                    reader.jsObject.ReturnFocusToDesigner();
                                };
                            })(f);

                            reader.readAsDataURL(f);
                        }
                    }, filesMask);
                    openDialog.action();
                }
            }
        }

        imageControl.action = function () { }

        imageControl.setImage = function (imageSrc) {
            imageContainer.style.display = imageSrc ? "" : "none";
            hintText.style.display = imageSrc ? "none" : "";
            if (imageSrc && imageContainer.isValidSrc(imageSrc)) {
                imageContainer.src = imageSrc;
            }
            else {
                imageContainer.removeAttribute("src");
            }
            imageControl.src = imageSrc;
            if (imageContainer.removeButton) removeButton.setEnabled(imageSrc);
        }

        this.AddDragAndDropToContainer(imageCell, function (files, content) {
            var fileName = files[0].name.toLowerCase();

            var fileExt = fileName.substring(fileName.lastIndexOf("."));
            if (filesMask.indexOf(fileExt) >= 0) {
                imageControl.setImage(content);
                imageControl.action(files, content);
            }
        });
    }
    else {
        imageControl.setImage = function (imageSrc) {
            imageContainer.style.display = imageSrc ? "" : "none";
            if (imageSrc && imageContainer.isValidSrc(imageSrc))
                imageContainer.src = imageSrc
            else {
                imageContainer.removeAttribute("src");
            }
            imageControl.src = imageSrc;
        }
    }

    imageControl.resize = function (newWidth, newHeight) {
        imageControl.style.width = newWidth + "px";
        imageControl.style.height = newHeight + "px";
        imageContainer.style.maxWidth = (easyType || hideButtons ? newWidth : (newWidth - 40)) + "px";
        imageContainer.style.maxHeight = newHeight + "px";
        imageCell.style.width = (easyType || hideButtons ? newWidth : (newWidth - 40)) + "px";
        imageCell.style.height = newHeight + "px";
    }

    return imageControl;
}