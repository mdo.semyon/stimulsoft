﻿
StiMobileDesigner.prototype.GroupPanel = function (caption) {
    var groupPanel = document.createElement("fieldset");
    groupPanel.caption = document.createElement("legend");
    groupPanel.caption.className = "stiDesignerGroupPanelCaption";
    groupPanel.caption.innerHTML = caption;
    groupPanel.appendChild(groupPanel.caption);
    groupPanel.className = "stiDesignerGroupPanel";

    groupPanel.container = document.createElement("div");
    groupPanel.appendChild(groupPanel.container);

    return groupPanel;
}

StiMobileDesigner.prototype.InteractiveGroupPanel = function (caption) {
    var groupPanel = document.createElement("div");
    groupPanel.jsObject = this;
    groupPanel.isOpening = false;

    var header = this.SmallButton(null, null, caption, "ArrowRight.png", null, null, this.GetStyles("GroupPanelHeader"), true);
    groupPanel.appendChild(header);

    var container = document.createElement("div");
    container.style.display = "none";
    groupPanel.appendChild(container);
    groupPanel.container = container;

    header.action = function () {
        groupPanel.setOpening(!groupPanel.isOpening);
    }

    groupPanel.setOpening = function (state) {
        this.isOpening = state;
        container.style.display = state ? "" : "none";
        header.image.src = this.jsObject.options.images[state ? "ArrowDownGray.png" : "ArrowRight.png"];
    }

    return groupPanel;
}