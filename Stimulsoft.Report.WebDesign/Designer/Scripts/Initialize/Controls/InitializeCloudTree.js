﻿
StiMobileDesigner.prototype.CloudTree = function () {
    var tree = document.createElement("div");
    tree.style.overflow = "auto";
    tree.style.position = "relative";    
    tree.jsObject = this;
    tree.selectedItem = null;
    tree.returnItems = {};
    tree.style.minWidth = "250px";
    tree.style.minHeight = "250px";

    var innerContainer = document.createElement("div");
    tree.appendChild(innerContainer);
    tree.innerContainer = innerContainer;
    
    tree.clear = function () {
        while (innerContainer.childNodes[0]) innerContainer.removeChild(innerContainer.childNodes[0]);
        tree.selectedItem = null;
    }

    tree.getOptionsForTree = function (rootItemKey) {
        var optionsObject = {
            Options: {
                SortType: "Name",
                SortDirection: "Ascending",
                ViewMode: "All",
                FilterIdent: "ReportTemplateItem"
            }
        }
        if (rootItemKey) optionsObject.ItemKey = rootItemKey;

        return optionsObject;
    }

    tree.addItems = function (rootItemObject, returnItemObject, dataItems) {
        tree.clear();
        tree.rootItem = tree.jsObject.CloudTreeItem(tree, rootItemObject || { Ident: "RootAllItem", Name: "RootFolder", Key: "root" });
        
        if (tree.rootItem.itemObject.Key != "root" && returnItemObject) {
            var returnItem = tree.jsObject.CloudTreeItem(tree, { Ident: "ReturnItem", Name: ". . .", Key: returnItemObject.Key + "ReturnItem", ReturnObject: returnItemObject });
            innerContainer.appendChild(returnItem);
            tree.returnItems[tree.rootItem.itemObject.Key] = returnItemObject;
        }

        if (dataItems) {
            for (var i = 0; i < dataItems.length; i++) {
                var item = tree.jsObject.CloudTreeItem(tree, dataItems[i]);
                innerContainer.appendChild(item);
            }
        }
    }

    tree.build = function (rootItemObject, returnItemObject, selectItemKey) {
        tree.clear();
        tree.progress.show(null, 150);
        tree.jsObject.SendCloudCommand("ItemFetchAll", this.getOptionsForTree(rootItemObject && rootItemObject.Key != "root" ? rootItemObject.Key : null),
            function (data) {
                if (data.ResultItems) {
                    tree.progress.hide();
                    tree.addItems(rootItemObject, returnItemObject, data.ResultItems);
                    tree.onbuildcomplete();
                    if (selectItemKey) {
                        for (var i = 0 ; i < innerContainer.childNodes.length; i++) {
                            if (innerContainer.childNodes[i].itemObject && selectItemKey == innerContainer.childNodes[i].itemObject.Key) {
                                innerContainer.childNodes[i].action();
                            }
                        }
                    }
                }
            },
            function (data, ignoreErrorMessage) {
                tree.progress.hide();
                if (!ignoreErrorMessage) {
                    var errorMessageForm = tree.jsObject.options.forms.errorMessageForm || tree.jsObject.InitializeErrorMessageForm();
                    errorMessageForm.show(tree.jsObject.formatResultMsg(data));
                }
            });
    }
    
    tree.action = function (item) { }
    tree.ondblClickAction = function (item) { }
    tree.onbuildcomplete = function () { }
        
    return tree;
}

StiMobileDesigner.prototype.CloudTreeItem = function (tree, itemObject) {
    var caption = itemObject.Name;
    var image = "CloudItems.BigFile.png";
    switch (itemObject.Ident){
        case "ReportTemplateItem": image = "CloudItems.BigReportTemplate.png"; break;
        case "FolderItem": image = "CloudItems.BigFolder.png"; break;
        case "ReturnItem": image = "CloudItems.ReturnItem.png"; break;
    } 

    var button = this.SmallButton(null, null, caption, image, caption, null, this.GetStyles("StandartSmallButton"), true);
    if (itemObject.Ident == "ReportTemplateItem") button.style.cursor = "pointer";
    if (button.imageCell) button.imageCell.style.padding = "0 10px 0 10px";
    if (button.caption) {
        button.caption.style.padding = "0 20px 0 10px";
        button.caption.style.lineHeight = "1.4";
    }

    button.tree = tree;
    button.itemObject = itemObject;
    button.style.minWidth = "250px";
    button.style.height = "50px";
    button.style.margin = "0px 10px 3px 10px";

    button.ondblclick = function () {
        if (this.itemObject.Ident == "FolderItem") {
            tree.build(this.itemObject, this.tree.rootItem.itemObject);
        }
        if (this.itemObject.Ident == "ReturnItem") {
            tree.build(this.itemObject.ReturnObject, this.tree.returnItems[this.itemObject.ReturnObject.Key]);
        }
        tree.ondblClickAction(this);
        if (event) event.stopPropagation();
    }

    button.action = function () {
        if (this.tree.selectedItem) this.tree.selectedItem.setSelected(false);
        this.setSelected(true);
        this.tree.selectedItem = this;
        this.tree.action(this);
    }
    

    return button;
}