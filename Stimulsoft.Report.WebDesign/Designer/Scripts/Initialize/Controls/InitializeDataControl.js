﻿
StiMobileDesigner.prototype.DataControl = function (name, width) {
    var dataControl = this.TextBoxWithEditButton(name, width, null, true);

    dataControl.button.action = function () {
        this.key = this.textBox.value;
        var this_ = this;
        this.jsObject.InitializeDataColumnForm(function (dataColumnForm) {
            dataColumnForm.parentButton = this_;
            dataColumnForm.changeVisibleState(true);

            dataColumnForm.action = function () {
                this.changeVisibleState(false);
                this.parentButton.textBox.value = this.dataTree.key;
                dataControl.action();
            }
        });
    }

    dataControl.action = function () { }

    return dataControl;
}