﻿
StiMobileDesigner.prototype.TextAreaWithEditButton = function (name, width, height, readOnly) {

    var textAreaWithButton = this.CreateHTMLTable();
    textAreaWithButton.textArea = this.TextArea(name, width, height);
    textAreaWithButton.addCell(textAreaWithButton.textArea);

    textAreaWithButton.button = this.SmallButton(name + "Button", null, null, "TextEditButton.png", this.loc.QueryBuilder.Edit, null, this.GetStyles("FormButton"), true);
    //Override
    textAreaWithButton.button.style.width = this.options.isTouchDevice ? "26px" : "21px";
    textAreaWithButton.button.style.height = this.options.isTouchDevice ? "26px" : "21px";
    textAreaWithButton.button.innerTable.style.width = "100%";
    textAreaWithButton.button.textArea = textAreaWithButton.textArea;
    var buttonCell = textAreaWithButton.addCell(textAreaWithButton.button);
    buttonCell.style.verticalAlign = "top";
    textAreaWithButton.button.style.marginLeft = "4px";

    if (readOnly) {
        textAreaWithButton.textArea.readOnly = readOnly;
        textAreaWithButton.textArea.style.cursor = "default";
        textAreaWithButton.textArea.mainControl = textAreaWithButton;
        
        textAreaWithButton.textArea.onclick = function () {
            if (!this.isTouchEndFlag && !this.jsObject.options.isTouchClick) 
                this.mainControl.button.onclick();
        }

        textAreaWithButton.textArea.ontouchend = function () {
            var this_ = this;
            this.isTouchEndFlag = true;
            clearTimeout(this.isTouchEndTimer);
            this.mainControl.button.ontouchend();
            this.isTouchEndTimer = setTimeout(function () {
                this_.isTouchEndFlag = false;
            }, 1000);
        }
    }

    return textAreaWithButton;
}

StiMobileDesigner.prototype.ExpressionTextArea = function (name, width, height) {
    var textAreaParent = document.createElement("div");
    textAreaParent.jsObject = this;
    if (name) this.options.controls[name] = textAreaParent;
    var textArea = this.TextArea(null, width, height);
    textAreaParent.appendChild(textArea);
    textAreaParent.textArea = textArea;
    textArea.style.borderStyle = "dashed";
    textArea.style.overflowY = "hidden";
    textArea.addInsertButton();
    textArea.insertButton.image.src = this.options.images["EditButton.png"];
    textArea.insertButton.style.margin = "5px 0 0 " + (width - (this.options.isTouchDevice ? 31 : 23)) + "px";
   
    textArea.insertButton.action = function () {
        var this_ = this;
        this.jsObject.InitializeExpressionEditorForm(function (expressionEditorForm) {
            var propertiesPanel = expressionEditorForm.jsObject.options.propertiesPanel;
            expressionEditorForm.propertiesPanelZIndex = propertiesPanel.style.zIndex;
            expressionEditorForm.propertiesPanelIsEnabled = propertiesPanel.isEnabled;
            expressionEditorForm.resultControl = textArea;
            expressionEditorForm.changeVisibleState(true);
        });
    }

    textAreaParent.setValue = function (value) {
        textArea.value = value;
    }

    textAreaParent.getValue = function () {
        return textArea.value;
    }

    textAreaParent.action = function () { }
    textArea.action = function () { textAreaParent.action(); }

    return textAreaParent;
}