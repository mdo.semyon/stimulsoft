﻿
StiMobileDesigner.prototype.ColorsCollectionControl = function (name, toolTip, width, height) {
    var colorsControl = this.PropertyTextBoxWithEditButton(name, width, true);
    colorsControl.key = [];

    colorsControl.button.action = function () {
        this.jsObject.InitializeColorsCollectionForm(function (colorsCollectionForm) {

            colorsCollectionForm.action = function () {
                colorsCollectionForm.changeVisibleState(false);                
                var colors = [];
                for (var i = 0; i < colorsCollectionForm.controls.colorsContainer.childNodes.length; i++) {
                    colors.push(colorsCollectionForm.controls.colorsContainer.childNodes[i].key);
                }
                colorsControl.setKey(colors);
                colorsControl.action();
            }

            colorsCollectionForm.show(colorsControl.key);
        });
    }

    colorsControl.setKey = function (key) {
        this.key = key;
        this.textBox.value = "[" + (!key || key.length == 0 ? this.jsObject.loc.Report.No : this.jsObject.loc.PropertyCategory.ColorsCategory) + "]";
    }

    colorsControl.action = function () { }

    return colorsControl;
}