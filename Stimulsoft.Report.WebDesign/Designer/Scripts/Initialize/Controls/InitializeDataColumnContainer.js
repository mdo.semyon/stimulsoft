﻿
StiMobileDesigner.prototype.DataColumnContainer = function (name, headerText, width, showItemImage) {
    var container = document.createElement("div");
    container.name = name;
    container.isSelected = false;
    var jsObject = this;
    container.dataColumn = "";
    container.dataColumnObject = null;
    container.allowSelected = false;
    if (width) container.style.width = width + "px";

    var innerTable = this.CreateHTMLTable();
    innerTable.style.width = innerTable.style.height = "100%";
    container.appendChild(innerTable);

    if (headerText) {
        var headerCell = innerTable.addTextCell(headerText);
        headerCell.className = "stiDesignerTextContainer";
        headerCell.style.padding = "12px 0 12px 0";
    }

    var innerContainer = document.createElement("div");
    container.innerContainer = innerContainer;
    innerContainer.className = "stiDataColumnContainer";
    innerContainer.style.height = "30px";
    innerTable.addCellInNextRow(innerContainer);
        
    container.clear = function () {
        container.dataColumn = "";
        container.dataColumnObject = null;
        container.item = null;
        container.setSelected(false);
        while (innerContainer.childNodes[0]) innerContainer.removeChild(innerContainer.childNodes[0]);

        var hintText = document.createElement("div");
        hintText.style.position = "absolute";
        hintText.style.top = "calc(50% - 6px)";
        hintText.style.width = "100%";
        hintText.style.textAlign = "center";
        hintText.innerHTML = jsObject.loc.Dashboard.DragDropDataFromDictionary;
        innerContainer.appendChild(hintText);
        innerContainer.style.borderStyle = "dashed";
    }

    container.addColumn = function (dataColumn, dataColumnObject) {
        while (innerContainer.childNodes[0]) innerContainer.removeChild(innerContainer.childNodes[0]);

        container.dataColumn = dataColumn;
        container.dataColumnObject = dataColumnObject;
        innerContainer.style.borderStyle = "solid";
        innerContainer.style.borderColor = "";

        var imageName = null;
        if (dataColumnObject) {
            if (dataColumnObject.typeItem == "Column" && jsObject.options.images[dataColumnObject.typeIcon + ".png"])
                imageName = dataColumnObject.typeIcon + ".png";
            else if (dataColumnObject.typeItem == "Meter" && jsObject.options.images["Dashboards.Meters." + dataColumnObject.typeIcon + ".png"])
                imageName = "Dashboards.Meters." + dataColumnObject.typeIcon + ".png";
        }

        var button = jsObject.StandartSmallButton(null, null, dataColumn, showItemImage ? imageName : null);
        innerContainer.appendChild(button);
        container.item = button;

        button.style.height = "30px";
        button.innerTable.style.width = "100%";
        if (button.caption) button.caption.style.width = "100%";
        button.itemObject = dataColumnObject;
        button.container = container;

        button.onmousedown = function (event) {
            if (this.isTouchStartFlag) return;
            if (event) event.preventDefault();

            if (dataColumnObject.typeItem == "Meter" && event.button != 2 && !this.jsObject.options.controlsIsFocused) {
                var itemInDragObject = this.jsObject.TreeItemForDragDrop({ name: dataColumn }, null, true);
                if (itemInDragObject.button.captionCell) itemInDragObject.button.captionCell.style.padding = "3px 15px 3px 5px";
                itemInDragObject.originalItem = this;
                itemInDragObject.beginingOffset = 0;
                this.jsObject.options.itemInDrag = itemInDragObject;
            }
        }

        var closeButton = jsObject.StandartSmallButton(null, null, null, "CloseForm.png");
        closeButton.style.background = "white";
        closeButton.style.height = closeButton.style.width = "26px";
        closeButton.style.marginRight = "2px";
        closeButton.style.display = "none";
        closeButton.innerTable.style.width = "100%";
        button.closeButton = closeButton;
        button.innerTable.addCell(closeButton);

        closeButton.onmouseenter = function () {
            if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
            this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
            this.isOver = true;
            closeButton.style.background = "lightgray";
        }

        closeButton.onmouseleave = function () {
            this.isOver = false;
            if (!this.isEnabled) return;
            this.className = (this.isSelected ? this.styles["selected"] : this.styles["default"]) + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
            this.style.background = "white";
        }

        closeButton.action = function () {
            closeButton.clicked = true;
            container.clear();
            container.action();
        }

        button.onmouseenter = function () {
            if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
            this.className = this.styles["over"] + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
            this.isOver = true;
            closeButton.style.display = "";
        }

        button.onmouseleave = function () {
            this.isOver = false;
            if (!this.isEnabled) return;
            this.className = (this.isSelected ? this.styles["selected"] : this.styles["default"]) + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
            closeButton.style.display = "none";
        }

        if (container.allowSelected) {
            button.action = function () {
                if (!closeButton.clicked) {
                    container.setSelected(true);
                    container.onSelected();
                }
                closeButton.clicked = false;
            }
        }
    }

    innerContainer.canInsert = function () {
        var typeItem = jsObject.options.itemInDrag ? jsObject.options.itemInDrag.originalItem.itemObject.typeItem : null;

        return (typeItem && typeItem == "Column" || typeItem == "DataSource" || typeItem == "BusinessObject" || typeItem == "Meter");
    }

    innerContainer.onmouseover = function () {
        if (innerContainer.canInsert()) {
            innerContainer.style.borderStyle = "dashed";      
            innerContainer.style.borderColor = jsObject.options.themeColors[jsObject.GetThemeColor()];
        }
    }

    innerContainer.onmouseout = function () {
        innerContainer.style.borderStyle = container.dataColumn ? "solid" : "dashed";
        innerContainer.style.borderColor = "";
        innerContainer.style.borderWidth = "1px";
    }   

    innerContainer.onmouseup = function () {
        if (innerContainer.canInsert()) {
            var itemObject = jsObject.CopyObject(jsObject.options.itemInDrag.originalItem.itemObject);
            var columnText = jsObject.options.itemInDrag.originalItem.getResultForEditForm(true);

            if (itemObject.typeItem == "Column") {
                var columnParent = jsObject.options.dictionaryTree.getCurrentColumnParent();
                if (columnParent) {
                    itemObject.currentParentType = columnParent.type;
                    itemObject.currentParentName = (columnParent.type == "BusinessObject") ? jsObject.options.itemInDrag.originalItem.getBusinessObjectFullName() : columnParent.name;
                }
            }
            else {
                if (itemObject.columns && itemObject.columns.length > 0) {
                    var columnObject = jsObject.CopyObject(itemObject.columns[0]);
                    columnObject.currentParentType = itemObject.typeItem;
                    columnObject.currentParentName = itemObject.name;
                    itemObject = columnObject;

                    jsObject.options.itemInDrag.originalItem.completeBuildTree();

                    for (var key in jsObject.options.itemInDrag.originalItem.childs) {
                        var columnItem = jsObject.options.itemInDrag.originalItem.childs[key];
                        if (columnItem && columnItem.itemObject && columnItem.itemObject.typeItem == "Column" && columnItem.itemObject.name == columnObject.name) {
                            var columnText = columnItem.getResultForEditForm(true);
                        }
                    }
                }
            }
            container.addColumn(columnText, itemObject);
            container.action();
        }
    }

    innerContainer.ontouchend = function () {
        innerContainer.onmouseup();
    }

    container.setSelected = function (state) {
        this.isSelected = state;
        if (this.item) this.item.setSelected(state);
    }

    container.action = function () { }

    container.onSelected = function () { }

    container.clear();

    return container;
}