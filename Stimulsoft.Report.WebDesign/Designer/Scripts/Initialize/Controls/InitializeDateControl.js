﻿
StiMobileDesigner.prototype.DateControl = function (name, width, toolTip) {
    var dateControl = this.CreateHTMLTable();
    dateControl.jsObject = this;
    if (name != null) this.options.controls[name] = dateControl;
    dateControl.name = name != null ? name : this.generateKey();
    dateControl.key = new Date();
    dateControl.isEnabled = true;
    dateControl.isSelected = false;
    dateControl.isOver = false;
    dateControl.style.margin = "0px";
    dateControl.style.padding = "0px";
    if (toolTip) dateControl.setAttribute("title", toolTip);
    var textBoxWidth = width - (this.options.isTouchDevice ? 23 : 15);
    dateControl.className = this.options.isTouchDevice ? "stiDesignerDateControl_Touch" : "stiDesignerDateControl_Mouse";

    //TextBox
    dateControl.textBox = document.createElement("input");
    dateControl.textBox.jsObject = this;
    dateControl.addCell(dateControl.textBox);
    dateControl.textBox.style.width = textBoxWidth + "px";
    dateControl.textBox.dateControl = dateControl;
    dateControl.textBox.readOnly = true;
    dateControl.textBox.style.cursor = "default";
    dateControl.textBox.value = dateControl.key.toLocaleString();
    dateControl.textBox.className = this.options.isTouchDevice ? "stiDesignerDateControl_TextBox_Touch" : "stiDesignerDateControl_TextBox_Mouse";
    dateControl.textBox.onclick = function () {
        if (!this.isTouchEndFlag)
            this.dateControl.button.onclick(); 
    }

    dateControl.textBox.ontouchend = function () {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        this.dateControl.button.ontouchend();
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    dateControl.textBox.onfocus = function () {
        this.jsObject.options.controlsIsFocused = this;
    }

    dateControl.textBox.onblur = function () {
        this.jsObject.options.controlsIsFocused = false;
    }

    //DropDownButton
    dateControl.button = this.SmallButton((name != null) ? name + "DropDownButton" : null, null, null, "DropDownButton.png", null, null, this.GetStyles("DateControlDropDownListButton"));
    dateControl.addCell(dateControl.button);
    dateControl.button.dateControl = dateControl;
    dateControl.button.action = function () {
        var datePicker = this.jsObject.options.menus.datePicker || this.jsObject.InitializeDatePicker();
        datePicker.parentDataControl = this.dateControl;
        datePicker.parentButton = this.dateControl.button;
        datePicker.changeVisibleState(!datePicker.visible);
    }

    dateControl.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    dateControl.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        if (!this.isSelected) {
            this.className = "stiDesignerDateControlOver" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        }
    }

    dateControl.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.isOver = false;
        if (!this.isSelected) {
            this.className = "stiDesignerDateControl" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
        }
    }

    dateControl.setEnabled = function (state) {
        this.isEnabled = state;
        this.button.setEnabled(state);
        this.textBox.disabled = !state;
        this.textBox.style.visibility = state ? "visible" : "hidden";
        this.className = (state ? "stiDesignerDateControl" : "stiDesignerDateControlDisabled") +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    dateControl.setSelected = function (state) {
        this.isSelected = state;
        this.className = (state ? "stiDesignerDateControlOver" :
            (this.isEnabled ? (this.isOver ? "stiDesignerDateControlOver" : "stiDesignerDateControl") : "stiDesignerDateControlDisabled")) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    dateControl.setKey = function (key) {
        this.key = key;
        this.textBox.value = key.toLocaleString();
    }

    dateControl.action = function () { }

    return dateControl;
}