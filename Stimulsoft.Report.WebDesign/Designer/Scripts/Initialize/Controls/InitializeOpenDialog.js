﻿
StiMobileDesigner.prototype.InitializeOpenDialog = function (nameDialog, actionFunction, fileMask) {
    if (this.options.openDialogs[nameDialog]) {
        this.options.mainPanel.removeChild(this.options.openDialogs[nameDialog]);
    }
    var inputFile = document.createElement("input");
    this.options.mainPanel.appendChild(inputFile);
    this.options.openDialogs[nameDialog] = inputFile;
    inputFile.style.display = "none";
    inputFile.id = nameDialog;
    inputFile.jsObject = this;
    inputFile.setAttribute("type", "file");
    inputFile.setAttribute("name", "files[]");
    inputFile.setAttribute("multiple", "");
    if (fileMask) inputFile.setAttribute("accept", fileMask);
    inputFile.addEventListener('change', actionFunction, false);

    inputFile.action = function () {
        this.style.display = "";
        this.focus();
        this.click();
        this.style.display = "none";
    }

    return inputFile;
}

StiMobileDesigner.prototype.ResetOpenDialogs = function () {
    for (var name in this.options.openDialogs) {
        var openDialog = this.options.openDialogs[name];
        if (openDialog) {
            openDialog.setAttribute("name", "files[]");
            openDialog.setAttribute("multiple", "");
            openDialog.setAttribute("value", "");
        }
    }
}

//Open Report
StiMobileDesigner.prototype.StiHandleOpenReport = function (evt) {
    var files = evt.target.files;
    var fileName = files[0] ? files[0].name : "Report";
    var filePath = evt.target.value;

    if (this.jsObject.options.jsMode) {
        var onlyPath = filePath;
        if (filePath && this.jsObject.EndsWith(evt.target.value, fileName)) {
            onlyPath = filePath.substring(0, filePath.length - fileName.length);
        }
        this.jsObject.SaveFileToRecentArray(fileName, onlyPath);
    }

    for (var i = 0, f; f = files[i]; i++) {
        var reader = new FileReader();
        reader.jsObject = this.jsObject;

        reader.onload = (function (theFile) {
            return function (e) {
                reader.jsObject.ResetOpenDialogs();
                reader.jsObject.OpenReport(fileName, reader.jsObject.options.mvcMode ? encodeURIComponent(e.target.result) : e.target.result, filePath);
                reader.jsObject.ReturnFocusToDesigner();
            };
        })(f);

        reader.readAsDataURL(f);
    }
}

StiMobileDesigner.prototype.OpenReport = function (fileName, fileContent, filePath) {
    if (this.EndsWith(fileName.toString().toLowerCase(), ".mrx")) {
        var passwordForm = this.options.forms.passwordForm || this.InitializePasswordForm();
        passwordForm.show(function (password) {
            this.jsObject.SendCommandOpenReport(fileContent, fileName, { password: password }, filePath);
        }, this.loc.Password.lbPasswordLoad);
    }
    else {
        this.SendCommandOpenReport(fileContent, fileName, { isPacked: this.EndsWith(fileName.toString().toLowerCase(), ".mrz") }, filePath);
    }
}

//Open Style
StiMobileDesigner.prototype.StiHandleOpenStyle = function (evt) {
    var files = evt.target.files;
    var fileName = files[0] ? files[0].name : "Styles";

    for (var i = 0, f; f = files[i]; i++) {
        var reader = new FileReader();
        reader.jsObject = this.jsObject;

        reader.onload = (function (theFile) {
            return function (e) {
                reader.jsObject.ResetOpenDialogs();                
                reader.jsObject.SendCommandOpenStyle(reader.jsObject.options.mvcMode ? encodeURIComponent(e.target.result) : e.target.result, fileName);
                reader.jsObject.ReturnFocusToDesigner();
            };
        })(f);

        reader.readAsDataURL(f);
    }
}

//Open Page
StiMobileDesigner.prototype.StiHandleOpenPage = function (evt) {
    var files = evt.target.files;
    var fileName = files[0] ? files[0].name : "Page";

    for (var i = 0, f; f = files[i]; i++) {
        var reader = new FileReader();
        reader.jsObject = this.jsObject;

        reader.onload = (function (theFile) {
            return function (e) {
                reader.jsObject.ResetOpenDialogs();
                reader.jsObject.SendCommandOpenPage(reader.jsObject.options.mvcMode ? encodeURIComponent(e.target.result) : e.target.result, fileName);
                reader.jsObject.ReturnFocusToDesigner();
            };
        })(f);

        reader.readAsDataURL(f);
    }
}