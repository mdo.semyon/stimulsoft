﻿
StiMobileDesigner.prototype.FilterControl = function (name, columns, widthContainer, heightContainer, isConditionsFilter) {
    var filterControl = document.createElement("div");
    filterControl.name = name;
    filterControl.jsObject = this;
    filterControl.columns = columns;
    filterControl.currentDataSourceName = null;
    filterControl.isConditionsFilter = isConditionsFilter;
    filterControl.controls = {};

    //ToolBar
    var toolBar = this.CreateHTMLTable();
    toolBar.style.margin = "4px";
    filterControl.appendChild(toolBar);
    filterControl.controls.toolBar = toolBar;

    var controlProps = [
        ["addFilter", this.StandartSmallButton(null, null, this.loc.FormBand.AddFilter.replace("&", ""), "AddFilter.png")],
        ["separator"],
        ["moveUp", this.StandartSmallButton(null, null, null, "MoveUp.png")],
        ["moveDown", this.StandartSmallButton(null, null, null, "MoveDown.png")],
        ["separator"],
        ["filterOn", this.CheckBox(null, this.loc.PropertyMain.FilterOn)],
        ["separator"],
        ["filterMode", this.DinamicDropDownList(filterControl.name + "FilterMode", 60, null, this.GetFilterTypeItems(), true)]
    ]

    for (var i = 0; i < controlProps.length; i++) {
        if (controlProps[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var control = controlProps[i][1];
        control.style.margin = "0 2px 0 2px";
        filterControl.controls[controlProps[i][0]] = control;
        if (controlProps[i][0] == "filterMode") {
            var textCell = toolBar.addCell();
            textCell.className = "stiDesignerCaptionControls";
            textCell.innerHTML = this.loc.PropertyMain.FilterMode + ":";
        }
        if (controlProps[i][0] == "filterOn") { control.style.margin = "0 6px 0 6px"; }
        toolBar.addCell(control);
    }

    //Container
    var filterContainer = this.FilterContainer(filterControl);
    filterControl.appendChild(filterContainer);
    if (widthContainer) filterContainer.style.width = widthContainer + "px";
    if (heightContainer) filterContainer.style.height = heightContainer + "px";
    filterControl.controls.filterContainer = filterContainer;

    filterControl.controls.addFilter.action = function () { filterContainer.addFilter(this.jsObject.FilterObject()); }
    filterControl.controls.moveUp.setEnabled(false);
    filterControl.controls.moveDown.setEnabled(false);

    filterControl.controls.moveUp.action = function () {
        if (filterContainer.selectedItem) { filterContainer.selectedItem.move("Up"); }
    }

    filterControl.controls.moveDown.action = function () {
        if (filterContainer.selectedItem) { filterContainer.selectedItem.move("Down"); }
    }

    filterContainer.onAction = function () {
        filterControl.controls.filterMode.setEnabled(filterContainer.childNodes.length > 1);
        var count = filterContainer.getCountItems();
        var index = filterContainer.selectedItem ? filterContainer.selectedItem.getIndex() : -1;
        filterControl.controls.moveUp.setEnabled(index > 0);
        filterControl.controls.moveDown.setEnabled(index != -1 && index < count - 1);
    }

    filterControl.fill = function (filters, filterOn, filterMode) {
        filterContainer.clear();
        filterControl.controls.filterOn.setChecked(filterOn);
        filterControl.controls.filterMode.setKey(filterMode);
        if (!filters) return;
        for (var i = 0; i < filters.length; i++) filterContainer.addFilter(filters[i], true);
        filterContainer.onAction();
    }

    filterControl.getValue = function () {
        result = {
            filters: [],
            filterOn: filterControl.controls.filterOn.isChecked,
            filterMode: filterControl.controls.filterMode.key
        }

        for (var num = 0; num < filterContainer.childNodes.length; num++) {
            var filter = this.jsObject.FilterObject();
            var filterItem = filterContainer.childNodes[num];
            result.filters.push(filter);
            filter.fieldIs = filterItem.fieldIs.key;
            filter.column = filterItem.column.textBox.value != "[" + this.jsObject.loc.PropertyEnum.StiCheckStyleNone + "]"
                ? (filterItem.column.key || filterItem.column.textBox.value) : "";
            filter.dataType = filterItem.dataType.key;
            filter.condition = filterItem.condition.key;
            filter.value1 = filterItem.value1.textBox.value;
            filter.value2 = filterItem.value2.textBox.value;
            filter.expression = filterItem.expression.textBox.value;
        }

        return result;
    }

    return filterControl;
}

StiMobileDesigner.prototype.FilterContainer = function (filterControl) {
    var filterContainer = document.createElement("div");
    filterContainer.jsObject = this;
    filterContainer.className = "stiDesignerFilterContainer";
    filterContainer.selectedItem = null;
    filterContainer.filterControl = filterControl;

    filterContainer.addFilter = function (filter, notAction) {
        var filterItem = this.jsObject.FilterItem(filterContainer);
        this.appendChild(filterItem);
        if (!notAction) filterContainer.onAction();

        filterItem.fieldIs.setKey(filter.fieldIs);
        filterItem.dataType.setKey(filter.dataType);
        var noneText = this.jsObject.loc.PropertyEnum.StiCheckStyleNone;
        if (filterControl.columns != null)
            filterItem.column.setKey(filter.column || "[" + noneText + "]");
        else
            filterItem.column.textBox.value = filter.column || "[" + noneText + "]";
        filterItem.value1.textBox.value = filter.value1;
        filterItem.value2.textBox.value = filter.value2;
        filterItem.expression.textBox.value = filter.expression;
        filterItem.condition.setKey(filter.condition);
        filterItem.updateControls();
        filterItem.condition.setKey(filter.condition);
    }

    filterContainer.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        filterControl.controls.filterMode.setEnabled(false);
    }

    filterContainer.getCountItems = function () {
        return filterContainer.childNodes.length;
    }

    filterContainer.onAction = function () { };

    return filterContainer;
}

StiMobileDesigner.prototype.FilterItem = function (filterContainer) {
    var filterItem = document.createElement("div");
    filterItem.jsObject = this;
    filterItem.key = this.newGuid().replace(/-/g, '');
    filterItem.isSelected = false;
    filterItem.className = "stiDesignerFilterPanel";

    //Header
    var header = this.CreateHTMLTable();
    header.className = "stiDesignerFilterPanelHeader";
    header.style.width = "100%";
    filterItem.appendChild(header);

    var headerButton = this.StandartSmallButton(null, null, this.loc.PropertyMain.Filter, "CheckBox.png");
    headerButton.style.margin = "2px 0px 2px 2px";
    headerButton.image.style.visibility = "hidden";
    headerButton.caption.style.width = "100%";
    headerButton.caption.style.textAlign = "center";
    header.addCell(headerButton).style.width = "100%";

    //Remove Button
    var removeButton = this.StandartSmallButton(null, null, null, "RemoveBlack.png");
    filterItem.removeButton = removeButton;
    removeButton.style.margin = "2px 2px 2px 0px";
    header.addCell(removeButton);
    removeButton.action = function () {
        filterItem.remove();
    }

    filterItem.setSelected = function (state) {
        if (state) {
            if (filterContainer.selectedItem) filterContainer.selectedItem.setSelected(false);
            filterContainer.selectedItem = this;
        }
        else {
            if (filterContainer.selectedItem && filterContainer.selectedItem == this) filterContainer.selectedItem = null;
        }
        filterItem.isSelected = state;
        headerButton.image.style.visibility = state ? "visible" : "hidden";
    }

    filterItem.remove = function () {
        filterContainer.removeChild(this);
        if (filterContainer.selectedItem == this) filterContainer.selectedItem = null;
        filterContainer.onAction();
    }

    filterItem.getIndex = function () {
        for (var i = 0; i < filterContainer.childNodes.length; i++)
            if (filterContainer.childNodes[i] == this) return i;
    };

    filterItem.move = function (direction) {
        var index = this.getIndex();
        filterContainer.removeChild(this);
        var count = filterContainer.getCountItems();
        var newIndex = direction == "Up" ? index - 1 : index + 1;
        if (direction == "Up" && newIndex == -1) newIndex = 0;
        if (direction == "Down" && newIndex >= count) {
            filterContainer.appendChild(this);
            filterContainer.onAction();
            return;
        }
        filterContainer.insertBefore(this, filterContainer.childNodes[newIndex]);
        filterContainer.onAction();
    }

    if (!filterContainer.filterControl.isConditionsFilter) {

        header.onclick = function () {
            if (this.isTouchEndFlag || this.jsObject.options.isTouchClick) return;
            this.action();
        }

        header.ontouchend = function () {
            if (this.jsObject.options.fingerIsMoved) return;
            var this_ = this;
            this.isTouchEndFlag = true;
            clearTimeout(this.isTouchEndTimer);
            this.action();
            this.isTouchEndTimer = setTimeout(function () {
                this_.isTouchEndFlag = false;
            }, 1000);
        }

        header.action = function () {
            filterItem.setSelected(!filterItem.isSelected);
            filterContainer.onAction();
        }

        headerButton.oldonmouseenter = headerButton.onmouseenter;
        headerButton.oldonmouseleave = headerButton.onmouseleave;
        headerButton.onmouseenter = function () {
            headerButton.oldonmouseenter();
            removeButton.onmouseenter();
        }
        headerButton.onmouseleave = function () {
            headerButton.oldonmouseleave();
            removeButton.onmouseleave();
        }
    }
    else {
        headerButton.onmouseover = null;
        headerButton.onmouseenter = null;
        headerButton.onmouseleave = null;
    }

    filterItem.innerTable = this.CreateHTMLTable();
    filterItem.innerTable.style.margin = "5px 0 0 5px";
    filterItem.appendChild(filterItem.innerTable);

    //Captions
    filterItem.fieldIsCaption = filterItem.innerTable.addCell();
    filterItem.fieldIsCaption.innerHTML = this.loc.PropertyMain.FieldIs;

    filterItem.dataTypeCaption = filterItem.innerTable.addCell();
    filterItem.dataTypeCaption.innerHTML = this.loc.PropertyMain.DataType;

    filterItem.columnCaption = filterItem.innerTable.addCell();
    filterItem.columnCaption.innerHTML = this.loc.PropertyMain.Column;

    //FieldIs
    filterItem.fieldIs = this.DinamicDropDownList(filterItem.key + "FieldIs", 115, null, this.GetFilterFieldIsItems(), true, false);
    filterItem.fieldIs.style.margin = "3px 7px 3px 0";
    filterItem.innerTable.addCellInNextRow(filterItem.fieldIs);
    filterItem.fieldIs.action = function () { filterItem.updateControls(); };

    //Data Type
    filterItem.dataType = this.DinamicDropDownList(filterItem.key + "DataType", 115, null, this.GetFilterDataTypeItems(), true, false);
    filterItem.dataType.style.margin = "3px 7px 3px 0";
    filterItem.innerTable.addCellInLastRow(filterItem.dataType);
    filterItem.dataType.action = function () { filterItem.updateControls(); };

    //Column
    if (filterContainer.filterControl && filterContainer.filterControl.columns) {
        filterItem.column = this.DinamicDropDownList(filterItem.key + "Column", 210, null, filterContainer.filterControl.columns, true, false);
        filterItem.column.style.margin = "3px 7px 3px 0";
        filterItem.innerTable.addCellInLastRow(filterItem.column);
    }
    else if (filterContainer.filterControl.isConditionsFilter) {
        filterItem.column = this.DataControl(filterItem.key + "Column", 230);
        filterItem.column.style.margin = "3px 7px 3px 0";
        filterItem.innerTable.addCellInLastRow(filterItem.column);
    }
    else {
        filterItem.column = this.TextBoxWithEditButton(filterItem.key + "Column", 230);
        filterItem.column.style.margin = "3px 7px 3px 0";
        filterItem.innerTable.addCellInLastRow(filterItem.column);
        filterItem.column.button.action = function () {
            var currentDataSourceName = (filterContainer.filterControl.currentDataSourceName != null)
                ? filterContainer.filterControl.currentDataSourceName
                : this.jsObject.options.selectedObject.properties["dataSource"];
            this.key = currentDataSourceName + "." + this.textBox.value;
            var this_ = this;

            this.jsObject.InitializeDataColumnForm(function (dataColumnForm) {
                dataColumnForm.dataTree.build("Column", currentDataSourceName);
                dataColumnForm.needBuildTree = false;
                dataColumnForm.parentButton = this_;
                dataColumnForm.changeVisibleState(true);
                dataColumnForm.action = function () {
                    this.changeVisibleState(false);
                    this.parentButton.textBox.value = this.dataTree.key != ""
                        ? this.dataTree.selectedItem.parent && this.dataTree.selectedItem.parent.itemObject.typeItem == "BusinessObject"
                            ? this.dataTree.key
                            : this.dataTree.key.substring(this.dataTree.key.indexOf(".") + 1, this.dataTree.key.length)
                        : "[" + this.jsObject.loc.PropertyEnum.StiCheckStyleNone + "]";
                }
            });
        }
    }

    filterItem.innerTable2 = this.CreateHTMLTable();
    filterItem.innerTable2.style.margin = "0 0 3px 5px";
    filterItem.appendChild(filterItem.innerTable2);

    //Condition
    filterItem.condition = this.DinamicDropDownList(filterItem.key + "Condition", 125, null, null, true, false);
    filterItem.condition.style.margin = "3px 7px 3px 0";
    filterItem.innerTable2.addCell(filterItem.condition);
    filterItem.condition.action = function () { filterItem.updateControls(); };

    //Value1
    filterItem.value1 = this.ExpressionControl(null, 160, null, null, true);
    filterItem.value1.style.margin = "3px 7px 3px 0";
    filterItem.innerTable2.addCell(filterItem.value1);

    //And Caption
    filterItem.andCaption = filterItem.innerTable2.addCell();
    filterItem.andCaption.innerHTML = this.loc.PropertyEnum.StiFilterModeAnd;

    //Value2
    filterItem.value2 = this.ExpressionControl(null, 160, null, null, true);
    filterItem.value2.style.margin = "3px 7px 3px 7px";
    filterItem.innerTable2.addCell(filterItem.value2);

    filterItem.innerTable3 = this.CreateHTMLTable();
    filterItem.innerTable3.style.margin = "0 0 3px 5px";
    filterItem.appendChild(filterItem.innerTable3);

    //Expression
    filterItem.expression = this.ExpressionControl(null, filterContainer.filterControl.isConditionsFilter ? 650 : 500, null, null, true);
    filterItem.expression.style.margin = "3px 7px 3px 0";
    filterItem.innerTable3.addCell(filterItem.expression);
    if (filterContainer.filterControl.columns) filterItem.expression.button.parentNode.style.display = "none";


    filterItem.updateControls = function () {
        this.condition.items = this.jsObject.GetFilterConditionItems(this.dataType.key);
        if (!this.condition.haveKey(this.condition.key) && this.condition.items != null && this.condition.items.length > 0) this.condition.setKey(this.condition.items[0].key);
        this.dataType.style.display = (this.fieldIs.key == "Value") ? "" : "none";
        this.dataTypeCaption.style.display = (this.fieldIs.key == "Value") ? "" : "none";
        this.column.style.display = (this.fieldIs.key == "Value") ? "" : "none";
        this.columnCaption.style.display = (this.fieldIs.key == "Value") ? "" : "none";
        this.condition.style.display = (this.fieldIs.key == "Value") ? "" : "none";
        this.value1.style.display = (this.fieldIs.key == "Value" && this.condition.key != "IsNull" && this.condition.key != "IsNotNull") ? "" : "none";
        this.andCaption.style.display = (this.fieldIs.key == "Value" && (this.condition.key == "Between" || this.condition.key == "NotBetween")) ? "" : "none";
        this.value2.style.display = (this.fieldIs.key == "Value" && (this.condition.key == "Between" || this.condition.key == "NotBetween")) ? "" : "none";
        this.expression.style.display = (this.fieldIs.key == "Expression") ? "" : "none";
        this.value1.button.parentElement.style.display = this.dataType.key == "Expression" && !filterContainer.filterControl.columns ? "" : "none";
        this.value2.button.parentElement.style.display = this.dataType.key == "Expression" && !filterContainer.filterControl.columns ? "" : "none";
    }

    return filterItem;
}

StiMobileDesigner.prototype.FilterObject = function () {
    var filter = {};
    filter.fieldIs = "Value";
    filter.column = "[" + this.loc.PropertyEnum.StiCheckStyleNone + "]";
    filter.dataType = "String";
    filter.condition = "EqualTo";
    filter.value1 = "";
    filter.value2 = "";
    filter.expression = "";

    return filter;
}