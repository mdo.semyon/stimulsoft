﻿
StiMobileDesigner.prototype.SummaryExpression = function (name) {
    var summaryExpression = document.createElement("div");
    summaryExpression.name = name;
    summaryExpression.controls = {};
    summaryExpression.jsObject = this;
    summaryExpression.style.overflowY = "auto";

    var expressionTextArea = this.TextArea(null, this.options.isTouchDevice ? 606 : 528, 50);
    expressionTextArea.style.margin = "8px 0 4px 8px";
    summaryExpression.appendChild(expressionTextArea);
    summaryExpression.controls.expressionTextArea = expressionTextArea;

    var controlsTable = this.CreateHTMLTable();
    summaryExpression.appendChild(controlsTable);

    controlProps = [
        ["summaryFunction", this.loc.FormSystemTextEditor.LabelSummaryFunction,
            this.DropDownList(name + "SummaryFunction", 250, null, this.GetTotalFuntionItems(true), true, false, null, true), "8px 8px 4px 0px"],
        ["dataBand", this.loc.FormSystemTextEditor.LabelDataBand,
            this.DropDownList(name + "DataBand", 250, null, null, true, false, null, true), "4px 8px 4px 0px"],
        ["dataColumn", this.loc.FormSystemTextEditor.LabelDataColumn,
            this.DataControl(name + "DataColumn", 250), "4px 8px 8px 0px"],
        ["summaryRunningHeader", null, this.FormBlockHeader(this.loc.FormSystemTextEditor.SummaryRunning), null],
        ["reportRadioButton", null, this.RadioButton(name + "Report", name + "RadioButtons", this.loc.FormSystemTextEditor.SummaryRunningByReport), "15px 8px 10px 15px"],
        ["columnRadioButton", null, this.RadioButton(name + "Column", name + "RadioButtons", this.loc.FormSystemTextEditor.SummaryRunningByColumn), "10px 8px 10px 15px"],
        ["pageRadioButton", null, this.RadioButton(name + "Page", name + "RadioButtons", this.loc.FormSystemTextEditor.SummaryRunningByPage), "10px 8px 15px 15px"],
        ["separator", null, this.FormSeparator(), null],
        ["runningTotal", null, this.CheckBox(name + "RunningTotal", this.loc.FormSystemTextEditor.RunningTotal), "10px 8px 10px 15px"],
        ["condition", null, this.CheckBox(name + "Condition", this.loc.FormSystemTextEditor.Condition), "10px 8px 10px 15px"],
        ["conditionText", null, this.ExpressionControl(name + "ConditionText", 400, null, null, true), "10px 8px 10px 15px"]
    ]

    for (var i = 0; i < controlProps.length; i++) {
        var control = controlProps[i][2];
        if (controlProps[i][3]) control.style.margin = controlProps[i][3];
        summaryExpression.controls[controlProps[i][0]] = control;

        if (controlProps[i][1] != null) {
            var textCell = controlsTable.addCellInNextRow();
            textCell.innerHTML = controlProps[i][1];
            textCell.className = "stiDesignerCaptionControlsBigIntervals";
            controlsTable.addCellInLastRow(control);
        }
        else {
            summaryExpression.appendChild(control);
        }

        if ("action" in control) control.action = function () {
            expressionTextArea.value = summaryExpression.getValue();
        }
    }

    summaryExpression.controls.condition.action = function () {
        summaryExpression.controls.conditionText.setEnabled(this.isChecked);
        expressionTextArea.value = summaryExpression.getValue();
    }

    summaryExpression.reset = function () {
        var dataBandItems = this.jsObject.GetDataBandItems();
        summaryExpression.controls.dataBand.items = dataBandItems;
        summaryExpression.controls.dataBand.menu.addItems(dataBandItems);
        summaryExpression.controls.dataBand.setKey("NotAssigned");
        summaryExpression.controls.conditionText.setEnabled(false);
        summaryExpression.controls.conditionText.textBox.value = "";
        summaryExpression.controls.condition.setChecked(false);
        summaryExpression.controls.runningTotal.setChecked(false);
        summaryExpression.controls.summaryFunction.setKey("Sum");
        summaryExpression.controls.dataColumn.textBox.value = "";
        summaryExpression.controls.reportRadioButton.onclick();
    }

    summaryExpression.fill = function (text) {
        summaryExpression.reset();
        if (!text) return;
        var startIndex = text.indexOf('(');
        var endIndex = text.lastIndexOf(')');

        if ((endIndex - startIndex) > 0) {
            var args = text.substr(startIndex + 1, endIndex - startIndex - 1);
            var strs = args.split(",");

            var funcStart = text.indexOf("{");
            if (funcStart != -1) {
                var funcEnd = text.indexOf("(", funcStart);
                if ((funcEnd - funcStart - 1) > 0) {
                    var func = text.substr(funcStart + 1, funcEnd - funcStart - 1);

                    var isRunningTotal = false;
                    var isCondition = false;

                    if (this.jsObject.EndsWith(func, "If")) {
                        func = func.substr(0, func.length - 2);
                        isCondition = true;
                    }
                    if (this.jsObject.EndsWith(func, "Running")) {
                        func = func.substr(0, func.length - 7);
                        isRunningTotal = true;
                    }
                    if (this.jsObject.EndsWith(func, "If")) {
                        func = func.substr(0, func.length - 2); //Second check, do not remove it
                        isCondition = true;
                    }

                    for (var i = 0; i < this.jsObject.options.aggrigateFunctions.length; i++) {
                        var srv = this.jsObject.options.aggrigateFunctions[i];
                        var result = false;
                        if (srv.serviceName == func) result = true;
                        if (("c" + srv.serviceName) == func) result = true;
                        if (("col" + srv.serviceName) == func) result = true;

                        if (result) {
                            summaryExpression.controls.summaryFunction.setKey(srv.serviceName);
                            if (text.indexOf("col" + srv.serviceName) != -1) summaryExpression.controls.columnRadioButton.onclick();
                            if (text.indexOf('c' + srv.serviceName) != -1) summaryExpression.controls.pageRadioButton.onclick();
                            summaryExpression.controls.condition.setChecked(isCondition);
                            summaryExpression.controls.conditionText.setEnabled(isCondition);
                            summaryExpression.controls.runningTotal.setChecked(isRunningTotal);
                            if (srv.recureParam) {
                                var strsLength = strs.length;
                                if (isCondition && strsLength > 0) {
                                    summaryExpression.controls.conditionText.textBox.value = strs[strsLength - 1];
                                    strsLength--;
                                }

                                if (func == "Count") {
                                    if (strsLength == 1) {
                                        summaryExpression.controls.dataBand.setKey(strs[0]);
                                    }
                                }
                                else {
                                    if (strsLength == 1) {
                                        if (this.jsObject.IsContains(summaryExpression.controls.dataBand.items, strs[0]))
                                            summaryExpression.controls.dataBand.setKey(strs[0]);
                                        else
                                            summaryExpression.controls.dataColumn.textBox.value = strs[0];
                                    }
                                    else if (strsLength == 2) {
                                        summaryExpression.controls.dataBand.setKey(strs[0]);
                                        summaryExpression.controls.dataColumn.textBox.value = strs[1];
                                    }
                                }
                            }
                            else {
                                if (strs.length == 1) {
                                    if (isCondition) {
                                        summaryExpression.controls.conditionText.textBox.value = strs[0];
                                    }
                                    else
                                        summaryExpression.controls.dataBand.setKey(strs[0]);
                                }
                                else if (strs.length == 2) {
                                    summaryExpression.controls.dataBand.setKey(strs[0]);
                                    summaryExpression.controls.conditionText.textBox.value = strs[1];
                                }
                            }
                        }
                    }
                }
            }
        }
        expressionTextArea.value = summaryExpression.getValue();
    }

    summaryExpression.getValue = function () {
        var text = "";
        if (summaryExpression.controls.summaryFunction.key) {
            var funcName = summaryExpression.controls.summaryFunction.key;

            if (summaryExpression.controls.pageRadioButton.isChecked) funcName = "c" + funcName;
            else if (summaryExpression.controls.columnRadioButton.isChecked) funcName = "col" + funcName;

            if (summaryExpression.controls.condition.isChecked) funcName += "If";
            if (summaryExpression.controls.runningTotal.isChecked) funcName += "Running";

            funcName += "(";
            if (summaryExpression.controls.summaryFunction.key != "Count") {
                var flag = false;
                if (summaryExpression.controls.dataBand.key.length > 0 && summaryExpression.controls.dataBand.key != "NotAssigned") {
                    funcName += summaryExpression.controls.dataBand.key;
                    flag = true;
                }
                if (summaryExpression.controls.dataColumn.textBox.value.length > 0) {
                    funcName += (flag ? "," : "") + summaryExpression.controls.dataColumn.textBox.value;
                    flag = true;
                }
                if (summaryExpression.controls.condition.isChecked) {
                    funcName += (flag ? "," : "") + summaryExpression.controls.conditionText.textBox.value;
                }
            }
            else {
                var flag = false;
                if (summaryExpression.controls.dataBand.key.length > 0 && summaryExpression.controls.dataBand.key != "NotAssigned") {
                    funcName += summaryExpression.controls.dataBand.key;
                    flag = true;
                }
                if (summaryExpression.controls.condition.isChecked) funcName += (flag ? "," : "") + summaryExpression.controls.conditionText.textBox.value;
            }
            funcName += ")";

            text = "{" + funcName + "}";
        }
        return text;
    }

    return summaryExpression;
}