﻿
StiMobileDesigner.prototype.MapIDControl = function (name, width) {
    var control = this.CreateHTMLTable();
    control.isEnabled = true;

    var button = this.SmallButton(null, null, "World ", "Maps.World.png", null, "Down", this.GetStyles("FormButton"), true);
    if (width) button.style.width = width + "px";
    button.style.height = "35px";
    button.innerTable.style.width = "100%";
    button.imageCell.style.width = "1px";
    button.imageCell.style.padding = "0 6px 0 6px";
    button.arrowCell.style.width = "1px";
    control.addCell(button);

    var allItems = {
        mainMenu: this.GetMapsCategoriesItems(),
        europe: this.GetEuropeMapsItems(),
        northAmerica: this.GetNorthAmericaMapsItems(),
        southAmerica: this.GetSouthAmericaMapsItems(),
        asia: this.GetAsiaMapsItems(),
        oceania: this.GetOceaniaMapsItems(),
        africa: this.GetAfricaMapsItems()
    };
    
    var mapsMenu = this.VerticalMenu(name + "MapsMenu", button, "Down", this.GetMapsCategoriesItems(), this.GetStyles("MenuMiddleItem"));
    this.InitializeSubMenu(name + "EuropeMapsMenu", allItems.europe, mapsMenu.items["Europe"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "NorthAmericaMapsMenu", allItems.northAmerica, mapsMenu.items["NorthAmerica"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "SouthAmericaMapsMenu", allItems.southAmerica, mapsMenu.items["SouthAmerica"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "AsiaMapsMenu", allItems.asia, mapsMenu.items["Asia"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "OceaniaMapsMenu", allItems.oceania, mapsMenu.items["Oceania"], mapsMenu, "MenuMiddleItem");
    this.InitializeSubMenu(name + "AfricaMapsMenu", allItems.africa, mapsMenu.items["Africa"], mapsMenu, "MenuMiddleItem");
       
    button.action = function () {
        mapsMenu.changeVisibleState(!mapsMenu.visible);
    }

    control.action = function () { }

    control.setKey = function (key) {
        this.key = key;
        for (var items in allItems) {
            if (allItems[items].length) {
                for (var i = 0; i < allItems[items].length; i++) {
                    if (key == allItems[items][i].key) {
                        button.image.src = this.jsObject.options.images[allItems[items][i].imageName];
                        button.caption.innerHTML = allItems[items][i].caption;
                    }
                }
            }
        }
    }

    control.setEnabled = function (state) {
        control.isEnabled = state;
        button.setEnabled(state);
    }

    mapsMenu.action = function (menuItem) {
        if (menuItem.haveSubMenu) return;
        mapsMenu.changeVisibleState(false);
        control.setKey(menuItem.key);
        control.action();
    }

    return control;
}