﻿
StiMobileDesigner.prototype.TextArea = function (name, width, height) {
    var textArea = document.createElement("textarea");
    if (width) {
        textArea.style.width = width + "px";
        textArea.style.minWidth = width + "px";
        textArea.style.maxWidth = width + "px";
    }
    if (height) {
        textArea.style.height = height + "px";
        textArea.style.minHeight = height + "px";
        textArea.style.maxHeight = height + "px";
    }

    textArea.jsObject = this;
    this.options.controls[name] = textArea;
    textArea.name = name;
    textArea.isEnabled = true;
    textArea.isSelected = false;
    textArea.isOver = false;
    textArea.className = this.options.isTouchDevice ? "stiDesignerTextArea_Touch" : "stiDesignerTextArea_Mouse";

    textArea.setEnabled = function (state) {
        this.isEnabled = state;
        this.disabled = !state;
        this.className = (state ? "stiDesignerTextArea" : "stiDesignerTextAreaDisabled") +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    textArea.onmouseover = function () {
        if (!this.jsObject.options.isTouchDevice) this.onmouseenter();
    }

    textArea.onmouseenter = function () {
        if (!this.isEnabled || this.jsObject.options.isTouchClick) return;
        this.isOver = true;
        if (!this.isSelected) this.className = "stiDesignerTextAreaOver" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    textArea.onfocus = function () {
        this.jsObject.options.controlsIsFocused = this;
    }

    textArea.onmouseleave = function () {
        if (!this.isEnabled) return;
        this.isOver = false;
        if (!this.isSelected) this.className = "stiDesignerTextArea" + (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    textArea.setSelected = function (state) {
        this.isSelected = state;
        this.className = (state ? "stiDesignerTextAreaOver" : (this.isEnabled ? (this.isOver ? "stiDesignerTextAreaOver" : "stiDesignerTextArea") : "stiDesignerTextAreaDisabled")) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
    }

    textArea.onblur = function () {
        this.jsObject.options.controlsIsFocused = false;
        this.action();
    }

    textArea.setReadOnly = function (state) {
        this.style.cursor = state ? "default" : "";
        this.readOnly = state;
        try {
            this.setAttribute("unselectable", state ? "on" : "off");
            this.setAttribute("onselectstart", state ? "return false" : "");
        }
        catch (e) { };
    }

    textArea.action = function () { };

    textArea.insertText = function (text) {
        if (this.selectionStart != null) {
            var cursorPosAfter = textArea.selectionStart + text.length;
            textArea.value = textArea.value.substring(0, textArea.selectionStart) + text + textArea.value.substring(textArea.selectionEnd);
            if (this.setSelectionRange) textArea.setSelectionRange(cursorPosAfter, cursorPosAfter);
        }
        else {
            textArea.value += text;
        }
    }

    textArea.addInsertButton = function () {
        var insertButton = this.jsObject.SmallButton(null, null, null, "GetItem.png", null, null, this.jsObject.GetStyles("FormButton"), true);
        this.insertButton = insertButton;
        insertButton.textArea = textArea;
        insertButton.style.position = "absolute";
        insertButton.style.marginLeft = (width - 40) + "px";
        insertButton.style.marginTop = "20px";
        if (this.parentElement) {
            this.parentElement.insertBefore(insertButton, this);
        }

        insertButton.action = function () {
            var dictionaryTree = this.jsObject.options.dictionaryTree;
            if (dictionaryTree && dictionaryTree.selectedItem) {
                textArea.insertText(dictionaryTree.selectedItem.getResultForEditForm());
            }
        }

        //Events
        this.onmouseup = function () { if (this.jsObject.options.itemInDrag) insertButton.action(); }
        this.ontouchend = function () { this.onmouseup(); }
    }

    textArea.removeInsertButton = function () {
        if (this.insertButton && this.parentElement) {
            this.parentElement.removeChild(this.insertButton);
            this.onmouseup = null;
            this.ontouchend = null;
            this.insertButton = null;
        }
    }

    textArea.resize = function (newWidth, newHeight) {
        if (newWidth) {
            textArea.style.width = newWidth + "px";
            textArea.style.minWidth = newWidth + "px";
            textArea.style.maxWidth = newWidth + "px";
        }
        if (newHeight) {
            textArea.style.height = newHeight + "px";
            textArea.style.minHeight = newHeight + "px";
            textArea.style.maxHeight = newHeight + "px";
        }
    }

    textArea.onkeypress = function () { this.onTextChange(); }
    textArea.onpaste = function () { this.onTextChange(); }
    textArea.oninput = function () { this.onTextChange(); }
    textArea.onTextChange = function () {}

    return textArea;
}