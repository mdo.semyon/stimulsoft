﻿
StiMobileDesigner.prototype.CollectionControl = function (name, checkBoxes, width) {
    var control = this.CreateHTMLTable();
    control.isEnabled = true;
    control.captionAll = this.loc.PropertyEnum.StiRestrictionsAll;
    control.captionNone = this.loc.PropertyEnum.StiRestrictionsNone;
    
    control.button = this.SmallButton(null, null, control.captionNone, null, null, "Down", this.GetStyles("FormButton"), true);
    if (width) control.button.style.width = width + "px";
    control.button.style.height = this.options.isTouchDevice ? "26px" : "21px";
    control.addCell(control.button);

    //Override Button
    var newCaption = document.createElement("div");
    newCaption.innerHTML = control.captionNone;
    newCaption.style.textOverflow = "ellipsis";
    newCaption.style.textAlign = "center";
    newCaption.style.overflow = "hidden";
    newCaption.style.padiingLeft = "5px";
    newCaption.style.width = (width - 21) + "px";
    newCaption.style.whiteSpace = "nowrap";
    control.button.arrow.style.marginBottom = "0px";
    control.button.caption.innerHTML = "";
    control.button.caption.appendChild(newCaption);
    control.button.caption = newCaption;

    control.menu = this.VerticalMenu(name + "Menu", control.button, "Down", null, this.GetStyles("MenuStandartItem"));
    control.button.action = function () {        
        control.menu.changeVisibleState(!control.menu.visible);
    }

    control.childControls = {};
    control.menu.isDinamic = true;
    control.menu.innerContent.style.width = null;
    control.menu.innerContent.style.overflowX = "visible";

    for (var i = 0; i < checkBoxes.length; i++) {
        var checkBox = this.CheckBox(null, checkBoxes[i][1]);
        checkBox.name = checkBoxes[i][0];
        checkBox.style.margin = "8px";
        control.childControls[checkBoxes[i][0]] = checkBox;
        control.menu.innerContent.appendChild(checkBox);

        checkBox.action = function () {
            var trueFlag = 0;
            var key = "";
            var captionText = "";

            for (var i = 0; i < checkBoxes.length; i++) {
                if (control.childControls[checkBoxes[i][0]].isChecked) {
                    trueFlag++;
                    key += " " + checkBoxes[i][0] + ",";
                    captionText += " " + checkBoxes[i][1] + ",";
                }
            }
            if (trueFlag == 0) {
                control.button.caption.innerHTML = control.captionNone;
            }
            else if (trueFlag == checkBoxes.length) {
                control.button.caption.innerHTML = control.captionAll;
            }
            else {
                control.button.caption.innerHTML = captionText != "" ? captionText.substring(1, captionText.length - 1) : captionText;
            }            
            control.key = key;
            control.action();
        }
    }

    control.setKey = function (key) {        
        this.key = key;
        var trueFlag = 0;
        var captionText = "";

        for (var i = 0; i < checkBoxes.length; i++) {
            var checked = key.indexOf(" " + checkBoxes[i][0] + ",") >= 0;
            control.childControls[checkBoxes[i][0]].setChecked(checked);
            if (checked) {
                trueFlag++;
                captionText += " " + checkBoxes[i][1] + ",";
            }
        }
        if (trueFlag == 0) {
            control.button.caption.innerHTML = control.captionNone;
        }
        else if (trueFlag == checkBoxes.length) {
            control.button.caption.innerHTML = control.captionAll;
        }
        else {
            control.button.caption.innerHTML = captionText != "" ? captionText.substring(1, captionText.length - 1) : captionText;
        }
    }

    control.setEnabled = function (state) {
        control.isEnabled = state;
        control.button.setEnabled(state);
    }

    control.action = function () { }

    return control;
}

StiMobileDesigner.prototype.StylePlacementCollectionControl = function (name, checkBoxes, width) {
    var control = this.CollectionControl(name, checkBoxes, width);

    for (var i = 0; i < checkBoxes.length; i++) {
        var checkBox = control.childControls[checkBoxes[i][0]];

        if (checkBox.name == "DataEvenStyle" || checkBox.name == "DataOddStyle") {
            checkBox.style.marginLeft = "15px";
        }

        checkBox.action = function () {
            var trueFlag = 0;
            var key = "";
            var captionText = "";

            if (this.name == "DataEvenStyle" || this.name == "DataOddStyle") {
                for (var i = 0; i < checkBoxes.length; i++) {
                    control.childControls[checkBoxes[i][0]].setChecked(this.name == checkBoxes[i][0]);                    
                }
                key += " " + this.name + ",";
                captionText += " " + this.captionCell.innerHTML + ",";
                trueFlag = 1;
            }
            else {            
                control.childControls["DataEvenStyle"].setChecked(false);
                control.childControls["DataOddStyle"].setChecked(false);
                
                for (var i = 0; i < checkBoxes.length; i++) {
                    if (control.childControls[checkBoxes[i][0]].isChecked) {
                        trueFlag++;
                        key += " " + checkBoxes[i][0] + ",";
                        captionText += " " + checkBoxes[i][1] + ",";
                    }
                }

                if (trueFlag == checkBoxes.length - 2) {
                    control.button.caption.innerHTML = captionText != "" ? captionText.substring(1, captionText.length - 1) : captionText;
                    control.key = " AllExeptStyles,";
                    control.action();
                    return;
                }
            }

            if (trueFlag == 0) {
                control.button.caption.innerHTML = control.captionNone;
            }
            else {
                control.button.caption.innerHTML = captionText != "" ? captionText.substring(1, captionText.length - 1) : captionText;
            }

            control.key = key;
            control.action();
        }
    }

    control.setKey = function (key) {
        this.key = key;
        var captionText = "";

        if (key == " AllExeptStyles,") {
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i][0] != "DataEvenStyle" && checkBoxes[i][0] != "DataOddStyle") {
                    captionText += " " + checkBoxes[i][1] + ",";
                    control.childControls[checkBoxes[i][0]].setChecked(true);
                }
                else {
                    control.childControls[checkBoxes[i][0]].setChecked(false);
                }
            }
            control.button.caption.innerHTML = captionText != "" ? captionText.substring(1, captionText.length - 1) : captionText;
        }
        else {
            var trueFlag = 0;
            for (var i = 0; i < checkBoxes.length; i++) {
                var checked = key.indexOf(" " + checkBoxes[i][0] + ",") >= 0;
                control.childControls[checkBoxes[i][0]].setChecked(checked);
                if (checked) {
                    trueFlag++;
                    captionText += " " + checkBoxes[i][1] + ",";
                }
            }
            if (trueFlag == 0) {
                control.button.caption.innerHTML = control.captionNone;
            }
            else {
                control.button.caption.innerHTML = captionText != "" ? captionText.substring(1, captionText.length - 1) : captionText;
            }
        }
    }

    return control;
}

