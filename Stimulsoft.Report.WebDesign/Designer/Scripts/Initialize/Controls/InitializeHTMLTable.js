﻿
StiMobileDesigner.prototype.CreateHTMLTable = function (rowsCount, cellsCount) {
    var table = document.createElement("table");
    table.jsObject = this;
    this.ClearStyles(table);
    table.cellPadding = 0;
    table.cellSpacing = 0;
    table.tr = [];
    table.tr[0] = document.createElement("tr");
    this.ClearStyles(table.tr[0]);
    table.appendChild(table.tr[0]);

    table.addCell = function (control) {
        var cell = document.createElement("td");
        this.jsObject.ClearStyles(cell);
        this.tr[0].appendChild(cell);
        if (control) cell.appendChild(control);

        return cell;
    }

    table.insetCell = function (index, control) {
        var cell = this.tr[0].insertCell(index);
        this.jsObject.ClearStyles(cell);
        if (control) cell.appendChild(control);

        return cell;
    }

    table.addTextCellInNextRow = function (text) {
        var rowCount = this.tr.length;
        this.tr[rowCount] = document.createElement("tr");
        this.jsObject.ClearStyles(this.tr[rowCount]);
        this.appendChild(this.tr[rowCount]);
        var cell = document.createElement("td");
        this.jsObject.ClearStyles(cell);
        this.tr[rowCount].appendChild(cell);
        cell.innerHTML = text;

        return cell;
    }

    table.addCellInNextRow = function (control) {
        var rowCount = this.tr.length;
        this.tr[rowCount] = document.createElement("tr");
        this.jsObject.ClearStyles(this.tr[rowCount]);
        this.appendChild(this.tr[rowCount]);
        var cell = document.createElement("td");
        this.jsObject.ClearStyles(cell);
        this.tr[rowCount].appendChild(cell);
        if (control) cell.appendChild(control);

        return cell;
    }

    table.addCellInLastRow = function (control) {
        var rowCount = this.tr.length;
        var cell = document.createElement("td");
        this.jsObject.ClearStyles(cell);
        this.tr[rowCount - 1].appendChild(cell);
        if (control) cell.appendChild(control);

        return cell;
    }

    table.addTextCellInLastRow = function (text) {
        var rowCount = this.tr.length;
        var cell = document.createElement("td");
        this.jsObject.ClearStyles(cell);
        this.tr[rowCount - 1].appendChild(cell);
        cell.innerHTML = text;

        return cell;
    }

    table.addCellInRow = function (rowNumber, control) {
        var cell = document.createElement("td");
        this.jsObject.ClearStyles(cell);
        this.tr[rowNumber].appendChild(cell);
        if (control) cell.appendChild(control);

        return cell;
    }

    table.addTextCell = function (text) {
        var cell = document.createElement("td");
        this.jsObject.ClearStyles(cell);
        this.tr[0].appendChild(cell);
        cell.innerHTML = text;

        return cell;
    }

    table.addRow = function () {
        var rowCount = this.tr.length;
        this.tr[rowCount] = document.createElement("tr");
        this.jsObject.ClearStyles(this.tr[rowCount]);
        this.appendChild(this.tr[rowCount]);

        return this.tr[rowCount];
    }

    return table;
}