﻿
StiMobileDesigner.prototype.ResultsControl = function (name, widthContainer, heightContainer) {
    var resultControl = document.createElement("div");
    resultControl.jsObject = this;
    resultControl.toolBar = this.CreateHTMLTable();
    resultControl.toolBar.style.margin = "4px";
    resultControl.appendChild(resultControl.toolBar);
    resultControl.currentDataSourceName = null;
    resultControl.noResultText = this.loc.PropertyEnum.DialogResultNo;

    //Add Result
    resultControl.addResultButton = this.DinamicStandartSmallButton(null, null, this.loc.FormBand.AddResult.replace("&", ""), "AddResult.png", null, null);
    resultControl.toolBar.addCell(resultControl.addResultButton);
    resultControl.addResultButton.action = function () {
        this.resultContainer.addResult({ "column": resultControl.noResultText, "aggrFunction": "No", "name" : "" });
    }

    //Remove Sort
    resultControl.removeResultButton = this.DinamicStandartSmallButton(null, null, this.loc.FormBand.RemoveResult.replace("&", ""), "Remove.png", null, null);
    resultControl.toolBar.addCell(resultControl.removeResultButton);
    resultControl.removeResultButton.action = function () { this.resultContainer.removeResult(); }

    //Container
    resultControl.resultContainer = this.ResultContainer(resultControl);
    if (widthContainer) resultControl.resultContainer.style.width = widthContainer + "px";
    if (heightContainer) resultControl.resultContainer.style.height = heightContainer + "px";
    resultControl.appendChild(resultControl.resultContainer);
    resultControl.addResultButton.resultContainer = resultControl.resultContainer;
    resultControl.removeResultButton.resultContainer = resultControl.resultContainer;

    resultControl.fill = function (results) {
        this.resultContainer.clear();
        if (!results) return;
        for (var i = 0; i < results.length; i++) this.resultContainer.addResult(results[i]);
    }

    resultControl.getValue = function () {
        var result = [];
        for (var i = 0; i < this.resultContainer.items.length; i++) {
            result.push({
                "column": this.resultContainer.items[i].column.textBox.value == resultControl.noResultText ? "" : this.resultContainer.items[i].column.textBox.value,
                "aggrFunction": this.resultContainer.items[i].aggrFunction.key == "No" ? "" : this.resultContainer.items[i].aggrFunction.key,
                "name": this.resultContainer.items[i].name.value
            });
        }
        return result;
    }

    return resultControl;
}

StiMobileDesigner.prototype.ResultContainer = function (resultControl) {
    var resultContainer = document.createElement("div");
    resultControl.resultContainer = resultContainer;
    resultContainer.items = [];
    resultContainer.jsObject = this;
    resultContainer.className = "stiDesignerSortContainer";
    resultContainer.resultControl = resultControl;

    resultContainer.addResult = function (resultObject) {
        var resultItem = this.jsObject.ResultItem(this);
        this.items.push(resultItem);
        this.appendChild(resultItem);
        resultItem.column.textBox.value = resultObject.column == "" ? resultControl.noResultText : resultObject.column;
        resultItem.aggrFunction.setKey(resultObject.aggrFunction == "" ? "No" : resultObject.aggrFunction);
        resultItem.name.value = resultObject.name;
        resultItem.setSelected();
        this.resultControl.removeResultButton.setEnabled(true);
    }

    resultContainer.removeResult = function () {
        for (var i = 0; i < this.items.length; i++)
            if (this.items[i].isSelected) {
                this.removeChild(this.items[i]);
                this.items.splice(i, 1);
            }
        if (this.items.length > 0) this.items[0].setSelected();
        this.resultControl.removeResultButton.setEnabled(this.items.length > 0);
    }

    resultContainer.clear = function () {
        this.items = [];
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        this.resultControl.removeResultButton.setEnabled(false);
    }

    return resultContainer;
}


StiMobileDesigner.prototype.ResultItem = function (resultContainer) {
    var resultItem = document.createElement("div");
    resultItem.jsObject = this;
    resultItem.resultContainer = resultContainer;
    resultItem.key = this.newGuid().replace(/-/g, '');
    resultItem.isSelected = false;
    resultItem.className = "stiDesignerSortPanel";
    resultItem.innerTable = this.CreateHTMLTable();
    resultItem.appendChild(resultItem.innerTable);

    //Captions
    resultItem.innerTable.addTextCell(this.loc.PropertyMain.Column).style.padding = "5px 5px 0 5px";
    resultItem.innerTable.addTextCell(this.loc.PropertyMain.AggregateFunction).style.padding = "5px 5px 0 5px";
    resultItem.innerTable.addTextCell(this.loc.PropertyMain.Name).style.padding = "5px 5px 0 5px";

    resultItem.updateControls = function () {
        var column = resultItem.column.textBox.value != resultContainer.resultControl.noResultText ? resultItem.column.textBox.value : "";
        resultItem.name.value = column;
        if (resultItem.aggrFunction.key != "No") {
            if (column) resultItem.name.value += ".";
            resultItem.name.value += resultItem.aggrFunction.key;
        }
    }

    //Column
    resultItem.column = this.DinamicTextBoxWithEditButton(null, 180);
    resultItem.innerTable.addCellInNextRow(resultItem.column).style.padding = "5px";
    resultItem.column.button.action = function () {
        var currentDataSourceName = (resultContainer.resultControl.currentDataSourceName != null)
                ? resultContainer.resultControl.currentDataSourceName : this.jsObject.options.selectedObject.properties["dataSource"];
        this.key = currentDataSourceName + "." + this.textBox.value;
        var this_ = this;

        this.jsObject.InitializeDataColumnForm(function (dataColumnForm) {
            dataColumnForm.dataTree.build("Column", currentDataSourceName);
            dataColumnForm.needBuildTree = false;
            dataColumnForm.parentButton = this_;
            dataColumnForm.changeVisibleState(true);
            
            dataColumnForm.action = function () {
                this.changeVisibleState(false);
                this.parentButton.textBox.value = this.dataTree.key != ""
                    ? this.dataTree.key.substring(this.dataTree.key.indexOf(".") + 1, this.dataTree.key.length)
                    : resultContainer.resultControl.noResultText;
                resultItem.updateControls();
            }
        });
    }

    //AggrFunction    
    resultItem.aggrFunction = this.DinamicDropDownList(null, 140, null, this.GetResultFunctionItems(), true, false);
    resultItem.innerTable.addCellInLastRow(resultItem.aggrFunction).style.padding = "5px";
    resultItem.aggrFunction.action = function () { resultItem.updateControls(); }

    //Name
    resultItem.name = this.TextBox(null, 140);
    resultItem.innerTable.addCellInLastRow(resultItem.name).style.padding = "5px";

    resultItem.setSelected = function () {
        for (var i = 0; i < this.resultContainer.items.length; i++) {
            this.resultContainer.items[i].className = "stiDesignerSortPanel";
            this.resultContainer.items[i].isSelected = false;
        }
        this.isSelected = true;
        this.className = "stiDesignerSortPanelSelected";
    }

    resultItem.onclick = function () {
        if (this.isTouchEndFlag || this.jsObject.options.isTouchClick) return;
        this.action();
    }

    resultItem.ontouchend = function () {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        if (this.jsObject.options.fingerIsMoved) return;
        this.action();
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    resultItem.action = function () {
        this.setSelected();
    }

    return resultItem;
}