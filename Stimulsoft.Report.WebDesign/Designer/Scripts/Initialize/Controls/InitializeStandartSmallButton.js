﻿
StiMobileDesigner.prototype.StandartSmallButton = function (name, groupName, caption, imageName, toolTip, arrow, allwaysEnabled) {
    return this.SmallButton(name, groupName, caption, imageName, toolTip, arrow, this.GetStyles("StandartSmallButton"), allwaysEnabled);
}

StiMobileDesigner.prototype.DinamicStandartSmallButton = function (name, groupName, caption, imageName, toolTip, arrow, allwaysEnabled) {
    return this.DinamicSmallButton(name, groupName, caption, imageName, toolTip, arrow, this.GetStyles("StandartSmallButton"), allwaysEnabled);
}