﻿
StiMobileDesigner.prototype.InitializeSaveAsPanel = function () {
    var saveAsPanel = document.createElement("div");
    saveAsPanel.jsObject = this;
    this.options.saveAsPanel = saveAsPanel;
    this.options.mainPanel.appendChild(saveAsPanel);
    saveAsPanel.style.display = "none";
    saveAsPanel.className = "stiDesignerNewReportPanel";
    saveAsPanel.style.overflow = "auto";    
    var jsObject = this;

    saveAsPanel.header = this.FileMenuPanelHeader(this.loc.MainMenu.menuFileSaveAs.replace("...", ""));
    saveAsPanel.appendChild(saveAsPanel.header);

    var mainTable = this.CreateHTMLTable();
    saveAsPanel.appendChild(mainTable);
    mainTable.style.margin = "10px 30px 0px 30px";
    mainTable.style.height = "calc(100% - 130px)";

    var mainButtonsTable = this.CreateHTMLTable();
    mainTable.addCell(mainButtonsTable).className = "wizardFormStepsPanel";
    var rightCell = mainTable.addCell();
    rightCell.className = "wizardFormStepsPanel";
    rightCell.style.border = "0";
    saveAsPanel.rightCell = rightCell;

    //Online
    var onlinePanel = this.InitializeOnlineSaveAsReportPanel();
    jsObject.options.mainPanel.appendChild(onlinePanel);

    //Browse
    var browsePanel = this.InitializeBrowseSaveAsReportPanel();
    jsObject.options.mainPanel.appendChild(browsePanel);

    saveAsPanel.getFileName = function (withExt) {
        return jsObject.GetReportFileName(withExt);
    }

    //Main Buttons
    var mainButtons = [];
    mainButtons.push(["onlineItemsSaveAs", this.getCloudName(), "Open.Online.png"]);
    mainButtons.push(["browseFilesSaveAs", this.loc.ReportOpen.Browse, "Open.OpenFiles.png"]);

    for (var i = 0; i < mainButtons.length; i++) {
        var button = this.RecentPanelButton(mainButtons[i][0], "SaveAsPanelMainButtons", mainButtons[i][1], mainButtons[i][2]);
        button.style.marginBottom = "3px";
        mainButtonsTable.addCellInNextRow(button);

        button.action = function (ignoreSelect) {
            if (this.isSelected && !ignoreSelect) return;
            this.setSelected(true);
            saveAsPanel.mode = this.name;
            onlinePanel.style.display = "none";
            browsePanel.style.display = "none";
            
            switch (this.name) {
                case "onlineItemsSaveAs": {
                    onlinePanel.show(saveAsPanel.getFileName(false), saveAsPanel.nextFunc);
                    break;
                }
                case "browseFilesSaveAs": {
                    browsePanel.show(saveAsPanel.getFileName(true), saveAsPanel.nextFunc);
                    break;
                }
            }
        }
    }   
        
    saveAsPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        jsObject.options.buttons.onlineItemsSaveAs.setSelected(false);
        jsObject.options.buttons.browseFilesSaveAs.setSelected(false);        
        if (state) {
            jsObject.options.buttons.onlineItemsSaveAs.action();
            saveAsPanel.header.innerHTML = jsObject.loc.MainMenu.menuFileSaveAs.replace("...", "");
            saveAsPanel.nextFunc = null;
        }
        else {
            onlinePanel.style.display = "none";
            browsePanel.style.display = "none";
        }
    }

    return saveAsPanel;
}

StiMobileDesigner.prototype.InitializeOnlineSaveAsReportPanel = function () {
    var form = this.BaseForm("onlineSaveAsReport", this.loc.MainMenu.menuFileSaveAs.replace("...", ""), 1, this.HelpLinks["onlineSaveReport"]);
    var jsObject = this;
        
    //New Folder
    var newFolderButton = jsObject.StandartSmallButton(null, null, this.loc.Cloud.FolderWindowTitleNew, "Folder.png");
    newFolderButton.style.margin = "10px 10px 10px 7px";
    newFolderButton.style.height = "30px";
    newFolderButton.style.display = "inline-block";
    newFolderButton.imageCell.style.padding = "0 7px 0 9px";
    newFolderButton.caption.style.padding = "0px 15px 0 0px";
    form.container.appendChild(newFolderButton);
    form.newFolderButton = newFolderButton;
    var newFolderSep = this.FormSeparator();
    form.container.appendChild(newFolderSep);

    //Online
    var onlineTree = jsObject.CloudTree();
    form.onlineTree = onlineTree;
    onlineTree.style.padding = "10px 0 10px 0";
    onlineTree.style.width = "100%";
    onlineTree.style.minWidth = "550px";
    onlineTree.style.height = "400px";
    onlineTree.progress = jsObject.AddProgressToControl(form.container);
    form.container.appendChild(onlineTree);
        
    //Cloud Demo text
    var text = "You can save your report in <a style='text-decoration: none;' href='https://cloud.stimulsoft.com/' target='_blank'>Stimulsoft Cloud.</a><br>" +
        "Please login using your Stimulsoft account credentials or register a new account";
    var demoPanel = jsObject.CloudDemoPanel(text);
    form.demoPanel = demoPanel;
    form.container.appendChild(demoPanel);

    newFolderButton.action = function () {
        this.jsObject.InitializeNewFolderForm(function (newFolderForm) {
            newFolderForm.show(function () {
                if (newFolderForm.nameTextBox.value) {
                    var newFolderKey = newFolderForm.jsObject.generateKey();

                    var param = {};
                    param.AllowSignalsReturn = true;
                    param.Items = [{
                        Ident: "FolderItem",
                        Name: newFolderForm.nameTextBox.value,
                        Description: "",
                        Type: "Common",
                        Key: newFolderKey
                    }];

                    if (onlineTree.rootItem && onlineTree.rootItem.itemObject.Key != "root") {
                        param.Items[0].FolderKey = onlineTree.rootItem.itemObject.Key;
                    }

                    onlineTree.progress.show();
                    newFolderForm.changeVisibleState(false);

                    newFolderForm.jsObject.SendCloudCommand("ItemSave", param,
                        function (data) {
                            onlineTree.progress.hide();
                            if (data.ResultItems && data.ResultItems.length > 0) {
                                onlineTree.build(onlineTree.rootItem.itemObject, onlineTree.returnItems[onlineTree.rootItem.itemObject.Key], newFolderKey);
                            }
                        },
                        function (data, ignoreErrorMessage) {
                            if (!ignoreErrorMessage) {
                                var errorMessageForm = newFolderForm.jsObject.options.forms.errorMessageForm || newFolderForm.jsObject.InitializeErrorMessageForm();
                                errorMessageForm.show(newFolderForm.jsObject.formatResultMsg(data));
                            }
                        });
                }
                else {
                    onlineTree.progress.hide();
                    var errorMessageForm = newFolderForm.jsObject.options.forms.errorMessageForm || newFolderForm.jsObject.InitializeErrorMessageForm();
                    errorMessageForm.show(newFolderForm.jsObject.loc.Errors.FieldRequire.replace("{0}", newFolderForm.jsObject.loc.PropertyMain.Name));
                }
            })
        });
    }

    //Name Control
    var nameTable = this.CreateHTMLTable();
    nameTable.style.marginTop = "15px";
    nameTable.style.width = "100%";

    var nameLabel = nameTable.addTextCell(this.loc.PropertyMain.Name + ":");
    nameLabel.className = "stiDesignerCaptionControls";
    nameLabel.style.paddingLeft = "15px";

    var nameControl = jsObject.TextBox(null, 450);
    form.nameControl = nameControl;
    nameControl.style.marginRight = "15px";
    var cellControl = nameTable.addCell(nameControl);
    cellControl.style.textAlign = "right";

    form.buttonOk.caption.innerHTML = this.loc.A_WebViewer.SaveReport;
    form.buttonOk.style.margin = "15px 15px 15px 0px";
    form.insertBefore(nameTable, form.buttonsPanel);
    form.buttonCancel.style.margin = "15px 15px 15px 0px";

    form.buttonCancel.action = function () {
        var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
        fileMenu.changeVisibleState(false);
    }

    form.cancelAction = function () {
        form.buttonCancel.action();
    }

    onlineTree.action = function (item) {
        if (item.itemObject.Ident == "ReportTemplateItem") {
            nameControl.value = item.itemObject.Name;
        }
    }

    onlineTree.ondblClickAction = function (item) {
        if (item.itemObject.Ident == "ReportTemplateItem") {
            form.action();
        }
    }

    onlineTree.onbuildcomplete = function (item) {
        form.buttonOk.setEnabled(true);
        newFolderButton.setEnabled(true);
        nameControl.setEnabled(true);
        var newFreeName = onlineTree.getNewFreeName(nameControl.value);
        nameControl.value = newFreeName;
    }

    onlineTree.getNewFreeName = function (name) {
        var item = onlineTree.checkExistItem(name, "ReportTemplateItem");
        if (!item) return name;
        
        var i = 2;
        while (onlineTree.checkExistItem(name + i, "ReportTemplateItem")) {
            i++;
        }

        return name + i;
    }

    onlineTree.checkExistItem = function (itemName, itemIdent) {
        for (var i = 0; i < this.innerContainer.childNodes.length; i++) {
            var item = this.innerContainer.childNodes[i];
            if (item.itemObject && item.itemObject.Ident == itemIdent && item.itemObject.Name == itemName) {
                return item.itemObject;
            }
        }
        return false;
    }
    
    form.setToLoginMode = function () {
        onlineTree.style.display = "none";
        demoPanel.style.display = "";
        form.buttonOk.style.display = "none";
        newFolderButton.style.display = "none";
        nameTable.style.display = "none";
        newFolderSep.style.display = "none";
        form.buttonCancel.style.display = "none";
        form.buttonsSeparator.style.display = "none";
    }

    form.setToTreeMode = function () {
        onlineTree.style.display = "";
        demoPanel.style.display = "none";
        form.buttonOk.style.display = "";
        newFolderButton.style.display = "inline-block";
        nameTable.style.display = "";
        newFolderSep.style.display = "";
        form.buttonCancel.style.display = "";
        form.buttonsSeparator.style.display = "";
        nameControl.setEnabled(false);
        onlineTree.build();
    }

    form.show = function (fileName, nextFunc) {
        form.style.display = "";
        form.visible = true;
        form.style.left = (jsObject.FindPosX(jsObject.options.saveAsPanel.rightCell, "stiDesignerMainPanel") + 10) + "px";
        form.style.top = jsObject.FindPosY(jsObject.options.saveAsPanel.rightCell, "stiDesignerMainPanel") + "px";
        nameControl.value = fileName;
        nameControl.focus();
        form.buttonOk.setEnabled(false);
        newFolderButton.setEnabled(false);
        form.nextFunc = nextFunc;

        if (this.jsObject.options.cloudParameters.sessionKey) {
            form.setToTreeMode();
        }
        else {
            form.setToLoginMode();
        }
    }

    form.action = function () {
        var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
        var newItem = onlineTree.checkExistItem(nameControl.value, "ReportTemplateItem");
        if (newItem) {
            //Replase existing online item
            var messageReplaceForm = jsObject.MessageFormForReplaceItem(nameControl.value);
            messageReplaceForm.changeVisibleState(true);
            messageReplaceForm.action = function (state) {
                if (state) {
                    form.changeVisibleState(false);
                    fileMenu.changeVisibleState(false);
                    setTimeout(function () {
                        jsObject.options.cloudParameters.reportTemplateItemKey = newItem.Key;
                        jsObject.options.cloudParameters.reportName = nameControl.value;
                        jsObject.SetWindowTitle(nameControl.value + " - " + jsObject.loc.FormDesigner.title);
                        jsObject.SendCommandItemResourceSave(newItem.Key);
                        if (form.nextFunc) form.nextFunc();
                    }, 200);
                }
            }
        }
        else {
            form.changeVisibleState(false);
            fileMenu.changeVisibleState(false);
            setTimeout(function () {
                //Create new online item
                var folderKey = onlineTree.rootItem && onlineTree.rootItem.itemObject.Key != "root" ? onlineTree.rootItem.itemObject.Key : null;
                jsObject.AddNewReportItemToCloud(nameControl.value, folderKey);
                if (form.nextFunc) form.nextFunc();
            }, 200);
        }
    }

    nameControl.actionOnKeyEnter = function () {
        form.action();
    }

    return form;
}

StiMobileDesigner.prototype.InitializeBrowseSaveAsReportPanel = function () {
    var form = this.BaseForm("browseSaveAsReport", this.loc.MainMenu.menuFileSaveAs.replace("...", ""), 1);
    var jsObject = this;

    var saveTypeTable = this.CreateHTMLTable();
    var textCell = saveTypeTable.addTextCell(this.loc.Cloud.SaveAsType + ":");
    textCell.className = "stiDesignerCaptionControls";
    textCell.style.padding = "0 8px 0 22px";
    saveTypeTable.style.marginBottom = "3px";

    var saveType = this.DropDownList("saveAsType", 80, null, this.GetSaveTypeItems(), true);
    saveTypeTable.addCell(saveType);

    var footerTable = this.CreateHTMLTable();
    footerTable.style.width = "100%";
    form.removeChild(form.buttonsPanel);
    form.appendChild(footerTable);
    footerTable.addCell(saveTypeTable).style.width = "1px";
    footerTable.addCell();
    footerTable.addCell(form.buttonOk).style.width = "1px";
    footerTable.addCell(form.buttonCancel).style.width = "1px";

    form.buttonOk.caption.innerHTML = this.loc.A_WebViewer.SaveReport;
    form.buttonOk.style.margin = "15px 15px 15px 0px";
    form.buttonCancel.style.margin = "15px 15px 15px 0px";

    form.buttonCancel.action = function () {
        var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
        fileMenu.changeVisibleState(false);
    }

    form.cancelAction = function () {
        form.buttonCancel.action();
    }

    //Name Control
    var nameTable = this.CreateHTMLTable();
    form.container.appendChild(nameTable);
    var nameLabel = nameTable.addTextCell(this.loc.PropertyMain.Name + ":");
    nameLabel.className = "stiDesignerCaptionControls";
    var nameControl = this.TextBox(null, 450);
    nameTable.addCell(nameControl);
    nameTable.style.margin = "15px";
    
    form.show = function (fileName, nextFunc) {
        form.style.display = "";
        form.style.left = (jsObject.FindPosX(jsObject.options.saveAsPanel.rightCell, "stiDesignerMainPanel") + 10) + "px";
        form.style.top = jsObject.FindPosY(jsObject.options.saveAsPanel.rightCell, "stiDesignerMainPanel") + "px";
        nameControl.value = fileName;
        nameControl.focus();
        form.nextFunc = nextFunc;
        saveTypeTable.style.display = jsObject.options.cloudMode && !jsObject.options.report.encryptedPassword ? "" : "none";
        saveType.setKey(jsObject.options.report.isJsonReport ? "json" : "xml"); 
    }

    form.action = function () {
        var fileMenu = jsObject.options.menus.fileMenu || jsObject.InitializeFileMenu();
        fileMenu.changeVisibleState(false);

        setTimeout(function () {
            jsObject.options.report.properties.reportFile = nameControl.value;
            jsObject.options.cloudParameters.reportTemplateItemKey = null;

            //Update designer title
            var reportFile = jsObject.options.report.properties.reportFile;
            if (reportFile != null) reportFile = reportFile.substring(reportFile.lastIndexOf("/")).substring(reportFile.lastIndexOf("\\"));
            var reportName = reportFile || Base64.decode(jsObject.options.report.properties.reportName.replace("Base64Code;", ""));
            jsObject.SetWindowTitle(reportName ? reportName + " - " + jsObject.loc.FormDesigner.title : jsObject.loc.FormDesigner.title);

            jsObject.SendCommandSaveAsReport(saveType.key);
            if (form.nextFunc) {
                form.nextFunc();
                form.nextFunc = null;
            }
        }, 200);
    }

    nameControl.actionOnKeyEnter = function () {
        form.action();
    }

    return form;
}