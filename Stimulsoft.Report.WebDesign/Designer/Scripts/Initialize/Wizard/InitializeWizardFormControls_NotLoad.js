﻿
StiMobileDesigner.prototype.WizardFormCheckBox = function (name, caption, key) {
    var checkBox = this.DinamicCheckBox(name, caption);
    checkBox.style.margin = "7px 10px 7px 10px";
    checkBox.key = key;
    checkBox.allwaysEnabled = true;
        
    return checkBox;
}

StiMobileDesigner.prototype.WizardFormColumnsOrderItem = function (name, caption) {
    var button = this.DinamicStandartSmallButton(name, null, caption, null, null, null);    

    button.caption.style.fontSize = "12px";
    button.style.margin = "1px 1px 0 1px";
    button.caption.style.paddingLeft = "15px";
    button.key = caption;
    button.allwaysEnabled = true;

    return button;
}

StiMobileDesigner.prototype.WizardFormTabbedPane = function (name, tabNames) {
    var tabs = [];
    for (var i = 0; i < tabNames.length; i++) {
        tabs.push({ "name": tabNames[i], "caption": tabNames[i] });
    }
    var tabbedPane = this.TabbedPane(name, tabs, this.GetStyles("StandartTab"));

    return tabbedPane;
}

StiMobileDesigner.prototype.WizardFormSeparator = function (caption) {
    var separator = document.createElement("div");
    separator.className = "wizardFormSeparator";
    separator.innerHTML = caption;

    return separator;
}

StiMobileDesigner.prototype.WizardFormColumnsHeader = function (wizardForm, dataSource) {
    var header = document.createElement("div");
    header.className = "wizardFormSeparator";
    header.innerTable = this.CreateHTMLTable();
    header.innerTable.style.width = "100%";
    header.appendChild(header.innerTable);

    header.caption = header.innerTable.addCell();
    header.caption.style.width = "100%";
    header.caption.innerHTML = dataSource.name;

    header.relationText = header.innerTable.addCell();
    header.relationText.innerHTML = this.loc.PropertyMain.DataRelation + ": ";

    var relationsItems = dataSource.relations ? this.GetRelationsItemsFromDataSource(dataSource) : [];
    header.relations = this.DinamicDropDownList("WizardFormColumnsHeader" + this.newGuid().replace(/-/g, ''), 200, null, relationsItems, true, false, 18);
    header.relations.wizardForm = wizardForm;
    header.relations.dataSourceName = dataSource.name;
    header.relations.action = function () {
        this.wizardForm.workPanels.columns.columnsHeaderKeys[this.dataSourceName] = this.key;
        this.wizardForm.workPanels.columns.update();
    };

    header.relations.style.marginLeft = "5px";
    header.innerTable.addCell(header.relations);
    header.relationText.style.display = relationsItems.length > 1 ? "" : "none";
    header.relations.style.display = relationsItems.length > 1 ? "" : "none";

    return header;
}

/*
StiMobileDesigner.prototype.WizardFormTotalControl = function (caption, wizardForm, dataSourceName) {
    var total = document.createElement("div");
    total.innerTable = this.CreateHTMLTable();
    total.appendChild(total.innerTable);

    total.caption = document.createElement("div");
    total.caption.className = "wizardFormTotalsColumnName";
    total.innerTable.addCell(total.caption);
    total.caption.innerHTML = caption;

    total.functionControl = this.DinamicDropDownList("WizardFormColumnsHeader" + this.newGuid().replace(/-/g, ''), 100, null, null, true, false, 18);
    total.functionControl.wizardForm = wizardForm;
    total.functionControl.dataSourceName = dataSourceName;
    total.functionControl.style.margin = "0px 6px 0px 6px";
    total.innerTable.addCell(total.functionControl);

    return total;
}

StiMobileDesigner.prototype.WizardFormTotalsHeader = function () {
    var header = this.CreateHTMLTable();

    header.column = header.addCell();
    header.column.className = "wizardFormTotalsHeaderColumn";
    header.column.innerHTML = this.loc.PropertyMain.Column;

    header.functionName = header.addCell();
    header.functionName.className = "wizardFormTotalsHeaderFunction";
    header.functionName.innerHTML = this.loc.PropertyMain.Function;

    return header;
}*/