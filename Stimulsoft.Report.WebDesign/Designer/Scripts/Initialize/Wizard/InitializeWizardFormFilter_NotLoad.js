﻿
StiMobileDesigner.prototype.InitializeWizardFormFilter = function (wizardForm) {
    var filterPanel = this.WizardFormWorkPanel(wizardForm, "filter");
    filterPanel.style.overflow = "hidden";
    filterPanel.helpTextStandart = "<b>" + this.loc.PropertyMain.Filters + "</b><br>" + this.loc.Wizards.infoFilters;
    this.InitializeWizardFormStepItem(wizardForm, filterPanel.name, this.loc.PropertyMain.Filters);
    filterPanel.wizardForm = wizardForm;
    filterPanel.selectedColumnsItem = null;

    filterPanel.onShow = function () {
        this.update();
    }

    filterPanel.onHide = function () {
        if (this.jsObject.GetCountObjects(this.wizardForm.dataSources) != 0) this.apply();
    }

    filterPanel.update = function () {
        this.clear();
        var dataSourcesTabs = [];
        for (var dataSourceName in this.wizardForm.dataSources) dataSourcesTabs.push(dataSourceName);
        this.tabbedPane = this.jsObject.WizardFormTabbedPane("WizardFormFilterTabbedPane", dataSourcesTabs);
        this.appendChild(this.tabbedPane);
        for (var i = 0; i < dataSourcesTabs.length; i++) {
            var columnsItems = this.wizardForm.workPanels.columns.getColumnsItems(dataSourcesTabs[i]);
            var filterControl = this.jsObject.FilterControl("WizardFormFilterControl" + this.jsObject.newGuid().replace(/-/g, ''), columnsItems, null, this.jsObject.options.isTouchDevice ? 274 : 284);
                        
            var currentTabPanel = this.tabbedPane.tabsPanels[dataSourcesTabs[i]];
            //currentTabPanel.style.padding = "1px";
            currentTabPanel.appendChild(this.jsObject.FormSeparator());
            currentTabPanel.appendChild(filterControl);
            currentTabPanel.filterControl = filterControl;

            currentTabPanel.appendChild(this.jsObject.FormSeparator());
            var filterEngineTable = this.jsObject.CreateHTMLTable();
            currentTabPanel.appendChild(filterEngineTable);
            filterEngineTable.addTextCell(this.jsObject.loc.PropertyMain.FilterEngine + ": ").className = "stiDesignerCaptionControls";
            var filterEngine = this.jsObject.DropDownList("dataFormFilterEngine" + dataSourcesTabs[i], 125, this.jsObject.loc.PropertyMain.FilterEngine,
                this.jsObject.GetFilterIngineItems(), true, false);
            filterEngine.setKey("ReportEngine");
            filterEngineTable.addCell(filterEngine).style.padding = "5px 15px 0 0";
            currentTabPanel.filterEngine = filterEngine;

            var currentDataSource = this.wizardForm.dataSources[dataSourcesTabs[i]];
            filterControl.fill(currentDataSource.filters, currentDataSource.filterOn, currentDataSource.filterMode);
        }
    }

    filterPanel.apply = function () {
        for (var dataSourceName in this.tabbedPane.tabsPanels) {
            var filterControl = this.tabbedPane.tabsPanels[dataSourceName].filterControl;
            var filterResult = filterControl.getValue();
            var dataSource = this.wizardForm.dataSources[dataSourceName];
            var filterEngine = this.tabbedPane.tabsPanels[dataSourceName].filterEngine;

            dataSource.filters = filterResult.filters;
            dataSource.filterOn = filterResult.filterOn;
            dataSource.filterMode = filterResult.filterMode;
            dataSource.filterEngine = filterEngine.key;
        }
    }
}



