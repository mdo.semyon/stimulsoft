
StiMobileDesigner.prototype.InitializeDesigner = function () {

    this.options.mobileDesigner.pressedDown = function () {
        var options = this.jsObject.options;
        
        //Close Current Menu
        if (options.currentMenu != null) {
            if (options.menuPressed != options.currentMenu && options.currentMenu.parentButton != options.buttonPressed &&
                !options.horMenuPressed && options.horMenuPressed != options.currentMenu.parentMenu &&
                (!(options.imageListMenuPressed || options.dropDownListMenuPressed || options.colorDialogPressed)))
                options.currentMenu.changeVisibleState(false);
        }

        //Close Current HorMenu
        if (options.currentHorMenu != null) {
            if (options.horMenuPressed != options.currentHorMenu && options.currentHorMenu.parentButton != options.buttonPressed &&
                options.currentHorMenu.parentButton != options.menuItemPressed && (!(options.imageListMenuPressed || options.dropDownListMenuPressed || options.colorDialogPressed)))
                options.currentHorMenu.changeVisibleState(false);
        }

        //Close Current ImageList
        if (options.currentImageListMenu != null)
            if (options.imageListMenuPressed != options.currentImageListMenu && options.currentImageListMenu.parentButton != options.buttonPressed)
                options.currentImageListMenu.changeVisibleState(false);

        //Close Current DropDownList
        if (options.currentDropDownListMenu != null)
            if (options.dropDownListMenuPressed != options.currentDropDownListMenu && options.currentDropDownListMenu.parentButton != options.buttonPressed)
                options.currentDropDownListMenu.changeVisibleState(false);

        //Close Current ColorDialog
        if (options.currentColorDialog != null)
            if (options.colorDialogPressed != options.currentColorDialog && options.currentColorDialog.parentButton != options.buttonPressed)
                options.currentColorDialog.changeVisibleState(false);

        //Close Current DatePicker
        if (options.currentDatePicker != null)
            if (options.datePickerPressed != options.currentDatePicker && options.currentDatePicker.parentButton != options.buttonPressed)
                options.currentDatePicker.changeVisibleState(false);

        //Close Draw Component Mode
        if (((!options.buttonPressed && !options.pagePressed) || (options.buttonPressed && options.buttonPressed.groupName != "Components")) && !options.menuPressed && options.insertPanel)
            options.insertPanel.resetChoose();

        //Close Draw Component Mode
        if (((!options.buttonPressed && !options.pagePressed) || (options.buttonPressed && !options.buttonPressed.toolboxOwner)) && !options.menuItemPressed && options.toolbox) {
            options.toolbox.resetChoose();
        }

        //Close Properties Panel       
        if (!options.propertiesPanelPressed && options.propertiesPanel.fixedViewMode && !options.menuPressed && !options.dropDownListMenuPressed &&
            !options.imageListMenuPressed && !options.colorDialogPressed && !options.datePickerPressed && !options.formPressed && !options.disabledPanelPressed)
            options.propertiesPanel.changeVisibleState(false);

        //Hide Check Popup Panel
        if (options.checkPopupPanel) {
            options.checkPopupPanel.hide();
        }

        //Hide Check Preview Panel
        if (options.checkPreviewPanel) {
            options.checkPreviewPanel.hide();
        }

        //Close Report Checker Panel
        if (options.checkPanel && options.checkPanel.isVisible && !options.checkPanelPressed)
            options.checkPanel.changeVisibleState(false);

        //Dictionary Panel Pressed
        if (options.dictionaryPanel && !options.dictionaryPanelPressed) {
            options.dictionaryPanel.setFocused(false);
        }

        options.buttonPressed = false;
        options.menuItemPressed = false;
        options.menuPressed = false;
        options.horMenuPressed = false;
        options.formPressed = false;
        options.dropDownListMenuPressed = false;
        options.imageListMenuPressed = false;
        options.colorDialogPressed = false;
        options.pagePressed = false;
        options.fingerIsMoved = false;
        options.disabledPanelPressed = false;
        options.datePickerPressed = false;
        options.propertiesPanelPressed = false;
        options.checkPanelPressed = false;
        options.dictionaryPanelPressed = false;
        options.startInsertDataToElement = false;
    }

    this.options.mobileDesigner.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.jsObject.options.isTouchClick = false;
        this.pressedDown();
        this.jsObject.options.designerIsFocused = true;
    }

    this.options.mobileDesigner.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        if (this.jsObject.options.buttonsTimer) {
            clearTimeout(this.jsObject.options.buttonsTimer[2]);
            this.jsObject.options.buttonsTimer[0].className = this.jsObject.options.buttonsTimer[1];
            this.jsObject.options.buttonsTimer = null;
        }
        this.jsObject.options.isTouchClick = true;
        this.pressedDown();
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    this.options.mobileDesigner.onmouseup = function () {
        if (this.isTouchEndFlag) return;
        this.ontouchend(true);
        clearTimeout(this.jsObject.options.scrollLeftTimer);
        clearTimeout(this.jsObject.options.scrollRightTimer);
    }

    this.options.mobileDesigner.ontouchend = function (mouseProcess) {
        var this_ = this;
        this.isTouchEndFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchEndTimer);
        this.jsObject.options.fingerIsMoved = false;
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    this.options.mobileDesigner.ontouchmove = function () {
        this.jsObject.options.fingerIsMoved = true;
    }
        
    var jsObject = this;
    
    setTimeout(function () {
        jsObject.addEvent(window, 'blur', function (event) {
            jsObject.options.designerIsFocused = false;
            jsObject.options.CTRL_pressed = false;
            jsObject.options.SHIFT_pressed = false;
        });

        jsObject.addEvent(window, 'focus', function (event) {
            jsObject.options.designerIsFocused = true;
        });
    }, 5000);

    this.addEvent(window, 'keyup', function (e) {
        if (e) {
            if (e.keyCode == 17) {
                jsObject.options.CTRL_pressed = false;
                jsObject.options.startMouseWheelDelta = null;
                jsObject.options.mouseWheelOldDelta = null;
            }
            else if (e.keyCode == 16) {
                jsObject.options.SHIFT_pressed = false;
            }
        }
    });

    this.addEvent(window, 'keydown', function (e) {
        if (!jsObject.options.designerIsFocused) return;
        if (e) {
            //Ctrl
            if (e.keyCode == 17) {
                jsObject.options.CTRL_pressed = true;
            }
            //Shift
            else if (e.keyCode == 16) {
                jsObject.options.SHIFT_pressed = true;
            }
            //Delete
            else if (e.keyCode == 46) {
                if (!jsObject.options.controlsIsFocused && !jsObject.options.currentForm) {
                    if (jsObject.options.dictionaryPanel && jsObject.options.dictionaryPanel.isFocused) {
                        if (jsObject.options.dictionaryPanel.toolBar.controls.DeleteItem.isEnabled) {
                            jsObject.DeleteItemDictionaryTree();
                        }
                    }
                    else if (jsObject.options.buttons.removeComponent.isEnabled) {
                        jsObject.ExecuteAction("removeComponent");
                    }
                }
                else if (jsObject.options.currentForm && jsObject.options.currentForm.name == "styleDesignerForm" && !jsObject.options.controlsIsFocused) {
                    var selectedItem = jsObject.options.currentForm.stylesTree.selectedItem;
                    if (selectedItem && selectedItem.itemObject.typeItem != "MainItem") {
                        selectedItem.remove();
                    }
                }
            }
            //Enter
            else if (e.keyCode == 13) {
                if (jsObject.options.CTRL_pressed) {
                    if (jsObject.options.currentForm && jsObject.options.currentForm.visible) {
                        jsObject.options.currentForm.action();
                    }
                }
                else if (jsObject.options.currentForm && jsObject.options.currentForm.name == "messageForm" && jsObject.options.currentForm.visible) {
                    jsObject.options.currentForm.action(true);
                    jsObject.options.currentForm.changeVisibleState(false);
                }
            }
            //Esc
            else if (e.keyCode == 27) {
                if (jsObject.options.currentForm && jsObject.options.currentForm.visible) {
                    var form = jsObject.options.currentForm;
                    form.changeVisibleState(false);
                    if (form.cancelAction) form.cancelAction();
                    jsObject.options.mobileDesigner.pressedDown();
                }
                else if (jsObject.options.menus.fileMenu && jsObject.options.menus.fileMenu.visible) {
                    jsObject.options.menus.fileMenu.changeVisibleState(false);
                }
            }
            //Ctrl+C
            else if (e.keyCode == 67 && jsObject.options.CTRL_pressed) {
                if ((!jsObject.options.currentForm || !jsObject.options.currentForm.visible) &&
                    jsObject.options.buttons.copyComponent.isEnabled && !jsObject.options.controlsIsFocused) {
                    jsObject.ExecuteAction("copyComponent");
                }
                else if (jsObject.options.currentForm && jsObject.options.currentForm.name == "styleDesignerForm" && !jsObject.options.controlsIsFocused) {
                    var selectedItem = jsObject.options.currentForm.stylesTree.selectedItem;
                    if (selectedItem && selectedItem.itemObject.properties) {
                        jsObject.options.currentForm.copiedStyle = jsObject.CopyObject(selectedItem.itemObject);
                    }
                }
            }
            //Ctrl+V
            else if (e.keyCode == 86 && jsObject.options.CTRL_pressed) {
                if ((!jsObject.options.currentForm || !jsObject.options.currentForm.visible) &&
                    jsObject.options.buttons.pasteComponent.isEnabled && !jsObject.options.controlsIsFocused && !jsObject.options.selectingRect &&
                    !jsObject.options.movingCloneComponents && !jsObject.options.drawComponent && !jsObject.options.cursorRect && !jsObject.options.startCopyWithCTRL) {
                    jsObject.PasteCurrentClipboardComponent();
                    jsObject.ExecuteAction("pasteComponent");
                }
                else if (jsObject.options.currentForm && jsObject.options.currentForm.name == "styleDesignerForm" &&
                    !jsObject.options.controlsIsFocused && jsObject.options.currentForm.copiedStyle) {
                    var newName = jsObject.options.currentForm.copiedStyle.properties.name + "_" + jsObject.loc.Report.CopyOf;
                    jsObject.options.currentForm.stylesTree.addItem(jsObject.CopyObject(jsObject.options.currentForm.copiedStyle), newName);
                }
            }
            //Ctrl+X
            else if (e.keyCode == 88 && jsObject.options.CTRL_pressed) {
                if ((!jsObject.options.currentForm || !jsObject.options.currentForm.visible) &&
                    jsObject.options.buttons.cutComponent.isEnabled && !jsObject.options.controlsIsFocused) {
                    jsObject.ExecuteAction("cutComponent");
                }
                else if (jsObject.options.currentForm && jsObject.options.currentForm.name == "styleDesignerForm" && !jsObject.options.controlsIsFocused) {
                    var selectedItem = jsObject.options.currentForm.stylesTree.selectedItem;
                    if (selectedItem && selectedItem.itemObject.properties) {
                        jsObject.options.currentForm.copiedStyle = jsObject.CopyObject(selectedItem.itemObject);
                        selectedItem.remove();
                    }
                }
            }
            //Ctrl+Z
            else if (e.keyCode == 90 && jsObject.options.CTRL_pressed) {
                if ((!jsObject.options.currentForm || !jsObject.options.currentForm.visible) &&
                    jsObject.options.buttons.undoButton.isEnabled && !jsObject.options.controlsIsFocused) {
                    jsObject.ExecuteAction("undoButton");
                }
            }
        }
    });

    this.addEvent(window, 'beforeunload', function (event) {
        if (jsObject.options.reportIsModified && !jsObject.options.scrollbiMode && !jsObject.options.ignoreBeforeUnload) {
            var msg = "Are you sure you want to leave this page without saving the report?";
            if (typeof event == "undefined") event = window.event;
            if (event) event.returnValue = msg;
            
            return msg;
        }
    });

    this.addEvent(window, 'resize', function (event) {
        if (jsObject.options.currentForm && !jsObject.options.currentForm.isNotModal && jsObject.options.currentForm.visible) {
            clearTimeout(jsObject.resizeTimer);
            jsObject.resizeTimer = setTimeout(function () {
                jsObject.SetObjectToCenter(jsObject.options.currentForm);
            }, 50);
        } 
    });

    //Add Drag & Drop to Designer

    this.AddDragAndDropToContainer(jsObject.options.mainPanel, function (files, content) {
        var reportExts = ["mrt", "mrx", "mrz"];
        for (var i = 0; i < files.length; i++) {
            if (i == 1) return; //Temporarily

            var fileName = files[i].name;
            var fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
            
            if (reportExts.indexOf(fileExt) >= 0) {
                jsObject.OpenReport(fileName, content);

                if (jsObject.options.jsMode && jsObject.options.cloudParameters) {
                    jsObject.options.cloudParameters.reportTemplateItemKey = null;
                }
            }
            else {
                jsObject.AddResourceFile(files[i], content);
            }            
        }
    });

    //Mouse wheel
    if ('onwheel' in document) {
        // IE9+, FF17+, Ch31+
        this.addEvent(jsObject.options.mainPanel, "wheel", function (e) { jsObject.onWheel(e); });
    } else if ('onmousewheel' in document) {
        // old event
        this.addEvent(jsObject.options.mainPanel, "mousewheel", function (e) { jsObject.onWheel(e); });
    } else {
        // Firefox < 17
        this.addEvent(jsObject.options.mainPanel, "MozMousePixelScroll", function (e) { jsObject.onWheel(e); });
    }

    var navigatorName = this.GetNavigatorName();
    var startZoom = 1;

    this.onWheel = function (e) {
        if (!jsObject.options.report) return;
        e = e || window.event;
        var delta = e.deltaY || e.detail || e.wheelDelta;        
        var deltaStep = 0.1;
        
        if (jsObject.options.CTRL_pressed) {
            if (jsObject.options.startMouseWheelDelta == null ||
                jsObject.options.mouseWheelOldDelta == null ||
                (jsObject.options.mouseWheelOldDelta > 0 && delta < 0) ||
                (jsObject.options.mouseWheelOldDelta < 0 && delta > 0))
            {
                jsObject.options.startMouseWheelDelta = 0;
                startZoom = jsObject.options.report.zoom;
            }
                        
            if ((delta > 0 && navigatorName != "MSIE") || (navigatorName == "MSIE" && delta < 0))
                jsObject.options.startMouseWheelDelta += deltaStep;
            else
                jsObject.options.startMouseWheelDelta -= deltaStep;
            
            var newZoom = Math.round((startZoom - jsObject.options.startMouseWheelDelta) * 10) / 10;
            if (newZoom < 0.1) newZoom = 0.1;
            if (newZoom > 2) newZoom = 2;
            jsObject.options.report.zoom = newZoom;
            if (jsObject.options.currentPage) jsObject.PreZoomPage(jsObject.options.currentPage);

            e.preventDefault ? e.preventDefault() : (e.returnValue = false);
        }
        else {
            jsObject.options.startMouseWheelDelta = null;
            jsObject.options.mouseWheelOldDelta = null;
        }

        jsObject.options.mouseWheelOldDelta = delta;
    }
}