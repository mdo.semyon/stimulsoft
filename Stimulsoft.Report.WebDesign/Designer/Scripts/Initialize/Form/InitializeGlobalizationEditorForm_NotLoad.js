﻿
StiMobileDesigner.prototype.InitializeGlobalizationEditorForm_ = function () {

    var form = this.BaseFormPanel("globalizationEditorForm", this.loc.FormGlobalizationEditor.title, 1, this.HelpLinks["globalizationEditor"]);
    form.buttonsSeparator.style.display = "none";
    form.buttonsPanel.style.display = "none";

    //MainTable
    var mainTable = this.CreateHTMLTable();
    form.container.appendChild(mainTable);
    mainTable.style.borderCollapse = "separate";

    //Toolbar
    var buttons = [
        ["addCulture", this.loc.FormGlobalizationEditor.AddCulture.replace("&", ""), "CultureEditor.AddCulture.png", null],
        ["removeCulture", this.loc.FormGlobalizationEditor.RemoveCulture.replace("&", ""), "Remove.png", null],
        ["separator"],
        ["getCultureSettings", null, "CultureEditor.GetCulture.png", this.loc.FormGlobalizationEditor.GetCulture],
        ["setCultureSettings", null, "CultureEditor.SetCulture.png", this.loc.FormGlobalizationEditor.SetCulture],
        ["separator"],
        ["autoLocalize", this.loc.FormGlobalizationEditor.AutoLocalizeReportOnRun, null, null]
    ]

    var toolBar = this.CreateHTMLTable();
    var toolBarCell = mainTable.addCell(toolBar);
    toolBarCell.className = "stiDesignerStyleDesignerFormToolbarCell";
    toolBarCell.setAttribute("colspan", "3");

    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton(buttons[i][0], null, buttons[i][1], buttons[i][2], buttons[i][3],
            buttons[i][0] == "addCulture" ? "Down" : null, this.GetStyles("StandartSmallButton"));
        button.style.margin = "4px";
        toolBar[buttons[i][0]] = button;
        toolBar.addCell(button);
    }

    //Cultures Container
    var culturesContainer = this.EasyContainer(120, 500);
    mainTable.addCellInNextRow(culturesContainer);

    //Properties Container
    var propertiesContainer = this.EasyContainer(250, 500);
    propertiesContainer.className = "stiDesignerGlobalizatonStringsFormContainer";
    mainTable.addCellInLastRow(propertiesContainer);

    //Text Area
    var textArea = this.TextArea(null, 300, 500);
    textArea.style.border = "0px";
    mainTable.addCellInLastRow(textArea);
    textArea.addInsertButton();
    textArea.insertButton.style.display = "none"; //addded only insert events

    //Add Culture
    var addCultureMenu = this.VerticalMenu("addCultureMenu", toolBar.addCulture, "Down", null, this.GetStyles("MenuStandartItem"));
    
    addCultureMenu.action = function (item) {
        addCultureMenu.changeVisibleState(false);

        form.jsObject.SendCommandAddGlobalizationStrings(item.key, function (answer) {
            if (answer.globalizationStrings) {
                var caption = form.getCultureDisplayName(answer.globalizationStrings.cultureName);
                var item = culturesContainer.addItem(null, answer.globalizationStrings, caption, null, true);
                item.action();
            }
        });
    }

    toolBar.addCulture.action = function () {
        var items = [];
        for (var i = 0; i < form.cultures.length; i++) {
            items.push(form.jsObject.Item(form.cultures[i].name, form.cultures[i].displayName, null, form.cultures[i].name));
        }               
        addCultureMenu.addItems(items);
        addCultureMenu.changeVisibleState(true);
    }

    //Remove Culture
    toolBar.removeCulture.action = function () {
        if (culturesContainer.selectedItem) {
            form.jsObject.SendCommandRemoveGlobalizationStrings(culturesContainer.selectedItem.getIndex(), function (answer) {
                if (answer.success) {
                    culturesContainer.selectedItem.remove();
                }
            });
        }
    }

    //Get Culture
    toolBar.getCultureSettings.action = function () {
        form.jsObject.GetCultureSettingsFromReport(culturesContainer.selectedItem.getIndex(), function (answer) {
            if (culturesContainer.selectedItem && answer.globalizationStrings) {
                culturesContainer.selectedItem.itemObject = answer.globalizationStrings;
                culturesContainer.selectedItem.action();
            }
        });
    }

    //Set Culture
    toolBar.setCultureSettings.action = function () {
        if (culturesContainer.selectedItem) {
            form.jsObject.SetCultureSettingsToReport(culturesContainer.selectedItem.itemObject.cultureName, function (answer) {
                for (var i = 0; i < culturesContainer.childNodes.length; i++) {
                    culturesContainer.childNodes[i].itemObject.reportItems = answer.reportItems;
                }
                culturesContainer.onAction();

                if (answer["reportGuid"] && answer["reportObject"]) {
                    form.jsObject.options.report = null;
                    form.jsObject.options.reportGuid = answer.reportGuid;
                    form.jsObject.LoadReport(form.jsObject.ParseReport(answer.reportObject), true);
                    form.jsObject.options.reportIsModified = true;
                    form.jsObject.options.buttons.undoButton.setEnabled(answer.enabledUndoButton);
                    form.jsObject.options.buttons.redoButton.setEnabled(true);
                }
                form.jsObject.BackToSelectedComponent(answer.selectedObjectName);
            });
        }
    }

    //Auto Localize
    toolBar.autoLocalize.action = function () {
        toolBar.autoLocalize.setSelected(!toolBar.autoLocalize.isSelected);
        this.jsObject.options.report.properties.autoLocalizeReportOnRun = toolBar.autoLocalize.isSelected;
        this.jsObject.SendCommandSetReportProperties(["autoLocalizeReportOnRun"]);
    }
        
    culturesContainer.onAction = function () {
        propertiesContainer.clear();
        textArea.value = "";
       
        if (this.selectedItem) {
            var reportItems = [];
            for (var itemName in this.selectedItem.itemObject.reportItems) {
                reportItems.push(itemName);
                reportItems.sort();
            }

            for (var i = 0; i < reportItems.length; i++) {
                var item = propertiesContainer.addItem(null, this.selectedItem.itemObject, reportItems[i], null, true);
                item.propertyName = reportItems[i];

                item.markIsChanged = function () {
                    if (this.caption) {
                        this.caption.style.fontWeight = "bold";
                        this.caption.style.color = "#000000";
                    }
                }

                if (this.selectedItem.itemObject.items[item.propertyName] != this.selectedItem.itemObject.reportItems[item.propertyName]) {
                    item.markIsChanged();
                }
            }

            if (propertiesContainer.childNodes.length > 0) {
                propertiesContainer.childNodes[0].action();
            }
        }

        toolBar.removeCulture.setEnabled(this.selectedItem);
        toolBar.getCultureSettings.setEnabled(this.selectedItem);
        toolBar.setCultureSettings.setEnabled(this.selectedItem);
    }

    propertiesContainer.onAction = function () {
        if (this.selectedItem) {
            var propertyName = this.selectedItem.propertyName;            
            var text = this.selectedItem.itemObject.items[propertyName];
            if (text == null) text = this.selectedItem.itemObject.reportItems[propertyName];
            textArea.value = text;
            textArea.focus();
        }
    }
        
    textArea.action = function () {
        if (culturesContainer.selectedItem && propertiesContainer.selectedItem) {
            var oldValue = culturesContainer.selectedItem.itemObject.items[propertiesContainer.selectedItem.propertyName];
            culturesContainer.selectedItem.itemObject.items[propertiesContainer.selectedItem.propertyName] = this.value;
            if (oldValue != this.value) {
                propertiesContainer.selectedItem.markIsChanged();
                form.jsObject.SendCommandApplyGlobalizationStrings(culturesContainer.selectedItem.getIndex(), propertiesContainer.selectedItem.propertyName, this.value);
            }
        }
    }

    form.getCultureDisplayName = function (cultureName) {
        if (form.cultures) {
            for (var i = 0; i < form.cultures.length; i++) {
                if (form.cultures[i].name == cultureName)
                    return form.cultures[i].displayName;
            }
        }
        return cultureName;
    }

    form.fill = function (globalizationStrings) {
        culturesContainer.clear();
        propertiesContainer.clear();
        
        for (var i = 0; i < globalizationStrings.length; i++) {
            var caption = form.getCultureDisplayName(globalizationStrings[i].cultureName);
            culturesContainer.addItem(null, globalizationStrings[i], caption, null, true);
        }

        if (culturesContainer.childNodes.length > 0) {
            culturesContainer.childNodes[0].action();
        }
    }

    var propertiesPanel = this.options.propertiesPanel;
        
    form.show = function (globProperties) {
        form.cultures = globProperties.cultures;
        form.fill(globProperties.globalizationStrings);
        
        propertiesPanel.setDictionaryMode(true);
        propertiesPanel.setEnabled(true);
        propertiesPanel.editFormControl = textArea;
        
        toolBar.autoLocalize.setSelected(this.jsObject.options.report.properties.autoLocalizeReportOnRun);

        this.changeVisibleState(true);
        textArea.focus();
    }

    form.onhide = function () {
        propertiesPanel.setDictionaryMode(false);
        propertiesPanel.editFormControl = null;
    }

    form.action = function () {
        this.changeVisibleState(false);
    }

    return form;
}