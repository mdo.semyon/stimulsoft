﻿
StiMobileDesigner.prototype.InitializeSelectConnectionForm_ = function () {
    var selectConnectionForm = this.BaseForm("selectConnectionForm", this.loc.FormTitles.ConnectionSelectForm, 3, this.HelpLinks["connectionNew"]);
    selectConnectionForm.connectionGroups = {};
    selectConnectionForm.connectionButtons = {};

    selectConnectionForm.skipSchemaWizard = this.CheckBox(null, this.loc.FormDictionaryDesigner.SkipSchemaWizard);
    selectConnectionForm.skipSchemaWizard.style.margin = "8px";

    var footerTable = this.CreateHTMLTable();
    footerTable.style.width = "100%";
    var buttonsPanel = selectConnectionForm.buttonsPanel;
    selectConnectionForm.removeChild(buttonsPanel);
    selectConnectionForm.appendChild(footerTable);
    footerTable.addCell(selectConnectionForm.skipSchemaWizard).style.width = "1px";
    footerTable.addCell();
    footerTable.addCell(selectConnectionForm.buttonCancel).style.width = "1px";

    var connectionsContainer = document.createElement("div");
    connectionsContainer.style.width = "820px";
    connectionsContainer.style.height = "500px";
    connectionsContainer.style.overflowX = "hidden";
    connectionsContainer.style.overflowY = "auto";
    selectConnectionForm.container.appendChild(connectionsContainer);

    var connectionGroups = [
            { name: "ReportConnections", localizedName: this.loc.FormDatabaseEdit.ReportConnections },
            { name: "Favorites" },
            { name: "SQL" },
            { name: "NoSQL" },
            { name: "REST" },
            { name: "Files", localizedName: this.loc.PropertyMain.File },
            { name: "Objects", localizedName: this.loc.QueryBuilder.Objects },
            { name: "Recent", localizedName: this.loc.FormDatabaseEdit.RecentConnections }
        ]
       
    for (var i = 0; i < connectionGroups.length; i++) {
        var groupName = connectionGroups[i].name;
        var group = this.InteractiveGroupPanel(connectionGroups[i].localizedName || groupName);
        group.style.display = "none";        
        selectConnectionForm.connectionGroups[groupName] = group;
        connectionsContainer.appendChild(group);
    }
    
    selectConnectionForm.onshow = function () {
        selectConnectionForm.skipSchemaWizard.setChecked(this.jsObject.GetCookie("StimulsoftMobileDesignerSkipSchemaWizard") == "true");
        this.jsObject.SendCommandGetConnectionTypes();
    }

    selectConnectionForm.fillConnections = function (connections) {
        var favoriteConnections = this.jsObject.GetFavoriteConnectionsFromCookies();
        var recentConnections = this.jsObject.GetRecentConnectionsFromCookies();
        var openingConnectionGroups = this.jsObject.GetOpeningConnectionGroups();
        var firstVisibleGroup = null;
        var countOpening = 0;

        for (var i = 0; i < connectionGroups.length; i++) {
            var groupName = connectionGroups[i].name;
            if (connections[groupName]) {
                for (var indexButton = 0; indexButton < connections[groupName].length; indexButton++) {
                    var haveFavorite = groupName != "Objects" && groupName != "Recent" && groupName != "ReportConnections";
                    var button = this.jsObject.ConnectionButton(connections[groupName][indexButton], groupName, this, haveFavorite);
                    var group = this.connectionGroups[groupName];
                    if (group) {
                        var showGroup = this.jsObject.options.permissionDataConnections.indexOf("All") >= 0 ||
                            this.jsObject.options.permissionDataConnections.indexOf("Create") >= 0 ||
                            groupName == "ReportConnections" || groupName == "Objects";

                        group.style.display = showGroup ? "" : "none";
                        if (!firstVisibleGroup && showGroup) firstVisibleGroup = group;
                        if (groupName == "ReportConnections" || groupName == "Favorites" || this.jsObject.options.jsMode || openingConnectionGroups.indexOf(groupName) >= 0) {
                            group.setOpening(true);
                            countOpening++;
                        }
                        group.container.appendChild(button);
                        this.connectionButtons[connections[groupName][indexButton].type] = button;
                        if (haveFavorite && favoriteConnections[connections[groupName][indexButton].type]) {
                            button.setFavorite(true, false, true);
                        }
                    }
                    if (connections[groupName][indexButton].type == "StiCrossTabAdapterService") {
                        button.setEnabled(false);
                    }
                }
            }
        }

        if (firstVisibleGroup && countOpening == 0) firstVisibleGroup.setOpening(true);

        if (recentConnections.length > 0) {
            this.connectionGroups.Recent.style.display = "";
            for (var i = 0; i < recentConnections.length; i++) {
                var button = this.jsObject.ConnectionButton(recentConnections[i], "Recent", this, false);
                this.connectionGroups.Recent.container.appendChild(button);
            }
        }
    }

    selectConnectionForm.addToFavoritesGroup = function (button) {
        var favoritesGroup = this.connectionGroups.Favorites;
        favoritesGroup.style.display = "";

        var favoriteButton = this.jsObject.ConnectionButton(button.connectionObject, button.groupName, this, true);
        favoriteButton.setFavorite(true);
        favoriteButton.inFavoriteGroup = true;
        favoritesGroup.container.appendChild(favoriteButton);
    }

    selectConnectionForm.deleteFromFavoritesGroup = function (button) {
        var favoritesGroup = this.connectionGroups.Favorites;
        if (!button.inFavoriteGroup) {
            for (var i = 0; i < favoritesGroup.container.childNodes.length; i++) {
                if (favoritesGroup.container.childNodes[i].connectionObject && favoritesGroup.container.childNodes[i].connectionObject.typeConnection == button.connectionObject.typeConnection)
                    favoritesGroup.container.removeChild(favoritesGroup.container.childNodes[i]);
            }
        }
        else {
            if (this.connectionButtons[button.connectionObject.typeConnection]) this.connectionButtons[button.connectionObject.typeConnection].setFavorite(false);
            favoritesGroup.container.removeChild(button);
        }

        if (favoritesGroup.container.childNodes.length == 0) favoritesGroup.style.display = "none";
    }
    
    selectConnectionForm.onhide = function () {
        this.jsObject.SaveOpeningConnectionGroups(this.connectionGroups);
    }

    return selectConnectionForm;
}

StiMobileDesigner.prototype.ConnectionButton = function (connectionObject, groupName, selectConnectionForm, haveFavorit) {
    var imageName = "Connections." + connectionObject.typeConnection + ".png";
    if (!this.options.images[imageName]) imageName = "Connections.BigDataSource.png";
    var caption = groupName == "Objects" ? this.GetLocalizedAdapterName(connectionObject.typeConnection) : connectionObject.name;

    var button = this.SmallButton(null, null, caption, imageName, caption, null, this.GetStyles("FormButton"), true);
    button.groupName = groupName;
    button.connectionObject = connectionObject;
    button.isFavorite = false;
    button.style.height = "45px";
    button.style.width = groupName == "Objects" ? "230px" : "180px";
    button.style.margin = "8px";
    button.style.display = "inline-block";
    button.style.overflow = "hidden";
    if (button.caption) {
        button.caption.style.paddingRight = "25px";
        button.caption.style.whiteSpace = "normal";
    }
    if (button.imageCell) button.imageCell.style.padding = "0 6px";

    if (haveFavorit) {
        var favoriteImg = document.createElement("img");
        favoriteImg.jsObject = this;
        favoriteImg.src = this.options.images["Connections.Favorites.png"];
        favoriteImg.style.visibility = "hidden";
        var favoriteImgCell = button.innerTable.addCell(favoriteImg);
        favoriteImgCell.style.verticalAlign = "top";
        favoriteImgCell.style.width = "1px";
        button.favoriteImg = favoriteImg;
        button.innerTable.style.width = "100%";
        if (button.imageCell) button.imageCell.style.width = "1px";

        favoriteImg.onclick = function (event) {
            if (this.isTouchProcessFlag) return;
            button.setFavorite(!button.isFavorite, true, true);
            button.favoriteClicked = true;
        }

        favoriteImg.ontouchend = function (event) {
            var this_ = this;
            this.isTouchProcessFlag = true;

            button.setFavorite(!button.isFavorite, true, true);
            button.favoriteClicked = true;

            setTimeout(function () {
                this_.isTouchProcessFlag = false;
            }, 1000);
        }

        button.onmouseover = function () {
            if (this.jsObject.options.isTouchDevice) return;
            this.className = this.styles["over"] + "_Mouse";
            this.isOver = true;
            favoriteImg.style.visibility = "visible";
        }

        button.onmouseout = function () {
            if (this.jsObject.options.isTouchDevice) return;
            this.isOver = false;
            this.className = (this.isSelected ? this.styles["selected"] : this.styles["default"]) + "_Mouse";
            if (!this.isFavorite) favoriteImg.style.visibility = "hidden";
        }

        button.setFavorite = function (state, updateCookies, updateFavoritesGroup) {
            this.isFavorite = state;
            favoriteImg.src = this.jsObject.options.images[this.isFavorite ? "Connections.FavoritesYellow.png" : "Connections.Favorites.png"];
            favoriteImg.style.visibility = state ? "visible" : "hidden";

            if (updateFavoritesGroup) {
                if (state)
                    selectConnectionForm.addToFavoritesGroup(this);
                else
                    selectConnectionForm.deleteFromFavoritesGroup(this);
            }

            if (updateCookies) {
                if (state)
                    this.jsObject.SaveFavoriteConnectionToCookies(this.connectionObject.typeConnection);
                else
                    this.jsObject.DeleteFavoriteConnectionFromCookies(this.connectionObject.typeConnection);
            }
        }
    }

    button.action = function () {
        if (this.favoriteClicked) {
            this.favoriteClicked = false;
            return;
        }
        
        this.jsObject.SetCookie("StimulsoftMobileDesignerSkipSchemaWizard", selectConnectionForm.skipSchemaWizard.isChecked.toString());
        selectConnectionForm.changeVisibleState(false);

        var this_ = this;
        var skipSchema = selectConnectionForm.skipSchemaWizard.isChecked;

        if (this.connectionObject) {
            var typeConnection = this.connectionObject.typeConnection;

            var isFileDataConnection = typeConnection && 
                (typeConnection.endsWith("StiXmlDatabase") ||
                typeConnection.endsWith("StiJsonDatabase") ||
                typeConnection.endsWith("StiDBaseDatabase") ||
                typeConnection.endsWith("StiCsvDatabase") ||
                typeConnection.endsWith("StiExcelDatabase"));
        
            if (isFileDataConnection) skipSchema = false;
        }

        if (this.groupName == "ReportConnections") {
            if (skipSchema) {
                this.jsObject.InitializeEditDataSourceForm(function (editDataSourceForm) {
                    editDataSourceForm.datasource = this_.jsObject.GetDataAdapterTypeFromDatabaseType(this_.connectionObject.typeConnection);
                    editDataSourceForm.nameInSource = this_.connectionObject.name;
                    editDataSourceForm.changeVisibleState(true);
                });
            }
            else {
                this.jsObject.InitializeSelectDataForm(function (selectDataForm) {
                    selectDataForm.databaseName = this_.connectionObject.name;
                    selectDataForm.connectionObject = this_.connectionObject;
                    selectDataForm.typeConnection = this_.connectionObject.typeConnection;
                    selectDataForm.changeVisibleState(true);
                });
            }
        }
        else if (this.groupName == "Objects") {
            if (this.connectionObject.typeConnection == "StiVirtualAdapterService") {
                this.jsObject.InitializeEditDataSourceFromOtherDatasourcesForm(function (form) {
                    form.datasource = this_.connectionObject.typeConnection;
                    form.changeVisibleState(true);
                });
            }
            else if (this.connectionObject.typeConnection == "StiCrossTabAdapterService") {
                this.jsObject.InitializeEditDataSourceFromCrossTabForm(function (form) {
                    form.datasource = this_.connectionObject.typeConnection;
                    form.changeVisibleState(true);
                });
            }
            else {
                this.jsObject.InitializeEditDataSourceForm(function (form) {
                    form.datasource = this_.connectionObject.typeConnection;
                    form.changeVisibleState(true);
                });
            }
        }
        else if (this.groupName == "Recent") {
            this.jsObject.InitializeEditConnectionForm(function (editConnectionForm) {
                editConnectionForm.skipSchemaWizard = skipSchema;
                editConnectionForm.connection = this_.connectionObject;
                editConnectionForm.connection.isRecentConnection = true;
                editConnectionForm.changeVisibleState(true);
            });
        }
        else {
            this.jsObject.InitializeEditConnectionForm(function (editConnectionForm) {
                editConnectionForm.skipSchemaWizard = skipSchema;
                editConnectionForm.connection = this_.connectionObject.typeConnection;
                editConnectionForm.changeVisibleState(true);
            });
        }
    }

    return button;
}

StiMobileDesigner.prototype.GetFavoriteConnectionsFromCookies = function () {
    var connectionsStr = this.GetCookie("StimulsoftMobileDesignerFavoriteConnections");
   return (connectionsStr ? JSON.parse(connectionsStr) : {});
}

StiMobileDesigner.prototype.SaveFavoriteConnectionToCookies = function (connectionType) {
    var favoriteConnections = this.GetFavoriteConnectionsFromCookies();
    favoriteConnections[connectionType] = true;
    this.SetCookie("StimulsoftMobileDesignerFavoriteConnections", JSON.stringify(favoriteConnections));
}

StiMobileDesigner.prototype.DeleteFavoriteConnectionFromCookies = function (connectionType) {
    var favoriteConnections = this.GetFavoriteConnectionsFromCookies();
    delete favoriteConnections[connectionType];
    this.SetCookie("StimulsoftMobileDesignerFavoriteConnections", JSON.stringify(favoriteConnections));
}

StiMobileDesigner.prototype.SaveOpeningConnectionGroups = function (connectionGroups) {
    var openingGroups = [];
    for (var name in connectionGroups) {
        if (connectionGroups[name].isOpening) openingGroups.push(name);
    }
    this.SetCookie("StimulsoftMobileDesignerOpeningConnectionGroups", JSON.stringify(openingGroups));
}

StiMobileDesigner.prototype.GetOpeningConnectionGroups = function () {
    var openingGroupsStr = this.GetCookie("StimulsoftMobileDesignerOpeningConnectionGroups");
    return openingGroupsStr ? JSON.parse(openingGroupsStr) : [];
}