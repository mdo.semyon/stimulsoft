﻿
StiMobileDesigner.prototype.InitializeBarCodeForm_ = function () {
    var barCodeForm = this.BaseFormPanel("barCodeForm", this.loc.Components.StiBarCode, 1, this.HelpLinks["barcodeform"]);
    barCodeForm.controls = {};
    var jsObject = this;

    //BarCode Sample
    var barCodeSample = ("createElementNS" in document) ? document.createElementNS("http://www.w3.org/2000/svg", "svg") : document.createElement("svg");
    barCodeForm.barCodeSample = barCodeSample;
    barCodeSample.setAttribute("width", "430");
    barCodeSample.setAttribute("height", "200");
    barCodeSample.style.overflow = "hidden";
    barCodeSample.style.margin = "5px 10px 5px 10px";
    barCodeForm.container.appendChild(barCodeSample);

    barCodeSample.update = function () {
        jsObject.InsertSvgContent(this, barCodeForm.barCode.sampleImage);
    }

    barCodeForm.container.appendChild(this.FormSeparator());

    //Properties
    var propertiesContainer = document.createElement("div");
    barCodeForm.propertiesContainer = propertiesContainer;
    propertiesContainer.className = "stiBarCodeFormPropertiesContainer";
    barCodeForm.container.appendChild(propertiesContainer);

    var properties = [
        ["codeType", this.loc.PropertyMain.Type, this.BarCodeTypeControl("barCodeFormCodeType", this.options.propertyControlWidth + 4), "6px"],
        ["code", this.loc.PropertyMain.Code, this.PropertyExpressionControl("barCodeFormCode", this.options.propertyControlWidth, false), "6px"],
        ["angle", this.loc.PropertyMain.Angle, this.PropertyDropDownList("barCodeFormAngle", this.options.propertyControlWidth, this.GetBarCodeAngleItems(), true, false), "6px"],
        ["autoScale", this.loc.PropertyMain.AutoScale, this.CheckBox("barCodeFormAutoScale"), "7px 6px 7px 6px"],
        ["foreColor", this.loc.PropertyMain.ForeColor, this.PropertyColorControl("barCodeFormForeColor", null, this.options.propertyControlWidth), "5px 6px 5px 6px"],
        ["backColor", this.loc.PropertyMain.BackColor, this.PropertyColorControl("barCodeFormBackColor", null, this.options.propertyControlWidth), "5px 6px 5px 6px"],
        ["font", this.loc.PropertyMain.Font, this.PropertyFontControl("barCodeFormFont", "font", true), "6px"],
        ["showLabelText", this.loc.PropertyMain.ShowLabelText, this.CheckBox("barCodeShowLabelText"), "8px 6px 7px 6px"],
        ["showQuietZones", this.loc.PropertyMain.ShowQuietZones, this.CheckBox("barCodeShowQuietZones"), "7px 6px 8px 6px"]
    ];
    
    var proprtiesTable = this.CreateHTMLTable();
    proprtiesTable.style.margin = "";
    propertiesContainer.appendChild(proprtiesTable);

    for (var i = 0; i < properties.length; i++) {
        var textCell = proprtiesTable.addTextCellInLastRow(properties[i][1]);
        textCell.className = "stiDesignerCaptionControlsBigIntervals";
        textCell.style.whiteSpace = "normal";
        textCell.style.width = "100%";
        if (properties[i][0] == "font") {
            textCell.style.verticalAlign = "top";
            textCell.style.paddingTop = "10px";
        }

        var control = properties[i][2];
        control.propertyName = properties[i][0];
        control.style.margin = properties[i][3];
        proprtiesTable.addCellInLastRow(control);
        proprtiesTable.addRow();
        jsObject.AddMainMethodsToPropertyControl(control);
        barCodeForm.controls[control.propertyName] = control;

        control.action = function () {
            barCodeForm.applyBarCodeProperties([{ name: this.propertyName, value: this.getValue() }]);
        }
    }

    propertiesContainer.update = function () {
        barCodeForm.controls.codeType.setKey(barCodeForm.barCode.codeType);

        var commonProperties = barCodeForm.barCode.properties.common;
        for (var i = 0; i < properties.length; i++) {
            var control = properties[i][2];
            if (commonProperties[control.propertyName] != null) {
                control.setValue(commonProperties[control.propertyName])
            }
            if (control.propertyName == "code") {
                control.setEnabled(barCodeForm.barCode.codeType != "StiSSCC18BarCodeType");
                if (barCodeForm.barCode.codeType == "StiSSCC18BarCodeType") {
                    control.textBox.value = Base64.decode(commonProperties[control.propertyName].replace("Base64Code;", "")).replace(/\u001f/g, '');
                }
            }
        }
    }

    barCodeForm.onshow = function () {
        var selectedObject = jsObject.options.selectedObject;
        if (!selectedObject) changeVisibleState(false);
        this.selectedObject = selectedObject;

        jsObject.options.propertiesPanel.setEditBarCodeMode(true);
        this.updateControls();
    }

    barCodeForm.onhide = function () {
        jsObject.options.propertiesPanel.setEditBarCodeMode(false);
    }

    barCodeForm.action = function () {
        this.changeVisibleState(false);
        jsObject.SendCommandSendProperties(this.selectedObject, []);
        jsObject.options.updateLastStyleProperties = true;
    }

    barCodeForm.cancelAction = function () {
        jsObject.SendCommandCanceledEditComponent(barCodeForm.barCode.name);
    }

    barCodeForm.applyBarCodeProperties = function (properties) {
        jsObject.SendCommandToDesignerServer("ApplyBarCodeProperties", { componentName: barCodeForm.barCode.name, properties: properties }, function (answer) {
            barCodeForm.barCode = answer.barCode;
            barCodeForm.updateControls();
        });
    }

    barCodeForm.updateControls = function () {
        barCodeForm.barCodeSample.update();
        barCodeForm.propertiesContainer.update();
        var barCodePropertiesPanel = jsObject.options.propertiesPanel.editBarCodePropertiesPanel;
        if (barCodePropertiesPanel) {
            barCodePropertiesPanel.updateProperties(barCodeForm.barCode.properties.additional);
        }
    }

    return barCodeForm;
}