﻿
StiMobileDesigner.prototype.InitializeVariableItemsForm_ = function () {

    //Variable Items Form
    var variableItemsForm = this.BaseForm("variableItemsForm", this.loc.PropertyMain.Items, 4, this.HelpLinks["variableItems"]);
    variableItemsForm.controls = {};

    var buttons = [
        ["newValue", null, "VariableNew.png", this.loc.FormDictionaryDesigner.ValueNew],
        ["newExpression", null, "ExpressionNew.png", this.loc.FormDictionaryDesigner.ExpressionNew],
        ["selectColumns", null, "ItemsFromColumn.png", this.loc.Wizards.SelectColumns],
        ["removeItem", null, "Remove.png", this.loc.MainMenu.menuEditDelete.replace("&", "")],
        ["separator"],
        ["moveUp", null, "MoveUp.png", this.loc.Buttons.Up],
        ["moveDown", null, "MoveDown.png", this.loc.Buttons.Down],
    ]

    //ToolBar
    variableItemsForm.toolBar = this.CreateHTMLTable();
    variableItemsForm.toolBar.style.margin = "4px";
    variableItemsForm.container.appendChild(variableItemsForm.toolBar);
    variableItemsForm.container.appendChild(this.FormSeparator());
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i][0] == "separator") {
            variableItemsForm.toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton(buttons[i][0], null, buttons[i][1], buttons[i][2], buttons[i][3], null, this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "1px";
        variableItemsForm.controls[buttons[i][0]] = button;
        variableItemsForm.toolBar.addCell(button);
        if (buttons[i][0] == "newExpression" || buttons[i][0] == "selectColumns") variableItemsForm.toolBar.addCell(this.HomePanelSeparator());
    }

    variableItemsForm.controls.removeItem.action = function () {
        var variableItemsForm = this.jsObject.options.forms.variableItemsForm;
        if (variableItemsForm.controls.itemsContainer.selectedItem) variableItemsForm.controls.itemsContainer.selectedItem.remove();
    }

    variableItemsForm.controls.newValue.action = function () {
        variableItemsForm.controls.itemsContainer.addItem(this.jsObject.VariableItemObject("value"));
    }

    variableItemsForm.controls.newExpression.action = function () {
        variableItemsForm.controls.itemsContainer.addItem(this.jsObject.VariableItemObject("expression"));
    }

    variableItemsForm.controls.moveUp.action = function () {
        if (variableItemsForm.controls.itemsContainer.selectedItem) { variableItemsForm.controls.itemsContainer.selectedItem.move("Up"); }
    }

    variableItemsForm.controls.moveDown.action = function () {
        if (variableItemsForm.controls.itemsContainer.selectedItem) { variableItemsForm.controls.itemsContainer.selectedItem.move("Down"); }
    }

    variableItemsForm.controls.selectColumns.action = function () {
        var jsObject = this.jsObject;
        jsObject.InitializeSelectColumnsForVariableForm(function (selectColumnsForm) {
            selectColumnsForm.changeVisibleState(true);

            selectColumnsForm.action = function () {
                selectColumnsForm.changeVisibleState(false);

                jsObject.SendCommandToDesignerServer("GetVariableItemsFromDataColumn", {
                    keysColumn: selectColumnsForm.controls.keys.textBox.value,
                    valuesColumn: selectColumnsForm.controls.values.textBox.value
                }, function (answer) {
                    variableItemsForm.controls.itemsContainer.clear();
                    if (answer.items) {
                        for (var i = 0; i < answer.items.length; i++) {
                            variableItemsForm.controls.itemsContainer.addItem(jsObject.VariableItemObject("value", answer.items[i]["key"], answer.items[i]["value"]));
                        }
                    }
                });
            }
        });
    }

    var innerTable = this.CreateHTMLTable();
    variableItemsForm.container.appendChild(innerTable);

    //Items Container
    variableItemsForm.controls.itemsContainer = this.VariableItemsFormItemsContainer();
    innerTable.addCell(variableItemsForm.controls.itemsContainer).className = "stiDesignerVariableItemsFormControlsLeftCell stiDesignerClearAllStyles";

    var controlsTable = this.CreateHTMLTable();
    controlsTable.style.margin = "4px 0 4px 0";
    innerTable.addCell(controlsTable).className = "stiDesignerVariableItemsFormControlsRightCell stiDesignerClearAllStyles";

    var controlProps = [
        ["key", this.loc.PropertyMain.Key + ":", 150, "TextBox"],
        ["keyFrom", this.loc.PropertyMain.RangeFrom + ":", 150, "TextBox"],
        ["keyTo", this.loc.PropertyMain.RangeTo + ":", 150, "TextBox"],
        ["keyBool", this.loc.PropertyMain.Key + ":", 150, "DropDownList", this.GetBoolItems()],
        ["keyDateTime", this.loc.PropertyMain.Key + ":", 150, "DateControl"],
        ["keyFromDateTime", this.loc.PropertyMain.RangeFrom + ":", 150, "DateControl"],
        ["keyToDateTime", this.loc.PropertyMain.RangeTo + ":", 150, "DateControl"],
        ["value", this.loc.PropertyMain.Value + ":", 150, "TextBox"]
    ]
    variableItemsForm.controlProps = controlProps;

    for (var i = 0; i < controlProps.length; i++) {
        variableItemsForm.controls[controlProps[i][0] + "Row"] = controlsTable.addRow();
        var textControl = controlsTable.addCellInLastRow();
        textControl.className = "stiDesignerCaptionControlsBigIntervals";
        textControl.innerHTML = controlProps[i][1];
        var control;
        if (controlProps[i][3] == "DropDownList") control = this.DropDownList("editVariableForm" + controlProps[i][0], controlProps[i][2], null, controlProps[i][4], true);
        if (controlProps[i][3] == "TextBox") control = this.TextBox("editVariableForm" + controlProps[i][0], controlProps[i][2]);
        if (controlProps[i][3] == "DateControl") control = this.DateControl("editVariableForm" + controlProps[i][0], controlProps[i][2]);

        control.variableItemsForm = variableItemsForm;
        control.shortName = controlProps[i][0];
        variableItemsForm.controls[controlProps[i][0]] = control;
        controlsTable.addCellInLastRow(control).className = "stiDesignerControlCellsBigIntervals";

        control.action = function () {
            var itemObjectPropName = this.shortName == "value" ? "value" : (this.shortName.indexOf("keyTo") != -1 ? "keyTo" : "key");
            var value = this.shortName == "keyBool" ? this.key : (this.shortName.indexOf("DateTime") != -1 ? this.jsObject.DateToStringAmericanFormat(this.key) : this.value);
            variableItemsForm.controls.itemsContainer.selectedItem.itemObject[itemObjectPropName] = Base64.encode(value);
            variableItemsForm.controls.itemsContainer.selectedItem.repaint();
        }
    }

    variableItemsForm.onshow = function () {
        var editVariableForm = this.jsObject.options.forms.editVariableForm;
        this.controls.itemsContainer.addItems(this.jsObject.CopyObject(editVariableForm.variable.items));
    }

    variableItemsForm.action = function () {
        var editVariableForm = this.jsObject.options.forms.editVariableForm;
        editVariableForm.variable.items = this.controls.itemsContainer.getItems();
        editVariableForm.controls.items.textArea.value = editVariableForm.getItemsStr();
        this.changeVisibleState(false);
    }

    variableItemsForm.showAndFillControlsByType = function (selectedItem) {
        var editVariableForm = this.jsObject.options.forms.editVariableForm;
        var variable = editVariableForm.variable;
        var type = variable.type;
        var basicType = variable.basicType;
        var itemObject = null;
        var itemType = null;
        if (selectedItem != null) {
            itemObject = selectedItem.itemObject;
            itemType = selectedItem.itemObject.type;
        }
        var showStringTextBoxes = (type != "datetime" && type != "bool") || itemType == "expression";
        for (var i = 0; i < this.controlProps.length; i++) {
            this.controls[this.controlProps[i][0]].setEnabled(itemObject != null);
            if ("setKey" in this.controls[this.controlProps[i][0]]) this.controls[this.controlProps[i][0]].setKey("");
            if (this.controls[this.controlProps[i][0]]["value"]) this.controls[this.controlProps[i][0]].value = "";
        }

        this.controls.keyRow.style.display = (basicType != "Range" && showStringTextBoxes) ? "" : "none";
        this.controls.keyFromRow.style.display = (basicType == "Range" && showStringTextBoxes) ? "" : "none";
        this.controls.keyToRow.style.display = (basicType == "Range" && showStringTextBoxes) ? "" : "none";
        this.controls.keyBoolRow.style.display = (type == "bool" && itemType != "expression") ? "" : "none";
        this.controls.keyDateTimeRow.style.display = (basicType != "Range" && type == "datetime" && itemType != "expression") ? "" : "none";
        this.controls.keyFromDateTimeRow.style.display = (basicType == "Range" && type == "datetime" && itemType != "expression") ? "" : "none";
        this.controls.keyToDateTimeRow.style.display = (basicType == "Range" && type == "datetime" && itemType != "expression") ? "" : "none";

        if (itemObject != null) {
            var key = itemObject.key != null ? Base64.decode(itemObject.key) : null;
            var keyTo = itemObject.keyTo != null ? Base64.decode(itemObject.keyTo) : null;
            var value = Base64.decode(itemObject.value || "");

            if (this.controls.keyRow.style.display == "") this.controls.key.value = key;
            if (this.controls.keyFromRow.style.display == "") this.controls.keyFrom.value = key;
            if (this.controls.keyToRow.style.display == "") this.controls.keyTo.value = keyTo;
            if (this.controls.keyBoolRow.style.display == "") this.controls.keyBool.setKey(key);
            if (this.controls.keyDateTimeRow.style.display == "") this.controls.keyDateTime.setKey(new Date(key));
            if (this.controls.keyFromDateTimeRow.style.display == "") this.controls.keyFromDateTime.setKey(new Date(key));
            if (this.controls.keyToDateTimeRow.style.display == "") this.controls.keyToDateTime.setKey(new Date(keyTo));
            if (this.controls.valueRow.style.display == "") this.controls.value.value = value;
        }
    }

    return variableItemsForm;
}

StiMobileDesigner.prototype.VariableItemsFormItemsContainer = function () {
    var container = this.Container("variableItemsFormItemsContainer", 250, 200);
    container.className = "stiDesignerVariableItemsFormContainer";

    //Override methods
    container.addItems = function (items) {
        this.clear();
        if (!items) return;
        var firstItem = null;
        for (var i = 0; i < items.length; i++) {
            var item = this.addItem(items[i]);
            if (firstItem == null) firstItem = item;
        }
        if (firstItem) firstItem.selected();
    }

    container.addItem = function (itemObject) {
        var editVariableForm = this.jsObject.options.forms.editVariableForm;
        var item = this.jsObject.DinamicStandartSmallButton(null, null, editVariableForm.getItemCaption(itemObject), this.getItemImageName(itemObject));
        item.itemObject = itemObject;
        item.container = this;
        this.appendChild(item);

        item.action = function () {
            this.selected();
        }

        item.remove = function () {
            this.container.removeChild(this);
            this.container.selectedItem = null;
            if (this.container.childNodes.length > 0) this.container.childNodes[0].selected();
            else this.container.onChange();
        }

        item.selected = function () {
            if (this.container.selectedItem) { this.container.selectedItem.setSelected(false); }
            this.setSelected(true);
            this.container.selectedItem = this;
            this.container.onChange();
        }

        item.repaint = function () {
            this.caption.innerHTML = this.jsObject.options.forms.editVariableForm.getItemCaption(this.itemObject);
        }

        item.getIndex = function () {
            for (var i = 0; i < container.childNodes.length; i++)
                if (container.childNodes[i] == this) return i;
        };

        item.move = function (direction) {
            var index = this.getIndex();
            container.removeChild(this);
            var count = container.getCountItems();
            var newIndex = direction == "Up" ? index - 1 : index + 1;
            if (direction == "Up" && newIndex == -1) newIndex = 0;
            if (direction == "Down" && newIndex >= count) {
                container.appendChild(this);
                container.onChange();
                return;
            }
            container.insertBefore(this, container.childNodes[newIndex]);
            container.onChange();
        }

        item.selected();

        return item;
    }

    container.getCountItems = function () {
        return container.childNodes.length;
    }

    container.getItems = function () {
        var items = [];
        for (var i = 0; i < this.childNodes.length; i++) {
            if (this.childNodes[i].itemObject)
                items.push(this.childNodes[i].itemObject);
        }

        return items.length > 0 ? items : null;
    }

    container.getItemImageName = function (itemObject) {
        if (itemObject.type == "expression") return "Expression.png";
        return "Variable.png";
    }

    container.onChange = function () {
        var variableItemsForm = this.jsObject.options.forms.variableItemsForm;
        variableItemsForm.controls.removeItem.setEnabled(this.selectedItem != null);
        variableItemsForm.showAndFillControlsByType(this.selectedItem);
        var count = this.getCountItems();
        var index = this.selectedItem ? this.selectedItem.getIndex() : -1;
        variableItemsForm.controls.moveUp.setEnabled(index > 0);
        variableItemsForm.controls.moveDown.setEnabled(index != -1 && index < count - 1);
    };

    return container;
}

StiMobileDesigner.prototype.GetVariableItemDefaultKey = function (type) {
    if (type == "datetime") return this.DateToStringAmericanFormat(new Date());
    if (type == "timespan") return "00:00:00";
    if (type == "bool") return "False";
    if (type == "string" || type == "char") return "";
    if (type == "guid") return this.newGuid();

    return "0";
}

StiMobileDesigner.prototype.VariableItemObject = function (typeItem, key, value) {
    var editVariableForm = this.options.forms.editVariableForm;
    var defaultKey = Base64.encode(key != null ? key.toString() : this.GetVariableItemDefaultKey(editVariableForm.controls.type.key));
    var defaultValue = Base64.encode(value != null ? value.toString() : "");

    var variableItemObject = {
        "type": typeItem,
        "key": typeItem == "expression" ? "" : defaultKey,
        "keyTo": editVariableForm.controls.basicType.key == "Range" ? (typeItem == "expression" ? "" : defaultKey) : null,
        "value": defaultValue,
        "valueBinding": null
    }

    return variableItemObject;
}

