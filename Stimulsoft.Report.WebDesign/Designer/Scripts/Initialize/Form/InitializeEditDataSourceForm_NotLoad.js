﻿
StiMobileDesigner.prototype.InitializeEditDataSourceForm_ = function () {
    //Edit DataSource Form

    var editDataSourceForm = this.BaseForm("editDataSourceForm", this.loc.PropertyMain.DataSource, 3, this.HelpLinks["dataSourceEdit"]);
    editDataSourceForm.datasource = null;
    editDataSourceForm.mode = "Edit";

    var saveCopyButton = this.FormButton(null, null, this.loc.Buttons.SaveCopy, null);
    saveCopyButton.style.display = "inline-block";
    saveCopyButton.style.margin = "8px";

    saveCopyButton.action = function () {
        if (editDataSourceForm.nameControl.value == editDataSourceForm.aliasControl.value) {
            editDataSourceForm.aliasControl.value += "Copy";
        }
        editDataSourceForm.nameControl.value += "Copy";

        var resultName = editDataSourceForm.nameControl.value;
        var i = 2;
        while (!(editDataSourceForm.nameControl.checkExists(this.jsObject.GetDataSourcesFromDictionary(this.jsObject.options.report.dictionary), "name") &&
            editDataSourceForm.nameControl.checkExists(this.jsObject.GetVariablesFromDictionary(this.jsObject.options.report.dictionary), "name"))) {
            editDataSourceForm.nameControl.value = editDataSourceForm.aliasControl.value = resultName + i;
            i++;
        }

        editDataSourceForm.mode = "New";
        editDataSourceForm.action();
    }

    var footerTable = this.CreateHTMLTable();
    footerTable.style.width = "100%";
    var buttonsPanel = editDataSourceForm.buttonsPanel;
    editDataSourceForm.removeChild(buttonsPanel);
    editDataSourceForm.appendChild(footerTable);
    footerTable.addCell(saveCopyButton).style.width = "1px";
    footerTable.addCell();
    footerTable.addCell(editDataSourceForm.buttonOk).style.width = "1px";
    footerTable.addCell(editDataSourceForm.buttonCancel).style.width = "1px";

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px 0 5px 0";
    editDataSourceForm.container.appendChild(innerTable);

    var textBoxes = [
        ["nameInSource", this.loc.PropertyMain.NameInSource],
        ["category", this.loc.PropertyMain.Category],
        ["name", this.loc.PropertyMain.Name],
        ["alias", this.loc.PropertyMain.Alias]
    ]

    for (var i = 0; i < textBoxes.length; i++) {
        editDataSourceForm[textBoxes[i][0] + "ControlRow"] = innerTable.addRow();
        var text = innerTable.addCellInLastRow();
        text.className = "stiDesignerCaptionControlsBigIntervals";
        text.innerHTML = textBoxes[i][1] + ":";
        var textBox = (textBoxes[i][0] == "nameInSource")
            ? this.TextBoxWithEditButton("editDataSourceForm" + textBoxes[i][0], 300)
            : this.TextBox("editDataSourceForm" + textBoxes[i][0], 230);
        editDataSourceForm[textBoxes[i][0] + "Control"] = textBox;
        innerTable.addCellInLastRow(textBox).className = "stiDesignerControlCellsBigIntervals";
    }

    editDataSourceForm.nameInSourceControl.button.action = function () {
        var editDataSourceForm = this.jsObject.options.forms.editDataSourceForm;
        this.jsObject.SendCommandGetAllConnections(editDataSourceForm.datasource["typeDataAdapter"]);
    }

    //Query Group
    var buttons = [
        ["runQueryScript", null, "Query.RunQuery.png", this.loc.FormDictionaryDesigner.Run],
        ["editQueryScript", null, "Query.SQL.png", this.loc.FormDictionaryDesigner.EditQuery],
        ["viewData", null, "Query.ViewData.png", this.loc.FormDictionaryDesigner.ViewData]
    ]

    editDataSourceForm.queryPanel = document.createElement("div");
    editDataSourceForm.container.appendChild(editDataSourceForm.queryPanel);
    editDataSourceForm.queryToolBar = this.CreateHTMLTable();
    editDataSourceForm.queryToolBar.style.margin = "4px 6px 4px 6px";
    editDataSourceForm.queryPanel.appendChild(this.FormSeparator());
    editDataSourceForm.queryPanel.appendChild(editDataSourceForm.queryToolBar);
    editDataSourceForm.queryToolBarSeparator = this.FormSeparator();
    editDataSourceForm.queryPanel.appendChild(editDataSourceForm.queryToolBarSeparator);
    for (var i = 0; i < buttons.length; i++) {
        var button = this.SmallButton(null, null, buttons[i][1], buttons[i][2], buttons[i][3], null, this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "1px";
        editDataSourceForm.queryToolBar[buttons[i][0]] = button;
        editDataSourceForm.queryToolBar.addCell(button);
    }

    //Query Text
    editDataSourceForm.queryTextGroup = this.GroupPanel(this.loc.FormDictionaryDesigner.QueryText + ":");
    editDataSourceForm.queryTextGroup.style.border = "0px";
    editDataSourceForm.queryTextGroup.style.marginTop = "5px";
    editDataSourceForm.queryPanel.appendChild(editDataSourceForm.queryTextGroup);
    editDataSourceForm.queryTextControl = this.TextArea("editDataSourceFormQueryTextControl", 600, 130);
    editDataSourceForm.queryTextControl.style.margin = "2px 4px 2px 4px";
    editDataSourceForm.queryTextGroup.container.appendChild(editDataSourceForm.queryTextControl);

    var sqlPropertiesTable = this.CreateHTMLTable();
    sqlPropertiesTable.style.margin = "4px 0 6px 0";
    editDataSourceForm.queryTextGroup.container.appendChild(sqlPropertiesTable);

    //Type
    sqlPropertiesTable.addTextCell(this.loc.PropertyMain.Type + ":").className = "stiDesignerCaptionControlsBigIntervals";    
    editDataSourceForm.queryTextTypeControl = this.DropDownList("editDataSourceFormQueryTextType", 160, null, this.GetQueryTextTypeItems(), true);
    sqlPropertiesTable.addCell(editDataSourceForm.queryTextTypeControl).className = "stiDesignerControlCellsBigIntervals";
    editDataSourceForm.queryTextTypeControl.setKey("Table");

    //Command Timeout
    sqlPropertiesTable.addTextCellInNextRow(this.loc.FormDictionaryDesigner.QueryTimeout + ":").className = "stiDesignerCaptionControlsBigIntervals";
    editDataSourceForm.commandTimeoutControl = this.TextBoxEnumerator("editDataSourceFormCommandTimeout", 160, null, null, 10000000, 1);
    sqlPropertiesTable.addCellInLastRow(editDataSourceForm.commandTimeoutControl).className = "stiDesignerControlCellsBigIntervals";
    editDataSourceForm.commandTimeoutControl.setValue(30);

    //editDataSourceForm.commandTimeoutControl.action = function () {
    //    if (!this.jsObject.options.jsMode && this.getValue() > this.jsObject.options.requestTimeout) {
    //        var form = this.jsObject.InitializeMessageFormForChangeRequestTimeout();
    //        form.show(this.jsObject.options.requestTimeout, this.getValue());
    //        form.action = function (state) {
    //            this.changeVisibleState(false);
    //        }
    //    }
    //}

    //ReconnectOnEachRow
    sqlPropertiesTable.addTextCellInNextRow(this.loc.PropertyMain.ReconnectOnEachRow + ":").className = "stiDesignerCaptionControlsBigIntervals";
    editDataSourceForm.reconnectOnEachRowControl = this.CheckBox("editDataSourceFormReconnectOnEachRow", null);
    sqlPropertiesTable.addCellInLastRow(editDataSourceForm.reconnectOnEachRowControl).className = "stiDesignerControlCellsBigIntervals";
    editDataSourceForm.reconnectOnEachRowControl.setChecked(false);

    editDataSourceForm.getFormParameters = function () {
        var formParameters = {
            nameInSource: editDataSourceForm.nameInSourceControl.textBox.value,
            name: editDataSourceForm.nameControl.value,
            alias: editDataSourceForm.aliasControl.value,
            sqlCommand: Base64.encode(editDataSourceForm.queryTextControl.value),
            commandTimeout: editDataSourceForm.commandTimeoutControl.textBox.value,
            type: editDataSourceForm.queryTextTypeControl.key,
            typeDataAdapter: editDataSourceForm.datasource.typeDataAdapter,
            mode: editDataSourceForm.mode,
            typeItem: editDataSourceForm.datasource.typeItem,
            columns: [],
            parameters: []
        }
        for (var key in editDataSourceForm.columnsAndParametersTree.columnsItem.childs) formParameters.columns.push(editDataSourceForm.columnsAndParametersTree.columnsItem.childs[key].itemObject);
        for (var key in editDataSourceForm.columnsAndParametersTree.parametersItem.childs) formParameters.parameters.push(editDataSourceForm.columnsAndParametersTree.parametersItem.childs[key].itemObject);

        return formParameters;
    }

    //edit query button
    editDataSourceForm.queryToolBar.editQueryScript.action = function () {
        this.jsObject.InitializeTextEditorFormOnlyText(function (textEditorOnlyText) {
            textEditorOnlyText.controlTextBox = editDataSourceForm.queryTextControl;

            textEditorOnlyText.showFunction = function () {
                this.textArea.value = editDataSourceForm.queryTextControl.value;
            }

            textEditorOnlyText.actionFunction = function () {
                editDataSourceForm.queryTextControl.value = this.textArea.value;
            }

            textEditorOnlyText.changeVisibleState(true);
        });
    }

    //run query button
    editDataSourceForm.queryToolBar.runQueryScript.action = function () {
        editDataSourceForm.checkParametersValuesAndShowValuesForm(function (params) { editDataSourceForm.jsObject.SendCommandRunQueryScript(params); });
    }

    //view data button
    editDataSourceForm.queryToolBar.viewData.action = function () {
        editDataSourceForm.checkParametersValuesAndShowValuesForm(function (params) { editDataSourceForm.jsObject.SendCommandViewData(params); });
    }

    //Columns Group
    var buttons = [
        ["columnNew", null, "ColumnNew.png", this.loc.FormDictionaryDesigner.ColumnNew],
        ["calculatedColumnNew", null, "CalcColumnNew.png", this.loc.FormDictionaryDesigner.CalcColumnNew],
        ["parameterNew", null, "ParameterNew.png", this.loc.FormDictionaryDesigner.DataParameterNew],
        ["removeItem", null, "Remove.png", this.loc.MainMenu.menuEditDelete.replace("&", "")],
        ["viewQuery", null, "Query.ViewData.png", this.loc.FormDictionaryDesigner.ViewData],
        ["retrieveColumns", this.loc.FormDictionaryDesigner.RetrieveColumns, "RetrieveColumnsArrow.png", null],
        ["retrieveColumnsAndParameters", null, "RetrieveColumns.png", null]
    ]

    editDataSourceForm.columnToolBar = this.CreateHTMLTable();
    editDataSourceForm.columnToolBar.style.margin = "4px 6px 4px 6px";
    editDataSourceForm.container.appendChild(this.FormSeparator());
    editDataSourceForm.container.appendChild(editDataSourceForm.columnToolBar);
    editDataSourceForm.container.appendChild(this.FormSeparator());
    for (var i = 0; i < buttons.length; i++) {
        var button = this.SmallButton(buttons[i][0], null, buttons[i][1], buttons[i][2], buttons[i][3],
            buttons[i][0] == "retrieveColumnsAndParameters" ? "Down" : null, this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "1px";
        editDataSourceForm.columnToolBar[buttons[i][0]] = button;
        editDataSourceForm.columnToolBar.addCell(button);
        if (buttons[i][0] == "removeItem") editDataSourceForm.columnToolBar.addCell(this.HomePanelSeparator());
    }
    
    var columnsAndParametersMenu = this.ColumnsAndParametersMenu();
    editDataSourceForm.columnToolBar.retrieveColumnsAndParameters.action = function () {
        columnsAndParametersMenu.changeVisibleState(!columnsAndParametersMenu.visible);
    }
    if (this.options.isJava){
    	editDataSourceForm.columnToolBar.retrieveColumnsAndParameters.style.opacity = 0;
    }

    columnsAndParametersMenu.onshow = function () {
        this.items["retrieveParameters"].setEnabled(editDataSourceForm.queryTextTypeControl.key == "StoredProcedure");
    }

    editDataSourceForm.columnToolBar.removeItem.action = function () {
        var columnsAndParametersTree = editDataSourceForm.columnsAndParametersTree;
        if (columnsAndParametersTree.selectedItem) {
            if (columnsAndParametersTree.selectedItem == columnsAndParametersTree.columnsItem ||
                columnsAndParametersTree.selectedItem == columnsAndParametersTree.parametersItem) {
                columnsAndParametersTree.selectedItem.removeAllChilds();
            }
            else {
                editDataSourceForm.columnsAndParametersTree.selectedItem.remove();
            }
        }
    }

    editDataSourceForm.columnToolBar.columnNew.action = function () {
        var newColumn = this.jsObject.ColumnObject(false, editDataSourceForm.columnsAndParametersTree.getItemObjects("Column"));
        editDataSourceForm.columnsAndParametersTree.addItem(newColumn, true);
    }

    editDataSourceForm.columnToolBar.calculatedColumnNew.action = function () {
        var newColumn = this.jsObject.ColumnObject(true, editDataSourceForm.columnsAndParametersTree.getItemObjects("Column"));
        editDataSourceForm.columnsAndParametersTree.addItem(newColumn, true);
    }

    editDataSourceForm.columnToolBar.parameterNew.action = function () {
        var newParameter = this.jsObject.ParameterObject(editDataSourceForm.columnsAndParametersTree.getItemObjects("Parameter"));
        if (editDataSourceForm.datasource && editDataSourceForm.datasource.parameterTypes) {
            for (var i = 0; i < editDataSourceForm.datasource.parameterTypes.length; i++) {
                if (i == 0) newParameter.type = editDataSourceForm.datasource.parameterTypes[0].typeValue.toString();
                if (editDataSourceForm.datasource.parameterTypes[i].typeName == "Text") {
                    newParameter.type = editDataSourceForm.datasource.parameterTypes[i].typeValue.toString();
                    break;
                };
            }
        }
        editDataSourceForm.columnsAndParametersTree.addItem(newParameter, true);
    }

    editDataSourceForm.columnToolBar.retrieveColumns.action = function () {
        editDataSourceForm.checkParametersValuesAndShowValuesForm(function (params) {
            if (columnsAndParametersMenu.retrieveColumnsAllowRun.isChecked) params.retrieveColumnsAllowRun = true;
            editDataSourceForm.jsObject.SendCommandRetrieveColumns(params);
        });
    }

    editDataSourceForm.columnToolBar.viewQuery.action = function () {
        this.jsObject.SendCommandViewData(editDataSourceForm.getFormParameters());
    }

    editDataSourceForm.checkParametersValuesAndShowValuesForm = function (completeFunction) {
        var formParameters = editDataSourceForm.getFormParameters();
        var jsObject = this.jsObject;

        this.jsObject.SendCommandGetParamsFromQueryString(Base64.encode(this.queryTextControl.value), this.datasource.name, function (variablesParams) {

            if (jsObject.GetCountObjects(editDataSourceForm.columnsAndParametersTree.parametersItem.childs) > 0 || (variablesParams && variablesParams.length > 0)) {
                jsObject.InitializeParametersValuesForm(function (parametersValuesForm) {
                    parametersValuesForm.show(formParameters, variablesParams, completeFunction);
                });
            }
            else
                completeFunction(formParameters);
        })
    }
    
    editDataSourceForm.columnToolBar.columnNew.setEnabled(this.options.permissionDataColumns.indexOf("All") >= 0 || this.options.permissionDataColumns.indexOf("Create") >= 0);
    editDataSourceForm.columnToolBar.parameterNew.setEnabled(this.options.permissionSqlParameters.indexOf("All") >= 0 || this.options.permissionSqlParameters.indexOf("Create") >= 0);
    editDataSourceForm.columnToolBar.calculatedColumnNew.setEnabled(this.options.permissionDataColumns.indexOf("All") >= 0 || this.options.permissionDataColumns.indexOf("Create") >= 0);

    //Columns And Parameters Tree
    var columnsContainerTable = this.CreateHTMLTable();
    columnsContainerTable.style.marginRight = "6px";
    editDataSourceForm.container.appendChild(columnsContainerTable);
    editDataSourceForm.columnsAndParametersTree = this.ColumnsAndParametersTree(editDataSourceForm, 250, 150);
    columnsContainerTable.addCell(editDataSourceForm.columnsAndParametersTree);

    editDataSourceForm.columnsContainerEditControlsTable = this.CreateHTMLTable();
    columnsContainerTable.addCell(editDataSourceForm.columnsContainerEditControlsTable).style.verticalAlign = "top";
    var controlsColumnsContainer = [
        ["nameInSource", this.loc.PropertyMain.NameInSource],
        ["name", this.loc.PropertyMain.Name],
        ["alias", this.loc.PropertyMain.Alias],
        ["expression", this.loc.PropertyMain.Expression],
        ["size", this.loc.PropertyMain.Size],
        ["type", this.loc.PropertyMain.Type]
    ]
    editDataSourceForm.columnsContainerEditControlsTable.editDataSourceForm = editDataSourceForm;
    editDataSourceForm.columnsContainerEditControlsTable.style.marginTop = "4px";
    editDataSourceForm.columnsContainerEditControlsTable.style.minWidth = "350px";

    for (var i = 0; i < controlsColumnsContainer.length; i++) {
        editDataSourceForm[controlsColumnsContainer[i][0] + "ControlRowEditColumn"] = editDataSourceForm.columnsContainerEditControlsTable.addRow();
        var text = editDataSourceForm.columnsContainerEditControlsTable.addCellInLastRow();
        text.className = "stiDesignerCaptionControls";
        text.innerHTML = controlsColumnsContainer[i][1] + ":";
        var control = controlsColumnsContainer[i][0] == "type"
            ? this.DropDownList("editDataSourceForm" + controlsColumnsContainer[i][0] + "ControlEditColumn", 130, null, null, true)
            : this.TextBox("editDataSourceForm" + controlsColumnsContainer[i][0] + "ControlEditColumn", 130);
        editDataSourceForm[controlsColumnsContainer[i][0] + "ControlEditColumn"] = control;
        editDataSourceForm.columnsContainerEditControlsTable.addCellInLastRow(control).className = "stiDesignerControlCells";
        control.propertyName = controlsColumnsContainer[i][0];
        control.allwaysEnabled = true;

        control.action = function () {
            var editDataSourceForm = this.jsObject.options.forms.editDataSourceForm;
            var selectedItem = editDataSourceForm.columnsAndParametersTree.selectedItem;
            if (!selectedItem) return;
            selectedItem.itemObject[this.propertyName] = (this.propertyName) != "type"
                ? (this.propertyName != "expression" ? this.value : Base64.encode(this.value))
                : this.key;
            selectedItem.repaint();
        }
    }

    editDataSourceForm.nameControl.action = function () {
        if (this.oldValue == editDataSourceForm.aliasControl.value) {
            editDataSourceForm.aliasControl.value = this.value;
        }
    }

    editDataSourceForm.typeControlEditColumn.menu.innerContent.style.maxHeight = "200px";

    editDataSourceForm.onshow = function () {
        this.mode = "Edit";
        if (typeof (this.datasource) == "string") {
            if (this.datasource == "BusinessObject") {
                this.datasource = this.jsObject.BusinessObject();
            }
            else {
                this.datasource = this.jsObject.DataSourceObject(this.datasource, this.nameInSource);
            }
            this.mode = "New";
        }
        saveCopyButton.style.display = this.mode == "Edit" ? "" : "none";
        this.editableDictionaryItem = this.mode == "Edit" && this.jsObject.options.dictionaryTree
            ? this.jsObject.options.dictionaryTree.selectedItem : null;
        this.nameControl.hideError();
        this.nameControl.focus();
        this.nameInSource = null;
        var caption = (this.mode == "New" && this.datasource.typeItem == "BusinessObject")
                ? this.jsObject.loc.FormDictionaryDesigner.NewBusinessObject
                : this.jsObject.loc.FormDictionaryDesigner[(this.datasource.typeItem == "DataSource" ? "DataSource" : "BusinessObject") + this.mode];

        if (caption) this.caption.innerHTML = caption;
        var props = ["nameInSource", "category", "name", "alias"];
        for (var i = 0; i < props.length; i++) {
            this[props[i] + "ControlRow"].style.display = this.datasource[props[i]] != null ? "" : "none";
            if (this.datasource[props[i]] != null)
                if (props[i] == "nameInSource") this[props[i] + "Control"].textBox.value = this.datasource[props[i]];
                else this[props[i] + "Control"].value = this.datasource[props[i]];
        }
        this.queryPanel.style.display = (this.datasource["sqlCommand"] != null && this.datasource["type"] != null) ? "" : "none";
        this.queryToolBar.style.display = this.datasource.typeDataSource != "StiMongoDbSource" ? "" : "none";
        this.queryToolBarSeparator.style.display = this.datasource.typeDataSource != "StiMongoDbSource" ? "" : "none";
        this.columnToolBar.retrieveColumns.style.display = this.datasource.typeItem == "DataSource" ? "" : "none";
        this.columnToolBar.retrieveColumnsAndParameters.style.display = this.datasource.parameterTypes && !this.jsObject.options.jsMode ? "" : "none";
        this.columnToolBar.viewQuery.style.display = this.datasource.typeDataSource == "StiDataTableSource" ? "" : "none";        
        this.columnToolBar.parameterNew.style.display = this.datasource.parameterTypes && !this.jsObject.options.jsMode ? "" : "none";
        this.columnToolBar.calculatedColumnNew.style.display = this.datasource.typeDataSource != "StiMongoDbSource" ? "" : "none";
        sqlPropertiesTable.style.display = this.datasource.typeDataSource != "StiMongoDbSource" ? "" : "none";
        if (this.datasource["sqlCommand"] != null) this.queryTextControl.value = Base64.decode(this.datasource.sqlCommand);
        if (this.datasource["type"] != null) this.queryTextTypeControl.setKey(this.datasource.type);
        if (this.datasource["commandTimeout"] != null) this.commandTimeoutControl.setValue(this.datasource.commandTimeout);
        if (this.datasource["reconnectOnEachRow"] != null) this.reconnectOnEachRowControl.setChecked(this.datasource.reconnectOnEachRow);

        this.businessObjectFullName = null;
        if (this.datasource.typeItem == "BusinessObject") {
            this.businessObjectFullName = this.jsObject.options.dictionaryTree.selectedItem.getBusinessObjectFullName();
            this.categoryControl.setEnabled(this.businessObjectFullName == null || this.businessObjectFullName.length <= (this.mode == "Edit" ? 1 : 0));
        }
        else {
            this.categoryControl.setEnabled(true);
        }

        var columns = [];
        var parameters = [];
        if (this.mode == "Edit") {
            var currObject = (this.datasource.typeItem == "DataSource")
            ? this.jsObject.GetDataSourceByNameFromDictionary(this.datasource.name)
            : this.jsObject.GetBusinessObjectByNameFromDictionary(this.businessObjectFullName);
            if (currObject) {
                if (currObject.columns) {
                    columns = currObject.columns;
                    columns.sort(this.jsObject.SortByName);
                }
                if (currObject.parameters) {
                    parameters = currObject.parameters;
                    parameters.sort(this.jsObject.SortByName);
                }
            }
        }
        this.columnsAndParametersTree.addColumnsAndParameters(columns, parameters, true);
        var permissionSqlParameters = this.jsObject.options.permissionSqlParameters;
        this.columnsAndParametersTree.parametersItem.style.display = (permissionSqlParameters.indexOf("All") >= 0 || permissionSqlParameters.indexOf("View") >= 0) &&
            this.datasource.parameterTypes && !this.jsObject.options.jsMode ? "" : "none";
        this.columnsAndParametersTree.onSelectedItem();

        if (this.mode == "New" && this.datasource.sqlCommand != null && !this.jsObject.options.jsMode) {
            this.jsObject.SendCommandGetSqlParameterTypes(this.datasource);
        }
    }

    editDataSourceForm.action = function () {
        this.datasource["mode"] = this.mode;

        if (!this.nameControl.checkNotEmpty(this.jsObject.loc.PropertyMain.Name)) return;
        if ((this.mode == "New" || this.nameControl.value != this.datasource.name) &&
            !(this.nameControl.checkExists(this.jsObject.GetDataSourcesFromDictionary(this.jsObject.options.report.dictionary), "name") &&
                this.nameControl.checkExists(this.jsObject.GetVariablesFromDictionary(this.jsObject.options.report.dictionary), "name")))
            return;

        if (this.mode == "Edit") this.datasource.oldName = this.datasource.name;

        var props = ["nameInSource", "category", "name", "alias"];
        for (var i = 0; i < props.length; i++) {
            if (this.datasource[props[i]] != null)
                if (props[i] == "nameInSource") this.datasource[props[i]] = this[props[i] + "Control"].textBox.value;
                else this.datasource[props[i]] = this[props[i] + "Control"].value;
        }
        if (this.datasource["sqlCommand"] != null) this.datasource.sqlCommand = Base64.encode(this.queryTextControl.value);
        if (this.datasource["type"] != null) this.datasource.type = this.queryTextTypeControl.key;
        if (this.datasource["commandTimeout"] != null) this.datasource.commandTimeout = this.commandTimeoutControl.textBox.value;
        if (this.datasource["reconnectOnEachRow"] != null) this.datasource.reconnectOnEachRow = this.reconnectOnEachRowControl.isChecked;

        var columns = [];
        var parameters = [];
        for (var key in this.columnsAndParametersTree.columnsItem.childs) columns.push(this.columnsAndParametersTree.columnsItem.childs[key].itemObject);
        for (var key in this.columnsAndParametersTree.parametersItem.childs) parameters.push(this.columnsAndParametersTree.parametersItem.childs[key].itemObject);
        this.datasource.columns = columns;
        this.datasource.parameters = parameters;

        this.changeVisibleState(false);
        if (this.datasource.typeItem == "DataSource")
            this.jsObject.SendCommandCreateOrEditDataSource(this.datasource);
        else {
            this.datasource.businessObjectFullName = this.businessObjectFullName;
            this.jsObject.SendCommandCreateOrEditBusinessObject(this.datasource);
        }
    }

    return editDataSourceForm;
}

StiMobileDesigner.prototype.ColumnsAndParametersTree = function (editDataSourceForm, width, height) {
    var tree = this.Tree();
    tree.style.width = width + "px";
    tree.style.height = height + "px";
    tree.className = "stiDesignerDataSourceFormColumnsAndParametersTree";

    tree.addRootItems = function () {
        this.rootItem = this.jsObject.ColumnsAndParametersTreeItem("root", "Connection.png", {}, this, editDataSourceForm);
        this.rootItem.setOpening(true);
        this.rootItem.button.style.display = "none";
        this.rootItem.iconOpening.style.display = "none";
        this.appendChild(this.rootItem);

        var permissionDataColumns = this.jsObject.options.permissionDataColumns;
        var permissionSqlParameters = this.jsObject.options.permissionSqlParameters;

        this.columnsItem = this.jsObject.ColumnsAndParametersTreeItem(this.jsObject.loc.FormPageSetup.Columns, "DataColumn.png", {}, this, editDataSourceForm);
        this.rootItem.addChild(this.columnsItem);
        this.columnsItem.style.display = (permissionDataColumns.indexOf("All") >= 0 || permissionDataColumns.indexOf("View") >= 0) ? "" : "none";

        this.parametersItem = this.jsObject.ColumnsAndParametersTreeItem(this.jsObject.loc.PropertyMain.Parameters, "Parameter.png", {}, this, editDataSourceForm);
        this.rootItem.addChild(this.parametersItem);
        this.parametersItem.style.display = (permissionSqlParameters.indexOf("All") >= 0 || permissionSqlParameters.indexOf("View") >= 0) ? "" : "none";
    }

    tree.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        this.items = {};
        this.selectedItem = null;
        this.addRootItems();
    }

    tree.addItem = function (itemObject, selectedAfter) {
        var parent = itemObject.typeItem == "Column" ? this.columnsItem : this.parametersItem;
        var caption = this.getItemCaption(itemObject);
        var item = this.jsObject.ColumnsAndParametersTreeItem(caption, itemObject.typeIcon + ".png", itemObject, this, editDataSourceForm);
        parent.addChild(item);
        parent.setOpening(true);
        if (selectedAfter) item.setSelected();

        return item;
    }

    tree.addColumnsAndParameters = function (columns, parameters, clearBefore) {
        if (clearBefore) this.clear();
        var firstItem = null;

        for (var i = 0; i < columns.length; i++) {
            var item = this.addItem(columns[i]);
            if (!firstItem) firstItem = item;
        }
        for (var i = 0; i < parameters.length; i++) {
            var item = this.addItem(parameters[i]);
            if (!firstItem) firstItem = item;
        }

        if (firstItem) firstItem.setSelected();
    }

    tree.getItemObjects = function (type) {
        var parent = type == "Column" ? this.columnsItem : this.parametersItem;
        var itemObjects = [];
        for (var key in parent.childs) itemObjects.push(parent.childs[key].itemObject);

        return itemObjects;
    }

    tree.onSelectedItem = function () {
        var enabledRemove = this.selectedItem != null &&
            (((this.selectedItem.itemObject.typeItem == "Column" || this.selectedItem == this.columnsItem) &&
             (this.jsObject.options.permissionDataColumns.indexOf("All") >= 0 || this.jsObject.options.permissionDataColumns.indexOf("Delete") >= 0)) ||
            ((this.selectedItem.itemObject.typeItem == "Parameter" || this.selectedItem == this.parametersItem) &&
             (this.jsObject.options.permissionSqlParameters.indexOf("All") >= 0 || this.jsObject.options.permissionSqlParameters.indexOf("Delete") >= 0)))

        editDataSourceForm.columnToolBar.removeItem.setEnabled(enabledRemove);

        var showEditControls = this.selectedItem && this.selectedItem != this.columnsItem && this.selectedItem != this.parametersItem;

        editDataSourceForm.nameControlRowEditColumn.style.display = showEditControls ? "" : "none";
        editDataSourceForm.nameInSourceControlRowEditColumn.style.display = showEditControls && this.selectedItem.itemObject.typeItem == "Column" ? "" : "none";
        editDataSourceForm.aliasControlRowEditColumn.style.display = showEditControls && this.selectedItem.itemObject.typeItem == "Column" ? "" : "none";
        editDataSourceForm.expressionControlRowEditColumn.style.display = showEditControls &&
            (this.selectedItem.itemObject.typeItem == "Parameter" || this.selectedItem.itemObject.isCalcColumn) ? "" : "none";
        editDataSourceForm.sizeControlRowEditColumn.style.display = showEditControls && this.selectedItem.itemObject.typeItem == "Parameter" ? "" : "none";
        editDataSourceForm.typeControlRowEditColumn.style.display = showEditControls ? "" : "none";

        var enabledEdit = this.selectedItem != null &&
            (((this.selectedItem.itemObject.typeItem == "Column") &&
             (this.jsObject.options.permissionDataColumns.indexOf("All") >= 0 || this.jsObject.options.permissionDataColumns.indexOf("Modify") >= 0)) ||
            ((this.selectedItem.itemObject.typeItem == "Parameter") &&
             (this.jsObject.options.permissionSqlParameters.indexOf("All") >= 0 || this.jsObject.options.permissionSqlParameters.indexOf("Modify") >= 0)));

        if (showEditControls) {
            editDataSourceForm.typeControlEditColumn.addItems(this.selectedItem.itemObject.typeItem == "Column"
            ? this.jsObject.GetColumnTypesItems()
            : this.jsObject.GetParameterTypeItems(editDataSourceForm.datasource.parameterTypes));

            var properties = ["name", "nameInSource", "alias", "expression", "size", "type"];
            for (var i = 0; i < properties.length; i++) {
                if (properties[i] != "type") {
                    editDataSourceForm[properties[i] + "ControlEditColumn"].value =
                    properties[i] != "expression" ? this.selectedItem.itemObject[properties[i]] : Base64.decode(this.selectedItem.itemObject[properties[i]]);
                }
                else editDataSourceForm[properties[i] + "ControlEditColumn"].setKey(this.selectedItem.itemObject[properties[i]]);
                editDataSourceForm[properties[i] + "ControlEditColumn"].setEnabled(enabledEdit);
            }
        }
    };

    tree.getItemCaption = function (itemObject) {
        if (itemObject.alias == null) return itemObject.name;
        return itemObject.name == itemObject.alias ? itemObject.name : itemObject.name + " [" + itemObject.alias + "]";
    }

    tree.addRootItems();
    return tree;
}

StiMobileDesigner.prototype.ColumnsAndParametersTreeItem = function (caption, imageName, itemObject, tree, editDataSourceForm) {
    var treeItem = this.TreeItem(caption, imageName, itemObject, tree);
    treeItem.visible = true;
    treeItem.style.width = "100%";
    treeItem.button.style.width = "100%";
    treeItem.button.imageCell.style.width = "1px";
    treeItem.button.captionCell.style.width = "1px";
    treeItem.button.captionCell.style.whiteSpace = "nowrap";
    treeItem.button.style.border = "0px";
    treeItem.button.style.height = this.options.isTouchDevice ? "28px" : "23px";

    //space
    treeItem.button.addCell().style.width = "100%";

    treeItem.button.onmouseover = function () {
        if (this.jsObject.options.isTouchDevice) return;
        this.className = "stiDesignerTreeItemButtonSelected_Mouse";
    }

    treeItem.button.onmouseout = function () {
        if (this.jsObject.options.isTouchDevice) return;
        this.className = this.treeItem.isSelected ? "stiDesignerTreeItemButtonSelected_Mouse" : "stiDesignerTreeItemButton_Mouse";
    }

    treeItem.setSelected = function () {
        if (this.tree.selectedItem) {
            this.tree.selectedItem.button.className = this.jsObject.options.isTouchDevice ? "stiDesignerTreeItemButton_Touch" : "stiDesignerTreeItemButton_Mouse";
            this.tree.selectedItem.isSelected = false;
        }
        this.button.className = this.jsObject.options.isTouchDevice ? "stiDesignerTreeItemButtonSelected_Touch" : "stiDesignerTreeItemButtonSelected_Mouse";
        this.tree.selectedItem = this;
        this.isSelected = true;
        this.tree.onSelectedItem(this);
    }

    treeItem.repaint = function () {
        if (this.itemObject.typeItem == "Column") {
            var imageName = this.jsObject.GetTypeIcon(this.itemObject.type) + ".png";
            if (this.itemObject.isCalcColumn) imageName = imageName.replace("DataColumn", "CalcColumn");
            this.button.image.src = this.jsObject.options.images[imageName];
        }
        this.button.captionCell.innerHTML = this.tree.getItemCaption(this.itemObject);
    }

    return treeItem;
}