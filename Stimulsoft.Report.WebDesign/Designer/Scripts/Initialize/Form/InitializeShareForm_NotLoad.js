﻿StiMobileDesigner.prototype.InitializeShareForm_ = function () {
    var shareForm = this.BaseForm("shareForm", this.loc.Cloud.ShareWindowTitleNew, 1, this.HelpLinks["share"]);
    shareForm.controls = {};
    shareForm.container.style.padding = "0px";
    shareForm.container.style.width = "580px";
    shareForm.buttonOk.caption.innerHTML = this.loc.Buttons.Save;
    shareForm.progress = this.AddProgressToControl(shareForm.container);

    var jsObject = this;
    var controlsTable = this.CreateHTMLTable();
    shareForm.container.appendChild(controlsTable);
    controlsTable.style.width = "100%";

    shareForm.addControlRow = function (table, textControl, controlName, control, margin) {
        this.controls[controlName] = control;
        this.controls[controlName + "Row"] = table.addRow();

        if (textControl != null) {
            var text = table.addCellInLastRow();
            this.controls[controlName + "Text"] = text;
            text.innerHTML = textControl;
            text.className = "stiDesignerCaptionControls";
            text.style.paddingLeft = "15px";
            text.style.width = "1px";
        }

        if (control) {
            control.form = this;
            control.style.margin = margin;
            var controlCell = table.addCellInLastRow(control);
            if (textControl == null) controlCell.setAttribute("colspan", 2);
        }

        return controlCell;
    }
        
    var buttons = [];
    buttons.push(["sharePrivate", "Share.BigPrivateShare.png", "<b>" + this.loc.Cloud.WizardPrivateShare + "</b><br>" + this.loc.Cloud.WizardPrivateShareDescription]);
    buttons.push(["sharePublic", "Share.BigPublicShare.png", "<b>" + this.loc.Cloud.WizardPublicShare + "</b><br>" + this.loc.Cloud.WizardPublicShareDescription]);

    var buttonsPanel = document.createElement("div");
    buttonsPanel.style.margin = "10px 0px 10px 0px";
    controlsTable.addCellInNextRow(buttonsPanel).colSpan = 2;

    var actionFunction = function () {
        if (!this.isEnabled) return;
        if (this.groupName != null) {
            this.setSelected(true);
        }
        shareForm.shareLevel = this.name.substr(5);
        shareForm.updateControlsStates();
    };

    for (var i = 0; i < buttons.length; i++) {
        var button = jsObject.FlatButton(buttons[i][0], buttons[i][2], buttons[i][1], "shareFormButtons");
        buttonsPanel.appendChild(button);
        buttonsPanel[buttons[i][0]] = button;
        button.action = actionFunction;
    }
    
    var sep1 = this.FormSeparator();
    sep1.style.marginBottom = "4px";
    controlsTable.addCellInNextRow(sep1).setAttribute("colspan", "2");
       
    var controlEndDate = this.DateControlWithCheckBox2(null, 250, null);
    shareForm.addControlRow(controlsTable, this.loc.Cloud.LabelEndDate, "endDate", controlEndDate, "4px 4px 4px 17px").parentNode.firstChild;
    shareForm.controlEndDate = controlEndDate;
    var date = new Date();
    date.setDate(date.getDate() + 1);
    controlEndDate.setKey(date);

    //Result Type
    var resultType = this.DropDownList(null, 266, null, this.GetExportFormatTypesItems(), true, true, false);
    resultType.image.style.width = "16px";
    var resultTypeValue = this.GetCookie("StimulsoftShareFormResultType") || "ReportSnapshot";
    resultType.setKey(resultTypeValue);
    shareForm.addControlRow(controlsTable, this.loc.Cloud.LabelResultType, "resultType", resultType, "4px 4px 4px 40px").parentNode.firstChild;
    
    resultType.action = function () {
        var shareUrl = shareForm.getShareLinkWithExport(shareForm.controls.linkControl.shareUrl);

        shareForm.controls.linkControl.value = shareForm.controls.buttonEmbedCode.isSelected
            ? "<iframe src='" + shareUrl +
            "' style='width:800px; height:600px; overflow:hidden; border: 1px solid gray;'></iframe>"
            : shareUrl;

        jsObject.SendCloudCommand("", { ShareUrl: shareUrl },
            function (data) { if (data.ResultSuccess && data.QRCodeImage) shareForm.controls.qrCodeContainer.src = data.QRCodeImage; },
            function (data) { },
            null,
            jsObject.options.nodeJsMode ? "service/qrcode/" : jsObject.options.cloudParameters.restUrl + "service/qrcode/" + jsObject.generateKey()
        );

        this.jsObject.SetCookie("StimulsoftShareFormResultType", this.key)
    }

    //Separator
    var sep2 = this.FormSeparator();
    sep2.style.marginTop = "4px";
    controlsTable.addCellInNextRow(sep2).setAttribute("colspan", "2");

    //Link & Embed code
    var linkToolbar = this.CreateHTMLTable();
    shareForm.addControlRow(controlsTable, " ", "linkToolbar", linkToolbar, "8px 4px 4px 35px");

    var buttons = ["Share", "EmbedCode", "QRCode"];
    for (var i = 0; i < buttons.length; i++) {
        var button = this.SmallButton(null, null, this.loc.Cloud["TabItem" + buttons[i]], null, null, null, this.GetStyles("StandartTab"));
        button.style.fontSize = "12px";
        button.name = buttons[i];
        shareForm.controls["button" + buttons[i]] = button;
        linkToolbar.addCell(button);

        if (i < buttons.length - 1) {
            var tabSep = document.createElement("div");
            tabSep.className = "stiDesignerTabbedPaneSeparator"
            linkToolbar.addCell(tabSep);
        }

        button.action = function () {
            for (var i = 0; i < buttons.length; i++) { shareForm.controls["button" + buttons[i]].setSelected(false); }
            this.setSelected(true);
            shareForm.controls.linkControl.value = this.name == "EmbedCode"
                ? "<iframe src='" + shareForm.getShareLinkWithExport(shareForm.controls.linkControl.shareUrl) +
                "' style='width:800px; height:600px; overflow:hidden; border: 1px solid gray;'></iframe>"
                : shareForm.getShareLinkWithExport(shareForm.controls.linkControl.shareUrl);
            if (shareForm.controls.copyBtn) shareForm.controls.copyBtn.style.display = this.name != "QRCode" ? "" : "none";
            shareForm.controls.refreshBtn.style.display = this.name == "Share" ? "" : "none";
            shareForm.controls.linkContainer.style.display = this.name != "QRCode" ? "" : "none";
            shareForm.controls.qrCodeContainer.style.display = this.name != "QRCode" ? "none" : "";

            button.jsObject.SetCookie("ShareFormDefaultMode", this.name);
        }
    }
    shareForm.controls.buttonShare.setSelected(true);

    var controlsBlock = document.createElement("div");
    controlsBlock.style.height = "110px";
    shareForm.addControlRow(controlsTable, " ", "controlsBlock", controlsBlock).style.padding = "4px 20px 0 40px";

    //Link
    var linkContainer = this.CreateHTMLTable();
    shareForm.controls.linkContainer = linkContainer;
    linkContainer.style.width = "100%";
    controlsBlock.appendChild(linkContainer);
    var linkControl = this.TextArea(null, null, 90);
    linkControl.style.border = "0px";
    linkControl.readOnly = true;
    linkControl.style.width = "100%";
    shareForm.controls.linkControl = linkControl;
    linkContainer.addCell(linkControl);
    linkContainer.className = "stiResourceContainerWithBorder";

    var buttonsCell = linkContainer.addCell();
    buttonsCell.style.verticalAlign = "top";
    buttonsCell.style.padding = "0 0 0 5px";
    buttonsCell.style.width = "1px";

    var linkButtons = [];
    linkButtons.push(["copyBtn", "Copy.png", this.loc.MainMenu.menuEditCopy.replace("&", "")]);
    linkButtons.push(["refreshBtn", "Share.Refresh.png", this.loc.PropertyMain.Refresh]);

    for (var i = 0; i < linkButtons.length; i++) {
        var button = this.FormButton(null, null, null, linkButtons[i][1], linkButtons[i][2]);
        buttonsCell.appendChild(button);
        shareForm.controls[linkButtons[i][0]] = button;
        button.style.minWidth = "";
        button.style.margin = "3px 3px 0 0";
        button.style.width = "22px";
        button.style.height = "21px";
    }

    if (shareForm.controls.copyBtn) {
        shareForm.controls.copyBtn.action = function () {
            this.jsObject.copyTextToClipboard(shareForm.controls.linkControl.value);
        };
    }

    shareForm.controls.refreshBtn.action = function () {
        if (shareForm.itemKey && shareForm.sessionKey) {
            shareForm.progress.show();
            jsObject.SendCloudCommand("ItemRefreshShareUrl", { ItemKey: shareForm.itemKey, SessionKey: shareForm.sessionKey },
                function (data) {
                    if (data.ResultSuccess && data.ResultUrl) {
                        shareForm.progress.hide();
                        shareForm.controls.linkControl.value = shareForm.getShareLinkWithExport(data.ResultUrl);
                        shareForm.controls.linkControl.shareUrl = data.ResultUrl;

                        jsObject.SendCloudCommand("", { ShareUrl: shareForm.getShareLinkWithExport(data.ResultUrl) },
                            function (data) { if (data.ResultSuccess && data.QRCodeImage) shareForm.controls.qrCodeContainer.src = data.QRCodeImage; },
                            function (data) { },
                            null, 
                            jsObject.options.nodeJsMode ? "service/qrcode/" : jsObject.options.cloudParameters.restUrl + "service/qrcode/" + jsObject.generateKey()
                        );

                        var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                        errorMessageForm.show(jsObject.loc.Messages.ShareURLOfTheItemHasBeenUpdated, "Info");
                    }
                },
                function (data, msg) {
                    shareForm.progress.hide();
                    var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                    errorMessageForm.show(msg);
                });
        }
    };
    
    //QrCode
    var qrCodeContainer = document.createElement("img");
    shareForm.controls.qrCodeContainer = qrCodeContainer;
    qrCodeContainer.style.display = "none";
    controlsBlock.appendChild(qrCodeContainer);

    //Warning compilation mode
    var warningSep = this.FormSeparator();
    shareForm.container.appendChild(warningSep);

    var warningText = this.CreateHTMLTable();
    warningText.className = "stiDesignerTextContainer";
    var img = document.createElement("img");
    img.style.margin = "10 10px 10px 20px";
    img.src = this.options.images["ReportChecker.Warning32.png"];
    warningText.addCell(img).style.width = "1px";
    warningText.addTextCell(this.loc.Messages.RenderingWillOccurInTheInterpretationMode).style.padding = "10px";
    shareForm.container.appendChild(warningText);

    shareForm.updateShareLevel = function () {
        if (this.shareLevel == "Private") buttonsPanel["sharePrivate"].action();
        else if (this.shareLevel == "Public") buttonsPanel["sharePublic"].action();
    }

    shareForm.updateControlsStates = function () {
        controlEndDate.setEnabled(shareForm.shareLevel != "Private");
        resultType.setEnabled(shareForm.shareLevel != "Private");
        shareForm.controls.copyBtn.setEnabled(shareForm.shareLevel != "Private");
        shareForm.controls.refreshBtn.setEnabled(shareForm.shareLevel != "Private");
        shareForm.controls.buttonShare.setEnabled(shareForm.shareLevel != "Private");
        shareForm.controls.buttonEmbedCode.setEnabled(shareForm.shareLevel != "Private");
        shareForm.controls.buttonQRCode.setEnabled(shareForm.shareLevel != "Private");
        linkControl.setEnabled(shareForm.shareLevel != "Private");
    }

    shareForm.getShareLinkWithExport = function (shareLink) {
        if (resultType.key == "ReportSnapshot")
            return shareLink;

        return shareLink + "/" + resultType.key.toLowerCase();
    }

    shareForm.fillShareInfo = function () {
        this.itemKey = jsObject.options.cloudParameters ? jsObject.options.cloudParameters.reportTemplateItemKey : null;

        if (this.itemKey) {
            shareForm.progress.show();
            jsObject.SendCloudCommand("ItemGetShareInfo", { ItemKey: this.itemKey },
                function (data) {
                    shareForm.progress.hide();
                    shareForm.controls.linkControl.shareUrl = data.ResultUrl;
                    shareForm.controls.linkControl.value = shareForm.getShareLinkWithExport(data.ResultUrl);

                    jsObject.SendCloudCommand("", { ShareUrl: shareForm.getShareLinkWithExport(data.ResultUrl) },
                        function (data) { if (data.ResultSuccess && data.QRCodeImage) shareForm.controls.qrCodeContainer.src = data.QRCodeImage; },
                        function (data) { },
                        null, 
                        jsObject.options.nodeJsMode ? "service/qrcode/" : jsObject.options.cloudParameters.restUrl + "service/qrcode/" + jsObject.generateKey()
                    );

                    shareForm.shareLevels.push(data.ResultShareLevel);
                    var shareLevel = shareForm.shareLevels[0];

                    for (var i = 0; i < shareForm.shareLevels.length; i++) {
                        if (shareForm.shareLevels[i] != shareLevel) shareLevel = false;
                    }

                    if (shareLevel) {
                        shareForm.shareLevel = shareLevel;
                        shareForm.updateShareLevel();
                    }

                    if (data.ResultShareExpires) {
                        controlEndDate.setKey(shareForm.jsObject.JSONDateFormatToDate(data.ResultShareExpires));
                        controlEndDate.setChecked(true);
                    } else {
                        controlEndDate.setChecked(false);
                    }
                },
                function (data, msg) {
                    shareForm.progress.hide();
                    var errorMessageForm = jsObject.options.forms.errorMessageForm || jsObject.InitializeErrorMessageForm();
                    errorMessageForm.show(msg);
                });
        }
    }

    shareForm.checkReportSavingToCloud = function () {
        if (jsObject.options.cloudParameters && jsObject.options.cloudParameters.reportTemplateItemKey) {
            shareForm.fillShareInfo();
        }
        else {
            var messageForm = jsObject.MessageFormForSaveReportToCloud();
            messageForm.changeVisibleState(true);
            messageForm.action = function (state) {
                if (state) {
                    var saveAsForm = jsObject.InitializeOnlineSaveAsReportPanel();
                    jsObject.options.mainPanel.appendChild(saveAsForm);
                    saveAsForm.style.display = "";
                    saveAsForm.visible = true;                    
                    saveAsForm.buttonOk.setEnabled(false);
                    saveAsForm.newFolderButton.setEnabled(false);
                    saveAsForm.nameControl.value = jsObject.GetReportFileName();
                    saveAsForm.nameControl.focus();
                    saveAsForm.setToTreeMode();
                    jsObject.SetObjectToCenter(saveAsForm);

                    saveAsForm.cancelAction = function () {
                        shareForm.changeVisibleState(false);
                    }
                }
                else {
                    shareForm.changeVisibleState(false);
                }
            }
            messageForm.cancelAction = function () {
                shareForm.changeVisibleState(false);
            }
        }
    }

    shareForm.show = function () {
        this.changeVisibleState(true);
        shareForm.shareLevels = [];
        shareForm.itemKey = null;
        shareForm.sessionKey = jsObject.options.SessionKey || (jsObject.options.cloudParameters && jsObject.options.cloudParameters.sessionKey);
        warningText.style.display = warningSep.style.display = jsObject.options.report && jsObject.options.report.properties.calculationMode == "Compilation" ? "" : "none";

        if (shareForm.sessionKey) {
            shareForm.checkReportSavingToCloud();
        }
        else {
            jsObject.options.forms.authForm.show();
        }
    }

    shareForm.action = function () {
        if (this.itemKey) {
            var params = {
                ItemKeys: [],
                ShareLevel: this.shareLevel,
                AllowSignalsReturn: true
            }
            params.ItemKeys.push(this.itemKey);

            if (controlEndDate.isChecked && this.shareLevel != "Private") {
                params.ShareExpires = jsObject.DateToJSONDateFormat(controlEndDate.key);
            }
            this.jsObject.SendCloudCommand("ItemSetShareInfo", params, function (data) {});
        }

        shareForm.changeVisibleState(false);
    }

    return shareForm;
}

StiMobileDesigner.prototype.FlatButton = function (name, caption, imageName, groupName) {
    var button = this.SmallButton(name, groupName, caption, imageName, null, null, this.GetStyles("StandartSmallButton"));
    button.style.clear = "both";
    button.style.minHeight = "65px";
    button.innerTable.style.height = "65px";
    button.imageCell.style.padding = "0px 0px 0px 135px";
    button.caption.style.padding = "0px 10px 0px 10px";
    button.caption.style.whiteSpace = "normal";
    button.caption.style.lineHeight = "1.5";

    return button;
}