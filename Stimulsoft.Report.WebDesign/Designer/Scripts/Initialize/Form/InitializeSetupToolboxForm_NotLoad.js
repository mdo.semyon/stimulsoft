﻿
StiMobileDesigner.prototype.InitializeSetupToolboxForm_ = function () {
    var form = this.BaseForm("setupToolboxForm", this.loc.FormDesigner.SetupToolbox, 4);

    //Tree
    var tree = this.Tree();
    tree.style.overflow = "auto";
    tree.style.width = "350px";
    tree.style.height = "400px";
    tree.style.padding = "10px";
    tree.groups = {};
    form.container.appendChild(tree);
    if (!this.options.componentsIntoInsertTab) form.container.appendChild(this.FormSeparator());

    var showToolbox = this.CheckBox(null, this.loc.MainMenu.menuViewShowToolbox);
    showToolbox.style.margin = "10px 100px 10px 15px";
    form.container.appendChild(showToolbox);

    var showInsertTab = this.CheckBox(null, this.loc.MainMenu.menuViewShowInsertTab);
    showInsertTab.style.margin = "10px 100px 10px 15px";    
    form.container.appendChild(showInsertTab);
    
    showToolbox.action = function () {
        if (!showInsertTab.isChecked && !this.isChecked)
            showInsertTab.setChecked(true);
    }

    showInsertTab.action = function () {
        if (!showToolbox.isChecked && !this.isChecked)
            showToolbox.setChecked(true);
    }

    form.buildTree = function () {
        tree.clear();

        var allComponents = {
            bands: {
                image: "SmallComponents.StiReportTitleBand.png",
                items: ["StiReportTitleBand", "StiReportSummaryBand", "StiPageHeaderBand", "StiPageFooterBand", "StiGroupHeaderBand",
                    "StiGroupFooterBand", "StiHeaderBand", "StiFooterBand", "StiColumnHeaderBand", "StiColumnFooterBand", "StiDataBand", "StiHierarchicalBand",
                    "StiChildBand", "StiEmptyBand", "StiOverlayBand"]
            },
            crossBands: {
                image: "SmallComponents.StiCrossGroupHeaderBand.png",
                items: ["StiCrossGroupHeaderBand", "StiCrossGroupFooterBand", "StiCrossHeaderBand", "StiCrossFooterBand", "StiCrossDataBand"]
            },
            components: {
                image: "SmallComponents.StiText.png",
                items: ["StiText", "StiTextInCells", "StiRichText", "StiImage", "StiBarCode", "StiPanel", "StiClone", "StiCheckBox",
                    "StiSubReport", "StiZipCode", "StiTable", "StiCrossTab"]
            },
            shapes: {
                image: "SmallComponents.StiShape.png",
                items: ["StiShape", "StiHorizontalLinePrimitive", "StiVerticalLinePrimitive", "StiRectanglePrimitive", "StiRoundedRectanglePrimitive"]
            },
            infographics: {
                image: "SmallComponents.StiChart.png",
                items: ["StiChart"/*, "StiMap"*/]
            },
            dashboards: {
                image: "SmallComponents.StiDashboard.png",
                text: this.jsObject.loc.Permissions.ItemDashboards,
                items: ["StiTableElement", "StiChartElement", "StiGaugeElement", "StiPivotElement", "StiIndicatorElement", "StiProgressElement", "StiMapElement",
                    "StiImageElement", "StiTextElement", "StiPanelElement", "StiShapeElement"]
            }
        }

        //if (this.jsObject.options.dashboardAssemblyLoaded) {
        //    allComponents.dashboards = {
        //        image: "SmallComponents.StiDashboard.png",
        //        text: this.jsObject.loc.Permissions.ItemDashboards,
        //        items: ["StiTableElement", "StiChartElement", "StiGaugeElement", "StiPivotElement", "StiIndicatorElement", "StiProgressElement", "StiMapElement",
        //            "StiImageElement", "StiTextElement", "StiPanelElement", "StiShapeElement"]
        //    }
        //}

        var selectedComponents = this.jsObject.GetComponentsIntoInsertTab();

        var rootItem = this.jsObject.TreeItemWithCheckBox("Components", "SmallComponents.StiText.png", null, tree);
        rootItem.button.style.display = "none";
        rootItem.iconOpening.style.display = "none";
        rootItem.setOpening(true);
        tree.appendChild(rootItem);

        var isDashboard = this.jsObject.options.currentPage && this.jsObject.options.currentPage.isDashboard;

        for (var groupName in allComponents) {
            var groupItem = this.jsObject.TreeItemWithCheckBox(allComponents[groupName].text || this.jsObject.loc.Report[this.jsObject.UpperFirstChar(groupName)],
                allComponents[groupName].image, null, tree);
            tree.groups[groupName] = groupItem;
            groupItem.setOpening(true);
            rootItem.addChild(groupItem);

            groupItem.style.display = (groupName == "dashboards" && isDashboard) ||
                ((groupName == "bands" || groupName == "crossBands" || groupName == "components" || groupName == "shapes" || groupName == "shapes") && !isDashboard) ? "" : "none";

            for (var i = 0; i < allComponents[groupName].items.length; i++) {
                var componentType = allComponents[groupName].items[i]
                if (!this.jsObject.options.visibilityComponents[componentType] &&
                    !this.jsObject.options.visibilityBands[componentType] &&
                    !this.jsObject.options.visibilityCrossBands[componentType] &&
                    (this.jsObject.options.dashboardAssemblyLoaded && !this.jsObject.options.visibilityDashboardElements[componentType]))
                continue;

                var text = this.jsObject.loc.Components[groupName == "dashboards" ? componentType.replace("Element", "") : componentType];
                var image = (groupName == "dashboards" ? "Dashboards.SmallComponents." : "SmallComponents.") + componentType + ".png"

                var componentItem = this.jsObject.TreeItemWithCheckBox(text, image, { componentName: componentType }, tree);
                groupItem.addChild(componentItem);

                if (this.jsObject.IsContains(selectedComponents[groupName], componentType)) {
                    componentItem.setChecked(true);
                    componentItem.checkBox.action();
                }
            }
        }
    }

    form.onshow = function () {
        form.buildTree(); 
        tree.style.display = this.jsObject.options.componentsIntoInsertTab ? "none" : "";
        showInsertTab.setChecked(this.jsObject.options.showInsertTab);
        showInsertTab.setEnabled(this.jsObject.options.showInsertButton); //supported old property
        showToolbox.setChecked(this.jsObject.options.showToolbox);
    }

    form.getSelectedComponents = function () {
        var components = {
            bands: [],
            crossBands: [],
            components: [],
            shapes: [],
            infographics: []
        }

        if (this.jsObject.options.dashboardAssemblyLoaded) {
            components.dashboards = [];
        }

        for (var groupName in components) {
            for (var i = 0; i < tree.groups[groupName].childsContainer.childNodes.length; i++) {
                var componentItem = tree.groups[groupName].childsContainer.childNodes[i];
                if (componentItem.isChecked) {
                    components[groupName].push(componentItem.itemObject.componentName);
                }
            }
        }

        return components;
    }

    form.action = function () {
        this.changeVisibleState(false);
        var components = this.getSelectedComponents();

        if (this.jsObject.options.insertPanel) this.jsObject.options.insertPanel.update(components);
        this.jsObject.SetCookie("StimulsoftMobileDesignerComponentsIntoInsertTab", JSON.stringify(components));
        this.jsObject.options.showInsertTab = showInsertTab.isChecked;
        this.jsObject.options.showToolbox = showToolbox.isChecked;
        this.jsObject.SetCookie("StimulsoftMobileDesignerSetupToolbox", JSON.stringify({
            showToolbox: showToolbox.isChecked,
            showInsertTab: showInsertTab.isChecked
        }));

        if (this.jsObject.options.buttons.insertToolButton) {
            this.jsObject.options.buttons.insertToolButton.parentElement.style.display = (!this.jsObject.options.showInsertButton ? false : this.jsObject.options.showInsertTab) ? "" : "none";
            if (this.jsObject.options.workPanel.currentPanel == this.jsObject.options.insertPanel && !showInsertTab.isChecked) {
                this.jsObject.options.workPanel.showPanel(this.jsObject.options.homePanel);
                this.jsObject.options.buttons.homeToolButton.setSelected(true);
            }
        }

        this.jsObject.options.toolbox.changeVisibleState(showToolbox.isChecked);
        if (showToolbox.isChecked) this.jsObject.options.toolbox.update(components);
    }

    return form;
}