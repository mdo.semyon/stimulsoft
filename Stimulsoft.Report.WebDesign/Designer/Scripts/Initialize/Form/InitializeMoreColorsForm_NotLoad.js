﻿
StiMobileDesigner.prototype.InitializeMoreColorsForm_ = function () {
    //More Colors Form
    var moreColorsForm = this.BaseForm("moreColors", this.loc.PropertyCategory.ColorsCategory, 4);
    moreColorsForm.width = 300;
    moreColorsForm.height = this.options.isTouchDevice ? 170 : 150;

    //Override methods
    moreColorsForm.onmousedown = function () { if (this.jsObject.options.isTouchDevice) return; this.ontouchstart(); }
    moreColorsForm.ontouchstart = function () { this.jsObject.options.colorDialogPressed = this; }

    var tabs = [];
    tabs.push({ "name": "custom", "caption": this.loc.FormColorBoxPopup.Custom });
    tabs.push({ "name": "webColors", "caption": this.loc.FormColorBoxPopup.Web });
    var tabbedPane = this.TabbedPane("moreColorsTabbedPane", tabs, this.GetStyles("StandartTab"));
    moreColorsForm.container.appendChild(tabbedPane);

    //Hide Header Caption
    moreColorsForm.caption.innerHTML = "";
    moreColorsForm.caption.appendChild(tabbedPane.tabsPanel);
    moreColorsForm.caption.style.padding = "0px";
    moreColorsForm.container.style.borderTop = "0px";

    //Custom
    tabbedPane.tabsPanels.custom.appendChild(this.FormSeparator());
    tabbedPane.tabsPanels.custom.appendChild(this.ColorFormCustomTable(moreColorsForm));
    tabbedPane.tabsPanels.custom.style.width = moreColorsForm.width + "px";
    tabbedPane.tabsPanels.custom.style.height = moreColorsForm.height + "px";

    //Web Colors
    var webColorPanel = this.ColorFormWebColorPanel(moreColorsForm);
    webColorPanel.style.margin = "5px";
    webColorPanel.style.height = (moreColorsForm.height - 11) + "px";
    tabbedPane.tabsPanels.webColors.appendChild(this.FormSeparator());
    tabbedPane.tabsPanels.webColors.appendChild(webColorPanel);
    tabbedPane.tabsPanels.webColors.style.width = moreColorsForm.width + "px";
    tabbedPane.tabsPanels.webColors.style.height = moreColorsForm.height + "px";

    moreColorsForm.onshow = function () {
        var options = this.jsObject.options;
        options.controls.moreColorsTabbedPane.showTabPanel("custom");
        this.webColorsButtons[0].action();
        var colorDialog = options.menus.colorDialog || this.jsObject.InitializeColorDialog();
        var key = colorDialog.parentButton.colorControl.key;
        if (key == null) key = "255,255,255,255";
        if (key == "transparent") key = "0,255,255,255";
        var colors = key.split(",");

        if (colors.length == 4) {
            options.controls.colorFormAlfaCanal.setKey(colors[0]);
            colors.splice(0, 1);
        }
        else
            options.controls.colorFormAlfaCanal.setKey("255");

        options.controls.colorFormRedColor.value = colors[0];
        options.controls.colorFormGreenColor.value = colors[1];
        options.controls.colorFormBlueColor.value = colors[2];
        this.updateColorBar();
    }

    moreColorsForm.updateColorBar = function () {
        var options = this.jsObject.options;
        options.controls.colorFormColorBar.style.opacity = this.jsObject.StrToInt(options.controls.colorFormAlfaCanal.key) / 255;
        options.controls.colorFormColorBar.style.background = "rgb(" + options.controls.colorFormRedColor.value + "," +
            options.controls.colorFormGreenColor.value + "," + options.controls.colorFormBlueColor.value + ")";
    }

    moreColorsForm.action = function () {
        this.changeVisibleState(false);
        var options = this.jsObject.options;
        var key = "transparent";
        if (options.controls.moreColorsTabbedPane.selectedTab.panelName == "custom") {
            if (options.controls.colorFormAlfaCanal.key != "0") {
                key = options.controls.colorFormRedColor.value + "," + options.controls.colorFormGreenColor.value + "," + options.controls.colorFormBlueColor.value;
                if (options.controls.colorFormAlfaCanal.key != "255") key = options.controls.colorFormAlfaCanal.key + "," + key;
            }
        }
        else {
            key = this.selectedWebColorsButton.key;
        }

        var colorDialog = this.jsObject.options.menus.colorDialog || this.jsObject.InitializeColorDialog();
        colorDialog.parentButton.choosedColor(key);
    }

    return moreColorsForm;
}

//Custom Tab
StiMobileDesigner.prototype.ColorFormCustomTable = function (moreColorsForm) {
    var customTable = this.CreateHTMLTable();
    customTable.style.width = "100%";
    customTable.style.height = "100%";
    var controlsTable = this.CreateHTMLTable();
    customTable.addCell(controlsTable).style.width = "1px";
    controlsTable.className = "stiColorFormControlsTable stiDesignerClearAllStyles";
    controlsTable.style.height = moreColorsForm.height + "px";

    //Red
    controlsTable.addRow();
    var cellRColor = controlsTable.addCellInLastRow();
    cellRColor.className = "stiDesignerCaptionControls";
    cellRColor.innerHTML = this.loc.PropertyColor.Red + ":";
    var textRColor = this.ColorTextBox("colorFormRedColor", 60);
    var cellRColorControl = controlsTable.addCellInLastRow(textRColor);

    //Green
    controlsTable.addRow();
    var cellGColor = controlsTable.addCellInLastRow();
    cellGColor.className = "stiDesignerCaptionControls";
    cellGColor.innerHTML = this.loc.PropertyColor.Green + ":";
    var textGColor = this.ColorTextBox("colorFormGreenColor", 60);
    var cellGColorControl = controlsTable.addCellInLastRow(textGColor);

    //Blue
    controlsTable.addRow();
    var cellBColor = controlsTable.addCellInLastRow();
    cellBColor.className = "stiDesignerCaptionControls";
    cellBColor.innerHTML = this.loc.PropertyColor.Blue + ":";
    var textBColor = this.ColorTextBox("colorFormBlueColor", 60);
    var cellBColorControl = controlsTable.addCellInLastRow(textBColor);

    //Alfa Canal
    controlsTable.addRow();
    var cellACanal = controlsTable.addCellInLastRow();
    cellACanal.className = "stiDesignerCaptionControls";
    cellACanal.innerHTML = "Alpha:";

    var items = [];
    items.push(this.Item("transparentItem0", "0", null, "0"));
    items.push(this.Item("transparentItem25", "25", null, "25"));
    items.push(this.Item("transparentItem50", "50", null, "50"));
    items.push(this.Item("transparentItem100", "100", null, "100"));
    items.push(this.Item("transparentItem150", "150", null, "150"));
    items.push(this.Item("transparentItem200", "200", null, "200"));
    items.push(this.Item("transparentItem255", "255", null, "255"));

    var alfaCanalControl = this.DropDownList("colorFormAlfaCanal", 60, null, items, false);
    alfaCanalControl.menu.style.zIndex = parseInt(moreColorsForm.style.zIndex) + 1;
    alfaCanalControl.action = function () {
        this.setKey(this.jsObject.StrToCorrectByte(this.key));
        this.jsObject.InitializeMoreColorsForm(function (moreColorsForm) {
            moreColorsForm.updateColorBar();
        });
    };

    var cellACanalControl = controlsTable.addCellInLastRow(alfaCanalControl);

    //Color Bar
    var colorBarContainer = document.createElement("div");
    colorBarContainer.className = "stiColorFormColorBar";
    colorBarContainer.style.backgroundImage = "url(" + this.options.images["ColorBarBackground.png"] + ")";
    customTable.addCell(colorBarContainer).style.textAlign = "center";

    var colorBar = document.createElement("div");
    colorBar.className = "stiColorFormColorBar";
    colorBar.style.border = "0px";
    this.options.controls.colorFormColorBar = colorBar;
    colorBarContainer.appendChild(colorBar);

    return customTable;
}

//Custom Tab
StiMobileDesigner.prototype.ColorFormWebColorPanel = function (moreColorsForm) {
    var webColorPanel = document.createElement("div");
    webColorPanel.className = "stiColorFormWebColorPanel";
    moreColorsForm.webColorsButtons = [];
    moreColorsForm.selectedWebColorsButton = null;

    for (var i = 0; i < this.ConstWebColors.length; i++) {
        var webColorButton = this.StandartSmallButton("webColorPanel" + this.ConstWebColors[i][0], "ColorFormWebColors", this.ConstWebColors[i][0], true, null, null);
        moreColorsForm.webColorsButtons.push(webColorButton);

        webColorButton.action = function () {
            this.setSelected(true);
            var this_ = this;
            this.jsObject.InitializeMoreColorsForm(function (moreColorsForm) {
                moreColorsForm.selectedWebColorsButton = this_;
            });
        }

        //Override image
        var newImageParent = document.createElement("div");
        newImageParent.className = "stiColorControlImage";
        var newImage = document.createElement("div");
        newImage.style.height = "100%";
        newImageParent.appendChild(newImage);

        var imageCell = webColorButton.image.parentElement;
        imageCell.removeChild(webColorButton.image);
        imageCell.appendChild(newImageParent);
        webColorButton.image = newImage;

        webColorButton.image.style.background = (this.ConstWebColors[i][1] != "transparent") ? "rgb(" + this.ConstWebColors[i][1] + ")" : "transparent";
        webColorButton.key = this.ConstWebColors[i][1];
        webColorPanel.appendChild(webColorButton);
    }

    return webColorPanel;
}

StiMobileDesigner.prototype.ColorTextBox = function (name, width) {
    var textBox = this.TextBox(name, width);

    textBox.action = function () {
        this.value = this.jsObject.StrToCorrectByte(this.value);
        this.jsObject.InitializeMoreColorsForm(function (moreColorsForm) {
            moreColorsForm.updateColorBar();
        });
    };

    return textBox;
}