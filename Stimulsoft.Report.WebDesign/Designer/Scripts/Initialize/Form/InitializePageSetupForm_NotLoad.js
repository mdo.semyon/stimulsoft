﻿
StiMobileDesigner.prototype.InitializePageSetupForm_ = function () {

    //Image Form
    var pageSetupForm = this.BaseForm("pageSetup", this.loc.Toolbars.ToolbarPageSetup, 1, this.HelpLinks["watermark"]);
    pageSetupForm.mode = "Paper";
    pageSetupForm.width = this.options.isTouchDevice ? 570 : 535;
    pageSetupForm.height = this.options.isTouchDevice ? 450 : 420;
    
    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerImageFormMainPanel";
    pageSetupForm.container.appendChild(mainTable);
    pageSetupForm.container.style.padding = "0px";

    //Buttons
    var buttonProps = [
        ["Paper", "PagePaper.png", this.loc.PropertyMain.Paper],
        ["Columns", "PageColumns.png", this.loc.PropertyMain.Columns],
        ["Watermark", "PageWatermark.png", this.loc.PropertyMain.Watermark]
    ];

    //Add Panels && Buttons
    var panelsContainer = mainTable.addCell();
    panelsContainer.className = "stiDesignerPageSetupFormPanel";
    var buttonsPanel = mainTable.addCell();
    buttonsPanel.style.verticalAlign = "top";
    pageSetupForm.mainButtons = {};
    pageSetupForm.panels = {};

    for (var i = 0; i < buttonProps.length; i++) {
        var panel = document.createElement("Div");
        if (i != 0) panel.style.display = "none";
        panelsContainer.appendChild(panel);
        pageSetupForm.panels[buttonProps[i][0]] = panel;

        var button = this.StandartFormBigButton("pageSetupForm" + buttonProps[i][0] + "Button", null, buttonProps[i][2], buttonProps[i][1], buttonProps[i][2], 80);
        button.style.margin = "2px";
        button.style.minWidth = "110px";
        if (button.caption) button.caption.style.padding = "4px 3px 4px 3px";
        pageSetupForm.mainButtons[buttonProps[i][0]] = button;
        buttonsPanel.appendChild(button);
        button.panelName = buttonProps[i][0];
        button.action = function () {
            pageSetupForm.setMode(this.panelName);
        }

        //add marker
        var marker = document.createElement("div");
        marker.style.display = "none";
        marker.className = "stiUsingMarker";
        var markerInner = document.createElement("div");
        marker.appendChild(markerInner);
        button.style.position = "relative";
        button.appendChild(marker);
        button.marker = marker;
    }

    pageSetupForm.panels.Paper.appendChild(this.PageSetupPaperBlock(pageSetupForm));
    pageSetupForm.panels.Columns.appendChild(this.PageSetupColumnsBlock(pageSetupForm));
    pageSetupForm.panels.Watermark.appendChild(this.PageSetupWatermarkBlock(pageSetupForm));

    pageSetupForm.setMode = function (mode) {
        pageSetupForm.mode = mode;
        for (var panelName in pageSetupForm.panels) {
            pageSetupForm.panels[panelName].style.display = mode == panelName ? "" : "none";
            pageSetupForm.mainButtons[panelName].setSelected(mode == panelName);
        }
        if (mode == "Watermark") {
            this.jsObject.options.controls.pageSetupWatermarkText.textArea.focus();
            this.jsObject.options.controls.pageSetupWatermarkImageGallery.autoscroll();
        }
    }

    pageSetupForm.changeSizeValues = function () {
        var controls = this.jsObject.options.controls;
        var buttons = this.jsObject.options.buttons;
        var temp = controls.pageSetupPageWidth.value;
        controls.pageSetupPageWidth.value = controls.pageSetupPageHeight.value;
        controls.pageSetupPageHeight.value = temp;
        var leftMargin = controls.pageSetupLeftMargin.value;
        var topMargin = controls.pageSetupTopMargin.value;
        var rightMargin = controls.pageSetupRightMargin.value;
        var bottomMargin = controls.pageSetupBottomMargin.value;
        controls.pageSetupLeftMargin.value = buttons.pageSetupPortrait.isSelected ? bottomMargin : topMargin;
        controls.pageSetupTopMargin.value = buttons.pageSetupPortrait.isSelected ? leftMargin : rightMargin;
        controls.pageSetupRightMargin.value = buttons.pageSetupPortrait.isSelected ? topMargin : bottomMargin;
        controls.pageSetupBottomMargin.value = buttons.pageSetupPortrait.isSelected ? rightMargin : leftMargin;
        pageSetupForm.pageSample.update();
    }

    pageSetupForm.applyPropertiesToObject = function (object) {
        var controls = this.jsObject.options.controls;
        var buttons = this.jsObject.options.buttons;

        object.paperSize = buttons.pageSetupPaperSize.key;
        object.unitWidth = controls.pageSetupPageWidth.value;
        object.unitHeight = controls.pageSetupPageHeight.value;
        object.orientation = buttons.pageSetupPortrait.isSelected ? "Portrait" : "Landscape";
        object.unitMargins = controls.pageSetupLeftMargin.value + "!" + controls.pageSetupTopMargin.value + "!" +
            controls.pageSetupRightMargin.value + "!" + controls.pageSetupBottomMargin.value;
        object.mirrorMargins = controls.pageSetupMirrorMargins.isChecked;
        object.columns = controls.pageSetupNumberOfColumns.value;
        object.columnWidth = controls.pageSetupColumnWidth.value;
        object.columnGaps = controls.pageSetupColumnGaps.value;
        object.rightToLeft = controls.pageSetupColumnsRightToLeft.isChecked;
        object.waterMarkText = Base64.encode(controls.pageSetupWatermarkText.getValue());
        object.waterMarkAngle = controls.pageSetupWatermarkAngle.value;
        object.waterMarkFont = controls.pageSetupWatermarkFontName.key + "!" + controls.pageSetupWatermarkFontSize.key + "!" +
            (buttons.pageSetupWatermarkFontBold.isSelected ? "1" : "0") + "!" + (buttons.pageSetupWatermarkFontItalic.isSelected ? "1" : "0") + "!" +
            (buttons.pageSetupWatermarkFontUnderline.isSelected ? "1" : "0") + "!" + (buttons.pageSetupWatermarkFontStrikeout.isSelected ? "1" : "0");
        object.waterMarkTextBrush = controls.pageSetupWatermarkTextBrush.key;
        object.waterMarkEnabled = controls.pageSetupWatermarkEnabled.key != "False";
        object.waterMarkEnabledExpression = controls.pageSetupWatermarkEnabled.key == "Expression" ?
            Base64.encode(controls.pageSetupWatermarkEnabledExpression.value) : "";

        object.waterMarkRightToLeft = controls.pageSetupWatermarkRightToLeft.isChecked;
        object.waterMarkTextBehind = controls.pageSetupWatermarkShowBehind.isChecked;        
        object.waterMarkImageAlign = controls.pageSetupWatermarkImageAlignment.key;
        object.waterMarkMultipleFactor = controls.pageSetupWatermarkMultipleFactor.value;
        object.waterMarkTransparency = controls.pageSetupWatermarkTransparency.value;
        object.waterMarkRatio = controls.pageSetupWatermarkAspectRatio.isChecked;
        object.waterMarkImageBehind = controls.pageSetupWatermarkShowImageBehind.isChecked;
        object.waterMarkStretch = controls.pageSetupWatermarkImageStretch.isChecked;
        object.waterMarkTiling = controls.pageSetupWatermarkImageTiling.isChecked;

        if (controls.pageSetupWatermarkImage.imageHyperlink) {
            object.watermarkImageHyperlink = controls.pageSetupWatermarkImage.imageHyperlink;
            object.watermarkImageSrc = "";
            object.watermarkImageContentForPaint = controls.pageSetupWatermarkImage.src;
        }
        else {
            var imageSrc = controls.pageSetupWatermarkImage.src || "";
            if (this.jsObject.options.mvcMode) imageSrc = encodeURIComponent(imageSrc);
            object.watermarkImageSrc = imageSrc;
            object.watermarkImageHyperlink = "";
        }
    }

    pageSetupForm.updateMarkers = function () {
        var controls = this.jsObject.options.controls;
        this.mainButtons["Watermark"].marker.style.display =
            controls.pageSetupWatermarkText.getValue() ||
            controls.pageSetupWatermarkImage.imageHyperlink ||
            controls.pageSetupWatermarkImage.src ? "" : "none";
    }

    pageSetupForm.action = function () {
        pageSetupForm.pageSample.innerHTML = ""; //clear page sample
        var currentPage = this.jsObject.options.currentPage;
        pageSetupForm.applyPropertiesToObject(currentPage.properties);

        this.changeVisibleState(false);
        this.jsObject.SendCommandSendProperties(currentPage, ["paperSize", "orientation", "unitWidth", "unitHeight", "unitMargins", "columns", "columnWidth", "columnGaps",
            "rightToLeft", "waterMarkText", "waterMarkAngle", "waterMarkFont", "waterMarkTextBrush", "waterMarkEnabled", "waterMarkEnabledExpression", "waterMarkRightToLeft",
            "waterMarkTextBehind", "waterMarkImageAlign", "waterMarkMultipleFactor", "waterMarkTransparency", "waterMarkRatio", "waterMarkImageBehind", "waterMarkStretch",
            "waterMarkTiling", "watermarkImageSrc", "mirrorMargins", "watermarkImageHyperlink"]);
    }

    pageSetupForm.onhide = function () {
        clearTimeout(this.markerTimer);
    }

    pageSetupForm.onshow = function () {
        var currentPage = this.jsObject.options.currentPage;
        var controls = this.jsObject.options.controls;
        var buttons = this.jsObject.options.buttons;
        pageSetupForm.setMode("Paper");
        pageSetupForm.panels.Watermark.showAllOptions = false;

        var waterMarkEnabledExpression = Base64.decode(currentPage.properties.waterMarkEnabledExpression);
        controls.pageSetupWatermarkEnabled.setKey(waterMarkEnabledExpression ? "Expression" : (currentPage.properties.waterMarkEnabled ? "True" : "False"));
        controls.pageSetupWatermarkEnabledExpression.value = waterMarkEnabledExpression;
        pageSetupForm.watermarkRows["EnabledExpression"].style.display = controls.pageSetupWatermarkEnabled.key == "Expression" ? "" : "none";

        pageSetupForm.changeVisibleStateMoreOptions(pageSetupForm.panels.Watermark.showAllOptions);

        buttons.pageSetupPaperSize.setKey(currentPage.properties.paperSize);
        controls.pageSetupPageWidth.value = currentPage.properties.unitWidth;
        controls.pageSetupPageHeight.value = currentPage.properties.unitHeight;
        if (currentPage.properties.orientation == "Portrait") buttons.pageSetupPortrait.setSelected(true);
        else buttons.pageSetupLandscape.setSelected(true);
        var margins = currentPage.properties.unitMargins.split("!");
        controls.pageSetupLeftMargin.value = margins[0];
        controls.pageSetupTopMargin.value = margins[1];
        controls.pageSetupRightMargin.value = margins[2];
        controls.pageSetupBottomMargin.value = margins[3];
        controls.pageSetupMirrorMargins.setChecked(currentPage.properties.mirrorMargins);
        controls.pageSetupNumberOfColumns.value = currentPage.properties.columns;
        controls.pageSetupColumnWidth.value = currentPage.properties.columnWidth;
        controls.pageSetupColumnGaps.value = currentPage.properties.columnGaps;
        controls.pageSetupColumnsRightToLeft.setChecked(currentPage.properties.rightToLeft);
        controls.pageSetupWatermarkText.setValue(Base64.decode(currentPage.properties.waterMarkText));
        controls.pageSetupWatermarkAngle.value = currentPage.properties.waterMarkAngle;
        var font = currentPage.properties.waterMarkFont.split("!");
        controls.pageSetupWatermarkFontName.setKey(font[0]);
        controls.pageSetupWatermarkFontSize.setKey(font[1]);
        buttons.pageSetupWatermarkFontBold.setSelected(font[2] == "1");
        buttons.pageSetupWatermarkFontItalic.setSelected(font[3] == "1");
        buttons.pageSetupWatermarkFontUnderline.setSelected(font[4] == "1");
        buttons.pageSetupWatermarkFontStrikeout.setSelected(font[5] == "1");
        controls.pageSetupWatermarkTextBrush.setKey(currentPage.properties.waterMarkTextBrush);
                
        controls.pageSetupWatermarkRightToLeft.setChecked(currentPage.properties.waterMarkRightToLeft);
        controls.pageSetupWatermarkShowBehind.setChecked(currentPage.properties.waterMarkTextBehind);
        controls.pageSetupWatermarkImageAlignment.setKey(currentPage.properties.waterMarkImageAlign);
        controls.pageSetupWatermarkMultipleFactor.value = currentPage.properties.waterMarkMultipleFactor;
        controls.pageSetupWatermarkTransparency.value = currentPage.properties.waterMarkTransparency;
        controls.pageSetupWatermarkImage.setImage(currentPage.properties.watermarkImageSrc);
        controls.pageSetupWatermarkAspectRatio.setChecked(currentPage.properties.waterMarkRatio);
        controls.pageSetupWatermarkShowImageBehind.setChecked(currentPage.properties.waterMarkImageBehind);
        controls.pageSetupWatermarkImageStretch.setChecked(currentPage.properties.waterMarkStretch);
        controls.pageSetupWatermarkImageTiling.setChecked(currentPage.properties.waterMarkTiling);
        pageSetupForm.pageSample.update();


        //Update Image Gallery
        controls.pageSetupWatermarkImage.itemName = null;
        controls.pageSetupWatermarkImage.imageHyperlink = currentPage.properties.watermarkImageHyperlink || "";
        
        if (this.jsObject.options.imagesGallery || this.jsObject.CheckImagesInDictionary()) {
            controls.pageSetupWatermarkImageGallery.changeVisibleState(true);
            controls.pageSetupWatermarkImageGallery.progress.firstChild.style.width = "32px";
            controls.pageSetupWatermarkImageGallery.progress.firstChild.style.height = "32px";
            controls.pageSetupWatermarkImageGallery.progress.show(170, -55);

            if (!this.jsObject.options.imagesGallery) {
                this.jsObject.SendCommandToDesignerServer("GetImagesGallery", null, function (answer) {
                    pageSetupForm.jsObject.options.imagesGallery = answer.imagesGallery;
                    pageSetupForm.fillImagesGallery(answer.imagesGallery);
                    pageSetupForm.selectGalleryItem();
                });
            }
            else {
                pageSetupForm.fillImagesGallery(this.jsObject.options.imagesGallery)
            }
        }
        else {
            controls.pageSetupWatermarkImageGallery.changeVisibleState(false);
        }
        
        pageSetupForm.selectGalleryItem();

        this.updateMarkers();
        this.markerTimer = setInterval(function () {
            pageSetupForm.updateMarkers();
        }, 250)
    };

    pageSetupForm.fillImagesGallery = function (imagesGallery) {
        var controls = this.jsObject.options.controls;
        controls.pageSetupWatermarkImageGallery.progress.hide();
        var allImages = [].concat(imagesGallery.variables, imagesGallery.resources);

        if (allImages.length > 0) {
            controls.pageSetupWatermarkImageGallery.addItems(allImages);
        }
        else {
            controls.pageSetupWatermarkImageGallery.changeVisibleState(false);
        }
    }

    pageSetupForm.selectGalleryItem = function () {
        var controls = this.jsObject.options.controls;
        var resourceIdent = this.jsObject.options.resourceIdent;
        var variableIdent = this.jsObject.options.variableIdent;
        var imageHyperlink = controls.pageSetupWatermarkImage.imageHyperlink;

        if (imageHyperlink.indexOf(resourceIdent) == 0 || imageHyperlink.indexOf(variableIdent) == 0) {
            var ident = imageHyperlink.indexOf(resourceIdent) == 0 ? resourceIdent : variableIdent;
            if (pageSetupForm.jsObject.options.imagesGallery) {
                var item = controls.pageSetupWatermarkImageGallery.getItemByPropertyValue("name", imageHyperlink.substring(imageHyperlink.indexOf(ident) + ident.length));
                if (item) item.action(true);
            }
        }
    }

    //Override
    if (pageSetupForm["buttonHelp"])
        pageSetupForm.buttonHelp.action = function () {
            switch (pageSetupForm.mode) {
                case "Watermark": this.jsObject.ShowHelpWindow(this.jsObject.HelpLinks["watermark"]); break;
                case "Columns": this.jsObject.ShowHelpWindow(this.jsObject.HelpLinks["columns"]); break;
                case "Paper": this.jsObject.ShowHelpWindow(this.form.helpUrl); break;
            }
        };

    return pageSetupForm
}

//Paper Block
StiMobileDesigner.prototype.PageSetupPaperBlock = function (pageSetupForm) {
    var paperSizeBlock = this.FormBlock(pageSetupForm.width, pageSetupForm.height);
    
    var paperSizeTable = this.CreateHTMLTable();
    paperSizeTable.style.width = "100%";
    paperSizeBlock.appendChild(paperSizeTable);
    
    paperSizeTable.addRow();
    var cellPaperSize = paperSizeTable.addCellInLastRow();
    cellPaperSize.style.padding = "0 0 6px 0";
    cellPaperSize.setAttribute("colspan", "2");
    cellPaperSize.appendChild(this.FormBlockHeader(this.loc.PropertyMain.PaperSize));

    //Paper Size
    paperSizeTable.addRow();
    paperSizeTable.addTextCellInLastRow(this.loc.PropertyMain.Size + ":").className = "stiDesignerCaptionControlsBigIntervals";
    
    var sizeButton = this.SmallButton("pageSetupPaperSize", null, " ", null, null, "Down", this.GetStyles("FormButton"));
    sizeButton.style.minWidth = "74px";
    sizeButton.innerTable.style.width = "100%";

    var sizeMenu = this.PageSizeMenu("pageSetupPageSizeMenu", sizeButton);    
    var buttonTable = this.CreateHTMLTable();
    buttonTable.addCell(sizeButton);
    paperSizeTable.addCellInLastRow(buttonTable).className = "stiDesignerControlCellsBigIntervals";
    
    sizeButton.action = function () { sizeMenu.changeVisibleState(!sizeMenu.visible); }
            
    sizeButton.TextSizes = function (jsObject, num){            
            if (num == 0) return "";
            var unit = jsObject.options.report.properties.reportUnit;
            var pageWidth = jsObject.ConvertPixelToUnit(jsObject.PaperSizesInPixels[num][0]).toFixed(1);
            var pageHeight = jsObject.ConvertPixelToUnit(jsObject.PaperSizesInPixels[num][1]).toFixed(1);
            
            return pageWidth + unit + " X " + pageHeight + unit; 
    }
            
    //Override methods        
    sizeButton.setKey = function (key) {
        this.key = key;
        this.caption.style.fontSize = "12px";
        var text = "<b>" + this.jsObject.options.paperSizes[key] + "</b>  ";
        if (key != 0) text += "(" + this.TextSizes(this.jsObject, key) + ") ";
        this.caption.innerHTML = text;
    }
    
    sizeMenu.action = function (menuItem) {
        menuItem.setSelected(true);
        this.changeVisibleState(false);
        this.parentButton.setKey(menuItem.key);
        if (menuItem.key == 0) return;
        var pageWidth = this.jsObject.ConvertPixelToUnit(this.jsObject.PaperSizesInPixels[parseInt(menuItem.key)][0]).toFixed(1);
        var pageHeight = this.jsObject.ConvertPixelToUnit(this.jsObject.PaperSizesInPixels[parseInt(menuItem.key)][1]).toFixed(1);
        if (Math.round(pageWidth) == pageWidth) pageWidth = Math.round(pageWidth);
        if (Math.round(pageHeight) == pageHeight) pageHeight = Math.round(pageHeight);
        this.jsObject.options.controls.pageSetupPageWidth.value = (this.jsObject.options.buttons.pageSetupPortrait.isSelected) ? pageWidth.toString() : pageHeight.toString();
        this.jsObject.options.controls.pageSetupPageHeight.value = (this.jsObject.options.buttons.pageSetupPortrait.isSelected) ? pageHeight.toString() : pageWidth.toString();
        pageSetupForm.pageSample.update();
    }
    
    sizeMenu.onshow = function() {        
        for (var itemName in this.items) {
            var num = parseInt(this.items[itemName].key);
            if (this.items[itemName].key == this.parentButton.key) this.items[itemName].setSelected(true);
            this.items[itemName].caption.innerHTML = "<b>" + this.jsObject.options.paperSizes[num] + "</b><br>  " +
                this.parentButton.TextSizes(this.jsObject, num);
        }
    }       
            
    //Page Width
    paperSizeTable.addRow();    
    paperSizeTable.addTextCellInLastRow(this.loc.PropertyMain.Width + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var pageWidthControl = this.TextBoxDoubleValue("pageSetupPageWidth", 70);   
    paperSizeTable.addCellInLastRow(pageWidthControl).className = "stiDesignerControlCellsBigIntervals";
    pageWidthControl.action = function () { pageSetupForm.pageSample.update(); }

    //Page Height
    paperSizeTable.addRow();    
    paperSizeTable.addTextCellInLastRow(this.loc.PropertyMain.Height + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var pageHeightControl = this.TextBoxDoubleValue("pageSetupPageHeight", 70);   
    paperSizeTable.addCellInLastRow(pageHeightControl).className = "stiDesignerControlCellsBigIntervals";
    pageHeightControl.action = function () { pageSetupForm.pageSample.update(); }

    //Orientation
    paperSizeTable.addRow();    
    var cellOrientation = paperSizeTable.addCellInLastRow();
    cellOrientation.style.padding = "10px 0 6px 0";
    cellOrientation.setAttribute("colspan", "2");    
    cellOrientation.appendChild(this.FormBlockHeader(this.loc.PropertyMain.Orientation));
    
    var orientationTable = this.CreateHTMLTable();
    orientationTable.style.marginLeft = "10px";
    cellOrientation.appendChild(orientationTable);    
    var portraitButton = this.StandartBigButton("pageSetupPortrait", "pageSetupOrientation", this.loc.FormPageSetup.PageOrientationPortrait, "Portrait.png", null, null);
    portraitButton.action = function () {
        if (!this.isSelected) { this.setSelected(true); pageSetupForm.changeSizeValues() };
    }
    orientationTable.addCell(portraitButton).style.padding = "5px";
    var landscapeButton = this.StandartBigButton("pageSetupLandscape", "pageSetupOrientation", this.loc.FormPageSetup.PageOrientationLandscape, "Landscape.png", null, null);
    landscapeButton.action = function () {
        if (!this.isSelected) { this.setSelected(true); pageSetupForm.changeSizeValues() };
    }
    orientationTable.addCell(landscapeButton).style.padding = "5px";
        
    //Margins
    paperSizeTable.addRow();    
    var cellMargins = paperSizeTable.addCellInLastRow();
    cellMargins.style.padding = "10px 0 6px 0";
    cellMargins.setAttribute("colspan", "2");    
    cellMargins.appendChild(this.FormBlockHeader(this.loc.FormPageSetup.groupMargins));
    
    //Left
    paperSizeTable.addRow();    
    paperSizeTable.addTextCellInLastRow(this.loc.PropertyEnum.StiBorderSidesLeft + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var leftControl = this.TextBoxDoubleValue("pageSetupLeftMargin", 70);   
    leftControl.action = function () { pageSetupForm.pageSample.update(); }
    paperSizeTable.addCellInLastRow(leftControl).className = "stiDesignerControlCellsBigIntervals";
    
    //Right
    paperSizeTable.addRow();    
    paperSizeTable.addTextCellInLastRow(this.loc.PropertyEnum.StiBorderSidesRight + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var rightControl = this.TextBoxDoubleValue("pageSetupRightMargin", 70);   
    rightControl.action = function () { pageSetupForm.pageSample.update(); }
    paperSizeTable.addCellInLastRow(rightControl).className = "stiDesignerControlCellsBigIntervals";
    
    //Top
    paperSizeTable.addRow();    
    paperSizeTable.addTextCellInLastRow(this.loc.PropertyEnum.StiBorderSidesTop + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var topControl = this.TextBoxDoubleValue("pageSetupTopMargin", 70);   
    topControl.action = function () { pageSetupForm.pageSample.update(); }
    paperSizeTable.addCellInLastRow(topControl).className = "stiDesignerControlCellsBigIntervals";
    
    //Bottom
    paperSizeTable.addRow();    
    paperSizeTable.addTextCellInLastRow(this.loc.PropertyEnum.StiBorderSidesBottom + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var bottomControl = this.TextBoxDoubleValue("pageSetupBottomMargin", 70);   
    bottomControl.action = function () { pageSetupForm.pageSample.update(); }
    paperSizeTable.addCellInLastRow(bottomControl).className = "stiDesignerControlCellsBigIntervals";
    
    //Margins
    paperSizeTable.addRow();
    var mirrorMarginsControl = this.CheckBox("pageSetupMirrorMargins", this.loc.PropertyMain.MirrorMargins);
    mirrorMarginsControl.action = function () { pageSetupForm.pageSample.update(); }
    mirrorMarginsControl.style.marginTop = "5px";
    paperSizeTable.addCellInLastRow(mirrorMarginsControl).className = "stiDesignerCaptionControlsBigIntervals";

    return paperSizeBlock;
}

//Columns Block
StiMobileDesigner.prototype.PageSetupColumnsBlock = function (pageSetupForm) {
    var columnsBlock = this.FormBlock(pageSetupForm.width, pageSetupForm.height);
    
    var columnsTable = this.CreateHTMLTable();
    columnsTable.style.width = "100%";
    columnsBlock.appendChild(columnsTable);
      
    columnsTable.addRow();
    var cellColumns = columnsTable.addCellInLastRow();
    cellColumns.style.padding = "0 0 6px 0";
    cellColumns.setAttribute("colspan", "2");
    cellColumns.appendChild(this.FormBlockHeader(this.loc.PropertyMain.Columns));

    //NumberOfColumns
    columnsTable.addRow();    
    columnsTable.addTextCellInLastRow(this.loc.FormPageSetup.NumberOfColumns).className = "stiDesignerCaptionControlsBigIntervals";

    var numColumnControl = this.TextBoxPositiveIntValue("pageSetupNumberOfColumns", 70);   
    columnsTable.addCellInLastRow(numColumnControl).className = "stiDesignerControlCellsBigIntervals";
    
    //Column Width
    columnsTable.addRow();    
    columnsTable.addTextCellInLastRow(this.loc.PropertyMain.ColumnWidth + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var widthColumnControl = this.TextBoxDoubleValue("pageSetupColumnWidth", 70);   
    columnsTable.addCellInLastRow(widthColumnControl).className = "stiDesignerControlCellsBigIntervals";
    
    //Column Gaps
    columnsTable.addRow();    
    columnsTable.addTextCellInLastRow(this.loc.PropertyMain.ColumnGaps + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var gapsColumnControl = this.TextBoxDoubleValue("pageSetupColumnGaps", 70);   
    columnsTable.addCellInLastRow(gapsColumnControl).className = "stiDesignerControlCellsBigIntervals";
    
    //Columns Right To Left
    columnsTable.addRow();
    
    var rightToLeftColumnsCell = columnsTable.addCellInLastRow();
    rightToLeftColumnsCell.className = "stiDesignerCaptionControlsBigIntervals";
    rightToLeftColumnsCell.style.paddingTop = "7px";
    rightToLeftColumnsCell.style.paddingBottom = "7px";
    rightToLeftColumnsCell.setAttribute("colspan", "2");
        
    var rightToLeftColumnsControl = this.CheckBox("pageSetupColumnsRightToLeft", this.loc.PropertyMain.RightToLeft);
    rightToLeftColumnsCell.appendChild(rightToLeftColumnsControl);
        
    return columnsBlock;
}

//Watermark Block
StiMobileDesigner.prototype.PageSetupWatermarkBlock = function (pageSetupForm) {
    var watermarkBlock = this.FormBlock(pageSetupForm.width, pageSetupForm.height);
    watermarkBlock.showAllOptions = false;
    pageSetupForm.watermarkRows = {};
    
    var mainTable = this.CreateHTMLTable();
    mainTable.style.width = "100%";
    mainTable.style.height = pageSetupForm.height + "px";
    watermarkBlock.appendChild(mainTable);

    var pageSample = document.createElement("div");
    pageSetupForm.pageSample = pageSample;
    pageSample.style.margin = "10px 5px 5px 5px";

    pageSample.update = function (imageSize) {
        this.innerHTML = "";
        //debugger;
        var pagePoperties = pageSetupForm.jsObject.CopyObject(pageSetupForm.jsObject.options.currentPage.properties);
        var pageWidth = parseInt(pageSetupForm.jsObject.ConvertUnitToPixel(
            pageSetupForm.jsObject.StrToDouble(pagePoperties.orientation == "Portrait" ? pagePoperties.unitWidth : pagePoperties.unitHeight)));
        var zoom = 125 / pageWidth;
        if (imageSize) pagePoperties.watermarkImageSize = imageSize;
        pageSetupForm.applyPropertiesToObject(pagePoperties);
        var pageSvg = pageSetupForm.jsObject.GetSvgPageForCheckPreview(pageSetupForm.jsObject.options.currentPage.properties.pageIndex, null, zoom, true, pagePoperties);
        pageSample.appendChild(pageSvg);
    }

    mainTable.addCell(pageSample).className = "stiDesignerPageSetupFormSampleCell";
    var watermarkTable = this.CreateHTMLTable();
    watermarkTable.style.width = "100%";
    mainTable.addCell(watermarkTable).style.verticalAlign = "top";
            
    //Text
    watermarkTable.addRow();
    watermarkTable.style.margin = "0 0 6px 0";
    var cellWatermarkText = watermarkTable.addCellInLastRow();
    cellWatermarkText.style.padding = "0 0 6px 0";
    cellWatermarkText.setAttribute("colspan", "2");
    cellWatermarkText.appendChild(this.FormBlockHeader(this.loc.Toolbars.ToolbarWatermarkText));

    watermarkTable.addRow();
    var watermarkTextControl = this.ExpressionTextArea("pageSetupWatermarkText", pageSetupForm.width - 171, 50);
    watermarkTextControl.action = function () { pageSample.update(); }
    watermarkTextControl.style.margin = "2px 0px 2px 8px";
    watermarkTable.addCellInLastRow(watermarkTextControl).setAttribute("colspan", "2");

    //Angle
    pageSetupForm.watermarkRows["Angle"] = watermarkTable.addRow();
    watermarkTable.addTextCellInLastRow(this.loc.PropertyMain.Angle + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var watermarkAngleControl = this.TextBoxIntValue("pageSetupWatermarkAngle", 50);
    watermarkAngleControl.style.marginTop = "2px";
    watermarkAngleControl.action = function () { pageSample.update(); }
    watermarkTable.addCellInLastRow(watermarkAngleControl).className = "stiDesignerControlCellsBigIntervals";

    //Font
    pageSetupForm.watermarkRows["Font"] = watermarkTable.addRow();
    watermarkTable.addTextCellInLastRow(this.loc.PropertyMain.Font + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var fontTable = this.CreateHTMLTable();
    watermarkTable.addCellInLastRow(fontTable).className = "stiDesignerControlCellsBigIntervals";

    var fontName = this.FontList("pageSetupWatermarkFontName", 96);
    fontName.action = function () {
        if (this.key == "Aharoni") { this.jsObject.options.buttons.pageSetupWatermarkFontBold.setSelected(true); }
        this.jsObject.options.buttons.pageSetupWatermarkFontBold.isEnabled = !(this.key == "Aharoni");
        pageSample.update();
    };

    fontTable.addCell(fontName);

    var sizeItems = [];
    for (var i = 0; i < this.options.fontSizes.length; i++) {
        sizeItems.push(this.Item("sizesFont" + i, this.options.fontSizes[i], null, this.options.fontSizes[i]));
    }
    var fontSize = this.DropDownList("pageSetupWatermarkFontSize", 60, this.loc.HelpDesigner.FontSize, sizeItems, false);
    fontSize.action = function () {
        this.setKey(Math.abs(this.jsObject.StrToDouble(this.key)).toString());
        pageSample.update();
    }
    fontTable.addCell(fontSize).style.padding = "0 4px 0 4px";

    pageSetupForm.watermarkRows["Font2"] = watermarkTable.addRow();
    var fontTable2 = this.CreateHTMLTable();
    watermarkTable.addCellInLastRow();
    watermarkTable.addCellInLastRow(fontTable2).className = "stiDesignerControlCellsBigIntervals";

    var textBrushButton = this.BrushControl("pageSetupWatermarkTextBrush", null, "TextColor.png", this.loc.PropertyMain.TextBrush);
    textBrushButton.action = function () { pageSample.update(); }
    fontTable2.addCell(textBrushButton).style.padding = "0 2px 0 0";

    var boldButton = this.StandartSmallButton("pageSetupWatermarkFontBold", null, null, "Bold.png", this.loc.PropertyMain.Bold, null);
    boldButton.action = function () {
        this.setSelected(!this.isSelected);
        pageSample.update();
    }
    fontTable2.addCell(boldButton).style.padding = "0 2px 0 4px";

    var italicButton = this.StandartSmallButton("pageSetupWatermarkFontItalic", null, null, "Italic.png", this.loc.PropertyMain.Italic, null);
    italicButton.action = function () {
        this.setSelected(!this.isSelected);
        pageSample.update();
    }
    fontTable2.addCell(italicButton).style.padding = "0 2px 0 2px";

    var underlineButton = this.StandartSmallButton("pageSetupWatermarkFontUnderline", null, null, "Underline.png", this.loc.PropertyMain.Underline, null);
    underlineButton.action = function () {
        this.setSelected(!this.isSelected);
        pageSample.update();
    }
    fontTable2.addCell(underlineButton).style.padding = "0 2px 0 2px";

    var strikeoutButton = this.StandartSmallButton("pageSetupWatermarkFontStrikeout", null, null, "Strikeout.png", this.loc.PropertyMain.FontStrikeout, null);
    strikeoutButton.action = function () {
        this.setSelected(!this.isSelected);
        pageSample.update();
    }
    fontTable2.addCell(strikeoutButton).style.padding = "0 2px 0 2px";

    //Watermark Enabled
    pageSetupForm.watermarkRows["Enabled"] = watermarkTable.addRow();
    watermarkTable.addTextCellInLastRow(this.loc.PropertyMain.Enabled + ":").className = "stiDesignerCaptionControlsBigIntervals";
    var watermarkEnabledControl = this.DropDownList("pageSetupWatermarkEnabled", 167, null, this.GetWatermarkEnabledItems(), false, null, false, true);
    watermarkTable.addCellInLastRow(watermarkEnabledControl).className = "stiDesignerControlCellsBigIntervals";

    watermarkEnabledControl.action = function () {
        pageSetupForm.watermarkRows["EnabledExpression"].style.display = this.key == "Expression" ? "" : "none";
        pageSample.update();
    }

    //Watermark Enabled Expression
    pageSetupForm.watermarkRows["EnabledExpression"] = watermarkTable.addRow();
    watermarkTable.addTextCellInLastRow(this.loc.PropertyMain.Expression + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var watermarkExpressionControl = this.ExpressionControl("pageSetupWatermarkEnabledExpression", 180);
    watermarkExpressionControl.action = function () { pageSample.update(); }
    watermarkTable.addCellInLastRow(watermarkExpressionControl).className = "stiDesignerControlCellsBigIntervals";

    //Watermark Right To Left
    pageSetupForm.watermarkRows["RightToLeft"] = watermarkTable.addRow();
    var watermarkRightToLeftCell = watermarkTable.addCellInLastRow();
    watermarkRightToLeftCell.className = "stiDesignerCaptionControlsBigIntervals";
    watermarkRightToLeftCell.style.paddingTop = "6px";
    watermarkRightToLeftCell.style.paddingBottom = "6px";
    watermarkRightToLeftCell.setAttribute("colspan", "2");

    var watermarkRightToLeftControl = this.CheckBox("pageSetupWatermarkRightToLeft", this.loc.PropertyMain.RightToLeft);
    watermarkRightToLeftControl.action = function () { pageSample.update(); }
    watermarkRightToLeftCell.appendChild(watermarkRightToLeftControl);

    //Watermark Show Behind
    pageSetupForm.watermarkRows["ShowBehind"] = watermarkTable.addRow();
    var watermarkShowBehindCell = watermarkTable.addCellInLastRow();
    watermarkShowBehindCell.className = "stiDesignerCaptionControlsBigIntervals";
    watermarkShowBehindCell.style.paddingTop = "6px";
    watermarkShowBehindCell.style.paddingBottom = "6px";
    watermarkShowBehindCell.setAttribute("colspan", "2");

    var watermarkShowBehindControl = this.CheckBox("pageSetupWatermarkShowBehind", this.loc.PropertyMain.ShowBehind);
    watermarkShowBehindControl.action = function () { pageSample.update(); }
    watermarkShowBehindCell.appendChild(watermarkShowBehindControl);

    //Watermark Image
    watermarkTable.addRow();
    var cellWatermarkImage = watermarkTable.addCellInLastRow();
    cellWatermarkImage.style.padding = "6px 0 6px 0";
    cellWatermarkImage.setAttribute("colspan", "2");
    cellWatermarkImage.appendChild(this.FormBlockHeader(this.loc.Toolbars.ToolbarWatermarkImage));
    
    watermarkTable.addRow();
    var watermarkImage = this.ImageControl("pageSetupWatermarkImage", pageSetupForm.width - 167, 80);    
    watermarkImage.style.margin = "2px 0 2px 8px";
    watermarkTable.addCellInLastRow(watermarkImage).setAttribute("colspan", "2");

    //Watermark Image Gallery
    var watermarkImageGallery = this.ImageGallery("pageSetupWatermarkImageGallery", pageSetupForm.width - 151, 60);
    watermarkTable.addCellInNextRow(watermarkImageGallery).setAttribute("colspan", "2");
        
    watermarkImageGallery.action = function (item) {
        watermarkImage.setImage(item.itemObject.src);
        pageSample.update(watermarkImage.imageContainer.naturalWidth + ";" + watermarkImage.imageContainer.naturalHeight);
        watermarkImage.imageHyperlink = item.itemObject.type == "StiResource"
            ? this.jsObject.options.resourceIdent + item.itemObject.name
            : (item.itemObject.type == "StiVariable" ? this.jsObject.options.variableIdent + item.itemObject.name : "");
    }

    watermarkImageGallery.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        pageSetupForm.waterMarkSeparator.style.margin = state ? "0 0 6px 0" : "6px 0 6px 0";
    }

    watermarkImage.action = function () {
        var imageSize = null;
        if (this.src) {
            imageSize = this.imageContainer.naturalWidth + ";" + this.imageContainer.naturalHeight;
        }
        pageSample.update(imageSize);
        this.imageHyperlink = "";
        this.itemName = null;
        if (watermarkImageGallery.selectedItem) watermarkImageGallery.selectedItem.select(false);
    }

    //Image Align
    pageSetupForm.watermarkRows["ImageAlign"] = watermarkTable.addRow();
    watermarkTable.addTextCellInLastRow(this.loc.PropertyMain.ImageAlignment + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var imageAlignList = this.DropDownList("pageSetupWatermarkImageAlignment", 167, null, this.GetImageAlignItems(), true, false, null, true);
    imageAlignList.style.marginTop = "2px";
    imageAlignList.action = function () { pageSample.update(); }

    watermarkTable.addCellInLastRow(imageAlignList).className = "stiDesignerControlCellsBigIntervals";

    //MultipleFactor
    pageSetupForm.watermarkRows["MultipleFactor"] = watermarkTable.addRow();
    watermarkTable.addTextCellInLastRow(this.loc.PropertyMain.ImageMultipleFactor + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var watermarkMFactorControl = this.TextBoxDoubleValue("pageSetupWatermarkMultipleFactor", 50);
    watermarkMFactorControl.action = function () { pageSample.update(); }
    watermarkTable.addCellInLastRow(watermarkMFactorControl).className = "stiDesignerControlCellsBigIntervals";

    //Transparency
    pageSetupForm.watermarkRows["Transparency"] = watermarkTable.addRow();
    watermarkTable.addTextCellInLastRow(this.loc.PropertyMain.ImageTransparency + ":").className = "stiDesignerCaptionControlsBigIntervals";

    var watermarkTransparencyControl = this.TextBoxPositiveIntValue("pageSetupWatermarkTransparency", 50);
    watermarkTransparencyControl.action = function () { pageSample.update(); }
    watermarkTable.addCellInLastRow(watermarkTransparencyControl).className = "stiDesignerControlCellsBigIntervals";

    //AspectRatio
    pageSetupForm.watermarkRows["AspectRatio"] = watermarkTable.addRow();
    var watermarkAspectRatioCell = watermarkTable.addCellInLastRow();
    watermarkAspectRatioCell.className = "stiDesignerCaptionControlsBigIntervals";
    watermarkAspectRatioCell.style.paddingTop = "8px";
    watermarkAspectRatioCell.style.paddingBottom = "6px";
    watermarkAspectRatioCell.setAttribute("colspan", "2");

    var watermarkAspectRatioControl = this.CheckBox("pageSetupWatermarkAspectRatio", this.loc.PropertyMain.AspectRatio);
    watermarkAspectRatioControl.action = function () { pageSample.update(); }
    watermarkAspectRatioCell.appendChild(watermarkAspectRatioControl);

    //Show Image Behind
    pageSetupForm.watermarkRows["ShowImageBehind"] = watermarkTable.addRow();
    var watermarkShowImgBehindCell = watermarkTable.addCellInLastRow();
    watermarkShowImgBehindCell.className = "stiDesignerCaptionControlsBigIntervals";
    watermarkShowImgBehindCell.style.paddingTop = "6px";
    watermarkShowImgBehindCell.style.paddingBottom = "6px";
    watermarkShowImgBehindCell.setAttribute("colspan", "2");

    var watermarkShowImgBehindControl = this.CheckBox("pageSetupWatermarkShowImageBehind", this.loc.PropertyMain.ShowImageBehind);
    watermarkShowImgBehindControl.action = function () { pageSample.update(); }
    watermarkShowImgBehindCell.appendChild(watermarkShowImgBehindControl);

    //Image Stretch
    pageSetupForm.watermarkRows["ImageStretch"] = watermarkTable.addRow();
    var watermarkStretchCell = watermarkTable.addCellInLastRow();
    watermarkStretchCell.className = "stiDesignerCaptionControlsBigIntervals";
    watermarkStretchCell.style.paddingTop = "6px";
    watermarkStretchCell.style.paddingBottom = "6px";
    watermarkStretchCell.setAttribute("colspan", "2");

    var watermarkStretchControl = this.CheckBox("pageSetupWatermarkImageStretch", this.loc.PropertyMain.ImageStretch);
    watermarkStretchControl.action = function () { pageSample.update(); }
    watermarkStretchCell.appendChild(watermarkStretchControl);

    //Image Tiling
    pageSetupForm.watermarkRows["ImageTiling"] = watermarkTable.addRow();
    var watermarkTilingCell = watermarkTable.addCellInLastRow();
    watermarkTilingCell.className = "stiDesignerCaptionControlsBigIntervals";
    watermarkTilingCell.style.paddingTop = "6px";
    watermarkTilingCell.style.paddingBottom = "6px";
    watermarkTilingCell.setAttribute("colspan", "2");

    var watermarkTilingControl = this.CheckBox("pageSetupWatermarkImageTiling", this.loc.PropertyMain.ImageTiling);
    watermarkTilingControl.action = function () { pageSample.update(); }
    watermarkTilingCell.appendChild(watermarkTilingControl);

    //More
    watermarkTable.addRow();
    var separator = this.Separator();
    pageSetupForm.waterMarkSeparator = separator;
    separator.style.margin = "6px 0 6px 0";
    watermarkTable.addCellInLastRow(separator).setAttribute("colspan", "2");
    
    watermarkTable.addRow();
    var moreButton = this.FormButton(null, "pageSetupWatermarkMoreOptions", watermarkBlock.showAllOptions ? this.loc.Buttons.LessOptions : this.loc.Buttons.MoreOptions);
    moreButton.style.display = "inline-block";
    moreButton.style.margin = "2px 7px 4px 8px";
    moreButton.style.minWidth = "120px";
    var watermarkTextControlCell = watermarkTable.addCellInLastRow(moreButton);
    watermarkTextControlCell.setAttribute("colspan", "2");
    watermarkTextControlCell.style.textAlign = "right";
        
    moreButton.action = function () {
        pageSetupForm.changeVisibleStateMoreOptions(!watermarkBlock.showAllOptions);
        this.jsObject.SetObjectToCenter(pageSetupForm);
    }

    pageSetupForm.changeVisibleStateMoreOptions = function (state) {
        watermarkBlock.showAllOptions = state;
        for (var name in pageSetupForm.watermarkRows) {
            pageSetupForm.watermarkRows[name].style.display = state ? "" : "none";
        }
        pageSetupForm.watermarkRows["EnabledExpression"].style.display = state && watermarkEnabledControl.key == "Expression" ? "" : "none";
        moreButton.caption.innerHTML = state ? this.jsObject.loc.Buttons.LessOptions : this.jsObject.loc.Buttons.MoreOptions;
    }

    pageSetupForm.changeVisibleStateMoreOptions(watermarkBlock.showAllOptions);

    return watermarkBlock;
}