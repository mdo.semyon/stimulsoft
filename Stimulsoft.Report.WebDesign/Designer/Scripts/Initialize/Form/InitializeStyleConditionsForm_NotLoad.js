﻿
StiMobileDesigner.prototype.InitializeStyleConditionsForm_ = function () {
   
    var form = this.BaseForm("styleConditionsForm", this.loc.PropertyMain.Conditions, 3, this.HelpLinks["conditions"]);
    form.controls = {};   
    form.container.style.padding = "0px";

    //Toolbar
    var buttons = [
        ["addCondition", this.loc.Chart.AddCondition.replace("&", ""), "AddCondition.png"],
        ["removeCondition", null, "Remove.png"],
        ["separator"],
        ["moveUp", null, "MoveUp.png"],
        ["moveDown", null, "MoveDown.png"]
    ]

    var toolBar = this.CreateHTMLTable();
    form.container.appendChild(toolBar);

    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i][0] == "separator") {
            toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton(null, null, buttons[i][1], buttons[i][2], null, null, this.GetStyles("StandartSmallButton"), true);
        button.style.margin = "4px";
        form.controls[buttons[i][0]] = button;
        toolBar.addCell(button);
    }
    form.controls.moveUp.setEnabled(false);
    form.controls.moveDown.setEnabled(false);

    //Conditions Container
    var conditionsContainer = this.StyleConditionsContainer(form, null, 400);
    form.container.appendChild(conditionsContainer);
    conditionsContainer.style.minWidth = "650px";

    conditionsContainer.onAction = function () {
        var count = this.getCountItems();
        var index = this.selectedItem ? this.selectedItem.getIndex() : -1;
        form.controls.moveUp.setEnabled(index > 0);
        form.controls.moveDown.setEnabled(index != -1 && index < count - 1);
    }    

    form.controls.addCondition.action = function () {
        conditionsContainer.addCondition();
    }

    form.controls.removeCondition.action = function () {
        conditionsContainer.removeCondition();
    }
        
    form.controls.moveUp.action = function () {
        if (conditionsContainer.selectedItem) { conditionsContainer.selectedItem.move("Up"); }
    }

    form.controls.moveDown.action = function () {
        if (conditionsContainer.selectedItem) { conditionsContainer.selectedItem.move("Down"); }
    }

    form.show = function (styleDesignerForm, propertyControl) {
        this.styleDesignerForm = styleDesignerForm;
        this.propertyControl = propertyControl;

        var conditions = styleDesignerForm.stylesTree.selectedItem.itemObject.properties.conditions;
        conditionsContainer.clear();
        form.changeVisibleState(true);

        if (!conditions) return;
        for (var i = 0; i < conditions.length; i++) conditionsContainer.addCondition(conditions[i], true);
        conditionsContainer.onAction();
    }

    form.action = function () {        
        var conditions = [];

        for (var i = 0; i < conditionsContainer.childNodes.length; i++) {
            conditions.push(conditionsContainer.childNodes[i].getConditionObject());
        }

        this.styleDesignerForm.stylesTree.selectedItem.itemObject.properties.conditions = conditions;
        this.styleDesignerForm.isModified = true;
        this.propertyControl.value = "[" + (conditions.length > 0 ? this.jsObject.loc.PropertyMain.Conditions : this.jsObject.loc.FormConditions.NoConditions) + "]";

        form.changeVisibleState(false);
    }

    return form;
}

StiMobileDesigner.prototype.StyleConditionsContainer = function (styleConditionsForm, width, height) {
    var container = document.createElement("div");
    container.jsObject = this;
    if (width) container.style.width = width + "px";
    if (height) container.style.height = height + "px";
    container.className = "stiDesignerSortContainer";
    container.styleConditionsForm = styleConditionsForm;
    container.selectedItem = null;

    container.addCondition = function (conditionObject, notAction) {
        var conditionItem = this.jsObject.StyleConditionsItem(this, conditionObject);
        this.appendChild(conditionItem);        
        conditionItem.setSelected();
        styleConditionsForm.controls.removeCondition.setEnabled(true);
        if (!notAction) this.onAction();
    }

    container.removeCondition = function () {
        for (var i = 0; i < this.childNodes.length; i++) {
            if (this.childNodes[i].isSelected) {
                this.removeChild(this.childNodes[i]);
                this.selectedItem = null;
            }
        }
        if (this.childNodes.length > 0) this.childNodes[0].setSelected();
        styleConditionsForm.controls.removeCondition.setEnabled(this.childNodes.length > 0);
        this.onAction();
    }

    container.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        styleConditionsForm.controls.removeCondition.setEnabled(false);
        styleConditionsForm.controls.moveUp.setEnabled(false);
        styleConditionsForm.controls.moveDown.setEnabled(false);
    }

    container.getCountItems = function () {
        return container.childNodes.length;
    }

    container.onAction = function () { };

    return container;
}

StiMobileDesigner.prototype.StyleConditionsItem = function (container, conditionObject) {
    var conditionItem = document.createElement("div");
    conditionItem.jsObject = this;
    conditionItem.key = this.newGuid().replace(/-/g, '');
    conditionItem.container = container;
    conditionItem.isSelected = false;
    conditionItem.className = "stiDesignerSortPanel";
    conditionItem.innerTable = this.CreateHTMLTable();
    conditionItem.innerTable.style.margin = "5px";
    conditionItem.appendChild(conditionItem.innerTable);
    conditionItem.controls = {};
    if (!conditionObject) conditionObject = this.StyleConditionObject();

    var placementItems = [
       ["ReportTitle", this.loc.Components.StiReportTitleBand],
       ["ReportSummary", this.loc.Components.StiReportSummaryBand],
       ["PageHeader", this.loc.Components.StiPageHeaderBand],
       ["PageFooter", this.loc.Components.StiPageFooterBand],
       ["GroupHeader", this.loc.Components.StiGroupHeaderBand],
       ["GroupFooter", this.loc.Components.StiGroupFooterBand],
       ["Header", this.loc.Components.StiHeaderBand],
       ["Footer", this.loc.Components.StiFooterBand],
       ["ColumnHeader", this.loc.Components.StiColumnHeaderBand],
       ["ColumnFooter", this.loc.Components.StiColumnFooterBand],
       ["Data", this.loc.Components.StiDataBand],
       ["DataOddStyle", this.loc.PropertyMain.OddStyle],
       ["DataEvenStyle", this.loc.PropertyMain.EvenStyle],
       ["Table", this.loc.Components.StiTable],
       ["Hierarchical", this.loc.Components.StiHierarchicalBand],
       ["Child", this.loc.Components.StiChildBand],
       ["Empty", this.loc.Components.StiEmptyBand],
       ["Overlay", this.loc.Components.StiOverlayBand],
       ["Panel", this.loc.Components.StiPanel],
       ["Page", this.loc.Components.StiPage]
    ]

    var componentTypeItems = [
       ["Text", this.loc.PropertyEnum.StiStyleComponentTypeText],
       ["Primitive", this.loc.PropertyEnum.StiStyleComponentTypePrimitive],
       ["Image", this.loc.PropertyEnum.StiStyleComponentTypeImage],
       ["CrossTab", this.loc.PropertyEnum.StiStyleComponentTypeCrossTab],
       ["Chart", this.loc.PropertyEnum.StiStyleComponentTypeChart],
       ["CheckBox", this.loc.PropertyEnum.StiStyleComponentTypeCheckBox]
    ]

    var locationItems = [
       ["TopLeft", this.loc.PropertyEnum.ContentAlignmentTopLeft],
       ["TopCenter", this.loc.PropertyEnum.ContentAlignmentTopCenter],
       ["TopRight", this.loc.PropertyEnum.ContentAlignmentTopRight],
       ["MiddleLeft", this.loc.PropertyEnum.ContentAlignmentMiddleLeft],
       ["MiddleCenter", this.loc.PropertyEnum.ContentAlignmentMiddleCenter],
       ["MiddleRight", this.loc.PropertyEnum.ContentAlignmentMiddleRight],
       ["BottomLeft", this.loc.PropertyEnum.ContentAlignmentBottomLeft],
       ["BottomCenter", this.loc.PropertyEnum.ContentAlignmentBottomCenter],
       ["BottomRight", this.loc.PropertyEnum.ContentAlignmentBottomRight],
       ["Left", this.loc.PropertyEnum.StiHorAlignmentLeft],
       ["Right", this.loc.PropertyEnum.StiHorAlignmentRight],
       ["Top", this.loc.PropertyEnum.StiVertAlignmentTop],
       ["Bottom", this.loc.PropertyEnum.StiVertAlignmentBottom],
       ["CenterHorizontal", this.loc.Toolbars.CenterHorizontally],
       ["CenterVertical", this.loc.Toolbars.CenterVertically]
    ]

    var controlProps = [
        ["placementCheckBox", this.CheckBox(null, this.loc.PropertyEnum.StiStyleConditionTypePlacement), "4px 50px 4px 4px"],
        ["operationPlacement", this.DropDownList(null, 140, null, this.GetFilterConditionItems("Boolean"), true), "4px"],
        ["placement", this.StylePlacementCollectionControl(conditionItem.key + "placement", placementItems, 180), "4px"],
        ["placementNestedLevelCheckBox", this.CheckBox(null, this.loc.PropertyMain.NestedLevel), "4px 10px 4px 4px"],
        ["operationPlacementNestedLevel", this.DropDownList(null, 140, null, this.GetFilterConditionItems("Numeric", true), true), "4px"],
        ["placementNestedLevel", this.TextBoxEnumerator(null, 73, null, false, 10000000, 1), "4px"],
        ["componentTypeCheckBox", this.CheckBox(null, this.loc.PropertyEnum.StiStyleConditionTypeComponentType), "4px 50px 4px 4px"],
        ["operationComponentType", this.DropDownList(null, 140, null, this.GetFilterConditionItems("Boolean"), true), "4px"],
        ["componentType", this.CollectionControl(conditionItem.key + "componentType", componentTypeItems, 180), "4px"],
        ["locationCheckBox", this.CheckBox(null, this.loc.PropertyEnum.StiStyleConditionTypeLocation), "4px 50px 4px 4px"],
        ["operationLocation", this.DropDownList(null, 140, null, this.GetFilterConditionItems("Boolean"), true), "4px"],
        ["location", this.CollectionControl(conditionItem.key + "location", locationItems, 180), "4px"],
        ["componentNameCheckBox", this.CheckBox(null, this.loc.PropertyEnum.StiStyleConditionTypeComponentName), "4px 50px 4px 4px"],
        ["operationComponentName", this.DropDownList(null, 140, null, this.GetFilterConditionItems("String", true), true), "4px"],
        ["componentName", this.TextBox(200), "4px"]
    ]

    var nestedLevelTable = this.CreateHTMLTable();

    for (var i = 0; i < controlProps.length; i++) {
        var control = controlProps[i][1];
        control.style.margin = controlProps[i][2];
        conditionItem.controls[controlProps[i][0]] = control;

        if (controlProps[i][0] == "placementNestedLevelCheckBox") {
            conditionItem.innerTable.addCellInLastRow();
            conditionItem.innerTable.addCellInLastRow(nestedLevelTable).setAttribute("colspan", "3");
        }

        if (controlProps[i][0] == "placementNestedLevelCheckBox" || controlProps[i][0] == "operationPlacementNestedLevel" || controlProps[i][0] == "placementNestedLevel") {
            nestedLevelTable.addCell(control);
        }
        else {
            conditionItem.innerTable.addCellInLastRow(control);            
        }

        if (controlProps[i][0] == "placement" || controlProps[i][0] == "componentType" || controlProps[i][0] == "location" || controlProps[i][0] == "placementNestedLevel") {
            if (controlProps[i][0] != "placementNestedLevel") conditionItem.innerTable.addTextCellInLastRow(this.loc.FormBand.And).style.paddingLeft = "4px";
            conditionItem.innerTable.addRow();            
        }        
    }

    conditionItem.updateControls = function () {
        conditionItem.controls.operationPlacement.setEnabled(conditionItem.controls.placementCheckBox.isChecked);
        conditionItem.controls.placement.setEnabled(conditionItem.controls.placementCheckBox.isChecked);
        conditionItem.controls.placementNestedLevelCheckBox.setEnabled(conditionItem.controls.placementCheckBox.isChecked);
        conditionItem.controls.operationPlacementNestedLevel.setEnabled(conditionItem.controls.placementCheckBox.isChecked && conditionItem.controls.placementNestedLevelCheckBox.isChecked);
        conditionItem.controls.placementNestedLevel.setEnabled(conditionItem.controls.placementCheckBox.isChecked && conditionItem.controls.placementNestedLevelCheckBox.isChecked);
        conditionItem.controls.operationComponentType.setEnabled(conditionItem.controls.componentTypeCheckBox.isChecked);
        conditionItem.controls.componentType.setEnabled(conditionItem.controls.componentTypeCheckBox.isChecked);
        conditionItem.controls.operationLocation.setEnabled(conditionItem.controls.locationCheckBox.isChecked);
        conditionItem.controls.location.setEnabled(conditionItem.controls.locationCheckBox.isChecked);
        conditionItem.controls.operationComponentName.setEnabled(conditionItem.controls.componentNameCheckBox.isChecked);
        conditionItem.controls.componentName.setEnabled(conditionItem.controls.componentNameCheckBox.isChecked);
    }

    //Fill controls
    conditionItem.controls.placementCheckBox.setChecked(conditionObject.type.indexOf("Placement") >= 0);
    conditionItem.controls.placementNestedLevelCheckBox.setChecked(conditionObject.type.indexOf("PlacementNestedLevel") >= 0);
    conditionItem.controls.componentTypeCheckBox.setChecked(conditionObject.type.indexOf("ComponentType") >= 0);
    conditionItem.controls.locationCheckBox.setChecked(conditionObject.type.indexOf("Location") >= 0);
    conditionItem.controls.componentNameCheckBox.setChecked(conditionObject.type.indexOf("ComponentName") >= 0);
    conditionItem.controls.operationPlacement.setKey(conditionObject.operationPlacement);
    conditionItem.controls.placement.setKey(conditionObject.placement);
    conditionItem.controls.operationPlacementNestedLevel.setKey(conditionObject.operationPlacementNestedLevel);
    conditionItem.controls.placementNestedLevel.setValue(conditionObject.placementNestedLevel);
    conditionItem.controls.operationComponentType.setKey(conditionObject.operationComponentType);
    conditionItem.controls.componentType.setKey(conditionObject.componentType);
    conditionItem.controls.operationLocation.setKey(conditionObject.operationLocation);
    conditionItem.controls.location.setKey(conditionObject.location);
    conditionItem.controls.operationComponentName.setKey(conditionObject.operationComponentName);
    conditionItem.controls.componentName.value = conditionObject.componentName;
    conditionItem.updateControls();

    var checkBoxes = ["placementCheckBox", "placementNestedLevelCheckBox", "componentTypeCheckBox", "locationCheckBox", "componentNameCheckBox"];
    for (var i = 0; i < checkBoxes.length; i++) {
        conditionItem.controls[checkBoxes[i]].action = function () { conditionItem.updateControls(); }
    }

    conditionItem.getConditionObject = function () {
        var conditionObject = {};
        conditionObject.type = "";
        if (conditionItem.controls.placementCheckBox.isChecked) conditionObject.type += " Placement,";
        if (conditionItem.controls.placementNestedLevelCheckBox.isChecked && conditionItem.controls.placementCheckBox.isChecked) conditionObject.type += " PlacementNestedLevel,";
        if (conditionItem.controls.componentTypeCheckBox.isChecked) conditionObject.type += " ComponentType,";
        if (conditionItem.controls.locationCheckBox.isChecked) conditionObject.type += " Location,";
        if (conditionItem.controls.componentNameCheckBox.isChecked) conditionObject.type += " ComponentName,";
        conditionObject.operationPlacement = conditionItem.controls.operationPlacement.key;
        conditionObject.placement = conditionItem.controls.placement.key;
        conditionObject.operationPlacementNestedLevel = conditionItem.controls.operationPlacementNestedLevel.key;
        conditionObject.placementNestedLevel = conditionItem.controls.placementNestedLevel.textBox.value;
        conditionObject.operationComponentType = conditionItem.controls.operationComponentType.key;
        conditionObject.componentType = conditionItem.controls.componentType.key;
        conditionObject.operationLocation = conditionItem.controls.operationLocation.key;
        conditionObject.location = conditionItem.controls.location.key;
        conditionObject.operationComponentName = conditionItem.controls.operationComponentName.key;
        conditionObject.componentName = conditionItem.controls.componentName.value;

        return conditionObject;
    }

    conditionItem.setSelected = function () {
        for (var i = 0; i < this.container.childNodes.length; i++) {
            this.container.childNodes[i].className = "stiDesignerSortPanel";
            this.container.childNodes[i].isSelected = false;
        }
        this.container.selectedItem = this;
        this.isSelected = true;
        this.className = "stiDesignerSortPanelSelected";
    }

    conditionItem.onclick = function () {
        if (this.isTouchEndFlag || this.jsObject.options.isTouchClick) return;
        this.action();
    }

    conditionItem.ontouchend = function () {
        var this_ = this;
        this.isTouchEndFlag = true;
        clearTimeout(this.isTouchEndTimer);
        if (this.jsObject.options.fingerIsMoved) return;
        this.action();
        this.isTouchEndTimer = setTimeout(function () {
            this_.isTouchEndFlag = false;
        }, 1000);
    }

    conditionItem.action = function () {
        this.setSelected();
        container.onAction();
    }

    conditionItem.getIndex = function () {
        for (var i = 0; i < container.childNodes.length; i++)
            if (container.childNodes[i] == this) return i;
    };

    conditionItem.move = function (direction) {
        var index = this.getIndex();
        container.removeChild(this);
        var count = container.getCountItems();
        var newIndex = direction == "Up" ? index - 1 : index + 1;
        if (direction == "Up" && newIndex == -1) newIndex = 0;
        if (direction == "Down" && newIndex >= count) {
            container.appendChild(this);
            container.onAction();
            return;
        }
        container.insertBefore(this, container.childNodes[newIndex]);
        container.onAction();
    }

    return conditionItem;
}