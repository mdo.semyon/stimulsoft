﻿
StiMobileDesigner.prototype.InitializeMessageForm = function () {
    var messageForm = this.BaseForm("messageForm", " ", 4);
    messageForm.messageText = "";
    messageForm.messageImage = null;
    messageForm.caption.style.textAlign = "center";
    messageForm.container.style.fontSize = "13px";
    messageForm.container.style.fontFamily = "Arial";
    messageForm.container.style.padding = "20px 50px 20px 30px";
    messageForm.container.style.lineHeight = "1.5";
    messageForm.style.maxWidth = "600px";
    messageForm.style.minWidth = "500px";

    messageForm.onshow = function () {
        if (this.messageImage) {
            var innerTable = this.jsObject.CreateHTMLTable();
            var img = document.createElement("img");
            img.src = this.jsObject.options.images[this.messageImage];
            innerTable.addCell(img).style.padding = "0 20px 0 0";
            innerTable.addTextCell(this.messageText);
            this.container.innerHTML = "";
            this.container.appendChild(innerTable);
        }
        else {
            this.container.innerHTML = this.messageText;
        }
    }

    //Override 
    while (messageForm.buttonsPanel.childNodes[0]) {
        messageForm.buttonsPanel.removeChild(messageForm.buttonsPanel.childNodes[0]);
    }

    var buttonsTable = this.CreateHTMLTable();
    messageForm.buttonsPanel.appendChild(buttonsTable);

    //Yes
    messageForm.buttonYes = this.FormButton(messageForm, name + "ButtonYes", this.loc.FormFormatEditor.nameYes, null);
    messageForm.buttonYes.action = function () {
        this.form.changeVisibleState(false);
        this.form.action(true);
    };
    buttonsTable.addCell(messageForm.buttonYes).style.padding = "8px 0px 8px 8px";

    //No
    messageForm.buttonNo = this.FormButton(messageForm, name + "ButtonNo", this.loc.FormFormatEditor.nameNo, null);
    messageForm.buttonNo.action = function () {
        this.form.changeVisibleState(false);
        this.form.action(false);
    };
    buttonsTable.addCell(messageForm.buttonNo).style.padding = "8px";

    //Cancel
    messageForm.buttonCancel = this.FormButton(messageForm, name + "ButtonCancel", this.loc.Buttons.Cancel.replace("&", ""), null);
    messageForm.buttonCancel.style.margin = "8px 8px 8px 0px";
    messageForm.buttonCancel.action = function () {
        this.form.changeVisibleState(false);
    };
    buttonsTable.addCell(messageForm.buttonCancel);

    return messageForm;
}

StiMobileDesigner.prototype.MessageFormForSave = function () {
    var messageForm = this.InitializeMessageForm();

    messageForm.messageText = this.loc.Questions.qnSaveChanges.replace("{0}",
        this.options.report.properties.reportFile || Base64.decode(this.options.report.properties.reportName.replace("Base64Code;", "")));
    messageForm.caption.innerHTML = this.loc.FormDesigner.title.toUpperCase();
    messageForm.buttonCancel.style.display = "";

    return messageForm;
}

StiMobileDesigner.prototype.MessageFormForDelete = function () {
    var messageForm = this.InitializeMessageForm();
    
    messageForm.messageText = this.loc.Questions.qnRemove;
    messageForm.caption.innerHTML = this.loc.FormDesigner.title.toUpperCase();
    messageForm.buttonCancel.style.display = "none";

    return messageForm;
}

StiMobileDesigner.prototype.MessageFormForReplaceItem = function (itemName) {
    var messageForm = this.InitializeMessageForm();
    
    messageForm.messageText = this.loc.Questions.qnReplace.replace("{0}", "\"" + itemName + "\"");
    messageForm.caption.innerHTML = this.loc.FormDesigner.title.toUpperCase();
    messageForm.buttonCancel.style.display = "";

    return messageForm;
}

StiMobileDesigner.prototype.MessageFormForApplyStyles = function () {
    var messageForm = this.InitializeMessageForm();
    
    messageForm.messageText = this.loc.FormStyleDesigner.qnApplyStyleCollection;
    messageForm.caption.innerHTML = this.loc.FormDesigner.title.toUpperCase();
    messageForm.buttonCancel.style.display = "none";

    return messageForm;
}

StiMobileDesigner.prototype.MessageFormForRenderReportInCompilationMode = function () {
    var messageForm = this.InitializeMessageForm();

    messageForm.messageImage = "ReportChecker.Warning32.png";
    messageForm.messageText = "    This report is in the compilation mode. To continue, the report will be converted to the interpretation mode.";
    messageForm.caption.innerHTML = this.loc.FormDesigner.title.toUpperCase();
    messageForm.buttonYes.caption.innerHTML = this.loc.DesignerFx.Continue;
    messageForm.buttonNo.caption.innerHTML = this.loc.PropertyEnum.DialogResultCancel;
    messageForm.buttonCancel.style.display = "none";

    return messageForm;
}

StiMobileDesigner.prototype.MessageFormForSaveReportToCloud = function () {
    var messageForm = this.InitializeMessageForm();

    messageForm.messageImage = "ReportChecker.Information32.png";
    messageForm.messageText = this.loc.Messages.ShareYourReportYouShouldSave;
    messageForm.caption.innerHTML = this.loc.FormDesigner.title.toUpperCase();
    messageForm.buttonYes.caption.innerHTML = this.loc.Buttons.Save;
    messageForm.buttonNo.caption.innerHTML = this.loc.Buttons.Cancel.replace("&", "");
    messageForm.buttonCancel.style.display = "none";

    return messageForm;
}

StiMobileDesigner.prototype.InitializeErrorMessageForm = function () {
    var form = this.BaseForm("errorMessageForm", this.loc.Errors.Error, 4);
    form.container.style.borderTop = "0px";
    form.buttonCancel.style.display = "none";
    form.caption.style.textAlign = "center";
    form.container.style.fontSize = "14px";
    form.container.style.fontFamily = "Arial";

    var table = this.CreateHTMLTable();
    form.container.appendChild(table);

    form.image = document.createElement("img");
    form.image.style.padding = "15px";
    form.image.src = this.options.images["ReportChecker.Error32.png"];
    table.addCellInLastRow(form.image);

    form.description = table.addCellInLastRow();
    form.description.className = "stiDesignerMessagesFormDescription";

    form.show = function (messageText, messageType) {
        if (this.jsObject.options.ignoreAllErrors) return;
        if (this.visible && this.jsObject.options.jsMode) {
            this.description.innerHTML += "<br/><br/>" + messageText;
            this.jsObject.SetObjectToCenter(this);
            return;
        }
        if (this.jsObject.options.forms.errorMessageForm) { //Fixed Bug
            this.jsObject.options.mainPanel.removeChild(this.jsObject.options.forms.errorMessageForm);
            this.jsObject.options.mainPanel.appendChild(this.jsObject.options.forms.errorMessageForm);
        }

        this.caption.innerHTML = this.jsObject.loc.FormDesigner.title;

        if (messageType == "Warning")
            this.image.src = this.jsObject.options.images["ReportChecker.Warning32.png"];
        else if (messageType == true || messageType == "Info")
            this.image.src = this.jsObject.options.images["ReportChecker.Information32.png"]; //messageType === true - for backward compatibility
        else {
            this.image.src = this.jsObject.options.images["ReportChecker.Error32.png"];
            this.caption.innerHTML = this.jsObject.loc.Errors.Error;
        }

        this.changeVisibleState(true);
        this.description.innerHTML = messageText;
        var processImage = this.jsObject.options.processImage || this.jsObject.InitializeProcessImage();
        processImage.hide();
    }

    form.action = function () {
        this.changeVisibleState(false);
        if (this.onAction) {
            this.onAction();
            this.onAction = null;
        }
    }

    return form;
}

StiMobileDesigner.prototype.InitializeMessageFormForChangeRequestTimeout = function () {
    var form = this.BaseForm("formChangeRequestTimeout", this.loc.FormDesigner.title.toUpperCase(), 4);    
    form.caption.style.textAlign = "center";
    form.container.style.fontSize = "13px";
    form.container.style.fontFamily = "Arial";
    form.container.style.padding = "20px 50px 20px 30px";
    form.container.style.lineHeight = "1.6";
    form.style.maxWidth = "600px";
    form.style.minWidth = "500px";
    form.buttonCancel.style.display = "none";

    var innerTable = this.CreateHTMLTable();
    var img = document.createElement("img");
    img.src = this.options.images["ReportChecker.Information32.png"];
    innerTable.addCell(img).style.padding = "0 20px 0 0";
    var textCell = innerTable.addTextCell(this.loc.Messages.ChangeRequestTimeout);
    form.container.appendChild(innerTable);

    form.show = function (timeoutValue, sqlTimeoutValue) {
        textCell.innerHTML = this.jsObject.loc.Messages.ChangeRequestTimeout.replace("{0}", sqlTimeoutValue);
        form.changeVisibleState(true);
    }

    return form;
}

StiMobileDesigner.prototype.MessageFormForDeleteUsedResource = function () {
    var form = this.InitializeMessageForm();
    form.onshow = function () { };
    form.caption.innerHTML = this.loc.FormDesigner.title.toUpperCase();
    form.buttonYes.caption.innerHTML = this.loc.Buttons.ForceDelete;
    form.buttonNo.caption.innerHTML = this.loc.Buttons.Cancel.replace("&", "");
    form.buttonCancel.style.display = "none";
    form.container.innerHTML = "";
    form.container.style.textAlign = "right";
    form.container.style.padding = "0";
    form.container.style.width = "500px";

    var innerTable = this.CreateHTMLTable();
    var img = document.createElement("img");
    img.src = this.options.images["ReportChecker.Error32.png"];
    innerTable.addCell(img).style.padding = "20px";
    var textCell = innerTable.addTextCell();
    textCell.style.paddingRight = "20px";
    textCell.style.textAlign = "left";
    form.container.appendChild(innerTable);

    var whereUsedButton = this.SmallButton(null, null, this.loc.Cloud.ButtonWhereUsed, null, null, null, this.GetStyles("HyperlinkButton"));
    whereUsedButton.style.display = "inline-block";
    whereUsedButton.style.marginRight = "5px";
    form.container.appendChild(whereUsedButton);

    var sep = this.FormSeparator();
    sep.style.display = "none";
    form.container.appendChild(sep);

    var itemsContainer = this.EasyContainer(490);
    itemsContainer.style.padding = "0 10px 10px 0";
    itemsContainer.style.textAlign = "left";
    itemsContainer.style.display = "none";
    form.container.appendChild(itemsContainer);

    whereUsedButton.action = function () {
        itemsContainer.style.display = sep.style.display = sep.style.display == "none" ? "" : "none";
    }

    form.show = function (resourceName, usedObjects) {
        itemsContainer.clear();
        for (var i = 0; i < usedObjects.length; i++) {
            var text = (!usedObjects[i].alias || usedObjects[i].name == usedObjects[i].alias) ? usedObjects[i].name : usedObjects[i].name + " [" + usedObjects[i].alias + "]";
            var imageName = (usedObjects[i].typeItem == "Component" ? usedObjects[i].type : "Connections.BigDataSource") + ".png"
            var item = this.jsObject.OneItemContainerItem({ name: text }, null, imageName);
            itemsContainer.appendChild(item);
        }
        textCell.innerHTML = this.jsObject.loc.Messages.ResourceCannotBeDeleted.replace("{0}", resourceName);
        this.changeVisibleState(true);
    }

    return form;
}