﻿
StiMobileDesigner.prototype.BaseForm = function (name, caption, level, helpUrl) {
    var form = document.createElement("div");
    form.style.webkitAppRegion = "no-drag"; //for node js
    form.name = name != null ? name : this.generateKey();
    form.id = this.options.mobileDesigner.id + form.name;
    if (this.options.forms[name] != null) {
        this.options.mainPanel.removeChild(this.options.forms[name]);
    }
    if (name != null) this.options.forms[name] = form;
    this.options.mainPanel.appendChild(form);
    form.className = "stiDesignerForm";
    form.jsObject = this;
    form.name = name;
    form.level = level;
    form.helpUrl = helpUrl;
    form.caption = null;
    form.visible = false;
    form.style.display = "none";
    form.style.zIndex = level * 10 + 1;
    this.options.forms[name] = form;

    //Header
    form.header = document.createElement("div");
    form.header.thisForm = form;
    form.appendChild(form.header);
    form.header.className = "stiDesignerFormHeader";
    var headerTable = this.CreateHTMLTable();
    headerTable.style.width = "100%";
    form.header.appendChild(headerTable);

    form.caption = headerTable.addCell();
    if (caption != null || typeof (caption) == "undefined") {
        if (caption) form.caption.innerHTML = caption;
        form.caption.style.textAlign = "left";
        form.caption.style.padding = "0px 10px 0 15px";
    }

    //Help Button
    if (helpUrl && (this.options.showDialogHelp || this.options.jsMode)) {
        form.buttonHelp = this.StandartSmallButton(name + "HelpButton", null, null, "HelpIcon.png", null, null);
        form.buttonHelp.style.display = "inline-block";
        form.buttonHelp.allwaysEnabled = true;
        form.buttonHelp.form = form;
        form.buttonHelp.action = function () { this.jsObject.ShowHelpWindow(this.form.helpUrl); };
        var helpButtonCell = headerTable.addCell(form.buttonHelp);
        helpButtonCell.style.verticalAlign = "top";
        helpButtonCell.style.width = "20px";
        helpButtonCell.style.textAlign = "right";
        helpButtonCell.style.padding = "2px 0px 1px 0px";
    }

    //Close Button
    form.buttonClose = this.StandartSmallButton(name + "CloseButton", null, null, "CloseForm.png", null, null);
    form.buttonClose.style.display = "inline-block";
    form.buttonClose.allwaysEnabled = true;
    form.buttonClose.form = form;
    form.buttonClose.action = function () {
        if (form["cancelAction"]) this.form.cancelAction();
        form.changeVisibleState(false);
    };
    var closeButtonCell = headerTable.addCell(form.buttonClose);
    closeButtonCell.style.verticalAlign = "top";
    closeButtonCell.style.width = "20px";
    closeButtonCell.style.textAlign = "right";
    closeButtonCell.style.padding = "2px 1px 1px 0px";

    //Container
    form.container = document.createElement("div");
    form.appendChild(form.container);
    form.container.className = "stiDesignerFormContainer";

    //Separator
    var separator = document.createElement("div");
    form.appendChild(separator);
    separator.className = "stiDesignerFormSeparator";
    form.buttonsSeparator = separator;

    //Buttons
    form.buttonsPanel = document.createElement("div");
    form.appendChild(form.buttonsPanel);
    form.buttonsPanel.className = "stiDesignerFormButtonsPanel";
    var buttonsTable = this.CreateHTMLTable();
    form.buttonsPanel.appendChild(buttonsTable);

    form.buttonOk = this.FormButton(form, name + "ButtonOk", this.loc.Buttons.Ok.replace("&", ""), null);
    form.buttonOk.action = function () { this.form.action(); };
    buttonsTable.addCell(form.buttonOk).style.lineHeight = "0";
    form.buttonOk.style.margin = "8px";
    form.buttonOk.style.display = "inline-block";

    form.buttonCancel = this.FormButton(form, name + "ButtonCancel", this.loc.Buttons.Cancel.replace("&", ""), null);
    form.buttonCancel.style.display = "inline-block";
    form.buttonCancel.action = function () {
        if (form["cancelAction"]) form.cancelAction();
        form.changeVisibleState(false);
    };
    form.buttonCancel.style.margin = "8px 8px 8px 0";
    buttonsTable.addCell(form.buttonCancel).style.lineHeight = "0";

    form.changeVisibleState = function (state) {
        if (state) {
            this.style.display = "";
            
            if (this.jsObject.options.currentForm && this.parentElement == this.jsObject.options.mainPanel) {
                this.jsObject.options.mainPanel.removeChild(this);
                this.jsObject.options.mainPanel.appendChild(this);
            }
            
            this.onshow();
            this.jsObject.SetObjectToCenter(this);
            if (!this.isNotModal) {
                if (!this.jsObject.options.disabledPanels) this.jsObject.InitializeDisabledPanels();
                this.jsObject.options.disabledPanels[this.level].changeVisibleState(true);
            }
            this.visible = true;
            
            this.currentFormDownLevel = this.jsObject.options.currentForm && this.jsObject.options.currentForm.visible
                ? this.jsObject.options.currentForm : null;
            this.jsObject.options.currentForm = this;

            d = new Date();
            var endTime = d.getTime() + this.jsObject.options.formAnimDuration;
            this.flag = false;
            this.jsObject.ShowAnimationForm(this, endTime);
            this.movedByUser = false;
        }
        else {
            clearTimeout(this.animationTimer);
            this.visible = false;
            this.jsObject.options.currentForm = this.currentFormDownLevel || null;
            this.style.display = "none";
            if (!this.jsObject.options.forms[this.name]) {
                this.jsObject.options.mainPanel.removeChild(this);
            }
            this.onhide();
            if (!this.isNotModal) {
                if (!this.jsObject.options.disabledPanels) this.jsObject.InitializeDisabledPanels();
                this.jsObject.options.disabledPanels[this.level].changeVisibleState(false);
            }
        }
    }

    form.action = function () { };
    form.onshow = function () { };
    form.oncompleteshow = function () { };
    form.onhide = function () { };

    form.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        this.ontouchstart(true);
    }

    form.ontouchstart = function (mouseProcess) {
        var this_ = this;
        this.isTouchStartFlag = mouseProcess ? false : true;
        clearTimeout(this.isTouchStartTimer);
        this.jsObject.options.formPressed = this;
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    form.header.saveStartPosition = function (event, currStartX, currStartY) {
        var formStartX = form.jsObject.FindPosX(form, "stiDesignerMainPanel");
        var formStartY = form.jsObject.FindPosY(form, "stiDesignerMainPanel");
        form.jsObject.options.formInDrag = [currStartX, currStartY, formStartX, formStartY, form];
    }

    //Mouse Events
    form.header.onmousedown = function (event) {
        if (form.jsObject.options.buttonPressed &&
            (form.jsObject.options.buttonPressed == form.buttonClose ||
            form.jsObject.options.buttonPressed == form.buttonHelp))
        {
            return;
        }
        if (!event || form.isTouchStartFlag) return;
        var mouseStartX = event.clientX;
        var mouseStartY = event.clientY;
        form.header.saveStartPosition(event, mouseStartX, mouseStartY);
    }

    //Touch Events
    form.header.ontouchstart = function (event) {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        var fingerStartX = event.touches[0].pageX;
        var fingerStartY = event.touches[0].pageY;
        form.header.saveStartPosition(event, fingerStartX, fingerStartY);
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    form.header.ontouchmove = function (event) {
        if (event) event.preventDefault();

        if (form.jsObject.options.formInDrag) {
            var formInDrag = this.thisForm.jsObject.options.formInDrag;
            var formStartX = formInDrag[2];
            var formStartY = formInDrag[3];
            var fingerCurrentXPos = event.touches[0].pageX;
            var fingerCurrentYPos = event.touches[0].pageY;
            var deltaX = formInDrag[0] - fingerCurrentXPos;
            var deltaY = formInDrag[1] - fingerCurrentYPos;
            var newPosX = formStartX - deltaX;
            var newPosY = formStartY - deltaY;
            formInDrag[4].style.left = newPosX + "px";
            formInDrag[4].style.top = newPosY + "px";
            formInDrag.movedByUser = true;
        }
    }

    form.header.ontouchend = function (event) {
        if (event) event.preventDefault();
        form.jsObject.options.formInDrag = false;
    }

    //Form Move
    form.move = function (evnt) {
        var leftPos = this.jsObject.options.formInDrag[2] + (evnt.clientX - this.jsObject.options.formInDrag[0]);
        var topPos = this.jsObject.options.formInDrag[3] + (evnt.clientY - this.jsObject.options.formInDrag[1]);
        this.style.left = leftPos > 0 ? leftPos + "px" : 0;
        this.style.top = topPos > 0 ? topPos + "px" : 0;
        this.movedByUser = true;
    }

    form.addControlRow = function (table, textControl, controlName, control, margin) {
        if (!this.controls) this.controls = {};
        this.controls[controlName] = control;
        this.controls[controlName + "Row"] = table.addRow();

        if (textControl != null) {
            var text = table.addCellInLastRow();
            this.controls[controlName + "Text"] = text;
            text.innerHTML = textControl;
            text.className = "stiDesignerCaptionControls";
            text.style.paddingLeft = "12px";
            text.style.minWidth = "100px";
        }

        if (control) {
            control.form = this;
            control.style.margin = margin;
            var controlCell = table.addCellInLastRow(control);
            if (textControl == null) controlCell.setAttribute("colspan", 2);
        }

        return controlCell;
    }

    return form;
}

StiMobileDesigner.prototype.FormBlock = function (width, height) {
    var formBlock = document.createElement("div");
    formBlock.className = "stiDesignerFormBlock";
    if (width) formBlock.style.minWidth = width + "px";
    if (height) formBlock.style.minHeight = height + "px";

    return formBlock;
}

StiMobileDesigner.prototype.FormBlockHeader = function (caption) {
    var formBlockHeader = document.createElement("div");
    formBlockHeader.className = "stiDesignerFormBlockHeader";

    var formBlockCaption = document.createElement("div");
    formBlockHeader.caption = formBlockCaption;
    formBlockCaption.style.padding = "6px 6px 6px 15px";
    formBlockCaption.innerHTML = "<b>" + caption + "<b>";
    formBlockHeader.appendChild(formBlockCaption);

    return formBlockHeader;
}

StiMobileDesigner.prototype.FormSeparator = function () {
    var separator = document.createElement("div");
    separator.className = "stiDesignerFormSeparator";

    return separator;
}

