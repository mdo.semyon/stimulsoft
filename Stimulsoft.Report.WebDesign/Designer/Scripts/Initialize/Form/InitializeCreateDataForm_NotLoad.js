﻿
StiMobileDesigner.prototype.InitializeCreateDataForm_ = function () {
    var form = this.BaseForm("createDataForm", " ", 1);
    form.controls = {};

    //Tabs
    var tabs = [];
    tabs.push({ "name": "Data", "caption": this.loc.PropertyCategory.DataCategory });
    tabs.push({ "name": "Styles", "caption": this.loc.PropertyMain.Styles });
    var tabbedPane = this.TabbedPane("createDataFormTabbedPane", tabs, this.GetStyles("StandartTab"));
    form.tabbedPane = tabbedPane;
    form.container.appendChild(tabbedPane);

    //Hide Header Caption
    form.caption.innerHTML = "";
    form.caption.appendChild(tabbedPane.tabsPanel);
    form.caption.style.padding = "0px";
    form.container.style.borderTop = "0px";

    for (var i = 0; i < tabs.length; i++) {
        var tabsPanel = tabbedPane.tabsPanels[tabs[i].name];
        tabsPanel.style.width = this.options.isTouchDevice ? "640px" : "635px";
        tabsPanel.style.height = this.options.isTouchDevice ? "470px" : "460px";
        tabsPanel.appendChild(this.FormSeparator());

        switch (tabs[i].name) {
            case "Data": this.InitializeCreateDataFormDataPanel(form, tabsPanel); break;
            case "Styles": this.InitializeCreateDataFormStylesPanel(form, tabsPanel); break;
        }
    }

    form.show = function (dataSource, point, pageName) {
        form.changeVisibleState(true);
        tabbedPane.showTabPanel("Data");
        form.dataSource = dataSource;
        form.point = point;
        form.pageName = pageName;
        form.controls.columnsContainer.clear();
        form.controls.columnsTree.build(dataSource);
        form.controls.themesContainer.update();
    }

    form.action = function () {
        var params = {
            dataSource: form.dataSource,
            point: form.point,
            pageName: form.pageName,
            columns: [],
            settings: {
                data: form.controls.data.isChecked,
                table: form.controls.table.isChecked,
                header: form.controls.header.isChecked,
                footer: form.controls.footer.isChecked
            },
            theme: form.selectedThemeButton ? form.selectedThemeButton.key : null
        };
        
        for (var i = 0; i < form.controls.columnsContainer.childNodes.length; i++) {
            params.columns.push(form.controls.columnsContainer.childNodes[i].itemObject);
        }

        form.changeVisibleState(false);
        this.jsObject.SendCommandCreateDataComponent(params);
    }

    return form;
}

StiMobileDesigner.prototype.InitializeCreateDataFormDataPanel = function (form, parentPanel) {
    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerCreateDataMainTable";
    parentPanel.appendChild(mainTable);

    //Columns Tree
    var columnsTree = this.Tree(300, 350);
    form.controls.columnsTree = columnsTree;
    columnsTree.className = "stiDesignerCreateDataColumsTree";
    mainTable.addCell(columnsTree);

    columnsTree.itemFromObject = function (itemObject) {
        var captionText = this.jsObject.options.dictionaryTree ? this.jsObject.options.dictionaryTree.getItemCaption(itemObject) : itemObject.name;
        var item = this.jsObject.TreeItem(captionText, itemObject.typeIcon + ".png", itemObject, columnsTree, true);

        item.getFullName = function (useNameInSourcesInRelation) {
            var currItem = this;
            var fullName = "";
            while (currItem.parent != null) {
                if (fullName != "") fullName = "." + fullName;
                if (currItem.itemObject.typeItem == "Relation" && useNameInSourcesInRelation && currItem.itemObject.nameInSource) {
                    fullName = currItem.itemObject.nameInSource + fullName;
                }
                else {
                    fullName = (currItem.itemObject.correctName || currItem.itemObject.name) + fullName;
                }
                currItem = currItem.parent;
            }
            return (currItem.itemObject.typeItem == "BusinessObject" ? currItem.itemObject.fullName : (currItem.itemObject.correctName || currItem.itemObject.name)) + "." + fullName;
        }

        return item;
    }

    columnsTree.build = function (dataSource) {
        columnsTree.clear();
        columnsTree.rootItem = this.itemFromObject(dataSource);
        columnsTree.rootItem.style.margin = "8px";
        columnsTree.rootItem.setOpening(true);
        columnsTree.appendChild(columnsTree.rootItem);
        columnsTree.addTreeItems(dataSource.relations, columnsTree.rootItem);
        columnsTree.addTreeItems(dataSource.columns, columnsTree.rootItem);
    }

    columnsTree.addTreeItems = function (collection, parentItem) {
        if (collection) {
            for (var i = 0; i < collection.length; i++) {
                var childItem = parentItem.addChild(columnsTree.itemFromObject(collection[i]));
                if (collection[i].typeItem == "Relation") {
                    this.addTreeItems(collection[i].relations, childItem);

                    var dataSource = this.jsObject.GetDataSourceByNameFromDictionary(collection[i].parentDataSource);
                    if (dataSource && dataSource.columns) {
                        this.addTreeItems(dataSource.columns, childItem);
                    }
                }
            }
        }
    }

    //Columns Container
    var columnsContainer = this.EasyContainer(300, this.options.isTouchDevice ? 395 : 390);
    form.controls.columnsContainer = columnsContainer;
    mainTable.addCell(columnsContainer).setAttribute("rowspan", "2");

    columnsTree.onChecked = function (item) {
        var checkItem = function (parentItem) {
            for (var i = 0; i < parentItem.childsContainer.childNodes.length; i++) {
                var item = parentItem.childsContainer.childNodes[i];
                if (item.childsContainer && item.childsContainer.childNodes.length > 0) {
                    checkItem(item);
                }
                if (item.itemObject && item.itemObject.typeItem == "Column") {
                    var fullName = item.getFullName();
                    if (item.isChecked) {
                        if (!columnsContainer.isContained(fullName)) {
                            var newItem = columnsContainer.addItem(fullName, item.itemObject, fullName, item.itemObject.typeIcon + ".png");
                            newItem.itemObject.fullName = fullName;
                            if (item.itemObject.type == "byte[]" || item.itemObject.type == "image") {
                                newItem.itemObject.imageColumnFullName = item.getFullName(true);
                            }
                        }
                    }
                    else {
                        columnsContainer.removeItem(fullName);
                    }
                }
            }
        }
        checkItem(columnsTree.rootItem);
    };

    //Container Buttons Up & Down
    var containerButtons = mainTable.addCell();
    containerButtons.setAttribute("rowspan", "2");
    containerButtons.style.verticalAlign = "top";
    var buttonUp = this.SmallButton("createDataFormButtonUp", null, null, "ArrowUp.png", null, null, this.GetStyles("FormButton"), true);
    buttonUp.style.margin = "4px";
    buttonUp.setEnabled(false);
    var buttonDown = this.SmallButton("createDataFormButtonUp", null, null, "ArrowDown.png", null, null, this.GetStyles("FormButton"), true);
    buttonDown.style.margin = "0px 4px 4px 4px";
    buttonDown.setEnabled(false);
    containerButtons.appendChild(buttonUp);
    containerButtons.appendChild(buttonDown);

    buttonUp.action = function () {
        if (columnsContainer.selectedItem) { columnsContainer.selectedItem.move("Up"); }
    }

    buttonDown.action = function () {
        if (columnsContainer.selectedItem) { columnsContainer.selectedItem.move("Down"); }
    }

    columnsContainer.onAction = function () {
        var count = columnsContainer.getCountItems();
        var index = columnsContainer.selectedItem ? columnsContainer.selectedItem.getIndex() : -1;
        buttonUp.setEnabled(index > 0);
        buttonDown.setEnabled(index != -1 && index < count - 1);
    }

    // Mark All & Reset
    var buttonsPanel = mainTable.addCellInNextRow();
    var buttonsTable = this.CreateHTMLTable();
    buttonsTable.className = "stiDesignerCreateDataColumsButtonsTable";
    buttonsTable.setAttribute("align", "right");
    buttonsPanel.appendChild(buttonsTable);
    var markAllButton = this.FormButton(null, "createDataFormMarkAll", this.loc.Wizards.MarkAll.replace("&", ""), null);
    var resetButton = this.FormButton(null, "createDataFormReset", this.loc.Wizards.Reset.replace("&", ""), null);
    buttonsTable.addCell(markAllButton).style.padding = "8px";
    buttonsTable.addCell(resetButton).style.padding = "8px 8px 8px 0";

    markAllButton.action = function () {
        columnsTree.rootItem.setChecked(true);
        columnsTree.rootItem.checkBox.action();
    }

    resetButton.action = function () {
        columnsTree.rootItem.setChecked(false);
        columnsTree.rootItem.checkBox.action();
    }

    //Additional Controls
    var controlsTable = this.CreateHTMLTable();
    parentPanel.appendChild(controlsTable);
    var controlProps = [
        ["data", this.RadioButton("createDataFormDataRadioButton", "createDataForm", this.loc.PropertyMain.Data)],
        ["header", this.CheckBox("createDataFormHeaderCheckBox", this.loc.Components.StiHeaderBand)],
        ["table", this.RadioButton("createDataFormTableRadioButton", "createDataForm", this.loc.Components.StiTable)],
        ["footer", this.CheckBox("createDataFormFooterCheckBox", this.loc.Components.StiFooterBand)]
    ]

    for (var i = 0; i < controlProps.length; i++) {
        if (i == 0 || i == 2) controlsTable.addRow();
        var control = controlProps[i][1];
        form.controls[controlProps[i][0]] = control;
        controlsTable.addCellInLastRow(control).style.padding = i <= 1 ? "10px 60px 5px 10px" : "5px 60px 10px 10px";
        if (controlProps[i][0] == "data") control.setChecked(true);
    }
}

StiMobileDesigner.prototype.InitializeCreateDataFormStylesPanel = function (form, parentPanel) {
    parentPanel.appendChild(this.FormBlockHeader(this.loc.Wizards.Themes));
    var themesContainer = this.ThemesContainer();
    form.controls.themesContainer = themesContainer;
    parentPanel.appendChild(themesContainer);

    themesContainer.update = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);

        var noneButton = form.jsObject.ThemeButton(form, form.jsObject.loc.PropertyEnum.StiCheckStyleNone, "ReportThemeNone.png", null);
        themesContainer.appendChild(noneButton);
        noneButton.action();

        if (form.jsObject.options.report.stylesCollection) {
            var stylesCollection = form.jsObject.options.report.stylesCollection;
            var collectionNames = {};
            for (var i = 0; i < stylesCollection.length; i++) {
                var collectionName = stylesCollection[i].properties.collectionName;
                if (collectionName != "" && !collectionNames[collectionName]) {
                    collectionNames[collectionName] = collectionName;
                    var collectionButton = form.jsObject.ThemeButton(form, collectionName, "ReportThemeNone.png", { type: "User", name: collectionName });                    
                    themesContainer.appendChild(collectionButton);
                }
            }
        }
    }

    parentPanel.appendChild(this.FormBlockHeader(this.loc.Wizards.DefaultThemes));
    var defThemesContainer = this.ThemesContainer();
    parentPanel.appendChild(defThemesContainer);

    var themeNames = ["Red", "Green", "Blue", "Gray"];
    for (var i = 0; i < 4; i++) {
        for (var k = 0; k < 4; k++) {
            var themePercent = 100 - k * 25;
            var themeName = themeNames[i] + "_" + themePercent;
            var button = this.ThemeButton(form, this.loc.PropertyColor[themeNames[i]] + " " + themePercent + "%",
                "ReportTheme" + themeNames[i] + themePercent + ".png", { type: "Default", name: themeNames[i] + "_" + themePercent });
            button.caption.style.fontSize = "12px";
            button.caption.style.whiteSpace = "nowrap";
            defThemesContainer.appendChild(button);
        }
    }
}

StiMobileDesigner.prototype.ThemesContainer = function () {
    var container = document.createElement("div");
    container.className = "styleDesignerItemsContainer";
    container.style.width = "635px";
    container.style.height = "204px";

    return container;
}

StiMobileDesigner.prototype.ThemeButton = function (form, caption, imageName, key) {
    var button = this.StandartBigButton(null, null, caption, imageName, caption);
    button.style.float = "left";
    button.style.display = "inline-block";
    button.key = key;
    button.style.margin = "4px";

    button.action = function () {
        if (form.selectedThemeButton != null) {
            form.selectedThemeButton.setSelected(false);
        }
        this.setSelected(true);
        form.selectedThemeButton = this;
    }

    return button;
}