﻿
StiMobileDesigner.prototype.InitializeEditConnectionForm_ = function () {

    //Edit Connection Form
    var editConnectionForm = this.BaseForm("editConnectionForm", this.loc.Database.Connection, 3, this.HelpLinks["connectionEdit"]);
    editConnectionForm.connection = null;
    editConnectionForm.mode = "Edit";

    var moveToResource = this.FormButton(null, null, this.loc.Buttons.MoveToResource, null);
    moveToResource.style.display = "inline-block";
    moveToResource.style.margin = "8px";

    moveToResource.action = function () {
        this.jsObject.SendCommandMoveConnectionDataToResource(editConnectionForm);
    }

    moveToResource.updateEnabledState = function () {
        var isXmlConnection = editConnectionForm.connection.typeConnection && editConnectionForm.connection.typeConnection.endsWith("StiXmlDatabase");

        this.setEnabled(
            (editConnectionForm.pathDataControl.textBox.value &&
                editConnectionForm.pathDataControl.textBox.value.indexOf(this.jsObject.options.resourceIdent) < 0) ||
            (isXmlConnection && editConnectionForm.xmlTypeControl.key == "AdoNetXml" && editConnectionForm.pathSchemaControl.textBox.value &&
                editConnectionForm.pathSchemaControl.textBox.value.indexOf(this.jsObject.options.resourceIdent) < 0)
        );
    }

    var footerTable = this.CreateHTMLTable();
    footerTable.style.width = "100%";
    var buttonsPanel = editConnectionForm.buttonsPanel;
    editConnectionForm.removeChild(buttonsPanel);
    editConnectionForm.appendChild(footerTable);
    footerTable.addCell(moveToResource).style.width = "1px";
    footerTable.addCell();
    footerTable.addCell(editConnectionForm.buttonOk).style.width = "1px";
    footerTable.addCell(editConnectionForm.buttonCancel).style.width = "1px";

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px 0 5px 0";
    editConnectionForm.container.appendChild(innerTable);

    var csvSeparatorTable = this.CreateHTMLTable();
    editConnectionForm.csvSeparatorControl = this.DropDownList(null, 200, null, this.GetCsvSeparatorItems(), true, false, null, true);
    editConnectionForm.csvSeparatorTextControl = this.TextBox(null, 80);
    csvSeparatorTable.addCell(editConnectionForm.csvSeparatorControl);
    csvSeparatorTable.addCell(editConnectionForm.csvSeparatorTextControl).style.paddingLeft = "10px";

    editConnectionForm.csvSeparatorControl.action = function () {
        editConnectionForm.csvSeparatorTextControl.style.display = this.key == "Other" ? "" : "none";
    }

    var controlProps = [
        ["name", this.loc.PropertyMain.Name + ":", this.TextBox(null, 180)],
        ["alias", this.loc.PropertyMain.Alias + ":", this.TextBox(null, 180)],
        ["xmlType", this.loc.FormDatabaseEdit.XmlType, this.DropDownList("editConnectionFormXmlType", 250, null, this.GetXmlTypeItems(), true, false, null, true)],
        ["pathSchema", this.loc.FormDatabaseEdit.PathSchema, this.TextBoxWithOpenDialog(null, 300)],
        ["pathData", this.loc.FormDatabaseEdit.PathToData, this.TextBoxWithOpenDialog(null, 300)],
        ["codePageDbase", this.loc.Export.Encoding, this.DropDownList(null, 300, null, this.GetDBaseCodePageItems(), true, false, null, true)],
        ["codePageCsv", this.loc.Export.Encoding, this.DropDownList(null, 200, null, this.GetCsvCodePageItems(), true, false, null, true)],
        ["csvSeparatorTable", this.loc.Export.Separator, csvSeparatorTable],
        ["firstRowIsHeader", "", this.CheckBox(null, this.loc.FormDatabaseEdit.FirstRowIsHeader)]
    ]
    //PathToData
    for (var i = 0; i < controlProps.length; i++) {
        editConnectionForm[controlProps[i][0] + "ControlRow"] = innerTable.addRow();
        var text = innerTable.addCellInLastRow();
        text.className = "stiDesignerCaptionControlsBigIntervals";
        text.innerHTML = controlProps[i][1];
        var control = controlProps[i][2];
        control.textCell = text;
        editConnectionForm[controlProps[i][0] + "Control"] = control;
        innerTable.addCellInLastRow(control).className = "stiDesignerControlCellsBigIntervals";
    }

    editConnectionForm.pathSchemaControl.openButton.style.display = "none";
    editConnectionForm.pathDataControl.openButton.style.display = "none";

    editConnectionForm.firstRowIsHeaderControl.style.margin = "3px 0 3px 0";

    //Resources Gallery
    var resourcesGallery = this.ImageGallery(null, 500, 85, this.loc.PropertyMain.Resources);
    editConnectionForm.container.appendChild(resourcesGallery);

    resourcesGallery.update = function (typeConnection) {
        this.clear();
        if (!this.jsObject.options.report) return;
                
        var typeConnection = editConnectionForm.connection.typeConnection;
        var types = [];
        switch (typeConnection) {
            case "StiXmlDatabase": types = ["Xml", "Xsd"]; break;
            case "StiJsonDatabase": types = ["Json"]; break;
            case "StiDBaseDatabase": types = ["Dbf"]; break;
            case "StiCsvDatabase": types = ["Csv"]; break;
            case "StiExcelDatabase": types = ["Excel"]; break;
        }
        
        var resources = this.jsObject.options.report.dictionary.resources;
        var items = [];
        var resourceIdent = this.jsObject.options.resourceIdent;
        var pathResourceName = editConnectionForm.pathDataControl.textBox.value.indexOf(resourceIdent) == 0 ? editConnectionForm.pathDataControl.textBox.value.substring(resourceIdent.length) : null;
        var schemaResourceName = editConnectionForm.pathSchemaControl.textBox.value.indexOf(resourceIdent) == 0 ? editConnectionForm.pathSchemaControl.textBox.value.substring(resourceIdent.length) : null;
        
        for (var i = 0; i < resources.length; i++) {
            if (types.indexOf(resources[i].type) >= 0) {
                var itemObject = this.jsObject.CopyObject(resources[i]);
                itemObject.imageName = "Resources.BigResource" + resources[i].type;
                var item = this.addItem(itemObject);
                
                item.action = function () { 
                    resourcesGallery.action(this); 
                }

                item.select = function (state) { 
                    if (state) {
                        var items = resourcesGallery.getItems();
                        for (var i = 0; i < items.length; i++) {
                            if (this.itemObject.type == items[i].itemObject.type && items[i].isSelected) {
                                items[i].setSelected(false);
                            }
                        }
                        this.setSelected(true);
                    }
                    else {
                        this.setSelected(false);
                    }
                }

                if (itemObject.name == pathResourceName || itemObject.name == schemaResourceName) {
                    item.select(true);
                }
            }
        };

        if (!this.innerContainer.innerTable) this.style.display = "none";
    }

    resourcesGallery.action = function (item) {
        item.select(true);
        editConnectionForm[item.itemObject.type == "Xsd" ? "pathSchemaControl" : "pathDataControl"].textBox.value = this.jsObject.options.resourceIdent + item.itemObject.name;

        if (item.itemObject.type == "Xsd" && editConnectionForm.xmlTypeControl.key == "Xml") {
            editConnectionForm.xmlTypeControl.setKey("AdoNetXml");
            editConnectionForm.xmlTypeControl.action();
        }

        editConnectionForm.pathDataControl.action();
    }

    //Controls actions
    editConnectionForm.xmlTypeControl.action = function () {
        editConnectionForm.pathSchemaControlRow.style.display = this.key == "AdoNetXml" ? "" : "none";
    }

    editConnectionForm.nameControl.action = function () {
        if (editConnectionForm.mode == "New" && editConnectionForm.connection.name != editConnectionForm.nameControl.value) editConnectionForm.nameIsChanged = true;
        if (this.oldValue == editConnectionForm.aliasControl.value) {
            editConnectionForm.aliasControl.value = this.value;
        }
    }

    editConnectionForm.aliasControl.action = function () {
        if (editConnectionForm.mode == "New" && editConnectionForm.connection.alias != editConnectionForm.aliasControl.value) editConnectionForm.aliasIsChanged = true;
    }

    editConnectionForm.pathDataControl.action = function () {
        if (editConnectionForm.mode == "New" && !editConnectionForm.nameIsChanged) {
            var newName = editConnectionForm.jsObject.GetConnectionNameFromPathData(this.textBox.value) || editConnectionForm.connection.name;
            editConnectionForm.nameControl.value = newName;
            
            if (this.jsObject.options.report) {
                var i = 2;
                while (!editConnectionForm.nameControl.checkExists(this.jsObject.options.report.dictionary.databases, "name")) {
                    editConnectionForm.nameControl.value = newName + i;
                    i++;
                }
                editConnectionForm.nameControl.hideError();
            }

            if (!editConnectionForm.aliasIsChanged) editConnectionForm.aliasControl.value = editConnectionForm.nameControl.value;
        }

        moveToResource.updateEnabledState();
    }
    
    editConnectionForm.pathDataControl.textBox.onchange = function () {
        editConnectionForm.pathDataControl.action();
    }

    editConnectionForm.pathSchemaControl.textBox.onchange = function () {
        moveToResource.updateEnabledState();
    }

    editConnectionForm.pathSchemaControl.action = function () {
        moveToResource.updateEnabledState();
    }

    //Connection String
    editConnectionForm.connectionStringRow = innerTable.addRow();
    var connectionStringText = innerTable.addCellInLastRow();
    connectionStringText.className = "stiDesignerCaptionControlsBigIntervals";
    connectionStringText.style.paddingTop = "10px";
    connectionStringText.style.verticalAlign = "top";
    connectionStringText.innerHTML = this.loc.FormDatabaseEdit.ConnectionString;

    var connectStrTable = this.CreateHTMLTable();
    connectStrTable.className = "stiColorControlWithBorder";
    innerTable.addCellInLastRow(connectStrTable).className = "stiDesignerControlCellsBigIntervals";
    
    var connectionStringControl = this.TextArea("editConnectionFormConnectionStringTextBox", 380, 180);
    connectionStringControl.style.border = "0px";
    connectionStringControl.style.overflowY = "hidden";
    editConnectionForm.connectionStringControl = connectionStringControl;
    connectStrTable.addCell(connectionStringControl);

    var connectStrButtons = this.CreateHTMLTable();
    connectStrTable.addCell(connectStrButtons).style.verticalAlign = "top";
        
    var buttonProps = [
        ["buildConnection", "ConnectionString.Build.png", this.loc.Buttons.Build.replace("...", "")],
        ["cleanConnection", "ConnectionString.Clean.png", this.loc.MainMenu.menuEditClearContents],
        ["testConnection", "ConnectionString.Check.png", this.loc.DesignerFx.TestConnection],
        ["infoConnection", "ConnectionString.Info.png", this.loc.PropertyMain.ConnectionString]
    ]

    for (var i = 0; i < buttonProps.length; i++) {
        var button = this.SmallButton(null, null, null, buttonProps[i][1], buttonProps[i][2], null, this.GetStyles("FormButton"), true);
        button.style.margin = "4px 4px 0 4px";
        editConnectionForm[buttonProps[i][0]] = button;
        connectStrButtons.addRow();
        connectStrButtons.addCellInLastRow(button);
    }

    //Test Connection
    editConnectionForm.testConnection.action = function () {
        this.setEnabled(false);
        this.jsObject.SendCommandTestConnection(
            editConnectionForm.connection.typeConnection,
            Base64.encode(connectionStringControl.value)
        );
    }

    //Build Connection
    editConnectionForm.buildConnection.action = function () {
        switch(editConnectionForm.connection.typeConnection) {
            case "StiODataDatabase": {
                this.jsObject.InitializeODataConnectionForm(function (oDataConnectionForm) {
                    oDataConnectionForm.show(oDataConnectionForm.jsObject.ParseODataConnectionString(connectionStringControl.value));

                    oDataConnectionForm.action = function () {
                        connectionStringControl.value = this.getConnectionString();
                        this.changeVisibleState(false);
                    };
                });
                break;
            }
        }
    }

    //Clean Connection
    editConnectionForm.cleanConnection.action = function () {
        connectionStringControl.value = "";
    }

    //Info Connection
    editConnectionForm.infoConnection.action = function () {
        this.jsObject.SendCommandGetSampleConnectionString(editConnectionForm.connection.typeConnection, function (answer) {
            if (answer.connectionString) {
                connectionStringControl.value = answer.connectionString;
            }
        });
    }

    //PromptUserNameAndPassword
    editConnectionForm.promptUserNameAndPasswordRow = innerTable.addRow();
    var promptUserNameAndPasswordText = innerTable.addCellInLastRow();
    promptUserNameAndPasswordText.className = "stiDesignerCaptionControlsBigIntervals";
    editConnectionForm.promptUserNameAndPasswordControl = this.CheckBox("editConnectionFormPromptUserNameAndPassword", this.loc.FormDatabaseEdit.PromptUserNameAndPassword);
    innerTable.addCellInLastRow(editConnectionForm.promptUserNameAndPasswordControl).className = "stiDesignerControlCellsBigIntervals";

    editConnectionForm.getCsvSeparatorType = function (sepText) {
        switch (sepText) {
            case "": return "System";
            case "\t": return "Tab"
            case ";": return "Semicolon";
            case ",": return "Comma";
            case " ": return "Space";
            default: return "Other";
        }
    }

    editConnectionForm.getCsvSeparatorText = function (sepType) {
        switch (sepType) {
            case "System": return "";
            case "Tab": return "\t";
            case "Semicolon": return ";";
            case "Comma": return ",";
            case "Space": return " ";
            case "Other": return this.csvSeparatorTextControl.value;
        }
    }

    editConnectionForm.onshow = function () {
        this.mode = "Edit";
        this.nameIsChanged = false;
        this.aliasIsChanged = false;

        if (typeof (this.connection) == "string") {
            this.connection = this.jsObject.ConnectionObject(this.connection);
            this.mode = "New";
        } else if (this.connection.isRecentConnection) {
            this.mode = "New";
        }        
        var caption = (this.connection.typeConnection && (this.connection.typeConnection.endsWith("StiXmlDatabase") || this.connection.typeConnection.endsWith("StiJsonDatabase")))
            ? this.jsObject.loc.FormDatabaseEdit[(this.connection.typeConnection && this.connection.typeConnection.endsWith("StiXmlDatabase") ? "Xml" : "Json") + this.mode]
            : this.jsObject.loc.FormDatabaseEdit[this.mode + "Connection"].replace("{0}", this.jsObject.GetConnectionNames(this.connection.typeConnection, true));

        this.editableDictionaryItem = this.mode == "Edit" && this.jsObject.options.dictionaryTree
            ? this.jsObject.options.dictionaryTree.selectedItem : null;

        if (caption) this.caption.innerHTML = caption;
        this.csvSeparatorTextControl.value = "";
        this.csvSeparatorTextControl.style.display = "none";
        this.nameControl.hideError();
        this.nameControl.focus();
        this.nameControl.value = this.connection.name;
        this.aliasControl.value = this.connection.alias;
        var isXmlConnection = this.connection.typeConnection && this.connection.typeConnection.endsWith("StiXmlDatabase");
        var isJsonConnection = this.connection.typeConnection && this.connection.typeConnection.endsWith("StiJsonDatabase");
        var isDBaseConnection = this.connection.typeConnection && this.connection.typeConnection.endsWith("StiDBaseDatabase");
        var isCsvConnection = this.connection.typeConnection && this.connection.typeConnection.endsWith("StiCsvDatabase");
        var isExcelConnection = this.connection.typeConnection && this.connection.typeConnection.endsWith("StiExcelDatabase");
        var isFileDataConnection = isXmlConnection || isJsonConnection || isDBaseConnection || isCsvConnection || isExcelConnection;
         
        if (isXmlConnection) {
            this.pathDataControl.textCell.innerHTML = this.jsObject.loc.FormDatabaseEdit.PathData;
            if (this.connection.pathSchema != null) this.pathSchemaControl.textBox.value = Base64.decode(this.connection.pathSchema);
            this.xmlTypeControl.setKey(this.connection.xmlType);
            this.pathSchemaControl.filesMask = ".xsd";
            this.pathDataControl.filesMask = ".xml";
        }
        else if (isJsonConnection) {
            this.pathDataControl.textCell.innerHTML = this.jsObject.loc.FormDatabaseEdit.PathJsonData;
            this.pathDataControl.filesMask = ".json";
        }
        else if (isDBaseConnection) {
            this.pathDataControl.textCell.innerHTML = this.jsObject.loc.FormDatabaseEdit.PathToData;
            this.codePageDbaseControl.setKey(this.connection.codePage);
            this.pathDataControl.filesMask = "*";
        }
        else if (isCsvConnection) {
            this.pathDataControl.textCell.innerHTML = this.jsObject.loc.FormDatabaseEdit.PathToData;
            this.codePageCsvControl.setKey(this.connection.codePage);
            var sepType = this.getCsvSeparatorType(this.connection.separator);
            this.csvSeparatorControl.setKey(sepType);
            this.csvSeparatorTextControl.style.display = sepType == "Other" ? "" : "none";
            if (sepType == "Other") this.csvSeparatorTextControl.value = this.connection.separator;
            this.pathDataControl.filesMask = ".csv";
        }
        else if (isExcelConnection) {
            this.pathDataControl.textCell.innerHTML = this.jsObject.loc.FormDatabaseEdit.PathToData;
            this.firstRowIsHeaderControl.setChecked(this.connection.firstRowIsHeader);
            this.pathDataControl.filesMask = ".xlsx,.xls";
        }
        else {
            if (this.connection.connectionString != null) this.connectionStringControl.value = Base64.decode(this.connection.connectionString);
            this.promptUserNameAndPasswordControl.setChecked(this.connection.promptUserNameAndPassword);
        }

        if (isFileDataConnection) {
            if (this.connection.pathData != null) this.pathDataControl.textBox.value = Base64.decode(this.connection.pathData);
            resourcesGallery.style.display = "";
            resourcesGallery.update();
        }
        else {
            resourcesGallery.style.display = "none";
        }
                
        if (isFileDataConnection) moveToResource.updateEnabledState();
        moveToResource.style.display = isFileDataConnection && (!this.jsObject.options.jsMode || this.jsObject.options.nodeJsMode) ? "" : "none";

        this.xmlTypeControlRow.style.display = isXmlConnection && !this.jsObject.options.isJava ? "" : "none";
        this.pathDataControlRow.style.display = isFileDataConnection ? "" : "none";
        this.pathSchemaControlRow.style.display = isXmlConnection && this.xmlTypeControl.key == "AdoNetXml" ? "" : "none";
        this.connectionStringRow.style.display = !isFileDataConnection ? "" : "none";
        this.buildConnection.style.display = this.jsObject.CanEditConnectionString(this.connection) ? "" : "none";
        this.promptUserNameAndPasswordRow.style.display = this.connectionStringRow.style.display == "" && this.connection.typeConnection != "StiMSAccessDatabase" ? "" : "none";
        this.codePageDbaseControlRow.style.display = isDBaseConnection ? "" : "none";
        this.codePageCsvControlRow.style.display = isCsvConnection ? "" : "none";
        this.csvSeparatorTableControlRow.style.display = isCsvConnection ? "" : "none";
        this.firstRowIsHeaderControlRow.style.display = isExcelConnection ? "" : "none";
    }

    editConnectionForm.action = function () {
        this.connection.mode = this.mode;
        this.connection.skipSchemaWizard = this.skipSchemaWizard;

        if (!this.nameControl.checkNotEmpty(this.jsObject.loc.PropertyMain.Name)) return;
        var allDatabases = this.jsObject.options.report.dictionary.databases;
        var connections = [];
        for (var i = 0; i < allDatabases.length; i++) {
            if (allDatabases[i].typeConnection) connections.push(allDatabases[i]);
        }
        if ((this.mode == "New" || this.connection.name != this.nameControl.value) &&
            !this.nameControl.checkExists(connections, "name")) return;

        if (this.mode == "Edit") this.connection["oldName"] = this.connection.name;
                
        this.connection.name = this.nameControl.value;
        this.connection.alias = this.aliasControl.value;

        var isXmlConnection = this.connection.typeConnection && this.connection.typeConnection.endsWith("StiXmlDatabase");
        var isJsonConnection = this.connection.typeConnection && this.connection.typeConnection.endsWith("StiJsonDatabase");
        var isDBaseConnection = this.connection.typeConnection && this.connection.typeConnection.endsWith("StiDBaseDatabase");
        var isCsvConnection = this.connection.typeConnection && this.connection.typeConnection.endsWith("StiCsvDatabase");
        var isExcelConnection = this.connection.typeConnection && this.connection.typeConnection.endsWith("StiExcelDatabase");
        var isFileDataConnection = isXmlConnection || isJsonConnection || isDBaseConnection || isCsvConnection || isExcelConnection;

        if (isXmlConnection) {
            this.connection.xmlType = this.xmlTypeControl.key;
            this.connection.pathData = Base64.encode(this.pathDataControl.textBox.value);
            this.connection.pathSchema = this.xmlTypeControl.key == "AdoNetXml" ? Base64.encode(this.pathSchemaControl.textBox.value) : "";
        }
        else if (isJsonConnection) {
            this.connection.pathData = Base64.encode(this.pathDataControl.textBox.value);
        }
        else if (isDBaseConnection) {
            this.connection.pathData = Base64.encode(this.pathDataControl.textBox.value);
            this.connection.codePage = this.codePageDbaseControl.key;
        }
        else if (isCsvConnection) {
            this.connection.pathData = Base64.encode(this.pathDataControl.textBox.value);
            this.connection.codePage = this.codePageCsvControl.key;
            this.connection.separator = this.getCsvSeparatorText(this.csvSeparatorControl.key);
        }
        else if (isExcelConnection) {
            this.connection.pathData = Base64.encode(this.pathDataControl.textBox.value);
            this.connection.firstRowIsHeader = this.firstRowIsHeaderControl.isChecked;
        }
        else {
            this.connection.connectionString = Base64.encode(this.connectionStringControl.value);
            this.connection.promptUserNameAndPassword = this.promptUserNameAndPasswordControl.isChecked;
        }
        
        if (this.mode == "New" && !this.connection.isRecentConnection) {
            this.jsObject.SaveRecentConnectionToCookies(this.connection);
        }               

        this.changeVisibleState(false);
        this.jsObject.SendCommandCreateOrEditConnection(this.connection);
    }

    return editConnectionForm;
}