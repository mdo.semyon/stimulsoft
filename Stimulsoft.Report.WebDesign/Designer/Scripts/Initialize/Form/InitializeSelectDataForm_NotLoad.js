﻿
StiMobileDesigner.prototype.InitializeSelectDataForm_ = function () {

    //Select Data Form
    var selectDataForm = this.BaseForm("selectDataForm", this.loc.FormDatabaseEdit.SelectData, 3, this.HelpLinks["data"]);
    selectDataForm.rootItems = ["Tables", "Views", "Queries", "StoredProcedures"];

    var newQuery = this.StandartSmallButton(null, null, this.loc.FormDictionaryDesigner.QueryNew, "DataSourceNew.png");
    newQuery.style.display = "inline-block";
    newQuery.style.margin = "4px";
    selectDataForm.container.appendChild(newQuery);
    var newQuerySep = this.FormSeparator();
    selectDataForm.container.appendChild(newQuerySep);

    newQuery.action = function () {
        var this_ = this;
        selectDataForm.changeVisibleState(false);
        this.jsObject.InitializeEditDataSourceForm(function (editDataSourceForm) {
            editDataSourceForm.datasource = this_.jsObject.GetDataAdapterTypeFromDatabaseType(selectDataForm.connectionObject.typeConnection);
            editDataSourceForm.nameInSource = selectDataForm.connectionObject.name;
            editDataSourceForm.changeVisibleState(true);
        });
    }

    //Tree
    var tree = this.Tree();
    tree.style.overflow = "auto";
    tree.style.width = "450px";
    tree.style.height = "350px";
    tree.style.padding = "10px";
    selectDataForm.container.appendChild(tree);

    //Progress
    var progress = this.Progress();
    progress.className = "stiDesignerSelectDataProgress";
    progress.style.display = "none";
    selectDataForm.container.appendChild(progress);

    selectDataForm.onshow = function () {
        tree.clear();
        //progress.style.display = ""; //becouse duplicate main loading
        this.jsObject.SendCommandGetDatabaseData(this.databaseName);

        var isFileDataConnection = this.typeConnection && (
            this.typeConnection.endsWith("StiXmlDatabase") ||
            this.typeConnection.endsWith("StiJsonDatabase") ||
            this.typeConnection.endsWith("StiDBaseDatabase") ||
            this.typeConnection.endsWith("StiCsvDatabase") ||
            this.typeConnection.endsWith("StiExcelDatabase"));

        newQuery.style.display = isFileDataConnection ? "none" : "inline-block";
        newQuerySep.style.display = isFileDataConnection ? "none" : "";
        selectDataForm.buttonOk.setEnabled(false);
    }

    selectDataForm.buildTree = function (data) {

        var addRelations = function (parentItem, collection) {
            if (collection) {
                collection.sort(selectDataForm.jsObject.SortByName);
                for (var m = 0; m < collection.length; m++) {
                    var relationObject = collection[m];
                    var relationItem = selectDataForm.jsObject.TreeItemWithCheckBox(relationObject.name, relationObject.typeIcon + ".png", relationObject, tree);
                    parentItem.addChild(relationItem);
                }
            }
        }

        var addColumns = function (parentItem, collection) {
            if (collection) {
                collection.sort(selectDataForm.jsObject.SortByName);
                for (var m = 0; m < collection.length; m++) {
                    var columnObject = collection[m];
                    var columnItem = selectDataForm.jsObject.TreeItemWithCheckBox(columnObject.name, columnObject.typeIcon + ".png", columnObject, tree);
                    parentItem.addChild(columnItem);
                }
            }
        }

        var addTables = function (parentItem, collection) {
            if (collection) {
                collection.sort(selectDataForm.jsObject.SortByName);

                for (var k = 0; k < collection.length; k++) {
                    var tableItem = selectDataForm.jsObject.TreeItemWithCheckBox(collection[k].name, "Data.Data" + collection[k].typeItem + ".png", collection[k], tree);
                    parentItem.addChild(tableItem);

                    var relations = collection[k].relations;
                    if (relations) addRelations(tableItem, relations);

                    var columns = collection[k].columns;
                    if (columns) addColumns(tableItem, columns);
                }
            }
        }

        progress.style.display = "none";
        selectDataForm.buttonOk.setEnabled(true);

        for (var i = 0; i < selectDataForm.rootItems.length; i++) {
            var caption = this.jsObject.loc.QueryBuilder.Collections;
            if (selectDataForm.typeConnection != "StiMongoDbDatabase") {
                switch (selectDataForm.rootItems[i]) {
                    case "Tables": caption = this.jsObject.loc.QueryBuilder.Tables; break;
                    case "Views": caption = this.jsObject.loc.QueryBuilder.Views; break;
                    case "Queries": caption = this.jsObject.loc.FormDictionaryDesigner.Queries; break;
                    default: caption = selectDataForm.rootItems[i]; break;
                }
            }

            var rootItem = selectDataForm.jsObject.TreeItemWithCheckBox(caption, "Data.Data" + selectDataForm.rootItems[i] + ".png", null, tree);
            tree["rootItem" + selectDataForm.rootItems[i]] = rootItem;
            tree.appendChild(rootItem);

            if (selectDataForm.typeConnection == "StiMongoDbDatabase" && selectDataForm.rootItems[i] != "Tables" ||
                ((selectDataForm.rootItems[i] == "StoredProcedures" || selectDataForm.rootItems[i] == "Queries" || selectDataForm.rootItems[i] == "Views") &&
                (!data || !data[selectDataForm.rootItems[i]])))
                rootItem.style.display = "none";

            if (data && data[selectDataForm.rootItems[i]]) {
                rootItem.setOpening(true);
                addTables(rootItem, data[selectDataForm.rootItems[i]]);
            }
        }
    }

    selectDataForm.action = function () {
        var resultCollection = [];

        for (var i = 0; i < selectDataForm.rootItems.length; i++) {
            var rootItem = tree["rootItem" + selectDataForm.rootItems[i]];
            if (!rootItem) continue;

            for (var tableKey in rootItem.childs) {
                var addTable = false;
                var tableItem = rootItem.childs[tableKey];
                if (tableItem.isChecked) addTable = true;

                var columns = [];
                var relations = [];
                var allColumns = [];
                for (var childKey in tableItem.childs) {
                    var childItem = tableItem.childs[childKey];
                    if (childItem.itemObject.typeItem == "Relation") {
                        if (childItem.isChecked) {
                            addTable = true;
                            relations.push(childItem.itemObject);
                        }
                    }
                    else {
                        allColumns.push(childItem.itemObject);
                        if (childItem.isChecked) {
                            addTable = true;
                            columns.push(childItem.itemObject);
                        }
                    }
                }
                columns.sort(selectDataForm.jsObject.SortByName);
                relations.sort(selectDataForm.jsObject.SortByName);

                if (addTable) {
                    var table = tableItem.itemObject;
                    table.countAllColumns = allColumns.length;
                    table.columns = columns;
                    table.relations = relations;
                    table.allColumns = allColumns;
                    resultCollection.push(table);
                }
            }
        }

        selectDataForm.changeVisibleState(false);
        if (resultCollection.length > 0) selectDataForm.jsObject.SendCommandApplySelectedData(resultCollection, selectDataForm.databaseName);
    }

    return selectDataForm;
}