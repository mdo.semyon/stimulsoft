﻿
StiMobileDesigner.prototype.InitializeImageForm_ = function () {
    //Image Form
    var imageForm = this.BaseFormPanel("imageForm", this.loc.PropertyCategory.ImageCategory, 2, this.HelpLinks["image"]);
    imageForm.mode = "ImageSrc";
    var panelWidth = 650;
    var panelHeight = 500;
    var resourceIdent = this.options.resourceIdent;
    var variableIdent = this.options.variableIdent;

    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerImageFormMainPanel";
    imageForm.container.appendChild(mainTable);
    imageForm.container.style.padding = "0px";

    //Buttons
    var buttonProps = [
        ["ImageSrc", "BigImage.png", this.loc.PropertyCategory.ImageCategory],
        ["ImageDataColumn", "BigImageDataColumn.png", this.loc.PropertyMain.DataColumn],
        ["ImageData", "BigImageExpression.png", this.loc.PropertyMain.Expression],
        ["ImageUrl", "BigImageWebUrl.png", this.loc.PropertyMain.Hyperlink],
        ["ImageFile", "BigImageLocalFile.png", this.loc.MainMenu.menuFile.replace("&", "")]
    ];

    if (this.options.cloudMode && !this.options.isOnlineVersion) {
        buttonProps.push(["ImageServer", "BigImageCloud.png", "Server"]);
    }

    //Add Panels && Buttons
    var panelsContainer = mainTable.addCell();
    var buttonsPanel = mainTable.addCell();
    buttonsPanel.style.verticalAlign = "top";
    imageForm.mainButtons = {};
    imageForm.panels = {};

    for (var i = 0; i < buttonProps.length; i++) {
        var panel = document.createElement("Div");
        panel.className = "stiDesignerEditFormPanel";
        if (i != 0) panel.style.display = "none";
        panel.style.height = panelHeight + "px";
        panel.style.width = panelWidth + "px";
        panel.style.overflow = "hidden";
        panelsContainer.appendChild(panel);
        imageForm.panels[buttonProps[i][0]] = panel;

        var button = this.StandartFormBigButton("imageForm" + buttonProps[i][0] + "Button", null, buttonProps[i][2], buttonProps[i][1], buttonProps[i][2], 80);
        button.style.margin = "2px";
        imageForm.mainButtons[buttonProps[i][0]] = button;
        buttonsPanel.appendChild(button);
        button.panelName = buttonProps[i][0];
        button.action = function () {
            imageForm.setMode(this.panelName);
        }
    }

    //Image
    var imageSrcContainer = this.ImageControl(null, panelWidth - 17, panelHeight - 142);
    imageSrcContainer.style.margin = "8px 0 8px 8px";
    imageForm.panels.ImageSrc.appendChild(imageSrcContainer);
    
    imageSrcContainer.action = function () {
        imageSrcContainer.variableName = null;
        imageSrcContainer.resourceName = null;
        imageSrcContainer.columnName = null;
        imageUrlControl.value = "";
        imageUrlContainer.setImage(null);
        if (imageSrcGallery.selectedItem) {
            imageSrcGallery.selectedItem.select(false);
        }
    }

    var imageSrcGallery = this.ImageGallery(null, panelWidth, 100, this.loc.Report.Gallery);
    imageForm.panels.ImageSrc.appendChild(imageSrcGallery);    

    imageSrcGallery.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        imageSrcContainer.resize(panelWidth - 17, state ? panelHeight - 142 : panelHeight - 17);
    }
            
    //Image Data Column
    var dataTreePlace = document.createElement("div");
    dataTreePlace.style.width = panelWidth + "px";
    dataTreePlace.style.height = (panelHeight - 124) + "px";
    dataTreePlace.style.overflow = "auto";
    var dataColumnTree = this.options.dataTree;
    imageForm.panels.ImageDataColumn.appendChild(dataTreePlace);
    
    var dataColumnGallery = this.ImageGallery(null, panelWidth, 100, this.loc.Report.Gallery);    
    imageForm.panels.ImageDataColumn.appendChild(dataColumnGallery);

    dataColumnGallery.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        dataTreePlace.style.height = (state ? panelHeight - 124 : panelHeight) + "px";
    }

    dataColumnGallery.action = function (item) {
        dataColumnTree.setKey(item.itemObject.name);
        dataColumnTree.autoscroll();
    }

    //Image Data
    var imageDataControl = this.TextArea("imageFormImageData", panelWidth - 22, panelHeight - 17);
    imageDataControl.style.margin = "8px";
    imageForm.panels.ImageData.appendChild(imageDataControl);
    imageForm.panels.ImageData.style.overflow = "hidden";
    imageDataControl.addInsertButton();

    //Image Url
    var tableImageUrl = this.CreateHTMLTable();
    tableImageUrl.style.width = "100%";
    imageForm.panels.ImageUrl.appendChild(tableImageUrl);
    var textCell = tableImageUrl.addTextCell(this.loc.PropertyMain.Hyperlink + ":");
    textCell.style.paddingLeft = "8px";
    textCell.style.width = "100px";

    var imageUrlControl = this.TextBox(null, "calc(100% - 21px)");
    imageUrlControl.style.margin = "8px";
    tableImageUrl.addCell(imageUrlControl);

    imageUrlControl.onkeyup = function () {
        clearTimeout(this.keyTimer);
        this.keyTimer = setTimeout(function () {
            imageUrlControl.action();
        }, 800);
    }

    var imageUrlContainer = this.ImageControl(null, panelWidth - 17, panelHeight - 173, true, true);
    imageUrlContainer.style.margin = "0 0 8px 8px";
    imageForm.panels.ImageUrl.appendChild(imageUrlContainer);
        
    var imageUrlContextMenu = this.InitializeDeleteItemsContextMenu("imageUrlContextMenu");

    var imageUrlGallery = this.ImageGallery(null, panelWidth, 100, this.loc.Report.History, imageUrlContextMenu);
    imageForm.panels.ImageUrl.appendChild(imageUrlGallery);

    imageUrlContextMenu.action = function (item) {
        this.changeVisibleState(false);
        switch (item.key) {
            case "delete": { 
                if (imageUrlGallery.selectedItem) {
                    imageForm.removeImageItemFromHistory(imageUrlGallery.selectedItem.itemObject.src, "StimulsoftMobileDesignerImageUrlHistory");
                    imageUrlGallery.selectedItem.remove();
                }
                break;
            }
            case "deleteAll": {
                imageUrlGallery.clear();
                this.jsObject.SetCookie("StimulsoftMobileDesignerImageUrlHistory", JSON.stringify([]));
                break;
            }
        }
    }

    imageUrlGallery.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        var height = state ? panelHeight - 173 : panelHeight - 49;
        if (this.jsObject.options.isTouchDevice) height -= 5;
        imageUrlContainer.resize(panelWidth - 17, height);
    }

    imageUrlGallery.action = function (item) {
        imageUrlContainer.setImage(item.itemObject.src);
        imageUrlControl.value = item.itemObject.src;
    }

    imageUrlControl.action = function () {
        if (this.value.indexOf(resourceIdent) == 0 || this.value.indexOf(variableIdent) == 0) {
            var ident = this.value.indexOf(resourceIdent) == 0 ? resourceIdent : variableIdent;
            var itemName = this.value.substring(this.value.indexOf(ident) + ident.length);
            var item = imageSrcGallery.getItemByPropertyValue("name", itemName);
            if (item) {
                item.action(true);
            }
            else {
                imageUrlContainer.setImage(null);
                imageSrcContainer.setImage(null);
                imageSrcContainer.variableName = null;
                imageSrcContainer.resourceName = null;
                imageSrcContainer.columnName = null;
                if (imageSrcGallery.selectedItem) {
                    imageSrcGallery.selectedItem.select(false);
                }
            }
        }
        else {
            imageUrlContainer.setImage(this.value);
        }
    }

    imageUrlControl.onmouseup = function () {
        if (this.jsObject.options.itemInDrag) {
            var dictionaryTree = imageForm.jsObject.options.dictionaryTree;
            if (dictionaryTree.selectedItem) {
                this.value += dictionaryTree.selectedItem.getResultForEditForm();
            }
        }
    }

    imageUrlControl.ontouchend = function () { this.onmouseup(); }

    //File Name
    var tableFileName = this.CreateHTMLTable();
    imageForm.panels.ImageFile.appendChild(tableFileName);
    var textCell2 = tableFileName.addTextCell(this.loc.Cloud.labelFileName);
    textCell2.style.paddingLeft = "8px";
    textCell2.style.width = "100px";

    var fileNameControl = this.TextBoxWithOpenDialog("imageFormFileName", 525, ".bmp,.gif,.jpeg,.jpg,.png,.tiff,.ico,.emf,.wmf,.svg");
    fileNameControl.style.margin = "8px";
    tableFileName.addCell(fileNameControl);
       
    var fileNameContainer = this.ImageControl(null, panelWidth - 17, panelHeight - 173, true, false);
    fileNameContainer.style.margin = "0 0 8px 8px";
    imageForm.panels.ImageFile.appendChild(fileNameContainer);
    fileNameContainer.style.display = "none";

    var fileNameGallery = this.ImageGallery(null, panelWidth, 100, this.loc.Report.History);
    imageForm.panels.ImageFile.appendChild(fileNameGallery);

    fileNameGallery.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
        var height = state ? panelHeight - 173 : panelHeight - 49;
        if (this.jsObject.options.isTouchDevice) panelHeight -= 5;
        fileNameContainer.resize(panelWidth - 17, height);
    }

    imageSrcGallery.action = function (item) {
        imageSrcContainer.setImage(item.itemObject.src);
        imageSrcContainer.variableName = item.itemObject.type == "StiVariable" ? item.itemObject.name : null;
        imageSrcContainer.resourceName = item.itemObject.type == "StiResource" ? item.itemObject.name : null;
        imageSrcContainer.columnName = item.itemObject.type == "StiDataColumn" ? item.itemObject.name : null;
        imageUrlControl.value = item.itemObject.type == "StiResource"
            ? resourceIdent + item.itemObject.name
            : (item.itemObject.type == "StiVariable" ? variableIdent + item.itemObject.name : "");
        imageUrlContainer.setImage(item.itemObject.type == "StiResource" || item.itemObject.type == "StiVariable" ? item.itemObject.src : null);
        imageDataControl.value = item.itemObject.type == "StiVariable" ? "{" + item.itemObject.name + "}" : "";
        if (item.itemObject.type == "StiDataColumn") {
            var item = dataColumnGallery.getItemByPropertyValue("name", item.itemObject.name);
            if (item) item.action(true);
        }
    }
    
    //Image Server
    if (this.options.cloudMode && !this.options.isOnlineVersion) {
        imageForm.cloudContainer = this.CloudContainer("imageFormCloudContainer", ["Image"], null, panelHeight);
        //imageForm.cloudContainer.style.margin = "8px auto 0 auto";
        imageForm.panels.ImageServer.appendChild(imageForm.cloudContainer);
        if (this.options.dictionaryTree.selectedItem) {
            this.options.dictionaryTree.selectedItem.setSelected();
        }
    }
        
    //Form Methods
    imageForm.reset = function () {
        imageSrcContainer.setImage(null);
        imageSrcContainer.variableName = null;
        imageSrcContainer.resourceName = null;
        imageSrcContainer.columnName = null;
        imageSrcGallery.clear();
        dataColumnTree.setKey("");
        dataColumnGallery.clear();
        imageDataControl.value = "";
        imageUrlControl.value = "";
        imageUrlContainer.setImage(null);
        fileNameControl.setValue("");
        fileNameContainer.setImage(null);
        fileNameGallery.clear();
        if (imageForm.cloudContainer) imageForm.cloudContainer.clear();
        imageForm.setMode("ImageSrc");
    }

    imageForm.setMode = function (mode) {
        imageForm.mode = mode;
        for (var panelName in imageForm.panels) {
            imageForm.panels[panelName].style.display = mode == panelName ? "" : "none";
            imageForm.mainButtons[panelName].setSelected(mode == panelName);
        }
        var propertiesPanel = imageForm.jsObject.options.propertiesPanel;
        propertiesPanel.editFormControl = null;
        propertiesPanel.setEnabled(mode == "ImageUrl" || mode == "ImageData" || mode == "ImageServer");
        if (mode == "ImageUrl") {
            propertiesPanel.editFormControl = imageUrlControl;
            imageUrlControl.focus();
        }
        if (mode == "ImageData") {
            propertiesPanel.editFormControl = imageDataControl;
            imageDataControl.focus();
        }
        if (mode == "ImageServer") {
            propertiesPanel.editFormControl = imageForm.cloudContainer;
        }
        if (mode == "ImageFile") {
            fileNameControl.textBox.focus();
        }
    }

    imageForm.addImageItemToHistory = function (imageSrc, cookiesKey) {
        var historyStr = this.jsObject.GetCookie(cookiesKey);
        var historyArray = historyStr ? JSON.parse(historyStr) : [];
        var newItem = {
            name: this.jsObject.GetFileNameFromPath(imageSrc),
            src: imageSrc
        }
        var haveThisItem = false;
        for (var i = 0; i < historyArray.length; i++) {
            if (historyArray[i].name == newItem.name && historyArray[i].src == newItem.src) {
                haveThisItem = true;
                break;
            }
        }
        if (!haveThisItem) {
            if (historyArray.length > 9) historyArray.splice(9, 10);
            historyArray.splice(0, 0, newItem);
            this.jsObject.SetCookie(cookiesKey, JSON.stringify(historyArray));
        }
    }

    imageForm.removeImageItemFromHistory = function (imageSrc, cookiesKey) {
        var historyStr = this.jsObject.GetCookie(cookiesKey);
        var historyArray = historyStr ? JSON.parse(historyStr) : [];
        for (var i = 0; i < historyArray.length; i++) {
            if (historyArray[i].src == imageSrc) {
                historyArray.splice(i, 1);
                break;
            }
        }
        this.jsObject.SetCookie(cookiesKey, JSON.stringify(historyArray));
    }

    imageForm.fillImageAndColumnsGalleries = function (imagesGallery) {
        imageSrcGallery.progress.hide();
        dataColumnGallery.progress.hide();
        var allImages = [].concat(imagesGallery.variables, imagesGallery.resources, imagesGallery.columns);

        if (allImages.length > 0) {
            imageSrcGallery.addItems(allImages);
            if (imagesGallery.columns.length > 0) {
                dataColumnGallery.addItems(imagesGallery.columns);
            }
            else {
                dataColumnGallery.changeVisibleState(false);
            }
        }
        else {
            imageSrcGallery.changeVisibleState(false);
            dataColumnGallery.changeVisibleState(false);
        }
    }

    imageForm.onhide = function () {
        imageForm.jsObject.options.propertiesPanel.setDictionaryMode(false);
    }

    imageForm.onshow = function () {
        imageForm.jsObject.options.propertiesPanel.setDictionaryMode(true);
        
        //Data Tree Build
        dataTreePlace.appendChild(dataColumnTree);
        dataColumnTree.build(null, null, null, true);
        dataColumnTree.action = function () { imageForm.action(); }

        imageForm.reset();

        //Update galleries
        if (imageForm.jsObject.options.imagesGallery || imageForm.jsObject.CheckImagesInDictionary()) {
            imageSrcGallery.changeVisibleState(true);
            dataColumnGallery.changeVisibleState(true);
            imageSrcGallery.progress.show(280, -25);
            dataColumnGallery.progress.show(280, -25);
            
            if (!imageForm.jsObject.options.imagesGallery) {
                imageForm.jsObject.SendCommandToDesignerServer("GetImagesGallery", null, function (answer) {
                    imageForm.jsObject.options.imagesGallery = answer.imagesGallery;
                    imageForm.fillImageAndColumnsGalleries(answer.imagesGallery);
                    var itemName = imageSrcContainer.variableName || imageSrcContainer.resourceName || imageSrcContainer.columnName;
                    if (itemName) {
                        var item = imageSrcGallery.getItemByPropertyValue("name", itemName);
                        if (item) item.action(true);
                    }
                });
            }
            else {
                imageForm.fillImageAndColumnsGalleries(this.jsObject.options.imagesGallery)
            }
        }
        else {
            imageSrcGallery.changeVisibleState(false);
            dataColumnGallery.changeVisibleState(false);
        }                
        
        var imageUrlHistory = this.jsObject.GetCookie("StimulsoftMobileDesignerImageUrlHistory");
        imageUrlGallery.changeVisibleState(imageUrlHistory && JSON.parse(imageUrlHistory).length > 0);
        if (imageUrlHistory) imageUrlGallery.addItems(JSON.parse(imageUrlHistory));
        
        fileNameGallery.changeVisibleState(false);
                
        if (this.jsObject.options.selectedObjects) {
            imageForm.setMode("ImageSrc");
        }
        else {
            var selectedObject = this.jsObject.options.selectedObject;
            var props = ["imageSrc", "imageUrl", "imageFile", "imageDataColumn", "imageData"];
            for (var i = 0; i < props.length; i++) {
                imageForm[props[i]] = selectedObject.properties[props[i]] != null
                ? (props[i] == "imageSrc" ? selectedObject.properties[props[i]] : Base64.decode(selectedObject.properties[props[i]]))
                : null;
            }
            if (imageForm.imageSrc) {
                imageForm.setMode("ImageSrc");
                imageSrcContainer.setImage(imageForm.imageSrc);
                imageSrcContainer.variableName = null;
                imageSrcContainer.resourceName = null;
                imageSrcContainer.columnName = null;
            }
            else if (imageForm.imageUrl) {
                if (imageForm.imageUrl.indexOf(this.jsObject.options.cloudServerUrl) == 0) {
                    imageForm.setMode("ImageServer");
                    var key = imageForm.imageUrl.replace(this.jsObject.options.cloudServerUrl, "");
                    var item = this.jsObject.options.dictionaryTree.getCloudItemByKey("Image", key);
                    if (item && imageForm.cloudContainer) imageForm.cloudContainer.addItem(item.itemObject);
                } else {
                    if (imageForm.imageUrl.indexOf(resourceIdent) == 0 || imageForm.imageUrl.indexOf(variableIdent) == 0) {
                        var ident = imageForm.imageUrl.indexOf(resourceIdent) == 0 ? resourceIdent : variableIdent;
                        imageUrlControl.value = imageForm.imageUrl;
                        imageSrcContainer.resourceName = imageForm.imageUrl.substring(imageForm.imageUrl.indexOf(ident) + ident.length);
                        imageForm.setMode("ImageSrc");
                        if (imageForm.jsObject.options.imagesGallery) {
                            var item = imageSrcGallery.getItemByPropertyValue("name", imageSrcContainer.resourceName);
                            if (item) {
                                item.action(true);
                            }
                            else {
                                imageForm.setMode("ImageUrl");
                            }
                        }
                    }
                    else{
                        imageForm.setMode("ImageUrl");
                        imageUrlControl.value = imageForm.imageUrl;
                        imageUrlContainer.setImage(imageForm.imageUrl)
                    }
                }
            }
            else if (imageForm.imageFile) {
                imageForm.setMode("ImageFile");
                fileNameControl.setValue(imageForm.imageFile);
            }
            else if (imageForm.imageDataColumn) {
                imageForm.setMode("ImageDataColumn");
                imageSrcContainer.columnName = imageForm.imageDataColumn;                
                if (imageForm.jsObject.options.imagesGallery) {
                    var item = dataColumnGallery.getItemByPropertyValue("name", imageForm.imageDataColumn);
                    if (item) item.select(true);
                }
                dataColumnTree.setKey(imageForm.imageDataColumn);
                setTimeout(function () { dataColumnTree.autoscroll(); });
            }
            else if (imageForm.imageData) {
                imageDataControl.value = imageForm.imageData;
                var variableName = imageForm.imageData.length > 1 ? imageForm.imageData.substring(1, imageForm.imageData.length - 1) : "";
                var variable = this.jsObject.GetVariableByNameFromDictionary(variableName);
                if (variable) {
                    imageSrcContainer.variableName = variableName;
                    imageForm.setMode("ImageSrc");
                    if (imageForm.jsObject.options.imagesGallery) {
                        var item = imageSrcGallery.getItemByPropertyValue("name", variableName);
                        if (item) item.action(true);
                    }
                }
                else {
                    imageForm.setMode("ImageData");
                }
            }
        }
    }

    imageForm.action = function () {
        this.changeVisibleState(false);
        var selectedObjects = this.jsObject.options.selectedObjects || [this.jsObject.options.selectedObject];
        var propertyNames = ["imageSrc", "imageUrl", "imageFile", "imageDataColumn", "imageData"];

        for (var i = 0; i < selectedObjects.length; i++) {
            var selectedObject = selectedObjects[i];

            for (var k = 0; k < propertyNames.length; k++) {
                selectedObject.properties[propertyNames[k]] = "";
            }

            switch (imageForm.mode) {
                case "ImageSrc":
                    {
                        if (imageSrcContainer.variableName != null) {
                            selectedObject.properties.imageData = Base64.encode("{" + imageSrcContainer.variableName + "}");
                        }
                        else if (imageSrcContainer.resourceName != null) {
                            selectedObject.properties.imageUrl = Base64.encode(resourceIdent + imageSrcContainer.resourceName);
                        }
                        else if (imageSrcContainer.columnName != null) {
                            selectedObject.properties.imageDataColumn = Base64.encode(imageSrcContainer.columnName);
                        }
                        else {
                            var srcValue = !imageSrcContainer.src ? "" : imageSrcContainer.src;
                            selectedObject.properties.imageSrc = this.jsObject.options.mvcMode ? encodeURIComponent(srcValue) : srcValue;
                        }
                        break;
                    }
                case "ImageUrl":
                    {
                        selectedObject.properties.imageUrl = Base64.encode(imageUrlControl.value);
                        if (imageUrlControl.value && imageUrlContainer.src &&
                            imageUrlControl.value.indexOf(resourceIdent) != 0 &&
                            imageUrlControl.value.indexOf(variableIdent) != 0) {
                                imageForm.addImageItemToHistory(imageUrlControl.value, "StimulsoftMobileDesignerImageUrlHistory");
                        }
                        break;
                    }
                case "ImageFile":
                    {
                        selectedObject.properties.imageFile = Base64.encode(fileNameControl.getValue());
                        break;
                    }
                case "ImageDataColumn":
                    {
                        var dataColumnFullName = dataColumnTree.selectedItem && dataColumnTree.selectedItem.itemObject &&
                            (dataColumnTree.selectedItem.itemObject.typeItem == "Column" || dataColumnTree.selectedItem.itemObject.typeItem == "Parameter")
                                ? dataColumnTree.selectedItem.getFullName(true) : "";
                        selectedObject.properties.imageDataColumn = Base64.encode(dataColumnFullName);
                        break;
                    }
                case "ImageData":
                    {
                        selectedObject.properties.imageData = Base64.encode(imageDataControl.value);
                        break;
                    }                
                case "ImageServer":
                    {
                        if (imageForm.cloudContainer && imageForm.cloudContainer.item && this.jsObject.options.cloudServerUrl) {
                            selectedObject.properties.imageUrl = Base64.encode(this.jsObject.options.cloudServerUrl + imageForm.cloudContainer.item.itemObject.key);
                        }
                        else
                            selectedObject.properties.imageUrl = "";
                        break;
                    }
            }
        }
        imageForm.jsObject.SendCommandSendProperties(selectedObjects, propertyNames);
    }
        
    return imageForm;
}