﻿
StiMobileDesigner.prototype.InitializeSaveReportForm_ = function () {
    var saveReportForm = this.BaseForm("saveReport", this.loc.A_WebViewer.SaveReport, 2);
    saveReportForm.buttonOk.caption.innerHTML = this.loc.A_WebViewer.SaveReport;
    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px";
    saveReportForm.container.appendChild(innerTable);

    innerTable.addTextCell(this.loc.Cloud.labelFileName).className = "stiDesignerCaptionControlsBigIntervals";

    saveReportForm.reportNameTextBox = this.TextBox("saveReportTextBox", 200);
    var textBoxCell = innerTable.addCell(saveReportForm.reportNameTextBox);
    textBoxCell.className = "stiDesignerControlCellsBigIntervals";

    saveReportForm.show = function (saveAs, nextFunc) {
        this.caption.innerHTML = saveAs ? this.jsObject.loc.MainMenu.menuFileSaveAs.replace("...", "") : this.jsObject.loc.A_WebViewer.SaveReport;
        this.saveAs = saveAs;
        this.nextFunc = nextFunc;
        this.changeVisibleState(true);
        if (this.jsObject.options.report) {
            var fileName = this.jsObject.options.report.properties.reportFile || "Report.mrt";
            if (this.jsObject.options.cloudMode && this.jsObject.options.cloudParameters.reportTemplateItemKey) {
                fileName = this.jsObject.options.cloudParameters.reportName + ".mrt";
            }
            if (this.jsObject.options.report.encryptedPassword) {
                if (fileName.toLowerCase().endsWith(".mrt") || fileName.toLowerCase().endsWith(".mrz")) {
                    fileName = fileName.substring(0, fileName.length - 4) + ".mrx";
                }
            }
            this.reportNameTextBox.value = fileName;
        }
        this.reportNameTextBox.focus();
    }

    saveReportForm.action = function () {
        if (this.jsObject.options.report) {
            var reportFile = this.reportNameTextBox.value;
            this.jsObject.options.report.properties.reportFile = reportFile;
            var reportName = reportFile.substring(reportFile.lastIndexOf("/")).substring(reportFile.lastIndexOf("\\"));
            this.jsObject.SetWindowTitle(reportName ? reportName + " - " + this.jsObject.loc.FormDesigner.title : this.jsObject.loc.FormDesigner.title);
        }

        this.changeVisibleState(false);

        if (this.saveAs)
            this.jsObject.SendCommandSaveAsReport();
        else
            this.jsObject.SendCommandSaveReport();

        if (this.nextFunc) this.nextFunc();
    }

    this.addEvent(saveReportForm.reportNameTextBox, 'keydown', function (e) {
        if (e && e.keyCode == 13 && saveReportForm.jsObject.options.controlsIsFocused) {
            saveReportForm.action();
        }
    });

    return saveReportForm;
}