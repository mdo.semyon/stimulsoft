﻿
StiMobileDesigner.prototype.InitializeDataForm_ = function () {
    //Data Form
    var dataForm = this.BaseFormPanel("dataForm", this.loc.PropertyMain.Data, 1, this.HelpLinks["data"]);

    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerImageFormMainPanel";
    dataForm.container.appendChild(mainTable);
    dataForm.container.style.padding = "0px";

    var buttonProps = [
        ["DataSource", "DataFormDataSource.png", this.loc.PropertyMain.DataSource],
        ["Relation", "DataFormDataRelation.png", this.loc.PropertyMain.DataRelation],
        ["MasterComponent", "DataFormMasterComponent.png", this.loc.PropertyMain.MasterComponent],
        ["Sort", "DataFormSort.png", this.loc.PropertyMain.Sort],
        ["Filter", "DataFormFilters.png", this.loc.PropertyMain.Filters]
    ];

    //Add Panels && Buttons
    var panelsContainer = mainTable.addCell();
    var buttonsPanel = mainTable.addCell();
    buttonsPanel.style.verticalAlign = "top";
    dataForm.mainButtons = {};
    dataForm.panels = {};

    for (var i = 0; i < buttonProps.length; i++) {
        var panel = document.createElement("Div");
        panel.className = "stiDesignerEditFormPanel";
        panel.style.width = "560px";
        if (i != 0) panel.style.display = "none";
        panelsContainer.appendChild(panel);
        dataForm.panels[buttonProps[i][0]] = panel;
        panel.onShow = function () { };

        var button = this.StandartFormBigButton("dataForm" + buttonProps[i][0] + "Button", null, buttonProps[i][2], buttonProps[i][1], buttonProps[i][2], 80);
        button.style.margin = "2px";
        button.style.minWidth = "110px";
        if (button.caption) button.caption.style.padding = "4px 3px 4px 3px";
        dataForm.mainButtons[buttonProps[i][0]] = button;
        buttonsPanel.appendChild(button);
        button.panelName = buttonProps[i][0];
        button.action = function () {
            dataForm.setMode(this.panelName);
        }

        //add marker
        var marker = document.createElement("div");
        marker.style.display = "none";
        marker.className = "stiUsingMarker";
        var markerInner = document.createElement("div");
        marker.appendChild(markerInner);
        button.style.position = "relative";
        button.appendChild(marker);
        button.marker = marker;
    }

    //Data Source
    var toolBarDataSource = this.CreateHTMLTable();
    dataForm.panels.DataSource.appendChild(toolBarDataSource);
    var newDataSourceButton = this.StandartSmallButton("dataFormNewDataSourceButton", null, this.loc.FormDictionaryDesigner.DataSourceNew, "DataSourceNew.png", null, null, true);
    var newBusinessObjectButton = this.StandartSmallButton("dataFormNewBusinessObjectButton", null, this.loc.FormDictionaryDesigner.NewBusinessObject, "BusinessObjectNew.png", null, null, true);
    newDataSourceButton.setEnabled(this.options.permissionDataSources.indexOf("All") >= 0 || this.options.permissionDataSources.indexOf("Create") >= 0);
    newBusinessObjectButton.setEnabled(this.options.permissionBusinessObjects.indexOf("All") >= 0 || this.options.permissionBusinessObjects.indexOf("Create") >= 0);
    if (this.options.jsMode) newBusinessObjectButton.style.display = "none";

    toolBarDataSource.addCell(newDataSourceButton).style.padding = "4px";
    toolBarDataSource.addCell(newBusinessObjectButton).style.padding = "4px";
    newDataSourceButton.action = function () {
        this.jsObject.InitializeSelectConnectionForm(function (selectConnectionForm) {
            selectConnectionForm.changeVisibleState(true);
        });
    };
    newBusinessObjectButton.action = function () {
        dataForm.sinchronizeWithMainDictionary();
        this.jsObject.InitializeEditDataSourceForm(function (editDataSourceForm) {
            editDataSourceForm.datasource = "BusinessObject";
            editDataSourceForm.changeVisibleState(true);
        });
    };

    dataForm.panels.DataSource.appendChild(this.FormSeparator());

    var dataSourcesTree = this.DataSourcesTree();
    dataSourcesTree.style.height = this.options.isTouchDevice ? "374px" : "384px";
    dataSourcesTree.style.overflow = "auto";
    dataForm.panels.DataSource.appendChild(dataSourcesTree);
    dataSourcesTree.action = function () { dataForm.action(); }

    dataForm.panels.DataSource.appendChild(this.FormSeparator());

    var toolBar2DataSource = this.CreateHTMLTable();
    dataForm.panels.DataSource.appendChild(toolBar2DataSource);
    var textCount = toolBar2DataSource.addCell();
    textCount.className = "stiDesignerCaptionControls";
    textCount.innerHTML = this.loc.PropertyMain.CountData + ": ";
    var countData = this.TextBox(null, 70);
    toolBar2DataSource.addCell(countData).style.padding = "5px 15px 5px 0";
    countData.action = function () {
        this.value = this.jsObject.StrToCorrectPositiveInt(this.value);
        dataSourcesTree.update();
    }
    dataSourcesTree.update = function () { dataSourcesTree.setEnabled(countData.value == "0"); }

    //Relation
    var toolBarRelation = this.CreateHTMLTable();
    dataForm.panels.Relation.appendChild(toolBarRelation);
    var newRelationButton = this.StandartSmallButton("dataFormNewRelationButton", null, this.loc.FormDictionaryDesigner.RelationNew, "RelationNew.png", null, null, true);
    newRelationButton.setEnabled(this.options.permissionDataRelations.indexOf("All") >= 0 || this.options.permissionDataRelations.indexOf("Create") >= 0);
    toolBarRelation.addCell(newRelationButton).style.padding = "4px";
    newRelationButton.action = function () {
        dataForm.sinchronizeWithMainDictionary();
        this.jsObject.InitializeEditRelationForm(function (editRelationForm) {
            editRelationForm.relation = null;
            editRelationForm.changeVisibleState(true);
        });
    };

    dataForm.panels.Relation.appendChild(this.FormSeparator());

    var relationsTree = this.RelationsTree();
    dataForm.panels.Relation.appendChild(relationsTree);
    relationsTree.action = function () { dataForm.action(); }

    dataForm.panels.Relation.onShow = function () {
        var dataSource = dataForm.jsObject.GetDataSourceByNameFromDictionary(dataSourcesTree.selectedItem ? dataSourcesTree.selectedItem.itemObject.name : "");
        var currRelationNameInSource = relationsTree.selectedItem ? relationsTree.selectedItem.itemObject.nameInSource : null;
        relationsTree.build(dataSource);
        if (currRelationNameInSource) {
            var relationItem = relationsTree.getItem(currRelationNameInSource, "nameInSource");
            if (relationItem) relationItem.setSelected();
        }
    }

    //MasterComponent
    var masterComponentsTree = this.MasterComponentsTree();
    dataForm.panels.MasterComponent.appendChild(masterComponentsTree);
    masterComponentsTree.action = function () { dataForm.action(); }

    //Sort
    var sortControl = this.SortControl("dataFormSortControl" + this.newGuid().replace(/-/g, ''), null, null, this.options.isTouchDevice ? 408 : 413);
    dataForm.panels.Sort.appendChild(sortControl);
    dataForm.panels.Sort.onShow = function () {
        var currentDataSourceName = dataForm.getCurrentDataSourceName();
        if (sortControl.currentDataSourceName != currentDataSourceName) sortControl.sortContainer.clear();
        sortControl.currentDataSourceName = currentDataSourceName;
    }

    //Filter
    var filterControl = this.FilterControl("dataFormFilterControl" + this.newGuid().replace(/-/g, ''), null, null, this.options.isTouchDevice ? 373 : 383);
    dataForm.panels.Filter.appendChild(filterControl);
    dataForm.panels.Filter.onShow = function () {
        var currentDataSourceName = dataForm.getCurrentDataSourceName();
        if (filterControl.currentDataSourceName != currentDataSourceName) filterControl.controls.filterContainer.clear();
        filterControl.currentDataSourceName = currentDataSourceName;
    }

    dataForm.panels.Filter.appendChild(this.FormSeparator());

    var toolBarFilter = this.CreateHTMLTable();
    dataForm.panels.Filter.appendChild(toolBarFilter);
    var textFilterEngine = toolBarFilter.addCell();
    textFilterEngine.className = "stiDesignerCaptionControls";
    textFilterEngine.innerHTML = this.loc.PropertyMain.FilterEngine + ": ";
    var filterEngine = this.DropDownList("dataFormFilterEngine", 125, this.loc.PropertyMain.FilterEngine, this.GetFilterIngineItems(), true, false);
    toolBarFilter.addCell(filterEngine).style.padding = "5px 15px 0 0";


    //Form Methods
    dataForm.sinchronizeWithMainDictionary = function () {
        if (this.jsObject.options.dictionaryTree && dataSourcesTree.selectedItem && dataSourcesTree.selectedItem.itemObject.typeItem && dataSourcesTree.selectedItem.itemObject.name) {
            var item = this.jsObject.options.dictionaryTree.getItemByNameAndType(dataSourcesTree.selectedItem.itemObject.name, dataSourcesTree.selectedItem.itemObject.typeItem);
            if (item) item.setSelected();
        }
    }

    dataForm.getCurrentDataSourceName = function () {
        var dataSourceName = dataSourcesTree.selectedItem &&
            (dataSourcesTree.selectedItem.itemObject.typeItem == "DataSource" || dataSourcesTree.selectedItem.itemObject.typeItem == "BusinessObject")
                ? dataSourcesTree.selectedItem.itemObject.name : null;
        return dataSourceName;
    }

    dataForm.setMode = function (mode) {
        dataForm.mode = mode;
        for (var panelName in dataForm.panels) {
            dataForm.panels[panelName].style.display = mode == panelName ? "" : "none";
            dataForm.mainButtons[panelName].setSelected(mode == panelName);
            if (mode == panelName) dataForm.panels[panelName].onShow();
        }
        var propertiesPanel = dataForm.jsObject.options.propertiesPanel;
        propertiesPanel.setEnabled(false);
    }

    dataForm.onhide = function () {
        dataForm.jsObject.options.propertiesPanel.setDictionaryMode(false);
        clearTimeout(this.markerTimer);
    }

    dataForm.rebuildTrees = function (objectName, objectType) {
        if (objectName) {
            if (objectType == "DataSource" || objectType == "BusinessObject") {
                dataSourcesTree.build(true);
                var dataSourceItem = dataSourcesTree.getItem(objectName, null, "DataSource");
                if (!dataSourceItem) {
                    var fullName = objectName;
                    if (fullName && fullName != "StiEmptyValue") {
                        var lastName = fullName.substring(fullName.lastIndexOf(".") + 1);
                        dataSourceItem = dataSourcesTree.getItem(lastName, null, "BusinessObject");
                    }
                }
                if (dataSourceItem) {
                    dataSourceItem.setSelected();
                    dataSourceItem.openTree();
                    dataSourcesTree.update();
                }
            }
            else if (objectType == "Relation") {
                var dataSource = this.jsObject.GetDataSourceByNameFromDictionary(dataSourcesTree.selectedItem ? dataSourcesTree.selectedItem.itemObject.name : "");
                relationsTree.build(dataSource);
                var relationItem = relationsTree.getItem(objectName, "nameInSource");
                if (relationItem) relationItem.setSelected();
            }
        }
    }

    dataForm.updateMarkers = function () {
        this.mainButtons["DataSource"].marker.style.display = dataSourcesTree.selectedItem && dataSourcesTree.selectedItem.itemObject.typeItem == "DataSource" ? "" : "none";
        this.mainButtons["Relation"].marker.style.display = relationsTree.selectedItem && relationsTree.selectedItem.itemObject.typeItem == "Relation" ? "" : "none";
        this.mainButtons["MasterComponent"].marker.style.display = masterComponentsTree.selectedItem && masterComponentsTree.selectedItem.itemObject.typeItem != "NoItem" ? "" : "none";
        this.mainButtons["Sort"].marker.style.display = sortControl.sortContainer.getCountItems() > 0 ? "" : "none";
        this.mainButtons["Filter"].marker.style.display = filterControl.controls.filterContainer.getCountItems() > 0 ? "" : "none";
    }

    dataForm.onshow = function () {
        var currentObject = this.jsObject.options.selectedObject || this.jsObject.GetCommonObject(this.jsObject.options.selectedObjects);
        if (!currentObject) return;

        //DataSource & BusinessObject
        dataForm.setMode("DataSource");
        dataSourcesTree.build(true);
        dataForm.jsObject.options.propertiesPanel.setDictionaryMode(true);

        countData.value = currentObject.typeComponent && currentObject.properties.countData != "StiEmptyValue" ? currentObject.properties.countData : "0";

        var dataSourceItem = currentObject.properties.dataSource != "StiEmptyValue" ? dataSourcesTree.getItem(currentObject.properties.dataSource, null, "DataSource") : null;
        if (!dataSourceItem) {
            var fullName = currentObject.properties.businessObject;
            if (fullName && fullName != "StiEmptyValue") {
                var lastName = fullName.substring(fullName.lastIndexOf(".") + 1);
                dataSourceItem = dataSourcesTree.getItem(lastName, null, "BusinessObject");
            }
        }
        if (dataSourceItem) {
            dataSourceItem.setSelected();
            dataSourceItem.openTree();
        }
        dataSourcesTree.update();

        //Relation
        relationsTree.build(dataSourceItem ? dataSourceItem.itemObject : null);
        var relationItem = currentObject.properties.dataRelation != "StiEmptyValue" ? relationsTree.getItem(currentObject.properties.dataRelation, "nameInSource") : null;
        if (relationItem) relationItem.setSelected();

        //MasterComponent
        masterComponentsTree.build();
        var masterComponentItem = currentObject.properties.masterComponent != "StiEmptyValue" ? masterComponentsTree.getItem(currentObject.properties.masterComponent) : null;
        if (masterComponentItem) masterComponentItem.setSelected();

        //Sort
        var sorts = currentObject.properties.sortData != "" && currentObject.properties.sortData != "StiEmptyValue" ? JSON.parse(Base64.decode(currentObject.properties.sortData)) : [];
        sortControl.currentDataSourceName = dataForm.getCurrentDataSourceName();
        sortControl.fill(sorts);

        //Filter
        filterEngine.setKey(currentObject.properties.filterEngine != "StiEmptyValue" ? currentObject.properties.filterEngine : "ReportEngine");
        var filters = currentObject.properties.filterData != "StiEmptyValue" && currentObject.properties.filterData != "" ? JSON.parse(Base64.decode(currentObject.properties.filterData)) : [];
        filterControl.currentDataSourceName = dataForm.getCurrentDataSourceName();
        filterControl.fill(
            filters,
            currentObject.properties.filterOn != "StiEmptyValue" ? currentObject.properties.filterOn : true,
            currentObject.properties.filterMode != "StiEmptyValue" ? currentObject.properties.filterMode : "And"
            );

        this.updateMarkers();
        this.markerTimer = setInterval(function () {
            dataForm.updateMarkers();
        }, 250)
    }

    dataForm.action = function () {
        this.changeVisibleState(false);
        var selectedObjects = this.jsObject.options.selectedObjects || [this.jsObject.options.selectedObject];
        if (!selectedObjects) return;
        for (var i = 0; i < selectedObjects.length; i++) {
            var currentObject = selectedObjects[i];
            currentObject.properties.dataSource = dataSourcesTree.selectedItem && dataSourcesTree.selectedItem.itemObject.typeItem == "DataSource" ? dataSourcesTree.selectedItem.itemObject.name : "[Not Assigned]";
            currentObject.properties.businessObject = dataSourcesTree.selectedItem && dataSourcesTree.selectedItem.itemObject.typeItem == "BusinessObject" ? dataSourcesTree.selectedItem.getBusinessObjectStringFullName() : "[Not Assigned]";
            currentObject.properties.dataRelation = relationsTree.selectedItem && relationsTree.selectedItem.itemObject.typeItem == "Relation" ? relationsTree.selectedItem.itemObject.nameInSource : "[Not Assigned]";
            currentObject.properties.masterComponent = masterComponentsTree.selectedItem && masterComponentsTree.selectedItem.itemObject.typeItem != "NoItem" ? masterComponentsTree.selectedItem.itemObject.name : "[Not Assigned]";
            currentObject.properties.countData = countData.value;
            currentObject.properties.filterEngine = filterEngine.key;

            var filterResult = filterControl.getValue();
            currentObject.properties.filterOn = filterResult.filterOn;
            currentObject.properties.filterMode = filterResult.filterMode;
            currentObject.properties.filterData = Base64.encode(JSON.stringify(filterResult.filters));

            var sortResult = sortControl.getValue();
            currentObject.properties.sortData = sortResult.length == 0 ? "" : Base64.encode(JSON.stringify(sortResult));
        }

        this.jsObject.SendCommandSendProperties(selectedObjects, ["dataSource", "dataRelation", "filterData", "masterComponent", "businessObject",
            "countData", "filterEngine", "filterOn", "filterMode", "sortData"]);
    }

    return dataForm;
}