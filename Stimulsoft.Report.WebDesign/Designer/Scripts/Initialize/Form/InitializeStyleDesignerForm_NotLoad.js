﻿
StiMobileDesigner.prototype.InitializeStyleDesignerForm_ = function () {

    var styleDesignerForm = this.BaseFormPanel("styleDesignerForm", this.loc.Toolbars.StyleDesigner, 2, this.HelpLinks["styleDesigner"]);
        
    //MainTable
    var mainTable = this.CreateHTMLTable();
    styleDesignerForm.container.appendChild(mainTable);
    mainTable.style.borderCollapse = "separate";

    //Toolbar
    var buttons = [
        ["actions", this.loc.FormDictionaryDesigner.Actions, null, null, "Down"],
        ["addStyle", this.loc.FormStyleDesigner.Add, "StyleAdd.png", null, "Down"],
        ["removeStyle", null, "Remove.png", this.loc.FormStyleDesigner.Remove, false],
        ["separator"],
        ["getStyle", null, "Styles.StylesGet.png", this.loc.FormStyleDesigner.GetStyle, false],
        ["separator"],
        ["settings", null, "Settings.png", null, "Down"],
        ["separator"]
    ]

    styleDesignerForm.toolBar = this.CreateHTMLTable();
    var toolBarCell = mainTable.addCell(styleDesignerForm.toolBar);
    toolBarCell.className = "stiDesignerStyleDesignerFormToolbarCell";
        
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i][0] == "separator") {
            styleDesignerForm.toolBar.addCell(this.HomePanelSeparator());
            continue;
        }
        var button = this.SmallButton(buttons[i][0], null, buttons[i][1], buttons[i][2], buttons[i][3], buttons[i][4], this.GetStyles("StandartSmallButton"));
        button.style.margin = "4px";
        styleDesignerForm.toolBar[buttons[i][0]] = button;
        styleDesignerForm.toolBar.addCell(button);
    }
        
    var findTextbox = this.TextBox("styleDesignerFormFindTextbox", 150);
    styleDesignerForm.findTextbox = findTextbox;
    findTextbox.style.marginLeft = "3px";
    styleDesignerForm.toolBar.addCell(findTextbox);

    var findImg = document.createElement("img");
    findImg.src = this.options.images["View.png"];
    styleDesignerForm.toolBar.addCell(findImg).style.padding = "5px 3px 0 3px";

    findTextbox.onchange = function () {
        styleDesignerForm.stylesTree.findItems(this.value);
    }

    //Get Style
    styleDesignerForm.toolBar.getStyle.action = function () {
        var componentsNames = styleDesignerForm.getSelectedComponentsNames();
        this.jsObject.SendCommandCreateStylesFromComponents(componentsNames);
    }

    //Remove Style
    styleDesignerForm.toolBar.removeStyle.action = function () {
        var selectedItem = styleDesignerForm.stylesTree.selectedItem;
        if (selectedItem) selectedItem.remove();
    }

    //Actions
    this.InitializeStyleActionsMenu(styleDesignerForm);

    //Add Style Menu
    this.InitializeAddStyleMenu(styleDesignerForm);

    //Settings
    this.InitializeSettingsStylesMenu(styleDesignerForm);

    //Items Tree
    styleDesignerForm.stylesTree = this.StylesTree(styleDesignerForm);
    mainTable.addCellInNextRow(styleDesignerForm.stylesTree).style.width = "100%";

    //PropertiesPanel
    styleDesignerForm.propertiesPanel = this.StyleDesignerFormPropertiesPanel(styleDesignerForm);
    var styleDesignerPropertiesPanel = this.options.propertiesPanel.styleDesignerPropertiesPanel;
    while (styleDesignerPropertiesPanel.childNodes[0]) styleDesignerPropertiesPanel.removeChild(styleDesignerPropertiesPanel.childNodes[0]);
    this.options.propertiesPanel.styleDesignerPropertiesPanel.appendChild(styleDesignerForm.propertiesPanel);

    //Methods
    styleDesignerForm.toolBar.updateControls = function () {
        var selectedItem = styleDesignerForm.stylesTree.selectedItem;
        styleDesignerForm.toolBar.removeStyle.setEnabled(selectedItem && selectedItem.itemObject.typeItem != "MainItem");

        var componentsNames = styleDesignerForm.getSelectedComponentsNames();
        styleDesignerForm.toolBar.getStyle.setEnabled(componentsNames.length > 0);
    }

    styleDesignerForm.getSelectedComponentsNames = function () {
        var selectedComponents = styleDesignerForm.jsObject.options.selectedObject
            ? [styleDesignerForm.jsObject.options.selectedObject]
            : (styleDesignerForm.jsObject.options.selectedObjects || []);

        var componentsNames = [];
        for (var i = 0; i < selectedComponents.length; i++) {
            if (selectedComponents[i].typeComponent != "StiPage" && selectedComponents[i].typeComponent != "StiReport") {
                componentsNames.push(selectedComponents[i].properties.name);
            }
        }

        return componentsNames;
    }

    styleDesignerForm.onshow = function () {
        findTextbox.value = "";
        this.settings = {
            filter: {
                "StiStyle": true,
                "StiChartStyle": true,
                "StiCrossTabStyle": true,
                "StiMapStyle": true,
                "StiGaugeStyle": true,
                "StiTableStyle": true
            },
            sort: "NoSorting"
        }
        this.jsObject.options.propertiesPanel.setStyleDesignerMode(true);
        this.copiedStyle = null;
        this.stylesCollection = this.jsObject.CopyObject(this.jsObject.options.report.stylesCollection);
        this.usingStyles = this.jsObject.GetAllUsingStyles();
        this.stylesTree.updateItems(this.stylesCollection);
    }

    styleDesignerForm.addStylesCollection = function (newStylesCollection, collectionName, removeExistingStyles) {
        if (removeExistingStyles) this.stylesCollection = [];
        this.stylesCollection = this.stylesCollection.concat(newStylesCollection);
        this.stylesTree.openedItems[collectionName] = true;
        this.stylesTree.updateItems(this.stylesCollection, { typeItem: "Folder", collectionName: collectionName }, true, true);
    }
    
    styleDesignerForm.onhide = function () {
        this.jsObject.options.propertiesPanel.setStyleDesignerMode(false);
    }

    styleDesignerForm.action = function () {
        this.changeVisibleState(false);
        this.jsObject.SendCommandUpdateStyles(this.stylesCollection);
    }

    return styleDesignerForm;
}

StiMobileDesigner.prototype.StyleDesignerFormPropertiesPanel = function (styleDesignerForm) {
    var propertiesPanel = document.createElement("div");
    propertiesPanel.className = "stiDesignerStyleDesignerFormPropertiesPanel";
    propertiesPanel.propertiesGroups = {};

    //Add Properties Groups
    var propertiesGroups = [
        ["main", this.loc.PropertyCategory.MainCategory],
        ["appearance", this.loc.PropertyCategory.AppearanceCategory],
        ["map", this.loc.Components.StiMap],
        ["parameters", this.loc.PropertyCategory.ParametersCategory],
        ["gridLines", this.loc.Chart.GridLines],
        ["area", this.loc.PropertyCategory.AreaCategory],
        ["series", this.loc.Chart.Series],
        ["seriesLabels", this.loc.PropertyCategory.SeriesLabelsCategory],
        ["trendLine", this.loc.PropertyCategory.TrendLineCategory],
        ["legend", this.loc.PropertyCategory.LegendCategory],
        ["axis", this.loc.PropertyCategory.AxisCategory],
        ["interlacing", this.loc.PropertyCategory.InterlacingCategory]
    ];

    for (var i = 0; i < propertiesGroups.length; i++) {
        var propertiesGroup = this.PropertiesGroup(propertiesGroups[i][0] + "StylesDesigner", propertiesGroups[i][1]);
        propertiesGroup.style.margin = "5px 0 5px 0";
        propertiesGroup.style.display = "none";
        propertiesPanel.propertiesGroups[propertiesGroups[i][0]] = propertiesGroup;
        propertiesPanel.appendChild(propertiesGroup);
        propertiesGroup.changeOpenedState(true);
    }

    //Add Properties Controls
    propertiesPanel.properties_ = {};
    var propAttributes = [
        ["name", this.loc.PropertyMain.Name, this.PropertyTextBox("styleDesignerPropertyName", this.options.propertyControlWidth), "main"],
        ["description", this.loc.PropertyMain.Description, this.PropertyTextBox("styleDesignerPropertyDescription", this.options.propertyControlWidth), "main"],
        ["collectionName", this.loc.PropertyMain.CollectionName, this.PropertyTextBox("styleDesignerPropertyCollectionName", this.options.propertyControlWidth), "main"],
        ["conditions", this.loc.PropertyMain.Conditions, this.PropertyTextBoxWithEditButton("styleDesignerPropertyConditions", this.options.propertyControlWidth, true), "main"],
        ["borderColor", this.loc.PropertyMain.BorderColor, this.PropertyColorControl("styleDesignerPropertyBorderColor", null, this.options.propertyControlWidth), "map"],
        ["borderSize", this.loc.PropertyMain.BorderSize, this.PropertyTextBox("styleDesignerPropertyBorderSize", this.options.propertyNumbersControlWidth), "map"],
        ["brush", this.loc.PropertyMain.Brush, this.PropertyBrushControl("styleDesignerPropertyBrush", null, this.options.propertyControlWidth), "appearance"],
        ["textBrush", this.loc.PropertyMain.TextBrush, this.PropertyBrushControl("styleDesignerPropertyTextBrush", null, this.options.propertyControlWidth), "appearance"],
        ["border", this.loc.PropertyMain.Borders, this.PropertyBorderControl("styleDesignerPropertyBorder", this.options.propertyControlWidth), "appearance"],
        ["font", this.loc.PropertyMain.Font, this.PropertyFontControl("styleDesignerPropertyFont", null, true), "appearance"],
        ["horAlignment", this.loc.PropertyMain.HorAlignment, this.PropertyDropDownList("styleDesignerPropertyHorizontalAlignment", this.options.propertyControlWidth, this.GetHorizontalAlignmentItems(), true, false), "appearance"],
        ["vertAlignment", this.loc.PropertyMain.VertAlignment, this.PropertyDropDownList("styleDesignerPropertyVerticalAlignment", this.options.propertyControlWidth, this.GetVerticalAlignmentItems(), true, false), "appearance"],
        ["color", this.loc.PropertyMain.Color, this.PropertyColorControl("styleDesignerPropertyColor", null, this.options.propertyControlWidth), "appearance"],
        ["backColor", this.loc.PropertyMain.BackColor, this.PropertyColorControl("styleDesignerPropertyBackColor", null, this.options.propertyControlWidth), "parameters"],
        ["colors", this.loc.PropertyCategory.ColorsCategory, this.PropertyColorsCollectionControl("styleDesignerPropertyColors", null, this.options.propertyControlWidth), "parameters"],
        ["foreColor", this.loc.PropertyMain.ForeColor, this.PropertyColorControl("styleDesignerPropertyForeColor", null, this.options.propertyControlWidth), "parameters"],
        ["defaultColor", this.loc.PropertyMain.DefaultColor, this.PropertyColorControl("styleDesignerPropertyDefaultColor", null, this.options.propertyControlWidth), "parameters"],
        ["heatmapColors", this.loc.PropertyMain.HeatmapColors, this.PropertyColorsCollectionControl("styleDesignerPropertyHeatmapColors", null, this.options.propertyControlWidth), "parameters"],
        ["allowUseBackColor", this.loc.PropertyMain.AllowUseBackColor, this.CheckBox("styleDesignerPropertyAllowUseBackColor"), "parameters"],
        ["allowUseForeColor", this.loc.PropertyMain.AllowUseForeColor, this.CheckBox("styleDesignerPropertyAllowUseForeColor"), "parameters"],
        ["allowUseBorderFormatting", this.loc.PropertyMain.AllowUseBorderFormatting, this.CheckBox("styleDesignerPropertyAllowUseBorderFormatting"), "parameters"],
        ["allowUseBorderSides", this.loc.PropertyMain.AllowUseBorderSides, this.CheckBox("styleDesignerPropertyAllowUseBorderSides"), "parameters"],
        ["allowUseBorderSidesFromLocation", this.loc.PropertyMain.AllowUseBorderSidesFromLocation, this.CheckBox("styleDesignerPropertyAllowUseBorderSidesFromLocation"), "parameters"],
        ["allowUseBrush", this.loc.PropertyMain.AllowUseBrush, this.CheckBox("styleDesignerPropertyAllowUseBrush"), "parameters"],
        ["allowUseFont", this.loc.PropertyMain.AllowUseFont, this.CheckBox("styleDesignerPropertyAllowUseFont"), "parameters"],
        ["allowUseImage", this.loc.PropertyMain.AllowUseImage, this.CheckBox("styleDesignerPropertyAllowUseImage"), "parameters"],
        ["allowUseTextBrush", this.loc.PropertyMain.AllowUseTextBrush, this.CheckBox("styleDesignerPropertyAllowUseTextBrush"), "parameters"],
        ["allowUseHorAlignment", this.loc.PropertyMain.AllowUseHorAlignment, this.CheckBox("styleDesignerPropertyAllowUseHorAlignment"), "parameters"],
        ["allowUseVertAlignment", this.loc.PropertyMain.AllowUseVertAlignment, this.CheckBox("styleDesignerPropertyAllowUseVertAlignment"), "parameters"],
        ["basicStyleColor", this.loc.PropertyMain.BasicStyleColor, this.PropertyColorControl("styleDesignerPropertyBasicStyleColor", null, this.options.propertyControlWidth), "parameters"],
        ["styleColors", this.loc.PropertyMain.StyleColors, this.PropertyColorsCollectionControl("styleDesignerPropertyStyleColors", null, this.options.propertyControlWidth), "parameters"],
        ["brushType", this.loc.PropertyMain.BrushType, this.PropertyDropDownList("styleDesignerPropertyBrushType", this.options.propertyControlWidth, this.GetChartStyleBrushTypeItems(), true, false), "parameters"],
        ["chartAreaShowShadow", this.loc.PropertyMain.ChartAreaShowShadow, this.CheckBox("styleDesignerAreaShowShadow"), "area"],
        ["chartAreaBorderColor", this.loc.PropertyMain.ChartAreaBorderColor, this.PropertyColorControl("styleDesignerAreaBorderColor", null, this.options.propertyControlWidth), "area"],
        ["chartAreaBrush", this.loc.PropertyMain.ChartAreaBrush, this.PropertyBrushControl("styleDesignerAreaBrush", null, this.options.propertyControlWidth), "area"],
        ["seriesLabelsBorderColor", this.loc.PropertyMain.SeriesLabelsBorderColor, this.PropertyColorControl("styleDesignerSeriesLabelsBorderColor", null, this.options.propertyControlWidth), "seriesLabels"],
        ["seriesLabelsColor", this.loc.PropertyMain.SeriesLabelsColor, this.PropertyColorControl("styleDesignerSeriesLabelsColor", null, this.options.propertyControlWidth), "seriesLabels"],
        ["seriesLabelsBrush", this.loc.PropertyMain.SeriesLabelsBrush, this.PropertyBrushControl("styleDesignerSeriesLabelsBrush", null, this.options.propertyControlWidth), "seriesLabels"],
        ["seriesLighting", this.loc.PropertyMain.SeriesLighting, this.CheckBox("styleDesignerPropertySeriesLighting"), "series"],
        ["seriesShowShadow", this.loc.PropertyMain.SeriesShowShadow, this.CheckBox("styleDesignerPropertySeriesShowShadow"), "series"],
        ["trendLineShowShadow", this.loc.PropertyMain.TrendLineShowShadow, this.CheckBox("styleDesignerTrendLineShowShadow"), "trendLine"],
        ["trendLineColor", this.loc.PropertyMain.TrendLineColor, this.PropertyColorControl("styleDesignerTrendLineColor", null, this.options.propertyControlWidth), "trendLine"],
        ["legendBorderColor", this.loc.PropertyMain.LegendBorderColor, this.PropertyColorControl("styleDesignerLegendBorderColor", null, this.options.propertyControlWidth), "legend"],
        ["legendLabelsColor", this.loc.PropertyMain.LegendLabelsColor, this.PropertyColorControl("styleDesignerLegendLabelsColor", null, this.options.propertyControlWidth), "legend"],
        ["legendTitleColor", this.loc.PropertyMain.LegendTitleColor, this.PropertyColorControl("styleDesignerLegendTitleColor", null, this.options.propertyControlWidth), "legend"],
        ["legendBrush", this.loc.PropertyMain.LegendBrush, this.PropertyBrushControl("styleDesignerLegendBrush", null, this.options.propertyControlWidth), "legend"],
        ["axisLabelsColor", this.loc.PropertyMain.AxisLabelsColor, this.PropertyColorControl("styleDesignerAxisLabelsColor", null, this.options.propertyControlWidth), "axis"],
        ["axisLineColor", this.loc.PropertyMain.AxisLineColor, this.PropertyColorControl("styleDesignerAxisLineColor", null, this.options.propertyControlWidth), "axis"],
        ["axisTitleColor", this.loc.PropertyMain.AxisTitleColor, this.PropertyColorControl("styleDesignerAxisTitleColor", null, this.options.propertyControlWidth), "axis"],
        ["interlacingHorBrush", this.loc.PropertyMain.InterlacingHorBrush, this.PropertyBrushControl("styleDesignerInterlacingHorBrush", null, this.options.propertyControlWidth), "interlacing"],
        ["interlacingVertBrush", this.loc.PropertyMain.InterlacingVertBrush, this.PropertyBrushControl("styleDesignerInterlacingVertBrush", null, this.options.propertyControlWidth), "interlacing"],
        ["gridLinesHorColor", this.loc.PropertyMain.GridLinesHorColor, this.PropertyColorControl("styleDesignerGridLinesHorColor", null, this.options.propertyControlWidth), "gridLines"],
        ["gridLinesVertColor", this.loc.PropertyMain.GridLinesVertColor, this.PropertyColorControl("styleDesignerGridLinesVertColor", null, this.options.propertyControlWidth), "gridLines"]
    ]

    for (var i = 0; i < propAttributes.length; i++) {
        var control = propAttributes[i][2];
        var property = this.Property(null, propAttributes[i][1], control);
        property.name = propAttributes[i][0];
        property.updateCaption();
        propertiesPanel.propertiesGroups[propAttributes[i][3]].container.appendChild(property);
        propertiesPanel.properties_[propAttributes[i][0]] = property;
        control.propertyName = propAttributes[i][0];
        control.groupName = propAttributes[i][3];

        control.action = function () {
            var needUpdateItems = false;

            var selectedItem = styleDesignerForm.stylesTree.selectedItem;
            if (!selectedItem) return;

            var styleProperties = selectedItem.itemObject.properties;
            if (!styleProperties && selectedItem.itemObject.typeItem == "Folder") styleProperties = selectedItem.itemObject;
            if (!styleProperties) return;

            if (this.propertyName == "collectionName" && this.value != styleProperties.collectionName) {
                needUpdateItems = true;
                if (styleProperties.typeItem == "Folder") {
                    for (var itemKey in selectedItem.childs) {
                        var itemObject = selectedItem.childs[itemKey].itemObject;
                        if (itemObject && itemObject.properties) {
                            itemObject.properties.collectionName = this.value;
                        }
                    }
                }
            }

            if (this.propertyName == "name" && (!this.value || styleDesignerForm.stylesTree.checkExistStyleName(this.value))) {
                this.value = styleProperties[this.propertyName];
                return;
            }

            if (this["setKey"] != null) styleProperties[this.propertyName] = this.key;
            else if (this["setChecked"] != null) styleProperties[this.propertyName] = this.isChecked;
            else if (this["value"] != null) styleProperties[this.propertyName] = this.value;
            else if (this["textBox"] != null && this.textBox["value"] != null) styleProperties[this.propertyName] = this.textBox.value;
            selectedItem.repaint();
            if (needUpdateItems) {
                styleDesignerForm.stylesTree.updateItems(styleDesignerForm.stylesCollection, selectedItem.itemObject, true, true);
            }
        }

        if (propAttributes[i][0] == "conditions") {
            control.button.action = function () {
                var this_ = this;
                this.jsObject.InitializeStyleConditionsForm(function (styleConditionsForm) {
                    styleConditionsForm.show(styleDesignerForm, this_.textBox);
                });
            }
        }
    }

    propertiesPanel.updatePropertiesCaptions = function () {
        for (var propertyName in this.properties_) {
            this.properties_[propertyName].updateCaption();
        }
    }
        
    propertiesPanel.updateControls = function (styleObject) {
        var propertyValues = styleObject ? (styleObject.typeItem == "Folder" ? { collectionName: styleObject.collectionName } : styleObject.properties) : null;
        if (!propertyValues) propertyValues = {};
        var propertyRows = this.properties_;
        var showingGroups = {};

        for (var i = 0; i < propertiesGroups.length; i++) {
            propertiesPanel.propertiesGroups[propertiesGroups[i][0]].style.display = "none";
        }

        for (var name in propertyRows) {
            propertyRows[name].style.display = propertyValues[name] != null ? "" : "none";
            if (propertyValues[name] != null) {
                var propertyValue = propertyValues[name];
                var propertyControl = propertyRows[name].propertyControl;
                if (!propertyControl) continue;                
                if (propertiesPanel.propertiesGroups[propertyControl.groupName])
                    propertiesPanel.propertiesGroups[propertyControl.groupName].style.display = "";

                if (name == "conditions") {
                    var conditionsText = "[" + ((propertyValue && propertyValue.length > 0) 
                        ? styleDesignerForm.jsObject.loc.PropertyMain.Conditions : styleDesignerForm.jsObject.loc.FormConditions.NoConditions) + "]";
                    propertyControl.textBox.value = conditionsText;
                }
                else if (propertyControl["setKey"] != null) propertyControl.setKey(propertyValue);
                else if (propertyControl["setChecked"] != null) propertyControl.setChecked(propertyValue);
                else if (propertyControl["value"] != null) propertyControl.value = propertyValue;
                else if (propertyControl["textBox"] != null && propertyControl.textBox["value"] != null) propertyControl.textBox.value = propertyValue;
            }
        }
    }

    return propertiesPanel;
}

StiMobileDesigner.prototype.StylesTree = function (styleDesignerForm) {
    var tree = this.Tree(500, 500);
    tree.style.overflow = "auto"; 
    tree.style.padding = "10px 0 10px 0"; 
    tree.openedItems = {};
    var jsObject = this;

    tree.reset = function (notResetOpenedItems) {
        this.clear();
        if (!notResetOpenedItems) this.openedItems = {};
        this.mainItem = jsObject.StylesTreeItem("Main", null, { typeItem: "MainItem" }, tree);
        this.mainItem.childsRow.style.display = "";
        this.mainItem.button.style.display = "none";
        this.mainItem.iconOpening.style.display = "none";
        this.appendChild(this.mainItem);
        styleDesignerForm.toolBar.updateControls();
        styleDesignerForm.propertiesPanel.updateControls();
    }

    tree.onSelectedItem = function (item) {
        styleDesignerForm.toolBar.updateControls();
        styleDesignerForm.propertiesPanel.updateControls(item.itemObject);
    }

    tree.onRemoveItem = function (item) {
        for (var i = 0; i < styleDesignerForm.stylesCollection.length; i++) {
            if (item.itemObject == styleDesignerForm.stylesCollection[i]) {
                styleDesignerForm.stylesCollection.splice(i, 1);
                break;
            }
        }
        if (item.parent && item.parent.itemObject.typeItem != "MainItem" && item.getCountElementsInCurrent() == 0) {
            item.parent.remove();
        }
    }

    tree.checkExistStyleName = function (styleName) {
        for (var i = 0; i < styleDesignerForm.stylesCollection.length; i++) {
            if (styleName == styleDesignerForm.stylesCollection[i].properties.name) {
                return true;
            }
        }
        return false;
    }

    tree.generateNewStyleName = function (baseNameForNewStyle) {
        var index = 1;
        var styleName = baseNameForNewStyle || jsObject.loc.FormStyleDesigner.Style;
        var findCompleted = false;

        while (!findCompleted) {
            findCompleted = true;
            if (tree.checkExistStyleName(styleName + index)) {
                findCompleted = false;
                index++;
            }

            //for (var i = 0; i < styleDesignerForm.stylesCollection.length; i++) {
            //    if (styleName + index == styleDesignerForm.stylesCollection[i].properties.name) {
            //        findCompleted = false;
            //        index++;
            //        break;
            //    }
            //}            
        }

        return styleName + index;
    }

    tree.addItem = function (styleObject, baseNameForNewStyle, itemWasReplaced) {
        styleDesignerForm.findTextbox.value = "";
        styleDesignerForm.findTextbox.onblur();
        if (!itemWasReplaced) {
            styleObject.properties.name = this.generateNewStyleName(baseNameForNewStyle);
        }
        styleDesignerForm.settings.filter[styleObject.type] = true;

        if (this.selectedItem) {
            var currItemObject = this.selectedItem.itemObject;
            var collectionName = currItemObject.collectionName || (currItemObject.properties ? currItemObject.properties.collectionName : "");
            if (collectionName != null) styleObject.properties.collectionName = collectionName;

            var i = 0;
            while (this.selectedItem.itemObject != styleDesignerForm.stylesCollection[i] && i < styleDesignerForm.stylesCollection.length) {
                i++;
            }
            styleDesignerForm.stylesCollection.splice(itemWasReplaced ? i : i + 1, 0, styleObject);
        }
        else {
            styleDesignerForm.stylesCollection.push(styleObject);
        }

        tree.updateItems(styleDesignerForm.stylesCollection, styleObject, true, !itemWasReplaced || (this.selectedItem && this.selectedItem.itemObject.typeItem == "Folder"));
    }

    tree.checkVisible = function (itemObject) {
        if (!itemObject) return false;
        if (itemObject.properties && styleDesignerForm.settings.filter[itemObject.type])
            return true;

        return false;
    }

    tree.compareObjects = function (object1, object2) {
        if (!object1 || !object2) return false;
        if (object1.properties && object2.properties) return object1 == object2;
        if (object1.typeItem == "Folder" && object2.typeItem == "Folder")
            return object1.collectionName == object2.collectionName;

        return false;
    }

    tree.findItems = function (text) {
        var choosedItems = [];
        for (var i = 0; i < styleDesignerForm.stylesCollection.length; i++) {
            var styleObject = styleDesignerForm.stylesCollection[i];
            var itemText = styleObject.properties.name;
            if (itemText) {
                var showItem = itemText.toLowerCase().indexOf(text.toLowerCase()) >= 0;
                if (showItem) choosedItems.push(styleObject);
            }
        }
        tree.updateItems(choosedItems, tree.selectedItem ? tree.selectedItem.itemObject : null);
    }

    tree.updateItems = function (stylesCollection, selectedStyleObject, notResetOpenedItems, scrollToSelectedItem) {
        this.currentScrollTop = this.scrollTop;
        this.reset(notResetOpenedItems);
        var collectionNames = [];
        var collectionStyles = {}
        var rootStyles = [];

        if (styleDesignerForm.settings.sort != "NoSorting") {
                stylesCollection.sort(function (a, b) {
                    if (a.properties.name && b.properties.name) {
                        var n = styleDesignerForm.settings.sort == "Ascending" ? -1 : 1;
                        if (a.properties.name < b.properties.name)
                            return n;
                        if (a.properties.name > b.properties.name)
                            return n * -1;
                    }
                    return 0
                });
        }
        
        for (var i = 0; i < stylesCollection.length; i++) {
            var styleObject = stylesCollection[i];
            var collectionName = styleObject.properties.collectionName;
            var styleItem = jsObject.StylesTreeItem(styleObject.properties.name, "Styles." + styleObject.type + "32.png", styleObject, tree);
            styleItem.marker.style.display = styleDesignerForm.usingStyles[styleObject.properties.name] ? "" : "none";

            if (collectionName) {
                if (!collectionStyles[collectionName]) {
                    collectionStyles[collectionName] = [];
                    collectionNames.push(collectionName);
                }
                collectionStyles[collectionName].push(styleItem);
            }
            else {
                rootStyles.push(styleItem);
            }
        }

        if (styleDesignerForm.settings.sort != "NoSorting") {
            collectionNames.sort(function (a, b) {
                if (a && b) {
                    var n = styleDesignerForm.settings.sort == "Ascending" ? -1 : 1;
                    if (a < b)
                        return n;
                    if (a > b)
                        return n * -1;
                }
                return 0
            });
        }

        for (var i = 0; i < collectionNames.length; i++) {
            var styleItems = collectionStyles[collectionNames[i]];
            if (styleItems) {
                var collectionItem = jsObject.StylesTreeItem(collectionNames[i], "CloudItems.BigFolder.png", { typeItem: "Folder", collectionName: collectionNames[i] }, tree);
                this.mainItem.addChild(collectionItem);
                if (this.compareObjects(selectedStyleObject, collectionItem.itemObject)) {
                    collectionItem.setSelected();
                }
                if (tree.openedItems[collectionNames[i]]) {
                    collectionItem.setOpening(true);
                }
                for (var k = 0; k < styleItems.length; k++) {
                    if (this.checkVisible(styleItems[k].itemObject)) {
                        var item = collectionItem.addChild(styleItems[k]);
                        if (this.compareObjects(selectedStyleObject, styleItems[k].itemObject)) {
                            item.setSelected();
                            item.openTree();
                            tree.openedItems[collectionItem.itemObject.collectionName] = true;
                        }
                    }
                }
                if (jsObject.GetCountObjects(collectionItem.childs) == 0) {
                    collectionItem.parent.childsContainer.removeChild(collectionItem);
                    delete collectionItem.parent.childs[collectionItem.id];
                    delete tree.items[collectionItem.id];
                }
            }
        }

        for (var i = 0; i < rootStyles.length; i++) {
            if (this.checkVisible(rootStyles[i].itemObject)) {
                var item = this.mainItem.addChild(rootStyles[i]);
                if (this.compareObjects(selectedStyleObject, rootStyles[i].itemObject)) {
                    item.setSelected();
                    item.openTree();
                }
            }
        }

        this.scrollToPos(scrollToSelectedItem);
    }

    tree.scrollToPos = function (toSelectedItem) {
        if (toSelectedItem && this.selectedItem && this.selectedItem.itemObject.typeItem != "MainItem") {
            var yPos = this.jsObject.FindPosY(this.selectedItem, null, false, this);
            this.scrollTop = yPos - this.offsetHeight + 50;
        }
        else {
            this.scrollTop = this.currentScrollTop;
        }
    }

    tree.onmouseup = function (event) {
        if (event.button == 2) {
            event.stopPropagation();
            if (!this.jsObject.options.report) return;
            var stylesContextMenu = this.jsObject.options.menus.stylesContextMenu || this.jsObject.InitializeStylesContextMenu(styleDesignerForm);
            var point = this.jsObject.FindMousePosOnMainPanel(event);
            stylesContextMenu.show(point.xPixels + 3, point.yPixels + 3, "Down", "Right");
        }
        return false;
    }

    tree.oncontextmenu = function (event) {
        return false;
    }

    return tree;
}

StiMobileDesigner.prototype.StylesTreeItem = function (caption, imageName, itemObject, tree) {
    var item = this.TreeItem(caption, imageName, itemObject, tree);
    var itemWidth = 430;
    item.style.margin = "2px 0 2px 5px";
    item.style.width = itemWidth + "px";

    //add marker
    var marker = document.createElement("div");
    marker.style.display = "none";
    marker.className = "stiUsingMarker";
    marker.style.right = "15px";
    marker.style.left = "auto";
    var markerInner = document.createElement("div");
    marker.appendChild(markerInner);
    item.button.style.position = "relative";
    item.button.appendChild(marker);
    item.marker = marker;

    //Override Item
    var innerButton = item.button;
    innerButton.style.overflow = "hidden";

    if (innerButton.imageCell) {
        innerButton.imageCell.parentNode.removeChild(innerButton.imageCell);
    }
    if (innerButton.captionCell) {
        innerButton.captionCell.parentNode.removeChild(innerButton.captionCell);
    }

    var innerTable = this.CreateHTMLTable();
    innerTable.style.width = itemWidth + "px";
    innerTable.style.margin = "3px";
    innerButton.addCell(innerTable);

    if (innerButton.image) {
        var imageCell = innerTable.addCell(innerButton.image);
        innerTable.imageCell = imageCell;
        imageCell.style.width = "1px";
        imageCell.style.padding = itemObject.typeItem == "Folder" ? "0 3px 0 0px" : "0 3px 0 3px";
    }

    var captionContainer = document.createElement("div");
    var captionCell = innerTable.addCell(captionContainer);
    captionCell.style.padding = "0px 5px 0px 0px";
    captionContainer.style.maxHeight = "38px";
    captionContainer.style.maxWidth = (itemWidth - 50) + "px";
    captionContainer.style.overflow = "hidden";

    var captionInnerContainer = document.createElement("div");
    captionContainer.appendChild(captionInnerContainer);
    captionInnerContainer.innerHTML = caption;
    captionInnerContainer.style.position = "relative";
    captionInnerContainer.style.whiteSpace = "nowrap";
    captionInnerContainer.style.padding = "1px 0 1px 0";
    
    item.repaint = function () {
        var properties = this.itemObject.properties;
        if (!properties) return;
        var defaultFont = "Arial!10!0!0!0!0";
        var defaultBrush = "1!transparent";
        var defaultTextBrush = "1!0,0,0";
        var defaultBorder = "default";

        captionInnerContainer.innerHTML = properties["description"] || properties["name"];

        var font = (properties["font"] != null && properties["allowUseFont"]) ? properties["font"] : defaultFont;
        var brush = (properties["brush"] != null && properties["allowUseBrush"]) ? properties["brush"] : defaultBrush;
        var textBrush = (properties["textBrush"] != null && properties["allowUseTextBrush"]) ? properties["textBrush"] : defaultTextBrush;
        var border = (properties["border"] != null && properties["allowUseBorderSides"]) ? properties["border"] : defaultBorder;

        if (border) {            
            var borderArray = border.split("!");
            var borderDropShadow = border != "default" && borderArray.length > 6 && borderArray[4] == "1";
            if (borderDropShadow) {
                var borderShadowSize = borderArray[5];
                var borderShadowColor = this.jsObject.GetColorFromBrushStr(Base64.decode(borderArray[6]));                
                var cssColor = "transparent";
                var colors = borderShadowColor.split(",");
                if (colors.length == 3) cssColor = "rgba(" + borderShadowColor + ",1)";
                if (colors.length == 4) cssColor = "rgba(" + colors[1] + "," + colors[2] + "," + colors[3] + "," + this.jsObject.StrToInt(colors[0]) / 255 + ")";
                innerButton.style.boxShadow = cssColor + " " + borderShadowSize + "px " + borderShadowSize + "px 0px";
            }
            else {
                innerButton.style.boxShadow = "none";
            }
        }

        this.jsObject.RepaintControlByAttributes(innerTable, font, brush, textBrush, border);
        captionInnerContainer.style.top = (captionInnerContainer.offsetHeight > 40 ? ((captionInnerContainer.offsetHeight - 40) / 2 * -1) : 0) + "px";

        if (this.itemObject.type == "StiCrossTabStyle") {
            var sampleTable = this.jsObject.CrossTabSampleTable(32, 32, 5, 5, this.itemObject.properties.color, this.itemObject.properties.color);
            if (innerTable.imageCell) {
                innerTable.imageCell.innerHTML = "";
                innerTable.imageCell.style.padding = "2px 6px 2px 3px";
                innerTable.imageCell.appendChild(sampleTable);
            }
        }
    }

    //Override
    item.button.onmouseup = function () {
        var itemInDrag = this.jsObject.options.itemInDrag;
        if (itemInDrag && itemInDrag.originalItem &&
            itemInDrag.originalItem != this.treeItem && itemInDrag.originalItem.itemObject.properties)
        {
            itemInDrag.originalItem.remove();
            this.treeItem.setSelected();
            tree.addItem(itemInDrag.originalItem.itemObject, null, true);
        }
    }
        
    item.button.onmousedown = function () {
        if (this.isTouchStartFlag) return;
        if (this.jsObject.options.controlsIsFocused && this.jsObject.options.controlsIsFocused.action) { //fixed bug with controls action onblur
            this.jsObject.options.controlsIsFocused.action();
        }
        if (tree.items[item.id]) {
            item.button.action();
        }
        if (event) event.preventDefault();
        
        if (item.itemObject.properties && event.button != 2 && !this.jsObject.options.controlsIsFocused) {
            var itemInDragObject = item.jsObject.TreeItemForDragDrop({
                name: item.itemObject.properties.name,
                typeIcon: "Styles." + item.itemObject.type + "32"
            }, item.tree);

            if (itemInDragObject.button.captionCell) itemInDragObject.button.captionCell.style.padding = "3px 15px 3px 5px";
            if (itemInDragObject.button.imageCell) itemInDragObject.button.imageCell.style.padding = "2px 5px 2px 5px";
            itemInDragObject.originalItem = item;
            itemInDragObject.beginingOffset = 0;
            item.jsObject.options.itemInDrag = itemInDragObject;
        }
    }

    item.button.ontouchstart = function () {
        var this_ = this;
        this.isTouchStartFlag = true;
        clearTimeout(this.isTouchStartTimer);
        if (this.jsObject.options.controlsIsFocused && this.jsObject.options.controlsIsFocused.action) { //fixed bug with controls action onblur
            this.jsObject.options.controlsIsFocused.action();
        }
        if (tree.items[item.id]) {
            item.button.action();
        }
        this.isTouchStartTimer = setTimeout(function () {
            this_.isTouchStartFlag = false;
        }, 1000);
    }

    item.iconOpening.action = function () {
        if (tree.isDisable) return;
        item.isOpening = !item.isOpening;
        item.childsRow.style.display = item.isOpening ? "" : "none";
        var imgName = item.isOpening ? "IconCloseItem.png" : "IconOpenItem.png";
        if (this.jsObject.options.isTouchDevice) imgName = imgName.replace(".png", "Big.png");
        item.iconOpening.src = this.jsObject.options.images[imgName];
        item.setSelected();
        if (item.itemObject.typeItem == "Folder") {
            tree.openedItems[item.itemObject.collectionName] = item.isOpening;
            item.button.image.src = this.jsObject.options.images["CloudItems." + (item.isOpening ? "BigFolderOpen.png" : "BigFolder.png")];
        }
    }

    item.openTree = function () {
        var item = this.parent;
        while (item != null) {
            item.isOpening = true;
            item.childsRow.style.display = "";
            item.iconOpening.src = this.jsObject.options.images[this.jsObject.options.isTouchDevice ? "IconCloseItemBig.png" : "IconCloseItem.png"];
            if (item.itemObject.typeItem == "Folder") {
                item.button.image.src = this.jsObject.options.images["CloudItems." + (item.isOpening ? "BigFolderOpen.png" : "BigFolder.png")];
                tree.openedItems[item.itemObject.collectionName] = item.isOpening;
            }
            item = item.parent;
        }        
    }

    item.setOpening = function (state) {
        if (this.buildChildsNotCompleted && state) this.completeBuildTree(true);
        this.isOpening = state;
        this.childsRow.style.display = state ? "" : "none";
        var imageType = state ? "Close" : "Open";
        this.iconOpening.src = this.jsObject.options.images[this.jsObject.options.isTouchDevice ? "Icon" + imageType + "ItemBig.png" : "Icon" + imageType + "Item.png"];
        if (item.itemObject.typeItem == "Folder") {
            item.button.image.src = this.jsObject.options.images["CloudItems." + (item.isOpening ? "BigFolderOpen.png" : "BigFolder.png")];
            tree.openedItems[item.itemObject.collectionName] = item.isOpening;
        }
    }    

    item.repaint();

    return item;
}

StiMobileDesigner.prototype.GetAllUsingStyles = function () {
    var styles = {};
    var report = this.options.report;
    if (report) {
        for (var pageName in report.pages) {
            var page = report.pages[pageName];
            if (page.properties.componentStyle && page.properties.componentStyle != "[None]") {
                styles[page.properties.componentStyle] = true;
            }
            for (var cName in page.components) {
                var component = page.components[cName];
                if (component.properties.componentStyle && component.properties.componentStyle != "[None]") {
                    styles[component.properties.componentStyle] = true;
                }
                if (component.properties.evenStyle && component.properties.evenStyle != "[None]") {
                    styles[component.properties.evenStyle] = true;
                }
                if (component.properties.oddStyle && component.properties.oddStyle != "[None]") {
                    styles[component.properties.oddStyle] = true;
                }
                if (component.properties.chartStyle && component.properties.chartStyle.type == "StiCustomStyle") {
                    styles[component.properties.chartStyle.name] = true;
                }
                if (component.properties.gaugeStyle && component.properties.gaugeStyle.type == "StiCustomGaugeStyle") {
                    styles[component.properties.gaugeStyle.name] = true;
                }
                if (component.properties.mapStyle && component.properties.mapStyle.type == "StiMapStyle") {
                    styles[component.properties.mapStyle.name] = true;
                }
                if (component.properties.conditions) {
                    var conditions = JSON.parse(Base64.decode(component.properties.conditions));
                    for (var i = 0; i < conditions.length; i++) {
                        if (conditions[i].Style) styles[conditions[i].Style] = true;
                    }
                }
            }
        }
    }

    return styles;
}