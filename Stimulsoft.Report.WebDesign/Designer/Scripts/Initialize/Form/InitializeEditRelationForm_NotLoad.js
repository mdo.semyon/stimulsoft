﻿
StiMobileDesigner.prototype.InitializeEditRelationForm_ = function () {

    //Edit Connection Form
    var editRelationForm = this.BaseForm("editRelationForm", this.loc.PropertyMain.Relation, 3, this.HelpLinks["relationEdit"]);
    editRelationForm.relation = null;
    editRelationForm.mode = "Edit";

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px 0 5px 0";
    editRelationForm.container.appendChild(innerTable);

    var textBoxes = [
        ["nameInSource", this.loc.PropertyMain.NameInSource],
        ["name", this.loc.PropertyMain.Name],
        ["alias", this.loc.PropertyMain.Alias]
    ]

    for (var i = 0; i < textBoxes.length; i++) {
        var text = i == 0 ? innerTable.addCell() : innerTable.addCellInNextRow();
        text.className = "stiDesignerCaptionControlsBigIntervals";
        text.innerHTML = textBoxes[i][1] + ":";
        editRelationForm[textBoxes[i][0] + "Control"] = this.TextBox("editRelationForm" + textBoxes[i][0], 220);
        if (i == 0)
            innerTable.addCell(editRelationForm[textBoxes[i][0] + "Control"]).className = "stiDesignerControlCellsBigIntervals";
        else
            innerTable.addCellInLastRow(editRelationForm[textBoxes[i][0] + "Control"]).className = "stiDesignerControlCellsBigIntervals";
    }

    editRelationForm.nameControl.action = function () {
        if (this.oldValue == editRelationForm.aliasControl.value) {
            editRelationForm.aliasControl.value = this.value;
        }
    }

    var dropDownLists = [
        ["parentDataSource", this.loc.PropertyMain.ParentSource],
        ["childDataSource", this.loc.PropertyMain.ChildSource]
    ]

    for (var i = 0; i < dropDownLists.length; i++) {
        var text = innerTable.addCellInNextRow();
        text.className = "stiDesignerCaptionControlsBigIntervals";
        text.innerHTML = dropDownLists[i][1] + ":";
        editRelationForm[dropDownLists[i][0] + "Control"] = this.DropDownList("editRelationForm" + dropDownLists[i][0], 220, null, null, true);
        innerTable.addCellInLastRow(editRelationForm[dropDownLists[i][0] + "Control"]).className = "stiDesignerControlCellsBigIntervals";

        editRelationForm[dropDownLists[i][0] + "Control"].action = function () {
            editRelationForm.parentDataSourceControl.hideError();
            editRelationForm.childDataSourceControl.hideError();
            var dataSource = this.jsObject.GetDataSourceByNameFromDictionary(this.key);
            var columns = this.jsObject.GetColumnsItemsFromDictionary(dataSource);
            if (this.name == "editRelationFormparentDataSource") {
                editRelationForm.parentColumnsContainer.addColumns(columns);
                editRelationForm.parentColumnsSelectedContainer.clear();
            }
            else {
                editRelationForm.childColumnsContainer.addColumns(columns);
                editRelationForm.childColumnsSelectedContainer.clear();
            }
        }
    }

    editRelationForm.container.appendChild(this.FormBlockHeader(this.loc.PropertyMain.ParentColumns));
    var parentDataSourceTable = this.CreateHTMLTable();
    editRelationForm.container.appendChild(parentDataSourceTable);

    editRelationForm.container.appendChild(this.FormBlockHeader(this.loc.PropertyMain.ChildColumns));
    var childDataSourceTable = this.CreateHTMLTable();
    editRelationForm.container.appendChild(childDataSourceTable);

    var containers = ["parentColumns", "childColumns"];
    var buttons = ["Left", "LeftAll", "Right", "RightAll"];

    for (var i = 0; i < containers.length; i++) {
        var table = (i == 0) ? parentDataSourceTable : childDataSourceTable;
        var selectedColumns = containers[i] + "SelectedContainer";
        editRelationForm[selectedColumns] = this.Container(selectedColumns, 200, 100);
        editRelationForm[selectedColumns].className = "stiDesignerRelationFormColumnsLeftContainer";
        table.addCell(editRelationForm[selectedColumns]);
        editRelationForm[selectedColumns].onChange = function () {
            var itemsCount = this.jsObject.GetCountObjects(this.items);
            var secondContainerName = this.name.indexOf("parentColumns") == 0 ? "childColumnsSelectedContainer" : "parentColumnsSelectedContainer";
            var secondSelectedContainerItemsCount = this.jsObject.GetCountObjects(this.jsObject.options.forms.editRelationForm[secondContainerName].items);
            this.buttonsContainer["Right"].setEnabled(itemsCount > 0);
            this.buttonsContainer["RightAll"].setEnabled(itemsCount > 0);
            var buttonOkEnabled = itemsCount > 0 && secondSelectedContainerItemsCount > 0 && itemsCount == secondSelectedContainerItemsCount;
            editRelationForm.buttonOk.setEnabled(buttonOkEnabled);
        }

        var buttonsCell = table.addCell();

        var allColumns = containers[i] + "Container";
        editRelationForm[allColumns] = this.Container(allColumns, 200, 100);
        editRelationForm[allColumns].className = "stiDesignerRelationFormColumnsRightContainer";
        table.addCell(editRelationForm[allColumns]);
        editRelationForm[allColumns].selectedContainer = editRelationForm[selectedColumns];
        editRelationForm[allColumns].onChange = function () {
            var itemsCount = this.jsObject.GetCountObjects(this.items);
            this.buttonsContainer["Left"].setEnabled(itemsCount > 0);
            this.buttonsContainer["LeftAll"].setEnabled(itemsCount > 0);
        }
        editRelationForm[allColumns].addColumns = function (columns) {
            this.clear();
            var selectedItemsStr = "";
            for (var i = 0; i < this.selectedContainer.items.length; i++) {
                selectedItemsStr += ";" + this.selectedContainer.items[i].name + ";";
            }

            if (columns) {
                columns.sort(editRelationForm.jsObject.SortByName);
                for (var i = 0; i < columns.length; i++) {
                    if (selectedItemsStr.indexOf(";" + columns[i].caption + ";") == -1) {
                        var item = this.addItem(columns[i].caption);

                        item.ondblclick = function () {
                            this.container.buttonsContainer.Left.action();
                        }
                    }
                }
            }
        }

        var buttonsContainer = {};
        editRelationForm[selectedColumns].buttonsContainer = buttonsContainer;
        editRelationForm[allColumns].buttonsContainer = buttonsContainer;

        for (var k = 0; k < buttons.length; k++) {
            buttonsContainer[buttons[k]] = this.DinamicStandartSmallButton(buttons[k], null, null, buttons[k] + ".png");
            buttonsContainer[buttons[k]].leftContainer = editRelationForm[selectedColumns];
            buttonsContainer[buttons[k]].rightContainer = editRelationForm[allColumns];
            buttonsContainer[buttons[k]].style.margin = "2px";
            buttonsCell.appendChild(buttonsContainer[buttons[k]]);

            buttonsContainer[buttons[k]].action = function () {
                if (this.name == "Left") {
                    if (!this.rightContainer.selectedItem) return;
                    var name = this.rightContainer.selectedItem.name;
                    this.rightContainer.removeItem(name);
                    var item = this.leftContainer.addItem(name);
                    item.ondblclick = function () { this.container.buttonsContainer.Right.action() };
                    return;
                }
                if (this.name == "LeftAll") {
                    for (var index = 0; index < this.rightContainer.items.length; index++) {
                        var item = this.leftContainer.addItem(this.rightContainer.items[index].name);
                        item.ondblclick = function () { this.container.buttonsContainer.Right.action() };
                    }
                    this.rightContainer.clear();
                }
                if (this.name == "Right") {
                    if (!this.leftContainer.selectedItem) return;
                    var name = this.leftContainer.selectedItem.name;
                    this.leftContainer.removeItem(name);
                    var item = this.rightContainer.addItem(name);
                    item.ondblclick = function () { this.container.buttonsContainer.Left.action() };
                    return;
                }
                if (this.name == "RightAll") {
                    for (var index = 0; index < this.leftContainer.items.length; index++) {
                        var item = this.rightContainer.addItem(this.leftContainer.items[index].name);
                        item.ondblclick = function () { this.container.buttonsContainer.Left.action() };
                    }
                    this.leftContainer.clear();
                }
            }
        }
    }

    editRelationForm.onshow = function () {
        this.mode = "Edit";
        if (this.relation == null) { this.relation = this.jsObject.RelationObject(); this.mode = "New"; }
        var caption = this.jsObject.loc.FormDictionaryDesigner["Relation" + this.mode];
        this.caption.innerHTML = caption;
        this.nameInSourceControl.value = this.relation.nameInSource;
        this.nameControl.hideError();
        this.nameControl.focus();
        this.nameControl.value = this.relation.name;
        this.aliasControl.value = this.relation.alias;

        var allDataSources = this.jsObject.GetDataSourcesFromDictionary(this.jsObject.options.report.dictionary);
        var allDataSourcesItems = [];
        for (var i = 0; i < allDataSources.length; i++) {
            allDataSourcesItems.push(this.jsObject.Item(allDataSources[i].name, allDataSources[i].name, null, allDataSources[i].name));
        }
        allDataSourcesItems.sort(this.jsObject.SortByName);
        this.parentDataSourceControl.items = allDataSourcesItems;
        this.childDataSourceControl.items = allDataSourcesItems;
        
        this.parentDataSourceControl.setKey(this.mode == "Edit" ? this.relation.parentDataSource : "");
        var currentParent = this.jsObject.options.dictionaryTree.getCurrentColumnParent();
        this.childDataSourceControl.setKey(this.mode == "Edit" ? this.relation.childDataSource : currentParent.name);
        
        this.parentColumnsSelectedContainer.clear();
        this.childColumnsSelectedContainer.clear();
        for (var i = 0; i < this.relation.parentColumns.length; i++) this.parentColumnsSelectedContainer.addItem(this.relation.parentColumns[i]);
        for (var i = 0; i < this.relation.childColumns.length; i++) this.childColumnsSelectedContainer.addItem(this.relation.childColumns[i]);
                
        var childDataSource = this.jsObject.GetDataSourceByNameFromDictionary(this.childDataSourceControl.key);
        var parentDataSource = this.jsObject.GetDataSourceByNameFromDictionary(this.parentDataSourceControl.key);
        var childColumns = this.jsObject.GetColumnsItemsFromDictionary(childDataSource);
        var parentColumns = this.jsObject.GetColumnsItemsFromDictionary(parentDataSource);

        this.parentColumnsContainer.addColumns(parentColumns);
        this.childColumnsContainer.addColumns(childColumns);
        this.parentDataSourceControl.hideError();
        this.childDataSourceControl.hideError();
    }

    editRelationForm.action = function () {
        if (this.parentDataSourceControl.key == this.childDataSourceControl.key && this.parentDataSourceControl.key != "") {
            var errorText = this.jsObject.loc.Notices.IsIdentical.replace("{0}", "ParentKey").replace("{1}", "ChildKey");
            this.parentDataSourceControl.showError(errorText);
            this.childDataSourceControl.showError(errorText);
            return;
        }
        this.relation["mode"] = this.mode;
        if (this.mode == "Edit") this.relation["oldNameInSource"] = this.relation.nameInSource;
        this.relation.nameInSource = this.nameInSourceControl.value;
        this.relation.name = this.nameControl.value;
        this.relation.alias = this.aliasControl.value;
        this.relation["changedChildDataSource"] = this.relation.childDataSource != this.childDataSourceControl.key;
        this.relation.parentDataSource = this.parentDataSourceControl.key;
        this.relation.childDataSource = this.childDataSourceControl.key;
        this.relation.childColumns = [];
        this.relation.parentColumns = [];

        var props = ["parentColumns", "childColumns"];
        for (var i = 0; i < props.length; i++) {
            var items = this[props[i] + "SelectedContainer"].items;
            for (var k = 0; k < items.length; k++)
                this.relation[props[i]].push(items[k].name);
        }

        if (!this.nameControl.checkNotEmpty(this.jsObject.loc.PropertyMain.Name)) return;
        this.changeVisibleState(false);
        this.jsObject.SendCommandCreateOrEditRelation(this.relation);
    }

    return editRelationForm;
}