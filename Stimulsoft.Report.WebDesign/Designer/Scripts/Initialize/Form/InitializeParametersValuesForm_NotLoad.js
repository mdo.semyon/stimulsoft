﻿
StiMobileDesigner.prototype.InitializeParametersValuesForm_ = function () {

    var editParametersValuesForm = this.BaseForm("editParametersValuesForm", this.loc.FormTitles.SqlExpressionsForm, 3);

    var innerTable = this.CreateHTMLTable();
    innerTable.style.margin = "5px";
    editParametersValuesForm.container.appendChild(innerTable);

    editParametersValuesForm.show = function (formParameters, variablesParams, completeFunction) {
        editParametersValuesForm.controls = {};
        editParametersValuesForm.formParameters = formParameters;
        editParametersValuesForm.completeFunction = completeFunction;

        if (variablesParams) {
            for (var i = 0; i < variablesParams.length; i++) {
                innerTable.addRow();
                var text = innerTable.addCellInLastRow();
                text.className = "stiDesignerCaptionControlsBigIntervals";
                text.innerHTML = variablesParams[i];
                editParametersValuesForm.controls[variablesParams[i]] = this.jsObject.TextBox(200);
                innerTable.addCellInLastRow(editParametersValuesForm.controls[variablesParams[i]]).className = "stiDesignerControlCellsBigIntervals";
            }
        }

        for (var i = 0; i < formParameters.parameters.length; i++) {
            innerTable.addRow();
            var text = innerTable.addCellInLastRow();
            text.className = "stiDesignerCaptionControlsBigIntervals";
            text.innerHTML = formParameters.parameters[i].name;
            editParametersValuesForm.controls[formParameters.parameters[i].name] = this.jsObject.TextBox(200);
            innerTable.addCellInLastRow(editParametersValuesForm.controls[formParameters.parameters[i].name]).className = "stiDesignerControlCellsBigIntervals";
        }

        editParametersValuesForm.changeVisibleState(true);
    }

    editParametersValuesForm.action = function (parameterNames) {
        var parametersValues = {};
        for (var name in editParametersValuesForm.controls) {
            parametersValues[name] = Base64.encode(editParametersValuesForm.controls[name].value);
        }

        editParametersValuesForm.formParameters.parametersValues = parametersValues;
        editParametersValuesForm.completeFunction(editParametersValuesForm.formParameters);
        editParametersValuesForm.changeVisibleState(false);
    }

    return editParametersValuesForm;
}