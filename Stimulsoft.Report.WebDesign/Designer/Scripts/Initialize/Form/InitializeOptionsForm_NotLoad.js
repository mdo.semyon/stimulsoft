﻿
StiMobileDesigner.prototype.InitializeOptionsForm_ = function () {

    var optionsForm = this.BaseForm("optionsForm", this.loc.FormOptions.title, 1, this.HelpLinks["options"]);
    optionsForm.mode = "Main";

    var restoreDefaults = this.FormButton(null, null, this.loc.Buttons.RestoreDefaults, null);
    restoreDefaults.style.display = "inline-block";
    restoreDefaults.style.margin = "8px";
    optionsForm.restoreDefaults = restoreDefaults;

    restoreDefaults.action = function () {
        optionsForm.fill(this.jsObject.options.defaultDesignerOptions);
    }

    var footerTable = this.CreateHTMLTable();
    footerTable.style.width = "100%";
    var buttonsPanel = optionsForm.buttonsPanel;
    optionsForm.removeChild(buttonsPanel);
    optionsForm.appendChild(footerTable);
    footerTable.addCell(restoreDefaults).style.width = "1px";
    footerTable.addCell();
    footerTable.addCell(optionsForm.buttonOk).style.width = "1px";
    footerTable.addCell(optionsForm.buttonCancel).style.width = "1px";

    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerImageFormMainPanel";
    optionsForm.container.appendChild(mainTable);
    optionsForm.container.style.padding = "0px";

    //Buttons
    var buttonProps = [
        ["Main", "OptionsForm.OptionsMain.png", this.loc.FormOptions.Main],
        ["Grid", "OptionsForm.OptionsGrid.png", this.loc.FormOptions.Grid],
        ["QuickInfo", "OptionsForm.OptionsQuickInfo.png", this.loc.MainMenu.menuViewQuickInfo],
        ["AutoSave", "OptionsForm.OptionsAutoSave.png", this.loc.FormOptions.AutoSave]
    ];

    //Add Panels && Buttons
    var panelsContainer = mainTable.addCell();
    var buttonsPanel = mainTable.addCell();
    buttonsPanel.style.verticalAlign = "top";
    optionsForm.mainButtons = {};
    optionsForm.panels = {};

    for (var i = 0; i < buttonProps.length; i++) {
        var panel = document.createElement("Div");
        panel.className = "stiDesignerEditFormPanel";
        if (i != 0) panel.style.display = "none";
        panelsContainer.appendChild(panel);
        optionsForm.panels[buttonProps[i][0]] = panel;

        var button = this.StandartFormBigButton(null, null, buttonProps[i][2], buttonProps[i][1], buttonProps[i][2], 80);
        button.style.margin = "2px";
        optionsForm.mainButtons[buttonProps[i][0]] = button;
        buttonsPanel.appendChild(button);
        button.panelName = buttonProps[i][0];
        button.action = function () {
            optionsForm.setMode(this.panelName);
        }
    }

    //Create Tables
    for (var name in optionsForm.panels) {
        var table = this.CreateHTMLTable();
        optionsForm.panels[name].appendChild(table);
        optionsForm.panels[name].innerTable = table;
        table.style.width = "100%";
    }

    optionsForm.controls = {};
    var controlProps = [
        ["groupMainOptions", null, this.FormBlockHeader(this.loc.FormOptions.groupMainOptions), "0 0 5px 0", optionsForm.panels.Main.innerTable],
        ["showHeaders", null, this.CheckBox(null, this.loc.MainMenu.menuViewShowHeaders), "5px 5px 5px 15px", optionsForm.panels.Main.innerTable],
        ["showRulers", null, this.CheckBox(null, this.loc.MainMenu.menuViewShowRulers), "5px 5px 5px 15px", optionsForm.panels.Main.innerTable],
        ["showOrder", null, this.CheckBox(null, this.loc.MainMenu.menuViewShowOrder), "5px 5px 5px 15px", optionsForm.panels.Main.innerTable],
        ["showToolbox", null, this.CheckBox(null, this.loc.MainMenu.menuViewShowToolbox), "5px 5px 5px 15px", optionsForm.panels.Main.innerTable],
        ["showInsertTab", null, this.CheckBox(null, this.loc.MainMenu.menuViewShowInsertTab), "5px 5px 5px 15px", optionsForm.panels.Main.innerTable],
        ["runDesignerAfterInsert", null, this.CheckBox(null, this.loc.FormOptions.EditAfterInsert), "5px 5px 5px 15px", optionsForm.panels.Main.innerTable],
        ["useLastFormat", null, this.CheckBox(null, this.loc.FormOptions.UseLastFormat), "5px 5px 5px 15px", optionsForm.panels.Main.innerTable],
        ["showDimensionLines", null, this.CheckBox(null, this.loc.FormOptions.ShowDimensionLines), "5px 5px 5px 15px", optionsForm.panels.Main.innerTable],
        ["generateLocalizedName", null, this.CheckBox(null, this.loc.FormOptions.GenerateLocalizedName), "5px 5px 5px 15px", optionsForm.panels.Main.innerTable],
        ["requestChangesWhenSaving", null, this.CheckBox(null, this.loc.Cloud.RequestChangesWhenSavingToCloud), "5px 5px 5px 15px", optionsForm.panels.Main.innerTable],
        ["groupGridOptions", null, this.FormBlockHeader(this.loc.FormOptions.groupGridOptions), "0 0 5px 0", optionsForm.panels.Grid.innerTable],
        ["alignToGrid", null, this.CheckBox(null, this.loc.MainMenu.menuViewAlignToGrid), "5px 5px 5px 15px", optionsForm.panels.Grid.innerTable],
        ["showGrid", null, this.CheckBox(null, this.loc.MainMenu.menuViewShowGrid), "5px 5px 5px 15px", optionsForm.panels.Grid.innerTable],
        ["groupGridDrawingOptions", null, this.FormBlockHeader(this.loc.FormOptions.groupGridDrawingOptions), "5px 0 5px 0", optionsForm.panels.Grid.innerTable],
        ["gridModeLines", null, this.RadioButton("optionsForm_Lines", "optionsFormGrid", this.loc.FormOptions.GridLines), "5px 5px 5px 15px", optionsForm.panels.Grid.innerTable],
        ["gridModeDots", null, this.RadioButton("optionsForm_Dots", "optionsFormGrid", this.loc.FormOptions.GridDots), "5px 5px 5px 15px", optionsForm.panels.Grid.innerTable],
        ["groupGridSize", null, this.FormBlockHeader(this.loc.FormOptions.groupGridSize), "5px 0 5px 0", optionsForm.panels.Grid.innerTable],
        ["gridSizeInch", this.loc.PropertyEnum.StiReportUnitTypeInches + ":", this.TextBox(null, 100), "5px 5px 5px 15px", optionsForm.panels.Grid.innerTable],
        ["gridSizeHundredthsOfInch", this.loc.PropertyEnum.StiReportUnitTypeHundredthsOfInch + ":", this.TextBox(null, 100), "5px 5px 5px 15px", optionsForm.panels.Grid.innerTable],
        ["gridSizeCentimetres", this.loc.PropertyEnum.StiReportUnitTypeCentimeters + ":", this.TextBox(null, 100), "5px 5px 5px 15px", optionsForm.panels.Grid.innerTable],
        ["gridSizeMillimeters", this.loc.PropertyEnum.StiReportUnitTypeMillimeters + ":", this.TextBox(null, 100), "5px 5px 5px 15px", optionsForm.panels.Grid.innerTable],
        ["gridSizePixels", this.loc.PropertyEnum.StiReportUnitTypePixels + ":", this.TextBox(null, 100), "5px 5px 5px 15px", optionsForm.panels.Grid.innerTable],
        ["groupOptionsOfQuickInfo", null, this.FormBlockHeader(this.loc.FormOptions.groupOptionsOfQuickInfo), "0 0 5px 0", optionsForm.panels.QuickInfo.innerTable],
        ["quickInfoTypeNone", null, this.RadioButton("optionsForm_QuickInfoNone", "optionsFormOptionsOfQuickInfo", this.loc.MainMenu.menuViewQuickInfoNone), "5px 5px 5px 15px", optionsForm.panels.QuickInfo.innerTable],
        ["quickInfoTypeShowComponentsNames", null, this.RadioButton("optionsForm_QuickInfoShowComponentsNames", "optionsFormOptionsOfQuickInfo", this.loc.MainMenu.menuViewQuickInfoShowComponentsNames), "5px 5px 5px 15px", optionsForm.panels.QuickInfo.innerTable],
        ["quickInfoTypeShowContent", null, this.RadioButton("optionsForm_QuickInfoShowContent", "optionsFormOptionsOfQuickInfo", this.loc.MainMenu.menuViewQuickInfoShowContent), "5px 5px 5px 15px", optionsForm.panels.QuickInfo.innerTable],
        ["quickInfoTypeShowFields", null, this.RadioButton("optionsForm_QuickInfoShowFields", "optionsFormOptionsOfQuickInfo", this.loc.MainMenu.menuViewQuickInfoShowFields), "5px 5px 5px 15px", optionsForm.panels.QuickInfo.innerTable],
        ["quickInfoTypeShowFieldsOnly", null, this.RadioButton("optionsForm_QuickInfoShowFieldsOnly", "optionsFormOptionsOfQuickInfo", this.loc.MainMenu.menuViewQuickInfoShowFieldsOnly), "5px 5px 5px 15px", optionsForm.panels.QuickInfo.innerTable],
        ["quickInfoTypeShowEvents", null, this.RadioButton("optionsForm_QuickInfoShowEvents", "optionsFormOptionsOfQuickInfo", this.loc.MainMenu.menuViewQuickInfoShowEvents), "5px 5px 5px 15px", optionsForm.panels.QuickInfo.innerTable],
        ["separator1", null, this.FormSeparator(), "5px 0 5px 0", optionsForm.panels.QuickInfo.innerTable],
        ["quickInfoOverlay", null, this.CheckBox(null, this.loc.MainMenu.menuViewQuickInfoOverlay), "5px 5px 5px 15px", optionsForm.panels.QuickInfo.innerTable],
        ["groupAutoSaveOptions", null, this.FormBlockHeader(this.loc.FormOptions.groupAutoSaveOptions), "0 0 5px 0", optionsForm.panels.AutoSave.innerTable],
        ["autoSaveInterval", this.loc.FormOptions.SaveReportEvery, this.DropDownList("reportSetupFormSaveReportEvery", 180, null, this.GetAutoSaveItems(), true), "5px 5px 5px 15px", optionsForm.panels.AutoSave.innerTable],
        ["separator2", null, this.FormSeparator(), "5px 0 5px 0", optionsForm.panels.AutoSave.innerTable],
        ["enableAutoSaveMode", null, this.CheckBox(null, this.loc.FormOptions.EnableAutoSaveMode), "5px 5px 5px 15px", optionsForm.panels.AutoSave.innerTable]
    ]

    for (var i = 0; i < controlProps.length; i++) {
        var name = controlProps[i][0];
        var caption = controlProps[i][1];
        var control = controlProps[i][2];
        if (controlProps[i][3]) control.style.margin = controlProps[i][3];
        var table = controlProps[i][4];
        optionsForm.controls[name] = control;
        if (i != 0) optionsForm.controls[name + "Row"] = table.addRow();
        if (caption) {
            var textCell = table.addTextCellInLastRow(caption);
            textCell.className = "stiDesignerCaptionControls";
            textCell.style.padding = "0 15px 0 15px";
        }
        var controlCell = table.addCellInLastRow(control);
        if (!caption) controlCell.setAttribute("colspan", "2");
    }

    //Hide not use properties
    optionsForm.controls.showOrder.style.display = "none";
    optionsForm.controls.showRulersRow.style.display = "none";
    optionsForm.controls.showDimensionLinesRow.style.display = "none";
    optionsForm.mainButtons.QuickInfo.style.display = "none";
    if (!this.options.cloudMode) optionsForm.controls.requestChangesWhenSaving.style.display = "none";

    optionsForm.controls.enableAutoSaveMode.action = function () {
        optionsForm.controls.autoSaveInterval.setEnabled(this.isChecked);
    }

    optionsForm.controls.showToolbox.action = function () {
        if (!optionsForm.controls.showInsertTab.isChecked && !this.isChecked)
            optionsForm.controls.showInsertTab.setChecked(true);
    }

    optionsForm.controls.showInsertTab.action = function () {
        if (!optionsForm.controls.showToolbox.isChecked && !this.isChecked)
            optionsForm.controls.showToolbox.setChecked(true);
    }

    //Form Methods
    optionsForm.setMode = function (mode) {
        optionsForm.mode = mode;
        for (var panelName in optionsForm.panels) {
            optionsForm.panels[panelName].style.display = mode == panelName ? "" : "none";
            optionsForm.mainButtons[panelName].setSelected(mode == panelName);
        }
    }

    optionsForm.fill = function (designerOptions) {
        var boolProps = ["showHeaders", "showRulers", "showOrder", "runDesignerAfterInsert", "useLastFormat", "showDimensionLines", "generateLocalizedName",
            "alignToGrid", "showGrid", "quickInfoOverlay", "enableAutoSaveMode"];
        for (var i = 0; i < boolProps.length; i++) {
            this.controls[boolProps[i]].setChecked(designerOptions[boolProps[i]]);
        }
        this.controls.autoSaveInterval.setEnabled(designerOptions.enableAutoSaveMode);
        if (this.controls["gridMode" + designerOptions.gridMode]) this.controls["gridMode" + designerOptions.gridMode].setChecked(true);

        var textProps = ["gridSizeInch", "gridSizeHundredthsOfInch", "gridSizeCentimetres", "gridSizeMillimeters", "gridSizePixels"];
        for (var i = 0; i < textProps.length; i++) {
            this.controls[textProps[i]].value = designerOptions[textProps[i]];
        }
        if (this.controls["quickInfoType" + designerOptions.quickInfoType]) this.controls["quickInfoType" + designerOptions.quickInfoType].setChecked(true);
        this.controls.autoSaveInterval.setKey(designerOptions.autoSaveInterval);
        this.controls.showToolbox.setChecked(this.jsObject.options.showToolbox);
        this.controls.showInsertTab.setChecked(this.jsObject.options.showInsertTab);
        this.controls.requestChangesWhenSaving.setChecked(this.jsObject.options.requestChangesWhenSaving);
    }

    optionsForm.show = function () {
        optionsForm.setMode("Main");
        var designerOptions = this.jsObject.options.report ? this.jsObject.options.report.info : this.jsObject.options.defaultDesignerOptions;
        optionsForm.fill(designerOptions);

        this.changeVisibleState(true);
    }

    optionsForm.action = function () {
        var designerOptions = {};
        var boolProps = ["showHeaders", "showRulers", "showOrder", "runDesignerAfterInsert", "useLastFormat", "showDimensionLines", "generateLocalizedName",
            "alignToGrid", "showGrid", "quickInfoOverlay", "enableAutoSaveMode"];
        for (var i = 0; i < boolProps.length; i++) {
            designerOptions[boolProps[i]] = this.controls[boolProps[i]].isChecked;
        }
        var textProps = ["gridSizeInch", "gridSizeHundredthsOfInch", "gridSizeCentimetres", "gridSizeMillimeters", "gridSizePixels"];
        for (var i = 0; i < textProps.length; i++) {
            designerOptions[textProps[i]] = this.controls[textProps[i]].value;
        }
        designerOptions.gridMode = this.controls.gridModeLines.isChecked ? "Lines" : "Dots";

        var quickInfoTypes = ["None", "ShowComponentsNames", "ShowContent", "ShowFields", "ShowFieldsOnly", "ShowEvents"];
        for (var i = 0; i < quickInfoTypes.length; i++) {
            if (this.controls["quickInfoType" + quickInfoTypes[i]] && this.controls["quickInfoType" + quickInfoTypes[i]].isChecked) {
                designerOptions.quickInfoType = quickInfoTypes[i];
                break;
            }
        }
        designerOptions.autoSaveInterval = this.controls.autoSaveInterval.key;

        this.jsObject.options.showInsertTab = optionsForm.controls.showInsertTab.isChecked;
        this.jsObject.options.showToolbox = optionsForm.controls.showToolbox.isChecked;
        this.jsObject.options.requestChangesWhenSaving = optionsForm.controls.requestChangesWhenSaving.isChecked;

        if (this.jsObject.options.buttons.insertToolButton) {
            this.jsObject.options.buttons.insertToolButton.parentElement.style.display = (!this.jsObject.options.showInsertButton ? false : this.jsObject.options.showInsertTab) ? "" : "none";
            if (this.jsObject.options.workPanel.currentPanel == this.jsObject.options.insertPanel && !this.jsObject.options.showInsertTab.isChecked) {
                this.jsObject.options.workPanel.showPanel(this.jsObject.options.homePanel);
                this.jsObject.options.buttons.homeToolButton.setSelected(true);
            }
        }

        this.jsObject.options.toolbox.changeVisibleState(this.jsObject.options.showToolbox);
        if (optionsForm.controls.showToolbox.isChecked) this.jsObject.options.toolbox.update();

        this.jsObject.SetCookie("StimulsoftMobileDesignerSetupToolbox", JSON.stringify({
            showToolbox: this.jsObject.options.showToolbox,
            showInsertTab: this.jsObject.options.showInsertTab
        }));

        this.jsObject.SetCookie("StimulsoftMobileDesignerRequestChangesWhenSaving", this.jsObject.options.requestChangesWhenSaving ? "true" : "false");

        this.changeVisibleState(false);
        this.jsObject.SendCommandApplyDesignerOptions(designerOptions);
    }

    return optionsForm;
}