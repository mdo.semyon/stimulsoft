﻿
StiMobileDesigner.prototype.FormButton = function (form, name, caption, imageName, tooltip) {
    var button = this.SmallButton(name, null, caption, imageName, tooltip, null, this.GetStyles("FormButton"), true);
    button.innerTable.style.width = "100%";
    button.style.minWidth = "80px";
    if (button.caption) button.caption.style.textAlign = "center";
    button.form = form;

    return button;
}