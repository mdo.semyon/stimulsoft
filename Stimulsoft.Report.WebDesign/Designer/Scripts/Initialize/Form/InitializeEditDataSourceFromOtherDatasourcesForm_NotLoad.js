﻿
StiMobileDesigner.prototype.InitializeEditDataSourceFromOtherDatasourcesForm_ = function () {

    var editDataSourceForm = this.BaseForm("editDataSourceFromOtherDatasourcesForm", this.loc.PropertyMain.DataSource, 1, this.HelpLinks["dataSourceFromOtherDatasources"]);
    editDataSourceForm.datasource = null;
    editDataSourceForm.mode = "Edit";
    editDataSourceForm.controls = {};

    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerImageFormMainPanel";
    editDataSourceForm.container.appendChild(mainTable);
    editDataSourceForm.container.style.padding = "0px";

    var buttonProps = [
        ["DataSource", "VirtualDataSource.VirtualSource.png", this.loc.PropertyMain.DataSource],
        ["Sort", "VirtualDataSource.VirtualSourceSort.png", this.loc.PropertyMain.Sort],
        ["Filter", "VirtualDataSource.VirtualSourceFilter.png", this.loc.PropertyMain.Filters],
        ["Groups", "VirtualDataSource.VirtualSourceGroup.png", this.loc.Wizards.Groups],
        ["Results", "VirtualDataSource.VirtualSourceResult.png", this.loc.Wizards.Results]
    ];

    //Add Panels && Buttons
    var panelsContainer = mainTable.addCell();
    var buttonsPanel = mainTable.addCell();
    buttonsPanel.style.verticalAlign = "top";
    editDataSourceForm.mainButtons = {};
    editDataSourceForm.panels = {};

    for (var i = 0; i < buttonProps.length; i++) {
        var panel = document.createElement("Div");
        panel.className = "stiDesignerEditFormPanel";
        panel.style.width = "560px";
        if (i != 0) panel.style.display = "none";
        panelsContainer.appendChild(panel);
        editDataSourceForm.panels[buttonProps[i][0]] = panel;
        panel.onShow = function () { };

        var button = this.StandartFormBigButton(null, null, buttonProps[i][2], buttonProps[i][1], buttonProps[i][2], 80);
        button.style.margin = "2px";
        editDataSourceForm.mainButtons[buttonProps[i][0]] = button;
        buttonsPanel.appendChild(button);
        button.panelName = buttonProps[i][0];
        button.action = function () {
            editDataSourceForm.showPanel(this.panelName);
        }
    }

    //Data Source
    var textBoxesTable = this.CreateHTMLTable();
    editDataSourceForm.panels.DataSource.appendChild(textBoxesTable);

    var textBoxes = [
        ["name", this.loc.PropertyMain.Name, "6px 0 3px 0"],
        ["alias", this.loc.PropertyMain.Alias, "3px 0 6px 0"]
    ]

    for (var i = 0; i < textBoxes.length; i++) {
        if (i != 0) textBoxesTable.addRow();
        var textCell = textBoxesTable.addTextCellInLastRow(textBoxes[i][1] + ":");
        textCell.className = "stiDesignerCaptionControlsBigIntervals";
        textCell.style.padding = "5px 50px 5px 15px";
        var textBox = this.TextBox(null, 230);
        textBox.style.margin = textBoxes[i][2];
        editDataSourceForm.controls[textBoxes[i][0]] = textBox;
        textBoxesTable.addCellInLastRow(textBox).className = "stiDesignerControlCellsBigIntervals";
    }

    editDataSourceForm.panels.DataSource.appendChild(this.FormSeparator());
    var toolBarDataSource = this.CreateHTMLTable();
    editDataSourceForm.panels.DataSource.appendChild(toolBarDataSource);
    var newDataSourceButton = this.StandartSmallButton(null, null, this.loc.FormDictionaryDesigner.DataSourceNew, "DataSourceNew.png", null, null, true);
    newDataSourceButton.setEnabled(this.options.permissionDataSources.indexOf("All") >= 0 || this.options.permissionDataSources.indexOf("Create") >= 0);

    toolBarDataSource.addCell(newDataSourceButton).style.padding = "4px";
    newDataSourceButton.action = function () {
        this.jsObject.InitializeSelectConnectionForm(function (selectConnectionForm) {
            selectConnectionForm.changeVisibleState(true);
        });
    };
    editDataSourceForm.panels.DataSource.appendChild(this.FormSeparator());
    
    editDataSourceForm.controls.name.action = function () {
        if (this.oldValue == editDataSourceForm.controls.alias.value) {
            editDataSourceForm.controls.alias.value = this.value;
        }
    }

    var dataSourcesTree = this.DataSourcesTree();
    dataSourcesTree.style.height = this.options.isTouchDevice ? "325px" : "341px";
    dataSourcesTree.style.overflow = "auto";
    editDataSourceForm.panels.DataSource.appendChild(dataSourcesTree);
    dataSourcesTree.action = function () { editDataSourceForm.action(); }

    //Sort
    var sortControl = this.SortControl("editDataSourceFormSortControl" + this.newGuid().replace(/-/g, ''), null, 555, this.options.isTouchDevice ? 408 : 413);
    editDataSourceForm.panels.Sort.appendChild(sortControl);
    editDataSourceForm.panels.Sort.onShow = function () {
        var currentDataSourceName = editDataSourceForm.getCurrentDataSourceName();
        if (sortControl.currentDataSourceName != currentDataSourceName) sortControl.sortContainer.clear();
        sortControl.currentDataSourceName = currentDataSourceName;
    }

    //Filter
    var filterControl = this.FilterControl("editDataSourceFormFilterControl" + this.newGuid().replace(/-/g, ''), null, null, this.options.isTouchDevice ? 413 : 418);
    editDataSourceForm.panels.Filter.appendChild(filterControl);
    editDataSourceForm.panels.Filter.onShow = function () {
        var currentDataSourceName = editDataSourceForm.getCurrentDataSourceName();
        if (filterControl.currentDataSourceName != currentDataSourceName) filterControl.controls.filterContainer.clear();
        filterControl.currentDataSourceName = currentDataSourceName;
    }

    //Groups
    var groupsControl = this.GroupsControl("editDataSourceFormGroupsControl" + this.newGuid().replace(/-/g, ''), 555, this.options.isTouchDevice ? 408 : 413);
    editDataSourceForm.panels.Groups.appendChild(groupsControl);
    editDataSourceForm.panels.Groups.onShow = function () {
        var currentDataSourceName = editDataSourceForm.getCurrentDataSourceName();
        if (groupsControl.currentDataSourceName != currentDataSourceName) groupsControl.sortContainer.clear();
        groupsControl.currentDataSourceName = currentDataSourceName;
    }

    //Results
    var resultsControl = this.ResultsControl("editDataSourceFormResultsControl" + this.newGuid().replace(/-/g, ''), 555, this.options.isTouchDevice ? 408 : 413);
    editDataSourceForm.panels.Results.appendChild(resultsControl);
    editDataSourceForm.panels.Results.onShow = function () {
        var currentDataSourceName = editDataSourceForm.getCurrentDataSourceName();
        if (resultsControl.currentDataSourceName != currentDataSourceName) resultsControl.resultContainer.clear();
        resultsControl.currentDataSourceName = currentDataSourceName;
    }

    editDataSourceForm.getCurrentDataSourceName = function () {
        return dataSourcesTree.selectedItem ? dataSourcesTree.selectedItem.itemObject.name : null;
    }

    editDataSourceForm.rebuildTrees = function (objectName) {
        if (objectName) {
            dataSourcesTree.build(true, true);
            var dataSourceItem = dataSourcesTree.getItem(objectName, null, "DataSource");
            if (dataSourceItem) {
                dataSourceItem.setSelected();
                dataSourceItem.openTree();
                dataSourcesTree.update();
            }
        }
    }

    editDataSourceForm.showPanel = function (selectedPanelName) {
        this.selectedPanelName = selectedPanelName;
        for (var panelName in this.panels) {
            this.panels[panelName].style.display = selectedPanelName == panelName ? "" : "none";
            this.mainButtons[panelName].setSelected(selectedPanelName == panelName);
            if (selectedPanelName == panelName) this.panels[panelName].onShow();
        }
    }

    editDataSourceForm.onshow = function () {
        this.showPanel("DataSource");
        this.mode = "Edit";

        if (typeof (this.datasource) == "string") {
            this.datasource = this.jsObject.DataSourceObject(this.datasource);
            this.mode = "New";
        }
        this.caption.innerHTML = this.jsObject.loc.FormDictionaryDesigner["DataSource" + this.mode];

        this.editableDictionaryItem = this.mode == "Edit" && this.jsObject.options.dictionaryTree
            ? this.jsObject.options.dictionaryTree.selectedItem : null;

        //DataSource                
        this.controls.name.hideError();
        this.controls.name.focus();
        this.controls.name.value = this.datasource.name;
        this.controls.alias.value = this.datasource.alias;

        dataSourcesTree.build(true, true);
        var selfDataSourceItem = dataSourcesTree.getItem(this.datasource.name, null, "DataSource");
        if (selfDataSourceItem && selfDataSourceItem.parent) {
            if (this.jsObject.GetCountObjects(selfDataSourceItem.parent.childs) == 1)
                selfDataSourceItem.parent.remove();
            else
                selfDataSourceItem.remove();
        }

        var dataSourceItem = dataSourcesTree.getItem(this.datasource.nameInSource, null, "DataSource");
        if (dataSourceItem) {
            dataSourceItem.setSelected();
            dataSourceItem.openTree();
        }

        //Sort
        var sorts = this.datasource.sortData != "" ? JSON.parse(Base64.decode(this.datasource.sortData)) : [];
        sortControl.currentDataSourceName = editDataSourceForm.getCurrentDataSourceName();
        sortControl.fill(sorts);

        //Filter
        var filters = this.datasource.filterData != "" ? JSON.parse(Base64.decode(this.datasource.filterData)) : [];
        filterControl.currentDataSourceName = editDataSourceForm.getCurrentDataSourceName();
        filterControl.fill(filters, this.datasource.filterOn, this.datasource.filterMode);

        //Groups
        var groups = this.datasource.groupsData != "" ? JSON.parse(Base64.decode(this.datasource.groupsData)) : [];
        groupsControl.currentDataSourceName = editDataSourceForm.getCurrentDataSourceName();
        groupsControl.fill(groups);

        //Results
        var results = this.datasource.resultsData != "" ? JSON.parse(Base64.decode(this.datasource.resultsData)) : [];
        resultsControl.currentDataSourceName = editDataSourceForm.getCurrentDataSourceName();
        resultsControl.fill(results);
    }

    editDataSourceForm.action = function () {
        this.datasource["mode"] = this.mode;

        if ((this.mode == "New" || this.datasource.name != this.controls.name.value) &&
            !(this.controls.name.checkExists(this.jsObject.GetDataSourcesFromDictionary(this.jsObject.options.report.dictionary), "name") &&
                this.controls.name.checkExists(this.jsObject.GetVariablesFromDictionary(this.jsObject.options.report.dictionary), "name"))) {
            this.showPanel("DataSource");
            return;
        }

        if (this.mode == "Edit") this.datasource.oldName = this.datasource.name;

        this.datasource.name = this.controls.name.value;
        this.datasource.alias = this.controls.alias.value;
        this.datasource.nameInSource = dataSourcesTree.selectedItem && dataSourcesTree.selectedItem.itemObject.typeItem != "NoItem"
            ? dataSourcesTree.selectedItem.itemObject.name : "";

        var filterResult = filterControl.getValue();
        this.datasource.filterOn = filterResult.filterOn;
        this.datasource.filterMode = filterResult.filterMode;
        this.datasource.filterData = Base64.encode(JSON.stringify(filterResult.filters));

        var sortResult = sortControl.getValue();
        this.datasource.sortData = sortResult.length == 0 ? "" : Base64.encode(JSON.stringify(sortResult));

        var groupColumns = groupsControl.getValue();
        this.datasource.groupsData = groupColumns.length == 0 ? "" : Base64.encode(JSON.stringify(groupColumns));

        var results = resultsControl.getValue();
        this.datasource.resultsData = results.length == 0 ? "" : Base64.encode(JSON.stringify(results));

        if (!this.controls.name.checkNotEmpty(this.jsObject.loc.PropertyMain.Name)) {
            this.showPanel("DataSource");
            return;
        }
        
        this.changeVisibleState(false);
        this.jsObject.SendCommandCreateOrEditDataSource(this.datasource);
    }

    return editDataSourceForm;
}