﻿
StiMobileDesigner.prototype.InitializeShapeForm_ = function () {
    var shapeForm = this.BaseFormPanel("shapeForm", this.loc.Components.StiShape, 1);
    shapeForm.controls = {};
    var jsObject = this;

    //Shape Sample
    var sampleContainer = document.createElement("div");
    sampleContainer.style.width = "400px";
    sampleContainer.style.height = "200px";
    sampleContainer.style.padding = "10px";
    sampleContainer.style.textAlign = "center";
    shapeForm.container.appendChild(sampleContainer);

    var shapeSample = this.CreateSvgElement("svg");
    shapeSample.setAttribute("width", "200");
    shapeSample.setAttribute("height", "200");
    shapeSample.style.overflow = "hidden";
    sampleContainer.appendChild(shapeSample);

    shapeSample.update = function () {
        var params = {
            shapeProperties: shapeForm.getControlsValues(),
            componentName: shapeForm.currentComponent.properties.name
        }

        jsObject.SendCommandToDesignerServer("GetShapeSampleImage", params, function (answer) {
            if (answer.content) {
                jsObject.InsertSvgContent(shapeSample, answer.content);
            }
        });
    }

    shapeForm.container.appendChild(this.FormSeparator());

    //Properties
    var propertiesContainer = document.createElement("div");
    propertiesContainer.className = "stiShapeFormPropertiesContainer";
    shapeForm.container.appendChild(propertiesContainer);

    var properties = [
        ["shapeType", this.loc.PropertyMain.Type, this.ShapeTypeControl("shapeFormShapeType", this.options.propertyControlWidth + 4), "6px"],
        ["size", this.loc.PropertyMain.Size, this.PropertyTextBox("shapeFormSize", this.options.propertyNumbersControlWidth), "6px"],
        ["shapeBorderColor", this.loc.PropertyMain.BorderColor, this.PropertyColorControl("shapeFormBorderColor", null, this.options.propertyControlWidth), "5px 6px 5px 6px"],
        ["shapeBorderStyle", this.loc.PropertyMain.Style, this.PropertyDropDownList("shapeFormBorderStyle", this.options.propertyControlWidth, this.GetBorderStyleItems(), true, true), "6px"],
        ["brush", this.loc.PropertyMain.Brush, this.PropertyBrushControl("shapeFormBrush", null, this.options.propertyControlWidth), "5px 6px 5px 6px"]
    ];
        
    var proprtiesTable = this.CreateHTMLTable();
    proprtiesTable.style.margin = "";
    propertiesContainer.appendChild(proprtiesTable);

    for (var i = 0; i < properties.length; i++) {
        var textCell = proprtiesTable.addTextCellInLastRow(properties[i][1]);
        textCell.className = "stiDesignerCaptionControlsBigIntervals";
        textCell.style.whiteSpace = "normal";
        textCell.style.width = "100%";

        var control = properties[i][2];
        control.propertyName = properties[i][0];
        control.style.margin = properties[i][3];
        proprtiesTable.addCellInLastRow(control);
        proprtiesTable.addRow();
        jsObject.AddMainMethodsToPropertyControl(control);
        shapeForm.controls[control.propertyName] = control;

        control.action = function () {
            shapeSample.update();
        }
    }
       
    shapeForm.show = function () {
        this.changeVisibleState(true);
        var selectedObject = this.jsObject.options.selectedObject;
        if (!selectedObject) this.changeVisibleState(false);
        this.currentComponent = selectedObject;        
        this.updateControlsValues();
        shapeSample.update();
    }

    shapeForm.action = function () {
        this.changeVisibleState(false);

        var values = this.getControlsValues();
        var propNames = [];
        for (var propName in values) {
            this.currentComponent.properties[propName] = values[propName];
            propNames.push(propName);
        }

        jsObject.SendCommandSendProperties(this.currentComponent, propNames);
        jsObject.options.updateLastStyleProperties = true;
    }
        
    shapeForm.getControlsValues = function () {
        var values = {};
        for (var i = 0; i < properties.length; i++) {
            var proprtyName = properties[i][0];
            var control = properties[i][2];
            values[proprtyName] = control.getValue();
        }
        return values;
    }

    shapeForm.updateControlsValues = function () {
        var compProperties = shapeForm.currentComponent.properties;
        for (var i = 0; i < properties.length; i++) {
            var proprtyName = properties[i][0];
            var control = properties[i][2];
            if (compProperties[proprtyName] != null) {
                control.setValue(compProperties[proprtyName])
            }
        }
    }

    return shapeForm;
}