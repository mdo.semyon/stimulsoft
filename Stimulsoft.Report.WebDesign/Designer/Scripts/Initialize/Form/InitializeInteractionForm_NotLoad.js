﻿
StiMobileDesigner.prototype.InitializeInteractionForm_ = function () {
    var interactionForm = this.BaseFormPanel("interactionForm", this.loc.PropertyMain.Interaction, 1);
    var mainHeight = 425;

    //Main Table
    var mainTable = this.CreateHTMLTable();
    mainTable.className = "stiDesignerImageFormMainPanel";
    interactionForm.container.appendChild(mainTable);
    interactionForm.container.style.padding = "0px";

    //Buttons
    var buttonProps = [
        ["Collapsed", "Interaction.InteractionCollapse.png", this.loc.PropertyMain.Collapsed],
        ["DrillDown", "Interaction.InteractionDrilldown.png", this.loc.PropertyMain.DrillDown],
        ["DrillDownParameters", "Interaction.InteractionDrilldown.png", this.loc.PropertyMain.DrillDownParameters],
        ["Sort", "Interaction.InteractionSorting.png", this.loc.PropertyMain.Sort],
        ["Selection", "Interaction.InteractionSelection.png", this.loc.PropertyMain.Selection],
        ["Bookmark", "Interaction.InteractionBookmark.png", this.loc.PropertyMain.Bookmark],
        ["Hyperlink", "Interaction.InteractionHyperlink.png", this.loc.PropertyMain.Hyperlink],
        ["Tag", "Interaction.InteractionTag.png", this.loc.PropertyMain.Tag],
        ["Tooltip", "Interaction.InteractionTooltip.png", this.loc.PropertyMain.ToolTip]
    ];

    //Add Panels && Buttons
    var panelsContainer = mainTable.addCell();
    var buttonsPanel = mainTable.addCell();
    buttonsPanel.style.verticalAlign = "top";
    interactionForm.mainButtons = {};
    interactionForm.panels = {};

    for (var i = 0; i < buttonProps.length; i++) {
        var panel = document.createElement("Div");
        panel.className = "stiDesignerInteractionFormPanel";
        panel.style.height = mainHeight + "px";
        if (i != 0) panel.style.display = "none";
        panelsContainer.appendChild(panel);
        interactionForm.panels[buttonProps[i][0]] = panel;
        panel.onShow = function () { };

        var button = this.StandartFormBigButton(null, null, buttonProps[i][2], buttonProps[i][1], buttonProps[i][2], 110);
        button.style.minHeight = "0px";
        button.style.minWidth = "110px";
        button.style.margin = "2px";
        interactionForm.mainButtons[buttonProps[i][0]] = button;
        buttonsPanel.appendChild(button);
        button.panelName = buttonProps[i][0];
        button.action = function () {
            interactionForm.showPanel(this.panelName);
        }
        if (buttonProps[i][0] == "Collapsed" || buttonProps[i][0] == "Selection") button.style.display = "none";

        //add marker
        var marker = document.createElement("div");
        marker.style.display = "none";
        marker.className = "stiUsingMarker";
        var markerInner = document.createElement("div");
        marker.appendChild(markerInner);
        button.style.position = "relative";
        button.appendChild(marker);
        button.marker = marker;
    }

    //Collapsed
    var collapsingEnabled = this.CheckBox(null, this.loc.PropertyMain.CollapsingEnabled);
    collapsingEnabled.style.margin = "8px 0 8px 13px";
    interactionForm.panels.Collapsed.appendChild(collapsingEnabled);
    interactionForm.panels.Collapsed.appendChild(this.FormBlockHeader(this.loc.PropertyMain.Collapsed));

    var collapsedTextArea = this.TextArea(null, 535, this.options.isTouchDevice ? mainHeight - 110 : mainHeight - 100);
    collapsedTextArea.style.margin = "4px";
    interactionForm.panels.Collapsed.appendChild(collapsedTextArea);
    collapsedTextArea.addInsertButton();
    interactionForm.panels.Collapsed.appendChild(this.FormSeparator());

    var collapseGroupFooter = this.CheckBox(null, this.loc.PropertyMain.CollapseGroupFooter);
    collapseGroupFooter.style.margin = "8px 0 0 13px";
    interactionForm.panels.Collapsed.appendChild(collapseGroupFooter);

    //DrillDown
    var drillDownEnabled = this.CheckBox(null, this.loc.PropertyMain.DrillDownEnabled);
    drillDownEnabled.style.margin = "8px 0 8px 13px";
    interactionForm.panels.DrillDown.appendChild(drillDownEnabled);
    interactionForm.panels.DrillDown.appendChild(this.FormBlockHeader(this.loc.PropertyMain.DrillDownPage));

    var pageContainer = this.EasyContainer(null, 200);
    pageContainer.style.border = "0px";
    pageContainer.style.margin = "8px";
    interactionForm.panels.DrillDown.appendChild(pageContainer);

    interactionForm.panels.DrillDown.appendChild(this.FormBlockHeader(this.loc.PropertyMain.DrillDownReport));
    var drillDownReportTable = this.CreateHTMLTable();
    drillDownReportTable.addTextCell(this.loc.Cloud.labelFileName).className = "stiDesignerDrillDownFormCaptionControls";
    var drillDownFileName = this.TextBox(null, 250);
    drillDownReportTable.addCell(drillDownFileName).style.padding = "8px";
    interactionForm.panels.DrillDown.appendChild(drillDownReportTable);
    interactionForm.panels.DrillDown.appendChild(this.FormSeparator());

    var drillDownModeTable = this.CreateHTMLTable();
    drillDownModeTable.addTextCell(this.loc.PropertyMain.DrillDownMode + ":").className = "stiDesignerDrillDownFormCaptionControls";
    var drillDownMode = this.DropDownList(null, 180, null, this.GetDrillDownModeItems(), true, false, null, true);
    drillDownModeTable.addCell(drillDownMode).style.padding = "8px";
    interactionForm.panels.DrillDown.appendChild(drillDownModeTable);
    if (this.options.isJava) {
        drillDownModeTable.style.display = 'none';
    }

    //DrillDown Parameters
    var parametersTable = this.CreateHTMLTable();
    interactionForm.panels.DrillDownParameters.appendChild(parametersTable);
    var drillDownParamsContainer = this.EasyContainer(160, mainHeight - 10);
    drillDownParamsContainer.style.margin = "5px";
    parametersTable.addCell(drillDownParamsContainer).className = "stiDesignerCreateDataColumsButtonsTable";

    drillDownParamsContainer.onSelected = function () {
        if (this.selectedItem && this.selectedItem.itemObject) {
            drillDownParametersName.value = this.selectedItem.itemObject.name;
            drillDownParametersExpression.value = Base64.decode(this.selectedItem.itemObject.expression);
        }
    }

    drillDownParamsContainer.updateItem = function () {
        if (this.selectedItem && this.selectedItem.itemObject) {
            this.selectedItem.itemObject.name = drillDownParametersName.value;
            this.selectedItem.itemObject.expression = Base64.encode(drillDownParametersExpression.value);
        }
    }

    var drillDownControlsTable = this.CreateHTMLTable();
    parametersTable.addCell(drillDownControlsTable).style.verticalAlign = "top";
    drillDownControlsTable.addTextCell(this.loc.PropertyMain.Name + ":").className = "stiDesignerCaptionControls";
    var drillDownParametersName = this.TextBox(null, 180);
    drillDownParametersName.action = function () { drillDownParamsContainer.updateItem(); }
    drillDownControlsTable.addCell(drillDownParametersName).style.padding = "8px 0 4px 0";

    var expressionTextCell = drillDownControlsTable.addTextCellInNextRow(this.loc.PropertyMain.Expression + ":");
    expressionTextCell.className = "stiDesignerCaptionControls";
    expressionTextCell.style.verticalAlign = "top";
    expressionTextCell.style.paddingTop = "8px";
    var drillDownParametersExpression = this.TextArea(null, 230, 100);
    drillDownParametersExpression.addInsertButton();
    drillDownParametersExpression.action = function () { drillDownParamsContainer.updateItem(); }
    drillDownControlsTable.addCellInLastRow(drillDownParametersExpression).style.padding = "4px 0 8px 0";

    drillDownParametersExpression.insertButton.action = function () {
        var dictionaryTree = this.jsObject.options.dictionaryTree;
        if (dictionaryTree && dictionaryTree.selectedItem) {
            var text = dictionaryTree.selectedItem.getResultForEditForm();
            if (text && text.indexOf("{") == 0 && text.endsWith("}")) {
                text = text.substring(1, text.length - 1);
            }
            drillDownParametersExpression.insertText(text);
        }
    }

    //Sort
    var sortingEnabled = this.CheckBox(null, this.loc.PropertyMain.SortingEnabled);
    sortingEnabled.style.margin = "8px 0 8px 13px";
    interactionForm.panels.Sort.appendChild(sortingEnabled);
    interactionForm.panels.Sort.appendChild(this.FormBlockHeader(this.loc.PropertyMain.SortingColumn));
    this.AddProgressToControl(interactionForm);

    //Selection
    var selectionEnabled = this.CheckBox(null, this.loc.PropertyMain.SelectionEnabled);
    selectionEnabled.style.margin = "8px 0 8px 13px";
    interactionForm.panels.Selection.appendChild(selectionEnabled);

    //Bookmark
    var bookmarkTextArea = this.TextArea(null, 535, mainHeight - 10);
    bookmarkTextArea.style.margin = "4px 0 0 4px";
    interactionForm.panels.Bookmark.appendChild(bookmarkTextArea);
    bookmarkTextArea.addInsertButton();

    //Hyperlink
    var hyperlinkTable = this.CreateHTMLTable();
    hyperlinkTable.addTextCell(this.loc.PropertyMain.Type + ":").className = "stiDesignerCaptionControlsBigIntervals";
    var hyperlinkType = this.DropDownList(null, 230, null, this.GetHyperlinkTypeItems(), true, false, null, true);
    hyperlinkTable.addCell(hyperlinkType).style.padding = "8px";
    interactionForm.panels.Hyperlink.appendChild(hyperlinkTable);
    interactionForm.panels.Hyperlink.appendChild(this.FormBlockHeader(this.loc.PropertyMain.Hyperlink));

    var hyperlinkTextArea = this.TextArea(null, 535, this.options.isTouchDevice ? mainHeight - 78 : mainHeight - 73);
    hyperlinkTextArea.style.margin = "4px 0 0 4px";
    interactionForm.panels.Hyperlink.appendChild(hyperlinkTextArea);
    hyperlinkTextArea.addInsertButton();

    //Tag
    var tagTextArea = this.TextArea(null, 535, mainHeight - 10);
    tagTextArea.style.margin = "4px 0 0 4px";
    interactionForm.panels.Tag.appendChild(tagTextArea);
    tagTextArea.addInsertButton();

    //Tooltip
    var tooltipTextArea = this.TextArea(null, 535, mainHeight - 10);
    tooltipTextArea.style.margin = "4px 0 0 4px";
    interactionForm.panels.Tooltip.appendChild(tooltipTextArea);
    tooltipTextArea.addInsertButton();

    interactionForm.resizeHeight = function (height) {
        collapsedTextArea.resize(null, this.jsObject.options.isTouchDevice ? height - 110 : height - 100);
        bookmarkTextArea.resize(null, height - 10);
        hyperlinkTextArea.resize(null, this.jsObject.options.isTouchDevice ? height - 78 : height - 73);
        tagTextArea.resize(null, height - 10);
        tooltipTextArea.resize(null, height - 10);
        drillDownParamsContainer.style.height = (height - 10) + "px";
        for (var i = 0; i < buttonProps.length; i++) {
            interactionForm.panels[buttonProps[i][0]].style.height = height + "px";
        }
    }

    //Form Methods
    interactionForm.showPanel = function (selectedPanelName) {
        this.selectedPanelName = selectedPanelName;
        for (var panelName in this.panels) {
            this.panels[panelName].style.display = selectedPanelName == panelName ? "" : "none";
            this.mainButtons[panelName].setSelected(selectedPanelName == panelName);
            if (selectedPanelName == panelName) this.panels[panelName].onShow();
        }

        var propertiesPanel = interactionForm.jsObject.options.propertiesPanel;
        propertiesPanel.editFormControl = null;
        propertiesPanel.setEnabled(
            selectedPanelName == "Collapsed" ||
            selectedPanelName == "Bookmark" ||
            selectedPanelName == "Hyperlink" ||
            selectedPanelName == "Tag" ||
            selectedPanelName == "Tooltip" ||
            selectedPanelName == "DrillDownParameters");
        if (selectedPanelName == "Collapsed") propertiesPanel.editFormControl = collapsedTextArea;
        if (selectedPanelName == "Bookmark") propertiesPanel.editFormControl = bookmarkTextArea;
        if (selectedPanelName == "Tooltip") propertiesPanel.editFormControl = tooltipTextArea;
        if (selectedPanelName == "Hyperlink") propertiesPanel.editFormControl = hyperlinkTextArea;

        if (selectedPanelName == "Sort" && !interactionForm.dataTree) {
            interactionForm.progress.show();
            setTimeout(function () {
                interactionForm.dataTree = interactionForm.jsObject.InteractionColumnsTree();
                interactionForm.panels.Sort.appendChild(interactionForm.dataTree);
                interactionForm.dataTree.build();
                interactionForm.dataTree.setKey(interactionForm.interaction != "StiEmptyValue" ? interactionForm.interaction.sortingColumn : "");
                interactionForm.progress.hide();
            }, 0);
        }
    }

    interactionForm.updateMarkers = function () {
        this.mainButtons["Collapsed"].marker.style.display = this.interaction.isBandInteraction && collapsingEnabled.isChecked ? "" : "none";
        this.mainButtons["DrillDown"].marker.style.display = drillDownEnabled.isChecked ? "" : "none";
        this.mainButtons["Selection"].marker.style.display = selectionEnabled.isChecked ? "" : "none";
        var showDrillDownParametersMarker = false;
        for (var i = 1; i <= 5; i++) {
            var itemObject = drillDownParamsContainer.childNodes[i - 1].itemObject;
            if (itemObject.name || itemObject.expression) showDrillDownParametersMarker = true;
        }
        this.mainButtons["DrillDownParameters"].marker.style.display = showDrillDownParametersMarker ? "" : "none";
        this.mainButtons["Sort"].marker.style.display = !sortingEnabled.isChecked ||
            (interactionForm.dataTree && interactionForm.dataTree.key) ||
            (interactionForm.interaction != "StiEmptyValue" && interactionForm.interaction.sortingColumn) ? "" : "none";
        this.mainButtons["Bookmark"].marker.style.display = bookmarkTextArea.value ? "" : "none";
        this.mainButtons["Hyperlink"].marker.style.display = hyperlinkTextArea.value ? "" : "none";
        this.mainButtons["Tag"].marker.style.display = tagTextArea.value ? "" : "none";
        this.mainButtons["Tooltip"].marker.style.display = tooltipTextArea.value ? "" : "none";
    }

    interactionForm.show = function (interactionValue) {
        var selectedObject = this.jsObject.options.selectedObject || this.jsObject.GetCommonObject(this.jsObject.options.selectedObjects);
        if (!selectedObject) return;
        this.interaction = interactionValue || selectedObject.properties.interaction;
        var isEmptyInteraction = this.interaction == "StiEmptyValue";

        this.jsObject.options.propertiesPanel.setDictionaryMode(true);
        this.changeVisibleState(true);
        this.showPanel("DrillDown");

        //Fill DrillDown Pages
        var items = this.jsObject.GetSubReportItems();
        for (var i = 0; i < items.length; i++) {
            var item = pageContainer.addItem(items[i].key, items[i], items[i].caption);
            if (i == 0) item.select();
        }

        //Fill DrillDown Parameters
        for (var i = 1; i <= 5; i++) {
            var itemObject = {
                name: this.interaction["drillDownParameter" + i + "Name"] || "",
                expression: this.interaction["drillDownParameter" + i + "Expression"] || ""
            }
            var item = drillDownParamsContainer.addItem("parameter" + i, itemObject, this.jsObject.loc.PropertyMain["DrillDownParameter" + i]);
            if (i == 1) item.action();
        }

        //Fill controls
        if (!isEmptyInteraction && this.interaction.isBandInteraction) {
            this.resizeHeight(540);
            this.mainButtons.Collapsed.style.display = "";
            this.mainButtons.Selection.style.display = "";
            this.showPanel("Collapsed");
            collapsingEnabled.setChecked(this.interaction.collapsingEnabled);
            collapseGroupFooter.setChecked(this.interaction.collapseGroupFooter);
            collapsedTextArea.value = Base64.decode(this.interaction.collapsedValue);
            selectionEnabled.setChecked(this.interaction.selectionEnabled);
        }

        drillDownEnabled.setChecked(!isEmptyInteraction ? this.interaction.drillDownEnabled : false);
        drillDownFileName.value = !isEmptyInteraction ? this.interaction.drillDownReport : "";
        drillDownMode.setKey(!isEmptyInteraction ? this.interaction.drillDownMode : "MultiPage");
        if (!isEmptyInteraction) {
            var pageItem = pageContainer.getItemByName(this.interaction.drillDownPage);
            if (pageItem)
                pageItem.select()
            else if (pageContainer.childNodes.length > 0)
                pageContainer.childNodes[0].select();
        }
        sortingEnabled.setChecked(!isEmptyInteraction ? this.interaction.sortingEnabled : false);
        bookmarkTextArea.value = !isEmptyInteraction ? Base64.decode(this.interaction.bookmark) : "";
        hyperlinkType.setKey(!isEmptyInteraction ? this.interaction.hyperlinkType : "HyperlinkExternalDocuments");
        hyperlinkTextArea.value = !isEmptyInteraction ? Base64.decode(this.interaction.hyperlink) : "";
        tagTextArea.value = !isEmptyInteraction ? Base64.decode(this.interaction.tag) : "";
        tooltipTextArea.value = !isEmptyInteraction ? Base64.decode(this.interaction.toolTip) : "";

        this.mainButtons.Selection.style.display = "none"; //Temporarily
        this.updateMarkers();
        this.markerTimer = setInterval(function () {
            interactionForm.updateMarkers();
        }, 250)
    }

    interactionForm.onhide = function () {
        this.jsObject.options.propertiesPanel.setDictionaryMode(false);
        clearTimeout(this.markerTimer);
    }

    interactionForm.applyValues = function () {
        if (this.interaction.isBandInteraction) {
            this.interaction.collapsingEnabled = collapsingEnabled.isChecked;
            this.interaction.collapseGroupFooter = collapseGroupFooter.isChecked;
            this.interaction.collapsedValue = Base64.encode(collapsedTextArea.value);
            this.interaction.selectionEnabled = selectionEnabled.isChecked;
        }
        this.interaction.drillDownPage = pageContainer.selectedItem && pageContainer.selectedItem.itemObject.name != "NotAssigned"
            ? pageContainer.selectedItem.itemObject.name : "";

        this.interaction.drillDownEnabled = drillDownEnabled.isChecked;
        this.interaction.drillDownReport = drillDownFileName.value;
        this.interaction.drillDownMode = drillDownMode.key;

        for (var i = 1; i <= 5; i++) {
            var itemObject = drillDownParamsContainer.childNodes[i - 1].itemObject;
            this.interaction["drillDownParameter" + i + "Name"] = itemObject.name;
            this.interaction["drillDownParameter" + i + "Expression"] = itemObject.expression;
        }

        this.interaction.sortingEnabled = sortingEnabled.isChecked;
        if (this.dataTree && this.dataTree.selectedItem) {
            interactionForm.interaction.sortingColumn = this.dataTree.getResult();
        }

        this.interaction.bookmark = Base64.encode(bookmarkTextArea.value);
        this.interaction.hyperlinkType = hyperlinkType.key;
        this.interaction.hyperlink = Base64.encode(hyperlinkTextArea.value);
        this.interaction.tag = Base64.encode(tagTextArea.value);
        this.interaction.toolTip = Base64.encode(tooltipTextArea.value);
    }

    interactionForm.action = function () {
        this.applyValues();
        this.changeVisibleState(false);
        this.jsObject.ApplyPropertyValue("interaction", this.interaction);
    }

    return interactionForm;
}

StiMobileDesigner.prototype.InteractionColumnsTree = function () {
    var tree = this.DataTree();
    tree.style.padding = "0px";

    tree.build = function () {
        this.clear();

        this.mainItem = this.jsObject.HiddenMainTreeItem(this, true);
        this.appendChild(this.mainItem);

        var dictionary = this.jsObject.options.report.dictionary;
        if (dictionary == null) return;

        var dataBands = this.jsObject.GetDataBandsForInteractionSort();
        if (dataBands) {
            for (var i = 0; i < dataBands.length; i++) {
                var dataSource = this.jsObject.GetDataSourceByNameFromDictionary(dataBands[i].dataSourceName);
                if (dataSource) {
                    var copyDataSource = this.jsObject.CopyObject(dataSource);
                    copyDataSource.name = copyDataSource.correctName = copyDataSource.alias = dataBands[i].componentName;
                    this.addTreeItems([copyDataSource], this.mainItem);
                }
            }
        }
    }

    tree.getResult = function () {
        if (!this.selectedItem || this.selectedItem.itemObject.typeItem == "NoItem") return "";

        var currItem = this.selectedItem;
        var fullName = "";
        while (currItem.parent != null) {
            if (fullName != "") fullName = "." + fullName;
            var name = currItem.itemObject.typeItem == "Relation" ? currItem.itemObject.nameInSource : (currItem.itemObject.correctName || currItem.itemObject.name);
            fullName = name + fullName;
            currItem = currItem.parent;
        }
        return fullName;
    }

    return tree;
}