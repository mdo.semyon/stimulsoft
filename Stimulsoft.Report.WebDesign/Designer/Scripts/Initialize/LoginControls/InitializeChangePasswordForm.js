﻿
StiMobileDesigner.prototype.InitializeChangePasswordForm = function () {
    var form = this.BaseForm("changePasswordForm", this.loc.Navigator.ButtonChangePassword, 3, null, true);
    var this_ = this;
    form.caption.style.padding = "0 10px 0 15px";    
    form.container.style.padding = "7px";

    var textBoxes = [
        ["currentPassword", this.loc.Administration.LabelCurrentPassword, 250],
        ["newPassword", this.loc.Administration.LabelNewPassword, 250]
    ]
    for (var i = 0; i < textBoxes.length; i++) {
        var control = this.TextBox(null, textBoxes[i][2]);
        control.setAttribute("type", "password");
        form.addControlRow(textBoxes[i][1], textBoxes[i][0], control, "4px 4px 4px 30px");
    }
    
    this.options.changePasswordForm = form;

    form.show = function () {
        form.buttonSave.setEnabled(true);
        this.changeVisibleState(true);
        form.controls.currentPassword.value = "";
        form.controls.newPassword.value = "";
        form.controls.currentPassword.focus();
    }

    form.action = function () {
        if (form.controls.currentPassword.checkEmpty(this_.loc.Notices.AuthPasswordIsNotSpecified) &&
            form.controls.newPassword.checkEmpty(this_.loc.Notices.AuthPasswordIsNotSpecified) &&
            form.controls.newPassword.checkLength(6, this_.loc.Notices.AuthPasswordIsTooShort)) {
            var params = {};
            form.buttonSave.setEnabled(false);
            this_.SendCloudCommand("UserChangePassword", { CurrentPassword: form.controls.currentPassword.value, NewPassword: form.controls.newPassword.value }, function () {
                form.changeVisibleState(false);
            }, function (data, msg) {
                form.buttonSave.setEnabled(true);
                this_.ShowMessagesTooltip(msg, form.controls.currentPassword);
            });
        }
    }
}