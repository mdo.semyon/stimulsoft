
StiMobileDesigner.prototype.OverrideToolbar = function () {    
    var toolBar = this.options.toolBar;
    var toolButoonCellClass = "stiDesignerToolButtonCell";
    var toolBarTable = toolBar.firstChild;
    if (this.options.buttons.aboutButton) this.options.buttons.aboutButton.style.display = "none";
    if (this.options.buttons.showToolBarButton) this.options.buttons.showToolBarButton.style.display = "none";

    //LogIn
    var loginButton = this.LoginToolButton("login", this.loc.Authorization.ButtonLogin, null, 30, 35);
    loginButton.allwaysEnabled = true;
    loginButton.caption.style.textAlign = "center";
    loginButton.caption.style.padding = "0 8px 0 8px";
    toolBarTable.addCell(loginButton).style.verticalAlign = "bottom";
    toolBar.loginButton = loginButton;

    loginButton.action = function () {
        this.jsObject.options.forms.authForm.show();
    }

    //UserName
    var userNameButton = this.LoginToolButton("userName", "", null, 30, 35);
    userNameButton.allwaysEnabled = true;
    userNameButton.style.webkitAppRegion = "no-drag";
    userNameButton.style.display = "none";
    toolBarTable.addCell(userNameButton).style.verticalAlign = "bottom";
    toolBar.userNameButton = userNameButton;
        
    var userNameTable = this.CreateHTMLTable();
    userNameTable.style.webkitAppRegion = "no-drag";
    userNameButton.nameCell = userNameTable.addCell();
    userNameButton.nameCell.className = "stiCloudReportsToolbarUserNameCell";
    userNameButton.caption.appendChild(userNameTable);
    userNameButton.userImageCell = userNameButton.innerTable.addCell();
    userNameButton.userImageCell.style.lineHeight = "0";

    this.InitializeUserMenu();
}

StiMobileDesigner.prototype.LoginToolButton = function (name, caption, imageName, height, width) {
    var button = this.SmallButton(name, null, caption, imageName, null, null, this.GetStyles("LoginToolButton"));
    button.innerTable.style.width = "100%";
    button.style.cursor = "default";
    if (height) button.style.height = height + "px";
    if (width) button.style.minWidth = width + "px";
    button.style.marginLeft = "4px";

    return button;
}