﻿
StiMobileDesigner.prototype.TextBoxWithHintText = function (name, width, hintText, toolTip, img) {
    var this_ = this;
	var table = this.CreateHTMLTable();
	
	var getPath = function (fileName) {
	    return this_.options.images["LoginControls.LogIn." + this_.GetThemeColor() + "." + fileName];
    }
	
    var textBox = this.TextBox(name, width, toolTip);
    textBox.setAttribute("placeholder", hintText);
    textBox.style.backgroundColor = "#f2f1f1";
    textBox.style.border = "none";
    textBox.style.width = "280px";
    textBox.style.height = "30px";
    var img_ = $("<img src='" + getPath(img) + "' style='vertical-align: middle;opacity: 0.8'></img>")[0];
    var iim = table.addCell(img_);
    iim.style.lineHeight = "0";
    iim.style.backgroundColor = "#f2f1f1";
    iim.fileName = img;
    iim.style.width = "30px";
    iim.style.textAlign = "center";
    table.addCell(textBox);
    textBox.table = table;
        
    //Override
    textBox.onkeypress = function (event) {
        if (this.readOnly) return false;
        if (event && event.keyCode == 13) {
            this.blur();
            if (!this.readOnly) this.action();
            return false;
        }
    }

    textBox.onblur = function () {
        this.isFocused = false;
        this.setSelected(false);
        this.jsObject.options.controlsIsFocused = false;
    }

    $(this).on("eventTheme", function () {
        img_.src = getPath(iim.fileName);
    });

    return textBox;
}