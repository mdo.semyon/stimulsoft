﻿
StiMobileDesigner.prototype.BaseFormPanel2 = function (wizardButtonProps, formButtonProps, autoHeight, caption) {

    var form = this.BaseForm(null, caption, 1);
    form.buttonOk.parentNode.removeChild(form.buttonOk);
    form.buttonCancel.parentNode.removeChild(form.buttonCancel);
    if (!autoHeight) {
        autoHeight = "max-height";
    }
    form.rhomb = $("<div class='stiCloudReportsWebClientFormArrow'></div>")[0];
    form.appendChild(form.rhomb);
    this.options.paintPanel.appendChild(form);
    form.formButtons = {};
    form.wizardButtons = [];
    form.style.display = "none";
    form.id = form.name;
    form.visible = false;
    form.isEnabled = true;
    form.className = "stiCloudReportsWebClientFormPanel";

    form.disabledPanel = document.createElement("div");
    form.disabledPanel.className = "stiCloudReportsWebClientFormPanelDisabled";
    form.header.className = "stiCloudReportsFormHeader";
    form.topSeparator = $("<div class='stiCloudReportsWebClientFormSeparator'></div>")[0];
    $(form.container).before($(form.topSeparator));
    form.caption.style.padding = "13px 10px 10px 8px";
    form.autoHeight = autoHeight;
    if (autoHeight) {
        $(window).resize(function () {
            form.updateHeight();
        });        
    }

    if (arguments[0] != null) {
        form.caption.innerHTML = "";
    }
    for (var i = 0; i < arguments.length; i++) {
        if (i > 1) continue;
        if (arguments[i] && arguments[i].length) {
            for (var k = 0; k < arguments[i].length; k++) {
                if (arguments[i][k][0].indexOf("separator") == 0 || (i == 0 && k > 0)) {
                    var sep = this.HeaderFormButtonsSeparator();
                    if (i == 0) {
                        form.caption.appendChild(sep);
                        form.formButtons["separator" + k] = sep;
                    } else {
                        form.buttonsTable.addCell(sep);
                        form.formButtons[arguments[i][k][0]] = sep;
                        continue;
                    }
                }
                var button = null;
                if (i == 0) {
                    button = this.HeaderButton(arguments[i][k][0], arguments[i][k][1]);
                    form.caption.appendChild(button);
                    if (arguments[i][k].length > 2) button.helpUrlKey = arguments[i][k][2];
                } else {
                    button = this.FormButton(null, arguments[i][k][0], arguments[i][k][1]);
                    if (arguments[i][k][2] && arguments[i][k][2] == "left") {
                        if (!form.leftPanel) {
                            form.leftPanel = $("<div style='float:left;padding: 3px'>")[0];
                            $(form.floatPanel).prepend(form.leftPanel);
                        }
                        form.leftPanel.appendChild(button);
                    } else {
                        form.buttonsTable.addCell(button);
                    }
                    button.style.margin = "5px";
                }
                if (i == 0) form.wizardButtons.push(button);
                else form.formButtons[button.name] = button;
            }
        }
    }
    
    //Button Methods
    if (form.formButtons.ok) form.formButtons.ok.action = function () { form.action(); };
    if (form.formButtons.cancel) form.formButtons.cancel.action = function () { form.changeVisibleState(false); form.oncancel(); }
    
    //Form Methods
    form.getDownFormPanel = function (formPanels) {
        var formPanels = formPanels || form.jsObject.options.paintPanel.getFormPanels();
        var downFormPanel = null;

        for (var pName in formPanels) {
            if (formPanels[pName] != form) {
                downFormPanel = formPanels[pName]
            }
            else {
                return downFormPanel;
            }
        }
        return downFormPanel;
    }

    form.updateHeight = function () {
        if (this.autoHeight) {
            $(form.container).css(this.autoHeight, ($(form.jsObject.options.paintPanel).height() - $(form.header).height() - $(form.buttonsPanel).height() - 25) + "px");
        }
    }

    form.changeVisibleState = function (state, onapply, notAnimate) {
        if (state && form.jsObject.options.currentTree == form.jsObject.options.mainTree) form.jsObject.options.treePanel.minimize();

        var formPanels = form.jsObject.options.paintPanel.getFormPanels();
        var downForm = form.getDownFormPanel(formPanels);
        var leftMargin = this.jsObject.options.treePanel.offsetWidth || this.jsObject.options.usersContainer.offsetWidth || this.jsObject.options.systemContainer.offsetWidth;
        var finishLeftPos = (state) ? form.minLeft + leftMargin : $(form).position().left + 200;
        var duration = state ? 350 : 150;
        var opacity = state ? 1 : 0;

        if (state) { 
            form.jsObject.options.infoPanel.hide();
            form.style.left = (leftMargin + 200) + "px";
            form.style.top = form.minTop + "px";
            form.style.display = "";
            form.style.opacity = 0;
            form.onshow();
            if (form.controls && form.controls.labelName) { form.controls.labelName.focus(); }
            form.container.style.top = form.header.offsetHeight + "px";
            form.updateHeight();
            if (downForm) downForm.setEnabled(false);
            var completeFunction = function () {     
                form.visible = true;
            }
        }
        else {
            if (!form.visible) return;
            form.visible = false;
            if (downForm) downForm.setEnabled(true);
            var completeFunction = function () {
                form.onhide(onapply);
                this.jsObject.options.paintPanel.removeChild(form);
                if (form.jsObject.options.paintPanel.getFormPanels().length == 0 && this.jsObject.options.currentTree) {
                    var selectedItem = form.jsObject.options.currentTree.getSelectedItem();
                    if (selectedItem) selectedItem.setSelected(null, true);
                    if (form.jsObject.options.currentTree == form.jsObject.options.mainTree) form.jsObject.options.treePanel.maximize();
                }
            }
        }
        if (!notAnimate) {
            $(form).animate({ left: finishLeftPos, opacity: opacity }, { complete: completeFunction, duration: duration });
        }
    }

    form.focusForm = function () {
        var left = this.style.left;
        var top = this.style.top;
        this.changeVisibleState(true, null, true);
        this.style.opacity = 1;
        this.style.left = left;
        this.style.top = top;
        this.setEnabled(true);
        var parent = this.parentNode;
        if (parent) {
            parent.removeChild(this);
            parent.appendChild(this);
        } else {
            this.jsObject.options.paintPanel.appendChild(this);
            this.changeVisibleState(true);
        }
    }

    form.changeVisibleStateHeaderAndStatusBar = function (state) {
        this.header.style.visibility = state ? "visible" : "hidden";
        this.container.style.visibility = state ? "visible" : "hidden";
    }

    form.setEnabled = function (value) {
        if (this.isEnabled != value) {
            this.isEnabled = value;
            if (value) {
                this.removeChild(this.disabledPanel);
            } else {
                this.disabledPanel.style.width = (this.offsetWidth - 2) + "px";
                this.disabledPanel.style.height = $(this).height() + "px";
                this.disabledPanel.style.top = 0;
                this.appendChild(this.disabledPanel);
            }
        }
    }

    form.action = function () { };
    form.onshow = function () { };
    form.onhide = function () { };
    form.oncancel = function () { };

    form.onkeyup = function (e) {
        if (e) {
            if (e.keyCode == 27) {
                form.changeVisibleState(false);
                form.oncancel();
            }
        }
    };

    return form;
}

StiMobileDesigner.prototype.BaseFormPanelForEditItems = function (wizardButtonProps, formButtonProps, autoHeight, caption) {
    var form = this.BaseFormPanel2(wizardButtonProps, formButtonProps, autoHeight, caption);
    form.controls = {};
    form.mode = "create"; //mode : "edit", "create"

    form.addControlRow = function (table, textControl, controlName, control, margin, textClass, textTooltip, customTooltip) {
        this.controls[controlName] = control;
        this.controls[controlName + "Row"] = table.addRow();

        if (textControl != null) {
            var text = table.addCellInLastRow();
            this.controls[controlName + "Text"] = text;
            text.innerHTML = textControl;
            text.className = textClass || this.textClass || "stiCloudReportsWebClientTextBeforeControl";
            if (textTooltip || customTooltip) {
                this.jsObject.appendTooltipInfo(text, null, textTooltip, this.jsObject, null, customTooltip);
            }            
        }

        if (control) {
            control.form = this;
            control.style.margin = margin;
            var controlCell = table.addCellInLastRow(control);
            if (textControl == null) controlCell.setAttribute("colspan", 2);
        }

        return controlCell;
    }

    form.superhide = function () { };

    form.onhide = function () {
        var removingContainers = [];
        for (var contName in this.jsObject.options.containers)
            if (contName.indexOf(this.name) == 0)
                delete this.jsObject.options.containers[contName];
        form.superhide();
    }
    

    //Name & Description
    var labelsTable = this.CreateHTMLTable();
    form.controls.labelsTable = labelsTable;
    form.container.appendChild(labelsTable);    
    var textBoxes = [
        ["labelName", this.loc.Common.LabelName, 250],
        ["labelDescription", this.loc.Common.LabelDescription, 420]
    ]
    for (var i = 0; i < textBoxes.length; i++) {
        var control = this.TextBox(null, textBoxes[i][2]);
        form.addControlRow(labelsTable, textBoxes[i][1], textBoxes[i][0], control, "4px 0px 4px 30px");
    }

    form.sendCommand = function (ident) {
        var param = {};
        param.Items = [form[ident]];
        param.AllowSignalsReturn = true;

        if (form.mode == "create") {
            param.Items[0].Key = form.jsObject.generateKey();
            if (form.folderKey) param.Items[0].FolderKey = form.folderKey;
        }
        
        form.jsObject.options.mainTree.wasClicked = false;
        form.changeVisibleState(false);
        form.jsObject.SendCloudCommand("ItemSave", param, function (data) {
            form.jsObject.options.controller.processSignals(data);
        });
    }

    form.checkNameField = function () {
        if (form.controls.labelName)
            if (!form.controls.labelName.checkEmpty(form.jsObject.loc.Messages.ThisFieldIsNotSpecified)) return false;

        return true;
    }

    return form;
}

StiMobileDesigner.prototype.EasyBaseFormPanel = function (autoHeight, wizardButtonProps, caption) {
    var formButtonProps = [
        ["ok", this.loc.Common.ButtonOK],
        ["cancel", this.loc.Common.ButtonCancel]
    ];
    var form = this.BaseFormPanelForEditItems(wizardButtonProps, formButtonProps, autoHeight, caption);

    return form;
}

StiMobileDesigner.prototype.MultiWizardBaseFormPanel = function (wizardButtonProps, autoHeight, formButtons) {
    var formButtonProps = [
        ["ok", this.loc.Common.ButtonOK],
        ["cancel", this.loc.Common.ButtonCancel]
    ];
    var form = this.BaseFormPanelForEditItems(wizardButtonProps, formButtons ? formButtons : formButtonProps, autoHeight);
    form.container.removeChild(form.controls.labelsTable);

    form.currentPanelIndex = 0;
    form.wizardPanels = {};
    for (var i = 0; i < form.wizardButtons.length; i++) {
        var wizardButton = form.wizardButtons[i];
        wizardButton.action = function () { form.showPanel(this.name); }
        var wizardPanel = document.createElement("div");
        wizardPanel.wizardButton = wizardButton;
        wizardPanel.name = wizardButton.name;
        form.wizardPanels[wizardButton.name] = wizardPanel;
        form.container.appendChild(wizardPanel);
    }

    form.showPanel = function (namePanel) {
        for (var i = 0; i < this.wizardButtons.length; i++) {
            var wizardButton = this.wizardButtons[i];
            if (wizardButton.isEnabled) wizardButton.setSelected(namePanel == wizardButton.name);
            this.wizardPanels[wizardButton.name].style.display = namePanel == wizardButton.name ? "" : "none";
            if (namePanel == wizardButton.name) {
                if (this.wizardPanels[wizardButton.name]["onshow"]) this.wizardPanels[wizardButton.name].onshow();
                this.currentPanelIndex = parseInt(i);
                if (wizardButton.helpUrlKey) form.helpUrlKey = wizardButton.helpUrlKey;
            }
        }
        this.updateHeaderControls();
    }

    form.updateHeaderControls = function () {
        this.wizardButtons[0].style.display = this.mode == "edit" ? "none" : "";
        if (this.mode == "edit" && this.formButtons["separator1"]) this.formButtons["separator1"].style.display = "none";
        if (this.formButtons.ok) {
            this.formButtons.ok.setEnabled(this.mode != "startWizard");
        }
    }

    form.showControlsByMode = function (mode) {
        this.mode = mode;        
        for (i = 1; i < this.wizardButtons.length; i++) {
            this.wizardButtons[i].setEnabled(this.mode != "startWizard");
        }
        this.showPanel(this.wizardButtons[mode == "startWizard" ? 0 : 1].name);
    }

    return form;
}

StiMobileDesigner.prototype.SimpleWizardBaseFormPanel = function (autoHeight) {
    var formButtonProps = [
        ["back", this.loc.Common.ButtonBack, "left"],
        ["ok", this.loc.Common.ButtonOK],
        ["cancel", this.loc.Common.ButtonCancel]
    ];
    var form = this.BaseFormPanelForEditItems(null, formButtonProps, autoHeight);

    form.onshow = function () {
        form.formButtons.back.style.display = (form.mode == "edit") ? "none" : "";
    }

    return form;
}