﻿
StiMobileDesigner.prototype.HeaderButton = function (name, caption, height) {
    var headerButton = this.SmallButton(name, null, caption, null, null, null, this.GetStyles("HeaderButton"));
    if (height != null) headerButton.style.height = this.options.isTouchDevice ? (height + 5) + "px" : height + "px";

    return headerButton;
}

StiMobileDesigner.prototype.HeaderFormButtonsSeparator = function () {
    var separator = document.createElement("div");
    separator.className = "stiCloudReportsWebClientHeaderFormButtonsSeparator";

    return separator;
}