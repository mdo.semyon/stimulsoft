
//StiMobileDesigner.prototype.SaveLicenseKey = function (licenceKey) {
//    this.SetCookie("StiLicenseKey", licenceKey);
//}

//StiMobileDesigner.prototype.LoadLicenseKey = function () {
//    return this.GetCookie("StiLicenseKey");
//}

//StiMobileDesigner.prototype.RemoveLicenseKey = function () {
//    this.SetCookie("StiLicenseKey", "");
//}

//StiMobileDesigner.prototype.SaveAccountSettings = function (userInfo) {
//    var accountSettings = {
//        FirstName: userInfo.FirstName,
//        LastName: userInfo.LastName,
//        UserName: userInfo.UserName,
//        UserKey: this.options.UserKey,
//        SessionKey: this.options.SessionKey,
//        SessionDate: this.DateToJSONDateFormat(new Date())
//    };

//    if (!this.options.licenseKey) {
//        this.SetCookie("StiAccountSettings", Stimulsoft.Base.StiEncryption.encryptS(JSON.stringify(accountSettings), this.options.encriptKey));
//    }
//    else {
//        this.options.accountSettings = accountSettings;
//    }
//}

//StiMobileDesigner.prototype.RemoveAccountSettings = function () {
//    this.SetCookie("StiAccountSettings", "");
//}

//StiMobileDesigner.prototype.LoadAccountSettings = function () {
//    var accountSettingsStr = this.GetCookie("StiAccountSettings");
//    if (accountSettingsStr) 
//        return JSON.parse(Stimulsoft.Base.StiEncryption.decryptS(accountSettingsStr, this.options.encriptKey));
//}

//StiMobileDesigner.prototype.SaveThemeSettings = function (themeSettings) {
//    this.SetCookie("StiThemeSettings", JSON.stringify(themeSettings));
//}

//StiMobileDesigner.prototype.LoadThemeSettings = function () {
//    var themeSettings = {
//        Theme: "Office2013White",
//        Style: "Blue"
//    };

//    if (this.GetCookie("StiThemeSettings")) {
//        themeSettings = JSON.parse(this.GetCookie("StiThemeSettings"));
//    }

//    return themeSettings;
//}

//StiMobileDesigner.prototype.CheckActivation = function () {
//    var licenseKey = this.options.licenseKey || this.LoadLicenseKey();
//    if (licenseKey) {
//        Stimulsoft.Base.StiLicense.loadFromString(licenseKey);

//        if (Stimulsoft.Base.StiLicense.licenseKey) {
//            this.SetUserSettings(Stimulsoft.Base.StiLicense.licenseKey);
//        }
//    }

//    //Update designer header
//    var reportName = null;

//    if (this.options.report) {
//        var reportFile = this.options.report.properties.reportFile;
//        if (reportFile != null) reportFile = reportFile.substring(reportFile.lastIndexOf("/")).substring(reportFile.lastIndexOf("\\"));
//        var reportName = reportFile || Base64.decode(this.options.report.properties.reportName.replace("Base64Code;", ""));
//    }

//    this.SetWindowTitle(reportName ? reportName + " - " + this.loc.FormDesigner.title : this.loc.FormDesigner.title);
//    this.UpdateTrialWatermarksOnPages();
//}

//StiMobileDesigner.prototype.SetUserSettings = function (licenseKey) {
//    var accountSettings = this.options.accountSettings || this.LoadAccountSettings();
//    if (accountSettings) {
//        this.options.user = {
//            FirstName: accountSettings.FirstName,
//            LastName: accountSettings.LastName,
//            UserName: accountSettings.UserName,
//            Key: accountSettings.UserKey
//        }

//        this.options.UserName = accountSettings.UserName;
//        this.options.UserKey = accountSettings.UserKey;
//        this.options.SessionKey = accountSettings.SessionKey;
//        this.UpdateUserNameButton(this.options.user);
//        this.StartNewSession();
//    }
//    else if (licenseKey && !this.options.SessionKey) {
//        var firstName = "";
//        var lastName = "";

//        if (licenseKey.owner) {
//            var ownerArray = licenseKey.owner.split(" ");
//            firstName = ownerArray[0].trim();
//            if (ownerArray.length > 1) lastName = ownerArray[1].trim();
//        }

//        var userInfo = {
//            Key: this.generateKey(),
//            FirstName: firstName,
//            LastName: lastName,
//            UserName: licenseKey.userName
//        }

//        this.UpdateUserNameButton(userInfo);
//        this.StartNewSession();
//    }
//}

StiMobileDesigner.prototype.UpdateTrialWatermarksOnPages = function () {
    if (this.options.report) {
        for (var pageName in this.options.report.pages) {
            var page = this.options.report.pages[pageName];
            if (page.controls.waterMarkCharsParent) {
                page.removeChild(page.controls.waterMarkCharsParent);
                page.controls.waterMarkCharsParent = null;
            }
            if (this.options.productVersion == this.options.productVersion.trim()) {
                this.CreatePageWaterMarkChars(page);
                this.RepaintWaterMarkChars(page);
            }
        }
    }
}

StiMobileDesigner.prototype.UpdateWindowTitle = function () {
    if (document.title) {
        document.title = document.title.replace(" " + this.getT(), "");
        if (this.options.productVersion == this.options.productVersion.trim()) document.title += " " + this.getT();
    }
}