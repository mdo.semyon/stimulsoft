StiMobileDesigner.prototype.InitializeAuthForm = function () {
    var authForm = this.BaseForm("authForm", this.loc.Authorization.WindowTitleLogin, 2, null, true);
    authForm.container.style.overflowX = "hidden";
    authForm.container.style.overflowY = "hidden";    
    authForm.onkeyup = null;
    authForm.controls = {};
    authForm.formWidth = 380;
    authForm.style.width = authForm.formWidth + "px";
    authForm.style.backgroundColor = "white";
    authForm.buttonsPanel.style.display = "none";
    authForm.buttonsSeparator.style.display = "none";
    authForm.showCaptions = false;
    authForm.mode = "login";
    authForm.result = {};
    authForm.className = "stiCloudReportsWebClientForm";
    authForm.container.className = "stiCloudReportsWebClientFormContainer";
    authForm.header.className = "stiCloudReportsWebClientFormHeader";
    authForm.caption.className = "stiDesignerLoginFormCaption";

    authForm.container.style.padding = "0px";
    authForm.caption.style.padding = "45px 133px 45px 33px";    
    var imgLogo = document.createElement("img");
    imgLogo.style.position = "absolute";
    imgLogo.style.top = "50px";
    imgLogo.style.right = "42px";
    imgLogo.src = this.options.images["LoginControls.LogoStimulsoftReports.png"];
    authForm.header.style.position = "relative";
    authForm.header.appendChild(imgLogo);

    //Add Panels
    authForm.this_ = this;
    authForm.panels = {};
    authForm.panels.login = this.AuthFormLoginPanel(authForm);
    authForm.panels.signUp = this.AuthFormSignUpPanel(authForm);
    authForm.panels.forgotPassword = this.AuthFormForgotPasswordPanel(authForm);
    authForm.panels.registerSuccessfully = this.AuthFormRegisterSuccessfullyPanel(authForm);
    authForm.panels.resetPassword = this.AuthFormResetPasswordPanel(authForm);
    authForm.panels.wheelPanel = this.AddWheelPanel();
    for (var pName in authForm.panels)
        authForm.container.appendChild(authForm.panels[pName]);

    authForm.onhide = function () { }

    authForm.cancelAction = function () {
        if (this.jsObject.options.forms.shareForm && this.jsObject.options.forms.shareForm.visible) {
            this.jsObject.options.forms.shareForm.changeVisibleState(false);
        }
    }

    authForm.onshow = function () {
        authForm.controls.buttonSignUp.setEnabled(false);
        authForm.controls.agreeCheckbox.setChecked(false);
        authForm.controls.buttonSignUp.agreeTermsAndPolicy = false;
        authForm.controls.buttonSignUp.completeCaptcha = false;

        if (window["captchaId"] != null && window["grecaptcha"] != null) {
            grecaptcha.reset(window["captchaId"]);
        }

        if (authForm.jsObject.options.previewMode) {
            authForm.jsObject.options.workPanel.showPanel(authForm.jsObject.options.homePanel);
            authForm.jsObject.options.buttons.homeToolButton.setSelected(true);
        }
    }

    authForm.changeMode = function (mode) {
        var changedFormSizes = this.mode != mode;

        this.mode = mode;
        switch (this.mode) {
            case "login":
                this.caption.innerHTML = this.jsObject.loc.Authorization.WindowTitleLogin;
                break;
            case "signUp":
            case "registerSuccessfully":
                this.caption.innerHTML = this.jsObject.loc.Authorization.WindowTitleSignUp;
                break;
            case "forgotPassword":
                if (authForm.controls.resetInfoText) {
                    authForm.controls.resetInfoText.parentNode.removeChild(authForm.controls.resetInfoText);
                }
                this.caption.innerHTML = this.jsObject.loc.Authorization.WindowTitleForgotPassword;
                break;
            case "resetPassword":
                this.caption.innerHTML = this.jsObject.loc.Authorization.WindowTitleForgotPassword;
                break;
        }
        
        if (this.caption.innerHTML) this.caption.innerHTML = this.caption.innerHTML.toUpperCase();
        
        for (var name in this.panels) {
            this.panels[name].style.display = name == mode ? "" : "none";
        }

        this.focusFirstControl();

        if (changedFormSizes && !this.movedByUser) this.jsObject.SetObjectToCenter(this);
    }

    authForm.focusFirstControl = function () {
        if (!this.jsObject.isTouchDevice) {
            if (this.mode == "login") { this.controls.loginUserName.focus(); return; }
            if (this.mode == "signUp") { this.controls.signUpFirstName.focus(); return; }
            if (this.mode == "forgotPassword") { this.controls.forgotPasswordUserName.focus(); return; }
            if (this.mode == "resetPassword") { this.controls.buttonSetPassword.focus(); return; }
        }
    }

    authForm.resetControls = function () {
        for (var name in this.controls) {
            if ("value" in this.controls[name] && this.controls[name] != this.controls.loginUserName) this.controls[name].value = "";
        }
    }

    authForm.hideAllProgressMarkers = function () {
        var buttons = ["buttonLogin", "buttonResetPassword", "buttonSignUp"];
        for (var i = 0; i < buttons.length; i++) {
            this.controls[buttons[i]].progressMarker.changeVisibleState(false);
        }
    }

    authForm.show = function (chh) {
        //Hide forms
        if (this.jsObject.options.forms.changePasswordForm) this.jsObject.options.forms.changePasswordForm.changeVisibleState(false);
        if (this.jsObject.options.forms.settingsForm) this.jsObject.options.forms.settingsForm.changeVisibleState(false);
        if (this.jsObject.options.forms.updateForm) this.jsObject.options.forms.updateForm.changeVisibleState(false);

        this.jsObject.options.logins = [];
        try {
            this.jsObject.options.logins = eval(this.jsObject.GetCookie("logins")) || [];
        }
        catch (e) { };
                
        this.resetControls();
        this.hideAllProgressMarkers();

        if (!chh) {
            this.changeMode("login");
        }
        
        this.changeVisibleState(true);
        this.focusFirstControl();
    }

    authForm.start = function () {
        authForm.show(true);
    }

    authForm.startLogin = function (userName, password, loginRememberMe, md5) {
        var this_ = this;
        
        var result = {};
        result.UserName = userName;
        result.Password = password;
        result.Device = { Ident: "WebDevice" };
        this.controls.buttonLogin.progressMarker.changeVisibleState(true);
        result.Localization = this_.jsObject.options.defaultLocalization;

        result.Ident = "UserLogin";
        if (md5) {
            result.md5 = md5;
        }
        result.LoginAndActivateLicense = true;

        var saveCookies = loginRememberMe || this_.controls.rememberMe.checked;
        this_.jsObject.SetCookie("loginRememberMe", saveCookies);

        this_.jsObject.SendCloudCommand("UserLogin", result,
                function (data) {
                    this_.hideAllProgressMarkers();

                    this_.jsObject.options.cloudParameters.sessionKey = data.ResultSessionKey;
                    this_.jsObject.options.cloudParameters.userKey = data.ResultUserKey;
                    var saveCookies = loginRememberMe || this_.controls.rememberMe.checked;
                    if (saveCookies && this_.jsObject.options.logins.indexOf(userName) < 0) {
                        this_.jsObject.options.logins.push(userName);
                    } else if (!saveCookies && this_.jsObject.options.logins.indexOf(userName) >= 0) {
                        this_.jsObject.options.logins.splice(this_.jsObject.options.logins.indexOf(userName), 1);
                    }                    
                    if (saveCookies) {
                        var expDate = new Date(new Date().getTime() + 2592000000);
                        this_.jsObject.SetCookie("sti_SessionKey", this_.jsObject.options.cloudParameters.sessionKey, this_.jsObject.options.cookiesPath, this_.jsObject.options.cookiesDomain, null, expDate.toUTCString());
                        
                        if (this_.jsObject.options.cloudParameters.userKey) {
                            this_.jsObject.SetCookie("sti_UserKey", this_.jsObject.options.cloudParameters.userKey, this_.jsObject.options.cookiesPath, this_.jsObject.options.cookiesDomain, null, expDate.toUTCString());
                        }
                    }
                    this_.jsObject.setUserInfo();
                    this_.changeVisibleState(false);
                    this_.ready = true;

                    //Build tree after login
                    var forms = this_.jsObject.options.forms;
                    if (forms.onlineOpenReport && forms.onlineOpenReport.visible) {
                        forms.onlineOpenReport.setToTreeMode();
                    }
                    if (forms.onlineSaveAsReport && forms.onlineSaveAsReport.visible) {
                        forms.onlineSaveAsReport.setToTreeMode();
                    }
                    if (forms.shareForm && forms.shareForm.visible) {
                        forms.shareForm.checkReportSavingToCloud();
                    }
            },
            function (data, msg) {
                this_.ready = true;
                this_.hideAllProgressMarkers();
                if (data && data.ResultNotice && data.ResultNotice.Ident == "AuthAccountIsNotActivated") {
                    var ttx = this_.jsObject.loc.Authorization.TextRegistrationSuccessfully.replace("{0}", this_.controls.loginUserName.value);
                    ttx = ttx.substring(ttx.indexOf("\n") + 1);
                    this_.controls.registerSuccessfullyText.innerHTML = ttx;
                    this_.changeMode("registerSuccessfully");
                } else {
                    if (this_.visible) this_.jsObject.ShowMessagesTooltip(this_.jsObject.formatResultMsg(data), this_.controls.loginUserName);
                }
            });
    }

    authForm.apply = function () {
        var result = {};
        result.typeResult = this.mode;
        var this_ = this;
        switch (this.mode) {
            case "login": {
                if (this.controls.loginUserName.checkEmpty(this.jsObject.loc.Messages.ThisFieldIsNotSpecified) &&
                    this.controls.loginPassword.checkEmpty(this.jsObject.loc.Messages.ThisFieldIsNotSpecified)) {
                        this.startLogin(this.controls.loginUserName.value, this.controls.loginPassword.value);
                }
                break;
            }
            case "signUp": {
                if (this.controls.signUpFirstName.checkEmpty(this.jsObject.loc.Notices.AuthFirstNameIsNotSpecified, null, null, true) &&
                    this.controls.signUpLastName.checkEmpty(this.jsObject.loc.Notices.AuthLastNameIsNotSpecified, null, null, true) &&
                    this.controls.signUpUserName.checkEmpty(this.jsObject.loc.Notices.AuthUserNameIsNotSpecified, null, null, true) &&
                    this.controls.signUpUserName.checkEmail(this.jsObject.loc.Notices.AuthUserNameShouldLookLikeAnEmailAddress) &&
                    this.controls.signUpPassword.checkEmpty(this.jsObject.loc.Notices.AuthPasswordIsNotSpecified, null, null, true) &&
                    this.controls.signUpPassword.checkLength(6, this.jsObject.loc.Notices.AuthPasswordIsTooShort)) {
                    this.controls.buttonSignUp.progressMarker.changeVisibleState(true);
                    result.FirstName = this.controls.signUpFirstName.value;
                    result.LastName = this.controls.signUpLastName.value;
                    result.UserName = this.controls.signUpUserName.value;
                    result.Password = this.controls.signUpPassword.value;
                    result.Localization = this.jsObject.options.defaultLocalization;
                    result.Module = "Reports";
                    this.jsObject.SendCloudCommand("UserSignUp", result,                        
                        function (data) {
                            if (data.ResultActivationByEmail) {
                                this_.controls.registerSuccessfullyText.innerHTML = this_.jsObject.loc.Authorization.TextRegistrationSuccessfully.replace("{0}", this_.controls.signUpUserName.value);
                                this_.controls.loginUserName.value = result.UserName;
                                this_.controls.loginPassword.value = result.Password;
                                this_.changeMode("registerSuccessfully");
                            }
                            else {
                                this_.changeMode("login");
                                this_.controls.rememberMe.checked = true;
                                this_.startLogin(result.UserName, result.Password);
                            }
                        },
                        function (data, msg) {
                            this_.jsObject.ShowMessagesTooltip(msg, this_.controls.signUpUserName);
                            this_.hideAllProgressMarkers();
                        });

                }
                break;
            }
            case "forgotPassword": {
                if (this.controls.forgotPasswordUserName.checkEmpty(this.jsObject.loc.Messages.ThisFieldIsNotSpecified) &&
                    this.controls.forgotPasswordUserName.checkEmail(this.jsObject.loc.Notices.AuthUserNameShouldLookLikeAnEmailAddress)) {
                    result.UserName = this.controls.forgotPasswordUserName.value;
                    this.controls.buttonResetPassword.progressMarker.changeVisibleState(true);
                    this.jsObject.SendCloudCommand("UserResetPassword", result, function (data) {
                        this_.hideAllProgressMarkers();
                        if (this_.controls.resetInfoText) {
                            this_.controls.resetInfoText.remove();                            
                        }

                        var info = $("<div style='text-align:left; white-space:normal; width:230px;overflow:hidden;' class='stiCloudReportsWebClientAuthFormTextCell'>" + this_.jsObject.formatString(this_.jsObject.loc.Notices.AuthSendMessageWithInstructions, result.UserName) + "</div>");
                        $(this_.controls.buttonResetPassword).after(info);

                        var hh = info.height();
                        info.height(0);
                        info.animate({ height: hh });
                        this_.controls.resetInfoText = info;
                    }, function (data, msg) {
                        this_.jsObject.ShowMessagesTooltip(msg, this_.controls.forgotPasswordUserName);
                        this_.hideAllProgressMarkers();
                    });
                }
                break;
            }
        }

        if (result["userName"] != null && result["userName"] == "")
            return false;
        else
            return result;
    }

    authForm.action = function () {
        this.apply();
    }

    var this_ = this;
    if (window.location.search.indexOf("reset=") >= 0) {
        authForm.resetId = window.location.search.substring(window.location.search.indexOf("reset=") + 6);
        authForm.changeMode("resetPassword");
        authForm.ready = true;
    }
    else {
        var currentSession = this_.GetCookie("sti_SessionKey");
        var userKey = this_.GetCookie("sti_UserKey");

        if (!this_.options.cloudParameters.sessionKey && currentSession) {
            this_.options.cloudParameters.sessionKey = currentSession;
        }

        if (!this_.options.cloudParameters.userKey && userKey) {
            this_.options.cloudParameters.userKey = userKey;
        }

        if (this_.options.cloudParameters.sessionKey && this_.options.cloudParameters.userKey) {
            //var sessionDateStr = this_.GetCookie("sti_SessionDate");
            //var sessionDate = sessionDateStr ? this_.JSONDateFormatToDate(sessionDateStr) : null;
            //var deltaDays = sessionDate ? Math.floor(((new Date()).getTime() - new Date(sessionDate).getTime()) / 1000 / 60 / 60 / 24) : null;

            authForm.changeVisibleState(false);
            authForm.ready = true;
            
            this_.setUserInfo();
        }
        else {
            authForm.changeMode("login");
            authForm.ready = true;
        }
    }

    //Initialize captcha
    document.write("<script src='https://www.google.com/recaptcha/api.js?onload=captchaLoadedComplete&render=explicit'></script>");
}

StiMobileDesigner.prototype.AuthFormLoginPanel = function (authForm) {
    var loginPanel = document.createElement("div");
    var mTable = this.CreateHTMLTable();
    var controlsTable = this.CreateHTMLTable();
    controlsTable.style.margin = "0 auto 0 auto";
    loginPanel.appendChild(mTable);
    mTable.style.width = "100%";
    var ct = mTable.addCell(controlsTable);
    ct.style.width = "50%";
    var loginRememberMe = true;//this.GetCookie("loginRememberMe") === 'true';
    var rDiv = $("<div style='width: " + authForm.formWidth + "px;margin:0 auto;border-top:'></div>")[0];
    var ds = mTable.addCellInNextRow(rDiv);
    ds.style.textAlign = "center";
    ds.colSpan = 2;

    //UserName
    if (authForm.showCaptions) {
        authForm.loginCaption = controlsTable.addCell();
        authForm.loginCaption.className = "stiCloudReportsWebClientAuthFormTextCell";
        authForm.loginCaption.innerHTML = this.loc.Authorization.TextUserName;
    }

    var loginControl = this.TextBoxWithHintText(null, 230, authForm.showCaptions ? "" : this.loc.Authorization.TextUserName, this.loc.Authorization.TextUserName, "login.png");
    authForm.controls.loginUserName = loginControl;
    controlsTable.addCellInNextRow(loginControl.table);
    loginControl.action = function () { authForm.action(); };
    loginControl.value = this.GetCookie("loginLogin") || "";

    //Password
    if (authForm.showCaptions) {
        authForm.passwordCaption = controlsTable.addCellInNextRow();
        authForm.passwordCaption.style.paddingTop = "17px";
        authForm.passwordCaption.className = "stiCloudReportsWebClientAuthFormTextCell";
        authForm.passwordCaption.innerHTML = this.loc.Authorization.TextPassword;
    }

    var passwordControl = this.TextBoxWithHintText(null, 230, authForm.showCaptions ? "" : this.loc.Authorization.TextPassword, this.loc.Authorization.TextPassword, "password.png");
    passwordControl.setAttribute("type", "password");
    authForm.controls.loginPassword = passwordControl;
    var passwControlCell = controlsTable.addCellInNextRow(passwordControl.table);
    if (!authForm.showCaptions) passwControlCell.style.paddingTop = "17px";
    passwordControl.action = function () { authForm.action(); };
    passwordControl.value = this.GetCookie("loginPassword") || "";

    var rememberMeLabel = document.createElement("label");    
    var rememberMeControl = document.createElement("input");
    rememberMeControl.style.marginTop = "15px";
    authForm.controls.rememberMe = rememberMeControl;
    rememberMeControl.setAttribute("type", "checkbox");
    controlsTable.addCellInNextRow(rememberMeLabel);
    rememberMeLabel.appendChild(rememberMeControl);
    var rememberMeText = document.createElement("span");
    rememberMeText.style.margin = "0 0 3px 4px";
    rememberMeLabel.appendChild(rememberMeText);
    rememberMeText.className = "stiCloudReportsWebClientAuthFormTextCell";
    rememberMeText.innerHTML = this.loc.Authorization.CheckBoxRememberMe;
    rememberMeControl.checked = loginRememberMe;
    
    var buttonsTable = this.CreateHTMLTable();
    controlsTable.addCellInNextRow(buttonsTable);
    buttonsTable.style.width = "100%";

    //Button Login
	var buttonLogin = this.LoginButton(null, this.loc.Authorization.ButtonLogin, null);
    authForm.controls.buttonLogin = buttonLogin;
    buttonLogin.style.margin = "8px 0 8px 0";
    buttonLogin.style.display = "inline-block";
    buttonsTable.addCellInLastRow(buttonLogin).style.paddingTop = "10px";
    buttonLogin.action = function () { authForm.action(); }
    this.AddSmallProgressMarkerToControl(buttonLogin, true);

    var downButtonsTable = this.CreateHTMLTable();
    downButtonsTable.style.width = "100%";
    controlsTable.addCellInNextRow(downButtonsTable).style.padding = "5px 0 20px 0";
    
    //Button Forgot Password
    var buttonForgotPassword = this.HiperLinkButtonForAuthForm(null, this.loc.Authorization.HyperlinkForgotPassword, true);
    authForm.controls.buttonForgotPassword = buttonForgotPassword;
    buttonForgotPassword.style.display = "inline-block";
    downButtonsTable.addCell(buttonForgotPassword, null, "vertical-align: top; text-align: left; padding-bottom: 25px;");
    buttonForgotPassword.action = function () { authForm.changeMode("forgotPassword"); }

    //Button Register Account
    var buttonRegisterAccount = this.HiperLinkButtonForAuthForm(null, this.loc.Authorization.HyperlinkRegisterAccount, true);
    authForm.controls.buttonRegisterAccount = buttonRegisterAccount;
    buttonRegisterAccount.style.display = "inline-block"; var cellButtonRegister = downButtonsTable.addCellInNextRow(buttonRegisterAccount);
    cellButtonRegister.style.textAlign = "center";
    cellButtonRegister.style.borderTop = "1px solid #f2f1f1";
    cellButtonRegister.style.paddingTop = "20px";
    buttonRegisterAccount.action = function () { authForm.changeMode("signUp"); }

    return loginPanel;
}

StiMobileDesigner.prototype.AuthFormSignUpPanel = function (authForm) {
    var signUpPanel = document.createElement("div");
    var mTable = this.CreateHTMLTable();
    var controlsTable = this.CreateHTMLTable();
    controlsTable.style.margin = "0 auto 0 auto";
    signUpPanel.appendChild(mTable);
    mTable.style.width = "100%";
    var ct = mTable.addCell(controlsTable);
    ct.style.width = "50%";
    var rDiv = $("<div style='width: " + authForm.formWidth + "px;margin:0 auto;'></div>")[0];
    var ds = mTable.addCellInNextRow(rDiv);
    ds.style.textAlign = "center";
    ds.colSpan = 2;

    //First Name
    if (authForm.showCaptions) {
        authForm.firstNameCaption = controlsTable.addCell();
        authForm.firstNameCaption.className = "stiCloudReportsWebClientAuthFormTextCell";
        authForm.firstNameCaption.innerHTML = this.loc.Authorization.TextFirstName;
    }

    var firstNameControl = this.TextBoxWithHintText(null, 230, authForm.showCaptions ? "" : this.loc.Authorization.TextFirstName, this.loc.Authorization.TextFirstName, "login.png");
    authForm.controls.signUpFirstName = firstNameControl;
    controlsTable.addCellInNextRow(firstNameControl.table);
    firstNameControl.action = function () { authForm.action(); };
    
    //Last Name
    if (authForm.showCaptions) {
        authForm.lastNameCaption = controlsTable.addCellInNextRow();
        authForm.lastNameCaption.className = "stiCloudReportsWebClientAuthFormTextCell";
        authForm.lastNameCaption.innerHTML = this.loc.Authorization.TextLastName;
        authForm.lastNameCaption.style.paddingTop = "17px";
    }

    var lastNameControl = this.TextBoxWithHintText(null, 230, authForm.showCaptions ? "" : this.loc.Authorization.TextLastName, this.loc.Authorization.TextLastName, "login.png");
    authForm.controls.signUpLastName = lastNameControl;
    var lastNameControlCell = controlsTable.addCellInNextRow(lastNameControl.table);
    if (!authForm.showCaptions) lastNameControlCell.style.paddingTop = "17px";
    lastNameControl.action = function () { authForm.action(); };
    
    //UserName
    if (authForm.showCaptions) {
        authForm.loginCaption2 = controlsTable.addCellInNextRow();
        authForm.loginCaption2.className = "stiCloudReportsWebClientAuthFormTextCell";
        authForm.loginCaption2.innerHTML = this.loc.Authorization.TextUserName;
        authForm.loginCaption2.style.paddingTop = "17px";
    }

    var loginControl = this.TextBoxWithHintText(null, 230, authForm.showCaptions ? "" : this.loc.Authorization.TextUserName, this.loc.Authorization.TextUserName, "email.png");
    authForm.controls.signUpUserName = loginControl;
    var loginControlCell = controlsTable.addCellInNextRow(loginControl.table);
    if (!authForm.showCaptions) loginControlCell.style.paddingTop = "17px";
    loginControl.action = function () { authForm.action(); };
    
    //Password
    if (authForm.showCaptions) {
        authForm.passwordCaption2 = controlsTable.addCellInNextRow();
        authForm.passwordCaption2.className = "stiCloudReportsWebClientAuthFormTextCell";
        authForm.passwordCaption2.innerHTML = this.loc.Authorization.TextPassword;
        authForm.passwordCaption2.style.paddingTop = "17px";
    }

    var passwordControl = this.TextBoxWithHintText(null, 230, authForm.showCaptions ? "" : this.loc.Authorization.TextPassword, this.loc.Authorization.TextPassword, "password.png");
    passwordControl.setAttribute("type", "password");
    authForm.controls.signUpPassword = passwordControl;
    var passwordControlCell = controlsTable.addCellInNextRow(passwordControl.table);
    if (!authForm.showCaptions) passwordControlCell.style.paddingTop = "17px";
    passwordControl.action = function () { authForm.action(); };
    
    //Captcha
    var captchaContainer = $("<div id='sti_authform_captcha_container'></div>")[0];
    authForm.controls.captchaCell = controlsTable.addCellInNextRow(captchaContainer);
        
    //Agree To Terms And Policy
    var agreeTextContainer = document.createElement("div");
    agreeTextContainer.style.width = "300px";
    controlsTable.addCellInNextRow(agreeTextContainer).style.padding = "30px 0 7px 0";

    var agreeCheckbox = this.CheckBox();
    agreeCheckbox.id = "sti_authform_agreecheckbox";
    authForm.controls.agreeCheckbox = agreeCheckbox;
    agreeCheckbox.style.marginRight = "6px";
    agreeCheckbox.style.display = "inline-block";
    agreeTextContainer.appendChild(agreeCheckbox);

    agreeCheckbox.action = function () {
        authForm.controls.buttonSignUp.agreeTermsAndPolicy = this.isChecked;
        if (authForm.controls.buttonSignUp.completeCaptcha) {
            authForm.controls.buttonSignUp.setEnabled(this.isChecked);
        }
    }

    var textTermsAndPolicy = this.loc.Cloud.AcceptTermsAndPrivacyPolicy;

    var agreeText = this.CreateHTMLTable();
    agreeText.className = "stiDesignerTextContainer";
    agreeText.style.fontSize = "14px";
    agreeText.style.display = "inline-block";
    agreeText.addTextCell(textTermsAndPolicy ? textTermsAndPolicy.substring(0, textTermsAndPolicy.indexOf("{0}")) : "I accept the");
    agreeTextContainer.appendChild(agreeText);

    var buttonPrivacy = this.HiperLinkButton(null, this.loc.Cloud.PrivacyPolicy || "Privacy");
    buttonPrivacy.style.margin = "0 4px 0 4px";
    buttonPrivacy.caption.style.padding = "0px";
    buttonPrivacy.caption.style.whiteSpace = "normal";
    buttonPrivacy.style.display = "inline-block";
    buttonPrivacy.action = function () {
        authForm.this_.openNewWindow("https://www.stimulsoft.com/en/privacy-policy");
    }
    agreeTextContainer.appendChild(buttonPrivacy);
    
    var andText = this.CreateHTMLTable();
    andText.className = "stiDesignerTextContainer";
    andText.style.fontSize = "14px";
    andText.style.display = "inline-block";
    andText.addTextCell(textTermsAndPolicy ? textTermsAndPolicy.substring(textTermsAndPolicy.indexOf("{0}") + 3, textTermsAndPolicy.indexOf("{1}")) : "and");
    agreeTextContainer.appendChild(andText);

    var buttonTerms = this.HiperLinkButton(null, this.loc.Cloud.TermsOfUse || "Terms");
    buttonTerms.style.margin = "0 4px 0 4px";
    buttonTerms.caption.style.padding = "0px";
    buttonTerms.caption.style.whiteSpace = "normal";
    buttonTerms.style.display = "inline-block";
    buttonTerms.action = function () {
        var form = authForm.this_.InitializeLicenseForm();
        form.buttonSave.caption.innerHTML = authForm.this_.loc.Common.ButtonOK;
        form.buttonCancel.style.display = "none";
    }
    agreeTextContainer.appendChild(buttonTerms);

    var endText = this.CreateHTMLTable();
    endText.className = "stiDesignerTextContainer";
    endText.style.fontSize = "14px";
    endText.style.display = "inline-block";
    endText.addTextCell(textTermsAndPolicy ? textTermsAndPolicy.substring(textTermsAndPolicy.indexOf("{1}") + 3) : "");
    agreeTextContainer.appendChild(endText);

    //Button SignUp
    var buttonSignUp = this.LoginButton(null, this.loc.Authorization.ButtonSignUp, null);
    buttonSignUp.id = "sti_authform_buttonsignup";
    authForm.controls.buttonSignUp = buttonSignUp;
    buttonSignUp.style.margin = "3px 0 8px 0";
    buttonSignUp.style.display = "inline-block";
    controlsTable.addCellInNextRow(buttonSignUp).style.textAlign = "right";
    buttonSignUp.action = function () { authForm.action(); }
    this.AddSmallProgressMarkerToControl(buttonSignUp, true);

    //Button AlreadyHaveCloudReportsAccount
    var buttonAlreadyHaveAccount = this.HiperLinkButton(null, this.loc.Authorization.HyperlinkAlreadyHaveAccount, 23);
    authForm.controls.buttonAlreadyHaveAccount = buttonAlreadyHaveAccount;
    buttonAlreadyHaveAccount.style.margin = "8px 0 45px 0";
    buttonAlreadyHaveAccount.style.display = "inline-block";
    rDiv.appendChild(buttonAlreadyHaveAccount);
    buttonAlreadyHaveAccount.action = function () { authForm.changeMode("login"); }

    return signUpPanel;
}

StiMobileDesigner.prototype.AuthFormResetPasswordPanel = function (authForm) {
    var resetPasswordPanel = document.createElement("div");
    var mTable = this.CreateHTMLTable();
    var controlsTable = this.CreateHTMLTable();
    controlsTable.style.margin = "0 auto 0 auto";
    resetPasswordPanel.appendChild(mTable);
    mTable.style.width = "100%";
    var ct = mTable.addCell(controlsTable);
    ct.style.width = "50%";
    var rDiv = $("<div style='width: " + authForm.formWidth + "px;margin:0 auto;'></div>")[0];
    var ds = mTable.addCellInNextRow(rDiv);
    ds.style.textAlign = "center";
    ds.colSpan = 2;

    //UserName
    if (authForm.showCaptions) {
        authForm.loginCaption13 = controlsTable.addCell();
        authForm.loginCaption13.className = "stiCloudReportsWebClientAuthFormTextCell";
        authForm.loginCaption13.innerHTML = this.loc.Authorization.TextPassword;
    }

    var hintText = this.loc.Administration.LabelNewPassword ? this.loc.Administration.LabelNewPassword.replace(":", "") : "";
    var passwordControl = this.TextBoxWithHintText(null, 230, authForm.showCaptions ? "" : hintText, hintText, "password.png");
    passwordControl.setAttribute("type", "password");
    authForm.controls.resetPasswordPassword = passwordControl;
    controlsTable.addCellInNextRow(passwordControl.table);
    passwordControl.action = function () {
        this.jsObject.options.forms.authForm.action();
    };

    //Button ResetPassword
    var buttonSetPassword = this.LoginButton(null, this.loc.Authorization.ButtonResetPassword, null);
    authForm.controls.buttonSetPassword = buttonSetPassword;
    buttonSetPassword.style.margin = "8px 0 45px 0";
    buttonSetPassword.style.display = "inline-block";
    controlsTable.addCellInNextRow(buttonSetPassword).style.paddingTop = "10px";
    var this_ = authForm.jsObject;

    buttonSetPassword.action = function () {
        authForm.jsObject.SendCloudCommand("UserResetPasswordComplete", { NewPassword: passwordControl.value, ResetKey: authForm.resetId },
            function (data) {
                authForm.startLogin(data.ResultUserName, passwordControl.value/*, this_.GetCookie("loginRememberMe") === "true"*/);
            });
    }
    this.AddSmallProgressMarkerToControl(buttonSetPassword, true);

    return resetPasswordPanel;
}

StiMobileDesigner.prototype.AuthFormForgotPasswordPanel = function (authForm) {
    var forgotPasswordPanel = document.createElement("div");
    var mTable = this.CreateHTMLTable();
    var controlsTable = this.CreateHTMLTable();
    controlsTable.style.margin = "0 auto 0 auto";
    forgotPasswordPanel.appendChild(mTable);
    mTable.style.width = "100%";
    var ct = mTable.addCell(controlsTable);
    ct.style.width = "50%";
    var rDiv = $("<div style='width: " + authForm.formWidth + "px;margin:0 auto;'></div>")[0];
    var ds = mTable.addCellInNextRow(rDiv);
    ds.style.textAlign = "center";
    ds.colSpan = 2;

    //UserName
    if (authForm.showCaptions) {
        authForm.loginCaption3 = controlsTable.addCell();
        authForm.loginCaption3.className = "stiCloudReportsWebClientAuthFormTextCell";
        authForm.loginCaption3.innerHTML = this.loc.Authorization.TextUserName;
    }

    var loginControl = this.TextBoxWithHintText(null, 230, authForm.showCaptions ? "" : this.loc.Authorization.TextUserName, this.loc.Authorization.TextUserName, "email.png");
    authForm.controls.forgotPasswordUserName = loginControl;
    controlsTable.addCellInNextRow(loginControl.table);
    loginControl.action = function () { this.jsObject.options.forms.authForm.action(); };

    //Button ResetPassword
    var buttonResetPassword = this.LoginButton(null, this.loc.Authorization.ButtonResetPassword, null);
    authForm.controls.buttonResetPassword = buttonResetPassword;
    buttonResetPassword.style.margin = "8px 0 8px 0";
    buttonResetPassword.style.display = "inline-block";
    controlsTable.addCellInNextRow(buttonResetPassword).style.paddingTop = "10px";
    buttonResetPassword.action = function () { this.jsObject.options.forms.authForm.action(); }
    this.AddSmallProgressMarkerToControl(buttonResetPassword, true);
       
    var downButtonsTable = this.CreateHTMLTable();
    downButtonsTable.style.width = "100%";
    controlsTable.addCellInNextRow(downButtonsTable).style.padding = "5px 0 20px 0";
        
    //Button Have Password
    var buttonHavePassword = this.HiperLinkButtonForAuthForm(null, this.loc.Authorization.HyperlinkHavePassword, true);
    authForm.controls.buttonHavePassword = buttonHavePassword;
    buttonHavePassword.style.display = "inline-block";
    downButtonsTable.addCell(buttonHavePassword, null, "vertical-align: top; text-align: left; padding-bottom: 25px;");
    buttonHavePassword.action = function () { this.jsObject.options.forms.authForm.changeMode("login"); }

    //Button Register Account
    var buttonRegisterAccount = this.HiperLinkButtonForAuthForm(null, this.loc.Authorization.HyperlinkRegisterAccount, true);
    authForm.controls.buttonRegisterAccountForgotPassword = buttonRegisterAccount;
    buttonRegisterAccount.style.display = "inline-block";
    var cellButtonRegister = downButtonsTable.addCellInNextRow(buttonRegisterAccount);
    cellButtonRegister.style.textAlign = "center";
    cellButtonRegister.style.borderTop = "1px solid #f2f1f1";
    cellButtonRegister.style.paddingTop = "20px";
    buttonRegisterAccount.action = function () { this.jsObject.options.forms.authForm.changeMode("signUp"); }

    return forgotPasswordPanel;
}

StiMobileDesigner.prototype.AuthFormRegisterSuccessfullyPanel = function (authForm) {
    var registerSuccessfullyPanel = document.createElement("div");
    var mTable = this.CreateHTMLTable();
    var controlsTable = this.CreateHTMLTable();
    controlsTable.style.margin = "0 auto 0 auto";
    registerSuccessfullyPanel.appendChild(mTable);
    mTable.style.width = "100%";
    var ct = mTable.addCell(controlsTable);
    ct.style.width = "50%";
    var rDiv = $("<div style='width: " + authForm.formWidth + "px;margin:0 auto;'></div>")[0];
    var ds = mTable.addCellInNextRow(rDiv);
    ds.style.textAlign = "center";
    ds.colSpan = 2;

    //Text
    var textPanel = document.createElement("div");
    textPanel.className = "stiCloudReportsWebClientAuthFormSuccessfullyText";
    authForm.controls.registerSuccessfullyText = textPanel;    
    var cell = controlsTable.addCellInNextRow(textPanel);
    cell.className = "stiCloudReportsWebClientAuthFormTextCell";
    cell.style.padding = "0px 30px 0px 30px";
    cell.colSpan = 2;
    cell.style.width = "250px";

    //Button Resend Email
    var buttonResendEmail = this.LoginButton(null, this.loc.Authorization.ButtonResendEmail, null);
    authForm.controls.buttonResendEmail = buttonResendEmail;
    buttonResendEmail.style.margin = "8px 0 8px 0";
    buttonResendEmail.style.display = "inline-block";
    buttonResendEmail.style.minWidth = "315px";
    controlsTable.addCellInNextRow(buttonResendEmail).style.textAlign = "left";

    buttonResendEmail.action = function () {
        this.jsObject.SendCloudCommand("UserActivate", { UserName: authForm.controls.loginUserName.value, ResultSuccess: true }, function () {
            authForm.resetControls();
            textPanel.innerHTML = "";
        }, function () {
            authForm.resetControls();
            textPanel.innerHTML = "";
        });
    }
    
    //Button Continue
    var buttonContinue = this.HiperLinkButton(null, this.loc.Common.ButtonBack, 23);
    authForm.controls.buttonContinue = buttonContinue;
    buttonContinue.style.margin = "8px 0 45px 0";
    buttonContinue.style.display = "inline-block";
    rDiv.appendChild(buttonContinue);
    buttonContinue.action = function () {
        authForm.resetControls();
        authForm.changeMode("login");
    }

    return registerSuccessfullyPanel;
}

StiMobileDesigner.prototype.LoginButton = function (name, caption, imageName, minWidth, tooltip) {
	var button = this.SmallButton(name, null, caption || "", imageName, tooltip, null, this.GetStyles("LoginButton"));
	
	button.setEnabled = function (state) {
	    this.style.opacity = state ? "1" : "0.3";
	    this.style.cursor = state ? "pointer" : "default";
	    this.isEnabled = state;
	    if (!state && !this.isOver) this.isOver = false;
	    this.className = (state ? (this.isOver ? this.styles["over"] : (this.isSelected ? this.styles["selected"] : this.styles["default"])) : this.styles["disabled"]) +
            (this.jsObject.options.isTouchDevice ? "_Touch" : "_Mouse");
	}

	button.innerTable.style.width = "100%";
    button.style.minWidth = (minWidth || 80) + "px";
    button.caption.style.textAlign = "center";
    button.style.cursor = "pointer";

    return button;
}

StiMobileDesigner.prototype.AddWheelPanel = function (authForm) {
    var panel = document.createElement("div");
    this.AddBigProgressMarkerToControl(panel);
    panel.progressMarker.changeVisibleState(true, 460, 350);
    
    return panel;
}

var captchaLoadedComplete = function (data) {
    if (window["grecaptcha"]) {
        var captchaContainer = document.getElementById("sti_authform_captcha_container");
        if (captchaContainer) {
            captchaContainer.style.marginTop = "17px";

            window["captchaId"] = grecaptcha.render('sti_authform_captcha_container', {
                'sitekey': '6LdmZCwUAAAAAHuIu_7B2NgFC7ks7A9y0NBHf9YD',
                'callback': function (response) {
                    if (response) {
                        var buttonSignUp = document.getElementById("sti_authform_buttonsignup");
                        if (buttonSignUp) {
                            buttonSignUp.completeCaptcha = true;
                            if (buttonSignUp.agreeTermsAndPolicy) {
                                buttonSignUp.setEnabled(true);
                            }
                        }
                    }
                }
            });
        }
    }
}