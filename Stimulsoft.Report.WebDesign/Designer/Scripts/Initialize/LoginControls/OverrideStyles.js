
StiMobileDesigner.prototype.OverrideStyles = function () {
    this.OldGetStyles = this.GetStyles;

    this.GetStyles = function (name) {
        if (this.OldGetStyles(name)) return this.OldGetStyles(name);
        else {
            switch (name) {
                case "LoginButton": return {
                    "default": "stiCloudReportsWebClientLoginButton",
                    "over": "stiCloudReportsWebClientLoginButtonOver",
                    "selected": "stiCloudReportsWebClientLoginButtonSelected",
                    "disabled": "stiCloudReportsWebClientLoginButtonDisabled"
                }

                case "AuthHyperlinkButton": return {
                    "default": "stiCloudReportsWebClientHyperlinkButton",
                    "over": "stiCloudReportsWebClientHyperlinkButtonOver",
                    "selected": "stiCloudReportsWebClientHyperlinkButtonSelected",
                    "disabled": "stiCloudReportsWebClientHyperlinkButtonDisabled"
                }

                case "LoginToolButton": return {
                    "default": "stiCloudReportsWebClientToolButton",
                    "over": "stiCloudReportsWebClientToolButtonOver",
                    "selected": "stiCloudReportsWebClientToolButtonSelected",
                    "disabled": "stiCloudReportsWebClientToolButtonDisabled"
                }

                case "HeaderButton": return {
                    "default": "stiCloudReportsWebClientHeaderButton",
                    "over": "stiCloudReportsWebClientHeaderButtonOver",
                    "selected": "stiCloudReportsWebClientHeaderButtonSelected",
                    "disabled": "stiCloudReportsWebClientHeaderButtonDisabled"
                }

                case "SmallButtonWithBorders": return {
                    "default": "stiCloudReportsWebClientSmallButtonWithBorders",
                    "over": "stiCloudReportsWebClientSmallButtonWithBordersOver",
                    "selected": "stiCloudReportsWebClientSmallButtonWithBordersSelected",
                    "disabled": "stiCloudReportsWebClientSmallButtonWithBordersDisabled"
                }
            }
        }
    }
}