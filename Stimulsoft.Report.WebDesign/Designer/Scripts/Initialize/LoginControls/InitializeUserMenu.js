﻿
StiMobileDesigner.prototype.InitializeUserMenu = function () {
    var items = [];
    items.push(this.Item("userInfo", "", null, ""));
    items.push("separator");    
    items.push(this.Item("profile", this.loc.Navigator.TextProfile, null, "accountMenuProfile"));
    items.push(this.Item("changePassword", this.loc.Navigator.ButtonChangePassword, null, "changePassword"));
    items.push("separator2");
    items.push(this.Item("logout", this.loc.Navigator.ButtonLogout, null, "accountMenuLogout"));
    
    
    var menu = this.VerticalMenu("userMenu", this.options.toolBar.userNameButton, "Down", items, this.GetStyles("MenuStandartItem"));
    this.options.userMenu = menu;
    menu.style.top = "40px";//this.options.toolBar.offsetHeight + "px";
    menu.style.right = "0px";
    menu.innerContent.className = "stiCloudReportsWebClientUserMenu";
    menu.innerContent.style.borderTop = "0px";
    menu.innerContent.style.borderRight = "0px";

    menu.items.userInfo.caption.style.padding = "5px 20px 5px 5px";
    menu.items.userInfo.style.height = "65px";
    menu.items.userInfo.isEnabled = false;

    var itemNames = ["profile", "changePassword", "logout"]
    for (var i = 0; i < itemNames.length; i++) {
        if (menu.items[itemNames[i]].caption) menu.items[itemNames[i]].caption.style.paddingLeft = "15px";
        menu.items[itemNames[i]].style.height = "35px";
    }

    var infoTable = document.createElement("table");
    menu.items.userInfo.caption.appendChild(infoTable);
    var infoTr = document.createElement("tr");
    infoTable.appendChild(infoTr);
    var infoTd = document.createElement("td");
    infoTr.appendChild(infoTd);
    menu.items.userInfo.userName = document.createElement("td");
    infoTr.appendChild(menu.items.userInfo.userName);
    menu.items.userInfo.userName.style.verticalAlign = "bottom";
    menu.items.userInfo.userName.style.paddingLeft = "5px";
    menu.items.userInfo.userName.style.width = "200px";
    menu.items.userInfo.userName.innerHTML = this.options.cloudParameters.userName;

    var this_ = this;
    this.options.toolBar.userNameButton.action = function () {
        this_.options.menus.userMenu.changeVisibleState(!this_.options.menus.userMenu.visible, null, null, null);
    }

    menu.changeVisibleState = function (state) {
        if (state) {
            this.onshow();
            this.style.opacity = 0;
            this.style.display = "";
            this.visible = true;
            if (this.parentButton) this.parentButton.setSelected(true);
            this.jsObject.options.currentMenu = this;
            this.style.width = this.innerContent.offsetWidth + "px";
            this.style.height = this.innerContent.offsetHeight + "px";
            $(this).animate({ opacity: 1 }, { duration: this.jsObject.options.formAnimDuration });
        }
        else {
            this.visible = false;
            if (this.parentButton) this.parentButton.setSelected(false);
            this.style.display = "none";
            if (this.jsObject.options.currentMenu == this) this.jsObject.options.currentMenu = null;
        }
    }

    menu.action = function (menuItem) {
        this.changeVisibleState(false);

        switch (menuItem.key) {
            case "accountMenuLogout": {
                this.jsObject.FinishSession();
                break;
            }
            case "changePassword": {
                this.jsObject.options.forms.changePasswordForm.show();
                break;
            }
            case "accountMenuProfile": {
                this.jsObject.InitializeSettingsForm().show();
                break;
            }
        }        
    }
    
    return menu; 
}