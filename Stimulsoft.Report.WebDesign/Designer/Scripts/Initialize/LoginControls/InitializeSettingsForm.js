﻿StiMobileDesigner.prototype.InitializeSettingsForm = function () {

    var wizardButtonProps = [
       ["Profile", this.loc.Navigator.TextProfile]
    ];

    var settingsForm = this.MultiWizardBaseFormPanel(wizardButtonProps, "max-height");
    this.options.mainPanel.appendChild(settingsForm);
    this.options.forms.settingsForm = settingsForm;
    settingsForm.className = "stiCloudReportsWebClientForm";
    settingsForm.container.className = "stiCloudReportsWebClientFormContainer";
    settingsForm.header.className = "stiCloudReportsWebClientFormHeader";
    settingsForm.level = 3;    
    settingsForm.style.zIndex = settingsForm.level * 10 + 1;
    settingsForm.container.style.padding = "0px 10px 0px 10px";
    settingsForm.helpUrlKey = "WindowOptions";
    settingsForm.rhomb.style.display = 'none';
    var profilePanel = settingsFormProfile(settingsForm, this);
    settingsForm.wizardPanels["Profile"].appendChild(profilePanel);
    settingsForm.showPanel("Profile");

    var this_ = this;
    settingsForm.show = function () {        
        if (!settingsForm.populated) {
            settingsForm.formButtons.ok.setEnabled(false);
        }
        this.changeVisibleState(true);

        profilePanel.progress.show();

        this_.SendCloudCommand("UserGet", { UserKey: this_.options.cloudParameters.userKey }, function (data) {
            profilePanel.populateFields(data.ResultUser, profilePanel.controls);
            profilePanel.controls.userPicture.setImage(data.ResultUser.Picture);
            profilePanel.data = $.extend({}, data.ResultUser);

            profilePanel.progress.hide();
            settingsForm.formButtons.ok.setEnabled(true); //added
            settingsForm.populated = true; //added

            //this_.SendCommand("RoleGet", { RoleKey: data.ResultUser.RoleKey }, function (data) {
            //    profilePanel.populateFields(data.ResultRole, profilePanel.controls);
            //    settingsForm.formButtons.ok.setEnabled(true);
            //    settingsForm.populated = true;
            //});
        });
    }

    settingsForm.changeVisibleState = function (state) {
        if (state) {
            this.style.display = "";
            this.jsObject.options.disabledPanels[this.level].style.display = "";
            this.onshow();
            this.jsObject.SetObjectToCenter(this);
            this.visible = true;
            this.jsObject.options.currentForm = this;

            d = new Date();
            var endTime = d.getTime() + this.jsObject.options.formAnimDuration;
            this.flag = false;
            this.jsObject.ShowAnimationForm(this, endTime);
        }
        else {
            clearTimeout(this.animationTimer);
            this.visible = false;
            this.jsObject.options.currentForm = null;
            this.style.display = "none";
            this.jsObject.options.disabledPanels[this.level].style.display = "none";
            this.onhide();
        }
    }
    
    settingsForm.action = function () {
        var pane = settingsForm.options.container;
        if ((pane.data.UserName == "demo") ||
           (pane.controls["FirstName"].checkEmpty(this.jsObject.loc.Notices.AuthFirstNameIsNotSpecified, function () { settingsForm.showPanel("Profile"); }) &&
            pane.controls["LastName"].checkEmpty(this.jsObject.loc.Notices.AuthLastNameIsNotSpecified, function () { settingsForm.showPanel("Profile"); })))
        {
            this_.SendCloudCommand("UserSave", { User: pane.data }, function (data) {
                this_.setUserInfo();
            });            
            
            settingsForm.changeVisibleState(false);
        }
    }

    
    function settingsFormProfile(settingsForm, this_) {
        var container = document.createElement("div");
        container.style.width = "450px";
        container.style.minHeight = "350px";
        container.style.position = "relative";
        settingsForm.options = {};
        settingsForm.options.container = container;
        container.jsObject = this_;
        container.className = "";
        container.controls = {};
        container.labels = {};
        this_.AddProgressToControl(container);

        var upTable = this_.CreateHTMLTable();
        container.appendChild(upTable);
        upTable.style.paddingTop = "0px";
        upTable.style.marginTop = "7px";

        var cWidth = 250;
        createPanel(this_.loc.Administration.TextUser, "textUser", [
            ["FirstName", this_.loc.Administration.LabelFirstName, cWidth],
            ["LastName", this_.loc.Administration.LabelLastName, cWidth],
            ["UserName", this_.loc.Administration.LabelUserName, cWidth, true]],
            false, this);
        upTable.addRow();
        var saveCol = upTable.addCellInLastRow();
        saveCol.colSpan = 2;
        saveCol.appendChild(this_.InfoPanelSeparator("-30px"));

        upTable.addRow();
        var text = upTable.addCellInLastRow();
        text.style.padding = "10px 10px 10px 0px";
        text.innerHTML = this_.loc.Administration.LabelPicture;
        container.labels["picture"] = text;
        var control = this_.EditUserPictureControl();
        control.style.margin = "7px 0 7px 0";
        container.controls["userPicture"] = control;
        upTable.addCellInLastRow(control);
        upTable.className = "stiDesignerTextContainer"; //added
        settingsForm.options.container.data = {};
        control.action = function () {
            if (control.img && control.img != "") {
                settingsForm.options.container.data.Picture = control.img;
            } else {
                delete settingsForm.options.container.data.Picture;
            }
        }

        function createPanel(caption, name, data, isLabel) {
            upTable.addRow();
            for (var i in data) {
                var row = upTable.addRow();
                row.style.height = "32px";
                var text = upTable.addCellInLastRow();                
                text.style.padding = "7px 10px 7px 0px";
                text.innerHTML = data[i][1];
                container.labels[data[i][0]] = text;
                var control = (isLabel || data[i][3]) ? upTable.addCellInLastRow() : this_.TextBox(data[i][0], data[i][2]);
                container.controls[data[i][0]] = control;
                if (!isLabel) {
                    upTable.addCellInLastRow(control);
                    control.oninput = function () {
                        settingsForm.options.container.data[this.name] = this.value;
                    }
                } else {
                    upTable.className = "stiCloudReportsWebClientTextBeforeControl";
                }
            }
        }

        container.populateFields = function (data, controls) {
            for (var i in data) {
                if (i in controls) {
                    if (controls[i] instanceof HTMLInputElement) {
                        controls[i].value = data[i];
                    } else {
                        controls[i].innerHTML = this_.JSONDateFormatToDate(data[i], true);
                    }
                }
            }
        }
                
        return container;
    }    

    return settingsForm;
    
}

StiMobileDesigner.prototype.InfoPanelSeparator = function (marginLeft) {
    var separator = document.createElement("div");
    separator.className = "stiCloudReportsWebClientInfoPanelSeparator";
    if (marginLeft) {
        separator.style.marginLeft = marginLeft;
        separator.style.width = "150%";
    }
    return separator;
}