
StiMobileDesigner.prototype.formatString = function (format, args) {
    if (format) {
        var result = format;
        for (var i = 1; i < arguments.length; i++) {
            result = result.replace('{' + (i - 1) + '}', arguments[i]);
        }
        return arguments.length > 1 ? result : format.replace('{0}', "");
    } else {
        return "";
    }
}

StiMobileDesigner.prototype.AddSmallProgressMarkerToControl = function (control, toTheRight) {
    var progressMarker = document.createElement("div");
    progressMarker.className = "stiCloudReportsSmallProgressMarker";
    var offset = this.options.IsTouchDevice ? "-22px" : "-20px";
    progressMarker.style.left = offset;
    progressMarker.style.top = offset;
    progressMarker.style.display = "none";
    var jsObject = this;

    var progress = document.createElement("div");
    progressMarker.appendChild(progress);
    progress.className = "client_navigator_loader_mini";

    progressMarker.changeVisibleState = function (state) {
        if (toTheRight) {
            this.style.left = ($(this.control).width() + 5) + "px";
            this.style.top = -($(this.control).height() / 2 + 8) + "px";
        }
        progressMarker.style.display = state ? "" : "none";
    }
    progressMarker.control = control;
    control.appendChild(progressMarker);
    control.progressMarker = progressMarker;
}

StiMobileDesigner.prototype.AddBigProgressMarkerToControl = function (control) {
    var progressMarker = document.createElement("div");
    progressMarker.className = "stiCloudReportsBigProgressMarker";
    progressMarker.style.display = "none";
    progressMarker.style.overflow = "hidden";
    var jsObject = this;

    var progress = document.createElement("div");
    progressMarker.appendChild(progress);
    progress.className = "client_navigator_loader";

    progressMarker.changeVisibleState = function (state, left, top) {
        progressMarker.style.display = state ? "" : "none";
        if (state) {
            progressMarker.style.left = (left || control.offsetWidth / 2 - 32) + "px";
            progressMarker.style.top = (top || $(control).height() / 2 - 32) + "px";
        }
    }

    control.appendChild(progressMarker);
    control.progressMarker = progressMarker;
}

StiMobileDesigner.prototype.StartNewSession = function () {
    this.options.toolBar.userNameButton.style.display = "";
    this.options.toolBar.loginButton.style.display = "none";
    //this.SetCookie("sti_SessionDate", this.DateToJSONDateFormat(new Date()));
}

StiMobileDesigner.prototype.FinishSession = function () {
    if (this.options.cloudParameters.sessionKey) {
        this.SendCloudCommand("UserLogout", {}, function (data) { });
    }

    var expDate = new Date(new Date().getTime() - 1);
    this.SetCookie("sti_SessionKey", "", this.options.cookiesPath, this.options.cookiesDomain, null, expDate.toUTCString());
    this.SetCookie("sti_UserKey", "", this.options.cookiesPath, this.options.cookiesDomain, null, expDate.toUTCString());
    //this.SetCookie("sti_SessionDate", "");
    this.options.cloudParameters.sessionKey = null;
    this.options.cloudParameters.userKey = null;
    this.options.toolBar.userNameButton.style.display = "none";
    this.options.toolBar.loginButton.style.display = "";

    if (this.options.isOnlineVersion) {
        this.options.productVersion = this.options.productVersion.trim();
        this.UpdateWindowTitle();
        this.UpdateTrialWatermarksOnPages();
    }
}

StiMobileDesigner.prototype.UpdateUserNameButton = function (user) {
    var imgSmall = this.getUserImage(user, true);
    var img = this.getUserImage(user, false);
    $(this.options.userImgSmall).replaceWith(imgSmall);
    this.options.userImgSmall = imgSmall;
    $(this.options.userMenu.items.userInfo.img).replaceWith(img);
    this.options.userMenu.items.userInfo.img = img;
    var fn = user.FirstName && user.FirstName.length > 0 ? user.FirstName : "";
    var ln = user.LastName && user.LastName.length > 0 ? user.LastName : "";
    this.options.userMenu.items.userInfo.userName.innerHTML = "<div style='font-weight:bold;font-size:17px;padding-bottom:5px'>" + fn + " " + ln + "</div>" + user.UserName;    
    this.options.toolBar.userNameButton.nameCell.innerHTML = user.FirstName && user.LastName
        ? user.FirstName + " " + user.LastName : user.UserName;
    this.options.toolBar.userNameButton.userImageCell.innerHTML = "";

    var userImg = this.getUserImage(user, false);
    if (userImg) {
        userImg.className = "stiCloudReportsToolbarUserImage";
        this.options.toolBar.userNameButton.userImageCell.appendChild(userImg);
    }
}

StiMobileDesigner.prototype.setUserInfo = function () {
    var this_ = this;
    var command = "CommandListRun";
    var params = {
        Commands: [],
        ResultSuccess: true
    }
    params.Commands.push({ Ident: "UserGet", UserKey: this.options.cloudParameters.userKey });
    params.Commands.push({ Ident: "RoleDefault" });
    params.Commands.push({ Ident: "WorkspaceGet" });
    params.Commands.push({ Ident: "LicenseGet" });
       
    this.SendCloudCommand(command, params,
        function (res) {
            var data = res.ResultCommands[0];
            this_.options.cloudParameters.user = data.ResultUser;
            this_.options.cloudParameters.userName = data.ResultUser.UserName;
            this_.options.cloudParameters.workspace = res.ResultCommands[2];
            this_.UpdateUserNameButton(data.ResultUser);
            this_.StartNewSession();

            if (this_.options.isOnlineVersion) {
                this_.options.productVersion = this_.options.productVersion.trim();
                if (!this_.options.cloudParameters.workspace.ResultWorkspace.IsTrial) {
                    this_.options.productVersion += " ";
                }
                this_.UpdateWindowTitle();
                this_.UpdateTrialWatermarksOnPages();
            }
        },
        function (data) {
            this_.FinishSession();
        });
}

StiMobileDesigner.prototype.getUserImage = function (user, isSmall) {
    var img;
    if (user.Picture) {
        img = isSmall ? $("<img style='max-width:24px;max-height:24px;' src='data:image/jpeg;base64, " + user.Picture + "'/>")[0] :
                        $("<img style='max-width:24px;max-height:24px;' src='data:image/jpeg;base64, " + user.Picture + "'/>")[0];
    } else {
        var bgColor = this.getUserImgColor(user.Key);
        var sign = this.getUserSign(user);
        img = isSmall ? $("<div style='line-height:1;width:24px;height:24px;text-align:center;color:white;font-family:Arial;font-size:12px;overflow:hidden;background-color:" +
             bgColor + "'><div style='margin-top: 6px;'>" + sign + "</div></div>")[0] :
                         $("<div style='line-height:1;width:24px;height:24px;text-align:center;color:white;font-family:Arial;font-size:12px;overflow:hidden;background-color:" +
             bgColor + "'><div style='margin-top: 6px;'>" + sign + "</div></div>")[0];
    }
    return img;
}

StiMobileDesigner.prototype.getUserSign = function (user) {
    var sb = "";
    if (user.FirstName != null && user.FirstName != "") sb += user.FirstName.substring(0, 1);
    if (user.LastName != null && user.LastName != "") sb += user.LastName.substring(0, 1);
    if (sb.length == 0) sb = user.UserName.substring(0, 1);
    return sb;
}

StiMobileDesigner.prototype.getUserImgColor = function (key) {
    var r = 0;
    var g = 0;
    var b = 0;
    for (var i = 0; i < key.length / 3; i += 3) {
        r += key.charCodeAt(i);
        g += key.charCodeAt(i + 1);
        b += key.charCodeAt(i + 2);
    }
    r = r % 100 + 100;
    g = g % 100 + 100;
    b = b % 100 + 100;
    return this.rgb(r, g, b);
}

StiMobileDesigner.prototype.dec2hex = function (d) {
    if (d > 15) {
        return d.toString(16)
    } else {
        return "0" + d.toString(16)
    }
}
StiMobileDesigner.prototype.rgb = function (r, g, b) {
    return "#" + this.dec2hex(r) + this.dec2hex(g) + this.dec2hex(b)
};

StiMobileDesigner.prototype.JSONDateFormatToDate = function (dateJSONFormat, format) {
    if (dateJSONFormat.substring(0, 6) == "/Date(") {
        var dateStr = dateJSONFormat.replace("/Date(", "").replace(")/", "");
        return typeof (format) == "boolean" ? this.formatDate(new Date(parseInt(dateStr)), this.options.STI_DATE_TIME_FORMAT)/*new Date(parseInt(dateStr)).toLocaleString()*/ : (format ? this.formatDate(new Date(parseInt(dateStr)), format) : new Date(parseInt(dateStr)));
    } else {
        return dateJSONFormat;
    }
}

StiMobileDesigner.prototype.DateToJSONDateFormat = function (date) {
    var offset = date.getTimezoneOffset() * -1;
    hoursOffset = Math.abs(parseInt(offset / 60));
    minutesOffset = Math.abs(offset % 60);
    if (hoursOffset.toString().length == 1) hoursOffset = "0" + hoursOffset;
    if (minutesOffset.toString().length == 1) minutesOffset = "0" + minutesOffset;
    return "/Date(" + Date.parse(date).toString() + ")/";
}

StiMobileDesigner.prototype.formatDate = function (formatDate, formatString) {
    var yyyy = formatDate.getFullYear();
    var yy = yyyy.toString().substring(2);
    var m = formatDate.getMonth() + 1;
    var mm = m < 10 ? "0" + m : m;
    var d = formatDate.getDate();
    var dd = d < 10 ? "0" + d : d;

    var h = formatDate.getHours();
    var hh = h < 10 ? "0" + h : h;
    var n = formatDate.getMinutes();
    var nn = n < 10 ? "0" + n : n;
    var s = formatDate.getSeconds();
    var ss = s < 10 ? "0" + s : s;

    formatString = formatString.replace(/yyyy/i, yyyy);
    formatString = formatString.replace(/yy/i, yy);
    formatString = formatString.replace(/mm/i, mm);
    formatString = formatString.replace(/m/i, m);
    formatString = formatString.replace(/dd/i, dd);
    formatString = formatString.replace(/d/i, d);
    formatString = formatString.replace(/hh/i, hh);
    formatString = formatString.replace(/h/i, h);
    formatString = formatString.replace(/nn/i, nn);
    formatString = formatString.replace(/n/i, n);
    formatString = formatString.replace(/ss/i, ss);
    formatString = formatString.replace(/s/i, s);

    return formatString;
}

StiMobileDesigner.prototype.convertToMB = function (value) {
    return this.round((value / 1024) / 1024, 3);
}

StiMobileDesigner.prototype.round = function (a, b) {
    b = b || 0;
    return Math.round(a * Math.pow(10, b)) / Math.pow(10, b);
}