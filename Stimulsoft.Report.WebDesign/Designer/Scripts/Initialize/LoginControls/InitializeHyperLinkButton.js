﻿
StiMobileDesigner.prototype.HiperLinkButton = function (name, caption, height) {
    var hiperLinkButton = this.SmallButton(name, null, caption, null, null, null, this.GetStyles("AuthHyperlinkButton"));
    if (height != null) hiperLinkButton.style.height = this.options.isTouchDevice ? (height + 5) + "px" : height + "px";
    hiperLinkButton.caption.style.padding = "";

    return hiperLinkButton;
}

StiMobileDesigner.prototype.HiperLinkButtonForAuthForm = function (name, caption, nowrap) {
    var hiperLinkButton = this.SmallButton(name, null, caption, null, null, null, this.GetStyles("AuthHyperlinkButton"));
    hiperLinkButton.style.maxWidth = "300px";

    if (hiperLinkButton.caption) {
        hiperLinkButton.caption.style.padding = "";
        hiperLinkButton.caption.style.whiteSpace = nowrap ? "nowrap" : "normal";
    }

    return hiperLinkButton;
}