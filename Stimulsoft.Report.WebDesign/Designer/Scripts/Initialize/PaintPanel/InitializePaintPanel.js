﻿
StiMobileDesigner.prototype.InitializePaintPanel = function () {
    var paintPanel = document.createElement("div");
    this.options.paintPanel = paintPanel;
    this.options.mainPanel.appendChild(paintPanel);
    paintPanel.jsObject = this;
    paintPanel.className = "stiDesignerPaintPanel";
    paintPanel.style.cursor = "default";

    paintPanel.style.top = (this.options.toolBar.offsetHeight + this.options.workPanel.offsetHeight + this.options.pagesPanel.offsetHeight) + "px";
    paintPanel.style.bottom = this.options.statusPanel.offsetHeight + "px";
    paintPanel.style.left = this.options.propertiesPanel.offsetWidth + "px";
    paintPanel.style.right = "0px";
    paintPanel.previewPages = [];

    paintPanel.oncontextmenu = function (event) {
        return false;
    }

    paintPanel.clear = function () {
        while (this.childNodes[0]) this.removeChild(this.childNodes[0]);
        this.previewPages = [];
    }

    paintPanel.addPage = function (page) {
        this.appendChild(page);
    }

    paintPanel.removePage = function (page) {
        this.removeChild(page);
    }

    paintPanel.findPageByIndex = function (pageIndex) {
        for (var pageName in this.jsObject.options.report.pages)
            if (this.jsObject.options.report.pages[pageName].properties.pageIndex == pageIndex)
                return this.jsObject.options.report.pages[pageName];

        return false;
    }

    paintPanel.getPagesCount = function (page) {
        count = 0;
        for (var pageName in this.jsObject.options.report.pages) count++
        return parseInt(count);
    }

    paintPanel.showPage = function (page) {
        var pagesButtons = this.jsObject.options.pagesPanel.pagesContainer.pages;
        var pageIndex = this.jsObject.StrToInt(page.properties.pageIndex);
        for (var i = 0; i < pagesButtons.length; i++) {
            pagesButtons[i].setSelected(false);
        }
        if (pageIndex < pagesButtons.length) {
            pagesButtons[pageIndex].setSelected(true);
        }
        if (this.jsObject.options.currentPage != null) {
            this.jsObject.options.currentPage.style.display = "none";
        }
        this.jsObject.PreZoomPage(page);
        page.style.display = "";
        this.jsObject.options.currentPage = page;
        page.setSelected();
        this.jsObject.UpdatePropertiesControls();

        this.jsObject.options.buttons.unitButton.style.display = page.isDashboard ? "none" : "";
    }

    paintPanel.setCopyStyleMode = function (state) {
        this.style.cursor = state ? (this.jsObject.options.jsMode ? "pointer" : "url('" + this.jsObject.options.urlCursorStyleSet + "'), pointer") : "default";
        this.copyStyleMode = state;
        this.jsObject.options.buttons.copyStyleButton.setSelected(state);
        if (state) {
            this.jsObject.options.copyStyleProperties = {};
            this.jsObject.SaveCurrentStylePropertiesToObject(this.jsObject.options.copyStyleProperties);
        }
        else {
            this.jsObject.options.copyStyleProperties = null;
        }
    }

    paintPanel.changeCursorType = function (draw) {
        this.style.cursor = draw
            ? (this.jsObject.options.jsMode ? "crosshair" : ("url('" + this.jsObject.options.urlCursorPen + "'), crosshair"))
            : (this.copyStyleMode ? (this.jsObject.options.jsMode ? "pointer" : "url('" + this.jsObject.options.urlCursorStyleSet + "'), pointer") : "default");
    }

    paintPanel.addPreviewPage = function (pageAttributes) {
        var page = document.createElement("DIV");
        page.style.margin = "10px";
        page.style.display = "inline-block";
        page.style.verticalAlign = "top";
        page.style.padding = pageAttributes["margins"];
        page.style.border = "1px solid gray";
        page.style.color = "#000000";
        page.style.background = pageAttributes["background"];
        page.innerHTML = pageAttributes["content"];
        var pageSizes = pageAttributes["sizes"].split(";");
        var marginsPx = pageAttributes["margins"].split(" ");
        var margins = [];
        for (var i = 0; i < marginsPx.length; i++) {
            margins.push(parseInt(marginsPx[i].replace("px", "")));
        }
        page.pageHeight = parseInt(pageSizes[1]);
        this.appendChild(page);
        this.previewPages.push(page);

        var currentPageHeight = page.offsetHeight - margins[0] - margins[2];
        if (paintPanel.maxHeights[pageSizes[1]] == null || currentPageHeight > paintPanel.maxHeights[pageSizes[1]])
            paintPanel.maxHeights[pageSizes[1]] = currentPageHeight;
    }

    paintPanel.addPreviewPages = function () {
        if (this.jsObject.previewCountPages == 0 || this.jsObject.options.previewPagesArray.length < 3) {
            var errorMessageForm = this.jsObject.options.forms.errorMessageForm || this.jsObject.InitializeErrorMessageForm();
            errorMessageForm.show("Error: Report cannot be rendered!");
        }

        if (this.jsObject.options.previewPagesArray == null) return;
        this.removePreviewPages();
        this.maxHeights = {};
        var count = this.jsObject.options.previewPagesArray.length;

        if (this.jsObject.options.css == null) {
            this.jsObject.options.css = document.createElement("STYLE");
            this.jsObject.options.css.setAttribute('type', 'text/css');
            this.jsObject.options.head.appendChild(this.jsObject.options.css);
        }

        //add pages styles
        if (this.jsObject.options.css.styleSheet) this.jsObject.options.css.styleSheet.cssText = this.jsObject.options.previewPagesArray[count - 2];
        else this.jsObject.options.css.innerHTML = this.jsObject.options.previewPagesArray[count - 2];

        //add chart scripts
        var currChartScripts = document.getElementById("chartScriptMobileDesigner");
        if (currChartScripts) this.jsObject.options.head.removeChild(currChartScripts);

        if (this.jsObject.options.previewPagesArray[count - 1]) {
            var chartScripts = document.createElement("Script");
            chartScripts.setAttribute('type', 'text/javascript');
            chartScripts.id = "chartScriptMobileDesigner";
            chartScripts.textContent = this.jsObject.options.previewPagesArray[count - 1];
            this.jsObject.options.head.appendChild(chartScripts);
        }

        //add page contents
        for (num = 0; num <= count - 3; num++) {
            this.addPreviewPage(this.jsObject.options.previewPagesArray[num]);
        }

        paintPanel.correctHeights();

        if (typeof stiEvalCharts === "function") stiEvalCharts();
    }

    paintPanel.correctHeights = function () {
        for (var i = 0; i < this.childNodes.length; i++) {
            if (this.childNodes[i].pageHeight != null) {
                var height = paintPanel.maxHeights[this.childNodes[i].pageHeight.toString()];
                if (height) this.childNodes[i].style.height = height + "px";
            }
        }
    }

    paintPanel.removePreviewPages = function () {
        for (var i = 0; i < this.previewPages.length; i++) {
            this.removeChild(this.previewPages[i]);
        }
        this.previewPages = [];
    }

    paintPanel.changeVisibleState = function (state) {
        this.style.display = state ? "" : "none";
    }

    paintPanel.onmouseup = function () {
        if (!this.jsObject.options.isTouchClick) paintPanel.action();
    }

    paintPanel.ondblclick = function () {
        if (this.jsObject.options.previewMode) return;
        if (!this.jsObject.options.pageIsDblClick && !this.jsObject.options.pageIsTouched &&
        this.jsObject.options.showFileMenuReportSetup && this.jsObject.options.report) {
            this.jsObject.InitializeReportSetupForm(function (reportSetupForm) {
                reportSetupForm.show();
            });
        }
        this.jsObject.options.pageIsDblClick = false;
    }

    paintPanel.action = function () {
        if (!this.jsObject.options.pageIsTouched && !this.jsObject.options.in_drag && !this.jsObject.options.in_resize &&
            this.jsObject.options.report && !this.jsObject.options.itemInDrag) {
            if (this.jsObject.options.currentPage && this.jsObject.options.selectingRect) {
                this.jsObject.MultiSelectComponents(this.jsObject.options.currentPage);
                this.jsObject.PaintSelectedLines();
            }
            else {
                this.jsObject.options.report.setSelected();
                if (this.jsObject.options.propertiesPanel && this.jsObject.options.showPropertiesGrid) {
                    this.jsObject.options.propertiesPanel.showContainer("Properties");
                }
                this.jsObject.UpdatePropertiesControls();
            }
        }
        this.jsObject.options.pageIsTouched = false;
    }
}