﻿
StiMobileDesigner.prototype.InitializePagePanel = function () {
    var pagePanel = this.ChildWorkPanel("pagePanel", "stiDesignerLayoutPanel");
    pagePanel.style.display = "none";

    pagePanel.mainTable = this.CreateHTMLTable();
    pagePanel.mainTable.addCell(this.PageSetupBlock());
    pagePanel.mainTable.addCell(this.GroupBlockSeparator());
    pagePanel.mainTable.addCell(this.ViewOptionsBlock());
    pagePanel.mainTable.addCell(this.GroupBlockSeparator());
    pagePanel.appendChild(pagePanel.mainTable);

    pagePanel.updateControls = function () {
        var buttons = this.jsObject.options.buttons;
        var designerOptions = this.jsObject.options.report ? this.jsObject.options.report.info : null;

        var buttonNames = ["pagePanelShowGrid", "pagePanelAlignToGrid", "pagePanelShowHeaders", "pagePanelGridMode",
            "groupBlockPageSetupButton", "marginsPage", "orientationPage", "pageSize", "columnsPage", "watermarkPage"];

        var isDashboard = this.jsObject.options.currentPage && this.jsObject.options.currentPage.isDashboard;

        for (var i = 0; i < buttonNames.length; i++) {
            var button = buttons[buttonNames[i]];
            if (button) {
                var isEnabled = designerOptions;
                if (isDashboard && ["marginsPage", "orientationPage", "pageSize", "columnsPage", "watermarkPage",
                    "pagePanelShowHeaders", "groupBlockPageSetupButton"].indexOf(buttonNames[i]) >= 0)
                {
                    isEnabled = false;
                }
                button.setEnabled(isEnabled);
            }
        }

        if (designerOptions) {
            buttons.pagePanelShowGrid.setSelected(designerOptions.showGrid);
            buttons.pagePanelAlignToGrid.setSelected(designerOptions.alignToGrid);
            buttons.pagePanelShowHeaders.setSelected(designerOptions.showHeaders);
            buttons.pagePanelGridMode.image.src = this.jsObject.options.images["ViewOptions.Grid" + designerOptions.gridMode + ".png"];
        }
    }
}

//PageSetup
StiMobileDesigner.prototype.PageSetupBlock = function () {
    var pageSetupGroupBlock = this.GroupBlock("groupBlockPageSetup", this.loc.Toolbars.ToolbarPageSetup, true, null);
    var innerTable = this.GroupBlockInnerTable();
    pageSetupGroupBlock.container.appendChild(innerTable);
    innerTable.style.height = "100%";

    //Margins
    var marginsButton = this.BigButton("marginsPage", null, this.loc.FormPageSetup.Margins, "Margins.png",
        [this.loc.HelpDesigner.Margins, this.HelpLinks["page"]], true, this.GetStyles("StandartBigButton"), null, 60);
    marginsButton.cellImage.style.height = "40px";
    innerTable.addCell(marginsButton).style.padding = "2px";
    var marginsMenu = this.MarginsMenu();

    marginsButton.action = function () {
        marginsMenu.changeVisibleState(!marginsMenu.visible);
    }

    //Orientation
    var orientationButton = this.BigButton("orientationPage", null, this.loc.FormPageSetup.Orientation, "Orientation.png",
        [this.loc.HelpDesigner.Orientation, this.HelpLinks["page"]], true, this.GetStyles("StandartBigButton"), null, 60);
    orientationButton.cellImage.style.height = "40px";
    innerTable.addCell(orientationButton).style.padding = "2px";
    var orientationMenu = this.OrientationMenu();

    orientationButton.action = function () {
        orientationMenu.changeVisibleState(!orientationMenu.visible);
    }

    //PageSize
    var pageSizeButton = this.BigButton("pageSize", null, this.loc.PropertyMain.Size, "Size.png",
        [this.loc.HelpDesigner.PageSize, this.HelpLinks["page"]], true, this.GetStyles("StandartBigButton"), null, 60);
    pageSizeButton.cellImage.style.height = "40px";
    innerTable.addCell(pageSizeButton).style.padding = "2px";
    var pageSizeMenu = this.PageSizeMenu("pageSizeMenu", pageSizeButton);

    pageSizeButton.action = function () {
        pageSizeMenu.changeVisibleState(!pageSizeMenu.visible);
    }

    //Columns
    var columnsButton = this.BigButton("columnsPage", null, this.loc.FormPageSetup.Columns, "Columns.png",
        [this.loc.HelpDesigner.Columns, this.HelpLinks["page"]], true, this.GetStyles("StandartBigButton"), null, 60);
    columnsButton.cellImage.style.height = "40px";
    innerTable.addCell(columnsButton).style.padding = "2px";
    var columnsMenu = this.ColumnsMenu();

    columnsButton.action = function () {
        columnsMenu.changeVisibleState(!columnsMenu.visible);
    }

    //Watermark
    var watermarkButton = this.StandartBigButton("watermarkPage", null, this.loc.PropertyMain.Watermark, "PageWatermark.png", null, 60);
    innerTable.addCell(watermarkButton).style.padding = "2px";

    watermarkButton.action = function () {
        this.jsObject.InitializePageSetupForm(function (pageSetupForm) {
            pageSetupForm.changeVisibleState(true);
            pageSetupForm.setMode("Watermark");
        });
    }

    return pageSetupGroupBlock;
}

//View Options
StiMobileDesigner.prototype.ViewOptionsBlock = function () {
    var viewOptionsGroupBlock = this.GroupBlock("groupBlockViewOptions", this.loc.Toolbars.ToolbarViewOptions);
    var innerTable = this.CreateHTMLTable();
    viewOptionsGroupBlock.container.appendChild(innerTable);
    innerTable.style.height = "100%";

    //ShowGrid
    var showGridButton = this.StandartSmallButton("pagePanelShowGrid", null, this.loc.MainMenu.menuViewShowGrid, "ViewOptions.ShowGrid.png",
        [this.loc.HelpDesigner.menuViewShowGrid, this.HelpLinks["viewOptions"]], null);
    showGridButton.propertyName = "showGrid";
    innerTable.addCell(showGridButton).style.padding = "1px 1px 0px 1px";

    //Show Headers
    var showHeadersButton = this.StandartSmallButton("pagePanelShowHeaders", null, this.loc.MainMenu.menuViewShowHeaders, "ViewOptions.ShowHeaders.png",
        [this.loc.HelpDesigner.menuViewShowHeaders, this.HelpLinks["viewOptions"]], null);
    innerTable.addCell(showHeadersButton).style.padding = "1px 1px 0px 1px";
    showHeadersButton.propertyName = "showHeaders";

    //AlignToGrid
    var alignToGridButton = this.StandartSmallButton("pagePanelAlignToGrid", null, this.loc.MainMenu.menuViewAlignToGrid, "ViewOptions.AlignToGrid.png",
        [this.loc.HelpDesigner.menuViewAlignToGrid, this.HelpLinks["viewOptions"]], null);
    alignToGridButton.propertyName = "alignToGrid";
    innerTable.addCellInNextRow(alignToGridButton).style.padding = "0 1px 0 1px";

    //GridMode
    var gridModeButton = this.StandartSmallButton("pagePanelGridMode", null, this.loc.FormOptions.GridMode, "ViewOptions.GridDots.png",
        [this.loc.HelpDesigner.GridMode, this.HelpLinks["viewOptions"]], "Down");
    innerTable.addCellInNextRow(gridModeButton).style.padding = "0 1px 1px 1px";

    var gridModeMenu = this.GridModeMenu();
    gridModeButton.action = function () { gridModeMenu.changeVisibleState(!gridModeMenu.visible); };

    var changeViewOption = function (button) {
        if (!button.jsObject.options.report) return;
        var designerOptions = button.jsObject.options.report.info;
        button.setSelected(!button.isSelected);
        designerOptions[button.propertyName] = button.isSelected;
        button.jsObject.SendCommandApplyDesignerOptions(designerOptions);
    }

    showGridButton.action = function () { changeViewOption(this); }
    showHeadersButton.action = function () { changeViewOption(this); }
    alignToGridButton.action = function () { changeViewOption(this); }

    return viewOptionsGroupBlock;
}