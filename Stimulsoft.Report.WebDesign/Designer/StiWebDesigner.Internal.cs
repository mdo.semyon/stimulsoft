﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.SaveLoad;
using System;
using System.Collections;
using System.Drawing.Printing;
using System.IO;

#if NETCORE
using Stimulsoft.System.Web.UI;
using Stimulsoft.System.Web.UI.WebControls;
#else
using System.Web.UI;
using System.Web.UI.WebControls;
#endif

namespace Stimulsoft.Report.Web
{
    public partial class StiWebDesigner :
        WebControl,
        INamingContainer
    {
        #region Check file formats

        /// <summary>
        /// Returns true if specified stream contains packed report.
        /// </summary>
        private static bool IsPackedFile(Stream stream)
        {
            if (stream.Length < 4) return false;

            int first = stream.ReadByte();
            int second = stream.ReadByte();
            int third = stream.ReadByte();
            stream.Seek(-3, SeekOrigin.Current);

            if (first == 0x1F && second == 0x8B && third == 0x08) return true;//Gzip
            if (first == 0x50 && second == 0x4B && third == 0x03) return true;//PKZip

            return false;
        }

        /// <summary>
        /// Returns true if specified stream contains encrypted report.
        /// </summary>
        private static bool IsEncryptedFile(Stream stream)
        {
            if (stream.Length < 4) return false;

            int first = stream.ReadByte();
            int second = stream.ReadByte();
            int third = stream.ReadByte();
            stream.Seek(-3, SeekOrigin.Current);

            if (first == 0x6D && second == 0x64 && third == 0x78) return true;  //mdx
            return false;
        }

        /// <summary>
        /// Returns true if specified stream contains packed report.
        /// </summary>
        private static bool IsJsonFile(Stream stream)
        {
            if (stream.Length < 2) return false;

            int first = stream.ReadByte();
            stream.Seek(-1, SeekOrigin.Current);

            if (first == 0x0000007b) return true;//JSON
            return false;
        }

        #endregion
        
        #region Helpers for commands

        internal static StiReport GetReportForDesigner(StiRequestParams requestParams, StiReport currentReport)
        {
            #region Get Report Cloud Server Mode
#if SERVER
            Hashtable designParams = GetDesignerParametersFromCloud(requestParams);
#else
            Hashtable designParams = null;
#endif

            if (requestParams.CloudMode && designParams != null)
            {
#if SERVER
                //if (designParams["reportTemplateItemKey"] != null)
                //{
                //    var getReportForDesignCommand = new StiReportCommands.Design()
                //    {
                //        ReportTemplateItemKey = (string)designParams["reportTemplateItemKey"],
                //        SessionKey = (string)designParams["sessionKey"]
                //    };

                //    if (designParams.ContainsKey("versionKey"))
                //    {
                //        getReportForDesignCommand.VersionKey = (string)designParams["versionKey"];
                //    }
                    
                //    var resultReportForDesignCommand = RunCommand(getReportForDesignCommand) as StiReportCommands.Design;

                //    if (resultReportForDesignCommand.ResultSuccess && resultReportForDesignCommand.ResultReport != null)
                //    {
                //        StiReport newReport = new StiReport();
                //        try
                //        {
                //            newReport.Load(new MemoryStream(resultReportForDesignCommand.ResultReport));
                //            newReport.Info.Zoom = 1;
                //            StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(), newReport);
                //        }
                //        catch (Exception e) { parameters["error"] = e.Message; };

                //        currentReport = newReport;
                //    }
                //}
                //else
                //{
                //    StiReport newReport = new StiReport();
                //    newReport.Info.Zoom = 1;
                //    StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(StiDesignerOptionsHelper.GetDesignerOptions(), newReport);
                //    currentReport = newReport;
                //}
#endif
            }
            #endregion

            #region Get Report Standart Mode
            else
            {
                if (currentReport == null) currentReport = GetNewReport(requestParams);
                Hashtable designerOptions = StiDesignerOptionsHelper.GetDesignerOptions(requestParams.HttpContext);
                StiDesignerOptionsHelper.ApplyDesignerOptionsToReport(designerOptions, currentReport);
                currentReport.Info.Zoom = 1;
                if (String.IsNullOrEmpty(currentReport.ReportFile))
                {
                    currentReport.ReportFile = !string.IsNullOrEmpty(currentReport.ReportName) ? currentReport.ReportName + ".mrt" : "Report.mrt";
                }
            }
            #endregion

            return currentReport;
        }

        internal static StiReport GetNewReport(StiRequestParams requestParams)
        {
            StiReport newReport = new StiReport();
            if (!StiOptions.Engine.FullTrust) newReport.CalculationMode = StiCalculationMode.Interpretation;
            newReport.ReportUnit = (StiReportUnitType)requestParams.GetEnum("defaultUnit", typeof(StiReportUnitType));
            newReport.Pages[0].PaperSize = PaperKind.Custom;
            newReport.Pages[0].PageWidth = newReport.Unit.ConvertFromHInches(827d);
            newReport.Pages[0].PageHeight = newReport.Unit.ConvertFromHInches(1169d);
            newReport.Info.Zoom = 1;

            return newReport;
        }

        internal static StiReport LoadReportFromContent(StiRequestParams requestParams)
        {
            StiReport report = new StiReport();
            MemoryStream stream = new MemoryStream();
            stream.Write(requestParams.Data, 0, requestParams.Data.Length);
            StiReportSLService service = null;
            stream.Position = 0;

            if (requestParams.Designer.Password != null) report.LoadEncryptedReport(stream, requestParams.Designer.Password);
            else if (IsPackedFile(stream)) report.LoadPackedReport(stream);
            else
            {
                if (IsJsonFile(stream)) service = new StiJsonReportSLService();
                else service = new StiXmlReportSLService();

                report.Load(service, stream);
            }
            stream.Close();

            report.Info.Zoom = 1;
            if (requestParams.Designer.FileName != null) report.ReportFile = requestParams.Designer.FileName;
            if (!StiOptions.Engine.FullTrust) report.CalculationMode = StiCalculationMode.Interpretation;

            return report;
        }

        #endregion

        #region URLs (.NET Core)
#if NETCORE
        /// <summary>
        /// Get the URL to load the images of the report designer.
        /// </summary>
        internal static string GetImageUrl(StiRequestParams requestParams, string imageName)
        {
            return imageName.Replace("'", "\\'").Replace("\"", "&quot;");
        }
#endif
        #endregion
    }
}
