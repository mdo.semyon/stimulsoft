﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.Components;
using Stimulsoft.Report.Export;
using System.Collections;
using System.Drawing.Imaging;

namespace Stimulsoft.Report.Web
{
    internal class StiShapeHelper
    {
        public static void GetShapeSampleImage(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var shapeProperties = param["shapeProperties"] as Hashtable;
            var componentName = param["componentName"] as string;

            if (shapeProperties != null)
            {
                var shape = report.GetComponentByName(componentName) as StiShape;
                if (shape != null)
                {
                    var shapeClone = shape.Clone() as StiShape;
                    StiReportEdit.SetShapeTypeProperty(shapeClone, shapeProperties["shapeType"]);
                    StiReportEdit.SetPropertyValue(report, "Brush", shapeClone, shapeProperties["brush"]);
                    StiReportEdit.SetPropertyValue(report, "Style", shapeClone, shapeProperties["shapeBorderStyle"]);
                    StiReportEdit.SetPropertyValue(report, "Size", shapeClone, shapeProperties["size"]);
                    StiReportEdit.SetPropertyValue(report, "BorderColor", shapeClone, shapeProperties["shapeBorderColor"]);
                    shapeClone.Width = report.Unit.ConvertFromHInches(200d);
                    shapeClone.Height = report.Unit.ConvertFromHInches(200d);
                    shapeClone.Report = report;

                    callbackResult["content"] = StiSvgHelper.SaveComponentToString(shapeClone, ImageFormat.Png, 0.75f, 100);
                }
            }
        }
    }
}