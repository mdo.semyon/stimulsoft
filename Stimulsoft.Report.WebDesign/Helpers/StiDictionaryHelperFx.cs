﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Text;
using Stimulsoft.Report.Dictionary;
using System.IO;
using System.Data;
using Stimulsoft.Base.Localization;
using System.Xml;
using System.Collections;
using Stimulsoft.Base;
using System.Globalization;

namespace Stimulsoft.Report.Web
{
    internal class StiDictionaryHelperFx
    {
        /// <summary>
        /// Creating a Dictionary with the specified data.
        /// </summary>
        private static StiDictionary CreateDictionary(StiRequestParams requestParams)
        {
            string dictionaryString = string.Format(
                    "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>" +
                    "<StiSerializer version=\"1.0\" application=\"StiDictionary\">" +
                    "<Databases isList=\"true\" count=\"1\">" +
                    "<Connection Ref=\"1\" type=\"Stimulsoft.Report.Dictionary.{0}\" isKey=\"true\">" +
                    "<Alias>Connection</Alias><Name>Connection</Name><ConnectionString>{1}</ConnectionString>" +
                    "</Connection></Databases>" +
                    "<DataSources isList=\"true\" count=\"1\">" +
                    "<DataSource Ref=\"2\" type=\"Stimulsoft.Report.Dictionary.{2}\" isKey=\"true\">" +
                    "<NameInSource>Connection</NameInSource><Name>DataSource</Name><Alias>DataSource</Alias>" +
                    "<SqlCommand>{3}</SqlCommand><Dictionary isRef=\"0\" /></DataSource></DataSources>" +
                    "</StiSerializer>",
                    requestParams.Dictionary.ConnectionType,
                    requestParams.Dictionary.ConnectionString,
                    requestParams.Dictionary.ConnectionType.Replace("Database", "Source"),
                    string.IsNullOrEmpty(requestParams.Dictionary.Query)
                        ? string.Empty
                        : requestParams.Dictionary.Query.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;")
            );

            Stream stream = new MemoryStream(Encoding.Default.GetBytes(dictionaryString));
            StiDictionary dictionary = new StiDictionary();
            dictionary.Load(stream);

            return dictionary;
        }

        /// <summary>
        /// Getting tables from the specified data source.
        /// </summary>
        private static StiWebActionResult RetrieveColumnsFromDataSet(StiRequestParams requestParams, DataSet dataSet)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                XmlDeclaration declaration = xml.CreateXmlDeclaration("1.0", "utf-8", null);
                xml.AppendChild(declaration);

                XmlElement tables = xml.CreateElement("Tables");
                xml.AppendChild(tables);

                if (dataSet != null)
                {
                    foreach (DataTable table in dataSet.Tables)
                    {
                        XmlElement tableNode = xml.CreateElement(XmlConvert.EncodeName(table.TableName));
                        tables.AppendChild(tableNode);

                        string columns = string.Empty;
                        foreach (DataColumn column in table.Columns)
                        {
                            XmlElement columnNode = xml.CreateElement(XmlConvert.EncodeName(column.ColumnName));
                            columnNode.SetAttribute("type", column.DataType.ToString());
                            tableNode.AppendChild(columnNode);
                        }
                    }
                }

                return StiWebActionResult.StringResult(requestParams, xml.OuterXml);
            }
            catch (Exception ex)
            {
                return StiWebActionResult.ErrorResult(requestParams, ex.Message.ToString());
            }
        }

        /// <summary>
        /// Getting tables from the specified XML data source.
        /// </summary>
        private static StiWebActionResult RetrieveColumnsFromXmlDatabase(StiRequestParams requestParams)
        {
            try
            {
                DataSet dataSet = new DataSet();
                if (!string.IsNullOrEmpty(requestParams.Dictionary.SchemaPath)) dataSet.ReadXmlSchema(requestParams.Dictionary.SchemaPath);
                else dataSet.ReadXml(requestParams.Dictionary.DataPath);

                return RetrieveColumnsFromDataSet(requestParams, dataSet);
            }
            catch (Exception ex)
            {
                return StiWebActionResult.ErrorResult(requestParams, ex.Message.ToString());
            }
        }

        /// <summary>
        /// Getting tables from the specified JSON data source.
        /// </summary>
        private static StiWebActionResult RetrieveColumnsFromJsonDatabase(StiRequestParams requestParams)
        {
            DataSet dataSet = StiJsonToDataSetConverter.GetDataSetFromFile(requestParams.Dictionary.DataPath);
            return RetrieveColumnsFromDataSet(requestParams, dataSet);
        }

        /// <summary>
        /// Prepare the user name and password info class.
        /// </summary>
        private static StiUserNameAndPassword CreateUserPasswordInfo(StiRequestParams requestParams)
        {
            if (string.IsNullOrEmpty(requestParams.Dictionary.UserName) || string.IsNullOrEmpty(requestParams.Dictionary.Password)) return null;
            return new StiUserNameAndPassword(requestParams.Dictionary.UserName, requestParams.Dictionary.Password);
        }

        /// <summary>
        /// Get a columns list of a specified database.
        /// </summary>
        private static StiWebActionResult RetrieveColumnsFromDictionary(StiRequestParams requestParams, StiDictionary dictionary)
        {
            if (dictionary.DataSources.Count == 0) return StiWebActionResult.ErrorResult(requestParams, "Data source not found.");
            StiDataSource dataSource = dictionary.DataSources[0];

            dictionary.Connect(false);
            if (dataSource is StiSqlSource)
            {
                StiDataStoreSource storeSource = dataSource as StiDataStoreSource;
                if (storeSource == null || storeSource.NameInSource == null || storeSource.NameInSource.Trim().Length == 0)
                {
                    return StiWebActionResult.ErrorResult(requestParams, "Unknown data source.");
                }

                StiDataAdapterService adapter = StiDataAdapterService.GetDataAdapter(dataSource);
                if (adapter != null)
                {
                    StiData selectedData = null;
                    foreach (StiData data in dataSource.Dictionary.DataStore)
                    {
                        if (data.Name.ToLower(CultureInfo.InvariantCulture) == storeSource.NameInSource.ToLower(CultureInfo.InvariantCulture))
                        {
                            selectedData = data;
                            break;
                        }
                    }

                    try
                    {
                        // Fill the user name adn passworf for database
                        StiUserNameAndPassword userPasswordInfo = CreateUserPasswordInfo(requestParams);
                        if (StiDictionary.CacheUserNamesAndPasswords == null) StiDictionary.CacheUserNamesAndPasswords = new Hashtable();
                        StiDictionary.CacheUserNamesAndPasswords[requestParams.Dictionary.ConnectionString] = userPasswordInfo;

                        // Do the data columns sync
                        dataSource.Dictionary.SynchronizeColumns(selectedData, dataSource);
                    }
                    catch (Exception ee)
                    {
                        return StiWebActionResult.ErrorResult(requestParams, ee.Message);
                    }
                }

                XmlDocument xml = new XmlDocument();
                XmlDeclaration declaration = xml.CreateXmlDeclaration("1.0", "utf-8", null);
                xml.AppendChild(declaration);

                XmlElement tables = xml.CreateElement("Tables");
                xml.AppendChild(tables);

                XmlElement tableNode = xml.CreateElement("RetrieveColumns");
                tables.AppendChild(tableNode);

                foreach (StiDataColumn column in dataSource.Columns)
                {
                    XmlElement columnNode = xml.CreateElement(XmlConvert.EncodeName(column.Name));
                    columnNode.SetAttribute("type", column.Type.ToString());
                    tableNode.AppendChild(columnNode);
                }

                dictionary.Disconnect();
                return StiWebActionResult.StringResult(requestParams, xml.OuterXml);
            }

            dictionary.Disconnect();
            return StiWebActionResult.ErrorResult(requestParams, "Data source is not SQL source.");
        }

        /// <summary>
        /// Get a list of columns of the database (XML-based source or a specified SQL query).
        /// </summary>
        public static StiWebActionResult RetrieveColumns(StiRequestParams requestParams)
        {
            if (requestParams.Dictionary.ConnectionType == "StiXmlDatabase") return RetrieveColumnsFromXmlDatabase(requestParams);
            if (requestParams.Dictionary.ConnectionType == "StiJsonDatabase") return RetrieveColumnsFromJsonDatabase(requestParams);
            StiDictionary dictionary = CreateDictionary(requestParams);
            return RetrieveColumnsFromDictionary(requestParams, dictionary);
        }
        
        /// <summary>
        /// Testing a database connection.
        /// </summary>
        public static StiWebActionResult TestConnection(StiRequestParams requestParams)
        {
            StiDictionary dictionary = CreateDictionary(requestParams);
            StiDataSource dataSource = dictionary.DataSources[0];
            StiSqlAdapterService adapter = StiDataAdapterService.GetDataAdapter(dataSource) as StiSqlAdapterService;
            if (adapter == null) return StiWebActionResult.ErrorResult(requestParams, requestParams.Dictionary.ConnectionType + " data adapter not found.");

            string result = adapter.TestConnection(requestParams.Dictionary.ConnectionString);
            if (result == StiLocalization.Get("DesignerFx", "ConnectionSuccessfull")) return StiWebActionResult.StringResult(requestParams, "Successfull");
            return StiWebActionResult.ErrorResult(requestParams, result);
        }
    }
}
