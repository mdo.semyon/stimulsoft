﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	Stimulsoft.Report Library										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Report.Dictionary;
using System;
using System.Reflection;

namespace Stimulsoft.Report.Web
{
    internal static class StiDashboardWebDesigner
    {
        #region Methods
        internal static void SetPropertyValue(object owner, string propertyName, object propertyValue)
        {
            var property = owner.GetType().GetProperty(propertyName);

            if (property != null) property.SetValue(owner, propertyValue, null);
        }

        internal static object GetPropertyValue(object owner, string propertyName)
        {
            var property = owner.GetType().GetProperty(propertyName);

            return property != null ? property.GetValue(owner, null) : null;
        }

        internal static object InvoikeStaticMethod(string assemblyName, string className, string methodName)
        {
            return InvoikeStaticMethod(assemblyName, className, methodName, null, null);
        }

        internal static object InvoikeStaticMethod(string assemblyName, string className, string methodName, object[] parameters)
        {
            return InvoikeStaticMethod(assemblyName, className, methodName, parameters, null);
        }

        internal static object InvoikeStaticMethod(string assemblyName, string className, string methodName, object[] parameters, Type[] parametersTypes)
        {
            var type = Type.GetType($"{assemblyName}.{className}, {assemblyName}, {StiVersion.VersionInfo}");
            if (type != null)
            {
                var bindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;

                var method = parametersTypes != null
                        ? type.GetMethod(methodName, bindingFlags, null, parametersTypes, null)
                        : type.GetMethod(methodName, bindingFlags);

                if (method != null)
                    return method.Invoke(null, parameters);
            }

            return null;
        }
        #endregion
    }
}
