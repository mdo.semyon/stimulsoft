#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Meters;
using Stimulsoft.Data.Engine;
using Stimulsoft.Data.Functions;
using Stimulsoft.Data.Helpers;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Export;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Stimulsoft.Report.Web.Heplers.Dashboards
{
    internal class StiTableElementHelper
    {
        #region Fields
        private IStiTableElement tableElement;
        #endregion

        #region Helper Methods
        private Hashtable GetMeterItem(IStiMeter meter)
        {
            Hashtable meterItem = new Hashtable();
            meterItem["typeItem"] = "Meter";
            meterItem["type"] = GetMeterType(meter);
            meterItem["typeIcon"] = GetMeterTypeIcon(meter);
            meterItem["label"] = GetMeterLabel(meter);
            meterItem["expression"] = meter.Expression != null ? StiEncodingHelper.Encode(meter.Expression) : string.Empty;
            meterItem["functions"] = GetMeterFunctions(meter, tableElement.Page as IStiDashboard);
            meterItem["currentFunction"] = StiExpressionHelper.GetFunction(meter.Expression);

            if (GetMeterType(meter) == "Sparklines")
            {
                var sparklinesType = GetSparklinesType(meter);
                meterItem["sparklinesType"] = GetSparklinesType(meter);

                if (sparklinesType == "Line" || sparklinesType == "Area")
                {
                    meterItem["showHighLowPoints"] = (bool)StiDashboardWebDesigner.GetPropertyValue(meter, "ShowHighLowPoints");
                    meterItem["showFirstLastPoints"] = (bool)StiDashboardWebDesigner.GetPropertyValue(meter, "ShowFirstLastPoints");
                }
            }

            return meterItem;
        }

        private ArrayList GetMetersItems()
        {
            var metersItems = new ArrayList();
            foreach (IStiMeter meter in tableElement.GetMeters())
            {
                metersItems.Add(GetMeterItem(meter));
            }

            return metersItems;
        }

        internal static IEnumerable<string> GetMeterFunctions(IStiMeter meter, IStiDashboard dashboard)
        {
            if (meter == null) return null;

            if (meter is IStiDimensionMeter)
            {
                if (StiDataExpressionHelper.IsDateDataColumnInExpression(dashboard, meter.Expression))
                    return Funcs.GetDateDimensionFunctions();
            }
            else if (meter is IStiMeasureMeter && !(GetMeterType(meter) == "Sparklines"))
            {
                if (StiDataExpressionHelper.IsNumericDataColumnInExpression(dashboard, meter.Expression))
                    return Funcs.GetAggregateMeasureFunctions();
                else
                    return Funcs.GetCommonMeasureFunctions();
            }

            return null;
        }

        private Hashtable GetTableElementJSProperties(Hashtable parameters)
        {
            double zoom = StiReportEdit.StrToDouble(parameters["zoom"] as string);

            Hashtable properties = new Hashtable();
            properties["meters"] = GetMetersItems();

            if ((string)parameters["command"] != "GetTableElementProperties")
            {
                properties["svgContent"] = StiEncodingHelper.Encode(GetTableElementSvgContent(tableElement, zoom));
            }

            return properties;
        }

        internal static string GetMeterLabel(IStiMeter meter)
        {
            return StiDashboardWebDesigner.InvoikeStaticMethod("Stimulsoft.Dashboard", "Helpers.StiLabelHelper", "GetLabel", new object[] { meter }) as string;
        }

        private static string GetSparklinesType(IStiMeter meter)
        {
            return StiDashboardWebDesigner.GetPropertyValue(meter, "Type").ToString();
        }

        internal static string GetMeterType(IStiMeter meter)
        {
            var meterType = meter.GetType().Name;

            if (meterType == "StiDimensionColumn") return "Dimension";
            if (meterType == "StiDataBarsColumn") return "DataBars";
            if (meterType == "StiColorScaleColumn") return "ColorScale";
            if (meterType == "StiIndicatorColumn") return "Indicator";
            if (meterType == "StiSparklinesColumn") return "Sparklines";

            return "Measure";
        }

        internal static string GetMeterTypeIcon(IStiMeter meter)
        {
            var meterType = GetMeterType(meter);

            if (meterType == "Sparklines")
            {
                string sparklinesType = GetSparklinesType(meter);

                if (sparklinesType == "Line") return "SparklinesLine";
                if (sparklinesType == "Area") return "SparklinesArea";
                if (sparklinesType == "Column") return "SparklinesColumn";
                if (sparklinesType == "WinLoss") return "SparklinesWinLoss";
            }

            return meterType;
        }

        internal static string GetTableElementSvgContent(IStiTableElement tableElement, double zoom)
        {
            if (tableElement.GetMeters().Count == 0)
                return string.Empty;

            using (var report = new StiReport())
            using (var stream = new MemoryStream())
            {
                report.IsDocument = true;
                report.ReportUnit = StiReportUnitType.HundredthsOfInch;

                var page = report.RenderedPages[0];
                page.Width = ((StiComponent)tableElement).Width;
                page.Height = ((StiComponent)tableElement).Height;
                page.Margins = new StiMargins(0, 0, 0, 0);

                var titleElement = tableElement as IStiTitleElement;
                if (titleElement != null) titleElement.Title = null;

                StiDashboardWebDesigner.InvoikeStaticMethod("Stimulsoft.Dashboard.Export", "StiDashboardExportTools", "RenderSingleElement", new object[] { page, tableElement, null });

                foreach (StiPage finalPage in report.RenderedPages)
                {
                    finalPage.GetComponentsList().ForEach(c => c.Page = finalPage);
                    finalPage.MoveComponentsToPage();
                }

                while (report.RenderedPages.Count > 1)
                {
                    report.RenderedPages.RemoveAt(report.RenderedPages.Count - 1);
                }

                report.ExportDocument(StiExportFormat.ImageSvg, stream, new StiSvgExportSettings());
                var svgContent = Encoding.UTF8.GetString(stream.ToArray());                

                return svgContent.Substring(svgContent.IndexOf("<svg"));
            }
        }
        #endregion

        #region Methods
        public void ExecuteJSCommand(Hashtable parameters, Hashtable callbackResult)
        {
            switch ((string)parameters["command"])
            {
                case "InsertMeters":
                    {
                        InsertMeters(parameters, callbackResult);
                        break;
                    }
                case "RemoveMeter":
                    {
                        RemoveMeter(parameters, callbackResult);
                        break;
                    }
                case "ConvertMeter":
                    {
                        ConvertMeter(parameters, callbackResult);
                        break;
                    }
                case "MoveMeter":
                    {
                        MoveMeter(parameters, callbackResult);
                        break;
                    }
                case "NewMeter":
                    {
                        NewMeter(parameters, callbackResult);
                        break;
                    }
                case "DuplicateMeter":
                    {
                        DuplicateMeter(parameters, callbackResult);
                        break;
                    }
                case "SetFunction":
                    {
                        SetFunction(parameters, callbackResult);
                        break;
                    }
                case "ChangeSparklinesType":
                    {
                        ChangeSparklinesType(parameters, callbackResult);
                        break;
                    }
                case "SetPropertyValue":
                    {
                        SetPropertyValue(parameters, callbackResult);
                        break;
                    }
                case "GetTableElementProperties":
                    {
                        break;
                    }
            }

            callbackResult["tableElement"] = GetTableElementJSProperties(parameters);
        }

        private void InsertMeters(Hashtable parameters, Hashtable callbackResult)
        {
            var draggedItem = parameters["draggedItem"] as Hashtable;
            var draggedItemObject = draggedItem["itemObject"] as Hashtable;
            var draggedColumns = new StiDataColumnsCollection();

            var allColumns = StiDictionaryHelper.GetColumnsByTypeAndNameOfObject(tableElement.Report, draggedItem);
            if (allColumns != null)
            {
                if (draggedItemObject["typeItem"] as string == "Column")
                {
                    var draggedColumn = allColumns[draggedItemObject["name"] as string];
                    if (draggedColumn != null) draggedColumns.Add(draggedColumn);
                }
                else
                {
                    draggedColumns = allColumns;
                }
            }

            foreach (StiDataColumn dataColumn in draggedColumns)
            {
                var tableColumn = StiDataColumnExt.IsNumericType(dataColumn)
                        ? tableElement.GetMeasure(dataColumn)
                        : tableElement.GetDimension(dataColumn);

                tableElement.InsertMeter(parameters["insertIndex"] != null ? Convert.ToInt32(parameters["insertIndex"]) : -1, tableColumn as IStiMeter);
            }
        }

        private void RemoveMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            tableElement.RemoveMeter(itemIndex);
        }

        private void ConvertMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            var meterType = parameters["meterType"] as string;
            var meters = tableElement.GetMeters();
            
            if (itemIndex < meters.Count)
            {                
                var currentMeter = meters[itemIndex];
                var dashboard = tableElement.Page as IStiDashboard;
                object newMeter = null;
                
                if (meterType == "Dimension")
                    newMeter = StiDashboardWebDesigner.InvoikeStaticMethod("Stimulsoft.Dashboard", "Helpers.StiMeterHelper+Table", "GetDimension",
                        new object[] { currentMeter }, new Type[] { currentMeter.GetType() });

                else if(meterType == "Measure")
                    newMeter = StiDashboardWebDesigner.InvoikeStaticMethod("Stimulsoft.Dashboard", "Helpers.StiMeterHelper+Table", "GetMeasure",
                        new object[] { currentMeter, dashboard }, new Type[] { currentMeter.GetType(), dashboard.GetType() });

                else if (meterType == "DataBars")
                    newMeter = StiDashboardWebDesigner.InvoikeStaticMethod("Stimulsoft.Dashboard", "Helpers.StiMeterHelper+Table", "GetDataBars",
                        new object[] { currentMeter, dashboard }, new Type[] { currentMeter.GetType(), dashboard.GetType() });

                else if (meterType == "ColorScale")
                    newMeter = StiDashboardWebDesigner.InvoikeStaticMethod("Stimulsoft.Dashboard", "Helpers.StiMeterHelper+Table", "GetColorScale",
                        new object[] { currentMeter, dashboard }, new Type[] { currentMeter.GetType(), dashboard.GetType() });

                else if (meterType == "Indicator")
                    newMeter = StiDashboardWebDesigner.InvoikeStaticMethod("Stimulsoft.Dashboard", "Helpers.StiMeterHelper+Table", "GetIndicator",
                        new object[] { currentMeter, dashboard }, new Type[] { currentMeter.GetType(), dashboard.GetType() });

                else if (meterType == "Sparklines")
                    newMeter = StiDashboardWebDesigner.InvoikeStaticMethod("Stimulsoft.Dashboard", "Helpers.StiMeterHelper+Table", "GetSparklines",
                        new object[] { currentMeter }, new Type[] { currentMeter.GetType() });

                if (newMeter != null) {
                    tableElement.RemoveMeter(itemIndex);
                    tableElement.InsertMeter(itemIndex, newMeter as IStiMeter);
                }
            }
        }

        private void MoveMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var fromIndex = Convert.ToInt32(parameters["fromIndex"]);
            var toIndex = Convert.ToInt32(parameters["toIndex"]);
            var meters = tableElement.GetMeters();

            if (fromIndex < meters.Count && toIndex < meters.Count)
            {
                var movingMeter = meters[fromIndex];
                tableElement.RemoveMeter(fromIndex);
                tableElement.InsertMeter(toIndex, movingMeter as IStiMeter);
            }
        }

        private void NewMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var insertIndex = Convert.ToInt32(parameters["insertIndex"]);

            switch (parameters["itemType"] as string)
            {
                case "newDimension":
                    {
                        tableElement.InsertNewDimension(insertIndex);
                        break;
                    }
                case "newMeasure":
                    {
                        tableElement.InsertNewMeasure(insertIndex);
                        break;
                    }
            }
        }

        private void DuplicateMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            var meters = tableElement.GetMeters();
            if (itemIndex < meters.Count)
            {
                var cloneMeter = (meters[itemIndex] as ICloneable).Clone();
                var insertIndex = itemIndex < meters.Count - 1 ? itemIndex + 1 : -1;
                tableElement.InsertMeter(insertIndex, cloneMeter as IStiMeter);
                callbackResult["insertIndex"] = insertIndex;
            }
        }

        private void SetFunction(Hashtable parameters, Hashtable callbackResult)
        {
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            var meters = tableElement.GetMeters();
            if (itemIndex < meters.Count)
            {
                var currentMeter = meters[itemIndex] as IStiMeter;
                currentMeter.Expression = StiExpressionHelper.ReplaceFunction(currentMeter.Expression, parameters["function"] as string);
            }
        }

        private void ChangeSparklinesType(Hashtable parameters, Hashtable callbackResult)
        {
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            var meters = tableElement.GetMeters();

            if (itemIndex < meters.Count)
            {
                var property = meters[itemIndex].GetType().GetProperty("Type");
                if (property != null)
                    property.SetValue(meters[itemIndex], Enum.Parse(property.PropertyType, parameters["sparklinesType"] as string), null);
            }
        }

        private void SetPropertyValue(Hashtable parameters, Hashtable callbackResult)
        {
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            var meters = tableElement.GetMeters();

            if (itemIndex < meters.Count)
            {
                var propertyName = parameters["propertyName"] as string;

                if (propertyName == "Exspression")
                    meters[itemIndex].Expression = StiEncodingHelper.DecodeString(parameters["propertyValue"] as string);
                else
                    StiDashboardWebDesigner.SetPropertyValue(meters[itemIndex], propertyName, parameters["propertyValue"]);
            }
        }

        public static void CreateTableElementFromDictionary(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            string currentPageName = (string)param["pageName"];
            double zoom = StiReportEdit.StrToDouble((string)param["zoom"]);
            Hashtable point = param["point"] as Hashtable;            
            StiPage currentPage = report.Pages[currentPageName];

            var newComponent = StiDashboardHelper.CreateDashboardElement(report, "StiTableElement");
            double compWidth = newComponent.DefaultClientRectangle.Size.Width;
            double compHeight = newComponent.DefaultClientRectangle.Size.Height;
            double compXPos = StiReportEdit.StrToDouble((string)point["x"]);
            double compYPos = StiReportEdit.StrToDouble((string)point["y"]);

            if (compXPos + currentPage.Margins.Left + compWidth * 2 > currentPage.Width)
            {
                compXPos = currentPage.Width - currentPage.Margins.Left - compWidth * 2;
            }

            StiReportEdit.AddComponentToPage(newComponent, currentPage);
            RectangleD compRect = new RectangleD(new PointD(compXPos, compYPos), new SizeD(compWidth, compHeight));
            StiReportEdit.SetComponentRect(newComponent, compRect);
            if (param["lastStyleProperties"] != null) StiReportEdit.SetAllProperties(newComponent, param["lastStyleProperties"] as ArrayList);

            #region Add Data Columns to Table Element
            var draggedItem = param["draggedItem"] as Hashtable;
            var allColumns = StiDictionaryHelper.GetColumnsByTypeAndNameOfObject(report, draggedItem);
            var tableElement = newComponent as IStiTableElement;

            if (allColumns != null && tableElement != null) 
            {   
                foreach (StiDataColumn dataColumn in allColumns)
                {   
                    tableElement.CreateMeter(dataColumn);
                }
            }
            #endregion

            Hashtable componentProps = new Hashtable();
            componentProps["name"] = newComponent.Name;
            componentProps["typeComponent"] = "StiTableElement";
            componentProps["componentRect"] = StiReportEdit.GetComponentRect(newComponent);
            componentProps["parentName"] = StiReportEdit.GetParentName(newComponent);
            componentProps["parentIndex"] = StiReportEdit.GetParentIndex(newComponent).ToString();
            componentProps["componentIndex"] = StiReportEdit.GetComponentIndex(newComponent).ToString();
            componentProps["childs"] = StiReportEdit.GetAllChildComponents(newComponent);
            componentProps["svgContent"] = StiReportEdit.GetSvgContent(newComponent, zoom);
            componentProps["pageName"] = currentPage.Name;
            componentProps["properties"] = StiReportEdit.GetAllProperties(newComponent);
            componentProps["rebuildProps"] = StiReportEdit.GetPropsRebuildPage(report, currentPage);

            callbackResult["newComponent"] = componentProps;
        }
        #endregion

        #region Constructor
        public StiTableElementHelper(IStiTableElement tableElement)
        {
            this.tableElement = tableElement;
        }
        #endregion   
    }
}
