#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Meters;
using Stimulsoft.Data.Helpers;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Maps;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace Stimulsoft.Report.Web.Heplers.Dashboards
{
    internal class StiMapElementHelper
    {
        #region Fields
        private IStiMapElement mapElement;
        #endregion

        #region Helper Methods
        private Hashtable GetMapElementJSProperties(Hashtable parameters)
        {
            double zoom = StiReportEdit.StrToDouble(parameters["zoom"] as string);

            Hashtable properties = new Hashtable();
            properties["name"] = mapElement.Name;
            properties["mapMode"] = mapElement.MapMode;
            properties["mapType"] = mapElement.MapType;
            properties["mapID"] = mapElement.MapID;
            properties["showValue"] = mapElement.ShowValue;
            properties["colorEach"] = mapElement.ColorEach;
            properties["displayNameType"] = mapElement.ShowName;
            properties["dataFrom"] = mapElement.DataFrom;
            properties["mapData"] = GetMapDataForJS(mapElement);
            properties["meters"] = GetMetersHash(mapElement.GetMeters());

            if ((string)parameters["command"] != "GetMapElementProperties")
            {
                properties["svgContent"] = StiEncodingHelper.Encode(GetMapElementSvgContent(mapElement, zoom));
            }

            return properties;
        }

        private Hashtable GetMeterHashItem(IStiMeter meter)
        {
            Hashtable meterItem = new Hashtable();
            meterItem["typeItem"] = "Meter";
            meterItem["type"] = StiTableElementHelper.GetMeterType(meter);
            meterItem["typeIcon"] = StiTableElementHelper.GetMeterTypeIcon(meter);
            meterItem["label"] = StiTableElementHelper.GetMeterLabel(meter);
            meterItem["expression"] = meter.Expression != null ? StiEncodingHelper.Encode(meter.Expression) : string.Empty;
            meterItem["functions"] = StiTableElementHelper.GetMeterFunctions(meter, mapElement.Page as IStiDashboard);
            meterItem["currentFunction"] = StiExpressionHelper.GetFunction(meter.Expression);

            return meterItem;
        }

        private Hashtable GetMetersHash(List<IStiMeter>meters)
        {
            var metersItems = new Hashtable();
            foreach (IStiMeter meter in meters)
            {
                metersItems[meter.GetType().Name] = GetMeterHashItem(meter);
            }

            return metersItems;
        }

        public static ArrayList GetMapDataForJS(IStiMapElement mapElement)
        {
            ArrayList resultData = new ArrayList();
            List<StiMapData> mapData = mapElement.GetMapData().OrderBy(x => x.Key).ToList();
            bool allowGroup = AllowGroup(mapElement);
            bool allowColor = AllowColor(mapElement);

            foreach (var data in mapData)
            {
                var row = new Hashtable();
                row["key"] = data.Key;
                row["name"] = data.Name;
                row["value"] = data.Value;
                if (allowGroup)
                    row["group"] = data.Group;
                if (allowColor)
                    row["color"] = data.Color;

                resultData.Add(row);
            }

            return resultData;
        }

        internal static StiMap CreateMapComponentFromMapElement(IStiMapElement mapElement)
        {
            var mapComponent = new StiMap();
            mapComponent.MapID = mapElement.MapID;
            mapComponent.MapType = mapElement.MapType;
            mapComponent.MapMode = mapElement.MapMode;
            mapComponent.MapData = StiJsonHelper.SaveToJsonString(mapElement.GetMapData());
            mapComponent.ShowValue = mapElement.ShowValue;
            mapComponent.ColorEach = mapElement.ColorEach;
            mapComponent.DisplayNameType = mapElement.ShowName;
            mapComponent.DataFrom = mapElement.DataFrom;
            mapComponent.Page = mapElement.Page;
            mapComponent.Width = (mapElement as StiComponent).Width;
            mapComponent.Height = (mapElement as StiComponent).Height;

            if (mapElement is IStiBrush)
                mapComponent.Brush = ((IStiBrush)mapElement).Brush.Clone() as StiBrush;

            foreach (var style in StiOptions.Services.MapStyles.Where(x => x.AllowDashboard))
            {
                if (style.StyleIdent == mapElement.Style)
                {
                    mapComponent.MapStyle = style.StyleId;
                    break;
                }
            }

            return mapComponent;
        }

        internal static string GetMapElementSvgContent(IStiMapElement mapElement, double zoom)
        {
            var mapComponent = CreateMapComponentFromMapElement(mapElement);
            mapComponent.Height -= 30;

            #region Check map content
            var contentIsEmpty = true;

            if (mapElement.MapMode == StiMapMode.Choropleth && mapElement.DataFrom == StiMapSource.Manual)
            {
                contentIsEmpty = false;
            }
            else
            {
                var meters = mapElement.GetMeters();
                foreach (IStiMeter meter in meters)
                {
                    if ((mapElement.MapMode == StiMapMode.Online && (
                            meter.GetType().Name == "StiLatitudeMapMeter" ||
                            meter.GetType().Name == "StiLongitudeMapMeter")) ||
                        ((mapElement.MapMode == StiMapMode.Choropleth && mapElement.DataFrom == StiMapSource.DataColumns) &&
                            (meter.GetType().Name == "StiColorMapMeter" ||
                            meter.GetType().Name == "StiGroupMapMeter" ||
                            meter.GetType().Name == "StiKeyMapMeter" ||
                            meter.GetType().Name == "StiNameMapMeter" ||
                            meter.GetType().Name == "StiValueMapMeter")))
                    {
                        contentIsEmpty = false;
                        break;
                    }
                }
            }
            #endregion

            return contentIsEmpty ? String.Empty : StiSvgHelper.SaveComponentToString(mapComponent, ImageFormat.Png, 0.75f, (float)(zoom * 100));
        }

        private static bool AllowGroup(IStiMapElement mapElement)
        {
            return (mapElement.MapType == StiMapType.Group || mapElement.MapType == StiMapType.HeatmapWithGroup);
        }

        private static bool AllowColor(IStiMapElement mapElement)
        {
            return (mapElement.MapType == StiMapType.Individual);
        }

        private IStiMeter GetMeterByContainerName(string containerName)
        {
            foreach (var meter in mapElement.GetMeters())
            {
                if (meter.GetType().Name == string.Format("Sti{0}MapMeter", containerName))
                {
                    return meter;
                }
            }

            return null;
        }
        #endregion

        #region Methods
        public void ExecuteJSCommand(Hashtable parameters, Hashtable callbackResult)
        {
            switch ((string)parameters["command"])
            {

                case "SetExpression":
                    {
                        SetExpression(parameters, callbackResult);
                        break;
                    }
                case "SetDataColumn":
                    {
                        SetDataColumn(parameters, callbackResult);
                        break;
                    }
                case "MoveMeter":
                    {
                        MoveMeter(parameters, callbackResult);
                        break;
                    }
                case "SetFunction":
                    {
                        SetFunction(parameters, callbackResult);
                        break;
                    }
                case "SetProperties":
                    {
                        SetProperties(parameters, callbackResult);
                        break;
                    }
                case "NewItem":
                    {
                        CreateNewItem(parameters, callbackResult);
                        break;
                    }
                case "UpdateMapData":
                    {
                        UpdateMapData(parameters, callbackResult);
                        break;
                    }                    
                case "GetMapElementProperties":
                    {
                        break;
                    }
            }

            callbackResult["mapElement"] = GetMapElementJSProperties(parameters);
        }

        private void SetExpression(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterByContainerName(parameters["containerName"] as string);
            if (meter != null)
            {
                meter.Expression = StiEncodingHelper.DecodeString(parameters["expressionValue"] as string);
            }
        }

        private void CreateNewItem(Hashtable parameters, Hashtable callbackResult)
        {
            switch (parameters["containerName"] as string)
            {
                case "Key":
                    {
                        mapElement.CreateNewKeyMeter();
                        break;
                    }
                case "Name":
                    {
                        mapElement.CreateNewNameMeter();
                        break;
                    }
                case "Value":
                    {
                        mapElement.CreateNewValueMeter();
                        break;
                    }
                case "Group":
                    {
                        mapElement.CreateNewGroupMeter();
                        break;
                    }
                case "Color":
                    {
                        mapElement.CreateNewColorMeter();
                        break;
                    }
                case "Latitude":
                    {
                        mapElement.CreateNewLatitudeMeter();
                        break;
                    }
                case "Longitude":
                    {
                        mapElement.CreateNewLongitudeMeter();
                        break;
                    }
            }
        }

        private void SetFunction(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterByContainerName(parameters["containerName"] as string);
            if (meter != null)
            {
                meter.Expression = StiExpressionHelper.ReplaceFunction(meter.Expression, parameters["function"] as string);
            }
        }

        private void SetDataColumn(Hashtable parameters, Hashtable callbackResult)
        {
            var dataColumnObject = parameters["dataColumnObject"] as Hashtable;
            StiDataColumn dataColumn = null;
            var allColumns = dataColumnObject != null ? StiDictionaryHelper.GetColumnsByTypeAndNameOfObject(mapElement.Report, dataColumnObject) : null;
            if (allColumns != null) dataColumn = allColumns[dataColumnObject["name"] as string];
                        
            switch (parameters["containerName"] as string)
            {
                case "Key":
                    {
                        mapElement.AddKeyMeter(dataColumn);
                        break;
                    }
                case "Name":
                    {
                        mapElement.AddNameMeter(dataColumn);
                        break;
                    }
                case "Value":
                    {
                        mapElement.AddValueMeter(dataColumn);
                        break;
                    }
                case "Group":
                    {
                        mapElement.AddGroupMeter(dataColumn);
                        break;
                    }
                case "Color":
                    {
                        mapElement.AddColorMeter(dataColumn);
                        break;
                    }
                case "Latitude":
                    {
                        mapElement.AddLatitudeMeter(dataColumn);
                        break;
                    }
                case "Longitude":
                    {
                        mapElement.AddLongitudeMeter(dataColumn);
                        break;
                    }
            }
        }

        private void MoveMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var toContainerName = parameters["toContainerName"] as string;
            var fromContainerName = parameters["fromContainerName"] as string;
            IStiMeter movingMeter = null;

            #region Get and Remove meter
            switch (fromContainerName)
            {
                case "Key":
                    {
                        movingMeter = mapElement.GetKeyMeter();
                        mapElement.RemoveKeyMeter();
                        break;
                    }
                case "Name":
                    {
                        movingMeter = mapElement.GetNameMeter();
                        mapElement.RemoveNameMeter();
                        break;
                    }
                case "Value":
                    {
                        movingMeter = mapElement.GetValueMeter();
                        mapElement.RemoveValueMeter();
                        break;
                    }
                case "Group":
                    {
                        movingMeter = mapElement.GetGroupMeter();
                        mapElement.RemoveGroupMeter();
                        break;
                    }
                case "Color":
                    {
                        movingMeter = mapElement.GetColorMeter();
                        mapElement.RemoveColorMeter();
                        break;
                    }
                case "Latitude":
                    {
                        movingMeter = mapElement.GetLatitudeMeter();
                        mapElement.RemoveLatitudeMeter();
                        break;
                    }
                case "Longitude":
                    {
                        movingMeter = mapElement.GetLongitudeMeter();
                        mapElement.RemoveLongitudeMeter();
                        break;
                    }
            }
            #endregion

            #region Insert meter
            if (movingMeter != null)
            {
                switch (toContainerName)
                {
                    case "Key":
                        {
                            mapElement.AddKeyMeter(movingMeter);
                            break;
                        }
                    case "Name":
                        {
                            mapElement.AddNameMeter(movingMeter);
                            break;
                        }
                    case "Value":
                        {
                            mapElement.AddValueMeter(movingMeter);
                            break;
                        }
                    case "Group":
                        {
                            mapElement.AddGroupMeter(movingMeter);
                            break;
                        }
                    case "Color":
                        {
                            mapElement.AddColorMeter(movingMeter);
                            break;
                        }
                    case "Latitude":
                        {
                            mapElement.AddLatitudeMeter(movingMeter);
                            break;
                        }
                    case "Longitude":
                        {
                            mapElement.AddLongitudeMeter(movingMeter);
                            break;
                        }
                }
            }
            #endregion
        }

        private void SetProperties(Hashtable parameters, Hashtable callbackResult)
        {
            var props = parameters["properties"] as Hashtable;
            mapElement.MapMode = (StiMapMode)Enum.Parse(typeof(StiMapMode), props["mapMode"] as string);
            mapElement.MapID = (StiMapID)Enum.Parse(typeof(StiMapID), props["mapID"] as string);
            mapElement.MapType = (StiMapType)Enum.Parse(typeof(StiMapType), props["mapType"] as string);
            mapElement.ShowValue = (bool)props["showValue"];
            mapElement.ColorEach = (bool)props["colorEach"];
            mapElement.ShowName = (StiDisplayNameType)Enum.Parse(typeof(StiDisplayNameType), props["displayNameType"] as string);
            mapElement.DataFrom = (StiMapSource)Enum.Parse(typeof(StiMapSource), props["dataFrom"] as string);
            
            if (parameters["updateMapData"] != null)
            {
                mapElement.MapData = null;
                callbackResult["mapData"] = GetMapDataForJS(mapElement);
            }
        }

        private void UpdateMapData(Hashtable parameters, Hashtable callbackResult)
        {
            List<StiMapData> mapData = mapElement.GetMapData().OrderBy(x => x.Key).ToList();
            var rowData = mapData[Convert.ToInt32(parameters["rowIndex"])];

            switch (parameters["columnName"] as string)
            {
                case "name":
                    rowData.Name = parameters["textValue"] as string;
                    break;

                case "value":
                    rowData.Value = parameters["textValue"] as string;
                    break;

                case "group":
                    rowData.Group = parameters["textValue"] as string;
                    break;

                case "color":
                    rowData.Color = parameters["textValue"] as string;
                    break;
            }

            mapElement.MapData = StiJsonHelper.SaveToJsonString(mapData);
        }

        public static ArrayList GetStylesContent(StiReport report, Hashtable param)
        {
            ArrayList stylesContent = new ArrayList();
            var component = report.GetComponentByName((string)param["componentName"]) as IStiMapElement;

            if (component != null)
            {
                var mapComponent = CreateMapComponentFromMapElement(component);

                foreach (var style in StiOptions.Services.MapStyles.Where(x => x.AllowDashboard))
                {
                    if (style is StiMapStyleFX)
                    {
                        mapComponent.MapStyle = ((Maps.StiMapStyleFX)style).StyleId;
                        Hashtable content = new Hashtable();
                        var width = 130;
                        var height = 50;
                        content["image"] = StiMapHelper.GetMapSampleImage(mapComponent, width, height, 1f);
                        content["ident"] = style.StyleIdent;
                        content["width"] = width;
                        content["height"] = height;
                        stylesContent.Add(content);
                    }
                }
            }

            return stylesContent;
        }
        #endregion

        #region Constructor
        public StiMapElementHelper(IStiMapElement mapElement)
        {
            this.mapElement = mapElement;
        }
        #endregion   
    }
}
