#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Meters;
using Stimulsoft.Data.Helpers;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Export;
using System;
using System.Collections;
using System.IO;
using System.Text;

namespace Stimulsoft.Report.Web.Heplers.Dashboards
{
    internal class StiPivotElementHelper
    {
        #region Fields
        private IStiPivotElement pivotElement;
        #endregion

        #region Helper Methods
        private Hashtable GetPivotElementJSProperties(Hashtable parameters)
        {
            double zoom = StiReportEdit.StrToDouble(parameters["zoom"] as string);

            Hashtable properties = new Hashtable();
            properties["name"] = pivotElement.Name;
            properties["meters"] = GetMetersHash();

            if ((string)parameters["command"] != "GetPivotElementProperties")
            {
                properties["svgContent"] = StiEncodingHelper.Encode(GetPivotElementSvgContent(pivotElement, zoom));
            }

            return properties;
        }
                
        private Hashtable GetMeterHashItem(IStiMeter meter)
        {
            Hashtable meterItem = new Hashtable();
            meterItem["typeItem"] = "Meter";
            meterItem["type"] = StiTableElementHelper.GetMeterType(meter);
            meterItem["typeIcon"] = StiTableElementHelper.GetMeterTypeIcon(meter);
            meterItem["label"] = StiTableElementHelper.GetMeterLabel(meter);
            meterItem["expression"] = meter.Expression != null ? StiEncodingHelper.Encode(meter.Expression) : string.Empty;
            meterItem["functions"] = StiTableElementHelper.GetMeterFunctions(meter, pivotElement.Page as IStiDashboard);
            meterItem["currentFunction"] = StiExpressionHelper.GetFunction(meter.Expression);

            return meterItem;
        }

        private Hashtable GetMetersHash()
        {
            var meters = pivotElement.GetMeters();
            var metersItems = new Hashtable();
            metersItems["columns"] = new ArrayList();
            metersItems["rows"] = new ArrayList();
            metersItems["summaries"] = new ArrayList();

            foreach (IStiMeter meter in meters)
            {
                switch (meter.GetType().Name)
                {
                    case "StiPivotColumn":
                        {
                            ((ArrayList)metersItems["columns"]).Add(GetMeterHashItem(meter));
                            break;
                        }
                    case "StiPivotRow":
                        {
                            ((ArrayList)metersItems["rows"]).Add(GetMeterHashItem(meter));
                            break;
                        }
                    case "StiPivotSummary":
                        {
                            ((ArrayList)metersItems["summaries"]).Add(GetMeterHashItem(meter));
                            break;
                        }
                }
            }

            return metersItems;
        }

        internal static string GetPivotElementSvgContent(IStiPivotElement pivotElement, double zoom)
        {
            if (pivotElement.GetMeters().Count == 0)
                return string.Empty;

            using (var report = new StiReport())
            using (var stream = new MemoryStream())
            {
                report.IsDocument = true;
                report.ReportUnit = StiReportUnitType.HundredthsOfInch;

                var page = report.RenderedPages[0];
                page.Width = ((StiComponent)pivotElement).Width;
                page.Height = ((StiComponent)pivotElement).Height;
                page.Margins = new StiMargins(0, 0, 0, 0);

                var titleElement = pivotElement as IStiTitleElement;
                if (titleElement != null) titleElement.Title = null;

                StiDashboardWebDesigner.InvoikeStaticMethod("Stimulsoft.Dashboard.Export", "StiDashboardExportTools", "RenderSingleElement", new object[] { page, pivotElement, null });
                StiPivotCreatingCache.Clear();

                foreach (StiPage finalPage in report.RenderedPages)
                {
                    finalPage.GetComponentsList().ForEach(c => c.Page = finalPage);
                    finalPage.MoveComponentsToPage();
                }

                while (report.RenderedPages.Count > 1)
                {
                    report.RenderedPages.RemoveAt(report.RenderedPages.Count - 1);
                }

                report.ExportDocument(StiExportFormat.ImageSvg, stream, new StiSvgExportSettings());
                var svgContent = Encoding.UTF8.GetString(stream.ToArray());

                return svgContent.Substring(svgContent.IndexOf("<svg"));
            }
        }
        #endregion

        #region Methods
        public void ExecuteJSCommand(Hashtable parameters, Hashtable callbackResult)
        {
            switch ((string)parameters["command"])
            {

                case "InsertMeters":
                    {
                        InsertMeters(parameters, callbackResult);
                        break;
                    }
                case "RemoveMeter":
                    {
                        RemoveMeter(parameters, callbackResult);
                        break;
                    }
                case "MoveMeter":
                    {
                        MoveMeter(parameters, callbackResult);
                        break;
                    }
                case "DuplicateMeter":
                    {
                        DuplicateMeter(parameters, callbackResult);
                        break;
                    }
                case "SetExpression":
                    {
                        SetExpression(parameters, callbackResult);
                        break;
                    }
                case "SetFunction":
                    {
                        SetFunction(parameters, callbackResult);
                        break;
                    }
                case "NewItem":
                    {
                        CreateNewItem(parameters, callbackResult);
                        break;
                    }
                case "GetPivotElementProperties":
                    {
                        break;
                    }
            }

            callbackResult["pivotElement"] = GetPivotElementJSProperties(parameters);
        }

        private IStiMeter GetMeterFromContainer(string containerName, int index)
        {
            switch (containerName)
            {
                case "columns": return pivotElement.GetColumnByIndex(index);
                case "rows": return pivotElement.GetRowByIndex(index);
                case "summaries": return pivotElement.GetSummaryByIndex(index);
            }

            return null;
        }

        private void SetExpression(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterFromContainer(parameters["containerName"] as string, Convert.ToInt32(parameters["itemIndex"]));
            if (meter != null)
            {
                meter.Expression = StiEncodingHelper.DecodeString(parameters["expressionValue"] as string);
            }
        }

        private void SetFunction(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterFromContainer(parameters["containerName"] as string, Convert.ToInt32(parameters["itemIndex"]));
            if (meter != null)
            {
                meter.Expression = StiExpressionHelper.ReplaceFunction(meter.Expression, parameters["function"] as string);
            }
        }

        private void RemoveMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var containerName = parameters["containerName"] as string;
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            switch (containerName)
            {
                case "columns":
                    {
                        pivotElement.RemoveColumn(itemIndex);
                        break;
                    }
                case "rows":
                    {
                        pivotElement.RemoveRow(itemIndex);
                        break;
                    }

                case "summaries":
                    {
                        pivotElement.RemoveSummary(itemIndex);
                        break;
                    }
            }            
        }

        private void MoveMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var fromIndex = Convert.ToInt32(parameters["fromIndex"]);
            var toIndex = Convert.ToInt32(parameters["toIndex"]);
            var toContainerName = parameters["toContainerName"] as string;
            var fromContainerName = parameters["fromContainerName"] as string;
            IStiMeter movingMeter = null;

            #region Get and Remove meter
            switch (fromContainerName)
            {
                case "columns":
                    {
                        movingMeter = pivotElement.GetColumnByIndex(fromIndex);
                        if (movingMeter != null)
                            pivotElement.RemoveColumn(fromIndex);
                        break;
                    }
                case "rows":
                    {
                        movingMeter = pivotElement.GetRowByIndex(fromIndex);
                        if (movingMeter != null)
                            pivotElement.RemoveRow(fromIndex);
                        break;
                    }

                case "summaries":
                    {
                        movingMeter = pivotElement.GetSummaryByIndex(fromIndex);
                        if (movingMeter != null)
                            pivotElement.RemoveSummary(fromIndex);
                        break;
                    }
            }
            #endregion

            #region Insert meter
            if (movingMeter != null)
            {
                switch (toContainerName)
                {
                    case "columns":
                        {
                            var columnMeter = pivotElement.GetColumn(movingMeter);
                            if (columnMeter != null)
                                pivotElement.InsertColumn(toIndex, columnMeter);
                            break;
                        }
                    case "rows":
                        {
                            var rowMeter = pivotElement.GetRow(movingMeter);
                            if (rowMeter != null)
                                pivotElement.InsertRow(toIndex, rowMeter);
                            break;
                        }

                    case "summaries":
                        {
                            var summaryMeter = pivotElement.GetSummary(movingMeter);
                            if (summaryMeter != null)
                                pivotElement.InsertSummary(toIndex, summaryMeter);
                            break;
                        }
                }
            }
            #endregion
        }

        private void DuplicateMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var containerName = parameters["containerName"] as string;
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            callbackResult["insertIndex"] = itemIndex + 1;

            switch (containerName)
            {
                case "columns":
                    {
                        var pivotColumn = pivotElement.GetColumnByIndex(itemIndex);
                        if (pivotColumn != null)
                        {
                            var cloneColumn = (pivotColumn as ICloneable).Clone();
                            pivotElement.InsertColumn(itemIndex + 1, cloneColumn as IStiMeter);
                        }
                        break;
                    }
                case "rows":
                    {
                        var pivotRow = pivotElement.GetRowByIndex(itemIndex);
                        if (pivotRow != null)
                        {
                            var cloneRow = (pivotRow as ICloneable).Clone();
                            pivotElement.InsertRow(itemIndex + 1, cloneRow as IStiMeter);
                        }
                        break;
                    }

                case "summaries":
                    {
                        var pivotSummary = pivotElement.GetSummaryByIndex(itemIndex);
                        if (pivotSummary != null)
                        {
                            var cloneSummary = (pivotSummary as ICloneable).Clone();
                            pivotElement.InsertSummary(itemIndex + 1, cloneSummary as IStiMeter);
                        }
                        break;
                    }
            }
        }

        private void InsertMeters(Hashtable parameters, Hashtable callbackResult)
        {
            var containerName = parameters["containerName"] as string;
            var draggedItem = parameters["draggedItem"] as Hashtable;
            var draggedItemObject = draggedItem["itemObject"] as Hashtable;
            var draggedColumns = new StiDataColumnsCollection();
            var insertIndex = parameters["insertIndex"] != null ? Convert.ToInt32(parameters["insertIndex"]) : -1;

            var allColumns = StiDictionaryHelper.GetColumnsByTypeAndNameOfObject(pivotElement.Report, draggedItem);
            if (allColumns != null)
            {
                if (draggedItemObject["typeItem"] as string == "Column")
                {
                    var draggedColumn = allColumns[draggedItemObject["name"] as string];
                    if (draggedColumn != null) draggedColumns.Add(draggedColumn);
                }
                else
                {
                    draggedColumns = allColumns;
                }
            }

            foreach (StiDataColumn dataColumn in draggedColumns)
            {
                switch (containerName)
                {
                    case "columns":
                        {
                            var pivotColumn = pivotElement.GetColumn(dataColumn);
                            if (pivotColumn != null)
                                pivotElement.InsertColumn(insertIndex, pivotColumn);
                            break;
                        }
                    case "rows":
                        {
                            var pivotRow = pivotElement.GetRow(dataColumn);
                            if (pivotRow != null)
                                pivotElement.InsertRow(insertIndex, pivotRow);
                            break;
                        }

                    case "summaries":
                        {
                            var pivotSummary = pivotElement.GetSummary(dataColumn);
                            if (pivotSummary != null)
                                pivotElement.InsertSummary(insertIndex, pivotSummary);
                            break;
                        }
                }
            }
        }

        private void CreateNewItem(Hashtable parameters, Hashtable callbackResult)
        {
            switch (parameters["containerName"] as string)
            {
                case "columns":
                    {
                        pivotElement.CreateNewColumn();
                        break;
                    }
                case "rows":
                    {
                        pivotElement.CreateNewRow();
                        break;
                    }
                case "summaries":
                    {
                        pivotElement.CreateNewSummary();
                        break;
                    }
            }
        }
        #endregion

        #region Constructor
        public StiPivotElementHelper(IStiPivotElement pivotElement)
        {
            this.pivotElement = pivotElement;
        }
        #endregion   
    }
}
