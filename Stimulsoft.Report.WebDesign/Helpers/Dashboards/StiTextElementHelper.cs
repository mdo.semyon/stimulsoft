#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dashboard;
using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;

namespace Stimulsoft.Report.Web.Heplers.Dashboards
{
    internal class StiTextElementHelper
    {
        #region Fields
        private IStiTextElement textElement;
        #endregion

        #region Helper Methods
        private Hashtable GetTextElementJSProperties()
        {
            Hashtable properties = new Hashtable();
            properties["name"] = textElement.Name;
            properties["text"] = StiEncodingHelper.Encode(textElement.Text);
            properties["border"] = StiReportEdit.BorderToStr(((IStiBorder)textElement).Border);
            properties["brush"] = StiReportEdit.BrushToStr(((IStiBrush)textElement).Brush);
            properties["horAlignment"] = ((IStiTextHorAlignment)textElement).HorAlignment.ToString();
            properties["vertAlignment"] = ((IStiVertAlignment)textElement).VertAlignment.ToString();
            properties["foreColor"] = StiReportEdit.GetStringFromColor(((IStiForeColor)textElement).ForeColor);
            properties["font"] = StiReportEdit.FontToStr(((IStiTextFont)textElement).GetFont());
            properties["svgContent"] = StiEncodingHelper.Encode(GetTextElementSvgContent(textElement));

            return properties;
        }

        internal static string GetTextElementSvgContent(IStiTextElement textElement)
        {
            var text = textElement.Text;

            #region StiAutoSizeHtmlTextHelper.Measure
            var rect = (textElement as StiComponent).ClientRectangle;
            rect.X = 0;
            rect.Y = 0;
            rect.Height = 1000;

            var options = new StiTextOptions
            {
                Angle = 0f,
                HotkeyPrefix = HotkeyPrefix.None,
                LineLimit = false,
                RightToLeft = false,
                Trimming = StringTrimming.None,
                WordWrap = false
            };
            var textScale = ((IStiElement)textElement).Zoom;

            var bmp = new Bitmap(1, 1);
            var g = Graphics.FromImage(bmp);

            SizeD size;
            using (var font = new Font("Arial", 12))
            {
                size = StiTextRenderer.MeasureText(g, text, font, rect, 1,
                    options.WordWrap, options.RightToLeft, textScale, options.Angle,
                    options.Trimming, options.LineLimit, true, options);
            }
            #endregion

            rect.Height = (textElement as StiComponent).ClientRectangle.Height;

            var factorX = size.Width > rect.Width ? rect.Width / size.Width : 1d;
            var factorY = size.Height > rect.Height ? rect.Height / size.Height : 1d;
            var factor = Math.Min(factorX, factorY);

            if (factor < 1d)
            {
                rect = new RectangleD(
                    rect.X + (rect.Width - size.Width * factor) / 2,
                    rect.Y + (rect.Height - size.Height * factor) / 2,
                    size.Width * factor, size.Height * factor);
            }
            
            var textComp = new StiText();
            textComp.ClientRectangle = rect;
            textComp.Page = textElement.Page;
            textComp.Text = text;
            textComp.AllowHtmlTags = true;
            textComp.HorAlignment = textElement is IStiTextHorAlignment ? ((IStiTextHorAlignment)textElement).HorAlignment : StiTextHorAlignment.Left;
            textComp.VertAlignment = textElement is IStiVertAlignment ? ((IStiVertAlignment)textElement).VertAlignment : StiVertAlignment.Top;

            return Export.StiSvgHelper.SaveComponentToString(textComp, ImageFormat.Png, 0.75f, 100);
        }
        #endregion

        #region Methods
        public void ExecuteJSCommand(Hashtable parameters, Hashtable callbackResult)
        {
            switch ((string)parameters["command"])
            {
                case "SetProperty":
                    {
                        SetProperty(parameters, callbackResult);
                        break;
                    }
            }

            callbackResult["textElement"] = GetTextElementJSProperties();
        }

        private void SetProperty(Hashtable parameters, Hashtable callbackResult)
        {
            switch (parameters["propertyName"] as string)
            {
                case "text":
                    {
                        textElement.Text = StiEncodingHelper.DecodeString(parameters["propertyValue"] as string);
                        break;
                    }
                case "fontName":
                    {
                        ((IStiTextFont)textElement).SetFontName(parameters["propertyValue"] as string);
                        break;
                    }
                case "fontSize":
                    {
                        ((IStiTextFont)textElement).SetFontSize((float)Convert.ToDouble(parameters["propertyValue"]));
                        break;
                    }
                case "horAlignment":
                    {
                        ((IStiTextHorAlignment)textElement).HorAlignment = (StiTextHorAlignment)Enum.Parse(typeof(StiTextHorAlignment), parameters["propertyValue"] as string);
                        break;
                    }
            }
        }

        public static void CreateTextElementFromDictionary(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            string currentPageName = (string)param["pageName"];
            double zoom = StiReportEdit.StrToDouble((string)param["zoom"]);
            Hashtable point = (Hashtable)param["point"];
            Hashtable itemObject = (Hashtable)param["itemObject"];
            StiPage currentPage = report.Pages[currentPageName];

            var newComponent = StiDashboardHelper.CreateDashboardElement(report, "StiTextElement");
            double compWidth = newComponent.DefaultClientRectangle.Size.Width;
            double compHeight = newComponent.DefaultClientRectangle.Size.Height;
            double compXPos = StiReportEdit.StrToDouble((string)point["x"]);
            double compYPos = StiReportEdit.StrToDouble((string)point["y"]);

            if (compXPos + currentPage.Margins.Left + compWidth * 2 > currentPage.Width)
            {
                compXPos = currentPage.Width - currentPage.Margins.Left - compWidth * 2;
            }

            StiReportEdit.AddComponentToPage(newComponent, currentPage);
            RectangleD compRect = new RectangleD(new PointD(compXPos, compYPos), new SizeD(compWidth, compHeight));
            StiReportEdit.SetComponentRect(newComponent, compRect);
            ((IStiTextElement)newComponent).Text = StiEncodingHelper.DecodeString((string)itemObject["fullName"]);
            if (param["lastStyleProperties"] != null) StiReportEdit.SetAllProperties(newComponent, param["lastStyleProperties"] as ArrayList);

            Hashtable componentProps = new Hashtable();
            componentProps["name"] = newComponent.Name;
            componentProps["typeComponent"] = "StiTextElement";
            componentProps["componentRect"] = StiReportEdit.GetComponentRect(newComponent);
            componentProps["parentName"] = StiReportEdit.GetParentName(newComponent);
            componentProps["parentIndex"] = StiReportEdit.GetParentIndex(newComponent).ToString();
            componentProps["componentIndex"] = StiReportEdit.GetComponentIndex(newComponent).ToString();
            componentProps["childs"] = StiReportEdit.GetAllChildComponents(newComponent);
            componentProps["svgContent"] = StiReportEdit.GetSvgContent(newComponent, zoom);
            componentProps["pageName"] = currentPage.Name;
            componentProps["properties"] = StiReportEdit.GetAllProperties(newComponent);
            componentProps["rebuildProps"] = StiReportEdit.GetPropsRebuildPage(report, currentPage);

            callbackResult["newComponent"] = componentProps;
        }
        #endregion

        #region Constructor
        public StiTextElementHelper(IStiTextElement textElement)
        {
            this.textElement = textElement;
        }
        #endregion   
    }
}