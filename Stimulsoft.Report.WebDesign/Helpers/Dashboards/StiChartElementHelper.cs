#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Meters;
using Stimulsoft.Data.Helpers;
using Stimulsoft.Report.Chart;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Export;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;

namespace Stimulsoft.Report.Web.Heplers.Dashboards
{
    internal class StiChartElementHelper
    {
        #region Fields
        private IStiChartElement chartElement;
        #endregion

        #region Helper Methods
        private Hashtable GetChartElementJSProperties(Hashtable parameters)
        {
            double zoom = StiReportEdit.StrToDouble(parameters["zoom"] as string);

            Hashtable properties = new Hashtable();
            properties["name"] = chartElement.Name;
            properties["meters"] = GetMetersHash();
            properties["seriesTypes"] = chartElement.GetChartSeriesTypes();

            if ((string)parameters["command"] != "GetChartElementProperties")
            {
                properties["svgContent"] = StiEncodingHelper.Encode(GetChartElementSvgContent(chartElement, zoom));
            }

            return properties;
        }
                
        private Hashtable GetMeterHashItem(IStiMeter meter)
        {
            Hashtable meterItem = new Hashtable();
            meterItem["typeItem"] = "Meter";
            meterItem["type"] = StiTableElementHelper.GetMeterType(meter);
            meterItem["typeIcon"] = StiTableElementHelper.GetMeterTypeIcon(meter);
            meterItem["label"] = StiTableElementHelper.GetMeterLabel(meter);
            meterItem["expression"] = meter.Expression != null ? StiEncodingHelper.Encode(meter.Expression) : string.Empty;
            meterItem["functions"] = StiTableElementHelper.GetMeterFunctions(meter, chartElement.Page as IStiDashboard);
            meterItem["currentFunction"] = StiExpressionHelper.GetFunction(meter.Expression);

            return meterItem;
        }

        private Hashtable GetMetersHash()
        {
            var meters = chartElement.GetOriginalMeters();
            var metersItems = new Hashtable();
            metersItems["values"] = new ArrayList();
            metersItems["arguments"] = new ArrayList();
            metersItems["weights"] = new ArrayList();
            metersItems["series"] = new ArrayList();

            foreach (IStiMeter meter in meters)
            {
                switch (meter.GetType().Name)
                {
                    case "StiValueChartMeter":
                        {
                            var valueMeterItem = GetMeterHashItem(meter);
                            valueMeterItem["seriesType"] = StiDashboardWebDesigner.GetPropertyValue(meter, "SeriesType");
                            ((ArrayList)metersItems["values"]).Add(valueMeterItem);
                            break;
                        }
                    case "StiArgumentChartMeter":
                        {
                            ((ArrayList)metersItems["arguments"]).Add(GetMeterHashItem(meter));
                            break;
                        }
                    case "StiWeightChartMeter":
                        {
                            ((ArrayList)metersItems["weights"]).Add(GetMeterHashItem(meter));
                            break;
                        }
                    case "StiSeriesChartMeter":
                        {
                            ((ArrayList)metersItems["series"]).Add(GetMeterHashItem(meter));
                            break;
                        }
                }
            }

            return metersItems;
        }

        internal static string GetChartElementSvgContent(IStiChartElement chartElement, double zoom)
        {
            if (chartElement.GetMeters().Count == 0)
                return string.Empty;

            var chartComponent = chartElement.GetChartComponent();
            chartComponent.Height -= 30;

            return StiSvgHelper.SaveComponentToString(chartComponent, ImageFormat.Png, 0.75f, (float)(zoom * 100));
        }
        #endregion

        #region Methods
        public void ExecuteJSCommand(Hashtable parameters, Hashtable callbackResult)
        {
            switch ((string)parameters["command"])
            {

                case "InsertMeters":
                    {
                        InsertMeters(parameters, callbackResult);
                        break;
                    }
                case "RemoveMeter":
                    {
                        RemoveMeter(parameters, callbackResult);
                        break;
                    }
                case "MoveMeter":
                    {
                        MoveMeter(parameters, callbackResult);
                        break;
                    }
                case "DuplicateMeter":
                    {
                        DuplicateMeter(parameters, callbackResult);
                        break;
                    }
                case "SetExpression":
                    {
                        SetExpression(parameters, callbackResult);
                        break;
                    }
                case "SetFunction":
                    {
                        SetFunction(parameters, callbackResult);
                        break;
                    }
                case "SetSeriesType":
                    {
                        SetSeriesType(parameters, callbackResult);
                        break;
                    }
                case "NewItem":
                    {
                        CreateNewItem(parameters, callbackResult);
                        break;
                    }
                case "GetChartElementProperties":
                    {
                        break;
                    }
            }

            callbackResult["chartElement"] = GetChartElementJSProperties(parameters);
        }

        private IStiMeter GetMeterFromContainer(string containerName, int index)
        {
            switch (containerName)
            {
                case "values": return chartElement.GetValueByIndex(index);
                case "arguments": return chartElement.GetArgumentByIndex(index);
                case "weights": return chartElement.GetWeightByIndex(index);
                case "series": return chartElement.GetSeries();
            }

            return null;
        }

        private void SetExpression(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterFromContainer(parameters["containerName"] as string, Convert.ToInt32(parameters["itemIndex"]));
            if (meter != null)
            {
                meter.Expression = StiEncodingHelper.DecodeString(parameters["expressionValue"] as string);
            }
        }

        private void SetFunction(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterFromContainer(parameters["containerName"] as string, Convert.ToInt32(parameters["itemIndex"]));
            if (meter != null)
            {
                meter.Expression = StiExpressionHelper.ReplaceFunction(meter.Expression, parameters["function"] as string);
            }
        }

        private void RemoveMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var containerName = parameters["containerName"] as string;
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            switch (containerName)
            {
                case "values":
                    {
                        chartElement.RemoveValue(itemIndex);
                        break;
                    }
                case "arguments":
                    {
                        chartElement.RemoveArgument(itemIndex);
                        break;
                    }

                case "weights":
                    {
                        chartElement.RemoveWeight(itemIndex);
                        break;
                    }

                case "series":
                    {
                        chartElement.RemoveSeries();
                        break;
                    }
            }            
        }

        private void MoveMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var fromIndex = Convert.ToInt32(parameters["fromIndex"]);
            var toIndex = Convert.ToInt32(parameters["toIndex"]);
            var toContainerName = parameters["toContainerName"] as string;
            var fromContainerName = parameters["fromContainerName"] as string;
            IStiMeter movingMeter = null;

            #region Get and Remove meter
            switch (fromContainerName)
            {
                case "values":
                    {
                        movingMeter = chartElement.GetValueByIndex(fromIndex);
                        if (movingMeter != null)
                            chartElement.RemoveValue(fromIndex);
                        break;
                    }
                case "arguments":
                    {
                        movingMeter = chartElement.GetArgumentByIndex(fromIndex);
                        if (movingMeter != null)
                            chartElement.RemoveArgument(fromIndex);
                        break;
                    }

                case "weights":
                    {
                        movingMeter = chartElement.GetWeightByIndex(fromIndex);
                        if (movingMeter != null)
                            chartElement.RemoveWeight(fromIndex);
                        break;
                    }
                case "series":
                    {
                        movingMeter = chartElement.GetSeries();
                        if (movingMeter != null)
                            chartElement.RemoveSeries();
                        break;
                    }
            }
            #endregion

            #region Insert meter
            if (movingMeter != null)
            {
                switch (toContainerName)
                {
                    case "values":
                        {
                            var valueMeter = chartElement.GetValue(movingMeter);
                            if (valueMeter != null)
                            {
                                if (parameters["oldSeriesType"] != null)
                                {
                                    var property = valueMeter.GetType().GetProperty("SeriesType");
                                    if (property != null) property.SetValue(valueMeter, Enum.Parse(property.PropertyType, parameters["oldSeriesType"] as string), null);
                                }
                                chartElement.InsertValue(toIndex, valueMeter);
                            }
                            break;
                        }
                    case "arguments":
                        {
                            var argumentsMeter = chartElement.GetArgument(movingMeter);
                            if (argumentsMeter != null)
                                chartElement.InsertArgument(toIndex, argumentsMeter);
                            break;
                        }

                    case "weights":
                        {
                            var weightsMeter = chartElement.GetWeight(movingMeter);
                            if (weightsMeter != null)
                                chartElement.InsertWeight(toIndex, weightsMeter);
                            break;
                        }
                    case "series":
                        {
                            var seriesMeter = chartElement.GetSeries(movingMeter);
                            if (seriesMeter != null)
                                chartElement.InsertSeries(seriesMeter);
                            break;
                        }
                }
            }
            #endregion
        }

        private void DuplicateMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var containerName = parameters["containerName"] as string;
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            callbackResult["insertIndex"] = itemIndex + 1;

            switch (containerName)
            {
                case "values":
                    {
                        var valueMeter = chartElement.GetValueByIndex(itemIndex);
                        if (valueMeter != null)
                        {
                            var cloneValue = (valueMeter as ICloneable).Clone();
                            chartElement.InsertValue(itemIndex + 1, cloneValue as IStiMeter);
                        }
                        break;
                    }
                case "arguments":
                    {
                        var argumentMeter = chartElement.GetArgumentByIndex(itemIndex);
                        if (argumentMeter != null)
                        {
                            var cloneArgument = (argumentMeter as ICloneable).Clone();
                            chartElement.InsertArgument(itemIndex + 1, cloneArgument as IStiMeter);
                        }
                        break;
                    }

                case "weights":
                    {
                        var weightMeter = chartElement.GetWeightByIndex(itemIndex);
                        if (weightMeter != null)
                        {
                            var cloneWeight = (weightMeter as ICloneable).Clone();
                            chartElement.InsertWeight(itemIndex + 1, cloneWeight as IStiMeter);
                        }
                        break;
                    }
            }
        }

        private void InsertMeters(Hashtable parameters, Hashtable callbackResult)
        {
            var containerName = parameters["containerName"] as string;
            var draggedItem = parameters["draggedItem"] as Hashtable;
            var draggedItemObject = draggedItem["itemObject"] as Hashtable;
            var draggedColumns = new StiDataColumnsCollection();
            var insertIndex = parameters["insertIndex"] != null ? Convert.ToInt32(parameters["insertIndex"]) : -1;

            var allColumns = StiDictionaryHelper.GetColumnsByTypeAndNameOfObject(chartElement.Report, draggedItem);
            if (allColumns != null)
            {
                if (draggedItemObject["typeItem"] as string == "Column")
                {
                    var draggedColumn = allColumns[draggedItemObject["name"] as string];
                    if (draggedColumn != null) draggedColumns.Add(draggedColumn);
                }
                else
                {
                    draggedColumns = allColumns;
                }
            }

            foreach (StiDataColumn dataColumn in draggedColumns)
            {
                switch (containerName)
                {
                    case "values":
                        {
                            var valueMeter = chartElement.GetValue(dataColumn);
                            if (valueMeter != null)
                                chartElement.InsertValue(insertIndex, valueMeter);
                            break;
                        }
                    case "arguments":
                        {
                            var argumentMeter = chartElement.GetArgument(dataColumn);
                            if (argumentMeter != null)
                                chartElement.InsertArgument(insertIndex, argumentMeter);
                            break;
                        }

                    case "weights":
                        {
                            var weightMeter = chartElement.GetWeight(dataColumn);
                            if (weightMeter != null)
                                chartElement.InsertWeight(insertIndex, weightMeter);
                            break;
                        }

                    case "series":
                        {
                            chartElement.AddSeries(dataColumn);
                            break;
                        }
                }
            }
        }

        private void CreateNewItem(Hashtable parameters, Hashtable callbackResult)
        {
            switch (parameters["containerName"] as string)
            {
                case "values":
                    {                        
                        var valueMeter = chartElement.CreateNewValue();
                        if (parameters["oldSeriesType"] != null)
                        {
                            var property = valueMeter.GetType().GetProperty("SeriesType");
                            if (property != null) property.SetValue(valueMeter, Enum.Parse(property.PropertyType, parameters["oldSeriesType"] as string), null);
                        }
                        break;
                    }
                case "arguments":
                    {
                        chartElement.CreateNewArgument();
                        break;
                    }
                case "weights":
                    {
                        chartElement.CreateNewWeight();
                        break;
                    }
                case "series":
                    {
                        chartElement.CreateNewSeries();
                        break;
                    }
            }
        }

        private void SetSeriesType(Hashtable parameters, Hashtable callbackResult)
        {   
            var seriesType = parameters["seriesType"] as string;
            var oldIsBubble = (bool)parameters["oldIsBubble"];
            var itemIndex = Convert.ToInt32(parameters["itemIndex"]);
            var valueMeter = chartElement.GetValueByIndex(itemIndex);
            if (valueMeter != null)
            {
                var property = valueMeter.GetType().GetProperty("SeriesType");
                if (property != null) property.SetValue(valueMeter, Enum.Parse(property.PropertyType, seriesType), null);
            }

            if (oldIsBubble && seriesType != "Bubble")
                chartElement.ConvertFromBubble();

            if (!oldIsBubble && seriesType == "Bubble")
                chartElement.ConvertToBubble();
        }

        public static ArrayList GetStylesContent(StiReport report, Hashtable param)
        {
            ArrayList stylesContent = new ArrayList();
            var component = report.GetComponentByName((string)param["componentName"]) as IStiChartElement;
            if (component != null)
            {
                StiChart chart = StiChartHelper.CloneChart(new StiChart());
                chart.Page = component.Page;
                var chartStyles = StiOptions.Services.ChartStyles.Where(x => x.AllowDashboard);

                foreach (var style in chartStyles)
                {
                    chart.Style = (Stimulsoft.Report.Chart.StiChartStyle)style.Clone();
                    chart.Core.ApplyStyle(chart.Style);

                    int width = 138;
                    int height = 67;

                    Hashtable content = new Hashtable();
                    content["image"] = StiChartHelper.GetChartSampleImage(chart, width, height, 1f);
                    content["ident"] = style.StyleIdent;
                    content["width"] = width;
                    content["height"] = height;
                    stylesContent.Add(content);
                }
            }

            return stylesContent;
        }
        #endregion

        #region Constructor
        public StiChartElementHelper(IStiChartElement chartElement)
        {
            this.chartElement = chartElement;
        }
        #endregion   
    }
}
