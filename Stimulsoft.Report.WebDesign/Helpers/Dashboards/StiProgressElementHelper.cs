#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Helpers;
using Stimulsoft.Base.Meters;
using Stimulsoft.Data.Helpers;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Export;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace Stimulsoft.Report.Web.Heplers.Dashboards
{
    internal class StiProgressElementHelper
    {
        #region Fields
        private IStiProgressElement progressElement;
        #endregion

        #region Helper Methods
        private Hashtable GetProgressElementJSProperties(Hashtable parameters)
        {
            double zoom = StiReportEdit.StrToDouble(parameters["zoom"] as string);

            Hashtable properties = new Hashtable();
            properties["name"] = progressElement.Name;
            properties["mode"] = progressElement.Mode;
            properties["meters"] = GetMetersHash(progressElement.GetMeters());

            if ((string)parameters["command"] != "GetProgressElementProperties")
            {
                properties["svgContent"] = StiEncodingHelper.Encode(GetProgressElementSvgContent(progressElement, zoom));
            }

            return properties;
        }

        private Hashtable GetMeterHashItem(IStiMeter meter)
        {
            Hashtable meterItem = new Hashtable();
            meterItem["typeItem"] = "Meter";
            meterItem["type"] = StiTableElementHelper.GetMeterType(meter);
            meterItem["typeIcon"] = StiTableElementHelper.GetMeterTypeIcon(meter);
            meterItem["label"] = StiTableElementHelper.GetMeterLabel(meter);
            meterItem["expression"] = meter.Expression != null ? StiEncodingHelper.Encode(meter.Expression) : string.Empty;
            meterItem["functions"] = StiTableElementHelper.GetMeterFunctions(meter, progressElement.Page as IStiDashboard);
            meterItem["currentFunction"] = StiExpressionHelper.GetFunction(meter.Expression);

            return meterItem;
        }

        private Hashtable GetMetersHash(List<IStiMeter>meters)
        {
            var metersItems = new Hashtable();
            foreach (IStiMeter meter in meters)
            {
                metersItems[meter.GetType().Name] = GetMeterHashItem(meter);
            }

            return metersItems;
        }

        internal static string GetProgressElementSvgContent(IStiProgressElement progressElement, double zoom)
        {
            if (progressElement.GetMeters().Count == 0)
                return string.Empty;

            return "";
        }
        #endregion

        #region Methods
        public void ExecuteJSCommand(Hashtable parameters, Hashtable callbackResult)
        {
            switch ((string)parameters["command"])
            {

                case "SetExpression":
                    {
                        SetExpression(parameters, callbackResult);
                        break;
                    }
                case "SetDataColumn":
                    {
                        SetDataColumn(parameters, callbackResult);
                        break;
                    }
                case "MoveMeter":
                    {
                        MoveMeter(parameters, callbackResult);
                        break;
                    }
                case "SetFunction":
                    {
                        SetFunction(parameters, callbackResult);
                        break;
                    }
                case "SetPropertyValue":
                    {
                        SetPropertyValue(parameters, callbackResult);
                        break;
                    }
                case "NewItem":
                    {
                        CreateNewItem(parameters, callbackResult);
                        break;
                    }
                case "GetProgressElementProperties":
                    {
                        break;
                    }
            }

            callbackResult["progressElement"] = GetProgressElementJSProperties(parameters);
        }

        private IStiMeter GetMeterByContainerName(string containerName)
        {
            foreach (var meter in progressElement.GetMeters())
            {
                if (meter.GetType().Name == string.Format("Sti{0}ProgressMeter", containerName))
                {
                    return meter;
                }
            }

            return null;
        }

        private void SetExpression(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterByContainerName(parameters["containerName"] as string);
            if (meter != null)
            {
                meter.Expression = StiEncodingHelper.DecodeString(parameters["expressionValue"] as string);
            }
        }

        private void SetFunction(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterByContainerName(parameters["containerName"] as string);
            if (meter != null)
            {
                meter.Expression = StiExpressionHelper.ReplaceFunction(meter.Expression, parameters["function"] as string);
            }
        }

        private void MoveMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var toContainerName = parameters["toContainerName"] as string;
            var fromContainerName = parameters["fromContainerName"] as string;
            IStiMeter movingMeter = null;

            #region Get and Remove meter
            switch (fromContainerName)
            {
                case "Value":
                    {
                        movingMeter = progressElement.GetValue();
                        progressElement.RemoveValue();
                        break;
                    }
                case "Target":
                    {
                        movingMeter = progressElement.GetTarget();
                        progressElement.RemoveTarget();
                        break;
                    }
                case "Series":
                    {
                        movingMeter = progressElement.GetSeries();
                        progressElement.RemoveSeries();
                        break;
                    }
            }
            #endregion

            #region Insert meter
            if (movingMeter != null)
            {
                switch (toContainerName)
                {
                    case "Value":
                        {
                            progressElement.AddValue(movingMeter);
                            break;
                        }
                    case "Target":
                        {
                            progressElement.AddTarget(movingMeter);
                            break;
                        }
                    case "Series":
                        {
                            progressElement.AddSeries(movingMeter);
                            break;
                        }
                }
            }
            #endregion
        }

        private void CreateNewItem(Hashtable parameters, Hashtable callbackResult)
        {
            switch (parameters["containerName"] as string)
            {
                case "Value":
                    {
                        progressElement.CreateNewValue();
                        break;
                    }
                case "Target":
                    {
                        progressElement.CreateNewTarget();
                        break;
                    }
                case "Series":
                    {
                        progressElement.CreateNewSeries();
                        break;
                    }
            }
        }

        private void SetDataColumn(Hashtable parameters, Hashtable callbackResult)
        {
            var dataColumnObject = parameters["dataColumnObject"] as Hashtable;
            StiDataColumn dataColumn = null;
            var allColumns = dataColumnObject != null ? StiDictionaryHelper.GetColumnsByTypeAndNameOfObject(progressElement.Report, dataColumnObject) : null;
            if (allColumns != null) dataColumn = allColumns[dataColumnObject["name"] as string];
            
            switch (parameters["containerName"] as string)
            {
                case "Value":
                    {
                        progressElement.AddValue(dataColumn);
                        break;
                    }
                case "Target":
                    {
                        progressElement.AddTarget(dataColumn);
                        break;
                    }
                case "Series":
                    {
                        progressElement.AddSeries(dataColumn);
                        break;
                    }
            }
        }

        private void SetPropertyValue(Hashtable parameters, Hashtable callbackResult)
        {
            var propertyName = parameters["propertyName"] as string;
            var propertyValue = parameters["propertyValue"] as string;

            switch (propertyName)
            {
                case "Mode":
                    {
                        progressElement.Mode = (StiProgressElementMode)Enum.Parse(typeof(StiProgressElementMode), propertyValue);
                        break;
                    }
            }
        }

        public static string GetProgressSampleImage(IStiProgressElement progressElement, int width, int height, float zoom)
        {
            var svgData = new StiSvgData()
            {
                X = 0,
                Y = 0,
                Width = width,
                Height = height,
                Component = progressElement as StiComponent
            };

            var sb = new StringBuilder();

            using (var ms = new StringWriter(sb))
            {
                var writer = new XmlTextWriter(ms);

                writer.WriteStartElement("svg");
                writer.WriteAttributeString("version", "1.1");
                writer.WriteAttributeString("baseProfile", "full");

                writer.WriteAttributeString("xmlns", "http://www.w3.org/2000/svg");
                writer.WriteAttributeString("xmlns:xlink", "http://www.w3.org/1999/xlink");
                writer.WriteAttributeString("xmlns:ev", "http://www.w3.org/2001/xml-events");

                writer.WriteAttributeString("height", svgData.Height.ToString());
                writer.WriteAttributeString("width", svgData.Width.ToString());

                //StiChartSvgHelper.WriteChart(writer, svgData, zoom, false); //TO DO

                writer.WriteFullEndElement();
                writer.Flush();
                ms.Flush();
                writer.Close();
                ms.Close();
            }

            return sb.ToString();
        }

        public static ArrayList GetStylesContent(StiReport report, Hashtable param)
        {
            ArrayList stylesContent = new ArrayList();
            var progressElement = report.GetComponentByName((string)param["componentName"]) as IStiProgressElement;
            if (progressElement != null)
            {
                var cloneProgressElement = (progressElement as StiComponent).Clone() as IStiProgressElement;
                int width = 130;
                int height = 50;

                foreach (var style in StiOptions.Services.Dashboards.ProgressStyles)
                {
                    cloneProgressElement.Style = style.Ident;

                    Hashtable content = new Hashtable();
                    content["image"] = GetProgressSampleImage(progressElement, width, height, 1f);
                    content["ident"] = style.Ident;
                    content["width"] = width;
                    content["height"] = height;
                    stylesContent.Add(content);
                }
            }

            return stylesContent;
        }
        #endregion

        #region Constructor
        public StiProgressElementHelper(IStiProgressElement progressElement)
        {
            this.progressElement = progressElement;
        }
        #endregion   
    }
}
