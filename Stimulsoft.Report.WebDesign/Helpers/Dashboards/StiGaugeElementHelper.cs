#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Meters;
using Stimulsoft.Data.Helpers;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.Gauge.Primitives;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Gauge;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;

namespace Stimulsoft.Report.Web.Heplers.Dashboards
{
    internal class StiGaugeElementHelper
    {
        #region Fields
        private IStiGaugeElement gaugeElement;
        #endregion

        #region Helper Methods
        private Hashtable GetGaugeElementJSProperties(Hashtable parameters)
        {
            double zoom = StiReportEdit.StrToDouble(parameters["zoom"] as string);

            Hashtable properties = new Hashtable();
            properties["name"] = gaugeElement.Name;
            properties["type"] = gaugeElement.Type;
            properties["calculationMode"] = gaugeElement.CalculationMode;
            properties["minimum"] = StiReportEdit.DoubleToStr(gaugeElement.Minimum);
            properties["maximum"] = StiReportEdit.DoubleToStr(gaugeElement.Maximum);
            properties["meters"] = GetMetersHash(gaugeElement.GetMeters());

            if ((string)parameters["command"] != "GetGaugeElementProperties")
            {
                properties["svgContent"] = StiEncodingHelper.Encode(GetGaugeElementSvgContent(gaugeElement, zoom));
            }

            return properties;
        }

        private Hashtable GetMeterHashItem(IStiMeter meter)
        {
            Hashtable meterItem = new Hashtable();
            meterItem["typeItem"] = "Meter";
            meterItem["type"] = StiTableElementHelper.GetMeterType(meter);
            meterItem["typeIcon"] = StiTableElementHelper.GetMeterTypeIcon(meter);
            meterItem["label"] = StiTableElementHelper.GetMeterLabel(meter);
            meterItem["expression"] = meter.Expression != null ? StiEncodingHelper.Encode(meter.Expression) : string.Empty;
            meterItem["functions"] = StiTableElementHelper.GetMeterFunctions(meter, gaugeElement.Page as IStiDashboard);
            meterItem["currentFunction"] = StiExpressionHelper.GetFunction(meter.Expression);

            return meterItem;
        }

        private Hashtable GetMetersHash(List<IStiMeter>meters)
        {
            var metersItems = new Hashtable();
            foreach (IStiMeter meter in meters)
            {
                metersItems[meter.GetType().Name] = GetMeterHashItem(meter);
            }

            return metersItems;
        }

        internal static string GetGaugeElementSvgContent(IStiGaugeElement gaugeElement, double zoom)
        {
            if (gaugeElement.GetMeters().Count == 0)
                return string.Empty;

            var gaugeComponent = gaugeElement.GetGaugeComponent();
            gaugeComponent.Height -= 30;

            return StiSvgHelper.SaveComponentToString(gaugeComponent, ImageFormat.Png, 0.75f, (float)(zoom * 100));
        }

        internal static StiGauge CreateGaugeComponentFromGaugeElement(IStiGaugeElement gaugeElement)
        {
            var gauge = new StiGauge();
            gauge.Type = gaugeElement.Type;
            gauge.CalculationMode = gaugeElement.CalculationMode;
            gauge.Minimum = gaugeElement.Minimum;
            gauge.Maximum = gaugeElement.Maximum;
            gauge.Page = gaugeElement.Page;
            gauge.Width = (gaugeElement as StiComponent).Width;
            gauge.Height = (gaugeElement as StiComponent).Height;
            foreach (var style in StiOptions.Services.GaugeStyles.Where(x => x.AllowDashboard))
            {
                if (style.StyleIdent == gaugeElement.Style)
                {
                    gauge.Style = style;
                    break;
                }
            }

            return gauge;
        }
        #endregion

        #region Methods
        public void ExecuteJSCommand(Hashtable parameters, Hashtable callbackResult)
        {
            switch ((string)parameters["command"])
            {

                case "SetExpression":
                    {
                        SetExpression(parameters, callbackResult);
                        break;
                    }
                case "SetDataColumn":
                    {
                        SetDataColumn(parameters, callbackResult);
                        break;
                    }
                case "MoveMeter":
                    {
                        MoveMeter(parameters, callbackResult);
                        break;
                    }
                case "SetFunction":
                    {
                        SetFunction(parameters, callbackResult);
                        break;
                    }
                case "SetPropertyValue":
                    {
                        SetPropertyValue(parameters, callbackResult);
                        break;
                    }
                case "NewItem":
                    {
                        CreateNewItem(parameters, callbackResult);
                        break;
                    }
                case "GetGaugeElementProperties":
                    {
                        break;
                    }
            }

            callbackResult["gaugeElement"] = GetGaugeElementJSProperties(parameters);
        }

        private IStiMeter GetMeterByContainerName(string containerName)
        {
            foreach (var meter in gaugeElement.GetMeters())
            {
                if (meter.GetType().Name == string.Format("Sti{0}GaugeMeter", containerName))
                {
                    return meter;
                }
            }

            return null;
        }

        private void MoveMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var toContainerName = parameters["toContainerName"] as string;
            var fromContainerName = parameters["fromContainerName"] as string;
            IStiMeter movingMeter = null;

            #region Get and Remove meter
            switch (fromContainerName)
            {
                case "Value":
                    {
                        movingMeter = gaugeElement.GetValue();
                        gaugeElement.RemoveValue();
                        break;
                    }
                case "Series":
                    {
                        movingMeter = gaugeElement.GetSeries();
                        gaugeElement.RemoveSeries();
                        break;
                    }
            }
            #endregion

            #region Insert meter
            if (movingMeter != null)
            {
                switch (toContainerName)
                {
                    case "Value":
                        {
                            gaugeElement.AddValue(movingMeter);
                            break;
                        }
                    case "Series":
                        {
                            gaugeElement.AddSeries(movingMeter);
                            break;
                        }
                }
            }
            #endregion
        }

        private void SetExpression(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterByContainerName(parameters["containerName"] as string);
            if (meter != null)
            {
                meter.Expression = StiEncodingHelper.DecodeString(parameters["expressionValue"] as string);
            }
        }

        private void SetFunction(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterByContainerName(parameters["containerName"] as string);
            if (meter != null)
            {
                meter.Expression = StiExpressionHelper.ReplaceFunction(meter.Expression, parameters["function"] as string);
            }
        }

        private void CreateNewItem(Hashtable parameters, Hashtable callbackResult)
        {
            switch (parameters["containerName"] as string)
            {
                case "Value":
                    {
                        gaugeElement.CreateNewValue();
                        break;
                    }                
                case "Series":
                    {
                        gaugeElement.CreateNewSeries();
                        break;
                    }
            }
        }

        private void SetDataColumn(Hashtable parameters, Hashtable callbackResult)
        {
            var dataColumnObject = parameters["dataColumnObject"] as Hashtable;
            StiDataColumn dataColumn = null;
            var allColumns = dataColumnObject != null ? StiDictionaryHelper.GetColumnsByTypeAndNameOfObject(gaugeElement.Report, dataColumnObject) : null;
            if (allColumns != null) dataColumn = allColumns[dataColumnObject["name"] as string];

            switch (parameters["containerName"] as string)
            {
                case "Value":
                    {
                        gaugeElement.AddValue(dataColumn);
                        break;
                    }
                case "Series":
                    {
                        gaugeElement.AddSeries(dataColumn);
                        break;
                    }
            }
        }

        private void SetPropertyValue(Hashtable parameters, Hashtable callbackResult)
        {
            var propertyName = parameters["propertyName"] as string;
            var propertyValue = parameters["propertyValue"] as string;

            switch (propertyName)
            {
                case "Type":
                    {
                        gaugeElement.Type = (StiGaugeType)Enum.Parse(typeof(StiGaugeType), propertyValue);
                        break;
                    }
                case "CalculationMode":
                    {
                        gaugeElement.CalculationMode = (StiGaugeCalculationMode)Enum.Parse(typeof(StiGaugeCalculationMode), propertyValue);
                        break;
                    }
                case "Minimum":
                    {
                        gaugeElement.Minimum = Convert.ToDecimal(propertyValue);
                        break;
                    }
                case "Maximum":
                    {
                        gaugeElement.Maximum = Convert.ToDecimal(propertyValue);
                        break;
                    }
            }
        }

        public static ArrayList GetStylesContent(StiReport report, Hashtable param)
        {
            ArrayList stylesContent = new ArrayList();
            var gaugeElement = report.GetComponentByName((string)param["componentName"]) as IStiGaugeElement;
            if (gaugeElement != null)
            {
                int width = 130;
                int height = 50;

                var gaugeComponent = gaugeElement.GetGaugeComponent(new SizeD(width, height));
                var gaugeStyles = StiOptions.Services.GaugeStyles.Where(x => x.AllowDashboard);

                foreach (var style in gaugeStyles)
                {
                    gaugeComponent.Style = (StiGaugeStyleXF)style.Clone();
                    gaugeComponent.ApplyStyle(gaugeComponent.Style);

                    Hashtable content = new Hashtable();
                    content["image"] = StiGaugeHelper.GetGaugeSampleImage(gaugeComponent, width, height, 1f);
                    content["ident"] = style.StyleIdent;
                    content["width"] = width;
                    content["height"] = height;
                    stylesContent.Add(content);
                }
            }

            return stylesContent;
        }
        #endregion

        #region Constructor
        public StiGaugeElementHelper(IStiGaugeElement gaugeElement)
        {
            this.gaugeElement = gaugeElement;
        }
        #endregion   
    }
}
