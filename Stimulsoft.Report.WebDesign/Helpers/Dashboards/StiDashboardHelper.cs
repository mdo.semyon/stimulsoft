#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Web.Heplers.Dashboards;
using System.Collections;
using System.Linq;

namespace Stimulsoft.Report.Web
{
    public class StiDashboardHelper
    { 
        #region CallBack methods
        public static void AddDashboard(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            var dashboardPage = StiDashboardCreator.CreateDashboard(report) as StiPage;

            int currentPageIndex = int.Parse((string)param["pageIndex"]);

            if (currentPageIndex + 1 == report.Pages.Count)
                report.Pages.Add(dashboardPage);
            else
            {
                report.Pages.Insert(currentPageIndex + 1, dashboardPage);
                dashboardPage.Name = StiNameCreation.CreateName(report, StiNameCreation.GenerateName(dashboardPage));
            }

            callbackResult["name"] = dashboardPage.Name;
            callbackResult["pageIndex"] = report.Pages.IndexOf(dashboardPage).ToString();
            callbackResult["properties"] = StiReportEdit.GetAllProperties(dashboardPage);
            callbackResult["pageIndexes"] = StiReportEdit.GetPageIndexes(report);
        }

        public static StiComponent CreateDashboardElement(StiReport report, string typeComponent)
        {
            StiComponent component = null;

            switch (typeComponent)
            {
                case "StiTableElement":
                    {
                        component = StiActivator.CreateObject("Stimulsoft.Dashboard.Components.Table.StiTableElement") as StiComponent;
                        break;
                    }
                case "StiChartElement":
                    {
                        component = StiActivator.CreateObject(" Stimulsoft.Dashboard.Components.Chart.StiChartElement") as StiComponent;
                        break;
                    }
                case "StiGaugeElement":
                    {
                        component = StiActivator.CreateObject("Stimulsoft.Dashboard.Components.Gauge.StiGaugeElement") as StiComponent;
                        break;
                    }
                case "StiPivotElement":
                    {
                        component = StiActivator.CreateObject("Stimulsoft.Dashboard.Components.Pivot.StiPivotElement") as StiComponent;
                        break;
                    }
                case "StiIndicatorElement":
                    {
                        component = StiActivator.CreateObject("Stimulsoft.Dashboard.Components.Indicator.StiIndicatorElement") as StiComponent;
                        break;
                    }
                case "StiProgressElement":
                    {
                        component = StiActivator.CreateObject("Stimulsoft.Dashboard.Components.Progress.StiProgressElement") as StiComponent;
                        break;
                    }
                case "StiMapElement":
                    {
                        component = StiActivator.CreateObject("Stimulsoft.Dashboard.Components.Map.StiMapElement") as StiComponent;
                        break;
                    }
                case "StiImageElement":
                    {
                        component = StiActivator.CreateObject("Stimulsoft.Dashboard.Components.Image.StiImageElement") as StiComponent;
                        break;
                    }
                case "StiTextElement":
                    {
                        component = StiActivator.CreateObject("Stimulsoft.Dashboard.Components.Text.StiTextElement") as StiComponent;
                        break;
                    }
                case "StiPanelElement":
                    {
                        component = StiActivator.CreateObject("Stimulsoft.Dashboard.Components.Panel.StiPanelElement") as StiComponent;
                        break;
                    }
                case "StiShapeElement":
                    {
                        component = StiActivator.CreateObject("Stimulsoft.Dashboard.Components.Shape.StiShapeElement") as StiComponent;
                        break;
                    }
            }
           
            component.Dockable = false;

            return component;
        }

        public static ArrayList GetDashboardStyles(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            ArrayList styles = new ArrayList();
            var typeComponent = param["typeComponent"] as string;

            var itemAuto = new Hashtable();
            itemAuto["ident"] = "Auto";

            switch (typeComponent)
            {
                case "StiDashboard":
                    {   
                        foreach (var style in StiOptions.Services.Dashboards.DashboardStyles)
                        {
                            var styleObject = new Hashtable();
                            styleObject["ident"] = style.Ident;
                            styleObject["brush"] = StiReportHelper.GetHtmlColor(style.Brush);
                            styles.Add(styleObject);
                        }
                        break;
                    }
                case "StiTableElement":
                    {
                        styles.Add(itemAuto);

                        foreach (var style in StiOptions.Services.Dashboards.TableStyles)
                        {
                            var styleObject = new Hashtable();
                            styleObject["ident"] = style.Ident;
                            styleObject["brushDark"] = StiReportHelper.GetHtmlColor(style.BrushDark);
                            styleObject["brushLight"] = StiReportHelper.GetHtmlColor(style.BrushLight);
                            styleObject["lineHighlight"] = StiReportHelper.GetHtmlColor(style.LineHighlight);
                            styles.Add(styleObject);
                        }
                        break;
                    }
                case "StiPivotElement":
                    {
                        styles.Add(itemAuto);

                        foreach (var style in StiOptions.Services.Dashboards.PivotStyles)
                        {
                            var styleObject = new Hashtable();
                            styleObject["ident"] = style.Ident;
                            styleObject["brushDark"] = StiReportHelper.GetHtmlColor(style.BrushDark);
                            styleObject["brushLight"] = StiReportHelper.GetHtmlColor(style.BrushLight);
                            styleObject["lineHighlight"] = StiReportHelper.GetHtmlColor(style.LineHighlight);
                            styles.Add(styleObject);
                        }
                        break;
                    }
                case "StiMapElement":
                    {                        
                        styles = StiMapElementHelper.GetStylesContent(report, param);
                        styles.Insert(0, itemAuto);
                        break;
                    }
                case "StiChartElement":
                    {
                        styles = StiChartElementHelper.GetStylesContent(report, param);
                        styles.Insert(0, itemAuto);
                        break;
                    }
                case "StiGaugeElement":
                    {
                        styles = StiGaugeElementHelper.GetStylesContent(report, param);
                        styles.Insert(0, itemAuto);
                        break;
                    }
                case "StiProgressElement":
                    {
                        styles = StiProgressElementHelper.GetStylesContent(report, param);
                        styles.Insert(0, itemAuto);
                        break;
                    }
                case "StiIndicatorElement":
                    {
                        styles = StiIndicatorElementHelper.GetStylesContent(report, param);
                        styles.Insert(0, itemAuto);
                        break;
                    }
            }

            return styles;
        }
        #endregion
    }
}
