#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Meters;
using Stimulsoft.Data.Helpers;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Export;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace Stimulsoft.Report.Web.Heplers.Dashboards
{
    internal class StiIndicatorElementHelper
    {
        #region Fields
        private IStiIndicatorElement indicatorElement;
        #endregion

        #region Helper Methods
        private Hashtable GetIndicatorElementJSProperties(Hashtable parameters)
        {
            double zoom = StiReportEdit.StrToDouble(parameters["zoom"] as string);

            Hashtable properties = new Hashtable();
            properties["name"] = indicatorElement.Name;
            properties["meters"] = GetMetersHash(indicatorElement.GetMeters());

            if ((string)parameters["command"] != "GetIndicatorElementProperties")
            {
                properties["svgContent"] = StiEncodingHelper.Encode(GetIndicatorElementSvgContent(indicatorElement, zoom));
            }

            return properties;
        }

        private Hashtable GetMeterHashItem(IStiMeter meter)
        {
            Hashtable meterItem = new Hashtable();
            meterItem["typeItem"] = "Meter";
            meterItem["type"] = StiTableElementHelper.GetMeterType(meter);
            meterItem["typeIcon"] = StiTableElementHelper.GetMeterTypeIcon(meter);
            meterItem["label"] = StiTableElementHelper.GetMeterLabel(meter);
            meterItem["expression"] = meter.Expression != null ? StiEncodingHelper.Encode(meter.Expression) : string.Empty;
            meterItem["functions"] = StiTableElementHelper.GetMeterFunctions(meter, indicatorElement.Page as IStiDashboard);
            meterItem["currentFunction"] = StiExpressionHelper.GetFunction(meter.Expression);

            return meterItem;
        }

        private Hashtable GetMetersHash(List<IStiMeter>meters)
        {
            var metersItems = new Hashtable();
            foreach (IStiMeter meter in meters)
            {
                metersItems[meter.GetType().Name] = GetMeterHashItem(meter);
            }

            return metersItems;
        }

        internal static string GetIndicatorElementSvgContent(IStiIndicatorElement indicatorElement, double zoom)
        {
            if (indicatorElement.GetMeters().Count == 0)
                return string.Empty;

            return "";
        }
        #endregion

        #region Methods
        public void ExecuteJSCommand(Hashtable parameters, Hashtable callbackResult)
        {
            switch ((string)parameters["command"])
            {

                case "SetExpression":
                    {
                        SetExpression(parameters, callbackResult);
                        break;
                    }
                case "SetDataColumn":
                    {
                        SetDataColumn(parameters, callbackResult);
                        break;
                    }
                case "MoveMeter":
                    {
                        MoveMeter(parameters, callbackResult);
                        break;
                    }
                case "SetFunction":
                    {
                        SetFunction(parameters, callbackResult);
                        break;
                    }
                case "NewItem":
                    {
                        CreateNewItem(parameters, callbackResult);
                        break;
                    }
                case "GetIndicatorElementProperties":
                    {
                        break;
                    }
            }

            callbackResult["indicatorElement"] = GetIndicatorElementJSProperties(parameters);
        }

        private IStiMeter GetMeterByContainerName(string containerName)
        {
            foreach (var meter in indicatorElement.GetMeters())
            {
                if (meter.GetType().Name == string.Format("Sti{0}IndicatorMeter", containerName))
                {
                    return meter;
                }
            }

            return null;
        }

        private void MoveMeter(Hashtable parameters, Hashtable callbackResult)
        {
            var toContainerName = parameters["toContainerName"] as string;
            var fromContainerName = parameters["fromContainerName"] as string;
            IStiMeter movingMeter = null;

            #region Get and Remove meter
            switch (fromContainerName)
            {
                case "Value":
                    {
                        movingMeter = indicatorElement.GetValue();
                        indicatorElement.RemoveValue();
                        break;
                    }
                case "Target":
                    {
                        movingMeter = indicatorElement.GetTarget();
                        indicatorElement.RemoveTarget();
                        break;
                    }
                case "Series":
                    {
                        movingMeter = indicatorElement.GetSeries();
                        indicatorElement.RemoveSeries();
                        break;
                    }
            }
            #endregion

            #region Insert meter
            if (movingMeter != null)
            {
                switch (toContainerName)
                {
                    case "Value":
                        {
                            indicatorElement.AddValue(movingMeter);
                            break;
                        }
                    case "Target":
                        {
                            indicatorElement.AddTarget(movingMeter);
                            break;
                        }
                    case "Series":
                        {
                            indicatorElement.AddSeries(movingMeter);
                            break;
                        }
                }
            }
            #endregion
        }

        private void SetExpression(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterByContainerName(parameters["containerName"] as string);
            if (meter != null)
            {
                meter.Expression = StiEncodingHelper.DecodeString(parameters["expressionValue"] as string);
            }
        }

        private void SetFunction(Hashtable parameters, Hashtable callbackResult)
        {
            var meter = GetMeterByContainerName(parameters["containerName"] as string);
            if (meter != null)
            {
                meter.Expression = StiExpressionHelper.ReplaceFunction(meter.Expression, parameters["function"] as string);
            }
        }

        private void CreateNewItem(Hashtable parameters, Hashtable callbackResult)
        {
            switch (parameters["containerName"] as string)
            {
                case "Value":
                    {
                        indicatorElement.CreateNewValue();
                        break;
                    }
                case "Target":
                    {
                        indicatorElement.CreateNewTarget();
                        break;
                    }
                case "Series":
                    {
                        indicatorElement.CreateNewSeries();
                        break;
                    }
            }
        }

        private void SetDataColumn(Hashtable parameters, Hashtable callbackResult)
        {
            var dataColumnObject = parameters["dataColumnObject"] as Hashtable;
            StiDataColumn dataColumn = null;
            var allColumns = dataColumnObject != null ? StiDictionaryHelper.GetColumnsByTypeAndNameOfObject(indicatorElement.Report, dataColumnObject) : null;
            if (allColumns != null) dataColumn = allColumns[dataColumnObject["name"] as string];
            
            switch (parameters["containerName"] as string)
            {
                case "Value":
                    {
                        indicatorElement.AddValue(dataColumn);
                        break;
                    }
                case "Target":
                    {
                        indicatorElement.AddTarget(dataColumn);
                        break;
                    }
                case "Series":
                    {
                        indicatorElement.AddSeries(dataColumn);
                        break;
                    }
            }
        }

        public static string GetIndicatorSampleImage(IStiIndicatorElement indicatorElement, int width, int height, float zoom)
        {
            var svgData = new StiSvgData()
            {
                X = 0,
                Y = 0,
                Width = width,
                Height = height,
                Component = indicatorElement as StiComponent
            };

            var sb = new StringBuilder();

            using (var ms = new StringWriter(sb))
            {
                var writer = new XmlTextWriter(ms);

                writer.WriteStartElement("svg");
                writer.WriteAttributeString("version", "1.1");
                writer.WriteAttributeString("baseProfile", "full");

                writer.WriteAttributeString("xmlns", "http://www.w3.org/2000/svg");
                writer.WriteAttributeString("xmlns:xlink", "http://www.w3.org/1999/xlink");
                writer.WriteAttributeString("xmlns:ev", "http://www.w3.org/2001/xml-events");

                writer.WriteAttributeString("height", svgData.Height.ToString());
                writer.WriteAttributeString("width", svgData.Width.ToString());

                //StiChartSvgHelper.WriteChart(writer, svgData, zoom, false); //TO DO

                writer.WriteFullEndElement();
                writer.Flush();
                ms.Flush();
                writer.Close();
                ms.Close();
            }

            return sb.ToString();
        }

        public static ArrayList GetStylesContent(StiReport report, Hashtable param)
        {
            ArrayList stylesContent = new ArrayList();
            var indicatorElement = report.GetComponentByName((string)param["componentName"]) as IStiIndicatorElement;
            if (indicatorElement != null)
            {
                var cloneProgressElement = (indicatorElement as StiComponent).Clone() as IStiIndicatorElement;
                int width = 130;
                int height = 50;

                foreach (var style in StiOptions.Services.Dashboards.ProgressStyles)
                {
                    cloneProgressElement.Style = style.Ident;

                    Hashtable content = new Hashtable();
                    content["image"] = GetIndicatorSampleImage(indicatorElement, width, height, 1f);
                    content["ident"] = style.Ident;
                    content["width"] = width;
                    content["height"] = height;
                    stylesContent.Add(content);
                }
            }

            return stylesContent;
        }
        #endregion

        #region Constructor
        public StiIndicatorElementHelper(IStiIndicatorElement indicatorElement)
        {
            this.indicatorElement = indicatorElement;
        }
        #endregion   
    }
}
