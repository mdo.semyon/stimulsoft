#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.Dashboard;
using System;
using System.Collections;

namespace Stimulsoft.Report.Web.Heplers.Dashboards
{
    internal class StiImageElementHelper
    {
        #region Fields
        private IStiImageElement imageElement;
        #endregion

        #region Helper Methods
        private Hashtable GetImageElementJSProperties()
        {
            Hashtable properties = new Hashtable();
            properties["imageUrl"] = !String.IsNullOrEmpty(imageElement.ImageHyperlink) ? StiEncodingHelper.Encode(imageElement.ImageHyperlink) : string.Empty;
            properties["imageSrc"] = imageElement.Image != null ? StiReportEdit.ImageToBase64(imageElement.Image) : string.Empty;

            return properties;
        }
        #endregion

        #region Methods
        public void ExecuteJSCommand(Hashtable parameters, Hashtable callbackResult)
        {
            switch ((string)parameters["command"])
            {   
                case "SetPropertyValue":
                    {
                        SetPropertyValue(parameters, callbackResult);
                        break;
                    }
            }

            callbackResult["imageElement"] = GetImageElementJSProperties();
        }

        private void SetPropertyValue(Hashtable parameters, Hashtable callbackResult)
        {
            var propertyName = parameters["propertyName"] as string;
            var propertyValue = parameters["propertyValue"] as string;

            switch (parameters["propertyName"] as string)
            {
                case "imageSrc":
                    {
                        imageElement.Image = !String.IsNullOrEmpty(propertyValue) ? Convert.FromBase64String(propertyValue.Substring(propertyValue.IndexOf("base64,") + 7)) : null;
                        break;
                    }
                case "imageUrl":
                    {
                        imageElement.ImageHyperlink = StiEncodingHelper.DecodeString(propertyValue);
                        break;
                    }
            }
        }
        #endregion

        #region Constructor
        public StiImageElementHelper(IStiImageElement imageElement)
        {
            this.imageElement = imageElement;
        }
        #endregion   
    }
}
