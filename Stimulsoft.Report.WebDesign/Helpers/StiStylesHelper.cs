﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Reflection;
using System.Drawing;
using System.Collections;
using Stimulsoft.Report.Components;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Base.Localization;
using System.IO;
using System.Collections.Generic;

namespace Stimulsoft.Report.Web
{
    internal class StiStylesHelper
    {
        #region Style Item
        private static Hashtable GetStyleProperties(StiBaseStyle style)
        {
            Hashtable properties = new Hashtable();
            properties["name"] = style.Name;
            properties["collectionName"] = style.CollectionName;
            properties["description"] = style.Description;
            properties["conditions"] = GetStyleConditionsProprty(style.Conditions);
            string brush = StiReportEdit.GetPropertyValue("Brush", style) as string;
            if (!String.IsNullOrEmpty(brush)) properties["brush"] = brush;
            string textBrush = StiReportEdit.GetPropertyValue("TextBrush", style) as string;
            if (textBrush != string.Empty) properties["textBrush"] = textBrush;
            string border = StiReportEdit.GetPropertyValue("Border", style) as string;
            if (!String.IsNullOrEmpty(border)) properties["border"] = border;
            string font = StiReportEdit.GetPropertyValue("Font", style) as string;
            if (!String.IsNullOrEmpty(font)) properties["font"] = font;
            string horAlignment = StiReportEdit.GetPropertyValue("HorAlignment", style) as string;
            if (horAlignment != string.Empty) properties["horAlignment"] = horAlignment;
            string vertAlignment = StiReportEdit.GetPropertyValue("VertAlignment", style) as string;
            if (horAlignment != string.Empty) properties["vertAlignment"] = vertAlignment;
            //string image = StiReportEdit.GetImageSrcProperty(style);
            //if (image != string.Empty) properties["image"] = image;
            string color = StiReportEdit.GetPropertyValue("Color", style) as string;
            if (!String.IsNullOrEmpty(color)) properties["color"] = color;
            string foreColor = StiReportEdit.GetPropertyValue("ForeColor", style) as string;
            if (!String.IsNullOrEmpty(foreColor)) properties["foreColor"] = foreColor;
            string backColor = StiReportEdit.GetPropertyValue("BackColor", style) as string;
            if (!String.IsNullOrEmpty(backColor)) properties["backColor"] = backColor;
            string basicStyleColor = StiReportEdit.GetPropertyValue("BasicStyleColor", style) as string;
            if (!String.IsNullOrEmpty(basicStyleColor)) properties["basicStyleColor"] = basicStyleColor;
            string brushType = StiReportEdit.GetPropertyValue("BrushType", style) as string;
            if (!String.IsNullOrEmpty(brushType)) properties["brushType"] = brushType;

            string[] boolProps = { "AllowUseBackColor", "AllowUseForeColor", "AllowUseBorderFormatting", "AllowUseBorderSides", "AllowUseBorderSidesFromLocation",
                                 "AllowUseBrush", "AllowUseFont", "AllowUseImage", "AllowUseTextBrush", "AllowUseHorAlignment", "AllowUseVertAlignment"};
            foreach (string propertyName in boolProps)
            {
                bool property;
                PropertyInfo pi = style.GetType().GetProperty(propertyName);
                if (pi != null)
                {
                    property = (bool)pi.GetValue(style, null);
                    string propNameLower = propertyName[0].ToString().ToLowerInvariant() + propertyName.Remove(0, 1);
                    properties[propNameLower] = property;
                }
            }

            if (style is StiCrossTabStyle)
            {
                var topBrush = new System.Windows.Media.SolidColorBrush(StiColorHelper.Light(StiColorHelper.ColorToWpfColor(((StiCrossTabStyle)style).Color), 50));
                var bottomBrush = new System.Windows.Media.SolidColorBrush(StiColorHelper.ColorToWpfColor(((StiCrossTabStyle)style).Color));
                properties["topColor"] = StiReportEdit.GetStringFromColor(Color.FromArgb(topBrush.Color.A, topBrush.Color.R, topBrush.Color.G, topBrush.Color.B));
                properties["bottomColor"] = StiReportEdit.GetStringFromColor(Color.FromArgb(bottomBrush.Color.A, bottomBrush.Color.R, bottomBrush.Color.G, bottomBrush.Color.B));
            }
            else if (style is StiMapStyle)
            {  
                properties["borderColor"] = StiReportEdit.GetStringFromColor(((StiMapStyle)style).BorderColor);
                properties["borderSize"] = StiReportEdit.DoubleToStr(((StiMapStyle)style).BorderSize);
                properties["colors"] = GetColorsCollectionProperty(((StiMapStyle)style).Colors);
                properties["defaultColor"] = StiReportEdit.GetStringFromColor(((StiMapStyle)style).DefaultColor);
                properties["heatmapColors"] = GetColorsCollectionProperty(((StiMapStyle)style).HeatmapColors);
            }
            else if (style is StiChartStyle)
            {
                StiChartStyle chartStyle = style as StiChartStyle;
                properties["gridLinesHorColor"] = StiReportEdit.GetStringFromColor(chartStyle.GridLinesHorColor);
                properties["gridLinesVertColor"] = StiReportEdit.GetStringFromColor(chartStyle.GridLinesVertColor);
                properties["chartAreaShowShadow"] = chartStyle.ChartAreaShowShadow;
                properties["chartAreaBrush"] = StiReportEdit.BrushToStr(chartStyle.ChartAreaBrush);
                properties["chartAreaBorderColor"] = StiReportEdit.GetStringFromColor(chartStyle.ChartAreaBorderColor);
                properties["seriesLabelsBorderColor"] = StiReportEdit.GetStringFromColor(chartStyle.SeriesLabelsBorderColor);
                properties["seriesLabelsColor"] = StiReportEdit.GetStringFromColor(chartStyle.SeriesLabelsColor);
                properties["seriesLabelsBrush"] = StiReportEdit.BrushToStr(chartStyle.SeriesLabelsBrush);
                properties["trendLineShowShadow"] = chartStyle.TrendLineShowShadow;
                properties["trendLineColor"] = StiReportEdit.GetStringFromColor(chartStyle.TrendLineColor);
                properties["legendBorderColor"] = StiReportEdit.GetStringFromColor(chartStyle.LegendBorderColor);
                properties["legendBrush"] = StiReportEdit.BrushToStr(chartStyle.LegendBrush);
                properties["legendLabelsColor"] = StiReportEdit.GetStringFromColor(chartStyle.LegendLabelsColor);
                properties["legendTitleColor"] = StiReportEdit.GetStringFromColor(chartStyle.LegendTitleColor);
                properties["axisLabelsColor"] = StiReportEdit.GetStringFromColor(chartStyle.AxisLabelsColor);
                properties["axisLineColor"] = StiReportEdit.GetStringFromColor(chartStyle.AxisLineColor);
                properties["axisTitleColor"] = StiReportEdit.GetStringFromColor(chartStyle.AxisTitleColor);
                properties["interlacingHorBrush"] = StiReportEdit.BrushToStr(chartStyle.InterlacingHorBrush);
                properties["interlacingVertBrush"] = StiReportEdit.BrushToStr(chartStyle.InterlacingVertBrush);
                properties["seriesLighting"] = chartStyle.SeriesLighting;
                properties["seriesShowShadow"] = chartStyle.SeriesShowShadow;
                properties["styleColors"] = GetColorsCollectionProperty(chartStyle.StyleColors);
            }

            return properties;
        }

        public static Hashtable StyleItem(StiBaseStyle style)
        {
            Hashtable styleItem = new Hashtable();
            styleItem["type"] = style.GetType().Name;
            styleItem["properties"] = GetStyleProperties(style);

            return styleItem;
        }
        #endregion

        #region Helper Methods
        public static void SetConditionTypeProperty(StiStyleCondition styleCondition, string propertyValue)
        {
            int value = 0;
            if (propertyValue.IndexOf(" ComponentType,") >= 0) value += 1;
            if (propertyValue.IndexOf(" Placement,") >= 0) value += 2;
            if (propertyValue.IndexOf(" PlacementNestedLevel,") >= 0) value += 4;
            if (propertyValue.IndexOf(" ComponentName,") >= 0) value += 8;
            if (propertyValue.IndexOf(" Location,") >= 0) value += 16;

            styleCondition.Type = (StiStyleConditionType)value;
        }

        public static void SetLocationProperty(StiStyleCondition styleCondition, string propertyValue)
        {
            int value = 0;
            if (propertyValue.IndexOf(" TopLeft,") >= 0) value += 1;
            if (propertyValue.IndexOf(" TopCenter,") >= 0) value += 2;
            if (propertyValue.IndexOf(" TopRight,") >= 0) value += 4;
            if (propertyValue.IndexOf(" MiddleLeft,") >= 0) value += 8;
            if (propertyValue.IndexOf(" MiddleCenter,") >= 0) value += 16;
            if (propertyValue.IndexOf(" MiddleRight,") >= 0) value += 32;
            if (propertyValue.IndexOf(" BottomLeft,") >= 0) value += 64;
            if (propertyValue.IndexOf(" BottomCenter,") >= 0) value += 128;
            if (propertyValue.IndexOf(" BottomRight,") >= 0) value += 256;
            if (propertyValue.IndexOf(" Left,") >= 0) value += 512;
            if (propertyValue.IndexOf(" Right,") >= 0) value += 1024;
            if (propertyValue.IndexOf(" Top,") >= 0) value += 2048;
            if (propertyValue.IndexOf(" Bottom,") >= 0) value += 4096;
            if (propertyValue.IndexOf(" CenterHorizontal,") >= 0) value += 8192;
            if (propertyValue.IndexOf(" CenterVertical,") >= 0) value += 16384;

            styleCondition.Location = (StiStyleLocation)value;
        }

        public static void SetComponentTypeProperty(StiStyleCondition styleCondition, string propertyValue)
        {
            int value = 0;
            if (propertyValue.IndexOf(" Text,") >= 0) value += 1;
            if (propertyValue.IndexOf(" Primitive,") >= 0) value += 2;
            if (propertyValue.IndexOf(" Image,") >= 0) value += 4;
            if (propertyValue.IndexOf(" CrossTab,") >= 0) value += 8;
            if (propertyValue.IndexOf(" Chart,") >= 0) value += 16;
            if (propertyValue.IndexOf(" CheckBox,") >= 0) value += 32;

            styleCondition.ComponentType = (StiStyleComponentType)value;
        }

        public static void SetPlacementProperty(StiStyleCondition styleCondition, string propertyValue)
        {
            int value = 0;
            if (propertyValue.IndexOf(" ReportTitle,") >= 0) value += 1;
            if (propertyValue.IndexOf(" ReportSummary,") >= 0) value += 2;
            if (propertyValue.IndexOf(" PageHeader,") >= 0) value += 4;
            if (propertyValue.IndexOf(" PageFooter,") >= 0) value += 8;
            if (propertyValue.IndexOf(" GroupHeader,") >= 0) value += 16;
            if (propertyValue.IndexOf(" GroupFooter,") >= 0) value += 32;
            if (propertyValue.IndexOf(" Header,") >= 0) value += 64;
            if (propertyValue.IndexOf(" Footer,") >= 0) value += 128;
            if (propertyValue.IndexOf(" ColumnHeader,") >= 0) value += 256;
            if (propertyValue.IndexOf(" ColumnFooter,") >= 0) value += 512;
            if (propertyValue.IndexOf(" Data,") >= 0) value += 1024;
            if (propertyValue.IndexOf(" DataEvenStyle,") >= 0)
            {
                styleCondition.Placement = (StiStyleComponentPlacement)2048;
                return;
            }
            if (propertyValue.IndexOf(" DataOddStyle,") >= 0)
            {
                styleCondition.Placement = (StiStyleComponentPlacement)4096;
                return;
            }
            if (propertyValue.IndexOf(" Table,") >= 0) value += 8192;
            if (propertyValue.IndexOf(" Hierarchical,") >= 0) value += 16384;
            if (propertyValue.IndexOf(" Child,") >= 0) value += 32768;
            if (propertyValue.IndexOf(" Empty,") >= 0) value += 65536;
            if (propertyValue.IndexOf(" Overlay,") >= 0) value += 131072;
            if (propertyValue.IndexOf(" Panel,") >= 0) value += 262144;
            if (propertyValue.IndexOf(" Page,") >= 0) value += 524288;
            if (propertyValue.IndexOf(" Empty,") >= 0) value += 65536;

            if (propertyValue == " AllExeptStyles,") styleCondition.Placement = StiStyleComponentPlacement.AllExeptStyles;
            else styleCondition.Placement = (StiStyleComponentPlacement)value;
        }

        public static void SetStyleConditionsProprty(StiBaseStyle style, ArrayList conditions)
        {
            style.Conditions.Clear();

            foreach (Hashtable conditionObject in conditions)
            {
                StiStyleCondition styleCondition = new StiStyleCondition();
                style.Conditions.Add(styleCondition);
                SetConditionTypeProperty(styleCondition, conditionObject["type"] as string);
                SetPlacementProperty(styleCondition, conditionObject["placement"] as string);
                styleCondition.OperationPlacement = (StiStyleConditionOperation)Enum.Parse(typeof(StiStyleConditionOperation), conditionObject["operationPlacement"] as string);
                styleCondition.PlacementNestedLevel = StiReportEdit.StrToInt(conditionObject["placementNestedLevel"] as string);
                styleCondition.OperationPlacementNestedLevel = (StiStyleConditionOperation)Enum.Parse(typeof(StiStyleConditionOperation), conditionObject["operationPlacementNestedLevel"] as string);
                SetComponentTypeProperty(styleCondition, conditionObject["componentType"] as string);
                styleCondition.OperationComponentType = (StiStyleConditionOperation)Enum.Parse(typeof(StiStyleConditionOperation), conditionObject["operationComponentType"] as string);
                SetLocationProperty(styleCondition, conditionObject["location"] as string);
                styleCondition.OperationLocation = (StiStyleConditionOperation)Enum.Parse(typeof(StiStyleConditionOperation), conditionObject["operationLocation"] as string);
                styleCondition.ComponentName = conditionObject["componentName"] as string;
                styleCondition.OperationComponentName = (StiStyleConditionOperation)Enum.Parse(typeof(StiStyleConditionOperation), conditionObject["operationComponentName"] as string);
            }
        }

        public static ArrayList GetStyleConditionsProprty(StiStyleConditionsCollection conditions)
        {
            ArrayList result = new ArrayList();

            foreach (StiStyleCondition condition in conditions)
            {
                Hashtable conditionObject = new Hashtable();
                result.Add(conditionObject);

                conditionObject["type"] = " " + condition.Type.ToString() + ",";
                conditionObject["placement"] = " " + condition.Placement.ToString() + ",";
                conditionObject["operationPlacement"] = condition.OperationPlacement.ToString();
                conditionObject["placementNestedLevel"] = condition.PlacementNestedLevel.ToString();
                conditionObject["operationPlacementNestedLevel"] = condition.OperationPlacementNestedLevel.ToString();
                conditionObject["componentType"] = " " + condition.ComponentType.ToString() + ",";
                conditionObject["operationComponentType"] = condition.OperationComponentType.ToString();
                conditionObject["location"] = " " + condition.Location.ToString() + ",";
                conditionObject["operationLocation"] = condition.OperationLocation.ToString();
                conditionObject["componentName"] = condition.ComponentName;
                conditionObject["operationComponentName"] = condition.OperationComponentName.ToString();
            }

            return result;
        }

        public static ArrayList GetStyles(StiReport report)
        {
            ArrayList items = new ArrayList();
            foreach (StiBaseStyle style in report.Styles)
            {
                items.Add(StyleItem(style));
            }

            return items;
        }

        public static void GenerateNewName(StiReport report, StiBaseStyle newStyle)
        {
            bool fail = true;
            int index = 1;

            while (fail)
            {
                fail = false;
                string name = StiLocalization.Get("FormStyleDesigner", "Style") + index.ToString();

                foreach (StiBaseStyle st in report.Styles)
                {
                    if (st.Name == name)
                    {
                        fail = true;
                        break;
                    }
                }
                index++;
                newStyle.Name = name;
            }
        }

        public static void ApplyStyleProperties(StiBaseStyle style, Hashtable properties, StiReport report)
        {
            foreach (DictionaryEntry property in properties)
            {
                string propertyName = (string)property.Key;
                string propertyNameUpper = propertyName[0].ToString().ToUpperInvariant() + propertyName.Remove(0, 1);
                object propertyValue = property.Value;
                PropertyInfo pi = style.GetType().GetProperty(propertyNameUpper);
                if (pi != null)
                {
                    if (pi.PropertyType == typeof(bool) || pi.PropertyType == typeof(string))
                    {
                        pi.SetValue(style, propertyValue, null);
                    }
                    else if (pi.PropertyType == typeof(StiBrush))
                    {
                        pi.SetValue(style, StiReportEdit.StrToBrush((string)propertyValue), null);
                    }
                    else if (pi.PropertyType == typeof(StiBorder))
                    {
                        pi.SetValue(style, StiReportEdit.StrToBorder((string)propertyValue), null);
                    }
                    else if (pi.PropertyType == typeof(Font))
                    {
                        pi.SetValue(style, StiReportEdit.StrToFont((string)propertyValue, report), null);
                    }
                    else if (pi.PropertyType == typeof(Color))
                    {
                        pi.SetValue(style, StiReportEdit.StrToColor((string)propertyValue), null);
                    }
                    else if (pi.PropertyType == typeof(StiTextHorAlignment))
                    {
                        pi.SetValue(style, (StiTextHorAlignment)Enum.Parse(typeof(StiTextHorAlignment), (string)propertyValue), null);
                    }
                    else if (pi.PropertyType == typeof(StiVertAlignment))
                    {
                        pi.SetValue(style, (StiVertAlignment)Enum.Parse(typeof(StiVertAlignment), (string)propertyValue), null);
                    }
                    else if (pi.PropertyType == typeof(StiBrushType))
                    {
                        pi.SetValue(style, (StiBrushType)Enum.Parse(typeof(StiBrushType), (string)propertyValue), null);
                    }
                    else if (pi.PropertyType == typeof(StiStyleConditionsCollection))
                    {
                        SetStyleConditionsProprty(style, propertyValue as ArrayList);
                    }
                    else if (pi.PropertyType == typeof(Color[]))
                    {
                        SetColorsCollectionProperty(style, propertyValue as ArrayList);
                    }
                    else if (pi.PropertyType == typeof(Double))
                    {
                        pi.SetValue(style, StiReportEdit.StrToDouble((string)propertyValue), null);
                    }
                }
            }

        }

        public static void WriteStylesToReport(StiReport report, ArrayList stylesCollection)
        {
            report.Styles.Clear();

            foreach (Hashtable styleObject in stylesCollection)
            {
                Assembly assembly = typeof(StiReport).Assembly;
                StiBaseStyle style = assembly.CreateInstance("Stimulsoft.Report." + (string)styleObject["type"]) as StiBaseStyle;

                if (style != null)
                {
                    GenerateNewName(report, style);
                    report.Styles.Add(style);
                    ApplyStyleProperties(style, (Hashtable)styleObject["properties"], report);
                }
            }
        }

        private static ArrayList GetColorsCollectionProperty(Color[] colors)
        {
            ArrayList result = new ArrayList();
            foreach (Color color in colors)
            {
                result.Add(StiReportEdit.GetStringFromColor(color));
            }

            return result;
        }

        private static void SetColorsCollectionProperty(StiBaseStyle style, ArrayList colorsString)
        {   
            StiChartStyle chartStyle = style as StiChartStyle;
            if (chartStyle != null)
            {
                var colors = new List<Color>();
                foreach (string colorString in colorsString)
                {
                    colors.Add(StiReportEdit.StrToColor(colorString));
                }
                chartStyle.StyleColors = colors.ToArray();
            }
        }
        #endregion

        #region Callback Methods
        public static void UpdateStyles(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            if (param["reportFile"] != null) report.ReportFile = (string)param["reportFile"];
            if (param["stylesCollection"] != null) WriteStylesToReport(report, (ArrayList)param["stylesCollection"]);
            if (param["collectionName"] != null) report.ApplyStyleCollection((string)param["collectionName"]);
            report.ApplyStyles();
            report.Info.Zoom = StiReportEdit.StrToDouble((string)param["zoom"]);
            callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(report);
            if (param["selectedObjectName"] != null) callbackResult["selectedObjectName"] = (string)param["selectedObjectName"];
            callbackResult["reportGuid"] = (string)param["reportGuid"];
        }

        public static void AddStyle(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Assembly assembly = typeof(StiReport).Assembly;
            string styleType = param["type"] as string;
            StiBaseStyle newStyle = assembly.CreateInstance("Stimulsoft.Report." + styleType) as StiBaseStyle;


            if (newStyle != null)
            {
                callbackResult["styleObject"] = StyleItem(newStyle);
            }
        }

        public static void CreateStyleCollection(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable properties = (Hashtable)param["styleCollectionProperties"];            
            Color baseColor = StiReportEdit.StrToColor((string)properties["color"]);

            var creator = new StiStylesCreator(report);
            creator.ShowBorders = (bool)properties["borders"];
            creator.MaxNestedLevel = StiReportEdit.StrToInt((string)properties["nestedLevel"]);

            creator.NestedFactor = (StiNestedFactor)Enum.Parse(typeof(StiNestedFactor), (string)properties["nestedFactor"]);
            creator.ShowReportTitles = (bool)properties["reportTitle"];
            creator.ShowReportSummaries = (bool)properties["reportSummary"];
            creator.ShowPageHeaders = (bool)properties["pageHeader"];
            creator.ShowPageFooters = (bool)properties["pageFooter"];
            creator.ShowGroupHeaders = (bool)properties["groupHeader"];
            creator.ShowGroupFooters = (bool)properties["groupFooter"];
            creator.ShowHeaders = (bool)properties["header"];
            creator.ShowDatas = (bool)properties["data"];
            creator.ShowFooters = (bool)properties["footer"];

            var styles = creator.CreateStyles((string)properties["collectionName"], baseColor);
            ArrayList newStylesCollection = new ArrayList();

            foreach (var style in styles)
            {
                newStylesCollection.Add(StyleItem(style));
            }

            callbackResult["removeExistingStyles"] = properties["removeExistingStyles"];
            callbackResult["collectionName"] = properties["collectionName"];
            callbackResult["newStylesCollection"] = newStylesCollection;
        }

        public static void CreateStylesFromComponents(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var stylesList = new List<StiBaseStyle>();
            var stylesListForJS = new ArrayList();
            var componentsNames = param["componentsNames"] as ArrayList;

            foreach (string componentName in componentsNames)
            {
                var comp = report.GetComponentByName(componentName);
                if (comp != null)
                {
                    StiBaseStyle style = null;

                    if (comp is Stimulsoft.Report.Chart.StiChart)
                    {
                        style = new StiChartStyle();
                        style.GetStyleFromComponent(comp, StiStyleElements.All);
                    }
                    else if (comp is Stimulsoft.Report.CrossTab.StiCrossTab)
                    {
                        style = new StiCrossTabStyle();
                        style.GetStyleFromComponent(comp, StiStyleElements.All);
                    }
                    else if (comp is Stimulsoft.Report.Maps.StiMap)
                    {
                        style = new StiMapStyle();
                        style.GetStyleFromComponent(comp, StiStyleElements.All);
                    }
                    else if (comp is Stimulsoft.Report.Gauge.StiGauge)
                    {
                        style = new StiGaugeStyle();
                        style.GetStyleFromComponent(comp, StiStyleElements.All);
                    }
                    else if (comp is Stimulsoft.Report.Components.Table.StiTable)
                    {
                        style = new StiTableStyle();
                        style.GetStyleFromComponent(comp, StiStyleElements.All);
                    }
                    else
                    {
                        style = new StiStyle();
                        style.GetStyleFromComponent(comp, StiStyleElements.All);
                        ((StiStyle)style).AllowUseHorAlignment = false;
                        ((StiStyle)style).AllowUseVertAlignment = false;
                    }
                    style.Name = comp.Name + "Style";
                    style.Description = string.Format("Style based on formating of {0} component", comp.Name);

                    bool find = false;
                    foreach (StiBaseStyle baseStyle in stylesList)
                    {
                        if (baseStyle.Equals(style, false, false))
                        {
                            find = true;
                            break;
                        }
                    }
                    if (!find)
                    {
                        stylesList.Add(style);
                        stylesListForJS.Add(StyleItem(style));
                    }   
                }
            }

            callbackResult["styles"] = stylesListForJS;
        }

        public static void OpenStyle(StiRequestParams requestParams, StiReport report, Hashtable callbackResult)
        {
            using (var stream = new MemoryStream(requestParams.Data))
            {
                StiStylesCollection styles = new StiStylesCollection();
                styles.Load(stream);

                ArrayList stylesCollectionForJS = new ArrayList();
                foreach (StiBaseStyle style in styles)
                {
                    if (style != null) stylesCollectionForJS.Add(StyleItem(style));
                }
                callbackResult["stylesCollection"] = stylesCollectionForJS;
            }
        }
        #endregion
    }
}