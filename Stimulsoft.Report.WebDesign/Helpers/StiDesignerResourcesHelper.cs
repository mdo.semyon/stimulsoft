﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Text;
using System.Reflection;
using System.IO;
using System.Collections;
using System;
using System.Drawing;
using Stimulsoft.Base;
using Stimulsoft.Report.Dashboard;

namespace Stimulsoft.Report.Web
{
    internal class StiDesignerResourcesHelper
    {
        #region Methods

        public static StiWebActionResult Get(StiRequestParams requestParams)
        {
            // Load DesignerFx scripts
            if (requestParams.Resource == "scripts")
            {
                byte[] bytes = GetScripts(requestParams);
                return new StiWebActionResult(bytes, "text/javascript");
            }

            // Load DesignerFx flash files
            if (requestParams.Resource != null && requestParams.Resource.EndsWith(".swf"))
            {
                byte[] bytes = GetFlashResources(requestParams);
                return new StiWebActionResult(bytes, "application/x-shockwave-flash");
            }

            // Load image by file name
            if (requestParams.Resource != null && (requestParams.Resource.EndsWith(".png") || requestParams.Resource.EndsWith(".gif") || requestParams.Resource.EndsWith(".cur")))
            {
                byte[] bytes = GetImage(requestParams);
                string contentType = requestParams.Resource.EndsWith(".cur") ? "application/octet-stream" : "image/" + requestParams.Resource.Substring(requestParams.Resource.Length - 3);
                return new StiWebActionResult(bytes, contentType);
            }

            // Load HTML5 designer scripts
            if (!string.IsNullOrEmpty(requestParams.Resource))
            {
                byte[] bytes = GetScripts(requestParams);
                return new StiWebActionResult(bytes, "text/javascript");
            }

            // Load theme CSS styles
            if (!string.IsNullOrEmpty(requestParams.Theme))
            {
                byte[] bytes = GetStyles(requestParams);
                return new StiWebActionResult(bytes, "text/css");
            }

            return new StiWebActionResult();
        }

        #endregion

        #region Scripts

        public static byte[] GetScripts(StiRequestParams requestParams)
        {
            var jsCacheParams = requestParams.Theme.ToString() + requestParams.CloudMode.ToString() + requestParams.Resource + requestParams.Version;
            string jsCacheGuid = "DesignerScripts_" + StiMD5Helper.ComputeHash(jsCacheParams);
            if (requestParams.Resource == "DesignerScripts" || requestParams.Resource == "AllNotLoadedScripts")
            {
                string jsCacheResult = requestParams.Cache.Helper.GetResourceInternal(requestParams, jsCacheGuid);
                if (!string.IsNullOrEmpty(jsCacheResult)) return Encoding.UTF8.GetBytes(jsCacheResult);
            }

            Assembly assembly = typeof(StiDesignerResourcesHelper).Assembly;
            string[] resourceNames = assembly.GetManifestResourceNames();
            string jsPath = string.Format("{0}.{1}.Scripts.", typeof(StiDesignerResourcesHelper).Namespace, requestParams.Component);
            string jsFirstResult = string.Empty;
            string jsResult = string.Empty;
            string[] scriptNames = requestParams.Resource.Split(';');

            foreach (var resourceName in resourceNames)
            {
                if (resourceName.IndexOf(jsPath) == 0 && resourceName.EndsWith(".js"))
                {
                    foreach (string scriptName in scriptNames)
                    {
                        if ((requestParams.Resource == "DesignerScripts" && !resourceName.EndsWith("_NotLoad.js")) ||
                            (requestParams.Resource == "AllNotLoadedScripts" && resourceName.EndsWith("_NotLoad.js")) ||
                            resourceName.EndsWith(scriptName + "_NotLoad.js") ||
                            requestParams.Resource == "scripts")
                        {
                            if (!requestParams.CloudMode && resourceName.IndexOf(".LoginControls") >= 0) continue;
                            if (!StiDashboardDesigner.IsAssemblyLoaded && resourceName.IndexOf(".Dashboards") >= 0) continue;

                            Stream stream = assembly.GetManifestResourceStream(resourceName);
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                string script = reader.ReadToEnd();
                                if (requestParams.CloudMode && resourceName.IndexOf("jquery-1.8.3.min.js") >= 0)
                                    jsFirstResult = string.Format("{0}{1}\r\n", jsFirstResult, script);
                                else
                                    jsResult = string.Format("{0}{1}\r\n", jsResult, script);
                            }
                            stream.Dispose();
                        }
                    }
                }
            }

            jsResult = string.Format("{0}\r\n{1}", jsFirstResult, jsResult);
            if (requestParams.Resource == "DesignerScripts" || requestParams.Resource == "AllNotLoadedScripts")
                requestParams.Cache.Helper.SaveResourceInternal(requestParams, jsResult, jsCacheGuid);

            return Encoding.UTF8.GetBytes(jsResult);
        }

        #endregion

        #region Styles

        internal static Hashtable GetStylesConstants(string css)
        {
            Hashtable constants = new Hashtable();
            string[] constantsArray = css.Split(';');
            for (int i = 0; i < constantsArray.Length; i++)
            {
                var index = constantsArray[i].IndexOf('@');
                string[] values = constantsArray[i].Substring(index >= 0 ? index : 0).Split('=');
                if (values.Length == 2) constants[values[0].Trim()] = values[1].Trim();
            }

            return constants;
        }

        public static byte[] GetStyles(StiRequestParams requestParams)
        {
            string stylesCacheGuid = "DesignerStyles_" + StiMD5Helper.ComputeHash(requestParams.Theme.ToString() + requestParams.CloudMode.ToString() + requestParams.Version);
            string stylesCacheResult = requestParams.Cache.Helper.GetResourceInternal(requestParams, stylesCacheGuid);
            if (!string.IsNullOrEmpty(stylesCacheResult)) return Encoding.UTF8.GetBytes(stylesCacheResult);

            Assembly assembly = typeof(StiDesignerResourcesHelper).Assembly;
            string[] resourceNames = assembly.GetManifestResourceNames();
            string stylesResult = string.Empty;
            string stylesPath = string.Format("{0}.{1}.Styles.Office2013.", typeof(StiDesignerResourcesHelper).Namespace, requestParams.Component);
            Hashtable designerConstants = null;
            Hashtable loginConstants = null;

            foreach (var name in resourceNames)
            {
                if (name.IndexOf(stylesPath) == 0 && name.EndsWith(".css"))
                {
                    if (!requestParams.CloudMode && name.IndexOf(".LoginControls") >= 0) continue;
                    if (!StiDashboardDesigner.IsAssemblyLoaded && name.IndexOf(".Dashboards") >= 0) continue;

                    Stream stream = assembly.GetManifestResourceStream(name);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string styleText = reader.ReadToEnd();

                        if (name.EndsWith(requestParams.Theme + ".Constants.css"))
                        {
                            if (name.IndexOf(".LoginControls") >= 0) loginConstants = GetStylesConstants(styleText);
                            else designerConstants = GetStylesConstants(styleText);
                        }
                        else if (!name.EndsWith(".Constants.css")) stylesResult += styleText + "\r\n";
                    }
                }
            }

            if (designerConstants != null)
            {
                foreach (DictionaryEntry constant in designerConstants)
                {
                    stylesResult = stylesResult.Replace((string)constant.Key, (string)constant.Value);
                }
            }

            if (loginConstants != null)
            {
                foreach (DictionaryEntry constant in loginConstants)
                {
                    stylesResult = stylesResult.Replace((string)constant.Key, (string)constant.Value);
                }
            }

            requestParams.Cache.Helper.SaveResourceInternal(requestParams, stylesResult, stylesCacheGuid);

            return Encoding.UTF8.GetBytes(stylesResult);
        }

        #endregion

        #region Flash

        public static byte[] GetFlashResources(StiRequestParams requestParams)
        {
            Assembly assembly = typeof(StiDesignerResourcesHelper).Assembly;
            string resourcePath = string.Format("{0}.{1}.Resources.{2}", typeof(StiDesignerResourcesHelper).Namespace, requestParams.Component, requestParams.Resource);
            Stream stream = assembly.GetManifestResourceStream(resourcePath);
            if (stream == null) return null;

            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            byte[] buffer = ms.ToArray();
            ms.Dispose();
            stream.Dispose();

            return buffer;
        }

        #endregion

        #region Images
        
        /// <summary>
        /// Get Bitmap image for Visual Studio design mode
        /// </summary>
        public static Bitmap GetBitmap(StiRequestParams requestParams, string imageName)
        {
            Assembly assembly = typeof(StiDesignerResourcesHelper).Assembly;
            string resourcePath = string.Format("{0}.{1}.Images.{2}", typeof(StiDesignerResourcesHelper).Namespace, requestParams.Component, imageName);
            Stream stream = assembly.GetManifestResourceStream(resourcePath);
            if (stream == null) return null;
            return new Bitmap(stream);
        }

        public static byte[] GetImage(StiRequestParams requestParams)
        {
            Assembly assembly = typeof(StiDesignerResourcesHelper).Assembly;
            string resourcePath = string.Format("{0}.{1}.Images.Office2013.{2}", typeof(StiDesignerResourcesHelper).Namespace, requestParams.Component, requestParams.Resource);
            Stream stream = assembly.GetManifestResourceStream(resourcePath);
            if (stream == null) return null;

            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            byte[] buffer = ms.ToArray();
            ms.Dispose();
            stream.Dispose();

            return buffer;
        }

        public static Hashtable GetImagesArray(StiRequestParams requestParams, string resourcesUrl)
        {
            Hashtable images = new Hashtable();
            Assembly assembly = typeof(StiDesignerResourcesHelper).Assembly;
            string[] resourceNames = assembly.GetManifestResourceNames();
            string resourcePath = string.Format("{0}.{1}.Images.Office2013.", typeof(StiDesignerResourcesHelper).Namespace, requestParams.Component);

            foreach (var name in resourceNames)
            {
                if (name.IndexOf(resourcePath) == 0 && (name.EndsWith(".png") || name.EndsWith(".gif") || name.EndsWith(".cur")))
                {
                    string imageName = name.Replace(resourcePath, "");

                    if (!requestParams.CloudMode && name.IndexOf(".LoginControls") >= 0) continue;
                    if (!StiDashboardDesigner.IsAssemblyLoaded && name.IndexOf(".Dashboards") >= 0) continue;

                    if (string.IsNullOrEmpty(resourcesUrl) || imageName.IndexOf("_FirstLoadingImages") >= 0)
                    {
                        imageName = imageName.Replace("_FirstLoadingImages.", "");

                        Stream stream = assembly.GetManifestResourceStream(name);
                        if (stream == null) return null;

                        MemoryStream ms = new MemoryStream();
                        stream.CopyTo(ms);
                        byte[] buffer = ms.ToArray();
                        ms.Dispose();
                        stream.Dispose();

                        if (name.EndsWith(".cur")) images[imageName] = string.Format("data:application/cur;base64,{0}", Convert.ToBase64String(buffer));
                        else images[imageName] = string.Format("data:image/{0};base64,{1}", imageName.Substring(imageName.Length - 3), Convert.ToBase64String(buffer));
                    }
                    else
                    {
                        images[imageName] = StiWebDesigner.GetImageUrl(requestParams, resourcesUrl + imageName);
                    }
                }
            }

            return images;
        }

        #endregion
    }
}
