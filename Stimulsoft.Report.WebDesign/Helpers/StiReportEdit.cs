#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Text;
using Stimulsoft.Report.Components;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.BarCodes;
using Stimulsoft.Report.Chart;
using Stimulsoft.Report.CrossTab;
using System.Drawing;
using System.Collections;
using Stimulsoft.Report.Components.Table;
using System.Globalization;
using Stimulsoft.Report.Components.ShapeTypes;
using System.Drawing.Drawing2D;
using Stimulsoft.Report.Components.TextFormats;
using System.Drawing.Printing;
using Stimulsoft.Report.Dictionary;
using System.Drawing.Imaging;
using System.IO;
using Stimulsoft.Base;
using Stimulsoft.Base.Serializing;
using System.Reflection;
using Stimulsoft.Report.Gauge;
using Stimulsoft.Base.Localization;
using Stimulsoft.Report.Events;
using Stimulsoft.Report.Maps;
using Stimulsoft.Report.Helpers;
using Stimulsoft.Report.Dashboard;
using Stimulsoft.Report.Web.Heplers.Dashboards;

namespace Stimulsoft.Report.Web
{
    internal class StiReportEdit
    {        
        #region Helpers methods
        public static void SetPropertyValue(StiReport report, string propertyName, object owner, object value)
        {
            #region DBS
            if (owner is IStiTextFont && propertyName == "Font")
            {
                var font = StiReportEdit.StrToFont((string)value, report);
                ((IStiTextFont)owner).SetFontName(font.FontFamily.Name);
                ((IStiTextFont)owner).SetFontSize(font.Size);
                ((IStiTextFont)owner).SetFontBoldStyle(font.Bold);
                ((IStiTextFont)owner).SetFontItalicStyle(font.Italic);
                ((IStiTextFont)owner).SetFontUnderlineStyle(font.Underline);
            }
            #endregion

            PropertyInfo property = null;
            string[] propertyArray = propertyName.Split('.');
            object parentOwner = null;

            if (propertyArray.Length == 1)
                property = owner.GetType().GetProperty(propertyName);
            else
            {
                for (int i = 0; i < propertyArray.Length; i++)
                {
                    parentOwner = owner;
                    property = owner.GetType().GetProperty(propertyArray[i]);

                    if (property != null)
                        owner = property.GetValue(owner, null);
                    else
                        return;
                }
                owner = parentOwner;
            }

            if (property != null)
            {
                //Decode string from Base64
                if (value is string && ((string)value).StartsWith("Base64Code;"))
                {
                    value = StiEncodingHelper.DecodeString(((string)value).Replace("Base64Code;", ""));
                }

                #region string, bool
                if (property.PropertyType == typeof(string) || property.PropertyType == typeof(bool))
                {
                    property.SetValue(owner, value, null);
                }
                #endregion

                #region int
                else if (property.PropertyType == typeof(int))
                {
                    property.SetValue(owner, StiReportEdit.StrToInt((string)value), null);
                }
                #endregion

                #region double
                else if (property.PropertyType == typeof(double))
                {
                    property.SetValue(owner, StiReportEdit.StrToDouble((string)value), null);
                }
                #endregion

                #region float
                else if (property.PropertyType == typeof(float))
                {
                    property.SetValue(owner, (float)StiReportEdit.StrToDouble((string)value), null);
                }
                #endregion

                #region StiPenStyle
                else if (property.PropertyType == typeof(StiPenStyle))
                {
                    property.SetValue(owner, (StiPenStyle)StiReportEdit.StrToInt((string)value), null);
                }
                #endregion                

                #region PaperKind
                else if (property.PropertyType == typeof(PaperKind))
                {
                    property.SetValue(owner, (PaperKind)StiReportEdit.StrToInt((string)value), null);
                }
                #endregion   

                #region StiGroupSortDirection
                else if (property.PropertyType == typeof(StiGroupSortDirection))
                {
                    property.SetValue(owner, (StiGroupSortDirection)StiReportEdit.StrToInt((string)value), null);
                }
                #endregion 

                #region StiGroupSummaryType
                else if (property.PropertyType == typeof(StiGroupSummaryType))
                {
                    property.SetValue(owner, (StiGroupSummaryType)StiReportEdit.StrToInt((string)value), null);
                }
                #endregion 

                #region Color
                else if (property.PropertyType == typeof(Color))
                {
                    property.SetValue(owner, StiReportEdit.StrToColor((string)value), null);
                }
                #endregion

                #region StiBrush
                else if (property.PropertyType == typeof(StiBrush))
                {
                    property.SetValue(owner, StiReportEdit.StrToBrush((string)value), null);
                }
                #endregion

                #region StiBorder
                else if (property.PropertyType == typeof(StiBorder))
                {
                    property.SetValue(owner, StiReportEdit.StrToBorder((string)value), null);
                }
                #endregion

                #region StiMargins
                else if (property.PropertyType == typeof(StiMargins))
                {
                    StiMargins margins = new StiMargins();
                    string[] marginsArray = ((string)value).Split(';');
                    margins.Left = StrToDouble(marginsArray[0]);
                    margins.Right = StrToDouble(marginsArray[1]);
                    margins.Top = StrToDouble(marginsArray[2]);
                    margins.Bottom = StrToDouble(marginsArray[3]);

                    property.SetValue(owner, margins, null);
                }
                #endregion

                #region Font
                else if (property.PropertyType == typeof(Font))
                {
                    property.SetValue(owner, StiReportEdit.StrToFont((string)value, report), null);
                }
                #endregion

                #region Size
                else if (property.PropertyType == typeof(Size))
                {
                    Size propertyValue = (Size)property.GetValue(owner, null);

                    string[] sizes = ((string)value).Split(';');
                    if (sizes.Length == 2)
                    {
                        propertyValue.Width = StiReportEdit.StrToInt(sizes[0]);
                        propertyValue.Height = StiReportEdit.StrToInt(sizes[1]);
                    }
                    property.SetValue(owner, propertyValue, null);
                }
                #endregion

                #region SizeD
                else if (property.PropertyType == typeof(SizeD))
                {
                    SizeD propertyValue = (SizeD)property.GetValue(owner, null);

                    string[] sizes = ((string)value).Split(';');
                    if (sizes.Length == 2)
                    {
                        propertyValue.Width = StiReportEdit.StrToDouble(sizes[0]);
                        propertyValue.Height = StiReportEdit.StrToDouble(sizes[1]);
                    }
                    property.SetValue(owner, propertyValue, null);
                }
                #endregion

                #region StiPage
                else if (property.PropertyType == typeof(StiPage))
                {   
                    StiPage page = report.Pages[(string)value];
                    property.SetValue(owner, page, null);
                }
                #endregion

                #region StiExpression
                else if (property.PropertyType.Name.StartsWith("Sti") && property.PropertyType.Name.EndsWith("Expression") && property.PropertyType.IsClass)
                {
                    var propertyExpression = property.GetValue(owner, null);
                    if (propertyExpression != null)
                    {
                        var propertyValue = propertyExpression.GetType().GetProperty("Value");
                        if (propertyValue != null)
                        {
                            propertyValue.SetValue(propertyExpression, (string)value, null);
                        }
                    }
                }
                #endregion

                #region Enum
                else if (property.PropertyType.IsEnum)
                {
                    property.SetValue(owner, Enum.Parse(property.PropertyType, (string)value), null);
                }
                #endregion

                #region StiDataRelaton
                else if (property.PropertyType == typeof(StiDataRelation))
                {
                    StiReportEdit.SetDataRelationProperty(owner, (string)value);
                }
                #endregion

                #region StiDataSource
                else if (property.PropertyType == typeof(StiDataSource))
                {
                    StiReportEdit.SetDataSourceProperty(owner, (string)value);
                }
                #endregion

                #region StiBusinessObject
                else if (property.PropertyType == typeof(StiBusinessObject))
                {
                    StiReportEdit.SetBusinessObjectProperty(owner, (string)value);
                }
                #endregion

                #region MasterComponent
                else if (property.PropertyType == typeof(StiComponent))
                {
                    StiReportEdit.SetMasterComponentProperty(owner, (string)value);
                }
                #endregion

                #region StiFiltersCollection
                else if (property.PropertyType == typeof(StiFiltersCollection))
                {
                    SetFiltersProperty(owner, (Hashtable)value);
                }
                #endregion

                #region Sort
                else if (property.PropertyType == typeof(string[]) && propertyName == "Sort")
                {
                    StiReportEdit.SetSortDataProperty((StiComponent)owner, (string)value);
                }
                #endregion

                #region Image
                if (property.PropertyType == typeof(Image))
                {
                    string propertyValue = value as string;
                    if (!string.IsNullOrEmpty(propertyValue) && propertyValue.StartsWith("data:image"))
                    {
                        property.SetValue(owner, Base64ToImage(propertyValue), null);
                    }
                    else
                    {
                        property.SetValue(owner, null, null);
                    }
                }
                #endregion

                #region Conditions
                else if (property.PropertyType == typeof(StiConditionsCollection))
                {
                    StiReportEdit.SetConditionsProperty(owner, value, report);
                }
                #endregion                

                #region Interaction
                else if (property.PropertyType == typeof(StiInteraction))
                {
                    StiReportEdit.SetInteractionProperty(owner, value);
                }
                #endregion

                #region TextFormat
                else if (property.PropertyType == typeof(StiFormatService))
                {
                    StiReportEdit.SetTextFormatProperty(owner, value);
                }
                #endregion
            }
        }

        public static object GetPropertyValue(string propertyName, object owner)
        {
            if (owner == null) return null;
            PropertyInfo property = null;
            string[] propertyArray = propertyName.Split('.');
            object parentOwner = null;

            if (propertyArray.Length == 1)
                property = owner.GetType().GetProperty(propertyName);
            else
            {
                for (int i = 0; i < propertyArray.Length; i++)
                {
                    parentOwner = owner;
                    property = owner.GetType().GetProperty(propertyArray[i]);
                    if (property != null)
                    {
                        owner = property.GetValue(owner, null);
                    }
                    else
                        return null;
                }
                owner = parentOwner;
            }

            if (property != null)
            {
                var value = property.GetValue(owner, null);
                if (value == null) return string.Empty;

                if (value is string) return "Base64Code;" + StiEncodingHelper.Encode((string)value);
                if (value is bool) return (bool)value;
                if (value is double || value is float) return value.ToString().Replace(",", ".");
                if (value is int) return value.ToString();
                if (value is StiPenStyle) return ((int)value).ToString();
                if (value is StiExpression)
                    return "Base64Code;" + StiEncodingHelper.Encode(((StiExpression)value).Value);
                if (value is StiArgumentExpression) return "Base64Code;" + StiEncodingHelper.Encode(((StiArgumentExpression)value).Value);
                if (value is StiListOfArgumentsExpression) return "Base64Code;" + StiEncodingHelper.Encode(((StiListOfArgumentsExpression)value).Value);
                if (value is Color) return StiReportEdit.GetStringFromColor((Color)value);
                if (value is StiBrush) return StiReportEdit.BrushToStr((StiBrush)value);
                if (value is StiBorder) return StiReportEdit.BorderToStr((StiBorder)value);
                if (value is Font) return StiReportEdit.FontToStr((Font)value);
                if (value is Size) return ((Size)value).Width + ";" + ((Size)value).Height;
                if (value is StiDataRelation) return ((StiDataRelation)value).NameInSource;
                if (value is StiDataSource) return ((StiDataSource)value).Name;
                if (value is StiBusinessObject) return ((StiBusinessObject)value).GetFullName();
                if (value is StiFiltersCollection) return StiReportEdit.GetFiltersProperty(owner);
                if (value is string[] && propertyName == "Sort") return StiReportEdit.GetSortDataProperty((StiComponent)owner);
                if (value is Image) return (value != null) ? ImageToBase64((Image)value) : String.Empty;
                if (value is StiMargins) return string.Format("{0};{1};{2};{3}", ((StiMargins)value).Left, ((StiMargins)value).Right, ((StiMargins)value).Top, ((StiMargins)value).Bottom);
                if (value is StiEvent) return ((StiEvent)value).Script;

                return value.ToString();
            }

            return null;
        }

        public static Hashtable GetFiltersProperty(object obj)
        {
            Hashtable properties = new Hashtable();
            properties["filterData"] = GetFilterDataProperty(obj);
            properties["filterOn"] = GetFilterOnProperty(obj);
            properties["filterMode"] = GetFilterModeProperty(obj);

            return properties;
        }

        public static void SetFiltersProperty(object obj, Hashtable value)
        {
            SetFilterDataProperty(obj, value["filterData"]);
            SetFilterOnProperty(obj, value["filterOn"]);
            SetFilterModeProperty(obj, value["filterMode"]);
        }

        public static string LowerFirstChar(string text)
        {
            return text[0].ToString().ToLower() + text.Substring(1);
        }

        public static string UpperFirstChar(string text)
        {
            string[] textArray = text.Split('.');
            if (textArray.Length == 1)
            {
                return text[0].ToString().ToUpper() + text.Substring(1);
            }
            else
            {
                string result = string.Empty;
                foreach (string word in textArray)
                {
                    if (result != string.Empty) result += ".";
                    result += word[0].ToString().ToUpper() + word.Substring(1);
                }
                return result;
            }
        }

        public static Image Base64ToImage(string base64String)
        {
            return StiImageConverter.StringToImage(base64String.Substring(base64String.IndexOf("base64,") + 7));
        }

        public static string ImageToBase64(byte[] image)
        {
            string mimeType = "data:image/png;base64,";

            if (Stimulsoft.Report.Helpers.StiImageHelper.IsMetafile(image))
            {
                mimeType = "data:image/x-wmf;base64,";
            }
            else if (Stimulsoft.Report.Helpers.StiImageHelper.IsBmp(image))
            {
                mimeType = "data:image/bmp;base64,";
            }
            else if (Stimulsoft.Report.Helpers.StiImageHelper.IsJpeg(image))
            {
                mimeType = "data:image/jpeg;base64,";
            }
            else if (Stimulsoft.Report.Helpers.StiImageHelper.IsGif(image))
            {
                mimeType = "data:image/gif;base64,";
            }
            else if (Stimulsoft.Base.Helpers.StiSvgHelper.IsSvg(image))
            {
                mimeType = "data:image/svg+xml;base64,";
            }

            return mimeType + Convert.ToBase64String(image);
        }

        public static string ImageToBase64(Image image)
        {
            return ImageToBase64(StiImageConverter.ImageToBytes(image));
        }

        public static string FontToStr(Font font)
        {
            string fontStr = string.Empty;

            if (font != null)
            {
                //OriginalFontName - temporarily for fonts from resources
                string fontName = !String.IsNullOrEmpty(font.OriginalFontName) ? font.OriginalFontName : font.Name;

                fontStr = string.Format("{0}!{1}!{2}!{3}!{4}!{5}",
                    fontName,
                    font.Size.ToString(),
                    font.Bold ? "1" : "0",
                    font.Italic ? "1" : "0",
                    font.Underline ? "1" : "0",
                    font.Strikeout ? "1" : "0");

            }

            return fontStr;
        }

        public static string BrushToStr(StiBrush brush)
        {
            string brushStr = "none";

            if (brush is StiEmptyBrush)
            {
                brushStr = "0";
            }
            else if (brush is StiSolidBrush)
            {
                StiSolidBrush solid = brush as StiSolidBrush;
                brushStr = string.Format("1!{0}",
                    GetStringFromColor(solid.Color));
            }
            else if (brush is StiHatchBrush)
            {
                StiHatchBrush hatch = brush as StiHatchBrush;
                brushStr = string.Format("2!{0}!{1}!{2}",
                    GetStringFromColor(hatch.ForeColor),
                    GetStringFromColor(hatch.BackColor),
                    ((int)hatch.Style).ToString()
                    );
            }
            else if (brush is StiGradientBrush)
            {
                StiGradientBrush gradient = brush as StiGradientBrush;
                brushStr = string.Format("3!{0}!{1}!{2}",
                    GetStringFromColor(gradient.StartColor),
                    GetStringFromColor(gradient.EndColor),
                    gradient.Angle.ToString()
                    );
            }
            else if (brush is StiGlareBrush)
            {
                StiGlareBrush glare = brush as StiGlareBrush;
                brushStr = string.Format("4!{0}!{1}!{2}!{3}!{4}",
                    GetStringFromColor(glare.StartColor),
                    GetStringFromColor(glare.EndColor),
                    glare.Angle.ToString(),
                    glare.Focus.ToString(),
                    glare.Scale.ToString()
                    );
            }
            else if (brush is StiGlassBrush)
            {
                StiGlassBrush glass = brush as StiGlassBrush;
                brushStr = string.Format("5!{0}!{1}!{2}",
                    GetStringFromColor(glass.Color),
                    glass.Blend.ToString(),
                    (glass.DrawHatch) ? "1" : "0"
                    );
            }

            return brushStr;
        }

        public static string BorderToStr(StiBorder border)
        {
            string borderStr = string.Empty;

            borderStr = string.Format("{0},{1},{2},{3}!{4}!{5}!{6}!{7}!{8}!{9}!{10}",
                (border.IsLeftBorderSidePresent) ? "1" : "0",
                (border.IsTopBorderSidePresent) ? "1" : "0",
                (border.IsRightBorderSidePresent) ? "1" : "0",
                (border.IsBottomBorderSidePresent) ? "1" : "0",
                border.Size.ToString(),
                GetStringFromColor(border.Color),
                ((int)border.Style).ToString(),
                (border.DropShadow) ? "1" : "0",
                border.ShadowSize.ToString(),
                StiEncodingHelper.Encode(BrushToStr(border.ShadowBrush)),
                border.Topmost ? "1" : "0"
            );

            return borderStr;
        }

        public static string GetStringFromColor(Color color)
        {
            if (color.A == 0)
                return "transparent";
            else
            {
                if (color.A == 255) return string.Format("{0},{1},{2}", color.R.ToString(), color.G.ToString(), color.B.ToString());
                else return string.Format("{0},{1},{2},{3}", color.A.ToString(), color.R.ToString(), color.G.ToString(), color.B.ToString());
            }
        }

        public static void AddComponentToPage(StiComponent component, StiPage currentPage)
        {
            currentPage.Components.Add(component);
            component.Select();
        }

        public static string GetParentName(StiComponent comp)
        {
            return (comp.Parent == null) ? string.Empty : comp.Parent.Name;
        }

        public static int GetParentIndex(StiComponent comp)
        {
            int index = 0;

            if (comp is StiCrossLinePrimitive)
            {
                int index1 = GetParentIndex(((StiCrossLinePrimitive)comp).GetStartPoint());
                int index2 = GetParentIndex(((StiCrossLinePrimitive)comp).GetStartPoint());
                return Math.Max(index1, index2);
            }

            StiComponent parent = comp.Parent;
            while (true)
            {
                if (parent == null || parent is StiPage)
                    return index;

                parent = parent.Parent;
                index++;
            }
        }

        public static int GetComponentIndex(StiComponent component)
        {
            StiContainer parent = component.Parent as StiContainer;
            List<StiComponent> componentsList = parent.GetComponentsList();
            int index = 0;
            foreach (StiComponent comp in componentsList)
            {
                if (comp == component) return index;
                index++;
            }

            return index;
        }

        public static string GetAllChildComponents(StiComponent component)
        {
            string allChildComponents = string.Empty;
            if (component == null) return allChildComponents;
            StiPage currentPage = component.Page;
            if (currentPage == null) return allChildComponents;
            StiComponentsCollection components = currentPage.GetComponents();

            foreach (StiComponent otherComponent in components)
            {
                if (otherComponent is StiCrossLinePrimitive)
                {
                    StiStartPointPrimitive startPoint = ((StiCrossLinePrimitive)otherComponent).GetStartPoint();
                    StiEndPointPrimitive endPoint = ((StiCrossLinePrimitive)otherComponent).GetEndPoint();
                    if (GetParentName(startPoint) == component.Name || GetParentName(endPoint) == component.Name)
                        allChildComponents += otherComponent.Name + ",";
                }
                else
                {
                    if (GetParentName(otherComponent) == component.Name)
                        allChildComponents += otherComponent.Name + ",";

                }
            }
            if (allChildComponents != "")
                allChildComponents = allChildComponents.Substring(0, allChildComponents.Length - 1);

            return allChildComponents;
        }

        public static Hashtable GetPropsRebuildPage(StiReport report, StiPage currentPage)
        {
            Hashtable props = new Hashtable();
            StiComponentsCollection components = currentPage.GetComponents();

            foreach (StiComponent component in components)
            {
                Hashtable compProperties = new Hashtable();
                props[component.Name] = compProperties;
                compProperties["componentRect"] = GetComponentRect(component);
                compProperties["parentName"] = GetParentName(component);
                compProperties["parentIndex"] = GetParentIndex(component).ToString();
                compProperties["componentIndex"] = GetComponentIndex(component).ToString();
                compProperties["childs"] = GetAllChildComponents(component);
                compProperties["clientLeft"] = DoubleToStr(component.Left);
                compProperties["clientTop"] = DoubleToStr(component.Top);
            }

            return props;
        }

        public static Hashtable GetPageIndexes(StiReport report)
        {
            Hashtable pageIndexes = new Hashtable();
            for (int index = 0; index < report.Pages.Count; index++)
                pageIndexes[report.Pages[index].Name] = index.ToString();

            return pageIndexes;
        }

        public static void SetComponentRectWithOffset(StiComponent comp, RectangleD newCompRect, string command, string resizeType, Hashtable compProps)
        {
            StiPage currentPage = comp.Page;

            RectangleD currCompRect = currentPage.Unit.ConvertFromHInches(comp.GetPaintRectangle());

            //set not selected all components            
            foreach (StiComponent component in currentPage.GetComponents())
            {
                component.IsSelected = false;
            }

            RectangleD rect = new RectangleD();

            if (!string.IsNullOrEmpty(resizeType) && resizeType.StartsWith("Multi"))
            {
                rect = new RectangleD(
                    currCompRect.Left - newCompRect.Left,
                    currCompRect.Top - newCompRect.Top,
                    newCompRect.Width - currCompRect.Width,
                    newCompRect.Height - currCompRect.Height
                ).AlignToGrid(currentPage.GridSize, currentPage.Report.Info.AlignToGrid);
            }
            else if (command == "MoveComponent")
            {
                rect = new RectangleD(
                    currCompRect.Left - newCompRect.Left,
                    currCompRect.Top - newCompRect.Top,
                    0, 0
                ).AlignToGrid(currentPage.GridSize, currentPage.Report.Info.AlignToGrid);
            }
            else if (command == "ResizeComponent")
            {
                bool invertWidth = compProps["invertWidth"] != null && (bool)compProps["invertWidth"];
                bool invertHeight = compProps["invertHeight"] != null && (bool)compProps["invertHeight"];

                switch (resizeType)
                {
                    case "Left":
                        {
                            rect = new RectangleD(
                                invertWidth ? -(currCompRect.Width + newCompRect.Width) : currCompRect.Left - newCompRect.Left, 0,
                                invertWidth ? -(currCompRect.Width + newCompRect.Width) : currCompRect.Left - newCompRect.Left, 0
                            ).AlignToGrid(currentPage.GridSize, currentPage.Report.Info.AlignToGrid);
                            break;
                        }
                    case "Right":
                    case "ResizeWidth":
                        {
                            rect = new RectangleD(
                                0, 0,
                                invertWidth ? -(currCompRect.Width + newCompRect.Width) : newCompRect.Width - currCompRect.Width, 0
                            ).AlignToGrid(currentPage.GridSize, currentPage.Report.Info.AlignToGrid);
                            break;
                        }
                    case "Top":
                        {
                            rect = new RectangleD(
                                0, invertHeight ? -(currCompRect.Height + newCompRect.Height) : currCompRect.Top - newCompRect.Top,
                                0, invertHeight ? -(currCompRect.Height + newCompRect.Height) : currCompRect.Top - newCompRect.Top
                            ).AlignToGrid(currentPage.GridSize, currentPage.Report.Info.AlignToGrid);
                            break;
                        }
                    case "Bottom":
                    case "ResizeHeight":
                        {
                            rect = new RectangleD(
                                0, 0, 0,
                                invertHeight ? -(currCompRect.Height + newCompRect.Height) : newCompRect.Height - currCompRect.Height
                            ).AlignToGrid(currentPage.GridSize, currentPage.Report.Info.AlignToGrid);
                            break;
                        }
                    case "LeftTop":
                        {
                            rect = new RectangleD(
                                invertWidth ? -(currCompRect.Width + newCompRect.Width) : currCompRect.Left - newCompRect.Left,
                                invertHeight ? -(currCompRect.Height + newCompRect.Height) : currCompRect.Top - newCompRect.Top,
                                invertWidth ? -(currCompRect.Width + newCompRect.Width) : currCompRect.Left - newCompRect.Left,
                                invertHeight ? -(currCompRect.Height + newCompRect.Height) : currCompRect.Top - newCompRect.Top
                            ).AlignToGrid(currentPage.GridSize, currentPage.Report.Info.AlignToGrid);
                            break;
                        }
                    case "LeftBottom":
                        {
                            rect = new RectangleD(
                                invertWidth ? -(currCompRect.Width + newCompRect.Width) : currCompRect.Left - newCompRect.Left, 0,
                                invertWidth ? -(currCompRect.Width + newCompRect.Width) : currCompRect.Left - newCompRect.Left,
                                invertHeight ? -(currCompRect.Height + newCompRect.Height) : newCompRect.Height - currCompRect.Height
                            ).AlignToGrid(currentPage.GridSize, currentPage.Report.Info.AlignToGrid);
                            break;
                        }
                    case "RightTop":
                        {
                            rect = new RectangleD(
                                0, invertHeight ? -(currCompRect.Height + newCompRect.Height) : currCompRect.Top - newCompRect.Top,
                                invertWidth ? -(currCompRect.Width + newCompRect.Width) : newCompRect.Width - currCompRect.Width,
                                invertHeight ? -(currCompRect.Height + newCompRect.Height) : currCompRect.Top - newCompRect.Top
                            ).AlignToGrid(currentPage.GridSize, currentPage.Report.Info.AlignToGrid);
                            break;
                        }
                    case "ResizeDiagonal":
                    case "RightBottom":
                        {
                            rect = new RectangleD(
                                0, 0,
                                invertWidth ? -(currCompRect.Width + newCompRect.Width) : newCompRect.Width - currCompRect.Width,
                                invertHeight ? -(currCompRect.Height + newCompRect.Height) : newCompRect.Height - currCompRect.Height
                            ).AlignToGrid(currentPage.GridSize, currentPage.Report.Info.AlignToGrid);
                            break;
                        }
                }
            }

            comp.Select();
            currentPage.ChangePosition(rect);

            if (comp is StiCrossLinePrimitive)
            {
                ChangeRectPrimitivePoints(comp, currentPage.Unit.ConvertFromHInches(comp.GetPaintRectangle()));
            }

            if (comp is StiCrossDataBand || comp is StiCrossHeaderBand || comp is StiCrossFooterBand
                || comp is StiCrossGroupHeaderBand || comp is StiCrossGroupFooterBand)
            {
                comp.GetContainer().Components.SortBandsByLeftPosition();
            }
            else if (comp is StiBand)
            {
                comp.GetContainer().Components.SortBandsByTopPosition();
            }

            comp.Page.Correct();
            comp.Page.Normalize();
            comp.Invert();
        }

        public static void SetComponentRect(StiComponent component, RectangleD rect, bool alignToGrid = true)
        {
            component.Select();

            if (alignToGrid)
            {
                rect = rect.AlignToGrid(component.Page.GridSize, component.Report.Info.AlignToGrid);
            }

            if (component is StiCrossLinePrimitive)
            {
                ChangeRectPrimitivePoints(component, rect);
            }

            component.SetPaintRectangle(rect);

            if (component is StiCrossDataBand || component is StiCrossHeaderBand || component is StiCrossFooterBand
                || component is StiCrossGroupHeaderBand || component is StiCrossGroupFooterBand)
            {
                component.GetContainer().Components.SortBandsByLeftPosition();
            }
            else if (component is StiBand)
            {
                component.GetContainer().Components.SortBandsByTopPosition();
            }

            component.Page.Correct();
        }

        public static string GetComponentRect(StiComponent component)
        {
            RectangleD rect = component.GetPaintRectangle(false, false);

            string componentRect = string.Format("{0}!{1}!{2}!{3}",
                DoubleToStr(rect.Left),
                DoubleToStr(rect.Top),
                DoubleToStr(rect.Width),
                DoubleToStr(rect.Height));

            return componentRect;
        }

        public static string GetPageSize(StiPage page)
        {
            string pageRect = string.Format("{0}!{1}",
                DoubleToStr(page.PageWidth),
                DoubleToStr(page.PageHeight)
                );

            return pageRect;
        }

        public static string GetPageMargins(StiPage page)
        {
            StiMargins margins = page.Margins;

            string result = string.Format("{0}!{1}!{2}!{3}",
                DoubleToStr(margins.Left),
                DoubleToStr(margins.Top),
                DoubleToStr(margins.Right),
                DoubleToStr(margins.Bottom));

            return result;
        }

        public static void GetAllComponentsPositions(StiReport report, Hashtable callbackResult)
        {
            ArrayList compPositions = new ArrayList();
            ArrayList pagePositions = new ArrayList();

            foreach (StiPage page in report.Pages)
            {
                Hashtable pageObject = new Hashtable();
                pagePositions.Add(pageObject);
                pageObject["name"] = page.Name;
                pageObject["size"] = GetPageSize(page);
                pageObject["margins"] = GetPageMargins(page);
                pageObject["columnWidth"] = DoubleToStr(page.ColumnWidth);
                pageObject["columnGaps"] = DoubleToStr(page.ColumnGaps);

                StiComponentsCollection components = page.GetComponents();
                foreach (StiComponent component in components)
                {
                    Hashtable compObject = new Hashtable();
                    compPositions.Add(compObject);
                    compObject["pageName"] = page.Name;
                    compObject["name"] = component.Name;
                    compObject["componentRect"] = GetComponentRect(component);
                    compObject["compPos"] = DoubleToStr(component.Left) + "!" + DoubleToStr(component.Top);
                    if (component is StiPanel || component is StiDataBand || component is StiHierarchicalBand) {
                        double columnWidth = 0;
                        double columnGaps = 0;
                        PropertyInfo columnWidthProp = component.GetType().GetProperty("ColumnWidth");
                        if (columnWidthProp != null) columnWidth = (double)columnWidthProp.GetValue(component, null);
                        PropertyInfo columnGapsProp = component.GetType().GetProperty("ColumnGaps");
                        if (columnGapsProp != null) columnGaps = (double)columnGapsProp.GetValue(component, null);
                        compObject["columnWidth"] = DoubleToStr(columnWidth);
                        compObject["columnGaps"] = DoubleToStr(columnGaps);
                    }
                    if (component is StiCrossTab)
                    {
                        compObject["crossTabFields"] = GetCrossTabFieldsProperties(component as StiCrossTab);
                    }
                }
            }

            callbackResult["pagePositions"] = pagePositions;
            callbackResult["compPositions"] = compPositions;
        }

        private static Hashtable GetIconSetItemObject(StiIconSetItem iconSetItem)
        {
            if (iconSetItem == null) return null;

            Hashtable iconSetItemObject = new Hashtable();
            iconSetItemObject["Icon"] = iconSetItem.Icon;
            iconSetItemObject["Operation"] = iconSetItem.Operation;
            iconSetItemObject["ValueType"] = iconSetItem.ValueType;
            iconSetItemObject["Value"] = iconSetItem.Value;

            return iconSetItemObject;
        }

        private static StiIconSetItem GetIconSetItemFromObject(object iconSetItemObject)
        {
            if (iconSetItemObject == null) return null;
            Hashtable iconSetItem = iconSetItemObject as Hashtable;

            return new StiIconSetItem(
                iconSetItem["Icon"] != null ? (StiIcon)Enum.Parse(typeof(StiIcon), (string)iconSetItem["Icon"]) : StiIcon.None,
                iconSetItem["Operation"] != null ? (StiIconSetOperation)Enum.Parse(typeof(StiIconSetOperation), (string)iconSetItem["Operation"]) : StiIconSetOperation.MoreThanOrEqual,
                iconSetItem["ValueType"] != null ? (StiIconSetValueType)Enum.Parse(typeof(StiIconSetValueType), (string)iconSetItem["ValueType"]) : StiIconSetValueType.Percent,
                iconSetItem["Value"] != null ? (float)StrToDouble((string)iconSetItem["Value"]) : 0f
            );
        }

        public static Color StrToColor(string colorStr)
        {
            Color newColor = Color.Transparent;

            if (colorStr != "transparent")
            {
                string[] colors = colorStr.Split(',');

                if (colors.Length == 3)
                {
                    int r1 = StrToInt(colors[0]);
                    int g1 = StrToInt(colors[1]);
                    int b1 = StrToInt(colors[2]);
                    newColor = Color.FromArgb(255, ((r1 > 255) ? 255 : r1), ((g1 > 255) ? 255 : g1), ((b1 > 255) ? 255 : b1));

                }
                else
                {
                    int a = StrToInt(colors[0]);
                    int r2 = StrToInt(colors[1]);
                    int g2 = StrToInt(colors[2]);
                    int b2 = StrToInt(colors[3]);
                    newColor = Color.FromArgb(((a > 255) ? 255 : a), ((r2 > 255) ? 255 : r2), ((g2 > 255) ? 255 : g2), ((b2 > 255) ? 255 : b2));
                }
            }

            return newColor;
        }

        public static double StrToDouble(string value)
        {
            CultureInfo currentCulture = CultureInfo.CurrentCulture;
            string numSep = currentCulture.NumberFormat.NumberDecimalSeparator;
            value = value.Replace(",", ".").Replace(".", numSep);

            double result = 0;
            double.TryParse(value, out result);

            return result;
        }

        public static string DoubleToStr(object value)
        {
            CultureInfo currentCulture = CultureInfo.CurrentCulture;
            string numSep = currentCulture.NumberFormat.NumberDecimalSeparator;
            string str = value.ToString();
            str = str.Replace(numSep, ".").Replace(",", ".");

            return str;
        }

        public static int StrToInt(string value)
        {
            int result = 0;
            int.TryParse(value, out result);

            return result;
        }

        public static StiBrush StrToBrush(string value)
        {
            string[] brushArray = value.Split('!');

            switch (brushArray[0])
            {
                case "0":
                    {
                        StiEmptyBrush emptyBrush = new StiEmptyBrush();
                        return emptyBrush;
                    }
                case "1":
                    {
                        StiSolidBrush solidBrush = new StiSolidBrush(StrToColor(brushArray[1]));
                        return solidBrush;
                    }
                case "2":
                    {
                        StiHatchBrush hatchBrush = new StiHatchBrush((HatchStyle)StrToInt(brushArray[3]), StrToColor(brushArray[1]), StrToColor(brushArray[2]));
                        return hatchBrush;
                    }
                case "3":
                    {
                        StiGradientBrush gradientBrush = new StiGradientBrush(StrToColor(brushArray[1]), StrToColor(brushArray[2]), StrToDouble(brushArray[3]));
                        return gradientBrush;
                    }
                case "4":
                    {
                        StiGlareBrush glareBrush = new StiGlareBrush(StrToColor(brushArray[1]), StrToColor(brushArray[2]), StrToDouble(brushArray[3]),
                            (float)StrToDouble(brushArray[4]), (float)StrToDouble(brushArray[5]));
                        return glareBrush;
                    }
                case "5":
                    {
                        StiGlassBrush glassBrush = new StiGlassBrush(StrToColor(brushArray[1]), brushArray[3] == "1", (float)StrToDouble(brushArray[2]));
                        return glassBrush;
                    }

            }

            return new StiEmptyBrush();
        }

        public static StiBorder StrToBorder(string value)
        {
            string[] borderArray = value.Split('!');
            string[] borderSides = borderArray[0].Split(',');

            StiBorderSides borderLeft = (borderSides[0] == "1") ? StiBorderSides.Left : StiBorderSides.None;
            StiBorderSides borderTop = (borderSides[1] == "1") ? StiBorderSides.Top : StiBorderSides.None;
            StiBorderSides borderRight = (borderSides[2] == "1") ? StiBorderSides.Right : StiBorderSides.None;
            StiBorderSides borderBottom = (borderSides[3] == "1") ? StiBorderSides.Bottom : StiBorderSides.None;

            double size = StrToDouble(borderArray[1]);
            Color color = StrToColor(borderArray[2]);
            StiPenStyle style = (StiPenStyle)StrToInt(borderArray[3]);
            bool dropShadow = borderArray[4] == "1";
            double shadowSize = StrToDouble(borderArray[5]);
            bool topmost = borderArray[7] == "1";

            StiBorder result = new StiBorder(
                borderLeft | borderTop | borderRight | borderBottom,
                color,
                size,
                style,
                dropShadow,
                shadowSize,
                StrToBrush(StiEncodingHelper.DecodeString(borderArray[6])),
                topmost
            );

            return result;
        }

        public static Font StrToFont(string value, StiReport report)
        {
            string[] fontArray = value.Split('!');
            var fontName = fontArray[0];

            var fontStyle = (FontStyle)0;

            if (fontArray[2] == "1")
                fontStyle |= FontStyle.Bold;

            if (fontArray[3] == "1")
                fontStyle |= FontStyle.Italic;

            if (fontArray[4] == "1")
                fontStyle |= FontStyle.Underline;

            if (fontArray[5] == "1")
                fontStyle |= FontStyle.Strikeout;


            FontFamily fontFamily = StiFontCollection.IsCustomFont(fontName) ? StiFontCollection.GetFontFamily(fontName) : new FontFamily(fontName);
            try
            {
                return StiFontCollection.CreateFont(fontName, (float)StrToDouble(fontArray[1]), StiFontUtils.CorrectStyle(fontFamily.Name, fontStyle));
            }
            finally
            {
                if (!StiFontCollection.IsCustomFont(fontName)) fontFamily.Dispose();
            }
        }

        public static StiConditionBorderSides StrBordersToConditionBorderSidesObject(string borders)
        {
            int borderSides = 0;
            if (borders.IndexOf("All") >= 0 || borders.IndexOf("Top") >= 0) borderSides += 1;
            if (borders.IndexOf("All") >= 0 || borders.IndexOf("Left") >= 0) borderSides += 2;
            if (borders.IndexOf("All") >= 0 || borders.IndexOf("Right") >= 0) borderSides += 4;
            if (borders.IndexOf("All") >= 0 || borders.IndexOf("Bottom") >= 0) borderSides += 8;

            return (StiConditionBorderSides)borderSides;
        }

        public static StiConditionPermissions StrPermissionsToConditionPermissionsObject(string strPermissions)
        {
            int permissions = 0;
            if (strPermissions.IndexOf("All") >= 0 || strPermissions == ("Font") || strPermissions.IndexOf("Font,") == 0 ||
                strPermissions.IndexOf(", Font,") >= 0 || strPermissions.EndsWith("Font")) permissions += 1;
            if (strPermissions.IndexOf("All") >= 0 || strPermissions.IndexOf("FontSize") >= 0) permissions += 2;
            if (strPermissions.IndexOf("All") >= 0 || strPermissions.IndexOf("FontStyleBold") >= 0) permissions += 4;
            if (strPermissions.IndexOf("All") >= 0 || strPermissions.IndexOf("FontStyleItalic") >= 0) permissions += 8;
            if (strPermissions.IndexOf("All") >= 0 || strPermissions.IndexOf("FontStyleUnderline") >= 0) permissions += 16;
            if (strPermissions.IndexOf("All") >= 0 || strPermissions.IndexOf("FontStyleStrikeout") >= 0) permissions += 32;
            if (strPermissions.IndexOf("All") >= 0 || strPermissions.IndexOf("TextColor") >= 0) permissions += 64;
            if (strPermissions.IndexOf("All") >= 0 || strPermissions.IndexOf("BackColor") >= 0) permissions += 128;
            if (strPermissions.IndexOf("All") >= 0 || strPermissions.IndexOf("Borders") >= 0) permissions += 256;

            return (StiConditionPermissions)permissions;
        }

        public static string GetReportFileName(StiReport report)
        {
            string fileName = String.Empty;
            if (report.ReportFile != null && report.ReportFile.Length > 0)
            {
                string[] path = report.ReportFile.Replace("\\\\", "/").Replace("\\", "/").Split('/');
                fileName = path[path.Length - 1];
            }
            
            fileName = fileName.Replace("<", " ").Replace(">", " ").Replace("\\", "-").Replace("/", "-").Replace(":", "-");
            fileName = fileName.Replace("?", ".").Replace("\"", "'").Replace("|", " ").Replace("*", " ");

            return fileName;
        }

        public static StiComponent CreateInfographicComponent(string componentTypeArray)
        {
            string[] compAttributes = componentTypeArray.Split(';');
            string componentType = compAttributes[1];

            switch (componentType)
            {
                case "StiChart":
                    {
                        StiChart chart = new StiChart();
                        string seriesType = compAttributes[2];

                        Assembly assembly = typeof(StiReport).Assembly;
                        var series = assembly.CreateInstance("Stimulsoft.Report.Chart." + seriesType) as StiSeries;
                        chart.Series.Add(series);

                        return chart;
                    }
                case "StiGauge":
                    {
                        StiGauge gauge = new StiGauge();
                        //string seriesType = compAttributes[2];

                        //Assembly assembly = typeof(StiReport).Assembly;
                        //var series = assembly.CreateInstance("Stimulsoft.Report.Gauge." + seriesType) as StiSeries;
                        //gauge..Series.Add(series);
                        //gauge.

                        return gauge;
                    }
                case "StiMap":
                    {
                        StiMap map = new StiMap();
                        string mapType = compAttributes[2];
                        map.MapID = (StiMapID)Enum.Parse(typeof(StiMapID), compAttributes[2]);

                        return map;
                    }
            }

            return null;
        }
        
        private static StiComponent CreateShapeComponent(string componentTypeArray)
        {
            StiShape comp = new StiShape();
            string[] compAttributes = componentTypeArray.Split(';');
            string shapeType = compAttributes[1];
            SetShapeTypeProperty(comp, shapeType);

            return comp;
        }

        private static StiComponent CreateBarCodeComponent(string componentTypeArray)
        {
            StiBarCode comp = new StiBarCode();
            comp.BackColor = Color.Transparent;
            string[] compAttributes = componentTypeArray.Split(';');
            SetBarCodeTypeProperty(comp, compAttributes[1]);
            comp.Code.Value = comp.BarCodeType.DefaultCodeValue;

            return comp;
        }

        private static void ApplyStyleCollection(StiComponent comp, StiStylesCollection stylesCollection)
        {
            if (comp is StiPage) return;
            foreach (StiBaseStyle style in stylesCollection)
            {
                if (!StiStyleConditionHelper.IsAllowStyle(comp, style)) continue;

                if (comp is StiDataBand)
                {
                    bool isOddStyleDataBand = false;
                    bool isEvenStyleDataBand = false;
                    foreach (StiStyleCondition condition in style.Conditions)
                    {
                        if ((condition.Placement & StiStyleComponentPlacement.DataOddStyle) > 0)
                        {
                            isOddStyleDataBand = true;
                            break;
                        }

                        if ((condition.Placement & StiStyleComponentPlacement.DataEvenStyle) > 0)
                        {
                            isEvenStyleDataBand = true;
                            break;
                        }
                    }
                    if (isOddStyleDataBand) ((StiDataBand)comp).OddStyle = style.Name;
                    else if (isEvenStyleDataBand) ((StiDataBand)comp).EvenStyle = style.Name;
                    else comp.ComponentStyle = style.Name;
                }
                else comp.ComponentStyle = style.Name;
            }

            ApplyStyles(comp, stylesCollection);
        }

        public static void ApplyStyles(StiComponent comp, StiStylesCollection stylesCollection)
        {
            if (!string.IsNullOrEmpty(comp.ComponentStyle))
            {
                #region Apply styles to component
                StiBaseStyle style = stylesCollection[comp.ComponentStyle];
                if (style != null)
                {
                    style.SetStyleToComponent(comp);
                }
                #endregion
            }
        }

        public static Hashtable GetComponentMainProperties(StiComponent component, double zoom)
        {
            Hashtable mainProps = new Hashtable();
            mainProps["name"] = component.Name;
            mainProps["typeComponent"] = component.GetType().Name;
            mainProps["componentRect"] = GetComponentRect(component);
            mainProps["parentName"] = GetParentName(component);
            mainProps["parentIndex"] = GetParentIndex(component).ToString();
            mainProps["componentIndex"] = GetComponentIndex(component).ToString();
            mainProps["childs"] = GetAllChildComponents(component);
            mainProps["svgContent"] = GetSvgContent(component, zoom);
            mainProps["pageName"] = component.Page.Name;
            mainProps["properties"] = GetAllProperties(component);

            return mainProps;
        }

        public static ArrayList GetTableCells(StiTable table, double zoom)
        {
            ArrayList components = new ArrayList();
            foreach (StiComponent component in table.Components)
            {
                components.Add(GetComponentMainProperties(component, zoom));
            }

            return components;
        }

        public static StiDataColumn GetColumnFromColumnPath(string columnPath, StiReport report)
        {
            string[] pathArray = columnPath.Split('.');
            if (pathArray.Length > 1)
            {
                var dataSource = report.Dictionary.DataSources[pathArray[pathArray.Length - 2]];
                if (dataSource == null && report.Dictionary.Relations[pathArray[pathArray.Length - 2]] != null)
                    dataSource = report.Dictionary.Relations[pathArray[pathArray.Length - 2]].ParentSource;

                if (dataSource != null)
                {
                    var column = dataSource.Columns[pathArray[pathArray.Length - 1]];
                    if (column != null && (column.Type == typeof(Image) || column.Type == typeof(byte[]) || column.Type == typeof(string)))
                    {
                        return column;
                    }
                }
            }

            return null;
        }

        public static string GetBase64PngFromMetaFileBytes(byte[] imageBytes, int width, int height)
        {
            if (imageBytes == null) return null;

            try
            {
                return ImageToBase64(StiMetafileConverter.MetafileToPngBytes(imageBytes, width, height));
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return null;
            }
        }

        public static string GetBase64PngFromMetaFileBytes(byte[] imageBytes)
        {
            return GetBase64PngFromMetaFileBytes(imageBytes, 500, 500);
        }

        public static string GetImageContentForPaint(StiImage imageComp)
        {
            if (imageComp.TakeImage() == null)
            {
                try
                {
                    if (imageComp.ImageURL != null && !String.IsNullOrEmpty(imageComp.ImageURL.Value))
                    {
                        if (StiHyperlinkProcessor.IsResourceHyperlink(imageComp.ImageURL.Value))
                        {
                            var resource = imageComp.Report.Dictionary.Resources[StiHyperlinkProcessor.GetResourceNameFromHyperlink(imageComp.ImageURL.Value)];
                            if (resource != null)
                            {
                                if (Stimulsoft.Report.Helpers.StiImageHelper.IsMetafile(resource.Content))
                                    return GetBase64PngFromMetaFileBytes(resource.Content);
                                else
                                    return StiReportEdit.ImageToBase64(resource.Content);
                            }
                        }
                        if (StiHyperlinkProcessor.IsVariableHyperlink(imageComp.ImageURL.Value))
                        {
                            var variable = imageComp.Report.Dictionary.Variables[StiHyperlinkProcessor.GetVariableNameFromHyperlink(imageComp.ImageURL.Value)];
                            if (variable != null)
                            {
                                return StiReportEdit.ImageToBase64(variable.ValueObject as Image);
                            }
                        }
                        else
                        {
                            if (imageComp.ImageURL.Value.StartsWith("{")) return null;
                            return imageComp.ImageURL.Value;
                        }
                    }
                    else if (!String.IsNullOrEmpty(imageComp.DataColumn))
                    {
                        var column = GetColumnFromColumnPath(imageComp.DataColumn, imageComp.Report);
                        if (column != null)
                        {
                            imageComp.Report.Dictionary.Connect(true, new List<StiDataSource> { column.DataSource });
                            var image = StiGalleriesHelper.GetImageFromColumn(column, imageComp.Report);
                            if (image != null)
                            {
                                return StiReportEdit.ImageToBase64(image);
                            }
                        }
                    }
                    else if (imageComp.ImageData != null && !String.IsNullOrEmpty(imageComp.ImageData.Value))
                    {
                        string variableName = imageComp.ImageData.Value.Substring(1, imageComp.ImageData.Value.Length - 2);

                        var variable = imageComp.Report.Dictionary.Variables[variableName];
                        if (variable != null)
                        {
                            var image = variable.ValueObject as Image;
                            return image != null ? StiReportEdit.ImageToBase64(image) : null;
                        }
                    }
                }
                catch
                {
                    return null;
                }
            }
            else if (Stimulsoft.Report.Helpers.StiImageHelper.IsMetafile(imageComp.TakeImage()))
            {
                var imageRect = imageComp.GetPaintRectangle(true, false);
                return GetBase64PngFromMetaFileBytes(imageComp.TakeImage(), (int)imageRect.Width * 2, (int)imageRect.Height * 2);
            }

            return null;
        }

        public static string GetWatermarkImageContentForPaint(StiPage page, Hashtable pageProps)
        {
            if (page.Watermark != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(page.Watermark.ImageHyperlink))
                    {
                        if (StiHyperlinkProcessor.IsResourceHyperlink(page.Watermark.ImageHyperlink))
                        {
                            var resource = page.Report.Dictionary.Resources[StiHyperlinkProcessor.GetResourceNameFromHyperlink(page.Watermark.ImageHyperlink)];
                            if (resource != null)
                            {
                                using (var gdiImage = StiImageConverter.BytesToImage(resource.Content))
                                {
                                    pageProps["watermarkImageSize"] = String.Format("{0};{1}", gdiImage.Width, gdiImage.Height);

                                    if (Stimulsoft.Report.Helpers.StiImageHelper.IsMetafile(resource.Content))
                                        return GetBase64PngFromMetaFileBytes(resource.Content);
                                    else
                                        return StiReportEdit.ImageToBase64(resource.Content);
                                }
                            }
                        }
                        if (StiHyperlinkProcessor.IsVariableHyperlink(page.Watermark.ImageHyperlink))
                        {
                            var variable = page.Report.Dictionary.Variables[StiHyperlinkProcessor.GetVariableNameFromHyperlink(page.Watermark.ImageHyperlink)];
                            if (variable != null)
                            {
                                var image = variable.ValueObject as Image;
                                pageProps["watermarkImageSize"] = String.Format("{0};{1}", image.Width, image.Height);
                                return StiReportEdit.ImageToBase64(image);
                            }
                        }
                    }
                    else if (Stimulsoft.Report.Helpers.StiImageHelper.IsMetafile(page.Watermark.TakeImage()))
                    {
                        return GetBase64PngFromMetaFileBytes(page.Watermark.TakeImage());
                    }
                }
                catch
                {
                    return null;
                }
            }

            return null;
        }

        private static StiResourcesCollection CloneResources(StiReport report)
        {
            StiResourcesCollection resources = new StiResourcesCollection();

            foreach (StiResource resource in report.Dictionary.Resources)
            {
                resources.Add(resource);
            }

            return resources;
        }

        private static void LoadResourcesToReport(StiReport report, StiResourcesCollection resources)
        {
            report.Dictionary.Resources.Clear();

            foreach (StiResource resource in resources)
            {
                report.Dictionary.Resources.Add(resource);
            }
        }

        public static StiReport CloneReport(StiReport report, bool withResources)
        {
            if (report == null) return null;
            StiReport reportCopy;
            StiResourcesCollection tempResources = null;

            if (!withResources)
            {
                //Save resources to temp collection
                tempResources = CloneResources(report);

                //Clear resources before clone report
                report.Dictionary.Resources.Clear();
            }

            if (!StiOptions.Engine.FullTrust || report.CalculationMode == StiCalculationMode.Interpretation || report.NeedsCompiling)
            {
                reportCopy = new StiReport();
                MemoryStream stream = new MemoryStream();
                report.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);
                reportCopy.Load(stream);
                stream.Close();
            }
            else
            {
                reportCopy = StiActivator.CreateObject(report.GetType()) as StiReport;

                // Copy report variables
                foreach (StiVariable variable in report.Dictionary.Variables)
                {
                    reportCopy.Dictionary.Variables[variable.Name].DialogInfo.Keys = variable.DialogInfo.Keys.Clone() as string[];
                    reportCopy.Dictionary.Variables[variable.Name].DialogInfo.Values = variable.DialogInfo.Values.Clone() as string[];
                    FieldInfo field = report.GetType().GetField(variable.Name);
                    if (field != null) reportCopy[variable.Name] = field.GetValue(report);
                }
            }

            if (report.Variables != null && report.Variables.Count > 0)
            {
                foreach (string key in report.Variables.Keys)
                {
                    reportCopy[key] = report[key];
                }
            }

            reportCopy.RegData(report.DataStore);
            reportCopy.RegBusinessObject(report.BusinessObjectsStore);

            if (tempResources != null)
            {
                //Load resources from temp collection
                LoadResourcesToReport(report, tempResources);
            }

            if (report.GlobalizationManager != null)
            {
                reportCopy.GlobalizationManager = report.GlobalizationManager;
            }

            return reportCopy;
        }

        public static void CopyReportDictionary(StiReport reportFrom, StiReport reportTo)
        {
            reportTo.Dictionary.DataStore.Clear();
            reportTo.Dictionary.DataSources.Clear();
            reportTo.Dictionary.BusinessObjects.Clear();
            reportTo.Dictionary.Databases.Clear();
            reportTo.Dictionary.Relations.Clear();
            reportTo.Dictionary.Variables.Clear();
            reportTo.Dictionary.Resources.Clear();
            reportTo.Dictionary.Restrictions.Clear();

            if (StiOptions.Designer.NewReport.AllowRegisterDataStoreFromOldReportInNewReport)
                reportTo.Dictionary.DataStore.RegData(reportFrom.Dictionary.DataStore);

            if (StiOptions.Designer.NewReport.AllowRegisterDatabasesFromOldReportInNewReport)
                reportTo.Dictionary.Databases.AddRange(reportFrom.Dictionary.Databases);

            if (StiOptions.Designer.NewReport.AllowRegisterDataSourcesFromOldReportInNewReport)
                reportTo.Dictionary.DataSources.AddRange(reportFrom.Dictionary.DataSources);
            
            if (StiOptions.Designer.NewReport.AllowRegisterRelationsFromOldReportInNewReport)
                reportTo.Dictionary.Relations.AddRange(reportFrom.Dictionary.Relations);

            if (StiOptions.Designer.NewReport.AllowRegisterVariablesFromOldReportInNewReport)
                reportTo.Dictionary.Variables.AddRange(reportFrom.Dictionary.Variables);

            if (StiOptions.Designer.NewReport.AllowRegisterResourcesFromOldReportInNewReport)
                reportTo.Dictionary.Resources.AddRange(reportFrom.Dictionary.Resources);

            if (StiOptions.Designer.NewReport.AllowRegisterRestrictionsFromOldReportInNewReport)
                reportTo.Dictionary.Restrictions = reportFrom.Dictionary.Restrictions;

            reportTo.Dictionary.BusinessObjects.AddRange(reportFrom.Dictionary.BusinessObjects);
            reportTo.Dictionary.SynchronizeBusinessObjects();

            reportTo.Tag = reportFrom.Tag;
            reportTo.Dictionary.Synchronize();
        }

        public static void ClearUndoArray(StiRequestParams requestParams)
        {
            requestParams.Cache.Helper.SaveObjectInternal(new ArrayList(), requestParams, StiCacheHelper.GUID_UndoArray);
        }

        public static void AddReportToUndoArray(StiRequestParams requestParams, StiReport report)
        {
            AddReportToUndoArray(requestParams, report, false);
        }

        public static void AddReportToUndoArray(StiRequestParams requestParams, StiReport report, bool withResources)
        {
            if (report == null) return;
            
            ArrayList undoArray = requestParams.Cache.Helper.GetObjectInternal(requestParams, StiCacheHelper.GUID_UndoArray) as ArrayList;
            if (undoArray == null || undoArray.Count == 0)
            {
                undoArray = new ArrayList();
                undoArray.Add(1);
                undoArray.Add(null);
            }

            StiReport cloneReport = CloneReport(report, withResources);
            int currentPos = (int)undoArray[0];
            undoArray.Insert(currentPos, new StiReportContainer(cloneReport, withResources, requestParams.Designer.Command));
            currentPos++;
            undoArray[0] = currentPos;

            if (undoArray.Count > currentPos + 1)
            {
                undoArray.RemoveRange(currentPos, undoArray.Count - currentPos);
                undoArray.Add(null);
            }

            if (undoArray.Count > requestParams.Designer.UndoMaxLevel + 2)
            {
                undoArray.RemoveAt(1);
                if (currentPos != 1) undoArray[0] = currentPos - 1;
            }

            requestParams.Cache.Helper.SaveObjectInternal(undoArray, requestParams, StiCacheHelper.GUID_UndoArray);
        }

        private static void AddPrimitivePoints(StiComponent addedComp, StiPage currentPage)
        {
            var primitive = addedComp as StiCrossLinePrimitive;
            if (primitive != null)
            {
                var rect = primitive.ClientRectangle;

                var startPoint = new StiStartPointPrimitive
                {
                    Left = rect.Left,
                    Top = rect.Top,
                    Width = 0,
                    Height = 0
                };
                currentPage.Components.Add(startPoint);

                var endPoint = new StiEndPointPrimitive();
                if (primitive is StiRectanglePrimitive)
                    endPoint.Left = rect.Right;
                else
                    endPoint.Left = rect.Left;

                endPoint.Top = rect.Bottom;
                endPoint.Width = 0;
                endPoint.Height = 0;

                currentPage.Components.Add(endPoint);

                startPoint.ReferenceToGuid = primitive.Guid;
                endPoint.ReferenceToGuid = primitive.Guid;

                currentPage.Correct();
            }
        }

        private static void RemovePrimitivePoints(StiComponent removiedComp)
        {
            StiCrossLinePrimitive primitive = removiedComp as StiCrossLinePrimitive;
            if (primitive != null)
            {
                StiStartPointPrimitive startPoint = primitive.GetStartPoint();
                if (startPoint != null && startPoint.Parent != null && startPoint.Parent.Components.Contains(startPoint))
                {
                    startPoint.ReferenceToGuid = null;
                    startPoint.Parent.Components.Remove(startPoint);
                }
                StiEndPointPrimitive endPoint = primitive.GetEndPoint();
                if (endPoint != null && endPoint.Parent != null && endPoint.Parent.Components.Contains(endPoint))
                {
                    endPoint.ReferenceToGuid = null;
                    endPoint.Parent.Components.Remove(endPoint);
                }
            }
        }

        private static void ChangeRectPrimitivePoints(StiComponent changedComp, RectangleD rect)
        {
            StiCrossLinePrimitive primitive = changedComp as StiCrossLinePrimitive;
            if (primitive != null)
            {
                StiStartPointPrimitive startPoint = primitive.GetStartPoint();
                if (startPoint != null)
                {
                    startPoint.SetPaintRectangle(new RectangleD(new PointD(rect.Left, rect.Top), new SizeD(0, 0)));
                }
                StiEndPointPrimitive endPoint = primitive.GetEndPoint();
                if (endPoint != null)
                {
                    endPoint.SetPaintRectangle(new RectangleD(new PointD(primitive is StiRectanglePrimitive ? rect.Right : rect.Left, rect.Bottom), new SizeD(0, 0)));
                }
            }
        }

        private static void CheckAllPrimitivePoints(StiPage page)
        {
            Hashtable primitiveElements = new Hashtable();

            //Find primitive componets
            foreach (StiComponent comp in page.GetComponents())
            {
                if (comp is StiPointPrimitive || comp is StiCrossLinePrimitive)
                {
                    string guid = comp is StiPointPrimitive ? ((StiPointPrimitive)comp).ReferenceToGuid : ((StiCrossLinePrimitive)comp).Guid;
                    if (!String.IsNullOrEmpty(guid))
                    {
                        if (primitiveElements[guid] == null) primitiveElements[guid] = new ArrayList();
                        ((ArrayList)primitiveElements[guid]).Add(comp);
                    }
                }
            }

            //Remove primitive components, which lost any elements
            foreach (string key in primitiveElements.Keys)
            {
                ArrayList primitiveComponents = primitiveElements[key] as ArrayList;
                if (primitiveComponents.Count < 3) {
                    foreach (StiComponent comp in primitiveComponents) {
                        if (comp != null && comp.Parent != null && comp.Parent.Components.Contains(comp)) {
                            comp.Parent.Components.Remove(comp);
                        }
                    }
                }
            }
        }

        private static bool IsAlignedByGrid(StiComponent component)
        {
            decimal gridSize = (decimal)component.Report.Info.GridSize;

            return (
                (decimal)component.Left % gridSize == 0 &&
                (decimal)component.Top % gridSize == 0 &&
                (decimal)component.Width % gridSize == 0 &&
                (decimal)component.Height % gridSize == 0
            );
        }

        private static void AddSubReportPage(StiSubReport subReport, Hashtable callbackResult)
        {
            if (subReport != null)
            {   
                StiPage page = new StiPage(subReport.Report);
                page.Skip = true;
                subReport.Report.Pages.Add(page);
                subReport.SubReportPage = page;
                page.Name = StiNameCreation.CreateName(subReport.Report, StiNameCreation.GenerateName(subReport.Report, subReport.LocalizedName + '_', "subReport_"));

                Hashtable subReportPage = new Hashtable();
                subReportPage["name"] = page.Name;
                subReportPage["pageIndex"] = subReport.Report.Pages.IndexOf(page).ToString();
                subReportPage["properties"] = GetAllProperties(page);
                subReportPage["pageIndexes"] = GetPageIndexes(subReport.Report);
                callbackResult["newSubReportPage"] = subReportPage;
            }
        }
        #endregion

        #region Get All Properties
        public static Hashtable GetAllProperties(StiComponent component)
        {
            Hashtable allProps = new Hashtable();
            StiReport report = component.Report;

            //Common Properties For All Components
            allProps["aliasName"] = StiEncodingHelper.Encode(component.Alias);
            
            if (!(component is IStiElement))
            {
                allProps["interaction"] = GetInteractionProperty(component.Interaction);
                allProps["events"] = GetEventsProperty(component);
                allProps["componentStyle"] = !string.IsNullOrEmpty(component.ComponentStyle) ? component.ComponentStyle : "[None]";
            }
            if (!(component is StiPrimitive))
            {
                allProps["conditions"] = GetConditionsProperty(component);
            }
            if (component is IStiElement)
            {
                allProps["isDashboardElement"] = true;
            }
            if (!(component is StiPage))
            {
                allProps["restrictions"] = component.Restrictions.ToString();
                allProps["locked"] = component.Locked;
                allProps["linked"] = component.Linked;
                allProps["useParentStyles"] = component.UseParentStyles;
            }
            if (component != null && component is StiBand) allProps["headerSize"] = GetComponentHeaderSize(component);

            switch (component.GetType().Name)
            {
                #region StiPage
                case "StiPage":
                {
                        var page = component as StiPage;

                        allProps["border"] = BorderToStr(page.Border);
                        allProps["brush"] = BrushToStr(page.Brush);
                        allProps["orientation"] = page.Orientation.ToString();
                        allProps["unitWidth"] = DoubleToStr(page.PageWidth);
                        allProps["unitHeight"] = DoubleToStr(page.PageHeight);
                        allProps["unitMargins"] = GetPageMargins(page);
                        allProps["columns"] = page.Columns.ToString();
                        allProps["columnWidth"] = DoubleToStr(page.ColumnWidth);
                        allProps["columnGaps"] = DoubleToStr(page.ColumnGaps);
                        allProps["rightToLeft"] = page.RightToLeft;
                        allProps["paperSize"] = ((int)page.PaperSize).ToString();
                        allProps["waterMarkRatio"] = page.Watermark.AspectRatio;
                        allProps["waterMarkRightToLeft"] = page.Watermark.RightToLeft;
                        allProps["waterMarkEnabled"] = page.Watermark.Enabled;
                        allProps["waterMarkEnabledExpression"] = StiEncodingHelper.Encode(page.Watermark.EnabledExpression);
                        allProps["waterMarkAngle"] = DoubleToStr(page.Watermark.Angle);
                        allProps["waterMarkText"] = StiEncodingHelper.Encode(page.Watermark.Text);
                        allProps["waterMarkFont"] = FontToStr(page.Watermark.Font);
                        allProps["waterMarkTextBrush"] = BrushToStr(page.Watermark.TextBrush);
                        allProps["waterMarkTextBehind"] = page.Watermark.ShowBehind;
                        allProps["waterMarkImageBehind"] = page.Watermark.ShowImageBehind;
                        allProps["waterMarkImageAlign"] = page.Watermark.ImageAlignment.ToString();
                        allProps["waterMarkMultipleFactor"] = DoubleToStr(page.Watermark.ImageMultipleFactor);
                        allProps["waterMarkStretch"] = page.Watermark.ImageStretch;
                        allProps["waterMarkTiling"] = page.Watermark.ImageTiling;
                        allProps["waterMarkTransparency"] = page.Watermark.ImageTransparency.ToString();
                        allProps["watermarkImageSrc"] = page.Watermark.TakeImage() != null ? ImageToBase64(page.Watermark.TakeImage()) : String.Empty;
                        allProps["watermarkImageHyperlink"] = page.Watermark.ImageHyperlink;

                        if (page.Watermark.TakeGdiImage() != null)
                        {
                            using (var gdiImage = page.Watermark.TakeGdiImage())
                            {
                                allProps["watermarkImageSize"] = gdiImage.Width + ";" + gdiImage.Height;
                            }
                        }
                        else allProps["watermarkImageSize"] = "0;0";

                        allProps["watermarkImageContentForPaint"] = GetWatermarkImageContentForPaint(page, allProps);
                        allProps["stopBeforePrint"] = page.StopBeforePrint.ToString();
                        allProps["titleBeforeHeader"] = page.TitleBeforeHeader;
                        allProps["aliasName"] = StiEncodingHelper.Encode(page.Alias);
                        allProps["largeHeight"] = page.LargeHeight;
                        allProps["largeHeightFactor"] = page.LargeHeightFactor.ToString();
                        allProps["largeHeightAutoFactor"] = page.LargeHeightAutoFactor.ToString();
                        allProps["resetPageNumber"] = page.ResetPageNumber;
                        allProps["printOnPreviousPage"] = page.PrintOnPreviousPage;
                        allProps["printHeadersFootersFromPreviousPage"] = page.PrintHeadersFootersFromPreviousPage;
                        allProps["enabled"] = page.Enabled;
                        allProps["mirrorMargins"] = page.MirrorMargins;
                        allProps["segmentPerWidth"] = page.SegmentPerWidth.ToString();
                        allProps["segmentPerHeight"] = page.SegmentPerHeight.ToString();
                        allProps["unlimitedHeight"] = page.UnlimitedHeight;
                        allProps["unlimitedBreakable"] = page.UnlimitedBreakable;
                        allProps["numberOfCopies"] = page.NumberOfCopies.ToString();
                        break;
                }
                #endregion

                #region StiText
                case "StiText":
                    {
                        allProps["border"] = BorderToStr(((StiText)component).Border);
                        allProps["brush"] = BrushToStr(((StiText)component).Brush);
                        allProps["text"] = StiEncodingHelper.Encode(((StiText)component).Text.Value);
                        allProps["textBrush"] = BrushToStr(((StiText)component).TextBrush);
                        allProps["font"] = FontToStr(((StiText)component).Font);
                        allProps["wordWrap"] = ((StiText)component).WordWrap;
                        allProps["hideZeros"] = ((StiText)component).HideZeros;
                        allProps["maxNumberOfLines"] = ((StiText)component).MaxNumberOfLines.ToString();
                        allProps["onlyText"] = ((StiText)component).OnlyText;
                        allProps["textMargins"] = string.Format("{0};{1};{2};{3}", ((StiText)component).Margins.Left,
                            ((StiText)component).Margins.Right, ((StiText)component).Margins.Top, ((StiText)component).Margins.Bottom);
                        allProps["allowHtmlTags"] = ((StiText)component).AllowHtmlTags;
                        allProps["editableText"] = ((StiText)component).Editable;
                        allProps["textAngle"] = DoubleToStr(((StiText)component).Angle);
                        allProps["horAlignment"] = ((StiText)component).HorAlignment.ToString();
                        allProps["vertAlignment"] = ((StiText)component).VertAlignment.ToString();
                        allProps["textFormat"] = StiTextFormatHelper.GetTextFormatItem(((StiText)component).TextFormat);
                        allProps["canBreak"] = ((StiText)component).CanBreak;
                        allProps["growToHeight"] = ((StiText)component).GrowToHeight;
                        allProps["autoWidth"] = ((StiText)component).AutoWidth;
                        allProps["printOn"] = ((StiText)component).PrintOn.ToString();
                        allProps["printable"] = ((StiText)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["trimming"] = ((StiText)component).TextOptions.Trimming.ToString();
                        allProps["textOptionsRightToLeft"] = ((StiText)component).TextOptions.RightToLeft;
                        allProps["processAt"] = ((StiText)component).ProcessAt.ToString();
                        allProps["processingDuplicates"] = ((StiText)component).ProcessingDuplicates.ToString();
                        allProps["shrinkFontToFit"] = ((StiText)component).ShrinkFontToFit;
                        allProps["shrinkFontToFitMinimumSize"] = ((StiText)component).ShrinkFontToFitMinimumSize.ToString();
                        allProps["textType"] = ((StiText)component).Type.ToString();
                        allProps["enabled"] = ((StiText)component).Enabled;
                        allProps["canGrow"] = ((StiText)component).CanGrow;
                        allProps["canShrink"] = ((StiText)component).CanShrink;
                        allProps["editable"] = ((StiText)component).Editable;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiText)component).DockStyle.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        allProps["renderTo"] = ((StiText)component).RenderTo;
                        allProps["globalizedName"] = ((StiText)component).GlobalizedName;
                        allProps["excelValue"] = StiEncodingHelper.Encode(((StiText)component).ExcelValue.Value);
                        allProps["exportAsImage"] = ((StiText)component).ExportAsImage;
                        allProps["lineSpacing"] = DoubleToStr(((StiText)component).LineSpacing);
                        break;
                    }
                #endregion               

                #region StiTextInCells
                case "StiTextInCells":
                    {
                        allProps["border"] = BorderToStr(((StiTextInCells)component).Border);
                        allProps["brush"] = BrushToStr(((StiTextInCells)component).Brush);
                        allProps["text"] = StiEncodingHelper.Encode(((StiTextInCells)component).Text.Value);
                        allProps["textFormat"] = StiTextFormatHelper.GetTextFormatItem(((StiTextInCells)component).TextFormat);
                        allProps["textBrush"] = BrushToStr(((StiTextInCells)component).TextBrush);
                        allProps["font"] = FontToStr(((StiTextInCells)component).Font);
                        allProps["horAlignment"] = ((StiTextInCells)component).HorAlignment.ToString();
                        allProps["wordWrap"] = ((StiTextInCells)component).WordWrap;
                        allProps["textMargins"] = string.Format("{0};{1};{2};{3}", ((StiTextInCells)component).Margins.Left,
                            ((StiTextInCells)component).Margins.Right, ((StiTextInCells)component).Margins.Top, ((StiTextInCells)component).Margins.Bottom);
                        allProps["onlyText"] = ((StiTextInCells)component).OnlyText;
                        allProps["cellWidth"] = DoubleToStr(((StiTextInCells)component).CellWidth);
                        allProps["cellHeight"] = DoubleToStr(((StiTextInCells)component).CellHeight);
                        allProps["horizontalSpacing"] = DoubleToStr(((StiTextInCells)component).HorSpacing);
                        allProps["verticalSpacing"] = DoubleToStr(((StiTextInCells)component).VertSpacing);
                        allProps["rightToLeft"] = ((StiTextInCells)component).RightToLeft;
                        allProps["editableText"] = ((StiTextInCells)component).Editable;
                        allProps["canBreak"] = ((StiTextInCells)component).CanBreak;
                        allProps["growToHeight"] = ((StiTextInCells)component).GrowToHeight;
                        allProps["printOn"] = ((StiTextInCells)component).PrintOn.ToString();
                        allProps["printable"] = ((StiTextInCells)component).Printable;
                        allProps["hideZeros"] = ((StiTextInCells)component).HideZeros;
                        allProps["continuousText"] = ((StiTextInCells)component).ContinuousText;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["processAt"] = ((StiTextInCells)component).ProcessAt.ToString();
                        allProps["textType"] = ((StiTextInCells)component).Type.ToString();
                        allProps["enabled"] = ((StiTextInCells)component).Enabled;
                        allProps["canGrow"] = ((StiTextInCells)component).CanGrow;
                        allProps["canShrink"] = ((StiTextInCells)component).CanShrink;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiTextInCells)component).DockStyle.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        allProps["globalizedName"] = ((StiTextInCells)component).GlobalizedName;
                        allProps["excelValue"] = StiEncodingHelper.Encode(((StiTextInCells)component).ExcelValue.Value);
                        allProps["exportAsImage"] = ((StiTextInCells)component).ExportAsImage;
                        allProps["lineSpacing"] = DoubleToStr(((StiTextInCells)component).LineSpacing);
                        break;
                    }
                #endregion               

                #region StiRichText
                case "StiRichText":
                    {
                        allProps["border"] = BorderToStr(((StiRichText)component).Border);
                        allProps["richText"] = GetRichTextProperty((StiRichText)component);
                        allProps["richTextDataColumn"] = ((StiRichText)component).DataColumn != null ? StiEncodingHelper.Encode(((StiRichText)component).DataColumn) : string.Empty;
                        allProps["richTextUrl"] = ((StiRichText)component).DataUrl != null ? StiEncodingHelper.Encode(((StiRichText)component).DataUrl.Value) : string.Empty;
                        allProps["editableText"] = ((StiRichText)component).Editable;
                        allProps["textMargins"] = string.Format("{0};{1};{2};{3}", ((StiRichText)component).Margins.Left,
                            ((StiRichText)component).Margins.Right, ((StiRichText)component).Margins.Top, ((StiRichText)component).Margins.Bottom);
                        allProps["onlyText"] = ((StiRichText)component).OnlyText;
                        allProps["wordWrap"] = ((StiRichText)component).WordWrap;
                        allProps["canBreak"] = ((StiRichText)component).CanBreak;
                        allProps["growToHeight"] = ((StiRichText)component).GrowToHeight;
                        allProps["printOn"] = ((StiRichText)component).PrintOn.ToString();
                        allProps["printable"] = ((StiRichText)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["processAt"] = ((StiRichText)component).ProcessAt.ToString();
                        allProps["enabled"] = ((StiRichText)component).Enabled;
                        allProps["canGrow"] = ((StiRichText)component).CanGrow;
                        allProps["canShrink"] = ((StiRichText)component).CanShrink;
                        allProps["editable"] = ((StiRichText)component).Editable;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiRichText)component).DockStyle.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion               

                #region StiImage
                case "StiImage":
                    {
                        allProps["border"] = BorderToStr(((StiImage)component).Border);
                        allProps["brush"] = BrushToStr(((StiImage)component).Brush);
                        allProps["horAlignment"] = ((StiImage)component).HorAlignment.ToString();
                        allProps["vertAlignment"] = ((StiImage)component).VertAlignment.ToString();
                        allProps["stretch"] = ((StiImage)component).Stretch;
                        allProps["ratio"] = ((StiImage)component).AspectRatio;
                        allProps["rotation"] = ((StiImage)component).ImageRotation.ToString();
                        allProps["imageMultipleFactor"] = DoubleToStr(((StiImage)component).MultipleFactor);
                        allProps["canBreak"] = ((StiImage)component).CanBreak;
                        allProps["growToHeight"] = ((StiImage)component).GrowToHeight;
                        allProps["printOn"] = ((StiImage)component).PrintOn.ToString();
                        allProps["printable"] = ((StiImage)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["imageDataColumn"] = ((StiImage)component).DataColumn != null ? StiEncodingHelper.Encode(((StiImage)component).DataColumn) : string.Empty;
                        allProps["imageSrc"] = ((StiImage)component).TakeImage() != null ? ImageToBase64(((StiImage)component).TakeImage()) : string.Empty;
                        allProps["imageUrl"] = ((StiImage)component).ImageURL != null ? StiEncodingHelper.Encode(((StiImage)component).ImageURL.Value) : string.Empty;
                        allProps["imageFile"] = ((StiImage)component).File != null ? StiEncodingHelper.Encode(((StiImage)component).File) : string.Empty;
                        allProps["imageData"] = ((StiImage)component).ImageData != null ? StiEncodingHelper.Encode(((StiImage)component).ImageData.Value) : string.Empty;
                        allProps["imageContentForPaint"] = GetImageContentForPaint((StiImage)component);
                        allProps["enabled"] = ((StiImage)component).Enabled;
                        allProps["canGrow"] = ((StiImage)component).CanGrow;
                        allProps["canShrink"] = ((StiImage)component).CanShrink;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiImage)component).DockStyle.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        allProps["globalizedName"] = ((StiImage)component).GlobalizedName;
                        break;
                    }
                #endregion               

                #region StiBarCode
                case "StiBarCode":
                    {
                        allProps["border"] = BorderToStr(((StiBarCode)component).Border);
                        allProps["code"] = ((StiBarCode)component).Code != null ? StiEncodingHelper.Encode(((StiBarCode)component).Code.Value) : string.Empty;
                        allProps["codeType"] = ((StiBarCode)component).BarCodeType.GetType().Name;
                        allProps["horAlignment"] = ((StiBarCode)component).HorAlignment.ToString();
                        allProps["vertAlignment"] = ((StiBarCode)component).VertAlignment.ToString();
                        allProps["font"] = FontToStr(((StiBarCode)component).Font);
                        allProps["barCodeAngle"] = ((int)((StiBarCode)component).Angle).ToString();
                        allProps["autoScale"] = ((StiBarCode)component).AutoScale;
                        allProps["showLabelText"] = ((StiBarCode)component).ShowLabelText;
                        allProps["showQuietZones"] = ((StiBarCode)component).ShowQuietZones;
                        allProps["foreColor"] = GetStringFromColor(((StiBarCode)component).ForeColor);
                        allProps["backColor"] = GetStringFromColor(((StiBarCode)component).BackColor);
                        allProps["growToHeight"] = ((StiBarCode)component).GrowToHeight;
                        allProps["printOn"] = ((StiBarCode)component).PrintOn.ToString();
                        allProps["printable"] = ((StiBarCode)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["enabled"] = ((StiBarCode)component).Enabled;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiBarCode)component).DockStyle.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion               

                #region StiShape
                case "StiShape":
                    {
                        allProps["brush"] = BrushToStr(((StiShape)component).Brush);
                        allProps["shapeType"] = GetShapeTypeProperty((StiShape)component);
                        allProps["shapeBorderStyle"] = ((int)((StiShape)component).Style).ToString();
                        allProps["size"] = DoubleToStr(((StiShape)component).Size);
                        allProps["shapeBorderColor"] = GetStringFromColor(((StiShape)component).BorderColor);
                        allProps["growToHeight"] = ((StiShape)component).GrowToHeight;
                        allProps["printOn"] = ((StiShape)component).PrintOn.ToString();
                        allProps["printable"] = ((StiShape)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["enabled"] = ((StiShape)component).Enabled;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiShape)component).DockStyle.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion               

                #region StiPanel
                case "StiPanel":
                    {
                        allProps["brush"] = BrushToStr(((StiPanel)component).Brush);
                        allProps["border"] = BorderToStr(((StiPanel)component).Border);
                        allProps["columns"] = ((StiPanel)component).Columns.ToString();
                        allProps["columnWidth"] = DoubleToStr(((StiPanel)component).ColumnWidth);
                        allProps["columnGaps"] = DoubleToStr(((StiPanel)component).ColumnGaps);
                        allProps["rightToLeft"] = ((StiPanel)component).RightToLeft;
                        allProps["growToHeight"] = ((StiPanel)component).GrowToHeight;
                        allProps["printOn"] = ((StiPanel)component).PrintOn.ToString();
                        allProps["printable"] = ((StiPanel)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["enabled"] = ((StiPanel)component).Enabled;
                        allProps["canGrow"] = ((StiPanel)component).CanGrow;
                        allProps["canShrink"] = ((StiPanel)component).CanShrink;
                        allProps["canBreak"] = ((StiPanel)component).CanBreak;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiPanel)component).DockStyle.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion 

                #region StiClone
                case "StiClone":
                    {
                        allProps["brush"] = BrushToStr(((StiClone)component).Brush);
                        allProps["border"] = BorderToStr(((StiClone)component).Border);
                        allProps["container"] = (((StiClone)component).Container != null) ? ((StiClone)component).Container.Name : "[Not Assigned]";
                        allProps["printOn"] = ((StiClone)component).PrintOn.ToString();
                        allProps["printable"] = ((StiClone)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["enabled"] = ((StiClone)component).Enabled;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiClone)component).DockStyle.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion 

                #region StiCheckBox
                case "StiCheckBox":
                    {
                        allProps["brush"] = BrushToStr(((StiCheckBox)component).Brush);
                        allProps["border"] = BorderToStr(((StiCheckBox)component).Border);
                        allProps["checked"] = StiEncodingHelper.Encode(((StiCheckBox)component).Checked.Value);
                        allProps["checkStyleForTrue"] = ((StiCheckBox)component).CheckStyleForTrue.ToString();
                        allProps["checkStyleForFalse"] = ((StiCheckBox)component).CheckStyleForFalse.ToString();
                        allProps["checkValues"] = ((StiCheckBox)component).Values;
                        allProps["editable"] = ((StiCheckBox)component).Editable;
                        allProps["textBrush"] = BrushToStr(((StiCheckBox)component).TextBrush);
                        allProps["size"] = DoubleToStr(((StiCheckBox)component).Size);
                        allProps["contourColor"] = GetStringFromColor(((StiCheckBox)component).ContourColor);
                        allProps["growToHeight"] = ((StiCheckBox)component).GrowToHeight;
                        allProps["printOn"] = ((StiCheckBox)component).PrintOn.ToString();
                        allProps["printable"] = ((StiCheckBox)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["enabled"] = ((StiCheckBox)component).Enabled;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiCheckBox)component).DockStyle.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        allProps["excelValue"] = StiEncodingHelper.Encode(((StiCheckBox)component).ExcelValue.Value);
                        break;
                    }
                #endregion 

                #region StiSubReport
                case "StiSubReport":
                    {
                        allProps["brush"] = BrushToStr(((StiSubReport)component).Brush);
                        allProps["border"] = BorderToStr(((StiSubReport)component).Border);
                        allProps["subReportPage"] = (((StiSubReport)component).SubReportPage != null) ? ((StiSubReport)component).SubReportPage.Name : "[Not Assigned]";
                        allProps["subReportUrl"] = ((StiSubReport)component).SubReportUrl != null ? StiEncodingHelper.Encode(((StiSubReport)component).SubReportUrl) : string.Empty;
                        allProps["printOn"] = ((StiSubReport)component).PrintOn.ToString();
                        allProps["printable"] = ((StiSubReport)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["enabled"] = ((StiSubReport)component).Enabled;
                        allProps["canGrow"] = ((StiSubReport)component).CanGrow;
                        allProps["canShrink"] = ((StiSubReport)component).CanShrink;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiSubReport)component).DockStyle.ToString();
                        allProps["subReportParameters"] = GetSubReportParametersProperty((StiSubReport)component);
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion 

                #region StiZipCode
                case "StiZipCode":
                    {
                        allProps["brush"] = BrushToStr(((StiZipCode)component).Brush);
                        allProps["border"] = BorderToStr(((StiZipCode)component).Border);
                        allProps["code"] = ((StiZipCode)component).Code != null ? StiEncodingHelper.Encode(((StiZipCode)component).Code.Value) : string.Empty;
                        allProps["size"] = DoubleToStr(((StiZipCode)component).Size);
                        allProps["foreColor"] = GetStringFromColor(((StiZipCode)component).ForeColor);
                        allProps["ratio"] = ((StiZipCode)component).Ratio;
                        allProps["growToHeight"] = ((StiZipCode)component).GrowToHeight;
                        allProps["printOn"] = ((StiZipCode)component).PrintOn.ToString();
                        allProps["printable"] = ((StiZipCode)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["enabled"] = ((StiZipCode)component).Enabled;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiZipCode)component).DockStyle.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion 

                #region StiChart
                case "StiChart":
                    {
                        allProps["brush"] = BrushToStr(((StiChart)component).Brush);
                        allProps["border"] = BorderToStr(((StiChart)component).Border);
                        allProps["growToHeight"] = ((StiChart)component).GrowToHeight;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["printOn"] = ((StiChart)component).PrintOn.ToString();
                        allProps["printable"] = ((StiChart)component).Printable;
                        allProps["sortData"] = GetSortDataProperty(component);
                        allProps["filterData"] = StiEncodingHelper.Encode(JSON.Encode(GetFiltersObject(((StiChart)component).Filters)));
                        allProps["filterMode"] = ((StiChart)component).FilterMode.ToString();
                        allProps["filterOn"] = ((StiChart)component).FilterOn;
                        allProps["dataSource"] = (((StiChart)component).DataSource != null) ? ((StiChart)component).DataSource.Name : "[Not Assigned]";
                        allProps["dataRelation"] = (((StiChart)component).DataRelation != null) ? ((StiChart)component).DataRelation.NameInSource : "[Not Assigned]";
                        allProps["masterComponent"] = (((StiChart)component).MasterComponent != null) ? ((StiChart)component).MasterComponent.Name : "[Not Assigned]";
                        allProps["countData"] = ((StiChart)component).CountData.ToString();
                        allProps["businessObject"] = ((StiChart)component).BusinessObject != null ? ((StiChart)component).BusinessObject.GetFullName() : "[Not Assigned]";
                        allProps["enabled"] = ((StiChart)component).Enabled;
                        allProps["chartStyle"] = StiChartHelper.GetStyle((StiChart)component);
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiChart)component).DockStyle.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion 

                #region StiGauge
                case "StiGauge":
                    {
                        allProps["brush"] = BrushToStr(((StiGauge)component).Brush);
                        allProps["border"] = BorderToStr(((StiGauge)component).Border);
                        allProps["growToHeight"] = ((StiGauge)component).GrowToHeight;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["printOn"] = ((StiGauge)component).PrintOn.ToString();
                        allProps["printable"] = ((StiGauge)component).Printable;
                        allProps["enabled"] = ((StiGauge)component).Enabled;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiGauge)component).DockStyle.ToString();
                        allProps["gaugeStyle"] = StiGaugeHelper.GetStyle((StiGauge)component);
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion 

                #region StiMap
                case "StiMap":
                    {
                        allProps["brush"] = BrushToStr(((StiMap)component).Brush);
                        allProps["border"] = BorderToStr(((StiMap)component).Border);
                        allProps["growToHeight"] = ((StiMap)component).GrowToHeight;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["printOn"] = ((StiMap)component).PrintOn.ToString();
                        allProps["printable"] = ((StiMap)component).Printable;
                        allProps["enabled"] = ((StiMap)component).Enabled;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["dockStyle"] = ((StiMap)component).DockStyle.ToString();
                        allProps["mapStyle"] = StiMapHelper.GetStyle((StiMap)component);
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion 

                #region StiTableCell
                case "StiTableCell":
                    {
                        allProps["cellType"] = ((StiTableCell)component).CellType.ToString();
                        allProps["cellDockStyle"] = ((StiTableCell)component).CellDockStyle.ToString();
                        allProps["fixedWidth"] = ((StiTableCell)component).FixedWidth;
                        allProps["border"] = BorderToStr(((StiTableCell)component).Border);
                        allProps["brush"] = BrushToStr(((StiTableCell)component).Brush);
                        allProps["text"] = StiEncodingHelper.Encode(((StiTableCell)component).Text.Value);
                        allProps["textBrush"] = BrushToStr(((StiTableCell)component).TextBrush);
                        allProps["font"] = FontToStr(((StiTableCell)component).Font);
                        allProps["wordWrap"] = ((StiTableCell)component).WordWrap;
                        allProps["hideZeros"] = ((StiTableCell)component).HideZeros;
                        allProps["maxNumberOfLines"] = ((StiTableCell)component).MaxNumberOfLines.ToString();
                        allProps["onlyText"] = ((StiTableCell)component).OnlyText;
                        allProps["textMargins"] = string.Format("{0};{1};{2};{3}", ((StiTableCell)component).Margins.Left,
                            ((StiTableCell)component).Margins.Right, ((StiTableCell)component).Margins.Top, ((StiTableCell)component).Margins.Bottom);
                        allProps["allowHtmlTags"] = ((StiTableCell)component).AllowHtmlTags;
                        allProps["editableText"] = ((StiTableCell)component).Editable;
                        allProps["textAngle"] = DoubleToStr(((StiTableCell)component).Angle);
                        allProps["horAlignment"] = ((StiTableCell)component).HorAlignment.ToString();
                        allProps["vertAlignment"] = ((StiTableCell)component).VertAlignment.ToString();
                        allProps["textFormat"] = StiTextFormatHelper.GetTextFormatItem(((StiTableCell)component).TextFormat);
                        allProps["canBreak"] = ((StiTableCell)component).CanBreak;
                        allProps["printOn"] = ((StiTableCell)component).PrintOn.ToString();
                        allProps["printable"] = ((StiTableCell)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["trimming"] = ((StiTableCell)component).TextOptions.Trimming.ToString();
                        allProps["textOptionsRightToLeft"] = ((StiTableCell)component).TextOptions.RightToLeft;
                        allProps["processAt"] = ((StiTableCell)component).ProcessAt.ToString();
                        allProps["processingDuplicates"] = ((StiTableCell)component).ProcessingDuplicates.ToString();
                        allProps["shrinkFontToFit"] = ((StiTableCell)component).ShrinkFontToFit;
                        allProps["shrinkFontToFitMinimumSize"] = ((StiTableCell)component).ShrinkFontToFitMinimumSize.ToString();
                        allProps["textType"] = ((StiTableCell)component).Type.ToString();
                        allProps["enabled"] = ((StiTableCell)component).Enabled;
                        allProps["canGrow"] = ((StiTableCell)component).CanGrow;
                        allProps["canShrink"] = ((StiTableCell)component).CanShrink;
                        allProps["editable"] = ((StiTableCell)component).Editable;
                        allProps["excelValue"] = StiEncodingHelper.Encode(((StiTableCell)component).ExcelValue.Value);
                        allProps["exportAsImage"] = ((StiTableCell)component).ExportAsImage;
                        allProps["lineSpacing"] = DoubleToStr(((StiTableCell)component).LineSpacing);
                        break;
                    }
                #endregion

                #region StiTableCellImage
                case "StiTableCellImage":
                    {
                        allProps["cellType"] = ((StiTableCellImage)component).CellType.ToString();
                        allProps["cellDockStyle"] = ((StiTableCellImage)component).CellDockStyle.ToString();
                        allProps["fixedWidth"] = ((StiTableCellImage)component).FixedWidth;
                        allProps["border"] = BorderToStr(((StiTableCellImage)component).Border);
                        allProps["brush"] = BrushToStr(((StiTableCellImage)component).Brush);
                        allProps["horAlignment"] = ((StiTableCellImage)component).HorAlignment.ToString();
                        allProps["vertAlignment"] = ((StiTableCellImage)component).VertAlignment.ToString();
                        allProps["stretch"] = ((StiTableCellImage)component).Stretch;
                        allProps["ratio"] = ((StiTableCellImage)component).AspectRatio;
                        allProps["rotation"] = ((StiTableCellImage)component).ImageRotation.ToString();
                        allProps["imageMultipleFactor"] = DoubleToStr(((StiTableCellImage)component).MultipleFactor);
                        allProps["canBreak"] = ((StiTableCellImage)component).CanBreak;
                        allProps["printOn"] = ((StiTableCellImage)component).PrintOn.ToString();
                        allProps["printable"] = ((StiTableCellImage)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["imageDataColumn"] = ((StiTableCellImage)component).DataColumn != null ? StiEncodingHelper.Encode(((StiTableCellImage)component).DataColumn) : string.Empty;
                        allProps["imageSrc"] = ((StiTableCellImage)component).TakeImage() != null ? ImageToBase64(((StiTableCellImage)component).TakeImage()) : string.Empty;
                        allProps["imageUrl"] = ((StiTableCellImage)component).ImageURL != null ? StiEncodingHelper.Encode(((StiTableCellImage)component).ImageURL.Value) : string.Empty;
                        allProps["imageFile"] = ((StiTableCellImage)component).File != null ? StiEncodingHelper.Encode(((StiTableCellImage)component).File) : string.Empty;
                        allProps["imageData"] = ((StiTableCellImage)component).ImageData != null ? StiEncodingHelper.Encode(((StiTableCellImage)component).ImageData.Value) : string.Empty;
                        allProps["imageContentForPaint"] = GetImageContentForPaint((StiImage)component);
                        allProps["enabled"] = ((StiTableCellImage)component).Enabled;
                        allProps["canGrow"] = ((StiTableCellImage)component).CanGrow;
                        allProps["canShrink"] = ((StiTableCellImage)component).CanShrink;
                        break;
                    }
                #endregion

                #region StiTableCellCheckBox
                case "StiTableCellCheckBox":
                    {
                        allProps["cellType"] = ((StiTableCellCheckBox)component).CellType.ToString();
                        allProps["cellDockStyle"] = ((StiTableCellCheckBox)component).CellDockStyle.ToString();
                        allProps["fixedWidth"] = ((StiTableCellCheckBox)component).FixedWidth;
                        allProps["brush"] = BrushToStr(((StiTableCellCheckBox)component).Brush);
                        allProps["border"] = BorderToStr(((StiTableCellCheckBox)component).Border);
                        allProps["checked"] = StiEncodingHelper.Encode(((StiTableCellCheckBox)component).Checked.Value);
                        allProps["checkStyleForTrue"] = ((StiTableCellCheckBox)component).CheckStyleForTrue.ToString();
                        allProps["checkStyleForFalse"] = ((StiTableCellCheckBox)component).CheckStyleForFalse.ToString();
                        allProps["checkValues"] = ((StiTableCellCheckBox)component).Values;
                        allProps["editable"] = ((StiTableCellCheckBox)component).Editable;
                        allProps["textBrush"] = BrushToStr(((StiTableCellCheckBox)component).TextBrush);
                        allProps["size"] = DoubleToStr(((StiTableCellCheckBox)component).Size);
                        allProps["contourColor"] = GetStringFromColor(((StiTableCellCheckBox)component).ContourColor);
                        allProps["printOn"] = ((StiTableCellCheckBox)component).PrintOn.ToString();
                        allProps["printable"] = ((StiTableCellCheckBox)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["enabled"] = ((StiTableCellCheckBox)component).Enabled;
                        allProps["excelValue"] = StiEncodingHelper.Encode(((StiTableCellCheckBox)component).ExcelValue.Value);
                        break;
                    }
                #endregion

                #region StiTableCellRichText
                case "StiTableCellRichText":
                    {
                        allProps["cellType"] = ((StiTableCellRichText)component).CellType.ToString();
                        allProps["cellDockStyle"] = ((StiTableCellRichText)component).CellDockStyle.ToString();
                        allProps["fixedWidth"] = ((StiTableCellRichText)component).FixedWidth;
                        allProps["border"] = BorderToStr(((StiTableCellRichText)component).Border);
                        allProps["richText"] = GetRichTextProperty((StiTableCellRichText)component);
                        allProps["richTextDataColumn"] = ((StiTableCellRichText)component).DataColumn != null ? StiEncodingHelper.Encode(((StiTableCellRichText)component).DataColumn) : string.Empty;
                        allProps["richTextUrl"] = ((StiTableCellRichText)component).DataUrl != null ? StiEncodingHelper.Encode(((StiTableCellRichText)component).DataUrl.Value) : string.Empty;
                        allProps["editableText"] = ((StiTableCellRichText)component).Editable;
                        allProps["textMargins"] = string.Format("{0};{1};{2};{3}", ((StiTableCellRichText)component).Margins.Left,
                            ((StiTableCellRichText)component).Margins.Right, ((StiTableCellRichText)component).Margins.Top, ((StiTableCellRichText)component).Margins.Bottom);
                        allProps["onlyText"] = ((StiTableCellRichText)component).OnlyText;
                        allProps["wordWrap"] = ((StiTableCellRichText)component).WordWrap;
                        allProps["canBreak"] = ((StiTableCellRichText)component).CanBreak;
                        allProps["printOn"] = ((StiTableCellRichText)component).PrintOn.ToString();
                        allProps["printable"] = ((StiTableCellRichText)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["processAt"] = ((StiTableCellRichText)component).ProcessAt.ToString();
                        allProps["enabled"] = ((StiTableCellRichText)component).Enabled;
                        allProps["canGrow"] = ((StiTableCellRichText)component).CanGrow;
                        allProps["canShrink"] = ((StiTableCellRichText)component).CanShrink;
                        allProps["editable"] = ((StiTableCellRichText)component).Editable;
                        break;
                    }
                #endregion


                #region StiHorizontalLinePrimitive
                case "StiHorizontalLinePrimitive":
                    {
                        allProps["color"] = GetStringFromColor(((StiHorizontalLinePrimitive)component).Color);
                        allProps["size"] = DoubleToStr(((StiHorizontalLinePrimitive)component).Size);
                        allProps["style"] = ((int)((StiHorizontalLinePrimitive)component).Style).ToString();
                        //allProps["startCapColor"] = GetStringFromColor(((StiHorizontalLinePrimitive)component).StartCap.Color);
                        //allProps["startCapFill"] = ((StiHorizontalLinePrimitive)component).StartCap.Fill;
                        //allProps["startCapWidth"] = ((StiHorizontalLinePrimitive)component).StartCap.Width.ToString();
                        //allProps["startCapHeight"] = ((StiHorizontalLinePrimitive)component).StartCap.Height.ToString();
                        //allProps["startCapStyle"] = ((StiHorizontalLinePrimitive)component).StartCap.Style.ToString();
                        //allProps["endCapColor"] = GetStringFromColor(((StiHorizontalLinePrimitive)component).StartCap.Color);
                        //allProps["endCapFill"] = ((StiHorizontalLinePrimitive)component).StartCap.Fill;
                        //allProps["endCapWidth"] = ((StiHorizontalLinePrimitive)component).StartCap.Width.ToString();
                        //allProps["endCapHeight"] = ((StiHorizontalLinePrimitive)component).StartCap.Height.ToString();
                        //allProps["endCapStyle"] = ((StiHorizontalLinePrimitive)component).StartCap.Style.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["enabled"] = component.Enabled;
                        allProps["printOn"] = component.PrintOn.ToString();
                        allProps["printable"] = component.Printable;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["isPrimitiveComponent"] = true;
                        break;
                    }
                #endregion

                #region StiVerticalLinePrimitive
                case "StiVerticalLinePrimitive":
                    {
                        allProps["color"] = GetStringFromColor(((StiVerticalLinePrimitive)component).Color);
                        allProps["size"] = DoubleToStr(((StiVerticalLinePrimitive)component).Size);
                        allProps["style"] = ((int)((StiVerticalLinePrimitive)component).Style).ToString();
                        //allProps["startCapColor"] = GetStringFromColor(((StiVerticalLinePrimitive)component).StartCap.Color);
                        //allProps["startCapFill"] = ((StiVerticalLinePrimitive)component).StartCap.Fill;
                        //allProps["startCapWidth"] = ((StiVerticalLinePrimitive)component).StartCap.Width.ToString();
                        //allProps["startCapHeight"] = ((StiVerticalLinePrimitive)component).StartCap.Height.ToString();
                        //allProps["startCapStyle"] = ((StiVerticalLinePrimitive)component).StartCap.Style.ToString();
                        //allProps["endCapColor"] = GetStringFromColor(((StiVerticalLinePrimitive)component).StartCap.Color);
                        //allProps["endCapFill"] = ((StiVerticalLinePrimitive)component).StartCap.Fill;
                        //allProps["endCapWidth"] = ((StiVerticalLinePrimitive)component).StartCap.Width.ToString();
                        //allProps["endCapHeight"] = ((StiVerticalLinePrimitive)component).StartCap.Height.ToString();
                        //allProps["endCapStyle"] = ((StiVerticalLinePrimitive)component).StartCap.Style.ToString();
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["enabled"] = component.Enabled;
                        allProps["printOn"] = component.PrintOn.ToString();
                        allProps["printable"] = component.Printable;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["isPrimitiveComponent"] = true;
                        break;
                    }
                #endregion

                #region StiRectanglePrimitive
                case "StiRectanglePrimitive":
                    {
                        allProps["color"] = GetStringFromColor(((StiRectanglePrimitive)component).Color);
                        allProps["size"] = DoubleToStr(((StiRectanglePrimitive)component).Size);
                        allProps["style"] = ((int)((StiRectanglePrimitive)component).Style).ToString();
                        allProps["leftSide"] = ((StiRectanglePrimitive)component).LeftSide;
                        allProps["rightSide"] = ((StiRectanglePrimitive)component).RightSide;
                        allProps["topSide"] = ((StiRectanglePrimitive)component).TopSide;
                        allProps["bottomSide"] = ((StiRectanglePrimitive)component).BottomSide;
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["enabled"] = component.Enabled;
                        allProps["printOn"] = component.PrintOn.ToString();
                        allProps["printable"] = component.Printable;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["isPrimitiveComponent"] = true;
                        break;
                    }
                #endregion

                #region StiRoundedRectanglePrimitive
                case "StiRoundedRectanglePrimitive":
                    {
                        allProps["color"] = GetStringFromColor(((StiRoundedRectanglePrimitive)component).Color);
                        allProps["size"] = DoubleToStr(((StiRoundedRectanglePrimitive)component).Size);
                        allProps["style"] = ((int)((StiRoundedRectanglePrimitive)component).Style).ToString();
                        allProps["round"] = DoubleToStr(((StiRoundedRectanglePrimitive)component).Round);
                        //allProps["leftSide"] = ((StiRoundedRectanglePrimitive)component).LeftSide;
                        //allProps["rightSide"] = ((StiRoundedRectanglePrimitive)component).RightSide;
                        //allProps["topSide"] = ((StiRoundedRectanglePrimitive)component).TopSide;
                        //allProps["bottomSide"] = ((StiRoundedRectanglePrimitive)component).BottomSide;
                        allProps["anchor"] = component.Anchor.ToString();
                        allProps["enabled"] = ((StiRoundedRectanglePrimitive)component).Enabled;
                        allProps["printOn"] = ((StiRoundedRectanglePrimitive)component).PrintOn.ToString();
                        allProps["printable"] = ((StiRoundedRectanglePrimitive)component).Printable;
                        allProps["shiftMode"] = component.ShiftMode.ToString();
                        allProps["isPrimitiveComponent"] = true;
                        break;
                    }
                #endregion 


                #region StiPageHeaderBand
                case "StiPageHeaderBand":
                    {
                        allProps["brush"] = BrushToStr(((StiPageHeaderBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiPageHeaderBand)component).Border);
                        allProps["printOn"] = ((StiPageHeaderBand)component).PrintOn.ToString();
                        allProps["resetPageNumber"] = ((StiPageHeaderBand)component).ResetPageNumber;
                        allProps["enabled"] = ((StiPageHeaderBand)component).Enabled;
                        allProps["canGrow"] = ((StiPageHeaderBand)component).CanGrow;
                        allProps["canShrink"] = ((StiPageHeaderBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiPageHeaderBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiPageHeaderBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiPageFooterBand
                case "StiPageFooterBand":
                    {
                        allProps["brush"] = BrushToStr(((StiPageFooterBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiPageFooterBand)component).Border);
                        allProps["printOn"] = ((StiPageFooterBand)component).PrintOn.ToString();
                        allProps["resetPageNumber"] = ((StiPageFooterBand)component).ResetPageNumber;
                        allProps["enabled"] = ((StiPageFooterBand)component).Enabled;
                        allProps["canGrow"] = ((StiPageFooterBand)component).CanGrow;
                        allProps["canShrink"] = ((StiPageFooterBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiPageFooterBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiPageFooterBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiReportTitleBand
                case "StiReportTitleBand":
                    {
                        allProps["brush"] = BrushToStr(((StiReportTitleBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiReportTitleBand)component).Border);
                        allProps["printIfEmpty"] = ((StiReportTitleBand)component).PrintIfEmpty;
                        allProps["resetPageNumber"] = ((StiReportTitleBand)component).ResetPageNumber;
                        allProps["enabled"] = ((StiReportTitleBand)component).Enabled;
                        allProps["canGrow"] = ((StiReportTitleBand)component).CanGrow;
                        allProps["canShrink"] = ((StiReportTitleBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiReportTitleBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiReportTitleBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiGroupHeaderBand
                case "StiGroupHeaderBand":
                    {
                        allProps["brush"] = BrushToStr(((StiGroupHeaderBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiGroupHeaderBand)component).Border);
                        allProps["canBreak"] = ((StiGroupHeaderBand)component).CanBreak;
                        allProps["printOn"] = ((StiGroupHeaderBand)component).PrintOn.ToString();
                        allProps["newPageBefore"] = ((StiGroupHeaderBand)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiGroupHeaderBand)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiGroupHeaderBand)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiGroupHeaderBand)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiGroupHeaderBand)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiGroupHeaderBand)component).SkipFirst;
                        allProps["condition"] = (((StiGroupHeaderBand)component).Condition != null) ? StiEncodingHelper.Encode(((StiGroupHeaderBand)component).Condition.Value) : string.Empty;
                        allProps["sortDirection"] = ((int)((StiGroupHeaderBand)component).SortDirection).ToString();
                        allProps["summarySortDirection"] = ((int)((StiGroupHeaderBand)component).SummarySortDirection).ToString();
                        allProps["summaryExpression"] = StiEncodingHelper.Encode(((StiGroupHeaderBand)component).SummaryExpression.ToString());
                        allProps["summaryType"] = ((int)((StiGroupHeaderBand)component).SummaryType).ToString();
                        allProps["printOnAllPages"] = ((StiGroupHeaderBand)component).PrintOnAllPages;
                        allProps["resetPageNumber"] = ((StiGroupHeaderBand)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiGroupHeaderBand)component).PrintAtBottom;
                        allProps["keepGroupTogether"] = ((StiGroupHeaderBand)component).KeepGroupTogether;
                        allProps["keepGroupHeaderTogether"] = ((StiGroupHeaderBand)component).KeepGroupHeaderTogether;
                        allProps["enabled"] = ((StiGroupHeaderBand)component).Enabled;
                        allProps["canGrow"] = ((StiGroupHeaderBand)component).CanGrow;
                        allProps["canShrink"] = ((StiGroupHeaderBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiGroupHeaderBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiGroupHeaderBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiGroupFooterBand
                case "StiGroupFooterBand":
                    {
                        allProps["brush"] = BrushToStr(((StiGroupFooterBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiGroupFooterBand)component).Border);
                        allProps["canBreak"] = ((StiGroupFooterBand)component).CanBreak;
                        allProps["printOn"] = ((StiGroupFooterBand)component).PrintOn.ToString();
                        allProps["newPageBefore"] = ((StiGroupFooterBand)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiGroupFooterBand)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiGroupFooterBand)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiGroupFooterBand)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiGroupFooterBand)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiGroupFooterBand)component).SkipFirst;
                        allProps["resetPageNumber"] = ((StiGroupFooterBand)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiGroupFooterBand)component).PrintAtBottom;
                        allProps["enabled"] = ((StiGroupFooterBand)component).Enabled;
                        allProps["canGrow"] = ((StiGroupFooterBand)component).CanGrow;
                        allProps["canShrink"] = ((StiGroupFooterBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiGroupFooterBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiGroupFooterBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiHeaderBand
                case "StiHeaderBand":
                    {
                        allProps["brush"] = BrushToStr(((StiHeaderBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiHeaderBand)component).Border);
                        allProps["canBreak"] = ((StiHeaderBand)component).CanBreak;
                        allProps["printOn"] = ((StiHeaderBand)component).PrintOn.ToString();
                        allProps["newPageBefore"] = ((StiHeaderBand)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiHeaderBand)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiHeaderBand)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiHeaderBand)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiHeaderBand)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiHeaderBand)component).SkipFirst;
                        allProps["printIfEmpty"] = ((StiHeaderBand)component).PrintIfEmpty;
                        allProps["printOnAllPages"] = ((StiHeaderBand)component).PrintOnAllPages;
                        allProps["resetPageNumber"] = ((StiHeaderBand)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiHeaderBand)component).PrintAtBottom;
                        allProps["enabled"] = ((StiHeaderBand)component).Enabled;
                        allProps["canGrow"] = ((StiHeaderBand)component).CanGrow;
                        allProps["canShrink"] = ((StiHeaderBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiHeaderBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiHeaderBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiFooterBand
                case "StiFooterBand":
                    {
                        allProps["brush"] = BrushToStr(((StiFooterBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiFooterBand)component).Border);
                        allProps["canBreak"] = ((StiFooterBand)component).CanBreak;
                        allProps["printOn"] = ((StiFooterBand)component).PrintOn.ToString();
                        allProps["newPageBefore"] = ((StiFooterBand)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiFooterBand)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiFooterBand)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiFooterBand)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiFooterBand)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiFooterBand)component).SkipFirst;
                        allProps["printIfEmpty"] = ((StiFooterBand)component).PrintIfEmpty;
                        allProps["printOnAllPages"] = ((StiFooterBand)component).PrintOnAllPages;
                        allProps["resetPageNumber"] = ((StiFooterBand)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiFooterBand)component).PrintAtBottom;
                        allProps["enabled"] = ((StiFooterBand)component).Enabled;
                        allProps["canGrow"] = ((StiFooterBand)component).CanGrow;
                        allProps["canShrink"] = ((StiFooterBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiFooterBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiFooterBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiColumnHeaderBand
                case "StiColumnHeaderBand":
                    {
                        allProps["brush"] = BrushToStr(((StiColumnHeaderBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiColumnHeaderBand)component).Border);
                        allProps["canBreak"] = ((StiColumnHeaderBand)component).CanBreak;
                        allProps["printOn"] = ((StiColumnHeaderBand)component).PrintOn.ToString();
                        allProps["newPageBefore"] = ((StiColumnHeaderBand)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiColumnHeaderBand)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiColumnHeaderBand)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiColumnHeaderBand)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiColumnHeaderBand)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiColumnHeaderBand)component).SkipFirst;
                        allProps["printIfEmpty"] = ((StiColumnHeaderBand)component).PrintIfEmpty;
                        allProps["printOnAllPages"] = ((StiColumnHeaderBand)component).PrintOnAllPages;
                        allProps["resetPageNumber"] = ((StiColumnHeaderBand)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiColumnHeaderBand)component).PrintAtBottom;
                        allProps["enabled"] = ((StiColumnHeaderBand)component).Enabled;
                        allProps["canGrow"] = ((StiColumnHeaderBand)component).CanGrow;
                        allProps["canShrink"] = ((StiColumnHeaderBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiColumnHeaderBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiColumnHeaderBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiColumnFooterBand
                case "StiColumnFooterBand":
                    {
                        allProps["brush"] = BrushToStr(((StiColumnFooterBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiColumnFooterBand)component).Border);
                        allProps["canBreak"] = ((StiColumnFooterBand)component).CanBreak;
                        allProps["printOn"] = ((StiColumnFooterBand)component).PrintOn.ToString();
                        allProps["newPageBefore"] = ((StiColumnFooterBand)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiColumnFooterBand)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiColumnFooterBand)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiColumnFooterBand)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiColumnFooterBand)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiColumnFooterBand)component).SkipFirst;
                        allProps["printIfEmpty"] = ((StiColumnFooterBand)component).PrintIfEmpty;
                        allProps["printOnAllPages"] = ((StiColumnFooterBand)component).PrintOnAllPages;
                        allProps["resetPageNumber"] = ((StiColumnFooterBand)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiColumnFooterBand)component).PrintAtBottom;
                        allProps["enabled"] = ((StiColumnFooterBand)component).Enabled;
                        allProps["canGrow"] = ((StiColumnFooterBand)component).CanGrow;
                        allProps["canShrink"] = ((StiColumnFooterBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiColumnFooterBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiColumnFooterBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiDataBand
                case "StiDataBand":
                    {
                        allProps["brush"] = BrushToStr(((StiDataBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiDataBand)component).Border);
                        allProps["canBreak"] = ((StiDataBand)component).CanBreak;
                        allProps["printOn"] = ((StiDataBand)component).PrintOn.ToString();
                        allProps["newPageBefore"] = ((StiDataBand)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiDataBand)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiDataBand)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiDataBand)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiDataBand)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiDataBand)component).SkipFirst;
                        allProps["columns"] = ((StiDataBand)component).Columns.ToString();
                        allProps["columnWidth"] = DoubleToStr(((StiDataBand)component).ColumnWidth);
                        allProps["columnGaps"] = DoubleToStr(((StiDataBand)component).ColumnGaps);
                        allProps["rightToLeft"] = ((StiDataBand)component).RightToLeft;
                        allProps["columnDirection"] = ((StiDataBand)component).ColumnDirection.ToString();
                        allProps["minRowsInColumn"] = ((StiDataBand)component).MinRowsInColumn.ToString();
                        allProps["sortData"] = GetSortDataProperty(component);
                        allProps["filterData"] = StiEncodingHelper.Encode(JSON.Encode(GetFiltersObject(((StiDataBand)component).Filters)));
                        allProps["filterMode"] = ((StiDataBand)component).FilterMode.ToString();
                        allProps["filterEngine"] = ((StiDataBand)component).FilterEngine.ToString();
                        allProps["filterOn"] = ((StiDataBand)component).FilterOn;
                        allProps["dataSource"] = (((StiDataBand)component).DataSource != null) ? ((StiDataBand)component).DataSource.Name : "[Not Assigned]";
                        allProps["dataRelation"] = (((StiDataBand)component).DataRelation != null) ? ((StiDataBand)component).DataRelation.NameInSource : "[Not Assigned]";
                        allProps["masterComponent"] = (((StiDataBand)component).MasterComponent != null) ? ((StiDataBand)component).MasterComponent.Name : "[Not Assigned]";
                        allProps["countData"] = ((StiDataBand)component).CountData.ToString();
                        allProps["oddStyle"] = !string.IsNullOrEmpty(((StiDataBand)component).OddStyle) ? ((StiDataBand)component).OddStyle : "[None]";
                        allProps["evenStyle"] = !string.IsNullOrEmpty(((StiDataBand)component).EvenStyle) ? ((StiDataBand)component).EvenStyle : "[None]";
                        allProps["businessObject"] = ((StiDataBand)component).BusinessObject != null ? ((StiDataBand)component).BusinessObject.GetFullName() : "[Not Assigned]";
                        allProps["calcInvisible"] = ((StiDataBand)component).CalcInvisible;
                        allProps["printOnAllPages"] = ((StiDataBand)component).PrintOnAllPages;
                        allProps["resetPageNumber"] = ((StiDataBand)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiDataBand)component).PrintAtBottom;
                        allProps["printIfDetailEmpty"] = ((StiDataBand)component).PrintIfDetailEmpty;
                        allProps["keepGroupTogether"] = ((StiDataBand)component).KeepGroupTogether;
                        allProps["keepDetailsTogether"] = ((StiDataBand)component).KeepDetailsTogether;
                        allProps["enabled"] = ((StiDataBand)component).Enabled;
                        allProps["canGrow"] = ((StiDataBand)component).CanGrow;
                        allProps["canShrink"] = ((StiDataBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiDataBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiDataBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiTable
                case "StiTable":
                    {
                        allProps["brush"] = BrushToStr(((StiTable)component).Brush);
                        allProps["border"] = BorderToStr(((StiTable)component).Border);
                        allProps["canBreak"] = ((StiTable)component).CanBreak;
                        allProps["printOn"] = ((StiTable)component).PrintOn.ToString();
                        allProps["newPageBefore"] = ((StiTable)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiTable)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiTable)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiTable)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiTable)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiTable)component).SkipFirst;
                        allProps["sortData"] = GetSortDataProperty(component);
                        allProps["filterData"] = StiEncodingHelper.Encode(JSON.Encode(GetFiltersObject(((StiTable)component).Filters)));
                        allProps["filterEngine"] = ((StiTable)component).FilterEngine.ToString();
                        allProps["filterMode"] = ((StiTable)component).FilterMode.ToString();
                        allProps["filterOn"] = ((StiTable)component).FilterOn;
                        allProps["dataSource"] = (((StiTable)component).DataSource != null) ? ((StiTable)component).DataSource.Name : "[Not Assigned]";
                        allProps["dataRelation"] = (((StiTable)component).DataRelation != null) ? ((StiTable)component).DataRelation.NameInSource : "[Not Assigned]";
                        allProps["masterComponent"] = (((StiTable)component).MasterComponent != null) ? ((StiTable)component).MasterComponent.Name : "[Not Assigned]";
                        allProps["countData"] = ((StiTable)component).CountData.ToString();
                        allProps["oddStyle"] = !string.IsNullOrEmpty(((StiTable)component).OddStyle) ? ((StiTable)component).OddStyle : "[None]";
                        allProps["evenStyle"] = !string.IsNullOrEmpty(((StiTable)component).EvenStyle) ? ((StiTable)component).EvenStyle : "[None]";
                        allProps["businessObject"] = ((StiTable)component).BusinessObject != null ? ((StiTable)component).BusinessObject.GetFullName() : "[Not Assigned]";
                        allProps["calcInvisible"] = ((StiTable)component).CalcInvisible;
                        allProps["printOnAllPages"] = ((StiTable)component).PrintOnAllPages;
                        allProps["resetPageNumber"] = ((StiTable)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiTable)component).PrintAtBottom;
                        allProps["printIfDetailEmpty"] = ((StiTable)component).PrintIfDetailEmpty;
                        allProps["keepGroupTogether"] = ((StiTable)component).KeepGroupTogether;
                        allProps["keepDetailsTogether"] = ((StiTable)component).KeepDetailsTogether;
                        allProps["enabled"] = ((StiTable)component).Enabled;
                        allProps["canGrow"] = ((StiTable)component).CanGrow;
                        allProps["canShrink"] = ((StiTable)component).CanShrink;
                        allProps["tableAutoWidth"] = ((StiTable)component).AutoWidth.ToString();
                        allProps["autoWidthType"] = ((StiTable)component).AutoWidthType.ToString();
                        allProps["columnCount"] = ((StiTable)component).ColumnCount.ToString();
                        allProps["rowCount"] = ((StiTable)component).RowCount.ToString();
                        allProps["headerRowsCount"] = ((StiTable)component).HeaderRowsCount.ToString();
                        allProps["footerRowsCount"] = ((StiTable)component).FooterRowsCount.ToString();
                        allProps["tableRightToLeft"] = ((StiTable)component).RightToLeft;
                        allProps["dockableTable"] = ((StiTable)component).DockableTable;
                        allProps["headerPrintOn"] = ((StiTable)component).HeaderPrintOn.ToString();
                        allProps["headerCanGrow"] = ((StiTable)component).HeaderCanGrow;
                        allProps["headerCanShrink"] = ((StiTable)component).HeaderCanShrink;
                        allProps["headerCanBreak"] = ((StiTable)component).HeaderCanBreak;
                        allProps["headerPrintAtBottom"] = ((StiTable)component).HeaderPrintAtBottom;
                        allProps["headerPrintIfEmpty"] = ((StiTable)component).HeaderPrintIfEmpty;
                        allProps["headerPrintOnAllPages"] = ((StiTable)component).HeaderPrintOnAllPages;
                        allProps["headerPrintOnEvenOddPages"] = ((StiTable)component).HeaderPrintOnEvenOddPages;
                        allProps["footerPrintOn"] = ((StiTable)component).FooterPrintOn.ToString();
                        allProps["footerCanGrow"] = ((StiTable)component).FooterCanGrow;
                        allProps["footerCanShrink"] = ((StiTable)component).FooterCanShrink;
                        allProps["footerCanBreak"] = ((StiTable)component).FooterCanBreak;
                        allProps["footerPrintAtBottom"] = ((StiTable)component).FooterPrintAtBottom;
                        allProps["footerPrintIfEmpty"] = ((StiTable)component).FooterPrintIfEmpty;
                        allProps["footerPrintOnAllPages"] = ((StiTable)component).FooterPrintOnAllPages;
                        allProps["footerPrintOnEvenOddPages"] = ((StiTable)component).FooterPrintOnEvenOddPages;
                        allProps["styleId"] = ((StiTable)component).TableStyleFX != null ? ((StiTable)component).TableStyleFX.StyleId.ToString() : "[None]";
                        break;
                    }
                #endregion

                #region StiHierarchicalBand
                case "StiHierarchicalBand":
                    {
                        allProps["brush"] = BrushToStr(((StiHierarchicalBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiHierarchicalBand)component).Border);
                        allProps["canBreak"] = ((StiHierarchicalBand)component).CanBreak;
                        allProps["printOn"] = ((StiHierarchicalBand)component).PrintOn.ToString();
                        allProps["newPageBefore"] = ((StiHierarchicalBand)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiHierarchicalBand)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiHierarchicalBand)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiHierarchicalBand)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiHierarchicalBand)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiHierarchicalBand)component).SkipFirst;
                        allProps["columns"] = ((StiHierarchicalBand)component).Columns.ToString();
                        allProps["columnWidth"] = DoubleToStr(((StiHierarchicalBand)component).ColumnWidth);
                        allProps["columnGaps"] = DoubleToStr(((StiHierarchicalBand)component).ColumnGaps);
                        allProps["rightToLeft"] = ((StiHierarchicalBand)component).RightToLeft;
                        allProps["columnDirection"] = ((StiHierarchicalBand)component).ColumnDirection.ToString();
                        allProps["minRowsInColumn"] = ((StiHierarchicalBand)component).MinRowsInColumn.ToString();
                        allProps["sortData"] = GetSortDataProperty(component);
                        allProps["filterData"] = StiEncodingHelper.Encode(JSON.Encode(GetFiltersObject(((StiHierarchicalBand)component).Filters)));
                        allProps["filterEngine"] = ((StiHierarchicalBand)component).FilterEngine.ToString();
                        allProps["filterMode"] = ((StiHierarchicalBand)component).FilterMode.ToString();
                        allProps["filterOn"] = ((StiHierarchicalBand)component).FilterOn;
                        allProps["dataSource"] = (((StiHierarchicalBand)component).DataSource != null) ? ((StiHierarchicalBand)component).DataSource.Name : "[Not Assigned]";
                        allProps["dataRelation"] = (((StiHierarchicalBand)component).DataRelation != null) ? ((StiHierarchicalBand)component).DataRelation.NameInSource : "[Not Assigned]";
                        allProps["masterComponent"] = (((StiHierarchicalBand)component).MasterComponent != null) ? ((StiHierarchicalBand)component).MasterComponent.Name : "[Not Assigned]";
                        allProps["countData"] = ((StiHierarchicalBand)component).CountData.ToString();
                        allProps["keyDataColumn"] = ((StiHierarchicalBand)component).KeyDataColumn;
                        allProps["masterKeyDataColumn"] = ((StiHierarchicalBand)component).MasterKeyDataColumn;
                        allProps["parentValue"] = StiEncodingHelper.Encode(((StiHierarchicalBand)component).ParentValue);
                        allProps["indent"] = DoubleToStr(((StiHierarchicalBand)component).Indent);
                        allProps["headers"] = StiEncodingHelper.Encode(((StiHierarchicalBand)component).Headers);
                        allProps["footers"] = StiEncodingHelper.Encode(((StiHierarchicalBand)component).Footers);
                        allProps["oddStyle"] = !string.IsNullOrEmpty(((StiHierarchicalBand)component).OddStyle) ? ((StiHierarchicalBand)component).OddStyle : "[None]";
                        allProps["evenStyle"] = !string.IsNullOrEmpty(((StiHierarchicalBand)component).EvenStyle) ? ((StiHierarchicalBand)component).EvenStyle : "[None]";
                        allProps["businessObject"] = ((StiHierarchicalBand)component).BusinessObject != null ? ((StiHierarchicalBand)component).BusinessObject.GetFullName() : "[Not Assigned]";
                        allProps["calcInvisible"] = ((StiHierarchicalBand)component).CalcInvisible;
                        allProps["printOnAllPages"] = ((StiHierarchicalBand)component).PrintOnAllPages;
                        allProps["resetPageNumber"] = ((StiHierarchicalBand)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiHierarchicalBand)component).PrintAtBottom;
                        allProps["printIfDetailEmpty"] = ((StiHierarchicalBand)component).PrintIfDetailEmpty;
                        allProps["keepGroupTogether"] = ((StiHierarchicalBand)component).KeepGroupTogether;
                        allProps["keepDetailsTogether"] = ((StiHierarchicalBand)component).KeepDetailsTogether;
                        allProps["enabled"] = ((StiHierarchicalBand)component).Enabled;
                        allProps["canGrow"] = ((StiHierarchicalBand)component).CanGrow;
                        allProps["canShrink"] = ((StiHierarchicalBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiHierarchicalBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiHierarchicalBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiChildBand
                case "StiChildBand":
                    {
                        allProps["brush"] = BrushToStr(((StiChildBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiChildBand)component).Border);
                        allProps["canBreak"] = ((StiChildBand)component).CanBreak;
                        allProps["printOn"] = ((StiChildBand)component).PrintOn.ToString();
                        allProps["newPageBefore"] = ((StiChildBand)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiChildBand)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiChildBand)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiChildBand)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiChildBand)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiChildBand)component).SkipFirst;
                        allProps["resetPageNumber"] = ((StiChildBand)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiChildBand)component).PrintAtBottom;
                        allProps["enabled"] = ((StiChildBand)component).Enabled;
                        allProps["canGrow"] = ((StiChildBand)component).CanGrow;
                        allProps["canShrink"] = ((StiChildBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiChildBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiChildBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiEmptyBand
                case "StiEmptyBand":
                    {
                        allProps["brush"] = BrushToStr(((StiEmptyBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiEmptyBand)component).Border);
                        allProps["printOn"] = ((StiEmptyBand)component).PrintOn.ToString();
                        allProps["oddStyle"] = !string.IsNullOrEmpty(((StiEmptyBand)component).OddStyle) ? ((StiEmptyBand)component).OddStyle : "[None]";
                        allProps["evenStyle"] = !string.IsNullOrEmpty(((StiEmptyBand)component).EvenStyle) ? ((StiEmptyBand)component).EvenStyle : "[None]";
                        allProps["resetPageNumber"] = ((StiEmptyBand)component).ResetPageNumber;
                        allProps["enabled"] = ((StiEmptyBand)component).Enabled;
                        allProps["canGrow"] = ((StiEmptyBand)component).CanGrow;
                        allProps["canShrink"] = ((StiEmptyBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiEmptyBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiEmptyBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiReportSummaryBand
                case "StiReportSummaryBand":
                    {
                        allProps["brush"] = BrushToStr(((StiReportSummaryBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiReportSummaryBand)component).Border);
                        allProps["canBreak"] = ((StiReportSummaryBand)component).CanBreak;
                        allProps["gGrowToHeight"] = ((StiReportSummaryBand)component).GrowToHeight;
                        allProps["newPageBefore"] = ((StiReportSummaryBand)component).NewPageBefore;
                        allProps["newPageAfter"] = ((StiReportSummaryBand)component).NewPageAfter;
                        allProps["newColumnBefore"] = ((StiReportSummaryBand)component).NewColumnBefore;
                        allProps["newColumnAfter"] = ((StiReportSummaryBand)component).NewColumnAfter;
                        allProps["breakIfLessThan"] = DoubleToStr(((StiReportSummaryBand)component).BreakIfLessThan);
                        allProps["skipFirst"] = ((StiReportSummaryBand)component).SkipFirst;
                        allProps["printIfEmpty"] = ((StiReportSummaryBand)component).PrintIfEmpty;
                        allProps["resetPageNumber"] = ((StiReportSummaryBand)component).ResetPageNumber;
                        allProps["printAtBottom"] = ((StiReportSummaryBand)component).PrintAtBottom;
                        allProps["enabled"] = ((StiReportSummaryBand)component).Enabled;
                        allProps["canGrow"] = ((StiReportSummaryBand)component).CanGrow;
                        allProps["canShrink"] = ((StiReportSummaryBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiReportSummaryBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiReportSummaryBand)component).MaxHeight);
                        break;
                    }
                #endregion

                #region StiOverlayBand
                case "StiOverlayBand":
                    {
                        allProps["brush"] = BrushToStr(((StiOverlayBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiOverlayBand)component).Border);
                        allProps["printOn"] = ((StiOverlayBand)component).PrintOn.ToString();
                        allProps["resetPageNumber"] = ((StiOverlayBand)component).ResetPageNumber;
                        allProps["enabled"] = ((StiOverlayBand)component).Enabled;
                        allProps["canGrow"] = ((StiOverlayBand)component).CanGrow;
                        allProps["canShrink"] = ((StiOverlayBand)component).CanShrink;
                        allProps["minHeight"] = DoubleToStr(((StiOverlayBand)component).MinHeight);
                        allProps["maxHeight"] = DoubleToStr(((StiOverlayBand)component).MaxHeight);
                        allProps["vertAlignment"] = ((StiOverlayBand)component).VertAlignment.ToString();
                        break;
                    }
                #endregion


                #region StiCrossTab
                case "StiCrossTab":
                    {
                        allProps["brush"] = BrushToStr(((StiCrossTab)component).Brush);
                        allProps["border"] = BorderToStr(((StiCrossTab)component).Border);
                        allProps["growToHeight"] = ((StiCrossTab)component).GrowToHeight;
                        allProps["printOn"] = ((StiCrossTab)component).PrintOn.ToString();
                        allProps["printable"] = ((StiCrossTab)component).Printable;
                        allProps["clientLeft"] = DoubleToStr(component.Left);
                        allProps["clientTop"] = DoubleToStr(component.Top);
                        allProps["sortData"] = GetSortDataProperty(component);
                        allProps["filterData"] = StiEncodingHelper.Encode(JSON.Encode(GetFiltersObject(((StiCrossTab)component).Filters)));
                        allProps["filterMode"] = ((StiCrossTab)component).FilterMode.ToString();
                        allProps["filterOn"] = ((StiCrossTab)component).FilterOn;
                        allProps["filterEngine"] = ((StiCrossTab)component).FilterEngine.ToString();
                        allProps["dataSource"] = (((StiCrossTab)component).DataSource != null) ? ((StiCrossTab)component).DataSource.Name : "[Not Assigned]";
                        allProps["dataRelation"] = (((StiCrossTab)component).DataRelation != null) ? ((StiCrossTab)component).DataRelation.NameInSource : "[Not Assigned]";
                        allProps["crossTabEmptyValue"] = StiEncodingHelper.Encode(((StiCrossTab)component).EmptyValue);
                        allProps["crossTabHorAlign"] = ((StiCrossTab)component).HorAlignment.ToString();
                        allProps["printIfEmpty"] = ((StiCrossTab)component).PrintIfEmpty;
                        allProps["rightToLeft"] = ((StiCrossTab)component).RightToLeft;
                        allProps["crossTabWrap"] = ((StiCrossTab)component).Wrap;
                        allProps["crossTabWrapGap"] = DoubleToStr(((StiCrossTab)component).WrapGap);
                        allProps["enabled"] = ((StiCrossTab)component).Enabled;
                        allProps["canGrow"] = ((StiCrossTab)component).CanGrow;
                        allProps["canShrink"] = ((StiCrossTab)component).CanShrink;
                        allProps["crossTabFields"] = GetCrossTabFieldsProperties(component as StiCrossTab);
                        allProps["dockStyle"] = ((StiCrossTab)component).DockStyle.ToString();
                        allProps["summaryDirection"] = ((StiCrossTab)component).SummaryDirection.ToString();
                        allProps["minSize"] = DoubleToStr(component.MinSize.Width) + ";" + DoubleToStr(component.MinSize.Height);
                        allProps["maxSize"] = DoubleToStr(component.MaxSize.Width) + ";" + DoubleToStr(component.MaxSize.Height);
                        break;
                    }
                #endregion

                #region StiCrossGroupHeaderBand
                case "StiCrossGroupHeaderBand":
                    {
                        allProps["brush"] = BrushToStr(((StiCrossGroupHeaderBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiCrossGroupHeaderBand)component).Border);
                        allProps["printOn"] = ((StiCrossGroupHeaderBand)component).PrintOn.ToString();
                        allProps["condition"] = (((StiCrossGroupHeaderBand)component).Condition != null) ? StiEncodingHelper.Encode(((StiCrossGroupHeaderBand)component).Condition.Value) : string.Empty;
                        allProps["sortDirection"] = ((int)((StiCrossGroupHeaderBand)component).SortDirection).ToString();
                        allProps["summarySortDirection"] = ((int)((StiCrossGroupHeaderBand)component).SummarySortDirection).ToString();
                        allProps["summaryExpression"] = StiEncodingHelper.Encode(((StiCrossGroupHeaderBand)component).SummaryExpression.ToString());
                        allProps["summaryType"] = ((int)((StiCrossGroupHeaderBand)component).SummaryType).ToString();
                        allProps["keepGroupTogether"] = ((StiCrossGroupHeaderBand)component).KeepGroupTogether;
                        allProps["keepGroupHeaderTogether"] = ((StiCrossGroupHeaderBand)component).KeepGroupHeaderTogether;
                        allProps["enabled"] = ((StiCrossGroupHeaderBand)component).Enabled;
                        allProps["canGrow"] = ((StiCrossGroupHeaderBand)component).CanGrow;
                        allProps["canShrink"] = ((StiCrossGroupHeaderBand)component).CanShrink;
                        allProps["canBreak"] = ((StiCrossGroupHeaderBand)component).CanBreak;
                        allProps["minWidth"] = DoubleToStr(((StiCrossGroupHeaderBand)component).MinWidth);
                        allProps["maxWidth"] = DoubleToStr(((StiCrossGroupHeaderBand)component).MaxWidth);
                        break;
                    }
                #endregion

                #region StiCrossGroupFooterBand
                case "StiCrossGroupFooterBand":
                    {
                        allProps["brush"] = BrushToStr(((StiCrossGroupFooterBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiCrossGroupFooterBand)component).Border);
                        allProps["printOn"] = ((StiCrossGroupFooterBand)component).PrintOn.ToString();
                        allProps["enabled"] = ((StiCrossGroupFooterBand)component).Enabled;
                        allProps["canGrow"] = ((StiCrossGroupFooterBand)component).CanGrow;
                        allProps["canShrink"] = ((StiCrossGroupFooterBand)component).CanShrink;
                        allProps["canBreak"] = ((StiCrossGroupFooterBand)component).CanBreak;
                        allProps["keepGroupFooterTogether"] = ((StiCrossGroupFooterBand)component).KeepGroupFooterTogether;
                        allProps["minWidth"] = DoubleToStr(((StiCrossGroupFooterBand)component).MinWidth);
                        allProps["maxWidth"] = DoubleToStr(((StiCrossGroupFooterBand)component).MaxWidth);
                        break;
                    }
                #endregion

                #region StiCrossHeaderBand
                case "StiCrossHeaderBand":
                    {
                        allProps["brush"] = BrushToStr(((StiCrossHeaderBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiCrossHeaderBand)component).Border);
                        allProps["enabled"] = ((StiCrossHeaderBand)component).Enabled;
                        allProps["canGrow"] = ((StiCrossHeaderBand)component).CanGrow;
                        allProps["canShrink"] = ((StiCrossHeaderBand)component).CanShrink;
                        allProps["canBreak"] = ((StiCrossHeaderBand)component).CanBreak;
                        allProps["keepHeaderTogether"] = ((StiCrossHeaderBand)component).KeepHeaderTogether;
                        allProps["minWidth"] = DoubleToStr(((StiCrossHeaderBand)component).MinWidth);
                        allProps["maxWidth"] = DoubleToStr(((StiCrossHeaderBand)component).MaxWidth);
                        break;
                    }
                #endregion

                #region StiCrossFooterBand
                case "StiCrossFooterBand":
                    {
                        allProps["brush"] = BrushToStr(((StiCrossFooterBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiCrossFooterBand)component).Border);
                        allProps["enabled"] = ((StiCrossFooterBand)component).Enabled;
                        allProps["canGrow"] = ((StiCrossFooterBand)component).CanGrow;
                        allProps["canShrink"] = ((StiCrossFooterBand)component).CanShrink;
                        allProps["canBreak"] = ((StiCrossFooterBand)component).CanBreak;
                        allProps["keepFooterTogether"] = ((StiCrossFooterBand)component).KeepFooterTogether;
                        allProps["minWidth"] = DoubleToStr(((StiCrossFooterBand)component).MinWidth);
                        allProps["maxWidth"] = DoubleToStr(((StiCrossFooterBand)component).MaxWidth);
                        break;
                    }
                #endregion

                #region StiCrossDataBand
                case "StiCrossDataBand":
                    {
                        allProps["brush"] = BrushToStr(((StiCrossDataBand)component).Brush);
                        allProps["border"] = BorderToStr(((StiCrossDataBand)component).Border);
                        allProps["sortData"] = GetSortDataProperty(component);
                        allProps["filterData"] = StiEncodingHelper.Encode(JSON.Encode(GetFiltersObject(((StiCrossDataBand)component).Filters)));
                        allProps["filterEngine"] = ((StiCrossDataBand)component).FilterEngine.ToString();
                        allProps["filterMode"] = ((StiCrossDataBand)component).FilterMode.ToString();
                        allProps["filterOn"] = ((StiCrossDataBand)component).FilterOn;
                        allProps["dataSource"] = (((StiCrossDataBand)component).DataSource != null) ? ((StiCrossDataBand)component).DataSource.Name : "[Not Assigned]";
                        allProps["dataRelation"] = (((StiCrossDataBand)component).DataRelation != null) ? ((StiCrossDataBand)component).DataRelation.NameInSource : "[Not Assigned]";
                        allProps["masterComponent"] = (((StiCrossDataBand)component).MasterComponent != null) ? ((StiCrossDataBand)component).MasterComponent.Name : "[Not Assigned]";
                        allProps["countData"] = ((StiCrossDataBand)component).CountData.ToString();
                        allProps["oddStyle"] = !string.IsNullOrEmpty(((StiCrossDataBand)component).OddStyle) ? ((StiCrossDataBand)component).OddStyle : "[None]";
                        allProps["evenStyle"] = !string.IsNullOrEmpty(((StiCrossDataBand)component).EvenStyle) ? ((StiCrossDataBand)component).EvenStyle : "[None]";
                        allProps["businessObject"] = ((StiCrossDataBand)component).BusinessObject != null ? ((StiCrossDataBand)component).BusinessObject.GetFullName() : "[Not Assigned]";
                        allProps["calcInvisible"] = ((StiCrossDataBand)component).CalcInvisible;
                        allProps["printIfDetailEmpty"] = ((StiCrossDataBand)component).PrintIfDetailEmpty;
                        allProps["keepDetailsTogether"] = ((StiCrossDataBand)component).KeepDetailsTogether;
                        allProps["enabled"] = ((StiCrossDataBand)component).Enabled;
                        allProps["canGrow"] = ((StiCrossDataBand)component).CanGrow;
                        allProps["canShrink"] = ((StiCrossDataBand)component).CanShrink;
                        allProps["canBreak"] = ((StiCrossDataBand)component).CanBreak;
                        allProps["minWidth"] = DoubleToStr(((StiCrossDataBand)component).MinWidth);
                        allProps["maxWidth"] = DoubleToStr(((StiCrossDataBand)component).MaxWidth);
                        break;
                    }
                #endregion


                #region StiDashboard
                case "StiDashboard":
                    {
                        var page = component as StiPage;
                        allProps["isDashboard"] = true;
                        allProps["gridSize"] = StiReportEdit.DoubleToStr(page.GridSize);
                        allProps["border"] = BorderToStr(page.Border);
                        allProps["brush"] = BrushToStr(page.Brush);
                        allProps["orientation"] = page.Orientation.ToString();
                        allProps["unitWidth"] = DoubleToStr(page.Width);
                        allProps["unitHeight"] = DoubleToStr(page.Height);
                        allProps["unitMargins"] = GetPageMargins(page);
                        allProps["aliasName"] = StiEncodingHelper.Encode(page.Alias);
                        allProps["elementStyle"] = (component as IStiDashboard).Style;
                        break;
                    }
                #endregion

                #region StiTableElement
                case "StiTableElement":
                    {
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        allProps["elementStyle"] = (component as IStiTableElement).Style;
                        break;
                    }
                #endregion

                #region StiChartElement
                case "StiChartElement":
                    {
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        allProps["elementStyle"] = (component as IStiChartElement).Style;
                        break;
                    }
                #endregion

                #region StiGaugeElement
                case "StiGaugeElement":
                    {
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        allProps["elementStyle"] = (component as IStiGaugeElement).Style;
                        break;
                    }
                #endregion

                #region StiPivotElement
                case "StiPivotElement":
                    {
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        allProps["elementStyle"] = (component as IStiPivotElement).Style;
                        break;
                    }
                #endregion

                #region StiIndicatorElement
                case "StiIndicatorElement":
                    {
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        allProps["elementStyle"] = (component as IStiIndicatorElement).Style;
                        break;
                    }
                #endregion

                #region StiProgressElement
                case "StiProgressElement":
                    {
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        allProps["elementStyle"] = (component as IStiProgressElement).Style;
                        break;
                    }
                #endregion

                #region StiMapElement
                case "StiMapElement":
                    {
                        allProps["mapMode"] = (component as IStiMapElement).MapMode;
                        allProps["mapType"] = (component as IStiMapElement).MapType;
                        allProps["mapID"] = (component as IStiMapElement).MapID;
                        allProps["showValue"] = (component as IStiMapElement).ShowValue;
                        allProps["colorEach"] = (component as IStiMapElement).ColorEach;
                        allProps["displayNameType"] = (component as IStiMapElement).ShowName;
                        allProps["dataFrom"] = (component as IStiMapElement).DataFrom;
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        allProps["elementStyle"] = (component as IStiMapElement).Style;
                        break;
                    }
                #endregion

                #region StiImageElement
                case "StiImageElement":
                    {
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        allProps["imageSrc"] = ((IStiImageElement)component).Image != null ? ImageToBase64(((IStiImageElement)component).Image) : string.Empty;
                        allProps["imageUrl"] = ((IStiImageElement)component).ImageHyperlink != null ? StiEncodingHelper.Encode(((IStiImageElement)component).ImageHyperlink) : string.Empty;
                        allProps["ratio"] = ((IStiImageElement)component).AspectRatio;
                        allProps["horAlignment"] = "Center";
                        allProps["vertAlignment"] = "Center";
                        allProps["stretch"] = true;
                        allProps["imageMultipleFactor"] = 1;
                        break;
                    }
                #endregion

                #region StiTextElement
                case "StiTextElement":
                    {
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        allProps["text"] = StiEncodingHelper.Encode(((IStiTextElement)component).Text);
                        allProps["horAlignment"] = ((IStiTextHorAlignment)component).HorAlignment.ToString();
                        allProps["vertAlignment"] = ((IStiVertAlignment)component).VertAlignment.ToString();
                        allProps["foreColor"] = GetStringFromColor(((IStiForeColor)component).ForeColor);
                        allProps["font"] = FontToStr(((IStiTextFont)component).GetFont());
                        break;
                    }
                #endregion

                #region StiPanelElement
                case "StiPanelElement":
                    {
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        break;
                    }
                #endregion

                #region StiShapeElement
                case "StiShapeElement":
                    {
                        allProps["border"] = BorderToStr((component as IStiBorder).Border);
                        allProps["brush"] = BrushToStr((component as IStiBrush).Brush);
                        break;
                    }
                #endregion
            }

            return allProps;
        }
        #endregion

        #region Get Property
        #region RichText
        public static string GetRichTextProperty(StiRichText component)
        {
            StiExpression text = component.Text;
            if (StiHyperlinkProcessor.IsServerHyperlink(text != null ? text.Value : null))
            {
                return StiEncodingHelper.Encode(text.Value);
            }
            RtfToHtmlConverter rtfConverter = new RtfToHtmlConverter();

            string htmlText = rtfConverter.ConvertRtfToHtml(component.RtfText);
            htmlText = htmlText.Replace("  ", "&nbsp;&nbsp;"); //Fixed bug with multispaces
            htmlText = htmlText.Replace("<SPAN />", "<BR>");

            return StiEncodingHelper.Encode(htmlText);
        }
        #endregion

        #region Sort
        public static string GetSortDataProperty(object object_)
        {
            ArrayList sorts = new ArrayList();

            string[] sort = null;
            PropertyInfo pi2 = object_.GetType().GetProperty("Sort");
            if (pi2 != null) sort = (string[])pi2.GetValue(object_, null);

            if (sort != null)
            {
                ArrayList singleSort = null;
                for (int i = 0; i < sort.Length; i++)
                {
                    if (sort[i] == "ASC" || sort[i] == "DESC" || i == sort.Length - 1)
                    {
                        if (i == sort.Length - 1) singleSort.Add(sort[i]);
                        if (singleSort != null) sorts.Add(GetSingleSort(object_, singleSort));
                        singleSort = new ArrayList();
                    }
                    singleSort.Add(sort[i]);
                }
            }

            return sorts.Count != 0 ? StiEncodingHelper.Encode(JSON.Encode(sorts)) : string.Empty;
        }

        private static string GetRelationNameByNameInSource(object object_, string nameInSource)
        {
            StiDictionary dictionary = null;

            if (object_ is StiComponent) dictionary = ((StiComponent)object_).Report.Dictionary;
            else if (object_ is StiDataSource) dictionary = ((StiDataSource)object_).Dictionary;

            if (dictionary != null)
            {
                StiDataRelationsCollection relations = dictionary.Relations;

                foreach (StiDataRelation relation in relations)
                {
                    if (relation.NameInSource == nameInSource)
                        return relation.Name;
                }
            }

            return string.Empty;
        }

        private static Hashtable GetSingleSort(object object_, ArrayList sortArray_)
        {
            Hashtable oneSort = new Hashtable();
            Array sortArray = sortArray_.ToArray();
            oneSort["direction"] = (string)sortArray.GetValue(0);

            oneSort["column"] = string.Empty;
            for (int k = 1; k < sortArray.Length - 1; k++)
                oneSort["column"] += GetRelationNameByNameInSource(object_, (string)sortArray.GetValue(k)) + ".";

            oneSort["column"] += (string)sortArray.GetValue(sortArray.Length - 1);

            return oneSort;
        }
        #endregion

        #region Filter
        public static ArrayList GetFiltersObject(StiFiltersCollection filters)
        {
            ArrayList result = new ArrayList();
            foreach (StiFilter filter in filters)
            {
                Hashtable singleFilter = new Hashtable();
                result.Add(singleFilter);

                singleFilter["fieldIs"] = filter.Item.ToString();
                singleFilter["dataType"] = filter.DataType.ToString();
                singleFilter["column"] = filter.Column;
                singleFilter["condition"] = filter.Condition.ToString();
                singleFilter["value1"] = filter.Value1.ToString();
                singleFilter["value2"] = filter.Value2.ToString();

                string expression = filter.Expression.Value;
                if (!String.IsNullOrEmpty(expression) && expression.StartsWith("{") && expression.EndsWith("}"))
                    expression = expression.Substring(1, expression.Length - 2);

                singleFilter["expression"] = expression;
            }

            return result;
        }

        public static string GetFilterDataProperty(object component)
        {
            StiFiltersCollection filters = null;
            ArrayList result = new ArrayList();

            PropertyInfo pi = component.GetType().GetProperty("Filters");
            if (pi != null) filters = (StiFiltersCollection)pi.GetValue(component, null);
            if (filters != null) result = GetFiltersObject(filters);

            return StiEncodingHelper.Encode(JSON.Encode(result));
        }

        public static bool GetFilterOnProperty(object component)
        {
            bool filterOn = true;

            PropertyInfo pi = component.GetType().GetProperty("FilterOn");
            if (pi != null) filterOn = (bool)pi.GetValue(component, null);

            return filterOn;
        }

        public static string GetFilterModeProperty(object component)
        {
            StiFilterMode filterMode = StiFilterMode.And;

            PropertyInfo pi = component.GetType().GetProperty("FilterMode");
            if (pi != null) filterMode = (StiFilterMode)pi.GetValue(component, null);

            return filterMode.ToString();
        }
        #endregion        

        #region SvgContent
        public static string GetSvgContent(StiComponent component, double zoom)
        {
            if (component is StiImage || component.Width == 0 || component.Height == 0) return string.Empty;

            string svgContent = string.Empty;

            if (component is IStiMapElement)
                svgContent = StiMapElementHelper.GetMapElementSvgContent(component as IStiMapElement, zoom);
            else if (component is IStiShapeElement)
                svgContent = StiShapeElementHelper.GetShapeElementSvgContent(component as IStiShapeElement, zoom);
            else if (component is IStiTextElement)
                svgContent = StiTextElementHelper.GetTextElementSvgContent(component as IStiTextElement);
            else if (component is IStiGaugeElement)
                svgContent = StiGaugeElementHelper.GetGaugeElementSvgContent(component as IStiGaugeElement, zoom);
            else if (component is IStiChartElement)
                svgContent = StiChartElementHelper.GetChartElementSvgContent(component as IStiChartElement, zoom);
            else if (component is IStiTableElement)
                svgContent = StiTableElementHelper.GetTableElementSvgContent(component as IStiTableElement, zoom);
            else if (component is IStiTableElement)
                svgContent = StiProgressElementHelper.GetProgressElementSvgContent(component as IStiProgressElement, zoom);
            else if (component is IStiIndicatorElement)
                svgContent = StiIndicatorElementHelper.GetIndicatorElementSvgContent(component as IStiIndicatorElement, zoom);
            else if (component is IStiPivotElement)
                svgContent = StiPivotElementHelper.GetPivotElementSvgContent(component as IStiPivotElement, zoom);
            else 
                svgContent = StiSvgHelper.SaveComponentToString(component, ImageFormat.Png, 0.75f, (float)(zoom * 100));

            return StiEncodingHelper.Encode(svgContent);
        }
        #endregion

        #region Conditions
        public static string GetConditionsProperty(object component)
        {
            ArrayList conditionsArray = new ArrayList();
            StiConditionsCollection conditions = null;
            PropertyInfo pi = component.GetType().GetProperty("Conditions");
            if (pi != null) conditions = (StiConditionsCollection)pi.GetValue(component, null);

            foreach (StiBaseCondition condition in conditions)
            {
                if (condition is StiDataBarCondition)
                {
                    conditionsArray.Add(GetDataBarConditionObject(condition as StiDataBarCondition));
                }
                else if (condition is StiIconSetCondition)
                {
                    conditionsArray.Add(GetIconSetConditionObject(condition as StiIconSetCondition));
                }
                else if (condition is StiColorScaleCondition)
                {
                    conditionsArray.Add(GetColorScaleConditionObject(condition as StiColorScaleCondition));
                }
                else if (condition is StiCondition)
                {
                    conditionsArray.Add(GetHighlightConditionObject(condition as StiCondition));
                }

            }
            return conditionsArray.Count > 0 ? StiEncodingHelper.Encode(JSON.Encode(conditionsArray)) : "";
        }

        public static Hashtable GetDataBarConditionObject(StiDataBarCondition condition)
        {
            Hashtable conditionObject = new Hashtable();
            conditionObject["ConditionType"] = "StiDataBarCondition";
            conditionObject["Column"] = condition.Column;
            conditionObject["MaximumType"] = condition.MaximumType;
            conditionObject["MaximumValue"] = DoubleToStr(condition.MaximumValue);
            conditionObject["MinimumType"] = condition.MinimumType;
            conditionObject["MinimumValue"] = DoubleToStr(condition.MinimumValue);
            conditionObject["Direction"] = condition.Direction;
            conditionObject["BrushType"] = condition.BrushType;
            conditionObject["PositiveColor"] = GetStringFromColor(condition.PositiveColor);
            conditionObject["NegativeColor"] = GetStringFromColor(condition.NegativeColor);
            conditionObject["ShowBorder"] = condition.ShowBorder;
            conditionObject["PositiveBorderColor"] = GetStringFromColor(condition.PositiveBorderColor);
            conditionObject["NegativeBorderColor"] = GetStringFromColor(condition.NegativeBorderColor);

            return conditionObject;
        }

        public static Hashtable GetIconSetConditionObject(StiIconSetCondition condition)
        {
            Hashtable conditionObject = new Hashtable();
            conditionObject["ConditionType"] = "StiIconSetCondition";
            conditionObject["Column"] = condition.Column;
            conditionObject["ContentAlignment"] = condition.ContentAlignment;
            conditionObject["IconSet"] = condition.IconSet;
            conditionObject["IconSetItem1"] = GetIconSetItemObject(condition.IconSetItem1);
            conditionObject["IconSetItem2"] = GetIconSetItemObject(condition.IconSetItem2);
            conditionObject["IconSetItem3"] = GetIconSetItemObject(condition.IconSetItem3);
            conditionObject["IconSetItem4"] = GetIconSetItemObject(condition.IconSetItem4);
            conditionObject["IconSetItem5"] = GetIconSetItemObject(condition.IconSetItem5);

            return conditionObject;
        }

        public static Hashtable GetColorScaleConditionObject(StiColorScaleCondition condition)
        {
            Hashtable conditionObject = new Hashtable();
            conditionObject["ConditionType"] = "StiColorScaleCondition";
            conditionObject["Column"] = condition.Column;
            conditionObject["ScaleType"] = condition.ScaleType;
            conditionObject["MaximumType"] = condition.MaximumType;
            conditionObject["MaximumValue"] = DoubleToStr(condition.MaximumValue);
            conditionObject["MaximumColor"] = GetStringFromColor(condition.MaximumColor);
            conditionObject["MinimumType"] = condition.MinimumType;
            conditionObject["MinimumValue"] = DoubleToStr(condition.MinimumValue);
            conditionObject["MinimumColor"] = GetStringFromColor(condition.MinimumColor);
            conditionObject["MidType"] = condition.MidType;
            conditionObject["MidValue"] = DoubleToStr(condition.MidValue);
            conditionObject["MidColor"] = GetStringFromColor(condition.MidColor);

            return conditionObject;
        }

        public static Hashtable GetHighlightConditionObject(StiCondition condition)
        {
            Hashtable conditionObject = new Hashtable();
            conditionObject["ConditionType"] = "StiHighlightCondition";
            conditionObject["BorderSides"] = condition.BorderSides.ToString();
            conditionObject["Permissions"] = condition.Permissions.ToString();
            conditionObject["Style"] = condition.Style.ToString();
            conditionObject["Font"] = FontToStr(condition.Font);
            conditionObject["BackColor"] = GetStringFromColor(condition.BackColor);
            conditionObject["TextColor"] = GetStringFromColor(condition.TextColor);
            conditionObject["Enabled"] = condition.Enabled;
            conditionObject["AssignExpression"] = StiEncodingHelper.Encode(condition.AssignExpression);
            conditionObject["CanAssignExpression"] = condition.CanAssignExpression;
            conditionObject["BreakIfTrue"] = condition.BreakIfTrue;

            if (condition is StiMultiCondition)
            {
                conditionObject["Filters"] = GetFilterDataProperty((StiMultiCondition)condition);
                conditionObject["FilterMode"] = GetFilterModeProperty((StiMultiCondition)condition);
            }
            else
            {
                ArrayList filters = new ArrayList();
                Hashtable filter = new Hashtable();
                filters.Add(filter);

                filter["fieldIs"] = condition.Item.ToString();
                filter["dataType"] = condition.DataType.ToString();
                filter["column"] = condition.Column;
                filter["condition"] = condition.Condition.ToString();
                filter["value1"] = condition.Value1.ToString();
                filter["value2"] = condition.Value2.ToString();

                string expression = condition.Expression.Value;
                if (!String.IsNullOrEmpty(expression) && expression.StartsWith("{") && expression.EndsWith("}"))
                    expression = expression.Substring(1, expression.Length - 2);

                filter["expression"] = expression;

                conditionObject["Filters"] = StiEncodingHelper.Encode(JSON.Encode(filters));
            }

            return conditionObject;
        }
        #endregion

        #region HeaderSize
        public static string GetComponentHeaderSize(object component)
        {
            object headerSize = StiReportEdit.GetPropertyValue("HeaderSize", component);
            return headerSize != null ? (string)headerSize : "0";
        }
        #endregion

        #region Interaction
        public static Hashtable GetInteractionProperty(StiInteraction interaction)
        {
            Hashtable interactionObject = new Hashtable();

            if (interaction is StiBandInteraction)
            {
                StiBandInteraction bandInteraction = interaction as StiBandInteraction;
                interactionObject["isBandInteraction"] = true;
                interactionObject["collapsingEnabled"] = bandInteraction.CollapsingEnabled;
                interactionObject["collapseGroupFooter"] = bandInteraction.CollapseGroupFooter;
                interactionObject["collapsedValue"] = StiEncodingHelper.Encode(bandInteraction.Collapsed.Value);
                interactionObject["selectionEnabled"] = bandInteraction.SelectionEnabled;
            }

            interactionObject["drillDownEnabled"] = interaction.DrillDownEnabled;
            interactionObject["drillDownReport"] = interaction.DrillDownReport;
            interactionObject["drillDownMode"] = interaction.DrillDownMode.ToString();
            interactionObject["drillDownPage"] = interaction.DrillDownPage != null ? interaction.DrillDownPage.Name : "";

            interactionObject["drillDownParameter1Name"] = interaction.DrillDownParameter1.Name;
            interactionObject["drillDownParameter1Expression"] = StiEncodingHelper.Encode(interaction.DrillDownParameter1.Expression.Value);
            interactionObject["drillDownParameter2Name"] = interaction.DrillDownParameter2.Name;
            interactionObject["drillDownParameter2Expression"] = StiEncodingHelper.Encode(interaction.DrillDownParameter2.Expression.Value);
            interactionObject["drillDownParameter3Name"] = interaction.DrillDownParameter3.Name;
            interactionObject["drillDownParameter3Expression"] = StiEncodingHelper.Encode(interaction.DrillDownParameter3.Expression.Value);
            interactionObject["drillDownParameter4Name"] = interaction.DrillDownParameter4.Name;
            interactionObject["drillDownParameter4Expression"] = StiEncodingHelper.Encode(interaction.DrillDownParameter4.Expression.Value);
            interactionObject["drillDownParameter5Name"] = interaction.DrillDownParameter5.Name;
            interactionObject["drillDownParameter5Expression"] = StiEncodingHelper.Encode(interaction.DrillDownParameter5.Expression.Value);

            interactionObject["sortingEnabled"] = interaction.SortingEnabled;

            if (!string.IsNullOrEmpty(interaction.SortingColumn))
            {
                string column = interaction.SortingColumn;
                if (column.StartsWith("{"))
                    column = column.Remove(0, 1);
                if (column.EndsWith("}"))
                    column = column.Remove(column.Length - 1, 1);
                interactionObject["sortingColumn"] = column;
            }
            else
                interactionObject["sortingColumn"] = string.Empty;

            interactionObject["bookmark"] = StiEncodingHelper.Encode(interaction.Bookmark.Value);

            string hyperlinkText = interaction.Hyperlink.Value;
            if (hyperlinkText.StartsWith("##"))
            {
                interactionObject["hyperlinkType"] = "HyperlinkUsingInteractionTag";
                interactionObject["hyperlink"] = StiEncodingHelper.Encode(hyperlinkText.Remove(0, 2));
            }
            else if (hyperlinkText.StartsWith("#"))
            {
                interactionObject["hyperlinkType"] = "HyperlinkUsingInteractionBookmark";
                interactionObject["hyperlink"] = StiEncodingHelper.Encode(hyperlinkText.Remove(0, 1));
            }
            else
            {
                interactionObject["hyperlinkType"] = "HyperlinkExternalDocuments";
                interactionObject["hyperlink"] = StiEncodingHelper.Encode(hyperlinkText);
            }

            interactionObject["tag"] = StiEncodingHelper.Encode(interaction.Tag.Value);
            interactionObject["toolTip"] = StiEncodingHelper.Encode(interaction.ToolTip.Value);

            return interactionObject;
        }
        #endregion

        #region CrossTabComponents
        public static Hashtable GetCrossTabFieldsProperties(StiCrossTab crossTab)
        {
            StiCrossTabHelper crossTabHelper = new StiCrossTabHelper(crossTab);
            Hashtable properties = crossTabHelper.GetFieldsPropertiesForJS();
            crossTabHelper.RestorePositions();
            crossTabHelper = null;

            return properties;
        }
        #endregion

        #region Events
        public static Hashtable GetEventsProperty(object object_)
        {
            Hashtable events = new Hashtable();

            string[] eventNames = { "GetExcelValueEvent", "GetCollapsedEvent", "GetTagEvent", "GetToolTipEvent", "GetValueEvent", "ClickEvent", "DoubleClickEvent",
                "GetDrillDownReportEvent", "MouseEnterEvent", "MouseLeaveEvent", "GetBookmarkEvent", "GetHyperlinkEvent", "AfterPrintEvent", "BeforePrintEvent",
                "BeginRenderEvent", "ColumnBeginRenderEvent", "ColumnEndRenderEvent", "EndRenderEvent", "RenderingEvent", "ExportedEvent", "ExportingEvent",
                "PrintedEvent", "PrintingEvent", "ReportCacheProcessingEvent", "GetDataUrlEvent", "GetImageDataEvent", "GetImageURLEvent", "GetBarCodeEvent",
                "GetCheckedEvent", "FillParametersEvent", "GetZipCodeEvent", "GetSummaryExpressionEvent" };

            foreach (string eventName in eventNames)
            {
                var value = StiReportEdit.GetPropertyValue(eventName, object_);
                if (value != null) events[eventName] = StiEncodingHelper.Encode(value as string);
            }

            return events;
        }
        #endregion

        #region SubReportParameters
        public static ArrayList GetSubReportParametersProperty(StiSubReport subReport)
        {
            ArrayList parameters = new ArrayList();

            foreach (StiParameter parameter in subReport.Parameters)
            {
                Hashtable parameterObject = new Hashtable();
                parameterObject["name"] = parameter.Name;
                parameterObject["expression"] = StiEncodingHelper.Encode(parameter.Expression);
                parameters.Add(parameterObject);
            }

            return parameters;
        }
        #endregion                

        #region ShapeType
        public static string GetShapeTypeProperty(StiShape component)
        {
            
            if (component.ShapeType is StiArrowShapeType)
            {
                StiArrowShapeType arrowShapeType = new StiArrowShapeType();
                switch (((StiArrowShapeType)component.ShapeType).Direction)
                {
                    case StiShapeDirection.Up: return "StiArrowShapeTypeUp";
                    case StiShapeDirection.Down: return "StiArrowShapeTypeDown";
                    case StiShapeDirection.Right: return "StiArrowShapeTypeRight";
                    case StiShapeDirection.Left: return "StiArrowShapeTypeLeft";
                }
            }

            return component.ShapeType.GetType().Name;
        }
        #endregion
        #endregion

        #region Set All Properties
        public static void SetAllProperties(StiComponent component, ArrayList props)
        {
            StiReport currReport = component.Report;

            foreach (Hashtable prop in props)
            {
                string propertyName = (string)prop["name"];
                object propertyValue = prop["value"];

                if (propertyName == "ratio") SetPropertyValue(currReport, component is StiZipCode ? "Ratio" : "AspectRatio", component, propertyValue);
                else if (propertyName == "rotation") SetPropertyValue(currReport, "ImageRotation", component, propertyValue);
                else if (propertyName == "imageMultipleFactor") SetPropertyValue(currReport, "MultipleFactor", component, propertyValue);
                else if (propertyName == "subReportPage") SetSubReportPageProperty(component, propertyValue);
                else if (propertyName == "subReportUrl") SetPropertyValue(currReport, "SubReportUrl", component, propertyValue as string != "" ? StiEncodingHelper.DecodeString(propertyValue as string) : null);
                else if (propertyName == "contourColor") SetPropertyValue(currReport, "ContourColor", component, propertyValue);
                else if (propertyName == "size") SetPropertyValue(currReport, "Size", component, propertyValue);
                else if (propertyName == "checkValues") SetPropertyValue(currReport, "Values", component, propertyValue);
                else if (propertyName == "editable") SetPropertyValue(currReport, "Editable", component, propertyValue);
                else if (propertyName == "checkStyleForFalse") SetPropertyValue(currReport, "CheckStyleForFalse", component, propertyValue);
                else if (propertyName == "checkStyleForTrue") SetPropertyValue(currReport, "CheckStyleForTrue", component, propertyValue);
                else if (propertyName == "checked") SetPropertyValue(currReport, "Checked.Value", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "container") SetContainerProperty(component, propertyValue);
                else if (propertyName == "columnWidth") SetPropertyValue(currReport, "ColumnWidth", component, propertyValue);
                else if (propertyName == "columnGaps") SetPropertyValue(currReport, "ColumnGaps", component, propertyValue);
                else if (propertyName == "columns") SetPropertyValue(currReport, "Columns", component, propertyValue);
                else if (propertyName == "columnDirection") SetPropertyValue(currReport, "ColumnDirection", component, propertyValue);
                else if (propertyName == "minRowsInColumn") SetPropertyValue(currReport, "MinRowsInColumn", component, propertyValue);
                else if (propertyName == "shapeBorderColor") SetPropertyValue(currReport, "BorderColor", component, propertyValue);
                else if (propertyName == "shapeBorderStyle") SetPropertyValue(currReport, "Style", component, propertyValue);
                else if (propertyName == "shapeType") SetShapeTypeProperty(component, propertyValue);
                else if (propertyName == "backColor") SetPropertyValue(currReport, "BackColor", component, propertyValue);
                else if (propertyName == "foreColor") SetPropertyValue(currReport, "ForeColor", component, propertyValue);
                else if (propertyName == "autoScale") SetPropertyValue(currReport, "AutoScale", component, propertyValue);
                else if (propertyName == "rightToLeft") SetPropertyValue(currReport, "RightToLeft", component, propertyValue);
                else if (propertyName == "showLabelText") SetPropertyValue(currReport, "ShowLabelText", component, propertyValue);
                else if (propertyName == "showQuietZones") SetPropertyValue(currReport, "ShowQuietZones", component, propertyValue);
                else if (propertyName == "barCodeAngle") SetPropertyValue(currReport, "Angle", component, propertyValue);
                else if (propertyName == "codeType") SetBarCodeTypeProperty(component, propertyValue);
                else if (propertyName == "code") SetPropertyValue(currReport, "Code.Value", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "stretch") SetPropertyValue(currReport, "Stretch", component, propertyValue);
                else if (propertyName == "componentStyle") component.ComponentStyle = propertyValue as string != "[None]" ? propertyValue as string : string.Empty;
                else if (propertyName == "unitWidth") SetPropertyValue(currReport, component is IStiDashboard ? "Width" : "PageWidth", component, propertyValue);
                else if (propertyName == "unitHeight") SetPropertyValue(currReport, component is IStiDashboard ? "Height" : "PageHeight", component, propertyValue);
                else if (propertyName == "orientation") SetPropertyValue(currReport, "Orientation", component, propertyValue);
                else if (propertyName == "unitMargins") SetMarginsProperty(component, propertyValue);
                else if (propertyName == "vertAlignment") SetPropertyValue(currReport, "VertAlignment", component, propertyValue);
                else if (propertyName == "horAlignment") SetPropertyValue(currReport, "HorAlignment", component, propertyValue);
                else if (propertyName == "textAngle") SetPropertyValue(currReport, "Angle", component, propertyValue);
                else if (propertyName == "textMargins") SetPropertyValue(currReport, "Margins", component, propertyValue);
                else if (propertyName == "maxNumberOfLines") SetPropertyValue(currReport, "MaxNumberOfLines", component, propertyValue);
                else if (propertyName == "cellWidth") SetPropertyValue(currReport, "CellWidth", component, propertyValue);
                else if (propertyName == "cellHeight") SetPropertyValue(currReport, "CellHeight", component, propertyValue);
                else if (propertyName == "horizontalSpacing") SetPropertyValue(currReport, "HorSpacing", component, propertyValue);
                else if (propertyName == "verticalSpacing") SetPropertyValue(currReport, "VertSpacing", component, propertyValue);
                else if (propertyName == "wordWrap") SetPropertyValue(currReport, "WordWrap", component, propertyValue);
                else if (propertyName == "font") SetPropertyValue(currReport, "Font", component, propertyValue);
                else if (propertyName == "textBrush") SetPropertyValue(currReport, "TextBrush", component, propertyValue);
                else if (propertyName == "text") SetTextProperty(component, propertyValue);
                else if (propertyName == "textFormat") SetTextFormatProperty(component, propertyValue);
                else if (propertyName == "editableText") SetPropertyValue(currReport, "Editable", component, propertyValue);
                else if (propertyName == "hideZeros") SetPropertyValue(currReport, "HideZeros", component, propertyValue);
                else if (propertyName == "continuousText") SetPropertyValue(currReport, "ContinuousText", component, propertyValue);
                else if (propertyName == "onlyText") SetPropertyValue(currReport, "OnlyText", component, propertyValue);
                else if (propertyName == "allowHtmlTags") SetPropertyValue(currReport, "AllowHtmlTags", component, propertyValue);
                else if (propertyName == "border") SetPropertyValue(currReport, "Border", component, propertyValue);
                else if (propertyName == "brush") SetPropertyValue(currReport, "Brush", component, propertyValue);
                else if (propertyName == "canGrow") SetPropertyValue(currReport, "CanGrow", component, propertyValue);
                else if (propertyName == "canShrink") SetPropertyValue(currReport, "CanShrink", component, propertyValue);
                else if (propertyName == "canBreak") SetPropertyValue(currReport, "CanBreak", component, propertyValue);
                else if (propertyName == "autoWidth") SetPropertyValue(currReport, "AutoWidth", component, propertyValue);
                else if (propertyName == "growToHeight") SetPropertyValue(currReport, "GrowToHeight", component, propertyValue);
                else if (propertyName == "enabled") SetPropertyValue(currReport, "Enabled", component, propertyValue);
                else if (propertyName == "printable") SetPropertyValue(currReport, "Printable", component, propertyValue);
                else if (propertyName == "printOn") SetPropertyValue(currReport, "PrintOn", component, propertyValue);
                else if (propertyName == "paperSize") SetPropertyValue(currReport, "PaperSize", component, propertyValue);
                else if (propertyName == "waterMarkRatio") SetPropertyValue(currReport, "Watermark.AspectRatio", component, propertyValue);
                else if (propertyName == "waterMarkRightToLeft") SetPropertyValue(currReport, "Watermark.RightToLeft", component, propertyValue);
                else if (propertyName == "waterMarkEnabled") SetPropertyValue(currReport, "Watermark.Enabled", component, propertyValue);
                else if (propertyName == "waterMarkEnabledExpression") SetPropertyValue(currReport, "Watermark.EnabledExpression", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "waterMarkAngle") SetPropertyValue(currReport, "Watermark.Angle", component, propertyValue);
                else if (propertyName == "waterMarkText") SetPropertyValue(currReport, "Watermark.Text", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "waterMarkFont") SetPropertyValue(currReport, "Watermark.Font", component, propertyValue);
                else if (propertyName == "waterMarkTextBrush") SetPropertyValue(currReport, "Watermark.TextBrush", component, propertyValue);
                else if (propertyName == "waterMarkTextBehind") SetPropertyValue(currReport, "Watermark.ShowBehind", component, propertyValue);
                else if (propertyName == "waterMarkImageBehind") SetPropertyValue(currReport, "Watermark.ShowImageBehind", component, propertyValue);
                else if (propertyName == "waterMarkImageAlign") SetPropertyValue(currReport, "Watermark.ImageAlignment", component, propertyValue);
                else if (propertyName == "waterMarkMultipleFactor") SetPropertyValue(currReport, "Watermark.ImageMultipleFactor", component, propertyValue);
                else if (propertyName == "waterMarkStretch") SetPropertyValue(currReport, "Watermark.ImageStretch", component, propertyValue);
                else if (propertyName == "waterMarkTiling") SetPropertyValue(currReport, "Watermark.ImageTiling", component, propertyValue);
                else if (propertyName == "waterMarkTransparency") SetPropertyValue(currReport, "Watermark.ImageTransparency", component, propertyValue);
                else if (propertyName == "newPageBefore") SetPropertyValue(currReport, "NewPageBefore", component, propertyValue);
                else if (propertyName == "newPageAfter") SetPropertyValue(currReport, "NewPageAfter", component, propertyValue);
                else if (propertyName == "newColumnBefore") SetPropertyValue(currReport, "NewColumnBefore", component, propertyValue);
                else if (propertyName == "newColumnAfter") SetPropertyValue(currReport, "NewColumnAfter", component, propertyValue);
                else if (propertyName == "skipFirst") SetPropertyValue(currReport, "SkipFirst", component, propertyValue);
                else if (propertyName == "condition") SetConditionProperty(component, propertyValue);
                else if (propertyName == "sortDirection") SetPropertyValue(currReport, "SortDirection", component, propertyValue);
                else if (propertyName == "summarySortDirection") SetPropertyValue(currReport, "SummarySortDirection", component, propertyValue);
                else if (propertyName == "summaryExpression") SetPropertyValue(currReport, "SummaryExpression.Value", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "summaryType") SetPropertyValue(currReport, "SummaryType", component, propertyValue);
                else if (propertyName == "sortData") SetSortDataProperty(component, propertyValue);
                else if (propertyName == "dataSource") SetDataSourceProperty(component, propertyValue);
                else if (propertyName == "dataRelation") SetDataRelationProperty(component, propertyValue);
                else if (propertyName == "masterComponent") SetMasterComponentProperty(component, propertyValue);
                else if (propertyName == "businessObject") SetBusinessObjectProperty(component, propertyValue);
                else if (propertyName == "countData") SetPropertyValue(currReport, "CountData", component, propertyValue);
                else if (propertyName == "filterEngine") SetPropertyValue(currReport, "FilterEngine", component, propertyValue);
                else if (propertyName == "filterData") SetFilterDataProperty(component, propertyValue);
                else if (propertyName == "filterOn") SetFilterOnProperty(component, propertyValue);
                else if (propertyName == "filterMode") SetFilterModeProperty(component, propertyValue);
                else if (propertyName == "imageDataColumn") SetPropertyValue(currReport, "DataColumn", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "imageSrc") SetImageProperty(component, propertyValue as string);
                else if (propertyName == "imageUrl") SetPropertyValue(currReport, "ImageURL.Value", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "imageFile") SetPropertyValue(currReport, "File", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "imageData") SetPropertyValue(currReport, "ImageData.Value", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "watermarkImageSrc") SetWatermarkImageProperty(component as StiPage, propertyValue as string);
                else if (propertyName == "watermarkImageHyperlink") SetPropertyValue(currReport, "Watermark.ImageHyperlink", component, propertyValue);
                else if (propertyName == "stopBeforePrint") SetPropertyValue(currReport, "StopBeforePrint", component, propertyValue);
                else if (propertyName == "titleBeforeHeader") SetPropertyValue(currReport, "TitleBeforeHeader", component, propertyValue);
                else if (propertyName == "crossTabEmptyValue") SetPropertyValue(currReport, "EmptyValue", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "crossTabHorAlign") SetPropertyValue(currReport, "HorAlignment", component, propertyValue);
                else if (propertyName == "printIfEmpty") SetPropertyValue(currReport, "PrintIfEmpty", component, propertyValue);
                else if (propertyName == "printOnAllPages") SetPropertyValue(currReport, "PrintOnAllPages", component, propertyValue);
                else if (propertyName == "crossTabWrap") SetPropertyValue(currReport, "Wrap", component, propertyValue);
                else if (propertyName == "crossTabWrapGap") SetPropertyValue(currReport, "WrapGap", component, propertyValue);
                else if (propertyName == "crossTabPrintTitle") SetPropertyValue(currReport, "PrintTitleOnAllPages", component, propertyValue);
                else if (propertyName == "keyDataColumn") SetPropertyValue(currReport, "KeyDataColumn", component, propertyValue);
                else if (propertyName == "masterKeyDataColumn") SetPropertyValue(currReport, "MasterKeyDataColumn", component, propertyValue);
                else if (propertyName == "parentValue") SetPropertyValue(currReport, "ParentValue", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "indent") SetPropertyValue(currReport, "Indent", component, propertyValue);
                else if (propertyName == "headers") SetPropertyValue(currReport, "Headers", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "footers") SetPropertyValue(currReport, "Footers", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "aliasName") component.Alias = StiEncodingHelper.DecodeString(propertyValue as string);
                else if (propertyName == "oddStyle") SetPropertyValue(currReport, "OddStyle", component, (propertyValue as string != "[None]") ? propertyValue as string : string.Empty);
                else if (propertyName == "evenStyle") SetPropertyValue(currReport, "EvenStyle", component, (propertyValue as string != "[None]") ? propertyValue as string : string.Empty);
                else if (propertyName == "largeHeight") SetPropertyValue(currReport, "LargeHeight", component, propertyValue);
                else if (propertyName == "largeHeightFactor") SetPropertyValue(currReport, "LargeHeightFactor", component, propertyValue);
                else if (propertyName == "calcInvisible") SetPropertyValue(currReport, "CalcInvisible", component, propertyValue);
                else if (propertyName == "richText") SetRichTextProperty(component, propertyValue);
                else if (propertyName == "richTextUrl") SetPropertyValue(currReport, "DataUrl.Value", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "richTextDataColumn") SetPropertyValue(currReport, "DataColumn", component, StiEncodingHelper.DecodeString(propertyValue as string));
                else if (propertyName == "restrictions") SetRestrictionsProperty(component, propertyValue);
                else if (propertyName == "conditions") SetConditionsProperty(component, propertyValue, currReport);
                else if (propertyName == "trimming") SetPropertyValue(currReport, "TextOptions.Trimming", component, propertyValue);
                else if (propertyName == "textOptionsRightToLeft") SetPropertyValue(currReport, "TextOptions.RightToLeft", component, propertyValue);
                else if (propertyName == "processAt") SetPropertyValue(currReport, "ProcessAt", component, propertyValue);
                else if (propertyName == "processingDuplicates") SetPropertyValue(currReport, "ProcessingDuplicates", component, propertyValue);
                else if (propertyName == "shrinkFontToFit") SetPropertyValue(currReport, "ShrinkFontToFit", component, propertyValue);
                else if (propertyName == "shrinkFontToFitMinimumSize") SetPropertyValue(currReport, "ShrinkFontToFitMinimumSize", component, propertyValue);
                else if (propertyName == "textType") SetPropertyValue(currReport, "Type", component, propertyValue);
                else if (propertyName == "resetPageNumber") SetPropertyValue(currReport, "ResetPageNumber", component, propertyValue);
                else if (propertyName == "printOnPreviousPage") SetPropertyValue(currReport, "PrintOnPreviousPage", component, propertyValue);
                else if (propertyName == "printHeadersFootersFromPreviousPage") SetPropertyValue(currReport, "PrintHeadersFootersFromPreviousPage", component, propertyValue);
                else if (propertyName == "printAtBottom") SetPropertyValue(currReport, "PrintAtBottom", component, propertyValue);
                else if (propertyName == "printIfDetailEmpty") SetPropertyValue(currReport, "PrintIfDetailEmpty", component, propertyValue);
                else if (propertyName == "keepGroupTogether") SetPropertyValue(currReport, "KeepGroupTogether", component, propertyValue);
                else if (propertyName == "keepGroupHeaderTogether") SetPropertyValue(currReport, "KeepGroupHeaderTogether", component, propertyValue);
                else if (propertyName == "keepGroupFooterTogether") SetPropertyValue(currReport, "KeepGroupFooterTogether", component, propertyValue);
                else if (propertyName == "keepHeaderTogether") SetPropertyValue(currReport, "KeepHeaderTogether", component, propertyValue);
                else if (propertyName == "keepFooterTogether") SetPropertyValue(currReport, "KeepFooterTogether", component, propertyValue);
                else if (propertyName == "keepDetailsTogether") SetPropertyValue(currReport, "KeepDetailsTogether", component, propertyValue);
                else if (propertyName == "interaction") SetInteractionProperty(component, propertyValue);
                else if (propertyName == "chartStyle") SetChartStyleProperty(component, propertyValue);
                else if (propertyName == "gaugeStyle") SetGaugeStyleProperty(component, propertyValue);
                else if (propertyName == "mapStyle") SetMapStyleProperty(component, propertyValue);
                else if (propertyName == "crossTabStyle") SetCrossTabStyleProperty(component, propertyValue);
                else if (propertyName == "shiftMode") SetShiftModeProperty(component, propertyValue);
                else if (propertyName == "cellDockStyle") SetPropertyValue(currReport, "CellDockStyle", component, propertyValue);
                else if (propertyName == "fixedWidth") SetPropertyValue(currReport, "FixedWidth", component, propertyValue);
                else if (propertyName == "tableAutoWidth") SetPropertyValue(currReport, "AutoWidth", component, propertyValue);
                else if (propertyName == "autoWidthType") SetPropertyValue(currReport, "AutoWidthType", component, propertyValue);
                else if (propertyName == "headerRowsCount") SetPropertyValue(currReport, "HeaderRowsCount", component, propertyValue);
                else if (propertyName == "footerRowsCount") SetPropertyValue(currReport, "FooterRowsCount", component, propertyValue);
                else if (propertyName == "tableRightToLeft") SetPropertyValue(currReport, "RightToLeft", component, propertyValue);
                else if (propertyName == "dockableTable") SetPropertyValue(currReport, "DockableTable", component, propertyValue);
                else if (propertyName == "headerPrintOn") SetPropertyValue(currReport, "HeaderPrintOn", component, propertyValue);
                else if (propertyName == "headerCanGrow") SetPropertyValue(currReport, "HeaderCanGrow", component, propertyValue);
                else if (propertyName == "headerCanShrink") SetPropertyValue(currReport, "HeaderCanShrink", component, propertyValue);
                else if (propertyName == "headerCanBreak") SetPropertyValue(currReport, "HeaderCanBreak", component, propertyValue);
                else if (propertyName == "headerPrintAtBottom") SetPropertyValue(currReport, "HeaderPrintAtBottom", component, propertyValue);
                else if (propertyName == "headerPrintIfEmpty") SetPropertyValue(currReport, "HeaderPrintIfEmpty", component, propertyValue);
                else if (propertyName == "headerPrintOnAllPages") SetPropertyValue(currReport, "HeaderPrintOnAllPages", component, propertyValue);
                else if (propertyName == "headerPrintOnEvenOddPages") SetPropertyValue(currReport, "HeaderPrintOnEvenOddPages", component, propertyValue);
                else if (propertyName == "footerPrintOn") SetPropertyValue(currReport, "FooterPrintOn", component, propertyValue);
                else if (propertyName == "footerCanGrow") SetPropertyValue(currReport, "FooterCanGrow", component, propertyValue);
                else if (propertyName == "footerCanShrink") SetPropertyValue(currReport, "FooterCanShrink", component, propertyValue);
                else if (propertyName == "footerCanBreak") SetPropertyValue(currReport, "FooterCanBreak", component, propertyValue);
                else if (propertyName == "footerPrintAtBottom") SetPropertyValue(currReport, "FooterPrintAtBottom", component, propertyValue);
                else if (propertyName == "footerPrintIfEmpty") SetPropertyValue(currReport, "FooterPrintIfEmpty", component, propertyValue);
                else if (propertyName == "footerPrintOnAllPages") SetPropertyValue(currReport, "FooterPrintOnAllPages", component, propertyValue);
                else if (propertyName == "footerPrintOnEvenOddPages") SetPropertyValue(currReport, "FooterPrintOnEvenOddPages", component, propertyValue);
                else if (propertyName == "locked") SetPropertyValue(currReport, "Locked", component, propertyValue);
                else if (propertyName == "linked") SetPropertyValue(currReport, "Linked", component, propertyValue);
                else if (propertyName == "dockStyle") SetPropertyValue(currReport, "DockStyle", component, propertyValue);
                else if (propertyName == "mirrorMargins") SetPropertyValue(currReport, "MirrorMargins", component, propertyValue);
                else if (propertyName == "subReportParameters") SetSubReportParametersProperty(component, propertyValue);
                else if (propertyName == "segmentPerWidth") SetPropertyValue(currReport, "SegmentPerWidth", component, propertyValue);
                else if (propertyName == "segmentPerHeight") SetPropertyValue(currReport, "SegmentPerHeight", component, propertyValue);
                else if (propertyName == "anchor") SetAnchorProperty(component, propertyValue);
                else if (propertyName == "minSize") SetPropertyValue(currReport, "MinSize", component, propertyValue);
                else if (propertyName == "maxSize") SetPropertyValue(currReport, "MaxSize", component, propertyValue);
                else if (propertyName == "minHeight") SetPropertyValue(currReport, "MinHeight", component, propertyValue);
                else if (propertyName == "maxHeight") SetPropertyValue(currReport, "MaxHeight", component, propertyValue);
                else if (propertyName == "minWidth") SetPropertyValue(currReport, "MinWidth", component, propertyValue);
                else if (propertyName == "maxWidth") SetPropertyValue(currReport, "MaxWidth", component, propertyValue);
                else if (propertyName == "color") SetPropertyValue(currReport, "Color", component, propertyValue);
                else if (propertyName == "style") SetPropertyValue(currReport, "Style", component, propertyValue);
                else if (propertyName == "round") SetPropertyValue(currReport, "Round", component, propertyValue);
                else if (propertyName == "leftSide") SetPropertyValue(currReport, "LeftSide", component, propertyValue);
                else if (propertyName == "rightSide") SetPropertyValue(currReport, "RightSide", component, propertyValue);
                else if (propertyName == "topSide") SetPropertyValue(currReport, "TopSide", component, propertyValue);
                else if (propertyName == "bottomSide") SetPropertyValue(currReport, "BottomSide", component, propertyValue);
                else if (propertyName == "startCapColor") SetPropertyValue(currReport, "StartCap.Color", component, propertyValue);
                else if (propertyName == "startCapFill") SetPropertyValue(currReport, "StartCap.Fill", component, propertyValue);
                else if (propertyName == "startCapWidth") SetPropertyValue(currReport, "StartCap.Width", component, propertyValue);
                else if (propertyName == "startCapHeight") SetPropertyValue(currReport, "StartCap.Height", component, propertyValue);
                else if (propertyName == "startCapStyle") SetPropertyValue(currReport, "StartCap.Style", component, propertyValue);
                else if (propertyName == "useParentStyles") SetPropertyValue(currReport, "UseParentStyles", component, propertyValue);
                else if (propertyName == "unlimitedHeight") SetPropertyValue(currReport, "UnlimitedHeight", component, propertyValue);
                else if (propertyName == "unlimitedBreakable") SetPropertyValue(currReport, "UnlimitedBreakable", component, propertyValue);
                else if (propertyName == "numberOfCopies") SetPropertyValue(currReport, "NumberOfCopies", component, propertyValue);
                else if (propertyName == "breakIfLessThan") SetPropertyValue(currReport, "BreakIfLessThan", component, propertyValue);
                else if (propertyName == "renderTo") SetPropertyValue(currReport, "RenderTo", component, propertyValue);
                else if (propertyName == "globalizedName") SetPropertyValue(currReport, "GlobalizedName", component, propertyValue);
                else if (propertyName == "excelValue") SetExcelValueProperty(component, propertyValue);
                else if (propertyName == "exportAsImage") SetPropertyValue(currReport, "ExportAsImage", component, propertyValue);
                else if (propertyName == "lineSpacing") SetPropertyValue(currReport, "LineSpacing", component, propertyValue);
                else if (propertyName == "elementStyle") SetPropertyValue(currReport, "Style", component, propertyValue);
            }
        }
        #endregion

        #region Set Property 
        #region SubReportPage
        public static void SetSubReportPageProperty(object component, object propertyValue)
        {
            StiSubReport comp = (StiSubReport)component;
            StiReport report = comp.Report;
            if (propertyValue as string != "[Not Assigned]")
                comp.SubReportPage = report.Pages[propertyValue as string];
            else
                comp.SubReportPage = null;
        }
        #endregion

        #region Container
        public static void SetContainerProperty(object component, object propertyValue)
        {
            StiClone comp = (StiClone)component;

            if (propertyValue as string != "[Not Assigned]")
            {
                StiReport report = comp.Report;
                comp.Container = (StiContainer)report.GetComponentByName(propertyValue as string);
            }
            else
                comp.Container = null;
        }
        #endregion       

        #region ShapeType
        public static void SetShapeTypeProperty(object component, object propValue)
        {
            var comp = component as StiShape;
            if (comp == null) return;
            string shapeType = propValue as string;

            if (shapeType.StartsWith("StiArrowShapeType"))
            {
                StiArrowShapeType arrowShapeType = new StiArrowShapeType();
                switch (shapeType)
                {
                    case "StiArrowShapeTypeUp": arrowShapeType.Direction = StiShapeDirection.Up; break;
                    case "StiArrowShapeTypeDown": arrowShapeType.Direction = StiShapeDirection.Down; break;
                    case "StiArrowShapeTypeRight": arrowShapeType.Direction = StiShapeDirection.Right; break;
                    case "StiArrowShapeTypeLeft": arrowShapeType.Direction = StiShapeDirection.Left; break;
                }
                comp.ShapeType = arrowShapeType;
            }
            else if (shapeType == "StiDiagonalDownLineShapeType") comp.ShapeType = new StiDiagonalDownLineShapeType();
            else if (shapeType == "StiDiagonalUpLineShapeType") comp.ShapeType = new StiDiagonalUpLineShapeType();
            else if (shapeType == "StiHorizontalLineShapeType") comp.ShapeType = new StiHorizontalLineShapeType();
            else if (shapeType == "StiLeftAndRightLineShapeType") comp.ShapeType = new StiLeftAndRightLineShapeType();
            else if (shapeType == "StiOvalShapeType") comp.ShapeType = new StiOvalShapeType();
            else if (shapeType == "StiRectangleShapeType") comp.ShapeType = new StiRectangleShapeType();
            else if (shapeType == "StiRoundedRectangleShapeType") comp.ShapeType = new StiRoundedRectangleShapeType();
            else if (shapeType == "StiTopAndBottomLineShapeType") comp.ShapeType = new StiTopAndBottomLineShapeType();
            else if (shapeType == "StiTriangleShapeType") comp.ShapeType = new StiTriangleShapeType();
            else if (shapeType == "StiVerticalLineShapeType") comp.ShapeType = new StiVerticalLineShapeType();
            else if (shapeType == "StiComplexArrowShapeType") comp.ShapeType = new StiComplexArrowShapeType();
            else if (shapeType == "StiBentArrowShapeType") comp.ShapeType = new StiBentArrowShapeType();
            else if (shapeType == "StiChevronShapeType") comp.ShapeType = new StiChevronShapeType();
            else if (shapeType == "StiDivisionShapeType") comp.ShapeType = new StiDivisionShapeType();
            else if (shapeType == "StiEqualShapeType") comp.ShapeType = new StiEqualShapeType();
            else if (shapeType == "StiFlowchartCardShapeType") comp.ShapeType = new StiFlowchartCardShapeType();
            else if (shapeType == "StiFlowchartCollateShapeType") comp.ShapeType = new StiFlowchartCollateShapeType();
            else if (shapeType == "StiFlowchartDecisionShapeType") comp.ShapeType = new StiFlowchartDecisionShapeType();
            else if (shapeType == "StiFlowchartManualInputShapeType") comp.ShapeType = new StiFlowchartManualInputShapeType();
            else if (shapeType == "StiFlowchartOffPageConnectorShapeType") comp.ShapeType = new StiFlowchartOffPageConnectorShapeType();
            else if (shapeType == "StiFlowchartPreparationShapeType") comp.ShapeType = new StiFlowchartPreparationShapeType();
            else if (shapeType == "StiFlowchartSortShapeType") comp.ShapeType = new StiFlowchartSortShapeType();
            else if (shapeType == "StiFrameShapeType") comp.ShapeType = new StiFrameShapeType();
            else if (shapeType == "StiMinusShapeType") comp.ShapeType = new StiMinusShapeType();
            else if (shapeType == "StiMultiplyShapeType") comp.ShapeType = new StiMultiplyShapeType();
            else if (shapeType == "StiParallelogramShapeType") comp.ShapeType = new StiParallelogramShapeType();
            else if (shapeType == "StiPlusShapeType") comp.ShapeType = new StiPlusShapeType();
            else if (shapeType == "StiRegularPentagonShapeType") comp.ShapeType = new StiRegularPentagonShapeType();
            else if (shapeType == "StiTrapezoidShapeType") comp.ShapeType = new StiTrapezoidShapeType();
            else if (shapeType == "StiOctagonShapeType") comp.ShapeType = new StiOctagonShapeType();
            else if (shapeType == "StiSnipSameSideCornerRectangleShapeType") comp.ShapeType = new StiSnipSameSideCornerRectangleShapeType();
            else if (shapeType == "StiSnipDiagonalSideCornerRectangleShapeType") comp.ShapeType = new StiSnipDiagonalSideCornerRectangleShapeType();
        }
        #endregion

        #region BarCodeType
        public static void SetBarCodeTypeProperty(object component, object propValue)
        {
            StiBarCode comp = (StiBarCode)component;
            string barCodeType = propValue as string;
            if (barCodeType == "StiAustraliaPost4StateBarCodeType") comp.BarCodeType = new StiAustraliaPost4StateBarCodeType();
            else if (barCodeType == "StiCode11BarCodeType") comp.BarCodeType = new StiCode11BarCodeType();
            else if (barCodeType == "StiCode128aBarCodeType") comp.BarCodeType = new StiCode128aBarCodeType();
            else if (barCodeType == "StiCode128bBarCodeType") comp.BarCodeType = new StiCode128bBarCodeType();
            else if (barCodeType == "StiCode128cBarCodeType") comp.BarCodeType = new StiCode128cBarCodeType();
            else if (barCodeType == "StiCode128AutoBarCodeType") comp.BarCodeType = new StiCode128AutoBarCodeType();
            else if (barCodeType == "StiCode39BarCodeType") comp.BarCodeType = new StiCode39BarCodeType();
            else if (barCodeType == "StiCode39ExtBarCodeType") comp.BarCodeType = new StiCode39ExtBarCodeType();
            else if (barCodeType == "StiCode93BarCodeType") comp.BarCodeType = new StiCode93BarCodeType();
            else if (barCodeType == "StiCode93ExtBarCodeType") comp.BarCodeType = new StiCode93ExtBarCodeType();
            else if (barCodeType == "StiCodabarBarCodeType") comp.BarCodeType = new StiCodabarBarCodeType();
            else if (barCodeType == "StiDataMatrixBarCodeType") comp.BarCodeType = new StiDataMatrixBarCodeType();
            else if (barCodeType == "StiEAN128aBarCodeType") comp.BarCodeType = new StiEAN128aBarCodeType();
            else if (barCodeType == "StiEAN128bBarCodeType") comp.BarCodeType = new StiEAN128bBarCodeType();
            else if (barCodeType == "StiEAN128cBarCodeType") comp.BarCodeType = new StiEAN128cBarCodeType();
            else if (barCodeType == "StiEAN128AutoBarCodeType") comp.BarCodeType = new StiEAN128AutoBarCodeType();
            else if (barCodeType == "StiEAN13BarCodeType") comp.BarCodeType = new StiEAN13BarCodeType();
            else if (barCodeType == "StiEAN8BarCodeType") comp.BarCodeType = new StiEAN8BarCodeType();
            else if (barCodeType == "StiGS1_128BarCodeType") comp.BarCodeType = new StiGS1_128BarCodeType();
            else if (barCodeType == "StiFIMBarCodeType") comp.BarCodeType = new StiFIMBarCodeType();
            else if (barCodeType == "StiIsbn10BarCodeType") comp.BarCodeType = new StiIsbn10BarCodeType();
            else if (barCodeType == "StiIsbn13BarCodeType") comp.BarCodeType = new StiIsbn13BarCodeType();
            else if (barCodeType == "StiITF14BarCodeType") comp.BarCodeType = new StiITF14BarCodeType();
            else if (barCodeType == "StiJan13BarCodeType") comp.BarCodeType = new StiJan13BarCodeType();
            else if (barCodeType == "StiJan8BarCodeType") comp.BarCodeType = new StiJan8BarCodeType();
            else if (barCodeType == "StiMsiBarCodeType") comp.BarCodeType = new StiMsiBarCodeType();
            else if (barCodeType == "StiPdf417BarCodeType") comp.BarCodeType = new StiPdf417BarCodeType();
            else if (barCodeType == "StiPharmacodeBarCodeType") comp.BarCodeType = new StiPharmacodeBarCodeType();
            else if (barCodeType == "StiMaxicodeBarCodeType") comp.BarCodeType = new StiMaxicodeBarCodeType();
            else if (barCodeType == "StiPlesseyBarCodeType") comp.BarCodeType = new StiPlesseyBarCodeType();
            else if (barCodeType == "StiPostnetBarCodeType") comp.BarCodeType = new StiPostnetBarCodeType();
            else if (barCodeType == "StiQRCodeBarCodeType") comp.BarCodeType = new StiQRCodeBarCodeType();
            else if (barCodeType == "StiDutchKIXBarCodeType") comp.BarCodeType = new StiDutchKIXBarCodeType();
            else if (barCodeType == "StiRoyalMail4StateBarCodeType") comp.BarCodeType = new StiRoyalMail4StateBarCodeType();
            else if (barCodeType == "StiSSCC18BarCodeType") comp.BarCodeType = new StiSSCC18BarCodeType();
            else if (barCodeType == "StiUpcABarCodeType") comp.BarCodeType = new StiUpcABarCodeType();
            else if (barCodeType == "StiUpcEBarCodeType") comp.BarCodeType = new StiUpcEBarCodeType();
            else if (barCodeType == "StiUpcSup2BarCodeType") comp.BarCodeType = new StiUpcSup2BarCodeType();
            else if (barCodeType == "StiUpcSup5BarCodeType") comp.BarCodeType = new StiUpcSup5BarCodeType();
            else if (barCodeType == "StiInterleaved2of5BarCodeType") comp.BarCodeType = new StiInterleaved2of5BarCodeType();
            else if (barCodeType == "StiStandard2of5BarCodeType") comp.BarCodeType = new StiStandard2of5BarCodeType();
        }
        #endregion      

        #region PageMargins
        public static void SetMarginsProperty(object component, object propertyValue)
        {
            StiPage comp = (StiPage)component;
            string[] margins = ((string)propertyValue).Split('!');

            comp.Margins.Left = StrToDouble(margins[0]);
            comp.Margins.Top = StrToDouble(margins[1]);
            comp.Margins.Right = StrToDouble(margins[2]);
            comp.Margins.Bottom = StrToDouble(margins[3]);
        }
        #endregion

        #region Text
        public static void SetTextProperty(object component, object propertyValue)
        {
            propertyValue = StiEncodingHelper.DecodeString(propertyValue as string).Replace("\n", "\r\n");
            StiExpression text = new StiExpression(propertyValue as string);
            PropertyInfo pi = component.GetType().GetProperty("Text");

            if (pi != null) pi.SetValue(component, text, null);
        }
        #endregion

        #region RichText
        public static void SetRichTextProperty(object component, object propValue)
        {
            string propertyValue = StiEncodingHelper.DecodeString(propValue as string);
            PropertyInfo pi = component.GetType().GetProperty("RtfText");
            if (pi != null)
            {
                if (!StiHyperlinkProcessor.IsServerHyperlink(propertyValue))
                {
                    HtmlToRtfConverter htmlToRtfConverter = new HtmlToRtfConverter();
                    string richText = htmlToRtfConverter.ConvertHtmlToRtf(propertyValue);
                    pi.SetValue(component, richText, null);
                }
                else
                {
                    pi.SetValue(component, string.Empty, null);
                    StiExpression text = new StiExpression(propertyValue);
                    PropertyInfo pi2 = component.GetType().GetProperty("Text");
                    if (pi2 != null) pi2.SetValue(component, text, null);
                }
            }
        }
        #endregion

        #region DataURL
        public static void SetRichTextDataUrlProperty(object component, string propertyValue)
        {
            StiRichText comp = (StiRichText)component;
            string dataUrl = StiEncodingHelper.DecodeString(propertyValue);
            StiDataUrlExpression url = new StiDataUrlExpression();
            url.Value = dataUrl;
            comp.DataUrl = url;
        }
        #endregion

        #region TextFormat
        public static void SetTextFormatProperty(object component, object propertyValue)
        {
            PropertyInfo pi = component.GetType().GetProperty("TextFormat");
            if (pi != null) pi.SetValue(component, StiTextFormatHelper.GetFormatService((Hashtable)propertyValue), null);
        }
        #endregion         

        #region Condition
        public static void SetConditionProperty(object component, object propValue)
        {
            string propertyValue = StiEncodingHelper.DecodeString(propValue as string);
            StiGroupHeaderBand comp = (StiGroupHeaderBand)component;
            StiGroupConditionExpression condExpr = new StiGroupConditionExpression(propertyValue);
            comp.Condition = condExpr;
        }
        private static string GetCorrectName(string value)
        {
            string result = string.Empty;
            if (value == "") return "";

            if (value.StartsWith("{") && value.EndsWith("}"))
            {
                value = value.Substring(1, value.Length - 2);

                string[] valueArray = value.Split('.');
                for (int i = 0; i < valueArray.Length; i++)
                {
                    if (result != "") result += ".";
                    result += StiNameValidator.CorrectName(valueArray[i]);
                }

                return "{" + result + "}";
            }
            return value;
        }
        #endregion       

        #region DataSource
        public static void SetDataSourceProperty(object component, object propValue)
        {
            StiComponent comp = component as StiComponent;
            string propertyValue = propValue as string == "[Not Assigned]" ? string.Empty : propValue as string;
            PropertyInfo pi = component.GetType().GetProperty("DataSourceName");
            if (pi != null)
            {
                string oldDataSourceName = (string)pi.GetValue(component, null);
                if (oldDataSourceName != propertyValue)
                    SetSortDataProperty(component, string.Empty);
                pi.SetValue(component, propertyValue, null);
            }
        }
        #endregion

        #region DataRelation
        public static void SetDataRelationProperty(object component, object propValue)
        {
            string propertyValue = propValue as string == "[Not Assigned]" ? null : propValue as string;
            PropertyInfo pi = component.GetType().GetProperty("DataRelationName");
            if (pi != null) pi.SetValue(component, propertyValue, null);
        }
        #endregion

        #region MasterComponent
        public static void SetMasterComponentProperty(object component, object propertyValue)
        {
            StiComponent comp = (StiComponent)component;
            StiComponent masterComponent = (propertyValue as string != "[Not Assigned]") ? comp.Report.GetComponentByName(propertyValue as string) : null;

            PropertyInfo pi = component.GetType().GetProperty("MasterComponent");
            if (pi != null) pi.SetValue(component, masterComponent, null);
        }
        #endregion

        #region BusinessObject
        public static void SetBusinessObjectProperty(object component, object propertyValue)
        {
            StiComponent comp = (StiComponent)component;
            ArrayList nameArray = new ArrayList();
            nameArray.AddRange(((string)propertyValue).Split('.'));
            nameArray.Reverse();
            StiBusinessObject businessObject = ((string)propertyValue != "[Not Assigned]")
                ? StiDictionaryHelper.GetBusinessObjectByFullName(comp.Report, nameArray) : null;
            PropertyInfo pi = component.GetType().GetProperty("BusinessObjectGuid");
            if (pi != null) pi.SetValue(component, businessObject != null ? businessObject.Guid : string.Empty, null);
        }
        #endregion 

        #region Sort
        public static void SetSortDataProperty(object object_, ArrayList sortArray)
        {
            List<string> result = new List<string>();
            for (int i = 0; i < sortArray.Count; i++)
            {
                Hashtable oneSort = (Hashtable)sortArray[i];
                result = GetSortArray(object_, oneSort, result);
            }

            PropertyInfo pi = object_.GetType().GetProperty("Sort");
            if (pi != null) pi.SetValue(object_, result.ToArray(), null);
        }

        public static void SetSortDataProperty(object object_, object propertyValue)
        {
            List<string> result = new List<string>();

            if (!string.IsNullOrEmpty(propertyValue as string))
            {
                ArrayList sortArray = (ArrayList)JSON.Decode(StiEncodingHelper.DecodeString(propertyValue as string));

                for (int i = 0; i < sortArray.Count; i++)
                {
                    Hashtable oneSort = (Hashtable)sortArray[i];
                    result = GetSortArray(object_, oneSort, result);
                }
            }

            PropertyInfo pi = object_.GetType().GetProperty("Sort");
            if (pi != null) pi.SetValue(object_, result.ToArray(), null);
        }

        private static List<string> GetSortArray(object object_, Hashtable sort, List<string> result)
        {
            StiDataSource dataSource = null;
            StiBusinessObject businessObject = null;

            if (object_ is StiVirtualSource)
            {
                dataSource = ((StiVirtualSource)object_).Dictionary.DataSources[((StiVirtualSource)object_).NameInSource];
            }
            else
            {
                PropertyInfo pDataSource = object_.GetType().GetProperty("DataSource");
                if (pDataSource != null) dataSource = (StiDataSource)pDataSource.GetValue(object_, null);
                if (dataSource == null)
                {
                    PropertyInfo pBusinessObject = object_.GetType().GetProperty("BusinessObject");
                    businessObject = pBusinessObject.GetValue(object_, null) as StiBusinessObject;
                }
            }

            if (dataSource != null || businessObject != null)
            {
                result.Add((string)sort["direction"]);
                string column = (string)sort["column"];
                if (column.StartsWith("{") && column.EndsWith("}"))
                {
                    result.Add(column);
                }
                else
                {
                    string[] columnPath = column.Split('.');
                    object dataObject = null;
                    if (dataSource != null) dataObject = dataSource;
                    if (businessObject != null) dataObject = businessObject;
                    result = GetColumnPathArray(dataObject, columnPath, result);
                }
            }

            return result;
        }

        private static List<string> GetColumnPathArray(object dataObject, string[] columnPath, List<string> result)
        {
            if (dataObject is StiDataSource)
            {
                StiDataSource dataSource = dataObject as StiDataSource;
                if (columnPath.Length == 1)
                {
                    result.Add(columnPath[0]);
                }
                else
                {
                    StiDataRelationsCollection relations = dataSource.GetParentRelations();

                    for (int level = 0; level < columnPath.Length - 1; level++)
                    {
                        StiDataRelation relation = GetChildRelation(relations, columnPath[level]);
                        if (relation != null)
                        {
                            result.Add(relation.NameInSource);
                            relations = relation.ParentSource.GetParentRelations();
                        }
                    }

                    result.Add(columnPath[columnPath.Length - 1]);
                }
            }
            else if (dataObject is StiBusinessObject)
            {
                if (columnPath.Length == 1)
                {
                    result.Add(columnPath[0]);
                }
                else
                {
                    //TO DO
                }
            }

            return result;
        }

        private static StiDataRelation GetChildRelation(StiDataRelationsCollection relations, string relationName)
        {
            foreach (StiDataRelation relation in relations)
                if (relation.Name == relationName)
                    return relation;

            return null;
        }
        #endregion

        #region Filter
        public static void SetFilterDataProperty(object component, ArrayList filters)
        {
            StiFiltersCollection filtersCollection = new StiFiltersCollection();

            for (var i = 0; i < filters.Count; i++)
            {
                Hashtable filter = (Hashtable)filters[i];
                filtersCollection.Add(FilterFromObject(filter));
            }

            PropertyInfo pi = component.GetType().GetProperty("Filters");
            if (pi != null) pi.SetValue(component, filtersCollection, null);
        }

        public static void SetFilterDataProperty(object component, object propertyValue)
        {
            ArrayList filtersObject = (ArrayList)JSON.Decode(StiEncodingHelper.DecodeString(propertyValue as string));

            StiFiltersCollection filters = new StiFiltersCollection();

            if (!string.IsNullOrEmpty(propertyValue as string))
            {
                for (int i = 0; i < filtersObject.Count; i++)
                {
                    Hashtable filter = (Hashtable)filtersObject[i];
                    filters.Add(FilterFromObject(filter));
                }
            }

            PropertyInfo pi = component.GetType().GetProperty("Filters");
            if (pi != null) pi.SetValue(component, filters, null);
        }

        private static StiFilter FilterFromObject(Hashtable filterObject)
        {
            StiFilter filter = new StiFilter();
            filter.Item = (((string)filterObject["fieldIs"]) == "Value") ? StiFilterItem.Value : StiFilterItem.Expression;
            filter.DataType = StrToFilterDataType((string)filterObject["dataType"]);
            filter.Column = (string)filterObject["column"];
            filter.Condition = StrToFilterCondition((string)filterObject["condition"]);
            filter.Value1 = (string)filterObject["value1"];
            filter.Value2 = (string)filterObject["value2"];            
            filter.Expression.Value = (string)filterObject["expression"] != string.Empty ? "{" + (string)filterObject["expression"] + "}" : string.Empty;

            return filter;
        }

        private static StiFilterDataType StrToFilterDataType(string dataType)
        {
            return (StiFilterDataType)Enum.Parse(typeof(StiFilterDataType), dataType);
        }

        private static StiFilterCondition StrToFilterCondition(string condition)
        {
            return (StiFilterCondition)Enum.Parse(typeof(StiFilterCondition), condition);
        }

        public static void SetFilterOnProperty(object component, object propertyValue)
        {
            PropertyInfo pi = component.GetType().GetProperty("FilterOn");
            if (pi != null) pi.SetValue(component, propertyValue, null);
        }

        public static void SetFilterModeProperty(object component, object propertyValue)
        {
            PropertyInfo pi = component.GetType().GetProperty("FilterMode");
            if (pi != null) pi.SetValue(component, propertyValue as string == "And" ? StiFilterMode.And : StiFilterMode.Or, null);
        }
        #endregion        

        #region ShiftMode
        public static void SetShiftModeProperty(object component, object propValue)
        {
            StiComponent comp = (StiComponent)component;
            string propertyValue = propValue as string;

            int value = 0;
            if (propertyValue.IndexOf("IncreasingSize") >= 0) value += 1;
            if (propertyValue.IndexOf("DecreasingSize") >= 0) value += 2;
            if (propertyValue.IndexOf("OnlyInWidthOfComponent") >= 0) value += 4;
            if (propertyValue.IndexOf("All") >= 0) value = 7;

            comp.ShiftMode = (Stimulsoft.Report.Components.StiShiftMode)value;
        }
        #endregion

        #region Restrictions
        public static void SetRestrictionsProperty(object component, object propValue)
        {
            StiComponent comp = (StiComponent)component;
            string propertyValue = propValue as string;

            int value = 0;
            if (propertyValue.IndexOf("AllowMove") >= 0) value += 1;
            if (propertyValue.IndexOf("AllowResize") >= 0) value += 2;
            if (propertyValue.IndexOf("AllowSelect") >= 0) value += 4;
            if (propertyValue.IndexOf("AllowChange") >= 0) value += 8;
            if (propertyValue.IndexOf("AllowDelete") >= 0) value += 16;
            if (propertyValue == "All") value = 31;

            comp.Restrictions = (Stimulsoft.Report.Components.StiRestrictions)value;
        }
        #endregion

        #region Conditions
        public static void SetConditionsProperty(object component, object propertyValue, StiReport report)
        {
            StiConditionsCollection conditions = new StiConditionsCollection();

            if (propertyValue as string != "")
            {
                ArrayList conditionsArray = JSON.Decode(StiEncodingHelper.DecodeString(propertyValue as string)) as ArrayList;

                foreach (Hashtable conditionObject in conditionsArray)
                {
                    string conditionType = (string)conditionObject["ConditionType"];

                    if (conditionType == "StiDataBarCondition")
                    {
                        conditions.Add(CreateDataBarCondition(conditionObject));
                    }
                    else if (conditionType == "StiIconSetCondition")
                    {
                        conditions.Add(CreateIconSetCondition(conditionObject));
                    }
                    else if (conditionType == "StiColorScaleCondition")
                    {
                        conditions.Add(CreateColorScaleCondition(conditionObject));
                    }
                    else if (conditionType == "StiHighlightCondition")
                    {
                        StiBaseCondition highlightCondition = CreateHighlightCondition(conditionObject, report);
                        if (highlightCondition != null) conditions.Add(highlightCondition);
                    }
                }
            }

            PropertyInfo pi = component.GetType().GetProperty("Conditions");
            if (pi != null) pi.SetValue(component, conditions, null);
        }

        public static StiBaseCondition CreateHighlightCondition(Hashtable conditionObject, StiReport report)
        {
            ArrayList filters = JSON.Decode(StiEncodingHelper.DecodeString((string)conditionObject["Filters"])) as ArrayList;

            if (filters.Count > 1)
            {
                StiMultiCondition condition = new StiMultiCondition();
                SetFilterDataProperty(condition, filters);
                if (conditionObject["FilterMode"] != null) SetFilterModeProperty(condition, (string)conditionObject["FilterMode"]);
                condition.TextColor = StrToColor((string)conditionObject["TextColor"]);
                condition.BackColor = StrToColor((string)conditionObject["BackColor"]);
                condition.Font = StrToFont((string)conditionObject["Font"], report);
                condition.Enabled = (bool)conditionObject["Enabled"];
                condition.CanAssignExpression = (bool)conditionObject["CanAssignExpression"];
                condition.AssignExpression = StiEncodingHelper.DecodeString((string)conditionObject["AssignExpression"]);
                condition.BorderSides = StrBordersToConditionBorderSidesObject((string)conditionObject["BorderSides"]);
                condition.Permissions = StrPermissionsToConditionPermissionsObject((string)conditionObject["Permissions"]);
                condition.Style = (string)conditionObject["Style"];
                condition.BreakIfTrue = (bool)conditionObject["BreakIfTrue"];

                return condition;
            }
            else if (filters.Count == 1)
            {
                StiCondition condition = new StiCondition();
                StiFilter filter = FilterFromObject((Hashtable)filters[0]);
                condition.Item = filter.Item;
                condition.DataType = filter.DataType;
                condition.Column = filter.Column;
                condition.Condition = filter.Condition;
                condition.Value1 = filter.Value1;
                condition.Value2 = filter.Value2;
                condition.Expression = filter.Expression;

                condition.BreakIfTrue = (bool)conditionObject["BreakIfTrue"];
                condition.TextColor = StrToColor((string)conditionObject["TextColor"]);
                condition.BackColor = StrToColor((string)conditionObject["BackColor"]);
                condition.Font = StrToFont((string)conditionObject["Font"], report);
                condition.Enabled = (bool)conditionObject["Enabled"];
                condition.CanAssignExpression = (bool)conditionObject["CanAssignExpression"];
                condition.AssignExpression = StiEncodingHelper.DecodeString((string)conditionObject["AssignExpression"]);
                condition.BorderSides = StrBordersToConditionBorderSidesObject((string)conditionObject["BorderSides"]);
                condition.Permissions = StrPermissionsToConditionPermissionsObject((string)conditionObject["Permissions"]);
                condition.Style = (string)conditionObject["Style"];

                return condition;
            }

            return null;
        }

        public static StiBaseCondition CreateDataBarCondition(Hashtable conditionObject)
        {
            StiDataBarCondition condition = new StiDataBarCondition(
                (string)conditionObject["Column"],
                (Components.StiBrushType)Enum.Parse(typeof(Components.StiBrushType), (string)conditionObject["BrushType"]),
                StrToColor((string)conditionObject["PositiveColor"]),
                StrToColor((string)conditionObject["NegativeColor"]),
                (bool)conditionObject["ShowBorder"],
                StrToColor((string)conditionObject["PositiveBorderColor"]),
                StrToColor((string)conditionObject["NegativeBorderColor"]),
                (StiDataBarDirection)Enum.Parse(typeof(StiDataBarDirection), (string)conditionObject["Direction"]),
                (StiMinimumType)Enum.Parse(typeof(StiMinimumType), (string)conditionObject["MinimumType"]),
                (float)StrToDouble((string)conditionObject["MinimumValue"]),
                (StiMaximumType)Enum.Parse(typeof(StiMaximumType), (string)conditionObject["MaximumType"]),
                (float)StrToDouble((string)conditionObject["MaximumValue"])
            );

            return condition;
        }

        public static StiBaseCondition CreateColorScaleCondition(Hashtable conditionObject)
        {
            StiColorScaleCondition condition = new StiColorScaleCondition(
                (string)conditionObject["Column"],
                (StiColorScaleType)Enum.Parse(typeof(StiColorScaleType), (string)conditionObject["ScaleType"]),
                StrToColor((string)conditionObject["MinimumColor"]),
                StrToColor((string)conditionObject["MidColor"]),
                StrToColor((string)conditionObject["MaximumColor"]),
                (StiMinimumType)Enum.Parse(typeof(StiMinimumType), (string)conditionObject["MinimumType"]),
                (float)StrToDouble((string)conditionObject["MinimumValue"]),
                (StiMidType)Enum.Parse(typeof(StiMidType), (string)conditionObject["MidType"]),
                (float)StrToDouble((string)conditionObject["MidValue"]),
                (StiMaximumType)Enum.Parse(typeof(StiMaximumType), (string)conditionObject["MaximumType"]),
                (float)StrToDouble((string)conditionObject["MaximumValue"])
            );

            return condition;
        }

        public static StiBaseCondition CreateIconSetCondition(Hashtable conditionObject)
        {
            StiIconSetCondition condition = new StiIconSetCondition(
                (string)conditionObject["Column"],
                (StiIconSet)Enum.Parse(typeof(StiIconSet), (string)conditionObject["IconSet"]),
                (ContentAlignment)Enum.Parse(typeof(ContentAlignment), (string)conditionObject["ContentAlignment"]),
                GetIconSetItemFromObject(conditionObject["IconSetItem1"]),
                GetIconSetItemFromObject(conditionObject["IconSetItem2"]),
                GetIconSetItemFromObject(conditionObject["IconSetItem3"]),
                GetIconSetItemFromObject(conditionObject["IconSetItem4"]),
                GetIconSetItemFromObject(conditionObject["IconSetItem5"])
            );

            return condition;
        }
        #endregion

        #region Interaction
        public static void SetInteractionProperty(object component, object propertyValue)
        {
            Hashtable interactionObject = (Hashtable)propertyValue;
            StiInteraction interaction = ((StiComponent)component).Interaction;

            if (interaction is StiBandInteraction)
            {
                ((StiBandInteraction)interaction).CollapsingEnabled = (bool)interactionObject["collapsingEnabled"];
                ((StiBandInteraction)interaction).CollapseGroupFooter = (bool)interactionObject["collapseGroupFooter"];
                ((StiBandInteraction)interaction).Collapsed.Value = StiEncodingHelper.DecodeString((string)interactionObject["collapsedValue"]);
                ((StiBandInteraction)interaction).SelectionEnabled = (bool)interactionObject["selectionEnabled"];
            }

            interaction.DrillDownEnabled = (bool)interactionObject["drillDownEnabled"];
            interaction.DrillDownReport = (string)interactionObject["drillDownReport"];
            interaction.DrillDownMode = (StiDrillDownMode)Enum.Parse(typeof(StiDrillDownMode), (string)interactionObject["drillDownMode"]);
            interaction.DrillDownPage = ((StiComponent)component).Report.Pages[(string)interactionObject["drillDownPage"]];
            interaction.DrillDownParameter1.Name = (string)interactionObject["drillDownParameter1Name"];
            interaction.DrillDownParameter1.Expression.Value = StiEncodingHelper.DecodeString((string)interactionObject["drillDownParameter1Expression"]);
            interaction.DrillDownParameter2.Name = (string)interactionObject["drillDownParameter2Name"];
            interaction.DrillDownParameter2.Expression.Value = StiEncodingHelper.DecodeString((string)interactionObject["drillDownParameter2Expression"]);
            interaction.DrillDownParameter3.Name = (string)interactionObject["drillDownParameter3Name"];
            interaction.DrillDownParameter3.Expression.Value = StiEncodingHelper.DecodeString((string)interactionObject["drillDownParameter3Expression"]);
            interaction.DrillDownParameter4.Name = (string)interactionObject["drillDownParameter4Name"];
            interaction.DrillDownParameter4.Expression.Value = StiEncodingHelper.DecodeString((string)interactionObject["drillDownParameter4Expression"]);
            interaction.DrillDownParameter5.Name = (string)interactionObject["drillDownParameter5Name"];
            interaction.DrillDownParameter5.Expression.Value = StiEncodingHelper.DecodeString((string)interactionObject["drillDownParameter5Expression"]);

            interaction.SortingEnabled = (bool)interactionObject["sortingEnabled"];
            interaction.SortingColumn = string.IsNullOrEmpty(interactionObject["sortingColumn"] as string) ? string.Empty : (string)interactionObject["sortingColumn"];

            interaction.Bookmark.Value = StiEncodingHelper.DecodeString((string)interactionObject["bookmark"]);
            interaction.Tag.Value = StiEncodingHelper.DecodeString((string)interactionObject["tag"]);
            interaction.ToolTip.Value = StiEncodingHelper.DecodeString((string)interactionObject["toolTip"]);

            switch ((string)interactionObject["hyperlinkType"])
            {
                case "HyperlinkUsingInteractionBookmark":
                    interaction.Hyperlink.Value = "#" + StiEncodingHelper.DecodeString((string)interactionObject["hyperlink"]);
                    break;

                case "HyperlinkUsingInteractionTag":
                    interaction.Hyperlink.Value = "##" + StiEncodingHelper.DecodeString((string)interactionObject["hyperlink"]);
                    break;

                case "HyperlinkExternalDocuments":
                    interaction.Hyperlink.Value = StiEncodingHelper.DecodeString((string)interactionObject["hyperlink"]);
                    break;
            }
        }
        #endregion

        #region ChartStyle
        public static void SetChartStyleProperty(object component, object propertyValue)
        {
            Hashtable param = new Hashtable();
            param["componentName"] = ((StiComponent)component).Name;
            param["styleType"] = ((Hashtable)propertyValue)["type"];
            param["styleName"] = ((Hashtable)propertyValue)["name"];

            StiChartHelper.SetChartStyle(((StiComponent)component).Report, param, new Hashtable());
        }
        #endregion

        #region GaugeStyle
        public static void SetGaugeStyleProperty(object component, object propertyValue)
        {
            Hashtable param = new Hashtable();
            param["componentName"] = ((StiComponent)component).Name;
            param["styleType"] = ((Hashtable)propertyValue)["type"];
            param["styleName"] = ((Hashtable)propertyValue)["name"];

            StiGaugeHelper.SetGaugeStyle(((StiComponent)component).Report, param, new Hashtable());
        }
        #endregion

        #region MapStyle
        public static void SetMapStyleProperty(object component, object propertyValue)
        {
            Hashtable param = new Hashtable();
            param["componentName"] = ((StiComponent)component).Name;
            param["styleType"] = ((Hashtable)propertyValue)["type"];
            param["styleName"] = ((Hashtable)propertyValue)["name"];

            StiMapHelper.SetMapStyle(((StiComponent)component).Report, param, new Hashtable());
        }
        #endregion

        #region CrossTabStyle
        public static void SetCrossTabStyleProperty(object component, object propertyValue)
        {
            Hashtable parameters = (Hashtable)propertyValue;
            StiCrossTab crossTab = (StiCrossTab)component;

            if (parameters["crossTabStyleIndex"] != null)
            {
                crossTab.CrossTabStyle = string.Empty;
                crossTab.CrossTabStyleIndex = Convert.ToInt32(parameters["crossTabStyleIndex"]);
            }
            else
            {
                crossTab.CrossTabStyleIndex = -1;
                crossTab.CrossTabStyle = (string)parameters["crossTabStyle"];
                crossTab.UpdateStyles();
            }

            crossTab.UpdateStyles();
        }
        #endregion

        #region SubReportParameters
        public static void SetSubReportParametersProperty(object component, object propertyValue)
        {
            StiSubReport subReport = component as StiSubReport;
            ArrayList parameters = propertyValue as ArrayList;
            subReport.Parameters.Clear();

            foreach (Hashtable parameterObject in parameters)
            {
                subReport.Parameters.Add(new StiParameter()
                {
                    Name = parameterObject["name"] as string,
                    Expression = StiEncodingHelper.DecodeString(parameterObject["expression"] as string)
                });
            }
        }

        #endregion

        #region Anchor
        public static void SetAnchorProperty(object component, object propValue)
        {
            StiComponent comp = (StiComponent)component;
            string propertyValue = propValue as string;

            int value = 0;
            if (propertyValue.IndexOf("Top") >= 0) value += 1;
            if (propertyValue.IndexOf("Bottom") >= 0) value += 2;
            if (propertyValue.IndexOf("Left") >= 0) value += 4;
            if (propertyValue.IndexOf("Right") >= 0) value += 8;

            comp.Anchor = (StiAnchorMode)value;
        }
        #endregion

        #region Image
        public static void SetImageProperty(StiComponent component, string propValue)
        {
            if (string.IsNullOrEmpty(propValue))
                ((StiImage)component).ResetImage();
            else
                ((StiImage)component).PutImage(Convert.FromBase64String(propValue.Substring(propValue.IndexOf("base64,") + 7)));
        }
        #endregion

        #region WatermarkImage
        public static void SetWatermarkImageProperty(StiPage page, string propValue)
        {
            if (string.IsNullOrEmpty(propValue))
                page.Watermark.ResetImage();
            else
                page.Watermark.PutImage(Convert.FromBase64String(propValue.Substring(propValue.IndexOf("base64,") + 7)));
        }
        #endregion

        #region ExcelValue
        public static void SetExcelValueProperty(object component, object propertyValue)
        {
            propertyValue = StiEncodingHelper.DecodeString(propertyValue as string).Replace("\n", "\r\n");
            StiExcelValueExpression excelValue = new StiExcelValueExpression(propertyValue as string);
            PropertyInfo pi = component.GetType().GetProperty("ExcelValue");

            if (pi != null) pi.SetValue(component, excelValue, null);
        }
        #endregion

        #endregion

        #region CallBack methods
        public static string WriteReportInObject(StiReport report)
        {
            StiDesignReportHelper reportHelper = new StiDesignReportHelper(report);
            string jsonReport = JSON.Encode(reportHelper.GetReportToObject());
            report.Info.Zoom = 1; //reset report zoom to 100% after serialize to json

            return jsonReport;
        }

        public static string WriteReportInObject(StiReport report, Hashtable attachedItems)
        {
            StiDesignReportHelper reportHelper = new StiDesignReportHelper(report);
            Hashtable reportObject = reportHelper.GetReportToObject();
            if (attachedItems != null && reportObject["dictionary"] != null) ((Hashtable)reportObject["dictionary"])["attachedItems"] = attachedItems;
            string jsonReport = JSON.Encode(reportObject);
            report.Info.Zoom = 1; //reset report zoom to 100% after serialize to json

            return jsonReport;
        }

        public static void CreateComponent(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            string currentPageName = (string)param["pageName"];
            string typeComponent = (string)param["typeComponent"];
            string[] componentRect = ((string)param["componentRect"]).Split('!');
            double zoom = StrToDouble((string)param["zoom"]);

            StiComponent newComponent = null;
            switch (typeComponent)
            {
                //Components
                case "StiText": { newComponent = new StiText(); break; }
                case "StiTextInCells": { newComponent = new StiTextInCells(); break; }
                case "StiRichText": { newComponent = new StiRichText(); break; }
                case "StiImage": { newComponent = new StiImage(); break; }
                case "StiBarCode": { newComponent = new StiBarCode(); break; }
                case "StiShape": { newComponent = new StiShape(); break; }
                case "StiPanel": { newComponent = new StiPanel(); break; }
                case "StiClone": { newComponent = new StiClone(); break; }
                case "StiCheckBox": { newComponent = new StiCheckBox(); break; }
                case "StiSubReport": { newComponent = new StiSubReport(); break; }
                case "StiZipCode": { newComponent = new StiZipCode(); break; }
                case "StiChart": { newComponent = new StiChart(); break; }
                case "StiTableCell": { newComponent = new StiTableCell(); break; }
                case "StiTableCellImage": { newComponent = new StiTableCellImage(); break; }
                case "StiTableCellCheckBox": { newComponent = new StiTableCellCheckBox(); break; }
                case "StiTableCellRichText": { newComponent = new StiTableCellRichText(); break; }

                //Primitives
                case "StiRectanglePrimitive": { newComponent = new StiRectanglePrimitive(); break; }
                case "StiRoundedRectanglePrimitive": { newComponent = new StiRoundedRectanglePrimitive(); break; }
                case "StiHorizontalLinePrimitive": { newComponent = new StiHorizontalLinePrimitive(); break; }
                case "StiVerticalLinePrimitive": { newComponent = new StiVerticalLinePrimitive(); break; }

                //Bands
                case "StiReportTitleBand": { newComponent = new StiReportTitleBand(); break; }
                case "StiReportSummaryBand": { newComponent = new StiReportSummaryBand(); break; }
                case "StiPageHeaderBand": { newComponent = new StiPageHeaderBand(); break; }
                case "StiPageFooterBand": { newComponent = new StiPageFooterBand(); break; }
                case "StiGroupHeaderBand": { newComponent = new StiGroupHeaderBand(); break; }
                case "StiGroupFooterBand": { newComponent = new StiGroupFooterBand(); break; }
                case "StiHeaderBand": { newComponent = new StiHeaderBand(); break; }
                case "StiFooterBand": { newComponent = new StiFooterBand(); break; }
                case "StiColumnHeaderBand": { newComponent = new StiColumnHeaderBand(); break; }
                case "StiColumnFooterBand": { newComponent = new StiColumnFooterBand(); break; }
                case "StiDataBand": { newComponent = new StiDataBand(); break; }
                case "StiTable":
                    {
                        newComponent = new StiTable();
                        if (param["additionalParams"] != null)
                        {
                            Hashtable additionalParams = (Hashtable)param["additionalParams"];
                            (newComponent as StiTable).RowCount = Convert.ToInt32(additionalParams["rowCount"]);
                            (newComponent as StiTable).ColumnCount = Convert.ToInt32(additionalParams["columnCount"]);
                        }
                        break;
                    }
                case "StiHierarchicalBand": { newComponent = new StiHierarchicalBand(); break; }
                case "StiChildBand": { newComponent = new StiChildBand(); break; }
                case "StiEmptyBand": { newComponent = new StiEmptyBand(); break; }
                case "StiOverlayBand": { newComponent = new StiOverlayBand(); break; }

                //Cross Bands
                case "StiCrossTab": { newComponent = new StiCrossTab(); break; }
                case "StiCrossGroupHeaderBand": { newComponent = new StiCrossGroupHeaderBand(); break; }
                case "StiCrossGroupFooterBand": { newComponent = new StiCrossGroupFooterBand(); break; }
                case "StiCrossHeaderBand": { newComponent = new StiCrossHeaderBand(); break; }
                case "StiCrossFooterBand": { newComponent = new StiCrossFooterBand(); break; }
                case "StiCrossDataBand": { newComponent = new StiCrossDataBand(); break; }

                //Dashboards elements
                case "StiTableElement":
                case "StiChartElement":
                case "StiGaugeElement":
                case "StiPivotElement":
                case "StiIndicatorElement":
                case "StiProgressElement":
                case "StiMapElement":
                case "StiImageElement":
                case "StiTextElement":
                case "StiPanelElement":
                    {
                        newComponent = StiDashboardHelper.CreateDashboardElement(report, typeComponent);
                        break;
                    }
            }

            if (typeComponent.StartsWith("Infographic"))
            {
                newComponent = CreateInfographicComponent(typeComponent);
                if (newComponent != null) typeComponent = newComponent.GetType().Name;
            }
            else if (typeComponent.StartsWith("StiShape;"))
            {
                newComponent = CreateShapeComponent(typeComponent);
                if (newComponent != null) typeComponent = newComponent.GetType().Name;
            }            
            else if (typeComponent.StartsWith("StiBarCode;"))
            {
                newComponent = CreateBarCodeComponent(typeComponent);
                if (newComponent != null) typeComponent = newComponent.GetType().Name;
            }
            //Dashboards shape element
            else if (typeComponent.StartsWith("StiShapeElement;"))
            {                
                newComponent = StiShapeElementHelper.CreateShapeElement(typeComponent);
                if (newComponent != null) typeComponent = newComponent.GetType().Name;
            }

            if (newComponent == null) return;

            StiPage currentPage = report.Pages[currentPageName];
            AddComponentToPage(newComponent, currentPage);

            double compWidth = StrToDouble(componentRect[2]);
            double compHeight = StrToDouble(componentRect[3]);

            if (newComponent is IStiElement && compWidth < 10 && compHeight < 10)
            {
                compWidth = newComponent.DefaultClientRectangle.Size.Width;
                compHeight = newComponent.DefaultClientRectangle.Size.Height;
            }
            else if (report.Unit.ConvertToHInches(compWidth) < 10 && report.Unit.ConvertToHInches(compHeight) < 10)
            {
                compWidth = report.Unit.ConvertFromHInches(newComponent.DefaultClientRectangle.Size.Width);
                compHeight = report.Unit.ConvertFromHInches(newComponent.DefaultClientRectangle.Size.Height);
            }

            RectangleD compRect = new RectangleD(
                new PointD(StrToDouble(componentRect[0]), StrToDouble(componentRect[1])),
                new SizeD(compWidth, compHeight)
                );

            SetComponentRect(newComponent, compRect);

            if (newComponent is StiCrossLinePrimitive)
                AddPrimitivePoints(newComponent, currentPage);

            if (newComponent is StiSubReport)
                AddSubReportPage(newComponent as StiSubReport, callbackResult);

            if (param["lastStyleProperties"] != null) SetAllProperties(newComponent, param["lastStyleProperties"] as ArrayList);

            callbackResult["name"] = newComponent.Name;
            callbackResult["typeComponent"] = typeComponent;
            callbackResult["componentRect"] = GetComponentRect(newComponent);
            callbackResult["parentName"] = GetParentName(newComponent);
            callbackResult["parentIndex"] = GetParentIndex(newComponent).ToString();
            callbackResult["componentIndex"] = GetComponentIndex(newComponent).ToString();
            callbackResult["childs"] = GetAllChildComponents(newComponent);
            callbackResult["svgContent"] = GetSvgContent(newComponent, zoom);
            callbackResult["pageName"] = currentPage.Name;
            callbackResult["properties"] = GetAllProperties(newComponent);
            callbackResult["rebuildProps"] = GetPropsRebuildPage(report, currentPage);
            callbackResult["largeHeightAutoFactor"] = currentPage.LargeHeightAutoFactor.ToString();
            if (newComponent is StiTable) callbackResult["tableCells"] = GetTableCells(newComponent as StiTable, zoom);
        }

        public static void RemoveComponent(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            ArrayList componentNames = (ArrayList)param["components"];

            report.IsModified = true;
            StiPage currPage = null;
            for (int i = 0; i < componentNames.Count; i++)
            {
                StiComponent currentComponent = report.GetComponentByName((string)componentNames[i]);
                if (currentComponent != null)
                {
                    if (currentComponent is StiCrossLinePrimitive)
                    {
                        RemovePrimitivePoints(currentComponent);
                    }

                    currentComponent.Parent.Components.Remove(currentComponent);
                    currPage = currentComponent.Page;
                    CheckAllPrimitivePoints(currPage);
                }
            }
            callbackResult["rebuildProps"] = GetPropsRebuildPage(report, currPage);
            callbackResult["pageName"] = currPage.Name;
            callbackResult["largeHeightAutoFactor"] = currPage.LargeHeightAutoFactor.ToString();
        }

        public static void ChangeRectComponent(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            StiPage currentPage = report.Pages[(string)param["pageName"]];
            double zoom = StrToDouble((string)param["zoom"]);
            ArrayList components = (ArrayList)param["components"];
            ArrayList componentsResultArray = new ArrayList();
            ArrayList changedComponents = new ArrayList();

            for (int i = 0; i < components.Count; i++)
            {
                Hashtable compProps = (Hashtable)components[i];
                StiComponent currentComponent = report.GetComponentByName((string)compProps["componentName"]);
                changedComponents.Add(currentComponent);
                string[] currentComponentRect = ((string)compProps["componentRect"]).Split('!');

                RectangleD compRect = new RectangleD(
                    new PointD(StrToDouble(currentComponentRect[0]), StrToDouble(currentComponentRect[1])),
                    new SizeD(StrToDouble(currentComponentRect[2]), StrToDouble(currentComponentRect[3]))
                );

                if (currentComponent is StiTableCell || !report.Info.AlignToGrid || param["runFromProperty"] != null)
                    SetComponentRect(currentComponent, compRect, false);
                else
                    SetComponentRectWithOffset(currentComponent, compRect, param["command"] as string, param["resizeType"] as string, compProps);

                Hashtable resultProps = new Hashtable();
                resultProps["componentName"] = currentComponent.Name;
                if (((string)param["command"] == "ResizeComponent") || currentComponent.DockStyle != StiDockStyle.None)
                {
                    resultProps["svgContent"] = GetSvgContent(currentComponent, zoom);
                }

                componentsResultArray.Add(resultProps);

                if ((string)param["command"] == "ResizeComponent" && currentComponent is StiTable)
                {
                    ((StiTable)currentComponent).DistributeRows();
                    callbackResult["cells"] = StiTableHelper.GetTableCellsProperties(currentComponent as StiTable, zoom);
                }
            }

            if (param["moveAfterCopyPaste"] != null)
            {
                foreach (StiComponent component in changedComponents)
                    component.Dockable = true;
            }

            callbackResult["components"] = componentsResultArray;
            callbackResult["pageName"] = currentPage.Name;
            callbackResult["rebuildProps"] = GetPropsRebuildPage(report, currentPage);
            callbackResult["largeHeightAutoFactor"] = currentPage.LargeHeightAutoFactor.ToString();
            callbackResult["isMultiResize"] = param["resizeType"] != null && ((string)param["resizeType"]).StartsWith("Multi");
        }

        public static void AddPage(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            StiPage newPage = new StiPage(report);
            int currentPageIndex = int.Parse((string)param["pageIndex"]);

            if (currentPageIndex + 1 == report.Pages.Count)
                report.Pages.Add(newPage);
            else
            {
                report.Pages.Insert(currentPageIndex + 1, newPage);
                newPage.Name = StiNameCreation.CreateName(report, StiNameCreation.GenerateName(newPage));
            }

            callbackResult["name"] = newPage.Name;
            callbackResult["pageIndex"] = report.Pages.IndexOf(newPage).ToString();
            callbackResult["properties"] = GetAllProperties(newPage);
            callbackResult["pageIndexes"] = GetPageIndexes(report);
        }

        public static void RemovePage(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            StiPage currentPage = report.Pages[(string)param["pageName"]];
            report.Pages.Remove(currentPage);

            callbackResult["pageName"] = currentPage.Name;
            callbackResult["pageIndexes"] = GetPageIndexes(report);
        }

        public static void ReadAllPropertiesFromString(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            bool updateAllControls = (bool)param["updateAllControls"];
            double zoom = StrToDouble((string)param["zoom"]);
            StiPage page = null;
            string svgContent = string.Empty;
            ArrayList components = (ArrayList)param["components"];
            ArrayList resultComponents = new ArrayList();
            Hashtable resultProperties = new Hashtable();

            for (int i = 0; i < components.Count; i++)
            {
                Hashtable componentProps = (Hashtable)components[i];
                string componentType = (string)componentProps["typeComponent"];
                string componentName = (string)componentProps["componentName"];
                ArrayList allProperties = (ArrayList)componentProps["properties"];

                if (componentType == "StiPage")
                {
                    page = report.Pages[componentName];
                    if (componentProps["cannotChange"] == null) SetAllProperties(page, allProperties);
                    resultProperties = GetAllProperties(page);
                    callbackResult["rebuildProps"] = GetPropsRebuildPage(report, page);
                }
                else
                {
                    StiComponent component = report.GetComponentByName(componentName);
                    if (componentProps["cannotChange"] == null) SetAllProperties(component, allProperties);
                    if (page == null) page = component.Page;

                    foreach (Hashtable property in allProperties)
                    {
                        if ((string)property["name"] == "dockStyle")
                        {
                            callbackResult["rebuildProps"] = GetPropsRebuildPage(report, page);
                            break;
                        }
                    }

                    resultProperties = GetAllProperties(component);
                    svgContent = GetSvgContent(component, zoom);
                }

                Hashtable resultCompProps = new Hashtable();
                resultComponents.Add(resultCompProps);

                resultCompProps["typeComponent"] = componentType;
                resultCompProps["componentName"] = componentName;
                resultCompProps["svgContent"] = svgContent;
                resultCompProps["properties"] = resultProperties;
            }

            callbackResult["components"] = resultComponents;
            callbackResult["largeHeightAutoFactor"] = page.LargeHeightAutoFactor.ToString();
            callbackResult["pageName"] = page.Name;
            callbackResult["updateAllControls"] = updateAllControls;
        }

        public static void ChangeUnit(StiReport report, string unitName)
        {
            report.IsModified = true;
            if (unitName == "cm") report.ReportUnit = StiReportUnitType.Centimeters;
            if (unitName == "hi") report.ReportUnit = StiReportUnitType.HundredthsOfInch;
            if (unitName == "in") report.ReportUnit = StiReportUnitType.Inches;
            if (unitName == "mm") report.ReportUnit = StiReportUnitType.Millimeters;
        }

        public static void SetToClipboard(StiRequestParams requestParams, StiReport report, Hashtable param, Hashtable callbackResult)
        {
            ArrayList componentNames = (ArrayList)param["components"];
            StiPage currentPage = report.Pages[((string)param["pageName"])];

            foreach (StiComponent component in report.GetComponents())
            {
                component.IsSelected = componentNames.Contains(component.Name) ? true : false;
            }

            var group = StiGroup.GetGroupFromPage(currentPage);
            requestParams.Cache.Helper.SaveObjectInternal(group.ToString("StiReport Clipboard"), requestParams, StiCacheHelper.GUID_Clipboard);

            foreach (StiComponent component in report.GetComponents())
                component.IsSelected = false;
        }

        public static void GetFromClipboard(StiRequestParams requestParams, StiReport report, Hashtable param, Hashtable callbackResult)
        {
            string text = requestParams.Cache.Helper.GetObjectInternal(requestParams, StiCacheHelper.GUID_Clipboard) as string;
            var group = StiGroup.CreateFromString(text, "StiReport Clipboard");

            StiPage currentPage = report.Pages[((string)param["pageName"])];
            double zoom = StrToDouble((string)param["zoom"]);
            StiInsertionComponents.InsertGroups(currentPage, group);

            ArrayList components = new ArrayList();
            foreach (StiComponent component in group.GetComponents())
            {
                if (component is StiCrossLinePrimitive)
                    AddPrimitivePoints(component, currentPage);

                Hashtable attributes = new Hashtable();
                attributes["name"] = component.Name;
                attributes["typeComponent"] = component.GetType().Name;
                attributes["componentRect"] = GetComponentRect(component);
                attributes["parentName"] = GetParentName(component);
                attributes["parentIndex"] = GetParentIndex(component).ToString();
                attributes["componentIndex"] = GetComponentIndex(component).ToString();
                attributes["childs"] = GetAllChildComponents(component);
                attributes["svgContent"] = GetSvgContent(component, zoom);
                attributes["pageName"] = currentPage.Name;
                attributes["properties"] = GetAllProperties(component);
                attributes["largeHeightAutoFactor"] = currentPage.LargeHeightAutoFactor.ToString();
                components.Add(attributes);
            }

            callbackResult["rebuildProps"] = GetPropsRebuildPage(report, currentPage);
            callbackResult["components"] = components;
        }

        public static StiReport GetUndoStep(StiRequestParams requestParams, StiReport currentReport, Hashtable param, Hashtable callbackResult)
        {
            ArrayList undoArray = requestParams.Cache.Helper.GetObjectInternal(requestParams, StiCacheHelper.GUID_UndoArray) as ArrayList;
            if (undoArray != null)
            {
                int currentPos = (int)undoArray[0];
                if (currentPos == undoArray.Count - 1) undoArray[currentPos] = new StiReportContainer(CloneReport(currentReport, true), true, StiDesignerCommand.Undo);
                if (currentPos > 1) currentPos--;

                StiReportContainer reportContainer = (StiReportContainer)undoArray[currentPos];
                StiReport report = reportContainer.report;
                report.Info.Zoom = StrToDouble((string)param["zoom"]);
                report.Info.ForceDesigningMode = true;

                if (param["reportFile"] != null) report.ReportFile = (string)param["reportFile"];

                if (!reportContainer.resourcesIncluded)
                {
                    LoadResourcesToReport(report, currentReport.Dictionary.Resources);
                }

                currentReport = report;
                callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(report);
                callbackResult["reportGuid"] = requestParams.Cache.ClientGuid;
                callbackResult["enabledUndoButton"] = currentPos > 1;
                undoArray[0] = currentPos;
            }
            callbackResult["selectedObjectName"] = param["selectedObjectName"];
            requestParams.Cache.Helper.SaveObjectInternal(undoArray, requestParams, StiCacheHelper.GUID_UndoArray);

            return currentReport;
        }

        public static StiReport GetRedoStep(StiRequestParams requestParams, StiReport currentReport, Hashtable param, Hashtable callbackResult)
        {
            ArrayList undoArray = requestParams.Cache.Helper.GetObjectInternal(requestParams, StiCacheHelper.GUID_UndoArray) as ArrayList;
            if (undoArray != null)
            {
                int currentPos = (int)undoArray[0];
                if (currentPos + 1 < undoArray.Count) currentPos++;

                StiReportContainer reportContainer = (StiReportContainer)undoArray[currentPos];
                StiReport report = reportContainer.report;
                report.Info.Zoom = StrToDouble((string)param["zoom"]);
                report.Info.ForceDesigningMode = true;

                if (param["reportFile"] != null) report.ReportFile = (string)param["reportFile"];

                if (!reportContainer.resourcesIncluded)
                {
                    LoadResourcesToReport(report, currentReport.Dictionary.Resources);
                }

                callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(report);
                callbackResult["enabledRedoButton"] = currentPos + 1 < undoArray.Count;
                currentReport = report;
                callbackResult["reportGuid"] = requestParams.Cache.ClientGuid;
                undoArray[0] = currentPos;
            }
            callbackResult["selectedObjectName"] = param["selectedObjectName"];
            requestParams.Cache.Helper.SaveObjectInternal(undoArray, requestParams, StiCacheHelper.GUID_UndoArray);

            return currentReport;
        }

        public static void RenameComponent(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            string typeComponent = (string)param["typeComponent"];
            string oldName = (string)param["oldName"];
            string newName = (string)param["newName"];

            if (typeComponent == "StiPage")
            {
                StiPage page = report.Pages[oldName];
                if (report.Pages[newName] == null)
                {
                    page.Name = newName;
                    callbackResult["rebuildProps"] = GetPropsRebuildPage(report, page);
                    newName = page.Name;
                }
                else newName = oldName;
            }
            else
            {
                StiComponent component = report.GetComponentByName(oldName);
                if (report.GetComponentByName(newName) == null)
                {
                    component.Name = newName;
                    callbackResult["rebuildProps"] = GetPropsRebuildPage(report, component.Page);
                    newName = component.Name;
                }
                else newName = oldName;
            }

            callbackResult["typeComponent"] = typeComponent;
            callbackResult["oldName"] = oldName;
            callbackResult["newName"] = newName;

        }

        public static void SaveComponentClone(StiRequestParams requestParams, StiComponent component)
        {
            //clone component
            StiComponent cloneComponent = component.Clone() as StiComponent;

            //Reset all parents
            cloneComponent.Report = null;
            cloneComponent.Parent = null;
            cloneComponent.Page = null;

            if (cloneComponent is StiCrossTab)
            {
                StiCrossTab crossTab = cloneComponent as StiCrossTab;
                foreach (StiComponent field in crossTab.Components)
                {
                    field.Report = null;
                    field.Page = null;
                    field.Parent = null;
                }
            }

            //Save clone component as string to cache
            var sr = new StiSerializing(new StiReportObjectStringConverter());
            var sb = new StringBuilder();
            using (var stringWriter = new StringWriter(sb))
            {
                sr.Serialize(cloneComponent, stringWriter, "CloneComponent", StiSerializeTypes.SerializeToAll);
                stringWriter.Close();
                requestParams.Cache.Helper.SaveObjectInternal(sb.ToString(), requestParams, StiCacheHelper.GUID_ComponentClone);
            }
        }

        public static void CanceledEditComponent(StiRequestParams requestParams, StiReport currentReport, Hashtable param)
        {
            var currentComponent = currentReport.GetComponentByName((string)param["componentName"]);
            if (currentComponent == null) return;
            StiComponent cloneComponent = null;

            if (currentComponent is StiChart) cloneComponent = new StiChart();
            else if (currentComponent is StiCrossTab) cloneComponent = new StiCrossTab();
            else if (currentComponent is StiGauge) cloneComponent = new StiGauge();
            else if (currentComponent is StiMap) cloneComponent = new StiMap();
            if (cloneComponent == null) return;

            //Get component from cahce
            string componentClone = requestParams.Cache.Helper.GetObjectInternal(requestParams, StiCacheHelper.GUID_ComponentClone) as string;
            using (var stringReader = new StringReader(componentClone))
            {
                var sr = new StiSerializing(new StiReportObjectStringConverter());
                sr.Deserialize(cloneComponent, stringReader, "CloneComponent");

                stringReader.Close();

                //Restore all parents
                cloneComponent.Report = currentReport;
                cloneComponent.Parent = currentComponent.Parent;
                cloneComponent.Page = currentComponent.Page;

                if (cloneComponent is StiCrossTab)
                {
                    StiCrossTab crossTab = cloneComponent as StiCrossTab;
                    crossTab.Components.SetParent(crossTab);

                    foreach (StiComponent field in crossTab.Components)
                    {
                        field.Report = currentReport;
                        field.Page = currentComponent.Page;
                    }
                }

                //Replace current component to component from Cache
                var parent = currentComponent.Parent;
                currentComponent.Parent.Components.Remove(currentComponent);
                parent.Components.Add(cloneComponent);
            }
        }

        public static void CreateTextComponentFromDictionary(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            string currentPageName = (string)param["pageName"];
            double zoom = StrToDouble((string)param["zoom"]);
            Hashtable point = (Hashtable)param["point"];
            Hashtable itemObject = (Hashtable)param["itemObject"];
            ArrayList newComponents = new ArrayList();
            bool isParameter = (string)itemObject["typeItem"] == "Parameter";
            bool createLabel = param["createLabel"] != null && (bool)param["createLabel"];
            StiPage currentPage = report.Pages[currentPageName];

            StiText sampleComp = new StiText();
            double compWidth = report.Unit.ConvertFromHInches(sampleComp.DefaultClientRectangle.Size.Width);
            double compHeight = report.Unit.ConvertFromHInches(sampleComp.DefaultClientRectangle.Size.Height);
            double comp1XPos = StrToDouble((string)point["x"]);
            double comp2XPos = comp1XPos + compWidth * 2;
            double compYPos = StrToDouble((string)point["y"]);
            var countComponents = (isParameter || !createLabel) ? 1 : 2;

            if (countComponents == 1)
            {
                if (comp1XPos + currentPage.Margins.Left + compWidth * 2 > currentPage.PageWidth)
                {
                    comp1XPos = currentPage.PageWidth - currentPage.Margins.Left - compWidth * 2;
                }
            }
            else if (countComponents == 2)
            {
                if (comp2XPos + currentPage.Margins.Left + compWidth > currentPage.PageWidth)
                {
                    comp2XPos = currentPage.PageWidth - currentPage.Margins.Left - compWidth;
                    comp1XPos = comp2XPos - compWidth * 2;
                }
            }

            for (int i = 0; i < countComponents; i++)
            {
                StiText newComponent = new StiText();
                AddComponentToPage(newComponent, currentPage);

                RectangleD compRect = new RectangleD(new PointD(i == 0 ? comp1XPos : comp2XPos, compYPos), new SizeD(i == 0 ? compWidth * 2 : compWidth, compHeight));
                SetComponentRect(newComponent, compRect);

                newComponent.Text = isParameter || !createLabel || (createLabel && i != 0) ? StiEncodingHelper.DecodeString((string)itemObject["fullName"]) : (string)itemObject["name"];

                if (param["lastStyleProperties"] != null) SetAllProperties(newComponent, param["lastStyleProperties"] as ArrayList);

                Hashtable componentProps = new Hashtable();
                componentProps["name"] = newComponent.Name;
                componentProps["typeComponent"] = "StiText";
                componentProps["componentRect"] = GetComponentRect(newComponent);
                componentProps["parentName"] = GetParentName(newComponent);
                componentProps["parentIndex"] = GetParentIndex(newComponent).ToString();
                componentProps["componentIndex"] = GetComponentIndex(newComponent).ToString();
                componentProps["childs"] = GetAllChildComponents(newComponent);
                componentProps["svgContent"] = GetSvgContent(newComponent, zoom);
                componentProps["pageName"] = currentPage.Name;
                componentProps["properties"] = GetAllProperties(newComponent);
                componentProps["rebuildProps"] = GetPropsRebuildPage(report, currentPage);

                newComponents.Add(componentProps);
            }
            callbackResult["newComponents"] = newComponents;
        }

        public static void CreateComponentFromResource(StiReport report, Hashtable param, Hashtable callbackResult)
        {            
            report.IsModified = true;
            string currentPageName = (string)param["pageName"];
            double zoom = StrToDouble((string)param["zoom"]);
            Hashtable point = (Hashtable)param["point"];
            Hashtable itemObject = (Hashtable)param["itemObject"];
            StiPage currentPage = report.Pages[currentPageName];
            StiComponent newComponent = null;
            string typeItem = itemObject["typeItem"] as string;
            string typeResource = itemObject["type"] as string;
            
            if (typeItem == "Resource" && (typeResource == "Report" || typeResource == "ReportSnapshot"))
            {
                newComponent = new StiSubReport()
                {
                    SubReportUrl = new StiHyperlinkExpression(StiHyperlinkProcessor.CreateResourceName(itemObject["name"] as string))
                };
            }
            else if (typeItem == "Resource" && (typeResource == "Rtf"))
            {
                newComponent = new StiRichText()
                {
                    DataUrl = new StiDataUrlExpression(StiHyperlinkProcessor.CreateResourceName(itemObject["name"] as string))
                };
            }
            else
            {
                newComponent = new StiImage();

                if (typeItem == "Variable" && itemObject["name"] != null)
                    (newComponent as StiImage).ImageData = new StiImageDataExpression("{" + itemObject["name"] as string + "}");
                else if (typeItem == "Column" && itemObject["fullName"] != null)
                {
                    string dataColumn = StiEncodingHelper.DecodeString((string)itemObject["fullName"]);
                    if (dataColumn.StartsWith("{") && dataColumn.EndsWith("}")) dataColumn = dataColumn.Substring(1, dataColumn.Length - 2);
                    (newComponent as StiImage).DataColumn = dataColumn;
                }
                else
                    (newComponent as StiImage).ImageURL = new StiImageURLExpression(StiHyperlinkProcessor.CreateResourceName(itemObject["name"] as string));
            }

            double compWidth = report.Unit.ConvertFromHInches(newComponent.DefaultClientRectangle.Size.Width);
            double compHeight = report.Unit.ConvertFromHInches(newComponent.DefaultClientRectangle.Size.Height);
            double compXPos = StrToDouble((string)point["x"]);
            double compYPos = StrToDouble((string)point["y"]);

            if (compXPos + currentPage.Margins.Left + compWidth * 2 > currentPage.PageWidth)
            {
                compXPos = currentPage.PageWidth - currentPage.Margins.Left - compWidth * 2;
            }

            AddComponentToPage(newComponent, currentPage);

            RectangleD compRect = new RectangleD(new PointD(compXPos, compYPos), new SizeD(compWidth, compHeight));
            SetComponentRect(newComponent, compRect);

            if (param["lastStyleProperties"] != null) SetAllProperties(newComponent, param["lastStyleProperties"] as ArrayList);

            Hashtable componentProps = new Hashtable();
            componentProps["name"] = newComponent.Name;
            componentProps["typeComponent"] = newComponent.GetType().Name;
            componentProps["componentRect"] = GetComponentRect(newComponent);
            componentProps["parentName"] = GetParentName(newComponent);
            componentProps["parentIndex"] = GetParentIndex(newComponent).ToString();
            componentProps["componentIndex"] = GetComponentIndex(newComponent).ToString();
            componentProps["childs"] = GetAllChildComponents(newComponent);
            componentProps["svgContent"] = GetSvgContent(newComponent, zoom);
            componentProps["pageName"] = currentPage.Name;
            componentProps["properties"] = GetAllProperties(newComponent);
            componentProps["rebuildProps"] = GetPropsRebuildPage(report, currentPage);

            callbackResult["newComponent"] = componentProps;
        }

        private static double AlignToMaxGrid(StiPage page, double value, bool converted)
        {
            if (converted) value = page.Unit.ConvertFromHInches(value);
            return StiAlignValue.AlignToMaxGrid(value,
                page.GridSize, page.Report.Info.AlignToGrid);
        }

        private static double AlignToGrid(StiPage page, double value, bool converted)
        {
            if (converted) value = page.Unit.ConvertFromHInches(value);
            return StiAlignValue.AlignToGrid(value,
                page.GridSize, page.Report.Info.AlignToGrid);
        }

        public static void CreateDataComponentFromDictionary(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            string currentPageName = (string)param["pageName"];
            double zoom = StrToDouble((string)param["zoom"]);
            Hashtable point = (Hashtable)param["point"];
            Hashtable dataSource = (Hashtable)param["dataSource"];
            Hashtable settings = (Hashtable)param["settings"];
            ArrayList columns = (ArrayList)param["columns"];

            StiPage currentPage = report.Pages[currentPageName];
            PointD cursorPoint = new PointD(StrToDouble((string)point["x"]), StrToDouble((string)point["y"]));

            if ((bool)settings["data"] || columns.Count == 0)
            {
                #region DataBand

                StiDataBand dataBand = new StiDataBand();
                string dataSourceName = (string)dataSource["name"];
                dataBand.Name = StiNameValidator.CorrectName("Data" + dataSourceName);
                dataBand.Name = StiNameCreation.CreateName(report, dataBand.Name, false, false, true);

                AddComponentToPage(dataBand, currentPage);

                double compWidth = report.Unit.ConvertFromHInches(dataBand.DefaultClientRectangle.Size.Width);
                double compHeight = report.Unit.ConvertFromHInches(dataBand.DefaultClientRectangle.Size.Height);
                RectangleD compRect = new RectangleD(cursorPoint, new SizeD(compWidth, compHeight));
                SetComponentRect(dataBand, compRect);

                if ((string)dataSource["typeItem"] == "DataSource")
                    SetDataSourceProperty(dataBand, dataSourceName);
                else
                    SetBusinessObjectProperty(dataBand, (string)dataSource["fullName"]);

                double width = dataBand.Parent is StiPage ? dataBand.Page.GetColumnWidth() : dataBand.Parent.Width;

                #region Create Header
                StiHeaderBand headerBand = null;
                if ((bool)settings["header"])
                {
                    headerBand = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.Bands.HeaderBand) as StiHeaderBand;
                    string baseName = dataBand.Report.Info.GenerateLocalizedName ? headerBand.LocalizedName + dataSourceName : "Header" + dataSourceName;
                    headerBand.Name = StiNameCreation.CreateName(dataBand.Report, StiNameValidator.CorrectName(baseName), false, false, true);
                    headerBand.Height = AlignToMaxGrid(dataBand.Page, 30, true);
                    int index = dataBand.Parent.Components.IndexOf(dataBand);
                    dataBand.Parent.Components.Insert(index, headerBand);
                }
                #endregion

                #region Create Footer
                StiFooterBand footerBand = null;
                if ((bool)settings["footer"])
                {
                    footerBand = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.Bands.FooterBand) as StiFooterBand;
                    string baseName = dataBand.Report.Info.GenerateLocalizedName ? footerBand.LocalizedName + dataSourceName : "Footer" + dataSourceName;
                    footerBand.Name = StiNameCreation.CreateName(dataBand.Report, StiNameValidator.CorrectName(baseName), false, false, true);
                    footerBand.Height = AlignToMaxGrid(dataBand.Page, 30, true);
                    int index = dataBand.Parent.Components.IndexOf(dataBand);
                    dataBand.Parent.Components.Insert(index + 1, footerBand);
                }
                #endregion

                #region Create columns
                double columnWidth = AlignToGrid(dataBand.Page, width / columns.Count, false);
                if (!(dataBand.Parent is StiPage))
                {
                    columnWidth = AlignToGrid(dataBand.Page, width / columns.Count, false);
                }
                double pos = 0;

                int indexNode = 1;

                foreach (Hashtable columnObject in columns)
                {
                    string columnName = (string)columnObject["fullName"];

                    if (indexNode == columns.Count)
                    {
                        columnWidth = AlignToGrid(dataBand.Page, width - pos, false);
                        if (columnWidth <= 0) columnWidth = dataBand.Page.GridSize;
                    }

                    #region HeaderText
                    if ((bool)settings["header"])
                    {
                        var headerText = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.Text) as StiText;
                        headerText.VertAlignment = StiVertAlignment.Center;
                        headerText.Font = new Font("Arial", 10, FontStyle.Bold);
                        string baseName = dataBand.Report.Info.GenerateLocalizedName ? headerBand.LocalizedName + columnName : "Header" + columnName;
                        headerText.Name = StiNameCreation.CreateName(dataBand.Report, StiNameValidator.CorrectName(baseName), false, false, true);
                        headerText.Top = 0;
                        headerText.Left = pos;
                        headerText.Width = columnWidth;
                        headerText.Height = headerBand.Height;
                        headerText.WordWrap = true;

                        int index = (columnName).LastIndexOf('.');

                        var column = StiDataBuilder.GetColumnFromPath(columnName, dataBand.Report.Dictionary);

                        if (column != null)
                        {
                            headerText.Text = column.Alias;
                        }
                        else
                        {
                            if (index == -1) headerText.Text = columnName;
                            else headerText.Text = columnName.Substring(index + 1);
                        }

                        headerBand.Components.Add(headerText);
                    }
                    #endregion

                    #region Data
                    var dataColumn = StiDataBuilder.GetColumnFromPath(columnName, dataBand.Report.Dictionary);
                    #region Image
                    if (dataColumn != null && (dataColumn.Type == typeof(Image) || dataColumn.Type == typeof(byte[])))
                    {
                        var dataImage = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.Image) as StiImage;
                        string baseName2 = dataBand.Report.Info.GenerateLocalizedName ? dataBand.LocalizedName + columnName : "Data" + columnName;
                        dataImage.Name = StiNameCreation.CreateName(dataBand.Report, StiNameValidator.CorrectName(baseName2), false, false, true);
                        dataImage.Top = 0;
                        dataImage.Left = pos;
                        dataImage.Width = columnWidth;
                        dataImage.Height = dataBand.Height;
                        dataImage.DataColumn = columnObject["imageColumnFullName"] != null ? (string)columnObject["imageColumnFullName"] : columnName;
                        dataImage.CanGrow = true;
                        dataBand.Components.Add(dataImage);
                    }
                    #endregion

                    #region CheckBox
                    else if (dataColumn != null && dataColumn.Type == typeof(bool))
                    {
                        var dataCheck = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.CheckBox) as Stimulsoft.Report.Components.StiCheckBox;
                        string baseName2 = dataBand.Report.Info.GenerateLocalizedName ? dataBand.LocalizedName + columnName : "Data" + columnName;
                        dataCheck.Name = StiNameCreation.CreateName(dataBand.Report, StiNameValidator.CorrectName(baseName2), false, false, true);
                        dataCheck.Top = 0;
                        dataCheck.Left = pos;
                        dataCheck.Width = columnWidth;
                        dataCheck.Height = dataBand.Height;
                        dataCheck.Checked.Value = "{" + columnName + "}";
                        dataBand.Components.Add(dataCheck);
                    }
                    #endregion

                    #region Text
                    else
                    {
                        var dataText = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.Text) as StiText;
                        string baseName2 = dataBand.Report.Info.GenerateLocalizedName ? dataBand.LocalizedName + columnName : "Data" + columnName;
                        dataText.Name = StiNameCreation.CreateName(dataBand.Report, StiNameValidator.CorrectName(baseName2), false, false, true);
                        dataText.VertAlignment = StiVertAlignment.Center;
                        dataText.Top = 0;
                        dataText.Left = pos;
                        dataText.Width = columnWidth;
                        dataText.Height = dataBand.Height;
                        dataText.Text = "{" + columnName + "}";
                        dataText.CanGrow = true;
                        dataText.WordWrap = true;
                        dataBand.Components.Add(dataText);
                    }
                    #endregion
                    #endregion

                    #region FooterText
                    if ((bool)settings["footer"])
                    {
                        var footerText = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.Text) as StiText;
                        footerText.VertAlignment = StiVertAlignment.Center;
                        footerText.HorAlignment = StiTextHorAlignment.Right;
                        footerText.Font = new Font("Arial", 10, FontStyle.Bold);
                        string baseName = dataBand.Report.Info.GenerateLocalizedName ? footerBand.LocalizedName + columnName : "Footer" + columnName;
                        footerText.Name = StiNameCreation.CreateName(dataBand.Report, StiNameValidator.CorrectName("Footer" + columnName), false, false, true);
                        footerText.Top = 0;
                        footerText.Left = pos;
                        footerText.Width = columnWidth;
                        footerText.Height = footerBand.Height;
                        footerText.WordWrap = true;

                        footerBand.Components.Add(footerText);
                    }
                    #endregion

                    pos += columnWidth;

                    indexNode++;
                }
                #endregion

                #region Theme
                if (param["theme"] != null)
                {
                    Hashtable themeProps = (Hashtable)param["theme"];
                    StiStylesCollection stylesCollection = new StiStylesCollection();

                    switch ((string)themeProps["type"])
                    {
                        case "Default":
                            {
                                string[] themeArray = ((string)themeProps["name"]).Split('_');
                                Color colorBase = Color.Empty;
                                string themeName = string.Empty;
                                string themeColor = themeArray[0];
                                int themePercent = int.Parse(themeArray[1]);

                                switch (themeArray[0])
                                {
                                    case "Red": { colorBase = Color.FromArgb(144, 60, 57); themeName = StiLocalization.Get("PropertyColor", "Red"); break; }
                                    case "Green": { colorBase = Color.FromArgb(117, 140, 72); themeName = StiLocalization.Get("PropertyColor", "Green"); break; }
                                    case "Blue": { colorBase = Color.FromArgb(69, 98, 135); themeName = StiLocalization.Get("PropertyColor", "Blue"); break; }
                                    case "Gray": { colorBase = Color.FromArgb(75, 75, 75); themeName = StiLocalization.Get("PropertyColor", "Gray"); break; }
                                }

                                StiStylesCreator creator = new StiStylesCreator(report);
                                string text = themeName + " " + themeArray[1] + "%";
                                Color color = StiColorUtils.Light(colorBase, (byte)(Math.Abs((themePercent - 100) / 25) * 45));
                                List<StiBaseStyle> styles = creator.CreateStyles(text, color);

                                foreach (StiBaseStyle style in styles)
                                {
                                    stylesCollection.Add(style);
                                }
                                break;
                            }
                        case "User":
                            {
                                foreach (StiBaseStyle style in report.Styles)
                                {
                                    if (style.CollectionName == (string)themeProps["name"])
                                        stylesCollection.Add(style);
                                }
                                break;
                            }
                    }

                    if (dataBand != null && stylesCollection != null)
                    {
                        foreach (StiBaseStyle style in stylesCollection)
                        {
                            if (!dataBand.Report.Styles.Contains(style))
                            {
                                dataBand.Report.Styles.Add(style);
                            }
                        }

                        ApplyStyleCollection(dataBand, stylesCollection);
                        foreach (StiComponent component in dataBand.GetComponents())
                        {
                            ApplyStyleCollection(component, stylesCollection);
                        }
                    }

                    if (headerBand != null)
                    {
                        ApplyStyleCollection(headerBand, stylesCollection);
                        foreach (StiComponent component in headerBand.GetComponents())
                        {
                            ApplyStyleCollection(component, stylesCollection);
                        }
                    }

                    if (footerBand != null)
                    {
                        ApplyStyleCollection(footerBand, stylesCollection);
                        foreach (StiComponent component in footerBand.GetComponents())
                        {
                            ApplyStyleCollection(component, stylesCollection);
                        }
                    }
                }
                #endregion

                #endregion
            }
            else
            {
                #region Table
                string dataSourceName = (string)dataSource["name"];

                StiTable table = new StiTable();

                AddComponentToPage(table, currentPage);

                if ((string)dataSource["typeItem"] == "DataSource")
                    SetDataSourceProperty(table, dataSourceName);
                else
                    SetBusinessObjectProperty(table, (string)dataSource["fullName"]);

                string baseName = table.Report.Info.GenerateLocalizedName ? table.LocalizedName + dataSourceName : "Table" + dataSourceName;
                table.Name = StiNameCreation.CreateName(table.Report, StiNameValidator.CorrectName(baseName), false, false, true);
                double width = table.Parent is StiPage ? table.Page.GetColumnWidth() : table.Parent.Width;

                int countRows = 1;
                if ((bool)settings["header"]) countRows++;
                if ((bool)settings["footer"]) countRows++;
                table.RowCount = countRows;
                table.ColumnCount = columns.Count;
                table.HeaderRowsCount = ((bool)settings["header"]) ? 1 : 0;
                table.FooterRowsCount = ((bool)settings["footer"]) ? 1 : 0;
                table.Height = (table.Page.GridSize * 4) * countRows;
                table.Width = width;

                int indexHeader = 0;
                int indexData = ((bool)settings["header"]) ? columns.Count : 0;

                foreach (Hashtable columnObject in columns)
                {
                    string columnName = (string)columnObject["fullName"];

                    #region Create Header
                    if ((bool)settings["header"])
                    {
                        var headerCell = (StiTableCell)table.Components[indexHeader];

                        int index = columnName.LastIndexOf('.');
                        var column = StiDataBuilder.GetColumnFromPath(columnName, table.Report.Dictionary);

                        if (column != null)
                        {
                            headerCell.Text = column.Alias;
                        }
                        else
                        {
                            if (index == -1) headerCell.Text = columnName;
                            else headerCell.Text = columnName.Substring(index + 1);
                        }
                        indexHeader++;
                    }
                    #endregion

                    #region Create Data
                    var dataColumn = StiDataBuilder.GetColumnFromPath(columnName, table.Report.Dictionary);
                    #region Image
                    if (dataColumn != null && (dataColumn.Type == typeof(Image) || dataColumn.Type == typeof(byte[])))
                    {
                        ((IStiTableCell)table.Components[indexData]).CellType = StiTablceCellType.Image;
                        var dataCell = (StiTableCellImage)table.Components[indexData];
                        dataCell.DataColumn = columnObject["imageColumnFullName"] != null ? (string)columnObject["imageColumnFullName"] : columnName;
                        indexData++;
                    }
                    #endregion

                    #region CheckBox
                    else if (dataColumn != null && dataColumn.Type == typeof(bool))
                    {
                        ((IStiTableCell)table.Components[indexData]).CellType = StiTablceCellType.CheckBox;
                        var dataCell = (StiTableCellCheckBox)table.Components[indexData];
                        dataCell.Checked.Value = "{" + columnName as string + "}";
                        indexData++;
                    }
                    #endregion

                    #region Text
                    else
                    {
                        var dataCell = (StiTableCell)table.Components[indexData];
                        dataCell.Text = "{" + columnName as string + "}";
                        indexData++;
                    }
                    #endregion
                    #endregion
                }

                #endregion
            }

            StiDesignReportHelper reportHelper = new StiDesignReportHelper(report);
            callbackResult["pageComponents"] = JSON.Encode(reportHelper.GetPage(report.Pages.IndexOf(currentPage)));
            callbackResult["pageName"] = currentPageName;
            callbackResult["rebuildProps"] = GetPropsRebuildPage(report, currentPage);
        }

        public static void SetReportProperties(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            Hashtable allProperties = (Hashtable)param["properties"];
            foreach (DictionaryEntry property in allProperties)
            {
                string propertyName = property.Key as string;
                object propertyValue = property.Value;

                if (propertyName == "reportUnit") ChangeUnit(report, propertyName);
                else SetPropertyValue(report, UpperFirstChar(propertyName), report, propertyValue);
            }
            callbackResult["properties"] = GetReportProperties(report);
        }

        public static Hashtable GetReportProperties(StiReport report)
        {
            Hashtable properties = new Hashtable();
            properties["reportUnit"] = report.Unit.ShortName;
            properties["reportFile"] = GetReportFileName(report);
            properties["events"] = GetEventsProperty(report);

            string[] propertyNames = { "ReportName", "ReportAlias", "ReportAuthor", "ReportDescription", "AutoLocalizeReportOnRun", "CacheAllData", "CacheTotals",
                "CalculationMode", "ConvertNulls", "Collate", "Culture", "EngineVersion", "NumberOfPass", "PreviewMode", "ReportCacheMode", "ParametersOrientation",
                "RequestParameters", "ScriptLanguage", "StopBeforePage", "StoreImagesInResources", "RetrieveOnlyUsedData", "RefreshTime"};
            foreach (string propertyName in propertyNames)
            {
                var value = StiReportEdit.GetPropertyValue(propertyName, report);
                if (value != null) { properties[LowerFirstChar(propertyName)] = value; }
            }

            return properties;
        }

        public static void PageMove(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            int pageIndex = StrToInt((string)param["pageIndex"]);
            string direction = (string)param["direction"];

            if (direction == "Left" && pageIndex > 0)
            {
                var page = report.Pages[pageIndex - 1];
                report.Pages.RemoveAt(pageIndex - 1);
                report.Pages.Insert(pageIndex, page);
            }
            else if (direction == "Right" && pageIndex < report.Pages.Count - 1)
            {
                var page = report.Pages[pageIndex];
                report.Pages.RemoveAt(pageIndex);
                report.Pages.Insert(pageIndex + 1, page);
            }

            callbackResult["pageIndexes"] = GetPageIndexes(report);
        }

        public static void AlignToGridComponents(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            StiPage currentPage = report.Pages[(string)param["pageName"]];
            ArrayList componentNames = (ArrayList)param["components"];
            for (int i = 0; i < componentNames.Count; i++)
            {
                StiComponent component = report.GetComponentByName((string)componentNames[i]);
                RectangleD rect = component.GetPaintRectangle(false, false);
                rect = rect.AlignToGrid(component.Page.GridSize, true);
                SetComponentRect(component, rect);
            }
            callbackResult["pageName"] = currentPage.Name;
            callbackResult["rebuildProps"] = GetPropsRebuildPage(report, currentPage);
        }

        public static void ChangeArrangeComponents(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            StiPage currentPage = report.Pages[(string)param["pageName"]];
            ArrayList componentNames = (ArrayList)param["components"];

            //set not selected all components            
            foreach (StiComponent component in currentPage.GetComponents())
            {
                component.IsSelected = false;
            }

            //set selected current components
            foreach (string componentName in componentNames)
            {
                StiComponent component = report.GetComponentByName(componentName);
                if (component != null) component.Select();
            }

            switch ((string)param["arrangeCommand"])
            {
                case "BringToFront": currentPage.BringToFront(); break;
                case "SendToBack": currentPage.SendToBack(); break;
                case "MoveForward": currentPage.MoveForward(); break;
                case "MoveBackward": currentPage.MoveBackward(); break;
                case "AlignLeft": currentPage.AlignTo(StiAligning.Left); break;
                case "AlignCenter": currentPage.AlignTo(StiAligning.Center); break;
                case "AlignRight": currentPage.AlignTo(StiAligning.Right); break;
                case "AlignTop": currentPage.AlignTo(StiAligning.Top); break;
                case "AlignMiddle": currentPage.AlignTo(StiAligning.Middle); break;
                case "AlignBottom": currentPage.AlignTo(StiAligning.Bottom); break;
                case "MakeHorizontalSpacingEqual": currentPage.MakeHorizontalSpacingEqual(); break;
                case "MakeVerticalSpacingEqual": currentPage.MakeVerticalSpacingEqual(); break;
                case "CenterHorizontally": currentPage.SetCenterHorizontally(); break;
                case "CenterVertically": currentPage.SetCenterVertically(); break;
            }

            currentPage.Correct();

            callbackResult["pageName"] = currentPage.Name;
            callbackResult["rebuildProps"] = GetPropsRebuildPage(report, currentPage);
        }

        public static void DuplicatePage(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            int pageIndex = Convert.ToInt32((string)param["pageIndex"]);
            StiPage page = report.Pages[pageIndex];

            MemoryStream stream = new MemoryStream();
            page.Save(stream);
            stream.Seek(0, SeekOrigin.Begin);

            StiPage newPage = null;
            newPage = new StiPage() { Name = "tempName" };

            newPage.Report = report;
            newPage.Load(stream);
            newPage.Name = StiNameCreation.CreateName(report, StiNameCreation.GenerateName(report, page.LocalizedName, page.GetType()));
            newPage.NewGuid();

            report.Pages.Insert(pageIndex + 1, newPage);

            StiComponentsCollection comps = newPage.GetComponents();
            foreach (StiComponent comp in comps)
            {
                comp.Name = StiNameCreation.CreateName(report, StiNameCreation.GenerateName(report, comp.LocalizedName, comp.GetType()));
            }

            if (param["reportFile"] != null) report.ReportFile = (string)param["reportFile"];
            callbackResult["reportGuid"] = param["reportGuid"];
            callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(report);
            callbackResult["selectedPageIndex"] = pageIndex + 1;
        }
        
        public static void SetEventValue(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            ArrayList components = (ArrayList)param["components"];
            foreach (Hashtable componentParams in components)
            {
                object object_ = null;

                if ((string)componentParams["typeComponent"] == "StiReport") object_ = report;
                else if ((string)componentParams["typeComponent"] == "StiPage") object_ = report.Pages[(string)componentParams["name"]];
                else object_ = report.GetComponentByName((string)componentParams["name"]);

                if (object_ != null)
                {
                    SetPropertyValue(report, (string)param["eventName"] + ".Script", object_, StiEncodingHelper.DecodeString((string)param["eventValue"]));
                }
            }
        }

        public static void ChangeSizeComponents(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            report.IsModified = true;
            StiPage currentPage = report.Pages[(string)param["pageName"]];
            ArrayList componentNames = (ArrayList)param["components"];
            double zoom = StrToDouble((string)param["zoom"]);
            StiComponentsCollection currentComponents = new StiComponentsCollection();

            //set not selected all components            
            foreach (StiComponent component in currentPage.GetComponents())
            {
                component.IsSelected = false;
            }

            //set selected current components
            foreach (string componentName in componentNames)
            {
                StiComponent component = report.GetComponentByName(componentName);
                if (component != null)
                {
                    component.Select();
                    currentComponents.Add(component);
                }
            }

            if (currentComponents.Count > 0)
            {
                switch ((string)param["actionName"])
                {
                    case "MakeSameSize": currentPage.MakeSameSize(new SizeD(currentComponents[0].Width, currentComponents[0].Height)); break;
                    case "MakeSameWidth": currentPage.MakeSameWidth(currentComponents[0].Width); break;
                    case "MakeSameHeight": currentPage.MakeSameHeight(currentComponents[0].Height); break;
                }
                currentPage.Correct();
            }

            callbackResult["pageName"] = currentPage.Name;
            Hashtable rebuildProps = GetPropsRebuildPage(report, currentPage);

            //repaint svg content
            foreach (StiComponent component in currentComponents)
            {
                if (rebuildProps[component.Name] != null)
                {
                    (rebuildProps[component.Name] as Hashtable)["svgContent"] = GetSvgContent(component, zoom);
                }
            }
            callbackResult["rebuildProps"] = rebuildProps;
        }
                
        public static void CreateMovingCopyComponent(StiRequestParams requestParams, StiReport report, Hashtable param, Hashtable callbackResult)
        {
            SetToClipboard(requestParams, report, param, callbackResult);

            string text = requestParams.Cache.Helper.GetObjectInternal(requestParams, StiCacheHelper.GUID_Clipboard) as string;
            var group = StiGroup.CreateFromString(text, "StiReport Clipboard");

            StiPage currentPage = report.Pages[((string)param["pageName"])];
            double zoom = StrToDouble((string)param["zoom"]);
            StiInsertionComponents.InsertGroups(currentPage, group);

            ArrayList components = new ArrayList();
            StiComponentsCollection groupComponents = group.GetComponents();
            if (groupComponents.Count > 0)
            {
                string[] componentRect = ((string)param["componentRect"]).Split('!');
                RectangleD compRect = new RectangleD(
                    new PointD(StrToDouble(componentRect[0]), StrToDouble(componentRect[1])),
                    new SizeD(StrToDouble(componentRect[2]), StrToDouble(componentRect[3]))
                );

                bool ignoreOffset = false;
                ArrayList componentNames = (ArrayList)param["components"];
                callbackResult["oldComponentNames"] = componentNames;

                if (componentNames.Count > 0)
                {
                    StiComponent compSource = report.GetComponentByName(componentNames[0] as string);
                    if (compSource != null && IsAlignedByGrid(compSource))
                        ignoreOffset = true;
                }

                if (ignoreOffset)
                    SetComponentRect(groupComponents[0], compRect);
                else
                {
                    SetComponentRect(groupComponents[0], compRect, false);
                    SetComponentRectWithOffset(groupComponents[0], compRect, "MoveComponent", null, null);
                }
            }

            foreach (StiComponent component in group.GetComponents())
            {
                if (component is StiCrossLinePrimitive)
                    AddPrimitivePoints(component, currentPage);
                
                Hashtable attributes = new Hashtable();
                attributes["name"] = component.Name;
                attributes["typeComponent"] = component.GetType().Name;
                attributes["componentRect"] = GetComponentRect(component);
                attributes["parentName"] = GetParentName(component);
                attributes["parentIndex"] = GetParentIndex(component).ToString();
                attributes["componentIndex"] = GetComponentIndex(component).ToString();
                attributes["childs"] = GetAllChildComponents(component);
                attributes["svgContent"] = GetSvgContent(component, zoom);
                attributes["pageName"] = currentPage.Name;
                attributes["properties"] = GetAllProperties(component);
                attributes["largeHeightAutoFactor"] = currentPage.LargeHeightAutoFactor.ToString();
                components.Add(attributes);
                component.Dockable = true;
            }

            callbackResult["components"] = components;
            callbackResult["rebuildProps"] = GetPropsRebuildPage(report, currentPage);
            callbackResult["isLastCommand"] = param["isLastCommand"];
        }

        public static void UpdateReportAliases(StiRequestParams requestParams, StiReport report, Hashtable param, Hashtable callbackResult)
        {
            if (report != null)
            {
                callbackResult["reportObject"] = StiReportEdit.WriteReportInObject(report);
                callbackResult["reportGuid"] = requestParams.Cache.ClientGuid;
                callbackResult["selectedObjectName"] = param["selectedObjectName"];
            }
        }

        public static void OpenPage(StiRequestParams requestParams, StiReport report, Hashtable callbackResult)
        {
            using (var stream = new MemoryStream(requestParams.Data))
            {
                report.IsModified = true;

                var page = new StiPage();
                page.Report = report;
                report.Pages.Add(page);
                page.Load(stream);

                page.Components.SortByPriority();

                #region Create names for components
                report.Pages.Remove(page);

                var tempPage = new StiPage(report);
                tempPage.Name = "#%#TempPageForStoreComponents#%#";
                report.Pages.Add(tempPage);

                var compcoll = new StiComponentsCollection();
                tempPage.Components = compcoll;

                page.Name = StiNameCreation.CreateName(report, "Page", true, false, true);
                var comps = page.GetComponents();

                foreach (StiComponent comp in comps)
                {
                    if (!StiNameCreation.IsValidName(report, comp.Name))
                        comp.Name = StiNameCreation.CreateName(report, StiNameCreation.GenerateName(comp));

                    tempPage.Components.Add(comp);
                }

                report.Pages.Remove(tempPage);
                report.Pages.Add(page);
                #endregion

                if (page.IsPage && page.ReportUnit != null)
                {
                    page.Convert(page.ReportUnit, page.Report.Unit);
                    page.ReportUnit = null;
                }

                StiDesignReportHelper reportHelper = new StiDesignReportHelper(report);
                var pageProps = reportHelper.GetPage(report.Pages.IndexOf(page));
                pageProps["pageIndexes"] = GetPageIndexes(report);

                callbackResult["pageProps"] = pageProps;
            }
        }
        #endregion
    }
}