﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.BarCodes;
using Stimulsoft.Report.Export;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace Stimulsoft.Report.Web
{
    internal class StiBarCodeHelper
    {
        public static Hashtable GetBarCodeJSObject(StiBarCode barCode)
        {
            Hashtable properties = new Hashtable();
            properties["name"] = barCode.Name;
            properties["codeType"] = barCode.BarCodeType.GetType().Name;
            properties["sampleImage"] = GetBarCodeSampleImage(barCode);            
            properties["properties"] = GetBarCodeProperties(barCode);

            return properties;
        }

        public static Hashtable GetBarCodeProperties(StiBarCode barCode)
        {
            Hashtable properties = new Hashtable();

            #region Common
            Hashtable propertiesCommon = new Hashtable();
            string[] propNamesCommon = { "Code", "Angle", "AutoScale", "ForeColor", "BackColor", "Font", "HorAlignment", "VertAlignment",
                "ShowLabelText", "ShowQuietZones" };
            foreach (string propName in propNamesCommon)
            {
                var value = StiReportEdit.GetPropertyValue(propName, barCode);
                if (value != null) propertiesCommon[StiReportEdit.LowerFirstChar(propName)] = value;
            }

            properties["common"] = propertiesCommon;
            #endregion

            #region Additional
            Hashtable propertiesAdditional = new Hashtable();
            string[] propNamesAdditional = { "Module", "Checksum", "CheckSum", "CheckSum1", "CheckSum2", "Height", "Ratio", "EncodingType", "MatrixSize",
                "UseRectangularSymbols", "SupplementCode", "ShowQuietZoneIndicator", "SupplementType", "AddClearZone", "PrintVerticalBars", "AspectRatio",
                "AutoDataColumns", "AutoDataRows", "DataColumns", "DataRows", "EncodingMode", "ErrorsCorrectionLevel", "RatioY", "Space", "ErrorCorrectionLevel",
                "CompanyPrefix", "ExtensionDigit", "SerialNumber", "Mode", "ProcessTilde", "StructuredAppendPosition", "StructuredAppendTotal", "TrimExcessData"
            };
            
            foreach (string propName in propNamesAdditional)
            {
                var value = StiReportEdit.GetPropertyValue(propName, barCode.BarCodeType);
                if (value != null) propertiesAdditional[StiReportEdit.LowerFirstChar(propName)] = value;
            }

            properties["additional"] = propertiesAdditional;
            #endregion

            return properties;
        }

        public static void ApplyBarCodeProperties(StiReport report, Hashtable param, Hashtable callbackResult)
        {
            var component = report.GetComponentByName((string)param["componentName"]);
            if (component != null)
            {
                StiBarCode barCode = component as StiBarCode;
                ArrayList properties = (ArrayList)param["properties"];
                foreach (Hashtable property in properties)
                {
                    string propertyName = property["name"] as string;
                    if (propertyName == "codeType")
                        StiReportEdit.SetBarCodeTypeProperty(barCode, property["value"]);
                    else
                        StiReportEdit.SetPropertyValue(report, StiReportEdit.UpperFirstChar(propertyName), barCode, property["value"]);
                }

                callbackResult["barCode"] = GetBarCodeJSObject(barCode);
            }
        }

        public static string GetBarCodeSampleImage(StiBarCode barCode)
        {
            var tempBarCode = (StiBarCode)barCode.Clone();
            tempBarCode.Report = barCode.Report;
            tempBarCode.Width = barCode.Report.Unit.ConvertFromHInches(440d);
            tempBarCode.Height = barCode.Report.Unit.ConvertFromHInches(200d);
            tempBarCode.HorAlignment = StiHorAlignment.Center;
            tempBarCode.VertAlignment = StiVertAlignment.Center;

            return StiSvgHelper.SaveComponentToString(tempBarCode, ImageFormat.Png, 0.75f, 150f);
        }
    }
}