﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.Table;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Web
{
    internal class StiWizardHelper
    {
        private static ArrayList GetGroupsFromDataSource(string dataSourceName, Hashtable dataSources)
        {            
            Hashtable dataSource = (Hashtable)dataSources[dataSourceName];
            ArrayList groups = (ArrayList)dataSource["groups"];
            
            return groups;
        }

        private static ArrayList GetColumnsFromDataSource(string dataSourceName, Hashtable dataSources)
        {
            Hashtable dataSource = (Hashtable)dataSources[dataSourceName];
            ArrayList columns = (ArrayList)dataSource["columns"];

            return columns;
        }

        private static ArrayList GetSortFromDataSource(string dataSourceName, Hashtable dataSources)
        {
            Hashtable dataSource = (Hashtable)dataSources[dataSourceName];
            ArrayList sort = (ArrayList)dataSource["sort"];

            return sort;
        }

        private static ArrayList GetFiltersFromDataSource(string dataSourceName, Hashtable dataSources)
        {
            Hashtable dataSource = (Hashtable)dataSources[dataSourceName];
            ArrayList filters = (ArrayList)dataSource["filters"];

            return filters;
        }

        private static Hashtable GetTotalsFromDataSource(string dataSourceName, Hashtable dataSources)
        {
            Hashtable dataSource = (Hashtable)dataSources[dataSourceName];
            Hashtable totals = (Hashtable)dataSource["totals"];

            return totals;
        }
                
        private static double AlignToMaxGrid(StiPage page, double value, bool converted)
		{
			if (converted)value = page.Unit.ConvertFromHInches(value);
			return StiAlignValue.AlignToMaxGrid(value, 
				page.GridSize, page.Report.Info.AlignToGrid);
		}

        private static double AlignToGrid(StiPage page, double value, bool converted)
        {
            if (converted) value = page.Unit.ConvertFromHInches(value);
            return StiAlignValue.AlignToGrid(value,
                page.GridSize, page.Report.Info.AlignToGrid);
        }

        public static StiReport GetReportFromWizardOptions(StiReport createdReport, Hashtable reportOptions, Hashtable wizardDataSources)
        {   
		    #region Fields

            ArrayList sorts = new ArrayList();
		    ArrayList filters = new ArrayList();
		    ArrayList totals = new ArrayList();
            ArrayList orderDataNames = (ArrayList)reportOptions["dataSourcesOrder"];
            Hashtable relations = (Hashtable)reportOptions["relations"];

            #endregion

            #region Set Report Language

            createdReport.ScriptLanguage = (string)reportOptions["language"] == "C" ? StiReportLanguageType.CSharp : StiReportLanguageType.VB;

            #endregion

            #region Set report unit

            StiReportEdit.ChangeUnit(createdReport, (string)reportOptions["unit"]);

            #endregion
            
            #region Set report orientation

            StiPage page = createdReport.GetCurrentPage();
            page.Orientation = (string)reportOptions["orientation"] == "Portrait" ? StiPageOrientation.Portrait : StiPageOrientation.Landscape;

            #endregion
            
            Hashtable businessObjects = new Hashtable();
            StiDataSourcesCollection dataSources = new StiDataSourcesCollection(null);

            foreach (DictionaryEntry wizardDataSource in wizardDataSources)
            {
                if (((Hashtable)wizardDataSource.Value)["typeItem"] as string == "BusinessObject")
                {
                    var fullName = wizardDataSource.Key as string;
                    ArrayList nameArray = new ArrayList();
                    nameArray.AddRange(fullName.Split('.'));
                    nameArray.Reverse();
                    StiBusinessObject businessObject = StiDictionaryHelper.GetBusinessObjectByFullName(createdReport, nameArray);
                    if (businessObject != null) businessObjects[fullName] = businessObject;
                }
                else
                {
                    StiDataSource dataSource = createdReport.DataSources[(string)wizardDataSource.Key];
                    if (dataSource != null) dataSources.Add(dataSource);
                }
            }
            if (dataSources.Count == 0 && businessObjects.Count == 0) return createdReport;

            bool first = true;
            string titleText = string.Empty;
            foreach (string orderDataName in orderDataNames)
            {
                StiDataSource dataSource = dataSources[orderDataName];
                StiBusinessObject businessObject = businessObjects[orderDataName] as StiBusinessObject;

                if (dataSource != null || businessObject != null)
                {
                    if (!first) titleText += ", ";
                    titleText += dataSource != null ? dataSource.Alias : businessObject.Alias;
                    first = false;
                }
            }

            #region Create Report Title
            StiReportTitleBand reportTitleBand = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.Bands.ReportTitleBand) as StiReportTitleBand;
            reportTitleBand.Name = "ReportTitle";
            reportTitleBand.Height = AlignToMaxGrid(page, 50, true);
            page.Components.Add(reportTitleBand);

            StiText reportTitleText = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.Text) as StiText;
            reportTitleText.HorAlignment = StiTextHorAlignment.Center;
            reportTitleText.VertAlignment = StiVertAlignment.Center;
            reportTitleText.Font = new Font("Arial", 20, FontStyle.Bold);
            reportTitleText.Name = "ReportTitleText";
            reportTitleText.Text = titleText;
            reportTitleText.Top = 0;
            reportTitleText.Left = 0;
            reportTitleText.Width = page.Width;
            reportTitleText.Height = reportTitleBand.Height;
            reportTitleBand.Components.Add(reportTitleText);
            #endregion
			
            StiDataBand masterDataband = null;

            #region Get all aggregates functions
            var aggrs = new Hashtable();
            foreach (var service in StiOptions.Services.AggregateFunctions.Where(s => s.ServiceEnabled))
            {
                aggrs[service.ServiceName] = service;
            }
            #endregion

            #region DataSources
            int dataSourceIndex = 0;
            foreach (string orderDataName in orderDataNames)
            {
                StiDataSource dataSource = dataSources[orderDataName];
                StiBusinessObject businessObject = businessObjects[orderDataName] as StiBusinessObject;
                if (dataSource == null && businessObject == null) return createdReport;

                StiHeaderBand headerBand = null;
                StiDataBand dataBand = null;
                StiTable table = null;
                StiFooterBand footerBand = null;

                string dataValue = dataSource != null
                    ? StiNameValidator.CorrectName(dataSource.Name) 
                    : businessObject != null 
                        ? StiNameValidator.CorrectBusinessObjectName(businessObject.GetFullName())
                        : null;

                string dataName = (orderDataNames.Count == 1) ? string.Empty : StiNameValidator.CorrectName(dataValue);

                ArrayList columnItems = GetColumnsFromDataSource(orderDataName, wizardDataSources);
                ArrayList groupItems = GetGroupsFromDataSource(orderDataName, wizardDataSources);
                Hashtable totalItems = GetTotalsFromDataSource(orderDataName, wizardDataSources);

                #region Create Header Band
                if ((string)reportOptions["componentType"] == "Data")
                {
                    headerBand = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.Bands.HeaderBand) as StiHeaderBand;
                    headerBand.Name = "Header" + dataName;
                    headerBand.Height = AlignToMaxGrid(page, 20, true);                    
                    page.Components.Add(headerBand);
                }
                #endregion				

                #region Create GroupHeaderBand
                for (int index = 0; index < groupItems.Count; index++)
                {
                    StiGroupHeaderBand groupHeaderBand = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.Bands.GroupHeaderBand) as StiGroupHeaderBand;
                    groupHeaderBand.Name =
                        string.Format("GroupHeader{0}{1}", dataName, index);
                    groupHeaderBand.Height = AlignToMaxGrid(page, 30, true);
                    groupHeaderBand.Condition.Value = "{" + dataValue + "." + groupItems[index] as string + "}";
                    page.Components.Add(groupHeaderBand);

                    StiText groupHeaderText = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.Text) as StiText;
                    groupHeaderText.Text = "{" + dataValue + "." + groupItems[index] as string + "}";
                    groupHeaderText.VertAlignment = StiVertAlignment.Center;
                    groupHeaderText.Font = new Font("Arial", 16, FontStyle.Bold);
                    groupHeaderText.Name =
                        string.Format("GroupHeaderText{0}{1}", dataName, index);
                    groupHeaderText.Top = 0;
                    groupHeaderText.Left = 0;
                    groupHeaderText.Width = page.Width;
                    groupHeaderText.Height = groupHeaderBand.Height;
                    groupHeaderText.CanGrow = true;
                    groupHeaderText.WordWrap = true;
                    groupHeaderBand.Components.Add(groupHeaderText);
                }
                #endregion
                
                #region Create Data Band
                if ((string)reportOptions["componentType"] == "Data")
                {
                    dataBand = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.Bands.DataBand) as StiDataBand;                                        
                    dataBand.Name = "Data" + dataName;
                    if (dataSource != null)
                        dataBand.DataSourceName = dataSource.Name;
                    else if (businessObject != null)
                        dataBand.BusinessObjectGuid = businessObject.Guid;
                    dataBand.Height = (AlignToMaxGrid(page, 20, true));
                    dataBand.MasterComponent = masterDataband;                    
                    page.Components.Add(dataBand);
                    StiReportEdit.SetSortDataProperty(dataBand, GetSortFromDataSource(orderDataName, wizardDataSources));
                    
                    #region Set relation
                    if (relations[orderDataName] != null)
                        if ((bool)((Hashtable)relations[orderDataName])["checked"])
                            dataBand.DataRelationName = (string)((Hashtable)relations[orderDataName])["nameInSource"];
                    #endregion

                    if (dataSourceIndex == 0)
                        masterDataband = dataBand;
                }
                else
                {
                    table = new StiTable();
                    table.Name = "Data" + dataName;
                    table.RowCount = 3;
                    table.HeaderRowsCount = 1;
                    table.FooterRowsCount = 1;
                    table.ColumnCount = columnItems.Count;
                    if (dataSource != null)
                        table.DataSourceName = dataSource.Name;
                    else if (businessObject != null)
                        table.BusinessObjectGuid = businessObject.Guid;
                    page.Components.Add(table);
                    table.Height = (page.GridSize * 9);
                    table.Width = page.Width;
                    table.MasterComponent = masterDataband;
                    table.AutoSizeCells();
                    StiReportEdit.SetSortDataProperty(table, GetSortFromDataSource(orderDataName, wizardDataSources));
                    
                    #region Set relation
                    if (relations[orderDataName] != null)
                        if ((bool)((Hashtable)relations[orderDataName])["checked"])
                            table.DataRelationName = (string)((Hashtable)relations[orderDataName])["nameInSource"];
                    #endregion

                    if (dataSourceIndex == 0)
                        masterDataband = table;
                }
                #endregion

                #region Create Filters
                bool filterOn = (bool)(((Hashtable)wizardDataSources[orderDataName])["filterOn"]);
                StiFilterMode filterMode = ((string)(((Hashtable)wizardDataSources[orderDataName])["filterMode"]) == "And") ? StiFilterMode.And : StiFilterMode.Or;
                StiFilterEngine filterEngine = (StiFilterEngine)Enum.Parse(typeof(StiFilterEngine), ((Hashtable)wizardDataSources[orderDataName])["filterEngine"] as string);

                if ((string)reportOptions["componentType"] == "Data")
                {
                    dataBand.FilterMode = filterMode;
                    StiReportEdit.SetFilterDataProperty(dataBand, GetFiltersFromDataSource(orderDataName, wizardDataSources));
                    dataBand.FilterOn = filterOn;
                    dataBand.FilterEngine = filterEngine;
                }
                else
                {
                    table.FilterMode = filterMode;
                    StiReportEdit.SetFilterDataProperty(table, GetFiltersFromDataSource(orderDataName, wizardDataSources));
                    table.FilterOn = filterOn;
                    table.FilterEngine = filterEngine;
                }
                #endregion
                
                #region Create GroupFooterBand
                for (int index = 0; index < groupItems.Count; index++)
                {
                    StiGroupFooterBand groupFooterBand = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.Bands.GroupFooterBand) as StiGroupFooterBand;
                    groupFooterBand.Name = 
                        string.Format("GroupFooter{0}{1}", dataName, index);
                    groupFooterBand.Height = AlignToMaxGrid(page, 10, true);
                    page.Components.Add(groupFooterBand);
                }
                #endregion
			
                #region Create Footer Band
                if ((string)reportOptions["componentType"] == "Data")
                {
                    footerBand = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.Bands.FooterBand) as StiFooterBand;
                    footerBand.Name = "Footer" + dataName;
                    footerBand.Height = AlignToMaxGrid(page, 20, true);
                    page.Components.Add(footerBand);
                }
                #endregion

                #region Create columns
                if (columnItems.Count > 0)
                {
                    double columnWidth = AlignToGrid(page, page.Width / columnItems.Count, false);
                    if (columnWidth * columnItems.Count > page.Width && columnWidth > createdReport.Info.GridSize * 2)
                    {
                        columnWidth -= createdReport.Info.GridSize;
                    }
                    double pos = 0;

                    int indexHeaderText = 1;
                    int indexDataText = 1;
                    int indexFooterText = 1;

                    if ((string)reportOptions["componentType"] == "Table")
                    {
                        indexHeaderText = 0;
                        indexDataText = table.ColumnCount;
                        indexFooterText = table.ColumnCount * 2;
                    }

                    for (int indexColum = 0; indexColum < columnItems.Count; indexColum++)
                    {
                        string columnFullName = dataValue + "." + columnItems[indexColum];
                        double width = columnWidth;
                        if (indexColum + 1 == columnItems.Count)
                        {
                            width = AlignToGrid(page, page.Width - pos, false);
                            if (width <= 0) width = page.GridSize;
                        }

                        #region DataBand
                        if ((string)reportOptions["componentType"] == "Data")
                        {
                            #region HeaderText
                            StiText headerText = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.Text) as StiText;
                            headerText.VertAlignment = StiVertAlignment.Center;
                            headerText.Font = new Font("Arial", 10, FontStyle.Bold);
                            headerText.Name = string.Format("HeaderText{0}{1}", dataName, indexHeaderText);
                            headerText.Top = 0;
                            headerText.Left = pos;
                            headerText.Width = width;
                            headerText.Height = headerBand.Height;
                            headerText.CanGrow = true;
                            headerText.WordWrap = true;
                            if (columnItems.Count > 1) headerText.GrowToHeight = true;
                            
                            int index = columnFullName.LastIndexOf('.');

                            StiDataColumn column = StiDataBuilder.GetColumnFromPath(
                                columnFullName, createdReport.Dictionary);
                            if (column != null)
                            {
                                headerText.Text = column.Alias;
                            }
                            else
                            {
                                if (index == -1) headerText.Text = columnFullName;
                                else headerText.Text = columnFullName.Substring(index + 1);
                            }

                            indexHeaderText++;
                            headerBand.Components.Add(headerText);
                            #endregion
                            
                            #region Data
                            StiDataColumn dataColumn = StiDataBuilder.GetColumnFromPath(columnFullName, dataBand.Report.Dictionary);
                            #region Image
                            if (dataColumn != null && (dataColumn.Type == typeof(Image) || dataColumn.Type == typeof(byte[])))
                            {
                                StiImage dataImage = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.Image) as StiImage;
                                dataImage.Name = string.Format("DataImage{0}{1}", dataName, indexDataText);
                                dataImage.Top = 0;
                                dataImage.Left = pos;
                                dataImage.Width = width;
                                dataImage.Height = dataBand.Height;
                                dataImage.DataColumn = columnFullName;
                                dataImage.CanGrow = true;
                                if (columnItems.Count > 1) dataImage.GrowToHeight = true;
                                dataBand.Components.Add(dataImage);
                            }
                            #endregion

                            #region CheckBox
                            else if (dataColumn != null && dataColumn.Type == typeof(bool))
                            {
                                Stimulsoft.Report.Components.StiCheckBox dataCheck = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.CheckBox) as Stimulsoft.Report.Components.StiCheckBox;
                                dataCheck.Name = string.Format("DataCheck{0}{1}", dataName, indexDataText);
                                dataCheck.Top = 0;
                                dataCheck.Left = pos;
                                dataCheck.Width = width;
                                dataCheck.Height = dataBand.Height;
                                dataCheck.Checked.Value = "{" + columnFullName + "}";
                                if (columnItems.Count > 1) dataCheck.GrowToHeight = true;
                                dataBand.Components.Add(dataCheck);
                            }
                            #endregion

                            #region Text
                            else
                            {
                                StiText dataText = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.Text) as StiText;
                                dataText.Name = string.Format("DataText{0}{1}", dataName, indexDataText);
                                dataText.TextQuality = StiTextQuality.Wysiwyg;
                                dataText.VertAlignment = StiVertAlignment.Center;
                                dataText.Top = 0;
                                dataText.Left = pos;
                                dataText.Width = width;
                                dataText.Height = dataBand.Height;
                                dataText.Text = "{" + columnFullName as string + "}";
                                dataText.CanGrow = true;
                                if (columnItems.Count > 1) dataText.GrowToHeight = true;
                                dataText.WordWrap = true;
                                dataBand.Components.Add(dataText);
                            }
                            #endregion

                            indexDataText++;
                            #endregion

                            #region FooterText
                            string totalStr = string.Empty;
                            if (totalItems.Count > 0 && totalItems[columnItems[indexColum]] != null)
                            {
                                totalStr = (string)totalItems[columnItems[indexColum]];
                                StiAggregateFunctionService service = aggrs[totalStr] as
                                    StiAggregateFunctionService;

                                if (service != null && service.RecureParam) totalStr += "(" + columnFullName + ")";
                                else totalStr += "()";

                                StiText footerText = StiActivator.CreateObject(StiOptions.Designer.ComponentsTypes.SimpleComponents.Text) as StiText;
                                footerText.VertAlignment = StiVertAlignment.Center;
                                footerText.HorAlignment = StiTextHorAlignment.Right;
                                footerText.Font = new Font("Arial", 10, FontStyle.Bold);
                                footerText.Name = string.Format("FooterText{0}{1}", dataName, indexFooterText);
                                footerText.Top = 0;
                                footerText.Left = pos;
                                footerText.Width = width;
                                footerText.Height = footerBand.Height;
                                footerText.CanGrow = true;
                                if (columnItems.Count > 1) footerText.GrowToHeight = true;
                                footerText.WordWrap = true;

                                footerText.Text = "{" + totalStr + "}";
                                indexFooterText++;
                                footerBand.Components.Add(footerText);
                            }
                            #endregion
                        }
                        #endregion

                        #region Table
                        else
                        {
                            #region HeaderCell
                            StiTableCell headerCell = (StiTableCell)table.Components[indexHeaderText];
                            headerCell.VertAlignment = StiVertAlignment.Center;
                            headerCell.Font = new Font("Arial", 10, FontStyle.Bold);
                            headerCell.WordWrap = true;

                            int index = columnFullName.LastIndexOf('.');

                            StiDataColumn column = StiDataBuilder.GetColumnFromPath(
                                columnFullName, createdReport.Dictionary);
                            if (column != null)
                            {
                                headerCell.Text = column.Alias;
                            }
                            else
                            {
                                if (index == -1) headerCell.Text = columnFullName;
                                else headerCell.Text = columnFullName.Substring(index + 1);
                            }

                            indexHeaderText++;
                            #endregion

                            #region DataCell
                            StiDataColumn dataColumn = StiDataBuilder.GetColumnFromPath(columnFullName, table.Report.Dictionary);

                            #region Image
                            if (dataColumn != null && (dataColumn.Type == typeof(Image) || dataColumn.Type == typeof(byte[])))
                            {
                                ((IStiTableCell)table.Components[indexDataText]).CellType = StiTablceCellType.Image;
                                StiTableCellImage dataCell = (StiTableCellImage)table.Components[indexDataText];
                                dataCell.VertAlignment = StiVertAlignment.Center;
                                dataCell.CanGrow = true;
                                dataCell.DataColumn = columnFullName;
                            }
                            #endregion

                            #region CheckBox
                            else if (dataColumn != null && dataColumn.Type == typeof(bool))
                            {
                                ((IStiTableCell)table.Components[indexDataText]).CellType = StiTablceCellType.CheckBox;
                                StiTableCellCheckBox dataCell = (StiTableCellCheckBox)table.Components[indexDataText];
                                dataCell.CanGrow = true;
                                dataCell.Checked.Value = "{" + columnFullName + "}";
                            }
                            #endregion

                            #region Text
                            else
                            {
                                StiTableCell dataCell = (StiTableCell)table.Components[indexDataText];
                                dataCell.CanGrow = true;
                                dataCell.VertAlignment = StiVertAlignment.Center;
                                dataCell.Text = "{" + columnFullName as string + "}";
                                dataCell.WordWrap = true;
                            }
                            #endregion

                            indexDataText++;
                            #endregion

                            #region FooterCell
                            string totalStr = string.Empty;
                            if (totalItems.Count > 0 && totalItems[columnItems[indexColum]] != null)
                            {
                                totalStr = (string)totalItems[columnItems[indexColum]];
                                StiAggregateFunctionService service = aggrs[totalStr] as
                                    StiAggregateFunctionService;

                                if (service != null && service.RecureParam) totalStr += "(" + columnFullName + ")";
                                else totalStr += "()";

                                StiTableCell footerCell = (StiTableCell)table.Components[indexFooterText];
                                footerCell.VertAlignment = StiVertAlignment.Center;
                                footerCell.HorAlignment = StiTextHorAlignment.Right;
                                footerCell.Font = new Font("Arial", 10, FontStyle.Bold);
                                footerCell.WordWrap = true;

                                footerCell.Text = "{" + totalStr + "}";
                                indexFooterText++;
                            }
                            #endregion
                        }
                        #endregion
                        pos += columnWidth;
                    }
                }
                #endregion

                dataSourceIndex++;
            }
            #endregion

            #region Aplly styles collection
            string[] theme = ((string)reportOptions["theme"]).Split('_');
            Color colorBase = Color.Empty;
            string themeName = string.Empty;
            string themeColor = theme[0];
            
            if (themeColor != "None")
            {
                int themePercent = int.Parse(theme[1]);
                switch (theme[0])
                {
                    case "Red": { colorBase = Color.FromArgb(144, 60, 57); themeName = StiLocalization.Get("PropertyColor", "Red"); break; }
                    case "Green": { colorBase = Color.FromArgb(117, 140, 72); themeName = StiLocalization.Get("PropertyColor", "Green"); break; }
                    case "Blue": { colorBase = Color.FromArgb(69, 98, 135); themeName = StiLocalization.Get("PropertyColor", "Blue"); break; }
                    case "Gray": { colorBase = Color.FromArgb(75, 75, 75); themeName = StiLocalization.Get("PropertyColor", "Gray"); break; }
                }

                StiStylesCreator creator = new StiStylesCreator(createdReport);
                string text = themeName + " " + theme[1] + "%";
                Color color = StiColorUtils.Light(colorBase, (byte)(Math.Abs((themePercent - 100) / 25) * 45));
                List<StiBaseStyle> styles = creator.CreateStyles(text, color);
                StiStylesCollection stylesCollection = new StiStylesCollection();

                foreach (StiBaseStyle style in styles)
                {
                    stylesCollection.Add(style);
                }
                
                if (stylesCollection != null)
                {
                    foreach (StiBaseStyle style in stylesCollection)
                    {
                        createdReport.Styles.Add(style);
                    }

                    string nameStylesCollection = null;
                    foreach (StiBaseStyle style in stylesCollection)
                    {
                        nameStylesCollection = style.CollectionName;
                    }

                    createdReport.ApplyStyleCollection(nameStylesCollection);
                }
            }
            #endregion

            return createdReport;
        }
    }
}