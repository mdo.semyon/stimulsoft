﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.Mvc;
using System;
using System.ComponentModel;
using System.Web.Mvc;
using Stimulsoft.Report.Web;
using System.Web.Caching;

namespace Stimulsoft.Report.MvcMobile
{
    [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiMvcDesigner component instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class StiMvcMobileDesigner : StiMvcDesigner
    {
        [Obsolete("This class is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiCacheHelper class instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class StiCacheHelper : Web.StiCacheHelper
        {
            public override object GetObject(string guid, StiServerCacheMode cacheMode, TimeSpan timeout, CacheItemPriority priority)
            {
                return GetObjectFromCache(guid);
            }

            public override void SaveObject(object obj, string guid, StiServerCacheMode cacheMode, TimeSpan timeout, CacheItemPriority priority)
            {
                SaveObjectToCache(obj, guid);
            }
        }

        public StiMvcMobileDesigner(HtmlHelper htmlHelper, string id, StiMvcDesignerOptions options) : base(htmlHelper, id, options)
        {
        }
    }
}
