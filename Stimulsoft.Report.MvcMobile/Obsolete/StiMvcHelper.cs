﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Web.Mvc;
using System.Web.UI;
using System.IO;
using System.ComponentModel;

namespace Stimulsoft.Report.MvcMobile
{
    public static class StiMvcBaseHelper
    {
        private static StiMvcHelper mvcHelper = new StiMvcHelper();
        [Obsolete("This html helper is obsolete. It will be removed in next versions. Please use the Stimulsoft() html helper forom Stimulsoft.Report.Mvc library.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static StiMvcHelper Stimulsoft(this HtmlHelper htmlHelper)
        {
            mvcHelper.htmlHelper = htmlHelper;
            return mvcHelper;
        }

        [Obsolete("This html helper is obsolete. It will be removed in next versions. Please use the Stimulsoft() html helper forom Stimulsoft.Report.Mvc library.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static StiMvcHelper StimulsoftMobile(this HtmlHelper htmlHelper)
        {
            mvcHelper.htmlHelper = htmlHelper;
            return mvcHelper;
        }
    }

    public class StiMvcHelper
    {
        #region Fields

        internal HtmlHelper htmlHelper;

        #endregion

        #region Controls

        #region MvcMobileViewer

        [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiMvcViewer component instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MvcHtmlString StiMvcMobileViewer()
        {
            return StiMvcMobileViewer("MvcMobileViewer");
        }

        [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiMvcViewer component instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MvcHtmlString StiMvcMobileViewer(StiMvcMobileViewerOptions options)
        {
            return StiMvcMobileViewer("MvcMobileViewer", options);
        }

        [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiMvcViewer component instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MvcHtmlString StiMvcMobileViewer(string ID)
        {
            return StiMvcMobileViewer(ID, new StiMvcMobileViewerOptions());
        }

        [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiMvcViewer component instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MvcHtmlString StiMvcMobileViewer(string ID, StiMvcMobileViewerOptions options)
        {
            if (options == null)
                throw new Exception("Failed to initialize the StiMvcViewer component. Please define the StiMvcViewerOptions to work correctly with the viewer.");

            if (string.IsNullOrEmpty(options.Actions.ViewerEvent))
                throw new Exception("Failed to initialize the StiMvcViewer component. Please define the ViewerEvent action to work correctly with the viewer.");

            StiMvcMobileViewer viewer = new StiMvcMobileViewer(htmlHelper, ID, options);
            var writer = new StringWriter();
            var htmlWriter = new HtmlTextWriter(writer, string.Empty);
            htmlWriter.NewLine = string.Empty;
            viewer.RenderControl(htmlWriter);
            htmlWriter.Dispose();

            return MvcHtmlString.Create(writer.ToString());
        }

        [Obsolete("This method is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MvcHtmlString RenderMvcMobileViewerScripts()
        {
            return MvcHtmlString.Create(string.Empty);
        }

        #endregion

        #region MvcMobileDesigner

        [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiMvcDesigner component instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MvcHtmlString StiMvcMobileDesigner()
        {
            return StiMvcMobileDesigner("MvcMobileDesigner");
        }

        [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiMvcDesigner component instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MvcHtmlString StiMvcMobileViewer(StiMvcMobileDesignerOptions options)
        {
            return StiMvcMobileDesigner("MvcMobileDesigner", options);
        }

        [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiMvcDesigner component instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MvcHtmlString StiMvcMobileDesigner(string ID)
        {
            return StiMvcMobileDesigner(ID, new StiMvcMobileDesignerOptions());
        }

        [Obsolete("This component is obsolete. It will be removed in next versions. Please use the StiMvcDesigner component instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MvcHtmlString StiMvcMobileDesigner(string ID, StiMvcMobileDesignerOptions options)
        {
            if (options == null)
                throw new Exception("Failed to initialize the StiMvcDesigner component. Please define the StiMvcDesignerOptions to work correctly with the designer.");

            if (string.IsNullOrEmpty(options.Actions.DesignerEvent))
                throw new Exception("Failed to initialize the StiMvcDesigner component. Please define the DesignerEvent action to work correctly with the designer.");

            StiMvcMobileDesigner designer = new StiMvcMobileDesigner(htmlHelper, ID, options);
            var writer = new StringWriter();
            var htmlWriter = new HtmlTextWriter(writer, string.Empty);
            htmlWriter.NewLine = string.Empty;
            designer.RenderControl(htmlWriter);
            htmlWriter.Dispose();

            return MvcHtmlString.Create(writer.ToString());
        }

        [Obsolete("This method is obsolete. It is no longer used and will be removed in next versions.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public MvcHtmlString RenderMvcMobileDesignerScripts()
        {
            return MvcHtmlString.Create(string.Empty);
        }

        #endregion

        #endregion
    }
}
