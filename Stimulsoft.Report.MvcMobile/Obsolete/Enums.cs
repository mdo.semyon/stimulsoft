﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;

namespace Stimulsoft.Report.MvcMobile
{
    #region StiToolbarAction
    [Obsolete("This enum is obsolete. It is no longer used and will be removed in next versions.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    internal enum StiToolbarAction
    {
        None,

        PrintPdf,
        PrintWithPreview,
        PrintWithoutPreview,
        SaveDocument,
        SavePdf,
        SaveXps,
        SavePpt2007,
        SaveHtml,
        SaveHtml5,
        SaveMht,
        SaveText,
        SaveRtf,
        SaveWord2007,
        SaveOdt,
        SaveExcel,
        SaveExcelXml,
        SaveExcel2007,
        SaveOds,
        SaveCsv,
        SaveDbf,
        SaveXml,
        SaveDif,
        SaveSylk,
        SaveImageBmp,
        SaveImageGif,
        SaveImageJpeg,
        SaveImagePcx,
        SaveImagePng,
        SaveImageTiff,
        SaveImageEmf,
        SaveImageSvg,
        SaveImageSvgz,
        Zoom25,
        Zoom50,
        Zoom75,
        Zoom100,
        Zoom150,
        Zoom200,
        ViewModeOnePage,
        ViewModeWholeReport,

        Parameters,
        Bookmarks,
        PrevPage,
        FirstPage,
        LastPage,
        NextPage,
        Design
    }
    #endregion

    #region StiContentAlignment
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiContentAlignment enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiContentAlignment
    {
        Left,
        Center,
        Right,
        Default
    }
    #endregion

    #region StiCacheMode
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiServerCacheMode enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiCacheMode
    {
        None,
        Page,
        Session,
        PageString,
        SessionString
    }
    #endregion

    #region StiWebViewMode
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiWebViewMode enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiWebViewMode
    {
        OnePage,
        WholeReport
    }
    #endregion

    #region StiPrintDestination
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiPrintDestination enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiPrintDestination
    {
        Pdf,
        Direct,
        WithPreview,
        PopupMenu
    }
    #endregion

    #region StiTheme
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiViewerTheme or Stimulsoft.Report.Web.StiDesignerTheme enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiTheme
    {
        Purple,
        Blue,
        Office2013,
        Office2013DarkGrayBlue,
        Office2013DarkGrayCarmine,
        Office2013DarkGrayGreen,
        Office2013DarkGrayOrange,
        Office2013DarkGrayPurple,
        Office2013DarkGrayTeal,
        Office2013DarkGrayViolet,
        Office2013LightGrayBlue,
        Office2013LightGrayCarmine,
        Office2013LightGrayGreen,
        Office2013LightGrayOrange,
        Office2013LightGrayPurple,
        Office2013LightGrayTeal,
        Office2013LightGrayViolet,
        Office2013WhiteBlue,
        Office2013WhiteCarmine,
        Office2013WhiteGreen,
        Office2013WhiteOrange,
        Office2013WhitePurple,
        Office2013WhiteTeal,
        Office2013WhiteViolet,
        Office2013VeryDarkGrayBlue,
        Office2013VeryDarkGrayCarmine,
        Office2013VeryDarkGrayGreen,
        Office2013VeryDarkGrayOrange,
        Office2013VeryDarkGrayPurple,
        Office2013VeryDarkGrayTeal,
        Office2013VeryDarkGrayViolet
    }
    #endregion

    #region StiShapeId
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiShapeId enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    internal enum StiShapeId
    {
        StiShapeTypeService = 0,
        StiSnipDiagonalSideCornerRectangleShapeType = 1,
        StiSnipSameSideCornerRectangleShapeType = 2,
        StiTrapezoidShapeType = 3,
        StiRegularPentagonShapeType = 4,
        StiPlusShapeType = 5,
        StiParallelogramShapeType = 6,
        StiMultiplyShapeType = 7,
        StiMinusShapeType = 8,
        StiFrameShapeType = 9,
        StiFlowchartSortShapeType = 10,
        StiFlowchartPreparationShapeType = 11,
        StiFlowchartOffPageConnectorShapeType = 12,
        StiFlowchartManualInputShapeType = 13,
        StiFlowchartDecisionShapeType = 14,
        StiFlowchartCollateShapeType = 15,
        StiFlowchartCardShapeType = 16,
        StiEqualShapeType = 17,
        StiDivisionShapeType = 18,
        StiChevronShapeType = 19,
        StiBentArrowShapeType = 20,
        StiComplexArrowShapeType = 21,
        StiVerticalLineShapeType = 22,
        StiTriangleShapeType = 23,
        StiTopAndBottomLineShapeType = 24,
        StiRoundedRectangleShapeType = 25,
        StiRectangleShapeType = 26,
        StiOvalShapeType = 27,
        StiLeftAndRightLineShapeType = 28,
        StiHorizontalLineShapeType = 29,
        StiDiagonalUpLineShapeType = 30,
        StiDiagonalDownLineShapeType = 31,
        StiArrowShapeType = 32,
    }
    #endregion

    #region BarCodeGeomId
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.BarCodeGeomId enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum BarCodeGeomId
    {
        BaseTransform,
        BaseFillRectangle,
        BaseFillRectangle2D,
        BaseDrawRectangle,
        BaseDrawString
    }
    #endregion

    #region StiRequestFromUserType
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiRequestFromUserType enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiRequestFromUserType
    {
        ListBool,
        ListChar,
        ListDateTime,
        ListTimeSpan,
        ListDecimal,
        ListFloat,
        ListDouble,
        ListByte,
        ListShort,
        ListInt,
        ListLong,
        ListGuid,
        ListString,
        RangeChar,
        RangeDateTime,
        RangeDouble,
        RangeFloat,
        RangeDecimal,
        RangeGuid,
        RangeByte,
        RangeShort,
        RangeInt,
        RangeLong,
        RangeString,
        RangeTimeSpan,
        ValueBool,
        ValueChar,
        ValueDateTime,
        ValueFloat,
        ValueDouble,
        ValueDecimal,
        ValueGuid,
        ValueImage,
        ValueString,
        ValueTimeSpan,
        ValueShort,
        ValueInt,
        ValueLong,
        ValueSbyte,
        ValueUshort,
        ValueUint,
        ValueUlong,
        ValueByte,
        ValueNullableBool,
        ValueNullableChar,
        ValueNullableDateTime,
        ValueNullableFloat,
        ValueNullableDouble,
        ValueNullableDecimal,
        ValueNullableGuid,
        ValueNullableTimeSpan,
        ValueNullableShort,
        ValueNullableInt,
        ValueNullableLong,
        ValueNullableSbyte,
        ValueNullableUshort,
        ValueNullableUint,
        ValueNullableUlong,
        ValueNullableByte
    }
    #endregion

    #region StiDesignerCacheMode
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiServerCacheMode enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiDesignerCacheMode
    {
        Page,
        Session
    }
    #endregion

    #region StiDictionaryImagesCacheID
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiImagesID enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiImagesID
    {
        BusinessObject = 0,
        CalcColumn,
        CalcColumnBinary,
        CalcColumnBool,
        CalcColumnChar,
        CalcColumnDateTime,
        CalcColumnDecimal,
        CalcColumnFloat,
        CalcColumnImage,
        CalcColumnInt,
        CalcColumnString,
        Class,
        Close,
        ColumnsOrder,
        Connection,
        ConnectionFail,
        DataColumn,
        DataColumnBinary,
        DataColumnBool,
        DataColumnChar,
        DataColumnDateTime,
        DataColumnDecimal,
        DataColumnFloat,
        DataColumnImage,
        DataColumnInt,
        DataColumnString,
        DataSource,
        DataSources,
        DataStore,
        DataTable,
        DataTables,
        Folder,
        Format,
        FormatBoolean,
        FormatCurrency,
        FormatDate,
        FormatGeneral,
        FormatNumber,
        FormatPercentage,
        FormatTime,
        Function,
        HtmlTag,
        LabelType,
        LockedCalcColumn,
        LockedCalcColumnBinary,
        LockedCalcColumnBool,
        LockedCalcColumnChar,
        LockedCalcColumnDateTime,
        LockedCalcColumnDecimal,
        LockedCalcColumnFloat,
        LockedCalcColumnImage,
        LockedCalcColumnInt,
        LockedCalcColumnString,
        LockedConnection,
        LockedDataColumn,
        LockedDataColumnBinary,
        LockedDataColumnBool,
        LockedDataColumnChar,
        LockedDataColumnDateTime,
        LockedDataColumnDecimal,
        LockedDataColumnFloat,
        LockedDataColumnImage,
        LockedDataColumnInt,
        LockedDataColumnString,
        LockedDataSource,
        LockedFolder,
        LockedParameter,
        LockedRelation,
        LockedVariable,
        LockedVariableBinary,
        LockedVariableBool,
        LockedVariableChar,
        LockedVariableDateTime,
        LockedVariableDecimal,
        LockedVariableFloat,
        LockedVariableImage,
        LockedVariableInt,
        LockedVariableString,
        Namespace,
        Parameter,
        Property,
        Queries,
        Query,
        RecentConnection,
        Relation,
        StoredProcedure,
        StoredProcedures,
        SystemVariable,
        SystemVariableColumn,
        SystemVariableGroupLine,
        SystemVariableIsFirstPage,
        SystemVariableIsFirstPageThrough,
        SystemVariableIsLastPage,
        SystemVariableIsLastPageThrough,
        SystemVariableLine,
        SystemVariableLineABC,
        SystemVariableLineRoman,
        SystemVariableLineThrough,
        SystemVariablePageNofM,
        SystemVariablePageNofMThrough,
        SystemVariablePageNumber,
        SystemVariablePageNumberThrough,
        SystemVariableReportAlias,
        SystemVariableReportAuthor,
        SystemVariableReportChanged,
        SystemVariableReportCreated,
        SystemVariableReportDescription,
        SystemVariableReportName,
        SystemVariables,
        SystemVariableTime,
        SystemVariableToday,
        SystemVariableTotalPageCount,
        SystemVariableTotalPageCountThrough,
        UndefinedConnection,
        UndefinedDataSource,
        Variable,
        VariableBinary,
        VariableBool,
        VariableChar,
        VariableDateTime,
        VariableDecimal,
        VariableFloat,
        VariableImage,
        VariableInt,
        VariableString,
        View,
        Views,
        LockedVariableListBool,
        LockedVariableListChar,
        LockedVariableListDateTime,
        LockedVariableListDecimal,
        LockedVariableListFloat,
        LockedVariableListImage,
        LockedVariableListInt,
        LockedVariableListString,
        LockedVariableRangeChar,
        LockedVariableRangeDateTime,
        LockedVariableRangeDecimal,
        LockedVariableRangeFloat,
        LockedVariableRangeInt,
        LockedVariableRangeString,
        VariableListBool,
        VariableListChar,
        VariableListDateTime,
        VariableListDecimal,
        VariableListFloat,
        VariableListImage,
        VariableListInt,
        VariableListString,
        VariableRangeChar,
        VariableRangeDateTime,
        VariableRangeDecimal,
        VariableRangeFloat,
        VariableRangeInt,
        VariableRangeString
    }
    #endregion

    #region StiInterfaceType
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiInterfaceType enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiInterfaceType
    {
        Auto,
        Mouse,
        Touch
    }
    #endregion

    #region StiDesignerPermissions
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiDesignerPermissions enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Flags]
    public enum StiDesignerPermissions
    {
        /// <summary>
        /// Deny all.
        /// </summary>
        None = 0,

        /// <summary>
        /// Allows to create an item.
        /// </summary>
        Create = 1,

        /// <summary>
        /// Allows to delete an item.
        /// </summary>
        Delete = 2,

        /// <summary>
        /// Allows to modify an item.
        /// </summary>
        Modify = 4,

        /// <summary>
        /// Allows to view an item.
        /// </summary>
        View = 8,

        /// <summary>
        /// Allows modify and view an item.
        /// </summary>
        ModifyView = Modify + View,

        /// <summary>
        /// Allow any action with an item.
        /// </summary>
        All = Create + Delete + Modify + View
    }
    #endregion

    #region StiDesignerComponents
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiDesignerComponents enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiDesignerComponents
    {
        StiText,
        StiTextInCells,
        StiRichText,
        StiImage,
        StiBarCode,
        StiPanel,
        StiClone,
        StiCheckBox,
        StiSubReport,
        StiZipCode,
        StiTable,
        StiCrossTab,
        StiCrossGroupHeaderBand,
        StiCrossGroupFooterBand,
        StiCrossHeaderBand,
        StiCrossFooterBand,
        StiCrossDataBand,
        StiReportTitleBand,
        StiReportSummaryBand,
        StiPageHeaderBand,
        StiPageFooterBand,
        StiGroupHeaderBand,
        StiGroupFooterBand,
        StiHeaderBand,
        StiFooterBand,
        StiColumnHeaderBand,
        StiColumnFooterBand,
        StiDataBand,
        StiHierarchicalBand,
        StiChildBand,
        StiEmptyBand,
        StiOverlayBand
    }
    #endregion

    #region StiSaveMode
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiSaveMode enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiSaveMode
    {
        Hidden,
        Visible,
        NewWindow
    }
    #endregion

    #region StiFirstDayOfWeek
    [Obsolete("This enum is obsolete. It will be removed in next versions. Please use the Stimulsoft.Report.Web.StiFirstDayOfWeek enum instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public enum StiFirstDayOfWeek
    {
        Monday,
        Sunday
    }
    #endregion
}
