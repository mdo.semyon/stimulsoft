﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Drawing;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Base.Dashboard
{
#if NETCORE
    using System = global::System;
#endif

    public class StiElementConsts
    {
        #region class Font
        public class Font
        {
            #region Properties
            public string Name { get; }

            public float Size { get; }

            public Color Color { get; }

            public Color SelectedColor = Color.White;

            public bool IsBold { get; }
            #endregion

            #region Methods
            public System.Drawing.Font GetGdiFont(double zoom = 1f, float? fontSize = null)
            {
                var style = FontStyle.Regular;
                if (IsBold) style |= FontStyle.Bold;

                return new System.Drawing.Font(Name, fontSize != null ? fontSize.Value * (float)zoom : Size * (float)zoom, style);
            }

            public System.Drawing.Font GetCachedGdiFont()
            {
                if (cachedFont == null)
                {
                    cachedFont = GetGdiFont(1);
                }
                return cachedFont;
            }
            #endregion

            #region Fields
            private System.Drawing.Font cachedFont;
            #endregion

            public Font(string name, float size, Color color, bool isBold = false)
            {
                this.Name = name;
                this.Size = size;
                this.Color = color;
                this.IsBold = isBold;
            }
        }
        #endregion

        public static class Table
        {
            public static Font Font = new Font("Arial", 10, Color.Black);

            public static Color BorderColor = Color.LightGray;

            public static class Header
            {
                public static Color SelectedBackgroundColor = Color.Gainsboro;

                public static Color MouseOverBackgroundColor = Color.FromArgb(236, 236, 236);

                public static Color BackgroundColor = Color.FromArgb(255, 240, 240, 240);

                public static int Height = 28;
            }

            public static class Cell
            {
                public static Color BackgroundColor = Color.White;

                public static Color BackgroundColorInterlaced = Color.FromArgb(0xff, 0xf9, 0xf9, 0xf9);

                public static Color ColorDataBarsOverlapped = Color.FromArgb(0xff, 0x33, 0x5e, 0x96);

                public static Color ColorDataBarsPositive = Color.FromArgb(0xff, 0x63, 0x8e, 0xc6);

                public static Color ColorDataBarsNegative = Color.FromArgb(0xff, 0xff, 0, 0);

                public static Color ColorWinLossPositive = Color.FromArgb(0xff, 0x63, 0x8e, 0xc6);

                public static Color ColorWinLossNegative = Color.FromArgb(0xff, 0xff, 0, 0);

                public static Color ColorSparkline = Color.FromArgb(0xff, 0x53, 0x7e, 0xb6);

                public static int Height = 28;
            }

            public static class CheckBoxCell
            {
                public static Color CheckColor = Color.Gray;

                public static Color IndeterminateCheckColor = Color.LightGray;

                public static Color SelectedBorderColor = Color.White;

                public static Color SelectedIndeterminateCheckColor = Color.White;
            }
        }

        public static class Chart
        {
            public static Font Font = new Font("Arial", 12, Color.FromArgb(255, 140, 140, 140));
        }

        public static class Highlight
        {
            public static Color Color = Color.FromArgb(0xff, 0x2b, 0x57, 0x9a);
        }

        public static class DragDrop
        {
            public static Font Font = new Font("Arial", 8, Color.DimGray);
        }

        public static class Pivot2
        {
            public static Font Font = new Font("Arial", 10, Color.Black);

            public static class Header
            {

                public static Color BorderColor = Color.LightGray;

                public static Color BackgroundColor = Color.FromArgb(0xff, 0xf9, 0xf9, 0xf9);

                public static int Height = 30;

                public static StiBorderSides BorderSide = StiBorderSides.All;

                public static Color ArrowColor = Color.FromArgb(140, 140, 140);
            }

            public static class Cell
            {
                public static Color BackgroundColor = Color.White;

                public static Color BorderColor = Color.FromArgb(128, Color.LightGray);

                public static int Height = 30;

                public static StiBorderSides BorderSide = StiBorderSides.Left | StiBorderSides.Right;
            }
        }
    }
}
