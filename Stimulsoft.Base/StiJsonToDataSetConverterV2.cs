﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Collections;
using System.Data;
using Stimulsoft.Base.Json;
using Stimulsoft.Base.Json.Linq;
using System.Collections.Generic;
using System.Xml.Linq;
using System.IO;
using Stimulsoft.Base.Helpers;
using System.Linq;
using System;
using System.Text.RegularExpressions;

namespace Stimulsoft.Base
{
    public static class StiJsonToDataSetConverterV2
    {
        private class StiJsonMetaData
        {
            #region Properties
            public string CollectionName { get; set; }
            public string Address { get; set; }
            public string Cast { get; set; }
            public object Object { get; set; }
            #endregion
        }

        private class StiJsonRelationData
        {
            #region Properties
            public DataColumn ChildColumn { get; set; }
            public DataTable ChildTable { get; set; }
            public string ParentTableName { get; set; }
            #endregion
        }

        private class StiJsonRelationIdData
        {
            #region Properties
            public object NewValue { get; set; }
            public string RelationId { get; set; }
            #endregion
        }

        #region Fields
        private static Hashtable objectCorrection;
        #endregion

        #region Methods

        public static DataSet GetDataSet(JToken json)
        {
            return GetDataSet(json, false);
        }
        
        public static DataSet GetDataSet(JToken json, bool relationDirectionChildToParent)
        {
            objectCorrection = new Hashtable();
            json = CorrectJson(json);
            
            var collections = new List<StiJsonMetaData>();

            var dataSet = new DataSet();

            FillCollection(collections, "", "", "", json, true);

            var addresses = new Hashtable();
            var tables = new Hashtable();
            var relations = new Hashtable();
            var lastIndexesForAddress = new Hashtable();
            var allRelationsColumn = new List<DataColumn>();

            foreach (StiJsonMetaData collection in collections)
            {
                if (collection == null) continue;
                var table = tables[collection.Cast] as List<StiJsonMetaData>;

                if (table == null) table = new List<StiJsonMetaData>();
                lastIndexesForAddress[table] = 0;
                addresses[collection.Address] = table;
                table.Add(collection);
                tables[collection.Cast] = table;
                if (((StiJsonRelationIdData)((JObject)collection.Object).Tag).RelationId != "-1" && ((StiJsonRelationIdData)((JObject)collection.Object).Tag).RelationId != null && ((StiJsonRelationIdData)((JObject)collection.Object).Tag).RelationId.IndexOf("#relation#") == 0)
                {
                    var relationParentCast = ((StiJsonRelationIdData)((JObject)collection.Object).Tag).RelationId.Replace("#relation#", "");
                    if (tables.ContainsKey(relationParentCast)) ((StiJsonRelationIdData)((JObject)collection.Object).Tag).RelationId = ((List<StiJsonMetaData>)tables[relationParentCast]).Count.ToString();
                    else ((StiJsonRelationIdData)((JObject)collection.Object).Tag).RelationId = "0";
                }
            }

            foreach (var cast in tables.Keys)
            {
                var table = tables[cast] as List<StiJsonMetaData>;
                var dataTableRowsCount = 0;
                foreach (StiJsonMetaData item in table)
                {
                    var tableName = item.CollectionName;
                    var dataTable = dataSet.Tables[tableName];
                    if (dataTable == null)
                    {
                        dataTable = new DataTable(tableName);
                        dataSet.Tables.Add(dataTable);
                    }

                    var dataRow = dataTable.NewRow();
                    dataTableRowsCount++;
                    foreach (var itemObject in (JContainer)item.Object)
                    {
                        JProperty columnName = itemObject as JProperty;
                        if (columnName != null)
                        {
                            var dataColumnType = typeof(string);
                            var dataColumn = dataTable.Columns[columnName.Name];
                            if (dataColumn == null)
                            {
                                if (columnName.Name != "relationId")
                                {
                                    if (columnName.Value.Type == JTokenType.Integer) dataColumnType = typeof(decimal);
                                    else if (columnName.Value.Type == JTokenType.Boolean) dataColumnType = typeof(bool);
                                }
                                dataColumn = new DataColumn(columnName.Name, dataColumnType);
                                dataTable.Columns.Add(dataColumn);
                            }

                            var dataHelp = columnName.Tag as StiJsonRelationIdData;
                            var columnValue = dataHelp != null && dataHelp.NewValue != null ? dataHelp.NewValue : ((Stimulsoft.Base.Json.Linq.JValue)columnName.Value).Value;


                            var itemDataHelp = ((JContainer)item.Object).Tag as StiJsonRelationIdData;

                            if (columnName.Name == "relationId" && itemDataHelp != null && itemDataHelp.RelationId != null)
                            {
                                var dataColumnRalationId = dataTable.Columns["relationId"];
                                if (dataColumnRalationId == null)
                                {
                                    dataColumnRalationId = new DataColumn("relationId");
                                    dataTable.Columns.Add(dataColumnRalationId);
                                    allRelationsColumn.Add(dataColumnRalationId);
                                }

                                if (itemDataHelp.RelationId == "-1")
                                {
                                    itemDataHelp.RelationId = "0";

                                    var table1 = addresses[item.Address] as List<StiJsonMetaData>;
                                    for (int index = (int)lastIndexesForAddress[table1]; index < table1.Count; index++)
                                    {
                                        var item1 = table1[index] as StiJsonMetaData;

                                        if (item1.Address == item.Address)
                                        {
                                            itemDataHelp.RelationId = index.ToString();
                                            lastIndexesForAddress[table1] = index;
                                            break;
                                        }
                                    }
                                }
                                dataRow[dataColumnRalationId] = itemDataHelp.RelationId;
                            }
                            else if (dataColumnType == typeof(string) && columnValue != null && columnValue.ToString().IndexOf("#relation#") == 0)
                            {
                                dataRow[dataColumn] = (dataTableRowsCount - 1).ToString();
                                var address = columnValue.ToString().Replace("#relation#", "");
                                if (addresses.ContainsKey(address))
                                {
                                    var list = addresses[address] as List<StiJsonMetaData>;
                                    if (list.Count > 0)
                                    {
                                        var dataHelpRelation = new StiJsonRelationData()
                                        {
                                            ChildColumn = dataColumn,
                                            ChildTable = dataTable,
                                            ParentTableName = list[0].CollectionName
                                        };

                                        relations[dataTable.TableName + "." + dataColumn.ColumnName] = dataHelpRelation;
                                    }
                                }
                            }
                            else
                            {
                                if (columnValue == null) dataRow[dataColumn] = DBNull.Value;
                                else dataRow[dataColumn] = columnValue;
                            }
                        }
                    }
                    dataTable.Rows.Add(dataRow);
                }
            }

            foreach (StiJsonRelationData relation in relations.Values)
            {
                var childColumn = relation.ChildColumn;
                var parentTable = dataSet.Tables[relation.ParentTableName];
                var parentColumn = parentTable.Columns["relationId"];

                if (parentColumn != null && childColumn != null)
                {
                    try
                    {
                        if (relationDirectionChildToParent && objectCorrection.ContainsKey(relation.ParentTableName)) dataSet.Relations.Add(parentTable.TableName, parentColumn, childColumn);
                        else dataSet.Relations.Add(parentTable.TableName, childColumn, parentColumn);
                    }
                    catch
                    {
                    }

                    for (var index = 0; index < allRelationsColumn.Count; index++)
                    {
                        if (allRelationsColumn[index] == parentColumn)
                        {
                            allRelationsColumn.RemoveAt(index);
                            break;
                        }
                    }
                }
            }

            foreach (DataColumn deleteColumn in allRelationsColumn)
            {
                try
                {
                    if (deleteColumn.Table.Columns.Contains(deleteColumn.ColumnName) && deleteColumn.Table.Columns.CanRemove(deleteColumn))
                        deleteColumn.Table.Columns.Remove(deleteColumn);
                }
                catch
                {
                }
            }

            StiJsonToDataSetConverter.CheckColumnType(dataSet);

            return dataSet;
        }

        public static DataSet GetDataSet(byte[] content)
        {
            return GetDataSet(content, false);
        }

        public static DataSet GetDataSet(byte[] content, bool relationDirectionChildToParent)
        {
            var str = StiBytesToStringConverter.ConvertBytesToString(content);
            return GetDataSet(str, relationDirectionChildToParent);
        }

        public static DataSet GetDataSet(string text)
        {
            return GetDataSet(text, false);
        }

        public static DataSet GetDataSet(string text, bool relationDirectionChildToParent)
        {
            if (text == null)
                return null;

            var jToken = JsonConvert.DeserializeObject(text) as JToken;

            return GetDataSet(jToken, relationDirectionChildToParent);
        }

        public static DataSet GetDataSet(XElement element)
        {
            return GetDataSet(element, false);
        }

        public static DataSet GetDataSet(XElement element, bool relationDirectionChildToParent)
        {
            var text = JsonConvert.SerializeXNode(element, Formatting.Indented);

            var jToken = JsonConvert.DeserializeObject(text) as JToken;
            return GetDataSet(jToken, relationDirectionChildToParent);
        }

        public static DataSet GetDataSetFromXml(byte[] array)
        {
            return GetDataSetFromXml(array, false);
        }

        public static DataSet GetDataSetFromXml(byte[] array, bool relationDirectionChildToParent)
        {
            if (array != null)
            {
                string text;
                using (var stream = new MemoryStream(array))
                {
                    var element = XElement.Load(stream);
                    text = JsonConvert.SerializeXNode(element, Formatting.Indented);
                }

                var jToken = JsonConvert.DeserializeObject(text) as JToken;
                return GetDataSet(jToken, relationDirectionChildToParent);
            }

            return null;
        }

        public static DataSet GetDataSetFromFile(string path)
        {
            return GetDataSetFromFile(path, false);
        }

        public static DataSet GetDataSetFromFile(string path, bool relationDirectionChildToParent)
        {
            if (File.Exists(path))
            {
                string text = File.ReadAllText(path);
                var jToken = JsonConvert.DeserializeObject(text) as JToken;
                return GetDataSet(jToken, relationDirectionChildToParent);
            }

            return null;
        }

        private static void CorrectArray(JToken jToken)
        {
            var jContainer = jToken as JContainer;
            var jArray = jToken as JArray;
            if (jArray != null)
            {
                for (var index = 0; index < jArray.Count; index++)
                {
                    var jTokenArray = jArray[index];
                    if (jTokenArray is JValue)
                    {
                        var value = ((JValue)jTokenArray).Value;
                        jArray[index] = new JObject(new JProperty("value", value));
                    }
                    else CorrectArray(jTokenArray);
                }
            }
            else if (jContainer != null)
            {
                for (var index = 0; index < jContainer.Count; index++)
                {
                    var item = jContainer.GetItem(index);
                    var itemJProperty = item as JProperty;
                    if (itemJProperty != null)
                    {
                        if (itemJProperty.Value.Type == JTokenType.Null)
                        {
                            index--;
                            itemJProperty.Remove();
                        }
                        else if(itemJProperty.Value.HasValues)
                        {
                            if (itemJProperty.Value.Type != JTokenType.Array)
                            {
                                var path = new Regex("\\[\\d*\\]").Replace(itemJProperty.Path, "").Replace('.', '_');
                                objectCorrection[path] = itemJProperty.Path;
                                var itemJArray = new JArray();
                                itemJArray.Add(itemJProperty.Value);
                                itemJProperty = new JProperty(itemJProperty.Name, itemJArray);

                                jContainer.RemoveItemAt(index);
                                jContainer.InsertItem(index, itemJProperty, true);
                            }
                            CorrectArray(itemJProperty.Value);
                        }
                    }
                }
            }

        }

        private static JToken CorrectJson(JToken jToken)
        {
            CorrectArray(jToken);

            var jContainer = jToken as JContainer;

            if (jContainer is JArray)
            {
                var root = new JProperty("root");
                root.Value = jToken;

                return root;
            }

            foreach (var item in jContainer)
            {
                var itemJArray = item as JArray;
                var itemJObject = item as JObject;
                var itemJProperty = item as JProperty;

                if (itemJProperty != null && !(itemJProperty.Value is JContainer))
                {
                    var root = new JProperty("root");
                    root.Value = jToken;

                    return root;
                }
            }

            return jToken;
        }

        private static string CorrectJsonString(string json)
        {
            string newJson = json;
            //string newJson = Regex.Replace(json, @"/,\s *}/gi", "}");
            //newJson = Regex.Replace(json, @"/:\s*null/gi", ": null");

            int pos = 0;
            while (pos < newJson.Length)
            {
                if (newJson[pos] == '{') return newJson;
                else if (newJson[pos] == '[') return "{\"root\": " + newJson + "}";
                pos++;
            }

            return newJson;
        }
        
        private static void FillCollection(List<StiJsonMetaData> collections, string collectionName, string parentName, string address, object json, bool first = false)
        {
            var cast = parentName + "_" + collectionName;
            if (string.IsNullOrEmpty(parentName)) cast = collectionName;

            var container = json as JContainer;
            var property = json as JProperty;

            var name = 0;
            foreach (var item in container)
            {
                var itemJArray = item as JArray;
                var itemJObject = item as JObject;
                var itemJProperty = item as JProperty;

                var itemName = itemJProperty != null ? itemJProperty.Name : name.ToString();
                if (property != null) itemName = property.Name;
                var itemValue = itemJProperty != null ? itemJProperty.Value : item;

                var itemValueJArray = itemValue as JArray;

                if (itemJArray != null || itemJObject != null || itemJProperty != null && (itemJProperty.Value.Type == JTokenType.Array || itemJProperty.Value.Type == JTokenType.Object))
                {
                    var isSetRelation = false;
                    if (container is JArray)
                    {
                        FillCollection(collections, collectionName, parentName, address + ".#array#" + name.ToString(), item);
                    }
                    else
                    {
                        if (!first && itemValueJArray != null)
                        {
                            foreach (var itemItem in itemValueJArray)
                            {
                                itemItem.Tag = SetRelationId(itemItem.Tag, "#relation#" + cast);
                                ((JObject)itemItem).Add("relationId", "#relation#" + cast);
                            }
                        }

                        FillCollection(collections, itemName, cast, address + "." + itemName, itemValue);
                        if (itemValueJArray == null)
                        {
                            itemValue.Tag = SetRelationId(itemValue.Tag, "-1");
                            ((JObject)itemValue).Add("relationId", "-1");
                        }
                        else
                        {
                            item.Tag = SetNewValue(item.Tag, "#relation#" + address + "." + itemName + ".#array#0");                            
                            isSetRelation = true;
                        }
                    }
                    if (!isSetRelation && itemValueJArray == null)
                    {
                        item.Tag = SetNewValue(item.Tag, "#relation#" + address + "." + itemName);
                    }
                }

                name++;
            }

            if (json is JArray) return;
            if (!string.IsNullOrEmpty(address))
            {
                if (!string.IsNullOrEmpty(parentName)) collectionName = parentName + "_" + collectionName;
                collections.Add(new StiJsonMetaData() { CollectionName = collectionName, Address = address, Cast = cast, Object = json });
            }
        }
        
        private static StiJsonRelationIdData SetRelationId(object tag, string relationId)
        {
            var dataHelp = (StiJsonRelationIdData)tag?? new StiJsonRelationIdData();
            dataHelp.RelationId = relationId;

            return dataHelp;
        }

        private static StiJsonRelationIdData SetNewValue(object tag, object value)
        {
            var dataHelp = (StiJsonRelationIdData)tag ?? new StiJsonRelationIdData();
            dataHelp.NewValue = value;

            return dataHelp;
        }
        #endregion
    }
}
