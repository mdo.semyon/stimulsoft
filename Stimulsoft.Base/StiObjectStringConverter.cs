#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Reflection;

namespace Stimulsoft.Base
{
	/// <summary>
	/// Helps a converts object to string and a converts string to object.
	/// </summary>
	public class StiObjectStringConverter
	{
		public virtual void SetProperty(PropertyInfo p, object parentObject, object obj)
		{
		}

		/// <summary>
		/// Converts object into the string.
		/// </summary>
		/// <param name="obj">Object for convertation.</param>
		/// <returns>String that represents object.</returns>
		public virtual string ObjectToString(object obj) 
		{
			if (obj is string)return (string)obj;

            if (obj is byte[])
                return Convert.ToBase64String(obj as byte[], 
                    StiBaseOptions.AllowInsertLineBreaksWhenSavingByteArray ? Base64FormattingOptions.InsertLineBreaks : Base64FormattingOptions.None);

			if (obj is Type)return obj.ToString();
            var conv = GetConverter(obj.GetType());
			return conv.ConvertToString(obj);			
		}
        
		/// <summary>
		/// Convertes string into object.
		/// </summary>
		/// <param name="str">String that represents object.</param>
		/// <param name="type">Object type.</param>
		/// <returns>Converted object.</returns>
		public virtual object StringToObject(string str, Type type) 
		{
			if (type == typeof(string))
			    return str;

		    if (type == typeof(byte[]))
		        return Convert.FromBase64String(str);

		    if (type == typeof(decimal))
		        return decimal.Parse(str);

		    if (type == typeof(Type))
		    {
		        var tp = StiTypeFinder.GetType(str);
		        if (tp != null) return tp;

		        var assemblys = AppDomain.CurrentDomain.GetAssemblies();
		        foreach (var assembly in assemblys)
		        {
		            tp = assembly.GetType(str);
		            if (tp != null) return tp;
		        }

		        if (true)
		            throw new TypeLoadException($"Type \"{str}\" not found");
		    }

		    if (type == typeof(object))return str;

			var converter = GetConverter(type);
			return converter.ConvertFromString(str);
		}

        private TypeConverter GetConverter(Type type)
        {
#if NETCORE
            // fix for NET Core 2.0: system graphic converters

            if (type == typeof(global::System.Drawing.Color))
                return new System.Drawing.ColorConverter();

            if (type == typeof(global::System.Drawing.Font))
                return new System.Drawing.FontConverter();
#endif
            return TypeDescriptor.GetConverter(type);
        }
    }
}
