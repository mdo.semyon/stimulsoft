﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Runtime.InteropServices;

namespace Stimulsoft.Base
{
    public static class SystemWinApi
    {
        #region struct WINDOWSSCALE
        public struct WINDOWSSCALE
        {
            public WINDOWSSCALE(double scaleX, double scaleY)
            {
                this.ScaleX = scaleX;
                this.ScaleY = scaleY;
                this.Scale = this.ScaleX;
            }

            public double ScaleX;
            public double ScaleY;
            public double Scale;

            public override string ToString()
            {
                return $"ScaleX={ScaleX}, ScaleY={ScaleY}";
            }
        }
        #endregion

        #region Methods

        private static WINDOWSSCALE? currentScale = null;
        public static WINDOWSSCALE GetWindowsScale()
        {
            if (currentScale == null)
            {
                int deviceDpiX;
                int deviceDpiY;

                IntPtr dc = GetDC(IntPtr.Zero);
                if (dc != IntPtr.Zero)
                {
                    deviceDpiX = GetDeviceCaps(dc, 88);
                    deviceDpiY = GetDeviceCaps(dc, 90);

                    ReleaseDC(IntPtr.Zero, dc);
                }
                else
                {
                    deviceDpiX = 96;
                    deviceDpiY = 96;
                }

                currentScale = new WINDOWSSCALE(deviceDpiX / 96d, deviceDpiY / 96d);
            }

            return currentScale.GetValueOrDefault();
        }

        public static double GetSystemScale()
        {
            return 1.0 / GetWindowsScale().Scale;
        }

        public static double GetWindowsStepScale()
        {
            var scale = SystemWinApi.GetWindowsScale().Scale;
            //if (scale == 1) return 1;
            //if (scale <= 1.5) return 1.5;
            if (scale < 1.5) return 1;
            if (scale < 2) return 1.5;
            return 2;
        }

        public static string GetWindowsStepScaleName(bool isBigImage = false)
        {
            var scale = SystemWinApi.GetWindowsStepScale();
            if (isBigImage) scale *= 2;
            if (scale == 1.5) return "_x1_5";
            if (scale == 2) return "_x2";
            if (scale == 3) return "_x3";
            if (scale == 4) return "_x4";
            return "";
        }

        private static SystemScaleID? currentSystemScale;
        public static SystemScaleID GetSystemScaleID()
        {
            if (currentSystemScale != null)
                return currentSystemScale.GetValueOrDefault();

            int deviceDpiX;
            int deviceDpiY;

            IntPtr dc = GetDC(IntPtr.Zero);
            if (dc != IntPtr.Zero)
            {
                deviceDpiX = GetDeviceCaps(dc, 88);
                deviceDpiY = GetDeviceCaps(dc, 90);

                ReleaseDC(IntPtr.Zero, dc);
            }
            else
            {
                deviceDpiX = 96;
                deviceDpiY = 96;
            }

            int dpi = Math.Max(deviceDpiX, deviceDpiY);
            if (dpi <= 96)
                currentSystemScale = SystemScaleID.x1;
            else if (dpi <= 192)
                currentSystemScale = SystemScaleID.x2;
            else
                currentSystemScale = SystemScaleID.x3;

            return currentSystemScale.GetValueOrDefault();
        }

        #endregion

        #region Methods.Imports

        [DllImport("User32.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr GetDC(IntPtr hWnd);

        [DllImport("User32.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);

        [DllImport("Gdi32.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

        #endregion
    }
}
