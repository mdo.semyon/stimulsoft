#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Linq;
using System.Collections;
using System.IO;
using System.Reflection;

namespace Stimulsoft.Base.Cloud
{
    public static class StiCloudConsts
	{
	    public static class Trial
	    {
	        public const int MaxDataRows = 30 * K;
            public const int MaxReportPages = 50;
	        public const int MaxFileSize = 3 * M;
	        public const int MaxResourceSize = 2 * M;
	        public const int MaxItems = 20;
        }

	    public static class Developer
	    {
	        public const int MaxDataRows = 60 * K;
	        public const int MaxReportPages = 100;
	        public const int MaxFileSize = 10 * M;
	        public const int MaxResourceSize = 4 * M;
        }

        public const int K = 1000;
	    public const int M = 1000 * K;
    }
}
