﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports  											}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;

namespace Stimulsoft.Base
{
    public class StiDataOptions
    {
        /// <summary>
        /// StiDataOptions.RetriveColumnsMode was obsoleted. Please use StiDataOptions.RetrieveColumnsMode.
        /// </summary>
        [Obsolete("StiDataOptions.RetriveColumnsMode was obsoleted. Please use StiDataOptions.RetrieveColumnsMode.")]
        public static StiRetrieveColumnsMode RetriveColumnsMode
        {
            get
            {
                return RetrieveColumnsMode;
            }
            set
            {
                RetrieveColumnsMode = value;
            }
        }

        /// <summary>
        /// Value which describes mode of retrieving database information.
        /// </summary>
        public static StiRetrieveColumnsMode RetrieveColumnsMode { get; set; } = StiRetrieveColumnsMode.SchemaOnly;

        /// <summary>
        /// Value which describes mode of retrieving information for stored procedures.
        /// </summary>
        [Obsolete("StiDataOptions.WizardStoredProcRetriveMode was obsoleted. Please use StiDataOptions.WizardStoredProcRetrieveMode.")]
        public static StiWizardStoredProcRetriveMode WizardStoredProcRetriveMode
        {
            get
            {
                return (StiWizardStoredProcRetriveMode)WizardStoredProcRetrieveMode;
            }
            set
            {
                WizardStoredProcRetrieveMode = (StiWizardStoredProcRetrieveMode)value;
            }
        }

        /// <summary>
        /// Value which describes mode of retrieving information for stored procedures.
        /// </summary>
        public static StiWizardStoredProcRetrieveMode WizardStoredProcRetrieveMode { get; set; } = StiWizardStoredProcRetrieveMode.All;

        /// <summary>
        /// Value which allows to use default Oracle Client connector.
        /// </summary>
        public static bool AllowUseOracleClientConnector { get; set; }
    }
}
