#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;

namespace Stimulsoft.Base
{
    public static class StiExceptionProvider
    {
        public static IStiCustomExceptionProvider CustomExceptionProvider { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether not to show the message from engine of the message.
        /// </summary>
        public static bool HideMessages { get; set; }

        /// <summary>
        /// Gets or sets the value, which indicates not to show the exception from engine of the exception.
        /// </summary>
        public static bool HideExceptions { get; set; }

        public static bool Show(Exception exception, bool rethrow = false)
        {
            if (CustomExceptionProvider != null)
                CustomExceptionProvider.Show(exception);

            else
            {
#if !NETCORE
                if (!HideMessages)
                {
                    using (var form = new StiExceptionForm(exception))
                    {
                        form.Owner = System.Windows.Forms.Form.ActiveForm;
                        form.ShowDialog();
                    }
                }
                else
#endif
                {
                    if (rethrow)
                        return !HideExceptions;

                    if (!HideExceptions) throw exception;
                }
            }
            return false;
        }
    }
}
