﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports 									            }
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using Stimulsoft.Base.Design;
using Stimulsoft.Base.Licenses;

namespace Stimulsoft.Base.Licenses
{
    /// <summary>
    /// This class is used for the license checking.
    /// </summary>
    public static class StiLicenseKeyValidator
    {
        #region Methods
        internal static bool IsValidInWebViewer(bool isWebViewer, StiLicenseKey key)
        {
            return isWebViewer
                ? IsValid(StiProductIdent.Web, key)
                : IsValidOnNetFramework(key);
        }

        internal static bool IsValid(StiProductIdent ident, StiLicenseKey key)
        {
            return key != null 
                && !string.IsNullOrWhiteSpace(key.Signature)
                && key.Products != null 
                && key.Products.Any(p => (p.Ident == ident || p.Ident == StiProductIdent.Ultimate) && p.ExpirationDate > StiVersion.Created);
        }

        internal static bool IsValidOnNetFramework(StiLicenseKey key)
        {
            return key != null
                && !string.IsNullOrWhiteSpace(key.Signature)
                && key.Products != null
                && key.Products.Any(p => IsNetPlatform(p.Ident) && p.ExpirationDate > StiVersion.Created);
        }

        internal static bool IsValidOnAnyPlatform(StiLicenseKey key)
        {
            return key != null
                && !string.IsNullOrWhiteSpace(key.Signature)
                && key.Products != null
                && key.Products.Any(p => p.ExpirationDate > StiVersion.Created);
        }

        internal static bool IsValidInDesignerOrOnSpecifiedPlatform(StiProductIdent ident, StiLicenseKey key)
        {
            return StiDesignerAppStatus.IsRunning
                ? IsValidOnAnyPlatform(key)
                : IsValid(ident, key);
        }

        private static bool IsNetPlatform(StiProductIdent ident)
        {
            return ident == StiProductIdent.Net
                   || ident == StiProductIdent.Silverlight
                   || ident == StiProductIdent.Web
                   || ident == StiProductIdent.Wpf
                   || ident == StiProductIdent.Ultimate;
        }

        internal static StiLicenseKey GetLicenseKey()
        {
            if (string.IsNullOrWhiteSpace(StiLicense.Key)) return null;

            if (StiLicense.LicenseKey == null) return null;
            if (StiLicense.LicenseKey.Signature == null) return null;

            return StiLicense.LicenseKey;
        }
        #endregion
    }
}