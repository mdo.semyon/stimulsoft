﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports 									            }
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Globalization;
using System.Security.Cryptography;
using System.Threading;
using System.Xml.Linq;
using Stimulsoft.Base.Json;
using Stimulsoft.Base;

namespace Stimulsoft.Base.Licenses
{
    public class StiLicenseProduct
    {
        #region Properties
        public DateTime ExpirationDate { get; set; }

        public StiProductIdent Ident { get; set; }
        #endregion

        #region Methods
        public override string ToString()
        {
            return string.Format("{0}-{1}", this.Ident, this.ExpirationDate);
        }
        #endregion
    }
}