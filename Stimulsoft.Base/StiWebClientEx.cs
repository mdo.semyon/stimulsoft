﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Net;

namespace Stimulsoft.Base
{
    [ToolboxItem(false)]
    public class StiWebClientEx : WebClient
    {
        #region Properties
        public CookieContainer CookieContainer { get; set; }
        #endregion

        #region WebClient override
        protected override WebRequest GetWebRequest(Uri address)
        {
            var r = base.GetWebRequest(address);
            if (CookieContainer != null)
            {
                var request = r as HttpWebRequest;
                if (request != null)
                {
                    request.CookieContainer = CookieContainer;
                }
            }
            return r;
        }
        #endregion

        public StiWebClientEx(CookieContainer container)
        {
            this.CookieContainer = container;
            this.Encoding = StiBaseOptions.WebClientEncoding;
        }
    }
}
