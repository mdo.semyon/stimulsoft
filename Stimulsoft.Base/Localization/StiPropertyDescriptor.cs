#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using Stimulsoft.Base.Design;


namespace Stimulsoft.Base.Localization
{
	/// <summary>
	/// Provides an abstraction of a property on a class.
	/// </summary>
	public class StiPropertyDescriptor : PropertyDescriptor
    {
        #region Events
        public static event StiProcessDescriptionEventHandler ProcessDescription;

        public static void InvokeProcessDescription(object sender, StiProcessDescriptionEventArgs e)
        {
            ProcessDescription?.Invoke(sender, e);
        }
        #endregion

        #region Fields
        protected PropertyDescriptor propertyDescriptor;
        private bool isEventDescriptor;
        protected bool isLocalizableProperties;
        internal int level = 0;
        internal int maxLevel = 0;
        #endregion

        #region Properties
        public override string Description
        {
            get
            {                
                var e = new StiProcessDescriptionEventArgs(this.ComponentType, this.Name, base.Description);

                InvokeProcessDescription(this, e);
                return e.Description;
            }
        }

        /// <summary>
		/// Gets the name of the category to which the member belongs, as specified in the StiCategoryAttribute.
		/// </summary>
		public override string Category
		{ 
			get
			{
				var category = propertyDescriptor.Attributes[typeof(StiCategoryAttribute)] as StiCategoryAttribute;
			    var categoryName = category == null ? "Misc" : category.Category;

				if (!isLocalizableProperties)
				{
					categoryName = UpdateCategoryName(categoryName);

				    if (level != 0)
				    {
				        return maxLevel != 0 && maxLevel > 9 && level < 10 
				            ? $"0{level}. {categoryName}"
				            : $"{level}. {categoryName}";
				    }

				    return categoryName;
				}

				categoryName = StiLocalization.Get("PropertyCategory", categoryName + "Category");

				if (categoryName != null)
				{
					categoryName = UpdateCategoryName(categoryName);

					if (level != 0)
					{
					    return maxLevel != 0 && maxLevel > 9 && level < 10 
					        ? $"0{level}. {categoryName}" 
					        : $"{level}. {categoryName}";
					}
					return categoryName;
				}

                return categoryName;
			} 
		}

        /// <summary>
        /// Gets the name of the category to which the member belongs, as specified in the StiCategoryAttribute without localization.
        /// </summary>
        public string OriginalCategory
        {
            get
            {
                var category = propertyDescriptor.Attributes[typeof(StiCategoryAttribute)] as StiCategoryAttribute;

                return category == null ? "Misc" : category.Category;
            }
        }
        
        /// <summary>
		/// Gets the name that can be displayed in a window, such as a Properties window.
		/// </summary>
		public override string DisplayName
		{ 
			get
			{
			    return !isLocalizableProperties 
			        ? propertyDescriptor.DisplayName 
			        : LocalizedName;
			}
		}

        public string LocalizedName
        {
            get
            {
                if (!isEventDescriptor)
                {
                    var key = propertyDescriptor.ComponentType.Name + propertyDescriptor.Name;

                    //����������� ��������, ���������� ������� ��������� � ����, ��� ��� �������� Category ������������ �������� �����
                    var name = propertyDescriptor.Name == "Category" ? null : StiLocalization.Get("PropertyMain", key, false);

                    if (name == null)
                        name = StiLocalization.Get("PropertyMain", propertyDescriptor.Name, false);

                    return name ?? propertyDescriptor.DisplayName;
                }
                else
                {
                    var name = StiLocalization.Get("PropertyEvents", propertyDescriptor.Name, false);
                    return name ?? propertyDescriptor.DisplayName;
                }
            }
        }


		/// <summary>
		/// Gets a type of the component.
		/// </summary>
		public override Type ComponentType 
		{
			get 
			{
				return propertyDescriptor.ComponentType;
			}
		}


		/// <summary>
		/// Gets a value indicating whether this property is read-only.
		/// </summary>
		public override bool IsReadOnly 
		{
			get 
			{
				return propertyDescriptor.IsReadOnly;
			}
		}


		/// <summary>
		/// Gets the type of the property.
		/// </summary>
		public override Type PropertyType 
		{
			get 
			{
				return propertyDescriptor.PropertyType;
			}
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return DisplayName;
        }

        private string UpdateCategoryName(string name)
        {
            int index = 1;
            while (index < name.Length)
            {
                if (Char.IsUpper(name[index]))
                {
                    name = name.Insert(index, " ");
                    index++;
                }
                index++;
            }

            return name;
        }
        

        /// <summary>
		/// Returns whether resetting an object changes its value.
		/// </summary>
		/// <param name="component">The component to test for reset capability.</param>
		/// <returns>true if resetting the component changes its value; otherwise, false.</returns>
		public override bool CanResetValue(object component) 
		{
			return propertyDescriptor.CanResetValue(component);
		}


		/// <summary>
		/// Gets the current value of the property on a component.
		/// </summary>
		/// <param name="component">The component with the property for which to retrieve the value.</param>
		/// <returns>The value of a property for a given component.</returns>
		public override object GetValue(object component) 
		{
			return propertyDescriptor.GetValue(component);
		}


		/// <summary>
		/// Resets the value for this property of the component to the default value.
		/// </summary>
		/// <param name="component">The component with the property value that is to be reset to the default value.</param>
		public override void ResetValue(object component) 
		{
			propertyDescriptor.ResetValue(component);
		}


		/// <summary>
		/// Sets the value of the component to a different value.
		/// </summary>
		/// <param name="component">The component with the property value that is to be set.</param>
		/// <param name="value">The new value.</param>
		public override void SetValue(object component, object value) 
		{
			propertyDescriptor.SetValue(component, value);
		}


		/// <summary>
		/// Determines a value indicating whether the value of this property needs to be persisted.
		/// </summary>
		/// <param name="component">The component with the property to be examined for persistence.</param>
		/// <returns>true if the property should be persisted; otherwise, false.</returns>
		public override bool ShouldSerializeValue(object component) 
		{
			return propertyDescriptor.ShouldSerializeValue(component);
        }
        #endregion

        /// <summary>
		/// Initializes a new instance of the StiPropertyDescriptor class with the name and attributes in the specified PropertyDescriptor.
		/// </summary>
		/// <param name="propertyDescriptor">Specified PropertyDescriptor.</param>
        public StiPropertyDescriptor(PropertyDescriptor propertyDescriptor)
            : this(propertyDescriptor, false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the StiPropertyDescriptor class with the name and attributes in the specified StiPropertyDescriptor.
        /// </summary>
        /// <param name="propertyDescriptor">Specified SPropertyDescriptor.</param>
        public StiPropertyDescriptor(PropertyDescriptor propertyDescriptor, bool isEventDescriptor) : base(propertyDescriptor)
		{
			isLocalizableProperties = StiPropertyGridOptions.Localizable;
			this.propertyDescriptor = propertyDescriptor;
            this.isEventDescriptor = isEventDescriptor;
		}
	}

}
