﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Stimulsoft.Base.Localization;
using static Stimulsoft.Base.SystemWinApi;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Base.Drawing
{
    public sealed class StiCapStyleHelper
    {
        private static WINDOWSSCALE scale = SystemWinApi.GetWindowsScale();
        public static int GetMaxWidthOfCapStyles(Graphics g, Font font)
        {
            var obj = Enum.GetValues(typeof(StiCapStyle));

            var maxWidth = 0;

            for (var k = 0; k < obj.Length; k++)
            {
                var capStyle = obj.GetValue(k);
                var locName = StiLocalization.Get("PropertyEnum", typeof(StiCapStyle).Name + Enum.GetName(typeof(StiCapStyle), capStyle), false);

                var strWidth = (int)g.MeasureString(locName, font).Width;

                maxWidth = Math.Max(maxWidth, strWidth);
            }
            return maxWidth + 60;
        }

        public static void DrawCapStyle(DrawItemEventArgs e)
        {
            var g = e.Graphics;
            var rect = e.Bounds;
            var borderRect = new Rectangle(rect.X + (int)Math.Ceiling(2 * scale.ScaleX), rect.Y + (int)Math.Ceiling(2 * scale.ScaleY), (int)Math.Ceiling(52 * scale.ScaleX), (int)Math.Ceiling(14 * scale.ScaleY));

            #region Fill
            rect.Width--;
            StiControlPaint.DrawItem(g, rect, e.State, SystemColors.Window, SystemColors.ControlText);
            #endregion

            #region Paint border style
            var obj = Enum.GetValues(typeof(StiCapStyle));

            using (var pen = new Pen(Color.DimGray, 1))
            {
                var capStyle = StiCapStyle.None;
                if (e.Index != -1) capStyle = (StiCapStyle)obj.GetValue(e.Index);

                g.FillRectangle(Brushes.White, borderRect);

                var yCenter = borderRect.Top + borderRect.Height / 2;
                var xStep = borderRect.Width / 4;
                PointF[] points;

                var mode = g.SmoothingMode;
                g.SmoothingMode = SmoothingMode.AntiAlias;
                switch (capStyle)
                {
                    #region None
                    case StiCapStyle.None:
                        g.DrawLine(pen, borderRect.X + xStep, yCenter, borderRect.Right - xStep, yCenter);
                        break;
                    #endregion

                    #region Arrow
                    case StiCapStyle.Arrow:
                        var capArrowRect = new Rectangle(borderRect.Right - xStep - (int)Math.Ceiling(4 * scale.ScaleX), borderRect.Y + (int)Math.Ceiling(2 * scale.ScaleY), (int)Math.Ceiling(5 * scale.ScaleX), (int)Math.Ceiling(11 * scale.ScaleY));

                        g.SmoothingMode = mode;
                        points = new[] 
                        {
                            new PointF(capArrowRect.Right, capArrowRect.Y + (float)(capArrowRect.Height / 2)), 
                            new PointF(capArrowRect.Left, capArrowRect.Y),
                            new PointF(capArrowRect.Left, capArrowRect.Bottom)
                        };
                        using (var capBrush = new SolidBrush(Color.DimGray))
                        {
                            g.DrawLine(pen, borderRect.X + xStep, points[0].Y, borderRect.Right - xStep, points[0].Y);
                            g.FillPolygon(capBrush, points);
                        }
                        break;
                    #endregion

                    #region Open
                    case StiCapStyle.Open:
                        var capOpenRect = new Rectangle(borderRect.Right - xStep - (int)Math.Ceiling(4 * scale.ScaleX), borderRect.Y + (int)Math.Ceiling(4 * scale.ScaleY), (int)Math.Ceiling(5 * scale.ScaleX), (int)Math.Ceiling(7 * scale.ScaleY));
                        points = new[] 
                        {
                            new PointF(capOpenRect.X, capOpenRect.Y),
                            new PointF(capOpenRect.Right, capOpenRect.Y + (float)(capOpenRect.Height / 2)),
                            new PointF(capOpenRect.X, capOpenRect.Bottom)
                        };
                        using (var openPen = new Pen(Color.DimGray))
                        {
                            g.DrawLine(pen, borderRect.X + xStep, points[1].Y, borderRect.Right - xStep, points[1].Y);
                            g.DrawLines(openPen, points);
                        }
                        break;
                    #endregion

                    #region Stealth
                    case StiCapStyle.Stealth:
                        var capStealthRect = new Rectangle(borderRect.Right - xStep - (int)Math.Ceiling(9 * scale.ScaleX), borderRect.Y + (int)Math.Ceiling(2 * scale.ScaleY), (int)Math.Ceiling(10 * scale.ScaleX), (int)Math.Ceiling(11 * scale.ScaleY));
                        points = new[] 
                        {
                            new PointF(capStealthRect.X, capStealthRect.Y), 
                            new PointF(capStealthRect.Right, capStealthRect.Y + (float)(capStealthRect.Height / 2)),
                            new PointF(capStealthRect.X, capStealthRect.Bottom), 
                            new PointF(capStealthRect.X  + (float)(capStealthRect.Width / 3), capStealthRect.Y + (float)(capStealthRect.Height / 2)),
                            new PointF(capStealthRect.X, capStealthRect.Y)
                        };
                        using (var capBrush = new SolidBrush(Color.DimGray))
                        {
                            g.DrawLine(pen, borderRect.X + xStep, points[1].Y, borderRect.Right - xStep, points[1].Y);
                            g.FillPolygon(capBrush, points);
                        }
                        break;
                    #endregion

                    #region Diamond
                    case StiCapStyle.Diamond:
                        var capDiamondRect = new Rectangle(borderRect.Right - xStep - (int)Math.Ceiling(6 * scale.ScaleX), borderRect.Y + (int)Math.Ceiling(3 * scale.ScaleY), (int)Math.Ceiling(8 * scale.ScaleX), (int)Math.Ceiling(9 * scale.ScaleY));
                        points = new[] 
                        {
                            new PointF(capDiamondRect.X, capDiamondRect.Y + (float)(capDiamondRect.Height / 2)), 
                            new PointF(capDiamondRect.X + (float)(capDiamondRect.Width / 2), capDiamondRect.Y),
                            new PointF(capDiamondRect.Right, capDiamondRect.Y + (float)(capDiamondRect.Height / 2)), 
                            new PointF(capDiamondRect.X + (float)(capDiamondRect.Width / 2), capDiamondRect.Bottom)
                        };
                        using (Brush diamondBrush = new SolidBrush(Color.DimGray))
                        {
                            g.DrawLine(pen, borderRect.X + xStep, points[0].Y, borderRect.Right - xStep, points[0].Y);
                            g.FillPolygon(diamondBrush, points);
                        }
                        break;
                    #endregion

                    #region Oval
                    case StiCapStyle.Oval:
                        g.DrawLine(pen, borderRect.X + xStep, yCenter, borderRect.Right - xStep, yCenter);

                        var capOvalRect = new Rectangle(borderRect.Right - xStep - (int)Math.Ceiling(4 * scale.ScaleX), borderRect.Y + (int)Math.Ceiling(4 * scale.ScaleY), (int)Math.Ceiling(6 * scale.ScaleX), (int)Math.Ceiling(6 * scale.ScaleY));
                        using (var ovalBrush = new SolidBrush(Color.DimGray))
                        {
                            g.FillEllipse(ovalBrush, capOvalRect);
                        }
                        break;
                    #endregion

                    #region Square
                    case StiCapStyle.Square:
                        g.DrawLine(pen, borderRect.X + xStep, yCenter, borderRect.Right - xStep, yCenter);

                        var capSquareRect = new Rectangle(borderRect.Right - xStep - (int)Math.Ceiling(4 * scale.ScaleX), borderRect.Y + (int)Math.Ceiling(4 * scale.ScaleY), (int)Math.Ceiling(6 * scale.ScaleX), (int)Math.Ceiling(6 * scale.ScaleY));
                        g.SmoothingMode = mode;
                        using (Brush squareBrush = new SolidBrush(Color.DimGray))
                        {
                            g.FillRectangle(squareBrush, capSquareRect);
                        }
                        break;
                    #endregion
                }
                g.SmoothingMode = mode;
            }

            g.DrawRectangle(Pens.Black, borderRect);
            #endregion

            #region Paint name
            using (var font = new Font("Arial", 8))
            using (var stringFormat = new StringFormat())
            {
                stringFormat.LineAlignment = StringAlignment.Center;

                var capStyle = obj.GetValue(e.Index);
                var locName = StiLocalization.Get("PropertyEnum", typeof(StiCapStyle).Name + Enum.GetName(typeof(StiCapStyle), capStyle), false);

                e.Graphics.DrawString(locName, font, Brushes.Black,
                    new Rectangle((int)Math.Ceiling(55 * scale.ScaleX), rect.Top + (int)Math.Ceiling(4 * scale.ScaleY), rect.Width - (int)Math.Ceiling(18 * scale.ScaleX), (int)Math.Ceiling(10 * scale.ScaleY)),
                    stringFormat);
            }
            #endregion
        }
    }
}
