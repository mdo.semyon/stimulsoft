#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;

namespace Stimulsoft.Base.Drawing
{
	public class StiImageUtils
	{
		public static Bitmap GetImage(Type type, string imageName)
		{
			return GetImage(type, imageName, true);
		}

		/// <summary>
		/// Gets the Image object associated with Type.
		/// </summary>
		/// <param name="type">The type with which Image object is associated.</param>
		/// <param name="imageName">The name of the image file to look for.</param>
		/// <returns>The Image object.</returns>
		public static Bitmap GetImage(Type type, string imageName, bool makeTransparent)
		{
			return GetImage(type.Module.Assembly, imageName, makeTransparent);
		}

		public static Bitmap GetImage(string assemblyName, string imageName)
		{
			return GetImage(assemblyName, imageName, true);
		}
		
		/// <summary>
		/// Gets the Image object placed in assembly.
		/// </summary>
		/// <param name="assemblyName">The name of assembly in which the Cursor object is placed.</param>
		/// <param name="imageName">The name of the image file to look for.</param>
		/// <returns>The Image object.</returns>
		public static Bitmap GetImage(string assemblyName, string imageName, bool makeTransparent)
		{			
			var assemblys = AppDomain.CurrentDomain.GetAssemblies();
			foreach (var assembly in assemblys)
			{
                if (StiBaseOptions.FullTrust)
                {
                    if (assembly.GetName().Name == assemblyName)
                        return GetImage(assembly, imageName, makeTransparent);
                }
                else
                {
                    var str = assembly.FullName;
                    var pos = str.IndexOf(',');
                    if (pos > -1) str = str.Substring(0, pos);

                    if (str == assemblyName)
                        return GetImage(assembly, imageName, makeTransparent);
                }
			}
			
			throw new Exception($"Can't find assembly '{assemblyName}'");
		}
		
		public static Bitmap GetImage(Assembly imageAssembly, string imageName)
		{
			return GetImage(imageAssembly, imageName, true);
		}

		/// <summary>
		/// Gets the Image object placed in assembly.
		/// </summary>
		/// <param name="imageAssembly">Assembly in which is the Image object is placed.</param>
		/// <param name="imageName">The name of the image file to look for.</param>
		/// <returns>The Image object.</returns>
		public static Bitmap GetImage(Assembly imageAssembly, string imageName, bool makeTransparent)
		{
			var stream = imageAssembly.GetManifestResourceStream(imageName);
			if (stream != null)
			{
				var image = new Bitmap(stream);

				if (makeTransparent)
				    MakeImageBackgroundAlphaZero(image);

				return image;			
			}

		    throw new Exception($"Can't find image '{imageName}' in resources of '{imageAssembly}' assembly");
		}

		public static void MakeImageBackgroundAlphaZero(Bitmap image)
		{
			var color1 = image.GetPixel(0, image.Height - 1);
			image.MakeTransparent();

			var color2 = Color.FromArgb(0, color1);
			image.SetPixel(0, image.Height - 1, color2);
		}
		
		public static Bitmap ReplaceImageColor(Bitmap bmp, Color colorForReplace, Color replacedColor)
		{
			var bufferImage = new Bitmap(bmp.Width, bmp.Height);
			var g = Graphics.FromImage(bufferImage);

			var imageAttr = new ImageAttributes();
		    imageAttr.SetRemapTable(new[]
		    {
		        new ColorMap
		        {
		            OldColor = replacedColor,
		            NewColor = colorForReplace
		        }
            });

			g.DrawImage(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height), 
				0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, imageAttr);

			g.Dispose();

			return bufferImage;
		}
		
		public static Bitmap ConvertToDisabled(Bitmap bmp)
		{
			var bufferImage = new Bitmap(bmp.Width, bmp.Height);
			var g = Graphics.FromImage(bufferImage);

			var imageAttr = new ImageAttributes();
        
			var disableMatrix = new ColorMatrix(new[]
			{
				new[] { 0.3f, 0.3f, 0.3f, 0, 0 },
				new[] { 0.59f, 0.59f, 0.59f, 0, 0 },
				new[] { 0.11f, 0.11f, 0.11f, 0, 0 },
				new[] { 0, 0, 0, 0.4f, 0, 0 },
				new[] { 0, 0, 0, 0, 0.4f, 0 },
				new[] { 0, 0, 0, 0, 0, 0.4f }
			});

			imageAttr.SetColorMatrix(disableMatrix);

			g.DrawImage(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height), 
				0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, imageAttr);

			g.Dispose();

			return bufferImage;
		}
		
		public static Bitmap ConvertToGrayscale(Bitmap bmp)
		{
			var bufferImage = new Bitmap(bmp.Width, bmp.Height);
			var g = Graphics.FromImage(bufferImage);

			var grayscaleMatrix = new ColorMatrix(new[]
			{
				new[]{0.3f,0.3f,0.3f,0,0},
				new[]{0.59f,0.59f,0.59f,0,0},
				new[]{0.11f,0.11f,0.11f,0,0},
				new[]{0f,0,0,1,0, 0 },
				new[]{0f,0,0,0,1, 0 },
				new[]{0f,0,0,0,0, 1 }
			});			


			var imageAttr = new ImageAttributes();
			imageAttr.SetColorMatrix(grayscaleMatrix);
			g.DrawImage(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height), 
				0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, imageAttr);

			g.Dispose();
			return bufferImage;
		}
	}
}
