#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;

namespace Stimulsoft.Base.Drawing
{
    public class FontV : IDisposable
    {
        #region IDisposable
        public void Dispose()
        {
            if (Font != null)
                Font.Dispose();

            if (fontsCollection != null)
                fontsCollection.Dispose();

            if (fontData != IntPtr.Zero)
                Marshal.FreeCoTaskMem(fontData);

            if (!string.IsNullOrWhiteSpace(CacheFile))
                try
                {
                    File.Delete(CacheFile);
                }
                catch
                { }
        }
        #endregion

        #region Fields
        private IntPtr fontData = IntPtr.Zero;
        private int fontDataLength = 0;
        private PrivateFontCollection fontsCollection;
        #endregion

        #region Properties
        public Font Font { get; }

        public string Name { get; set; }

        public string CacheFile { get; } = null;
        #endregion

        #region Methods
        private static string CreateTempFile(byte[] content, string extension)
        {
            try
            {
                string temp = StiFontCollection.DefaultCachePath;
                if (string.IsNullOrWhiteSpace(temp))
                {
                    temp = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    temp = Path.Combine(temp, "StimulsoftReportsResources");
                }
                if (!Directory.Exists(temp)) Directory.CreateDirectory(temp);

                string cache = GetHashName(content) + "." + extension.ToLowerInvariant();
                temp = Path.Combine(temp, cache);

                if (!File.Exists(temp))
                {
                    File.WriteAllBytes(temp, content);
                }

                return temp;
            }
            catch
            {
            }
            return null;
        }

        internal static string GetHashName(byte[] content)
        {
            var crc = Crypto.Crc32.Calculate(content);
            return $"{crc:X8}-{content.Length}";
        }
        #endregion

        public FontV(string name, byte[] content, string extension)
        {
            if (content == null) return;

            fontDataLength = content.Length;
            Name = name;

            fontsCollection = new PrivateFontCollection();

            CacheFile = CreateTempFile(content, extension);
            if (!string.IsNullOrEmpty(CacheFile))
            {
                try
                {
                    fontsCollection.AddFontFile(CacheFile);
                }
                catch
                {
                    CacheFile = null;
                }
            }
            if (string.IsNullOrEmpty(CacheFile))
            {
                fontData = Marshal.AllocCoTaskMem(content.Length);
                Marshal.Copy(content, 0, fontData, content.Length);

                fontsCollection.AddMemoryFont(fontData, content.Length);
            }

            Font = new Font(fontsCollection.Families[0], 32);
        }
    }
}
