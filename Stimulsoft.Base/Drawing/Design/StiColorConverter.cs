#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.ComponentModel;
using System.Globalization;
using System.Drawing;
using Stimulsoft.Base.Localization;

#if NETCORE
using Stimulsoft.System.Drawing;
#endif

namespace Stimulsoft.Base.Drawing.Design
{
	/// <summary>
	/// Converts colors from one data type to another.
	/// </summary>
	public class StiColorConverter : ColorConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))return true; 
			return base.CanConvertFrom(context, sourceType); 
		} 

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			var colorName = value as string;
            if (colorName != null & colorName.IndexOf(",", StringComparison.InvariantCulture) != -1)
			{
				colorName = colorName.Trim();
			
				var strs = colorName.Split(',');
				if (strs.Length == 4)
				    return Color.FromArgb(
				        int.Parse(strs[0].Trim()), 
				        int.Parse(strs[1].Trim()), 
				        int.Parse(strs[2].Trim()), 
				        int.Parse(strs[3].Trim()));

				return Color.FromArgb(
					int.Parse(strs[0].Trim()),
					int.Parse(strs[1].Trim()),
					int.Parse(strs[2].Trim()));
			}
			return base.ConvertFrom(context, culture, value); 
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, 
			Type destinationType)
		{
			if (destinationType == typeof(string))return true;
			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, 
			object value, Type destinationType) 
		{
			if (destinationType == typeof(string))
			{
				Color color = (Color)value;
				string colorName = null;
#if NETCORE
                if (color.GetIsSystemColor())
				{
                    colorName = StiLocalization.Get("PropertySystemColors", color.Name, false);
                    if (colorName == null) return color.Name;
				}
				else if (color.GetIsKnownColor())
				{
					colorName = StiLocalization.Get("PropertyColor", color.Name);
				}
#else
                if (color.IsSystemColor)
				{
                    colorName = StiLocalization.Get("PropertySystemColors", color.Name, false);
                    if (colorName == null) return color.Name;
				}
				else if (color.IsKnownColor)
				{
					colorName = StiLocalization.Get("PropertyColor", color.Name);
				}
#endif
                else
                {
					if (color.A == 255)
						return $"{color.R},{color.G},{color.B}";

					else 
						return $"{color.A},{color.R},{color.G},{color.B}";
				}
				return colorName;
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}
	}
}
