﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Net;
using System.Text;

namespace Stimulsoft.Base.Drawing
{
    public static class StiBingMapHelper
    {
        #region Methods
        public static Bitmap GetHashImage(Size size, List<string> pushPins = null)
        {
            var altLonStr = pushPins == null || pushPins.Count == 0 ? "{{alt}},{{lon}}/" : "";
            var url = $"https://dev.virtualearth.net/REST/v1/Imagery/Map/CanvasLight/{altLonStr}/?mapSize={size.Width},{size.Height}&key={BingKey}";

            return hyperlinkToImageCache.ContainsKey(url) ? hyperlinkToImageCache[url] : null;
        }

        public static Bitmap GetImage(Size size, List<string> pushPins = null)
        {
            var altLonStr = pushPins == null || pushPins.Count == 0 ? "{{alt}},{{lon}}/" : "";

            #region Check Min Width and Height
            if (size.Width < 50)
                size = new Size(50, size.Height);
            if (size.Height < 50)
                size = new Size(size.Width, 50);
            #endregion

            var url = $"https://dev.virtualearth.net/REST/v1/Imagery/Map/CanvasLight/{altLonStr}/?mapSize={size.Width},{size.Height}&key={BingKey}";

            var image = hyperlinkToImageCache.ContainsKey(url) ? hyperlinkToImageCache[url] : null;
            if (image != null)
                return image;

            var request = WebRequest.Create(url);
            request.Method = "POST";
            var postData = new StringBuilder();

            int count = pushPins.Count;
            if (count > 100)
                count = 100;

            if (pushPins.Count > 0)
            {
                for (var i = 0; i < count; i++)
                {
                    postData.Append(pushPins[i] + (i != count - 1 ? "&" : ""));
                }
            }

            var byteArray = Encoding.UTF8.GetBytes(postData.ToString());
            request.ContentType = "text/plain; charset=utf-8";
            request.ContentLength = byteArray.Length;

            var dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            var response = request.GetResponse();
            dataStream = response.GetResponseStream();
            image = new Bitmap(dataStream);
            dataStream.Close();
            response.Close();

            hyperlinkToImageCache[url] = image;

            return image;
        }
        #endregion

        #region Fields
        private static Dictionary<string, Bitmap> hyperlinkToImageCache = new Dictionary<string, Bitmap>();
        #endregion

        #region Properties.Static
        public static string BingKey { get; set; } = "AjGy60ciMrcB7Acfl0kqPEAS2zNzuISiRVr7GUmKCFUTELF9fIj7tGshe6oJVmbS";
        #endregion
    }
}
