#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Stimulsoft.Base.Localization;
using static Stimulsoft.Base.SystemWinApi;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Base.Drawing
{
	public sealed class StiHatchStyleHelper
	{
        private static WINDOWSSCALE scale = SystemWinApi.GetWindowsScale();

        public static int GetWidthOfHatchStyle(Graphics g, Font font, int index)
		{
			var hatchStyle = StiBrushes.HatchStyles[index];
			var str = StiLocalization.Get("PropertyHatchStyle", hatchStyle.ToString());

			var strWidth = (int)g.MeasureString(str, font).Width;
			return strWidth + (int)Math.Ceiling(40 * scale.ScaleX);
		}

		public static void DrawHatchStyle(DrawItemEventArgs e)
		{
		    if (e.Index < 0) return;

		    var g = e.Graphics;
		    var rect = e.Bounds;
		    if ((e.State & DrawItemState.Selected) > 0)rect.Width--;
		    var hatchRect = new Rectangle(rect.X + (int)Math.Ceiling(2 * scale.ScaleX), rect.Y + (int)Math.Ceiling(2 * scale.ScaleY), (int)Math.Ceiling(19 * scale.ScaleX), rect.Height - (int)Math.Ceiling(5 * scale.ScaleY));
			
		    StiControlPaint.DrawItem(g, rect, e.State, SystemColors.Window, SystemColors.ControlText);

		    using (var brush = new HatchBrush(StiBrushes.HatchStyles[e.Index], Color.Black, Color.White))
		    {
		        g.FillRectangle(brush, hatchRect);
		    }

		    g.DrawRectangle(Pens.Black, hatchRect);

		    #region Paint name
		    using (var font = new Font("Arial", 8))
		    {
		        using (var stringFormat = new StringFormat())
		        {
		            stringFormat.LineAlignment = StringAlignment.Center;
		            stringFormat.FormatFlags = StringFormatFlags.NoWrap;
		            stringFormat.Trimming = StringTrimming.EllipsisCharacter;

		            var hatchStyle = StiBrushes.HatchStyles[e.Index];
		            var name = StiLocalization.Get("PropertyHatchStyle", hatchStyle.ToString());

		            e.Graphics.DrawString(name, 
		                font, Brushes.Black, 
		                new Rectangle((int)Math.Ceiling(25 * scale.ScaleX), rect.Top, rect.Width - (int)Math.Ceiling(18 * scale.ScaleX), (int)Math.Ceiling(16 * scale.ScaleY)),
		                stringFormat);
		        }
		    }
		    #endregion
		}
	}
}
