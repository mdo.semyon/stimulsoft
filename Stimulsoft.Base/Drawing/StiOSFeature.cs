#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Security;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Base.Drawing
{
	/// <summary>
	/// Summary description for StiOSFeature.
	/// </summary>
	[SuppressUnmanagedCodeSecurity]
	public sealed class StiOSFeature
	{
		#region Properties
	    public static bool IsLayeredWindows { get; }

	    public static bool IsThemes { get; }
	    #endregion

		static StiOSFeature()
		{
			IsLayeredWindows = OSFeature.Feature.GetVersionPresent(OSFeature.LayeredWindows) != null;
			IsThemes = OSFeature.Feature.GetVersionPresent(OSFeature.Themes) != null;
		}
	}
}
