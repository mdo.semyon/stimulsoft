#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using Stimulsoft.Base.Localization;
using static Stimulsoft.Base.SystemWinApi;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Base.Drawing
{
	public sealed class StiPenStyleHelper
	{
        private static WINDOWSSCALE scale = SystemWinApi.GetWindowsScale();

        public static int GetMaxWidthOfPenStyles(Graphics g, Font font)
		{
			var obj = Enum.GetValues(typeof(StiPenStyle));
			
			var maxWidth = 0;

			for (var k = 0; k < obj.Length; k++)
			{
				var str = ((StiPenStyle)obj.GetValue(k)).ToString();
				var locName = StiLocalization.Get("PropertyEnum", "StiPenStyle" + str);
					
				if (locName != null)str = locName;

				var strWidth = (int)g.MeasureString(str, font).Width;

				maxWidth = Math.Max(maxWidth, strWidth);
			}
			return maxWidth + (int)Math.Ceiling(60 * scale.ScaleX);
		}


		public static void DrawPenStyle(DrawItemEventArgs e)
		{
			var g = e.Graphics;
			var rect = e.Bounds;
			var borderRect = new Rectangle(rect.X + (int)Math.Ceiling(2 * scale.ScaleX), rect.Y + (int)Math.Ceiling(2 * scale.ScaleY), (int)Math.Ceiling(52 * scale.ScaleX), (int)Math.Ceiling(14 * scale.ScaleY));
			
			#region Fill
			rect.Width--;
			StiControlPaint.DrawItem(g, rect, e.State, SystemColors.Window, SystemColors.ControlText);
			#endregion
			
			#region Paint border style
			var obj = Enum.GetValues(typeof(StiPenStyle));

			using (var pen = new Pen(Color.Black, 2))
			{
				var penStyle = StiPenStyle.Solid;
				if (e.Index != -1)penStyle = (StiPenStyle)obj.GetValue(e.Index);
				pen.DashStyle = StiPenUtils.GetPenStyle(penStyle);
            			
				g.FillRectangle(Brushes.White, borderRect);

				var center = rect.Top + rect.Height /2;

				if (penStyle == StiPenStyle.Double)
				{
					pen.Width = (int)(1 * scale.ScaleY);
					g.DrawLine(pen, (int)Math.Ceiling(2 * scale.ScaleX), center - (int)Math.Ceiling(1 * scale.ScaleY), (int)Math.Ceiling(54 * scale.ScaleX), center - (int)Math.Ceiling(1 * scale.ScaleY));
					g.DrawLine(pen, (int)Math.Ceiling(2 * scale.ScaleX), center + (int)Math.Ceiling(1 * scale.ScaleY), (int)Math.Ceiling(54 * scale.ScaleX), center + (int)Math.Ceiling(1 * scale.ScaleY));
				}
				else if (penStyle != StiPenStyle.None)
				{
					g.DrawLine(pen, (int)Math.Ceiling(2 * scale.ScaleX), center, (int)Math.Ceiling(54 * scale.ScaleX), center);
				}
			}

			g.DrawRectangle(Pens.Black, borderRect);
			#endregion

			#region Paint name
			using (var font = new Font("Arial", 8))
			{
				using (var stringFormat = new StringFormat())
				{
					stringFormat.LineAlignment = StringAlignment.Center;

					var name = ((StiPenStyle)obj.GetValue(e.Index)).ToString();

					var locName = StiLocalization.Get("PropertyEnum", "StiPenStyle" + name);
					
					if (locName != null)name = locName;

					e.Graphics.DrawString(name, 
						font, Brushes.Black, 
						new Rectangle((int)Math.Ceiling(55 * scale.ScaleX), rect.Top + (int)Math.Ceiling(4 * scale.ScaleY), rect.Width - (int)Math.Ceiling(18 * scale.ScaleX), (int)Math.Ceiling(10 * scale.ScaleY)), 
						stringFormat);
				}
			}
			#endregion

		}
	}
}
