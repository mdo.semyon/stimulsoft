﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft


using Stimulsoft.Base.Design;
using System;

namespace Stimulsoft.Base.Server
{
    internal interface IStiAccount
    {
        #region Methods
        void CheckLicense();

        void Logout();

        IStiNotificationsControl GetNotificationsControl(IntPtr intPtr);

        IStiNotificationMenuControl GetNotificationMenuControl(IntPtr intPtr);

        IStiLoginMenuControl GetLoginMenuContol();
        
        IStiShareMenuControl GetShareMenuControl(object viewer);

        IStiSubscriptionsWindow GetSubscriptionsWindow();

        IStiChangePasswordWindow GetChangePasswordWindow();

        IStiUpdateWindow GetUpdateWindow();

        IStiProfileWindow GetProfileWindow();

        IStiPublishWindow GetPublishWindow(IStiReport report);

        IStiCloudShareWindow GetCloudShareWindow(IStiReport report, string reportItemKey);

        IStiAuthorizationWindow GetAuthorizationWindow();

        IStiCloudSaveWindow GetCloudSaveWindow(IStiReport report, string password, string fileName);

        IStiCloudOpenWindow GetCloudOpenWindow();

        IStiMessageWindow GetMessageWindow(StiAccountMessageEventArgs accountMessage);

        IStiNuGetWindow GetNuGetWindow(StiDataConnector connector);

        void InvokeUserInfoChanged();

        void InvokeNotificationsLoaded();

        void InvokeNotificationsChanged();

        void InvokeShowNewNotification(EventArgs arg);
        #endregion

        #region Properties
        IStiAccountUser User { get; set; }

        IStiAccountActions Actions { get; set; }
        #endregion

        #region Events
        event EventHandler UserInfoChanged;

        event EventHandler<EventArgs> ShowNewNotification;

        event EventHandler NotificationsLoaded;

        event EventHandler NotificationsChanged;
        #endregion
    }
}