#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Text;
using System.Threading;

namespace Stimulsoft.Base.Helpers
{
    public static class StiValueHelper
	{
        #region Methods
        public static bool IsZero(object value)
	    {
            if (value == null) return true;

	        var type = Type.GetTypeCode(value.GetType());
            switch (type)
	        {
	            case TypeCode.SByte:
                    return (SByte)value == 0;

	            case TypeCode.Int16:
	                return (Int16)value == 0;

                case TypeCode.UInt16:
                    return (UInt16)value == 0;

                case TypeCode.Int32:
                    return (Int32)value == 0;

                case TypeCode.UInt32:
                    return (UInt32)value == 0;

                case TypeCode.Int64:
                    return (Int64)value == 0;

                case TypeCode.UInt64:
                    return (UInt64)value == 0;

                case TypeCode.Single:
                    return (Single)value == 0;

                case TypeCode.Double:
                    return (Double)value == 0;

                case TypeCode.Decimal:
                    return (Decimal)value == 0;
            }
            return false;
        }
        
        public static bool EqualDecimal(object value1, object value2)
	    {
	        if (value1 == null) return false;
	        if (value2 == null) return false;

	        return TryToDecimal(value1) == TryToDecimal(value2);
	    }
	    #endregion

        #region Methods.TryTo
        public static string TryToString(object value)
	    {
	        if (value is string)
	            return value as string;

	        if (value == null)
	            return null;

	        return value.ToString();
	    }

	    public static float TryToFloat(object value)
	    {
	        try
	        {
                if (value == null)
                    return 0;

	            if (value is bool)
	                return (bool)value ? 1 : 0;

	            if (value is float)
	                return (float)value;

                if (value is string)
	            {
	                float result;
	                float.TryParse((string)value, out result);
	                return result;
	            }

	            if (!value.GetType().IsNumericType())
	                return 0;

                return Convert.ToSingle(value);
	        }
	        catch
	        {
	            return 0f;
	        }
	    }

        public static double TryToDouble(object value)
        {
            try
            {
                if (value == null)
                    return 0;

                if (value is bool)
                    return (bool)value ? 1 : 0;

                if (value is double)
                    return (double)value;

                var str = value as string;
                if (str != null)
                {
                    var sep = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

                    double result;
                    double.TryParse(str.Replace(".", ",").Replace(",", sep), out result);
                    return result;
                }

                if (!value.GetType().IsNumericType())
                    return 0;

                return Convert.ToDouble(value);
            }
            catch
            {
                return 0d;
            }
        }

	    public static decimal TryToDecimal(object value)
	    {
	        try
	        {
	            if (value == null)
	                return 0;

	            if (value is bool)
	                return (bool)value ? 1 : 0;

	            if (value is decimal)
	                return (decimal)value;

	            var str = value as string;
	            if (str != null)
	            {
	                var sep = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

	                decimal result;
	                decimal.TryParse(str.Replace(".", ",").Replace(",", sep), out result);
	                return result;
	            }

	            if (!value.GetType().IsNumericType())
	                return 0;

	            return Convert.ToDecimal(value);
	        }
	        catch
	        {
	            return 0m;
	        }
	    }

	    public static int TryToInt(object value)
	    {
	        try
	        {
	            if (value == null)
	                return 0;

	            if (value is bool)
	                return (bool)value ? 1 : 0;

	            if (value is int)
	                return (int)value;

	            if (value is string)
	            {
	                int result;
	                int.TryParse((string)value, out result);
	                return result;
	            }

	            if (!value.GetType().IsNumericType())
	                return 0;

	            return Convert.ToInt16(value);
	        }
	        catch
	        {
	            return 0;
	        }
	    }

	    public static long TryToLong(object value)
	    {
	        try
	        {
	            if (value == null)
	                return 0;

	            if (value is bool)
	                return (bool)value ? 1 : 0;

	            if (value is long)
	                return (long)value;

	            if (value is string)
	            {
	                long result;
	                long.TryParse((string)value, out result);
	                return result;
	            }

	            if (!value.GetType().IsNumericType())
	                return 0;

	            return Convert.ToInt64(value);
	        }
	        catch
	        {
	            return 0;
	        }
	    }

	    internal static ulong TryToULong(object value)
	    {
	        try
	        {
	            if (value == null)
	                return 0;

	            if (value is bool)
	                return (ulong)((bool)value ? 1 : 0);

	            if (value is ulong)
	                return (ulong)value;

	            if (value is string)
	            {
	                ulong result;
	                ulong.TryParse((string)value, out result);
	                return result;
	            }

	            if (!value.GetType().IsNumericType())
	                return 0;

	            return Convert.ToUInt64(value);
	        }
	        catch
	        {
	            return 0;
	        }
	    }

	    public static DateTime TryToDateTime(object value)
	    {
	        try
	        {
	            if (value is DateTime)
	                return (DateTime)value;

	            return (DateTime)StiConvert.ChangeType(value, typeof(DateTime), false);
	        }
	        catch
	        {
	            return DateTime.Now;
	        }
	    }

	    public static TimeSpan TryToTimeSpan(object value)
	    {
	        try
	        {
	            if (value is TimeSpan)
	                return (TimeSpan)value;

	            return (TimeSpan)StiConvert.ChangeType(value, typeof(TimeSpan), false);
	        }
	        catch
	        {
	            return new TimeSpan(0);
	        }
	    }
        #endregion

        #region Methods.TryToNullable
        public static double? TryToNullableDouble(object value)
        {
            if (value == null)
                return null;

            try
            {
                var str = value as string;
                if (str != null)
                {
                    var sep = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

                    double result;
                    if (double.TryParse(str.Replace(".", ",").Replace(",", sep), out result))
                        return result;
                    else
                        return null;
                }

                if (!value.GetType().IsNumericType()) return 0;
                return Convert.ToDouble(value);
            }
            catch
            {
                return null;
            }
        }
        
	    public static decimal? TryToNullableDecimal(object value)
	    {
	        if (value == null)
	            return null;

	        try
	        {
	            var str = value as string;
	            if (str != null)
	            {
	                var sep = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

	                decimal result;
	                if (decimal.TryParse(str.Replace(".", ",").Replace(",", sep), out result))
	                    return result;

	                return null;
	            }

	            if (!value.GetType().IsNumericType()) return 0;
	            return Convert.ToDecimal(value);
	        }
	        catch
	        {
	            return null;
	        }
	    }

        public static int? TryToNullableInt(object value)
        {
            if (value == null)return null;

            try
            {
                if (value is string)
                {
                    int result;
                    if (int.TryParse((string)value, out result))
                        return result;
                    else
                        return null;
                }

                if (!value.GetType().IsNumericType()) return null;
                return Convert.ToInt16(value);
            }
            catch
            {
                return null;
            }
        }

        public static DateTime? TryToNullableDateTime(object value)
	    {
	        if (value is DateTime) return (DateTime)value;
	        if (value == null) return null;

	        return (DateTime?)StiConvert.ChangeType(value, typeof(DateTime?), false);
	    }

	    public static TimeSpan? TryToNullableTimeSpan(object value)
	    {
	        if (value is TimeSpan) return (TimeSpan)value;
	        if (value == null) return null;

	        return (TimeSpan?)StiConvert.ChangeType(value, typeof(TimeSpan?), false);
	    }
        #endregion

        #region Methods.Parse
        public static decimal ParseDecimal(string value)
		{
            if (value == "0")
                return 0m;

            var before = new StringBuilder();
            var after = new StringBuilder();
            var dec = 1;

            var isBefore = true;
            foreach (var chr in value)
            {
                if (chr == ',' || chr == '.')
                {
                    isBefore = false;
                }
                else
                {
                    if (isBefore)
                        before = before.Append(chr);
                    else
                    {
                        after = after.Append(chr);
                        dec *= 10;
                    }
                }
            }
            if (before.Length == 0 && after.Length != 0)
                return (decimal)long.Parse(after.ToString()) / dec;

		    if (before.Length != 0 && after.Length != 0)
		        return long.Parse(before.ToString()) + (decimal)long.Parse(after.ToString()) / dec;

		    if (before.Length != 0 && after.Length == 0)
		        return long.Parse(before.ToString());

		    return 0m;
		}

	    public static double ParseDouble(string value)
	    {
	        if (value == "0")
	            return 0d;

	        var before = new StringBuilder();
	        var after = new StringBuilder();
	        var dec = 1;

	        var isBefore = true;
	        foreach (var chr in value)
	        {
	            if (chr == ',' || chr == '.')
	            {
	                isBefore = false;
	            }
	            else
	            {
	                if (isBefore)
	                    before = before.Append(chr);
	                else
	                {
	                    after = after.Append(chr);
	                    dec *= 10;
	                }
	            }
	        }
	        if (before.Length == 0 && after.Length != 0)
	            return (double)long.Parse(after.ToString()) / dec;

            if (before.Length != 0 && after.Length != 0)
	            return long.Parse(before.ToString()) + (double)long.Parse(after.ToString()) / dec;

            if (before.Length != 0 && after.Length == 0)
	            return long.Parse(before.ToString());

            return 0d;
	    }

	    public static float ParseFloat(string value)
	    {
	        if (value == "0")
	            return 0f;

	        var before = new StringBuilder();
	        var after = new StringBuilder();
	        var dec = 1;

	        var isBefore = true;
	        foreach (var chr in value)
	        {
	            if (chr == ',' || chr == '.')
	            {
	                isBefore = false;
	            }
	            else
	            {
	                if (isBefore)
	                    before = before.Append(chr);
	                else
	                {
	                    after = after.Append(chr);
	                    dec *= 10;
	                }
	            }
	        }
	        if (before.Length == 0 && after.Length != 0)
	            return (float)long.Parse(after.ToString()) / dec;

            if (before.Length != 0 && after.Length != 0)
	            return long.Parse(before.ToString()) + (float)long.Parse(after.ToString()) / dec;

            if (before.Length != 0 && after.Length == 0)
	            return long.Parse(before.ToString());

            return 0f;
	    }
        #endregion
    }
}
