﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

#if NETCORE
using Stimulsoft.System.Windows.Forms;
#else
using System.Windows.Forms;
#endif

namespace Stimulsoft.Base.Helpers
{
    public static class StiComponentProgressHelper
    {
        #region Fields
        private static float deltaValue = 10;
        private static int intervalValue = 20;
        private static Timer timer = new Timer();
        private static bool lockCompletedProgressHandler;
        private static object lockObject = new object();
        private static ObservableCollection<IStiAppComponent> hash = new ObservableCollection<IStiAppComponent>();
        #endregion

        #region Properties
        public static float CurrentValue { get; set; }
        #endregion

        #region Events
        #region Tick
        public static event EventHandler Tick;

        private static void InvokeTick()
        {
            Tick?.Invoke(null, EventArgs.Empty);
        }
        #endregion

        #region CompletedProgress
        public static event EventHandler CompletedProgress;

        private static void InvokeCompletedProgress(object comp)
        {
            if (lockCompletedProgressHandler) return;

            CompletedProgress?.Invoke(comp, EventArgs.Empty);
        }
        #endregion
        #endregion

        #region Handlers
        private static void ActiveProgress_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove && e.OldItems.Count > 0)
                InvokeCompletedProgress(e.OldItems[0]);
            
            if (hash.Count > 0)
                timer.Start();
            else
                timer.Stop();
        }

        private static void Timer_Tick(object sender, EventArgs e)
        {
            CurrentValue += deltaValue;

            if (CurrentValue >= 360)
                CurrentValue = 0;

            InvokeTick();
        }
        #endregion

        #region Methods
        public static void Init()
        {
            timer.Interval = intervalValue;
            timer.Tick += Timer_Tick;
            hash.CollectionChanged += ActiveProgress_CollectionChanged;
        }

        public static void Add(IStiAppComponent comp)
        {
            lock (lockObject)
            {
                hash.Add(comp);
            }
        }

        public static bool Contains(IStiAppComponent comp)
        {
            lock (lockObject)
            {
                return hash.Contains(comp);
            }
        }

        public static void Remove(IStiAppComponent comp, bool lockCompledProgressr = false)
        {
            lock (lockObject)
            {
                lockCompletedProgressHandler = lockCompledProgressr;
                hash.Remove(comp);
                lockCompletedProgressHandler = false;
            }
        }

        public static IEnumerable<IStiAppComponent> FetchAllComponents()
        {
            lock (lockObject)
            {
                return hash.Where(c => c != null);
            }
        }
        #endregion
    }
}
