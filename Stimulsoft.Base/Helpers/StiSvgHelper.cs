#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Base.Helpers
{
    public static class StiSvgHelper
    {
        #region Consts
        private const int MaxSvgOffset = 1000;
        #endregion

        #region Fields.Static
        private static object lockObject = new object();
        private static bool tryToLoadAssembly = false;
        #endregion

        #region Properties.Status
        private static Assembly svgAssembly;
        public static Assembly SvgAssembly
        {
            get
            {
                lock (lockObject)
                {
                    if (svgAssembly == null && !tryToLoadAssembly)
                    {
                        svgAssembly = StiAssemblyFinder.GetAssembly("Svg.dll");
                        tryToLoadAssembly = true;
                    }
                    return svgAssembly;
                }
            }
        }

        public static bool IsSvgAssemblyLoaded
        {
            get
            {
                return svgAssembly != null;
            }
        }
        #endregion

        #region Methods.Static
        public static Image ConvertSvgToImage(byte[] svgObject, int width, int height, bool stretch = true, bool aspectRatio = false, bool rethrowOutOfMemory = false)
        {
            if (svgObject == null || svgObject.Length == 0) return null;

            Image result;
            if (ConvertSvgToImageInternal(svgObject, width, height, stretch, aspectRatio, out result, rethrowOutOfMemory)) return result;

            if (width < 250 && height < 30) return null;

            var image = new Bitmap(width, height);
            using (var g = Graphics.FromImage(image))
            using (var font = new Font("Arial", 8, FontStyle.Bold))
            using (var sf = StringFormat.GenericDefault)
            using (var pen = new Pen(Color.Gray))
            {
                pen.DashStyle = DashStyle.Dash;

                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;

                g.Clear(Color.White);
                g.DrawString(string.Format(StiLocalization.Get("Notices", "IsNotFound"), "Svg.dll"), font, Brushes.Gray, new Rectangle(0, 0, width, height), sf);

            }

            return image;
        }

        private static bool ConvertSvgToImageInternal(byte[] svgObject, int width, int height, bool stretch, bool aspectRatio, out Image result, bool rethrowOutOfMemory)
        {
            result = null;

            try
            {
                if (SvgAssembly == null) return false;
                if (svgObject == null || svgObject.Length == 0) return true;

                //using (var stream = new MemoryStream(svgObject))
                //{
                //    var typeSvgDocument = SvgAssembly.GetType("Svg.SvgDocument");
                //    if (typeSvgDocument == null) return false;

                //    var methodOpen = typeSvgDocument.GetMethod("Open", new[] { typeof(Stream) });
                //    if (methodOpen == null) return false;

                //    var methodGenericOpen = methodOpen.MakeGenericMethod(typeSvgDocument);
                //    var document = methodGenericOpen.Invoke(null, new object[] { stream });
                //    if (document == null) return false;
                //}

                var document = OpenSvg(svgObject);

                if (document == null) return false;

                #region AspectRatio and stretch correction
                var typeSvgUnit = SvgAssembly.GetType("Svg.SvgUnit");
                PropertyInfo propertySvgUnitValue = null;
                if (typeSvgUnit != null)
                {
                    propertySvgUnitValue = typeSvgUnit.GetProperty("Value");
                }
                bool needFixedBitmap = false;
                if ((!stretch || aspectRatio) && (propertySvgUnitValue != null))
                {
                    var svgSize = GetSvgSize(document);

                    if (svgSize.Width != 0 && svgSize.Height != 0)
                    {
                        if (svgSize.Width > width || svgSize.Height > height)
                        {
                            needFixedBitmap = true;
                        }

                        if (aspectRatio)
                        {
                            double svgAspect = svgSize.Width / svgSize.Height;
                            double aspect = (float)width / height;
                            if (svgAspect > aspect)
                            {
                                height = (int)Math.Round(width / svgAspect);
                            }
                            else
                            {
                                width = (int)Math.Round(height * svgAspect);
                            }
                        }
                    }
                }
                #endregion

                if (stretch)
                {
                    var methodDraw = document.GetType().GetMethod("Draw", new[] { typeof(int), typeof(int) });
                    if (methodDraw == null) return false;

                    result = methodDraw.Invoke(document, new object[] { width, height }) as Image;
                }
                else
                {
                    if (needFixedBitmap)
                    {
                        Bitmap bmp = new Bitmap(width, height);

                        var methodDraw = document.GetType().GetMethod("Draw", new[] { typeof(Bitmap) });
                        if (methodDraw == null) return false;

                        object obj = methodDraw.Invoke(document, new object[] { bmp }) as Image;
                        result = bmp;
                    }
                    else
                    {
                        var methodDraw = document.GetType().GetMethod("Draw", new Type[0]);
                        if (methodDraw == null) return false;

                        result = methodDraw.Invoke(document, new object[0]) as Image;
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                if (ex.InnerException is OutOfMemoryException && rethrowOutOfMemory) throw new OutOfMemoryException();
                return false;
            }
        }

        public static bool DrawSvg(byte[] svgObject, RectangleF rect, bool stretch, bool aspectRatio, double zoom, Graphics gr)
        {
            if (svgObject == null || svgObject.Length == 0) return true;
            if (DrawSvgInternal(svgObject, rect, stretch, aspectRatio, zoom, gr)) return true;
            return false;
        }

        private static bool DrawSvgInternal(byte[] svgObject, RectangleF rect, bool stretch, bool aspectRatio, double zoom, Graphics gr)
        {
            try
            {
                if (SvgAssembly == null) return false;
                if (svgObject == null || svgObject.Length == 0) return true;

                var document = OpenSvg(svgObject);
                if (document == null) return false;

                var svgSize = GetSvgSize(document);

                #region AspectRatio and stretch correction
                float width = rect.Width;
                float height = rect.Height;
                if ((!stretch || aspectRatio) && (svgSize.Width != 0 && svgSize.Height != 0))
                {
                    if (aspectRatio)
                    {
                        double svgAspect = svgSize.Width / svgSize.Height;
                        double aspect = (float)width / height;
                        if (svgAspect > aspect)
                        {
                            height = (float)Math.Round(width / svgAspect);
                        }
                        else
                        {
                            width = (float)Math.Round(height * svgAspect);
                        }
                    }
                    if (!stretch)
                    {
                        width = svgSize.Width * (float)zoom;
                        height = svgSize.Height * (float)zoom;
                    }
                }
                #endregion

                var state = gr.Save();
                gr.TranslateTransform(rect.X, rect.Y);
                gr.ScaleTransform(width / svgSize.Width, height / svgSize.Height);

                var methodDraw = document.GetType().GetMethod("Draw", new[] { typeof(Graphics) });
                if (methodDraw == null) return false;
                methodDraw.Invoke(document, new object[] { gr });

                gr.Restore(state);

                return true;
            }
            catch
            {
                return false;
            }
        }

        private static SizeF GetSvgSize(object document)
        {
            SizeF size = new Size();

            var typeSvgDocument = SvgAssembly.GetType("Svg.SvgDocument");
            var typeSvgUnit = SvgAssembly.GetType("Svg.SvgUnit");
            PropertyInfo propertySvgUnitValue = null;
            if (typeSvgUnit != null)
            {
                propertySvgUnitValue = typeSvgUnit.GetProperty("Value");
            }
            if (propertySvgUnitValue != null)
            {
                float svgWidth = 0;
                float svgHeight = 0;

                var propertyWidth = typeSvgDocument.GetProperty("Width");
                if (propertyWidth != null)
                {
                    var propertyValue = propertyWidth.GetValue(document, new object[0]);
                    if (propertyValue != null)
                    {
                        var propertyValueValue = propertySvgUnitValue.GetValue(propertyValue, new object[0]);
                        svgWidth = (float)propertyValueValue;
                    }
                }

                var propertyHeight = typeSvgDocument.GetProperty("Height");
                if (propertyHeight != null)
                {
                    var propertyValue = propertyHeight.GetValue(document, new object[0]);
                    if (propertyValue != null)
                    {
                        var propertyValueValue = propertySvgUnitValue.GetValue(propertyValue, new object[0]);
                        svgHeight = (float)propertyValueValue;
                    }
                }

                size.Width = svgWidth;
                size.Height = svgHeight;
            }
            return size;
        }

        private static object OpenSvg(byte[] svgObject)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.XmlResolver = null;

            using (var stream = new MemoryStream(svgObject))
            {
                xmlDoc.Load(stream);
            }

            #region Fix width="100%" problem
            var attrWidth = xmlDoc.DocumentElement.Attributes["width"];
            var attrHeight = xmlDoc.DocumentElement.Attributes["height"];
            var attrViewBox = xmlDoc.DocumentElement.Attributes["viewBox"];
            if ((attrWidth != null && attrWidth.Value == "100%") && (attrHeight != null && attrHeight.Value == "100%") && (attrViewBox != null && !string.IsNullOrWhiteSpace(attrViewBox.Value)))
            {
                string[] parts = attrViewBox.Value.Split(new char[] { ' ' });
                if (parts.Length == 4)
                {
                    attrWidth.Value = parts[2];
                    attrHeight.Value = parts[3];
                }
            }
            #endregion

            //Fix text "desc" problem
            CheckDescRecursive(xmlDoc.DocumentElement);

            var typeSvgDocument = SvgAssembly.GetType("Svg.SvgDocument");
            if (typeSvgDocument == null) return null;

            var methodOpen = typeSvgDocument.GetMethod("Open", new[] { typeof(XmlDocument) });
            if (methodOpen == null) return null;

            var document = methodOpen.Invoke(null, new object[] { xmlDoc });

            return document;
        }

        private static void CheckDescRecursive(XmlNode baseNode)
        {
            foreach (XmlNode node in baseNode.ChildNodes)
            {
                if (node.HasChildNodes)
                {
                    if ((node.NodeType == XmlNodeType.Element) && (node.Name == "text") && (node.ChildNodes.Count == 2) && (node.ChildNodes[0].Name == "desc") && (node.ChildNodes[1].NodeType == XmlNodeType.Text))
                    {
                        node.RemoveChild(node.ChildNodes[0]);
                    }
                    else
                    {
                        CheckDescRecursive(node);
                    }
                }
            }
        }
        #endregion

        #region Methods.Status
        public static bool IsSvgFile(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return false;
            if (!File.Exists(fileName)) return false;

            var bytes = File.ReadAllBytes(fileName);
            return IsSvg(bytes);
        }

        public static bool IsSvg(byte[] data)
        {
            try
            {
                if (data.Length > 5 && data[0] == '<' && data[1] == 's' && data[2] == 'v' && data[3] == 'g' && char.IsWhiteSpace((char)data[4])) return true;

                var stack = new Stack<bool>();
                var flag1 = false;
                var level = 0;
                var pos = 0;
                while (pos < data.Length - 5 && pos < MaxSvgOffset)
                {
                    if (data[pos] == '<')
                    {
                        if (level == 0 && data[pos + 1] == 's' && data[pos + 2] == 'v' && data[pos + 3] == 'g' && char.IsWhiteSpace((char) data[pos + 4]))
                            return true;

                        if (data[pos + 1] == '/')
                        {
                            level--;
                            flag1 = stack.Pop();
                        }
                        else
                        {
                            level++;
                            stack.Push(flag1);
                            if (data[pos + 1] == '!' || data[pos + 1] == '?')
                                flag1 = true;
                        }
                    }
                    else if (data[pos] == '>')
                    {
                        if ((pos > 1 && data[pos - 1] == '/') || flag1)
                        {
                            level--;
                            flag1 = stack.Pop();
                        }
                    }
                    pos++;
                }
            }
            catch
            {
            }

            return false;
        }
        #endregion
    }
}