﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports 									            }
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;

namespace Stimulsoft.Base.Design
{
    public static class StiLibStatus
    {
        public static bool IsJsAvailable()
        {
            return IsAvailable(StiLibType.Php) || IsAvailable(StiLibType.Js);
        }

        public static bool IsAvailable(StiLibType type)
        {
            return Directory.Exists(GetLibPath(type));
        }

        public static string GetJsLibPathIfExists()
        {
            if (IsAvailable(StiLibType.Js))
            {
                var folder = GetLibPath(StiLibType.Js);
                return Directory.Exists(folder) ? folder : null;
            }

            if (IsAvailable(StiLibType.Php))
            {
                var folder = Path.Combine(GetLibPath(StiLibType.Php), "JS");
                return Directory.Exists(folder) ? folder : null;
            }

            return null;
        }

        public static string GetLibPath(StiLibType type)
        {
            var designerPath = StiDesignerAppStatus.GetDesignerPath(StiPlatformType.Wpf);

            var designerFolder = Path.GetDirectoryName(designerPath);
            var libFolder = Path.Combine(designerFolder, "..", "..", "Libs");

            switch (type)
            {
                case StiLibType.Net:
                    return Path.Combine(libFolder, "Net");

                case StiLibType.Wpf:
                    return Path.Combine(libFolder, "Wpf");

                case StiLibType.Web:
                    return Path.Combine(libFolder, "Web");

                case StiLibType.Js:
                    return Path.Combine(libFolder, "JS");

                case StiLibType.Php:
                    return Path.Combine(libFolder, "PHP");

                case StiLibType.Java:
                    return Path.Combine(libFolder, "Java");

                case StiLibType.Flex:
                    return Path.Combine(libFolder, "Flex");

                default:
                    throw new NotSupportedException();
            }
        }
    }
}