﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports 									            }
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF STIMULSOFT										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Reflection;

namespace Stimulsoft.Base.Design
{
    public static class StiDesignerAppStatus
    {
        public static bool IsAvailable(StiPlatformType type)
        {
            return File.Exists(GetDesignerPath(type));
        }

        public static string GetDesignerPath(StiPlatformType type)
        {
            var designerFolder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            switch (type)
            {
                case StiPlatformType.WinForms:
                    return Path.Combine(designerFolder, "Designer.WinForms.exe");

                case StiPlatformType.Wpf:
                    return Path.Combine(designerFolder, "DesignerV2.Wpf.exe");

                case StiPlatformType.Js:
                    return Path.Combine(designerFolder, "..", "Js", "Designer.exe");

                case StiPlatformType.Flex:
                    return Path.Combine(designerFolder, "..", "Flex", "DesignerFx.exe");

                default:
                    throw new NotSupportedException();
            }
        }

        public static bool IsRunning
        {
            get
            {
                var assembly = Assembly.GetEntryAssembly();
                if (assembly == null) return false;

                if (!assembly.FullName.Contains(StiPublicKeyToken.Key))return false;

                var name = assembly.GetName();

                var code = name.CodeBase;
                if (string.IsNullOrWhiteSpace(code)) return false;

                return
                    code.EndsWithInvariantIgnoreCase("viewer.exe") ||
                    code.EndsWithInvariantIgnoreCase("designer.exe") ||
                    code.EndsWithInvariantIgnoreCase("designer.winforms.exe") ||
                    code.EndsWithInvariantIgnoreCase("designer.wpf.exe") ||
                    code.EndsWithInvariantIgnoreCase("designerv2.wpf.exe");
            }
        }
    }
}