#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Text.RegularExpressions;

namespace Stimulsoft.Base
{
    public static class StringExt
    {
        public static string ToLowerNull(string str)
        {
            return string.IsNullOrWhiteSpace(str) ? str : str.ToLowerInvariant();
        }

        public static bool IsBase64String(this string s)
        {
            s = s.Trim().Replace("\r", string.Empty).Replace("\n", string.Empty);
            return (s.Length % 4 == 0) && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
        }

        public static bool StartsWithInvariantIgnoreCase(this string str, string check)
        {
            return str.StartsWith(check, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool EndsWithInvariantIgnoreCase(this string str, string check)
	    {
            return str.EndsWith(check, StringComparison.InvariantCultureIgnoreCase);
	    }

        public static bool StartsWithInvariant(this string str, string check)
        {
            return str.StartsWith(check, StringComparison.InvariantCulture);
        }

        public static bool EndsWithInvariant(this string str, string check)
        {
            return str.EndsWith(check, StringComparison.InvariantCulture);
        }

        public static string ToEmpty(this string str)
        {
            return str ?? string.Empty;
        }
    }
}
