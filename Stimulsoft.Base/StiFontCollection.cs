#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Drawing;
using System.Collections;
using Stimulsoft.Base.Drawing;

namespace Stimulsoft.Base
{
    public static class StiFontCollection
    {
        #region Fields.Static
        private static object lockObject = new object();
        private static Hashtable fontFamilyHash = new Hashtable();
        private static Hashtable fontPathHash = new Hashtable();
        private static Hashtable fontVHash = new Hashtable();
        private static Hashtable fontVCountHash = new Hashtable();
        #endregion

        #region Properties.Static
        private static PrivateFontCollection instance = new PrivateFontCollection();
        public static PrivateFontCollection Instance
        {
            get
            {
                return instance ?? (instance = new PrivateFontCollection());
            }
        }

        public static string DefaultCachePath { get; set; }
        #endregion

        #region Methods.Static
        public static void AddFontFile(string fileName)
        {
            lock (lockObject)
            {
                Instance.AddFontFile(fileName);
                fontPathHash[Instance.Families.LastOrDefault().Name] = fileName;
            }
        }

        public static void AddMemoryFont(IntPtr memory, int length)
        {
            lock (lockObject)
            {
                Instance.AddMemoryFont(memory, length);
            }
        }

        public static List<FontFamily> GetFontFamilies()
        {
            lock (lockObject)
            {
                var fonts = FontFamily.Families.ToList();
                fonts.AddRange(instance.Families);

                foreach (DictionaryEntry de in fontVHash)
                {
                    var fontV = de.Value as FontV;
                    if (fontV != null && fontV.Font != null)
                        fonts.Add(fontV.Font.FontFamily);
                }

                return fonts.OrderBy(f => f.Name).ToList();
            }
        }

        public static List<FontFamily> GetFontFamiliesFirstCustom()
        {
            lock (lockObject)
            {
                var listResult = new List<FontFamily>();

                foreach (DictionaryEntry de in fontVHash)
                {
                    var fontV = de.Value as FontV;
                    if (fontV != null && fontV.Font != null)
                        listResult.Add(fontV.Font.FontFamily);
                }

                listResult = listResult.OrderBy(x => x.Name).ToList();

                listResult.AddRange(FontFamily.Families.ToList());
                listResult.AddRange(instance.Families);

                return listResult;
            }
        }

        public static FontFamily GetFontFamily(string fontName, bool allowNullResult = false)
        {
            lock (lockObject)
            {
                var fontFamily = fontFamilyHash[fontName] as FontFamily;
                if (fontFamily != null) return fontFamily;

                fontFamily = GetFontFamilies().FirstOrDefault(f => f.Name == fontName);
                if (fontFamily == null)
                {
                    using (var font = new Font(fontName, 1f, FontStyle.Regular))
                    {
                        if ((font.Name != fontName) && allowNullResult)
                            return null;

                        fontFamily = font.FontFamily;
                    }
                }

                fontFamilyHash[fontName] = fontFamily;
                return fontFamily;
            }
        }

        public static bool IsCustomFont(string fontName)
        {
            if (Instance.Families.Any(f => f.Name == fontName)) return true;
            return fontVHash.Values
                .Cast<FontV>()
                .Any(f => f != null && f.Font.FontFamily.Name == fontName);
        }

        public static string GetCustomFontPath(string fontName)
        {
            var path = fontPathHash[fontName] as string;
            if (path == null) return null;
            if (!path.StartsWith("file:///")) path = "file:///" + path;

            return path;
        }

        public static Font CreateFont(string fontName, float fontSize, FontStyle fontStyle)
        {
            return new Font(GetFontFamily(fontName), fontSize, fontStyle);
        }
        #endregion

        #region Methods.ResourceFont
        public static void AddResourceFont(string name, byte[] content, string extension)
        {
            if (content == null || content.Length == 0) return;

            string hash = FontV.GetHashName(content);
            FontV fontV = fontVHash[hash] as FontV;
            if (fontV == null)
            {
                lock (lockObject)
                {
                    fontV = new FontV(name, content, extension);
                    fontVHash[hash] = fontV;
                    fontPathHash[fontV.Font.FontFamily.Name] = fontV.CacheFile;
                    fontFamilyHash.Remove(fontV.Font.FontFamily.Name);
                }
            }
            IncreaseCount(fontV);
        }

        public static FontFamily GetFontFamilyByContent(string name, byte[] content, string extension)
        {
            if (content == null || content.Length == 0) return null;

            string hash = FontV.GetHashName(content);
            FontV fontV = fontVHash[hash] as FontV;
            if (fontV == null)
            {
                lock (lockObject)
                {
                    fontV = new FontV(name, content, extension);
                    fontVHash[hash] = fontV;
                    fontPathHash[fontV.Font.FontFamily.Name] = fontV.CacheFile;
                    fontFamilyHash.Remove(fontV.Font.FontFamily.Name);
                }
                //IncreaseCount(fontV);
            }
            return fontV.Font.FontFamily;
        }

        private static void IncreaseCount(FontV fontV)
        {
            lock (lockObject)
            {
                int count = 1;
                object objCount = fontVCountHash[fontV];
                if (objCount != null)
                {
                    count = Convert.ToInt32(objCount);
                    count++;
                }
                fontVCountHash[fontV] = count;
            }
        }

        public static void RemoveResourceFont(string name, byte[] content)
        {
            if (content == null || content.Length == 0) return;

            string hash = FontV.GetHashName(content);
            var fontV = fontVHash[hash];
            if (fontV == null) return;

            lock (lockObject)
            {
                object objCount = fontVCountHash[fontV];
                if (objCount == null) return;
                int count = Convert.ToInt32(objCount);
                if (count > 0) count--;
                fontVCountHash[fontV] = count;
            }
        }

        public static void RemoveUnusedResourceFonts()
        {
            lock (lockObject)
            {
                ArrayList fonts1 = new ArrayList();
                ArrayList fonts2 = new ArrayList();
                foreach (DictionaryEntry de in fontVCountHash)
                {
                    int count = Convert.ToInt32(de.Value);
                    if (count == 0)
                    {
                        fonts2.Add(de.Key);
                        var fontV = de.Key as FontV;
                        if (fontV != null)
                        {
                            foreach (DictionaryEntry de2 in fontVHash)
                            {
                                if (de2.Value == fontV) fonts1.Add(de2.Key);
                            }
                        }
                    }
                }

                foreach (object obj in fonts1)
                {
                    fontVHash.Remove(obj);
                }
                foreach (object obj in fonts2)
                {
                    fontVCountHash.Remove(obj);
                }
            }
        }

        public static void RemoveAllResourceFonts()
        {
            lock (lockObject)
            {
                fontVCountHash.Clear();
                foreach (DictionaryEntry de in fontVHash)
                {
                    var fontV = de.Value as FontV;
                    if (fontV != null) fontV.Dispose();
                }
                fontVHash.Clear();
            }
        }
        #endregion
    }
}
