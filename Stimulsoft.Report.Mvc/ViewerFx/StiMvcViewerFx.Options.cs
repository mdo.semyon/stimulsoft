﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Globalization;
using Stimulsoft.Base.Json;
using Stimulsoft.Report.Web;
using System.Web.Caching;

namespace Stimulsoft.Report.Mvc
{
    public class StiMvcViewerFxOptions
    {
        #region Actions

        public class ActionOptions
        {
            /// <summary>
            /// Gets or sets the action method name of preparing the rendered report.
            /// </summary>
            public string GetReport { get; set; }
            
            /// <summary>
            /// Gets or sets the action method name of loading a localization file for a viewer.
            /// </summary>
            public string GetLocalization { get; set; }

            /// <summary>
            /// Gets or sets the action method name of exporting a report to the required format.
            /// </summary>
            public string ExportReport { get; set; }

            /// <summary>
            /// Gets or sets the action method name of exporting a report to the required format and send it by email.
            /// </summary>
            public string EmailReport { get; set; }

            /// <summary>
            /// Gets or sets the action method name of clicking to design button.
            /// </summary>
            public string DesignReport { get; set; }
            
            /// <summary>
            /// Gets or sets the name of the action method for redirect to the desired view when exiting the viewer.
            /// </summary>
            public string Exit { get; set; }

            /// <summary>
            /// Gets or sets the action method name of the report viewer default events.
            /// </summary>
            public string ViewerEvent { get; set; }

            #region Obsolete

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the option GetReport instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public string GetReportSnapshot
            {
                get
                {
                    return GetReport;
                }
                set
                {
                    GetReport = value;
                }
            }

            #endregion

            public ActionOptions()
            {
                GetReport = string.Empty;
                GetLocalization = string.Empty;
                ExportReport = string.Empty;
                EmailReport = string.Empty;
                DesignReport = string.Empty;
                Exit = string.Empty;
                ViewerEvent = string.Empty;
            }
        }

        #endregion

        #region Appearance

        public class AppearanceOptions
        {
            /// <summary>
            /// Gets or sets the text to display in the About dialog of the viewer.
            /// </summary>
            public string AboutDialogTextLine1 { get; set; }

            /// <summary>
            /// Gets or sets the text to display in the About dialog of the viewer.
            /// </summary>
            public string AboutDialogTextLine2 { get; set; }

            /// <summary>
            /// Gets or sets the URL to display in the About dialog of the viewer.
            /// </summary>
            public string AboutDialogUrl { get; set; }

            /// <summary>
            /// Gets or sets the URL text to display in the About dialog of the viewer.
            /// </summary>
            public string AboutDialogUrlText { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the Cancel button in the window of getting the data from the server.
            /// </summary>
            public bool ShowCancelButton { get; set; }

            /// <summary>
            /// Gets or sets the background color of the viewer.
            /// </summary>
            public Color BackgroundColor { get; set; }

            /// <summary>
            /// Gets or sets the Window Mode property of the SWF file for transparency, layering, positioning, and rendering in the browser. 
            /// </summary>
            public StiWMode FlashWMode { get; set; }

            /// <summary>
            /// Gets or sets the value that allows automatically hide report scrollbars if not required.
            /// </summary>
            public bool AutoHideScrollbars { get; set; }

            /// <summary>
            /// Gets or sets a color of the border of the current report page.
            /// </summary>
            public Color CurrentPageBorderColor { get; set; }

            /// <summary>
            /// Gets or sets a count columns in parameters panel.
            /// </summary>
            public int ParametersPanelColumnsCount { get; set; }

            /// <summary>
            /// Gets or sets a editor width in parameters panel.
            /// </summary>
            public int ParametersPanelEditorWidth { get; set; }
            
            private string parametersPanelDateFormat = StiDateFormatMode.FromClient;
            /// <summary>
            /// Gets or sets a date format for datetime parameters in parameters panel. To use a server date format please set the StiDateFormatMode.FromServer or "FromServer" string value.
            /// The default is the client system date format.
            /// </summary>
            public string ParametersPanelDateFormat
            {
                get
                {
                    if (parametersPanelDateFormat == StiDateFormatMode.FromServer) return CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern + " " + CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern;
                    return parametersPanelDateFormat;
                }
                set
                {
                    parametersPanelDateFormat = value;
                }
            }

            /// <summary>
            /// Gets or sets a browser window to open links from the report.
            /// </summary>
            public string OpenLinksWindow { get; set; }

            /// <summary>
            /// Gets or sets a browser window to open the exported report.
            /// </summary>
            public string OpenExportedReportWindow { get; set; }

            /// <summary>
            /// Gets or sets an images quality for the report components such as Rich Text, Rotated Text, Charts, Bar Codes.
            /// </summary>
            public StiImagesQuality ImagesQuality
            {
                get
                {
                    return StiReportHelperFx.ImagesQuality;
                }
                set
                {
                    StiReportHelperFx.ImagesQuality = value;
                }
            }

            /// <summary>
            /// Gets or sets a value which indicates that show the tooltips.
            /// </summary>
            public bool ShowTooltips { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that show or hide the help link in tooltips.
            /// </summary>
            public bool ShowTooltipsHelp { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that show or hide the help button in dialogs.
            /// </summary>
            public bool ShowFormsHelp { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that show or hide help tips in dialogs.
            /// </summary>
            public bool ShowFormsHints { get; set; }

            /// <summary>
            /// Gets or sets a value which change the title of the window that contains the viewer.
            /// If empty, it will use the alias or the name of the report. If "Default", it will use the title of the HTML page.
            /// </summary>
            public string BrowserTitle { get; set; }

            #region Obsolete

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the option OpenLinksWindow instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public string OpenLinksTarget
            {
                get
                {
                    return OpenLinksWindow;
                }
                set
                {
                    OpenLinksWindow = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the option OpenExportedReportWindow instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public string OpenExportedReportTarget
            {
                get
                {
                    return OpenExportedReportWindow;
                }
                set
                {
                    OpenExportedReportWindow = value;
                }
            }

            #endregion

            public AppearanceOptions()
            {
                AboutDialogTextLine1 = string.Empty;
                AboutDialogTextLine2 = string.Empty;
                AboutDialogUrl = string.Empty;
                AboutDialogUrlText = string.Empty;
                ShowCancelButton = false;
                BackgroundColor = Color.White;
                FlashWMode = StiWMode.Window;
                AutoHideScrollbars = false;
                CurrentPageBorderColor = Color.Gold;
                ParametersPanelColumnsCount = 2;
                ParametersPanelEditorWidth = 200;
                OpenLinksWindow = StiTargetWindow.Blank;
                OpenExportedReportWindow = StiTargetWindow.Blank;
                ShowTooltips = true;
                ShowTooltipsHelp = true;
                ShowFormsHelp = true;
                ShowFormsHints = true;
                BrowserTitle = "Default";
            }
        }

        #endregion

        #region Email

        public class EmailOptions
        {
            /// <summary>
            /// Gets or sets a value which allows to display the Email dialog, or send Email with the default settings.
            /// </summary>
            public bool ShowEmailDialog { get; set; }

            /// <summary>
            /// Gets or sets a value which allows to display the export dialog for Email, or export report for Email with the default settings.
            /// </summary>
            public bool ShowExportDialog { get; set; }

            /// <summary>
            /// Gets or sets the default email address of the message created in the viewer. 
            /// </summary>
            public string DefaultEmailAddress { get; set; }

            public EmailOptions()
            {
                ShowEmailDialog = true;
                ShowExportDialog = true;
                DefaultEmailAddress = string.Empty;
            }
        }

        #endregion

        #region Exports

        public class ExportOptions
        {
            /// <summary>
            /// Gets or sets a value which allows to display the export dialog, or to export with the default settings.
            /// </summary>
            public bool ShowExportDialog { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the report document file.
            /// </summary>
            public bool ShowExportToDocument { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the PDF format.
            /// </summary>
            public bool ShowExportToPdf { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the XPS format.
            /// </summary>
            public bool ShowExportToXps { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the Power Point 2007-2010 format.
            /// </summary>
            public bool ShowExportToPowerPoint { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the HTML format.
            /// </summary>
            public bool ShowExportToHtml { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the HTML5 format.
            /// </summary>
            public bool ShowExportToHtml5 { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the MHT (Web Archive) format.
            /// </summary>
            public bool ShowExportToMht { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the TEXT format.
            /// </summary>
            public bool ShowExportToText { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the Rich Text format.
            /// </summary>
            public bool ShowExportToRtf { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the Word 2007-2010 format.
            /// </summary>
            public bool ShowExportToWord2007 { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the Open Document Text format.
            /// </summary>
            public bool ShowExportToOpenDocumentWriter { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the Excel BIFF format.
            /// </summary>
            public bool ShowExportToExcel { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the Excel XML format.
            /// </summary>
            public bool ShowExportToExcelXml { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the Excel 2007-2010 format.
            /// </summary>
            public bool ShowExportToExcel2007 { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the Open Document Calc format.
            /// </summary>
            public bool ShowExportToOpenDocumentCalc { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the CSV format.
            /// </summary>
            public bool ShowExportToCsv { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the DBF format.
            /// </summary>
            public bool ShowExportToDbf { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the XML format.
            /// </summary>
            public bool ShowExportToXml { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the DIF format.
            /// </summary>
            public bool ShowExportToDif { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the Sylk format.
            /// </summary>
            public bool ShowExportToSylk { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the BMP image format.
            /// </summary>
            public bool ShowExportToImageBmp { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the GIF image format.
            /// </summary>
            public bool ShowExportToImageGif { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the JPEG image format.
            /// </summary>
            public bool ShowExportToImageJpeg { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the PCX image format.
            /// </summary>
            public bool ShowExportToImagePcx { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the PNG image format.
            /// </summary>
            public bool ShowExportToImagePng { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the TIFF image format.
            /// </summary>
            public bool ShowExportToImageTiff { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the Metafile image format.
            /// </summary>
            public bool ShowExportToImageMetafile { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the SVG image format.
            /// </summary>
            public bool ShowExportToImageSvg { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the viewer to the SVGZ image format.
            /// </summary>
            public bool ShowExportToImageSvgz { get; set; }

            public ExportOptions()
            {
                ShowExportDialog = true;
                ShowExportToDocument = true;
                ShowExportToPdf = true;
                ShowExportToXps = true;
                ShowExportToPowerPoint = true;
                ShowExportToHtml = true;
                ShowExportToHtml5 = true;
                ShowExportToMht = true;
                ShowExportToText = true;
                ShowExportToRtf = true;
                ShowExportToWord2007 = true;
                ShowExportToOpenDocumentWriter = true;
                ShowExportToExcel = true;
                ShowExportToExcelXml = true;
                ShowExportToExcel2007 = true;
                ShowExportToOpenDocumentCalc = true;
                ShowExportToCsv = true;
                ShowExportToDbf = true;
                ShowExportToXml = true;
                ShowExportToDif = true;
                ShowExportToSylk = true;
                ShowExportToImageBmp = true;
                ShowExportToImageGif = true;
                ShowExportToImageJpeg = true;
                ShowExportToImagePcx = true;
                ShowExportToImagePng = true;
                ShowExportToImageTiff = true;
                ShowExportToImageMetafile = true;
                ShowExportToImageSvg = true;
                ShowExportToImageSvgz = true;
            }
        }

        #endregion

        #region Print

        public class PrintOptions
        {
            /// <summary>
            /// Gets or sets a value which allows to display the Print dialog, or print report with the default settings.
            /// </summary>
            public bool ShowPrintDialog { get; set; }

            /// <summary>
            /// Gets or sets a value which allows the automatic page orientation when printing the report.
            /// </summary>
            public bool AutoPageOrientation { get; set; }

            /// <summary>
            /// Gets or sets a value which allows the automatic page scale when printing the report.
            /// </summary>
            public bool AutoPageScale { get; set; }

            /// <summary>
            /// Gets or sets a value which allows Default printing (by Flash engine).
            /// </summary>
            public bool AllowDefaultPrint { get; set; }

            /// <summary>
            /// Gets or sets a value which allows printing in PDF format.
            /// </summary>
            public bool AllowPrintToPdf { get; set; }

            /// <summary>
            /// Gets or sets a value which allows printing in HTML format.
            /// </summary>
            public bool AllowPrintToHtml { get; set; }

            /// <summary>
            /// Gets or sets a value which allows send the report to print as Image (only for Default format).
            /// </summary>
            public bool PrintAsBitmap { get; set; }

            public PrintOptions()
            {
                ShowPrintDialog = true;
                AutoPageOrientation = true;
                AutoPageScale = true;
                AllowDefaultPrint = true;
                AllowPrintToPdf = true;
                AllowPrintToHtml = true;
                PrintAsBitmap = true;
            }
        }

        #endregion

        #region Server

        public class ServerOptions
        {
            /// <summary>
            /// Gets or sets the name of the query processing controller of the report viewer. 
            /// </summary>
            public string Controller { get; set; }

            /// <summary>
            /// Gets or sets the URL pattern of the route of the report viewer. The {action} parameter can be used for the component actions, for example: /Home/{action}
            /// </summary>
            public string RouteTemplate { get; set; } = string.Empty;

            private int requestTimeout = 30;
            /// <summary>
            /// Gets or sets time which indicates how many seconds the client side will wait for the response from the server side. The default value is 30 seconds.
            /// </summary>
            public int RequestTimeout
            {
                get
                {
                    return requestTimeout;
                }
                set
                {
                    // Min 1 sec. Max 6 hours.
                    requestTimeout = Math.Max(1, Math.Min(21600, value));
                }
            }

            /// <summary>
            /// Gets or sets time which indicates how many minutes the result of the report rendering will be stored in the server cache or session. The default value is 10 minutes.
            /// </summary>
            public int CacheTimeout { get; set; }

            /// <summary>
            /// Gets or sets the mode of the report caching.
            /// </summary>
            public StiServerCacheMode CacheMode { get; set; }

            /// <summary>
            /// Specifies the relative priority of report, stored in the system cache.
            /// </summary>
            public CacheItemPriority CacheItemPriority { get; set; }

            /// <summary>
            /// Gets or sets the the number of repeats of requests of the server side to the client side, when getting errors of obtaining data.
            /// </summary>
            public int RepeatCount { get; set; }
            
            /// <summary>
            /// Gets or sets the value that enables or disables the logging of all requests/responses.
            /// </summary>
            public bool EnableDataLogger { get; set; }
            
            /// <summary>
            /// Gets or sets a value which indicates that the viewer will use relative or absolute URLs.
            /// </summary>
            public bool UseRelativeUrls { get; set; }

            /// <summary>
            /// Gets or sets a value which enables or disables the transfer of URL parameters when requesting the scripts and styles of the viewer.
            /// </summary>
            public bool PassQueryParametersForResources { get; set; }
            
            public ServerOptions()
            {
                Controller = string.Empty;
                CacheTimeout = 10;
                CacheMode = StiServerCacheMode.ObjectCache;
                CacheItemPriority = CacheItemPriority.Default;
                RepeatCount = 1;
                EnableDataLogger = false;
                UseRelativeUrls = true;
                PassQueryParametersForResources = true;
            }
        }

        #endregion
        
        #region Toolbar

        public class ToolbarOptions
        {
            /// <summary>
            /// Gets or sets a value which indicates that toolbar will be shown in the viewer.
            /// </summary>
            public bool Visible { get; set; }
            
            /// <summary>
            /// Gets or sets a value which indicates that navigate panel will be shown in the viewer.
            /// </summary>
            public bool ShowNavigatePanel { get; set; }
            
            /// <summary>
            /// Gets or sets a value which indicates that view mode panel will be shown in the viewer.
            /// </summary>
            public bool ShowViewModePanel { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Print button in the toolbar of the viewer.
            /// </summary>
            public bool ShowPrintButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Open button in the toolbar of the viewer.
            /// </summary>
            public bool ShowOpenButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Save button in the toolbar of the viewer.
            /// </summary>
            public bool ShowSaveButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Send Email button in the toolbar of the viewer.
            /// </summary>
            public bool ShowSendEmailButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Bookmarks button in the toolbar of the viewer.
            /// </summary>
            public bool ShowBookmarksButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Parameters button in the toolbar of the viewer.
            /// </summary>
            public bool ShowParametersButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Thumbnails button in the toolbar of the viewer.
            /// </summary>
            public bool ShowThumbnailsButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Find button in the toolbar of the viewer.
            /// </summary>
            public bool ShowFindButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Full Screen button in the toolbar of the viewer.
            /// </summary>
            public bool ShowFullScreenButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Exit button in the toolbar of the viewer.
            /// </summary>
            public bool ShowExitButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the About button in the toolbar of the viewer.
            /// </summary>
            public bool ShowAboutButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Design button in the toolbar of the viewer.
            /// </summary>
            public bool ShowDesignButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the First Page button in the navigate panel of the viewer.
            /// </summary>
            public bool ShowFirstPageButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Previous Page button in the navigate panel of the viewer.
            /// </summary>
            public bool ShowPreviousPageButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Go To Page button in the navigate panel of the viewer.
            /// </summary>
            public bool ShowGoToPageButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Next Page button in the navigate panel of the viewer.
            /// </summary>
            public bool ShowNextPageButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Last Page button in the navigate panel of the viewer.
            /// </summary>
            public bool ShowLastPageButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Single Page button in the view mode panel of the viewer.
            /// </summary>
            public bool ShowSinglePageViewModeButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Continuous Page button in the view mode panel of the viewer.
            /// </summary>
            public bool ShowContinuousPageViewModeButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Multiple Page button in the view mode panel of the viewer.
            /// </summary>
            public bool ShowMultiplePageViewModeButton { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of Zoom buttons in the toolbar of the viewer.
            /// </summary>
            public bool ShowZoomButtons { get; set; }

            private int zoom = StiZoomModeFx.Default;
            /// <summary>
            /// Gets or sets the report showing zoom.
            /// </summary>
            public int Zoom
            {
                get
                {
                    return zoom;
                }
                set
                {
                    if (value > 500) zoom = 500;
                    if (value < 0 || (value < 10 && value > 4)) zoom = 10;
                    else zoom = value;
                }
            }

            public ToolbarOptions()
            {
                Visible = true;
                ShowNavigatePanel = true;
                ShowViewModePanel = true;
                ShowPrintButton = true;
                ShowOpenButton = false;
                ShowSaveButton = true;
                ShowSendEmailButton = false;
                ShowBookmarksButton = true;
                ShowParametersButton = true;
                ShowThumbnailsButton = true;
                ShowFindButton = true;
                ShowFullScreenButton = true;
                ShowExitButton = false;
                ShowAboutButton = true;
                ShowDesignButton = false;
                ShowFirstPageButton = true;
                ShowPreviousPageButton = true;
                ShowGoToPageButton = true;
                ShowNextPageButton = true;
                ShowLastPageButton = true;
                ShowSinglePageViewModeButton = true;
                ShowContinuousPageViewModeButton = true;
                ShowMultiplePageViewModeButton = true;
                ShowZoomButtons = true;
            }
        }

        #endregion

        #region StiMvcViewerFxOptions

        /// <summary>
        /// A class which controls settings of the viewer actions.
        /// </summary>
        public ActionOptions Actions { get; set; }

        /// <summary>
        /// A class which controls settings of the viewer appearance.
        /// </summary>
        public AppearanceOptions Appearance { get; set; }

        /// <summary>
        /// A class which controls the email options.
        /// </summary>
        public EmailOptions Email { get; set; }

        /// <summary>
        /// A class which controls the export options.
        /// </summary>
        public ExportOptions Exports { get; set; }
        
        /// <summary>
        /// A class which controls the print options.
        /// </summary>
        public PrintOptions Print { get; set; }

        /// <summary>
        /// A class which controls the server options.
        /// </summary>
        public ServerOptions Server { get; set; }

        /// <summary>
        /// A class which controls settings of the viewer toolbar.
        /// </summary>
        public ToolbarOptions Toolbar { get; set; }

        /// <summary>
        /// Gets or sets the current visual theme which is used for drawing visual elements of the viewer.
        /// </summary>
        public StiViewerFxTheme Theme { get; set; }
        
        /// <summary>
        /// Gets or sets a path to the localization file for the viewer.
        /// </summary>
        public string Localization { get; set; }
        
        /// <summary>
        /// Gets or sets the width of the viewer.
        /// </summary>
        public Unit Width { get; set; }
        
        /// <summary>
        /// Gets or sets the height of the viewer.
        /// </summary>
        public Unit Height { get; set; }

        public StiMvcViewerFxOptions()
        {
            Actions = new ActionOptions();
            Appearance = new AppearanceOptions();
            Email = new EmailOptions();
            Exports = new ExportOptions();
            Print = new PrintOptions();
            Server = new ServerOptions();
            Toolbar = new ToolbarOptions();

            Theme = StiViewerFxTheme.Office2013;
            Localization = string.Empty;
            Width = Unit.Percentage(100);
            Height = Unit.Pixel(650);
        }

        #endregion
    }
}
