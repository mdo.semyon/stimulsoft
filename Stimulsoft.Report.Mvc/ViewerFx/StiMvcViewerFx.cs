﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Report.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcViewerFx : Panel
    {
        #region Fields

        private HtmlHelper htmlHelper;
        private StiMvcViewerFxOptions options;

        #endregion

        #region Properties

        private static StiCacheHelper cacheHelper = null;
        /// <summary>
        /// Gets or sets an instance of the StiCacheHelper class that will be used for report caching on the server side.
        /// </summary>
        public static StiCacheHelper CacheHelper
        {
            get
            {
                if (cacheHelper == null) cacheHelper = new StiCacheHelper();
                return cacheHelper;
            }
            set
            {
                cacheHelper = value;
            }
        }

        #endregion

        #region Internal

        /// <summary>
        /// Get the URL for viewer requests
        /// </summary>
        internal static string GetRequestUrl(HtmlHelper htmlHelper, string template, string controller, bool useRelativeUrls, bool passQueryParameters)
        {
            return StiMvcViewer.GetRequestUrl(htmlHelper, template, controller, useRelativeUrls, passQueryParameters);
        }

        /// <summary>
        /// Get the URL for viewer resources
        /// </summary>
        private static string GetResourceUrl(string requestUrl, string viewerEvent)
        {
            var resourceUrl = requestUrl.Replace("{action}", viewerEvent) + (requestUrl.IndexOf("?") > 0 ? "&" : "?");
            resourceUrl += "stiweb_component=ViewerFx&stiweb_action=Resource&stiweb_data={0}&stiweb_version=" + StiVersionHelper.AssemblyVersion;

            return resourceUrl;
        }

        #endregion

        public StiMvcViewerFx(HtmlHelper htmlHelper, string id, StiMvcViewerFxOptions options)
        {
            this.htmlHelper = htmlHelper;
            this.ID = id;
            this.options = options;
        }
    }
}
