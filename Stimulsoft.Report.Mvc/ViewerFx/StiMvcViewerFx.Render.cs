﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using Stimulsoft.Base;
using Stimulsoft.Base.Licenses;
using Stimulsoft.Report.Web;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcViewerFx : Panel
    {
        #region Render scripts

        private StiJavaScript RenderMainScript()
        {
            string requestUrl = GetRequestUrl(htmlHelper, options.Server.RouteTemplate, options.Server.Controller, options.Server.UseRelativeUrls, options.Server.PassQueryParametersForResources);
            string resourceUrl = GetResourceUrl(requestUrl, options.Actions.ViewerEvent);
            var script = new StiJavaScript();
            script.ScriptUrl = string.Format(resourceUrl, "scripts");

            return script;
        }

        private StiJavaScript RenderFlashScript()
        {
            var productVersion = StiVersionHelper.ProductVersion.Trim();

            #region LicenseKey
            var key = StiLicenseKeyValidator.GetLicenseKey();
            var isTrial = !StiLicenseKeyValidator.IsValid(StiProductIdent.Web, key);
            if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key))isTrial = true;

            #region IsValidLicenseKey
            if (!isTrial)
            {
                try
                {
                    using (var rsa = new RSACryptoServiceProvider(512))
                    using (var sha = new SHA1CryptoServiceProvider())
                    {
                        rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                        isTrial = !rsa.VerifyData(key.GetCheckBytes(), sha, key.GetSignatureBytes());
                    }
                }
                catch (Exception)
                {
                    isTrial = true;
                }
            }
            #endregion

            if (!isTrial) productVersion += " ";
            #endregion

            string requestUrl = GetRequestUrl(htmlHelper, options.Server.RouteTemplate, options.Server.Controller, options.Server.UseRelativeUrls, options.Server.PassQueryParametersForResources);
            string resourceUrl = GetResourceUrl(requestUrl, options.Actions.ViewerEvent);
            string version = Convert.ToBase64String(Encoding.UTF8.GetBytes(productVersion));
            string config = RenderConfig();
            requestUrl = Convert.ToBase64String(Encoding.UTF8.GetBytes(GetRequestUrl(htmlHelper, options.Server.RouteTemplate, options.Server.Controller, options.Server.UseRelativeUrls, true)));

            var actions = string.Format("previewreport:'{0}',exportreport:'{1}',emailreport:'{2}',designreport:'{3}',getlocalization:'{4}',exit:'{5}'",
                options.Actions.GetReport,
                string.IsNullOrEmpty(options.Actions.ExportReport) ? options.Actions.ViewerEvent : options.Actions.ExportReport,
                options.Actions.EmailReport,
                options.Actions.DesignReport,
                string.IsNullOrEmpty(options.Actions.GetLocalization) ? options.Actions.ViewerEvent : options.Actions.GetLocalization,
                options.Actions.Exit);

            var script = new StiJavaScript();
            script.Text = string.Format(
                "swfobject.embedSWF('{0}','flashContent','100%','100%','11.1.0','{1}'," +
                "{{config:'{2}',url:'{3}',version:'{4}',theme:'{5}',id:'{6}',cachetimeout:'{7}',cachemode:'{8}',cacheitempriority:'{9}',clientguid:'{10}',{11}}}," +
                "{{quality:'high',bgcolor:'{12}',allowscriptaccess:'always',allowfullscreen:true,wmode:'{13}'}}," +
                "{{id:'{6}',name:'{6}',align:'middle'}});",
                string.Format(resourceUrl, "ViewerFx_Web.swf"),
                string.Format(resourceUrl, "Install.swf"),
                HttpUtility.UrlEncode(config),
                HttpUtility.UrlEncode(requestUrl),
                HttpUtility.UrlEncode(version),
                options.Theme.ToString().Replace("Office2007", string.Empty),
                this.ID,
                options.Server.CacheTimeout,
                options.Server.CacheMode,
                options.Server.CacheItemPriority,
                StiGuidUtils.NewGuid(),
                actions,
                StiReportHelper.GetHtmlColor(options.Appearance.BackgroundColor),
                options.Appearance.BackgroundColor == Color.Transparent ? "transparent" : options.Appearance.FlashWMode.ToString().ToLower());
            return script;
        }

        private Panel RenderFlashWrapper()
        {
            var text = new LiteralControl();
            text.Text = "<iframe id='printFrame' name='printFrame' width='0' height='0' frameborder='0' style='position:absolute;'></iframe>" +
                "<div id='flashContent'><p>To view this page ensure that Adobe Flash Player 11.1 or greater is installed.</p>" +
                "<a href=\"http://www.adobe.com/go/getflashplayer\"><img src='data:image/gif;base64," +
                "R0lGODlhngAnALMAAAcHB////35+fkNDQ84gJ9vV1aCgoMw+Q8MnLYiIiLy7u2ZmZtx6feWcniIiIgAAACH5BAkAAA8ALAAAAACeACcAAAT+kMhJq7046827" +
                "/2AojldgnmiqrmzrvnAsxyQ533iu73Qt8sCg0CUoGo0t32/IbPKO0KQS5KxaY9CjdDo5HDLXsBiVRbK4h0bB1AC3EnDFzSA3FeAJwxplgO8DfXkneAl/YWVF" +
                "WzUMKW0YLAYDCQoJCyyFKgMDJwoOcAsAAieaCQKhJgMLCZomAHiGV4iiZzUHsAGOJSqRLIYDsAYCDnsKmycOBgEDsyYOcgN1AK1jKbKKIre4bikOLJqeygAD" +
                "yaMFAgkmxXwLBdIolcpyq9PUJ9a0I3UquRa7lgGUMP2aVsDYiQLdEKYzCBAaw4bhACBrpelhLETXPjBq5EWDCjj+6RI4M+AJjjQD/wZB67RG3YlILl9ughag" +
                "oBwACnLWu7fCRgoGHT4yCyCtUk4Fa0CicFBxGcRRyQAYUhXPBEh3VmRp1RJgxMYTQIOmaPen6EOaBw22e1rQ2Ko686oivCmm1FaMJkaM/bDCgDhSqCqaEEYu" +
                "wDkU4xQAWCyJj4PFKQcsdtVqMjond+5m+SPiwE8vXza0uJWtHjVzmo0YEtGgFwLRpmPvUJBaQOG8IDy3eO1Rtm8cwe7exv2h9W7Yv5PHCC5rOHEPpU3w3qa8" +
                "eout+Drodo3cunehWS73/AALNGgOu/DIW4HpIJxkBW7rQRGw/fwUdAbxia8e4CsdmR3+0d542v20BGKqTEKUCp2I59c5m8RUlUql4DQhYgaNY8dMCcojiSnO" +
                "xYCaai6Ql0JoVKSAFj0oqNINKrdJuGIASvEyIyDCEPOihjPWaJEMtBWhT3YaGHcCP3ypOCRWxyizhwApPYXKkEqpc+Mvh8HoUo+XocRDHyGmsMEBDNyCYooY" +
                "arIGk4BY4uVglAH0lyYWDoJOQcnMqJBCdjjgTGBq0vjhQDxEh4IGpZ2J5iiTRKPiJH6h0FZDRxVDpWVTvrPSMCcsEFmjVkmiYT0ZbNdIDZksKemcEyGWE0Nc" +
                "KrlUU8wodSGNl3FKTakrIBlCqigwWYpMgKxBloxUipfphgdhYWVrrID8WAWvkoaFqqwnTOYKodMksNhEyL6jbETiZAmjVeJJxhiujO6KwXYFWOvDd/QGocF5" +
                "XBBQ77465OsBvwDP4K9YARec0cD9GKywCgh3t/DCDff28MMRV2zxxQhHAAA7' alt='Get Adobe Flash Player' border='0'/></a></div>";

            var mainPanel = new Panel();
            mainPanel.ID = this.ID + "_FxViewerMainPanel";
            mainPanel.Style.Add("display", "inline-block");
            mainPanel.Style.Add("width", this.Width.ToString());
            mainPanel.Style.Add("height", this.Height.ToString());
            mainPanel.Controls.Add(text);

            return mainPanel;
        }

        #endregion

        protected override void Render(HtmlTextWriter writer)
        {
            CreateChildControls();

            base.Render(writer);
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            this.Width = options.Width.IsEmpty ? Unit.Percentage(100) : options.Width;
            this.Height = options.Height.IsEmpty ? Unit.Pixel(650) : options.Height;
            this.BackColor = options.Appearance.BackgroundColor;

            StiJavaScript mainScript = RenderMainScript();
            this.Controls.Add(mainScript);

            StiJavaScript flashScript = RenderFlashScript();
            this.Controls.Add(flashScript);

            Panel flashWrapper = RenderFlashWrapper();
            this.Controls.Add(flashWrapper);
        }
    }
}
