﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Xml;
using System.Drawing;
using System.Web.UI.WebControls;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcViewerFx : Panel
    {
        private string RenderConfig()
        {
            // Create the Xml document
            XmlDocument doc = new XmlDocument();
            XmlDeclaration declaration = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(declaration);

            XmlElement category = null;
            XmlElement subcategory = null;
            XmlElement element = null;

            // Create the main node
            XmlElement config = doc.CreateElement("StiSerializer");
            config.SetAttribute("type", "Net");
            config.SetAttribute("version", "1.02");
            config.SetAttribute("application", "StiOptions");
            doc.AppendChild(config);

            #region Connection

            category = doc.CreateElement("Connection");
            config.AppendChild(category);

            element = doc.CreateElement("ClientRepeatCount");
            element.InnerText = options.Server.RepeatCount.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ClientRequestTimeout");
            element.InnerText = options.Server.RequestTimeout.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("EnableDataLogger");
            element.InnerText = options.Server.EnableDataLogger.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowCancelButton");
            element.InnerText = options.Appearance.ShowCancelButton.ToString();
            category.AppendChild(element);

            #endregion

            #region Appearance

            category = doc.CreateElement("Appearance");
            config.AppendChild(category);

            element = doc.CreateElement("ShowTooltips");
            element.InnerText = options.Appearance.ShowTooltips.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowTooltipsHelp");
            element.InnerText = options.Appearance.ShowTooltipsHelp.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowFormsHelp");
            element.InnerText = options.Appearance.ShowFormsHelp.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowFormsHints");
            element.InnerText = options.Appearance.ShowFormsHints.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("DateFormat");
            element.InnerText = options.Appearance.ParametersPanelDateFormat;
            category.AppendChild(element);

            #endregion

            #region AboutDialog

            category = doc.CreateElement("AboutDialog");
            config.AppendChild(category);

            element = doc.CreateElement("TextLine1");
            element.InnerText = options.Appearance.AboutDialogTextLine1;
            category.AppendChild(element);

            element = doc.CreateElement("TextLine2");
            element.InnerText = options.Appearance.AboutDialogTextLine2;
            category.AppendChild(element);

            element = doc.CreateElement("Url");
            element.InnerText = options.Appearance.AboutDialogUrl;
            category.AppendChild(element);

            element = doc.CreateElement("UrlText");
            element.InnerText = options.Appearance.AboutDialogUrlText;
            category.AppendChild(element);

            element = doc.CreateElement("FrameworkType");
            element.InnerText = "ASP.NET MVC";
            category.AppendChild(element);

            #endregion

            #region Localization

            subcategory = doc.CreateElement("Localizations");
            config.AppendChild(subcategory);

            element = doc.CreateElement("Localization");
            element.InnerText = string.IsNullOrEmpty(options.Localization) ? "Default" : options.Localization;
            subcategory.AppendChild(element);

            #endregion

            #region Print

            category = doc.CreateElement("Print");
            config.AppendChild(category);

            element = doc.CreateElement("ShowPrintDialog");
            element.InnerText = options.Print.ShowPrintDialog.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AutoPageOrientation");
            element.InnerText = options.Print.AutoPageOrientation.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AutoPageScale");
            element.InnerText = options.Print.AutoPageScale.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowDefaultPrint");
            element.InnerText = options.Print.AllowDefaultPrint.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowPrintToPdf");
            element.InnerText = options.Print.AllowPrintToPdf.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowPrintToHtml");
            element.InnerText = options.Print.AllowPrintToHtml.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("PrintAsBitmap");
            element.InnerText = options.Print.PrintAsBitmap.ToString();
            category.AppendChild(element);

            #endregion

            #region Theme

            element = doc.CreateElement("Theme");
            element.InnerText = options.Theme.ToString().Replace("Office2007", string.Empty);
            config.AppendChild(element);

            #endregion

            #region Viewer

            category = doc.CreateElement("Viewer");
            config.AppendChild(category);

            element = doc.CreateElement("DefaultEmail");
            element.InnerText = options.Email.DefaultEmailAddress.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("Title");
            element.InnerText = options.Appearance.BrowserTitle;
            category.AppendChild(element);

            #region Appearance

            subcategory = doc.CreateElement("Appearance");
            category.AppendChild(subcategory);

            element = doc.CreateElement("AutoHideScrollbars");
            element.InnerText = options.Appearance.AutoHideScrollbars.ToString();
            subcategory.AppendChild(element);

            Color color = options.Appearance.CurrentPageBorderColor;
            element = doc.CreateElement("CurrentPageBorderColor");
            element.InnerText = string.Format("{0}, {1}, {2}, {3}", color.A, color.R, color.G, color.B);
            subcategory.AppendChild(element);

            element = doc.CreateElement("OpenLinksTarget");
            element.InnerText = options.Appearance.OpenLinksWindow;
            subcategory.AppendChild(element);

            element = doc.CreateElement("VariablesPanelColumns");
            element.InnerText = options.Appearance.ParametersPanelColumnsCount.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("VariablesPanelEditorWidth");
            element.InnerText = options.Appearance.ParametersPanelEditorWidth.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowEmailDialog");
            element.InnerText = options.Email.ShowEmailDialog.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowEmailExportDialog");
            element.InnerText = options.Email.ShowExportDialog.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Toolbar

            subcategory = doc.CreateElement("Toolbar");
            category.AppendChild(subcategory);

            element = doc.CreateElement("ShowMainToolbar");
            element.InnerText = options.Toolbar.Visible.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNavigateToolbar");
            element.InnerText = options.Toolbar.ShowNavigatePanel.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowViewModeToolbar");
            element.InnerText = options.Toolbar.ShowViewModePanel.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Buttons

            element = doc.CreateElement("ShowAboutButton");
            element.InnerText = options.Toolbar.ShowAboutButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowBookmarksButton");
            element.InnerText = options.Toolbar.ShowBookmarksButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExitButton");
            element.InnerText = options.Toolbar.ShowExitButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowFindButton");
            element.InnerText = options.Toolbar.ShowFindButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowFirstPageButton");
            element.InnerText = options.Toolbar.ShowFirstPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowFullScreenButton");
            element.InnerText = options.Toolbar.ShowFullScreenButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowGoToPageButton");
            element.InnerText = options.Toolbar.ShowGoToPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowLastPageButton");
            element.InnerText = options.Toolbar.ShowLastPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNextPageButton");
            element.InnerText = options.Toolbar.ShowNextPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOpenButton");
            element.InnerText = options.Toolbar.ShowOpenButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeContinuousButton");
            element.InnerText = options.Toolbar.ShowContinuousPageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeMultipleButton");
            element.InnerText = options.Toolbar.ShowMultiplePageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeSingleButton");
            element.InnerText = options.Toolbar.ShowSinglePageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowParametersButton");
            element.InnerText = options.Toolbar.ShowParametersButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowParametersPanel");
            element.InnerText = options.Toolbar.ShowParametersButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviousPageButton");
            element.InnerText = options.Toolbar.ShowPreviousPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPrintButton");
            element.InnerText = options.Toolbar.ShowPrintButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSaveButton");
            element.InnerText = options.Toolbar.ShowSaveButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowThumbnailsButton");
            element.InnerText = options.Toolbar.ShowThumbnailsButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowDesignButton");
            element.InnerText = options.Toolbar.ShowDesignButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSendEMailButton");
            element.InnerText = options.Toolbar.ShowSendEmailButton.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Zoom

            element = doc.CreateElement("ShowZoom");
            element.InnerText = options.Toolbar.ShowZoomButtons.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("Zoom");
            element.InnerText = options.Toolbar.Zoom.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Exports

            subcategory = doc.CreateElement("Exports");
            category.AppendChild(subcategory);

            element = doc.CreateElement("OpenExportedReportTarget");
            element.InnerText = options.Appearance.OpenExportedReportWindow;
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportDialog");
            element.InnerText = options.Exports.ShowExportDialog.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDocument");
            element.InnerText = options.Exports.ShowExportToDocument.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPdf");
            element.InnerText = options.Exports.ShowExportToPdf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToXps");
            element.InnerText = options.Exports.ShowExportToXps.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPpt");
            element.InnerText = options.Exports.ShowExportToPowerPoint.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToHtml");
            element.InnerText = options.Exports.ShowExportToHtml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToHtml5");
            element.InnerText = options.Exports.ShowExportToHtml5.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToMht");
            element.InnerText = options.Exports.ShowExportToMht.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToText");
            element.InnerText = options.Exports.ShowExportToText.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToRtf");
            element.InnerText = options.Exports.ShowExportToRtf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToWord2007");
            element.InnerText = options.Exports.ShowExportToWord2007.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToOpenDocumentWriter");
            element.InnerText = options.Exports.ShowExportToOpenDocumentWriter.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcel");
            element.InnerText = options.Exports.ShowExportToExcel.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcelXml");
            element.InnerText = options.Exports.ShowExportToExcelXml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcel2007");
            element.InnerText = options.Exports.ShowExportToExcel2007.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToOpenDocumentCalc");
            element.InnerText = options.Exports.ShowExportToOpenDocumentCalc.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToCsv");
            element.InnerText = options.Exports.ShowExportToCsv.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDbf");
            element.InnerText = options.Exports.ShowExportToDbf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToXml");
            element.InnerText = options.Exports.ShowExportToXml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDif");
            element.InnerText = options.Exports.ShowExportToDif.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSylk");
            element.InnerText = options.Exports.ShowExportToSylk.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToBmp");
            element.InnerText = options.Exports.ShowExportToImageBmp.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToGif");
            element.InnerText = options.Exports.ShowExportToImageGif.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToJpeg");
            element.InnerText = options.Exports.ShowExportToImageJpeg.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPcx");
            element.InnerText = options.Exports.ShowExportToImagePcx.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPng");
            element.InnerText = options.Exports.ShowExportToImagePng.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToTiff");
            element.InnerText = options.Exports.ShowExportToImageTiff.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToMetafile");
            element.InnerText = options.Exports.ShowExportToImageMetafile.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSvg");
            element.InnerText = options.Exports.ShowExportToImageSvg.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSvgz");
            element.InnerText = options.Exports.ShowExportToImageSvgz.ToString();
            subcategory.AppendChild(element);

            #endregion

            #endregion

            return StiGZipHelper.Pack(doc.OuterXml);
        }
    }
}
