﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Web.Mvc;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Web;
using Stimulsoft.Report.Export;
using System.Xml;
using System;
using System.ComponentModel;
using System.Web;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcViewerFx : Panel
    {
        #region GetReportResult

        /// <summary>
        /// Get the action result required for show the specified report in the viewer.
        /// </summary>
        public static ActionResult GetReportResult(StiReport report)
        {
            return GetReportResult(null, report);
        }

        /// <summary>
        /// Get the action result required for show the specified report in the viewer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult GetReportResult(StiRequestParams requestParams, StiReport report)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            requestParams.Cache.Helper.SaveReportInternal(requestParams, report);
            StiWebActionResult result = (requestParams.Action == StiAction.Variables || requestParams.Action == StiAction.Sorting || requestParams.Action == StiAction.DrillDown)
                    ? StiReportHelperFx.GetInteractionResult(requestParams, report)
                    : StiReportHelperFx.GetReportSnapshotResult(requestParams, report);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        #endregion

        #region PrintReportResult

        /// <summary>
        /// Get the action result required for print the report to PDF format.
        /// </summary>
        public static ActionResult PrintReportResult()
        {
            return PrintReportResult(null, null, null);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified report will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiReport report)
        {
            return PrintReportResult(null, report, null);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiExportSettings settings)
        {
            return PrintReportResult(null, null, settings);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified report and export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiReport report, StiExportSettings settings)
        {
            return PrintReportResult(null, report, settings);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified request parameters will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams)
        {
            return PrintReportResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams, StiReport report)
        {
            return PrintReportResult(requestParams, report, null);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified request parameters and export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams, StiExportSettings settings)
        {
            return PrintReportResult(requestParams, null, settings);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified request parameters, report and export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams, StiReport report, StiExportSettings settings)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            if (settings == null) settings = GetExportSettings(requestParams);
            if (settings is StiPdfExportSettings) ((StiPdfExportSettings)settings).AutoPrintMode = StiPdfAutoPrintMode.Dialog;
            StiWebActionResult result = StiExportsHelper.ExportReportResult(requestParams, report, settings);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        #endregion

        #region ExportReportResult

        /// <summary>
        /// Get the action result required for export the report.
        /// </summary>
        public static ActionResult ExportReportResult()
        {
            return ExportReportResult(null, null, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified report will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiReport report)
        {
            return ExportReportResult(null, report, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiExportSettings settings)
        {
            return ExportReportResult(null, null, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified report and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiReport report, StiExportSettings settings)
        {
            return ExportReportResult(null, report, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams)
        {
            return ExportReportResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiReport report)
        {
            return ExportReportResult(requestParams, report, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiExportSettings settings)
        {
            return ExportReportResult(requestParams, null, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters, report and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiReport report, StiExportSettings settings)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            if (settings == null) settings = GetExportSettings(requestParams);
            StiWebActionResult result = StiExportsHelper.ExportReportResult(requestParams, report, settings);
            return new StiMvcActionResult(result.Data, result.ContentType, result.FileName, !string.IsNullOrEmpty(result.FileName));
        }

        #endregion

        #region EmailReportResult

        /// <summary>
        /// Get the action result required for send the report by Email with specified options.
        /// </summary>
        public static ActionResult EmailReportResult(StiEmailOptions options)
        {
            return EmailReportResult(null, null, options, null);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified request parameters will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiRequestParams requestParams, StiEmailOptions options)
        {
            return EmailReportResult(requestParams, null, options, null);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified export settings will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiEmailOptions options, StiExportSettings settings)
        {
            return EmailReportResult(null, null, options, settings);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified report will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiReport report, StiEmailOptions options)
        {
            return EmailReportResult(null, report, options, null);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified report and export settings will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiReport report, StiEmailOptions options, StiExportSettings settings)
        {
            return EmailReportResult(null, report, options, settings);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified request parameters, report and export settings will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiRequestParams requestParams, StiReport report, StiEmailOptions options, StiExportSettings settings)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            if (settings == null) settings = GetExportSettings(requestParams);
            StiWebActionResult result = StiExportsHelper.EmailReportResult(requestParams, report, settings, options);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        #endregion

        #region GetLocalizationResult

        /// <summary>
        /// Get the action result required for loading the locatization file.
        /// </summary>
        public static ActionResult GetLocalizationResult()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetLocalizationResult(requestParams);
        }

        /// <summary>
        /// Get the action result required for loading the locatization file with the specified file path.
        /// </summary>
        public static ActionResult GetLocalizationResult(string localizationPath)
        {
            StiRequestParams requestParams = GetRequestParams();
            requestParams.Localization = localizationPath;
            return GetLocalizationResult(requestParams);
        }

        /// <summary>
        /// Get the action result required for loading the specified locatization file.
        /// </summary>
        public static ActionResult GetLocalizationResult(XmlDocument localization)
        {
            StiRequestParams requestParams = GetRequestParams();
            StiWebActionResult result = StiWebActionResult.StringResult(requestParams, localization.OuterXml);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        /// <summary>
        /// Get the action result required for loading the locatization file. The specified request parameters will be used.
        /// </summary>
        public static ActionResult GetLocalizationResult(StiRequestParams requestParams)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            StiWebActionResult result = StiLocalizationHelperFx.GetLocalizationResult(requestParams);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        #endregion

        #region ViewerEventResult

        /// <summary>
        /// Get the action result required for the various requests of the viewer.
        /// </summary>
        public static ActionResult ViewerEventResult()
        {
            return ViewerEventResult(null, null);
        }

        /// <summary>
        /// Get the action result required for the various requests of the viewer. The specified report will be used.
        /// </summary>
        public static ActionResult ViewerEventResult(StiReport report)
        {
            return ViewerEventResult(null, report);
        }

        /// <summary>
        /// Get the action result required for the various requests of the viewer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult ViewerEventResult(StiRequestParams requestParams)
        {
            return ViewerEventResult(requestParams, null);
        }

        /// <summary>
        /// Get the action result required for the various requests of the viewer. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult ViewerEventResult(StiRequestParams requestParams, StiReport report)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (requestParams.Component == StiComponentType.ViewerFx)
            {
                StiWebActionResult result;
                switch (requestParams.Action)
                {
                    case StiAction.Resource:
                        result = StiViewerResourcesHelper.Get(requestParams);
                        return new StiMvcActionResult(result.Data, result.ContentType, result.FileName, false, true);

                    case StiAction.Variables:
                    case StiAction.Sorting:
                    case StiAction.DrillDown:
                        if (report == null) report = GetReportObject(requestParams);
                        result = StiReportHelperFx.GetInteractionResult(requestParams, report);
                        return new StiMvcActionResult(result.Data, result.ContentType, result.FileName, false);

                    case StiAction.PrintReport:
                        return PrintReportResult(requestParams, report);

                    case StiAction.ExportReport:
                        return ExportReportResult(requestParams, report);

                    case StiAction.GetLocalization:
                        return GetLocalizationResult(requestParams);
                }
            }

            return new ViewResult();
        }

        #endregion


        #region Obsolete

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportSnapshotResult(StiReport report)
        {
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportSnapshotResult(HttpRequestBase request, StiReport report)
        {
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the ExportReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult ExportReportResult(HttpRequestBase request)
        {
            return ExportReportResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the ExportReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult ExportReportResult(HttpRequestBase request, StiReport report)
        {
            return ExportReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the EmailReportResult(options) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult EmailReportResult(HttpRequestBase request, StiEmailOptions options)
        {
            return EmailReportResult(options);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the EmailReportResult(report, options) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult EmailReportResult(HttpRequestBase request, StiReport report, StiEmailOptions options)
        {
            return EmailReportResult(report, options);
        }

        #endregion
    }
}
