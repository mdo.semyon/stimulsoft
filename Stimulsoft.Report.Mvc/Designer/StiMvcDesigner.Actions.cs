﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using Stimulsoft.Report.Web;
using Stimulsoft.Report.Export;
using System.Web;
using System.ComponentModel;
using System.Data;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcDesigner : Panel
    {
        #region GetReportResult
        /// <summary>
        /// Get the action result required for edit a new report template in the designer or edit a report template opened using the file menu.
        /// </summary>
        public static ActionResult GetReportResult()
        {
            StiRequestParams requestParams = GetRequestParams();
            return ProcessRequestResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for edit a new report template in the designer or edit a report template opened using the file menu.
        /// The specified request parameters will be used.
        /// </summary>
        public static ActionResult GetReportResult(StiRequestParams requestParams)
        {
            return ProcessRequestResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for edit the specified report template in the designer.
        /// </summary>
        public static ActionResult GetReportResult(StiReport report)
        {
            StiRequestParams requestParams = GetRequestParams();
            return ProcessRequestResult(requestParams, report, null);
        }

        /// <summary>
        /// Get the action result required for edit the specified report template in the designer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult GetReportResult(StiRequestParams requestParams, StiReport report)
        {
            return ProcessRequestResult(requestParams, report, null);
        }
        #endregion

        #region SaveReportResult
        /// <summary>
        /// Get the action result required for saving the report template.
        /// </summary>
        public static ActionResult SaveReportResult()
        {
            return ProcessRequestResult(null, null, null);
        }

        /// <summary>
        /// Get the action result required for saving the report template. After saving it is possible to display a dialog box with specified message.
        /// </summary>
        public static ActionResult SaveReportResult(string errorMessage)
        {
            return ProcessRequestResult(null, null, errorMessage);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The report will be sent back to the designer.
        /// </summary>
        public static ActionResult SaveReportResult(StiReport report)
        {
            return ProcessRequestResult(null, report, null);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The specified request parameters will be used.
        /// </summary>
        public static ActionResult SaveReportResult(StiRequestParams requestParams)
        {
            return ProcessRequestResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The specified request parameters will be used.
        /// After saving it is possible to display a dialog box with specified message.
        /// </summary>
        public static ActionResult SaveReportResult(StiRequestParams requestParams, string errorMessage)
        {
            return ProcessRequestResult(requestParams, null, errorMessage);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The specified request parameters will be used. The report will be sent back to the designer.
        /// </summary>
        public static ActionResult SaveReportResult(StiRequestParams requestParams, StiReport report)
        {
            return ProcessRequestResult(requestParams, report, null);
        }
        #endregion

        #region PreviewReportResult
        /// <summary>
        /// Get the action result required for preview the report in the designer.
        /// </summary>
        public static ActionResult PreviewReportResult()
        {
            return PreviewReportResult(null, null);
        }

        /// <summary>
        /// Get the action result required for preview the report in the designer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult PreviewReportResult(StiRequestParams requestParams)
        {
            return PreviewReportResult(requestParams, null);
        }

        /// <summary>
        /// Get the action result required for preview the report in the designer. The specified report template or snapshot will be used.
        /// </summary>
        public static ActionResult PreviewReportResult(StiReport report)
        {
            return PreviewReportResult(null, report);
        }

        /// <summary>
        /// Get the action result required for preview the report in the designer. The specified request parameters and report template or snapshot will be used.
        /// </summary>
        public static ActionResult PreviewReportResult(StiRequestParams requestParams, StiReport report)
        {
            // The 'report' object should be taken using the GetActionReportObject() method
            return ProcessRequestResult(requestParams, report, null);
        }
        #endregion

        #region ExportReportResult
        /// <summary>
        /// Get the action result required for export the report.
        /// </summary>
        public static ActionResult ExportReportResult()
        {
            return ExportReportResult(null, null, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified report will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiReport report)
        {
            return ExportReportResult(null, report, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiExportSettings settings)
        {
            return ExportReportResult(null, null, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified report and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiReport report, StiExportSettings settings)
        {
            return ExportReportResult(null, report, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams)
        {
            return ExportReportResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiReport report)
        {
            return ExportReportResult(requestParams, report, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiExportSettings settings)
        {
            return ExportReportResult(requestParams, null, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters, report and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiReport report, StiExportSettings settings)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            if (settings == null) settings = GetExportSettings(requestParams);
            return StiMvcViewer.ExportReportResult(requestParams, report, settings);
        }
        #endregion

        #region DesignerEventResult
        /// <summary>
        /// Get the action result required for the various requests of the designer.
        /// </summary>
        public static ActionResult DesignerEventResult()
        {
            return ProcessRequestResult(null, null, null);
        }

        /// <summary>
        /// Get the action result required for the various requests of the designer.The specified report will be used.
        /// </summary>
        public static ActionResult DesignerEventResult(StiReport report)
        {
            return ProcessRequestResult(null, report, null);
        }

        /// <summary>
        /// Get the action result required for the various requests of the designer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult DesignerEventResult(StiRequestParams requestParams)
        {
            return ProcessRequestResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for the various requests of the designer. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult DesignerEventResult(StiRequestParams requestParams, StiReport report)
        {
            return ProcessRequestResult(requestParams, report, null);
        }
        #endregion

        #region ProcessRequestResult
        private static ActionResult ProcessRequestResult(StiRequestParams requestParams, StiReport report, string errorMessage)
        {
            if (requestParams == null) requestParams = GetRequestParams();

            // Process preview tab requests
            if (requestParams.Component == StiComponentType.Viewer) return StiMvcViewer.ViewerEventResult(requestParams, report);

            // Process designer requests
            if (requestParams.Component == StiComponentType.Designer)
            {
                StiWebActionResult result = null;
                if (report == null && requestParams.Action != StiAction.OpenReport && requestParams.Action != StiAction.Resource)
                    report = GetActionReportObject(requestParams);

                switch (requestParams.Action)
                {
                    case StiAction.Resource:
                        result = StiDesignerResourcesHelper.Get(requestParams);
                        break;

                    case StiAction.GetReport:
                        report = StiWebDesigner.GetReportForDesigner(requestParams, report);
                        break;

                    case StiAction.OpenReport:
                        if (report == null)
                        {
                            try
                            {
                                // The try/catch block is needed for opening incorrect report file or wrong report password
                                report = StiWebDesigner.LoadReportFromContent(requestParams);
                            }
                            catch (Exception e)
                            {
                                errorMessage = e.Message;
                            }
                        }
                        break;

                    case StiAction.PreviewReport:
                        // Set report for LoadReportToViewer command
                        requestParams.Report = report;
                        break;

                    case StiAction.Exit:
                        result = new StiWebActionResult();
                        break;
                }

                if (!string.IsNullOrEmpty(errorMessage)) result = StiWebActionResult.ErrorResult(requestParams, errorMessage);
                if (result == null) result = StiWebDesigner.CommandResult(requestParams, report);
                bool isResourceAction = requestParams.Action == StiAction.Resource;
                return new StiMvcActionResult(result.Data, result.ContentType, result.FileName, !string.IsNullOrEmpty(result.FileName) && !isResourceAction, isResourceAction);
            }

            return new ViewResult();
        }
        #endregion


        #region Obsolete

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the DesignerEventResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult DesignerEventResult(HttpContextBase httpContext)
        {
            return DesignerEventResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the DesignerEventResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult DesignerEventResult(HttpContextBase httpContext, StiReport report)
        {
            return DesignerEventResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the DesignerEventResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult DesignerEventResult(HttpContextBase httpContext, StiReport report, StiCacheHelper cacheHelper)
        {
            StiMvcDesigner.CacheHelper = cacheHelper;
            return DesignerEventResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportTemplateResult(HttpContextBase httpContext)
        {
            return GetReportResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportTemplateResult(HttpContextBase httpContext, StiReport report)
        {
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportTemplateResult(HttpContextBase httpContext, StiReport report, StiCacheHelper cacheHelper)
        {
            StiMvcDesigner.CacheHelper = cacheHelper;
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the PreviewReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportSnapshotResult(HttpContextBase httpContext)
        {
            return PreviewReportResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the PreviewReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportSnapshotResult(HttpContextBase httpContext, StiReport report)
        {
            return PreviewReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the PreviewReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportSnapshotResult(HttpContextBase httpContext, StiReport report, StiCacheHelper cacheHelper)
        {
            StiMvcDesigner.CacheHelper = cacheHelper;
            return PreviewReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetNewReportDataResult(HttpContextBase httpContext)
        {
            return GetReportResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetNewReportDataResult(HttpContextBase httpContext, DataSet data)
        {
            StiReport report = GetActionReportObject();
            report.RegData(data);
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetNewReportDataResult(HttpContextBase httpContext, StiReport report)
        {
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetNewReportDataResult(HttpContextBase httpContext, StiReport report, StiCacheHelper cacheHelper)
        {
            StiMvcDesigner.CacheHelper = cacheHelper;
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetNewReportDataResultHelper(HttpContextBase httpContext, StiReport report, DataSet data)
        {
            if (report == null) report = GetActionReportObject();
            report.RegData(data);
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetNewReportDataResultHelper(HttpContextBase httpContext, StiReport report, DataSet data, StiCacheHelper cacheHelper)
        {
            StiMvcDesigner.CacheHelper = cacheHelper;
            if (report == null) report = GetActionReportObject();
            report.RegData(data);
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult OpenReportTemplateResult(HttpContextBase httpContext)
        {
            return GetReportResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult OpenReportTemplateResult(HttpContextBase httpContext, DataSet data)
        {
            StiReport report = GetActionReportObject();
            if (data != null) report.RegData(data);
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult OpenReportTemplateResult(HttpContextBase httpContext, DataSet data, StiCacheHelper cacheHelper)
        {
            StiMvcDesigner.CacheHelper = cacheHelper;
            StiReport report = GetActionReportObject();
            if (data != null) report.RegData(data);
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the SaveReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult SaveReportTemplateResult(HttpContextBase httpContext)
        {
            return SaveReportResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the SaveReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult SaveAsReportTemplateResult(HttpContextBase httpContext)
        {
            return SaveReportResult();
        }

        #endregion
    }
}
