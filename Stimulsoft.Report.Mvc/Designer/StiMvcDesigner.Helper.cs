﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Web.UI.WebControls;
using Stimulsoft.Report.Web;
using System.Collections.Specialized;
using System.Web.Routing;
using Stimulsoft.Report.Export;
using System.Web;
using System.Collections;
using System.ComponentModel;
using System;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcDesigner : Panel
    {
        #region Request Params

        /// <summary>
        /// Get the all request parameters of the report designer.
        /// </summary>
        public static StiRequestParams GetRequestParams()
        {
            StiRequestParams requestParams = StiRequestParamsHelper.Get();
            requestParams.Cache.Helper = CacheHelper;
            return requestParams;
        }

        #endregion

        #region Form and Route Values

        /// <summary>
        /// Get the POST form values for the start page of the report designer.
        /// </summary>
        public static NameValueCollection GetFormValues()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetFormValues(requestParams);
        }

        /// <summary>
        /// Get the POST form values for the start page of the report designer. The specified request parameters will be used.
        /// </summary>
        public static NameValueCollection GetFormValues(StiRequestParams requestParams)
        {
            return requestParams.FormValues;
        }

        /// <summary>
        /// Get the route values for the start page of the report designer.
        /// </summary>
        public static RouteValueDictionary GetRouteValues()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetRouteValues(requestParams);
        }

        /// <summary>
        /// Get the route values for the start page of the report designer. The specified request parameters will be used.
        /// </summary>
        public static RouteValueDictionary GetRouteValues(StiRequestParams requestParams)
        {
            var routeValues = new RouteValueDictionary();
            if (requestParams.Routes != null)
            {
                foreach (string key in requestParams.Routes.Keys)
                {
                    routeValues.Add(key, requestParams.Routes[key]);
                }
            }

            return routeValues;
        }

        #endregion

        #region Report

        /// <summary>
        /// Get the current report template for the current action of the component.
        /// Returns report template from the client for OpenReport action, new report template for CreateReport action, copy of the report template for PreviewReport action.
        /// Otherwise returns report template from the cache.
        /// </summary>
        public static StiReport GetActionReportObject()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetActionReportObject(requestParams);
        }

        /// <summary>
        /// Get the current report template for the current action of the component.
        /// Returns report template from the client for OpenReport action, new report template for CreateReport action, copy of the report template for PreviewReport action.
        /// Otherwise returns report template from the cache. The specified request parameters will be used.
        /// </summary>
        public static StiReport GetActionReportObject(StiRequestParams requestParams)
        {
            if (requestParams.Action == StiAction.OpenReport)
            {
                try
                {
                    // The try/catch block is needed for opening incorrect report file or wrong report password
                    return StiWebDesigner.LoadReportFromContent(requestParams);
                }
                catch
                {
                    return null;
                }
            }
            StiReport currentReport = GetReportObject(requestParams);
            if (requestParams.Action == StiAction.CreateReport)
            {
                StiReport newReport = StiWebDesigner.GetNewReport(requestParams);
                if (currentReport != null) StiReportEdit.CopyReportDictionary(currentReport, newReport);
                return newReport;
            }
            if (requestParams.Action == StiAction.PreviewReport) return StiReportEdit.CloneReport(currentReport, true);
            return currentReport;
        }

        /// <summary>
        /// Get the current report template from the cache.
        /// </summary>
        public static StiReport GetReportObject()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetReportObject(requestParams);
        }

        /// <summary>
        /// Get the current report template from the cache. The specified request parameters will be used.
        /// </summary>
        public static StiReport GetReportObject(StiRequestParams requestParams)
        {
            return requestParams.Cache.Helper.GetReportInternal(requestParams);
        }

        #endregion

        #region Export Settings

        /// <summary>
        /// Get the export settings from the dialog form of the report preview.
        /// </summary>
        public static StiExportSettings GetExportSettings()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetExportSettings(requestParams);
        }

        /// <summary>
        /// Get the export settings from the dialog form of the report preview. The specified request parameters will be used.
        /// </summary>
        public static StiExportSettings GetExportSettings(StiRequestParams requestParams)
        {
            return StiExportsHelper.GetExportSettings(requestParams);
        }

        #endregion


        #region Obsolete
        
        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the RequestParams.All property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static Hashtable GetHttpContextParameters(HttpContextBase httpContext)
        {
            StiRequestParams requestParams = GetRequestParams();
            return requestParams.All;
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the RequestParams.Designer.Command property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static string GetActionCommandName(HttpContextBase httpContext)
        {
            StiRequestParams requestParams = GetRequestParams();
            return requestParams.Designer.Command.ToString();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetRouteValues() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static RouteValueDictionary GetRouteValues(HttpContextBase httpContext)
        {
            return GetRouteValues();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportObject() or GetActionReportObject() methods instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static StiReport GetReportObject(HttpContextBase httpContext)
        {
            return GetReportObject();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportObject() or GetActionReportObject() methods instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static StiReport GetReportObject(HttpContextBase httpContext, Hashtable parameters)
        {
            return GetReportObject();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportObject() or GetActionReportObject() methods instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static StiReport GetReportObject(HttpContextBase httpContext, Hashtable parameters, StiCacheHelper cacheHelper)
        {
            return GetReportObject();
        }

        #endregion
    }
}
