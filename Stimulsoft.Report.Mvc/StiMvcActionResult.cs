﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.IO;
using System.Text;
using System.Web.Mvc;
using System.Web;

namespace Stimulsoft.Report.Mvc
{
    public class StiMvcActionResult : ActionResult
    {
        public byte[] Data { get; private set; }

        public string ContentType { get; private set; }

        public bool EnableBrowserCache { get; private set; }

        public string FileName { get; private set; }

        public bool SaveFileDialog { get; private set; }
        
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            if (Data != null && Data.Length > 0)
            {
                HttpResponseBase response = context.HttpContext.Response;
                
                if (!string.IsNullOrEmpty(FileName))
                {
                    string type = SaveFileDialog ? "attachment" : "inline";
                    string value = string.Format("{0}; filename=\"{1}\"; filename*=UTF-8''{2}", type, FileName, HttpUtility.UrlPathEncode(FileName));
                    response.AppendHeader("Content-Disposition", value);
                }

                if (EnableBrowserCache)
                {
                    response.Cache.SetExpires(DateTime.Now.AddYears(1));
                    response.Cache.SetCacheability(HttpCacheability.Public);
                }
                else
                {
                    response.Cache.SetExpires(DateTime.MinValue);
                    response.Cache.SetCacheability(HttpCacheability.NoCache);
                }

                response.ContentType = ContentType;
                response.ContentEncoding = Encoding.UTF8;
                response.AddHeader("Content-Length", Data.Length.ToString());
                response.BinaryWrite(Data);
            }
        }

        public StiMvcActionResult() :
            this(string.Empty, "text/plain", null, false)
        {
        }

        public StiMvcActionResult(string data) :
            this(data, "text/plain", null, false)
        {
        }

        public StiMvcActionResult(string data, string contentType) :
            this(data, contentType, null, false)
        {
        }

        public StiMvcActionResult(string data, string contentType, string fileName, bool saveFileDialog) :
            this(Encoding.UTF8.GetBytes(data), contentType, fileName, saveFileDialog, false)
        {
        }

        public StiMvcActionResult(Stream data, string contentType) :
            this(data, contentType, null, false)
        {
        }

        public StiMvcActionResult(Stream data, string contentType, string fileName, bool saveFileDialog) :
            this(((MemoryStream)data).ToArray(), contentType, fileName, saveFileDialog, false)
        {
        }

        public StiMvcActionResult(byte[] data, string contentType) :
            this(data, contentType, null, false, false)
        {
        }

        public StiMvcActionResult(byte[] data, string contentType, string fileName, bool saveFileDialog) :
            this(data, contentType, fileName, saveFileDialog, false)
        {
        }

        public StiMvcActionResult(byte[] data, string contentType, string fileName, bool saveFileDialog, bool enableBrowserCache)
        {
            this.Data = data;
            this.ContentType = contentType;
            this.FileName = fileName;
            this.SaveFileDialog = saveFileDialog;
            this.EnableBrowserCache = enableBrowserCache;
        }
    }
}
