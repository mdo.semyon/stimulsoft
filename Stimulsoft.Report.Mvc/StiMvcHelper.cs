﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Web.Mvc;
using System.Web.UI;
using System.IO;

namespace Stimulsoft.Report.Mvc
{
    public static class StiMvcBaseHelper
    {
        private static StiMvcHelper mvcHelper = new StiMvcHelper();
        public static StiMvcHelper Stimulsoft(this HtmlHelper htmlHelper)
        {
            mvcHelper.htmlHelper = htmlHelper;
            return mvcHelper;
        }
    }

    public class StiMvcHelper
    {
        #region Fields

        internal HtmlHelper htmlHelper;

        #endregion
        
        #region Controls

        #region MvcViewer

        public MvcHtmlString StiMvcViewer(StiMvcViewerOptions options)
        {
            return StiMvcViewer("MvcViewer", options);
        }

        public MvcHtmlString StiMvcViewer(string ID, StiMvcViewerOptions options)
        {
            if (options == null)
                throw new Exception("Failed to initialize the StiMvcViewer component. Please define the StiMvcViewerOptions to work correctly with the viewer.");
            
            if (string.IsNullOrEmpty(options.Actions.ViewerEvent))
                throw new Exception("Failed to initialize the StiMvcViewer component. Please define the ViewerEvent action to work correctly with the viewer.");

            var viewer = new StiMvcViewer(htmlHelper, ID, options);
            var writer = new StringWriter();
            var htmlWriter = new HtmlTextWriter(writer, string.Empty);
            htmlWriter.NewLine = string.Empty;
            viewer.RenderControl(htmlWriter);
            htmlWriter.Dispose();

            return MvcHtmlString.Create(writer.ToString());
        }

        #endregion

        #region MvcViewerFx

        public MvcHtmlString StiMvcViewerFx(StiMvcViewerFxOptions options)
        {
            return StiMvcViewerFx("MvcViewerFx", options);
        }

        public MvcHtmlString StiMvcViewerFx(string ID, StiMvcViewerFxOptions options)
        {
            if (options == null)
                throw new Exception("Failed to initialize the StiMvcViewerFx component. Please define the StiMvcViewerFxOptions to work correctly with the viewer.");

            if (string.IsNullOrEmpty(options.Actions.ViewerEvent))
                throw new Exception("Failed to initialize the StiMvcViewerFx component. Please define the ViewerEvent action to work correctly with the viewer.");

            var viewer = new StiMvcViewerFx(htmlHelper, ID, options);
            var writer = new StringWriter();
            var htmlWriter = new HtmlTextWriter(writer, string.Empty);
            htmlWriter.NewLine = string.Empty;
            viewer.RenderControl(htmlWriter);
            htmlWriter.Dispose();

            return MvcHtmlString.Create(writer.ToString());
        }

        #endregion

        #region MvcDesigner

        public MvcHtmlString StiMvcDesigner(StiMvcDesignerOptions options)
        {
            return StiMvcDesigner("MvcDesigner", options);
        }

        public MvcHtmlString StiMvcDesigner(string ID, StiMvcDesignerOptions options)
        {
            if (options == null)
                throw new Exception("Failed to initialize the StiMvcDesigner component. Please define the StiMvcDesignerOptions to work correctly with the designer.");

            if (string.IsNullOrEmpty(options.Actions.DesignerEvent))
                throw new Exception("Failed to initialize the StiMvcDesigner component. Please define the DesignerEvent action to work correctly with the designer.");

            var designer = new StiMvcDesigner(htmlHelper, ID, options);
            var writer = new StringWriter();
            var htmlWriter = new HtmlTextWriter(writer, string.Empty);
            htmlWriter.NewLine = string.Empty;
            designer.RenderControl(htmlWriter);
            htmlWriter.Dispose();

            return MvcHtmlString.Create(writer.ToString());
        }

        #endregion

        #region MvcDesignerFx

        public MvcHtmlString StiMvcDesignerFx(StiMvcDesignerFxOptions options)
        {
            return StiMvcDesignerFx("MvcDesignerFx", options);
        }

        public MvcHtmlString StiMvcDesignerFx(string ID, StiMvcDesignerFxOptions options)
        {
            if (options == null)
                throw new Exception("Failed to initialize the StiMvcDesignerFx component. Please define the StiMvcDesignerFxOptions to work correctly with the designer.");

            if (string.IsNullOrEmpty(options.Actions.DesignerEvent))
                throw new Exception("Failed to initialize the StiMvcDesignerFx component. Please define the DesignerEvent action to work correctly with the designer.");

            var designer = new StiMvcDesignerFx(htmlHelper, ID, options);
            var writer = new StringWriter();
            var htmlWriter = new HtmlTextWriter(writer, string.Empty);
            htmlWriter.NewLine = string.Empty;
            designer.RenderControl(htmlWriter);
            htmlWriter.Dispose();

            return MvcHtmlString.Create(writer.ToString());
        }

        #endregion

        #endregion
    }
}
