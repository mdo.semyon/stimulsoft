﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Globalization;
using Stimulsoft.Base.Json;
using Stimulsoft.Report.Web;
using System.Web.Caching;

namespace Stimulsoft.Report.Mvc
{
    public class StiMvcDesignerFxOptions
    {
        #region Actions

        public class ActionOptions
        {
            /// <summary>
            /// Gets or sets the action method name of loading the report template.
            /// </summary>
            public string GetReport { get; set; }

            /// <summary>
            /// Gets or sets the action method name of creating the new report template using the file menu.
            /// </summary>
            public string CreateReport { get; set; }

            /// <summary>
            /// Gets or sets the action method name of saving the report template.
            /// </summary>
            public string SaveReport { get; set; }

            /// <summary>
            /// Gets or sets the action method name of saving as file the report template.
            /// </summary>
            public string SaveReportAs { get; set; }

            /// <summary>
            /// Gets or sets the action method name of preparing the rendered report for preview.
            /// </summary>
            public string PreviewReport { get; set; }

            /// <summary>
            /// Gets or sets the action method name of loading a localization file for a designer.
            /// </summary>
            public string GetLocalization { get; set; }

            /// <summary>
            /// Gets or sets the action method name of exporting a report to the required format.
            /// </summary>
            public string ExportReport { get; set; }

            /// <summary>
            /// Gets or sets the action method name of exporting a report to the required format and send it by email.
            /// </summary>
            public string EmailReport { get; set; }
            
            /// <summary>
            /// Gets or sets the name of the action method for redirect to the desired view when exiting the designer.
            /// </summary>
            public string Exit { get; set; }

            /// <summary>
            /// Gets or sets the action method name of the report designer default events.
            /// </summary>
            public string DesignerEvent { get; set; }

            #region Obsolete

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the GetReport option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public string GetReportTemplate
            {
                get
                {
                    return GetReport;
                }
                set
                {
                    GetReport = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the CreateReport option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public string CreateReportTemplate
            {
                get
                {
                    return CreateReport;
                }
                set
                {
                    CreateReport = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the SaveReport option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public string SaveReportTemplate
            {
                get
                {
                    return SaveReport;
                }
                set
                {
                    SaveReport = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the SaveReportAs option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public string SaveReportTemplateAs
            {
                get
                {
                    return SaveReportAs;
                }
                set
                {
                    SaveReportAs = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the PreviewReport option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public string GetReportSnapshot
            {
                get
                {
                    return PreviewReport;
                }
                set
                {
                    PreviewReport = value;
                }
            }

            #endregion

            public ActionOptions()
            {
                GetReport = string.Empty;
                CreateReport = string.Empty;
                SaveReport = string.Empty;
                SaveReportAs = string.Empty;
                PreviewReport = string.Empty;
                GetLocalization = string.Empty;
                ExportReport = string.Empty;
                EmailReport = string.Empty;
                Exit = string.Empty;
                DesignerEvent = string.Empty;
            }
        }

        #endregion

        #region Appearance

        public class AppearanceOptions
        {
            /// <summary>
            /// Gets or sets the text to display in the About dialog of the designer.
            /// </summary>
            public string AboutDialogTextLine1 { get; set; }

            /// <summary>
            /// Gets or sets the text to display in the About dialog of the designer.
            /// </summary>
            public string AboutDialogTextLine2 { get; set; }

            /// <summary>
            /// Gets or sets the URL to display in the About dialog of the designer.
            /// </summary>
            public string AboutDialogUrl { get; set; }

            /// <summary>
            /// Gets or sets the URL text to display in the About dialog of the designer.
            /// </summary>
            public string AboutDialogUrlText { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the Cancel button in the window of getting the data from the designer.
            /// </summary>
            public bool ShowCancelButton { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the More Details button in the error dialog.
            /// </summary>
            public bool ShowMoreDetailsButton { get; set; }

            /// <summary>
            /// Gets or sets the Window Mode property of the SWF file for transparency, layering, positioning, and rendering in the browser. 
            /// </summary>
            public StiWMode FlashWMode { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that show or hide tooltips.
            /// </summary>
            public bool ShowTooltips { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that show or hide the help link in tooltips.
            /// </summary>
            public bool ShowTooltipsHelp { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that show or hide the help button in dialogs.
            /// </summary>
            public bool ShowDialogsHelp { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that show or hide help tips in dialogs.
            /// </summary>
            public bool ShowDialogsHints { get; set; }

            /// <summary>
            /// Gets or sets the report page grid size of the designer in centimetres.
            /// </summary>
            public double GridSizeInCentimetres { get; set; }

            /// <summary>
            /// Gets or sets the report page grid size of the designer in hundredths of inches.
            /// </summary>
            public double GridSizeInHundredthsOfInches { get; set; }

            /// <summary>
            /// Gets or sets the report page grid size of the designer in inches.
            /// </summary>
            public double GridSizeInInches { get; set; }

            /// <summary>
            /// Gets or sets the report page grid size of the designer in millimeters.
            /// </summary>
            public double GridSizeInMillimeters { get; set; }

            private string dateFormat = StiDateFormatMode.FromClient;
            /// <summary>
            /// Gets or sets a date format for datetime values in designer. To use a server date format please set the StiDateFormatMode.FromServer or "FromServer" string value.
            /// The default is the client system date format.
            /// </summary>
            public string DateFormat
            {
                get
                {
                    if (dateFormat == StiDateFormatMode.FromServer) return CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern + " " + CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern;
                    return dateFormat;
                }
                set
                {
                    dateFormat = value;
                }
            }

            /// <summary>
            /// Gets or sets the text to display in the browser title with report name.
            /// </summary>
            public string BrowserTitle { get; set; }

            /// <summary>
            /// Gets or sets the value that allows automatically hide report scrollbars in preview if not required.
            /// </summary>
            public bool AutoHideScrollbars { get; set; }

            /// <summary>
            /// Gets or sets a color of the border of the current report page in preview.
            /// </summary>
            public Color CurrentPageBorderColor { get; set; }

            /// <summary>
            /// Gets or sets a count columns in parameters panel.
            /// </summary>
            public int ParametersPanelColumnsCount { get; set; }

            /// <summary>
            /// Gets or sets a editor width in parameters panel.
            /// </summary>
            public int ParametersPanelEditorWidth { get; set; }

            /// <summary>
            /// Gets or sets a browser window to open links from the report.
            /// </summary>
            public string OpenLinksWindow { get; set; }

            /// <summary>
            /// Gets or sets a browser window to open the exported report.
            /// </summary>
            public string OpenExportedReportWindow { get; set; }

            /// <summary>
            /// Gets or sets an images quality for the RichText and Charts components.
            /// </summary>
            public StiImagesQuality ImagesQuality
            {
                get
                {
                    return StiReportHelperFx.ImagesQuality;
                }
                set
                {
                    StiReportHelperFx.ImagesQuality = value;
                }
            }

            #region Obsolete

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the OpenLinksWindow option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public string OpenLinksTarget
            {
                get
                {
                    return OpenLinksWindow;
                }
                set
                {
                    OpenLinksWindow = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the OpenExportedReportWindow option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public string OpenExportedReportTarget
            {
                get
                {
                    return OpenExportedReportWindow;
                }
                set
                {
                    OpenExportedReportWindow = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the ShowDialogsHelp option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public bool ShowFormsHelp
            {
                get
                {
                    return ShowDialogsHelp;
                }
                set
                {
                    ShowDialogsHelp = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the ShowDialogsHints option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public bool ShowFormsHints
            {
                get
                {
                    return ShowDialogsHints;
                }
                set
                {
                    ShowDialogsHints = value;
                }
            }

            #endregion

            public AppearanceOptions()
            {
                AboutDialogTextLine1 = string.Empty;
                AboutDialogTextLine2 = string.Empty;
                AboutDialogUrl = string.Empty;
                AboutDialogUrlText = string.Empty;
                ShowCancelButton = false;
                ShowMoreDetailsButton = true;
                FlashWMode = StiWMode.Window;
                ShowTooltips = true;
                ShowTooltipsHelp = true;
                ShowDialogsHelp = true;
                ShowDialogsHints = true;
                GridSizeInCentimetres = 0.2;
                GridSizeInHundredthsOfInches = 10;
                GridSizeInInches = 0.1;
                GridSizeInMillimeters = 2;
                BrowserTitle = string.Empty;
                AutoHideScrollbars = false;
                CurrentPageBorderColor = Color.Gold;
                ParametersPanelColumnsCount = 2;
                ParametersPanelEditorWidth = 200;
                OpenLinksWindow = StiTargetWindow.Blank;
                OpenExportedReportWindow = StiTargetWindow.Blank;
            }
        }

        #endregion

        #region Behavior

        public class BehaviorOptions
        {
            /// <summary>
            /// Gets or sets the value that displays or hides the report name dialog at saving.
            /// </summary>
            public bool ShowSaveDialog { get; set; }

            /// <summary>
            /// Gets or sets the value that marks a report as modified when it is opened.
            /// </summary>
            public bool SetReportModifiedWhenOpening { get; set; }

            /// <summary>
            /// Gets or sets an auto save interval of the report in minutes. The default value is 0 (auto save feature is disabled).
            /// </summary>
            public int AutoSaveInterval { get; set; }

            /// <summary>
            /// Gets or sets the Save report template mode - Hidden (AJAX mode), Visible or New Window.
            /// </summary>
            public StiSaveMode SaveReportMode { get; set; }

            /// <summary>
            /// Gets or sets the SaveAs report template mode - Hidden (AJAX mode), Visible or New Window.
            /// </summary>
            public StiSaveMode SaveReportAsMode { get; set; }

            /// <summary>
            /// Gets or sets the value that displays the new report wizard at designer startup.
            /// </summary>
            public bool RunWizardAfterLoad { get; set; }

            /// <summary>
            /// Gets or sets the name of the Javascript function that will be called by the designer when performing actions.
            /// </summary>
            public string DesignerEventFunction { get; set; }

            /// <summary>
            /// Gets or sets a URL for redirecting when exiting the designer.
            /// </summary>
            public string ExitUrl { get; set; }

            #region Obsolete

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the SaveReportMode option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public StiSaveMode SaveReportTemplateMode
            {
                get
                {
                    return SaveReportMode;
                }
                set
                {
                    SaveReportMode = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the SaveReportAsMode option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public StiSaveMode SaveReportTemplateAsMode
            {
                get
                {
                    return SaveReportAsMode;
                }
                set
                {
                    SaveReportAsMode = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the ShowSaveDialog option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public bool ShowSaveFileDialog
            {
                get
                {
                    return ShowSaveDialog;
                }
                set
                {
                    ShowSaveDialog = value;
                }
            }

            #endregion

            public BehaviorOptions()
            {
                ShowSaveDialog = false;
                SetReportModifiedWhenOpening = false;
                AutoSaveInterval = 0;
                SaveReportMode = StiSaveMode.Hidden;
                SaveReportAsMode = StiSaveMode.Hidden;
                RunWizardAfterLoad = false;
                DesignerEventFunction = string.Empty;
                ExitUrl = string.Empty;
            }
        }

        #endregion

        #region Dictionary

        public class DictionaryOptions
        {
            /// <summary>
            /// Gets or sets the value that enables or disables the ability to modify the dictionary of the designer.
            /// </summary>
            public bool AllowModifyDictionary { get; set; }

            /// <summary>
            /// Gets or sets the value that enables or disables the ability to modify connections in the dictionary of the designer.
            /// </summary>
            public bool AllowModifyConnections { get; set; }

            /// <summary>
            /// Gets or sets the value that enables or disables the ability to modify data sources in the dictionary of the designer.
            /// </summary>
            public bool AllowModifyDataSources { get; set; }

            /// <summary>
            /// Gets or sets the value that enables or disables the ability to modify relations in the dictionary of the designer.
            /// </summary>
            public bool AllowModifyRelations { get; set; }

            /// <summary>
            /// Gets or sets the value that enables or disables the ability to modify variables in the dictionary of the designer.
            /// </summary>
            public bool AllowModifyVariables { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the connection type in the dictionary of the designer.
            /// </summary>
            public bool ShowConnectionType { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the actions menu in the dictionary of the designer.
            /// </summary>
            public bool ShowActionsMenu { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the test connection button in the connection dialog.
            /// </summary>
            public bool ShowTestConnectionButton { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the alias for bisiness objects in the designer.
            /// </summary>
            public bool ShowOnlyAliasForBusinessObjects { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the alias for connections in the designer.
            /// </summary>
            public bool ShowOnlyAliasForConnections { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the alias for data sources in the designer.
            /// </summary>
            public bool ShowOnlyAliasForDataSources { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the alias for data relations in the designer.
            /// </summary>
            public bool ShowOnlyAliasForDataRelations { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the alias for data columns in the designer.
            /// </summary>
            public bool ShowOnlyAliasForDataColumns { get; set; }

            /// <summary>
            /// Gets or sets the value that displays or hides the alias for variables in the designer.
            /// </summary>
            public bool ShowOnlyAliasForVariables { get; set; }

            public DictionaryOptions()
            {
                AllowModifyDictionary = true;
                AllowModifyConnections = true;
                AllowModifyDataSources = true;
                AllowModifyRelations = true;
                AllowModifyVariables = true;
                ShowConnectionType = true;
                ShowActionsMenu = true;
                ShowTestConnectionButton = true;
                ShowOnlyAliasForBusinessObjects = false;
                ShowOnlyAliasForConnections = false;
                ShowOnlyAliasForDataSources = false;
                ShowOnlyAliasForDataRelations = false;
                ShowOnlyAliasForDataColumns = false;
                ShowOnlyAliasForVariables = false;
            }
        }

        #endregion

        #region Email

        public class EmailOptions
        {
            /// <summary>
            /// Gets or sets a value which allows to display the Email dialog, or send Email with the default settings.
            /// </summary>
            public bool ShowEmailDialog { get; set; }

            /// <summary>
            /// Gets or sets a value which allows to display the export dialog for Email, or export report for Email with the default settings.
            /// </summary>
            public bool ShowExportDialog { get; set; }

            /// <summary>
            /// Gets or sets the default email address of the message created in the preview. 
            /// </summary>
            public string DefaultEmailAddress { get; set; }

            public EmailOptions()
            {
                ShowEmailDialog = true;
                ShowExportDialog = true;
                DefaultEmailAddress = string.Empty;
            }
        }

        #endregion

        #region Exports

        public class ExportOptions
        {
            /// <summary>
            /// Gets or sets a value which allows to display the export dialog, or to export with the default settings.
            /// </summary>
            public bool ShowExportDialog { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the report document file.
            /// </summary>
            public bool ShowExportToDocument { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the PDF format.
            /// </summary>
            public bool ShowExportToPdf { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the XPS format.
            /// </summary>
            public bool ShowExportToXps { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the Power Point 2007-2010 format.
            /// </summary>
            public bool ShowExportToPowerPoint { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the HTML format.
            /// </summary>
            public bool ShowExportToHtml { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the HTML5 format.
            /// </summary>
            public bool ShowExportToHtml5 { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the MHT (Web Archive) format.
            /// </summary>
            public bool ShowExportToMht { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the TEXT format.
            /// </summary>
            public bool ShowExportToText { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the Rich Text format.
            /// </summary>
            public bool ShowExportToRtf { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the Word 2007-2010 format.
            /// </summary>
            public bool ShowExportToWord2007 { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the Open Document Text format.
            /// </summary>
            public bool ShowExportToOpenDocumentWriter { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the Excel BIFF format.
            /// </summary>
            public bool ShowExportToExcel { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the Excel XML format.
            /// </summary>
            public bool ShowExportToExcelXml { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the Excel 2007-2010 format.
            /// </summary>
            public bool ShowExportToExcel2007 { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the Open Document Calc format.
            /// </summary>
            public bool ShowExportToOpenDocumentCalc { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the CSV format.
            /// </summary>
            public bool ShowExportToCsv { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the DBF format.
            /// </summary>
            public bool ShowExportToDbf { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the XML format.
            /// </summary>
            public bool ShowExportToXml { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the DIF format.
            /// </summary>
            public bool ShowExportToDif { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the Sylk format.
            /// </summary>
            public bool ShowExportToSylk { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the BMP image format.
            /// </summary>
            public bool ShowExportToImageBmp { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the GIF image format.
            /// </summary>
            public bool ShowExportToImageGif { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the JPEG image format.
            /// </summary>
            public bool ShowExportToImageJpeg { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the PCX image format.
            /// </summary>
            public bool ShowExportToImagePcx { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the PNG image format.
            /// </summary>
            public bool ShowExportToImagePng { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the TIFF image format.
            /// </summary>
            public bool ShowExportToImageTiff { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the Metafile image format.
            /// </summary>
            public bool ShowExportToImageMetafile { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the SVG image format.
            /// </summary>
            public bool ShowExportToImageSvg { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that the user can save the report from the preview to the SVGZ image format.
            /// </summary>
            public bool ShowExportToImageSvgz { get; set; }

            public ExportOptions()
            {
                ShowExportDialog = true;
                ShowExportToDocument = true;
                ShowExportToPdf = true;
                ShowExportToXps = true;
                ShowExportToPowerPoint = true;
                ShowExportToHtml = true;
                ShowExportToHtml5 = true;
                ShowExportToMht = true;
                ShowExportToText = true;
                ShowExportToRtf = true;
                ShowExportToWord2007 = true;
                ShowExportToOpenDocumentWriter = true;
                ShowExportToExcel = true;
                ShowExportToExcelXml = true;
                ShowExportToExcel2007 = true;
                ShowExportToOpenDocumentCalc = true;
                ShowExportToCsv = true;
                ShowExportToDbf = true;
                ShowExportToXml = true;
                ShowExportToDif = true;
                ShowExportToSylk = true;
                ShowExportToImageBmp = true;
                ShowExportToImageGif = true;
                ShowExportToImageJpeg = true;
                ShowExportToImagePcx = true;
                ShowExportToImagePng = true;
                ShowExportToImageTiff = true;
                ShowExportToImageMetafile = true;
                ShowExportToImageSvg = true;
                ShowExportToImageSvgz = true;
            }
        }

        #endregion
        
        #region File Menu

        public class FileMenuOptions
        {
            /// <summary>
            /// Gets or sets a visibility of the New item in the file menu.
            /// </summary>
            public bool ShowNew { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the New Report item in the file menu.
            /// </summary>
            public bool ShowNewReport { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the New Report with Wizard item in the file menu.
            /// </summary>
            public bool ShowNewReportWithWizard { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the New Page item in the file menu.
            /// </summary>
            public bool ShowNewPage { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Open Report item in the file menu.
            /// </summary>
            public bool ShowOpen { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Save Report item in the file menu.
            /// </summary>
            public bool ShowSave { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Save Report As item in the file menu.
            /// </summary>
            public bool ShowSaveAs { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Preview item in the file menu. Only for Office2007 theme.
            /// </summary>
            public bool ShowPreview { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Preview as PDF item in the file menu. Only for Office2007 theme.
            /// </summary>
            public bool ShowPreviewAsPdf { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Preview as HTML item in the file menu. Only for Office2007 theme.
            /// </summary>
            public bool ShowPreviewAsHtml { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Preview as XPS item in the file menu. Only for Office2007 theme.
            /// </summary>
            public bool ShowPreviewAsXps { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Close item in the file menu.
            /// </summary>
            public bool ShowClose { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the About item in the file menu.
            /// </summary>
            public bool ShowAbout { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Exit item in the file menu.
            /// </summary>
            public bool ShowExit { get; set; }

            /// <summary>
            /// Gets or sets the text to display in the top of the file menu.
            /// </summary>
            public string Caption { get; set; }

            #region Obsolete

            [Obsolete("This option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public bool ShowOpenReportFromGoogleDocs { get; set; }

            [Obsolete("This option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public bool ShowSaveAsToGoogleDocs { get; set; }

            [Obsolete("This option is obsolete. It will be removed in next versions.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public bool ShowDeletePage { get; set; }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the ShowExit option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public bool ShowExitButton
            {
                get
                {
                    return ShowExit;
                }
                set
                {
                    ShowExit = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the ShowOpen option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public bool ShowOpenReport
            {
                get
                {
                    return ShowOpen;
                }
                set
                {
                    ShowOpen = value;
                }
            }

            [Obsolete("This option is obsolete. It will be removed in next versions. Please use the ShowOpen option instead.")]
            [EditorBrowsable(EditorBrowsableState.Never)]
            [JsonIgnore]
            public bool ShowSaveReport
            {
                get
                {
                    return ShowSave;
                }
                set
                {
                    ShowSave = value;
                }
            }

            #endregion

            public FileMenuOptions()
            {
                ShowNew = true;
                ShowNewReport = true;
                ShowNewReportWithWizard = true;
                ShowNewPage = true;
                ShowOpen = true;
                ShowSave = true;
                ShowSaveAs = true;
                ShowPreview = true;
                ShowPreviewAsPdf = true;
                ShowPreviewAsHtml = true;
                ShowPreviewAsXps = true;
                ShowClose = true;
                ShowAbout = true;
                ShowExit = true;
                Caption = "Default";
            }
        }

        #endregion

        #region Preview Toolbar

        public class PreviewToolbarOptions
        {
            /// <summary>
            /// Gets or sets a value which indicates that toolbar will be shown in the viewer.
            /// </summary>
            public bool Visible { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that navigate panel will be shown in the viewer.
            /// </summary>
            public bool ShowNavigatePanel { get; set; }

            /// <summary>
            /// Gets or sets a value which indicates that view mode panel will be shown in the viewer.
            /// </summary>
            public bool ShowViewModePanel { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Print button in the toolbar of the viewer.
            /// </summary>
            public bool ShowPrintButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Open button in the toolbar of the viewer.
            /// </summary>
            public bool ShowOpenButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Save button in the toolbar of the viewer.
            /// </summary>
            public bool ShowSaveButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Send Email button in the toolbar of the viewer.
            /// </summary>
            public bool ShowSendEmailButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Bookmarks button in the toolbar of the viewer.
            /// </summary>
            public bool ShowBookmarksButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Parameters button in the toolbar of the viewer.
            /// </summary>
            public bool ShowParametersButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Thumbnails button in the toolbar of the viewer.
            /// </summary>
            public bool ShowThumbnailsButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Find button in the toolbar of the viewer.
            /// </summary>
            public bool ShowFindButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Full Screen button in the toolbar of the viewer.
            /// </summary>
            public bool ShowFullScreenButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the First Page button in the navigate panel of the viewer.
            /// </summary>
            public bool ShowFirstPageButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Previous Page button in the navigate panel of the viewer.
            /// </summary>
            public bool ShowPreviousPageButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Go To Page button in the navigate panel of the viewer.
            /// </summary>
            public bool ShowGoToPageButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Next Page button in the navigate panel of the viewer.
            /// </summary>
            public bool ShowNextPageButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Last Page button in the navigate panel of the viewer.
            /// </summary>
            public bool ShowLastPageButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Single Page button in the view mode panel of the viewer.
            /// </summary>
            public bool ShowSinglePageViewModeButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Continuous Page button in the view mode panel of the viewer.
            /// </summary>
            public bool ShowContinuousPageViewModeButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of the Multiple Page button in the view mode panel of the viewer.
            /// </summary>
            public bool ShowMultiplePageViewModeButton { get; set; }

            /// <summary>
            /// Gets or sets a visibility of Zoom buttons in the toolbar of the viewer.
            /// </summary>
            public bool ShowZoomButtons { get; set; }

            public PreviewToolbarOptions()
            {
                Visible = true;
                ShowNavigatePanel = true;
                ShowViewModePanel = true;
                ShowPrintButton = true;
                ShowOpenButton = true;
                ShowSaveButton = true;
                ShowSendEmailButton = false;
                ShowBookmarksButton = true;
                ShowParametersButton = true;
                ShowThumbnailsButton = true;
                ShowFindButton = true;
                ShowFullScreenButton = true;
                ShowFirstPageButton = true;
                ShowPreviousPageButton = true;
                ShowGoToPageButton = true;
                ShowNextPageButton = true;
                ShowLastPageButton = true;
                ShowSinglePageViewModeButton = true;
                ShowContinuousPageViewModeButton = true;
                ShowMultiplePageViewModeButton = true;
                ShowZoomButtons = true;
            }
        }

        #endregion

        #region Print

        public class PrintOptions
        {
            /// <summary>
            /// Gets or sets a value which allows to display the Print dialog, or print report with the default settings.
            /// </summary>
            public bool ShowPrintDialog { get; set; }

            /// <summary>
            /// Gets or sets a value which allows the automatic page orientation when printing the report.
            /// </summary>
            public bool AutoPageOrientation { get; set; }

            /// <summary>
            /// Gets or sets a value which allows the automatic page scale when printing the report.
            /// </summary>
            public bool AutoPageScale { get; set; }

            /// <summary>
            /// Gets or sets a value which allows Default printing (by Flash engine).
            /// </summary>
            public bool AllowDefaultPrint { get; set; }

            /// <summary>
            /// Gets or sets a value which allows printing in PDF format.
            /// </summary>
            public bool AllowPrintToPdf { get; set; }

            /// <summary>
            /// Gets or sets a value which allows printing in HTML format.
            /// </summary>
            public bool AllowPrintToHtml { get; set; }

            /// <summary>
            /// Gets or sets a value which allows send the report to print as Image (only for Default format).
            /// </summary>
            public bool PrintAsBitmap { get; set; }

            public PrintOptions()
            {
                ShowPrintDialog = true;
                AutoPageOrientation = true;
                AutoPageScale = true;
                AllowDefaultPrint = true;
                AllowPrintToPdf = true;
                AllowPrintToHtml = true;
                PrintAsBitmap = true;
            }
        }

        #endregion

        #region Server

        public class ServerOptions
        {
            /// <summary>
            /// Gets or sets the name of the query processing controller of the report designer. 
            /// </summary>
            public string Controller { get; set; }

            /// <summary>
            /// Gets or sets the URL pattern of the route of the report designer. The {action} parameter can be used for the component actions, for example: /Home/{action}
            /// </summary>
            public string RouteTemplate { get; set; } = string.Empty;

            private int requestTimeout = 30;
            /// <summary>
            /// Gets or sets time which indicates how many seconds the client side will wait for the response from the server side. The default value is 30 seconds.
            /// </summary>
            public int RequestTimeout
            {
                get
                {
                    return requestTimeout;
                }
                set
                {
                    // Min 1 sec. Max 6 hours.
                    requestTimeout = Math.Max(1, Math.Min(21600, value));
                }
            }

            /// <summary>
            /// Gets or sets time which indicates how many minutes the result of the report rendering will be stored in the server cache or session. The default value is 10 minutes.
            /// </summary>
            public int CacheTimeout { get; set; }

            /// <summary>
            /// Gets or sets the mode of the report caching.
            /// </summary>
            public StiServerCacheMode CacheMode { get; set; }

            /// <summary>
            /// Specifies the relative priority of report, stored in the system cache.
            /// </summary>
            public CacheItemPriority CacheItemPriority { get; set; }

            /// <summary>
            /// Gets or sets the the number of repeats of requests of the server side to the client side, when getting errors of obtaining data.
            /// </summary>
            public int RepeatCount { get; set; }
            
            /// <summary>
            /// Gets or sets the value that enables or disables the logging of all requests/responses.
            /// </summary>
            public bool EnableDataLogger { get; set; }
            
            /// <summary>
            /// Gets or sets a value which indicates that the designer will use relative or absolute URLs.
            /// </summary>
            public bool UseRelativeUrls { get; set; }

            /// <summary>
            /// Gets or sets a value which enables or disables the transfer of URL parameters when requesting the scripts and styles of the designer.
            /// </summary>
            public bool PassQueryParametersForResources { get; set; }

            public ServerOptions()
            {
                Controller = string.Empty;
                CacheTimeout = 10;
                CacheMode = StiServerCacheMode.ObjectCache;
                CacheItemPriority = CacheItemPriority.Default;
                RepeatCount = 1;
                EnableDataLogger = false;
                UseRelativeUrls = true;
                PassQueryParametersForResources = true;
            }
        }

        #endregion
        
        #region Toolbar

        public class ToolbarOptions
        {
            /// <summary>
            /// Gets or sets a visibility of the Select Language control of the designer.
            /// </summary>
            public bool ShowSelectLanguage { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Code Tab of the designer.
            /// </summary>
            public bool ShowCodeTab { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Preview Report Tab of the designer.
            /// </summary>
            public bool ShowPreviewReportTab { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Dictionary Tab of the designer.
            /// </summary>
            public bool ShowDictionaryTab { get; set; }
            
            /// <summary>
            /// Gets or sets a visibility of the Events Tab of the designer.
            /// </summary>
            public bool ShowEventsTab { get; set; }

            private int zoom = StiZoomModeFx.Default;
            /// <summary>
            /// Gets or sets the report page zoom.
            /// </summary>
            public int Zoom
            {
                get
                {
                    return zoom;
                }
                set
                {
                    if (value > 500) zoom = 500;
                    if (value < 0 || (value < 10 && value > 4)) zoom = 10;
                    else zoom = value;
                }
            }

            public ToolbarOptions()
            {
                ShowSelectLanguage = true;
                ShowCodeTab = false;
                ShowPreviewReportTab = true;
                ShowDictionaryTab = true;
                ShowEventsTab = true;
            }
        }

        #endregion

        #region StiMvcDesignerFxOptions

        /// <summary>
        /// A class which controls settings of the designer actions.
        /// </summary>
        public ActionOptions Actions { get; set; }

        /// <summary>
        /// A class which controls settings of the designer appearance.
        /// </summary>
        public AppearanceOptions Appearance { get; set; }

        /// <summary>
        /// A class which controls settings of the designer behavior.
        /// </summary>
        public BehaviorOptions Behavior { get; set; }

        /// <summary>
        /// A class which controls settings of the designer dictionary.
        /// </summary>
        public DictionaryOptions Dictionary { get; set; }

        /// <summary>
        /// A class which controls the email options.
        /// </summary>
        public EmailOptions Email { get; set; }

        /// <summary>
        /// A class which controls the export options.
        /// </summary>
        public ExportOptions Exports { get; set; }

        /// <summary>
        /// A class which controls settings of the designer file menu.
        /// </summary>
        public FileMenuOptions FileMenu { get; set; }

        /// <summary>
        /// A class which controls settings of the preview toolbar.
        /// </summary>
        public PreviewToolbarOptions PreviewToolbar { get; set; }

        /// <summary>
        /// A class which controls the print options.
        /// </summary>
        public PrintOptions Print { get; set; }

        /// <summary>
        /// A class which controls the server options.
        /// </summary>
        public ServerOptions Server { get; set; }
        
        /// <summary>
        /// A class which controls settings of the designer toolbar.
        /// </summary>
        public ToolbarOptions Toolbar { get; set; }
        
        /// <summary>
        /// Gets or sets the current visual theme which is used for drawing visual elements of the designer.
        /// </summary>
        public StiDesignerFxTheme Theme { get; set; }

        /// <summary>
        /// Gets or sets a path to the localization file for the designer.
        /// </summary>
        public string Localization { get; set; }

        /// <summary>
        /// Gets or sets a path to the localization directory for the designer.
        /// </summary>
        public string LocalizationDirectory { get; set; }

        /// <summary>
        /// Gets or sets the width of the designer.
        /// </summary>
        public Unit Width { get; set; }

        /// <summary>
        /// Gets or sets the height of the designer.
        /// </summary>
        public Unit Height { get; set; }

        #region Obsolete

        [Obsolete("This class is obsolete. It will be removed in next versions. Please use the FileMenuOptions class instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class MainMenuOptions : FileMenuOptions
        {
        }

        [Obsolete("This option is obsolete. It will be removed in next versions. Please use the GetReport option instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [JsonIgnore]
        public FileMenuOptions MainMenu
        {
            get
            {
                return FileMenu;
            }
            set
            {
                FileMenu = value;
            }
        }

        #endregion

        public StiMvcDesignerFxOptions()
        {
            Actions = new ActionOptions();
            Appearance = new AppearanceOptions();
            Behavior = new BehaviorOptions();
            Dictionary = new DictionaryOptions();
            Email = new EmailOptions();
            Exports = new ExportOptions();
            FileMenu = new FileMenuOptions();
            PreviewToolbar = new PreviewToolbarOptions();
            Print = new PrintOptions();
            Server = new ServerOptions();
            Toolbar = new ToolbarOptions();

            Theme = StiDesignerFxTheme.Office2013;
            Localization = string.Empty;
            LocalizationDirectory = string.Empty;
            Width = Unit.Empty;
            Height = Unit.Empty;
        }

        #endregion
    }
}
