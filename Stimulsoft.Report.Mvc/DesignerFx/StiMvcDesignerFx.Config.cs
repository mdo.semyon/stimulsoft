﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Xml;
using System.Drawing;
using System.Web.UI.WebControls;
using Stimulsoft.Base;
using System.IO;
using System.Linq;
using System.Web;
using Stimulsoft.Base.Localization;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcDesignerFx : Panel
    {
        private string GetLocalizationsList()
        {
            if (string.IsNullOrEmpty(options.LocalizationDirectory)) return string.Empty;

            #region Get the localization files directory

            string path = options.LocalizationDirectory;
            if (!Directory.Exists(path)) path = HttpContext.Current.Server.MapPath(options.LocalizationDirectory);
            if (!Directory.Exists(path)) path = HttpContext.Current.Server.MapPath("/" + options.LocalizationDirectory);

            #endregion

            #region Get the localization files list

            if (Directory.Exists(path))
            {
                var dir = new DirectoryInfo(path);

                XmlDocument xml = new XmlDocument();
                XmlElement list = xml.CreateElement("LocalizationsList");
                xml.AppendChild(list);

                XmlElement value;
                XmlElement element;

                string languages = string.Empty;
                string language;
                string description;
                string cultureName;

                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    if (file.FullName.EndsWith(".xml") && !file.FullName.EndsWith(".ext.xml"))
                    {
                        try
                        {
                            StiLocalization.GetParam(file.FullName, out language, out description, out cultureName);
                            if (!string.IsNullOrEmpty(language))
                            {
                                value = xml.CreateElement("Value");
                                list.AppendChild(value);

                                element = xml.CreateElement("FileName");
                                element.InnerText = Path.Combine(path, file.Name);
                                value.AppendChild(element);

                                element = xml.CreateElement("Language");
                                element.InnerText = language;
                                value.AppendChild(element);

                                element = xml.CreateElement("Description");
                                element.InnerText = description;
                                value.AppendChild(element);

                                element = xml.CreateElement("CultureName");
                                element.InnerText = cultureName;
                                value.AppendChild(element);
                            }
                        }
                        catch
                        {
                        }
                    }
                }

                return list.InnerXml;
            }

            #endregion

            return string.Empty;
        }

        /// <summary>
        /// Get the list of registered data adapters.
        /// </summary>
        private string GetDataAdaptersList()
        {
            var adapters = string.Empty;
            foreach (var service in StiOptions.Services.DataAdapters.Where(s => s.ServiceEnabled))
            {
                adapters += service.GetType() + "\n";
            }

            #region Databases

            string result = string.Format(
                "<AdapterXmlData>{0}</AdapterXmlData>" +
                "<AdapterJsonData>{0}</AdapterJsonData>" +
                "<AdapterDB2>{1}</AdapterDB2>" +
                "<AdapterFirebird>{2}</AdapterFirebird>" +
                "<AdapterInformix>{3}</AdapterInformix>" +
                "<AdapterMSAccess>{4}</AdapterMSAccess>" +
                "<AdapterMySql>{5}</AdapterMySql>" +
                "<AdapterOdbc>{6}</AdapterOdbc>" +
                "<AdapterOleDb>{7}</AdapterOleDb>" +
                "<AdapterOracle>{8}</AdapterOracle>" +
                "<AdapterOracleODP>{9}</AdapterOracleODP>" +
                "<AdapterPostgreSQL>{10}</AdapterPostgreSQL>" +
                "<AdapterSql>{11}</AdapterSql>" +
                "<AdapterSqlCe>{12}</AdapterSqlCe>" +
                "<AdapterSQLite>{13}</AdapterSQLite>" +
                "<AdapterSybaseAds>{14}</AdapterSybaseAds>" +
                "<AdapterSybaseAse>{15}</AdapterSybaseAse>" +
                "<AdapterUniDirect>{16}</AdapterUniDirect>" +
                "<AdapterDataView>{17}</AdapterDataView>" +
                "<AdapterVistaDB>{18}</AdapterVistaDB>" +
                "<AdapterVirtual>{19}</AdapterVirtual>" +
                "<AdapterDBase>{20}</AdapterDBase>" +
                "<AdapterUser>{21}</AdapterUser>" +
                "<AdapterBusinessObject>{22}</AdapterBusinessObject>" +
                "<AdapterCrossTab>{23}</AdapterCrossTab>" +
                "<AdapterCsv>{24}</AdapterCsv>" +
                "<AdapterDotConnectUniversal>{25}</AdapterDotConnectUniversal>" +
                "<AdapterEffiProz>{26}</AdapterEffiProz>",
                adapters.IndexOf("StiDataTableAdapterService") >= 0,
                adapters.IndexOf("StiDB2AdapterService") >= 0,
                adapters.IndexOf("StiFirebirdAdapterService") >= 0,
                adapters.IndexOf("StiInformixAdapterService") >= 0,
                adapters.IndexOf("StiMSAccessAdapterService") >= 0,
                adapters.IndexOf("StiMySqlAdapterService") >= 0,
                adapters.IndexOf("StiOdbcAdapterService") >= 0,
                adapters.IndexOf("StiOleDbAdapterService") >= 0,
                adapters.IndexOf("StiOracleAdapterService") >= 0,
                adapters.IndexOf("StiOracleODPAdapterService") >= 0,
                adapters.IndexOf("StiPostgreSQLAdapterService") >= 0,
                adapters.IndexOf("StiSqlAdapterService") >= 0,
                adapters.IndexOf("StiSqlCeAdapterService") >= 0,
                adapters.IndexOf("StiSQLiteAdapterService") >= 0,
                adapters.IndexOf("StiSybaseAdsAdapterService") >= 0,
                adapters.IndexOf("StiSybaseAseAdapterService") >= 0,
                adapters.IndexOf("StiUniDirectAdapterService") >= 0,
                adapters.IndexOf("StiDataViewAdapterService") >= 0,
                adapters.IndexOf("StiVistaDBAdapterService") >= 0,
                adapters.IndexOf("StiVirtualAdapterService") >= 0,
                adapters.IndexOf("StiDBaseAdapterService") >= 0,
                adapters.IndexOf("StiUserAdapterService") >= 0,
                false, //adapters.IndexOf("StiBusinessObjectAdapterService") >= 0,
                adapters.IndexOf("StiCrossTabAdapterService") >= 0,
                adapters.IndexOf("StiCsvAdapterService") >= 0,
                adapters.IndexOf("StiDotConnectUniversalAdapterService") >= 0,
                adapters.IndexOf("StiEffiProzAdapterService") >= 0);

            #endregion

            return result;
        }

        private string RenderConfig()
        {
            // Create the Xml document
            XmlDocument doc = new XmlDocument();
            XmlDeclaration declaration = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(declaration);

            XmlElement category = null;
            XmlElement subcategory = null;
            XmlElement element = null;

            // Create the main node
            XmlElement config = doc.CreateElement("StiSerializer");
            config.SetAttribute("type", "Net");
            config.SetAttribute("version", "1.02");
            config.SetAttribute("application", "StiOptions");
            doc.AppendChild(config);

            #region Connection

            category = doc.CreateElement("Connection");
            config.AppendChild(category);

            element = doc.CreateElement("ClientRepeatCount");
            element.InnerText = options.Server.RepeatCount.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ClientRequestTimeout");
            element.InnerText = options.Server.RequestTimeout.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("EnableDataLogger");
            element.InnerText = options.Server.EnableDataLogger.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowCancelButton");
            element.InnerText = options.Appearance.ShowCancelButton.ToString();
            category.AppendChild(element);

            #endregion

            #region Appearance

            category = doc.CreateElement("Appearance");
            config.AppendChild(category);

            element = doc.CreateElement("ShowTooltips");
            element.InnerText = options.Appearance.ShowTooltips.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowTooltipsHelp");
            element.InnerText = options.Appearance.ShowTooltipsHelp.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowFormsHelp");
            element.InnerText = options.Appearance.ShowDialogsHelp.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ShowFormsHints");
            element.InnerText = options.Appearance.ShowDialogsHints.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("DateFormat");
            element.InnerText = options.Appearance.DateFormat;
            category.AppendChild(element);

            #endregion

            #region AboutDialog

            category = doc.CreateElement("AboutDialog");
            config.AppendChild(category);

            element = doc.CreateElement("TextLine1");
            element.InnerText = options.Appearance.AboutDialogTextLine1;
            category.AppendChild(element);

            element = doc.CreateElement("TextLine2");
            element.InnerText = options.Appearance.AboutDialogTextLine2;
            category.AppendChild(element);

            element = doc.CreateElement("Url");
            element.InnerText = options.Appearance.AboutDialogUrl;
            category.AppendChild(element);

            element = doc.CreateElement("UrlText");
            element.InnerText = options.Appearance.AboutDialogUrlText;
            category.AppendChild(element);

            element = doc.CreateElement("FrameworkType");
            element.InnerText = "ASP.NET MVC";
            category.AppendChild(element);

            #endregion

            #region Localization

            subcategory = doc.CreateElement("Localizations");
            config.AppendChild(subcategory);

            element = doc.CreateElement("Localization");
            element.InnerText = options.Localization;
            subcategory.AppendChild(element);

            string localizationsList = GetLocalizationsList();
            if (!string.IsNullOrEmpty(localizationsList))
            {
                element = doc.CreateElement("LocalizationsList");
                element.InnerXml = localizationsList;
                subcategory.AppendChild(element);
            }

            #endregion

            #region Print

            category = doc.CreateElement("Print");
            config.AppendChild(category);

            element = doc.CreateElement("ShowPrintDialog");
            element.InnerText = options.Print.ShowPrintDialog.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AutoPageOrientation");
            element.InnerText = options.Print.AutoPageOrientation.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AutoPageScale");
            element.InnerText = options.Print.AutoPageScale.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowDefaultPrint");
            element.InnerText = options.Print.AllowDefaultPrint.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowPrintToPdf");
            element.InnerText = options.Print.AllowPrintToPdf.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AllowPrintToHtml");
            element.InnerText = options.Print.AllowPrintToHtml.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("PrintAsBitmap");
            element.InnerText = options.Print.PrintAsBitmap.ToString();
            category.AppendChild(element);

            #endregion

            #region Theme

            element = doc.CreateElement("Theme");
            element.InnerText = options.Theme.ToString().Replace("Office2007", string.Empty);
            config.AppendChild(element);

            #endregion

            #region Other

            element = doc.CreateElement("DesignerEventFunction");
            element.InnerText = options.Behavior.DesignerEventFunction;
            config.AppendChild(element);

            #endregion

            #region Designer

            category = doc.CreateElement("Designer");
            config.AppendChild(category);

            element = doc.CreateElement("SaveMode");
            element.InnerText = options.Behavior.SaveReportMode.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("SaveAsMode");
            element.InnerText = options.Behavior.SaveReportAsMode.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("SaveAsOnServer");
            element.InnerText = (!string.IsNullOrEmpty(options.Actions.SaveReportAs)).ToString();
            category.AppendChild(element);

            element = doc.CreateElement("ExitOnServer");
            element.InnerText = (!string.IsNullOrEmpty(options.Actions.Exit)).ToString();
            category.AppendChild(element);

            element = doc.CreateElement("CreateReportOnServer");
            element.InnerText = (!string.IsNullOrEmpty(options.Actions.CreateReport)).ToString();
            category.AppendChild(element);

            element = doc.CreateElement("AutoSaveInterval");
            element.InnerText = options.Behavior.AutoSaveInterval.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("RunWizardAfterLoad");
            element.InnerText = options.Behavior.RunWizardAfterLoad.ToString();
            category.AppendChild(element);

            element = doc.CreateElement("Title");
            element.InnerText = options.Appearance.BrowserTitle;
            category.AppendChild(element);

            element = doc.CreateElement("ExitUrl");
            element.InnerText = options.Behavior.ExitUrl;
            category.AppendChild(element);

            #region Appearance

            subcategory = doc.CreateElement("Appearance");
            category.AppendChild(subcategory);

            element = doc.CreateElement("GridSizeCentimetres");
            element.InnerText = options.Appearance.GridSizeInCentimetres.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("GridSizeHundredthsOfInch");
            element.InnerText = options.Appearance.GridSizeInHundredthsOfInches.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("GridSizeInch");
            element.InnerText = options.Appearance.GridSizeInInches.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("GridSizeMillimeters");
            element.InnerText = options.Appearance.GridSizeInMillimeters.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSaveFileDialog");
            element.InnerText = options.Behavior.ShowSaveDialog.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowMoreDetailsButton");
            element.InnerText = options.Appearance.ShowMoreDetailsButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("SetReportModifiedWhenOpening");
            element.InnerText = options.Behavior.SetReportModifiedWhenOpening.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Database

            element = doc.CreateElement("Database");
            element.InnerXml = GetDataAdaptersList();
            category.AppendChild(element);

            #endregion

            #region Dictionary

            subcategory = doc.CreateElement("Dictionary");
            category.AppendChild(subcategory);

            element = doc.CreateElement("AllowModifyConnections");
            element.InnerText = options.Dictionary.AllowModifyConnections.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("AllowModifyDataSources");
            element.InnerText = options.Dictionary.AllowModifyDataSources.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("AllowModifyDictionary");
            element.InnerText = options.Dictionary.AllowModifyDictionary.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("AllowModifyRelations");
            element.InnerText = options.Dictionary.AllowModifyRelations.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("AllowModifyVariables");
            element.InnerText = options.Dictionary.AllowModifyVariables.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowConnectionType");
            element.InnerText = options.Dictionary.ShowConnectionType.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowActionsMenu");
            element.InnerText = options.Dictionary.ShowActionsMenu.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowTestConnectionButton");
            element.InnerText = options.Dictionary.ShowTestConnectionButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForBusinessObject");
            element.InnerText = options.Dictionary.ShowOnlyAliasForBusinessObjects.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForDatabase");
            element.InnerText = options.Dictionary.ShowOnlyAliasForConnections.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForDataSource");
            element.InnerText = options.Dictionary.ShowOnlyAliasForDataSources.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForDataRelation");
            element.InnerText = options.Dictionary.ShowOnlyAliasForDataRelations.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForDataColumn");
            element.InnerText = options.Dictionary.ShowOnlyAliasForDataColumns.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOnlyAliasForVariable");
            element.InnerText = options.Dictionary.ShowOnlyAliasForVariables.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region MainMenu

            subcategory = doc.CreateElement("MainMenu");
            category.AppendChild(subcategory);

            element = doc.CreateElement("ShowClose");
            element.InnerText = options.FileMenu.ShowClose.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExitButton");
            element.InnerText = (options.FileMenu.ShowExit && (!string.IsNullOrEmpty(options.Actions.Exit) || !string.IsNullOrEmpty(options.Behavior.ExitUrl))).ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNew");
            element.InnerText = options.FileMenu.ShowNew.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNewPage");
            element.InnerText = options.FileMenu.ShowNewPage.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNewReport");
            element.InnerText = options.FileMenu.ShowNewReport.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNewReportWithWizard");
            element.InnerText = options.FileMenu.ShowNewReportWithWizard.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOpenReport");
            element.InnerText = options.FileMenu.ShowOpen.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreview");
            element.InnerText = options.FileMenu.ShowPreview.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviewAsPdf");
            element.InnerText = options.FileMenu.ShowPreviewAsPdf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviewAsHtml");
            element.InnerText = options.FileMenu.ShowPreviewAsHtml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviewAsXps");
            element.InnerText = options.FileMenu.ShowPreviewAsXps.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSaveAs");
            element.InnerText = options.FileMenu.ShowSaveAs.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSaveReport");
            element.InnerText = options.FileMenu.ShowSave.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("Caption");
            element.InnerText = options.FileMenu.Caption;
            subcategory.AppendChild(element);

            #endregion

            #region Toolbar

            subcategory = doc.CreateElement("Toolbar");
            category.AppendChild(subcategory);

            element = doc.CreateElement("ShowSelectLanguage");
            element.InnerText = options.Toolbar.ShowSelectLanguage.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowAboutButton");
            element.InnerText = options.FileMenu.ShowAbout.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowCodeTab");
            element.InnerText = options.Toolbar.ShowCodeTab.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviewReportTab");
            element.InnerText = options.Toolbar.ShowPreviewReportTab.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowDictionaryTab");
            element.InnerText = options.Toolbar.ShowDictionaryTab.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowEventsTab");
            element.InnerText = options.Toolbar.ShowEventsTab.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Zoom

            element = doc.CreateElement("Zoom");
            element.InnerText = options.Toolbar.Zoom.ToString();
            subcategory.AppendChild(element);

            #endregion

            #endregion

            #region Viewer

            category = doc.CreateElement("Viewer");
            config.AppendChild(category);

            #region Appearance

            subcategory = doc.CreateElement("Appearance");
            category.AppendChild(subcategory);

            element = doc.CreateElement("AutoHideScrollbars");
            element.InnerText = options.Appearance.AutoHideScrollbars.ToString();
            subcategory.AppendChild(element);

            Color color = options.Appearance.CurrentPageBorderColor;
            element = doc.CreateElement("CurrentPageBorderColor");
            element.InnerText = string.Format("{0}, {1}, {2}, {3}", color.A, color.R, color.G, color.B);
            subcategory.AppendChild(element);

            element = doc.CreateElement("OpenLinksTarget");
            element.InnerText = options.Appearance.OpenLinksWindow;
            subcategory.AppendChild(element);

            element = doc.CreateElement("VariablesPanelColumns");
            element.InnerText = options.Appearance.ParametersPanelColumnsCount.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("VariablesPanelEditorWidth");
            element.InnerText = options.Appearance.ParametersPanelEditorWidth.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Toolbar

            subcategory = doc.CreateElement("Toolbar");
            category.AppendChild(subcategory);

            element = doc.CreateElement("ShowMainToolbar");
            element.InnerText = options.PreviewToolbar.Visible.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNavigateToolbar");
            element.InnerText = options.PreviewToolbar.ShowNavigatePanel.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowViewModeToolbar");
            element.InnerText = options.PreviewToolbar.ShowViewModePanel.ToString();
            subcategory.AppendChild(element);

            #endregion

            #region Buttons

            element = doc.CreateElement("ShowAboutButton");
            element.InnerText = "False";
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowBookmarksButton");
            element.InnerText = options.PreviewToolbar.ShowBookmarksButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowFindButton");
            element.InnerText = options.PreviewToolbar.ShowFindButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowFirstPageButton");
            element.InnerText = options.PreviewToolbar.ShowFirstPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowFullScreenButton");
            element.InnerText = options.PreviewToolbar.ShowFullScreenButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowGoToPageButton");
            element.InnerText = options.PreviewToolbar.ShowGoToPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowLastPageButton");
            element.InnerText = options.PreviewToolbar.ShowLastPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowNextPageButton");
            element.InnerText = options.PreviewToolbar.ShowNextPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowOpenButton");
            element.InnerText = options.PreviewToolbar.ShowOpenButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeContinuousButton");
            element.InnerText = options.PreviewToolbar.ShowContinuousPageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeMultipleButton");
            element.InnerText = options.PreviewToolbar.ShowMultiplePageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPageViewModeSingleButton");
            element.InnerText = options.PreviewToolbar.ShowSinglePageViewModeButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowParametersButton");
            element.InnerText = options.PreviewToolbar.ShowParametersButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowParametersPanel");
            element.InnerText = options.PreviewToolbar.ShowParametersButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPreviousPageButton");
            element.InnerText = options.PreviewToolbar.ShowPreviousPageButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowPrintButton");
            element.InnerText = options.PreviewToolbar.ShowPrintButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSaveButton");
            element.InnerText = options.PreviewToolbar.ShowSaveButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowSendEMailButton");
            element.InnerText = options.PreviewToolbar.ShowSendEmailButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowThumbnailsButton");
            element.InnerText = options.PreviewToolbar.ShowThumbnailsButton.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowDesignButton");
            element.InnerText = "False";
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExitButton");
            element.InnerText = "False";
            subcategory.AppendChild(element);

            #endregion

            #region Exports

            subcategory = doc.CreateElement("Exports");
            category.AppendChild(subcategory);

            element = doc.CreateElement("ShowExportDialog");
            element.InnerText = options.Exports.ShowExportDialog.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDocument");
            element.InnerText = options.Exports.ShowExportToDocument.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPdf");
            element.InnerText = options.Exports.ShowExportToPdf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToXps");
            element.InnerText = options.Exports.ShowExportToXps.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPpt");
            element.InnerText = options.Exports.ShowExportToPowerPoint.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToHtml");
            element.InnerText = options.Exports.ShowExportToHtml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToMht");
            element.InnerText = options.Exports.ShowExportToMht.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToText");
            element.InnerText = options.Exports.ShowExportToText.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToRtf");
            element.InnerText = options.Exports.ShowExportToRtf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToWord2007");
            element.InnerText = options.Exports.ShowExportToWord2007.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToOpenDocumentWriter");
            element.InnerText = options.Exports.ShowExportToOpenDocumentWriter.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcel");
            element.InnerText = options.Exports.ShowExportToExcel.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcelXml");
            element.InnerText = options.Exports.ShowExportToExcelXml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToExcel2007");
            element.InnerText = options.Exports.ShowExportToExcel2007.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToOpenDocumentCalc");
            element.InnerText = options.Exports.ShowExportToOpenDocumentCalc.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToCsv");
            element.InnerText = options.Exports.ShowExportToCsv.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDbf");
            element.InnerText = options.Exports.ShowExportToDbf.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToXml");
            element.InnerText = options.Exports.ShowExportToXml.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToDif");
            element.InnerText = options.Exports.ShowExportToDif.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSylk");
            element.InnerText = options.Exports.ShowExportToSylk.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToBmp");
            element.InnerText = options.Exports.ShowExportToImageBmp.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToGif");
            element.InnerText = options.Exports.ShowExportToImageGif.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToJpeg");
            element.InnerText = options.Exports.ShowExportToImageJpeg.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPcx");
            element.InnerText = options.Exports.ShowExportToImagePcx.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToPng");
            element.InnerText = options.Exports.ShowExportToImagePng.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToTiff");
            element.InnerText = options.Exports.ShowExportToImageTiff.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToMetafile");
            element.InnerText = options.Exports.ShowExportToImageMetafile.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSvg");
            element.InnerText = options.Exports.ShowExportToImageSvg.ToString();
            subcategory.AppendChild(element);

            element = doc.CreateElement("ShowExportToSvgz");
            element.InnerText = options.Exports.ShowExportToImageSvgz.ToString();
            subcategory.AppendChild(element);

            #endregion

            #endregion

            return StiGZipHelper.Pack(doc.OuterXml);
        }
    }
}
