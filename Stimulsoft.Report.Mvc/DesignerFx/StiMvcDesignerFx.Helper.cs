﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Web;
using System.Collections.Specialized;
using System.Web.Routing;
using Stimulsoft.Report.Export;
using System.ComponentModel;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcDesignerFx : Panel
    {
        #region Request Params

        /// <summary>
        /// Get the all request parameters of the report designer.
        /// </summary>
        public static StiRequestParams GetRequestParams()
        {
            StiRequestParams requestParams = StiRequestParamsHelper.Get();
            requestParams.Cache.Helper = CacheHelper;
            return requestParams;
        }

        #endregion

        #region Form and Route Values

        /// <summary>
        /// Get the POST form values for the start page of the report designer.
        /// </summary>
        public static NameValueCollection GetFormValues()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetFormValues(requestParams);
        }

        /// <summary>
        /// Get the POST form values for the start page of the report designer. The specified request parameters will be used.
        /// </summary>
        public static NameValueCollection GetFormValues(StiRequestParams requestParams)
        {
            return requestParams.FormValues;
        }

        /// <summary>
        /// Get the route values for the start page of the report designer.
        /// </summary>
        public static RouteValueDictionary GetRouteValues()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetRouteValues(requestParams);
        }

        /// <summary>
        /// Get the route values for the start page of the report designer. The specified request parameters will be used.
        /// </summary>
        public static RouteValueDictionary GetRouteValues(StiRequestParams requestParams)
        {
            return StiMvcViewer.GetRouteValues(requestParams);
        }

        #endregion

        #region Report

        /// <summary>
        /// Get the report template or report snapshot, sent by the client. If the report was not passed, the method tries to take it from the cache.
        /// </summary>
        public static StiReport GetReportObject()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetReportObject(requestParams);
        }

        /// <summary>
        /// Get the report template or report snapshot, sent by the client. If the report was not passed, the method tries to take it from the cache.
        /// The specified request parameters will be used.
        /// </summary>
        public static StiReport GetReportObject(StiRequestParams requestParams)
        {
            return requestParams.Cache.Helper.GetReportInternal(requestParams);
        }

        #endregion

        #region Export Settings

        /// <summary>
        /// Get the export settings from the dialog form of the report preview.
        /// </summary>
        public static StiExportSettings GetExportSettings()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetExportSettings(requestParams);
        }

        /// <summary>
        /// Get the export settings from the dialog form of the report preview. The specified request parameters will be used.
        /// </summary>
        public static StiExportSettings GetExportSettings(StiRequestParams requestParams)
        {
            return StiExportsHelper.GetExportSettings(requestParams);
        }

        #endregion

        #region Email Options

        /// <summary>
        /// Get the Email options from the dialog, sent by the client.
        /// </summary>
        public static StiEmailOptions GetEmailOptions()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetEmailOptions(requestParams);
        }

        /// <summary>
        /// Get the Email options from the dialog, sent by the client. The specified request parameters will be used.
        /// </summary>
        public static StiEmailOptions GetEmailOptions(StiRequestParams requestParams)
        {
            return StiWebViewer.GetEmailOptions(requestParams);
        }

        #endregion

        #region Localization

        /// <summary>
        /// Get the localization file name, requested on the client side.
        /// </summary>
        public static string GetLocalizationName()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetLocalizationName(requestParams);
        }

        /// <summary>
        /// Get the localization file name, requested on the client side. The specified request parameters will be used.
        /// </summary>
        public static string GetLocalizationName(StiRequestParams requestParams)
        {
            return requestParams.Localization;
        }

        #endregion


        #region Obsolete

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetRequestParams() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static StiRequestParams GetRequestData()
        {
            return GetRequestParams();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the RequestParams.Designer.IsNewReport property instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static bool GetNewReportFlag()
        {
            StiRequestParams requestParams = GetRequestParams();
            return requestParams.Designer.IsNewReport;
        }

        #endregion
    }
}
