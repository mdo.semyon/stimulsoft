﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Web.Mvc;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Web;
using Stimulsoft.Report.Export;
using System;
using System.ComponentModel;
using System.Xml;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcDesignerFx : Panel
    {
        #region GetReportResult

        /// <summary>
        /// Get the action result required for edit a new report template in the designer.
        /// </summary>
        public static ActionResult GetReportResult()
        {
            return GetReportResult(null, null);
        }

        /// <summary>
        /// Get the action result required for edit a new report template in the designer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult GetReportResult(StiRequestParams requestParams)
        {
            return GetReportResult(requestParams, null);
        }

        /// <summary>
        /// Get the action result required for edit the specified report template in the designer.
        /// </summary>
        public static ActionResult GetReportResult(StiReport report)
        {
            return GetReportResult(null, report);
        }

        /// <summary>
        /// Get the action result required for edit the specified report template in the designer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult GetReportResult(StiRequestParams requestParams, StiReport report)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            StiWebActionResult result = StiReportHelperFx.GetReportTemplateResult(requestParams, report, null);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        #endregion

        #region PreviewReportResult

        /// <summary>
        /// Get the action result required for preview the report in the designer.
        /// </summary>
        public static ActionResult PreviewReportResult()
        {
            return PreviewReportResult(null, null);
        }

        /// <summary>
        /// Get the action result required for preview the report in the designer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult PreviewReportResult(StiRequestParams requestParams)
        {
            return PreviewReportResult(requestParams, null);
        }

        /// <summary>
        /// Get the action result required for preview the report in the designer. The specified report template or snapshot will be used.
        /// </summary>
        public static ActionResult PreviewReportResult(StiReport report)
        {
            return PreviewReportResult(null, report);
        }

        /// <summary>
        /// Get the action result required for preview the report in the designer. The specified request parameters and report template or snapshot will be used.
        /// </summary>
        public static ActionResult PreviewReportResult(StiRequestParams requestParams, StiReport report)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject();
            StiWebActionResult result = StiReportHelperFx.GetInteractionResult(requestParams, report);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        #endregion

        #region SaveReportResult

        /// <summary>
        /// Get the action result required for saving the report template.
        /// </summary>
        public static ActionResult SaveReportResult()
        {
            return SaveReportResult(null, null, string.Empty);
        }

        /// <summary>
        /// Get the action result required for saving the report template. After saving it is possible to display a dialog box with specified error code.
        /// If the error code is zero, the dialog box with a message about successful saving will be displayed.
        /// </summary>
        public static ActionResult SaveReportResult(int errorCode)
        {
            return SaveReportResult(null, null, errorCode);
        }

        /// <summary>
        /// Get the action result required for saving the report template. After saving it is possible to display a dialog box with specified message.
        /// </summary>
        public static ActionResult SaveReportResult(string errorMessage)
        {
            return SaveReportResult(null, null, errorMessage);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The report will be sent back to the designer.
        /// </summary>
        public static ActionResult SaveReportResult(StiReport report)
        {
            return SaveReportResult(null, report, string.Empty);
        }

        /// <summary>
        /// Get the action result required for saving the report template. After saving it is possible to display a dialog box with specified error code.
        /// If the error code is zero, the dialog box with a message about successful saving will be displayed. The report will be sent back to the designer.
        /// </summary>
        public static ActionResult SaveReportResult(StiReport report, int errorCode)
        {
            return SaveReportResult(null, report, errorCode);
        }

        /// <summary>
        /// Get the action result required for saving the report template. After saving it is possible to display a dialog box with specified message.
        /// The report will be sent back to the designer.
        /// </summary>
        public static ActionResult SaveReportResult(StiReport report, string errorMessage)
        {
            return SaveReportResult(null, report, errorMessage);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The specified request parameters will be used.
        /// </summary>
        public static ActionResult SaveReportResult(StiRequestParams requestParams)
        {
            return SaveReportResult(requestParams, null, string.Empty);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The specified request parameters will be used.
        /// After saving it is possible to display a dialog box with specified error code.
        /// If the error code is zero, the dialog box with a message about successful saving will be displayed.
        /// </summary>
        public static ActionResult SaveReportResult(StiRequestParams requestParams, int errorCode)
        {
            return SaveReportResult(requestParams, null, errorCode);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The specified request parameters will be used.
        /// After saving it is possible to display a dialog box with specified message.
        /// </summary>
        public static ActionResult SaveReportResult(StiRequestParams requestParams, string errorMessage)
        {
            return SaveReportResult(requestParams, null, errorMessage);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The specified request parameters will be used. The report will be sent back to the designer.
        /// </summary>
        public static ActionResult SaveReportResult(StiRequestParams requestParams, StiReport report)
        {
            return SaveReportResult(requestParams, report, string.Empty);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The specified request parameters will be used.
        /// After saving it is possible to display a dialog box with specified error code. 
        /// If the error code is zero, the dialog box with a message about successful saving will be displayed. The report will be sent back to the designer.
        /// </summary>
        public static ActionResult SaveReportResult(StiRequestParams requestParams, StiReport report, int errorCode)
        {
            return SaveReportResult(requestParams, report, errorCode >= 0 ? errorCode.ToString() : string.Empty);
        }

        /// <summary>
        /// Get the action result required for saving the report template. The specified request parameters will be used.
        /// After saving it is possible to display a dialog box with specified message. The report will be sent back to the designer.
        /// </summary>
        public static ActionResult SaveReportResult(StiRequestParams requestParams, StiReport report, string errorMessage)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            string message = string.IsNullOrEmpty(errorMessage) ? "-1" : errorMessage;
            StiWebActionResult result = (report != null) ? StiReportHelperFx.GetReportTemplateResult(requestParams, report, message) : StiWebActionResult.StringResult(requestParams, message);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        #endregion

        #region PrintReportResult

        /// <summary>
        /// Get the action result required for print the report to PDF format.
        /// </summary>
        public static ActionResult PrintReportResult()
        {
            return PrintReportResult(null, null, null);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified report will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiReport report)
        {
            return PrintReportResult(null, report, null);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiExportSettings settings)
        {
            return PrintReportResult(null, null, settings);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified report and export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiReport report, StiExportSettings settings)
        {
            return PrintReportResult(null, report, settings);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified request parameters will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams)
        {
            return PrintReportResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams, StiReport report)
        {
            return PrintReportResult(requestParams, report, null);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified request parameters and export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams, StiExportSettings settings)
        {
            return PrintReportResult(requestParams, null, settings);
        }

        /// <summary>
        /// Get the action result required for print the report to PDF format. The specified request parameters, report and export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams, StiReport report, StiExportSettings settings)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            if (settings == null) settings = GetExportSettings(requestParams);
            if (settings is StiPdfExportSettings) ((StiPdfExportSettings)settings).AutoPrintMode = StiPdfAutoPrintMode.Dialog;
            StiWebActionResult result = StiExportsHelper.ExportReportResult(requestParams, report, settings);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        #endregion

        #region ExportReportResult

        /// <summary>
        /// Get the action result required for export the report.
        /// </summary>
        public static ActionResult ExportReportResult()
        {
            return ExportReportResult(null, null, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified report will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiReport report)
        {
            return ExportReportResult(null, report, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiExportSettings settings)
        {
            return ExportReportResult(null, null, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified report and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiReport report, StiExportSettings settings)
        {
            return ExportReportResult(null, report, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams)
        {
            return ExportReportResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiReport report)
        {
            return ExportReportResult(requestParams, report, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiExportSettings settings)
        {
            return ExportReportResult(requestParams, null, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters, report and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiReport report, StiExportSettings settings)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            if (settings == null) settings = GetExportSettings(requestParams);
            StiWebActionResult result = StiExportsHelper.ExportReportResult(requestParams, report, settings);
            return new StiMvcActionResult(result.Data, result.ContentType, result.FileName, !string.IsNullOrEmpty(result.FileName));
        }

        #endregion

        #region EmailReportResult

        /// <summary>
        /// Get the action result required for send the report by Email with specified options.
        /// </summary>
        public static ActionResult EmailReportResult(StiEmailOptions options)
        {
            return EmailReportResult(null, null, options, null);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified request parameters will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiRequestParams requestParams, StiEmailOptions options)
        {
            return EmailReportResult(requestParams, null, options, null);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified export settings will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiEmailOptions options, StiExportSettings settings)
        {
            return EmailReportResult(null, null, options, settings);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified report will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiReport report, StiEmailOptions options)
        {
            return EmailReportResult(null, report, options, null);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified report and export settings will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiReport report, StiEmailOptions options, StiExportSettings settings)
        {
            return EmailReportResult(null, report, options, settings);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified request parameters, report and export settings will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiRequestParams requestParams, StiReport report, StiEmailOptions options, StiExportSettings settings)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            if (settings == null) settings = GetExportSettings(requestParams);
            StiWebActionResult result = StiExportsHelper.EmailReportResult(requestParams, report, settings, options);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        #endregion

        #region GetLocalizationResult

        /// <summary>
        /// Get the action result required for loading the locatization file.
        /// </summary>
        public static ActionResult GetLocalizationResult()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetLocalizationResult(requestParams);
        }

        /// <summary>
        /// Get the action result required for loading the locatization file with the specified file path.
        /// </summary>
        public static ActionResult GetLocalizationResult(string localizationPath)
        {
            StiRequestParams requestParams = GetRequestParams();
            requestParams.Localization = localizationPath;
            return GetLocalizationResult(requestParams);
        }

        /// <summary>
        /// Get the action result required for loading the specified locatization file.
        /// </summary>
        public static ActionResult GetLocalizationResult(XmlDocument localization)
        {
            StiRequestParams requestParams = GetRequestParams();
            StiWebActionResult result = StiWebActionResult.StringResult(requestParams, localization.OuterXml);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        /// <summary>
        /// Get the action result required for loading the locatization file. The specified request parameters will be used.
        /// </summary>
        public static ActionResult GetLocalizationResult(StiRequestParams requestParams)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            StiWebActionResult result = StiLocalizationHelperFx.GetLocalizationResult(requestParams);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }

        #endregion

        #region DesignerEventResult

        /// <summary>
        /// Get the action result required for the various requests of the designer.
        /// </summary>
        public static ActionResult DesignerEventResult()
        {
            return DesignerEventResult(null, null);
        }

        /// <summary>
        /// Get the action result required for the various requests of the designer. The specified report will be used.
        /// </summary>
        public static ActionResult DesignerEventResult(StiReport report)
        {
            return DesignerEventResult(null, report);
        }

        /// <summary>
        /// Get the action result required for the various requests of the designer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult DesignerEventResult(StiRequestParams requestParams)
        {
            return DesignerEventResult(requestParams, null);
        }

        /// <summary>
        /// Get the action result required for the various requests of the designer. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult DesignerEventResult(StiRequestParams requestParams, StiReport report)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (requestParams.Component == StiComponentType.DesignerFx)
            {
                StiWebActionResult result = null;
                switch (requestParams.Action)
                {
                    case StiAction.Resource:
                        result = StiDesignerResourcesHelper.Get(requestParams);
                        return new StiMvcActionResult(result.Data, result.ContentType, result.FileName, false, true);

                    case StiAction.PreviewReport:
                    case StiAction.Variables:
                    case StiAction.Sorting:
                    case StiAction.DrillDown:
                        return PreviewReportResult(requestParams, report);

                    case StiAction.PrintReport:
                        return PrintReportResult(requestParams, report);

                    case StiAction.ExportReport:
                        return ExportReportResult(requestParams, report);

                    case StiAction.GetLocalization:
                        return GetLocalizationResult(requestParams);

                    case StiAction.TestConnection:
                        result = StiDictionaryHelperFx.TestConnection(requestParams);
                        break;

                    case StiAction.RetrieveColumns:
                        result = StiDictionaryHelperFx.RetrieveColumns(requestParams);
                        break;

                    case StiAction.GetReportCode:
                        result = StiReportHelperFx.GetReportCodeResult(requestParams);
                        break;

                    case StiAction.GetImage:
                        result = StiReportHelperFx.GetComponentImageResult(requestParams);
                        break;
                }

                if (result != null) return new StiMvcActionResult(result.Data, result.ContentType, result.FileName, false);
            }

            return new ViewResult();
        }

        #endregion


        #region Obsolete

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportTemplateResult()
        {
            return GetReportResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportTemplateResult(StiReport report)
        {
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the PreviewReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportSnapshotResult()
        {
            return PreviewReportResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the PreviewReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportSnapshotResult(StiReport report)
        {
            return PreviewReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the SaveReportResult(0) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult SaveReportResult(bool showSaveMessage)
        {
            return SaveReportResult(0);
        }

        #endregion
    }
}
