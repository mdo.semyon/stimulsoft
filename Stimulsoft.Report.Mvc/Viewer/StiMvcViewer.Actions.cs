﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using Stimulsoft.Report.Web;
using Stimulsoft.Report.Export;
using System.ComponentModel;
using System.Web;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcViewer : Panel
    {
        #region GetReportResult
        /// <summary>
        /// Get the action result required for show the specified report in the viewer.
        /// </summary>
        public static ActionResult GetReportResult(StiReport report)
        {
            return GetReportResult(null, report);
        }

        /// <summary>
        /// Get the action result required for show the specified report in the viewer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult GetReportResult(StiRequestParams requestParams, StiReport report)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            StiReportHelper.ApplyQueryParameters(requestParams, report);
            requestParams.Cache.Helper.SaveReportInternal(requestParams, report);
            return ViewerEventResult(requestParams, report);
        }
        #endregion

        #region PrintReportResult
        /// <summary>
        /// Get the action result required for print the report.
        /// </summary>
        public static ActionResult PrintReportResult()
        {
            return PrintReportResult(null, null, null);
        }

        /// <summary>
        /// Get the action result required for print the report. The specified report will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiReport report)
        {
            return PrintReportResult(null, report, null);
        }

        /// <summary>
        /// Get the action result required for print the report. The specified export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiExportSettings settings)
        {
            return PrintReportResult(null, null, settings);
        }

        /// <summary>
        /// Get the action result required for print the report. The specified report and export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiReport report, StiExportSettings settings)
        {
            return PrintReportResult(null, report, settings);
        }

        /// <summary>
        /// Get the action result required for print the report. The specified request parameters will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams)
        {
            return PrintReportResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for print the report. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams, StiReport report)
        {
            return PrintReportResult(requestParams, report, null);
        }

        /// <summary>
        /// Get the action result required for print the report. The specified request parameters and export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams, StiExportSettings settings)
        {
            return PrintReportResult(requestParams, null, settings);
        }

        /// <summary>
        /// Get the action result required for print the report. The specified request parameters, report and export settings will be used.
        /// </summary>
        public static ActionResult PrintReportResult(StiRequestParams requestParams, StiReport report, StiExportSettings settings)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            if (settings == null) settings = GetExportSettings(requestParams);
            StiWebActionResult result = StiExportsHelper.PrintReportResult(requestParams, report, settings);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }
        #endregion

        #region ExportReportResult
        /// <summary>
        /// Get the action result required for export the report.
        /// </summary>
        public static ActionResult ExportReportResult()
        {
            return ExportReportResult(null, null, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified report will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiReport report)
        {
            return ExportReportResult(null, report, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiExportSettings settings)
        {
            return ExportReportResult(null, null, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified report and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiReport report, StiExportSettings settings)
        {
            return ExportReportResult(null, report, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams)
        {
            return ExportReportResult(requestParams, null, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiReport report)
        {
            return ExportReportResult(requestParams, report, null);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiExportSettings settings)
        {
            return ExportReportResult(requestParams, null, settings);
        }

        /// <summary>
        /// Get the action result required for export the report. The specified request parameters, report and export settings will be used.
        /// </summary>
        public static ActionResult ExportReportResult(StiRequestParams requestParams, StiReport report, StiExportSettings settings)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            if (settings == null) settings = GetExportSettings(requestParams);
            StiWebActionResult result = StiExportsHelper.ExportReportResult(requestParams, report, settings);
            return new StiMvcActionResult(result.Data, result.ContentType, result.FileName, !string.IsNullOrEmpty(result.FileName));
        }
        #endregion

        #region EmailReportResult
        /// <summary>
        /// Get the action result required for send the report by Email with specified options.
        /// </summary>
        public static ActionResult EmailReportResult(StiEmailOptions options)
        {
            return EmailReportResult(null, null, options, null);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified request parameters will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiRequestParams requestParams, StiEmailOptions options)
        {
            return EmailReportResult(requestParams, null, options, null);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified export settings will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiEmailOptions options, StiExportSettings settings)
        {
            return EmailReportResult(null, null, options, settings);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified report will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiReport report, StiEmailOptions options)
        {
            return EmailReportResult(null, report, options, null);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. ThThe specified report and export settings will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiReport report, StiEmailOptions options, StiExportSettings settings)
        {
            return EmailReportResult(null, report, options, settings);
        }

        /// <summary>
        /// Get the action result required for send the report by Email with specified options. The specified request parameters, report and export settings will be used.
        /// </summary>
        public static ActionResult EmailReportResult(StiRequestParams requestParams, StiReport report, StiEmailOptions options, StiExportSettings settings)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            if (settings == null) settings = GetExportSettings(requestParams);
            StiWebActionResult result = StiExportsHelper.EmailReportResult(requestParams, report, settings, options);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }
        #endregion

        #region InteractionResult
        /// <summary>
        /// Get the action result required for interactive operations with a report.
        /// </summary>
        public static ActionResult InteractionResult()
        {
            return InteractionResult(null, null);
        }

        /// <summary>
        /// Get the action result required for interactive operations with a report. The specified request parameters will be used.
        /// </summary>
        public static ActionResult InteractionResult(StiRequestParams requestParams)
        {
            return InteractionResult(requestParams, null);
        }

        /// <summary>
        /// Get the action result required for interactive operations with a report. The specified report will be used.
        /// </summary>
        public static ActionResult InteractionResult(StiReport report)
        {
            return InteractionResult(null, report);
        }

        /// <summary>
        /// Get the action result required for interactive operations with a report. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult InteractionResult(StiRequestParams requestParams, StiReport report)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (report == null) report = GetReportObject(requestParams);
            StiWebActionResult result = StiReportHelper.InteractionResult(requestParams, report);
            return new StiMvcActionResult(result.Data, result.ContentType);
        }
        #endregion

        #region ViewerEventResult
        /// <summary>
        /// Get the action result required for the various requests of the viewer.
        /// </summary>
        public static ActionResult ViewerEventResult()
        {
            return ViewerEventResult(null, null);
        }

        /// <summary>
        /// Get the action result required for the various requests of the viewer.The specified report will be used.
        /// </summary>
        public static ActionResult ViewerEventResult(StiReport report)
        {
            return ViewerEventResult(null, report);
        }

        /// <summary>
        /// Get the action result required for the various requests of the viewer. The specified request parameters will be used.
        /// </summary>
        public static ActionResult ViewerEventResult(StiRequestParams requestParams)
        {
            return ViewerEventResult(requestParams, null);
        }

        /// <summary>
        /// Get the action result required for the various requests of the viewer. The specified request parameters and report will be used.
        /// </summary>
        public static ActionResult ViewerEventResult(StiRequestParams requestParams, StiReport report)
        {
            if (requestParams == null) requestParams = GetRequestParams();
            if (requestParams.Component == StiComponentType.Viewer && requestParams.Action != StiAction.Undefined)
            {
                StiWebActionResult result = null;
                if (report == null && requestParams.Action != StiAction.OpenReport && requestParams.Action != StiAction.Resource)
                    report = GetReportObject(requestParams);

                switch (requestParams.Action)
                {
                    case StiAction.Resource:
                        result = StiViewerResourcesHelper.Get(requestParams);
                        return new StiMvcActionResult(result.Data, result.ContentType, result.FileName, false, true);
                        
                    case StiAction.OpenReport:
                        result = StiReportHelper.OpenReportResult(requestParams);
                        break;

                    case StiAction.PrintReport:
                        return PrintReportResult(requestParams, report);

                    case StiAction.ExportReport:
                        return ExportReportResult(requestParams, report);

                    case StiAction.InitVars:
                    case StiAction.Variables:
                    case StiAction.Sorting:
                    case StiAction.DrillDown:
                    case StiAction.Collapsing:
                    case StiAction.RefreshReport:
                        return InteractionResult(requestParams, report);

                    case StiAction.UpdateCache:
                        requestParams.Cache.Helper.UpdateObjectCacheInternal(requestParams, null);
                        break;
                }

                if (result == null) result = StiReportHelper.ViewerResult(requestParams, report);
                bool isResourceAction = requestParams.Action == StiAction.Resource;
                return new StiMvcActionResult(result.Data, result.ContentType, result.FileName, !string.IsNullOrEmpty(result.FileName) && !isResourceAction, isResourceAction);
            }

            return new ViewResult();
        }
        #endregion


        #region Obsolete

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportSnapshotResult(StiReport report)
        {
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult GetReportSnapshotResult(HttpContextBase httpContext, StiReport report)
        {
            return GetReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the InteractionResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult InteractionResult(HttpContextBase httpContext)
        {
            return InteractionResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the PrintReportResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult PrintReportResult(HttpContextBase httpContext)
        {
            return PrintReportResult();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the PrintReportResult(report) method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult PrintReportResult(HttpContextBase httpContext, StiReport report)
        {
            return PrintReportResult(report);
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the ViewerEventResult() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ActionResult ViewerEventResult(HttpContextBase httpContext)
        {
            return ViewerEventResult();
        }

        #endregion
    }
}
