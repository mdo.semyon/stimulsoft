﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Web.UI.WebControls;
using Stimulsoft.Report.Web;
using System.Collections.Specialized;
using System.Web.Routing;
using Stimulsoft.Report.Export;
using System.ComponentModel;
using System.Web;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcViewer : Panel
    {
        #region Request Params
        /// <summary>
        /// Get the all request parameters of the report viewer.
        /// </summary>
        public static StiRequestParams GetRequestParams()
        {
            StiRequestParams requestParams = StiRequestParamsHelper.Get();
            requestParams.Cache.Helper = CacheHelper;
            return requestParams;
        }
        #endregion

        #region Form and Route Values
        /// <summary>
        /// Get the POST form values for the start page of the report viewer.
        /// </summary>
        public static NameValueCollection GetFormValues()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetFormValues(requestParams);
        }

        /// <summary>
        /// Get the POST form values for the start page of the report viewer. The specified request parameters will be used.
        /// </summary>
        public static NameValueCollection GetFormValues(StiRequestParams requestParams)
        {
            return requestParams.FormValues;
        }

        /// <summary>
        /// Get the route values for the start page of the report viewer.
        /// </summary>
        public static RouteValueDictionary GetRouteValues()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetRouteValues(requestParams);
        }

        /// <summary>
        /// Get the route values for the start page of the report viewer. The specified request parameters will be used.
        /// </summary>
        public static RouteValueDictionary GetRouteValues(StiRequestParams requestParams)
        {
            var routeValues = new RouteValueDictionary();
            if (requestParams.Routes != null)
            {
                foreach (string key in requestParams.Routes.Keys)
                {
                    routeValues.Add(key, requestParams.Routes[key]);
                }
            }

            return routeValues;
        }
        #endregion

        #region Report
        /// <summary>
        /// Get the report template or report snapshot from the cache.
        /// </summary>
        public static StiReport GetReportObject()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetReportObject(requestParams);
        }

        /// <summary>
        /// Get the report template or report snapshot from the cache. The specified request parameters will be used.
        /// </summary>
        public static StiReport GetReportObject(StiRequestParams requestParams)
        {
            return requestParams.Cache.Helper.GetReportInternal(requestParams);
        }
        #endregion

        #region Export Settings
        /// <summary>
        /// Get the export settings from the dialog form of the report viewer.
        /// </summary>
        public static StiExportSettings GetExportSettings()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetExportSettings(requestParams);
        }

        /// <summary>
        /// Get the export settings from the dialog form of the report viewer. The specified request parameters will be used.
        /// </summary>
        public static StiExportSettings GetExportSettings(StiRequestParams requestParams)
        {
            return StiExportsHelper.GetExportSettings(requestParams);
        }
        #endregion

        #region Email Options
        /// <summary>
        /// Get the Email options from the dialog, sent by the client.
        /// </summary>
        public static StiEmailOptions GetEmailOptions()
        {
            StiRequestParams requestParams = GetRequestParams();
            return GetEmailOptions(requestParams);
        }

        /// <summary>
        /// Get the Email options from the dialog, sent by the client. The specified request parameters will be used.
        /// </summary>
        public static StiEmailOptions GetEmailOptions(StiRequestParams requestParams)
        {
            return StiWebViewer.GetEmailOptions(requestParams);
        }
        #endregion


        #region Obsolete

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportObject() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static StiReport GetReportObject(HttpContextBase httpContext)
        {
            return GetReportObject();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetReportObject() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static StiReport GetReportObject(HttpContextBase httpContext, bool isDrillDownReport)
        {
            return GetReportObject();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetRouteValues() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static RouteValueDictionary GetRouteValues(HttpContextBase httpContext)
        {
            return GetRouteValues();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetFormValues() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static NameValueCollection GetFormValues(HttpContextBase httpContext)
        {
            return GetFormValues();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetRequestParams() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static StiRequestParams GetViewerParameters()
        {
            return GetRequestParams();
        }

        [Obsolete("This method is obsolete. It will be removed in next versions. Please use the GetRequestParams() method instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static StiRequestParams GetViewerParameters(HttpContextBase httpContext)
        {
            return GetRequestParams();
        }

        #endregion
    }
}
