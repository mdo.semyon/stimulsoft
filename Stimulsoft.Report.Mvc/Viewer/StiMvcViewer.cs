﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Web.UI.WebControls;
using System.Web.Mvc;
using System;
using System.Web;
using System.Web.Routing;
using Stimulsoft.Report.Web;
using Stimulsoft.Base;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcViewer : Panel
    {
        #region Fields

        private HtmlHelper htmlHelper;
        private StiMvcViewerOptions options;

        #endregion

        #region Properties

        private static StiCacheHelper cacheHelper = null;
        /// <summary>
        /// Gets or sets an instance of the StiCacheHelper class that will be used for report caching on the server side.
        /// </summary>
        public static StiCacheHelper CacheHelper
        {
            get
            {
                if (cacheHelper == null) cacheHelper = new StiCacheHelper();
                return cacheHelper;
            }
            set
            {
                cacheHelper = value;
            }
        }

        private string clientGuid = null;
        internal string ClientGuid
        {
            get
            {
                if (clientGuid == null) clientGuid = StiGuidUtils.NewGuid();
                return clientGuid;
            }
            set
            {
                clientGuid = value;
            }
        }

        private bool reportDesignerMode = false;
        internal bool ReportDesignerMode
        {
            get
            {
                return reportDesignerMode;
            }
            set
            {
                reportDesignerMode = value;
            }
        }

        #endregion

        #region Internal

        /// <summary>
        /// Get the URL for the viewer requests.
        /// </summary>
        internal static string GetRequestUrl(HtmlHelper htmlHelper, string template, string controller, bool useRelativeUrls, bool passQueryParameters)
        {
            var result = string.Empty;
            if (!useRelativeUrls)
            {
                Uri url = HttpContext.Current.Request.Url;

                // Get port number
                if (HttpContext.Current.Request.Headers != null &&
                    HttpContext.Current.Request.Headers.Count > 0 &&
                    !string.IsNullOrEmpty(HttpContext.Current.Request.Headers["Host"]))
                {
                    string[] values = HttpContext.Current.Request.Headers["Host"].Split(':');
                    if (values.Length > 1)
                    {
                        UriBuilder builder = new UriBuilder(url);
                        int port;
                        if (int.TryParse(values[1], out port)) builder.Port = port;
                        url = builder.Uri;
                    }
                }

                result = url.GetLeftPart(UriPartial.Authority);
                if (HttpContext.Current.Request.IsSecureConnection) result = result.Replace("http:", "https:");
            }

            result += htmlHelper.ViewContext.HttpContext.Request.ApplicationPath;

            if (!string.IsNullOrEmpty(template))
            {
                if (template.StartsWith("/")) template = template.Substring(1);
                while (template.EndsWith("/")) template = template.Substring(0, template.Length - 1);
                if (template.IndexOf("{action}") < 0) template = template + "/{action}";

                if (!result.EndsWith("/")) result += "/";
                result = result + template;
            }
            else
            {
                string[] routes = ((Route)htmlHelper.ViewContext.RouteData.Route).Url.Split('/');
                foreach (string part in routes)
                {
                    if (!result.EndsWith("/")) result += "/";

                    var name = GetRouteParameterName(part);
                    if (name == "controller" && !string.IsNullOrEmpty(controller)) result += part.Replace("{controller}", controller);
                    else if (name == "action") result += part;
                    else if (name != null)
                    {
                        var value = htmlHelper.ViewContext.RouteData.Values[name];
                        if (value != null) result += part.Replace("{" + name + "}", value.ToString());
                    }
                    else if (part != null) result += part;
                }
            }

            while (result.EndsWith("/")) result = result.Substring(0, result.Length - 1);

            if (passQueryParameters)
            {
                string query = htmlHelper.ViewContext.HttpContext.Request.QueryString.ToString();
                if (!string.IsNullOrEmpty(query)) result += "?" + query;
            }

            return result;
        }

        private static string GetRouteParameterName(string part)
        {
            if (string.IsNullOrEmpty(part)) return null;
            if (part.IndexOf("{") < 0 || part.IndexOf("}") < 0) return null;
            return part.Substring(part.IndexOf("{") + 1, part.IndexOf("}") - part.IndexOf("{") - 1);
        }

        #endregion

        public StiMvcViewer(HtmlHelper htmlHelper, string viewerId, StiMvcViewerOptions options)
        {
            StiOptions.Configuration.IsWeb = true;

            this.htmlHelper = htmlHelper;
            this.ID = viewerId;
            this.options = options;
        }
    }
}
