﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections;
using System.Security.Cryptography;
using Stimulsoft.Base.Json;
using Stimulsoft.Base;
using Stimulsoft.Base.Localization;
using System.Web;
using Stimulsoft.Base.Json.Serialization;
using Stimulsoft.Base.Json.Converters;
using System.Text;
using Stimulsoft.Base.Licenses;
using Stimulsoft.Report.Web;

namespace Stimulsoft.Report.Mvc
{
    public partial class StiMvcViewer : Panel
    {
        protected override void Render(HtmlTextWriter writer)
        {
            CreateChildControls();

            base.Render(writer);
        }

        private string RenderJsonParameters()
        {
            #region Routes

            var routes = new Hashtable();
            foreach (string key in htmlHelper.ViewContext.RouteData.Values.Keys) routes[key] = htmlHelper.ViewContext.RouteData.Values[key];
            string jsRoutes = JsonConvert.SerializeObject(routes, Formatting.None);

            #endregion

            #region Form Values

            var formValues = new Hashtable();
            if (options.Server.PassFormValues)
            {
                foreach (string key in htmlHelper.ViewContext.HttpContext.Request.Form.Keys)
                {
                    formValues[key] = htmlHelper.ViewContext.HttpContext.Request.Form[key];
                }
            }

            #endregion

            var productVersion = StiVersionHelper.ProductVersion.Trim();

            #region LicenseKey
            var licenseKey = StiLicenseKeyValidator.GetLicenseKey();
            var isTrial = !StiLicenseKeyValidator.IsValid(StiProductIdent.Web, licenseKey);
            if (!typeof(StiLicense).AssemblyQualifiedName.Contains(StiPublicKeyToken.Key))isTrial = true;

            #region IsValidLicenseKey
            if (!isTrial)
            {
                try
                {
                    using (var rsa = new RSACryptoServiceProvider(512))
                    using (var sha = new SHA1CryptoServiceProvider())
                    {
                        rsa.FromXmlString("<RSAKeyValue><Modulus>iyWINuM1TmfC9bdSA3uVpBG6cAoOakVOt+juHTCw/gxz/wQ9YZ+Dd9vzlMTFde6HAWD9DC1IvshHeyJSp8p4H3qXUKSC8n4oIn4KbrcxyLTy17l8Qpi0E3M+CI9zQEPXA6Y1Tg+8GVtJNVziSmitzZddpMFVr+6q8CRi5sQTiTs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");
                        isTrial = !rsa.VerifyData(licenseKey.GetCheckBytes(), sha, licenseKey.GetSignatureBytes());
                    }
                }
                catch (Exception)
                {
                    isTrial = true;
                }
            }
            #endregion

            if (!isTrial) productVersion += " ";
            #endregion

            if (ReportDesignerMode) options.Server.AllowAutoUpdateCache = false;

            var jsOptions = new Hashtable();
            jsOptions["viewerId"] = this.ID;
            jsOptions["theme"] = options.Theme;
            jsOptions["clientGuid"] = this.ClientGuid;
            jsOptions["requestAbsoluteUrl"] = GetRequestUrl(htmlHelper, options.Server.RouteTemplate, options.Server.Controller, false, true);
            jsOptions["requestUrl"] = GetRequestUrl(htmlHelper, options.Server.RouteTemplate, options.Server.Controller, options.Server.UseRelativeUrls, true);
            jsOptions["requestStylesUrl"] = GetRequestUrl(htmlHelper, options.Server.RouteTemplate, options.Server.Controller, options.Server.UseRelativeUrls, options.Server.PassQueryParametersForResources);
            jsOptions["routes"] = routes;
            jsOptions["formValues"] = formValues;
            jsOptions["cultureName"] = StiLocalization.CultureName;
            jsOptions["shortProductVersion"] = StiVersionHelper.AssemblyVersion;
            jsOptions["productVersion"] = productVersion;
            jsOptions["viewerHeightType"] = this.Height.Type.ToString();
            jsOptions["reportDesignerMode"] = this.ReportDesignerMode;
            jsOptions["frameworkType"] = "ASP.NET MVC";

            jsOptions["actions"] = options.Actions;
            jsOptions["server"] = options.Server;
            jsOptions["appearance"] = options.Appearance;
            jsOptions["toolbar"] = options.Toolbar;
            jsOptions["exports"] = options.Exports;
            jsOptions["email"] = options.Email;

            string jsonOptions = JsonConvert.SerializeObject(jsOptions, Formatting.None,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = { new StringEnumConverter() }
                });

            string jsonDefaultExportSettings = JsonConvert.SerializeObject(
                StiExportsHelper.GetDefaultExportSettings(options.Exports.DefaultSettings),
                Formatting.None, new StringEnumConverter());

            string jsonScriptsUrl = JsonConvert.SerializeObject(ReportDesignerMode ? this.GetScriptsUrl() : null);

            return string.Format(
                "{{options:{0},defaultExportSettings:{1},scriptsUrl:{2}}}",
                jsonOptions, jsonDefaultExportSettings, jsonScriptsUrl);
        }

        private string GetScriptsUrl()
        {
            string localizationBase64 = string.IsNullOrEmpty(options.Localization) ? null : Convert.ToBase64String(Encoding.UTF8.GetBytes(options.Localization));
            string requestUrl = GetRequestUrl(htmlHelper, options.Server.RouteTemplate, options.Server.Controller, options.Server.UseRelativeUrls, options.Server.PassQueryParametersForResources);

            string scriptUrl = requestUrl.Replace("{action}", options.Actions.ViewerEvent) + (requestUrl.IndexOf("?") > 0 ? "&" : "?");
            scriptUrl += "stiweb_component=Viewer&stiweb_action=Resource&stiweb_data=scripts&stiweb_theme=" + options.Theme.ToString();
            if (!string.IsNullOrEmpty(localizationBase64)) scriptUrl += "&stiweb_loc=" + HttpUtility.UrlEncode(localizationBase64);
            scriptUrl += "&stiweb_cachemode=" + (options.Server.UseCacheForResources
                ? options.Server.CacheMode == StiServerCacheMode.ObjectSession || options.Server.CacheMode == StiServerCacheMode.StringSession
                    ? "session"
                    : "cache"
                : "none");
            scriptUrl += "&stiweb_version=" + StiVersionHelper.AssemblyVersion;
            
            return scriptUrl;
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            this.Width = options.Width;
            this.Height = options.Height.IsEmpty ? (options.Appearance.ScrollbarsMode ? Unit.Pixel(650) : Unit.Percentage(100)) : options.Height;
            this.BackColor = options.Appearance.BackgroundColor;

            #region Render Control

            Panel mainPanel = new Panel();
            mainPanel.CssClass = "stiJsViewerMainPanel";
            mainPanel.ID = this.ID + "_JsViewerMainPanel";
            this.Controls.Add(mainPanel);

            string jsParameters = RenderJsonParameters();
            if (ReportDesignerMode)
            {
                StiJavaScript scriptInit = new StiJavaScript();
                scriptInit.Text = string.Format("var js{0}Parameters = {1};", this.ID, jsParameters);
                mainPanel.Controls.Add(scriptInit);
            }
            else
            {
                StiJavaScript scriptEngine = new StiJavaScript();
                scriptEngine.ScriptUrl = GetScriptsUrl();
                mainPanel.Controls.Add(scriptEngine);

                StiJavaScript scriptInit = new StiJavaScript();
                scriptInit.Text = string.Format(
                    "var js{0} = new StiJsViewer({1}); " +
                    "if (document.readyState == 'complete') js{0}.postAction(); " +
                    "else js{0}.addEvent(window, 'load', function () {{ js{0}.postAction() }});",
                    this.ID, jsParameters);
                mainPanel.Controls.Add(scriptInit);
            }

            #endregion
        }
    }
}
