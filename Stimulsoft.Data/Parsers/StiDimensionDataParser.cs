#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;

using Stimulsoft.Data.Expressions.NCalc;
using Stimulsoft.Data.Functions;
using Stimulsoft.Data.Helpers;
using Stimulsoft.Base.Meters;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Stimulsoft.Base;

namespace Stimulsoft.Data.Parsers
{
    public class StiDimensionDataParser : StiDataParser
    {
        #region Methods
        public object[] Calculate(object[] row)
        {
            this.currentRow = row;

            return NormalizeDates(Dimensions.Select(CalculateDimension).ToArray());
        }

        private static object[] NormalizeDates(object[] values)
        {
            for (var index = 0; index < values.Length; index++)
            {
                if (!(values[index] is DateTime)) continue;

                values[index] = ((DateTime)values[index]).Date;
            }
            return values;
        }

        private object CalculateDimension(IStiDimensionMeter dimension)
        {
            if (string.IsNullOrWhiteSpace(dimension.Expression)) return null;

            var columnName = GetDimensionGroupColumn(dimension);
            if (columnName == null)
                return CalculateDimensionExpression(dimension);
            else
                return CalculateDimensionGroup(columnName);
        }

        private string GetDimensionGroupColumn(IStiDimensionMeter dimension)
        {
            if (string.IsNullOrWhiteSpace(dimension.Expression)) return null;

            if (expressionToColumn.ContainsKey(dimension.Expression))
                return expressionToColumn[dimension.Expression];

            var expression = Funcs.ToDataName(dimension.Expression);

            var column = Table.Columns
                .Cast<DataColumn>()
                .FirstOrDefault(c => Funcs.ToDataName(c.ColumnName) == expression)?
                .ColumnName;

            expressionToColumn.Add(dimension.Expression, column);

            return column;
        }

        private object CalculateDimensionExpression(IStiDimensionMeter dimension)
        {
            try
            {
                var e = GetExpression(dimension.Expression);

                return e.Evaluate();
            }
            catch
            {
                return null;
            }
        }

        private object CalculateDimensionGroup(string columnName)
        {
            var columnIndex = GetDataColumnIndex(columnName);

            return currentRow?[columnIndex];
        }

        private Expression GetExpression(string expression)
        {
            if (queryToExpression.ContainsKey(expression))
                return queryToExpression[expression];

            var expObject = StiExpressionHelper.NewExpression(expression);

            expObject.EvaluateFunction += (name, args) => args.Result = RunFunction(name, args);
            expObject.EvaluateParameter += delegate(string name, ParameterArgs args)
            {
                if (IsSystemVariable(name))
                    args.Result = GetSystemVariableValue(name);
                else if (IsVariable(name))
                    args.Result = GetVariableValue(name);
                else
                    args.Result = currentRow[GetDataColumnIndex(name)];
            };

            queryToExpression[expression] = expObject;

            return expObject;
        }
        #endregion

        #region Properties
        protected IEnumerable<IStiDimensionMeter> Dimensions { get; }
        #endregion

        #region Fields
        private object[] currentRow;
        private Dictionary<string, Expression> queryToExpression = new Dictionary<string, Expression>();
        private Dictionary<string, string> expressionToColumn = new Dictionary<string, string>();
        #endregion

        public StiDimensionDataParser(IStiAppDictionary dictionary, DataTable table, List<IStiMeter> meters) 
            : base(dictionary, table, meters)
        {
            this.Dimensions = Meters.Where(m => m is IStiDimensionMeter).Cast<IStiDimensionMeter>();
    }
    }
}