#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;

using Stimulsoft.Base.Helpers;
using Stimulsoft.Data.Exceptions;
using Stimulsoft.Data.Expressions.NCalc;
using Stimulsoft.Data.Functions;
using Stimulsoft.Base.Meters;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Stimulsoft.Base;

namespace Stimulsoft.Data.Parsers
{
    public abstract class StiDataParser
    {
        #region Methods
        protected object RunFunction(string name, FunctionArgs args)
        {
            var lowerName = Funcs.ToLowerCase(name);

            switch (lowerName)
            {
                case "all":
                    return Funcs.All(GetDataColumnFromArg0(name, args));

                #region Avg
                case "avg":
                    return Funcs.Avg(GetDataColumnFromArg0(name, args));

                case "avgd":
                    return Funcs.AvgD(GetDataColumnFromArg0(name, args));

                case "avgi":
                    return Funcs.AvgI(GetDataColumnFromArg0(name, args));

                case "avgdate":
                    return Funcs.AvgDate(GetDataColumnFromArg0(name, args));

                case "avgtime":
                    return Funcs.AvgTime(GetDataColumnFromArg0(name, args));
                #endregion

                case "count":
                    return Funcs.Count(GetDataColumnFromArg0(name, args));

                case "distinct":
                    return Funcs.Distinct(GetDataColumnFromArg0(name, args));

                case "distinctcount":
                    return Funcs.DistinctCount(GetDataColumnFromArg0(name, args));

                case "grandtotal":
                    {
                        IsGrandTotal = true;
                        var result = Funcs.Sum(GetDataColumnFromArg0(name, args));
                        IsGrandTotal = false;
                        return result;
                    }

                case "first":
                    return Funcs.First(GetDataColumnFromArg0(name, args));

                case "last":
                    return Funcs.Last(GetDataColumnFromArg0(name, args));

                case "median":
                    return Funcs.Median(GetDataColumnFromArg0(name, args));

                #region Max
                case "max":
                    return Funcs.Max(GetDataColumnFromArg0(name, args));

                case "maxd":
                    return Funcs.MaxD(GetDataColumnFromArg0(name, args));

                case "maxi":
                    return Funcs.MaxI(GetDataColumnFromArg0(name, args));

                case "maxdate":
                    return Funcs.MaxDate(GetDataColumnFromArg0(name, args));

                case "maxtime":
                    return Funcs.MaxTime(GetDataColumnFromArg0(name, args));

                case "maxstr":
                    return Funcs.MaxStr(GetDataColumnFromArg0(name, args));
                #endregion

                #region Min
                case "min":
                    return Funcs.Min(GetDataColumnFromArg0(name, args));

                case "mind":
                    return Funcs.MinD(GetDataColumnFromArg0(name, args));

                case "mini":
                    return Funcs.MinI(GetDataColumnFromArg0(name, args));

                case "mindate":
                    return Funcs.MinDate(GetDataColumnFromArg0(name, args));

                case "mintime":
                    return Funcs.MinTime(GetDataColumnFromArg0(name, args));

                case "minstr":
                    return Funcs.MinStr(GetDataColumnFromArg0(name, args));
                #endregion

                #region Sum
                case "sum":
                    return Funcs.Sum(GetDataColumnFromArg0(name, args));

                case "sumd":
                    return Funcs.SumD(GetDataColumnFromArg0(name, args));

                case "sumi":
                    return Funcs.SumI(GetDataColumnFromArg0(name, args));

                case "sumdistinct":
                    return Funcs.SumDistinct(GetDataColumnFromArg0(name, args));

                case "sumtime":
                    return Funcs.SumTime(GetDataColumnFromArg0(name, args));
                #endregion

                #region Date
                case "addmonths":
                    {
                        var arg0 = GetObjectFromArg(0, name, "date", args);
                        var arg1 = StiValueHelper.TryToInt(GetObjectFromArg(1, name, "months", args));

                        return Funcs.AddMonthsObject(arg0, arg1);
                    }

                case "addyear":
                    {
                        var arg0 = GetObjectFromArg(0, name, "date", args);
                        var arg1 = StiValueHelper.TryToInt(GetObjectFromArg(1, name, "years", args));

                        return Funcs.AddYearsObject(arg0, arg1);
                    }

                case "datediff":
                    return Funcs.DateDiffObject(GetObjectFromArg0(name, "date1", args), GetObjectFromArg1(name, "date2", args));

                case "day":
                    return Funcs.DayObject(GetObjectFromArg0(name, "date", args));

                case "daysinmonth":
                    {
                        if (args.Parameters.Length == 1)
                            return Funcs.DaysInMonthObject(GetObjectFromArg0(name, "date", args));
                        else
                            return Funcs.DaysInMonthObject(GetObjectFromArg0(name, "year", args), GetObjectFromArg1(name, "month", args));
                    }

                case "daysinyear":
                    {
                        var value = GetObjectFromArg0(name, "value", args);
                        if (value is DateTime)
                            return Funcs.DaysInYearObject((DateTime)value);
                        else
                            return Funcs.DaysInYearObject(StiValueHelper.TryToInt(value));
                    }

                case "dayofweek":
                {
                    var arg0 = GetObjectFromArg0(name, "date", args);

                    if (args.Parameters.Length == 1)
                        return Funcs.DayOfWeekObject(arg0);

                    else if (args.Parameters.Length == 2)
                    {
                        var arg1 = GetObjectFromArg1(name, "localized", args);

                        if (arg1 is bool)
                            return Funcs.DayOfWeekObject(arg0, (bool) arg1);

                        else
                            return Funcs.DayOfWeekObject(arg0, arg1.ToString());
                    }
                    else if (args.Parameters.Length == 3)
                    {
                        var arg1 = GetObjectFromArg1(name, "culture", args);
                        var arg2 = GetObjectFromArg2(name, "upperCase", args);

                        return Funcs.DayOfWeekObject(arg0, arg1.ToString(), arg2 is bool && (bool) arg2);
                    }
                    else throw new StiArgumentCountException(name);
                }

                case "dayofweekident":
                    return Funcs.DayOfWeekIdentObject(GetObjectFromArg0(name, "date", args));

                case "dayofweekindex":
                    return Funcs.DayOfWeekIndexObject(GetObjectFromArg0(name, "date", args));

                case "dayofyear":
                    return Funcs.DayOfYearObject(GetObjectFromArg0(name, "date", args));

                case "financialquarter":
                    return Funcs.FinancialQuarterObject(GetObjectFromArg0(name, "date", args));

                case "financialquarterindex":
                    return Funcs.FinancialQuarterIndexObject(GetObjectFromArg0(name, "date", args));

                case "hour":
                    return Funcs.HourObject(GetObjectFromArg0(name, "date", args));

                case "makedate":
                case "dateserial":
                    {
                        var arg0 = GetObjectFromArg(0, name, "year", args);
                        var arg1 = GetObjectFromArg(1, name, "months", args);
                        var arg2 = GetObjectFromArg(2, name, "day", args);

                        return Funcs.MakeDateObject(arg0, arg1, arg2);
                    }

                case "makedatetime":
                    {
                        var arg0 = GetObjectFromArg(0, name, "year", args);
                        var arg1 = GetObjectFromArg(1, name, "months", args);
                        var arg2 = GetObjectFromArg(2, name, "day", args);
                        var arg3 = GetObjectFromArg(3, name, "hour", args);
                        var arg4 = GetObjectFromArg(4, name, "minute", args);
                        var arg5 = GetObjectFromArg(5, name, "second", args);

                        return Funcs.MakeDateTimeObject(arg0, arg1, arg2, arg3, arg4, arg5);
                    }

                case "maketime":
                case "timeserial":
                    {
                        var arg0 = GetObjectFromArg(0, name, "hour", args);
                        var arg1 = GetObjectFromArg(1, name, "minute", args);
                        var arg2 = GetObjectFromArg(2, name, "second", args);

                        return Funcs.MakeTimeObject(arg0, arg1, arg2);
                    }

                case "minute":
                    return Funcs.MinuteObject(GetObjectFromArg0(name, "date", args));

                case "month":
                    return Funcs.MonthObject(GetObjectFromArg0(name, "date", args));

                case "monthident":
                    return Funcs.MonthIdentObject(GetObjectFromArg0(name, "date", args));

                case "monthname":
                {
                    var arg0 = GetObjectFromArg0(name, "date", args);

                    if (args.Parameters.Length == 1)
                        return Funcs.MonthNameObject(arg0);

                    else if (args.Parameters.Length == 2)
                    {
                        var arg1 = GetObjectFromArg1(name, "localized", args);

                        if (arg1 is bool)
                            return Funcs.MonthNameObject(arg0, (bool)arg1);

                        else
                            return Funcs.MonthNameObject(arg0, arg1.ToString());
                    }
                    else if (args.Parameters.Length == 3)
                    {
                        var arg1 = GetObjectFromArg1(name, "culture", args);
                        var arg2 = GetObjectFromArg2(name, "upperCase", args);

                        return Funcs.MonthNameObject(arg0, arg1.ToString(), arg2 is bool && (bool)arg2);
                    }
                    else throw new StiArgumentCountException(name);
                }

                case "now":
                    return Funcs.Now();

                case "quarter":
                    return Funcs.QuarterObject(GetObjectFromArg0(name, "date", args));

                case "quarterindex":
                    return Funcs.QuarterIndexObject(GetObjectFromArg0(name, "date", args));

                case "second":
                    return Funcs.SecondObject(GetObjectFromArg0(name, "date", args));

                case "year":
                    return Funcs.YearObject(GetObjectFromArg0(name, "date", args));

                case "yearmonth":
                    return Funcs.YearMonthObject(GetObjectFromArg0(name, "date", args));
                #endregion

                #region String
                case "insert":
                    {
                        var arg0 = GetObjectFromArg0(name, "str", args);
                        var arg1 = StiValueHelper.TryToInt(GetObjectFromArg(1, "startIndex", name, args));
                        var arg2 = StiValueHelper.TryToString(GetObjectFromArg(2, "value", name, args));
                        return Funcs.InsertObject(arg0, arg1, arg2);
                    }

                case "left":
                    {
                        var arg0 = GetObjectFromArg0(name, "str", args);
                        var arg1 = StiValueHelper.TryToInt(GetObjectFromArg(1, "length", name, args));
                        return Funcs.LeftObject(arg0, arg1);
                    }

                case "length":
                    return Funcs.LengthObject(GetObjectFromArg0(name, "str", args));

                case "remove":
                    {
                        var arg0 = GetObjectFromArg0(name, "str", args);
                        var arg1 = StiValueHelper.TryToInt(GetObjectFromArg(1, name, "startIndex", args));
                        var arg2 = StiValueHelper.TryToInt(GetObjectFromArg(2, name, "count", args));
                        return Funcs.RemoveObject(arg0, arg1, arg2);
                    }

                case "replace":
                    {
                        var arg0 = GetObjectFromArg0(name, "str", args);
                        var arg1 = StiValueHelper.TryToString(GetObjectFromArg(1, name, "oldValue", args));
                        var arg2 = StiValueHelper.TryToString(GetObjectFromArg(2, name, "newValue", args));
                        return Funcs.ReplaceObject(arg0, arg1, arg2);
                    }

                case "right":
                    {
                        var arg0 = GetObjectFromArg0(name, "str", args);
                        var arg1 = StiValueHelper.TryToInt(GetObjectFromArg(1, "length", name, args));
                        return Funcs.RightObject(arg0, arg1);
                    }

                case "topropercase":
                    return Funcs.ToProperCaseObject(GetObjectFromArg0(name, "str", args));

                case "tolowercase":
                    return Funcs.ToLowerCaseObject(GetObjectFromArg0(name, "str", args));

                case "tostring":
                    return Funcs.ToStringObject(GetObjectFromArg0(name, "value", args));

                case "touppercase":
                    return Funcs.ToUpperCaseObject(GetObjectFromArg0(name, "str", args));

                case "substring":
                    {
                        var arg0 = GetObjectFromArg0(name, "str", args);
                        var arg1 = StiValueHelper.TryToInt(GetObjectFromArg(1, "index", name, args));
                        var arg2 = args.Parameters.Length > 2 ? StiValueHelper.TryToInt(GetObjectFromArg(2, "length", name, args)) : -1;

                        return Funcs.SubstringObject(arg0, arg1, arg2);
                    }

                case "trim":
                    return Funcs.TrimObject(GetObjectFromArg0(name, "str", args));

                case "trimstart":
                    return Funcs.TrimStartObject(GetObjectFromArg0(name, "str", args));

                case "trimend":
                    return Funcs.TrimEndObject(GetObjectFromArg0(name, "str", args));
                #endregion

                #region Math
                case "abs":
                    return Funcs.AbsObject(GetObjectFromArg0(name, "value", args));

                case "acos":
                    return Funcs.AcosObject(GetObjectFromArg0(name, "value", args));

                case "asin":
                    return Funcs.AsinObject(GetObjectFromArg0(name, "value", args));

                case "atan":
                    return Funcs.AtanObject(GetObjectFromArg0(name, "value", args));

                case "ceiling":
                    return Funcs.CeilingObject(GetObjectFromArg0(name, "value", args));

                case "cos":
                    return Funcs.CosObject(GetObjectFromArg0(name, "value", args));

                case "div":
                    return Funcs.DivObject(GetObjectFromArg(0, name, "value1", args), GetObjectFromArg(0, name, "value2", args), GetObjectFromArg(2, name, "zeroResult", args));

                case "exp":
                    return Funcs.ExpObject(GetObjectFromArg0(name, "value", args));

                case "floor":
                    return Funcs.FloorObject(GetObjectFromArg0(name, "value", args));

                case "log":
                    return Funcs.LogObject(GetObjectFromArg0(name, "value", args));

                case "round":
                    return Funcs.RoundObject(GetObjectFromArg0(name, "value", args));

                case "sign":
                    return Funcs.SignObject(GetObjectFromArg0(name, "value", args));

                case "sin":
                    return Funcs.SinObject(GetObjectFromArg0(name, "value", args));

                case "sqrt":
                    return Funcs.SqrtObject(GetObjectFromArg0(name, "value", args));

                case "tan":
                    return Funcs.TanObject(GetObjectFromArg0(name, "value", args));

                case "truncate":
                    return Funcs.TruncateObject(GetObjectFromArg0(name, "value", args));
                #endregion

                default:
                    throw new StiFunctionNotFoundException(name);
            }
        }

        protected IStiAppVariable GetVariable(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return null;

            if (nameToVariable.ContainsKey(name))
                return nameToVariable[name];

            var variable = Dictionary?.GetVariableByName(name);

            nameToVariable[name] = variable;

            return variable;
        }

        protected bool IsVariable(string name)
        {
            return GetVariable(name) != null;
        }

        protected object GetVariableValue(string name)
        {
            var variable = GetVariable(name);
            return variable?.GetValue();
        }

        protected bool IsSystemVariable(string name)
        {
            return Dictionary.IsSystemVariable(name);
        }

        protected object GetSystemVariableValue(string name)
        {
            return Dictionary.GetSystemVariableValue(name);
        }

        private static object GetObjectFromArg(int argIndex, string argName, string funcName, FunctionArgs args)
        {
            if (argIndex >= args.Parameters.Length)
                throw new StiArgumentNotFoundException(funcName, argName);

            return args.Parameters[argIndex].Evaluate();
        }

        private static object GetObjectFromArg0(string argName, string funcName, FunctionArgs args)
        {
            return GetObjectFromArg(0, argName, funcName, args);
        }

        private static object GetObjectFromArg1(string argName, string funcName, FunctionArgs args)
        {
            return GetObjectFromArg(1, argName, funcName, args);
        }

        private static object GetObjectFromArg2(string argName, string funcName, FunctionArgs args)
        {
            return GetObjectFromArg(2, argName, funcName, args);
        }

        private static object GetDataColumnFromArg0(string funcName, FunctionArgs args)
        {
            return GetObjectFromArg0("dataColumn", funcName, args);
        }

        protected int GetDataColumnIndex(string columnName)
        {
            if (nameToIndex.ContainsKey(columnName))
                return nameToIndex[columnName];

            var dataName = Funcs.ToDataName(columnName);

            var dataColumn = Table.Columns.Cast<DataColumn>()
                .FirstOrDefault(c => DataEqual(c, dataName));

            if (dataColumn == null)
                throw new StiColumnNotFoundException(columnName);

            var dataColumnIndex = Table.Columns.IndexOf(dataColumn);
            if (dataColumnIndex == -1)
                throw new StiColumnNotFoundException(columnName);

            nameToIndex.Add(columnName, dataColumnIndex);

            return dataColumnIndex;
        }

        private bool DataEqual(DataColumn dataColumn, string searchColumnName)
        {
            var dataColumnName = Funcs.ToDataName(dataColumn.ColumnName);
            if (!searchColumnName.Contains(".")) return false;

            return dataColumnName == searchColumnName;
        }

        protected int GetDimensionIndex(IStiDimensionMeter dimension)
        {
            return Meters
                .Where(m => m is IStiDimensionMeter)
                .ToList()
                .IndexOf(dimension);
        }
        #endregion

        #region Properties
        protected IStiAppDictionary Dictionary { get; }

        protected DataTable Table { get; }

        protected List<IStiMeter> Meters { get; }

        protected bool IsGrandTotal { get; set; }//It is grand measure function now is processed
        #endregion

        #region Fields
        private Dictionary<string, int> nameToIndex = new Dictionary<string, int>();
        private Dictionary<string, IStiAppVariable> nameToVariable = new Dictionary<string, IStiAppVariable>();
        #endregion

        protected StiDataParser(IStiAppDictionary dictionary, DataTable table, List<IStiMeter> meters)
        {
            this.Dictionary = dictionary;
            this.Table = table;
            this.Meters = meters;
        }
    }
}