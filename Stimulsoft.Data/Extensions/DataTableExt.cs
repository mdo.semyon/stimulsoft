#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Stimulsoft.Data.Extensions
{
    public static class DataTableExt
    {
        public static string GetUniqueName(this DataTable table, string baseName)
        {
            if (baseName == null) return null;

            var name = baseName;
            var index = 2;

            while (table.Columns.Contains(name))
            {
                name = baseName + index++;
            }
            return name;
        }

        public static IEnumerable<DataRelation> ParentRelationList(this DataTable table)
        {
            return table.ParentRelations.Cast<DataRelation>().ToList();
        }

        public static IEnumerable<DataRelation> ChildRelationList(this DataTable table)
        {
            return table.ChildRelations.Cast<DataRelation>().ToList();
        }
    }
}