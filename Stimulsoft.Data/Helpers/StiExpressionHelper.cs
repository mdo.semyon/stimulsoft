#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System.Collections.Generic;
using System.Linq;
using System.Text;

using Stimulsoft.Base;
using Stimulsoft.Data.Expressions.NCalc;
using Stimulsoft.Data.Expressions.NCalc.Domain;

namespace Stimulsoft.Data.Helpers
{
    public class StiExpressionHelper
    {
        #region class ArgumentExtractionVisitor
        private class ArgumentExtractionVisitor : LogicalExpressionVisitor
        {
            public HashSet<string> Parameters = new HashSet<string>();

            public override void Visit(Identifier function)
            {
                Parameters.Add(function.Name);
            }

            public override void Visit(UnaryExpression expression)
            {
            }

            public override void Visit(BinaryExpression expression)
            {
                expression.LeftExpression.Accept(this);
                expression.RightExpression.Accept(this);
            }

            public override void Visit(TernaryExpression expression)
            {
                expression.LeftExpression.Accept(this);
                expression.RightExpression.Accept(this);
                expression.MiddleExpression.Accept(this);
            }

            public override void Visit(Function function)
            {
                foreach (var expression in function.Expressions)
                {
                    expression.Accept(this);
                }
            }

            public override void Visit(LogicalExpression expression)
            {
                expression.Accept(this);
            }

            public override void Visit(ValueExpression expression)
            {
            }
        }
        #endregion

        #region Fields
        private static Dictionary<string, List<string>> expressionToArguments = new Dictionary<string, List<string>>();
        #endregion

        #region Properties
        public static IStiReportPage ReportPage { get; set; }
        #endregion

        #region Methods
        public static Expression NewExpression(string expression)
        {
            return new Expression(PrepareExpression(expression), EvaluateOptions.IgnoreCase);
        }

        public static string PrepareExpression(string expression)
        {
            expression = string.IsNullOrWhiteSpace(expression)
                ? expression
                : expression.Replace("\"", "\'");

            return EscapeExpression(expression);
        }

        public static string EscapeExpression(string expression)
        {
            if (string.IsNullOrWhiteSpace(expression))
                return expression;

            try
            {
                var tokens = new List<StiToken>();
                var lexer = new StiLexer(expression);
                while (true)
                {
                    var token = lexer.GetToken();
                    if (token == null || token.Type == StiTokenType.EOF) break;
                    tokens.Add(token);
                }

                var sb = new StringBuilder(expression);
                var offset = 0;

                for (var index = 0; index < tokens.Count - 2; index++)
                {
                    if (tokens[index].Type == StiTokenType.Ident &&
                        tokens[index + 1].Type == StiTokenType.Dot &&
                        tokens[index + 2].Type == StiTokenType.Ident)
                    {
                        if (index != 0 && index + 3 < tokens.Count && (tokens[index - 1].Type == StiTokenType.LBracket || tokens[index + 3].Type == StiTokenType.RBracket))
                            continue;

                        var pos1 = tokens[index].Index;
                        var pos2 = tokens[index + 2].Index + tokens[index + 2].Length;
                        sb.Insert(pos1 + offset, "[");
                        offset++;
                        sb.Insert(pos2 + offset, "]");
                        offset++;
                        index++;
                    }
                }

                return sb.ToString();
            }
            catch
            {
            }

            return expression;
        }

        public static string ReplaceFunction(string expression, string newFunction)
        {
            var currentFunction = GetFunction(expression);

            if (currentFunction != null)
                expression = expression.Substring(currentFunction.Length);

            else
            {
                expression = expression.Trim();

                if (!expression.StartsWith("("))
                    expression = $"({expression}";

                if (!expression.EndsWith(")"))
                    expression = $"{expression})";
            }

            return $"{newFunction}{expression}";
        }

        public static string RemoveFunction(string expression)
        {
            var currentFunction = GetFunction(expression);
            if (currentFunction != null)
                expression = expression.Substring(currentFunction.Length);

            expression = expression.Trim();

            if (expression.StartsWith("("))
                expression = expression.Substring(1);

            if (expression.EndsWith(")"))
                expression = expression.Substring(0, expression.Length - 1);

            return expression;
        }

        public static bool IsFunctionPresent(string expression)
        {
            var function = GetFunction(expression);
            return !string.IsNullOrWhiteSpace(function);
        }

        public static string GetFunction(string expression)
        {
            if (string.IsNullOrWhiteSpace(expression)) return null;

            try
            {
                var parsedExpression = Expression.Compile(PrepareExpression(expression), true);
                var parsedFunction = parsedExpression as Function;
                var functionName = parsedFunction?.Identifier?.Name;

                return functionName != null && expression.Trim().StartsWith(functionName) ? functionName : null;
            }
            catch
            {
                return null;
            }
        }

        public static List<string> GetArguments(string expression)
        {
            if (string.IsNullOrWhiteSpace(expression))
                return new List<string>();

            try
            {
                lock (expressionToArguments)
                {
                    if (expressionToArguments.ContainsKey(expression))
                        return expressionToArguments[expression];
                }

                var parsedExpression = Expression.Compile(PrepareExpression(expression), true);

                var visitor = new ArgumentExtractionVisitor();
                parsedExpression.Accept(visitor);

                var list = visitor.Parameters.ToList();

                lock (expressionToArguments)
                {
                    expressionToArguments[expression] = list;
                }

                return list;
            }
            catch
            {
            }

            return new List<string>();
        }

        public static string GetFirstArgumentFromExpression(string expression)
        {
            expression = RemoveFunction(expression);
            var args = GetArguments(expression);
            if (args == null) return null;

            var arg = args.FirstOrDefault();
            if (string.IsNullOrWhiteSpace(arg)) return null;

            return arg;
        }

        public static string ParseReportExpression(string text)
        {
            if (ReportPage != null)
                return ReportPage.ParseExpression(text);

            return text;
        }
        #endregion
    }
}