#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Helpers;
using Stimulsoft.Data.Extensions;
using System;
using System.Linq;

namespace Stimulsoft.Data.Functions
{
    public partial class Funcs
    {
        public static decimal Max(object value)
        {
            if (!ListExt.IsList(value))
                return StiValueHelper.TryToDecimal(value);

            return SkipNulls(ListExt.ToList(value))
                .TryCastToDecimal()
                .Max();
        }

        public static double MaxD(object value)
        {
            if (!ListExt.IsList(value))
                return StiValueHelper.TryToDouble(value);
            
            return SkipNulls(ListExt.ToList(value))
                .TryCastToDouble()
                .Max();
        }

        public static long MaxI(object value)
        {
            if (!ListExt.IsList(value))
                return StiValueHelper.TryToLong(value);

            return SkipNulls(ListExt.ToList(value))
                .TryCastToLong()
                .Max();
        }

        public static DateTime? MaxDate(object value)
        {
            if (!ListExt.IsList(value))
                return StiValueHelper.TryToNullableDateTime(value);

            return SkipNulls(ListExt.ToList(value))
                .TryCastToNullableDateTime()
                .Max();
        }

        public static TimeSpan? MaxTime(object value)
        {
            if (!ListExt.IsList(value))
                return StiValueHelper.TryToNullableTimeSpan(value);

            return SkipNulls(ListExt.ToList(value))
                .TryCastToNullableTimeSpan()
                .Max();
        }

        public static string MaxStr(object value)
        {
            if (!ListExt.IsList(value))
                return ToString(value);

            return SkipNulls(ListExt.ToList(value))
                .OrderBy(ToString)
                .Cast<string>()
                .LastOrDefault();
        }
    }
}