#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Helpers;
using Stimulsoft.Data.Extensions;
using Stimulsoft.Data.Types;
using System.Collections.Generic;
using System.Linq;

namespace Stimulsoft.Data.Functions
{
    public partial class Funcs
    {
        public static long Count(object value)
        {
            if (ListExt.IsList(value))
                return OptionalSkipNulls(ListExt.ToList(value)).Count();
            else
                return StiValueHelper.TryToLong(value);
        }

        public static object Distinct(object value)
        {
            if (ListExt.IsList(value))
                return OptionalSkipNulls(ListExt.ToList(value)).Distinct();
            else
                return value;
        }

        public static decimal DistinctCount(object value)
        {
            if (ListExt.IsList(value))
                return OptionalSkipNulls(ListExt.ToList(value)).Distinct().Count();
            else
                return StiValueHelper.TryToDecimal(value);
        }

        public static object First(object value)
        {
            if (ListExt.IsList(value))
                return SkipNulls(ListExt.ToList(value)).FirstOrDefault();
            else
                return value;
        }

        public static object Last(object value)
        {
            if (ListExt.IsList(value))
                return SkipNulls(ListExt.ToList(value)).LastOrDefault();
            else
                return value;
        }

        public static object All(object value)
        {
            if (ListExt.IsList(value))
                return SkipNulls(ListExt.ToList(value)).Select(v => new SimpleValue(v));
            else
                return new SimpleValue(value);
        }

        public static IEnumerable<object> RunningTotal(IEnumerable<object> values)
        {
            var items = values.Select(v => v is IEnumerable<object>
                ? Sum(v as IEnumerable<object>)
                : StiValueHelper.TryToDecimal(v));

            var start = 0m;
            foreach (var item in items)
            {
                yield return start + item;
                start += item;
            }
        }
    }
}