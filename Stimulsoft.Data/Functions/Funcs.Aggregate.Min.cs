#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Helpers;
using Stimulsoft.Data.Extensions;
using System;
using System.Linq;

namespace Stimulsoft.Data.Functions
{
    public partial class Funcs
    {
        public static decimal Min(object value)
        {
            if (ListExt.IsList(value))
                return SkipNulls(ListExt.ToList(value)).TryCastToDecimal().Min();
            else
                return StiValueHelper.TryToDecimal(value);
        }

        public static double MinD(object value)
        {
            if (!ListExt.IsList(value))
                return StiValueHelper.TryToDouble(value);
            
            return SkipNulls(ListExt.ToList(value))
                .TryCastToDouble()
                .Min();
        }

        public static long MinI(object value)
        {
            if (!ListExt.IsList(value))
                return StiValueHelper.TryToLong(value);

            return SkipNulls(ListExt.ToList(value))
                .TryCastToLong()
                .Min();
        }

        public static DateTime? MinDate(object value)
        {
            if (!ListExt.IsList(value))
                return StiValueHelper.TryToNullableDateTime(value);

            return SkipNulls(ListExt.ToList(value))
                .TryCastToNullableDateTime()
                .Min();
        }

        public static TimeSpan? MinTime(object value)
        {
            if (!ListExt.IsList(value))
                return StiValueHelper.TryToNullableTimeSpan(value);

            return SkipNulls(ListExt.ToList(value))
                .TryCastToNullableTimeSpan()
                .Min();
        }

        public static string MinStr(object value)
        {
            if (!ListExt.IsList(value))
                return ToString(value);

            return SkipNulls(ListExt.ToList(value))
                .OrderBy(ToString)
                .Cast<string>()
                .FirstOrDefault();
        }
    }
}