#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Linq;
using Stimulsoft.Base.Helpers;
using Stimulsoft.Data.Extensions;

namespace Stimulsoft.Data.Functions
{
    public partial class Funcs
    {
        public static StiMonth? MonthIdent(DateTime? dateTime)
        {
            if (dateTime == null) return null;
            return (StiMonth)Enum.ToObject(typeof(StiMonth), Month(dateTime));
        }

        public static object MonthIdentObject(object value)
        {
            if (ListExt.IsList(value))
                return ListExt.ToNullableDateTimeList(value).Select(MonthIdent);
            else
                return MonthIdent(StiValueHelper.TryToNullableDateTime(value));
        }

        public static int Month(DateTime? dateTime)
        {
            if (dateTime == null) return -1;
            return dateTime.Value.Month;
        }

        public static object MonthObject(object value)
        {
            if (ListExt.IsList(value))
                return ListExt.ToNullableDateTimeList(value).Select(Month);
            else
                return Month(StiValueHelper.TryToNullableDateTime(value));
        }

        public static string MonthName(DateTime? date)
        {
            if (date.HasValue)
                return StiMonthToStrHelper.MonthName(date.Value);
            else
                return string.Empty;
        }

        public static object MonthNameObject(object value)
        {
            if (ListExt.IsList(value))
                return ListExt.ToNullableDateTimeList(value).Select(MonthName);
            else
                return MonthName(StiValueHelper.TryToNullableDateTime(value));
        }

        public static string MonthName(DateTime? date, bool localized)
        {
            if (date.HasValue)
                return StiMonthToStrHelper.MonthName(date.Value, localized);
            else
                return string.Empty;
        }

        public static object MonthNameObject(object value, bool localized)
        {
            if (ListExt.IsList(value))
                return ListExt.ToNullableDateTimeList(value).Select(d => MonthName(d, localized));
            else
                return MonthName(StiValueHelper.TryToNullableDateTime(value), localized);
        }

        public static string MonthName(DateTime? date, string culture)
        {
            if (date.HasValue)
                return StiMonthToStrHelper.MonthName(date.Value, culture);
            else
                return string.Empty;
        }

        public static object MonthNameObject(object value, string culture)
        {
            if (ListExt.IsList(value))
                return ListExt.ToNullableDateTimeList(value).Select(d => MonthName(d, culture));
            else
                return MonthName(StiValueHelper.TryToNullableDateTime(value), culture);
        }

        public static string MonthName(DateTime? date, string culture, bool upperCase)
        {
            if (date.HasValue)
                return StiMonthToStrHelper.MonthName(date.Value, culture, upperCase);
            else
                return string.Empty;
        }

        public static object MonthNameObject(object value, string culture, bool upperCase)
        {
            if (ListExt.IsList(value))
                return ListExt.ToNullableDateTimeList(value).Select(d => MonthName(d, culture, upperCase));
            else
                return MonthName(StiValueHelper.TryToNullableDateTime(value), culture, upperCase);
        }
    }
}