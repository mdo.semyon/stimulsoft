#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using Stimulsoft.Base.Helpers;
using Stimulsoft.Data.Types;

namespace Stimulsoft.Data.Comparers
{
    public class StiObjectComparer : IEqualityComparer<object>
    {
        #region IEqualityComparer
        public new bool Equals(object x, object y)
        {
            return Compare(x, y) == 0;
        }

        public int GetHashCode(object x)
        {
            return ((IStructuralEquatable)x).GetHashCode(EqualityComparer<object>.Default);
        }
        #endregion

        #region Properties
        public static readonly StiObjectComparer Default = new StiObjectComparer();
        #endregion

        #region Methods
        public static int Compare(object x, object y)
        {
            if (x == DBNull.Value && y == DBNull.Value) return 0;
            if (x == null && y == null) return 0;
            if (x == DBNull.Value) return -1;
            if (y == DBNull.Value) return 1;
            if (x == null) return -1;
            if (y == null) return 1;

            var type = x.GetType();

            if (type == typeof(DateTime))
                return DateTimeCompare((DateTime)x, (DateTime)y);

            if (type == typeof(string))
                return String.Compare(x.ToString(), y.ToString(), CultureInfo.CurrentCulture, CompareOptions.OrdinalIgnoreCase);

            if (type == typeof(bool))
                return DefaultCompare<bool>(x, y);

            if (type == typeof(Int16))
                return Comparer<Int16>.Default.Compare(Convert.ToInt16(x), Convert.ToInt16(y));

            if (type == typeof(Int32))
                return Comparer<Int32>.Default.Compare(Convert.ToInt32(x), Convert.ToInt32(y));

            if (type == typeof(Int64))
                return Comparer<Int64>.Default.Compare(Convert.ToInt64(x), Convert.ToInt64(y));

            if (type == typeof(UInt16))
                return Comparer<UInt16>.Default.Compare(Convert.ToUInt16(x), Convert.ToUInt16(y));

            if (type == typeof(UInt32))
                return Comparer<UInt32>.Default.Compare(Convert.ToUInt32(x), Convert.ToUInt32(y));

            if (type == typeof(UInt64))
                return Comparer<UInt64>.Default.Compare(Convert.ToUInt64(x), Convert.ToUInt64(y));

            if (type == typeof(float))
            {
                if (x is string && y is string)
                {
                    var sep = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                    var xx = ((string) x).Replace(".", ",").Replace(",", sep);
                    var yy = ((string) y).Replace(".", ",").Replace(",", sep);

                    return Comparer<float>.Default.Compare(StiValueHelper.TryToFloat(xx), StiValueHelper.TryToFloat(yy));
                }
                else
                    return Comparer<float>.Default.Compare(StiValueHelper.TryToFloat(x), StiValueHelper.TryToFloat(y));
            }

            if (type == typeof(double))
            {
                if (x is string && y is string)
                {
                    var sep = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                    var xx = ((string) x).Replace(".", ",").Replace(",", sep);
                    var yy = ((string) y).Replace(".", ",").Replace(",", sep);

                    return Comparer<double>.Default.Compare(StiValueHelper.TryToDouble(xx), StiValueHelper.TryToDouble(yy));
                }
                else
                    return Comparer<double>.Default.Compare(StiValueHelper.TryToDouble(x), StiValueHelper.TryToDouble(y));
            }

            if (type == typeof(decimal))
            {
                if (x is string && y is string)
                {
                    var sep = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                    var xx = ((string) x).Replace(".", ",").Replace(",", sep);
                    var yy = ((string) y).Replace(".", ",").Replace(",", sep);

                    return Comparer<decimal>.Default.Compare(StiValueHelper.TryToDecimal(xx), StiValueHelper.TryToDecimal(yy));
                }
                else
                    return Comparer<decimal>.Default.Compare(StiValueHelper.TryToDecimal(x), StiValueHelper.TryToDecimal(y));
            }

            if (type == typeof(byte[]))
                return ArrayCompare(x as byte[], y as byte[]);

            if (x is SimpleValue || y is SimpleValue)
            {
                var xValue = x is SimpleValue ? ((SimpleValue)x).Value : x;
                var yValue = y is SimpleValue ? ((SimpleValue)y).Value : y;
                return Compare(xValue, yValue);
            }

            return Comparer.Default.Compare(x, y);
        }

        private static int DefaultCompare<T>(object a, object b)
        {
            return Comparer<T>.Default.Compare((T)a, (T)b);
        }

        private static int DateTimeCompare(DateTime a, DateTime b)
        {
            return Comparer<DateTime>.Default.Compare(a.Date, b.Date);
        }

        private static int ArrayCompare(byte[] a, byte[] b)
        {
            if (a.Length < b.Length) return -1;
            if (a.Length > b.Length) return 1;

            var result = a.AsQueryable().SequenceEqual(b);
            if (!result) return -1;
            return 0;
        }
        #endregion
    }
}