#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Stimulsoft.Data.Engine
{
    public class StiDataPicker
    {
        #region Fields.Static
        private static object lockObject = new object();
        private static DataTable nullTable = new DataTable();
        private static Dictionary<string, DataTable> hashCache = new Dictionary<string, DataTable>();
        #endregion

        #region Methods
        /// <summary>
        /// Returns all data tables which is used in all elements of the dashboard
        /// </summary>
        public static IEnumerable<DataTable> Fetch(IStiQueryObject query)
        {
            var dataNames = query?.RetrieveUsedDataNames();
            var dataSources = query.GetDataSources(dataNames);
            if (dataSources == null || !dataSources.Any()) return null;

            dataSources = StiDataSourceChainFinder.Find(dataSources, true);

            var databases = dataSources
                .Select(d => d.GetConnection())
                .Where(d => d != null);//Take only databases for the specified datasources

            if (dataSources.All(ExistsInCache))
                return dataSources.Select(GetFromCache);

            var dictionary = query.GetDictionary();
            dictionary.OpenConnections(databases);

            var dataTables = new List<DataTable>();
            foreach (var dataSource in dataSources)
            {
                var dataTable = dataSource.GetDataTable();
                AddToCache(dataSource, dataTable);
                if (dataTable == null) continue;

                dataTables.Add(dataTable);
                AddTableNameToColumnNames(dataTable);
            }

            dictionary.CloseConnections();

            return dataTables;
        }

        private static DataTable AddTableNameToColumnNames(DataTable table)
        {
            var tablePrefix = $"{table.TableName}.";
            foreach (DataColumn column in table.Columns)
            {
                if (!column.ColumnName.StartsWith(tablePrefix))
                    column.ColumnName = $"{tablePrefix}{column.ColumnName}";
            }

            return table;
        }
        #endregion

        #region Methods.Cache
        internal static void ClearCache()
        {
            lock (lockObject)
            {
                hashCache.Clear();
            }
        }

        private static DataTable GetFromCache(IStiAppDataSource dataSource)
        {
            lock (lockObject)
            {
                var key = GetCacheKey(dataSource);
                return hashCache.ContainsKey(key) ? hashCache[key] : null;
            }
        }

        private static bool ExistsInCache(IStiAppDataSource dataSource)
        {
            lock (lockObject)
            {
                var key = GetCacheKey(dataSource);
                return hashCache.ContainsKey(key);
            }
        }

        private static void AddToCache(IStiAppDataSource dataSource, DataTable dataTable)
        {
            lock (lockObject)
            {
                if (dataTable == null)
                    dataTable = nullTable;

                var key = GetCacheKey(dataSource);

                if (hashCache.ContainsKey(key))
                    hashCache[key] = dataTable;
                else
                    hashCache[key] = dataTable;
            }
        }

        private static string GetCacheKey(IStiAppDataSource dataSource)
        {
            var connection = dataSource.GetConnection();

            return connection == null
                ? $"{dataSource.GetName()}"
                : $"{connection.GetName()}.{dataSource.GetName()}";
        }
        #endregion
    }
}