#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Json;
using Stimulsoft.Base.Json.Converters;

namespace Stimulsoft.Data.Engine
{
    #region StiDataJoinType
    [JsonConverter(typeof(StringEnumConverter))]
    public enum StiDataJoinType
    {
        Inner = 1,
        Left,
        Right,
        Cross,
        Full
    }
    #endregion

    #region StiDataSortDirection
    [JsonConverter(typeof(StringEnumConverter))]
    public enum StiDataSortDirection
    {
        Ascending = 1,
        Descending,
        None
    }
    #endregion

    #region StiDataFilterCondition
    public enum StiDataFilterCondition
    {
        EqualTo,
        NotEqualTo,
        GreaterThan,
        GreaterThanOrEqualTo,
        LessThan,
        LessThanOrEqualTo,
        Between,
        NotBetween,

        Containing,
        NotContaining,
        BeginningWith,
        EndingWith,

        IsNull,
        IsNotNull,
        IsBlanks,
        IsNotBlanks
    }
    #endregion

    #region StiDataFilterMode
    public enum StiDataFilterMode
    {
        And,
        Or
    }
    #endregion

    #region StiDataActionType
    // Sorted by order of use in the table
    public enum StiDataActionType
    {
        Limit,
        Replace,
        RunningTotal,
        Percentage,
    }
    #endregion

    #region StiDataFilterConditionGroupType
    public enum StiDataFilterConditionGroupType
    {
        Equal,
        NotEqual,
        Custom,
        Empty
    }
    #endregion
}