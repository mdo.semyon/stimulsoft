#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

#if NETCORE
using Stimulsoft.System.Data;
#endif

namespace Stimulsoft.Data.Engine
{
    public static class StiDataJoiner
    {
        #region Fields
        private static object lockObject = new object();
        private static DataTable nullTable = new DataTable();
        private static Dictionary<int, DataTable> hashCache = new Dictionary<int, DataTable>();
        #endregion

        #region Methods
        public static DataTable Join(IEnumerable<DataTable> tables, List<StiDataLink> links)
        {
            if (tables.Count() < 2)
                return tables.FirstOrDefault();

            var resultTable = GetFromCache(tables);
            if (resultTable == nullTable)
                return null;

            if (resultTable != null)
                return resultTable;            

            var stackedTables = tables.ToList();
            var primaryTable = stackedTables.First();
            stackedTables = stackedTables.Skip(1).ToList();

            var onlyTableHash = new Hashtable();
            var onlyTables = new List<DataTable>();
            
            resultTable = primaryTable.AsEnumerable().CopyToDataTable();
            CopyColumns(primaryTable, resultTable);

            var mergedTables = new List<DataTable> { primaryTable };

            while (stackedTables.Any())
            {
                var stackedTable = stackedTables.FirstOrDefault();
                var mergedTable = mergedTables.FirstOrDefault(t => FindLink(stackedTable, t, links) != null);
                var dataRelation = FindLink(mergedTable, stackedTable, links);

                #region Find table and relation to merge
                if (mergedTable == null || dataRelation == null)
                {
                    stackedTables.Remove(stackedTable);

                    if (onlyTableHash[stackedTable] == null)
                        stackedTables.Add(stackedTable);
                    else
                        onlyTables.Add(stackedTable);

                    onlyTableHash[stackedTable] = stackedTable;

                    continue;
                }
                #endregion

                CopyColumns(stackedTable, resultTable);

                var destination = resultTable.Rows.Count == 0 ? mergedTable : resultTable;
                var rows = new StiDataRowJoiner(resultTable, destination, stackedTable)
                    .Join(StiDataJoinType.Left, dataRelation);

                resultTable = rows.Any() ? rows.CopyToDataTable() : new DataTable();

                stackedTables.Remove(stackedTable);
                mergedTables.Add(stackedTable);
            }

            foreach (var table in onlyTables)
            {
                resultTable = MergeSideBySide(resultTable, table);
            }

            AddToCache(tables, resultTable);

            return resultTable;
        }

        private static void CopyColumns(DataTable from, DataTable to)
        {
            foreach (DataColumn column in from.Columns)
            {
                if (!to.Columns.Contains(column.ColumnName))
                    to.Columns.Add(new DataColumn(column.ColumnName, column.DataType));
            }
        }

        private static DataTable MergeSideBySide(DataTable table1, DataTable table2)
        {
            var result = new DataTable();

            CopyColumns(table1, result);
            CopyColumns(table2, result);

            for (var rowIndex = 0; rowIndex < Math.Max(table1.Rows.Count, table2.Rows.Count); rowIndex++)
            {
                var row = result.NewRow();

                if (rowIndex < table1.Rows.Count)
                {
                    for (var index = 0; index < table1.Columns.Count; index++)
                    {
                        row[index] = table1.Rows[rowIndex][index];
                    }
                }

                if (rowIndex < table2.Rows.Count)
                {
                    for (var index = 0; index < table2.Columns.Count; index++)
                    {
                        row[table1.Columns.Count + index] = table2.Rows[rowIndex][index];
                    }
                }

                result.Rows.Add(row);
            }

            return result;
        }

        private static StiDataLink FindLink(DataTable table1, DataTable table2, List<StiDataLink> links)
        {
            if (table1 == null || table2 == null) return null;

            var relations = links.Where(l => 
                (l.ParentTable == table1.TableName && l.ChildTable == table2.TableName) || 
                (l.ParentTable == table2.TableName && l.ChildTable == table1.TableName));

            if (relations.Any(r => r.Active))
                return relations.First(r => r.Active);

            else if (relations.Any())
                return relations.FirstOrDefault();

            relations = links.Where(r => 
                (r.ParentTable == table1.TableName && r.ChildTable == table2.TableName) || 
                (r.ParentTable == table2.TableName && r.ChildTable == table1.TableName));

            if (relations.Any(r => r.Active))
                return relations.First(r => r.Active);

            else if (relations.Any())
                return relations.FirstOrDefault();

            else
                return null;
        }
        #endregion

        #region Methods.Cache
        internal static void ClearCache()
        {
            lock (lockObject)
            {
                hashCache.Clear();
            }
        }

        private static int GetCacheKey(IEnumerable<DataTable> tables)
        {
            return tables
                .SelectMany(t => t.Columns.Cast<DataColumn>())
                .Select(c => c.GetHashCode())
                .Aggregate(0, (c1, c2) => unchecked(c1 + c2));
        }

        private static DataTable GetFromCache(IEnumerable<DataTable> tables)
        {
            lock (lockObject)
            {
                var key = GetCacheKey(tables);
                return hashCache.ContainsKey(key) ? hashCache[key] : null;
            }
        }

        private static void AddToCache(IEnumerable<DataTable> tables, DataTable dataTable)
        {
            lock (lockObject)
            {
                var key = GetCacheKey(tables);

                if (dataTable == null)
                    dataTable = nullTable;

                if (hashCache.ContainsKey(key))
                    hashCache[key] = dataTable;
                else
                    hashCache[key] = dataTable;
            }
        }
        #endregion
    }
}