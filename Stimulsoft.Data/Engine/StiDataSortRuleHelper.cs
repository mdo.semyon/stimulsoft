#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stimulsoft.Base;

namespace Stimulsoft.Data.Engine
{
    public class StiDataSortRuleHelper
    {
        #region Methods.Static
        internal static List<StiDataSortRule> Validate(List<StiDataSortRule> rules, List<string> columnKeys)
        {
            return rules
                .Where(r => StiKeyHelper.IsKey(r.Key))
                .Where(r => columnKeys.Contains(r.Key))
                .ToList();
        }

        internal static string GetDataTableSortQuery(List<StiDataSortRule> rules, IEnumerable<IStiAppDataColumn> columns)
        {
            return GetDataTableSortQuery(rules,
                columns.Select(c => c.GetKey()).ToList(),
                columns.Select(c => c.GetName()).ToList());
        }

        internal static string GetDataTableSortQuery(List<StiDataSortRule> rules, List<string> columnKeys, List<string> columnNames)
        {
            var sb = new StringBuilder();

            if (rules != null)
            {
                foreach (var rule in rules.Where(r => !string.IsNullOrWhiteSpace(r.Key)))
                {
                    if (sb.Length > 0)
                        sb = sb.Append(", ");

                    var columnIndex = columnKeys.IndexOf(rule.Key);
                    var columnName = StiDataColumnRuleHelper.GetGoodColumnName(columnNames[columnIndex]);

                    if (rule.Direction == StiDataSortDirection.Descending)
                        sb = sb.Append($"{columnName} DESC");
                    else
                        sb = sb.Append(columnName);
                }
            }

            return sb.ToString();
        }

        public static StiDataSortDirection GetSortDirection(List<StiDataSortRule> rules, string columnKey)
        {
            var rule = rules.FirstOrDefault(r => string.Equals(r.Key, columnKey, StringComparison.InvariantCultureIgnoreCase));

            return rule == null
                ? StiDataSortDirection.None
                : rule.Direction;
        }

        public static List<StiDataSortRule> SetSortDirection(List<StiDataSortRule> rules, string columnKey, StiDataSortDirection direction)
        {
            var rule = rules.FirstOrDefault(r => string.Equals(r.Key, columnKey, StringComparison.InvariantCultureIgnoreCase));

            if (rule == null && direction != StiDataSortDirection.None)
            {
                rule = new StiDataSortRule(columnKey, direction);
                rules.Add(rule);
            }
            else
            {
                if (direction == StiDataSortDirection.None)
                    rules.Remove(rule);
                else
                    rule.Direction = direction;
            }

            return rules;
        }
        #endregion
    }
}