#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Helpers;
using Stimulsoft.Data.Exceptions;
using Stimulsoft.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Stimulsoft.Data.Engine
{
    public class StiDataFilterRuleHelper
    {
        #region Methods.Static
        internal static List<StiDataFilterRule> Validate(List<StiDataFilterRule> rules, List<string> columnKeys)
        {
            return rules
                .Where(r => StiKeyHelper.IsKey(r.Key))
                .Where(r => columnKeys.Contains(r.Key))
                .ToList();
        }

        internal static string GetDataTableFilterQuery(List<StiDataFilterRule> rules, IEnumerable<IStiAppDataColumn> columns)
        {
            return GetDataTableFilterQuery(rules,
                columns.Select(c => c.GetKey()).ToList(),
                columns.Select(c => c.GetName()).ToList(),
                columns.Select(c => c.GetDataType()).ToList());
        }            

        internal static string GetDataTableFilterQuery(List<StiDataFilterRule> rules, List<string> columnKeys, List<string> columnNames, List<Type> columnTypes)
        {
            var sb = new StringBuilder();

            if (rules != null)
            {
                rules = rules
                    .Where(r => !string.IsNullOrWhiteSpace(r.Key))
                    .OrderBy(r => columnKeys.FindIndex(k => k == r.Key))
                    .ToList();

                var groups = rules.GroupBy(r => r.Key);

                foreach (var group in groups)
                {
                    var keyRules = rules.Where(r => r.Key == group.Key);
                    var sbr = GetFilterQuery(keyRules, columnKeys, columnNames, columnTypes);

                    if (sbr.Length > 0)
                    {
                        if (sb.Length > 0)
                            sb = sb.Append(" AND ");

                        if (keyRules.Count() > 1 && groups.Count() > 1)
                            sb = sb.Append("(");

                        sb = sb.Append(sbr);

                        if (keyRules.Count() > 1 && groups.Count() > 1)
                            sb = sb.Append(")");
                    }
                }
            }

            return sb.ToString();
        }

        private static StringBuilder GetFilterQuery(IEnumerable<StiDataFilterRule> keyRules, List<string> columnKeys, List<string> columnNames, List<Type> columnTypes)
        {
            var sbr = new StringBuilder();

            foreach (var rule in keyRules.Where(r => r.IsEnabled))
            {
                if (sbr.Length > 0)
                    sbr = sbr.Append(rule.FilterMode == StiDataFilterMode.And ? " AND " : " OR ");

                var columnIndex = columnKeys.IndexOf(rule.Key);
                var columnName = columnNames[columnIndex];
                var columnType = columnTypes[columnIndex];

                var value = rule.IsExpression ? StiExpressionHelper.ParseReportExpression(rule.Value) : rule.Value;
                var value2 = rule.IsExpression ? StiExpressionHelper.ParseReportExpression(rule.Value2) : rule.Value2;
                var condition = GetCondition(columnName, rule.Condition, value, value2, columnType);

                sbr = sbr.Append(condition);
            }

            return sbr;
        }

        private static string GetCondition(string columnName, StiDataFilterCondition condition, string value, string value2, Type columnType)
        {
            var column = StiDataColumnRuleHelper.GetGoodColumnName(columnName);
            switch (condition)
            {
                case StiDataFilterCondition.EqualTo:
                    value = GetQueryValue(value, columnType, true);
                    return $"{column} = {value}";

                case StiDataFilterCondition.NotEqualTo:
                    value = GetQueryValue(value, columnType, true);
                    return $"{column} <> {value}";

                case StiDataFilterCondition.GreaterThan:
                    value = GetQueryValue(value, columnType, true);
                    return $"{column} > {value}";

                case StiDataFilterCondition.GreaterThanOrEqualTo:
                    value = GetQueryValue(value, columnType, true);
                    return $"{column} >= {value}";

                case StiDataFilterCondition.LessThan:
                    value = GetQueryValue(value, columnType, true);
                    return $"{column} < {value}";

                case StiDataFilterCondition.LessThanOrEqualTo:
                    value = GetQueryValue(value, columnType, true);
                    return $"{column} <= {value}";

                case StiDataFilterCondition.Between:
                    value = GetQueryValue(value, columnType, true);
                    value2 = GetQueryValue(value2, columnType, true);
                    return $"({column} > {value} AND {column} < {value2})";

                case StiDataFilterCondition.NotBetween:
                    value = GetQueryValue(value, columnType, true);
                    value2 = GetQueryValue(value2, columnType, true);
                    return $"({column} < {value} OR {column} > {value2})";

                case StiDataFilterCondition.Containing:
                    value = GetQueryValue(value, columnType, false);
                    return $"{column} LIKE '*{value}*'";

                case StiDataFilterCondition.NotContaining:
                    value = GetQueryValue(value, columnType, false);
                    return $"NOT ({column} LIKE '*{value}*')";

                case StiDataFilterCondition.BeginningWith:
                    value = GetQueryValue(value, columnType, false);
                    return $"{column} LIKE '{value}*'";

                case StiDataFilterCondition.EndingWith:
                    value = GetQueryValue(value, columnType, false);
                    return $"{column} LIKE '*{value}'";

                case StiDataFilterCondition.IsNull:
                    return $"{column} is null";

                case StiDataFilterCondition.IsNotNull:
                    return $"{column} is not null";

                case StiDataFilterCondition.IsBlanks:
                    return $"TRIM({column}) = ''";

                case StiDataFilterCondition.IsNotBlanks:
                    return $"TRIM({column}) <> ''";

                default:
                    throw new StiTypeNotRecognizedException(condition);
            }
        }

        private static string GetQueryValue(string value, Type type, bool stringQuotes)
        {
            if (type.IsNumericType())
            {
                if (string.IsNullOrEmpty(value))
                    return "0";
                
                decimal num;
                if (!decimal.TryParse(value.Replace(",", "."), NumberStyles.Float, CultureInfo.InvariantCulture, out num))
                    return "0";

                return value;
            }

            if (type == typeof(bool))
                return (value != null && value.ToLower() == "true").ToString().ToUpper();

            if (type.IsDateType())
            {
                DateTime date;
                if (!DateTime.TryParseExact(value, "MM'/'dd'/'yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date)) date = new DateTime(1800, 1, 1);
                value = date.ToShortDateString();
            }

            if (type.IsEnum && Enum.IsDefined(type, value))
                value = value != null ? Enum.GetName(type, value) : string.Empty;

            value = value.Replace("'", "''");

            if (stringQuotes)
                return $"'{value}'";

            return value;
        }
        #endregion
    }
}