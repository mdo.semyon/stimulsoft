#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;

namespace Stimulsoft.Data.Engine
{
    internal class StiDataFilterComparer : IComparer<object>
    {
        public int Compare(object x, object y)
        {
            if (x is string && y is string)
                return string.Compare((string)x, (string)y, StringComparison.Ordinal);

            if (x is string && IsNumber(y))
                return -1;

            if (y is string && IsNumber(x))
                return 1;

            if (IsNumber(x) && IsNumber(y))
                return Convert.ToDouble(x).CompareTo(Convert.ToDouble(y));

            if (x is DateTime && y is DateTime)
                return -((DateTime) x).CompareTo((DateTime) y);

            return 0;
        }

        private bool IsNumber(object o)
        {
            return o is int || o is double || o is float || o is long;
        }
    }
}
