#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Data.Design;
using System;
using System.ComponentModel;

namespace Stimulsoft.Data.Engine
{
    [TypeConverter(typeof(StiDataFilterRuleConverter))]
    public class StiDataFilterRule : IStiJsonReportObject
    {
        #region IStiJsonReportObject
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyStringNullOrEmpty("Key", Key);
            jObject.AddPropertyEnum("Condition", Condition, StiDataFilterCondition.EqualTo);
            jObject.AddPropertyStringNullOrEmpty("Value", Value);
            jObject.AddPropertyStringNullOrEmpty("Value2", Value2);
            jObject.AddPropertyBool("IsEnabled", IsEnabled, true);
            jObject.AddPropertyBool("IsExpression", IsExpression, false);
            jObject.AddPropertyEnum("FilterMode", FilterMode, StiDataFilterMode.Or);

            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Key":
                        Key = property.Value.ToObject<string>();
                        break;

                    case "Condition":
                        Condition = (StiDataFilterCondition)Enum.Parse(typeof(StiDataFilterCondition), property.Value.ToObject<string>());
                        break;

                    case "Value":
                        Value = property.Value.ToObject<string>();
                        break;

                    case "Value2":
                        Value2 = property.Value.ToObject<string>();
                        break;

                    case "IsEnabled":
                        IsEnabled = property.Value.ToObject<bool>();
                        break;

                    case "IsExpression":
                        IsExpression = property.Value.ToObject<bool>();
                        break;

                    case "FilterMode":
                        FilterMode = (StiDataFilterMode)Enum.Parse(typeof(StiDataFilterMode), property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region Methods
        public static StiDataFilterRule LoadFromJson(JObject json)
        {
            var rule = new StiDataFilterRule();
            rule.LoadFromJsonObject(json);
            return rule;
        }

        public override string ToString()
        {
            return $"{Key} {Condition} {Value}";
        }

        public virtual int GetUniqueCode()
        {
            unchecked
            {
                var hashCode = Key != null ? Key.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ Condition.GetHashCode();
                hashCode = (hashCode * 397) ^ (Value != null ? Value.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Value2 != null ? Value2.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsEnabled.GetHashCode();
                hashCode = (hashCode * 397) ^ IsExpression.GetHashCode();
                hashCode = (hashCode * 397) ^ FilterMode.GetHashCode();
                return hashCode;
            }
        }
        #endregion

        #region Properties
        [StiSerializable]
        [DefaultValue(null)]
        public string Key { get; set; }

        [StiSerializable]
        [DefaultValue(StiDataFilterCondition.EqualTo)]
        public StiDataFilterCondition Condition { get; set; }

        [StiSerializable]
        [DefaultValue(null)]
        public string Value { get; set; }

        [StiSerializable]
        [DefaultValue(null)]
        public string Value2 { get; set; }

        [StiSerializable]
        [DefaultValue(true)]
        public bool IsEnabled { get; set; } = true;

        [StiSerializable]
        [DefaultValue(false)]
        public bool IsExpression { get; set; } = false;

        [StiSerializable]
        [DefaultValue(StiDataFilterMode.And)]
        public StiDataFilterMode FilterMode { get; set; }
        #endregion

        public StiDataFilterRule()
            : this(null, StiDataFilterCondition.EqualTo, null, null, StiDataFilterMode.And)
        {
        }

        public StiDataFilterRule(string key, StiDataFilterCondition condition)
            : this(key, condition, null, null, StiDataFilterMode.And)
        {
        }

        public StiDataFilterRule(string key, StiDataFilterCondition condition, string value)
            : this(key, condition, value, null, StiDataFilterMode.And)
        {
        }

        public StiDataFilterRule(string key, StiDataFilterCondition condition, string value, string value2, StiDataFilterMode filterMode)
        {
            Key = key;
            Condition = condition;
            Value = value;
            Value2 = value2;
            FilterMode = filterMode;
        }
    }
}