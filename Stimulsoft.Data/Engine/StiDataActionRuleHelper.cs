#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Stimulsoft.Base.Helpers;
using Stimulsoft.Data.Comparers;
using Stimulsoft.Data.Functions;
using Stimulsoft.Data.Helpers;

#if NETCORE
using Stimulsoft.System.Data;
#endif

namespace Stimulsoft.Data.Engine
{
    public class StiDataActionRuleHelper
    {
        #region Methods.Static
        public static void ApplyActions(DataTable table, List<StiDataActionRule> actions, List<string> columnKeys)
        {
            actions.Sort(new StiDataActionComparer());

            foreach (var action in actions)
            {
                var columnIndex = columnKeys.IndexOf(action.Key);
                switch (action.Type)
                {
                    case StiDataActionType.Limit:
                        ApplyLimitAction(table, action.StartIndex, action.RowsCount);
                        break;

                    case StiDataActionType.Replace:
                        ApplyReplaceAction(table, columnIndex, action.ValueFrom, action.ValueTo, action.MatchCase);
                        break;

                    case StiDataActionType.RunningTotal:
                        ApplyRunningTotalAction(table, columnIndex, action.InitialValue);
                        break;

                    case StiDataActionType.Percentage:
                        ApplyPercentageAction(table, columnIndex);
                        break;
                }
            }
        }

        private static void ApplyLimitAction(DataTable table, int startIndex, int rowsCount)
        {
            if (rowsCount < 0)
                rowsCount = table.Rows.Count;

            var rows = table.AsEnumerable().Skip(startIndex).Take(Math.Max(0, rowsCount));
            table.AsEnumerable().Except(rows).ToList().ForEach(r => r.Delete());
        }

        private static void ApplyReplaceAction(DataTable table, int columnIndex, string valueFrom, string valueTo, bool matchCase)
        {
            valueFrom = StiExpressionHelper.ParseReportExpression(valueFrom);
            valueTo = StiExpressionHelper.ParseReportExpression(valueTo);

            table.AsEnumerable().ToList().ForEach(r =>
            {
                var value = StiValueHelper.TryToString(r[columnIndex]);
                if (!string.IsNullOrEmpty(value))
                {
                    if (matchCase)
                        r[columnIndex] = value.Replace(valueFrom, valueTo);
                    else
                    {
                        var index = value.ToLowerInvariant().IndexOf(valueFrom.ToLowerInvariant(), StringComparison.Ordinal);
                        if (index >= 0)
                            r[columnIndex] = value.Substring(0, index) + valueTo + value.Substring(index + valueFrom.Length);
                    }
                }
            });
        }

        private static void ApplyRunningTotalAction(DataTable table, int columnIndex, string initialValue)
        {
            if (columnIndex == -1) return;

            initialValue = StiExpressionHelper.ParseReportExpression(initialValue);

            decimal currentValue;
            decimal.TryParse(initialValue.Replace(",", "."), NumberStyles.Float, CultureInfo.InvariantCulture, out currentValue);

            table.AsEnumerable().ToList().ForEach(r => r[columnIndex] = currentValue += StiValueHelper.TryToDecimal(r[columnIndex]));
        }

        private static void ApplyPercentageAction(DataTable table, int columnIndex)
        {
            var sum = Funcs.Sum(table.AsEnumerable().Select(r => r[columnIndex]));
            table.AsEnumerable().ToList().ForEach(r => r[columnIndex] = Math.Round(StiValueHelper.TryToDecimal(r[columnIndex]) / sum * 100, 2));
        }
        #endregion
    }
}