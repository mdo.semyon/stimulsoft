#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Data.Design;
using System.ComponentModel;

namespace Stimulsoft.Data.Engine
{
    [TypeConverter(typeof(StiDataLinkConverter))]
    [StiSerializable]
    public class StiDataLink : IStiJsonReportObject
    {
        #region IStiJsonReportObject
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyStringNullOrEmpty("Key", Key);

            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Key":
                        Key = property.Value.ToObject<string>();
                        break;
                }
            }
        }
        #endregion

        #region Properties
        public string ParentTable { get; set; }

        public string ChildTable { get; set; }

        public string ParentColumn { get; set; }

        public string ChildColumn { get; set; }

        public string ParentKey
        {
            get
            {
                if (string.IsNullOrEmpty(ParentTable) || string.IsNullOrEmpty(ParentColumn))
                    return null;

                return $"{ParentTable}.{ParentColumn}";
            }
        }

        public string ChildKey
        {
            get
            {
                if (string.IsNullOrEmpty(ChildTable) || string.IsNullOrEmpty(ChildColumn))
                    return null;

                return $"{ChildTable}.{ChildColumn}";
            }
        }
        
        /// <summary>
        /// Gets or sets the key to the data relation in the report dictionary.
        /// </summary>
        public string Key { get; set; }

        public bool Active { get; set; }
        #endregion

        #region Methods
        public static StiDataLink LoadFromJson(JObject json)
        {
            var link = new StiDataLink();
            link.LoadFromJsonObject(json);
            return link;
        }
        #endregion

        public StiDataLink()
        {
        }

        public StiDataLink(string key)
        {
            Key = key;
        }

        public StiDataLink(string parentTable, string childTable, string parentColumn, string childColumn, bool active)
        {
            ParentTable = parentTable;
            ChildTable = childTable;
            ParentColumn = parentColumn;
            ChildColumn = childColumn;
            Active = active;
        }

        public StiDataLink(string parentTable, string childTable, string parentColumn, string childColumn, bool active, string key) :
            this(parentTable, childTable, parentColumn, childColumn, active)
        {
            Key = key;
        }
    }
}