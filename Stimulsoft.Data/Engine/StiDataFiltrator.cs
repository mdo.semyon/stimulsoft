#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

#if NETCORE
using Stimulsoft.System.Data;
#endif

namespace Stimulsoft.Data.Engine
{
    public static class StiDataFiltrator
    {
        #region Fields
        private static object lockObject = new object();
        private static DataTable nullTable = new DataTable();
        private static Dictionary<int, DataTable> hashCache = new Dictionary<int, DataTable>();
        #endregion

        #region Methods
        public static DataTable Filter(DataTable inTable, IEnumerable<StiDataFilterRule> filters)
        {
            if (filters?.Count() == 0)
                return inTable;

            var columnNames = inTable.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList();
            var columnTypes = inTable.Columns.Cast<DataColumn>().Select(c => c.DataType).ToList();
            var columnKeys = columnNames;

            var filter = StiDataFilterRuleHelper.GetDataTableFilterQuery(filters.ToList(), columnKeys, columnNames, columnTypes);
            if (string.IsNullOrWhiteSpace(filter))
                return inTable;

            var outTable = GetFromCache(inTable, filters);
            if (outTable == nullTable)
                return null;

            if (outTable != null)
                return outTable;                        

            inTable.DefaultView.RowFilter = filter;
            outTable = inTable.DefaultView.ToTable();

            AddToCache(inTable, filters, outTable);

            return outTable;
        }
        #endregion

        #region Methods.Cache
        internal static void ClearCache()
        {
            lock (lockObject)
            {
                hashCache.Clear();
            }
        }

        private static int GetCacheKey(DataTable table, IEnumerable<StiDataFilterRule> filters)
        {
            var tableHash = table.Columns
                .Cast<DataColumn>()
                .Select(c => c.GetHashCode())
                .Aggregate(0, (c1, c2) => unchecked(c1 + c2));

            var filterHash = filters
                .Select(c => c.GetUniqueCode())
                .Aggregate(0, (c1, c2) => unchecked(c1 + c2));

            return unchecked(tableHash + filterHash);
        }

        private static DataTable GetFromCache(DataTable inTable, IEnumerable<StiDataFilterRule> filters)
        {
            lock (lockObject)
            {
                var key = GetCacheKey(inTable, filters);
                return hashCache.ContainsKey(key) ? hashCache[key] : null;
            }
        }

        private static void AddToCache(DataTable inTable, IEnumerable<StiDataFilterRule> filters, DataTable outTable)
        {
            lock (lockObject)
            {
                var key = GetCacheKey(inTable, filters);

                if (outTable == null)
                    outTable = nullTable;

                if (hashCache.ContainsKey(key))
                    hashCache[key] = outTable;
                else
                    hashCache[key] = outTable;
            }
        }
        #endregion
    }
}