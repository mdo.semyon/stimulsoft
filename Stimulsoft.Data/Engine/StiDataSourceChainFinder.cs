#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Stimulsoft.Data.Engine
{
    public static class StiDataSourceChainFinder
    {
        public static IEnumerable<IStiAppDataSource> Find(IEnumerable<IStiAppDataSource> dataSources, bool includeStartEnd = false)
        {
            var primaryTable = dataSources.First();
            
            var chainDataSources = dataSources
                .Skip(1)
                .Select(t => Find(primaryTable, t))
                .Where(t => t != null)
                .SelectMany(t => t)
                .Where(t => !dataSources.Contains(t))
                .Distinct();

            return includeStartEnd 
                ? dataSources.Union(chainDataSources).Distinct() 
                : chainDataSources;
        }

        public static IEnumerable<IStiAppDataSource> Find(IEnumerable<IStiAppDataSource> dataSources, string table1, string table2)
        {
            var t1 = dataSources.FirstOrDefault(d => d.GetName() == table1);
            var t2 = dataSources.FirstOrDefault(d => d.GetName() == table2);

            return Find(new List<IStiAppDataSource> { t1, t2 }, true);
        }

        private static IEnumerable<IStiAppDataSource> Find(IStiAppDataSource dataSource1, IStiAppDataSource dataSource2)
        {
            var parentPath = FindInParent(dataSource1, dataSource2);
            var childPath = FindInChild(dataSource1, dataSource2);

            if (parentPath == null && childPath == null)
                return null;

            if (parentPath != null && childPath == null)
                return parentPath;

            if (parentPath == null && childPath != null)
                return childPath;

            return parentPath.Count() >= childPath.Count() ? childPath : parentPath;
        }

        private static IEnumerable<IStiAppDataSource> FindInParent(IStiAppDataSource dataSource1, IStiAppDataSource dataSource2)
        {
            if (!dataSource1.FetchParentRelations(true).Any()) return null;

            var directRelations = dataSource1.FetchParentRelations(true).Where(r => r.GetParentDataSource() == dataSource2);
            if (directRelations.Any())
                return new List<IStiAppDataSource> { dataSource1, dataSource2 };

            foreach (var relation in dataSource1.FetchParentRelations(true).Where(r => r.GetActiveState()))
            {
                var path = FindInParent(relation.GetParentDataSource(), dataSource2)?.ToList();
                if (path == null) continue;

                path.Insert(0, relation.GetChildDataSource());
                return path;
            }

            return null;
        }

        private static IEnumerable<IStiAppDataSource> FindInChild(IStiAppDataSource dataSource1, IStiAppDataSource dataSource2)
        {
            if (!dataSource1.FetchChildRelations(true).Any())
                return FindInParent(dataSource1, dataSource2);//Try to find in parent (only for Child!)

            var onceRelation = dataSource1.FetchChildRelations(true).FirstOrDefault(r => r.GetChildDataSource() == dataSource2);
            if (onceRelation != null)
                return new List<IStiAppDataSource> { dataSource1, dataSource2 };

            foreach (var relation in dataSource1.FetchChildRelations(true).Where(r => r.GetActiveState()))
            {
                var path = FindInChild(relation.GetChildDataSource(), dataSource2)?.ToList();
                if (path == null) continue;

                path.Insert(0, relation.GetParentDataSource());
                return path;
            }

            return null;
        }
    }
}