#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using Stimulsoft.Data.Extensions;

#if NETCORE
using Stimulsoft.System.Data;
#endif

namespace Stimulsoft.Data.Engine
{
    public class StiDataRowJoiner
    {
        #region Methods
        public IEnumerable<DataRow> Join(StiDataJoinType type, StiDataLink link)
        {
            switch (type)
            {
                case StiDataJoinType.Inner:
                    return InnerJoinRows(link);

                case StiDataJoinType.Left:
                    return LeftJoinRows(link);

                case StiDataJoinType.Cross:
                    return CrossJoinRows();

                case StiDataJoinType.Full:
                    return FullJoinRows(link);

                default:
                    throw new NotSupportedException();
            }
        }

        private IEnumerable<DataRow> InnerJoinRows(StiDataLink link)
        {
            var fieldIndex1 = GetFieldIndex(table1, link);
            var fieldIndex2 = GetFieldIndex(table2, link);
            var rows1 = table1.AsEnumerable().ToList();
            var rows2 = table2.AsEnumerable().ToList();

            return rows1.Join(rows2,
                    r1 => GetHashCode(r1, fieldIndex1),
                    r2 => GetHashCode(r2, fieldIndex2),
                    SplitRows);
        }

        private IEnumerable<DataRow> LeftJoinRows(StiDataLink link)
        {
            var fieldIndex1 = GetFieldIndex(table1, link);
            var fieldIndex2 = GetFieldIndex(table2, link);
            var rows1 = table1.AsEnumerable().ToList();
            var rows2 = table2.AsEnumerable().ToList();

            return rows1.GroupJoin(rows2,
                r1 => GetHashCode(r1, fieldIndex1),
                r2 => GetHashCode(r2, fieldIndex2),
                (r1, r2) => new { key = r1, rows = r2 })
                .SelectMany(a => a.rows.DefaultIfEmpty(), (r1, r2) => SplitRows(r1.key, r2));
        }

        private IEnumerable<DataRow> CrossJoinRows()
        {
            var rows1 = table1.AsEnumerable().ToList();
            var rows2 = table2.AsEnumerable().ToList();

            return rows1.SelectMany(r1 => rows2.Select(r2 => SplitRows(r1, r2)));
        }

        private IEnumerable<DataRow> FullJoinRows(StiDataLink link)
        {
            var fieldIndex1 = GetFieldIndex(table1, link);
            var fieldIndex2 = GetFieldIndex(table2, link);
            var rows1 = table1.AsEnumerable().ToList();
            var rows2 = table2.AsEnumerable().ToList();

            return rows1
                .FullOuterJoin(rows2,
                    r1 => GetHashCode(r1, fieldIndex1),
                    r2 => GetHashCode(r2, fieldIndex2),
                    SplitRows);
        }

        private int GetHashCode(DataRow row, int fieldIndex)
        {
            return row[fieldIndex] != null ? row[fieldIndex].GetHashCode() : 0;
        }

        private DataRow SplitRows(DataRow row1, DataRow row2)
        {
            var values = new object[resultTable.Columns.Count];

            foreach (DataColumn column in resultTable.Columns)
            {
                var index = resultColumnIndexes.ContainsKey(column.ColumnName) ? resultColumnIndexes[column.ColumnName] : -1;
                if (index == -1) continue;

                var index1 = column1Indexes.ContainsKey(column.ColumnName) ? column1Indexes[column.ColumnName] : -1;
                var index2 = column2Indexes.ContainsKey(column.ColumnName) ? column2Indexes[column.ColumnName] : -1;

                if (index1 != -1 && row1 != null)
                    values[index] = row1[index1];

                if (index2 != -1 && row2 != null)
                    values[index] = row2[index2];
            }

            return resultTable.LoadDataRow(values.ToArray(), false);
        }

        private int GetFieldIndex(DataTable table, StiDataLink link)
        {
            var column = table.Columns
                .Cast<DataColumn>()
                .FirstOrDefault(c => c.ColumnName == link.ParentKey || 
                                     c.ColumnName == link.ChildKey || 
                                     c.Table.TableName + '.' + c.ColumnName == link.ParentKey ||
                                     c.Table.TableName + '.' + c.ColumnName == link.ChildKey);

            if (column == null) return -1;

            return table.Columns.IndexOf(column);
        }
        #endregion

        #region Fields
        private DataTable resultTable;
        private DataTable table1;
        private DataTable table2;
        private Dictionary<string, int> resultColumnIndexes = new Dictionary<string, int>();
        private Dictionary<string, int> column1Indexes = new Dictionary<string, int>();
        private Dictionary<string, int> column2Indexes = new Dictionary<string, int>();
        #endregion

        public StiDataRowJoiner(DataTable resultTable, DataTable table1, DataTable table2)
        {
            this.resultTable = resultTable;
            this.table1 = table1;
            this.table2 = table2;

            foreach (DataColumn column in resultTable.Columns)
            {
                resultColumnIndexes.Add(column.ColumnName, resultTable.Columns.IndexOf(column.ColumnName));
            }

            foreach (DataColumn column in table1.Columns)
            {
                column1Indexes.Add(column.ColumnName, table1.Columns.IndexOf(column.ColumnName));
            }

            foreach (DataColumn column in table2.Columns)
            {
                column2Indexes.Add(column.ColumnName, table2.Columns.IndexOf(column.ColumnName));
            }
        }
    }
}