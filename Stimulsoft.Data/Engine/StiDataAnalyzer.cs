#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base.Meters;
using System.Collections.Generic;
using System.Linq;

namespace Stimulsoft.Data.Engine
{
    public static class StiDataAnalyzer
    {
        #region Methods
        public static StiDataTable Analyse(IStiQueryObject query, IEnumerable<IStiMeter> meters, IEnumerable<StiDataFilterRule> filters = null)
        {
            if (meters == null || !meters.Any())
                return new StiDataTable();

            var tables = StiDataPicker.Fetch(query);
            if (tables == null || !tables.Any())
                return new StiDataTable();

            var joinedTable = StiDataJoiner.Join(tables, StiDataLinkHelper.GetLinks(query.GetDictionary()));
            if (joinedTable == null)
                return new StiDataTable();

            var filteredTable = StiDataFiltrator.Filter(joinedTable, filters);
            if (filteredTable == null)
                return new StiDataTable();

            return StiDataFinalizer.Finalize(query.GetDictionary(), joinedTable, meters.ToList());
        }
        #endregion
    }
}