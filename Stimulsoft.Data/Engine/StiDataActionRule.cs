﻿#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Dashboards											}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Data.Design;
using System;
using System.ComponentModel;
using System.Linq;

namespace Stimulsoft.Data.Engine
{
    [TypeConverter(typeof(StiDataActionConverter))]
    public class StiDataActionRule : IStiJsonReportObject
    {
        #region IStiJsonReportObject
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyEnum("Type", Type);
            jObject.AddPropertyStringNullOrEmpty("Key", Key);
            jObject.AddPropertyInt("StartIndex", StartIndex);
            jObject.AddPropertyInt("RowsCount", RowsCount, -1);
            jObject.AddPropertyStringNullOrEmpty("InitialValue", InitialValue);
            jObject.AddPropertyStringNullOrEmpty("ValueFrom", ValueFrom);
            jObject.AddPropertyStringNullOrEmpty("ValueTo", ValueTo);
            jObject.AddPropertyBool("MatchCase", MatchCase);

            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Type":
                        Type = (StiDataActionType)Enum.Parse(typeof(StiDataActionType), property.Value.ToObject<string>());
                        break;

                    case "Key":
                        Key = property.Value.ToObject<string>();
                        break;

                    case "StartIndex":
                        StartIndex = property.Value.ToObject<int>();
                        break;

                    case "RowsCount":
                        RowsCount = property.Value.ToObject<int>();
                        break;

                    case "InitialValue":
                        InitialValue = property.Value.ToObject<string>();
                        break;

                    case "ValueFrom":
                        ValueFrom = property.Value.ToObject<string>();
                        break;

                    case "ValueTo":
                        ValueTo = property.Value.ToObject<string>();
                        break;

                    case "MatchCase":
                        MatchCase = property.Value.ToObject<bool>();
                        break;
                }
            }
        }
        #endregion

        #region Properties
        [StiSerializable]
        public StiDataActionType Type { get; set; }

        [StiSerializable]
        [DefaultValue(null)]
        public string Key { get; set; }

        [StiSerializable]
        [DefaultValue(0)]
        public int StartIndex { get; set; }

        [StiSerializable]
        [DefaultValue(-1)]
        public int RowsCount { get; set; } = -1;

        [StiSerializable]
        [DefaultValue(null)]
        public string InitialValue { get; set; }

        [StiSerializable]
        [DefaultValue(null)]
        public string ValueFrom { get; set; }

        [StiSerializable]
        [DefaultValue(null)]
        public string ValueTo { get; set; }

        [StiSerializable]
        [DefaultValue(false)]
        public bool MatchCase { get; set; }
        #endregion

        #region Methods
        public static StiDataActionRule LoadFromJson(JObject json)
        {
            var link = new StiDataActionRule();
            link.LoadFromJsonObject(json);
            return link;
        }
        #endregion

        public StiDataActionRule()
        {
        }

        public StiDataActionRule(StiDataActionType type) : this(type, null)
        {
        }

        public StiDataActionRule(StiDataActionType type, string key)
        {
            Type = type;
            Key = key;
        }
    }
}
