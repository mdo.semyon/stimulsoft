#region Copyright (C) 2003-2018 Stimulsoft
/*
{*******************************************************************}
{																	}
{	Stimulsoft Reports												}
{	                         										}
{																	}
{	Copyright (C) 2003-2018 Stimulsoft     							}
{	ALL RIGHTS RESERVED												}
{																	}
{	The entire contents of this file is protected by U.S. and		}
{	International Copyright Laws. Unauthorized reproduction,		}
{	reverse-engineering, and distribution of all or any portion of	}
{	the code contained in this file is strictly prohibited and may	}
{	result in severe civil and criminal penalties and will be		}
{	prosecuted to the maximum extent possible under the law.		}
{																	}
{	RESTRICTIONS													}
{																	}
{	THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES			}
{	ARE CONFIDENTIAL AND PROPRIETARY								}
{	TRADE SECRETS OF Stimulsoft										}
{																	}
{	CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON		}
{	ADDITIONAL RESTRICTIONS.										}
{																	}
{*******************************************************************}
*/
#endregion Copyright (C) 2003-2018 Stimulsoft

using Stimulsoft.Base;
using Stimulsoft.Base.Json.Linq;
using Stimulsoft.Base.Serializing;
using Stimulsoft.Data.Design;
using System;
using System.ComponentModel;

namespace Stimulsoft.Data.Engine
{
    [TypeConverter(typeof(StiDataSortRuleConverter))]
    public class StiDataSortRule : IStiJsonReportObject
    {
        #region IStiJsonReportObject
        public virtual JObject SaveToJsonObject(StiJsonSaveMode mode)
        {
            var jObject = new JObject();

            jObject.AddPropertyStringNullOrEmpty("Key", Key);
            jObject.AddPropertyEnum("Direction", Direction, StiDataSortDirection.Ascending);

            return jObject;
        }

        public virtual void LoadFromJsonObject(JObject jObject)
        {
            foreach (var property in jObject.Properties())
            {
                switch (property.Name)
                {
                    case "Key":
                        this.Key = property.Value.ToObject<string>();
                        break;

                    case "Direction":
                        this.Direction = (StiDataSortDirection)Enum.Parse(typeof(StiDataSortDirection), property.Value.ToObject<string>());
                        break;
                }
            }
        }
        #endregion

        #region Methods
        public static StiDataSortRule LoadFromJson(JObject json)
        {
            var rule = new StiDataSortRule();
            rule.LoadFromJsonObject(json);
            return rule;
        }

        public override string ToString()
        {
            return $"{Direction} {Key}";
        }
        #endregion

        #region Properties
        [StiSerializable]
        [DefaultValue(null)]
        public string Key { get; set; }

        [StiSerializable]
        [DefaultValue(StiDataSortDirection.Ascending)]
        public StiDataSortDirection Direction { get; set; }
        #endregion

        public StiDataSortRule(string key, StiDataSortDirection direction)
        {
            this.Key = key;
            this.Direction = direction;
        }

        public StiDataSortRule() : this(null, StiDataSortDirection.Ascending)
        {
        }
    }
}